set ASSEMBLYINFO=.\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\Properties\AssemblyInfo.cs

for /F "tokens=2 delims=()" %%A in ('find "assembly: AssemblyVersion" %ASSEMBLYINFO%') do set REVISION=%%A
set REVISION=%REVISION:~0,-1%
set REVISION=%REVISION:~1,100%

set archname=UcrfRfa-%REVISION%

set instfolder=_instfolder

mkdir %instfolder%
cd %instfolder%
del /Q *.*
mkdir "Enums"
del /Q .\Enums\*.*


mkdir "Plugins"
del /Q .\Plugins\*.*

copy ..\Libs\UcrfRfaNET.Schema                 		.\ 
copy ..\bin\Release\NetPlugins2.dll			.\ 
copy ..\bin\Release\NPOI.OOXML.dll			.\ 
copy ..\bin\Release\NPOI.OpenXmlFormats.dll		.\ 
copy ..\bin\Release\NPOI.OpenXml4Net.dll		.\ 
copy ..\bin\Release\NPOI.dll				.\ 
copy ..\bin\Release\ICSharpCode.SharpZipLib.dll		.\ 
copy ..\ChangeLog\UcrfRfaNetChange.DOC                  			.\ 
copy ..\Mapconf\ICSM3.mapconf                  			.\ 
copy ..\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\Localization\XICSM_UcrfRfaNET_RUS.txt 			.\ 
copy ..\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\Localization\XICSM_UcrfRfaNET_CST.txt 			.\ 
copy ..\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\Localization\SYS_CST.txt 			.\ 
copy ..\bin\Release\ArticleLib.dll                      			.\
copy ..\bin\Release\AMS.Profile.dll      					.\
copy ..\bin\Release\AxInterop.MapXLib.5.0.dll           			.\
copy ..\bin\Release\Interop.MapXLib.5.0.dll           			.\
copy ..\bin\Release\ComponentsLib.dll    					.\ 
copy ..\bin\Release\DocRecognizer.dll                   			.\ 
copy ..\bin\Release\FC81Wrapper.dll                     			.\ 
copy ..\bin\Release\Idwm.NET.dll                        			.\ 
copy ..\bin\Release\Interop.RSAGeography.dll            			.\ 
copy ..\bin\Release\Interop.LISBCProtect.dll            			.\ 
copy ..\bin\Release\Interop.LISBCTxServer.dll           			.\ 
copy ..\bin\Release\Interop.PiFolio.dll                 			.\ 
copy ..\bin\Release\Interop.Protect.dll                 			.\ 
copy ..\bin\Release\LisLocalizationLib.dll              			.\ 
copy ..\bin\Release\LIS_ICSM_Math.dll                   			.\ 
copy ..\bin\Release\LisUtility.dll                              		.\ 
copy ..\bin\Release\LisCommonLib.dll                           			.\ 
copy ..\bin\Release\LisMapWf.dll                           			.\ 
copy ..\bin\Release\OfficeBridge.dll                    			.\ 
copy ..\bin\Release\System.Data.SQLite.dll              			.\ 
copy ..\bin\Release\StatusDivision.dll                  			.\ 
copy ..\bin\Release\WPFToolkit.dll                      			.\ 
copy ..\bin\Release\XICSM_Intermodulation.dll               			.\ 
copy ..\bin\Release\XICSM_LatinPlugin.dll               			.\ 
copy ..\bin\Release\XICSM_T11Plugin.dll                 			.\ 
copy ..\bin\Release\XICSM_OPORA.dll                     			.\ 
copy ..\bin\Release\XICSM_UcrfRfaNET.dll                			.\ 
copy ..\UcrfaNetPlugin\Assemblies\ZedGraph.dll                  		.\ 
copy ..\UcrfaNetPlugin\Assemblies\System.Windows.Forms.DataVisualization.dll    .\ 
copy ..\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\CompLib\CheckBoxComboBox.dll				.\ 
copy ..\UcrfaNetPlugin\UcrfRfaNET\UcrfRfaNET\HelpClasses\XML\SettingGSFields.xml				.\ 
copy ..\Idwm\Libs\DFORRT.DLL                            			.\ 
copy ..\Idwm\Libs\Geopnt1.bin                           			.\ 
copy ..\Idwm\Libs\Geoidx1.bin                           			.\ 
copy ..\Idwm\Libs\IDWM32D.DLL                           			.\ 
copy ..\Libs\LISBCProtect.dll                                   		.\ 
copy ..\Libs\LISBCTxServer.dll                                  		.\ 
copy ..\Libs\LisIdwm.dll                                        		.\ 
copy ..\Libs\Protect.dll                                        		.\ 
copy ..\Libs\RSAGeography.dll                                   		.\ 
rem copy ..\FC81Wrapper\obj\Release\Interop.FCEngine.dll        		    .\ 
copy ..\DocRecognizer\obj\Release\DocRecognizer.dll        			    .\ 
rem copy ..\AuxiliaryFiles\reg_calcICSM.bat                         		.\ 
rem copy ..\AuxiliaryFiles\reg_mapICSM.bat                          		.\ 
rem copy ..\AuxiliaryFiles\unreg_calcICSM.bat                       		.\ 
rem copy ..\AuxiliaryFiles\unreg_mapICSM.bat                        		.\ 
copy ..\Libs\zlib.dll             
                                                                .\

copy ..\EriFiles\*.*      		                        .\Enums\
copy ..\Plugins\*.*      		                        .\Plugins\

      
rar a -dh -m5 -r -sfx %archname%.exe *
copy %archname%.exe .\..
del %archname%.exe
cd ..