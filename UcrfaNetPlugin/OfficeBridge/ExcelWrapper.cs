﻿using System;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;


namespace Lis.OfficeBridge
{
    /* 
        using (ExcelBridge excel = new ExcelBridge())
        {
            excel.Init();
            excel.DisplayAlerts(false);
            excel.AddWorkbook();
            excel.SetCellValue(1, 2, "SSSS");
            excel.
     * egroundColor(1, 2, Color.Yellow);
            excel.SetCellBackgroundColor(1, 2, Color.Red);
            excel.SetCellFormat(1, 3, CellFormat.ExcelTextFormat);
            excel.SetCellValue(1, 3, "009922");
            excel.Save("C:\\xxx.xls");
            excel.Close();
        }
    */

    public enum CellFormat
    {
        ExcelTextFormat
    };

    public class ExcelBridge : IDisposable
    {
        private Type _excelTypeApplication = null;
        private object _excelObject = null;
        private object _oWorkbooks = null;
        private object _oWorkbook = null;
        private object _oWorksheet = null;
        private bool _disposed = false;
        
        // Освободить "книгу" 
        private void ReleaseWorkbook()
        {
            if (_oWorkbook != null)
            {
                Marshal.ReleaseComObject(_oWorkbook);
                _oWorkbook = null;
            }
        }

        // осовбодить коллекцию "книг"
        private void ReleaseWorkbooks()
        {
            if (_oWorkbooks != null)
            {
                Marshal.ReleaseComObject(_oWorkbooks);
                _oWorkbooks = null;
            }
        }

        // Освободить "лист" - закладку на "книге"
        private void ReleaseWorksheet()
        {
            if (_oWorksheet != null)
            {
                Marshal.ReleaseComObject(_oWorksheet);
                _oWorksheet = null;
            }
        }

        /// <summary>
        /// Создать Excel automation server
        /// </summary>
        /// <returns>true - если успешно</returns>
        public bool Init()
        {
            const string applName = "Excel.Application";
            
            _excelTypeApplication = Type.GetTypeFromProgID(applName);
            _excelObject = Activator.CreateInstance(_excelTypeApplication);
            if (_excelObject!=null)
                _oWorkbooks = _excelObject.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, _excelObject, null);
            return _excelObject != null;
        }

        /// <summary>
        /// Перед использованием нужно добавить книгу.
        /// У каждой книги по умолчанию присуствует один лист
        /// </summary>
        public void AddWorkbook(string NameSheet)
        {
            ReleaseWorkbook();
            ReleaseWorksheet();
            GC.GetTotalMemory(true); // Вызываем сборщик мусора для немедленной очистки памяти
            _oWorkbook = _oWorkbooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, _oWorkbooks, null);
            _oWorksheet = _oWorkbook.GetType().InvokeMember("ActiveSheet", BindingFlags.GetProperty, null, _oWorkbook, null);
        }

        /// <summary>
        /// Включить/выключить алерты, например для того, чтобы не спрашивало
        /// хотите ли перезаписать файл и т.д.
        /// </summary>        
        public void DisplayAlerts(bool display)
        {
            object[] args = new object[] { display };
            if (_excelObject!=null)
                _excelObject.GetType().InvokeMember("DisplayAlerts", BindingFlags.SetProperty, null, _excelObject, args);
        }
        
        /// <summary>
        /// Установка ширины столбцов
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Width"></param>

        public void SetColumnWidth(int col, int length_row)
        {
            //
        }

        public void SetColumnWidth(int col, int row, double Width)
        {
            object oRange = null;

            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("ColumnWidth", BindingFlags.SetProperty, null, oRange, new object[] { Width });

                   
                    //object[] args = new object[] { "A1", "Q1" };
                   


                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }

        }

        /// <summary>
        /// Установка направления текста
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Orientation"></param>
        public void SetTextOrientation(int col, int row, int Orientation)
        {
           
            object oRange = null;

            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("Orientation", BindingFlags.SetProperty, null, oRange, new object[] { Orientation });


                    //object[] args = new object[] { "A1", "Q1" };



                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }

        }

      
        /// <summary>
        /// Выравнивание текста в ячейке по вертикали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetVerticalAlignment(int col, int row, int Alignment)
        {
            object oRange = null;

            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("VerticalAlignment", BindingFlags.SetProperty, null, oRange, new object[] { Alignment });
                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Выравнивание текста в ячейке по горизонтали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetHorisontalAlignment(int col, int row, int Alignment)
        {
          
            object oRange = null;

            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("HorizontalAlignment", BindingFlags.SetProperty, null, oRange, new object[] { Alignment });
                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }
        }
           
       
        /// <summary>
        /// Установка высоты строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Height"></param>
        public void SetRowHeight(int col, int row, double Height)
        {
           object oRange = null;
            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("RowHeight", BindingFlags.SetProperty, null, oRange, new object[] { Height });
                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }
        }


        /// <summary>
        /// Объединение ячеек
        /// </summary>
        /// <param name="col1"></param>
        /// <param name="row1"></param>
        /// <param name="col2"></param>
        /// <param name="row2"></param>
        public void UnionCell(int col1, int row1, int col2, int row2)
        {
            object oRange = null;
            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col1 + 0x40) + row1.ToString(), Convert.ToChar(col2 + 0x40) + row2.ToString() };
                    //object[] args = new object[] { "A1", "Q1" };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    oRange.GetType().InvokeMember("MergeCells", BindingFlags.SetProperty, null, oRange, new object[] { true });

                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {
               
            }

        }

        public void UnionCell(int col1, int row1, int col2, int row2, ICellStyle st, string val)
        {

        }

        /// <summary>
        /// Установить размер шрифта
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetCellFontSize(int col, int row,string Name, int Size)
        {
            object oRange = null;
            object Font = null;
        
            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    Font = oRange.GetType().InvokeMember("Font", BindingFlags.GetProperty, null, oRange, null);
                    oRange.GetType().InvokeMember("Name", BindingFlags.SetProperty, null, Font, new object[] { Name });
                    oRange.GetType().InvokeMember("Size", BindingFlags.SetProperty, null,  Font, new object[] { Size });
                    oRange.GetType().InvokeMember("WrapText", BindingFlags.SetProperty, null, oRange, new object[] { true });

                    

                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                    if (Font != null)
                        Marshal.ReleaseComObject(Font);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetBorderStyle(int col, int row, int Style)
        {
             object oRange = null;
        
            try
            {
                try
                {
            object[] args = new object[] { Convert.ToChar(col + 0x40) + row.ToString(), Convert.ToChar(col + 0x40) + row.ToString() };
            oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
            object[] args1 = new object[] { Style };
            object Borders = oRange.GetType().InvokeMember("Borders", BindingFlags.GetProperty, null, oRange, null);
            Borders = oRange.GetType().InvokeMember("LineStyle", BindingFlags.SetProperty, null, Borders, args1);


                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col1"></param>
        /// <param name="row1"></param>
        /// <param name="col2"></param>
        /// <param name="row2"></param>
        /// <param name="Size"></param>
        public void SetBorderStyle(int col1, int row1, int col2, int row2, int Style)
        {
            object oRange = null;

            try
            {
                try
                {
                    object[] args = new object[] { Convert.ToChar(col1 + 0x40) + row1.ToString(), Convert.ToChar(col2 + 0x40) + row2.ToString() };
                    oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                    object[] args1 = new object[] { Style };
                    object Borders = oRange.GetType().InvokeMember("Borders", BindingFlags.GetProperty, null, oRange, null);
                    Borders = oRange.GetType().InvokeMember("LineStyle", BindingFlags.SetProperty, null, Borders, args1);


                }
                finally
                {
                    if (oRange != null)
                        Marshal.ReleaseComObject(oRange);
                }
            }
            catch (Exception ex)
            {

            }
        } 
        
        /// <summary>
        /// Установить формат для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="cellFormat">формат ячейки</param>
        public void SetCellFormat(int col, int row, CellFormat cellFormat)
        {
            object oRange = null;
            object oSelection = null;
            try
            {
                string cellName = Convert.ToChar(col + 0x40) + row.ToString();
                object[] args = new object[] { cellName, cellName };

                oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                oRange.GetType().InvokeMember("Select", BindingFlags.InvokeMethod, null, oRange, null);

                object[] args2 = new object[1];
                switch (cellFormat)
                {
                    case CellFormat.ExcelTextFormat:
                        args2[0] = "@";
                        break;
                }

                oSelection = _excelObject.GetType().InvokeMember("Selection", BindingFlags.GetProperty, null, _excelObject, null);
                oSelection.GetType().InvokeMember("NumberFormat", BindingFlags.SetProperty, null, oSelection, args2);
            }
            finally
            {
                if (oSelection!=null)
                    Marshal.ReleaseComObject(oSelection);
                if (oRange!=null)
                    Marshal.ReleaseComObject(oRange);
            }
        }

        /// <summary>
        ///  Установить содержимое ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="val">Содержимое ячейки</param>
        public void SetCellValue(int col, int row, string val)
        {
            object oRange = null;
            try
            {
                string cellName = Convert.ToChar(col + 0x40) + row.ToString();
                object[] args = new object[] { cellName, cellName };

                oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                oRange.GetType().InvokeMember("Value2", BindingFlags.SetProperty, null, oRange, new object[] { val });
           
            }
            finally
            {
                if (oRange!=null)
                    Marshal.ReleaseComObject(oRange);
            }
        }
        /// <summary>
        /// Установить цвет фона для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет фона</param>
        public void SetCellBackgroundColor(int col, int row, Color color)
        {
            object oRange = null;
            object oSelection = null;
            object oInterior = null;
            try
            {
                string cellName = Convert.ToChar(col + 0x40) + row.ToString();
                object[] args = new object[] { cellName, cellName };

                oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                oRange.GetType().InvokeMember("Select", BindingFlags.InvokeMethod, null, oRange, null);

                oSelection = _excelObject.GetType().InvokeMember("Selection", BindingFlags.GetProperty, null, _excelObject, null);
                oInterior = oSelection.GetType().InvokeMember("Interior", BindingFlags.GetProperty, null, oSelection, null);

                object[] args2 = new object[] { ColorTranslator.ToOle(color) };
                oInterior.GetType().InvokeMember("Color", BindingFlags.SetProperty, null, oInterior, args2);
            }
            finally
            {
                if (oInterior!=null)
                    Marshal.ReleaseComObject(oInterior);
                if (oSelection!=null)
                    Marshal.ReleaseComObject(oSelection);
                if (oRange!=null)
                    Marshal.ReleaseComObject(oRange);
            }
        }

        /// <summary>
        /// Установить цвет фона шрифта ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет шрифта</param>
        public void SetCellForegroundColor(int col, int row, Color color)
        {
            object oRange = null;
            object oSelection = null;
            object oFont = null;

            try
            {
                string cellName = Convert.ToChar(col + 0x40) + row.ToString();
                object[] args = new object[] { cellName, cellName };

                oRange = _oWorksheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, _oWorksheet, args);
                oRange.GetType().InvokeMember("Select", BindingFlags.InvokeMethod, null, oRange, null);

                oSelection = _excelObject.GetType().InvokeMember("Selection", BindingFlags.GetProperty, null, _excelObject, null);
                oFont = oSelection.GetType().InvokeMember("Font", BindingFlags.GetProperty, null, oSelection, null);

                object[] args2 = new object[] { ColorTranslator.ToOle(color) };
                oFont.GetType().InvokeMember("Color", BindingFlags.SetProperty, null, oFont, args2);
            }
            finally
            {
                if (oFont!=null)
                    Marshal.ReleaseComObject(oFont);
                if (oSelection!=null)
                    Marshal.ReleaseComObject(oSelection);
                if (oRange!=null)
                    Marshal.ReleaseComObject(oRange);
            }
        }

        /// <summary>
        /// Закрыть Excel
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            ReleaseWorksheet();
            ReleaseWorkbook();
            ReleaseWorkbooks();

            if (_excelObject != null)
            {                
                _excelObject.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, _excelObject, null);
                
                Marshal.ReleaseComObject(_excelObject); // Уничтожение объекта Excel.
                _excelObject = null;
            }
            GC.GetTotalMemory(true); // Вызываем сборщик мусора для немедленной очистки памяти
            return true;
        }

        /// <summary>
        /// Сохранить Excel файл
        /// </summary>
        /// <param name="fileName">имя файла</param>
        public void Save(string fileName)
        {
            object[] args = new object[6];

            args[0] = fileName;
            args[1] = 1;
            args[2] = "";
            args[3] = "";
            args[4] = false;
            args[5] = false;
            
            _oWorkbook.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, _oWorkbook, args);            
        }

        public ICellStyle GetCellStyleBorder()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleHead()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleBackgroundColor()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleBackgroundDefaultColor()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleDefault()
        {
            ICellStyle cr = null;
            return cr;
        }

        public void SetCellStyle(int col, int row, ICellStyle st)
        {
            //
        }

        public void SetBackgroundColor()
        {
            //
        }

        public void SetBorderStyle()
        {
            //
        }

        public void SetBorderStyleDefault()
        {
            //
        }

        #region Implementation of IDisposable

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Close();
                }               
 
                // Note disposing has been done.
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
