﻿using System;
using System.Reflection;

namespace Lis.OfficeBridge
{
    /// <summary>
    /// Класс для возврата ошибок
    /// </summary>
    public sealed class WordBridgeException : Exception
    {
        public WordBridgeException(string message) : base(message) { }
        public WordBridgeException(string message, Exception ex) : base(message, ex) { }
    }

    public class WordBridge : IDisposable
    {
        /// <summary>
        /// Парметры формата сохранения файла
        /// </summary>
        public enum WdSaveFormatOffice
        {
            WdFormatDocument = 0,  //Microsoft Office Word format.
            WdFormatDocument97 = 0,  //Microsoft Word 97 document format.
            WdFormatTemplate = 1,  //Word template format.
            WdFormatTemplate97 = 1,  //Word 97 template format.
            WdFormatText = 2,  //Microsoft Windows text format.
            WdFormatTextLineBreaks = 3,  //Windows text format with line breaks preserved.
            WdFormatDosText = 4,  //Microsoft DOS text format.
            WdFormatDosTextLineBreaks = 5,//Microsoft DOS text with line breaks preserved.
            WdFormatRtf = 6,  //Rich text format (RTF).
            WdFormatEncodedText = 7,  //Encoded text format.
            WdFormatUnicodeText = 7,  //Unicode text format.
            WdFormatPdf = 7,  //PDF format.
            WdFormatHtml = 8,  //Standard HTML format.
            WdFormatWebArchive = 9,  //Web archive format.
            WdFormatFilteredHtml = 10, //Filtered HTML format.
            WdFormatXml = 11, //Extensible Markup Language (XML) format.
            WdFormatXmlDocument = 12, //XML document format.
            WdFormatXmlDocumentMacroEnabled = 13,//XML document format with macros enabled.
            WdFormatXmlTemplate = 14, //XML template format.
            WdFormatXmlTemplateMacroEnabled = 15, //XML template format with macros enabled.
            WdFormatDocumentDefault = 16, //Word default document file format. For Microsoft Office Word 2007, this is the DOCX format.
            WdFormatXps = 18, //XPS format.
        };
        private object _internalWord = null;
        private object _objCurDoc = null;
        /// <summary>
        /// Конструктор
        /// </summary>
        public WordBridge()
        {
            _internalWord = null;
            _objCurDoc = null;
        }
        /// <summary>
        /// Деструктор
        /// </summary>
        ~WordBridge()
        {
            Dispose(false);
        }
        /// <summary>
        /// Инициализирует Word
        /// </summary>
        /// <exception cref="WordBridgeException"/>
        public void Init()
        {
            try
            {
                Type objClassType = Type.GetTypeFromProgID("Word.Application");
                _internalWord = Activator.CreateInstance(objClassType);
            }
            catch (Exception ex)
            {
                _internalWord = null;
                throw new WordBridgeException(ex.Message);
            }
        }
        /// <summary>
        /// Закрывает активный документ
        /// </summary>
        /// <exception cref="WordBridgeException"/>
        public void CloseDoc()
        {
            if (_objCurDoc != null)
            {
                //Close Active Document
                _objCurDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, _objCurDoc, null);
                _objCurDoc = null;
            }
        }
        /// <summary>
        /// Закрываем приложение Word
        /// </summary>
        /// <exception cref="WordBridgeException"/>
        public void Quit()
        {
            if (_internalWord != null)
            {
                _internalWord.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, _internalWord, null);
                _internalWord = null;
            }
        }
        /// <summary>
        /// Отображает / скрывает приложение
        /// </summary>
        /// <param name="isVisible">TRUE - отображает / FALSE - скрывает приложение</param>
        /// <exception cref="WordBridgeException"/>
        public void ShowWord(bool isVisible)
        {
            if (_internalWord == null)
                throw new WordBridgeException("Application was not initialized");
            object[] parameters = new Object[1];
            parameters[0] = isVisible;
            _internalWord.GetType().InvokeMember("Visible", BindingFlags.SetProperty, null, _internalWord, parameters);
        }
        /// <summary>
        /// Открывает документ
        /// </summary>
        /// <param name="fileName">Файл, который необходимо открыть</param>
        /// <exception cref="WordBridgeException"/>
        public void OpenFile(string fileName)
        {
            if (_internalWord == null)
                throw new WordBridgeException("Application was not initialized");
            object objDocsLate = _internalWord.GetType().InvokeMember("Documents", BindingFlags.GetProperty, null, _internalWord, null);
            object[] parameters = new Object[1];
            parameters[0] = fileName;
            _objCurDoc = objDocsLate.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, objDocsLate, parameters);
        }

        /// <summary>
        /// Открывает документ только для чтения
        /// </summary>
        /// <param name="fileName">Файл, который необходимо открыть</param>
        /// <exception cref="WordBridgeException"/>
        public void OpenFileReadOnly(string fileName)
        {
            if (_internalWord == null)
                throw new WordBridgeException("Application was not initialized");
            object objDocsLate = _internalWord.GetType().InvokeMember("Documents", BindingFlags.GetProperty, null, _internalWord, null);
            object[] parameters = new Object[4];
            parameters[0] = fileName;
            parameters[1] = false;
            parameters[2] = true;
            parameters[3] = false;
            _objCurDoc = objDocsLate.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, objDocsLate, parameters);
        }

        /// <summary>
        /// Сохраняет документ
        /// </summary>
        /// <exception cref="WordBridgeException"/>
        public void SaveDocument()
        {
            if (_objCurDoc == null)
                throw new WordBridgeException("Document was not initialized");
            _objCurDoc.GetType().InvokeMember("Save", BindingFlags.InvokeMethod, null, _objCurDoc, null);
        }
        /// <summary>
        /// Сахраняет файл в определенном формате
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="saveFormat">Формат сохранения</param>
        /// <exception cref="WordBridgeException"/>
        public void SaveAs(string filename, WdSaveFormatOffice saveFormat)
        {
            if (_objCurDoc == null)
                throw new WordBridgeException("Document was not initialized");
            object[] parameters = new Object[2];
            parameters[0] = filename;
            parameters[1] = saveFormat;
            _objCurDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, _objCurDoc, parameters);
        }

        [Obsolete("Не протестирована", true)]
        public bool FindReplace(string findtext, string replacetext)
        {
            try
            {
                object objSelection = _internalWord.GetType().InvokeMember(
                  "Selection", BindingFlags.GetProperty, null, _internalWord, null);

                object[] parameters = new Object[2];
                parameters[0] = 0;
                parameters[1] = 0;

                objSelection.GetType().InvokeMember(
                  "SetRange", BindingFlags.InvokeMethod, null, objSelection, parameters);

                object objFind = objSelection.GetType().InvokeMember(
                  "Find", BindingFlags.GetProperty, null, objSelection, null);

                parameters = new Object[11];
                parameters[0] = findtext; //find text
                parameters[1] = false; //match case
                parameters[2] = true; //match whole word
                parameters[3] = false; //match wild card
                parameters[4] = false; //match sounds like
                parameters[5] = false; //match all word forms
                parameters[6] = true; //forward
                parameters[7] = false; //wrap
                parameters[8] = false; //format
                parameters[9] = replacetext; //replace text
                parameters[10] = 2; //replace?
                objFind.GetType().InvokeMember("Execute",
                    BindingFlags.InvokeMethod, null, objFind, parameters);
                return true;
            }
            catch
            {
                return false;
            }

        }

        [Obsolete("Не протестирована", true)]
        public bool PrintPreview()
        {
            if (_objCurDoc == null) return false;
            try
            {
                _objCurDoc.GetType().InvokeMember("PrintPreview",
                    BindingFlags.InvokeMethod, null, _objCurDoc, null);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //[Obsolete("Не протестирована", true)]
        public bool Print()
        {
            if (_objCurDoc == null) return false;
            try
            {
                object[] parameters = new Object[15];
                parameters[0] = false; //in bacground?
                parameters[1] = Missing.Value; //append
                parameters[2] = 0; //all document wdPrintAllDocument
                parameters[3] = Missing.Value; //Output file name
                parameters[4] = Missing.Value; //From
                parameters[5] = Missing.Value; //To page
                parameters[6] = Missing.Value; //Item?
                parameters[7] = 1; //Copies
                parameters[8] = Missing.Value; //Pages
                parameters[9] = Missing.Value; //Page type?
                parameters[10] = false; //print to file
                parameters[11] = false; //collate
                parameters[12] = Missing.Value; //only for MAC
                parameters[13] = Missing.Value; //Manual duplex
                parameters[14] = Missing.Value; //collate


                _objCurDoc.GetType().InvokeMember("PrintOut",
                    BindingFlags.InvokeMethod, null, _objCurDoc, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #region IDisposable methods
        private bool _disposed = false;
        // Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }
                // Call the appropriate methods to clean up unmanaged resources here.
                if (_objCurDoc != null)
                    CloseDoc();
                if (_internalWord != null)
                    Quit();
                // Note disposing has been done.
                _disposed = true;
            }
        }
        #endregion
    }
}
