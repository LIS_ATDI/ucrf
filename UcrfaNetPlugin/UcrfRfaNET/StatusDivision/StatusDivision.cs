﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.StationsStatus
{
   /// <summary>
   /// Класс определяет функции для загрузки статусов, 
   /// которые отчечают за определенные действия над станцией
   /// Например:
   ///  - статусы станций, которые принимают участвие в расчетах ЕМС
   /// </summary>
   public class StatusDivision
   {
      public const string Status = "XNRFA_STATUS";

      public enum Division
      {
         /// <summary>
         /// принимает участвие в расчетах
         /// </summary>
         EMC,   // Для подсчетов ЕМС
         BASE,  // Используються как базовые
      }

      private static List<string> _statusForEms;
      private static List<string> _statusForAbonent;
      /// <summary>
      /// Статический конструктор
      /// </summary>
      static StatusDivision()
      {
         _statusForEms = GetStatusForSmth(Division.EMC);
         _statusForAbonent = GetStatusForSmth(Division.BASE);
      }
      /// <summary>
      /// Проверяет участвие статуса в расчетах ЕМС
      /// </summary>
      /// <param name="status">Код статуса станции</param>
      /// <returns>TRUE если статус участвует в расчетах, иначе FALSE</returns>
      public static bool CanUseForCalculationEms(string status)
      {         
         return _statusForEms.Contains(status);
      }
      /// <summary>
      /// Проверяет статус на возможность быть базовой станцией для абонента
      /// </summary>
      /// <param name="status">Код статуса станции</param>
      /// <returns>TRUE если может быть базовой станцией для абонента, иначе FALSE</returns>
      public static bool CanUseForAbonentStation(string status)
      {
          return _statusForAbonent.Contains(status);
      }
      /// <summary>
      /// Возвращает список статусов станций, которые участвуют в расчетах EMC
      /// (необходиома для внешних сборок)
      /// </summary>
      /// <returns>список статусов станций, которые участвуют в расчетах EMC</returns>
      public static List<string> GetStatusForEms()
      {
         return GetStatusForSmth(Division.EMC);
      }
      //===================================================
      /// <summary>
      /// Возвращает статусы для определенного действия
      /// </summary>
      /// <param name="div">Код действия</param>
      /// <returns>Список статусов</returns>
      private static List<string> GetStatusForSmth(Division div)
      {
         List<string> retVal = new List<string>();
         IMRecordset rs = new IMRecordset(Status, IMRecordset.Mode.ReadOnly);
         try
         {
            rs.Select("ID,STATUS,WHERE_IS_USED");
            rs.SetWhere("WHERE_IS_USED", IMRecordset.Operation.Like, string.Format("*{0}*", div.ToString()));
            for(rs.Open(); !rs.IsEOF(); rs.MoveNext())
               retVal.Add(rs.GetS("STATUS"));
         }
         finally
         {
            if(rs.IsOpen() == true)
               rs.Close();
            rs.Destroy();
         }
         return retVal;
      }
   }
}
