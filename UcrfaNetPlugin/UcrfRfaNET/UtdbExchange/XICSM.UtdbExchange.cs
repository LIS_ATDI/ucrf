﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XICSM.UcrfRfaNET.GlobalDB;
using XICSM.UcrfRfaNET;
using ICSM;
using System.Windows.Forms;
using System.Collections;
using System.IO;
namespace XICSM.UtdbExchange
{ 
    //class PlanSystItem
    //    {
    //    //enum type
    //    //{
    //    //    A,
    //    //    B
    //    //}
    //        public string action { get; set; }
    //        public string bandId {get; set; }
    //        public int idA { get; set; }
    //        public int idB {get; set;}
    //        public bool delA { get; set; }
    //        public bool delB{ get; set; }
    //        public PlanSystItem(string action)
    //        {
    //            this.action = action;
    //            delA = false;
    //            delB = false;
    //            idA = -1;
    //            idB = -1;
    //        }
    //        public void AddDelBand ( string role, int id)
    //        {
    //            this.action = action;
    //            if (role.Contains("A"))
    //            {
    //                delA = true;
    //                delB = false;
    //                idA = id;
    //            }
    //            else
    //            {
    //                delA = false;
    //                delB = true;
    //                idB = id;
    //            }
    //        }
    //        public bool ContainsBand(int id)
    //        {
    //            if (idA == id||idB == id)
    //                return true;
    //            return false;
    //        }

    //        //public string GetTypeToString()
    //        //{
    //        //    return banbType == type.A ? "A" : "B";
    //        //}

    //    }
    public class Plugin : IPlugin
    {
        XmlDocument docRadioSystems;
        XmlDocument docPlanSyst;
        XmlDocument docPlanBand;


        void LoadData()
        {
            docRadioSystems = null;
            docPlanSyst = null;
            //Dictionary<int, string> RadioSystemsID = new Dictionary<int, string>();
            //RadioSystemsID.Clear();
            List<int> RadioSystemsID = new List<int>();
            List<string> RadioSystemsAction = new List<string>();
            //List<int> PlanSystID = new List<int>();
            //List<string> PlanSystAction = new List<string>();
            Dictionary<int, string> PlanSyst = new Dictionary<int, string>();
            List<int> PlanBandID = new List<int>();
            List<string> PlanBandAction = new List<string>();
            List<int> delBand = new List<int>();
            IMRecordset r = new IMRecordset("XUTDB_X_CACHE", IMRecordset.Mode.ReadWrite);
            r.Select("OBJ_TABLE,OBJ_ID,DATE_CACHED,ACTION,INFO,STATUS");
            r.SetWhere("STATUS", IMRecordset.Operation.Eq, 0);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {

                string Table = r.GetS("OBJ_TABLE");
                if (Table == "RADIO_SYSTEMS") //составляем перечень последних изменений в каждой таблице по айди
                {
                    if (RadioSystemsID.Contains(r.GetI("OBJ_ID")) == false)// при множественных изменениях записи айди должен сохраняться только раз.
                    {
                        RadioSystemsID.Add(r.GetI("OBJ_ID"));
                        RadioSystemsAction.Add(r.GetS("ACTION"));
                    }
                    else if (r.GetS("ACTION") == "D")
                    {
                        int index = RadioSystemsID.IndexOf(r.GetI("OBJ_ID"));
                        RadioSystemsAction[index] = r.GetS("ACTION");// при множественном изменении записи сохраняется только первое действие(если удаление и редакирование то так и нужно). Если последним действием было удаление, то нужно его поставить.
                    }
                }
                else if (Table == "NATFPLAN_SYST")
                {
                    if (PlanSyst.ContainsKey(r.GetI("OBJ_ID")) == false)
                    {
                        PlanSyst.Add(r.GetI("OBJ_ID"), r.GetS("ACTION"));
                    }
                    else if (r.GetS("ACTION") == "D")
                    {
                        PlanSyst[r.GetI("OBJ_ID")] = r.GetS("ACTION");
                    }
                }
                else if (Table == "NATFPLAN_BAND")
                {
                    if (PlanBandID.Contains(r.GetI("OBJ_ID")) == false && r.GetS("ACTION") != "D")
                    {
                        PlanBandID.Add(r.GetI("OBJ_ID"));
                        PlanBandAction.Add(r.GetS("ACTION"));
                    }
                    else if (r.GetS("ACTION") == "D")
                    {
                        if (!delBand.Contains(r.GetI("OBJ_ID")))
                            delBand.Add(r.GetI("OBJ_ID"));

                    }


                }
                else
                {
                    IMRecordset rs = new IMRecordset("XUTDB_X_LOG", IMRecordset.Mode.ReadWrite);
                    rs.Select("INFO, EVENT, ACTION, DIRECTION, EVENT_DATE, OBJ_ID, OBJ_TABLE,");

                    rs.Put("OBJ_TABLE", Table);
                    rs.Put("OBJ_ID", r.GetI("OBJ_ID"));
                    rs.Put("ACTION", r.GetS("ACTION"));
                    rs.Put("EVENT_DATE", DateTime.Today);
                    rs.Put("EVENT", r.GetS("ACTION"));
                    rs.Put("INFO", Table + " не соответствует ни одной сущности");
                    rs.Update();
                    rs.Close();
                }
                r.Edit();
                r.Put("STATUS", 1);
                r.Update();


            }
            r.Close();
            //Эти хитрые манипуляции призваны избежать повторений в случае одновременного изменения таблиц NATFPLAN_SYST и NATFPLAN_BAND
            if (PlanBandID.Count() > 0)
            {
                for (int i = 0; i < PlanBandID.Count(); i++)
                {
                    IMRecordset rs = new IMRecordset("NATFPLAN_BAND", IMRecordset.Mode.ReadWrite);
                    rs.Select("NFPS_ID,ROLE");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, PlanBandID[i]);
                    rs.Open();

                    if (!PlanSyst.ContainsKey(rs.GetI("NFPS_ID")))
                    {
                        PlanSyst.Add(rs.GetI("NFPS_ID"), "");

                    }
                }
            }
            //


            if (RadioSystemsID.Count() > 0)
            {
                docRadioSystems = new XmlDocument();
                docRadioSystems.AppendChild(docRadioSystems.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
                docRadioSystems.AppendChild(docRadioSystems.CreateNode(XmlNodeType.Element, "ROOT", ""));
                for (int i = 0; i < RadioSystemsID.Count(); i++)
                {
                    IMRecordset rs = new IMRecordset("RADIO_SYSTEMS", IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,NAME,DESCRIPTION,EFIS_NAME,SONS,SERVICE,PARAMETERS,ICST_TYPE,ICST_SIGNAL,RS_MEMO,CUST_TXT1,CUST_TXT2,CUST_TXT3,SERVICE_ID,SERVICE_NAME,SERVICE_DESC,SERVICE_PARENT,SERVICE_LEVEL,SERVICE_CODE,SERVICE_NATURAL,SERVICE_APPARATUS,SERVICE_FORM,SERVICE_OBJECT,SERVICE_STATUS,SERVICE_SEQ,IMPOP_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, RadioSystemsID[i]);
                    rs.Open();
                    XmlNode node = docRadioSystems.CreateElement("Row");
                    docRadioSystems.DocumentElement.AppendChild(node);
                    XmlNode child;
                    child = docRadioSystems.CreateElement("NAME");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("NAME")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("DESCRIPTION");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("DESCRIPTION")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("EFIS_NAME");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("EFIS_NAME")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SONS");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SONS")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("PARAMETERS");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("PARAMETERS")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("ICST_TYPE");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("ICST_TYPE").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("ICST_SIGNAL");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("ICST_SIGNAL").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("RS_MEMO");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("RS_MEMO")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("CUST_TXT1");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT1")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("RS_MEMO");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("RS_MEMO")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("CUST_TXT2");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT2")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("CUST_TXT3");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT3")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_ID");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_ID")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_NAME");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_NAME")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_DESC");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_DESC")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_PARENT");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_PARENT")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_LEVEL");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("SERVICE_LEVEL").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_CODE");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_CODE")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_NATURAL");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_NATURAL")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_APPARATUS");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_APPARATUS")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_FORM");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_FORM")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_OBJECT");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_OBJECT")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_STATUS");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_STATUS")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("SERVICE_SEQ");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("SERVICE_SEQ").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("IMPOP_ID");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("IMPOP_ID").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("CREATED_BY");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CREATED_BY")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("DATE_MODIFIED");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.Get("DATE_MODIFIED").ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("MODIFIED_BY");
                    child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("MODIFIED_BY")));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("OBJ_ID");
                    child.AppendChild(docRadioSystems.CreateTextNode(RadioSystemsID[i].ToString()));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("ACTION");
                    child.AppendChild(docRadioSystems.CreateTextNode(RadioSystemsAction[i]));
                    node.AppendChild(child);
                    child = docRadioSystems.CreateElement("GUID");
                    child.AppendChild(docRadioSystems.CreateTextNode(getGuid("RADIO_SYSTEMS", RadioSystemsID[i], "ICSM")));
                    node.AppendChild(child);
                    docRadioSystems.DocumentElement.AppendChild(node);

                }
            }
            /////
           
            if (PlanSyst.Count() > 0)
            {
                docPlanSyst = new XmlDocument(); 
            docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
            docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.Element, "ROOT", ""));
                //for (int i = 0; i < PlanSyst.Count(); i++)
                foreach (var item in PlanSyst)
                {
                    IMRecordset rs = new IMRecordset("NATFPLAN_SYST", IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,RADIOSYS,NAME,ABBREV,RS_MEMO,ACCEPT_SERV,CLASS_OF_STATION,GEO_USE,FREQ_IDENT,A,B,DUPLEX_SPACING,BIUSE_DATE,BIUSE_REF,FREEZE_DATE,FREEZE_REF,EOUSE_DATE,IMPOP_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,STATUS");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, item.Key);
                    rs.Open();
                    XmlNode node = docPlanSyst.CreateElement("Row");
                    docPlanSyst.DocumentElement.AppendChild(node);
                    XmlNode child;
                    child = docPlanSyst.CreateElement("ID");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetI("ID").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("RADIOSYS");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("RADIOSYS")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("NAME");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("NAME")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("ABBREV");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("ABBREV")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("RS_MEMO");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("RS_MEMO")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("ACCEPT_SERV");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("ACCEPT_SERV")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("CLASS_OF_STATION");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("CLASS_OF_STATION")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("GEO_USE");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("GEO_USE")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("FREQ_IDENT");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("FREQ_IDENT")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("A");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("A")));
                    node.AppendChild(child);
                    int indexA = PlanBandID.IndexOf(rs.GetI("ID"));
                    if (indexA >= 0)
                        node.AppendChild(getBand(rs.GetI("ID"), "A", docPlanSyst, PlanBandAction[indexA]));
                    else
                        BandNode(0, "", docPlanSyst, "");
                    child = docPlanSyst.CreateElement("B");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("B")));
                    node.AppendChild(child);
                    int indexB = PlanBandID.IndexOf(rs.GetI("ID"));
                    if (indexB >= 0)
                        node.AppendChild(getBand(rs.GetI("ID"), "B", docPlanSyst, PlanBandAction[indexB]));
                    else
                        BandNode(0, "", docPlanSyst, "");
                    child = docPlanSyst.CreateElement("DUPLEX_SPACING");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetI("DUPLEX_SPACING").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("STATUS");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("STATUS")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("BIUSE_DATE");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("BIUSE_DATE").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("BIUSE_REF");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("BIUSE_REF")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("FREEZE_DATE");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("FREEZE_DATE").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("FREEZE_REF");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("FREEZE_REF")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("EOUSE_DATE");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("EOUSE_DATE").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("IMPOP_ID");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetI("IMPOP_ID").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("DATE_CREATED");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("DATE_CREATED").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("CREATED_BY");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("CREATED_BY")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("DATE_MODIFIED");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("DATE_MODIFIED").ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("MODIFIED_BY");
                    child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("MODIFIED_BY")));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("OBJ_ID");
                    child.AppendChild(docPlanSyst.CreateTextNode(item.Key.ToString()));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("ACTION");
                    child.AppendChild(docPlanSyst.CreateTextNode(item.Value));
                    node.AppendChild(child);
                    child = docPlanSyst.CreateElement("GUID");
                    child.AppendChild(docPlanSyst.CreateTextNode(getGuid("NATFPLAN_SYST", item.Key, "ICSM")));
                    node.AppendChild(child);
                    node.AppendChild(getSTD(rs.GetI("ID"), docPlanSyst));
                    docPlanSyst.DocumentElement.AppendChild(node);
                }
            }
            if (delBand.Count > 0)
            {
                if (docPlanSyst == null)
                {
                    docPlanSyst = new XmlDocument();
                    docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
                    docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.Element, "ROOT", ""));
                }
                foreach (int id in delBand)
                {
                    docPlanSyst.DocumentElement.AppendChild(getDelBand(id, docPlanSyst));
                }
            }


        }

        void OnAnswerRres()
        {
            LoadData();
            CGlobalDB gdb = new CGlobalDB();
            if (gdb.OpenConnection())
            {
                if (docRadioSystems != null)
                    gdb.WriteXMLToDB(docRadioSystems.InnerXml, EEssence.RadioSystems, EService.RRES);
                if (docPlanSyst != null)
                    gdb.WriteXMLToDB(docPlanSyst.InnerXml, EEssence.Nf_Plan, EService.RRES);
                gdb.CloseConnection();

                //test
                //MemoryStream stream = new MemoryStream();
                //docPlanSyst.Save(stream);
                //List<byte> buffer = new List<byte>(stream.ToArray());
                //string str = Encoding.ASCII.GetString(buffer.ToArray());
                //MessageBox.Show(str);
                //MessageBox.Show(docPlanSyst.InnerXml);
            }
        }
        XmlNode getDelBand(int ID, XmlDocument docPlanSyst)
        {

           
            XmlNode node = docPlanSyst.CreateElement("Row");
            docPlanSyst.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanSyst.CreateElement("ID");

            node.AppendChild(child);
            child = docPlanSyst.CreateElement("RADIOSYS");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("NAME");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ABBREV");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("RS_MEMO");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ACCEPT_SERV");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("CLASS_OF_STATION");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("GEO_USE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREQ_IDENT");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("A");
            node.AppendChild(child);
            node.AppendChild(BandNode(ID, "A", docPlanSyst, "D"));
            child = docPlanSyst.CreateElement("B");
            node.AppendChild(child);
            node.AppendChild(BandNode(ID, "B", docPlanSyst, "D"));
            child = docPlanSyst.CreateElement("DUPLEX_SPACING");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("STATUS");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("BIUSE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("BIUSE_REF");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREEZE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREEZE_REF");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("EOUSE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("IMPOP_ID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("DATE_CREATED");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("CREATED_BY");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("DATE_MODIFIED");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MODIFIED_BY");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("OBJ_ID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ACTION");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("GUID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("NATFPLAN_STD");
            node.AppendChild(child);
          
            return node;
        }
        XmlNode BandNode(int ID, string role, XmlDocument docPlanSyst, string action)
        {
            XmlNode node = docPlanSyst.CreateElement("BAND_" + role);
            docPlanSyst.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanSyst.CreateElement("ID");
            child.AppendChild(docPlanSyst.CreateTextNode(ID.ToString()));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ROLE");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MIN_FREQ");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MAX_FREQ");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            child = docPlanSyst.CreateElement("PAIRING");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            child = docPlanSyst.CreateElement("ACTION");
            child.AppendChild(docPlanSyst.CreateTextNode(action));
            node.AppendChild(child);
            return node;
        }
        XmlNode getBand(int Nfps_ID, string role, XmlDocument docPlanBand,string action)
        {
            IMRecordset rs = new IMRecordset("NATFPLAN_BAND", IMRecordset.Mode.ReadWrite);
            rs.Select("ID,NFPS_ID,ROLE,MIN_FREQ,MAX_FREQ,PAIRING");
            rs.SetWhere("NFPS_ID", IMRecordset.Operation.Eq, Nfps_ID);
            rs.SetWhere("ROLE", IMRecordset.Operation.Eq, role);
            rs.Open();
            XmlNode node = docPlanBand.CreateElement("BAND_" + role);
            docPlanBand.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanBand.CreateElement("ID");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("ID").ToString()));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("ROLE");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("ROLE")));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("MIN_FREQ");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("MIN_FREQ").ToString()));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("MAX_FREQ");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("MAX_FREQ").ToString()));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("PAIRING");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("PAIRING")));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("ACTION");
            child.AppendChild(docPlanSyst.CreateTextNode(action));
            node.AppendChild(child);
            rs.Close();
            return node;
        }


        XmlNode getSTD(int ID, XmlDocument docPlanBand)
        {
            IMRecordset rs = new IMRecordset("NATFPLAN_STD", IMRecordset.Mode.ReadWrite);
            rs.Select("ID,LINKF1,ID_NUMBER");
            rs.SetWhere("LINKF1", IMRecordset.Operation.Eq, ID);
            rs.Open();
            XmlNode node = docPlanBand.CreateElement("NATFPLAN_STD");
            docPlanBand.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanBand.CreateElement("ID");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("ID").ToString()));
            node.AppendChild(child);
            child = docPlanBand.CreateElement("ID_NUMBER");
            child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("ID_NUMBER")));
            node.AppendChild(child);           
            rs.Close();
            return node;
        }


        public string Description
        {
            get
            {
                return "UTDB exchange module";
            }
        }

        public void GetMainMenu(IMMainMenu mainMenu)
        {
            mainMenu.InsertItem("UCRF Plugin\\On Answer Rres", OnAnswerRres, "MOB_STATION2");
        }

        public string Ident
        {

            get
            {
                return "UtdbExchange";
            }
        }

        public bool OtherMessage(string message, object inParam, ref object outParam)
        {
            System.Diagnostics.Debug.Indent();
            System.Diagnostics.Debug.WriteLine("OtherMessage(" + message + ")");
            System.Diagnostics.Debug.Unindent();
            if (message == "UTDB_EXCHANGE")
                OnAnswerRres();
            return false;
        }

        public void RegisterBoard(IMBoard b)
        {
            //throw new NotImplementedException();
        }

    public void RegisterSchema(IMSchema s)
        {

            s.DeclareTable("XUTDB_X_CACHE", "Кэш обмена UTDB", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("DATE_CACHED", "DATE", null, "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_CACHE", "PRIMARY", "OBJ_TABLE,OBJ_ID,DATE_CACHED");
                s.DeclareField("ACTION", "VARCHAR(1)", null, "NOTNULL", null);
                s.DeclareField("INFO", "VARCHAR(1000)", null, "NOTNULL", null);
                s.DeclareField("STATUS", "NUMBER(2,0)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_LOG", "Журнал обмена UTDB", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("EVENT_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_LOG", "PRIMARY", "OBJ_TABLE,OBJ_ID,EVENT_DATE");
                s.DeclareField("DIRECTION", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("ACTION", "VARCHAR(1)", null, "NOTNULL", null);
                s.DeclareField("EVENT", "VARCHAR(1000)", null, "NOTNULL", null);
                s.DeclareField("INFO", "VARCHAR(1000)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_SYN", "Синонимы импортируемых объектов", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("EXTERNAL_SVC", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("EXTERNAL_ID", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_SYN", "PRIMARY", "OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");

            }
            s.DeclareTable("XUTDB_X_STATUS", " ", "PLUGIN_3,99");
            {
                s.DeclareField("CODE", "NUMBER(2,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(30)", null, null, null);
                s.DeclareField("DESC", "VARCHAR(1000)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_EVENT", " ", "PLUGIN_3,99");
            {
                s.DeclareField("CODE", "NUMBER(2,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(30)", null, null, null);
                s.DeclareField("DESC", "VARCHAR(1000)", null, null, null);
            }
        }

        public double SchemaVersion
        {

            get
            {
                return 20120712.1041;
            }
        }

        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {

            if (dbCurVersion < 20120712.1040)
            {
                s.CreateTables("XUTDB_X_CACHE");
                s.CreateTables("XUTDB_X_LOG");
                s.CreateTables("XUTDB_X_SYN");
                s.CreateTables("XUTDB_X_STATUS");
                s.CreateTables("XUTDB_X_EVENT");
                s.SetDatabaseVersion(20120712.1041);
            }
            else if (dbCurVersion < 20120712.1041)
            {
                s.CreateTableFields("XUTDB_X_CACHE", "STATUS");
                s.CreateTables("XUTDB_X_STATUS");
                s.CreateTables("XUTDB_X_EVENT");
                s.SetDatabaseVersion(20120712.1041);
            }

            return true;
        }
        string getGuid(string ObjTable, int id, string External_SVC)
        {

            IMRecordset rs = new IMRecordset("XUTDB_X_SYN", IMRecordset.Mode.ReadWrite);
            rs.Select("OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");
            rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, id);
            rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, ObjTable);
            rs.Open();
            if (rs.GetS("EXTERNAL_ID").Length>1)
            {
                return rs.GetS("EXTERNAL_ID");
            }
            string guid = Guid.NewGuid().ToString();
            rs.AddNew();
            rs.Put("OBJ_TABLE", ObjTable);
            rs.Put("OBJ_ID", id);
            rs.Put("EXTERNAL_SVC", External_SVC);
            rs.Put("EXTERNAL_ID", guid);
            rs.Update();
            rs.Close();
            return guid;


        }
    }
}
