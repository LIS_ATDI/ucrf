﻿using System;
using System.Configuration;
using System.Reflection;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET
{
   internal static class ApplSetting
   {
      public static bool IsDebugSite { get; set; }
      public static bool IsDebug { get; set; }
      public static bool IsDebugCalculation { get; set; }
      public static bool IsRunTest { get; set; }
      public static bool UseGlobalDbService { get; set; }
      public static string RunTestLogPath { get; set; }
      public static DateTime URCMMemoDate { get; set; }
      public static bool UseAllStationForEmcCalculation { get; set; }
      public static string TableForSetArticle { get; set; }  //TODO Удалить
      public static bool UsePtkFeaturesInUrcpDrvAppl { get; set; }
      //======================
      static ApplSetting()
      {
         IsDebug = false;
         IsRunTest = false;
         URCMMemoDate = DateTime.Now;
         UseAllStationForEmcCalculation = false;
         TableForSetArticle = "";
         IsDebugSite = true;
         UseGlobalDbService = false;
         //------
         ReadAppSettings();
         IsDebugCalculation = IsDebug;
      }
      //===================================================
      // Get the AppSettings section.        
      // This function uses the AppSettings property
      // to read the appSettings configuration section.
      private static void ReadAppSettings()
      {
         try
         {
            // Get the AppSettings section.
            string p = Assembly.GetExecutingAssembly().Location;
            Configuration cfg = ConfigurationManager.OpenExeConfiguration(p);
            KeyValueConfigurationCollection appSettings = cfg.AppSettings.Settings;
            foreach (KeyValueConfigurationElement item in appSettings)
            {
               switch (item.Key)
               {
                  case "IsDebug":
                     if (item.Value.ToUpper() == "TRUE")
                        IsDebug = true;
                     break;
                  case "IsDebugSite":
                     if (item.Value.ToUpper() == "FALSE")
                         IsDebugSite = false;
                     break;
                  case "TableForSetArticle":
                     TableForSetArticle = item.Value;
                     break;
                  case "IsRunTest":
                     if (item.Value.ToUpper() == "TRUE")
                        IsRunTest = true;
                     break;
                  case "RunTestLogPath":
                     RunTestLogPath = item.Value;
                     break;
                  case "URCMMemoDate":
                     URCMMemoDate = item.Value.ToDataTime();
                     if (URCMMemoDate == new DateTime())
                         URCMMemoDate = DateTime.Now;
                     break;
                  case "UseAllStationForEmcCalculation":
                     if (item.Value.ToUpper() == "TRUE")
                        UseAllStationForEmcCalculation = true;
                     break;
                   case "UseGlobalDbService":
                     if (item.Value.ToUpper() == "TRUE")
                         UseGlobalDbService = true;
                       break;
               }
            }
         }
         catch (Exception)
         {

         }
      }
   }
}
