﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.Reminder
{
    /// <summary>
    /// Пользователь, который получит уведомление
    /// </summary>
    internal class ReminderUser
    {       
        public int Id { get; protected set; }

        public const string LastNameField = "LastName";
        public const string FirstNameField = "FirstName";
        public const string MiddleNameField = "MiddleName";

        public string LastName { get; protected set; }
        public string FirstName { get; protected set; }
        public string MiddleName { get; protected set; }

        public void Load(int userId)
        {
            Id = userId;
            LastName = CUsers.GetUserFio(userId);            
        }
    }
}
