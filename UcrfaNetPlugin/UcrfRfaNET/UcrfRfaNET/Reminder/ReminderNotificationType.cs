﻿using LisCommonLib.TextStrings;

namespace XICSM.UcrfRfaNET.Reminder
{
   /// <summary>
   /// Список типов уведомления
   /// </summary>
    public enum ReminderNotificationType
    {
        Unknown = 0,
        CreatedMk = 1,
    };

    public static class ReminderNotificationTypeExtension
    {
        /// <summary>
        /// Конвертирует латинский ReminderNotificationType в 
        /// удобочитаемое национальное название
        /// </summary>
        /// <param name="val">ReminderNotificationType</param>
        /// <returns>локализированное название уведомления на национальном языке</returns>
        public static string ToStringEri(this ReminderNotificationType val)
        {
            return HelpClasses.EriFiles.GetEriDescription(val.ToString(), HelpClasses.EriFiles.ReminderNotifyTypeEri);
        }
        /// <summary>
        /// Строку в ReminderNotificationType
        /// </summary>
        /// <param name="val">строка</param>
        /// <returns>ReminderNotificationType</returns>
        public static ReminderNotificationType ToReminderNotificationType(this string val)
        {
            ReminderNotificationType retVal;
            try
            {
                retVal = (ReminderNotificationType) System.Enum.Parse(typeof (ReminderNotificationType), val);
            }
            catch(System.Exception)
            {
                retVal = ReminderNotificationType.Unknown;
            }
            return retVal;
        }
    }
}