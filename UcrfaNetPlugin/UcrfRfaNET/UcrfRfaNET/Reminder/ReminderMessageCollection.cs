﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Icsm;

namespace XICSM.UcrfRfaNET.Reminder
{
    class ReminderMessageCollection : BindingList<ReminderMessage>
    {
        /// <summary>
        /// Загружает ReminderMessages из XNRFA_REMINDER
        /// Предполагается, что таблица будет содержать по одной записи на каждый тип уведомления
        /// Если какого-либо типа уведомдения не содержиться в таблице базы данных, для него будет создан
        /// объект со значениями по умолчанию.
        /// </summary>
        public void Load()
        {
            using (LisRecordSet rs = new LisRecordSet(PlugTbl.Reminder, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID");
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    int id = rs.GetI("ID");
                    ReminderMessage message = new ReminderMessage();
                    message.Load(id);
                    Add(message);
                }
            }

            foreach (string name in Enum.GetNames(typeof(ReminderNotificationType)))
            {
                bool isPresent = false;
                ReminderNotificationType rnt = name.ToReminderNotificationType();
                if (rnt == ReminderNotificationType.Unknown)
                    continue;

                foreach (ReminderMessage message in this)
                {
                    if (message.NotificationType == rnt)
                    {
                        isPresent = true;
                        break;
                    }
                }

                if (!isPresent)
                {
                    ReminderMessage message = new ReminderMessage();
                    message.NotificationType = rnt;
                    Add(message);
                }
            }
        }

        /// <summary>
        /// Сохранить коллекция RemonderMessage в базу данных
        /// В этот момент могут быть сохранены те объекты, которые были созданы
        /// по умолчанию в функции Load
        /// </summary>
        public void Save()
        {
            foreach (ReminderMessage message in this)
                message.Save();
        }

        /// <summary>
        /// Цепляет тип уведомления к записи
        /// </summary>
        /// <param name="notificationType">Тип уведомления</param>
        /// <param name="tableName">таблица, которая содержит запись, к которому должно быть прикреплено напоминание</param>
        /// <param name="tableId">идентификатор записи, в вушеуказанной таблице</param>
        public static void Apply(ReminderNotificationType notificationType, string tableName, int tableId)
        {
            ReminderMessageCollection messageCollection = new ReminderMessageCollection();
            messageCollection.Load();

            using (LisTransaction t = new LisTransaction())
            {
                foreach (ReminderMessage message in messageCollection)
                {
                    if (message.NotificationType == notificationType && message.Active)
                    {
                        int reminderId = IM.NullI;
                        DateTime dueDate = DateTime.Now.AddMinutes(message.Delay);
                        using (LisRecordSet r = new LisRecordSet(ICSMTbl.itblReminder, IMRecordset.Mode.ReadWrite))
                        {
                            r.Select("ID,TYPE,TITLE,DESCRIPTION,PRIORITY,STATUS,DUE_DATE,TABLE_NAME,TABLE_ID,REQUESTER_UID");
                            r.Open();

                            r.AddNew();
                            reminderId = IM.AllocID(ICSMTbl.itblReminder, 1, -1);
                            r.Put("ID", reminderId);
                            r.Put("TYPE", message.ReminderType);
                            r.Put("TITLE", message.Title);
                            r.Put("DESCRIPTION", message.Description);
                            r.Put("PRIORITY", message.Priority);
                            r.Put("STATUS", message.Status);
                            r.Put("DUE_DATE", dueDate);
                            r.Put("TABLE_NAME", tableName);
                            r.Put("TABLE_ID", tableId);
                            r.Put("REQUESTER_UID", IM.ConnectedUser());
                            r.Update();
                        }
                        
                        using (LisRecordSet r = new LisRecordSet(ICSMTbl.itblReminderTrg, IMRecordset.Mode.ReadWrite))
                        {
                            TimeSpan time = (dueDate - new DateTime(2000, 1, 1));
                            
                            r.Select("RMDR_ID,TARGET_USER,STATUS,CALL_TIME");
                            r.Open();
                            foreach (ReminderUser user in message.ReminderUserList)
                            {
                                r.AddNew();
                                r.Put("RMDR_ID", reminderId);
                                r.Put("TARGET_USER", CUsers.GetUserLoginById(user.Id));
                                r.Put("STATUS", message.Status);
                                r.Put("CALL_TIME", time.TotalMilliseconds);
                                r.Update();
                            }
                        }
                        t.Commit();
                        return;
                    }                    
                }                
            }
        }
    }
}
