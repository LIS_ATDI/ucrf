﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Icsm;

namespace XICSM.UcrfRfaNET.Reminder
{
    /// <summary>
    /// Коллекция пользователей, которые должны получить уведомление
    /// </summary>
    internal class ReminderUserCollection : BindingList<ReminderUser>
    {
        public int ReminderId { get; protected set; }

        public void Load(int reminderId)
        {
            ReminderId = reminderId;
            using (LisRecordSet r = new LisRecordSet(PlugTbl.ReminderUsers, IMRecordset.Mode.ReadOnly))
            {
                r.Select("REMINDER_ID,USER_ID");
                r.SetWhere("REMINDER_ID", IMRecordset.Operation.Eq, ReminderId);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    AddUserById(r.GetI("USER_ID"));                    
                }
            }
        }

        public void Save()
        {
            using (LisRecordSet r = new LisRecordSet(PlugTbl.ReminderUsers, IMRecordset.Mode.ReadWrite))
            {
                r.Select("REMINDER_ID,USER_ID");
                r.SetWhere("REMINDER_ID", IMRecordset.Operation.Eq, ReminderId);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    r.Delete();

            }

            using (LisRecordSet r = new LisRecordSet(PlugTbl.ReminderUsers, IMRecordset.Mode.ReadWrite))
            {
                r.Select("REMINDER_ID,USER_ID");
                r.SetWhere("REMINDER_ID", IMRecordset.Operation.Eq, ReminderId);

                r.Open();
                foreach (ReminderUser user in this)
                {
                    r.AddNew();
                    r.Put("REMINDER_ID", ReminderId);
                    r.Put("USER_ID", user.Id);
                    r.Update();
                }
            }
    
        }

        public void AddUserById(int userId)
        {
            foreach (ReminderUser user in this)
                if (user.Id == userId)
                    return;

            ReminderUser newUser = new ReminderUser();
            newUser.Load(userId);
            Add(newUser);            
        }
    }
}
