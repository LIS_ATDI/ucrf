﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Reminder
{
    internal partial class ReminderForm : Form
    {
        private ReminderMessageCollection _reminderMessage = new ReminderMessageCollection();
        
        ComboBoxDictionaryList<string, string> _typeDictionary = new ComboBoxDictionaryList<string, string>();
        ComboBoxDictionaryList<string, string> _priorityDictionary = new ComboBoxDictionaryList<string, string>();
        ComboBoxDictionaryList<string, string> _statusDictionary = new ComboBoxDictionaryList<string, string>();

        public ReminderForm()
        {
            InitializeComponent();
            _reminderMessage.Load();
            CLocaliz.TxT(this);
        }
        
        private void ReminderForm_Load(object sender, EventArgs e)
        {            
            lbNotificationType.DataSource = _reminderMessage;
            lbNotificationType.DisplayMember = ReminderMessage.NotificationTypeStringField;
            
            tbTitle.DataBindings.Add("Text", _reminderMessage, ReminderMessage.TitleField);
            tbDescription.DataBindings.Add("Text", _reminderMessage, ReminderMessage.DescriptionField);

            tbInterval.DataBindings.Add("Text", _reminderMessage, ReminderMessage.DelayField);
            activeCheckBox.DataBindings.Add("Checked", _reminderMessage, ReminderMessage.ActiveField);
            
            _typeDictionary = new ComboBoxDictionaryList<string, string>();
            _typeDictionary.InitComboBox(cbReminderType);

            Dictionary<string,string> typeStatList = EriFiles.GetEriCodeAndDescr(EriFiles.ReminderTypeEri);
            foreach (KeyValuePair<string, string> pair in typeStatList)
                _typeDictionary.Add(new ComboBoxDictionary<string, string>(pair.Key, pair.Value));
            cbReminderType.DataBindings.Add("SelectedValue", _reminderMessage, ReminderMessage.ReminderTypeField, true);

            _priorityDictionary = new ComboBoxDictionaryList<string, string>();
            _priorityDictionary.InitComboBox(cbPriority);
            Dictionary<string,string> priorityList = EriFiles.GetEriCodeAndDescr(EriFiles.ReminderPriorityEri);
            foreach (KeyValuePair<string, string> pair in priorityList)
                _priorityDictionary.Add(new ComboBoxDictionary<string, string>(pair.Key, pair.Value));
            cbPriority.DataBindings.Add("SelectedValue", _reminderMessage, ReminderMessage.PriorityField, true);

            _statusDictionary = new ComboBoxDictionaryList<string, string>();
            _statusDictionary.InitComboBox(cbStatus);
            Dictionary<string,string> statusList = EriFiles.GetEriCodeAndDescr(EriFiles.ReminderStatusEri);
            foreach (KeyValuePair<string, string> pair in statusList)
                _statusDictionary.Add(new ComboBoxDictionary<string, string>(pair.Key, pair.Value));
            cbStatus.DataBindings.Add("SelectedValue", _reminderMessage, ReminderMessage.StatusField, true);

            gridReminderUsers.AutoGenerateColumns = false;
            DataGridViewTextBoxColumn lastName = new DataGridViewTextBoxColumn();
            lastName.HeaderText = "П.I.О.";
            lastName.Name = "lastName_DataGridViewTextBoxColumn";
            lastName.DataPropertyName = ReminderUser.LastNameField;
            lastName.Width = 300;
            gridReminderUsers.Columns.Add(lastName);
            gridReminderUsers.DataSource = _reminderMessage;
            gridReminderUsers.DataMember = ReminderMessage.ReminderUserListField;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _reminderMessage.Save();
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ReminderMessage message = lbNotificationType.SelectedItem as ReminderMessage;
            if (message!=null)
            {
                RecordPtr user = RecordPtr.UserSearch("Search for employee", ICSMTbl.itblEmployee, "");
                if (user.Id!=IM.NullI)
                    message.ReminderUserList.AddUserById(user.Id);        
            }                    
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ReminderMessage message = lbNotificationType.SelectedItem as ReminderMessage;
            if (message != null)
            {
                if (gridReminderUsers.SelectedRows.Count > 0)
                {
                    int rowIndex = gridReminderUsers.SelectedRows[0].Index;
                    message.ReminderUserList.RemoveAt(rowIndex);
                }
            }
        }        
    }
}