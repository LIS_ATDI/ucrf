﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Icsm;

namespace XICSM.UcrfRfaNET.Reminder
{
    internal class ReminderMessage
    {                
        
        /// <summary>
        /// Поля, облегчающие биндинг
        /// </summary>
        
        public const string NotificationTypeField = "NotificationType";
        public const string NotificationTypeStringField = "NotificationTypeString";
        public const string TitleField = "Title";
        public const string ReminderTypeField = "ReminderType";
        public const string DescriptionField = "Description";
        public const string PriorityField = "Priority";
        public const string StatusField = "Status";
        public const string ReminderUserListField = "ReminderUserList";
        public const string DelayField = "Delay";
        public const string ActiveField = "Active";

        /// <summary>
        /// Тип уведомления - то есть тип события, такой как создание пакета, и пр.
        /// </summary>
        public ReminderNotificationType NotificationType { get; set; }
        public string NotificationTypeString { get { return NotificationType.ToStringEri();} }

        /// <summary>
        /// Тип напоминания, всем из группы, кому-либо из группы, 
        /// </summary>
        public string ReminderType { get; set; }
        /// <summary>
        /// Название (заголовок) напоминания
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Детальное описание напоминания
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Приоритет - от очень высокого до низкий
        /// </summary>
        public string Priority { get; set; }
        /// <summary>
        /// Статус - пропущенный, текущий, отложенный
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Задержва в минутах от текущего времени
        /// </summary>
        public int Delay { get; set; }
        /// <summary>
        /// Активно или нет
        /// </summary>
        public bool Active { get; set; }
        /// <summary>
        /// Идентификатор в XNRFA_REMINDER
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// Коллекция пользователей, которые получат напоминание
        /// </summary>
        public ReminderUserCollection ReminderUserList { get; protected set; }
        
        public ReminderMessage()
        {
            Id = IM.NullI;
            Delay = 0;        
            Active = true;
            ReminderUserList = new ReminderUserCollection();
        }

        /// <summary>
        /// Загрузить напоминание по id
        /// </summary>
        /// <param name="id">id в XNRFA_TABLES</param>
                
        public virtual void Load(int id)
        {
            Id = id;
            using (LisRecordSet r = new LisRecordSet(PlugTbl.Reminder, IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,NOTIFY_TYPE,TYPE,TITLE,DESCRIPTION,PRIORITY,STATUS,DELAY,ACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    NotificationType = r.GetS("NOTIFY_TYPE").ToReminderNotificationType();
                    ReminderType = r.GetS("TYPE");
                    Title = r.GetS("TITLE");
                    Description = r.GetS("DESCRIPTION");
                    Priority = r.GetS("PRIORITY");
                    Status = r.GetS("STATUS");
                    Delay = r.GetI("DELAY");
                    if (Delay == IM.NullI)
                        Delay = 0;
                    Active = r.GetI("ACTIVE")==1 ? true : false;
                }
            }

            ReminderUserList.Load(Id);                        
        }

        public virtual void Save()
        {
            using (LisTransaction t = new LisTransaction())
            {
                using (LisRecordSet r = new LisRecordSet(PlugTbl.Reminder, IMRecordset.Mode.ReadWrite))
                {
                    r.Select("ID,NOTIFY_TYPE,TYPE,TITLE,DESCRIPTION,PRIORITY,STATUS,DELAY,ACTIVE");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    r.Open();
                    
                    if (r.IsEOF())
                    {
                        r.AddNew();
                        Id = IM.AllocID(PlugTbl.Reminder, 1, -1);
                        r.Put("ID", Id);
                    }
                    else 
                    { 
                        r.Edit(); 
                    }

                    r.Put("NOTIFY_TYPE", NotificationType.ToString());
                    r.Put("TYPE", ReminderType);
                    r.Put("TITLE", Title);
                    r.Put("DESCRIPTION", Description);
                    r.Put("PRIORITY", Priority);
                    r.Put("STATUS", Status);
                    r.Put("DELAY", Delay);
                    r.Put("ACTIVE", Active ? 1 : 0);                    
                    r.Update();                    
                }

                ReminderUserList.Save();
                
                t.Commit();
            }
        }
    }
}