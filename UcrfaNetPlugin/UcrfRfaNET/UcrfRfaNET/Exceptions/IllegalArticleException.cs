﻿using System;
using System.Runtime.Serialization;

namespace XICSM.UcrfRfaNET.Exceptions
{
    [Serializable]
    public class IllegalArticleException : Exception
    {
        public IllegalArticleException() : this("Illegal article. Article can't be NULL.")
        {
        }

        public IllegalArticleException(string message) : base(message)
        {
        }

        public IllegalArticleException(string message, System.Exception inner) : base(message, inner)
        {
        }

        protected IllegalArticleException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
