﻿using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET;
using System.Xml.Schema;
using System.Collections.Specialized;
using System.IO;
using System.Security;
using XICSM.UcrfRfaNET.StationsStatus;

namespace XICSM.UcrfRfaNET.GlobalDB
{
   //======================================================
   /// <summary>
   /// Класс для обработки XML файлов из/в промежуточной БД
   /// </summary>
   internal class CGlobalXML
   {
      
      //===================================================
      /// <summary>
      /// Создает XML файл оборудования
      /// </summary>
      /// <param name="equipList">список оборудования</param>
      /// <param name="FileName">имя файла (если NULL, то создает новый файл)</param>
      /// <returns>количество добавленых оборудований в файл</returns>
      public static int CreateEquipXML(List<RecordPtr> equipList, ref string FileName)
      {
         if (string.IsNullOrEmpty(FileName))
            FileName = CreateFileName();

         int equipCounter = 0;
         XmlDocument xmlDoc = NewXML("", "EquipElements", "");
         XmlElement xmlRoot = xmlDoc.DocumentElement; //Главный элемент

         foreach (RecordPtr rp in equipList)
         {
            // Проверяем запись: она уже передавалать в глобальную БД
            Int64 intGuid = GetGUIDRecord(rp);
            if (intGuid != 0)
               continue;  // Эта запись уже передана в глобальную БД

            // Добавляем 
            Int32 guid = CGlobalDB.newGUID();
            IMRecordset rsEquipment = null;

            try{rsEquipment = IMRecordset.ForRead(rp, "ID,CODE,NAME");}
            catch{rsEquipment = null;}

            if (rsEquipment != null)
            {// Добавляем оборудование
               XmlElement equipElem = xmlDoc.CreateElement("", "EquipElement", "");
               {// ECode
                  XmlElement ECode = xmlDoc.CreateElement("", "ECode", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(rsEquipment.GetS("CODE")));
                  ECode.AppendChild(value);
                  equipElem.AppendChild(ECode);
               }
               {// EEquipName
                  XmlElement EEquipName = xmlDoc.CreateElement("", "EEquipName", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(rsEquipment.GetS("NAME")));
                  EEquipName.AppendChild(value);
                  equipElem.AppendChild(EEquipName);
               }
               {// EUnit
                  XmlElement EUnit = xmlDoc.CreateElement("", "EUnit", "");
                  XmlText value = xmlDoc.CreateTextNode("1");
                  EUnit.AppendChild(value);
                  equipElem.AppendChild(EUnit);
               }
               {// Создаем атрибут
                  equipElem.SetAttribute("GUID", guid.ToString());
               }
               xmlRoot.AppendChild(equipElem);
               equipCounter++;
               rsEquipment.Close();
               rsEquipment.Destroy();  //Закрываем Recordset

               // Помечаем запись, как уже переданая в глобальную БД
               // !!ПОДВОДНЫЙ КАМЕНЬ!!
               // Мы можем не отрпавить файл в глобальную БД.
               // Что тогда делать????????
               AddGUIDRecord(rp, guid);
            }
         }
         xmlDoc.Save(FileName);
         return equipCounter;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл с сайтом (site)
      /// </summary>
      /// <param name="siteList">список сайтов</param>
      /// <param name="FileName">имя файла (если NULL, то создает новый файл)</param>
      /// <returns>количество добавленых сайтов в файл</returns>
      public static int CreateSiteXML(List<RecordPtr> siteList, ref string FileName)
      {
         if (string.IsNullOrEmpty(FileName))
            FileName = CreateFileName();

         int siteCounter = 0;
         XmlDocument xmlDoc = NewXML("", "SiteElements", "");
         XmlElement xmlRoot = xmlDoc.DocumentElement; //Главный элемент

         foreach (RecordPtr rp in siteList)
         {
            // Проверяем запись: она уже передавалать в глобальную БД
            Int64 intGuid = GetGUIDRecord(rp);
            if (intGuid != 0)
               continue;  // Эта запись уже передана в глобальную БД

            // Добавляем 
            Int32 guid = CGlobalDB.newGUID();
            IMRecordset rsSite = null;

            try { rsSite = IMRecordset.ForRead(rp, "ID,NAME,CODE,REMARK"); }
            catch { rsSite = null; }

            if (rsSite != null)
            {// Добавляем оборудование
               XmlElement siteElem = xmlDoc.CreateElement("", "SiteElement", "");
               {// Наименование
                  XmlElement EName = xmlDoc.CreateElement("", "EName", "");
                  XmlText value = xmlDoc.CreateTextNode(rsSite.GetS("NAME"));
                  EName.AppendChild(value);
                  siteElem.AppendChild(EName);
               }
               {// КОАТУУ
                  XmlElement EKOATUU = xmlDoc.CreateElement("", "EKOATUU", "");
                  XmlText value = xmlDoc.CreateTextNode(rsSite.GetS("CODE"));
                  EKOATUU.AppendChild(value);
                  siteElem.AppendChild(EKOATUU);
               }
               {// Адрес
                  XmlElement EAddress = xmlDoc.CreateElement("", "EAddress", "");
                  XmlText value = xmlDoc.CreateTextNode(rsSite.GetS("REMARK"));
                  EAddress.AppendChild(value);
                  siteElem.AppendChild(EAddress);
               }
               {// Коеф. местоположения
                  XmlElement PosCoeff = xmlDoc.CreateElement("", "PosCoeff", "");
                  XmlText value = xmlDoc.CreateTextNode("1");
                  PosCoeff.AppendChild(value);
                  siteElem.AppendChild(PosCoeff);
               }
               {// Создаем атрибут
                  siteElem.SetAttribute("GUID", guid.ToString());
               }
               xmlRoot.AppendChild(siteElem);
               siteCounter++;
               rsSite.Close();
               rsSite.Destroy();

               // Помечаем запись, как уже переданая в глобальную БД
               // !!ПОДВОДНЫЙ КАМЕНЬ!!
               // Мы можем не отрпавить файл в глобальную БД.
               // Что тогда делать????????
               AddGUIDRecord(rp, guid);
            }
         }
         xmlDoc.Save(FileName);
         return siteCounter;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл заявки на виставлення рахунку
      /// </summary>
      /// <param name="id">идентификатор заявки в БД</param>
      /// <param name="FileName">имя файла (если NULL, то создает новый файл)</param>
      /// <param name="freeApp">Является ли заявка бесплатной</param>
      /// <returns>количество добавленых работ в файл</returns>
      public static XmlDocument CreateAppPayXML(int appId, ref string FileName, ref List<string> errors, bool freeApp)
      {
         if (string.IsNullOrEmpty(FileName))
            FileName = CreateFileName();

         int packetId = IM.NullI;
         List<Work> works = new List<Work>();
         List<int> appIds = new List<int>();
         string department = "";
         string numIn = "";
         DateTime dateIn = IM.NullT;
         int idOwner = IM.NullI;
         int idPayOwner = IM.NullI;
         if (appId != IM.NullI)
         {
            IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadOnly);
            rsApp.Select("ID,PACKET_ID,STATE,DEPARTMENT,CUST_TXT1,DOC_DATE,OWNER_ID");
            rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
               rsApp.Open();
               if (rsApp.IsEOF() == false)
               {
                  appId = rsApp.GetI("ID");
                  packetId = rsApp.GetI("PACKET_ID");
                  department = rsApp.GetS("DEPARTMENT");
                  numIn = rsApp.GetS("CUST_TXT1");
                  dateIn = rsApp.GetT("DOC_DATE");
                  if (rsApp.GetI("OWNER_ID") != IM.NullI)
                     idPayOwner = rsApp.GetI("OWNER_ID");
               }
               else
                  errors.Add(CLocaliz.TxT("Can't find packet"));
             }
            finally
            {
               rsApp.Close();
               rsApp.Destroy();
            }

           
           
            IMRecordset rsAps = new IMRecordset(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadWrite);
            rsAps.Select("ID,APPLPAY_ID,APPL_ID");
            rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
            try
            {
               rsAps.Open();
               for (; !rsAps.IsEOF(); rsAps.MoveNext())
                  appIds.Add(rsAps.GetI("APPL_ID"));
            }
            finally
            {
               rsAps.Close();
               rsAps.Destroy();
            }

            works.Clear();

            if (appId != IM.NullI)
            {
               IMRecordset rsWorks = new IMRecordset(PlugTbl.itblXnrfaDrvApplWorks, IMRecordset.Mode.ReadWrite);
               rsWorks.Select("ID,APPLPAY_ID,APPL_ID,WORKTYPE,ARTICLE,COUNT,NOTES,COEFFICIENT,LINKNUM");
               rsWorks.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsWorks.Open();
                  for (; !rsWorks.IsEOF(); rsWorks.MoveNext())
                  {
                     Work w = new Work();
                     w.appId = rsWorks.GetI("APPL_ID");
                     w.type = (WorkType)rsWorks.GetI("WORKTYPE");
                     w.article = rsWorks.GetS("ARTICLE");
                     w.count = rsWorks.GetI("COUNT");
                     w.notes = rsWorks.GetS("NOTES");
                     w.index = rsWorks.GetD("COEFFICIENT");
                     w.linkNum = rsWorks.GetI("LINKNUM");
                     works.Add(w);
                  }
               }
               finally
               {
                  rsWorks.Close();
                  rsWorks.Destroy();
               }
            }
         }
         
         XmlDocument xmlDoc = NewXML("", "DECLAR", "");
         XmlElement xmlRoot = xmlDoc.DocumentElement; //Главный элемент   
         XmlElement appElem = xmlDoc.CreateElement("", "AppPay", "");
         XmlElement header = xmlDoc.CreateElement("", "Header", "");
         IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
         rsPacket.Select("ID,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD,CONTENT");
         rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packetId);
         try
         {
            rsPacket.Open();
            if (!rsPacket.IsEOF())
            {
               if(idOwner == IM.NullI)
                  idOwner = rsPacket.GetI("OWNER_ID");
               RecordPtr user = new RecordPtr(ICSMTbl.itblUsers, idOwner);
               Int64 guid = GetGUIDRecord(user);
               if(guid == 0)
                  errors.Add(string.Format(CLocaliz.TxT("Can't find GUID for ID={0} TABLE_NAME='{1}'"), user.Id, user.Table));

               if (idPayOwner == IM.NullI)
                  idPayOwner = idOwner;
               RecordPtr payOwner = new RecordPtr(ICSMTbl.itblUsers, idPayOwner);
               Int64 guidPayOwner = GetGUIDRecord(payOwner);
               if(guidPayOwner == 0)
                  errors.Add(string.Format(CLocaliz.TxT("Can't find GUID for ID={0} TABLE_NAME='{1}'"), payOwner.Id, payOwner.Table));

               {//OwnerGUID 
                  XmlElement usguid = xmlDoc.CreateElement("", "OwnerGUID", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(guidPayOwner.ToString()));
                  usguid.AppendChild(value);
                  header.AppendChild(usguid);
               }
               {//OwnerPayGUID 
                  XmlElement usguid = xmlDoc.CreateElement("", "OwnerPayGUID", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(guid.ToString()));
                  usguid.AppendChild(value);
                  header.AppendChild(usguid);
               }
               {// PacketId
                  XmlElement pId = xmlDoc.CreateElement("", "PacketID", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(packetId.ToString()));
                  pId.AppendChild(value);
                  header.AppendChild(pId);
               }

               string InternalNumber = numIn;//rsPacket.GetS("NUMBER_IN");
               if (InternalNumber.Length > 40)
                   errors.Add(CLocaliz.TxT("In number is more than 40 symbols"));
               {//InNumber 
                   XmlElement intNum = xmlDoc.CreateElement("", "InternalNumber", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(InternalNumber));
                   intNum.AppendChild(value);
                   header.AppendChild(intNum);
               }
               DateTime InternalDate = dateIn;//rsPacket.GetT("DATE_IN");
               {//InDate 
                   XmlElement intDate = xmlDoc.CreateElement("", "InternalDate", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(InternalDate)));
                   intDate.AppendChild(value);
                   header.AppendChild(intDate);
               }
               string NumberOut = rsPacket.GetS("NUMBER_OUT");
               {// OutNumber
                  XmlElement outNbr = xmlDoc.CreateElement("", "OutNumber", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(NumberOut));
                  outNbr.AppendChild(value);
                  header.AppendChild(outNbr);
               }
               DateTime DateOut = rsPacket.GetT("DATE_OUT");
               {//OutDate 
                  XmlElement outDt = xmlDoc.CreateElement("", "OutDate", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(DateOut)));
                  outDt.AppendChild(value);
                  header.AppendChild(outDt);
               }

               //string NumberIn = numIn;//rsPacket.GetS("NUMBER_IN");
               string NumberIn = rsPacket.GetS("NUMBER_IN");
               if(NumberIn.Length > 40)
                  errors.Add(CLocaliz.TxT("In number is more than 40 symbols"));
               {//InNumber 
                  XmlElement inNum = xmlDoc.CreateElement("", "InNumber", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(NumberIn));
                  inNum.AppendChild(value);
                  header.AppendChild(inNum);
               }
               //DateTime DateIn = dateIn;//rsPacket.GetT("DATE_IN");
               DateTime DateIn = rsPacket.GetT("DATE_IN");
               {//InDate 
                  XmlElement inDt = xmlDoc.CreateElement("", "InDate", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(DateIn)));
                  inDt.AppendChild(value);
                  header.AppendChild(inDt);
               }
               {//AppID 
                  XmlElement appid = xmlDoc.CreateElement("", "AppID", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(appId.ToString()));
                  appid.AppendChild(value);
                  header.AppendChild(appid);
               }
               int usId = HelpFunction.getUserIDByName(IM.ConnectedUser());
               RecordPtr us = new RecordPtr(ICSMTbl.itblEmployee, usId);
               Int64 usid = GetGUIDRecord(us);
               if(usid == 0)
                  errors.Add(string.Format(CLocaliz.TxT("Can't find GUID for ID={0} TABLE_NAME='{1}'"), us.Id, us.Table));
               {//UserGUID 
                  XmlElement usguid = xmlDoc.CreateElement("", "UserGUID", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(usid.ToString()));
                  usguid.AppendChild(value);
                  header.AppendChild(usguid);
               }
               {//NTYPE 
                  //!!!!!freeApp
                  XmlElement ntype = xmlDoc.CreateElement("", "NTYPE", "");
                  XmlText value = freeApp ? xmlDoc.CreateTextNode("4") : (department == Enum.GetName(typeof(ManagemenUDCR), ManagemenUDCR.URZP)) ? xmlDoc.CreateTextNode("2") : xmlDoc.CreateTextNode("1");
                  ntype.AppendChild(value);
                  header.AppendChild(ntype);
               }
               {//FilialCode
                   XmlElement ntype = xmlDoc.CreateElement("", "FilialCode", "");
                   XmlText value = xmlDoc.CreateTextNode(CUsers.GetUserAreaCode());
                   ntype.AppendChild(value);
                   header.AppendChild(ntype);
               }
               {//Standard


                   XmlElement ntype = xmlDoc.CreateElement("", "Standard", "");
                   XmlText value = xmlDoc.CreateTextNode(string.IsNullOrEmpty(rsPacket.GetS("STANDARD").Trim()) ? "null" : rsPacket.GetS("STANDARD"));
                   ntype.AppendChild(value);
                   header.AppendChild(ntype);
               }

            }
         }
         finally
         {
            rsPacket.Close();
            rsPacket.Destroy();
         }
         bool work1 = false;
         bool work2 = false;
         appElem.AppendChild(header);
         XmlElement worksElem = xmlDoc.CreateElement("", "Works", "");
         foreach (Work w in works)
         {
            if(w.type == WorkType.work1)
               work1 = true;
            if(w.type == WorkType.work2)
               work2 = true;
            {// WorkType
               XmlElement WrkType = xmlDoc.CreateElement("", "WorkType", "");
               string val = "";
               if (w.type != WorkType.other)
                  val = "Work" + ((int)w.type + 1).ToString();
               else
                  val = "OtherWork";
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(val));
               WrkType.AppendChild(value);
               WrkType.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
               worksElem.AppendChild(WrkType);
            }
         }
         foreach (Work w in works)
         {
            if (w.type != WorkType.work50)
            {
               {// Article               
                  if (string.IsNullOrEmpty(w.article))
                     errors.Add(CLocaliz.TxT("Article is empty"));
                  XmlElement Article = xmlDoc.CreateElement("", "Article", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(w.article));
                  Article.AppendChild(value);
                  Article.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                  worksElem.AppendChild(Article);
               }
            }
            else
            {
               IMRecordset article = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
               article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,DEFAULTTYPE");
               article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, "work50");

               string art = "";
               try
               {
                  article.Open();
                  if (!article.IsEOF())
                  {
                     art = article.GetS("ARTICLE");
                  }
               } 
               finally
               {
                  article.Close();
                  article.Destroy();
               }
               XmlElement Article = xmlDoc.CreateElement("", "Article", "");
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(art));
               Article.AppendChild(value);
               Article.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
               worksElem.AppendChild(Article);
            }
         }
         foreach (Work w in works)
         {
            {// Amount
               double Count = 0; double Koef = 0; double PeopleHours = 0;
               if (w.type != WorkType.work50)
               {
                 
                  if (w.count <= 0)
                     errors.Add(CLocaliz.TxT("Amount of a work is empty"));
                  XmlElement Amount = xmlDoc.CreateElement("", "Amount", "");
                  if (w.article.Trim() == ArticleLib.ArticleFunc.article_19)
                  {
                      PeopleHours = w.count * w.index;
                  }
                  else
                  {
                      PeopleHours = w.count;
                  }
                  XmlText value = xmlDoc.CreateTextNode(PeopleHours.ToString());
                  Amount.AppendChild(value);
                  Amount.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                  worksElem.AppendChild(Amount);
               }
               else
               {
                   if (w.article.Trim() == ArticleLib.ArticleFunc.article_19)
                   {
                       PeopleHours = w.count * w.index;
                   }
                   else
                   {
                       PeopleHours = w.index;
                   }

                  XmlElement Amount = xmlDoc.CreateElement("", "Amount", "");
                  XmlText value = xmlDoc.CreateTextNode(Convert.ToInt32(PeopleHours).ToString());
                  Amount.AppendChild(value);
                  Amount.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                  worksElem.AppendChild(Amount);
               }
            }
         }
         foreach (Work w in works)
         {
            {// ComplCoeff
               XmlElement ComplCoeff = xmlDoc.CreateElement("", "ComplCoeff", "");
               XmlText value = xmlDoc.CreateTextNode("1");
               ComplCoeff.AppendChild(value);
               ComplCoeff.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
               worksElem.AppendChild(ComplCoeff);
            }
         }
         foreach (Work w in works)
         {
            if (w.type != WorkType.work14 && w.type != WorkType.work50)
            {
               {// Coeff
                  
                  if (w.index <= 0)
                     errors.Add(CLocaliz.TxT("Position coefficient is empty"));

                  double Koef = 0;
                  if (w.article.Trim() == ArticleLib.ArticleFunc.article_19)
                  {
                      Koef = 1;
                  }
                  else
                  {
                      Koef = w.index;
                  }

                  XmlElement PosCoeff = xmlDoc.CreateElement("", "PosCoeff", "");
                  XmlText value = xmlDoc.CreateTextNode(HelpClasses.HelpFunction.DoubleToStringPoint(Koef));
                  PosCoeff.AppendChild(value);
                  PosCoeff.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                  worksElem.AppendChild(PosCoeff);
               }
            }
            else
            {
               XmlElement PosCoeff = xmlDoc.CreateElement("", "PosCoeff", "");
               XmlText value = xmlDoc.CreateTextNode("1");
               PosCoeff.AppendChild(value);
               PosCoeff.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
               worksElem.AppendChild(PosCoeff);
            }
         }
         foreach (Work w in works)
         {
            if (w.type == WorkType.work3 || w.type == WorkType.work4)
            {
               
               string koa = GetKOATUU(w.appId, w.linkNum);
               {// KOATUU
                  if (koa == "")
                     koa = "9999999";
                     //errors.Add(CLocaliz.TxT("KOATUU is empty"));
                  XmlElement KOATUU = xmlDoc.CreateElement("", "KOATUU", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(koa));
                  KOATUU.AppendChild(value);
                  KOATUU.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                  worksElem.AppendChild(KOATUU);
               }
            }
         }
         appElem.AppendChild(worksElem);
         XmlElement Documents = xmlDoc.CreateElement("", "Documents", "");
         List<BaseAppClass.DocData> ddlist = new List<BaseAppClass.DocData>();
         
         ddlist = GetDocInfo(appIds);
         // Проверка документов
         if(work1 == true || department == Enum.GetName(typeof(ManagemenUDCR), ManagemenUDCR.URZP))
         {// Может вообще не быть документов
         }
         else if(work2 == true)
         {
            foreach(int _id in appIds)
            {
               bool isDozv = false;
               foreach (BaseAppClass.DocData doc in ddlist)
               {
                  if(doc.DocType == CDocTypes.dozvil)
                     isDozv = true;
               }
               Dictionary<int, PositionState2> positions = BaseAppClass.GetPositions(PlugTbl.itblXnrfaAppl, _id);
               string rem = "";
               if (positions.Count > 0)
                   rem = positions[1] == null ? PositionState2.NoPosition : positions[1].FullAddressAuto;
               if(isDozv == false)
                  errors.Add(string.Format(CLocaliz.TxT("Station '{0}' doesn't have document 'EXPL'"), rem));
            }
         }
         else
         {
          /*foreach(int _id in appIds)
            {
               bool isDozv = false;
               bool isVisn = false;
               foreach (BaseAppClass.DocData doc in ddlist)
               {
                  if(doc.DocType == CDocTypes.dozvil)
                     isDozv = true;
                  if(doc.DocType == CDocTypes.vysnovok)
                     isVisn = true;
               }
               List<string> remar = BaseAppClass.GetFullAddress(PlugTbl.itblXnrfaAppl, _id);
               string rem = "";
               if (remar.Count > 0)
                  rem = remar[0];
               if (isVisn == false)
                  errors.Add(string.Format(CLocaliz.TxT("Station '{0}' doesn't have document 'EMS'"), rem));
               if(isDozv == false)
                  errors.Add(string.Format(CLocaliz.TxT("Station '{0}' doesn't have document 'EXPL'"), rem));
            }*/
         }
         foreach (BaseAppClass.DocData doc in ddlist)
         {
            {// DocType
               if(string.IsNullOrEmpty(doc.DocType))
                  errors.Add(CLocaliz.TxT("DocType is empty"));
               XmlElement DocType = xmlDoc.CreateElement("", "DocType", "");
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(doc.DocType));
               DocType.AppendChild(value);
               DocType.SetAttribute("ROWNUM", doc.rownum);
               Documents.AppendChild(DocType);
            }
         }
         foreach (BaseAppClass.DocData doc in ddlist)
         {
            {// Number
               if(string.IsNullOrEmpty(doc.Number) || doc.Number.Length > 30)
                  errors.Add(CLocaliz.TxT("Number is empty or more than 30"));
               XmlElement Number = xmlDoc.CreateElement("", "Number", "");
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(doc.Number));
               Number.AppendChild(value);
               Number.SetAttribute("ROWNUM", doc.rownum);
               Documents.AppendChild(Number);
            }
         }
         foreach (BaseAppClass.DocData doc in ddlist)
         {
            {// EndDate
               XmlElement EndDate = xmlDoc.CreateElement("", "EndDate", "");
               XmlText value = xmlDoc.CreateTextNode(DateToXMLDate(doc.EndDate));
               EndDate.AppendChild(value);
               EndDate.SetAttribute("ROWNUM", doc.rownum);
               Documents.AppendChild(EndDate);
            }
         }
         foreach (BaseAppClass.DocData doc in ddlist)
         {
            {// FullAddress
               XmlElement FullAddress = xmlDoc.CreateElement("", "FullAddress", "");
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(doc.FullAddress));
               FullAddress.AppendChild(value);
               FullAddress.SetAttribute("ROWNUM", doc.rownum);
               Documents.AppendChild(FullAddress);
            }
         }
         foreach (BaseAppClass.DocData doc in ddlist)
         {
            {// Link
               if(string.IsNullOrEmpty(doc.Link))
                  errors.Add(CLocaliz.TxT("Link is empty"));
               XmlElement Link = xmlDoc.CreateElement("", "Link", "");
               XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(doc.Link));
               Link.AppendChild(value);
               Link.SetAttribute("ROWNUM", doc.rownum);
               Documents.AppendChild(Link);
            }
         }
         appElem.AppendChild(Documents);
         xmlRoot.AppendChild(appElem);
         {// Создаем атрибут
            xmlRoot.SetAttribute("xmlns", "http://www.lissoft.com.ua/XMLSchema.xsd");
            xmlRoot.SetAttribute("DataType", "APPPAY");
            xmlRoot.SetAttribute("NAMESPACE", "http://www.lissoft.com.ua/XMLSchema.xsd");
            xmlRoot.SetAttribute("SCHEMAFILE", "XMLSchemaICSM.xsd");
         }
         xmlDoc.Save(FileName);
          
         string xmlschemaerror = "";
         if (!CXMLTest.TestXmlToSchema(xmlDoc.OuterXml, ref xmlschemaerror))
         {
            errors.Add(CLocaliz.TxT("Исходящий XML не прошел проверку"));
            errors.Add(xmlschemaerror);
         }
            
         return xmlDoc;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл заявки на виставлення рахунку від УРЧМ
      /// </summary>
      /// <param name="appId">идентификатор заявки в БД</param>
      /// <param name="FileName">имя файла (если NULL, то создает новый файл)</param>
      /// <returns>количество добавленых работ в файл</returns>
      public static XmlDocument CreateAppPayXMLforURCM(int appId, ref string FileName, ref List<string> errors, AutoRegimEnum autoRegim)
      {


         if (string.IsNullOrEmpty(FileName))
            FileName = CreateFileName();

         int packetId = IM.NullI;
         List<Work> works = new List<Work>();

       

         MemoLines art_lines = new MemoLines();
         if (appId != IM.NullI)
         {   
            works.Clear();

            if (appId != IM.NullI)
            {               
               if (appId != IM.NullI && appId > 0)
               {
                  art_lines.Load(appId,false,IM.NullI);
                  art_lines.Sort();
               }
               foreach (MemoLines.MemoLine line in art_lines.lines)
               {
                   /*  Work w = new Work();
                     w.type = WorkType.work51;
                     w.article = line.article;
                     w.count = line.workCount;
                     works.Add(w);                  
                    */
                   bool Res = false;
                   Work w = new Work();
                   w.type = WorkType.work51;
                   w.article = line.article;
                   w.count = line.workCount;
                   for (int j = 0; j < works.Count; j++)
                   {
                       if (works[j].article == line.article)
                       {
                           Res = true;
                           works[j].count += line.workCount;
                           break;
                       }
                   }
                   if (!Res)
                   {
                       works.Add(w);
                   }

               }
            }
         }

        

         XmlDocument xmlDoc = NewXML("", "DECLAR", "");
         XmlElement xmlRoot = xmlDoc.DocumentElement; //Главный элемент   
         XmlElement appElem = xmlDoc.CreateElement("", "AppPay", "");
         XmlElement header = xmlDoc.CreateElement("", "Header", "");
         IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,OWNER_ID,CREATED_DATE,STATUS,EMPLOYEE_ID,DOC_PATH,MEMO_NUMBER");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
            {
               int idOwner = rs.GetI("OWNER_ID");
               RecordPtr user = new RecordPtr(ICSMTbl.itblUsers, idOwner);
               Int64 guid = GetGUIDRecord(user);
               if (guid == 0)
                  errors.Add(string.Format(CLocaliz.TxT("Can't find GUID for ID={0} TABLE_NAME='{1}'"), user.Id, user.Table));

               {//OwnerGUID 
                  XmlElement usguid = xmlDoc.CreateElement("", "OwnerGUID", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(guid.ToString()));
                  usguid.AppendChild(value);
                  header.AppendChild(usguid);
               }
               {//OwnerPayGUID 
                   XmlElement usguid = xmlDoc.CreateElement("", "OwnerPayGUID", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(guid.ToString()));
                   usguid.AppendChild(value);
                   header.AppendChild(usguid);
               }

               string InternalNumber = rs.GetS("DOC_PATH");
               if (InternalNumber.Length > 40)
                   errors.Add(CLocaliz.TxT("In number is more than 40 symbols"));
               {//InNumber 
                   XmlElement intNum = xmlDoc.CreateElement("", "InternalNumber", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(InternalNumber));
                   intNum.AppendChild(value);
                   header.AppendChild(intNum);
               }
                
                DateTime InternalDate = rs.GetT("CREATED_DATE");
               {//InDate 
                   XmlElement intDate = xmlDoc.CreateElement("", "InternalDate", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(InternalDate)));
                   intDate.AppendChild(value);
                   header.AppendChild(intDate);
               }

               string NumberOut = "BN";
               {// OutNumber
                   XmlElement outNbr = xmlDoc.CreateElement("", "OutNumber", "");
                   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(NumberOut));
                   outNbr.AppendChild(value);
                   header.AppendChild(outNbr);
               }
               string NumberIn = "BN";
               if (NumberIn.Length > 40)
                  errors.Add(CLocaliz.TxT("In number is more than 40 symbols"));
               {//InNumber 
                  XmlElement inNum = xmlDoc.CreateElement("", "InNumber", "");
                  XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(NumberIn));
                  inNum.AppendChild(value);
                  header.AppendChild(inNum);
               }
               //DateTime dateIn = rs.GetT("CREATED_DATE");
               
               {//InDate 
                //   XmlElement inDt = xmlDoc.CreateElement("", "InDate", "");
                //   XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(IM.NullT)));
                //   inDt.AppendChild(value);
                //   header.AppendChild(inDt);
               }
               {//AppID 
                  XmlElement appid = xmlDoc.CreateElement("", "AppID", "");
                  XmlText value = xmlDoc.CreateTextNode(appId.ToString());
                  appid.AppendChild(value);
                  header.AppendChild(appid);
               }
               int usId = HelpFunction.getUserIDByName(IM.ConnectedUser());
               RecordPtr us = new RecordPtr(ICSMTbl.itblEmployee, usId);
               Int64 usid = GetGUIDRecord(us);
               if (usid == 0)
                  errors.Add(string.Format(CLocaliz.TxT("Can't find GUID for ID={0} TABLE_NAME='{1}'"), us.Id, us.Table));
               {//UserGUID 
                  XmlElement usguid = xmlDoc.CreateElement("", "UserGUID", "");
                  XmlText value = xmlDoc.CreateTextNode(usid.ToString());
                  usguid.AppendChild(value);
                  header.AppendChild(usguid);
               }
               {//NTYPE 
                  XmlElement ntype = xmlDoc.CreateElement("", "NTYPE", "");
                  XmlText value = xmlDoc.CreateTextNode("3");
                  ntype.AppendChild(value);
                  header.AppendChild(ntype);
               }
               {//FilialCode
                   XmlElement ntype = xmlDoc.CreateElement("", "FilialCode", "");
                   XmlText value = xmlDoc.CreateTextNode(CUsers.GetUserAreaCode());
                   ntype.AppendChild(value);
                   header.AppendChild(ntype);
               }
               {//Standard

                   string Standard = "";
                   if (appId != IM.NullI)
                   {
                       IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadOnly);
                       rsApp.Select("ID,PACKET_ID,STATE,DEPARTMENT,CUST_TXT1,DOC_DATE,OWNER_ID");
                       rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                       try
                       {
                           rsApp.Open();
                           if (rsApp.IsEOF() == false)
                           {
                               packetId = rsApp.GetI("PACKET_ID");
                               if (packetId != IM.NullI)
                               {

                                   IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
                                   rsPacket.Select("ID,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD,CONTENT");
                                   rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packetId);
                                   try
                                   {
                                       rsPacket.Open();
                                       if (!rsPacket.IsEOF())
                                       {
                                           Standard = rsPacket.GetS("STANDARD");
                                       }
                                   }
                                   finally
                                   {
                                       rsPacket.Close();
                                       rsPacket.Destroy();
                                   }
                               }

                           }
                              
                       }
                       finally
                       {
                           rsApp.Close();
                           rsApp.Destroy();
                       }
                   }
                 

                   XmlElement ntype = xmlDoc.CreateElement("", "Standard", "");
                   XmlText value = xmlDoc.CreateTextNode(string.IsNullOrEmpty(Standard.Trim()) ? "null" : Standard);
                   ntype.AppendChild(value);
                   header.AppendChild(ntype);
               }
               {//IsAuto
                   XmlElement isAutoElement = xmlDoc.CreateElement("", "IsAuto", "");
                   XmlText value = xmlDoc.CreateTextNode(autoRegim.Code());
                   isAutoElement.AppendChild(value);
                   header.AppendChild(isAutoElement);
               }
            }
         }
         finally
         {
            rs.Close();
            rs.Destroy();
         }
         appElem.AppendChild(header);

         if (autoRegim != AutoRegimEnum.AutoStandard)
         {
             //Формируем полный XML файл
             XmlElement worksElem = xmlDoc.CreateElement("", "Works", "");
             foreach (Work w in works)
             {
                 {
                     // WorkType
                     XmlElement WrkType = xmlDoc.CreateElement("", "WorkType", "");
                     string val = "Work51";
                     XmlText value = xmlDoc.CreateTextNode(val);
                     WrkType.AppendChild(value);
                     WrkType.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                     worksElem.AppendChild(WrkType);
                 }
             }
             foreach (Work w in works)
             {

                 {
                     // Article               
                     if (string.IsNullOrEmpty(w.article))
                         errors.Add(CLocaliz.TxT("Article is empty"));
                     XmlElement Article = xmlDoc.CreateElement("", "Article", "");
                     XmlText value = xmlDoc.CreateTextNode(w.article);
                     Article.AppendChild(value);
                     Article.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                     worksElem.AppendChild(Article);
                 }
             }
             foreach (Work w in works)
             {
                 {
                     double Count = 0; double Koef = 0; double PeopleHours = 0;
                     // Amount
                     if (w.count <= 0)
                         errors.Add(CLocaliz.TxT("Amount of a work is empty"));

                     if (w.article.Trim() == ArticleLib.ArticleFunc.article_19)
                     {
                         PeopleHours = w.count * w.index;
                     }
                     else
                     {
                         PeopleHours = w.count;
                     }

                     XmlElement Amount = xmlDoc.CreateElement("", "Amount", "");
                     XmlText value = xmlDoc.CreateTextNode(PeopleHours.ToString());
                     Amount.AppendChild(value);
                     Amount.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                     worksElem.AppendChild(Amount);
                 }
             }
             foreach (Work w in works)
             {
                 {
                     // ComplCoeff
                     XmlElement ComplCoeff = xmlDoc.CreateElement("", "ComplCoeff", "");
                     XmlText value = xmlDoc.CreateTextNode("1");
                     ComplCoeff.AppendChild(value);
                     ComplCoeff.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                     worksElem.AppendChild(ComplCoeff);
                 }
             }
             foreach (Work w in works)
             {
                 XmlElement PosCoeff = xmlDoc.CreateElement("", "PosCoeff", "");
                 XmlText value = xmlDoc.CreateTextNode("1");
                 PosCoeff.AppendChild(value);
                 PosCoeff.SetAttribute("ROWNUM", (works.IndexOf(w) + 1).ToString());
                 worksElem.AppendChild(PosCoeff);
             }
             appElem.AppendChild(worksElem);
             XmlElement Documents = xmlDoc.CreateElement("", "Documents", "");
             appElem.AppendChild(Documents);
             //AddDocuments
             XmlElement AddDocuments = xmlDoc.CreateElement("", "AddDocuments", "");
             int indexRow = 0;
             //Сеть
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     indexRow++;
                     if (work.NetId != IM.NullI)
                     {
                         XmlElement elem = xmlDoc.CreateElement("", "NetName", "");
                         XmlText value = xmlDoc.CreateTextNode(string.Format("{0}/{1}", work.NetName, work.PermNum));
                         elem.AppendChild(value);
                         elem.SetAttribute("ROWNUM", (indexRow).ToString());
                         AddDocuments.AppendChild(elem);
                     }
                 }
             }
             //Номер разрешения
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     indexRow++;
                     if (work.NetId == IM.NullI)
                     {
                         XmlElement elem = xmlDoc.CreateElement("", "NumDoc", "");
                         XmlText value = xmlDoc.CreateTextNode(work.PermNum);
                         elem.AppendChild(value);
                         elem.SetAttribute("ROWNUM", (indexRow).ToString());
                         AddDocuments.AppendChild(elem);
                     }
                 }
             }
             //Дата разрешения
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     indexRow++;
                     if (work.NetId == IM.NullI)
                     {
                         XmlElement elem = xmlDoc.CreateElement("", "DateDoc", "");
                         XmlText value = xmlDoc.CreateTextNode(SecurityElement.Escape(DateToXMLDate(work.PermDateStart)));
                         elem.AppendChild(value);
                         elem.SetAttribute("ROWNUM", (indexRow).ToString());
                         AddDocuments.AppendChild(elem);
                     }
                 }
             }
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     XmlElement elem = xmlDoc.CreateElement("", "Province", "");
                     XmlText value;
                     string tmp1 = work.Province.Trim().ToLower();
                     string tmp2 = "Вся Україна".ToLower();

                     if ((tmp1.IndexOf(";") >= 0) || (tmp1 == tmp2))
                     {
                          value = xmlDoc.CreateTextNode("Україна");
                     }
                     else
                     {
                          value = xmlDoc.CreateTextNode(work.Province);
                     }
                     
                     elem.AppendChild(value);
                     elem.SetAttribute("ROWNUM", (++indexRow).ToString());
                     AddDocuments.AppendChild(elem);
                 }
             }
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     XmlElement elem = xmlDoc.CreateElement("", "Address", "");
                     XmlText value = xmlDoc.CreateTextNode(work.adres);
                     elem.AppendChild(value);
                     elem.SetAttribute("ROWNUM", (++indexRow).ToString());
                     AddDocuments.AppendChild(elem);
                 }
             }
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     XmlElement elem = xmlDoc.CreateElement("", "Count", "");
                     XmlText value = xmlDoc.CreateTextNode(work.count.ToString());
                     elem.AppendChild(value);
                     elem.SetAttribute("ROWNUM", (++indexRow).ToString());
                     AddDocuments.AppendChild(elem);
                 }
             }
             indexRow = 0;
             foreach (MemoLines.MemoLine line in art_lines.lines)
             {
                 foreach (MemoLines.URCMWork work in line.appIds)
                 {
                     XmlElement elem = xmlDoc.CreateElement("", "Article", "");
                     XmlText value = xmlDoc.CreateTextNode(line.article);
                     elem.AppendChild(value);
                     elem.SetAttribute("ROWNUM", (++indexRow).ToString());
                     AddDocuments.AppendChild(elem);
                 }
             }
             appElem.AppendChild(AddDocuments);
         }
         xmlRoot.AppendChild(appElem);
         {// Создаем атрибут
            xmlRoot.SetAttribute("xmlns", "http://www.lissoft.com.ua/XMLSchema.xsd");
            xmlRoot.SetAttribute("DataType", "APPPAY");
            xmlRoot.SetAttribute("NAMESPACE", "http://www.lissoft.com.ua/XMLSchema.xsd");
            xmlRoot.SetAttribute("SCHEMAFILE", "XMLSchemaICSM.xsd");
         }
         xmlDoc.Save(FileName);

         string xmlschemaerror = "";
         if (!CXMLTest.TestXmlToSchema(xmlDoc.OuterXml, ref xmlschemaerror))
         {
             errors.Add(CLocaliz.TxT("Исходящий XML не прошел проверку"));
             errors.Add(xmlschemaerror);
         }
        
         return xmlDoc;
      }  
      //===================================================
      /// <summary>
      /// Создает новый XML документ
      /// В теогии все документы будут одинаковыми
      /// </summary>
      /// <param name="prefix">Префикс элемента</param>
      /// <param name="rootElem">Имя элемента</param>
      /// <returns>Новый XML документ</returns>
      private static XmlDocument NewXML(string prefix, string rootElem, string nameSpaceURI)
      {
         XmlDocument retXML = new XmlDocument();
         // Добавляем шапку XML
         XmlDeclaration xmldecl = retXML.CreateXmlDeclaration("1.0", "UTF-8", null);
         retXML.AppendChild(retXML.CreateElement(prefix, rootElem, nameSpaceURI));
         XmlElement xmlRoot = retXML.DocumentElement; //Главный элемент
         retXML.InsertBefore(xmldecl, xmlRoot);
         return retXML;
      }
      //===================================================
      /// <summary>
      /// Генерирует новое имя файла
      /// </summary>
      /// <returns>новое имя файла</returns>
      private static string GetRandomName()
      {
         string retName = System.Guid.NewGuid().ToString("N");
         return retName;
      }
      //===================================================
      /// <summary>
      /// Генерирует полный путь к временному XML файлу
      /// </summary>
      /// <returns>полный путь к XML файлу</returns>
      private static string CreateFileName()
      {
         string newName = IM.GetWorkspaceFolder() + "\\" + GetRandomName() + ".xml";
         return newName;
      }
      //===================================================
      /// <summary>
      /// Ищет GUID записи
      /// </summary>
      /// <param name="rec">RecordPtr записи</param>
      /// <returns>-1 если GUID для данной записи нет или GUID записи</returns>
      public static Int64 GetGUIDRecord(RecordPtr rec)
      {
         Int64 retVal = 0;  //Нет GUID
         IMRecordset rsGuid = new IMRecordset(PlugTbl.itblXnrfaGUID, IMRecordset.Mode.ReadOnly);
         rsGuid.Select("ID,GUID_ID,OBJ_ID,OBJ_TABLE");
         rsGuid.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rec.Id);
         rsGuid.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, rec.Table);
         try
         {
            rsGuid.Open();
            if (!rsGuid.IsEOF())
               retVal = (Int64)rsGuid.GetD("GUID_ID");
         }
         finally
         {
            rsGuid.Close();
            rsGuid.Destroy();
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Ищет запись по GUID и названию таблицы
      /// </summary>
      /// <param name="guid">GUID для данной записи нет или GUID записи</param>
      /// <returns>RecordPtr записи</returns>
      public static RecordPtr GetRecordByGUID(double guid, string name_table)
      {
          List<int> list_obj_id = new List<int>();
          List<string> list_obj_table = new List<string>();
          RecordPtr result = new RecordPtr();
          IMRecordset rsGuid = new IMRecordset(PlugTbl.itblXnrfaGUID, IMRecordset.Mode.ReadOnly);
          rsGuid.Select("ID,GUID_ID,OBJ_ID,OBJ_TABLE");
          rsGuid.SetWhere("GUID_ID", IMRecordset.Operation.Eq, guid);
          rsGuid.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, name_table);
          try
          {
              list_obj_id.Clear();
              list_obj_table.Clear();
              rsGuid.Open();
              if (!rsGuid.IsEOF())
              {
                  while (!rsGuid.IsEOF())
                  {
                      list_obj_id.Add(rsGuid.GetI("OBJ_ID"));
                      list_obj_table.Add(rsGuid.GetS("OBJ_TABLE"));
                      rsGuid.MoveNext();
                  }
                  for (int i = 0; i < list_obj_id.Count(); i++)
                  {
                      if (list_obj_id[i] == list_obj_id.Max())
                      {
                          result.Id = list_obj_id[i];
                          result.Table = list_obj_table[i];
                          break;
                      }

                  }


              }
          
          }
          finally
          {
              rsGuid.Close();
              rsGuid.Destroy();
          }
          
          return result;
      }

      //===================================================
      /// <summary>
      /// Ищет номер ID записи таблицы XNRFA_MON_CONTRACTSGUID по значению OWNER_ID и названию таблицы 
      /// </summary>
      /// <returns>RecordPtr записи</returns>
      public static int GetID_Mon_Contracts(int OWNER_ID, string name_table)
      {
          int id_mon_contracts = -1;
          RecordPtr rc = GetRecordByOBJ_ID(OWNER_ID, name_table);
          if (rc.Id > -1)
          {
              IMRecordset rsID = new IMRecordset(PlugTbl.xnrfa_monitor_contracts, IMRecordset.Mode.ReadOnly);
              rsID.Select("ID,RN_AGENT");
              rsID.SetWhere("RN_AGENT", IMRecordset.Operation.Eq, rc.Id.ToString());
          
          try
          {

              rsID.Open();
              if (!rsID.IsEOF())
              {
                  id_mon_contracts = rsID.GetI("ID");
              }

          }
          finally
          {
              rsID.Close();
              rsID.Destroy();
          }
          }

          return id_mon_contracts;
      }

      //===================================================
      /// <summary>
      /// Ищет запись по OBJ_ID  и названию таблицы
      /// </summary>
      /// <returns>RecordPtr записи</returns>
      public static RecordPtr GetRecordByOBJ_ID(int OBJ_ID, string name_table)
      {
          List<int> list_obj_id = new List<int>();
          List<string> list_obj_table = new List<string>();
          RecordPtr result = new RecordPtr();
          IMRecordset rsGuid = new IMRecordset(PlugTbl.itblXnrfaGUID, IMRecordset.Mode.ReadOnly);
          rsGuid.Select("ID,GUID_ID,OBJ_ID,OBJ_TABLE");
          rsGuid.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, OBJ_ID);
          rsGuid.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, name_table);
          try
          {
              list_obj_id.Clear();
              list_obj_table.Clear();
              rsGuid.Open();
              if (!rsGuid.IsEOF())
              {
                  while (!rsGuid.IsEOF())
                  {
                      list_obj_id.Add(rsGuid.GetI("GUID_ID"));
                      list_obj_table.Add(rsGuid.GetS("OBJ_TABLE"));
                      rsGuid.MoveNext();
                  }
                  for (int i = 0; i < list_obj_id.Count(); i++)
                  {
                      if (list_obj_id[i] == list_obj_id.Max())
                      {
                          result.Id = list_obj_id[i];
                          result.Table = list_obj_table[i];
                          break;
                      }

                  }


              }

          }
          finally
          {
              rsGuid.Close();
              rsGuid.Destroy();
          }

          return result;
      }
      //===================================================
      /// <summary>
      /// Ищет запись по GUID
      /// </summary>
      /// <param name="guid">GUID для данной записи нет или GUID записи</param>
      /// <returns>RecordPtr записи</returns>
      public static RecordPtr GetRecordByGUID(double guid)
      {
         RecordPtr result = new RecordPtr();
         IMRecordset rsGuid = new IMRecordset(PlugTbl.itblXnrfaGUID, IMRecordset.Mode.ReadOnly);
         rsGuid.Select("ID,GUID_ID,OBJ_ID,OBJ_TABLE");
         rsGuid.SetWhere("GUID_ID", IMRecordset.Operation.Eq, guid);
         try
         {
            rsGuid.Open();
            if (!rsGuid.IsEOF())
            {
               result.Id = rsGuid.GetI("OBJ_ID");
               result.Table = rsGuid.GetS("OBJ_TABLE");
            }
         }
         finally
         {
            rsGuid.Close();
            rsGuid.Destroy();
         }
         return result;
      }
      //===================================================
      /// <summary>
      /// Добавляет новый GUID записи
      /// </summary>
      /// <param name="rec">RecordPtr записи</param>
      /// <param name="guid">GUID записи</param>
      public static void AddGUIDRecord(RecordPtr rec, Int32 guid)
      {
         IMRecordset rsGuid = new IMRecordset(PlugTbl.itblXnrfaGUID, IMRecordset.Mode.ReadWrite);
         rsGuid.Select("ID,GUID_ID,OBJ_ID,OBJ_TABLE");
         rsGuid.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rec.Id);
         rsGuid.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, rec.Table);
         try
         {
            rsGuid.Open();
            if (rsGuid.IsEOF())
            {
                rsGuid.AddNew();
                rsGuid.Put("ID", IM.AllocID(PlugTbl.itblXnrfaGUID, 1, -1));
                rsGuid.Put("GUID_ID", guid);
                rsGuid.Put("OBJ_ID", rec.Id);
                rsGuid.Put("OBJ_TABLE", rec.Table);
                rsGuid.Update();
            }
         }
         finally
         {
            rsGuid.Close();
            rsGuid.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Возвращает данные о документе для XML заявки на выставление счета
      /// </summary>
      /// <param name="Link">ссылка на файл</param>
      /// <param name="DocType">тип документа</param>
      /// <param name="DocNumber">номер документа</param>
      /// <param name="EndDate">дата окончания срока действия</param>
      public static List<BaseAppClass.DocData> GetDocInfo(List<int> appIds)
      {
         List<BaseAppClass.DocData> docs = new List<BaseAppClass.DocData>();
         foreach (int id in appIds)
         {
            if (id != IM.NullI && id > 0)
            {
               List<BaseAppClass.DocData> newdocs = new List<BaseAppClass.DocData>();
               newdocs = BaseAppClass.GetDocInfo(PlugTbl.itblXnrfaAppl, id);
               for(int i = 0; i < newdocs.Count; ++i)
               {
                  BaseAppClass.DocData dd = new BaseAppClass.DocData();
                  dd = newdocs[i];
                  dd.rownum = (docs.Count + 1).ToString();
                  dd.appId = id;
                  docs.Add(dd);
               }
            }
         }
         return docs;
      }
      //===================================================
      /// <summary>
      /// Возвращает данные о документе для XML заявки на выставление счета
      /// </summary>
      /// <param name="PosGUID">GUID позиции</param>
      /// <param name="Distance">расстояние</param>
      public static void GetPositionInfo(ref string PosGUID, ref string Distance)
      {
         PosGUID = "link";
         Distance = "type";
      }
      //===================================================
      /// <summary>
      /// Возвращает KOATUU для заявки
      /// </summary>
      /// <param name="appId">Id заявки</param>
      /// <param name="KOATUU">KOATUU</param>
      public static string GetKOATUU(int appId, int lNumber)
      {
         string coatuu = BaseAppClass.GetCOATUU(PlugTbl.itblXnrfaAppl, appId, lNumber);
         return coatuu;
      }      
      //===================================================
      /// <summary>
      /// Возвращает строку с датой, пригодную для XML
      /// </summary>
      /// <param name="date">дата</param>
      /// <returns>строка с датой</returns>
      private static string DateToXMLDate(DateTime date)
      {
         return date.ToString("yyyy-MM-dd");
      }
      //===================================================
      /// <summary>
      /// Разбирает XML файл ответа из Паруса
      /// </summary>
      /// <param name="xml">текст XML</param>
      /// <param name="err">Код возможной ошибки</param>
      /// <returns>успешно или нет прошло чтение файла</returns>
      public static bool XMLAnswerRead(string xml, out int err)
      {
         string xmlschemaerror = "";
         if (!CXMLTest.TestXmlToSchema(xml, ref xmlschemaerror))
         {
            err = (int)DRVError.de_20;
            return false;
         }
         XmlDocument xmlDoc = new XmlDocument();
         xmlDoc.Load(new StringReader(xml));
         string strid = "";
         XmlElement root = xmlDoc.DocumentElement;
         XmlNodeList idList = xmlDoc.GetElementsByTagName("IDENT_ICSM");
         if (idList.Count > 0)
         {
            XmlNode el = idList[0];
            strid = el.InnerText;
         }

         if (strid == "")
         {
            err = (int)DRVError.de_1;
            return false;
         }

         XmlNodeList resultList = root.GetElementsByTagName("ENV_TYPE");
         string strresult = "";
         if (resultList.Count > 0)
            strresult = resultList[0].InnerText;

         if (strresult == "")
         {
            err = (int)DRVError.de_2;
            return false;
         }

         int appId = ConvertType.ToInt32(strid, IM.NullI);
         if (appId == IM.NullI)
         {
            err = (int)DRVError.de_3;
            return false;
         }

         int status = ConvertType.ToInt32(strresult, IM.NullI);
         if (status == IM.NullI)
         {
            err = (int)DRVError.de_4;
            return false;
         }

         XmlNodeList DATETIMEList = root.GetElementsByTagName("DATETIME");
         string strDATETIME = "";
         if (DATETIMEList.Count > 0)
            strDATETIME = DATETIMEList[0].InnerText;
         if (strDATETIME == "")
         {
            err = (int)DRVError.de_5;
            return false;
         }

         DateTime opDate = new DateTime();

         try
         {
            opDate = Convert.ToDateTime(strDATETIME);
         }
         catch (Exception)// e)
         {
            err = (int)DRVError.de_7;
            return false;
         }

         XmlNodeList ACCAGENTList = root.GetElementsByTagName("ACCAGENT");
         string strACCAGENT = "";
         if (ACCAGENTList.Count > 0)
            strACCAGENT = ACCAGENTList[0].InnerText;
         if (strACCAGENT == "")
         {
            err = (int)DRVError.de_6;
            return false;
         }

         double emplGuid = IM.NullD;
         emplGuid = ConvertType.ToDouble(strACCAGENT, IM.NullD);
         if (emplGuid == IM.NullD)
         {
            err = (int)DRVError.de_8;
            return false;
         }

         // Внесено исправление (в базе идентификаторов идет поиск только по OBJ_TABLE=ICSMTbl.itblEmployee)
         RecordPtr employee = GlobalDB.CGlobalXML.GetRecordByGUID(emplGuid, ICSMTbl.itblEmployee);
         if (employee.Id <= 0 || employee.Id == IM.NullI || employee.Table != ICSMTbl.itblEmployee)
         {
            err = (int)DRVError.de_9;
            return false;
         }

         string strACCNUMB = "";
         if (status == 4 || status == 5 || status == -42 || status == -41 || status == 3 || status == 2)
         {
            XmlNodeList ACCNUMBList = root.GetElementsByTagName("ACCNUMB");

            if (ACCNUMBList.Count > 0)
               strACCNUMB = ACCNUMBList[0].InnerText;
            if (strACCNUMB == "")
            {
               err = (int)DRVError.de_10;
               return false;
            }
         }

         string strACCDATE = "";
         DateTime accDate = new DateTime();
         if (status == 4 || status == 5 || status == -42 || status == -41 || status == 3 || status == 2)
         {
            XmlNodeList ACCDATEList = root.GetElementsByTagName("ACCDATE");

            if (ACCDATEList.Count > 0)
               strACCDATE = ACCDATEList[0].InnerText;
            if (strACCDATE == "")
            {
               err = (int)DRVError.de_11;
               return false;
            }

            try
            {
               accDate = Convert.ToDateTime(strACCDATE);
            }
            catch (Exception)// e)
            {
               err = (int)DRVError.de_12;
               return false;
            }
         }

         string strMSG = "";
         if (status == -1)
         {
            XmlNodeList MSGList = root.GetElementsByTagName("MSG");

            if (MSGList.Count > 0)
               strMSG = MSGList[0].InnerText;
            if (strMSG == "")
            {
               err = (int)DRVError.de_13;
               return false;
            }
         }

         DateTime payDate = new DateTime();
         List<XMLRecord> payDateList = new List<XMLRecord>();
         string strPAYDATE = "";
         if (status == 3)
         {
            XmlNodeList PAYDATEList = root.GetElementsByTagName("PAYDATE");
            if (PAYDATEList.Count > 0)
               strPAYDATE = PAYDATEList[0].InnerText;
            if (strPAYDATE == "")
            {
               err = (int)DRVError.de_14;
               return false;
            }
            try
            {
               payDate = Convert.ToDateTime(strPAYDATE);
            }
            catch (Exception)// e)
            {
               err = (int)DRVError.de_15;
               return false;
            }
         }
         else if (status == 4)
         {
            XmlNodeList PAYDATEList = root.GetElementsByTagName("PAYDATE");
            foreach (XmlNode node in PAYDATEList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               payDateList.Add(rec);
               if (node != PAYDATEList[0])
                  strPAYDATE += " / " + node.InnerText;
               else
                  strPAYDATE = node.InnerText;
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_14;
                  return false;
               }
            }

         }

         string strPAYNUMB = "";
         List<XMLRecord> payNumbList = new List<XMLRecord>();
         if (status == 4)
         {
            XmlNodeList PAYNUMBList = root.GetElementsByTagName("PAYNUMB");
            foreach (XmlNode node in PAYNUMBList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               payNumbList.Add(rec);
               if (node != PAYNUMBList[0])
                  strPAYNUMB += " / " + node.InnerText;
               else
                  strPAYNUMB = node.InnerText;
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_16;
                  return false;
               }
            }

         }
         string strFORMNUMB = "";
         List<XMLRecord> forNumbList = new List<XMLRecord>();
         if (status == 5)
         {
            XmlNodeList FORMNUMBList = root.GetElementsByTagName("FORMNUMB");
            foreach (XmlNode node in FORMNUMBList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               forNumbList.Add(rec);
               if (node != FORMNUMBList[0])
                  strFORMNUMB += " / " + node.InnerText;
               else
                  strFORMNUMB = node.InnerText;
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_17;
                  return false;
               }
            }
         }
         string strISSUEDAT = "";
         List<XMLRecord> issueDateList = new List<XMLRecord>();
         List<XMLRecord> numberList = new List<XMLRecord>();
         List<XMLRecord> typeList = new List<XMLRecord>();
         if (status == 5)
         {
            XmlNodeList ISSUEDATList = root.GetElementsByTagName("ISSUEDAT");
            foreach (XmlNode node in ISSUEDATList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               issueDateList.Add(rec);
               if (node != ISSUEDATList[0])
                  strISSUEDAT += " / " + node.InnerText;
               else
                  strISSUEDAT = node.InnerText;
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_18;
                  return false;
               }
            }
            XmlNodeList NUMBERList = root.GetElementsByTagName("NUMBER");
            foreach (XmlNode node in NUMBERList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               numberList.Add(rec);
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_25;
                  return false;
               }
            }
            XmlNodeList DOCTYPEList = root.GetElementsByTagName("DOCTYPE");
            foreach (XmlNode node in DOCTYPEList)
            {
               XMLRecord rec = new XMLRecord();
               rec.rownum = node.Attributes["ROWNUM"].InnerText;
               rec.value = node.InnerText;
               typeList.Add(rec);
               if (node.InnerText == "")
               {
                  err = (int)DRVError.de_26;
                  return false;
               }
            }
         }


         IMRecordset rsDrvApplURCM = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
         rsDrvApplURCM.Select("ID,INVOICE,INVOICE_FROM,INVOICE_TO,INV_EMPLOYEE_ID");
         rsDrvApplURCM.SetWhere("ID", IMRecordset.Operation.Eq, appId);
         try
         {
             rsDrvApplURCM.Open();
             if (!rsDrvApplURCM.IsEOF())
             {
                 rsDrvApplURCM.Edit();
                 rsDrvApplURCM.Put("INVOICE", strACCNUMB);
                 rsDrvApplURCM.Put("INVOICE_FROM", accDate);
                 rsDrvApplURCM.Put("INVOICE_TO", payDate);
                 rsDrvApplURCM.Put("INV_EMPLOYEE_ID", employee.Id);
                 rsDrvApplURCM.Update();
             }
         }
         finally
         {
             rsDrvApplURCM.Close();
             rsDrvApplURCM.Destroy();
         }

         IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
         rsApp.Select("ID,STATE,INVOICE,INVOICE_FROM,INVOICE_TO,INV_EMPLOYEE_ID");
         rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);

         try
         {
            rsApp.Open();
            if (!rsApp.IsEOF())
            {
               rsApp.Edit();
               rsApp.Put("STATE", status);
               rsApp.Put("INVOICE", strACCNUMB);
               rsApp.Put("INVOICE_FROM", accDate);
               rsApp.Put("INVOICE_TO", payDate);
               rsApp.Put("INV_EMPLOYEE_ID", employee.Id);
               rsApp.Update();
            }
            else
            {
               err = (int)DRVError.de_19;
               return false;
            }
         }
         finally
         {
            rsApp.Close();
            rsApp.Destroy();
         }

         List<int> appIds = new List<int>();
         int ownerPayId = IM.NullI;
         using (Icsm.LisRecordSet rsAps = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadOnly))
         {
             bool isFirst = true;
             rsAps.Select("ID,APPLPAY_ID,APPL_ID,ApplicationPay.OWNER_ID");
             rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
             for (rsAps.Open(); !rsAps.IsEOF(); rsAps.MoveNext())
             {
                 if (isFirst)
                 {
                     isFirst = false;
                     ownerPayId = rsAps.GetI("ApplicationPay.OWNER_ID");
                 }
                 appIds.Add(rsAps.GetI("APPL_ID"));
             }
         }

         List<XMLRecord> newEventsFor5 = new List<XMLRecord>();
         if (status == 5)
         {
            List<BaseAppClass.DocData> ddlist = new List<BaseAppClass.DocData>();
            ddlist = GetDocInfo(appIds);
            for (int i = 0; i < numberList.Count; ++i)
            {
               foreach (BaseAppClass.DocData docdst in ddlist)
               {
                  if (numberList[i].value == docdst.Number)
                  {
                     XMLRecord rec = new XMLRecord();
                     rec.rownum = numberList[i].rownum;
                     rec.appId = docdst.appId;
                     rec.numDoc = numberList[i].value;
                     for (int j = 0; j < forNumbList.Count; ++j)
                     {
                        if (forNumbList[j].rownum == numberList[i].rownum)
                        {
                            rec.value = forNumbList[j].value;
                            break;
                        }
                     }
                     for (int j = 0; j < issueDateList.Count; ++j)
                     {
                        if (issueDateList[j].rownum == numberList[i].rownum)
                        {
                           rec.date = issueDateList[j].value;
                           break;
                        }
                     }
                     for (int j = 0; j < typeList.Count; ++j)
                     {
                        if (typeList[j].rownum == numberList[i].rownum)
                        {
                           if (typeList[j].value == "EXPL")
                              rec.type = EDocEvent.evDOZV;
                           else if (typeList[j].value == "EMS")
                              rec.type = EDocEvent.evVISN;
                           else
                           {
                              err = (int)DRVError.de_27;
                              return false;
                           }
                           break;
                        }
                     }
                     newEventsFor5.Add(rec);
                     break;
                  }
               }
            }
         }

         if (status == 5)
         {
            List<XMLRecord> dozvola = new List<XMLRecord>();
            List<XMLRecord> vysnovki = new List<XMLRecord>();
            foreach (XMLRecord rec in newEventsFor5)
            {
               if(rec.type == EDocEvent.evVISN)
                  vysnovki.Add(rec);
               else
               {
                  dozvola.Add(rec);
               }
            }

            
             
            foreach (XMLRecord rec in vysnovki)
            {
               DateTime From = Convert.ToDateTime(rec.date);
               DateTime To = IM.NullT;
               To = From.AddMonths(6);
               CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, rec.appId, EDocEvent.evDRVDocVyd, opDate, rec.value, From, "", employee.Id);
               CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, rec.appId, rec.type, From, rec.numDoc, To, "", employee.Id);
               Documents.ApplDocuments.PrintConclusionBase(rec.appId, rec.numDoc, From, rec.value);
               StationStatus wf = new StationStatus();
               StatusType curStatus = BaseAppClass.ReadStatus(rec.appId);
               StatusType newStatus = wf.GetNewStatus(curStatus, EDocEvent.evDRVPrintVisn);
               if ((curStatus != newStatus) && (EPacketType.PckSetCallSign != GetPacketType(rec.appId)))
                   BaseAppClass.UpdateStatus(newStatus, rec.appId, employee.Id);
            }
            foreach (XMLRecord rec in dozvola)
            {
               DateTime From = Convert.ToDateTime(rec.date);
               DateTime To = IM.NullT;
               CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, rec.appId, EDocEvent.evDRVDocVyd, opDate, rec.value, From, "", employee.Id);
               CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, rec.appId, rec.type, From, rec.numDoc, To, "", employee.Id);
               Documents.ApplDocuments.PrintPermisionBase(rec.appId, rec.numDoc, From, rec.value);
               StationStatus wf = new StationStatus();
               StatusType curStatus = BaseAppClass.ReadStatus(rec.appId);
               StatusType newStatus = wf.GetNewStatus(curStatus, EDocEvent.evDRVPrintDozv);
               if ((curStatus != newStatus) && (EPacketType.PckSetCallSign != GetPacketType(rec.appId)))
                   BaseAppClass.UpdateStatus(newStatus, rec.appId, employee.Id);
            }
         }
         else
         {
            foreach (int selAppId in appIds)
            {
               if (status == -1)
                  CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVVidh, opDate, "", IM.NullT, CLocaliz.TxT("DRV_errorCode"), employee.Id);
               else if (status == 2)
                  CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAccForm, opDate, strACCNUMB, IM.NullT, "", employee.Id);
               else if (status == 3)
                  CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVVyst, opDate, strACCNUMB, payDate, "", employee.Id);
               else if (status == 4)
               {
                   using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                   {
                       using (Icsm.LisRecordSet rsAppl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadWrite))
                       {
                           //Устанавливаем плательщика
                           rsAppl.Select("ID,PAY_OWNER_ID");
                           rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, selAppId);
                           rsAppl.Open();
                           if (!rsAppl.IsEOF())
                           {
                               rsAppl.Edit();
                               rsAppl.Put("PAY_OWNER_ID", ownerPayId);
                               rsAppl.Update();
                           }
                       }
                       //CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAccPay4, opDate, strPAYNUMB, Convert.ToDateTime(payDateList[0].value), strPAYDATE, employee.Id);
                       CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAccPay4, opDate, strACCNUMB, Convert.ToDateTime(payDateList[0].value), strPAYDATE, employee.Id);
                       tr.Commit();
                   }
               }
               else if (status == -5)
                   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAppAnnul, opDate, "", IM.NullT, "", employee.Id);
               else if (status == 1)
                   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAddApp, opDate, "", IM.NullT, "", employee.Id);
               else if (status == -41)
                   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAccAnnul41, opDate, strACCNUMB, IM.NullT, "", employee.Id);
               else if (status == -42)
                   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, selAppId, EDocEvent.evDRVAccAnul42, opDate, strACCNUMB, IM.NullT, "", employee.Id);
            }
         }
         err = 0;
         return true;
      }

      /// <summary>
      /// Получить тип пакета
      /// </summary>
      /// <returns></returns>
      public static EPacketType GetPacketType(int ApplID_)
      {
          
          EPacketType packetType = EPacketType.PckUnknown;
          IMRecordset lk_ = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
          lk_.Select("APPL_ID,PACKET_ID");
          lk_.SetWhere("APPL_ID", IMRecordset.Operation.Eq, ApplID_);
          lk_.Open();
          if (!lk_.IsEOF())
          {
              IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
              rsPacket.Select("ID,CONTENT");
              rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, lk_.GetI("PACKET_ID"));
              rsPacket.Open();
              if (!rsPacket.IsEOF())
              {// Пакет существует

                  try
                  {
                      packetType = (EPacketType)Enum.Parse(typeof(EPacketType), rsPacket.GetS("CONTENT"));
                  }
                  catch
                  {
                      packetType = EPacketType.PckUnknown;
                  }

              }
              rsPacket.Close();
              rsPacket.Destroy();
          }
          lk_.Close();
          lk_.Destroy();
          return packetType;
      }
       
   }

    
}
