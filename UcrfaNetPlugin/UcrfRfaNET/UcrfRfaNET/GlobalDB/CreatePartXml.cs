﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.GlobalDB
{
    /// <summary>
    /// Клас, який з вибраних записів(1 або більше) формує єдиний XML файл
    /// </summary>
    class CreatePartXml
    {
        /// <summary>
        /// Вертає сформований XDoc 
        /// </summary>
        /// <param name="r">Рекордсет по доступу до БД</param>
        /// <param name="listFields">список необхідних полів</param>
        /// <param name="context">контекст виділених записів</param>
        /// <param name="curDepth">поточна глибина</param>
        /// <param name="maxDepth">макс глибин</param>
        /// <param name="refName">таблиця по ссилкі</param>
        /// <returns>XML</returns>
        public XDocument ExportTableRecursion(IMRecordset r, Dictionary<string, string> listFields, IMQueryMenuNode.Context context, int curDepth, int maxDepth, string refName)
        {
            XDocument xdoc = new XDocument(new XDeclaration("1.0", "UTF-8", null));
            OrmTable ormTab = OrmSchema.Table(context.TableName);
            OrmField[] fldMas = ormTab.Fields;
            List<string> listSelect = new List<string>();

            //Формування Select
            GetCorrectNameFields(fldMas, listFields.Keys.ToList(), listSelect);
            string selFld = GetListSelect(listSelect);
            if (selFld.Length > 0)
                r.Select(selFld);
            r.Select("ID");
            r.Select("TABLE_NAME");
            //Формування набору записів
            r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
            try
            {
                XElement root = new XElement("BODY");
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    XElement tableRoot = new XElement(context.TableName);
                    GetXDoc(fldMas, listSelect, context.TableName, r, tableRoot, refName);
                    root.Add(tableRoot);
                }
                              
                xdoc.Add(root);
            }
            finally
            {
                r.Final();
            }

            return xdoc;
        }

        /// <summary>
        /// Вертає коректні значення імен для таблиці
        /// </summary>
        /// <param name="fldMas"></param>
        /// <param name="listFields"></param>
        /// <param name="listSelect"></param>
        private void GetCorrectNameFields(OrmField[] fldMas, List<string> listFields, List<string> listSelect)
        {
            foreach (OrmField fldCicle in fldMas)
            {
                if (fldCicle == null)
                    continue;
                if (fldCicle is OrmFieldF)
                {
                    OrmFieldF fld = fldCicle as OrmFieldF;
                    if ((fld.DDesc != null) &&
                        (fld.IsCompiledInEdition) &&
                        (fld.Nature == OrmFieldNature.Column) &&
                        ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                         (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                         (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)) &&
                        (listFields.Contains(/*CLocaliz.TxT*/(fld.Name))))
                    {
                        try
                        {

                            listSelect.Add(fld.Name);
                        }
                        catch (Exception ex)
                        {
                            CLogs.WriteError(ELogsWhat.AutoXml, ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Отримати зі списку полів строку для роботи з r.Select 
        /// </summary>
        /// <param name="list"> список </param>
        /// <returns> строку </returns>
        private string GetListSelect(List<string> list)
        {
            string selectedFld = "";
            foreach (string s in list)
                if (s != "ID")
                    selectedFld += s + ",";
            return selectedFld.ToSubstringLast();
        }

        /// <summary>
        /// Добавляе значение в XML
        /// </summary>
        /// <param name="root">XElement базовый элемент XML</param>  
        private void AddElement(XElement root, string elemName, ExportToXmlUtdb.ElemType elemType, string refName, object val)
        {
            XElement subItem = new XElement(elemName);
            subItem.SetValue(val);
            root.Add(subItem);
        }

        /// <summary>
        /// Додатково вносить елементи elemName зі значення val
        /// </summary>
        /// <param name="elemName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private void AdditionalFields(string elemName, string val, XElement root)
        {
            XElement subItem = new XElement(elemName);
            subItem.SetValue(val);
            root.Add(subItem);
        }

        /// <summary>
        /// Формує XDocument
        /// </summary>
        /// <param name="fldMas"></param>
        /// <param name="listSelect"></param>
        /// <param name="tableName"></param>
        /// <param name="rs"></param>
        /// <param name="root"></param>
        /// <param name="refName"></param>
        private void GetXDoc(OrmField[] fldMas, List<string> listSelect, string tableName, IMRecordset rs, XElement root, string refName)
        {
            foreach (OrmField fldCicle in fldMas)
            {
                if (!listSelect.Contains(fldCicle.Name) || fldCicle == null)
                    continue;
                if (fldCicle is OrmFieldF)
                {
                    string depthNameAll = (!string.IsNullOrEmpty(tableName))
                                              ? string.Format("{0}.{1}", tableName, fldCicle.Name)
                                              : fldCicle.Name;
                    OrmFieldF fld = fldCicle as OrmFieldF;
                    if ((fld.DDesc != null) &&
                        (fld.IsCompiledInEdition) &&
                        (fld.Nature == OrmFieldNature.Column) &&
                        (listSelect.Contains(fld.Name)))
                    {
                        switch (fld.DDesc.Coding)
                        {
                            case OrmDataCoding.tvalDATETIME:
                                {
                                    DateTime dt = rs.GetT(fld.Name);
                                    if (dt != IM.NullT)
                                        AddElement(root, depthNameAll, ExportToXmlUtdb.ElemType.Date, refName, dt);
                                }
                                break;
                            case OrmDataCoding.tvalNUMBER:
                                {
                                    double valDouble = rs.GetD(fld.Name);
                                    if (valDouble != IM.NullD)
                                        AddElement(root, depthNameAll, ExportToXmlUtdb.ElemType.Real, refName, valDouble);
                                }
                                break;
                            case OrmDataCoding.tvalSTRING:
                                {
                                    string valString = rs.GetS(fld.Name);
                                    if (!string.IsNullOrEmpty(valString))
                                        AddElement(root, depthNameAll, ExportToXmlUtdb.ElemType.String, refName,
                                                   valString);
                                }
                                break;
                        }
                    }
                }
                else if (fldCicle is OrmFieldJ)
                {
                    OrmFieldJ fld = fldCicle as OrmFieldJ;
                    if ((fld.Join != null) &&
                        (fld.Join.JoinedTable != null) &&
                        (fld.Join.From[0].IsCompiledInEdition) &&
                        (fld.Join.To[0].IsCompiledInEdition))
                    {
                        string nameFieldFrom = fld.Join.From[0].Name;
                        string nameFieldTo = fld.Join.To[0].Name;
                        string nameTable = fld.Join.JoinedTableName;
                        if (!string.IsNullOrEmpty(nameFieldFrom) &&
                            !string.IsNullOrEmpty(nameFieldTo) &&
                            !string.IsNullOrEmpty(nameTable) &&
                            (nameFieldTo.ToUpper() == "ID"))
                        {
                            // TODO якщо необхідно буде рекурсія по таблицях
                            // string refNameAll = string.Format("{0}.{1}", tableName, nameFieldFrom);

                            //int newId = 0;
                            //if ((string.IsNullOrEmpty(selectFields) == false) && (selectFields.Split(',').Length > 0))
                            //    newId = rs.GetI(nameFieldFrom);

                            // ExportTableRecursion(root, nameTable, newId, ++curDepth, maxDepth, refNameAll, selectFields);
                        }
                    }
                }
            }
            //додати поле підрозділ, означення
            string subUnit = EriFiles.GetEriDescription(rs.GetS("TABLE_NAME"), "ALL_STATIONS");
            //string defin = EriFiles.GetEriDescription("SID", "ALL_STATIONS");

            AdditionalFields(tableName + "." + rs.GetS("TABLE_NAME"), subUnit, root);
            //AdditionalFields(tableName + ".SID", defin, root);     
        }
    }
}
