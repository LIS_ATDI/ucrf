﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.GlobalDB
{
    public partial class GlobalDbTest : Form
    {
        private readonly string _icsmTestService = EService.IcsmTest;
        private readonly string _icsmTestEssence = EEssence.TestEssence;

        private GlobalDbLocal _globalDb;
        private GlobalDbLocal GlobalDb
        {
            get
            {
                return _globalDb ?? (_globalDb = new GlobalDbLocal());
            }
        }

        public GlobalDbTest()
        {
            InitializeComponent();
        }

        private void GlobalDbTest_Load(object sender, System.EventArgs e)
        {
            GetDbProviderInfo();
        }


        private void GetDbProviderInfo()
        {
            DbProviderInfo dbProviderInfo = new OracleDbProvider().GetDbProviderInfo();
            if(dbProviderInfo == null)
            {
                WriteMessage("Провайдер Oracle не знайдено!!!");
            }
            //var stringBuilder = new StringBuilder();
            WriteMessage(string.Format("=>Назва провайдера          : {0}",dbProviderInfo.Name));
            WriteMessage(string.Format("=>Унікальна назва провайдера: {0}",dbProviderInfo.InvariantName));
            WriteMessage(string.Format("=>Інформація про бібліотеку : {0}",dbProviderInfo.AssemblyQualifiedName));
            WriteMessage(string.Format("=>Опис                      : {0}",dbProviderInfo.Description));
        }

        private void ClearMessage()
        {
            OutTextBox.Clear();
        }

        private void AddMessage(string message)
        {
            OutTextBox.Text += string.Concat(message, Environment.NewLine);
        }

        private void WriteException(Exception exception)
        {
            AddMessage("Помилка:");
            AddMessage(exception.Message);
            AddMessage(exception.StackTrace);
            Exception innerException = exception.InnerException;
            if (innerException != null)
            {
                AddMessage("==================================");
                AddMessage("Внутрішня помилка:");
                WriteException(innerException);
            }
        }

        private void WriteMessage(string message)
        {
            AddMessage(message);
        }

        private void Execute(Action func)
        {
            try
            {
                Enabled = false;
                ClearMessage();
                WriteMessage("Подключення...");
                if(GlobalDb.OpenConnection())
                {
                    WriteMessage("Подключення виконано");
                    WriteMessage("====================");
                    func();
                }
            }
            catch(Exception ex)
            {
                WriteException(ex);
            }
            finally
            {
                Enabled = true;
                GlobalDb.CloseConnection();
            }
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                bool isTestServiceExist = false;
                bool isTestEssenceExist = false;
                WriteMessage("=> Зчитування сервісів...");
                String[] services = GlobalDb.GetServices();
                foreach(string service in services)
                {
                    WriteMessage(string.Format(" -> {0}", service));
                    if(_icsmTestService == service)
                        isTestServiceExist = true;
                }
                WriteMessage("=> Зчитування сутностей...");
                String[] essences = GlobalDb.GetEssences();
                foreach(string essence in essences)
                {
                    WriteMessage(string.Format(" -> {0}", essence));
                    if(_icsmTestEssence == essence)
                        isTestEssenceExist = true;
                }
                if(!isTestServiceExist)
                    WriteMessage(string.Format("<== Відсутній тестовий сервіс \"{0}\"==>", _icsmTestService));
                if(!isTestEssenceExist)
                    WriteMessage(string.Format("<== Відсутня тестова сутність \"{0}\"==>", _icsmTestEssence));
                if(CUsers.ConnectedUser() == PluginSetting.PluginFolderSetting.UserByDefault)
                {
                    if(isTestServiceExist && isTestEssenceExist)
                    {
                        btnGenerateGuid.Enabled = true;
                        btnGetCount.Enabled = true;
                        btnReadData.Enabled = true;
                        btnSendData.Enabled = true;
                        btnInstalService.Enabled = false;
                    }
                    else
                    {
                        btnGenerateGuid.Enabled = false;
                        btnGetCount.Enabled = false;
                        btnReadData.Enabled = false;
                        btnSendData.Enabled = false;
                        btnInstalService.Enabled = true;
                    }
                }
            });
        }

        private void btnGenerateGuid_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                WriteMessage("Генерація нового GUID...");
                WriteMessage(string.Format(" -> {0}", GlobalDb.NewGuid("GUID")));
            });
        }

        private void btnGetCount_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                WriteMessage("Кількість не прочитаних записів...");
                WriteMessage(string.Format(" -> {0}", GlobalDb.GetCountRecords(_icsmTestService)));
            });
        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                WriteMessage("Запис даних...");
                string fileName = string.Empty;
                using(var dlg = new OpenFileDialog())
                {
                    if(dlg.ShowDialog() == DialogResult.OK)
                        fileName = dlg.FileName;
                }
                if(!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
                {
                    using(TextReader sr = new StreamReader(fileName, Encoding.GetEncoding(1251)))
                    {
                        string data = sr.ReadToEnd();
                        if(GlobalDb.WriteXml(data, _icsmTestEssence, _icsmTestService))
                            WriteMessage("Виконано");
                        else
                            WriteMessage("Помилка");
                    }
                }
            });
        }

        private void btnReadData_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                if(GlobalDb.GetCountRecords(_icsmTestService) <= 0)
                {
                    WriteMessage("Відсутні дані для зчитування");
                    return;
                }
                using(var saveFileDialog1 = new SaveFileDialog())
                {
                    saveFileDialog1.Filter = "All files (*.*)|*.*";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.RestoreDirectory = true;
                    saveFileDialog1.CheckPathExists = true;
                    if(saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        using(Stream fileStream = saveFileDialog1.OpenFile())
                        {
                            using(TextWriter sr = new StreamWriter(fileStream, Encoding.GetEncoding(1251)))
                            {
                                string readData = GlobalDb.ReadXml(_icsmTestService);
                                sr.Write(readData);
                            }
                            GlobalDb.ChangeStatus(1, 0, _icsmTestService);
                            WriteMessage("Виконано");
                        }
                    }
                }
            });
        }

        private void btnInstalService_Click(object sender, EventArgs e)
        {
            Execute(() =>
            {
                bool isTestServiceExist = false;
                bool isTestEssenceExist = false;
                String[] services = GlobalDb.GetServices();
                foreach(string service in services)
                {
                    if(_icsmTestService == service)
                    {
                        isTestServiceExist = true;
                        break;
                    }
                }
                String[] essences = GlobalDb.GetEssences();
                foreach(string essence in essences)
                {
                    if(_icsmTestEssence == essence)
                    {
                        isTestEssenceExist = true;
                        break;
                    }
                }
                if(!isTestServiceExist)
                {
                    GlobalDb.AddService(_icsmTestService);
                    WriteMessage(string.Format("Встановлено сервис '{0}'", _icsmTestService));
                }
                if(!isTestEssenceExist){
                    GlobalDb.AddEssence(_icsmTestEssence);
                    WriteMessage(string.Format("Встановлено сутність '{0}'", _icsmTestEssence));
                }
                if(!isTestServiceExist || !isTestEssenceExist){
                    GlobalDb.BindServiceWithEssence(_icsmTestService, _icsmTestEssence);
                    WriteMessage(string.Format("Встановлено зв'язок між сервісом '{0}' и сутністю '{1}'", _icsmTestService, _icsmTestEssence));
                }
                btnGenerateGuid.Enabled = true;
                btnGetCount.Enabled = true;
                btnReadData.Enabled = true;
                btnSendData.Enabled = true;
                btnInstalService.Enabled = false;
            });
        }
    }
}
