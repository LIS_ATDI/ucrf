﻿namespace XICSM.UcrfRfaNET.GlobalDB
{
    partial class GlobalDbTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Connect = new System.Windows.Forms.Button();
            this.OutTextBox = new System.Windows.Forms.TextBox();
            this.btnGenerateGuid = new System.Windows.Forms.Button();
            this.btnGetCount = new System.Windows.Forms.Button();
            this.btnSendData = new System.Windows.Forms.Button();
            this.btnReadData = new System.Windows.Forms.Button();
            this.btnInstalService = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(12, 12);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(198, 23);
            this.Connect.TabIndex = 1;
            this.Connect.Text = "Тестове підключення";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // OutTextBox
            // 
            this.OutTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OutTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OutTextBox.Location = new System.Drawing.Point(216, 12);
            this.OutTextBox.Multiline = true;
            this.OutTextBox.Name = "OutTextBox";
            this.OutTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.OutTextBox.Size = new System.Drawing.Size(750, 379);
            this.OutTextBox.TabIndex = 2;
            this.OutTextBox.WordWrap = false;
            // 
            // btnGenerateGuid
            // 
            this.btnGenerateGuid.Enabled = false;
            this.btnGenerateGuid.Location = new System.Drawing.Point(12, 41);
            this.btnGenerateGuid.Name = "btnGenerateGuid";
            this.btnGenerateGuid.Size = new System.Drawing.Size(198, 23);
            this.btnGenerateGuid.TabIndex = 4;
            this.btnGenerateGuid.Text = "Генерація нового GUID";
            this.btnGenerateGuid.UseVisualStyleBackColor = true;
            this.btnGenerateGuid.Click += new System.EventHandler(this.btnGenerateGuid_Click);
            // 
            // btnGetCount
            // 
            this.btnGetCount.Enabled = false;
            this.btnGetCount.Location = new System.Drawing.Point(12, 70);
            this.btnGetCount.Name = "btnGetCount";
            this.btnGetCount.Size = new System.Drawing.Size(198, 23);
            this.btnGetCount.TabIndex = 5;
            this.btnGetCount.Text = "Кіль-сть не прочитаних записів";
            this.btnGetCount.UseVisualStyleBackColor = true;
            this.btnGetCount.Click += new System.EventHandler(this.btnGetCount_Click);
            // 
            // btnSendData
            // 
            this.btnSendData.Enabled = false;
            this.btnSendData.Location = new System.Drawing.Point(12, 99);
            this.btnSendData.Name = "btnSendData";
            this.btnSendData.Size = new System.Drawing.Size(198, 23);
            this.btnSendData.TabIndex = 6;
            this.btnSendData.Text = "Відправити дані";
            this.btnSendData.UseVisualStyleBackColor = true;
            this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // btnReadData
            // 
            this.btnReadData.Enabled = false;
            this.btnReadData.Location = new System.Drawing.Point(12, 128);
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.Size = new System.Drawing.Size(198, 23);
            this.btnReadData.TabIndex = 7;
            this.btnReadData.Text = "Прочитати дані";
            this.btnReadData.UseVisualStyleBackColor = true;
            this.btnReadData.Click += new System.EventHandler(this.btnReadData_Click);
            // 
            // btnInstalService
            // 
            this.btnInstalService.Enabled = false;
            this.btnInstalService.Location = new System.Drawing.Point(12, 157);
            this.btnInstalService.Name = "btnInstalService";
            this.btnInstalService.Size = new System.Drawing.Size(198, 23);
            this.btnInstalService.TabIndex = 8;
            this.btnInstalService.Text = "Встановити тестовий сервіс";
            this.btnInstalService.UseVisualStyleBackColor = true;
            this.btnInstalService.Click += new System.EventHandler(this.btnInstalService_Click);
            // 
            // GlobalDbTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 403);
            this.Controls.Add(this.btnInstalService);
            this.Controls.Add(this.btnReadData);
            this.Controls.Add(this.btnSendData);
            this.Controls.Add(this.btnGetCount);
            this.Controls.Add(this.btnGenerateGuid);
            this.Controls.Add(this.OutTextBox);
            this.Controls.Add(this.Connect);
            this.Name = "GlobalDbTest";
            this.Text = "Тест UTDB";
            this.Load += new System.EventHandler(this.GlobalDbTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.TextBox OutTextBox;
        private System.Windows.Forms.Button btnGenerateGuid;
        private System.Windows.Forms.Button btnGetCount;
        private System.Windows.Forms.Button btnSendData;
        private System.Windows.Forms.Button btnReadData;
        private System.Windows.Forms.Button btnInstalService;

    }
}