﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.GlobalDB
{

    internal class DbProviderInfo
    {
        public string Name
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }

        public string InvariantName
        {
            get;
            private set;
        }
        public string AssemblyQualifiedName
        {
            get;
            private set;
        }

        public DbProviderInfo(string name, string description, string invariantName, string assemblyQualifiedName)
        {
            Name = name;
            Description = description;
            InvariantName = invariantName;
            AssemblyQualifiedName = assemblyQualifiedName;
        }

    }

    internal class OracleDbProvider
    {

        private const string OracleDbProviderName = "Oracle.DataAccess.Client";

        private DbProviderFactory _dbProviderFactory;
        public DbProviderFactory DbProviderFactory
        {
            get
            {
                return _dbProviderFactory ?? (_dbProviderFactory = DbProviderFactories.GetFactory(OracleDbProviderName));
            }
        }

        public DbProviderInfo GetDbProviderInfo()
        {
            const int nameIndex = 0;
            const int descriptionIndex = 1;
            const int invariantNameIndex = 2;
            const int assemblyQualifiedNameIndex = 3;
            DataTable table = DbProviderFactories.GetFactoryClasses();
            foreach(DataRow row in table.Rows)
            {
                string invariantName = row[invariantNameIndex].ToString();
                if(invariantName == OracleDbProviderName)
                {
                    string name = row[nameIndex].ToString();
                    string description = row[descriptionIndex].ToString();
                    string assemblyQualifiedName = row[assemblyQualifiedNameIndex].ToString();
                    return new DbProviderInfo(name, description, invariantName, assemblyQualifiedName);
                }
            }
            return null;
        }

    }

    class GlobalDbLocal : IGlobalDb
    {

        private const int MaxStrigLength = 4000;
        private DbConnection _dbConnection;

        private DbProviderFactory DbProviderFactory
        {
            get
            {
                return OracleDbProvider.DbProviderFactory;
            }
        }

        private OracleDbProvider _oracleDbProvider;
        private OracleDbProvider OracleDbProvider
        {
            get
            {
                return _oracleDbProvider ?? (_oracleDbProvider = new OracleDbProvider());
            }
        }

        private int GetNextExchangeId()
        {
            int retVal = -1;
            if(_dbConnection == null)
                return retVal;
            const string sqlQuery = "select UTDB.GetNextExchangeId as iRecId from dual";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                {
                    object str = dr.GetValue(dr.GetOrdinal("iRecId"));
                    retVal = str.ToString().ToInt32(-1);
                }
            }
            return retVal;
        }

        private void WriteStringXmlFirst(int recId, string strXml, string service, string essence)
        {
            if(_dbConnection == null)
                return;
            DbCommand cmd = _dbConnection.CreateCommand();
            cmd.Connection = _dbConnection;
            cmd.CommandText = "UTDB.WriteStringXmlFirst";
            cmd.CommandType = CommandType.StoredProcedure;
            DbParameter recIdParm = cmd.CreateParameter();
            recIdParm.ParameterName = "iRecId";
            recIdParm.DbType = DbType.Int64;
            recIdParm.Direction = ParameterDirection.Input;
            recIdParm.Value = recId;
            DbParameter strXmlParam = cmd.CreateParameter();
            strXmlParam.ParameterName = "strXml";
            strXmlParam.DbType = DbType.String;
            strXmlParam.Direction = ParameterDirection.Input;
            strXmlParam.Value = strXml;
            DbParameter strEssenceParam = cmd.CreateParameter();
            strEssenceParam.ParameterName = "strEssence";
            strEssenceParam.DbType = DbType.String;
            strEssenceParam.Direction = ParameterDirection.Input;
            strEssenceParam.Value = essence;
            DbParameter strServiceParam = cmd.CreateParameter();
            strServiceParam.ParameterName = "strService";
            strServiceParam.DbType = DbType.String;
            strServiceParam.Direction = ParameterDirection.Input;
            strServiceParam.Value = service;
            cmd.Parameters.Add(recIdParm);
            cmd.Parameters.Add(strXmlParam);
            cmd.Parameters.Add(strEssenceParam);
            cmd.Parameters.Add(strServiceParam);
            cmd.ExecuteNonQuery();
        }

        private void WriteStringXmlLast(int recId, string strXml)
        {
            if(_dbConnection == null)
                return;
            DbCommand cmd = _dbConnection.CreateCommand();
            cmd.Connection = _dbConnection;
            cmd.CommandText = "UTDB.WriteStringXmlLast";
            cmd.CommandType = CommandType.StoredProcedure;
            DbParameter recIdParm = cmd.CreateParameter();
            recIdParm.ParameterName = "iRecId";
            recIdParm.DbType = DbType.Int64;
            recIdParm.Direction = ParameterDirection.Input;
            recIdParm.Value = recId;
            DbParameter strXmlParam = cmd.CreateParameter();
            strXmlParam.ParameterName = "strXml";
            strXmlParam.DbType = DbType.String;
            strXmlParam.Direction = ParameterDirection.Input;
            strXmlParam.Value = strXml;
            cmd.Parameters.Add(recIdParm);
            cmd.Parameters.Add(strXmlParam);
            cmd.ExecuteNonQuery();
        }

        private IEnumerable<string> SplitByLength(string str, int maxLength)
        {
            int index = 0;
            while(index + maxLength <= str.Length)
            {
                yield return str.Substring(index, maxLength);
                index += maxLength;
            }

            yield return str.Substring(index);
        }

        #region Implementation of IGlobalDb

        /// <summary>
        /// Opening Connection to datatbase
        /// </summary>
        /// <returns>if connected success return true</returns>
        public bool OpenConnection()
        {
            if(_dbConnection == null)
            {
                DbConnection dbConnection = DbProviderFactory.CreateConnection();
                if(dbConnection == null)
                    return false;
                dbConnection.ConnectionString = GetConnString();
                dbConnection.Open();
                _dbConnection = dbConnection;
            }
            return true;
        }

        /// <summary>
        /// Closes conection of DB
        /// </summary>
        /// <returns>true - OK, false - Error</returns>
        public bool CloseConnection()
        {
            if(_dbConnection != null)
            {
                _dbConnection.Close();
                _dbConnection.Dispose();
                _dbConnection = null;
            }
            return true;
        }

        /// <summary>
        /// Возвращает количество записей, которые необходимо обработать
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>Кол-во не обработаных записей или -1 если ошибка</returns>
        public int GetCountRecords(string service)
        {
            int retVal = -1;
            if(_dbConnection != null)
            {
                string sqlQuery = string.Format("select UTDB.CountUnreadXML('{0}') as COUNT from dual", service);
                using(DbDataReader dr = ExecuteQuery(sqlQuery))
                {
                    if(dr.Read())
                    {
                        object str = dr.GetValue(dr.GetOrdinal("COUNT"));
                        retVal = str.ToString().ToInt32(-1);
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        /// Передаем XML в промежуточную БД (создаем все необходимые ссылки)
        /// </summary>
        /// <param name="clobString">XML строка</param>
        /// <param name="essence">имя сущности</param>
        /// <param name="service">имя сервиса</param>
        /// <returns>TRUE - файл сохранен, иначе FALSE</returns>
        public bool WriteXml(string clobString, string essence, string service)
        {
            int recId = GetNextExchangeId();
            if(recId == -1)
                return false;
            if(_dbConnection != null)
            {
                DbCommand cmnd = _dbConnection.CreateCommand();
                cmnd.Connection = _dbConnection;
                cmnd.CommandText = "UTDB.WriteStringXmlNext";
                cmnd.CommandType = CommandType.StoredProcedure;
                DbParameter recIdParm = cmnd.CreateParameter();
                recIdParm.ParameterName = "iRecId";
                recIdParm.DbType = DbType.Int64;
                recIdParm.Direction = ParameterDirection.Input;
                recIdParm.Value = recId;
                DbParameter strXmlParam = cmnd.CreateParameter();
                strXmlParam.ParameterName = "strXml";
                strXmlParam.DbType = DbType.String;
                strXmlParam.Direction = ParameterDirection.Input;
                cmnd.Parameters.Add(recIdParm);
                cmnd.Parameters.Add(strXmlParam);
                bool isFirst = true;
                foreach(string xmlStr in SplitByLength(clobString, MaxStrigLength))
                {
                    if(isFirst)
                    {
                        WriteStringXmlFirst(recId, xmlStr, service, essence);
                        isFirst = false;
                        continue;
                    }
                    strXmlParam.Value = xmlStr;
                    cmnd.ExecuteNonQuery();
                }
                if(!isFirst)
                    WriteStringXmlLast(recId, string.Empty);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Ситает XML данные
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>XML строка</returns>
        public string ReadXml(string service)
        {
            string xml = "";
            if(_dbConnection != null)
            {
                string sqlQuery = string.Format("select UTDB.ReadXML('{0}') as XMLSTR from dual", service);
                using(DbDataReader dr = ExecuteQuery(sqlQuery))
                {
                    if(dr.Read())
                    {
                        object str = dr.GetValue(dr.GetOrdinal("XMLSTR"));
                        xml = str.ToString();
                    }
                }
            }
            return xml;
        }

        /// <summary>
        /// Изменить статус обработки записи
        /// </summary>
        /// <returns>XML строка</returns>
        public bool ChangeStatus(int state, int errorCode, string service)
        {
            if(_dbConnection != null)
            {
                string sqlQuery = string.Format("begin UTDB.ChangeStateXML('{0}',{1},{2}); end;", service, state, errorCode);
                ExecuteNonQuery(sqlQuery);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Generate new GUID
        /// </summary>
        /// <returns>new GUID</returns>
        public int NewGuid(string tableName)
        {
            int retVal = -1;
            string sqlQuery = "SELECT UTDB.SQV_" + tableName + ".NEXTVAL as ID from dual";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                {
                    object str = dr.GetValue(dr.GetOrdinal("ID"));
                    retVal = str.ToString().ToInt32(-1);
                }
            }
            return retVal;
        }

        /// <summary>
        /// Return all essences
        /// </summary>
        /// <returns>list of all assences</returns>
        public string[] GetEssences()
        {
            var entitiesList = new List<string>();
            const string sqlQuery = "SELECT NAME from UTDB.ESSENCES";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                while(dr.Read())
                {
                    string str = dr.GetValue(dr.GetOrdinal("NAME")).ToString();
                    entitiesList.Add(str);
                }
            }
            return entitiesList.ToArray();
        }

        /// <summary>
        /// Return all essences
        /// </summary>
        /// <returns>list of all assences</returns>
        public string[] GetServices()
        {
            var servicesList = new List<string>();
            const string sqlQuery = "SELECT NAME from UTDB.SERVICES";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                while(dr.Read())
                {
                    string str = dr.GetValue(dr.GetOrdinal("NAME")).ToString();
                    servicesList.Add(str);
                }
            }
            return servicesList.ToArray();
        }

        /// <summary>
        /// Add service
        /// </summary>
        /// <param name="serviceName">Service name</param>
        public void AddService(string serviceName)
        {
            string[] services = GetServices();
            foreach(string service in services)
            {
                if(serviceName == service)
                    throw new GlobalDbException("Service already exist");
            }
            int serviceId = 0;
            string sqlQuery = "SELECT MAX(ID) as MAXVAL from UTDB.SERVICES";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                    serviceId = dr.GetValue(dr.GetOrdinal("MAXVAL")).ToString().ToInt32(1);
            }
            serviceId++;
            sqlQuery = string.Format("insert into UTDB.SERVICES (ID, NAME) values ({0}, '{1}')", serviceId, serviceName);
            ExecuteNonQuery(sqlQuery);
        }

        /// <summary>
        /// Add essence
        /// </summary>
        /// <param name="essenceName">Essence name</param>
        public void AddEssence(string essenceName)
        {
            string[] essences = GetServices();
            foreach(string essence in essences)
            {
                if(essenceName == essence)
                    throw new GlobalDbException("Essence already exist");
            }
            int essenceId = 0;
            string sqlQuery = "SELECT MAX(ID) as MAXVAL from UTDB.ESSENCES";
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                    essenceId = dr.GetValue(dr.GetOrdinal("MAXVAL")).ToString().ToInt32(1);
            }
            essenceId++;
            sqlQuery = string.Format("insert into UTDB.ESSENCES (ID, NAME) values ({0}, '{1}')", essenceId, essenceName);
            ExecuteNonQuery(sqlQuery);
        }

        /// <summary>
        /// Connect service wirh essence
        /// </summary>
        /// <param name="serviceName">Service name</param>
        /// <param name="essenceName">Essence name</param>
        public void BindServiceWithEssence(string serviceName, string essenceName)
        {
            int serviceId = -1;
            int essenceId = -1;
            string sqlQuery = string.Format("SELECT ID from UTDB.SERVICES WHERE NAME = '{0}'", serviceName);
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                    serviceId = dr.GetValue(dr.GetOrdinal("ID")).ToString().ToInt32(-1);
            }
            sqlQuery = string.Format("SELECT ID from UTDB.ESSENCES WHERE NAME = '{0}'", essenceName);
            using(DbDataReader dr = ExecuteQuery(sqlQuery))
            {
                if(dr.Read())
                    essenceId = dr.GetValue(dr.GetOrdinal("ID")).ToString().ToInt32(-1);
            }
            if(serviceId == -1)
                throw new GlobalDbException(string.Format("Service '{0}' not exist", serviceName));
            if(essenceId == -1)
                throw new GlobalDbException(string.Format("Essence '{0}' not exist", essenceName));
            sqlQuery = string.Format("insert into UTDB.RELATION (SERVICE_ID, ESSENCE_ID) values ({0}, {1})", serviceId, essenceId);
            ExecuteNonQuery(sqlQuery);
        }

        #endregion

        //===================================================
        /// <summary>
        /// Возвращает строку подключения
        /// </summary>
        /// <returns>строка подключения</returns>
        private string GetConnString()
        {
            string ipDb = "";
            string port = "";
            string serviceName = "";
            string user = "";
            string password = "";
            {// IP
                var rs = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
                rs.Select("WHAT");
                rs.SetWhere("ITEM", IMRecordset.Operation.Like, "XNFRA_UTDB_IP");
                try
                {
                    rs.Open();
                    if(!rs.IsEOF())
                    {
                        string val = rs.GetS("WHAT");
                        string[] spl = val.Split(';');
                        if(spl.Length > 0)
                            ipDb = spl[0];
                        if(spl.Length > 1)
                            port = spl[1];
                        if(spl.Length > 2)
                            serviceName = spl[2];
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }

            {// User
                var rs = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
                rs.Select("WHAT");
                rs.SetWhere("ITEM", IMRecordset.Operation.Like, "XNFRA_UTDB_USER");
                try
                {
                    rs.Open();
                    if(!rs.IsEOF())
                        user = rs.GetS("WHAT");
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }

            {// Passw
                var rs = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
                rs.Select("WHAT");
                rs.SetWhere("ITEM", IMRecordset.Operation.Like, "XNFRA_UTDB_PASSW");
                try
                {
                    rs.Open();
                    if(!rs.IsEOF())
                        password = rs.GetS("WHAT");
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }


            const string oradb = "Data Source=(DESCRIPTION="
                                 + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))"
                                 + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2})));"
                                 + "User Id={3};Password={4};";
            string retStr = string.Format(oradb, ipDb, port, serviceName, user, password);
            return retStr;
        }
        //===================================================
        /// <summary>
        /// Выполнить запрос без ответа
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns>TRUE - все ОК</returns>
        private void ExecuteNonQuery(string query)
        {
            DbCommand cmd = _dbConnection.CreateCommand();
            cmd.Connection = _dbConnection;
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        //===================================================
        /// <summary>
        /// Выполнить запрос с ответом
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns>OracleDataReader - ответ</returns>
        private DbDataReader ExecuteQuery(string query)
        {
            DbCommand cmd = _dbConnection.CreateCommand();
            cmd.Connection = _dbConnection;
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            return cmd.ExecuteReader();
        }
    }

    [Serializable]
    public class GlobalDbException : Exception
    {

        public GlobalDbException(string message) : base(message)
        {
        }

        public GlobalDbException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
