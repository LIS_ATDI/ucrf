﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.GlobalDB
{
    class TestGlobalDb
    {
        public static void RunTestGlobalDb(IGlobalDb gDb)
        {
            List<string> messList = new List<string>();
            try
            {
                const string strOk = "OK";
                const string strError = "Error";
                using (LisProgressBar pb = new LisProgressBar("Test global DB"))
                {
                    pb.SetProgress(0, 10);
                    string mess = "Connection to Global DB...";
                    pb.SetBig(mess);
                    //Подключение к БД
                    if (gDb.OpenConnection() == false)
                    {
                        mess += strError;
                        messList.Add(mess);
                        return;
                    }
                    mess += strOk;
                    messList.Add(mess);
                    //Список сервисов
                    bool canSendTestMessage = false;
                    {
                        messList.Add("Reading essences...");
                        pb.SetBig("Reading essences...");
                        String[] essences = gDb.GetEssences();
                        foreach (string essence in essences)
                        {
                            messList.Add(string.Format("  - {0}", essence));
                            if (essence == EEssence.TestEssence)
                                canSendTestMessage = true;
                        }
                    }
                    //Генерация нового GUID
                    {
                        messList.Add("Generating new GUIDs...");
                        pb.SetBig("Generating new GUIDs...");
                        for (int i = 0; i < 2; i++)
                            messList.Add(string.Format("  {0} - {1}", i, gDb.NewGuid("GUID")));
                    }
                    if (canSendTestMessage)
                    {
                        //Отправка данных
                        mess = "Writing data...";
                        pb.SetBig(mess);
                        string fileName = "";
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            if (dlg.ShowDialog() == DialogResult.OK)
                                fileName = dlg.FileName;
                        }
                        if ((string.IsNullOrEmpty(fileName) == false) && (System.IO.File.Exists(fileName)))
                        {
                            string data = "";
                            using (
                                System.IO.TextReader sr = new System.IO.StreamReader(fileName,
                                                                                     Encoding.GetEncoding(1251)))
                            {
                                data = sr.ReadToEnd();
                            }
                            if (gDb.WriteXml(data, EEssence.TestEssence, EService.IcsmTest))
                                mess += strOk;
                            else
                                mess += strError;
                            messList.Add(mess);
                            //Кол-во записей
                            mess = "Count records...";
                            int count = gDb.GetCountRecords(EService.IcsmTest);
                            mess += count;
                            messList.Add(mess);
                            pb.SetBig(mess);
                            for (int i = 0; i < count; i++)
                            {
                                string fileRead = fileName + i;
                                mess = string.Format("Reading record...{0} to file {1}...", i, fileRead);
                                pb.SetBig(mess);
                                string readData = gDb.ReadXml(EService.IcsmTest);
                                using (System.IO.TextWriter sr = new System.IO.StreamWriter(fileRead, false))
                                {
                                    sr.Write(readData);
                                }
                                mess += strOk;
                                messList.Add(mess);
                                gDb.ChangeStatus(1, 0, EService.IcsmTest);
                            }
                            messList.Add(string.Format("Count records...{0}", gDb.GetCountRecords(EService.IcsmTest)));
                        }
                        else
                            messList.Add("No data for writing...");
                    }
                    mess = "Closing connection...";
                    pb.SetBig(mess);
                    if (gDb.CloseConnection())
                        mess += strOk;
                    else
                        mess += strError;
                    messList.Add(mess);
                }
            }
            catch (Exception ex)
            {
                messList.Add(ex.Message);
            }
            finally
            {
                if (messList.Count > 0)
                {
                    ICSM.IMLogFile log = new ICSM.IMLogFile();
                    log.Create("TestGlobalDb.log");
                    foreach (string mess in messList)
                        log.Info(mess + "\r\n");
                    log.Display("Test global DB");
                    log.Destroy();
                }
            }
        }
    }
}
