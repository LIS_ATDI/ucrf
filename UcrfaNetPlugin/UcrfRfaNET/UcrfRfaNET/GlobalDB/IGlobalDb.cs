﻿using System;

namespace XICSM.UcrfRfaNET.GlobalDB
{
    internal interface IGlobalDb
    {
        //===================================================
        /// <summary>
        /// Opening Connection to datatbase
        /// </summary>
        /// <returns>if connected success return true</returns>
        bool OpenConnection();

        /// <summary>
        /// Closes conection of DB
        /// </summary>
        /// <returns>true - OK, false - Error</returns>
        bool CloseConnection();

        /// <summary>
        /// Возвращает количество записей, которые необходимо обработать
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>Кол-во не обработаных записей или -1 если ошибка</returns>
        int GetCountRecords(string service);

        //===================================================
        /// <summary>
        /// Передаем XML в промежуточную БД (создаем все необходимые ссылки)
        /// </summary>
        /// <param name="clobString">XML строка</param>
        /// <param name="essence">имя сущности</param>
        /// <param name="service">имя сервиса</param>
        /// <returns>TRUE - файл сохранен, иначе FALSE</returns>
        bool WriteXml(string clobString, string essence, string service);

        /// <summary>
        /// Ситает XML данные
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>XML строка</returns>
        string ReadXml(string service);

        /// <summary>
        /// Изменить статус обработки записи
        /// </summary>
        /// <returns>XML строка</returns>
        bool ChangeStatus(int state, int errorCode, string service);

        /// <summary>
        /// Generate new GUID
        /// </summary>
        /// <returns>new GUID</returns>
        int NewGuid(string tableName);

        /// <summary>
        /// Return all essences
        /// </summary>
        /// <returns>list of all essences</returns>
        String[] GetEssences();

        /// <summary>
        /// Return all services
        /// </summary>
        /// <returns>list of all services</returns>
        string[] GetServices();

        /// <summary>
        /// Add service
        /// </summary>
        /// <param name="serviceName">Service name</param>
        void AddService(string serviceName);

        /// <summary>
        /// Add essence
        /// </summary>
        /// <param name="essenceName">Essence name</param>
        void AddEssence(string essenceName);

        /// <summary>
        /// Connect service wirh essence
        /// </summary>
        /// <param name="serviceName">Service name</param>
        /// <param name="essenceName">Essence name</param>
        void BindServiceWithEssence(string serviceName, string essenceName);
    }
}
