﻿using System;
using System.Collections.Generic;
using System.Linq;
using ICSM;
using System.Xml.Linq;
using System.Xml;

namespace XICSM.UcrfRfaNET.GlobalDB
{
   internal enum EStateXML
   {
      Active = 0,     //Не обработана 
      NotActive = 1,  //Обраюотана
      Error = 2       //Обработана с ошибкой
   }
    /// <summary>
    /// 
    /// </summary>
   internal enum XMLParserClass
   {
       Undefined_XML=0, //undefined xml cause an error 
       Parus_XML_contragents=1,//Parus contragents schema we shal use propriate class to work with it
       Parus_XML_DRVmemo=2,//Parus answer schema for DRV memo 
       Parus_XML_DRVMemoUrcm=3,//Parus answer schema for DRV memo
       Parus_XML_DRVmemoError=4, //Ошибка из ДРВ
       Parus_XML_MonitoringContract = 5, //Договора за мониторинг
   }
   /// <summary>
   /// Класс для взаемодействия с промежуточной БД
   /// </summary>
   internal class CGlobalDB
   {
       /// <summary>
       /// Выясняем каким классом будем обрабатывать заявку
       /// </summary>
       /// <param name="xml">данные заявки</param>
       /// <returns>код завершения</returns>
       public int ExecuteData(string xml, string service)
       {
           int result = 0;
           XDocument xdoc = XDocument.Parse(xml);
           XElement root = xdoc.Root;
           switch (root.FirstAttribute.Value)
           {
               case @"http://www.parus.com.ua/agnlist.xsd":
                   result=Process(xml, XMLParserClass.Parus_XML_contragents, service);
                   break;
               case @"http://www.parus.com.ua/-1.xsd":
                   result = Process(xml, XMLParserClass.Parus_XML_DRVmemoError, service);
                   break;
               case @"http://www.parus.com.ua/-42.xsd":
               case @"http://www.parus.com.ua/-41.xsd":
               case @"http://www.parus.com.ua/-5.xsd":
               case @"http://www.parus.com.ua/5.xsd":
               case @"http://www.parus.com.ua/4.xsd":
               case @"http://www.parus.com.ua/3.xsd":
               case @"http://www.parus.com.ua/2.xsd":
               case @"http://www.parus.com.ua/1.xsd":
                   result = Process(xml, XMLParserClass.Parus_XML_DRVmemo, service);
                   break;
               case @"http://www.parus.com.ua/7.xsd":
               case @"http://www.parus.com.ua/8.xsd":
                   result = Process(xml, XMLParserClass.Parus_XML_DRVMemoUrcm, service);
                   break;
               case @"http://www.parus.com.ua/contracts.xsd":
                   result = Process(xml, XMLParserClass.Parus_XML_MonitoringContract, service);
                   break;
           }
           return result;
       }
       /// <summary>
       /// Вызываем нужный класс обработки заявки
       /// </summary>
       /// <param name="XML">данные заявки</param>
       /// <param name="xml_parser">перечесления имени класса</param>
       /// <returns>код завершения</returns>
       private int Process(string XML, XMLParserClass xml_parser, string service)
       {
           int result = 0;
           switch (xml_parser)
           {
               case XMLParserClass.Undefined_XML:
                   result = -1;//undefined class maybe we should choose a code eror for such thing
                   break;
               case XMLParserClass.Parus_XML_contragents:
                   CParusXMLReader.XMLImport(XML, out result);
                   break;
              case XMLParserClass.Parus_XML_DRVmemo:
              case XMLParserClass.Parus_XML_DRVMemoUrcm:
              case XMLParserClass.Parus_XML_DRVmemoError:
                   ProcessDrvMemo(XML, out result);
                   break;
               case XMLParserClass.Parus_XML_MonitoringContract:
                   Documents.MonitoringContract.XmlMonitoringContractRead(XML, out result, service);
                   break;
               default:
                   result = -1;//undefined class maybe we should choose a code eror for such thing
                   break;
           }
           return result;
       }

       private bool ProcessDrvMemo(string xml, out int err)
       {
           string xmlschemaerror = "";
           if (!CXMLTest.TestXmlToSchema(xml, ref xmlschemaerror))
           {
               err = (int)DRVError.de_20;
               return false;
           }
           string strid = "";
           XmlDocument xmlDoc = new XmlDocument();
           xmlDoc.Load(new System.IO.StringReader(xml));
           XmlElement root = xmlDoc.DocumentElement;
           XmlNodeList idList = xmlDoc.GetElementsByTagName("IDENT_ICSM");
           if (idList.Count > 0)
           {
               XmlNode el = idList[0];
               strid = el.InnerText;
           }
           if (strid == "")
           {
               err = (int)DRVError.de_1;
               return false;
           }

           int appId = ConvertType.ToInt32(strid, IM.NullI);
           if (appId == IM.NullI)
           {
               err = (int)DRVError.de_3;
               return false;
           }
           //----
           bool isUrcm = false;
           IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly);
           rs.Select("ID");
           rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
           try
           {
               rs.Open();
               if (!rs.IsEOF())
                   isUrcm = true;
           }
           finally
           {
               if (rs.IsOpen())
                   rs.Close();
               rs.Destroy();
           }

           if(isUrcm)
               return HelpClasses.CDRVMemoURCM.XmlAnswerRead(xml, out err);
           return CGlobalXML.XMLAnswerRead(xml, out err);
       }
       /// <summary>
       /// Умная статическая функция цикл считывания всех заявок из базы обмена она сама открывает базу и сама все пишет и считывает после заершения база закрывается 
       /// </summary>
       /// <param name="silent">ведем себя тихо или показываем сообщения про ошибки пользователю</param>
       /// <returns>Кол-во обработанных пакетов</returns>
       public static int ReadUTDB(bool silent, string service)
       {
           int count = 0;
           int retVal = 0;
           bool open=false;
           CGlobalDB gdb=null;
           using (LisProgressBar pb = new LisProgressBar("UTDB"))
           {
              try
              {
                 pb.SetBig("Connecting to DB");
                 gdb = new CGlobalDB();
                 open = gdb.OpenConnection();
                 if (open)
                 {

                    count = gdb.GetCountRecords(service);
                    
                    if (count > 0)
                    {
                       for (int i = 0; i < count; i++)
                       {
                          pb.SetBig("Data parsing");
                          pb.SetProgress(i, count);
                          pb.UserCanceled();
                          string xml = gdb.ReadXmlFromDb(service);
                          string xmlschemaerror = "";
                          if (!CXMLTest.TestXmlToSchema(xml, ref xmlschemaerror))
                          {
                             const string msg = "ХМL файл не пройшов валідацію, не відповідає ХSD схемі";
                             gdb.ChangeStatus(EStateXML.Error, (int)DRVError.de_20, service);
                             pb.SetBig(msg);
                             CLogs.WriteLogTxt(msg + " " + xmlschemaerror);
                             CLogs.WriteError(ELogsWhat.Utdb, msg + " " + xmlschemaerror, silent);
                             System.Threading.Thread.Sleep(2000);
                          }
                          else//all is fine
                          {
                              
                              int resultImport = gdb.ExecuteData(xml, service);
                             if (resultImport != 0)//we have some errors during import
                             {
                                const string msg = "Помилка під час імпорту XML даних";
                                gdb.ChangeStatus(EStateXML.Error, resultImport, service);
                                pb.SetBig(msg);
                                CLogs.WriteLogTxt(msg);
                                CLogs.WriteError(ELogsWhat.Utdb, msg, silent);
                                System.Threading.Thread.Sleep(2000);
                             }
                             else
                             {
                                 gdb.ChangeStatus(EStateXML.NotActive, resultImport, service);
                                retVal++;
                             }
                          }
                       }
                    }
                    else
                    {
                       const string msg = "У проміжній базі немає записів для імпорту";
                       pb.SetBig(msg);
                       CLogs.WriteInfo(ELogsWhat.Utdb, msg, silent);
                       CLogs.WriteLogTxt(msg);
                       System.Threading.Thread.Sleep(2000);
                    }
                 }
                 else
                 {
                    const string msg = "Неможливо відкрити проміжну базу даних";
                    pb.SetBig(msg);
                    CLogs.WriteError(ELogsWhat.Utdb, msg, silent);
                    System.Threading.Thread.Sleep(2000);
                 }
              }
              catch (Exception e)
              {
                 CLogs.WriteError(ELogsWhat.Utdb, e.Message, silent);
              }
              finally
              {
                 if (open)
                 {
                    gdb.CloseConnection();
                 }
              }
              System.Threading.Thread.Sleep(1000);
           }
           return retVal;
       }

      //===================================================
      // Properties
      //
      //private OracleConnection _localConn = null;
      //private List<object> _entitiesList = new List<object>();
       private IGlobalDb _gDb = null;
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      public CGlobalDB()
      {
          if(ApplSetting.UseGlobalDbService == false)
          {
              _gDb = new GlobalDbLocal();
          }
          else
          {
              throw new NotImplementedException();
          }
      }
      //===================================================
      /// <summary>
      /// Opening Connection to datatbase
      /// </summary>
      /// <returns>if connected success return true</returns>
      public bool OpenConnection()
      {
         return _gDb.OpenConnection();
      }
      //===================================================
      /// <summary>
      /// Closes conection of DB
      /// </summary>
      /// <returns>true - OK, false - Error</returns>
      public bool CloseConnection()
      {
          return _gDb.CloseConnection();
      }
      //===================================================
      /// <summary>
      /// Возвращает количество записей, которые необходимо обработать
      /// </summary>
      /// <returns>Кол-во не обработаных записей или -1 если ошибка</returns>
      public Int32 GetCountRecords()
      {
          return _gDb.GetCountRecords(EService.ICSM);
      }
      /// <summary>
      /// Возвращает количество записей, которые необходимо обработать для указанного сервиса
      /// <param name="clobString">XML строка</param>
      /// </summary>
      /// <returns>Кол-во не обработаных записей или -1 если ошибка</returns>
      public Int32 GetCountRecords(string services)
      {
          return _gDb.GetCountRecords(services);
      }

      //===================================================
      /// <summary>
      /// Передаем XML в промежуточную БД (создаем все необходимые ссылки)
      /// </summary>
      /// <param name="clobString">XML строка</param>
      /// <param name="essence">имя сущности</param>
      /// <returns>TRUE - файл сохранен, иначе FALSE</returns>
      public bool WriteXMLToDB(string clobString, string essence)
      {
         return WriteXMLToDB(clobString, essence, EService.ICSM);
      }
      //===================================================
      /// <summary>
      /// Передаем XML в промежуточную БД (создаем все необходимые ссылки)
      /// </summary>
      /// <param name="clobString">XML строка</param>
      /// <param name="essence">имя сущности</param>
      /// <returns>TRUE - файл сохранен, иначе FALSE</returns>
      public bool WriteXMLToDB(string clobString, string essence, string service)
      {
          return _gDb.WriteXml(clobString, essence, service);
      }
      //===================================================
      /// <summary>
      /// Ситает XML данные
      /// </summary>
      /// <returns>XML строка</returns>
      public string ReadXmlFromDb()
      {
          return _gDb.ReadXml(EService.ICSM);
      }
      /// <summary>
      /// Ситает XML данные по конкретному сервису
      /// <param name="service">имя сервиса</param>
      /// </summary>
      /// <returns>XML строка</returns>
      public string ReadXmlFromDb(string service)
      {
          return _gDb.ReadXml(service);
      }
      //===================================================
      /// <summary>
      /// Изменить статус обработки записи
      /// </summary>
      /// <returns>XML строка</returns>
      public bool ChangeStatus(EStateXML state, int errorCode)
      {
          return _gDb.ChangeStatus((int)state, errorCode, EService.ICSM);
      }
      /// <summary>
      /// Изменить статус обработки записи для конкретного сервиса
      /// </summary>
      /// <returns>XML строка</returns>
      public bool ChangeStatus(EStateXML state, int errorCode, string service)
      {
          return _gDb.ChangeStatus((int)state, errorCode, service);
      }
      //===================================================
      /// <summary>
      /// Генерирует новый ID
      /// </summary>
      /// <param name="conn">Обьект подключения</param>
      /// <param name="tableName">Имя таблицы, для которо</param>
      /// <returns>Новый ID или -1, если ошибка</returns>
      private Int32 NewID(string tableName)
      {
          return _gDb.NewGuid(tableName);
      }
      /// <summary>
      /// Считывает и возвращает список сущностей 
      /// </summary>
      /// <returns>список сущностей</returns>
      public List<string> ReadEssences()
      {
          return _gDb.GetEssences().ToList();
      }
      //===================================================
      /// <summary>
      /// Генерирует новий GUID
      /// </summary>
      /// <returns>новый GUID</returns>
      public static Int32 newGUID()
      {
          Int32 retVal = -1;
          CGlobalDB gdb = new CGlobalDB();
          if (gdb.OpenConnection())
          {
              retVal = gdb.NewID("GUID");
              gdb.CloseConnection();
          }
          return retVal;
      }
   }
}
