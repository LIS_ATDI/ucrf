﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.GlobalDB
{
   /// <summary>
   /// Экспортирует станцию в XML формата промежуточной БД
   /// </summary>
   class ExportToXmlUtdb
   {
       public enum ElemType
       {
           Number,
           Real,
           String,
           Date
       }
      /// <summary>
      /// Конструктор
      /// </summary>
      public ExportToXmlUtdb()
      {
         
      }

      /// <summary>
      /// Экспортирует таблицу в пром. БД только с указанными полями
      /// </summary>
      /// <param name="tableName">имя таблицы</param>
      /// <param name="id">Id</param>      
      /// <param name="select">список полей, которые необходимо записать в XML</param>
      /// <returns>XML</returns>
      public XDocument ExportTableSomeFields(string tableName, int id, string select)
      {
          XDocument xdoc = new XDocument(new XDeclaration("1.0", "UTF-8", null));
          XElement root = new XElement("BODY");
          xdoc.Add(root);
          ExportTableRecursion(root, tableName, id, 0, 1, "", select);

          return xdoc;
      }

      /// <summary>
      /// Экспортирует таблицу в пром. БД
      /// </summary>
      /// <param name="tableName">Название таблицы</param>
      /// <param name="id">Id записи</param>
      /// <param name="depth">Максимальная глубина ссылочных таблиц, которые необходимо поместить в XML. если = 0 то лимит устанавливается в 100</param>
      /// <returns>XDocument с созданным XML</returns>
      public XDocument ExportTable(string tableName, int id, int depth)
      {
         if(depth == 0)
            depth = 100;
         //-------
         XDocument xdoc = new XDocument(new XDeclaration("1.0", "UTF-8", null));
         XElement root = new XElement("BODY");
         xdoc.Add(root);
         ExportTableRecursion(root, tableName, id, 0, depth, "","");
         return xdoc;
      }
      /// <summary>
      /// Функция рекурсивно заполняет XML данними из таблиц
      /// </summary>
      /// <param name="root">Отновное тело элемента XML</param>
      /// <param name="tableName">Название таблицы из которой экспортируем данные</param>
      /// <param name="id">Id записи</param>
      /// <param name="curDepth">Текущая глубина таблиц</param>
      /// <param name="maxDepth">Мак. допустимая глубина</param>
      /// <param name="refName">Кто ссылаеться на данную запись</param>
      /// <param name="selectFields">список полей для SELECT</param>
      private void ExportTableRecursion(XElement root, string tableName, int id, int curDepth, int maxDepth, string refName, string selectFields)
      {        
         if (curDepth >= maxDepth)
            return;  //Дальше нельзя углубляться

         List<string> listSelect = new List<string>();
         OrmTable ormTab = OrmSchema.Table(tableName);
         OrmField[] fldMas = ormTab.Fields;

         if((string.IsNullOrEmpty(selectFields) == false) && (selectFields.Split(',').Length > 0))
         {
             string[] fields = selectFields.Split(',');
             foreach (string field in fields)
                 listSelect.Add(field.Trim());
         }
         else
         {
             foreach (OrmField fldCicle in fldMas)
             {
                 if (fldCicle == null)
                     continue;
                 if (fldCicle is OrmFieldF)
                 {
                     OrmFieldF fld = fldCicle as OrmFieldF;
                     if ((fld.DDesc != null) &&
                         (fld.IsCompiledInEdition) &&
                         (fld.Nature == OrmFieldNature.Column) &&
                         ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                          (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                          (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)))
                     {
                         try
                         {
                             listSelect.Add(fld.Name);
                         }
                         catch (Exception ex)
                         {
                             CLogs.WriteError(ELogsWhat.AutoXml, ex);
                         }
                     }
                 }
             }
         }

         //Формирование select(...)
         IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
         foreach (string list in listSelect)
             rs.Select(list);
         rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
            {        
                //Робота з ХМL
                foreach (OrmField fldCicle in fldMas)
               {
                  if (!listSelect.Contains(fldCicle.Name) || fldCicle == null)
                       continue;                  
                  if (fldCicle is OrmFieldF)
                  {
                      string depthNameAll = (!string.IsNullOrEmpty(tableName))
                                              ? string.Format("{0}.{1}", tableName, fldCicle.Name)
                                              : fldCicle.Name;
                     OrmFieldF fld = fldCicle as OrmFieldF;
                     if ((fld.DDesc != null) &&
                         (fld.IsCompiledInEdition) &&
                         (fld.Nature == OrmFieldNature.Column) &&
                         (listSelect.Contains(fld.Name)))
                     {
                        switch (fld.DDesc.Coding)
                        {
                           case OrmDataCoding.tvalDATETIME:
                              {
                                 DateTime dt = rs.GetT(fld.Name);
                                 if (dt != IM.NullT)
                                    AddElement(root, depthNameAll, ElemType.Date, refName, dt);
                              }
                              break;
                           case OrmDataCoding.tvalNUMBER:
                              {
                                 double valDouble = rs.GetD(fld.Name);
                                 if (valDouble != IM.NullD)
                                    AddElement(root, depthNameAll, ElemType.Real, refName, valDouble);
                              }
                              break;
                           case OrmDataCoding.tvalSTRING:
                              {
                                 string valString = rs.GetS(fld.Name);
                                 if (!string.IsNullOrEmpty(valString))
                                    AddElement(root, depthNameAll, ElemType.String, refName, valString);
                              }
                              break;
                        }
                     }
                  }                      
                  else if (fldCicle is OrmFieldJ)
                  {
                     OrmFieldJ fld = fldCicle as OrmFieldJ;
                     if ((fld.Join != null) &&
                         (fld.Join.JoinedTable != null) &&
                         (fld.Join.From[0].IsCompiledInEdition) &&
                         (fld.Join.To[0].IsCompiledInEdition))
                     {
                        string nameFieldFrom = fld.Join.From[0].Name;
                        string nameFieldTo = fld.Join.To[0].Name;
                        string nameTable = fld.Join.JoinedTableName;
                        if (!string.IsNullOrEmpty(nameFieldFrom) &&
                            !string.IsNullOrEmpty(nameFieldTo) &&
                            !string.IsNullOrEmpty(nameTable) &&
                            (nameFieldTo.ToUpper() == "ID"))
                        {
                           string refNameAll = string.Format("{0}.{1}", tableName, nameFieldFrom);

                           int newId = 0;
                           if ((string.IsNullOrEmpty(selectFields) == false) && (selectFields.Split(',').Length > 0))
                               newId = rs.GetI(nameFieldFrom);
                           ExportTableRecursion(root, nameTable, newId, ++curDepth, maxDepth, refNameAll, selectFields);
                        }
                     }
                  }
               }
            }
         }
         catch (Exception ex)
         {
            CLogs.WriteError(ELogsWhat.AutoXml, ex);
            throw;
         }
         finally
         {
            if(rs.IsOpen())
               rs.Close();
            rs.Destroy();
         }
      }
      /// <summary>
      /// Добавляе значение в XML
      /// </summary>
      /// <param name="root">XElement базовый элемент XML</param>
      /// <param name="elemName">Название поля</param>
      /// <param name="elemType">Тип поля</param>
      /// <param name="refName">Кто ссылаеться на данную запись</param>
      /// <param name="val">Значение поля</param>
      private void AddElement(XElement root, string elemName, ElemType elemType, string refName, object val)
      {
         XElement subItem = new XElement("SUBITEM");
         subItem.SetValue(val);
         XElement item = new XElement("ITEM");
         item.SetAttributeValue("name", elemName);
         item.SetAttributeValue("type", elemType.ToString());
         if(!string.IsNullOrEmpty(refName))
            item.SetAttributeValue("reference", refName);
         item.Add(subItem);
         root.Add(item);
      }
   }
}
