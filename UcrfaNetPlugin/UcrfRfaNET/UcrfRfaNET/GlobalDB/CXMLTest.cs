﻿using System;
using System.Xml;
using System.IO;
using System.Xml.Schema;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET
{
   internal static class CXMLTest
   {
      //===================================================
      static string errors = "";
      //===================================================
      /// <summary>
      /// Tests all XML by schema
      /// </summary>
      /// <param name="_xmlData">XML string</param>
      /// <param name="_xsdFileSchema"></param>
      /// <param name="error">Error string</param>
      /// <returns>True - OK, False - Error</returns>
      public static bool TestAllXmlToSchema(string _xmlData, string _fileSchema, string _nameSpace, ref string error)
      {
         bool retVal = true;
         errors = "";
         string namespacemy = _nameSpace;
         string file = _fileSchema;
         string xsdPathName = GetPathToSchemas();

         file = xsdPathName + file;
         if (!System.IO.File.Exists(file))
         {
            error = string.Format("Can't find a schema '{0}'", file);
            return false;
         }
         
         // Create a new validating reader
         XmlValidatingReader reader = new XmlValidatingReader(new XmlTextReader(new StringReader(_xmlData)));
         // Create a schema collection, add the xsd to it
         XmlSchemaCollection schemaCollection = new XmlSchemaCollection();
         schemaCollection.Add(namespacemy, file);
         // Add the schema collection to the XmlValidatingReader
         reader.Schemas.Add(schemaCollection);
         reader.ValidationEventHandler += new ValidationEventHandler(reader_ValidationEventHandler);
         while (reader.Read()) { }
         if (!string.IsNullOrEmpty(errors))
            retVal = false;
         error = errors;
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Tests XML by schema
      /// </summary>
      /// <param name="_xmlData">XML string</param>
      /// <param name="_xsdPathName">Full path to xsd files</param>
      /// <param name="error">Error string</param>
      /// <returns>True - OK, False - Error</returns>
      public static bool TestXmlToSchema(string _xmlData, string _xsdPathName, ref string error)
      {
         bool retVal = true;
         errors = "";
         // Create a new validating reader
         XmlValidatingReader reader = new XmlValidatingReader(new XmlTextReader(new StringReader(_xmlData)));

         string namespacemy = "";
         string file = "";
         while (reader.Read())
         {
            if ((reader.Name == "DECLAR") && (reader.HasAttributes))
            {
               namespacemy = reader.GetAttribute("NAMESPACE");
               file = reader.GetAttribute("SCHEMAFILE");
               break;
            }
         }

         if (string.IsNullOrEmpty(namespacemy) || string.IsNullOrEmpty(file))
         {
            error = "Can't find attributes 'NAMESPACE' or 'SCHEMAFILE'";
            return false;
         }

         file = _xsdPathName + file;
         if (!System.IO.File.Exists(file))
         {
            error = string.Format("Can't find schema '{0}'", file);
            return false;
         }

         reader = new XmlValidatingReader(new XmlTextReader(new StringReader(_xmlData)));
         // Create a schema collection, add the xsd to it
         XmlSchemaCollection schemaCollection = new XmlSchemaCollection();
         schemaCollection.Add(namespacemy, file);
         // Add the schema collection to the XmlValidatingReader
         reader.Schemas.Add(schemaCollection);
         reader.ValidationEventHandler += new ValidationEventHandler(reader_ValidationEventHandler);
         while (reader.Read()) { }
         if (!string.IsNullOrEmpty(errors))
            retVal = false;
         error = errors;
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Tests XML by schema
      /// </summary>
      /// <param name="_xmlData">XML string</param>
      /// <param name="_xsdPathName">Full path to xsd files</param>
      /// <param name="error">Error string</param>
      /// <returns>True - OK, False - Error</returns>
      public static bool TestXmlToSchema(string _xmlData, ref string error)
      {
         string fullPath = GetPathToSchemas();
         return TestXmlToSchema(_xmlData, fullPath, ref error);
      }
      //===================================================
      /// <summary>
      /// Handler of errors
      /// </summary>
      private static void reader_ValidationEventHandler(object sender, ValidationEventArgs e)
      {
         errors += string.Format("\nLine {0}; {1}", e.Exception.LineNumber, e.Exception.Message);
      }
      //===================================================
      /// <summary>
      /// Возвращет путь к файлам с схемами
      /// </summary>
      /// <returns>Возвращет путь к файлам с схемами</returns>
      private static string GetPathToSchemas()
      {
         string fullPath = "";
         IMRecordset rs = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
         try
         {
            rs.Select("WHAT");
            rs.SetWhere("ITEM", IMRecordset.Operation.Like, "XNRFA_SCHEMA_PATH");
            rs.Open();
            if (!rs.IsEOF())
               fullPath = rs.GetS("WHAT") + "\\";
         }
         finally
         {
            rs.Destroy();
         }
         return fullPath;
      }
   }
}
