﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.Icsm;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal class SpecialReportProducer
    {
        internal class DataRow
        {
            public int PTK_Count{ get; set;}
            public int TestLineup_TV_Count { get; set; }
            public int TestLineup_NV_Count { get; set; }
            public int PIO_Count { get; set; }
            public int PVP_Count { get; set; }

            public override string  ToString()
            {
                return PTK_Count.ToString()+"/"+
                    TestLineup_TV_Count.ToString()+"/"+
                    TestLineup_NV_Count.ToString()+"/"+
                    PIO_Count.ToString()+"/"+
                    PVP_Count.ToString();
            }
        }
        
        private int _id = IM.NullI;
        private DateTime _startDate = DateTime.Now;
        private DateTime _dueDate  = DateTime.Now;
        private Dictionary<DateTime, DataRow> _result = new Dictionary<DateTime, DataRow>();

        public const string FileExtension = ".rtf";
        public const string FilePrefix = "Звіт_";

        public DateTime StartDate
        {
            get
            {
                return _startDate;                
            }            
        }
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }            
        }

        public string FileName
        {
            get
            {
                return FilePrefix + _startDate.ToFilePartString() + "-" + _dueDate.ToFilePartString()+FileExtension;
            }            
        }

        public void Load(int id)
        {
            //DateTime startDate, DateTime dueDate            
            _id = id;
            
            using (LisRecordSet rs = new LisRecordSet(PlugTbl.itblXnrfaReport, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID,START_DATE,DUE_DATE");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                rs.Open();
                _startDate = rs.GetT("START_DATE");
                _dueDate = rs.GetT("DUE_DATE");  
            }
        }

        public void CreateReport(DateTime startDate, DateTime dueDate)
        {
            _result.Clear();
            _startDate = startDate.TruncateByDay();
            _dueDate = dueDate.TruncateByDay();
            
            TimeSpan timeInterval = _dueDate - _startDate;

            DateTime currentDate = _startDate;
            
            do
            {
                DataRow dataRow = new DataRow();
                _result.Add(currentDate, dataRow);
                currentDate = currentDate.AddDays(1);                
            } while (currentDate <= _dueDate);
            
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування звіту")))
            {
                try
                {
                    DateTime _EndDate1 = _dueDate.AddDays(1);

                    pb.SetBig("Підрахунок погоджень для натурних та тестових випробувань");
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.PacketToAppl, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("DOC_DATE,IS_NV,IS_TV");
                        rs.SetWhere("DOC_DATE", IMRecordset.Operation.Ge, _startDate);
                        rs.SetWhere("DOC_DATE", IMRecordset.Operation.Lt, _EndDate1);

                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            DateTime dt = rs.GetT("DOC_DATE").TruncateByDay();
                            int nv = rs.GetI("IS_NV");
                            int tv = rs.GetI("IS_TV");

                            if (_result.ContainsKey(dt))
                            {
                                if (nv != 0)
                                    _result[dt].TestLineup_NV_Count++;

                                if (tv != 0)
                                    _result[dt].TestLineup_TV_Count++;
                            }
                        }
                    }

                    pb.SetBig("Підрахунок проведених актів ПТК");
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.PacketToAppl, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("PTK_DATE_FROM");
                        rs.SetWhere("PTK_DATE_FROM", IMRecordset.Operation.Ge, _startDate);
                        rs.SetWhere("PTK_DATE_FROM", IMRecordset.Operation.Lt, _EndDate1);

                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            DateTime dt = rs.GetT("PTK_DATE_FROM").TruncateByDay();
                            if (_result.ContainsKey(dt))
                                _result[dt].PTK_Count++;
                        }
                    }

                    pb.SetBig("Підрахунок проведених ПІО/ПВП");

                    CountWithDiffTable(PlugTbl.itblXnrfaDiffMobSta, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDiffMobSta2, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDeiffEtSta, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDeiffMwSta, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDiffFmDigital, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDeiffDvbSta, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDeiffFmSta, _startDate, _EndDate1);
                    CountWithDiffTable(PlugTbl.itblXnrfaDeiffTvSta, _startDate, _EndDate1);

                    Save();

                    pb.SetBig("Формування звіту");
                    string path = PrintDocs.getPath(DocType.REPORT);
                    string fullPath = Path.Combine(path, FileName);
                    PrintDocs printDocs = new PrintDocs();
                    printDocs.CreateOneReport(DocType.REPORT, AppType.AppUnknown, _startDate, _dueDate, PlugTbl.itblXnrfaReport,
                                              _id, "", fullPath, false);

                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.itblXnrfaReport, IMRecordset.Mode.ReadWrite))
                    {
                        rs.Select("ID,PATH");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, _id);
                        rs.Open();

                        if (rs.IsOpen())
                        {
                            rs.Edit();
                            rs.Put("PATH", fullPath);
                            rs.Update();
                        }
                    }
                }
                catch(Exception ex)
                {
                    CLogs.WriteError(ELogsWhat.Critical, ex);
                    throw;
                }                
            }        
        }

        private void CountWithDiffTable(string table, DateTime startDate, DateTime dueDate)
        {            
            using (LisRecordSet rs = new LisRecordSet(table, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("MEASURE_DATE1");
                rs.SetWhere("MEASURE_DATE1", IMRecordset.Operation.Ge, startDate);
                rs.SetWhere("MEASURE_DATE1", IMRecordset.Operation.Lt, dueDate);

                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    DateTime dt = rs.GetT("MEASURE_DATE1").TruncateByDay();
                    if (_result.ContainsKey(dt))
                        _result[dt].PIO_Count++;
                }
            }

            using (LisRecordSet rs = new LisRecordSet(table, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("MEASURE_DATE2");
                rs.SetWhere("MEASURE_DATE2", IMRecordset.Operation.Ge, startDate);
                rs.SetWhere("MEASURE_DATE2", IMRecordset.Operation.Lt, dueDate);

                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    DateTime dt = rs.GetT("MEASURE_DATE2").TruncateByDay();
                    if (_result.ContainsKey(dt))
                        _result[dt].PIO_Count++;
                }
            }

            using (LisRecordSet rs = new LisRecordSet(table, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("MEASURE_TIME_TO3");
                rs.SetWhere("MEASURE_TIME_TO3", IMRecordset.Operation.Ge, startDate);
                rs.SetWhere("MEASURE_TIME_TO3", IMRecordset.Operation.Lt, dueDate);

                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    DateTime dt = rs.GetT("MEASURE_TIME_TO3").TruncateByDay();
                    if (_result.ContainsKey(dt))
                        _result[dt].PVP_Count++;
                }
            }
        }    

        private void Save()
        {
            TimeSpan idSpan = DateTime.Now - new DateTime(1970, 1, 1, 1, 1, 1);
            _id = IM.NullI;
            using (LisRecordSet rs = new LisRecordSet(PlugTbl.itblXnrfaReport, IMRecordset.Mode.ReadWrite))
            {
                rs.Select("ID,START_DATE,DUE_DATE,DATE_CREATED,CREATED_BY");
                rs.Open();
                rs.AddNew();
                _id = IM.AllocID(PlugTbl.itblXnrfaReport, 1, -1);
                rs.Put("ID", _id);
                rs.Put("START_DATE", _startDate);
                rs.Put("DUE_DATE", _dueDate);
                rs.Put("DATE_CREATED", DateTime.Now);
                rs.Put("CREATED_BY", IM.ConnectedUser());                
                rs.Update();
            }

            using (LisRecordSet rs = new LisRecordSet(PlugTbl.itblXnrfaSpecReport, IMRecordset.Mode.ReadWrite))
            {                                
                rs.Select("ID,DATE_IN,REPORT_ID,PTK,TV,NV,PIO,PVP");
                rs.Open();

                foreach (KeyValuePair<DateTime, DataRow> pair in _result)
                {
                    rs.AddNew();
                    rs.Put("ID", IM.AllocID(PlugTbl.itblXnrfaSpecReport, 1, -1));
                    rs.Put("DATE_IN", pair.Key);
                    rs.Put("REPORT_ID", _id);
                    rs.Put("PTK", pair.Value.PTK_Count);
                    rs.Put("TV", pair.Value.TestLineup_TV_Count);
                    rs.Put("NV", pair.Value.TestLineup_NV_Count);
                    rs.Put("PIO", pair.Value.PIO_Count);
                    rs.Put("PVP", pair.Value.PVP_Count);
                    rs.Update();
                }
            }
        }

        public void Show()
        {
            string path = "";
            using (LisRecordSet rs = new LisRecordSet(PlugTbl.itblXnrfaReport, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID,PATH");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, _id);
                rs.Open();
                if (!rs.IsEOF())
                {
                    path = rs.GetS("PATH");
                }
            }

            if (!string.IsNullOrEmpty(path))
                System.Diagnostics.Process.Start(path);
        }
    }
}
