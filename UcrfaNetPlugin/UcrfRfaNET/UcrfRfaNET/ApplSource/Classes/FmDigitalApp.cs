﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRS;
using XICSM.UcrfRfaNET.StationsStatus;

namespace XICSM.UcrfRfaNET.ApplSource
{
    public class FmDigitalChannel
    {
        public double Freq;
        public string Channel;
        public double Bandwidth;

        public FmDigitalChannel()
        {
        }

        public override string ToString()
        {
            return Channel;
        }
    }
    class FmDigitalApp : FMTVBase
    {
        public static readonly string TableName = ICSMTbl.itblTDAB;

        protected StationFM station = new StationFM();
        protected double Longitude = 0; //Долгота
        protected double Latitude = 0;  //Широта           
        protected IMObject objAssign = null;
        protected string RxMode = "";

        protected List<string> SystemModeList = null;
        protected List<string> StandardModeList = null;
        protected List<FmDigitalChannel> BlockList = null;
        //List<string> ContourList = null;

        bool NeedSavePosition = false;



        public FmDigitalApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, FmDigitalApp.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppR2d;

            BlockList = GetBlockList();
            SystemModeList = new List<string>();
            SystemModeList.Add("MODE1");
            SystemModeList.Add("MODE2");
            SystemModeList.Add("MODE3");
            SystemModeList.Add("MODE4");

            StandardModeList = new List<string>();
            StandardModeList.Add("MPEG2");
            StandardModeList.Add("MPEG4");

            appType = AppType.AppR2d;
            department = DepartmentType.VRS;
            departmentSector = DepartmentSectorType.VRS_SR;

            int assignId = objStation.GetI("ASSGN_ID");

            if (assignId != IM.NullI)
                objAssign = IMObject.LoadFromDB(ICSMTbl.itblFmtvAssign, assignId);

            List<string> ContourList = new List<string>();

            //TODO дописать поля для сохранения дозвола и висновка
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            if (objAssign != null)
            {
                objAssign.Dispose();
                objAssign = null;
            }
            station.Dispose();
            base.DisposeExec();
        }
        //===================================================
        /// <summary>
        /// Создание XML строки для построения грида
        /// </summary>
        /// <returns>XML строка</returns>
        protected override string GetXMLParamGrid()
        {
            string XmlString =
              "<items>"
            + "<item key='header1'    type='lat'       flags=''      tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
            + "<item key='header2'    type='lat'       flags='s'     tab=''   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LATITUDE
            + "<item key='mKLat'       type='lat'       flags=''     tab=''  display='Широта (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLat'       type='lat'       flags='s'     tab='1'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LONGITUDE
            + "<item key='mKLon'       type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLon'       type='lat'       flags='s'     tab='2'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.ASL 
            + "<item key='mkAsl'      type=''       flags=''      tab=''  display='Висота поверхні Землі, м' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='kAsl'       type='double'       flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // objPosition.GetFullAddress
            + "<item key='mkAddr'     type=''      flags=''      tab=''  display='Адреса встановлення РЕЗ' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kAddr'      type='string'      flags='s'     tab='3'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

            //objEquip.NAME
            + "<item key='mkName'     type=''      flags=''      tab=''  display='Номер РЕЗ/назва опори' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='KName'      type=''      flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='KPillarName'      type=''      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='mkCntrName'  type=''      flags=''      tab=''  display='Номер контуру; номер РЕЗ в синхронній мережі' ReadOnly='true' background='' bold=''/>"
            + "<item key='kCntrName'   type='lat'       flags='s'      tab='4'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"
            + "<item key='NetName'        type='string'      flags='s'     tab='5'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='mkSystem'     type=''      flags=''      tab=''  display='Варіант системи/кількість несівних/захисний інтервал' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kSystem'      type='lat'   flags='s'     tab='6'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
            + "<item key='kCount'       type='lat'   flags='s'     tab='7'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"
            + "<item key='kInterval'    type='lat'   flags='s'     tab='8'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

            + "<item key='mkReceive'      type=''      flags=''      tab=''  display='Втрати у фільтрі/Тип спектр. маски/Тип прийому' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kFilterLosses'  type='lat'   flags='s'     tab='9'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
            + "<item key='kSpectralMask'  type='lat'   flags='s'     tab='10'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
            + "<item key='kRecvType'      type='lat'   flags='s'     tab='11'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            + "<item key='mkStandard'  type=''      flags=''      tab=''  display='Стандарт компресії/Зміщення несівної частоти, Гц' ReadOnly='true' background='' bold=''/>"
            + "<item key='kStandard'   type='string'       flags='s'      tab='12'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"
            + "<item key='kOffset'     type='double'      flags='s'     tab='13'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kEquip'     type='lat'      flags=''      tab=''  display='Назва (тип РЕЗ)' ReadOnly='true' background='' bold='y'/>"
            + "<item key='equip'      type='lat'      flags='s'     tab='14'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            //PWR_ANT
            + "<item key='kPowerA'    type='lat'      flags=''      tab=''  display='Потужність передавача, Вт' ReadOnly='true' background='' bold=''/>"
            + "<item key='powera'     type='lat'      flags='s'     tab='15'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kRxBand'    type='lat'      flags=''      tab=''  display='Ширина смуги, МГц/Клас випромінювання' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='rxband'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='kEMI'       type='string'    flags='s'    tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kCert'      type='lat'      flags=''      tab=''  display='Сертифікат відповідності (номер / дата)' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='certno'     type='string'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='certda'     type='datetime'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kFeeder'      type='lat'      flags=''   tab=''   display='Довжина фідера, м / Втрати у фідері, дБ/м' ReadOnly='true' background='' bold=''/>"
            + "<item key='feederlen'    type='double'   flags='s'  tab='16'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='feederloss'   type='double'  flags='s'  tab='17'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='kAntennaDir'       type='lat'      flags=''      tab=''  display='Спрямованість антени/поляризація' ReadOnly='true' background='' bold='y' />"
            + "<item key='dir'        type='lat'      flags='s'     tab='18'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
            + "<item key='polar'        type='lat'      flags='s'     tab='19'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"

            + "<item key='kAntennaHeight' type='lat'      flags=''      tab=''  display='Висота антени, м / макс. ефект. висота' ReadOnly='true' background='' bold=''/>"
            + "<item key='height'         type='lat'      flags='s'     tab='20'  display='' background='' bold='' HorizontalAlignment='center'/>"
            + "<item key='maxHeight'      type='lat'      flags='s'     tab=''  display='' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kAntennaGain' type='lat'      flags=''      tab=''  display='Макс. коеф. підсилення антени: Г, дБ / В, дБ' background='' bold=''/>"
            + "<item key='horzGain'     type='lat'      flags='s'     tab='21'  display='' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='vertGain'     type='lat'      flags='s'     tab='22'  display='' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='kEVP' type='lat'      flags=''      tab=''  display='ЕВП: Г, дБВт / В, дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='HorzEVP'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='VertEVP'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kEVPmax' type='lat'      flags=''      tab=''  display='ЕВП макс., дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='EVPmax'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kState'   type='lat'      flags=''      tab=''    display='Внутрішній стан / Міжнародний стан' background='' bold='' />"
            + "<item key='homeState' type='lat'      flags='s'     tab='23'  display='' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='internationalState' type='lat'      flags='s'     tab='24'  display='' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='mkFreq'      type=''       flags=''      tab=''  display='Блок/центральна частота, МГц' ReadOnly='true' background='' bold='y' fontColor=''/>"
            + "<item key='kBlocFreq'   type='double'       flags='s'     tab='25'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center' fontColor=''/>"
            + "<item key='kCenterFreq' type='double'       flags='s'     tab=''  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' fontColor=''/>"


            + "<item key='mKObj'       type=''       flags=''     tab=''  display='Об\"єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='KObj'      type=''     flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "</items>";
            return XmlString;
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            if (newStation)
            {
                objStation.Put("DIAGA", "HH");
                objStation.Put("STANDARD", CRadioTech.TDAB);
                objStation.Put("ADM", "UKR");

                objStation.Put("CUST_TXT1", NumberIn);
                objStation.Put("CUST_DAT1", DateIn);

                objAssign = IMObject.New(ICSMTbl.itblFmtvAssign);
                objStation.Put("ASSGN_ID", objAssign.GetI("ID"));

                objAssign.Put("STN_ID", objStation.GetI("ID").ToString());
                objAssign.Put("CLASS", "BC");
                objAssign.Put("DIGITAL", 1);
                objAssign.Put("ADM", "UKR");
                objAssign.Put("COUNTRY", "UKR");
                objAssign.Put("IS_ALLOTM", "N");
                objAssign.Put("RX_MODE", RxMode);
                objAssign.SaveToDB();
            }

            int EquipID = objStation.GetI("EQUIP_ID");

            //if (NeedSavePosition)
            //    station.objPosition.Save();

            objStation.Put("SITE_ID", station.objPosition.Id);
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус
            objStation.Put("CUST_CHB1", IsTechnicalUser);
            CJournal.CheckTable(TableName, objStation.GetI("ID"), objStation);
            objStation.SaveToDB();
            // Сохраняем лицензии
            SaveLicence();
            return true;
        }

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (station.objPosition != null) ? station.objPosition.Province : "";
            string city = (station.objPosition != null) ? station.objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if ((docType == DocType.VISN) || (docType == DocType.VISN_NR))
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.DOZV_OPER))
                fullPath += "ЦЗМ-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }

        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "KLat")
            {
                int siteId = objStation.GetI("SITE_ID");

                if (siteId != IM.NullI)
                {
                    station.objPosition = new PositionState2();
                    station.objPosition.LoadStatePosition(siteId, ICSMTbl.itblPositionBro);
                    station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                }

                AutoFill(cell, grid);
            }
            else if (cell.Key == "equip")
            {
                int equipId = objStation.GetI("EQUIP_ID");

                if (equipId != IM.NullI)
                    station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipBro, equipId);

                AutoFill(cell, grid);
            }
            else
                if (cell.Key == "powera")
                {
                    double PwrAnt = objStation.GetD("PWR_ANT");
                    cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
                }
                else if (cell.Key == "KName")
                {
                    cell.Value = objStation.GetS("CUST_TXT7");
                }
                else if (cell.Key == "kStandard")
                {
                    cell.Value = objStation.GetS("REMARK");
                }
                else if (cell.Key == "kCntrName")
                {
                    string aName = objStation.GetS("A_NAME");
                    string Adm = objStation.GetS("ADM");
                    string ContourName = "";

                    if (!string.IsNullOrEmpty(aName))
                        ContourName = aName;
                    //ContourName = aName.Replace(Adm, "");  TODO Продивитись чому неправильно

                    cell.Value = ContourName;
                }
                else if (cell.Key == "kOffset")
                {
                    double FrequencyOffset = objStation.GetD("OFFSET");
                    cell.Value = (FrequencyOffset * 1000.0).ToString("F0");
                }
                else if (cell.Key == "kRecvType")
                {
                    if (objAssign != null)
                    {
                        RxMode = objAssign.GetS("RX_MODE");
                        SelectByShort(cell, RxMode);
                    }
                }
                else if (cell.Key == "height")
                {
                    double EffectiveHeight = objStation.GetD("AGL");
                    if (EffectiveHeight != IM.NullD)
                        cell.Value = EffectiveHeight.ToString("F0");
                }
                else if (cell.Key == "maxHeight")
                {
                    double EffectiveHeightMax = objStation.GetD("EFHGT_MAX");
                    if (EffectiveHeightMax != IM.NullD)
                        cell.Value = EffectiveHeightMax.ToString("F0");
                }
                else if (cell.Key == "feederlen")
                {
                    double FeederLen = objStation.GetD("CUST_NBR1");
                    if (FeederLen != IM.NullD)
                        cell.Value = FeederLen.ToString("F0");
                }
                else if (cell.Key == "feederloss")
                {
                    double FeederLoss = objStation.GetD("CUST_NBR2");
                    if (FeederLoss != IM.NullD)
                        cell.Value = FeederLoss.ToString("F3");
                }
                else if (cell.Key == "kFilterLosses")
                {
                    double FilterLoss = objStation.GetD("TX_LOSSES") - objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                    if (FilterLoss != IM.NullD)
                        cell.Value = FilterLoss.ToString("F3");
                }
                else if (cell.Key == "kSystem")
                {
                    int TDabMode = objStation.GetI("TDAB_MODE");
                    if (TDabMode != IM.NullI)
                    {
                        cell.Value = "MODE" + TDabMode.ToString();
                        AutoFill(cell, grid);
                    }
                }
            if (cell.Key == "dir")
            {
                //-- From FMTVBase
                SetDirection();
                SelectByShort(cell, Direction);
                //-- From FMTVBase
            }
            else if (cell.Key == "polar")
            {
                //-- From FMTVBase
                SelectByShort(cell, objStation.GetS("POLARIZATION"));
                AutoFill(cell, grid);
                //-- From FMTVBase
            }
            else if (cell.Key == "horzGain")
            {
                //-- From FMTVBase
                UpdateHorzGain(cell);
                //-- From FMTVBase
            }
            else if (cell.Key == "vertGain")
            {
                //-- From FMTVBase
                UpdateVertGain(cell);
                //-- From FMTVBase
            }
            else if (cell.Key == "kSpectralMask")
            {
                int spectralMask = IM.NullI;//objStation.GetI("HM_CRITICAL");     Не находит поле в БД   29.03.11     
                if (spectralMask != IM.NullI)
                {
                    SelectByShort(cell, spectralMask.ToString());
                }
            }
            else if (cell.Key == "homeState")
            {
                string homeState = objStation.GetS("CUST_TXT4");
                SelectByShort(cell, homeState);
            }
            else if (cell.Key == "internationalState")
            {
                string internationalState = objStation.GetS("CUST_TXT5");
                SelectByShort(cell, internationalState);
            }
            else if (cell.Key == "KObj")
            {
                cell.Value = objStation.GetI("ID").ToString();
            }
            else if (cell.Key == "HorzEVP")
            {
                cell.Value = objStation.GetD("ERP_H").ToString("F3");
            }
            else if (cell.Key == "VertEVP")
            {
                cell.Value = objStation.GetD("ERP_V").ToString("F3");
            }
            else if (cell.Key == "kBlocFreq")
            {
                AutoFill(cell, grid);
            }
        }

        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KLon") ||
                         (cell.Key == "kAddr") ||
                         (cell.Key == "equip"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
            }
            else if (cell.Key == "system")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CFmTransmissionSystem.getTransmissionSystemList();
            }
            else
                if (cell.Key == "powera")
                {
                    double PwrAnt = objStation.GetD("PWR_ANT");
                    cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
                }
                else if (cell.Key == "polar")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Small);
                }
                else if (cell.Key == "dir")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CAntennaDirection.getAntennaDirectionList();
                }
                else if (cell.Key == "homeState")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CFMState.getInternalStateList();
                }
                else if (cell.Key == "internationalState")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CFMState.getForeignStateList();
                }
                else if ((cell.Key == "horzGain") ||
                         (cell.Key == "vertGain"))
                {
                    cell.cellStyle = EditStyle.esEllipsis;
                }
                else if ((cell.Key == "HorzEVP") ||
                          (cell.Key == "VertEVP") ||
                          (cell.Key == "EVPmax"))
                {
                    cell.cellStyle = EditStyle.esEllipsis;
                    cell.ButtonText = "Розрахунок.";
                }
                else if (cell.Key == "NetName")
                {
                    cell.cellStyle = EditStyle.esPickList;
                }
                else if (cell.Key == "kCntrName")
                {
                    cell.PickList = GetContourList();
                    cell.cellStyle = EditStyle.esPickList;
                }
                else if (cell.Key == "kBlocFreq")
                {
                    List<string> BlockStringList = new List<string>();
                    for (int i = 0; i < BlockList.Count; i++)
                    {
                        BlockStringList.Add(BlockList[i].Channel);
                    }
                    cell.PickList = BlockStringList;
                    cell.cellStyle = EditStyle.esPickList;
                }
                else if (cell.Key == "kSystem")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = SystemModeList;
                }
                else if (cell.Key == "kStandard")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = StandardModeList;
                }
                else if (cell.Key == "kRecvType")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CRxMode.getRxModeList();
                }
                else if (cell.Key == "kSpectralMask")
                {
                    cell.cellStyle = EditStyle.esPickList;
                    cell.PickList = CSpectralMask.getSpectralMaskList();
                }
                else if (cell.Key == "kAsl")
                {
                    cell.cellStyle = EditStyle.esEllipsis;
                }
        }

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRS formVrs = new FormVRS(recordID.Id, recordID.Table);
            formVrs.ShowDialog();
            formVrs.Dispose();
        }

        protected List<string> GetContourList()
        {
            List<string> ContourList = new List<string>();

            IMRecordset r = new IMRecordset(ICSMTbl.itblItuContour, IMRecordset.Mode.ReadOnly);
            r.Select("ID,ADM,NUM");
            r.SetWhere("ADM", IMRecordset.Operation.Eq, "UKR");
            r.OrderBy("NUM", OrderDirection.Ascending);
            try
            {
                r.Open();
                while (!r.IsEOF())
                {
                    string NumContour = r.GetI("NUM").ToString();
                    if (!ContourList.Contains(NumContour))
                        ContourList.Add(NumContour);
                    r.MoveNext();
                }
                //}
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return ContourList;
        }

        public List<FmDigitalChannel> GetBlockList()
        {
            List<FmDigitalChannel> BlockList = new List<FmDigitalChannel>();
            string[] FreqPlansList = { "T-DAB1", "T-DAB2" };

            for (int i = 0; i < FreqPlansList.Length; i++)
            {
                int FreqPlanID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                r.Select("ID,COD");
                r.SetWhere("COD", IMRecordset.Operation.Eq, FreqPlansList[i]);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        FreqPlanID = r.GetI("ID");
                    //}
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }

                if (FreqPlanID != IM.NullI)
                {
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r2.Select("PLAN_ID,CHANNEL,FREQ,BANDWIDTH");
                    r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, FreqPlanID);

                    for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                    {
                        FmDigitalChannel fmChannel = new FmDigitalChannel();
                        fmChannel.Channel = r2.GetS("CHANNEL");
                        fmChannel.Freq = r2.GetD("FREQ");
                        fmChannel.Bandwidth = r2.GetD("BANDWIDTH");
                        BlockList.Add(fmChannel);
                    }

                    r2.Close();
                    r2.Destroy();
                }
            }

            return BlockList;
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "equip")
            {
                // Выбор оборудования
                RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipBro, "NAME", cell.Value, true); 
                if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                {
                    station.objEquip = IMObject.LoadFromDB(recEquip);
                    objStation.Put("EQUIP_ID", recEquip.Id);
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "KLon")
            {
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionBro, 1, Longitude, Latitude);
                if (newPos != null)
                {
                    station.objPosition = newPos;
                    station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                    objStation.Put("SITE_ID", newPos.Id);
                    AutoFill(grid.GetCellFromKey("KLat"), grid);
                    NeedSavePosition = station.objPosition.IsChanged;
                }
            }
            else if (cell.Key == "kAddr")
            {
                if (ShowMessageReference(station.objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms = Longitude;
                    newPos.LatDms = Latitude;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionBro, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        station.objPosition = newPos;
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(grid.GetCellFromKey("KLat"), grid);
                        NeedSavePosition = true;
                    }
                }            
            }
            else if (cell.Key.Equals("horzGain"))
            {
                //-- From FMTVBase
                OnPresButtonHorzGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                Cell fillCell = grid.GetCellFromKey("polar");
                AutoFill(fillCell, grid);
                //-- From FMTVBase
            }
            else if (cell.Key.Equals("vertGain"))
            {
                //-- From FMTVBase
                OnPresButtonVertGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                Cell fillCell = grid.GetCellFromKey("polar");
                AutoFill(fillCell, grid);
                //-- From FMTVBase
            }
            else if (cell.Key == "HorzEVP")
            {
                //-- From FMTVBase
                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("TX_LOSSES");
                // Base power is constant part of power without gain antenna
                cell.Value = CalcERPHorz(Power - FeederLoss);
                //-- From FMTVBase
            }

            else if (cell.Key == "VertEVP")
            {
                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("TX_LOSSES");
                // Base power is constant part of power without gain antenna
                cell.Value = CalcERPVert(Power - FeederLoss);
                //-- From FMTVBase
            }
            else if (cell.Key == "EVPmax")
            {
                string polarization = objStation.GetS("POLARIZATION");

                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("TX_LOSSES");

                //-- From FMTVBase
                if (polarization == "V")
                {
                    cell.Value = CalcERPVert(Power - FeederLoss);
                }
                else if (polarization == "H")
                {
                    cell.Value = CalcERPHorz(Power - FeederLoss);
                }
                else if (polarization == "M")
                {
                    cell.Value = CalcERPMixed(Power - FeederLoss);
                }
                //-- From FMTVBase
            }

            else if (cell.Key == "kAsl")
            {
                if (station.objPosition != null)
                {
                    double Longitude = station.objPosition.LonDec;
                    double Latitude = station.objPosition.LatDec;
                    //Cell fillCell = grid.GetCellFromKey("KOrbit");
                    //double orbita = 0.0;//ConvertType.ToDouble(fillCell, IM.NullD);

                    if (Longitude != IM.NullD && Latitude != IM.NullD)
                    {
                        double asl = IMCalculate.CalcALS(Longitude, Latitude, "4DEC");
                        if (asl != IM.NullD)
                        {
                            cell.Value = IM.RoundDeci(asl, 1).ToString();
                            OnCellValidate(cell, grid);
                        }
                    }
                }
            }
            else
            {
                base.OnPressButton(cell, grid);
            }
        }

        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            FmDigitalApp retStation = new FmDigitalApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;

        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            FmDigitalApp retStation = newAppl as FmDigitalApp;//(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            /*retStation.CopyFromMe(grid,
               (stationList[0].objEquip != null) ? stationList[0].objEquip.GetI("ID") : IM.NullI,
               (stationList[0].objAntenna != null) ? stationList[0].objAntenna.GetI("ID") : IM.NullI,
               (stationList[0].objStation != null) ? stationList[0].objStation.GetS("POLAR") : "",
               (stationList[0].objStation != null) ? stationList[0].objStation.GetD("PWR_ANT") : 0.0);*/
            return retStation;
        }

        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            try
            {
                Cell fillCell = null;

                if (cell.Key == "KLat")
                {
                    if (station.objPosition != null)
                    {
                        double latDMS = station.objPosition.LatDms;
                        double lonDMS = station.objPosition.LongDms;
                        this.Longitude = lonDMS;
                        this.Latitude = latDMS;

                        cell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                        OnCellValidate(cell, grid);

                        fillCell = grid.GetCellFromKey("KLon");
                        if (fillCell != null)
                        {
                            fillCell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                            OnCellValidate(fillCell, grid);
                        }

                        double asl = station.objPosition.Asl;
                        fillCell = grid.GetCellFromKey("kAsl");
                        if (fillCell != null)
                        {
                            fillCell.Value = (asl != IM.NullD) ? asl.ToString("0") : "";
                            OnCellValidate(fillCell, grid);
                        }

                        fillCell = grid.GetCellFromKey("kAddr");
                        if (fillCell != null)
                        {
                            fillCell.Value = station.objPosition.FullAddressAuto;
                            OnCellValidate(fillCell, grid);
                        }

                        fillCell = grid.GetCellFromKey("KPillarName");
                        if (fillCell != null)
                        {
                            fillCell.Value = station.objPosition.Name;
                        }
                    }
                    else
                    {
                        fillCell = grid.GetCellFromKey("KLon");
                        if (fillCell != null)
                        {
                            fillCell.Value = "";
                        }

                        fillCell = grid.GetCellFromKey("kAsl");
                        if (fillCell != null)
                        {
                            fillCell.Value = "";
                        }

                        fillCell = grid.GetCellFromKey("kAddr");
                        if (fillCell != null)
                        {
                            fillCell.Value = "";
                        }
                    }
                }
                else if (cell.Key == "equip")
                {
                    if (station.objEquip != null)
                    {// Заполяем поля значениями                    

                        cell.Value = station.objEquip.GetS("NAME");

                        fillCell = grid.GetCellFromKey("certno");
                        if (fillCell != null)
                        {
                            fillCell.Value = station.objEquip.GetS("CUST_TXT1").ToString();
                            //OnCellValidate(fillCell, grid);
                        }
                        if (fillCell != null)
                        {
                            fillCell = grid.GetCellFromKey("certda");
                            DateTime dt = station.objEquip.GetT("CUST_DAT1");

                            if (dt.Year > 1990)
                                fillCell.Value = dt.ToString("dd.MM.yyyy");
                            else
                                fillCell.Value = "";
                        }
                        fillCell = grid.GetCellFromKey("kEMI");
                        if (fillCell != null)
                        {
                            string DesigEmission = objStation.GetS("DESIG_EM");
                            fillCell.Value = DesigEmission;
                            fillCell = grid.GetCellFromKey("rxband");
                            double BW = HelpFunction.GetBandwidthFromDesigEmission(DesigEmission);
                            fillCell.Value = (BW / 1000000.0).ToString("F3");
                        }
                        fillCell = grid.GetCellFromKey("powera");
                        if (fillCell != null)
                        {
                            double PwrAnt = objStation.GetD("PWR_ANT");
                            fillCell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
                        }
                    }
                    else
                    {
                        cell.Value = "";

                        fillCell = grid.GetCellFromKey("certno");
                        if (fillCell != null)
                        {
                            fillCell.Value = "";
                        }

                        fillCell = grid.GetCellFromKey("certda");
                        if (fillCell != null)
                        {
                            fillCell.Value = "";
                        }
                    }
                }
                else if (cell.Key == "kSystem")
                {
                    int TDabMode = objStation.GetI("TDAB_MODE");
                    Cell CountCell = grid.GetCellFromKey("kCount");
                    Cell IntervalCell = grid.GetCellFromKey("kInterval");

                    switch (TDabMode)
                    {
                        case 1:
                            CountCell.Value = "1536";
                            IntervalCell.Value = "246";
                            break;
                        case 2:
                            CountCell.Value = "62";
                            IntervalCell.Value = "384";
                            break;
                        case 3:
                            CountCell.Value = "31";
                            IntervalCell.Value = "192";
                            break;
                        case 4:
                            CountCell.Value = "123";
                            IntervalCell.Value = "768";
                            break;
                    }
                }
                else if (cell.Key == "kBlocFreq")
                {
                    cell.Value = objStation.GetS("BLOCK");

                    fillCell = grid.GetCellFromKey("kCenterFreq");
                    if (fillCell != null)
                    {
                        fillCell.Value = objStation.GetD("FREQ").ToString("F3");
                    }
                }
                else if (cell.Key == "polar")
                {
                    //-- From FMTVBase
                    AutoFillPolar(cell, grid.GetCellFromKey("horzGain"), grid.GetCellFromKey("vertGain"));
                    //-- From FMTVBase
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    //NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(lon, 0, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (station.objPosition != null)
                    {
                        station.objPosition.LongDms = lon;
                        IndicateDifference(cell, station.objPosition.LonDiffersFromAdm);
                    }
                    Longitude = lon;
                }
            }
            else if (cell.Key == "KLat")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    //NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(0, lat, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (station.objPosition != null)
                    {
                        station.objPosition.LatDms = lat;
                        IndicateDifference(cell, station.objPosition.LatDiffersFromAdm);
                    }
                    Latitude = lat;
                }
            }
            else if (cell.Key == "kAddr")
            {
                if (station.objPosition != null)
                    IndicateDifference(cell, station.objPosition.AddrDiffersFromAdm);
            }
            else if (cell.Key == "kRecvType")
            {
                RxMode = CRxMode.getShortRxMode(cell.Value);
                if (objAssign != null)
                    objAssign.Put("RX_MODE", RxMode);
            }
            else if (cell.Key == "kCntrName")
            {
                string ContourNum = cell.Value;
                string Adm = objStation.GetS("ADM");
                objStation.Put("A_NAME", Adm + ContourNum);
            }
            else if (cell.Key == "equip")
            {
                if (station.objEquip != null)
                {
                    string DesigEmission = station.objEquip.GetS("DESIG_EMISSION");
                    //double BandWidth = station.objEquip.GetD("BW");
                    double PowerAnt = station.objEquip.GetD("MAX_POWER");
                    objStation.Put("DESIG_EM", DesigEmission);
                    objStation.Put("PWR_ANT", PowerAnt - 30.0);
                    //objStation.Put("BW", BandWidth);               
                }
            }
            else if (cell.Key == "kSystem")
            {
                int TDabMode = cell.PickList.IndexOf(cell.Value) + 1;
                objStation.Put("TDAB_MODE", TDabMode);
                AutoFill(cell, grid);
            }
            //-- From FMTVBase
            else if (cell.Key == "polar" || cell.Key == "dir")
            {
                Cell polarCell = grid.GetCellFromKey("polar");
                Cell dirCell = grid.GetCellFromKey("dir");
                Cell horzGainCell = grid.GetCellFromKey("horzGain");
                Cell vertGainCell = grid.GetCellFromKey("vertGain");

                OnValidateDirectionAndPolarization(polarCell, dirCell, horzGainCell, vertGainCell);
            }
            else if (cell.Key == "horzGain")
            {
                OnValidateGain(cell, "H");
            }
            else if (cell.Key == "vertGain")
            {
                OnValidateGain(cell, "V");
            }
            //-- From FMTVBase
            else if (cell.Key == "kSpectralMask")
            {
                int spectralMask = Convert.ToInt32(CSpectralMask.getShortSpectralMask(cell.Value));
                //objStation.Put("HM_CRITICAL", spectralMask);  Не находит поле в БД    29.03.11         
            }
            else if (cell.Key == "height")
            {
                double EffectiveHeight = IM.NullD;
                Double.TryParse(cell.Value, out EffectiveHeight);
                objStation.Put("AGL", EffectiveHeight);
            }
            else if (cell.Key == "maxHeight")
            {
                double EffectiveHeightMax = IM.NullD;
                Double.TryParse(cell.Value, out EffectiveHeightMax);
                objStation.Put("EFHGT_MAX", EffectiveHeightMax);
            }
            else if (cell.Key == "feederlen")
            {
                double FeederLen = IM.NullD;
                Double.TryParse(cell.Value, out FeederLen);
                objStation.Put("CUST_NBR1", FeederLen);
                double FeederLoss = objStation.GetD("CUST_NBR2");

                Cell FilterCell = grid.GetCellFromKey("kFilterLosses");
                double FilterLosses = IM.NullD;
                Double.TryParse(FilterCell.Value, out FilterLosses);
                objStation.Put("TX_LOSSES", FilterLosses + FeederLoss * FeederLen);
                //SetTxLosses();
            }
            else if (cell.Key == "feederloss")
            {
                double FeederLoss = IM.NullD;
                Double.TryParse(cell.Value, out FeederLoss);
                objStation.Put("CUST_NBR2", FeederLoss);
                double FeederLen = objStation.GetD("CUST_NBR1");

                Cell FilterCell = grid.GetCellFromKey("kFilterLosses");
                double FilterLosses = IM.NullD;
                Double.TryParse(FilterCell.Value, out FilterLosses);
                objStation.Put("TX_LOSSES", FilterLosses + FeederLoss * FeederLen);

                //SetTxLosses();
            }
            else if (cell.Key == "kFilterLosses")
            {
                double FilterLoss = IM.NullD;
                Double.TryParse(cell.Value, out FilterLoss);

                double FeederLoss = objStation.GetD("CUST_NBR2");
                double FeederLen = objStation.GetD("CUST_NBR1");

                if (FeederLoss != IM.NullD && FeederLen != IM.NullD)
                    objStation.Put("TX_LOSSES", FilterLoss + FeederLoss * FeederLen);
            }
            else if (cell.Key == "kStandard")
            {
                string Standard = cell.Value;
                objStation.Put("REMARK", Standard);
            }
            else if (cell.Key == "homeState")
            {
                string homeState = CFMState.getShortState(cell.Value);
                objStation.Put("CUST_TXT4", homeState);
            }
            else if (cell.Key == "internationalState")
            {
                string internationalState = CFMState.getShortState(cell.Value);
                objStation.Put("CUST_TXT5", internationalState);
            }
            else if (cell.Key == "powera")
            {
                double MaxPower = 1E+99;
                if (station.objEquip != null)
                    MaxPower = station.objEquip.GetD("MAX_POWER");

                double PwrAnt = Convert.ToDouble(cell.Value);
                double ConvertedPwrAnt = 30 + 10.0 * Math.Log10(PwrAnt);
                if (MaxPower < ConvertedPwrAnt)
                {
                    double VisibleMax = Math.Pow(10, (MaxPower - 30.0) / 10.0);
                    MessageBox.Show("Занадто велике значення потужності. Не повинно перевищувати " + VisibleMax.ToString("F0"));
                }
                else
                {
                    double SavedPwrAnt = 10.0 * Math.Log10(PwrAnt);
                    objStation.Put("PWR_ANT", SavedPwrAnt);
                }
                //double PwrAnt = objStation.GetD("PWR_ANT");
                //cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
            }
            else if (cell.Key == "kBlocFreq")
            {
                string Block = cell.Value;
                objStation.Put("BLOCK", Block);

                for (int i = 0; i < BlockList.Count; i++)
                {
                    if (BlockList[i].Channel == Block)
                    {
                        objStation.Put("FREQ", BlockList[i].Freq);
                        AutoFill(cell, grid);
                    }
                }
            }
        }
        //===========================================================
        public void SetTxLosses()
        {
            double FeederLen = objStation.GetD("CUST_NBR1");
            double FeederLoss = objStation.GetD("CUST_NBR2");
            double FilterLoss = 0;//objStation.GetD("CUST_NBR3");
            objStation.Put("TX_LOSSES", FeederLen * FeederLoss + FilterLoss);
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "kAddr"))// && (station.objPosition != null))
                {
                    if (station.objPosition != null)
                    {
                        int posID = station.objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionBro, posID, (IM.TableRight(ICSMTbl.itblPositionBro) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        station.objPosition.LoadStatePosition(station.objPosition.Id, station.objPosition.TableName);
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(gridParam.GetCellFromKey("KLat"), gridParam);
                    }
                    else
                        ShowMessageNoReference();

                }
                /*else if ((cell.Key == "kAntType_1")
                    || (cell.Key == "kAntType_2")
                    || (cell.Key == "kAntType_3")
                    || (cell.Key == "kAntType_4")
                    || (cell.Key == "kAntType_5")
                    || (cell.Key == "kAntType_6"))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(9));
                    if (stationList[index - 1].objAntenna != null)
                    {
                        int AntID = stationList[index - 1].AntID;
                        RecordPtr rcPtr = new RecordPtr(stationList[0].objAntenna.GetS("TABLE_NAME"), AntID);
                        rcPtr.UserEdit();
                    }
                }  */
                else if ((cell.Key == "equip") && (station.objEquip != null))
                {
                    int EquipID = station.objEquip.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(station.objEquip.GetS("TABLE_NAME"), EquipID);
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KObj") && (objStation != null))
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(TableName, ID);
                    rcPtr.UserEdit();
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        public double GetPower()
        {
            if (objStation != null)
            {
                double PwrAnt = objStation.GetD("PWR_ANT");
                return Math.Pow(10.0, PwrAnt / 10.0); ;
            }
            else
                return IM.NullD;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
    }
}
