﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    struct FmDigChannel
    {
        public double Freq;
        public string Channel;
        public double Bandwidth;
    }

    class FmDigitallStation : ERPStation
    {        

        public List<string> SystSendList = new List<string>();

        public Dictionary<string, string> InnerStateDict = new Dictionary<string, string>();
        public Dictionary<string, string> OuterStateDict = new Dictionary<string, string>();
        public List<string> CountNesivList = new List<string>();
        public List<string> SafeIntervList = new List<string>();
        public List<string> SpectrMaskList = new List<string>();
        public Dictionary<string, string> TypeReceiveDict = new Dictionary<string, string>();
        public List<string> VariantSystList = new List<string>();
        public List<string> StandComprList = new List<string>();
        public List<string> ChannelList = new List<string>();
        public List<Double> FreqList = new List<Double>();
        public List<FmDigChannel> BlockList = new List<FmDigChannel>();
        public List<string> ChannelBlockList = new List<string>();
        public List<string> ContourList = new List<string>();
        
        public FmDigitallEquipment FmDgEquipment { get; set; }
        public FmDigitallAntenna FmDgAntenna { get; set; }

        public int SiteId { get; set; }
        public int AssgnId { get; set; }
        public int EquipId { get; set; }
        public int PlanId { get; set; }
        public string NameNumber { get; set; }
        public string Countur { get; set; }
        public string NameSynchr { get; set; }

        public string NameVariant { get; set; }

        public string CountOstov { get; set; }
        public int SafeInterv { get; set; }
        public double LoseFiltr { get; set; }
        public string SpectrMaskFiltr { get; set; }
        public string TypeReceive { get; set; }
        public string StandCompr { get; set; }
        public double ShiftOstov { get; set; }

        public string NameRez { get; set; }
        public double PowerTx { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }


        public double HeightAnten { get; set; }
        public double MaxHigh { get; set; }


        public string InnerState { get; set; }
        public string OuterState { get; set; }

        public string Block { get; set; }
        public double FreqCentr { get; set; }

        public int ObjRCHP { get; set; }

        public static readonly string TableName = ICSMTbl.itblTDAB;

        public FmDigitallStation()
        {            
        }

        public override void Load()
        {
            //Заповнення комбо боксів
            FillCombo();

            FmDgEquipment=new FmDigitallEquipment();
            FmDgEquipment.TableName = ICSMTbl.itblEquipBro;

            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                //r.Select("ID,CUST_TXT7,SITE_ID,PLAN_ID,ASSGN_ID,EQUIP_ID,A_NAME,ADM,TDAB_MODE,HM_CRITICAL");
                r.Select("ID,CUST_TXT7,SITE_ID,PLAN_ID,ASSGN_ID,EQUIP_ID,A_NAME,ADM,TDAB_MODE");
                r.Select("REMARK,OFFSET,CUST_NBR1,CUST_NBR2,TX_LOSSES,DIAGH,DIAGV,POLARIZATION");
                r.Select("ERP_H,ERP_V,AGL,EFHGT_MAX,FREQ,CUST_TXT4,CUST_TXT5,BLOCK,PWR_ANT,GAIN");
                //r.Select("");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");
                    SiteId = r.GetI("SITE_ID");
                    PlanId = r.GetI("PLAN_ID");
                    AssgnId = r.GetI("ASSGN_ID");
                    FmDgEquipment.Id = r.GetI("EQUIP_ID");
                    if (FmDgEquipment.Id != IM.NullI)
                        FmDgEquipment.Load();
                    NameNumber = r.GetS("CUST_TXT7");

                    //  ContourList = GetContourList();
                    string aName = r.GetS("A_NAME");
                    string Adm = r.GetS("ADM");
                    if (!string.IsNullOrEmpty(aName))
                        Countur = aName;
//                        Countur = aName.Replace(Adm, "");

                    int TDabMode = r.GetI("TDAB_MODE");
                    if (TDabMode != IM.NullI)
                        NameVariant = "MODE" + TDabMode.ToString();

                    switch (TDabMode)
                    {
                        case 1:
                            CountOstov = "1536";
                            SafeInterv = 246;
                            break;
                        case 2:
                            CountOstov = "62";
                            SafeInterv = 384;
                            break;
                        case 3:
                            CountOstov = "31";
                            SafeInterv = 192;
                            break;
                        case 4:
                            CountOstov = "123";
                            SafeInterv = 768;
                            break;
                    }

                    //  NameSynchr = r.GetS("CODE");
                    //SpectrMaskFiltr = r.GetS("HM_CRITICAL").ToInt32(IM.NullI) == 2 ? SpectrMaskList[0] : SpectrMaskList[1];
                    SpectrMaskFiltr = SpectrMaskList[0];

                    FeederLength = r.GetD("CUST_NBR1");
                    FeederLosses = r.GetD("CUST_NBR2");
                    //Висновок
                //    Finding = r.GetS("CUST_TXT13");
                    LoseFiltr = r.GetD("TX_LOSSES") - FeederLength * FeederLosses;
                    StandCompr = r.GetS("REMARK");
                    ShiftOstov = r.GetD("OFFSET") * 1000;

                    Erp.Horizontal[PowerUnits.dBW] = r.GetD("ERP_H");
                    Erp.Vertical[PowerUnits.dBW] = r.GetD("ERP_V");
                    HorizontalDiagramm.DiagrammAsString = r.GetS("DIAGH");
                    VerticalDiagramm.DiagrammAsString = r.GetS("DIAGV");
                    HorizontalDiagramm.Gain = r.GetD("GAIN");
                    VerticalDiagramm.Gain = r.GetD("GAIN");
                    _gain = r.GetD("GAIN");

                    Polarization = r.GetS("POLARIZATION");
                    
                    HeightAnten = r.GetD("AGL");
                    MaxHigh = r.GetD("EFHGT_MAX");
                    Power[PowerUnits.dBW] = r.GetD("PWR_ANT");

                    Block = r.GetS("BLOCK");
                    FreqCentr = r.GetD("FREQ");
                    InnerState = r.GetS("CUST_TXT4");
                    OuterState = r.GetS("CUST_TXT5");
                    ObjRCHP = r.GetI("ID");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            Position = new PositionState();
            Position.LoadStatePosition(SiteId, ICSMTbl.itblPositionBro);
            NameRez = Position.Name;

            //ASSIGN
            IMRecordset rAssgn = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadOnly);
            try
            {
                rAssgn.Select("ID,RX_MODE");
                rAssgn.SetWhere("ID", IMRecordset.Operation.Eq, AssgnId);
                rAssgn.Open();
                if (!rAssgn.IsEOF())
                    TypeReceive = rAssgn.GetS("RX_MODE");
            }
            finally
            {
                if (rAssgn.IsOpen())
                    rAssgn.Close();
                rAssgn.Destroy();
            }
            //Обладнання 

            FmDgEquipment = new FmDigitallEquipment();
            FmDgEquipment.Id = EquipId;
            if (EquipId != IM.NullI)
            {
                FmDgEquipment.Load();

                NameRez = FmDgEquipment.Name;
                PowerTx = FmDgEquipment.MaxPower[PowerUnits.dBm];
                ClassEmission = FmDgEquipment.DesigEmission;
                Bandwith = FmDgEquipment.Bandwidth;
                CertificateDate = FmDgEquipment.Certificate.Date;
                CertificateNumber = FmDgEquipment.Certificate.Symbol;
            }
        }

        //Визначення спрямованості антени
        /*public string SetDirection(string diagH, string diagV)
        {
            string direction = "ND";
            if (!(string.IsNullOrEmpty(diagH) && string.IsNullOrEmpty(diagV)))
            {
                if ((diagV.Equals("OMNI") && (diagH.Equals("OMNI") || string.IsNullOrEmpty(diagH))) ||
                      (diagH.Equals("OMNI") && string.IsNullOrEmpty(diagV)))
                    direction = "ND";
                else
                    direction = "D";
            }
            return direction;
        }*/


        private string ADMParseNumber(string admKey)
        {
            if (string.IsNullOrEmpty(admKey))
                return string.Empty;

            int lenght = admKey.LastIndexOf('0') - 3;
            if (lenght <= 0)
                return string.Empty;

            string number = admKey.Substring(3, lenght);

            return number;
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,CUST_TXT7,SITE_ID,PLAN_ID,ASSGN_ID,EQUIP_ID,A_NAME,ADM,TDAB_MODE");
                //r.Select("CUST_NBR2,CUST_NBR1,TX_LOSSES,HM_CRITICAL,REMARK,OFFSET,BLOCK,ERP_H,ERP_V");
                r.Select("CUST_NBR2,CUST_NBR1,TX_LOSSES,REMARK,OFFSET,BLOCK,ERP_H,ERP_V");
                r.Select("POLARIZATION,AGL,EFHGT_MAX,CUST_TXT4,CUST_TXT5,FREQ");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", Id);
                    if (Position.TableName != ICSMTbl.itblPositionBro)
                    {
                        Position.Id = IM.NullI;
                        Position.TableName = ICSMTbl.itblPositionBro;
                        Position.SaveToBD();
                        SiteId = Position.Id;
                    }
                    //else
                    //{
                    //    SiteId = Position.Id;
                    //    Position.SaveToBD();
                    //}
                    r.Put("SITE_ID", Position.Id);
                    r.Put("EQUIP_ID", FmDgEquipment.Id);
                    r.Put("CUST_TXT7", NameNumber);
                    r.Put("ID", ObjRCHP);

                    //Put("ERP_H", Evpg);
                    //r.Put("ERP_V", Evpv);

                    string adm = r.GetS("ADM");
                    r.Put("A_NAME", adm + Countur);
                    // r.Put("CODE", NameSynchr);
                    string tdab = NameVariant.Substring(NameVariant.Count() - 1);
                    r.Put("TDAB_MODE", Convert.ToInt32(tdab));

                    double FilterLoss = IM.NullD;
                    double.TryParse(LoseFiltr.ToString(), out FilterLoss);
                    double FeederLoss = r.GetD("CUST_NBR2");
                    double FeederLen = r.GetD("CUST_NBR1");
                    if (FeederLoss != IM.NullD && FeederLen != IM.NullD)
                        r.Put("TX_LOSSES", FilterLoss + FeederLoss * FeederLen);
                    //r.Put("HM_CRITICAL", SpectrMaskList.IndexOf(SpectrMaskFiltr));
                    r.Put("REMARK", StandCompr);
                    r.Put("OFFSET", ShiftOstov * 0.001);

                    //C
                    r.Put("PWR_ANT", _power[PowerUnits.dBW]);
                    r.Put("POLARIZATION", Polarization);

                    if (Polarization != "V")
                    {
                        r.Put("DIAGH", HorizontalDiagramm.DiagrammAsString);
                        r.Put("ERP_H", Erp.Horizontal[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGH", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    if (Polarization != "H")
                    {
                        r.Put("DIAGV", VerticalDiagramm.DiagrammAsString);
                        r.Put("ERP_V", Erp.Vertical[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGV", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    r.Put("AGL", HeightAnten);
                    r.Put("EFHGT_MAX", MaxHigh);

                    r.Put("CUST_TXT4", InnerState);
                    r.Put("CUST_TXT5", OuterState);
                    r.Put("BLOCK", Block);
                    //r.Put("FREQ_BLOCK", Block);
                    r.Put("FREQ", FreqCentr);

                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }


            //ASSIGN
            IMRecordset rAssgn = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadWrite);
            try
            {
                rAssgn.Select("ID,RX_MODE");
                rAssgn.SetWhere("ID", IMRecordset.Operation.Eq, AssgnId);
                rAssgn.Open();
                if (!rAssgn.IsEOF())
                    rAssgn.Put("RX_MODE", TypeReceive);
            }
            finally
            {
                if (rAssgn.IsOpen())
                    rAssgn.Close();
                rAssgn.Destroy();
            }

            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);
            
            FillComments(ApplId);

            //Удаление временной строчки...
            IMRecordset delTempRec = new IMRecordset(PlugTbl.itblXnrfaDiffFmDigital, IMRecordset.Mode.ReadWrite);
            try
            {
                delTempRec.Select("ID");
                delTempRec.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                delTempRec.Open();
                if (!delTempRec.IsEOF())
                    delTempRec.Delete();
            }
            finally
            {
                if (delTempRec.IsOpen())
                    delTempRec.Close();
                delTempRec.Destroy();
            }
        }

        private void FillCombo()
        {
            /*//Направленість
            DirectAntDict.Add("D", "Спрямована");
            DirectAntDict.Add("ND", "Неспрямована");

            //Поляризація
            PolarAntDict.Add("V", "Вертикальна");
            PolarAntDict.Add("H", "Горизонтальна");
            PolarAntDict.Add("M", "Змішана");*/

            // fills Внутрішній стан 
            InnerStateDict.Add("1", "Рассматриваемый");
            InnerStateDict.Add("2", "Разрешенный");
            InnerStateDict.Add("3", "Действующий");
            InnerStateDict.Add("4", "Новое сост.8");
            InnerStateDict.Add("5", "Новое сост.2");
            InnerStateDict.Add("6", "Новое сост.3");
            InnerStateDict.Add("7", "Столб");
            InnerStateDict.Add("8", "Рекоммендация");
            InnerStateDict.Add("9", "Уточнение хар-к");
            InnerStateDict.Add("10", "Планируемый");
            InnerStateDict.Add("33", "BR IFIC ADD");
            InnerStateDict.Add("34", "BR IFIC REC");
            InnerStateDict.Add("35", "BR IFIC SUP");
            InnerStateDict.Add("36", "BR IFIC MOD");
            InnerStateDict.Add("38", "Geneva 06");

            // Міжнародний стан
            OuterStateDict.Add("A", "Скоорд.");
            OuterStateDict.Add("B", "Берлин 59");
            OuterStateDict.Add("C", "Чужой скоорд.");
            OuterStateDict.Add("D", "Удаленный, был.");
            OuterStateDict.Add("E", "Цифра, планируем.");
            OuterStateDict.Add("F", "Прошел коорд.");
            OuterStateDict.Add("G", "Женева 84");
            OuterStateDict.Add("G06", "GENEVA 2006");
            OuterStateDict.Add("H", "Цифра, план Честер");
            OuterStateDict.Add("I", "Честер");
            OuterStateDict.Add("K", "На коорд.");
            OuterStateDict.Add("L", "Цифра, удаленный");
            OuterStateDict.Add("M", "Цифра, не надо коорд.");
            OuterStateDict.Add("N", "Не надо коорд.");
            OuterStateDict.Add("O", "Планируемый");
            OuterStateDict.Add("P", "Под крышей");
            OuterStateDict.Add("Q", "Цифра, регистр.");
            OuterStateDict.Add("R", "Регистр");
            OuterStateDict.Add("S", "Стокгольм");
            OuterStateDict.Add("T", "Цифра, скоорд.");
            OuterStateDict.Add("TP", "Цифра, скоорд. на пп");
            OuterStateDict.Add("U", "Цифра на коорд.");
            OuterStateDict.Add("Y", "Цифра, нескоорд.");
            OuterStateDict.Add("Z", "Не скоорд.");

            // кількість несівних 
            CountNesivList.Add("1536");
            CountNesivList.Add("62");
            CountNesivList.Add("31");
            CountNesivList.Add("123");

            // захисний інтервал
            SafeIntervList.Add("246");
            SafeIntervList.Add("384");
            SafeIntervList.Add("192");
            SafeIntervList.Add("768");

            // тип спектр. маски фільтра 
            SpectrMaskList.Add("не критична");
            SpectrMaskList.Add("критична");

            // тип прийома
            TypeReceiveDict.Add("MO", "мобільний");
            TypeReceiveDict.Add("FX", "фіксований");
            TypeReceiveDict.Add("PO", "портат. зовн.");
            TypeReceiveDict.Add("PI", "портат. внутр.");

            // Варіант системи         
            VariantSystList.Add("MODE1");
            VariantSystList.Add("MODE2");
            VariantSystList.Add("MODE3");
            VariantSystList.Add("MODE4");

            //Стандарт компресії
            StandComprList.Add("MPEG2");
            StandComprList.Add("MPEG4");


            IMRecordset r = new IMRecordset(ICSMTbl.itblItuContour, IMRecordset.Mode.ReadOnly);
            r.Select("ID,ADM,NUM");
            r.SetWhere("ADM", IMRecordset.Operation.Eq, "UKR");
            r.OrderBy("NUM", OrderDirection.Ascending);
            try
            {
                r.Open();
                while (!r.IsEOF())
                {
                    string numContour = r.GetI("NUM").ToString();
                    if (!ContourList.Contains(numContour))
                        ContourList.Add(numContour);
                    r.MoveNext();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Channel

            string[] FreqPlansList = { "T-DAB1", "T-DAB2" };

            for (int i = 0; i < FreqPlansList.Length; i++)
            {
                int FreqPlanID = IM.NullI;
                r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                r.Select("ID,COD");
                r.SetWhere("COD", IMRecordset.Operation.Eq, FreqPlansList[i]);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        FreqPlanID = r.GetI("ID");
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }

                if (FreqPlanID != IM.NullI)
                {
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r2.Select("PLAN_ID,CHANNEL,FREQ,BANDWIDTH");
                    r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, FreqPlanID);

                    for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                    {
                        FmDigChannel fmChannel = new FmDigChannel();
                        fmChannel.Channel = r2.GetS("CHANNEL");
                        fmChannel.Freq = r2.GetD("FREQ");
                        fmChannel.Bandwidth = r2.GetD("BANDWIDTH");
                        ChannelBlockList.Add(fmChannel.Channel);
                        BlockList.Add(fmChannel);
                    }
                    if (r2.IsOpen())
                        r2.Close();
                    r2.Destroy();
                }
            }



        }

    }
}
