﻿using System;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStation2AppDiffStation : BaseStation, ICloneable
    {
        public MobStationAntenna mobDiffStationAntena;
        public MobStationEquipment mobDiffStationEquip;

        public bool IsEnableDiff { get; set; }

        public int PosId { get; set; }

        public int EquipId { get; set; }
        public int AntId { get; set; }

        //public PositionState Position { get; set; }
        public string EquipmentName { get; set; }

        public string Modulation { get; set; }

        //ширина смуги
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        //Сертифікати
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }

        public double AglMaxEmission { get; set; }
        public double AglSiteMax { get; set; }

        public string AntenName { get; set; }
        public string AntenType { get; set; }
        public double WideAnten { get; set; }
        public double HighAnten { get; set; }
        public double KoefAnten { get; set; }

        public double MaxPower { get; set; }

        public string NomReceive { get; set; }
        public string NomSend { get; set; }
        public List<double> NomReceiveList = new List<double>();
        public List<double> NomSendList = new List<double>();

        public string PolarAnten { get; set; }

        public int OnOff { get; set; }

        public static readonly string TableName = PlugTbl.itblXnrfaDiffMobSta2;

        public MobStation2AppDiffStation()
        {            
        }

        public object Clone()
        {
            MobStation2AppDiffStation clone = new MobStation2AppDiffStation();
            clone.Id = Id;
            clone.IsEnableDiff = IsEnableDiff;

            clone.mobDiffStationAntena = mobDiffStationAntena;
            clone.mobDiffStationEquip = mobDiffStationEquip;

            clone.PosId = PosId;

            clone.EquipId = EquipId;
            clone.AntId = AntId;

            clone.Position = Position;
            clone.EquipmentName = EquipmentName;

            clone.Modulation = Modulation;

            clone.Bandwith = Bandwith;
            clone.ClassEmission = ClassEmission;
            //Сертифікати
            clone.CertificateNumber = CertificateNumber;
            clone.CertificateDate = CertificateDate;

            clone.AglMaxEmission = AglMaxEmission;
            clone.AglSiteMax = AglSiteMax;
            clone.AntenType = AntenType;
            clone.WideAnten = WideAnten;
            clone.HighAnten = HighAnten;
            clone.KoefAnten = KoefAnten;

            clone.MaxPower = MaxPower;

            clone.NomReceive = NomReceive;
            clone.NomSend = NomSend;
            clone.NomReceiveList = NomReceiveList;
            clone.NomSendList = NomSendList;

            clone.PolarAnten = PolarAnten;

            clone.OnOff = OnOff;
            return clone;
        }

        public override void Save()
        {            
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta2, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,MOBSTA2_ID,POS_ID,POS_TABLE,BW,DESIG_EM,MODULATION,ELEVATION,PWR_ANT,ASL,CERT_NUM,CERT_DATE");
                r.Select("AZIMUTH,ASL,POLAR,MODULATION,ELEVATION,WIDTH_ANT,ONOFF,GAIN");
                r.Select("EQUIP_NAME,COMMENTS");
                r.Select("ANTEN_TYPE,INACTIVE");

                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("MOBSTA2_ID", Id);
                }
                else // 
                    r.Edit();

                r.Put("INACTIVE", 0);
                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }
                r.Put("ELEVATION", AglSiteMax);
                r.Put("AZIMUTH", AglMaxEmission);
                r.Put("POLAR", PolarAnten);
                r.Put("WIDTH_ANT", WideAnten);
                r.Put("ASL", HighAnten);
                r.Put("ANTEN_TYPE", AntenType);
                r.Put("ONOFF", OnOff);
                r.Put("GAIN", KoefAnten);
                r.Put("PWR_ANT", MaxPower);
                r.Put("BW", Bandwith);
                r.Put("CERT_NUM", mobDiffStationEquip.Certificate.Symbol);
                r.Put("CERT_DATE", mobDiffStationEquip.Certificate.Date);
                r.Put("DESIG_EM", ClassEmission);
                r.Put("MODULATION", Modulation);
                r.Put("EQUIP_NAME", EquipmentName);
                r.Put("ASL", HighAnten);
                r.Put("COMMENTS", StatComment);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Частоти Diff
            string[] nomReceiveArray = NomReceive.Split(';');
            string[] nomSendArray = NomSend.Split(';');

            //Видалення старих даних
            IMRecordset rFreq = new IMRecordset(PlugTbl.itblXnrfaMobSta2Freq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA2_ID,TR");
            rFreq.SetWhere("MOBSTA2_ID", IMRecordset.Operation.Eq, Id);
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                rFreq.Delete();
            }
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();

            rFreq = new IMRecordset(PlugTbl.itblXnrfaMobSta2Freq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA2_ID,TR");
            rFreq.Open();
            for (int i = 0; i < nomReceiveArray.Length; i++)
            {
                rFreq.AddNew();
                rFreq.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobSta2Freq, 1, -1));
                rFreq.Put("MOBSTA2_ID", Id);
                double currReceive;
                if (double.TryParse(nomReceiveArray[i], out currReceive))
                    rFreq.Put("FREQUENCY", currReceive);
                else
                    rFreq.Put("FREQUENCY", 0.0);
                rFreq.Put("TR", "R");
                rFreq.Update();
            }
            for (int i = 0; i < nomSendArray.Length; i++)
            {
                rFreq.AddNew();
                rFreq.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobSta2Freq, 1, -1));
                rFreq.Put("MOBSTA2_ID", Id);
                double currSend;
                if (double.TryParse(nomSendArray[i], out currSend))
                    rFreq.Put("FREQUENCY", currSend);
                else
                    rFreq.Put("FREQUENCY", 0.0);
                rFreq.Put("TR", "T");
                rFreq.Update();
            }
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();
        }

        public override void Load()
        {
            mobDiffStationAntena = new MobStationAntenna();
            mobDiffStationAntena.TableName = ICSMTbl.itblAntennaMob2;

            mobDiffStationEquip = new MobStationEquipment();
            mobDiffStationEquip.TableName = ICSMTbl.itblEquipMob2;

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta2, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,POS_TABLE,DESIG_EM,BW,CERT_NUM,CERT_DATE,POLAR");
                r.Select("MODULATION,PWR_ANT,AZIMUTH,POLAR,MODULATION,ELEVATION,WIDTH_ANT");
                r.Select("GAIN,ASL,ONOFF,COMMENTS");
                r.Select("ANTEN_TYPE");
                r.Select("EQUIP_NAME,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                IsEnableDiff = false;
                if (!r.IsEOF())
                {
                    
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Id = r.GetI("ID");
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;

                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        if (tmpPosId != IM.NullI)
                        {
                            Position.LoadStatePosition(tmpPosId, tmpPosTable);
                            PosId = r.GetI("POS_ID");
                            Position.TableName = r.GetS("POS_TABLE");
                            Bandwith = r.GetD("BW");

                            ClassEmission = r.GetS("DESIG_EM");
                            mobDiffStationEquip.Certificate.Symbol = r.GetS("CERT_NUM");
                            mobDiffStationEquip.Certificate.Date = r.GetT("CERT_DATE");
                            PolarAnten = r.GetS("POLAR");
                            EquipmentName = r.GetS("EQUIP_NAME");
                            Modulation = r.GetS("MODULATION");
                            MaxPower = r.GetD("PWR_ANT");
                            AglSiteMax = r.GetD("ELEVATION");
                            AglMaxEmission = r.GetD("AZIMUTH");
                            PolarAnten = r.GetS("POLAR");
                            Modulation = r.GetS("MODULATION");
                            WideAnten = r.GetD("WIDTH_ANT");
                            HighAnten = r.GetD("ASL");
                            KoefAnten = r.GetD("GAIN");
                            AntenType = r.GetS("ANTEN_TYPE");
                            OnOff = r.GetI("ONOFF");
                            StatComment = r.GetS("COMMENTS");
                        }
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            IMRecordset rFreq = new IMRecordset(PlugTbl.itblXnrfaMobSta2Freq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA2_ID,TR");
            rFreq.SetWhere("MOBSTA2_ID", IMRecordset.Operation.Eq, Id);
            rFreq.SetWhere("TR", IMRecordset.Operation.Eq, "R");
            NomReceive = string.Empty;
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                double currDouble = rFreq.GetD("FREQUENCY");
                NomReceive += currDouble + ";";
            }
            if (NomReceive.Length > 0)
                NomReceive = NomReceive.Remove(NomReceive.Length - 1);
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();

            rFreq = new IMRecordset(PlugTbl.itblXnrfaMobSta2Freq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA2_ID,TR");
            rFreq.SetWhere("MOBSTA2_ID", IMRecordset.Operation.Eq, Id);
            rFreq.SetWhere("TR", IMRecordset.Operation.Eq, "T");
            NomSend = string.Empty;
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                double currDouble = rFreq.GetD("FREQUENCY");
                NomSend += currDouble + ";";
            }

            if (NomSend.Length > 0)
                NomSend = NomSend.Remove(NomSend.Length - 1);
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();
        }

        public void Remove()
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta2, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();                    
                }
                //DeleteFrequencies();
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
