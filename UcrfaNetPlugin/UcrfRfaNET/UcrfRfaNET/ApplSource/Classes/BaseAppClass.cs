﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.DiffClasses;
using XICSM.UcrfRfaNET.FlexiGrid;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Collections.Generic;
using System.Drawing;
using XICSM.UcrfRfaNET.StationsStatus;
using XICSM.UcrfRfaNET.Documents;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using OrmCs;

namespace XICSM.UcrfRfaNET.ApplSource
{

    public static class CallBackFunction
    {
        public delegate void callbackEvent(string what);
        public static callbackEvent callbackEventHandler;

        public delegate void callbackEventFunc(Action<string> what);
        public static callbackEventFunc callbackEventHandlerFunc;
    }

    //===============================================================
    //===============================================================
    /// <summary>
    /// Базовый класс станции
    /// </summary>
    public class BaseStation : IDisposable
    {
        private class Doclink
        {
            public string Path { get; set; }
            public string Name { get; set; }
            public int DocId { get; set; }
            public int IsUrl { get; set; }
        };


        public int Id { get; set; }
        //public string TableName { get; set;}
        public int ApplId { get; set; }

        // Висновок
        public string Finding { get; set; }

        public string StatComment { get; set; }

        public virtual PositionState Position { get; set; }

        public void Dispose()
        {
            DisposeExec();
        }

        public virtual void Load(int id)
        {
        }

        public virtual void Load()
        {
        }

        public virtual void Save()
        {
        }

        protected virtual void DisposeExec()
        {
        }

        /// <summary>
        /// "Подтягивает" данные оборудования в данные станции.
        /// </summary>
        public virtual void OnNewEquipment()
        {
        }

        /// <summary>
        /// "Подтягивает" данные антенны в данные станции.
        /// </summary>
        public virtual void OnNewAntenna()
        {
        }

        /// <summary>
        /// Функции для установки позиции. 
        /// Должны быть переопределены в производных классах.
        /// 
        /// Не абстрактные, так как старые классы, такие как StationZS
        /// тоже наследуются от этой BaseStation
        /// </summary>
        /// <returns></returns>
        protected virtual PositionState GetPositionState()
        {
            return null;

        }

        protected virtual void SetPositionState(PositionState newPositionaState)
        {

        }

        public void FillComments(int applId)
        {
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rs.Select("ID,COMMENTS");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    rs.Edit();
                    rs.Put("COMMENTS", StatComment);
                    rs.Update();
                }
            }
            finally
            {
                if (rs.IsOpen())
                    rs.Close();
                rs.Destroy();
            }
        }
       


        public void RenameStationInTheDoclink(string tableName, int id, string positionTableName, int positionId)
        {
            IMRecordset renameStatRec = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            renameStatRec.Select("REC_TAB,REC_ID,PATH,NAME,IS_URL,DOC_ID");
            renameStatRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableName);
            renameStatRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);

            List<Doclink> doclinkList = new List<Doclink>();
            try
            {
                for (renameStatRec.Open(); !renameStatRec.IsEOF(); renameStatRec.MoveNext())
                {
                    //renameStatRec.Edit();
                    Doclink doclink = new Doclink();
                    doclink.Path = renameStatRec.GetS("PATH");
                    doclink.Name = renameStatRec.GetS("NAME");
                    doclink.IsUrl = renameStatRec.GetI("IS_URL");
                    doclink.DocId = renameStatRec.GetI("DOC_ID");
                    doclinkList.Add(doclink);
                    //renameStatRec.Put("REC_TAB", positionTableName);
                    //renameStatRec.Put("REC_ID", positionId);
                    renameStatRec.Delete();
                }
            }
            finally
            {
                if (renameStatRec.IsOpen())
                    renameStatRec.Close();
                renameStatRec.Destroy();
            }

            renameStatRec = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            renameStatRec.Select("REC_TAB,REC_ID,PATH,NAME,IS_URL,DOC_ID");
            renameStatRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableName);
            renameStatRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);

            try
            {
                renameStatRec.Open();
                foreach (Doclink doclink in doclinkList)
                {
                    renameStatRec.AddNew();
                    renameStatRec.Put("REC_TAB", positionTableName);
                    renameStatRec.Put("REC_ID", positionId);
                    renameStatRec.Put("NAME", doclink.Name);
                    renameStatRec.Put("PATH", doclink.Path);
                    renameStatRec.Put("IS_URL", doclink.IsUrl);
                    renameStatRec.Put("DOC_ID", doclink.DocId);
                    renameStatRec.Update();
                }
            }
            finally
            {
                if (renameStatRec.IsOpen())
                    renameStatRec.Close();
                renameStatRec.Destroy();
            }
        }

        //===========================================================
        /// <summary>
        /// Создает обьект станции
        /// </summary>
        /// <param name="appId">ID заявки</param>
        public static BaseStation GetStationFromAppl(int appId)
        {
            BaseStation retStation = null;
            int objId = IM.NullI;
            string tableName = "";
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,ID,OBJ_ID1,OBJ_TABLE");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    objId = rs.GetI("OBJ_ID1");
                    tableName = rs.GetS("OBJ_TABLE");
                }
            }
            finally
            {
                if (rs.IsOpen())
                    rs.Close();
                rs.Destroy();
            }
            if ((objId != IM.NullI) && (!string.IsNullOrEmpty(tableName)))
                retStation = GetStation(objId, tableName);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Возвращает бизнес-объект заявки
        /// </summary>
        /// <param name="objId">ID станции</param>
        /// <param name="objTable">Название таблицы</param>
        public static BaseStation GetStation(int objId, string objTable)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading the station")))
            {
                pb.SetBig("Loading the station");
                BaseStation retStation = null;
                switch (objTable)
                {
                    case PlugTbl.XfaAbonentStation:
                        {
                            int apId = IM.NullI;
                            string packetType = "";
                            //Вытаскиваем тип заявки
                            using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                            {
                                r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_TABLE");
                                r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, objTable);
                                r.SetAdditional("([OBJ_ID1]=" + objId + ") OR ( [OBJ_ID2]=" + objId + " )");
                                r.Open();
                                if (!r.IsEOF())
                                    apId = r.GetI("ID");
                            }
                            if (apId != IM.NullI)
                            {
                                using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly))
                                {
                                    r.Select("APPL_ID,Packet.TYPE");
                                    r.SetWhere("APPL_ID", IMRecordset.Operation.Eq, apId);
                                    r.Open();
                                    if (!r.IsEOF())
                                        packetType = r.GetS("Packet.TYPE");
                                }
                            }
                            //В зависимости от того какая заявка формируем соответсв. форму
                            if (packetType != AppType.AppTR_З.ToString())
                            {
                                retStation = new AbonentStation();
                                retStation.Load(objId);
                            }
                            else
                            {
                                retStation = new RailwayApplStation();
                                retStation.Load(objId);
                            }
                        }
                        break;
                }
                return retStation;
            }
        }

        #region Taxes and bandwidth
        /// <summary>
        /// Рассчитывает активную полосу пропускания для станции.
        /// </summary>
        /// <returns>Значение в мегагерцах, или IM.NullD</returns>
        public virtual double GetActiveBandwidth()
        {
            return IM.NullD;
        }
        #endregion
    }

    //==============================================================
    //==============================================================
    /// <summary>
    /// Бизнес класс для управления заявкой
    /// </summary>
    public abstract class BaseAppClass : BaseGridController, INotifyPropertyChanged
    {


        /// <summary>
        /// Класс хранения сведений о перечне различных групп совпадающих сведений
        /// </summary>
        public class ListGroup
        {
            public int Number_group;
            public List<ParamsLicense> LicCompare = new List<ParamsLicense>();
            public List<int> CompareIndex = new List<int>();
        };

        /// <summary>
        /// Класс хранения параметров лицензии
        /// </summary>
        public class ParamsLicense
        {
            public int ID;
            public int index;
            public string name_lic;
            public DateTime doc_start;
            public DateTime doc_stop;

        };


        /// <summary>
        /// Метод поиска случаев, когда у нескольких лицензий  совпадает и конечная дата и дата выдачи 
        /// </summary>

        public bool GetStatusLic(ref List<ParamsLicense> PS, DateTime start, DateTime end, string name_lic)
        {
            ParamsLicense result;
            bool Temp = false;

            result = PS.Find(delegate(ParamsLicense bk)
            {
                return ((bk.doc_start == start) && (bk.doc_stop == end) && (bk.name_lic == name_lic));

            }
            );
            if (result == null) Temp = true;
            return Temp;
        }


        public bool GetStatusIndexInt(ref List<int> CH,int i)
        {
            int fr;
             bool Temp = false;
             fr = CH.Find(delegate(int vr)
                    {
                        return ((vr==i));
                    }
                );
        if (fr == IM.NullI) Temp = true;
            return Temp;
        }

        public bool GetStatusString(ref List<DateTime> CH, DateTime str)
        {
            DateTime fr;
            bool Temp = false;
            fr = CH.Find(delegate(DateTime vr)
            {
                return (vr == str);
            }
               );
            if (fr == IM.NullT) Temp = true;
            return Temp;
        }

        public bool GetStatusString(ref List<string> CH, string str)
        {
            string fr;
            bool Temp = false;
            fr = CH.Find(delegate(string vr)
            {
                return (vr == str);
            }
               );
            if (fr == null) Temp = true;
            return Temp;
        }

        public bool GetStatusInt(ref List<int> CH, int num)
        {
            
            bool Temp = true;
            for (int i = 0; i < CH.Count; i++)
            {
                if (CH[i] == num)
                {
                    Temp = false;
                    break;
                }
            }

            return Temp;
        }


        /// <summary>
        /// Метод поиска случаев, когда у нескольких лицензий конечная дата совпадает, а дата выдачи отличается
        /// </summary>
        public void GetStatusLic1(ref List<ListGroup> LGP, ref List<ParamsLicense> PS, ref List<int> OUT_INDEX)
        {
            try
            {
                if (PS.Count > 1)
                {
                    List<DateTime> OUT_INDEX_OBJECT = new List<DateTime>();

                    for (int i = 0; i <= PS.Count - 2; i++)
                    {
                        for (int j = i + 1; j <= PS.Count - 1; j++)
                        {
                            if ((PS[i].doc_start != PS[j].doc_start) && (PS[i].doc_stop == PS[j].doc_stop))
                            {
                                OUT_INDEX.Add(i);
                                OUT_INDEX.Add(j);

                            }
                        }
                    }


                    bool status_x = false;
                    for (int i = 0; i <= OUT_INDEX.Count - 2; i++)
                    {
                        for (int j = i + 1; j <= OUT_INDEX.Count - 1; j++)
                        {
                            if (PS[OUT_INDEX[i]].doc_stop != PS[OUT_INDEX[j]].doc_stop)
                            {
                               // if (GetStatusString(ref OUT_INDEX_OBJECT, PS[OUT_INDEX[i]].doc_stop))
                               // {
                                    OUT_INDEX_OBJECT.Add(PS[OUT_INDEX[i]].doc_stop);
                                    OUT_INDEX_OBJECT.Add(PS[OUT_INDEX[j]].doc_stop);
                                    status_x = true;
                              //  }
                            }
                        }
                    }
                    if ((!status_x) && (OUT_INDEX.Count > 0))
                    {
                        OUT_INDEX_OBJECT.Add(PS[OUT_INDEX[0]].doc_stop);
                    }

                    for (int i = 0; i < OUT_INDEX_OBJECT.Count; i++)
                    {
                        ListGroup cx = new ListGroup();
                        cx.CompareIndex = new List<int>();
                        cx.Number_group = i;

                        for (int j = 0; j < OUT_INDEX.Count; j++)
                        {
                            if (PS[OUT_INDEX[j]].doc_stop == OUT_INDEX_OBJECT[i])
                            {

                                if (GetStatusInt(ref cx.CompareIndex, OUT_INDEX[j]))
                                {
                                    cx.CompareIndex.Add(OUT_INDEX[j]);
                                }
                             

                            }

                        }


                        LGP.Add(cx);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in method GetStatusLic1 "+ex.Message);
            }
        }


         /// <summary>
        /// Метод поиска значений индексов массива, которые не удовлетворяют условиям 1 и 2 
        /// </summary>

        public void GetFreeIndex(ref List<ListGroup> LGP1, ref  List<ListGroup> LGP2, ref List<ParamsLicense> PS1, ref List<ParamsLicense> PS2, ref List<int> OUT_INDEX1, ref List<int> OUT_INDEX2,ref List<int> OUT_INDEX3)
        {
            try
            {
                List<DateTime> OUT_INDEX_OBJECT = new List<DateTime>();
                bool FDT;



                for (int jj = 0; jj < PS1.Count; jj++)
                {
                    FDT = false;
                    for (int kk = 0; kk < LGP1.Count; kk++)
                    {

                        for (int rr = 0; rr < LGP1[kk].CompareIndex.Count; rr++)
                        {
                            if (LGP1[kk].CompareIndex[rr] == jj)
                            {
                                FDT = true;
                            }
                        }

                    }


                    for (int kk = 0; kk < LGP2.Count; kk++)
                    {

                        for (int rr = 0; rr < LGP2[kk].CompareIndex.Count; rr++)
                        {
                            if (LGP2[kk].CompareIndex[rr] == jj)
                            {
                                FDT = true;
                            }
                        }
                    }

                    if (!FDT)
                    {
                        OUT_INDEX3.Add(jj);
                    }
                }




                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in method GetFreeIndex "+ex.Message);
            }
        }
      
        /// <summary>
        /// Метод поиска случаев, когда у нескольких лицензий  совпадает и конечная дата и дата выдачи 
        /// </summary>

        public void GetStatusLic2(ref List<ListGroup> LGP2, ref List<ParamsLicense> PS2, ref List<int> OUT_INDEX2)
        {
            try
            {
            
                if (PS2.Count > 1)
                {
                    List<DateTime> OUT_INDEX_OBJECT = new List<DateTime>();
                    


                    for (int i = 0; i <= PS2.Count - 2; i++)
                    {
                        for (int j = i + 1; j <= PS2.Count - 1; j++)
                        {

                            if ((DateTime.Compare(PS2[i].doc_start, PS2[j].doc_start) == 0) && (DateTime.Compare(PS2[i].doc_stop, PS2[j].doc_stop) == 0))
                            {
                                OUT_INDEX2.Add(i);
                                OUT_INDEX2.Add(j);

                            }
                        }
                    }


                    bool status_x = false;
                    for (int i = 0; i <= OUT_INDEX2.Count - 2; i++)
                    {
                        for (int j = i + 1; j <= OUT_INDEX2.Count - 1; j++)
                        {
                            if ((PS2[OUT_INDEX2[i]].doc_stop != PS2[OUT_INDEX2[j]].doc_stop))
                            {
                                
                                    OUT_INDEX_OBJECT.Add(PS2[OUT_INDEX2[i]].doc_stop);
                                    OUT_INDEX_OBJECT.Add(PS2[OUT_INDEX2[j]].doc_stop);
                                    status_x = true;
                                

                            }

                        }
                    }

                    if ((!status_x) && (OUT_INDEX2.Count > 0))
                    {
                        OUT_INDEX_OBJECT.Add(PS2[OUT_INDEX2[0]].doc_stop);
                    }

                    for (int i = 0; i < OUT_INDEX_OBJECT.Count; i++)
                    {
                        ListGroup cx = new ListGroup();
                        cx.CompareIndex = new List<int>();
                        cx.Number_group = i;

                        for (int j = 0; j < OUT_INDEX2.Count; j++)
                        {
                            if (PS2[OUT_INDEX2[j]].doc_stop == OUT_INDEX_OBJECT[i])
                            {

                               if (GetStatusInt(ref cx.CompareIndex, OUT_INDEX2[j]))
                                {
                                    cx.CompareIndex.Add(OUT_INDEX2[j]);
                                }

                            }

                        }


                        LGP2.Add(cx);
                    }
                }
                   }
            catch (Exception ex)
            {
                MessageBox.Show("Error in method GetStatusLic2 "+ex.Message);
            }
        }


        private List<ListGroup> GroupLic1 = new List<ListGroup>();
        private List<ListGroup> GroupLic2 = new List<ListGroup>();
        public List<ParamsLicense> LicCompare1 = new List<ParamsLicense>();
        public List<ParamsLicense> LicCompare2 = new List<ParamsLicense>();
        public List<ParamsLicense> FinalCompare = new List<ParamsLicense>();

        private List<int> CompareIndex1 = new List<int>();
        private List<int> CompareIndex2 = new List<int>();
        private List<int> CompareIndex3 = new List<int>();
        private List<int> CompareIndex4 = new List<int>();


        protected BaseStation _stationObject;
        public BaseStation Station { get { return _stationObject; } }
        public IWin32Window OwnerWindows { get; set; }

        protected static Dictionary<string, string> posFieldNames = new Dictionary<string, string>();
        protected static string[] posTables = { "POSITION_WIM", "POSITION_BRO", "POSITION_MW", "POSITION_MOB2", "POSITION_ES" };
        public static StatusType CurrStatus;
        
        //===========================================================
        //===========================================================
        //===========================================================
        //===========================================================
        /// <summary>
        /// Возвращает радиотехнологию заявки 
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Радиотехнология заявки</returns>
        public static string GetStandardFromAppl(int applId)
        {
            string retStandard = "";
            int objId = IM.NullI;
            string tableName = "";
            {
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID,OBJ_ID1,OBJ_TABLE");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                try
                {
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        objId = rs.GetI("OBJ_ID1");
                        tableName = rs.GetS("OBJ_TABLE");
                    }
                }
                finally
                {
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }
            }
            if (string.IsNullOrEmpty(tableName) == false)
            {
                try
                {
                    IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,STANDARD");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, objId);
                    try
                    {
                        rs.Open();
                        if (!rs.IsEOF())
                        {
                            retStandard = rs.GetS("STANDARD");
                        }
                    }
                    finally
                    {
                        if (rs.IsOpen())
                            rs.Close();
                        rs.Destroy();
                    }
                }
                catch(Exception ex)
                {
                    CLogs.WriteError(ELogsWhat.Appl, ex);
                }
            }
            return retStandard;
        }
        /// <summary>
        /// Создает форму для ввода заявки
        /// </summary>
        /// <param name="_appType">Тип заявки</param>
        /// <param name="_packetId">ID пакета</param>
        /// <param name="_ownerId">ID контрагента</param>
        /// <returns>фому для ввода заявки</returns>
        public static MainAppForm CreateAppl(AppType _appType, int _packetId, int _ownerId, string _radioTech)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Creating a new application")))
            {
                pb.SetBig(_radioTech);
                BaseAppClass appl = null;
                CurrStatus = new StatusType();
                switch (_appType)
                {
                    case AppType.AppZS:
                        appl = new EarthAppSite(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppBS:
                        appl = new MobSta2App(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppRS:
                        appl = new MicrowaveApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppRR:
                        appl = new MobStaApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppTR:
                        appl = new MobStaTechnologicalApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppTR_З:
                        appl = new RailwayAppl(0, _ownerId, _packetId, _radioTech);
                        break;                        
                    case AppType.AppTV2:
                        appl = new CTvAnalog(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppR2:
                        appl = new FmAnalogApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppTV2d:
                        appl = new CTvDigital(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppR2d:
                        appl = new FmDigitalApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppAR_3:
                        appl = new AbonentAppl(0, _ownerId, _packetId, _radioTech, AppType.AppAR_3);
                        break;
                    case AppType.AppAP3:
                        appl = new AbonentAppl(0, _ownerId, _packetId, _radioTech, AppType.AppAP3);
                        break;
                    case AppType.AppZRS:
                        appl = new ShipApp(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppVP:
                        appl = new EmitterAppl(0, _ownerId, _packetId, _radioTech);
                        break;
                    case AppType.AppA3:
                        appl = new AmateurAppl(0, _ownerId, _packetId, _radioTech, CurrStatus);
                        break;
                }
                MainAppForm newForm = new MainAppForm(appl);
                return newForm;
            }
        }
        //===========================================================
        /// <summary>
        /// Создает форму для ввода заявки
        /// </summary>
        /// <returns>фому с заявкой</returns>

        public static MainAppForm ShowAppl(int appId)
        {
            return ShowAppl(appId,"");
        }
        public static MainAppForm ShowAppl(int appId, string tableName)
        {
            BaseAppClass appl = GetAppl(appId, false);
            if (appl != null)
            {
                if (!string.IsNullOrEmpty(tableName))
                    appl.SetRight(tableName);
            }
            else
            {
                MessageBox.Show(CLocaliz.TxT("Can't find the station"), CLocaliz.TxT("Error"));
                return null;
            }
            return new MainAppForm(appl);
        }
        //===========================================================
        /// <summary>
        /// Создает базовый обьект заявки
        /// </summary>
        /// <param name="appId">ID заявки</param>
        /// <returns>базовый обьект заявки</returns>
        public static BaseAppClass GetBaseAppl(int appId)
        {
            return GetAppl(appId, true);
        }
        //===========================================================
        /// <summary>
        /// Создает обьект заявки
        /// </summary>
        /// <param name="appId">ID заявки</param>
        /// <param name="isBaseAppl">TRUE - необходимо вернуть базовый класс заявки, иначе FALSE</param>
        /// <returns>фому с заявкой</returns>       
        public static BaseAppClass GetAppl(int appId, bool isBaseAppl)
        {
            BaseAppClass appl = null;
            int objId = IM.NullI;
            int packetId = IM.NullI;
            string tableName = "";

            YXnrfaAppl appl_ = new YXnrfaAppl();
            appl_.Table = "XNRFA_APPL";
            appl_.Format("ID,OBJ_ID1,OBJ_TABLE,STANDARD");
            if (appl_.Fetch(appId))
            {
                objId = appl_.m_obj_id1;
                tableName = appl_.m_obj_table;
                standart = appl_.m_standard;
            }
            appl_.Close();
            appl_.Dispose();

            /*
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,OBJ_ID1,OBJ_TABLE,STANDARD");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    objId = rs.GetI("OBJ_ID1");
                    tableName = rs.GetS("OBJ_TABLE");
                    standart = rs.GetS("STANDARD");
                }
                rs.Close();
            }
            finally
            {
                rs.Final();
            }
             */ 
            // Get packet Id

            IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
            rs.Select("Application.ID,Packet.ID");
            rs.SetWhere("Application.ID", IMRecordset.Operation.Eq, appId);
            rs.OrderBy("Packet.ID", OrderDirection.Descending);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                    packetId = rs.GetI("Packet.ID");
                rs.Close();
            }
            finally
            {
                rs.Final();
            }
            if ((objId != IM.NullI) && (!string.IsNullOrEmpty(tableName)))
                appl = GetStation(objId, tableName, packetId, isBaseAppl);
            return appl;
        }
        /// <summary>
        /// Возвращает индекс класса, который необходимо создать
        /// </summary>
        /// <param name="isBaseAppl">TRUE - только базовый класс</param>
        /// <param name="objId">ID станции</param>
        /// <param name="diffTableName">Нозвание таблицы DIFF</param>
        /// <param name="tableNameStation">Нозвание таблицы станции</param>
        /// <returns></returns>
        private static int GetIndexBaseAppl(bool isBaseAppl, int objId, string diffTableName, string tableNameStation)
        {
            UcrfDepartment curDept = CUsers.GetCurDepartment();
            bool isUrcp = (curDept == UcrfDepartment.URCP);
            // функция вызывается только для Централизованных РЧП, посему предполагаем, что филиал == УРЗП
            bool isUrzp = ((curDept == UcrfDepartment.URZP) || (curDept == UcrfDepartment.Branch));
            if (isBaseAppl)
                return 0;   //Базовый класс
            if (isUrzp)
                return 2; //Diff класс
            if (isUrcp && IsPresent(objId, diffTableName, tableNameStation))
                return 1;   //Commit класс
            return 0;       //Базовый класс
        }
        //===========================================================
        /// <summary>
        /// Возвращает бизнес-объект заявки
        /// </summary>
        /// <param name="isBaseAppl">TRUE - необходимо вернуть базовый класс заявки, иначе FALSE</param>        
        public static BaseAppClass GetStation(int _ObjId, string _ObjTable, int _packetID, bool isBaseAppl)
        {
            //using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading the application")))
            {
                //pb.SetBig("Loading the application");
                if (_packetID == IM.NullI)
                    _packetID = 0;

                BaseAppClass obj = null;

                if (_ObjTable == EarthApp.TableName)
                {
                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDeiffEtSta, _ObjTable))
                    {
                        case 1:
                            obj = new EarthAppCommit(_ObjId, 0, _packetID, CRadioTech.ZS);
                            break;
                        case 2:
                            obj = new EarthAppDiff(_ObjId, 0, _packetID, CRadioTech.ZS);
                            break;
                        default:
                            obj = new EarthAppSite(_ObjId, 0, _packetID, CRadioTech.ZS);
                            break;
                    }
                }
                else if (_ObjTable == MobSta2App.TableName)
                {

                    string radioTech = GetRadioTech(_ObjTable, _ObjId, CRadioTech.UNKNOWN);

                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDiffMobSta2, _ObjTable))
                    {
                        case 1://Commit
                            obj = new MobStation2AppCommit(_ObjId, 0, _packetID, radioTech);
                            break;
                        case 2://Diff
                            obj = new MobStation2AppDiff(_ObjId, 0, _packetID, radioTech);
                            break;
                        default://Base
                            obj = new MobSta2App(_ObjId, 0, _packetID, radioTech);
                            break;
                    }
                }
                else if (_ObjTable == AbonentStation.TableName)
                {
                    string radTech = CRadioTech.KX;
                    // Вытаскиваем стандарт
                    IMRecordset rt = new IMRecordset(_ObjTable, IMRecordset.Mode.ReadOnly);
                    rt.Select("STANDARD");
                    rt.SetWhere("ID", IMRecordset.Operation.Eq, _ObjId);
                    try
                    {
                        rt.Open();
                        if (!rt.IsEOF())
                        {
                            radTech = rt.GetS("STANDARD");
                        }
                    }
                    finally
                    {
                        rt.Final();
                    }
                    int apId = IM.NullI;                   
                    string packetType = "";
                    //Вытаскиваем тип заявки                    
                    IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_TABLE,TABLE_NAME,Railway1.ID,Railway1.TABLE_NAME,Railway2.ID,Railway2.TABLE_NAME,APPL_TYPE");
                    r.SetWhere("OBJ_TABLE",IMRecordset.Operation.Eq,_ObjTable);
                    r.SetAdditional("([OBJ_ID1]=" + _ObjId + ") OR ( [OBJ_ID2]=" + _ObjId + " )");
                    r.Open();
                    if (!r.IsEOF())
                    {
                        apId = r.GetI("ID");
                        packetType = r.GetS("APPL_TYPE");                       
                    }
                    r.Final();
                    if (apId != IM.NullI)
                    {
                        r = new IMRecordset("XNRFA_PAC_TO_APPL", IMRecordset.Mode.ReadOnly);
                        r.Select("APPL_ID,PACKET_ID,Packet.ID,Packet.TYPE");
                        r.SetWhere("APPL_ID", IMRecordset.Operation.Eq, apId);
                        r.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, _packetID);
                        r.Open();
                        if (!r.IsEOF())
                        {
                            if (string.IsNullOrEmpty(packetType))
                                packetType = r.GetS("Packet.TYPE");
                        }
                        r.Final();
                    }
                    //В зависимости от того какая заявка формируем соответсв. форму
                    if (packetType.Trim() == "") packetType = AppType.AppUnknown.ToString();
                    if (!(packetType==AppType.AppTR_З.ToString()))
                    {
                        string radioTech = GetRadioTech(_ObjTable, _ObjId, radTech);
                        if (packetType == AppType.AppAP3.ToString())
                            obj = new AbonentAppl(_ObjId, 0, _packetID, radioTech, AppType.AppAP3);
                        else obj = new AbonentApplExt(_ObjId, 0, _packetID, radioTech, (AppType)Enum.Parse(typeof(AppType), packetType));
                    }
                    else
                    {
                        string radioTech = GetRadioTech(_ObjTable, _ObjId, radTech);
                        obj = new RailwayAppl(_ObjId, 0, _packetID, radioTech);
                    }
                }
                else if (_ObjTable == MicrowaveApp.TableName)
                {
                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDeiffMwSta, _ObjTable))
                    {
                        case 1://Commit
                            obj = new MicrowaveAppCommit(_ObjId, 0, _packetID, CRadioTech.RS);
                            break;
                        case 2://Diff
                            obj = new MicrowaveAppDiff(_ObjId, 0, _packetID, CRadioTech.RS);
                            break;
                        default://Base
                            obj = new MicrowaveApp(_ObjId, 0, _packetID, CRadioTech.RS);
                            break;
                    }
                }
                else if (_ObjTable == CTvAnalog.TableName)
                {
                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDeiffTvSta, _ObjTable))
                    {
                        case 1://Commit
                            obj = new CTvAnalogAppCommit(_ObjId, 0, _packetID, CRadioTech.ATM);
                            break;
                        case 2://Diff
                            obj = new CTvAnalogAppDiff(_ObjId, 0, _packetID, CRadioTech.ATM);
                            break;
                        default://Base
                            obj = new CTvAnalog(_ObjId, 0, _packetID, CRadioTech.ATM);
                            break;
                    }
                }
                else if (_ObjTable == CTvDigital.TableName)
                {
                    string radioTech = GetRadioTech(_ObjTable, _ObjId, CRadioTech.DVBT);

                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDeiffDvbSta, _ObjTable))
                    {
                        case 1://Commit
                            obj = new TvDigitallCommit(_ObjId, 0, _packetID, radioTech);
                            break;
                        case 2://Diff
                            obj = new TvDigitallDiff(_ObjId, 0, _packetID, radioTech);
                            break;
                        default://Base
                            obj = new CTvDigital(_ObjId, 0, _packetID, radioTech);
                            break;
                    }
                }
                else if (_ObjTable == FmAnalogApp.TableName)
                {
                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDeiffFmSta, _ObjTable))
                    {
                        case 1://Commit
                            obj = new FmAnalogAppCommit(_ObjId, 0, _packetID, CRadioTech.AAB);
                            break;
                        case 2://Diff
                            obj = new FmAnalogAppDiff(_ObjId, 0, _packetID, CRadioTech.AAB);
                            break;
                        default://Base
                            obj = new FmAnalogApp(_ObjId, 0, _packetID, CRadioTech.AAB);
                            break;
                    }
                }
                else if (_ObjTable == FmDigitalApp.TableName)
                {
                    switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDiffFmDigital, _ObjTable))
                    {
                        case 1://Commit
                            obj = new FmDigitallCommit(_ObjId, 0, _packetID, CRadioTech.TDAB);
                            break;
                        case 2://Diff
                            obj = new FmDigitallDiff(_ObjId, 0, _packetID, CRadioTech.TDAB);
                            break;
                        default://Base
                            obj = new FmDigitalApp(_ObjId, 0, _packetID, CRadioTech.TDAB);
                            break;
                    }
                }
                else if (_ObjTable == MobStaApp.TableName)
                {
                    string radioTech = CRadioTech.UNKNOWN;
                    // Вытаскиваем стандарт
                    IMRecordset r = IMRecordset.ForRead(new RecordPtr(_ObjTable, _ObjId), "STANDARD");
                    try
                    {
                        // IM will throw an Exception, if necessary
                        radioTech = r.GetS("STANDARD");
                    }
                    finally
                    {
                        r.Destroy();
                    }
                    
                    if (CRadioTech.getListRadioTech(AppType.AppRR).Contains(radioTech))
                    {
                        switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDiffMobSta, _ObjTable))
                        {
                            case 1: //Commit
                                obj = new MobStationAppCommit(_ObjId, 0, _packetID, radioTech);
                                break;
                            case 2: //Diff
                                obj = new MobStationAppDiff(_ObjId, 0, _packetID, radioTech);
                                break;
                            default: //Base
                                obj = new MobStaApp(_ObjId, 0, _packetID, radioTech);
                                break;
                        }
                    }
                    if (obj == null)
                    {
                        if (CRadioTech.getListRadioTech(AppType.AppTR).Contains(radioTech))
                        {
                                switch (GetIndexBaseAppl(isBaseAppl, _ObjId, PlugTbl.itblXnrfaDiffMobSta, _ObjTable))
                                {
                                    case 1: //Commit
                                        obj = new MobStaTechAppCommit(_ObjId, 0, _packetID, radioTech);
                                        break;
                                    case 2: //Diff
                                        obj = new MobStaTechAppDiff(_ObjId, 0, _packetID, radioTech);
                                        break;
                                    default: //Base
                                        obj = new MobStaTechnologicalApp(_ObjId, 0, _packetID, radioTech);
                                        break;
                                }
                            
                        }
                        
                    }
                }
                else if (_ObjTable == ICSMTbl.itblMicrows)
                {
                    int newID = IM.NullI;
                    // Вытащим ID MICROWA
                    IMRecordset r = new IMRecordset(_ObjTable, IMRecordset.Mode.ReadOnly);
                    r.Select("MW_ID");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _ObjId);
                    try
                    {
                        r.Open();
                        if (!r.IsEOF())
                            newID = r.GetI("MW_ID");
                    }
                    finally
                    {
                        r.Close();
                        r.Destroy();
                    }
                    if (newID != IM.NullI)
                        obj = GetStation(newID, ICSMTbl.itblMicrowa, _packetID, isBaseAppl);
                }
                else if (_ObjTable == ShipApp.TableName)
                {
                    obj = new ShipApp(_ObjId, 0, _packetID, CRadioTech.MMS);
                }
                else if (_ObjTable == EmitterAppl.TableName)
                {
                    obj = new EmitterAppl(_ObjId, 0, _packetID, CRadioTech.VP);
                }
                else if (_ObjTable == AmateurAppl.TableName)
                {
                    obj = new AmateurAppl(_ObjId, 0, _packetID, CRadioTech.AP);
                }

                return obj;
            }
        }

        

        protected static int GetCellIndexFromKey(string cellKey)
        {
            if (string.IsNullOrEmpty(cellKey))
            {
                throw new ArgumentException(cellKey);
            }
            string[] parts = cellKey.Split('_');
            if (parts.Length <= 1)
            {
                throw new IllegalCellIndexException(cellKey);
            }
            int index;
            if(!int.TryParse(parts[parts.Length - 1], out index))
            {
                throw new IllegalCellIndexException(cellKey);
            }
            return index;
        }

        public virtual int GetTxCount()
        {
            return 1;
        }

        /// <summary>
        /// Збереження у БД значення Perm_num
        /// </summary>
        /// <param name="numb"></param>
        /// <param name="tableName"></param>
        /// <param name="id"></param>
        public void SaveFileNumber(string numb, string tableName, int id)
        {
            IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,PERM_NUM");
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("PERM_NUM", numb);
                r.Update();
            }
            r.Final();
        }


        /// <summary>
        /// Возвращает название радиотехнологии
        /// </summary>
        /// <param name="_ObjTable">Таблица поиска</param>
        /// <param name="_ObjId">запись в таблице поиска</param>
        /// <param name="defaultName">значение радиотезнологии по умолчанию</param>
        /// <returns></returns>
        private static string GetRadioTech(string _ObjTable, int _ObjId, string defaultName)
        {
            string radioTech = defaultName;
            // Вытаскиваем стандарт
            IMRecordset r = new IMRecordset(_ObjTable, IMRecordset.Mode.ReadOnly);
            r.Select("STANDARD");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _ObjId);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    radioTech = r.GetS("STANDARD");
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            return radioTech;
        }

        /// <summary>
        /// Возвращает ID заявки
        /// </summary>
        /// <param name="stationId">ID станции</param>
        /// <param name="tableName">Название таблицы станции</param>
        /// <returns>Возвращает ID заявки или IM.NULLI если завка не найдена</returns>
        public static int GetApplId(int stationId, string tableName)
        {
            int retVal = IM.NullI;
            string strId = stationId.ToString();
            // Ищем пакет, с которым связана заявка
            IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
            rs.Select("Application.ID");
            rs.SetWhere("Application.OBJ_TABLE", IMRecordset.Operation.Like, tableName);
            rs.SetAdditional("([Application.OBJ_ID1]=" + strId + " OR [Application.OBJ_ID2]=" + strId +
                             " OR [Application.OBJ_ID3]=" + strId + " OR [Application.OBJ_ID4]=" + strId +
                             " OR [Application.OBJ_ID5]=" + strId + " OR [Application.OBJ_ID6]=" + strId + ")");
            rs.OrderBy("CREATED_DATE", OrderDirection.Descending);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                    retVal = rs.GetI("Application.ID");
            }
            finally
            {
                if (rs.IsOpen())
                    rs.Close();
                rs.Destroy();
            }
            return retVal;
        }
        //============================================================
        /// <summary>      
        /// Upadtes the station's status
        /// </summary>
        //============================================================
        public static void UpdateStatus(StatusType newStatus, int idAppl)
        {
            UpdateStatus(newStatus, idAppl, CUsers.GetCurUserID());
        }
        //============================================================
        /// <summary>
        /// Обновить список докментов 
        /// </summary>
        public static void UpdateListDoc()
        {

        }
        /// <summary>      
        /// Upadtes the station's status
        /// </summary>
        //============================================================
        public static void UpdateStatus(StatusType newStatus, int idAppl, int emplId)
        {
            string tableName = "";
            List<int> lstId = new List<int>();
            {// Вытаскиваем ID станций
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID");
                rs.Select("OBJ_ID1");
                rs.Select("OBJ_ID2");
                rs.Select("OBJ_ID3");
                rs.Select("OBJ_ID4");
                rs.Select("OBJ_ID5");
                rs.Select("OBJ_ID6");
                rs.Select("OBJ_TABLE");
                rs.Select("COMMENTS");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
                rs.Open();
                if (!rs.IsEOF())
                {
                    tableName = rs.GetS("OBJ_TABLE");
                    lstId.Add(rs.GetI("OBJ_ID1"));
                    lstId.Add(rs.GetI("OBJ_ID2"));
                    lstId.Add(rs.GetI("OBJ_ID3"));
                    lstId.Add(rs.GetI("OBJ_ID4"));
                    lstId.Add(rs.GetI("OBJ_ID5"));
                    lstId.Add(rs.GetI("OBJ_ID6"));

                }
                rs.Close();
                rs.Destroy();
            }
            if (string.IsNullOrEmpty(tableName) == false)
            {// Обновляем поля в станциях
                StatusType curStatus = new StatusType();
                foreach (int item in lstId)
                {
                    IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,STATUS");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, item);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        curStatus = rs.GetS("STATUS").ToStatusType();
                        if (curStatus != newStatus)
                        {
                            rs.Edit();
                            rs.Put("STATUS", newStatus.ToStr());
                            rs.Update();
                        }
                    }
                    rs.Close();
                    rs.Destroy();
                }
                if (!curStatus.Equals(newStatus))
                {
                    string str = string.Format("{0} -> {1}", curStatus.ToStr(), newStatus.ToStr());
                    CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, idAppl, EDocEvent.evChangedState, DateTime.Now, str, IM.NullT, "", emplId);
                }
            }
        }
        //============================================================
        /// <summary>      
        /// Reads the station's status
        /// </summary>
        //============================================================
        public static StatusType ReadStatus(int idAppl)
        {
            StatusType retVal = new StatusType();
            string tableName = "";
            List<int> lstId = new List<int>();
            {// Вытаскиваем ID станций
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID");
                rs.Select("OBJ_ID1");
                rs.Select("OBJ_ID2");
                rs.Select("OBJ_ID3");
                rs.Select("OBJ_ID4");
                rs.Select("OBJ_ID5");
                rs.Select("OBJ_ID6");
                rs.Select("OBJ_TABLE");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
                rs.Open();
                if (!rs.IsEOF())
                {
                    tableName = rs.GetS("OBJ_TABLE");
                    lstId.Add(rs.GetI("OBJ_ID1"));
                    lstId.Add(rs.GetI("OBJ_ID2"));
                    lstId.Add(rs.GetI("OBJ_ID3"));
                    lstId.Add(rs.GetI("OBJ_ID4"));
                    lstId.Add(rs.GetI("OBJ_ID5"));
                    lstId.Add(rs.GetI("OBJ_ID6"));
                }
                rs.Close();
                rs.Destroy();
            }
            if (string.IsNullOrEmpty(tableName) == false)
            {// Обновляем поля в станциях
                foreach (int item in lstId)
                {
                    StatusType curStatus = new StatusType();
                    IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                    rs.Select("STATUS");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, item);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        curStatus = rs.GetS("STATUS").ToStatusType();
                    }
                    rs.Close();
                    rs.Destroy();

                    if (!curStatus.IsEmpty())
                    {
                        retVal = curStatus;
                        break;
                    }
                }
            }
            return retVal;
        }
        //===========================================================
        public int GetID()
        {
            if (objStation != null)
                return objStation.GetI("ID");
            return IM.NullI;
        }

        public static void FillColorComboBox(CellStringBrowsableProperty stringProp)
        {
            if (stringProp.Items.Count == 2)
            {
                if (stringProp.Items[0] != stringProp.Items[1])
                {
                    if (string.IsNullOrEmpty(stringProp.Items[0]) || string.IsNullOrEmpty(stringProp.Items[1]))
                    {
                        stringProp.BackColor = CellStringProperty.ColorBgr(2);
                        return;
                    }
                    stringProp.BackColor = CellStringProperty.ColorBgr(0);
                    return;
                }

            }
            else if (stringProp.Items.Count == 1)
            {
                stringProp.BackColor = CellStringProperty.ColorBgr(2);
            }
        }

        public static void FillDiffComboBox(CellStringComboboxAplyProperty stringProp, string station, string diffstation)
        {
            if (!string.IsNullOrEmpty(station))
                if (!string.IsNullOrEmpty(diffstation))
                {
                    if (station != diffstation)
                    {
                        stringProp.Items.Add(diffstation);
                        stringProp.BackColor = CellStringComboboxAplyProperty.ColorBgr(0);
                    }
                    else
                    {
                        stringProp.Value = station;
                        stringProp.Cell.cellStyle = EditStyle.esSimple;
                        stringProp.BackColor = CellStringComboboxAplyProperty.ColorBgr(4);
                    }
                }
                else
                {
                    stringProp.Cell.cellStyle = EditStyle.esSimple;
                    stringProp.Value = station;
                    stringProp.BackColor = CellStringComboboxAplyProperty.ColorBgr(4);
                }
            else
            {
                if (!string.IsNullOrEmpty(diffstation))
                {
                    stringProp.Value = diffstation;
                    stringProp.Items.Add(diffstation);
                    stringProp.BackColor = CellStringComboboxAplyProperty.ColorBgr(2);
                    stringProp.Cell.cellStyle = EditStyle.esSimple;
                }
                else
                {
                    stringProp.BackColor = CellStringComboboxAplyProperty.ColorBgr(4);
                    stringProp.Cell.cellStyle = EditStyle.esSimple;
                }
            }
        }

        public static void FillColorComboBox(CellStringComboboxAplyProperty stringProp)
        {
            if (stringProp.Items.Count == 2)
            {
                if (stringProp.Items[0] != stringProp.Items[1])
                {
                    if (string.IsNullOrEmpty(stringProp.Items[0]) || string.IsNullOrEmpty(stringProp.Items[1]))
                    {
                        stringProp.BackColor = CellStringProperty.ColorBgr(2);
                        return;
                    }
                    stringProp.BackColor = CellStringProperty.ColorBgr(0);
                    return;
                }
            }
            else if (stringProp.Items.Count == 1)
            {
                stringProp.BackColor = CellStringProperty.ColorBgr(2);
            }
        }

        //===========================================================
        //===========================================================
        //===========================================================
        //===========================================================
        //===========================================================

        protected DataGridView gridLicence = null;  //для заполнения лицензий
        protected DataGridView gridEventLog = null;  //для заполнения событий
        protected DataGridView gridChangeLog = null;  //для заполнения изменений
        protected RecordPtr recordID;   //Идентификатор записи
        public IMObject objStation;  //Станция
        public List<int> licenceListId = new List<int>(); //Список лицензий
        protected List<RecordPtr> DeletedIds = new List<RecordPtr>(); //Список секторов для удаления
        public Dictionary<string, int> arts = new Dictionary<string, int>();//Список статей
        protected int packetID = 0;     //ID пакета
        protected int applID = 0;       //ID заявки
        protected string NumBlank { get; set; }


        public int ApplID
        {
            get { return applID; }
            protected set
            {
                if (applID != value)
                    applID = value;
            }
        }
        public virtual int OwnerID
        {
            get
            {
                int retVal = IM.NullI;
                if (objStation != null)
                {
                    try { retVal = objStation.GetI("OWNER_ID"); }
                    catch { }
                }
                return retVal;
            }
        }

        public int PacketID
        {
            get { return packetID; }
        }

        protected bool newStation = false;// Создали новую станцию

        private bool isChangeDop = false; //Были изменены доп параметры

        private static string standart = "";

        public bool IsChangeDop { get { return isChangeDop; } set { isChangeDop = value; } }
        public string SCVisnovok = "";  //Особливі умови висновка
        public string SCDozvil { get; set; }    //Особливі умови дозвола
        public string SCNote = "";      //Примітки
        public string radioTech = "";   //радиотехнология (Стандарт)
        public AppType appTp;


        public string MMSI = "";      //Ідентифікатор МПС
        public string Oznach_Rez = "";      //Означення РЕЗ

        public DepartmentType department = DepartmentType.Unknown;   //Департамент
        public DepartmentSectorType departmentSector = DepartmentSectorType.VFSR_SRZ;  //Сектор в департаменте
        public ManagemenUDCR managementUDCR = ManagemenUDCR.URCP;
        protected string NumberIn = "";
        protected string NumberOut = "";
        protected DateTime DateOut = DateTime.Now;
        protected DateTime DateIn = DateTime.Now;
        private int _isTechnicalUser = 0;
        public int IsTechnicalUser
        {
            get { return _isTechnicalUser; }
            set
            {
                if (_isTechnicalUser != value)
                {
                    _isTechnicalUser = value;
                    IsTechUser = !IsTechUser;
                    IsNotTechUser = !IsNotTechUser;
                    IsChangeDop = true;
                }
            }
        }
        /// <summary>
        /// (Binding) Технологический пользователь
        /// </summary>
        public bool IsTechUser
        {
            get { return (IsTechnicalUser > 0); }
            set
            {
                NotifyPropertyChanged("IsTechUser");
            }
        }

        private string comment; //Коментарий
        public string Comment
        {
            get { return comment; }
            set
            {
                if (value != comment)
                {
                    comment = value;
                    NotifyPropertyChanged("Comment");
                }
            }
        }

        /// <summary>
        /// (Binding) Не технологический пользователь
        /// </summary>
        public bool IsNotTechUser
        {
            get { return !IsTechUser; }
            set
            {

                NotifyPropertyChanged("IsNotTechUser");
            }
        }

        
        public bool ReadOnly { get; set; }
        public string objTableName { get { return recordID.Table; } }
        public string Standard { get { return radioTech; } }


        public int recID { get { return recordID.Id; } }
        public Grid gridParam;
        public int priceId = IM.NullI;
        public string priceArticle = "";
        //-----
        public AppType appType = AppType.AppUnknown;      //тип заявки
        public AppType ApplType { get { return appType; } }

        //-----
        //public string ForeighnStatus { get; set; }
        private StatusType _status;
        public StatusType Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    NotifyPropertyChanged("Status");
                }
            }
        }

        public EPacketType PacketType
        {
            get
            {
                EPacketType retVal = EPacketType.PckUnknown;
                IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsPacket.Select("ID,CONTENT");
                    rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packetID);
                    rsPacket.Open();
                    if (!rsPacket.IsEOF())
                    {// Пакет существует
                        try { retVal = (EPacketType)Enum.Parse(typeof(EPacketType), rsPacket.GetS("CONTENT")); }
                        catch { retVal = EPacketType.PckUnknown; };
                    }
                }
                finally
                {
                    rsPacket.Destroy();
                }
                return retVal;
            }
            protected set
            {
            }
        }

        //===========================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_id">ID записи</param>
        /// <param name="_tableName">Имя таблицы</param>
        public BaseAppClass(int _id, string _tableName, int _ownerId, int _packetID, string _radioTech)
        {
            if (posFieldNames.Count == 0)
            {
                posFieldNames.Add("MOB_STATION", "POS_ID");
                posFieldNames.Add("MOB_STATION2", "POS_ID");
                posFieldNames.Add("EARTH_STATION", "SITE_ID");
                posFieldNames.Add("MICROWS", "SITE_ID");
                posFieldNames.Add("TV_STATION", "SITE_ID");
                posFieldNames.Add("FM_STATION", "SITE_ID");
                posFieldNames.Add("DVBT_STATION", "SITE_ID");
                posFieldNames.Add("TDAB_STATION", "SITE_ID");
            }
            
            OwnerWindows = null;
            ReadOnly = true; //По умолчанию только на чтение
            UcrfDepartment curDept = CUsers.GetCurDepartment();
            SetRight(_tableName);

            radioTech = _radioTech;
            packetID = _packetID;            

            if (packetID == IM.NullI)
                packetID = 0;

            if (packetID == 0)
            {
                string strID = _id.ToString();
                // Ищем пакет, с которым связана заявка
                IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                rs.Select("Packet.ID");
                rs.SetWhere("Application.OBJ_TABLE", IMRecordset.Operation.Like, _tableName);
                rs.SetAdditional("([Application.OBJ_ID1]=" + strID + " OR [Application.OBJ_ID2]=" + strID +
                                 " OR [Application.OBJ_ID3]=" + strID + " OR [Application.OBJ_ID4]=" + strID +
                                 " OR [Application.OBJ_ID5]=" + strID + " OR [Application.OBJ_ID6]=" + strID + ")");
                rs.OrderBy("Packet.ID", OrderDirection.Descending);
                try
                {
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        packetID = rs.GetI("Packet.ID");
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }

            EPacketType packetType = EPacketType.PckUnknown;
            // Вытаскиваем данные из пакета
            if (packetID != 0)
            {
                CPacket packet = new CPacket(packetID);

                NumberOut = packet.NumberOut;
                DateOut = packet.DateOut;
                NumberIn = packet.NumberIn;
                DateIn = packet.DateIn;

                if (packet.PacketState == StatePacket.SentToUrcp && (curDept != UcrfDepartment.URCP))
                    ReadOnly = true;

                packetType = packet.PacketType;

                /*IMRecordset r2 = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
                r2.Select("ID,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,CREATED_DATE");
                r2.SetWhere("ID", IMRecordset.Operation.Eq, packetID);
                r2.OrderBy("CREATED_DATE", OrderDirection.Descending);
                try
                {
                    r2.Open();
                    if (!r2.IsEOF())
                    {
                        NumberOut = r2.GetS("NUMBER_OUT");
                        DateOut = r2.GetT("DATE_OUT");
                        NumberIn = r2.GetS("NUMBER_IN");
                        DateIn = r2.GetT("DATE_IN");
                    }
                }
                finally
                {
                    r2.Close();
                    r2.Destroy();
                }*/
            }

            // Попытаемся выбрать первый сектор из всех секторов
            if (packetID != 0)
            {
                string strID = _id.ToString();
                // Ищем пакет, с которым связана заявка
                IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                rs.Select(
                   "Application.ID,Packet.ID,Application.OBJ_ID1,Application.OBJ_ID2,Application.OBJ_ID3,Application.OBJ_ID4,Application.OBJ_ID5,Application.OBJ_ID6,Application.OBJ_TABLE,Application.PRICE_ID");
                rs.SetWhere("Packet.ID", IMRecordset.Operation.Eq, packetID);
                rs.SetWhere("Application.OBJ_TABLE", IMRecordset.Operation.Like, _tableName);
                rs.SetAdditional("([Application.OBJ_ID1]=" + strID + " OR [Application.OBJ_ID2]=" + strID +
                                 " OR [Application.OBJ_ID3]=" + strID + " OR [Application.OBJ_ID4]=" + strID +
                                 " OR [Application.OBJ_ID5]=" + strID + " OR [Application.OBJ_ID6]=" + strID + ")");
                try
                {
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        _id = rs.GetI("Application.OBJ_ID1");
                        applID = rs.GetI("Application.ID"); //Забыраем ID заявки
                        priceId = rs.GetI("Application.PRICE_ID");
                        int i = 0;
                        do
                        {
                            try
                            {
                                ++i;
                                int testid = rs.GetI("Application.OBJ_ID" + i);
                                if (testid != IM.NullI && testid != 0)
                                {
                                    recordID.Id = testid;
                                    recordID.Table = _tableName;
                                    //Проверка существования станции в БД
                                    IMRecordset rsTest = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
                                    rsTest.Select("ID");
                                    rsTest.SetWhere("ID", IMRecordset.Operation.Eq, testid);
                                    try
                                    {
                                        rsTest.Open();
                                        if (rsTest.IsEOF())
                                            CLogs.WriteError(ELogsWhat.Appl, string.Format("Can't load station for APPL ID={0} - Station ID = {1}, Station table = {2}", applID, testid, _tableName));
                                    }
                                    finally
                                    {
                                        rsTest.Final();
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                CLogs.WriteError(ELogsWhat.Appl, string.Format("Can't load station for APPL ID={0} - {1}", applID, e));
                            }
                        } while (i < 6);
                    }
                    else
                    {
                        applID = IM.AllocID(PlugTbl.itblXnrfaAppl, 1, -1);
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }

                //Удалил сдвиг, так как удалялась ссылка на станцию (rev. 1219)
                //if (needUpdate)
                //{
                //   IMRecordset rs2 = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                //   rs2.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE,PRICE_ID");
                //   rs2.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                //   try
                //   {
                //      rs2.Open();
                //      if (!rs2.IsEOF())
                //      {
                //         rs2.Edit();
                //         for (int i = 1; i <= 6; ++i)
                //         {
                //            if (i <= ids.Count)
                //               rs2.Put("OBJ_ID" + i.ToString(), ids[i - 1]);
                //            else
                //               rs2.Put("OBJ_ID" + i.ToString(), IM.NullI);
                //         }

                //         rs2.Update();
                //      }
                //   }
                //   finally
                //   {
                //      rs2.Close();
                //      rs2.Destroy();
                //   }
                //}
            }

            recordID.Id = _id;
            recordID.Table = _tableName;
            if (recordID.Table == "")
                throw new IMException("TableName is empty");

            Status = new StatusType();
            //CurrStatus = new StatusType();
            if (recordID.Id > 0)
            {
                if (recordID.Table != AbonentStation.TableName && recordID.Table != EmitterAppl.TableName && recordID.Table != AmateurAppl.TableName)
                {
                    objStation = IMObject.LoadFromDB(recordID);
                    Status = objStation.GetS("STATUS").ToStatusType();
                    if (recordID.Table != ICSMTbl.itblGSM)
                        _isTechnicalUser = (int)objStation.GetD("CUST_CHB1");
                }
            }
            else
            {
                // new appl

                if (recordID.Table == AbonentStation.TableName || recordID.Table == EmitterAppl.TableName || recordID.Table == AmateurAppl.TableName || recordID.Table == ShipApp.TableName)
                // if  (recordID.Table == AmateurAppl.TableName)
                {
                    Status = "AA".ToStatusType();
                    CurrStatus = "AA".ToStatusType();
                }
                else


                    if (packetType == EPacketType.PctLICENCE)
                    {
                        Status = "L".ToStatusType();
                        CurrStatus = "L".ToStatusType();
                    }
                    else
                    {
                        Status = "A".ToStatusType();
                        CurrStatus = "A".ToStatusType();
                    }

                if (recordID.Table != AbonentStation.TableName && recordID.Table != EmitterAppl.TableName && recordID.Table != AmateurAppl.TableName)
                {
                    objStation = IMObject.New(recordID.Table);
                    recordID.Id = objStation.GetI("ID");
                    objStation["OWNER_ID"] = _ownerId;

                    if (recordID.Table != ICSMTbl.itblGSM)
                        objStation.Put("CUST_CHB1", IsTechnicalUser);
                }
                else
                {
                     recordID.Id = IM.AllocID(recordID.Table, 1, -1);
                }
                newStation = true;            
            }

            UpdateForeingMkStatus();
            
            IMRecordset article = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
            //article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,DEFAULTTYPE,STATUS,DATEOUTACTION,DATESTARTACTION");
            article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,DEFAULTTYPE");
            article.SetWhere("STANDARD", IMRecordset.Operation.Like, Standard);
            // article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, Enum.GetName(typeof(WorkType), WorkType.work51));
            string department = Enum.GetName(typeof(ManagemenUDCR), ManagemenUDCR.URCM);
            article.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
            //article.SetWhere("STATUS", IMRecordset.Operation.Eq, 1);
            //article.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
            //article.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

            arts.Clear();
             
            /*
            IMRecordset article = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
            article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,DEFAULTTYPE");
            article.SetWhere("STANDARD", IMRecordset.Operation.Like, Standard);
            // article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, Enum.GetName(typeof(WorkType), WorkType.work51));
            string department = Enum.GetName(typeof(ManagemenUDCR), ManagemenUDCR.URCM);
            article.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
            arts.Clear();
            */

            try
            {
                article.Open();
                for (; !article.IsEOF(); article.MoveNext())
                {
                    string key = article.GetS("ARTICLE");
                    if (arts.ContainsKey(key) == false)
                    {
                        arts.Add(key, article.GetI("ID"));
                        if (article.GetI("ID") == priceId)
                        {
                            priceArticle = article.GetS("ARTICLE");
                        }
                    }
                }
            }
            finally
            {
                article.Destroy();
            }
        }
        /// <summary>
        /// Возвращает список сетей
        /// </summary>
        /// <returns>Список сетей</returns>
        public string[] GetNetNames()
        {
            return Net.NetObject.GetConnectedNetsName(recordID.Id, recordID.Table);
        }

        /// <summary>
        /// запись статуса
        /// </summary>
        public void SetForeingStatus(int PacketId)
        {
            IMRecordset rStatus = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
            rStatus.Select("ID,STATUS_COORD");
            rStatus.SetWhere("ID", IMRecordset.Operation.Eq, PacketId);
            try
            {
                rStatus.Open();
                if (!rStatus.IsEOF())
                {
                    rStatus.Edit();
                    rStatus.Put("STATUS_COORD",Status.StatusAsString);
                    rStatus.Update();
                   
                }
            }
            finally
            {
                rStatus.Final();
            }
        }


        /// <summary>
        /// Обновить статус МК
        /// </summary>
        public void UpdateForeingMkStatus()
        {
            IMRecordset rStatus = new IMRecordset(PlugTbl.APPL,IMRecordset.Mode.ReadOnly);
            rStatus.Select("ID,STATUS_COORD");
            rStatus.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            try
            {
                rStatus.Open();
                if (!rStatus.IsEOF())
                {
                    StatusType tmpStatus = Status;
                    tmpStatus.ForStatusAsString = rStatus.GetS("STATUS_COORD");
                    Status = null;
                    Status = tmpStatus;
                }
            }
            finally 
            {
                rStatus.Final();                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        public void SetRight(string tableName)
        {
            UcrfDepartment curDept = CUsers.GetCurDepartment();
            string region = "";
            if (curDept == UcrfDepartment.Branch)
            {
                IM.ExecuteScalar(ref region, "select case appl.OBJ_TABLE "
                    + " when 'MICROWA' then MwPos.PROVINCE "
                    + " when 'EARTH_STATION' then EsPos.PROVINCE "
                    + " when 'MOB_STATION2' then BsPos.PROVINCE "
                    + " when 'MOB_STATION' then MsPos.PROVINCE "
                    + " when 'TV_STATION' then TvPos.PROVINCE "
                    + " when 'FM_STATION' then FmPos.PROVINCE "
                    + " when 'DVBT_STATION' then DvbtPos.PROVINCE "
                    + " end from %XNRFA_APPL appl "
                    + " left outer join %MICROWA mw on (appl.OBJ_ID1 = mw.ID) left outer join %MICROWS mws on (mw.ID = mws.MW_ID and mw.A = mws.ROLE) left outer join %POSITION_MW MwPos on (mws.SITE_ID = MwPos.ID) "
                    + " left outer join %EARTH_STATION es on (appl.OBJ_ID1 = es.ID) left outer join %POSITION_ES EsPos on (es.SITE_ID = EsPos.ID) "
                    + " left outer join %MOB_STATION2 bs on (appl.OBJ_ID1 = bs.ID) left outer join %POSITION_MOB2 BsPos on (bs.POS_ID = BsPos.ID) "
                    + " left outer join %MOB_STATION ms on (appl.OBJ_ID1 = ms.ID) left outer join %POSITION_WIM MsPos on (ms.POS_ID = MsPos.ID) "
                    + " left outer join %TV_STATION tvs on (appl.OBJ_ID1 = tvs.ID) left outer join %POSITION_BRO TvPos on (tvs.SITE_ID = TvPos.ID) "
                    + " left outer join %FM_STATION fms on (appl.OBJ_ID1 = fms.ID) left outer join %POSITION_BRO FmPos on (fms.SITE_ID = FmPos.ID) "
                    + " left outer join %DVBT_STATION dvbts on (appl.OBJ_ID1 = dvbts.ID) left outer join %POSITION_BRO DvbtPos on (dvbts.SITE_ID = DvbtPos.ID) "
                    + " where appl.ID = " + ApplID.ToString());
                
            }

            IMTableRight rights = IM.TableRight(tableName);
            if ((((rights & IMTableRight.Insert) == IMTableRight.Insert) && ((rights & IMTableRight.Update) == IMTableRight.Update))
                || (curDept == UcrfDepartment.URZP) 
                || (curDept == UcrfDepartment.Branch && region != "" && CUsers.GetUserRegion(IM.ConnectedUser()) == region))
                ReadOnly = false;
        }

        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            if (objStation != null)
            {
                objStation.Dispose();
                objStation = null;
            }
            base.DisposeExec();
        }

        //===========================================================
        /// <summary>
        /// Инициализаци меню
        /// </summary>
        /// <param name="menus">ссылка на компоненту меню</param>
        public virtual void InitMenu(ToolStripItemCollection menus)
        {
            const string menuURZP = "MenuURZP";
            const string menuEXIT = "MenuEXIT";
            const string menuURCHP = "MenuURCHP";
            const string menuURCHM = "MenuURCHM";

            List<string> listUsedMenu = new List<string>();
            UcrfDepartment curDept = CUsers.GetCurDepartment();
            if (curDept == UcrfDepartment.URCP || (curDept == UcrfDepartment.Branch && IsDecentralized()))
                listUsedMenu.Add(menuURCHP);
            else if (curDept == UcrfDepartment.URZP || (curDept == UcrfDepartment.Branch && !IsDecentralized()))
                listUsedMenu.Add(menuURZP);
            else if (curDept == UcrfDepartment.URCM || curDept == UcrfDepartment.Branch)
                listUsedMenu.Add(menuURCHM);

            foreach (ToolStripItem item in menus)
            {
                if (curDept == UcrfDepartment.Branch)
                {
                    switch (item.Name) {
                        case menuURZP: item.Text = "ПТК/НВ/ТВ"; break;
                        case menuURCHP: item.Text = "Дозвільні документи"; break;
                        case menuURCHM: item.Text = "Моніторинг"; break;
                    }
                }
                
                if (item.Name == menuEXIT)
                    continue;

                if ((curDept == UcrfDepartment.URCM || curDept == UcrfDepartment.Branch) && (listUsedMenu.Contains(item.Name) == true))
                    continue; //Пропускаем меню УРЧМ (не обращаем внимание на только для чтения)

                if ((ReadOnly == false) && (listUsedMenu.Contains(item.Name) == true))
                    continue;

                if ((item.Name == menuURZP) && (curDept == UcrfDepartment.URZP || (curDept == UcrfDepartment.Branch && !IsDecentralized())))
                    continue;

                //item.Enabled = !ReadOnly;
                item.Enabled = false;

                if ((item.Name == menuURCHM) && (curDept == UcrfDepartment.Branch)) item.Enabled = true;

                if (item is ToolStripMenuItem)
                {
                    ToolStripMenuItem node = (ToolStripMenuItem)item;
                    InitMenu(node.DropDownItems);
                    for (int i = 0; i < node.DropDownItems.Count; i++) { if (node.DropDownItems[i].Name.Trim() == "miArticle") node.DropDownItems[i].Enabled = true; }
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Добавление сектора на удаление
        /// </summary>
        public virtual void AddDeletedSector(int _sectorID, string _tableName)
        {
            RecordPtr rec = new RecordPtr();
            rec.Id = _sectorID;
            rec.Table = _tableName;
            DeletedIds.Add(rec);
        }
        //===========================================================
        /// <summary>
        /// Сохранение удаленных сектров
        /// </summary>
        public virtual void SaveDeletedSectors()
        {
            foreach (RecordPtr rec in DeletedIds)
            {
                int DeletedID = rec.Id;
                IMRecordset rd = new IMRecordset(rec.Table, IMRecordset.Mode.ReadWrite);
                rd.Select("ID,STATUS");
                rd.SetWhere("ID", IMRecordset.Operation.Eq, DeletedID);
                try
                {
                    rd.Open();
                    if (!rd.IsEOF())
                    {
                        rd.Edit();
                        string currentStatus = rd.GetS("STATUS");
                        rd.Put("STATUS", "ZIP");
                        rd.Update();
                    }
                }
                finally
                {
                    rd.Close();
                    rd.Destroy();
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Проверка количесво найденых лицензий и выводит сообщение, если необходимо
        /// </summary>
        /// <returns>TRUE - технический пользователь иначе не технический пользователь</returns>
        public virtual bool CheckTechUser()
        {
            return CheckTechUser(false);
        }
        //===========================================================
        /// <summary>
        /// Проверка количесво найденых лицензий и выводит сообщение, если необходимо
        /// </summary>
        /// <returns>TRUE - технический пользователь иначе не технический пользователь</returns>
        public virtual bool CheckTechUser(bool isSilent)
        {
            if (licenceListId.Count == 0)
            {//лицензии не найдены
                if (IsTechUser == false && !LicParams.GetLicParams().AutoSetTechno)
                {
                    if (MessageBox.Show("Ліцензій на користування радіочастотним ресурсом не знайдено.\nПрисвоїти статус \"Технологічний\"?",
                                        "Ліцензій не знайдено", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        IsTechnicalUser = 1; //Технологический
                    }
                }
            }
            else
            {
                IsTechnicalUser = 0; //Не технологический
                if (isSilent == false)
                    MessageBox.Show(string.Format(CLocaliz.TxT("{0} licence(s) linked."), licenceListId.Count), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return IsTechUser;
        }
        //===========================================================
        /// <summary>
        /// Добавление станции к сети
        /// </summary>
        public virtual void Add2Net()
        {
            string mess = "Даний тип станції неможливо добавити до мережі!";
            MessageBox.Show(mess, "Default message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        //===========================================================
        /// <summary>
        /// Обновление данных формы
        /// </summary>
        /// <param name="form">форма, которую обновляем</param>
        public virtual void UpdateForm(MainAppForm form)
        {
            UcrfDepartment curDept = CUsers.GetCurDepartment();
            if ((curDept == UcrfDepartment.URZP) || (curDept == UcrfDepartment.Branch && !IsDecentralized()))
            {
                form.Text = CLocaliz.TxT("PTK Results:") + " " + form.Text + " " + GetType().Name + " ApplID:" + ApplID.ToString() + ((ReadOnly == true) ? " ReadOnly" : ""); //TODO Удалить после отладки
            }
            else
            {
                form.Text = form.Text + " " + GetType().Name + " ApplID:" + ApplID.ToString() + ((ReadOnly == true) ? " ReadOnly" : ""); //TODO Удалить после отладки
            }
        }

        private bool IsDecentralized()
        {
            return 0 != (ApplType & (AppType.AppAP3 | AppType.AppAR_3 | AppType.AppRRTR | AppType.AppZRS | AppType.AppVP | AppType.AppA3 | AppType.AppTR_З));
        }
        //===========================================================
        /// <summary>
        /// Обновление данных поля РДПО владельца сайта
        /// </summary>
        /// <param name="form">TextBox</param>
        public virtual void UpdateOwnerRDPO(TextBox textBox)
        {
            if (objStation == null)
                return;  //Для Абонентов не подходит
            int ID_user = objStation.GetI("OWNER_ID");
            if ((ID_user > 0) && (ID_user != IM.NullI))
            {
                using (IMObject user = IMObject.LoadFromDB(new RecordPtr("USERS", ID_user)))
                {
                    textBox.Text = user.GetS("REGIST_NUM");
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Обновление данных поля Названия владельца сайта
        /// </summary>
        /// <param name="form">TextBox</param>
        public virtual void UpdateOwnerName(TextBox textBox)
        {
            if (objStation == null)
                return;  //Для Абонентов не подходит
            int ID_user = objStation.GetI("OWNER_ID");
            if ((ID_user > 0) && (ID_user != IM.NullI))
            {
                using (IMObject user = IMObject.LoadFromDB(new RecordPtr("USERS", ID_user)))
                {
                    textBox.Text = user.GetS("NAME");
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Статическая процедура проверки технологическая станция или нет
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">Id записи</param>
        public static bool IsTechUserProc(string tableName, int id)
        {
            // переменная, хранящая информацию о признаке "Технологичности" БС
            bool isTech = false;
            int OBJ_ID = -1;
            //поиск по таблице XNRFA_APPL для заданного значения id
            using (Icsm.LisRecordSet rs_appl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
            {
                rs_appl.Select("ID,OBJ_ID1,OBJ_TABLE");
                rs_appl.SetWhere("ID", IMRecordset.Operation.Eq, id);
                for (rs_appl.Open(); !rs_appl.IsEOF(); rs_appl.MoveNext())
                {
                    // если ссылка OBJ_ID1 не пустая используем ее
                    if (rs_appl.GetI("OBJ_ID1") >= 0) OBJ_ID = rs_appl.GetI("OBJ_ID1");
                    // если ссылка OBJ_TABLE не пустая используем ее
                    string OBJ_TBL = rs_appl.GetS("OBJ_TABLE");
                    if ((OBJ_ID >= 0) && (OBJ_TBL == tableName))
                    {
                        //поиск по таблице OBJ_TBL, которое мы извлекли с поля OBJ_TABLE таблицы XNRFA_APPL
                        using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(OBJ_TBL, IMRecordset.Mode.ReadOnly))
                        {
                            // делаем выборку (для таблиц MOB_STATION, или MOB_STATION2)
                            // в выборку включены несколько полей, среди которых наиболее важные - CUST_CHB1 и CLASS
                            // первый параметр CUST_CHB1 определяет стутус (если CUST_CHB1=1 - то технологическая), иначе нет
                            // второй параметр CLASS определяет класс станции (если FB - значит базовая станция).
                            rs.Select("ID,CUST_CHB1,CLASS");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, OBJ_ID);
                            rs.Open();
                            if (!rs.IsEOF())
                            {

                                switch (rs.GetS("CLASS").ToUpper())
                                {
                                    default: //case "FB":
                                        // присваиваем isTech true если станция с признаком "Технологическая"
                                        isTech = (rs.GetI("CUST_CHB1") == 1) ? true : false;
                                        if (isTech) break;
                                        break;
                                    case "FL":
                                        break;
                                }


                            }
                        }
                    }
                }
            }

            return isTech;
        }


        //===========================================================
        /// <summary>
        /// Проверяет, является ли город областным центром
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">Id записи</param>
        /// <param name="isFirstLink">Только для релеек (TRUE - cтанция 1, FALSE - cтанция 1)</param>
        public static bool IsRegionCenter(string tableName, int id, bool isFirstLink)
        {
            int isCenter = 0;
            switch(tableName)
            {
                case PlugTbl.itblXnrfaAppl:
                    using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,OBJ_ID1,OBJ_TABLE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        rs.Open();
                        if(!rs.IsEOF())
                        {
                            string table = rs.GetS("OBJ_TABLE");
                            int tableId = rs.GetI("OBJ_ID1");
                            isCenter = IsRegionCenter(table, tableId, isFirstLink) ? 1 : 0;
                        }
                    }
                    break;
                case ICSMTbl.itblEarthStation:
                case ICSMTbl.itblMobStation:
                case ICSMTbl.itblMobStation2:
                case ICSMTbl.itblTV:
                case ICSMTbl.itblFM:
                case ICSMTbl.itblDVBT:
                case ICSMTbl.itblTDAB:
                    using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,Position.City.CUST_CHB1");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        rs.Open();
                        if (!rs.IsEOF())
                            isCenter = rs.GetI("Position.City.CUST_CHB1");
                    }
                    break;
                case ICSMTbl.itblMicrowa:
                    using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID");
                        if(isFirstLink)
                            rs.Select("StationA.Position.City.CUST_CHB1");
                        else
                            rs.Select("StationB.Position.City.CUST_CHB1");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        rs.Open();
                        if (!rs.IsEOF())
                        {
                            if(isFirstLink)
                                isCenter = rs.GetI("StationA.Position.City.CUST_CHB1");
                            else
                                isCenter = rs.GetI("StationB.Position.City.CUST_CHB1");
                        }
                    }
                    break;
            }
            return (isCenter == 1);
        }


        //===========================================================
        /// <summary>
        /// Возвращает COATUU позиции заявки
        /// </summary>
        public static string GetCOATUU(string tableName, int Id, int lNumber)
        {
            string coatuu = "99999999";

            if (tableName == PlugTbl.itblXnrfaAppl)
            {

                IMRecordset applFrom = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                applFrom.Select("ID,OBJ_TABLE");

                applFrom.Select("EarthStation.ID");
                applFrom.Select("EarthStation.Position.City.CODE");

                applFrom.Select("Microwave.ID");
                applFrom.Select("Microwave.StationA.Position.City.CODE");
                applFrom.Select("Microwave.StationB.Position.City.CODE");

                applFrom.Select("BaseSector1.ID");
                applFrom.Select("BaseSector1.Position.City.CODE");

                applFrom.Select("MobileSector1.ID");
                applFrom.Select("MobileSector1.Position.City.CODE");

                applFrom.Select("TvStation.Position.City.CODE");
                applFrom.Select("FmStation.Position.City.CODE");
                applFrom.Select("DvbtStation.Position.City.CODE");
                applFrom.Select("TdabStation.Position.City.CODE");

                applFrom.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                try
                {
                    applFrom.Open();
                    if (!applFrom.IsEOF())
                    {
                        if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblEarthStation)
                        {
                            coatuu = applFrom.GetS("EarthStation.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMicrowa)
                        {
                            if (lNumber == 2)
                                coatuu = applFrom.GetS("Microwave.StationB.Position.City.CODE");
                            else
                                coatuu = applFrom.GetS("Microwave.StationA.Position.City.CODE");// +" / " + applFrom.GetS("Microwave.StationB.Position.REMARK");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMobStation2)
                        {
                            coatuu = applFrom.GetS("BaseSector1.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMobStation)
                        {
                            coatuu = applFrom.GetS("MobileSector1.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblTV)
                        {
                            coatuu = applFrom.GetS("TvStation.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblFM)
                        {
                            coatuu = applFrom.GetS("FmStation.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblDVBT)
                        {
                            coatuu = applFrom.GetS("DvbtStation.Position.City.CODE");
                        }
                        else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblTDAB)
                        {
                            coatuu = applFrom.GetS("TdabStation.Position.City.CODE");
                        }
                    }
                }
                finally
                {
                    applFrom.Close();
                    applFrom.Destroy();
                }
            }
            return coatuu;
        }
        public struct DocData
        {
            public int appId;
            public string rownum;
            public string DocType;
            public string Number;
            public DateTime EndDate;
            public string FullAddress;
            public string Link;
        }
        //===========================================================
        /// <summary>CGlobalXML.
        /// Возвращает данные о документах заявки
        /// </summary>
        public static List<DocData> GetDocInfo(string tableName, int Id)
        {
            List<DocData> dd = new List<DocData>();
            {// Дозвола
                IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
                    rsDocLink.SetWhere("REC_TAB", IMRecordset.Operation.Like, tableName);
                    rsDocLink.OrderBy("DOC_ID", OrderDirection.Descending);
                    for (rsDocLink.Open(); !rsDocLink.IsEOF(); rsDocLink.MoveNext())
                    {
                        bool exit = false;
                        int docId = rsDocLink.GetI("DOC_ID");
                        IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                        rsDocFiles.Select("ID,DOC_TYPE,DOC_REF,EXP_DATE,CREATED_BY,PATH,DATE_CREATED");
                        rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, docId);
                        rsDocFiles.SetWhere("DOC_TYPE", IMRecordset.Operation.Like, "DE");
                        try
                        {
                            rsDocFiles.Open();
                            if (!rsDocFiles.IsEOF())
                            {
                                DocData doc = new DocData();
                                doc.DocType = "EXPL";
                                doc.Number = rsDocFiles.GetS("DOC_REF");
                                doc.EndDate = rsDocFiles.GetT("EXP_DATE");
                                doc.Link = rsDocFiles.GetS("PATH");
                                Dictionary<int, PositionState2> positions = BaseAppClass.GetPositions(tableName, Id);
                                if (positions.Count > 0)
                                    doc.FullAddress = positions[1] == null ? PositionState2.NoPosition : positions[1].FullAddressAuto;
                                dd.Add(doc);
                                exit = true;
                            }
                        }
                        finally
                        {
                            rsDocFiles.Destroy();
                        }
                        if (exit == true)
                            break;
                    }
                }
                finally
                {
                    rsDocLink.Destroy();
                }
            }
            //-----------------------------
            {// Висновки
                IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
                    rsDocLink.SetWhere("REC_TAB", IMRecordset.Operation.Like, tableName);
                    rsDocLink.OrderBy("DOC_ID", OrderDirection.Descending);
                    for (rsDocLink.Open(); !rsDocLink.IsEOF(); rsDocLink.MoveNext())
                    {
                        bool exit = false;
                        IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                        rsDocFiles.Select("ID,DOC_TYPE,DOC_REF,EXP_DATE,CREATED_BY,PATH,DATE_CREATED");
                        rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, rsDocLink.GetI("DOC_ID"));
                        rsDocFiles.SetWhere("DOC_TYPE", IMRecordset.Operation.Like, "VE");
                        try
                        {
                            rsDocFiles.Open();
                            if (!rsDocFiles.IsEOF())
                            {
                                DocData doc = new DocData();
                                doc.DocType = "EMS";
                                doc.Number = rsDocFiles.GetS("DOC_REF");
                                doc.EndDate = rsDocFiles.GetT("EXP_DATE");
                                doc.Link = rsDocFiles.GetS("PATH");
                                Dictionary<int, PositionState2> positions = BaseAppClass.GetPositions(tableName, Id);
                                if (positions.Count > 0)
                                    doc.FullAddress = positions[1] == null ? PositionState2.NoPosition : positions[1].FullAddressAuto;
                                dd.Add(doc);
                                exit = true;
                            }
                        }
                        finally
                        {
                            rsDocFiles.Destroy();
                        }
                        if (exit == true)
                            break;
                    }
                }
                finally
                {
                    rsDocLink.Close();
                    rsDocLink.Destroy();
                }
            }
            return dd;

        }
        //===========================================================
        /// <summary>
        /// Возвращает SID заявки
        /// </summary>
        public static string GetSid(int Id, out string standard)
        {
            string Sid = "";
            IMRecordset rs = new IMRecordset(PlugTbl.AllStations, IMRecordset.Mode.ReadOnly);
            try
            {
                rs.Select("SID,STANDARD");
                rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, Id);
                rs.Open();
                if (!rs.IsEOF())
                {
                    Sid = rs.GetS("SID");
                    standard = rs.GetS("STANDARD");
                }
                else
                {
                    standard = "";
                }
            }
            finally
            {
                rs.Destroy();
            }
            return Sid;
        }

        public static Positions GetSitePositions(string tableName, int id)
        {
            return new Positions(GetPositions(tableName, id));
        }
        //===========================================================
        /// <summary>
        /// Возвращает адрес позиции заявки
        /// </summary>
        public static Dictionary<int, PositionState2> GetPositions(string tableName, int Id)
        {
            Dictionary<int, PositionState2> retVal = new Dictionary<int, PositionState2>();

            if (tableName == PlugTbl.itblXnrfaAppl)
            {// Заявка
                string tablename = "";
                int recordID = 0;
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("OBJ_TABLE,OBJ_ID1");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        tablename = rs.GetS("OBJ_TABLE");
                        recordID = rs.GetI("OBJ_ID1");
                    }
                }
                finally
                {
                    rs.Destroy();
                }
                retVal = GetPositions(tablename, recordID);
            }
            else if ((tableName == ICSMTbl.itblEarthStation)
                  || (tableName == ICSMTbl.itblFM)
                  || (tableName == ICSMTbl.itblMobStation2)
                  || (tableName == ICSMTbl.itblMobStation)
                  || (tableName == ICSMTbl.itblTV)
                  || (tableName == ICSMTbl.itblDVBT)
                  || (tableName == ICSMTbl.itblTDAB)
                  || (tableName == PlugTbl.XfaAmateur)
                  || (tableName == ICSMTbl.itblMicrows)
               )
            {
                using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("Position.ID");
                    rs.Select("Position.TABLE_NAME");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        int siteId = rs.GetI("Position.ID");
                        string siteTable = rs.GetS("Position.TABLE_NAME");
                        if (siteId == IM.NullD || string.IsNullOrEmpty(siteTable))
                        {
                            retVal.Add(1, null);
                        }
                        else
                        {
                            PositionState2 pos = new PositionState2();
                            pos.LoadStatePosition(siteId, siteTable);
                            retVal.Add(1, pos);
                        }
                    }
                }
            }
            else if (tableName == ICSMTbl.itblMicrowa)
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("StationA.ID,StationB.ID");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        Dictionary<int, PositionState2> mwaddr = GetPositions(ICSMTbl.itblMicrows, rs.GetI("StationA.ID"));
                        retVal.Add(1, mwaddr[1]);
                        mwaddr = GetPositions(ICSMTbl.itblMicrows, rs.GetI("StationB.ID"));
                        retVal.Add(2, mwaddr[1]);
                    }
                }
                finally
                {
                    rs.Destroy();
                }
            }
            else if (tableName == PlugTbl.XfaAbonentStation)
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("LOCATION,Position.ID,Position.TABLE_NAME");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        PositionState2 pos = new PositionState2();
                        int siteId = rs.GetI("Position.ID");
                        string siteTable = rs.GetS("Position.TABLE_NAME");
                        if (siteId == IM.NullD || string.IsNullOrEmpty(siteTable))
                            pos.Remark = rs.GetS("LOCATION"); //FullAddress should return this value. No Admin site, shurely
                        else
                            pos.LoadStatePosition(siteId, siteTable);
                        retVal.Add(1, pos);
                    }
                }
                finally
                {
                    rs.Destroy();
                }
            }
            return retVal;
        }
        //===========================================================
        /// <summary>
        /// Возвращает ID владельца
        /// </summary>
        public static int GetStationOwnerId(string tableName, int Id)
        {
            int ownId = IM.NullI;

            if (tableName == PlugTbl.itblXnrfaAppl)
            {// Заявка
                string tablename = "";
                int recordID = 0;
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("OBJ_TABLE,OBJ_ID1");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        tablename = rs.GetS("OBJ_TABLE");
                        recordID = rs.GetI("OBJ_ID1");
                    }
                }
                finally
                {
                    rs.Destroy();
                }
                ownId = GetStationOwnerId(tablename, recordID);
            }
            else if ((tableName == ICSMTbl.itblEarthStation)
                  || (tableName == ICSMTbl.itblFM)
                  || (tableName == ICSMTbl.itblMobStation2)
                  || (tableName == ICSMTbl.itblMobStation)
                  || (tableName == ICSMTbl.itblTV)
                  || (tableName == ICSMTbl.itblDVBT)
                  || (tableName == ICSMTbl.itblTDAB)
                  || (tableName == ICSMTbl.itblMicrowa)
                  || (tableName == ICSMTbl.itblGSM))
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("Owner.ID");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                        ownId = rs.GetI("Owner.ID");
                }
                finally
                {
                    rs.Destroy();
                }
            }
            return ownId;
        }

        //===========================================================
        /// <summary>
        /// Возвращет количество работ по заявке в ДРВ от УРЧМ
        /// </summary>
        public virtual int GetURCMWorksCount()
        {
            return 1;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(string tabName, List<int> Id, List<string> provs, ref string radioTech, ref string adres)
        {
            radioTech = CRadioTech.UNKNOWN;
            if (Id.Count == 0 || Id[0] == IM.NullI)
                return IM.NullI;
            // Вытаскиваем стандарт
            if (tabName != ICSMTbl.itblShip)
            {
                IMRecordset r = new IMRecordset(tabName, IMRecordset.Mode.ReadOnly);
                r.Select("STANDARD");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id[0]);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        radioTech = r.GetS("STANDARD");
                }
                finally
                {
                    r.Destroy();
                }
            }

            if (tabName == MobStaApp.TableName)
            {
                bool find = false;
                foreach (string tech in CRadioTech.getListRadioTech(AppType.AppTR))
                    if (tech == radioTech)
                    {
                        find = true;
                        return MobStaTechnologicalApp.GetDataForURCM(Id[0], radioTech, ref adres);

                    }

                if (!find)
                {
                    foreach (string tech in CRadioTech.getListRadioTech(AppType.AppRR))
                        if (tech == radioTech)
                        {
                            find = true;
                            return MobStaApp.GetDataForURCM(Id, radioTech, ref adres);
                        }
                }
            }
            else if (tabName == EarthApp.TableName)
            {
                return EarthApp.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == MobSta2App.TableName)
            {
                return MobSta2App.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == MicrowaveApp.TableName)
            {
                return MicrowaveApp.GetDataForURCM(Id[0], provs, ref adres);
            }
            else if (tabName == CTvAnalog.TableName)
            {
                return CTvAnalog.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == CTvDigital.TableName)
            {
                return CTvDigital.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == FmAnalogApp.TableName)
            {
                return FmAnalogApp.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == FmDigitalApp.TableName)
            {
                return FmDigitalApp.GetDataForURCM(Id[0], ref adres);
            }
            else if (tabName == AbonentStation.TableName)
            {
                return AbonentAppl.GetDataForURCM(Id[0], ref adres);
            }
            else if (string.IsNullOrEmpty(tabName) == false)
            {
                int retVal = IM.NullI;
                IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsAppl.Select("ID");
                    rsAppl.Select("WORKS_COUNT");
                    rsAppl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, tabName);
                    rsAppl.SetWhere("OBJ_ID1", IMRecordset.Operation.Eq, Id[0]);
                    rsAppl.Open();
                    if (!rsAppl.IsEOF())
                    {
                        retVal = rsAppl.GetI("WORKS_COUNT");
                    }
                }
                finally
                {
                    rsAppl.Final();
                }
                return retVal;
            }
            return IM.NullI;
        }

        //Структура адреса станции
        public struct AdressStruct
        {
            public string index;
            public string reg_ind;
            public string region;
            public string distr_ind;
            public string district;
            public string city_type;
            public string city;
            public string adress;
            public string name;
            public string coatuu;
            public string remark;
        }
        //===========================================================
        /// <summary>
        /// Возвращает адрес позиции заявки по пунктам
        /// </summary>
        public static Dictionary<int, AdressStruct> GetAdress(string tableName, int Id)
        {
            Dictionary<int, AdressStruct> adress = new Dictionary<int, AdressStruct>();
            if (tableName == PlugTbl.itblXnrfaAppl)
            {// Заявка
                string tablename = "";
                int recordID = 0;
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("OBJ_TABLE,OBJ_ID1");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        tablename = rs.GetS("OBJ_TABLE");
                        recordID = rs.GetI("OBJ_ID1");
                    }
                }
                finally
                {
                    rs.Destroy();
                }
                adress = GetAdress(tablename, recordID);
            }
            else if ((tableName == ICSMTbl.itblEarthStation)
                  || (tableName == ICSMTbl.itblFM)
                  || (tableName == ICSMTbl.itblMobStation2)
                  || (tableName == ICSMTbl.itblMobStation)
                  || (tableName == ICSMTbl.itblTV)
                  || (tableName == ICSMTbl.itblDVBT)
                  || (tableName == ICSMTbl.itblTDAB)
               )
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("Position.NAME," +
                       "Position.City.NAME," +
                       "Position.City.CODE," +
                       "Position.City.POSTCODE," +
                       "Position.REMARK," +
                       "Position.CITY," +
                       "Position.ADDRESS," +
                       "Position.City.SUBPROVINCE," +
                       "Position.City.PROVINCE," +
                       "Position.City.CUST_TXT1," +
                       "Position.City.CUST_TXT3," +
                       "Position.City.CUST_TXT2");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        AdressStruct adr = new AdressStruct();
                        adr.index = rs.GetS("Position.City.POSTCODE");
                        adr.distr_ind = rs.GetS("Position.City.CUST_TXT2");
                        adr.district = rs.GetS("Position.City.SUBPROVINCE");
                        adr.reg_ind = rs.GetS("Position.City.CUST_TXT3");
                        adr.region = rs.GetS("Position.City.PROVINCE");
                        adr.city_type = rs.GetS("Position.City.CUST_TXT1");
                        adr.city = rs.GetS("Position.City.NAME");
                        adr.adress = rs.GetS("Position.ADDRESS");
                        adr.name = rs.GetS("Position.NAME");
                        adr.coatuu = rs.GetS("Position.City.CODE");
                        adr.remark = rs.GetS("Position.REMARK");
                        adress.Add(1, adr);
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }
            else if (tableName == ICSMTbl.itblMicrowa)
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("StationA.Position.NAME,StationB.Position.NAME," +
                       "StationA.Position.City.NAME,StationB.Position.City.NAME," +
                       "StationB.Position.City.CODE,StationA.Position.City.CODE," +
                       "StationA.Position.City.POSTCODE,StationB.Position.City.POSTCODE," +
                       "StationA.Position.REMARK,StationB.Position.REMARK," +
                       "StationA.Position.CITY,StationB.Position.CITY," +
                       "StationA.Position.ADDRESS,StationB.Position.ADDRESS," +
                       "StationA.Position.City.SUBPROVINCE,StationB.Position.City.SUBPROVINCE," +
                       "StationA.Position.City.PROVINCE,StationB.Position.City.PROVINCE," +
                       "StationA.Position.City.CUST_TXT1,StationB.Position.City.CUST_TXT1," +
                       "StationA.Position.City.CUST_TXT3,StationB.Position.City.CUST_TXT3," +
                       "StationA.Position.City.CUST_TXT2,StationB.Position.City.CUST_TXT2");

                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        AdressStruct adr = new AdressStruct();
                        adr.index = rs.GetS("StationA.Position.City.POSTCODE");
                        adr.distr_ind = rs.GetS("StationA.Position.City.CUST_TXT2");
                        adr.district = rs.GetS("StationA.Position.City.SUBPROVINCE");
                        adr.reg_ind = rs.GetS("StationA.Position.City.CUST_TXT3");
                        adr.region = rs.GetS("StationA.Position.City.PROVINCE");
                        adr.city_type = rs.GetS("StationA.Position.City.CUST_TXT1");
                        adr.city = rs.GetS("StationA.Position.City.NAME");
                        adr.adress = rs.GetS("StationA.Position.ADDRESS");
                        adr.name = rs.GetS("StationA.Position.NAME");
                        adr.coatuu = rs.GetS("StationA.Position.City.CODE");
                        adr.remark = rs.GetS("StationA.Position.REMARK");
                        adress.Add(1, adr);
                        AdressStruct adr2 = new AdressStruct();
                        adr2.index = rs.GetS("StationB.Position.City.POSTCODE");
                        adr2.distr_ind = rs.GetS("StationB.Position.City.CUST_TXT2");
                        adr2.district = rs.GetS("StationB.Position.City.SUBPROVINCE");
                        adr2.reg_ind = rs.GetS("StationB.Position.City.CUST_TXT3");
                        adr2.region = rs.GetS("StationB.Position.City.PROVINCE");
                        adr2.city_type = rs.GetS("StationB.Position.City.CUST_TXT1");
                        adr2.city = rs.GetS("StationB.Position.City.NAME");
                        adr2.adress = rs.GetS("StationB.Position.ADDRESS");
                        adr2.name = rs.GetS("StationB.Position.NAME");
                        adr2.coatuu = rs.GetS("StationB.Position.City.CODE");
                        adr2.remark = rs.GetS("StationB.Position.REMARK");
                        adress.Add(2, adr2);
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }
            else if (tableName == ICSMTbl.itblGSM)
            {
                IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("GSM_TXT12");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        //adress.Add(rs.GetS("GSM_TXT12"));
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
            }
            return adress;
        }
        //===========================================================
        /// <summary>
        /// Отобразить данные о владельце
        /// </summary>
        public virtual void ShowOperator()
        {
            if ((OwnerID > 0) && (OwnerID != IM.NullI))
            {
                RecordPtr rcPtr = new RecordPtr(ICSMTbl.itblUsers, OwnerID);
                rcPtr.UserEdit();
            }
        }
        /// <summary>
        /// Выберает оборудование из таблицы
        /// </summary>
        /// <param name="title">Шапка (локализируеться автоматически)</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="fieldFilterName">Название поля фильтрации</param>
        /// <param name="initialValue">Значение фильтрации</param>
        /// <param name="needToShowMessage">true - отобразить сообщение о проверке мощности</param>
        /// <returns>RecordPtr на выбраную таблицу</returns>
        protected RecordPtr SelectEquip(string title, string tableName, string fieldFilterName, string initialValue, bool needToShowMessage)
        {
            return SelectEquip(title, tableName, fieldFilterName, initialValue, null, needToShowMessage);
        }

        protected RecordPtr SelectOwner(string title, string tableName, string fieldFilterName, string initialValue, bool needToShowMessage)
        {
            return SelectEquip(title, tableName, fieldFilterName, initialValue, null, needToShowMessage);
        }
        //===========================================================
        /// <summary>
        /// Выберает оборудование из таблицы
        /// </summary>
        /// <param name="title">Шапка (локализируеться автоматически)</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="fieldFilterName">Название поля фильтрации</param>
        /// <param name="initialValue">Значение фильтрации</param>
        /// <param name="dopFilter">SQL дополнитеольного фильта</param>
        /// <param name="needToShowMessage">true - отобразить сообщение о проверке мощности</param>
        /// <returns>RecordPtr на выбраную таблицу</returns>
        protected RecordPtr SelectEquip(string title, string tableName, string fieldFilterName, string initialValue, string dopFilter, bool needToShowMessage)
        {
            string criteria = HelpFunction.ReplaceQuotaSumbols(initialValue);
            string param = string.Format("(upper([{0}]) LIKE upper('%{1}%'))", fieldFilterName, criteria);
            if (string.IsNullOrEmpty(dopFilter) == false)
                param = string.Format("{0} AND ({1})", param, dopFilter);
            RecordPtr retRec = SelectRecord(title, tableName, param, fieldFilterName, OrderDirection.Ascending);

            if (tableName == PlugTbl.XvAbonentOwner || tableName == PlugTbl.ABONENT_OWNER)
                return retRec;
            if ((needToShowMessage == true) && (retRec.Id != IM.NullI))
                MessageBox.Show(CLocaliz.TxT("Check the power of equipment!"),
                                CLocaliz.TxT("Message"),
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return retRec;
        }
        //===========================================================
        /// <summary>
        /// Выберает запись из таблицы
        /// </summary>
        /// <param name="title">Шапка (локализируеться автоматически)</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="filterSql">SQL для фильтрации</param>
        /// <param name="orderField">Поле сортировки</param>
        /// <param name="order">Сортировка</param>
        /// <returns>RecordPtr на выбраную таблицу</returns>
        public static RecordPtr SelectRecord(string title, string tableName, string filterSql, string orderField, OrderDirection order)
        {            
            return RecordPtr.UserSelect(CLocaliz.TxT(title), tableName, filterSql, string.Format("[{0}]", orderField), order);
        }
        //===========================================================
        /// <summary>
        /// Выбор статьи
        /// </summary>
        public virtual void SelectArticle()
        {
            if (Article.ArticleChangeForm.UpdateArticle(applID))
                UpdateChangeGrid();
        }
        //===========================================================
        /// <summary>
        /// Сохранение статьи
        /// </summary>
        public void SaveArticle(string art)
        {
            if (arts.ContainsKey(art))
                priceId = arts[art];
            else
                priceId = IM.NullI;
            IMTransaction.Begin();
            try
            {
                //---------------------------------------------
                if (packetID != 0)
                {// Сохраняем заявку

                    YXnrfaAppl appl_ = new YXnrfaAppl();
                    appl_.Table = "XNRFA_APPL";
                    appl_.Format("ID,PRICE_ID");
                    if (appl_.Fetch(applID))
                    {
                        appl_.m_price_id = priceId;
                        appl_.Save();
                      
                    }
                    appl_.Close();
                    appl_.Dispose();
                   
                    /*
                    IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                    r.Select("ID,PRICE_ID");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                    try
                    {
                        r.Open();
                        if (!r.IsEOF())
                        {// Заявки нет                        
                            r.Edit();
                            r["PRICE_ID"] = priceId;
                            r.Update();
                        }
                    }
                    finally
                    {
                        r.Close();
                        r.Destroy();
                    }
                     */ 
                }
                IMTransaction.Commit();
            }
            catch (Exception ex)
            {
                IMTransaction.Rollback();
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        //===========================================================
        public virtual bool OnAfterSave()
        {
            return false;
        }
        //===========================================================
        /// <summary>
        /// Сохранить тип заявки
        /// </summary>
        /// <param name="applID_"></param>
        /// <param name="ApplTp"></param>
        public void SaveApplType(int applID_, string ApplTp)
        {
            try {
                    using (LisProgressBar pb = new LisProgressBar((CLocaliz.TxT("Saving application type...")))) {
                        pb.SetSmall(CLocaliz.TxT("Please wait"));
                        // Сохраняем заявку
                        IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                        r.Select("ID,APPL_TYPE");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, applID_);
                        try {
                            r.Open();
                            if (!r.IsEOF()) {
                                r.Edit();
                                r.Put("APPL_TYPE",ApplTp.ToString());
                                r.Update();
                            }
                        }
                        finally
                        {
                            r.Close();
                            r.Destroy();
                        }
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //===========================================================
        /// <summary>
        /// Сохраняем заявку в БД
        /// </summary>
        public bool OnSaveAppl()
        {
            bool saveApplResult = false;
            if (arts.ContainsKey(priceArticle))
                priceId = arts[priceArticle];
            else
                priceId = IM.NullI;

            try
            {
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    using (LisProgressBar pb = new LisProgressBar((CLocaliz.TxT("Saving application..."))))
                    {
                        pb.SetSmall(CLocaliz.TxT("Please wait"));
                        //---------------------------------------------
                        if (packetID != 0)
                        {
                            // Сохраняем заявку
                            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                            r.Select("ID,OBJ_ID1,OBJ_TABLE,EMPLOYEE_ID,CREATE_DATE,STANDARD,PRICE_ID,DATE_CREATED,CREATED_BY,APPL_TYPE");
                            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                            try
                            {
                                r.Open();
                                if (r.IsEOF())
                                {
                                    // Заявки нет
                                    r.AddNew();
                                    r["ID"] = applID;
                                    r["APPL_TYPE"] = ApplType.ToString();
                                    r["OBJ_ID1"] = recordID.Id;
                                    r["OBJ_TABLE"] = recordID.Table;
                                    r["EMPLOYEE_ID"] = HelpFunction.getUserIDByName(IM.ConnectedUser());
                                    r["CREATE_DATE"] = DateTime.Now;
                                    r["STANDARD"] = radioTech;
                                    r["PRICE_ID"] = priceId;
                                    r["CREATED_BY"] = IM.ConnectedUser();
                                    r["DATE_CREATED"] = DateTime.Now;
                                    r.Update();
                                    AddPacketEvent();
                                    UpdateEventGrid();
                                    {
                                        // Создаем ссылку на пакет
                                        IMRecordset rPacket = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadWrite);
                                        rPacket.Select("APPL_ID,PACKET_ID,CREATED_DATE");
                                        rPacket.SetWhere("APPL_ID", IMRecordset.Operation.Eq, -1);
                                        try
                                        {
                                            rPacket.Open();
                                            rPacket.AddNew();
                                            rPacket.Put("APPL_ID", applID);
                                            rPacket.Put("PACKET_ID", packetID);
                                            rPacket.Put("CREATED_DATE", DateTime.Now);
                                            rPacket.Update();
                                        }
                                        finally
                                        {
                                            rPacket.Close();
                                            rPacket.Destroy();
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                r.Close();
                                r.Destroy();
                            }
                        }

                        if (newStation == true)
                        {
                            // Изменяем статус станции
                            // Status here could be "" or "L"
                            // if Status == "L", nothing should happen, if "", is changed to "A" or "AA"
                            StationStatus wf = new StationStatus();
                                if (!string.IsNullOrEmpty(CurrStatus.StatusAsString.Trim()))
                                {
                                    Status = wf.GetNewStatus(CurrStatus, appType, StationStatus.ETypeEvent.TypeAppl);
                                    CurrStatus = Status;

                                }
                                else
                                {
                           
                                    Status = wf.GetNewStatus(Status, appType, StationStatus.ETypeEvent.TypeAppl);
                                }
                        }

                        SetForeingStatus(packetID);
                        saveApplResult = SaveAppl();
                        SaveDeletedSectors();
                    }
                    tr.Commit();
                    IsChangeDop = false;
                }
                InitChangeGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
                saveApplResult = false;
            }
            UpdateForeingMkStatus();
            return saveApplResult;
        }
        //===========================================================
        /// <summary>
        /// Привязываем события по пакету
        /// </summary>
        public void AddPacketEvent()
        {
            //EDocEvent docEvents = EDocEvent.evCreatePacket;
            //if (CUsers.GetUserDepartment() == ManagemenUDCROfICSM.URCP)
            //   docEvents = EDocEvent.evCreatePacketURCP;
            //else if (CUsers.GetUserDepartment() == ManagemenUDCROfICSM.URZP)
            //   docEvents = EDocEvent.evCreatePacketURZP;

            //if (applID != 0 && applID != IM.NullI)
            //{
            //   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, applID, docEvents, DateOut, NumberOut, DateIn.AddDays(30), "", HelpFunction.getUserIDByName(IM.ConnectedUser()));
            //   CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, applID, docEvents, DateIn, NumberIn, DateIn.AddDays(30), "", HelpFunction.getUserIDByName(IM.ConnectedUser()));
            //}
            AddPacketEvent(applID, DateIn, DateOut, NumberIn, NumberOut);
        }
        //===========================================================
        /// <summary>
        /// Привязываем события по пакету
        /// </summary>
        public static void AddPacketEvent(int applID, DateTime dateIn, DateTime dateOut, string numIn, string numOut)
        {
            EDocEvent docEvents = EDocEvent.evCreatePacket;
            if (CUsers.GetCurDepartment() == UcrfDepartment.URCP)
                docEvents = EDocEvent.evCreatePacketURCP;
            else if (CUsers.GetCurDepartment() == UcrfDepartment.URZP)
                docEvents = EDocEvent.evCreatePacketURZP;
            else if (CUsers.GetCurDepartment() == UcrfDepartment.Branch)
                docEvents = EDocEvent.evBranchPacket;

            if (applID != 0 && applID != IM.NullI)
            {
                CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, applID, docEvents, dateOut, numOut, dateIn.AddDays(30), "", HelpFunction.getUserIDByName(IM.ConnectedUser()));
                CEventLog.AddEvent(PlugTbl.itblXnrfaAppl, applID, docEvents, dateIn, numIn, dateIn.AddDays(30), "", HelpFunction.getUserIDByName(IM.ConnectedUser()));
            }
        }

        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public abstract void UpdateToNewPacket();

        public virtual void AddSectorSps(Grid grid) { }
        public virtual void AddSectorUps(Grid grid) { }
        
        public virtual void Coordination() { }
        //===========================================================
        /// <summary>
        /// Callback when use select Add Sector
        /// </summary>
        public virtual void AddSector(Grid grid) { }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public virtual BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl) { return null; }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public virtual BaseAppClass CreateStation() { return null; }
        //===========================================================
        /// <summary>
        /// DoubleClick на гриде
        /// </summary>
        /// <param name="grid">Грид</param>
        /// 
        public void GridDoubleClick(Grid grid)
        {
            Cell cell = grid.SelectedCell;
            if (cell != null)
            {
                ShowObject(cell);
                if (cell.Key == "NewKey-KAddr") {
                        DoubleClickOnGrid(cell);
                }
            }
        }
        /// <summary>
        /// Обработчик двойного нажатия левой кнопки мыши на ячейке
        /// </summary>
        public virtual void DoubleClickOnGrid(Cell cell) { }
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected virtual void ShowObject(Cell cell)
        {
        }
        //===========================================================
        /// <summary>
        /// Callback when use select Add Sector
        /// </summary>
        /// <param name="form">TextBox</param>
        public virtual void RemoveSector(Grid grid)
        {

        }
        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public virtual void CalculateEMS()
        {
            MessageBox.Show("This function is not implemented yet", "Message");
        }
        //============================================================
        /// <summary>
        /// Проверка необходимости сохранить заявку
        /// </summary>
        /// <returns>TRUE - заявка не изменялась или сохраненна, иначе FALSE</returns>
        protected bool NeedSaveAppl()
        {
            if (ReadOnly)
                return true;

            if (gridParam.Edited || IsChangeDop)
            {
                if (MessageBox.Show("Ви бажаєте зберегти зміни?", "Підтвердження", MessageBoxButtons.YesNo) ==
                    DialogResult.Yes)
                {
                    // Заставить явно вызваться все CellValidate-ы, некоторые из них явно пишут в IMObject
                    // причем тогда, когда форма закрыватеся и фокус сооветвенно теряется.
                    //  это будет самый быстрый путь.
                    gridParam.CellSelect(gridParam.GetCell(0, 0));
                    OnSaveAppl();
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected abstract bool SaveAppl();
        //===========================================================
        /// <summary>
        /// Сохраняем лицензии
        /// </summary>
        public virtual void SaveLicence()
        {
            // Привязка лицензий к заявке
            if ((applID != 0) && (applID != IM.NullI))
            {
                IMRecordset rsAppLic = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadWrite);
                rsAppLic.Select("ID,APPL_ID,LIC_ID");
                rsAppLic.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applID);
                try
                {
                    rsAppLic.Open();
                    bool isAdd = false;
                    foreach (int licenID in licenceListId)
                    {
                        if (rsAppLic.IsEOF() || (isAdd == true))
                        {
                            isAdd = true;
                            rsAppLic.AddNew();
                            rsAppLic["ID"] = IM.AllocID(PlugTbl.itblXnrfaApplLic, 1, -1);
                            rsAppLic["APPL_ID"] = applID;
                        }
                        else
                        {
                            rsAppLic.Edit();
                        }
                        rsAppLic["LIC_ID"] = licenID;
                        rsAppLic.Update();
                        rsAppLic.MoveNext();
                    }
                    if (isAdd == false)
                        while (!rsAppLic.IsEOF())
                        {
                            rsAppLic.Delete();
                            rsAppLic.MoveNext();
                        }
                }
                finally
                {
                    rsAppLic.Close();
                    rsAppLic.Destroy();
                }
            }
        }

        //============================================================
        /// <summary>
        /// Читает список лицензий для заявки.
        /// Данные о лицензиях извлекаются из XNRFA_APPL_LIC
        /// </summary>
        /// <returns></returns>
        public List<int> ReadLicenseIDList()
        {
            return ReadLicenseIDList(ApplID);            
        }
        //============================================================
        /// <summary>
        /// Читает список лицензий для заявки.
        /// Данные о лицензиях извлекаются из XNRFA_APPL_LIC
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns></returns>
        public static List<int> ReadLicenseIDList(int applId)
        {
            List<int> licenseIdList = new List<int>();

            IMRecordset rsAppLic = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadOnly);
            rsAppLic.Select("ID,APPL_ID,LIC_ID");
            rsAppLic.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applId);
            try
            {
                for (rsAppLic.Open(); !rsAppLic.IsEOF(); rsAppLic.MoveNext())
                {
                    licenseIdList.Add(rsAppLic.GetI("LIC_ID"));
                }
            }
            finally
            {
                if (rsAppLic.IsOpen())
                    rsAppLic.Close();

                rsAppLic.Destroy();
            }

            return licenseIdList;
        }

        static Dictionary<string, string> areaCache = new Dictionary<string, string>();
        //============================================================
        /// <summary>
        /// Возвращает список ID записей из таблицы выделения каналов, которые подходят
        /// </summary>
        /// <param name="ownerID">Заявитель</param>
        /// <param name="PlanName">Область</param>
        /// <param name="provs">Радиотехнология</param>
        /// <returns>список идентификаторов</returns>
        protected List<int> GetID_CH_ALLOTMENTS(int ownerID, string PlanName, List<string> provs) //Провинции
        {
            List<int> ids = new List<int>();
            int IDPlan = 0;
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME");
            r.SetWhere("NAME", IMRecordset.Operation.Like, PlanName);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    IDPlan = r.GetI("ID");
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            // area cache
            if (areaCache.Count == 0)
            {
                IMRecordset arRs = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                try
                {
                    arRs.Select("NAME,CODE");
                    for (arRs.Open(); !arRs.IsEOF(); arRs.MoveNext())
                        areaCache.Add(arRs.GetS("NAME"), arRs.GetS("CODE"));
                }
                finally
                {
                    arRs.Destroy();
                }
            }
            List<string> provinceCodes = new List<string>();
            string pCode = null;
            foreach (string p in provs)
                if (areaCache.TryGetValue(p, out pCode))
                    provinceCodes.Add(pCode);


            // Вытаскиваем все подходяцие записи из таблицы выделения
            IMRecordset r2 = new IMRecordset(ICSMTbl.itblAhAllotments, IMRecordset.Mode.ReadOnly);
            r2.Select("ID,OWNER_ID,PLAN_ID,AREA_ID,Area.CODE");
            r2.Select("AREA_TYPE,Area.CODE,Area.NAME,AREA_LIST");
            r2.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerID);
            r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDPlan);
            try
            {
                for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                {
                    bool found = r2.GetS("AREA_TYPE") == "I" && provinceCodes.Contains(r2.GetS("Area.CODE"));
                    if (!found && r2.GetS("AREA_TYPE") == "L")
                    {
                        string pcList = r2.GetS("AREA_LIST");
                        foreach (string pc in provinceCodes)
                            if (pcList.IndexOf(pc) > -1)
                            {
                                found = true;
                                break;
                            }
                    }
                    if (found)
                        ids.Add(r2.GetI("ID"));
                }
            }
            finally
            {
                r2.Close();
                r2.Destroy();
            }

            return ids;
        }
        /// <summary>
        /// Заполнение поля Value2 для Cell
        /// </summary>
        /// <param name="source">входящая строка</param>
        /// <returns>исходящая строка</returns>
        public static string FillValue2(string source)
        {
            if (string.IsNullOrEmpty(source))
                return "\t";
            return source;
        }

        protected string NormalizeNumberList(string SrcString)
        {
            if (SrcString == "")
                return "";
            string rs1 = SrcString.Replace("; ", ";");
            string rs2 = rs1.Replace(";", "; ");
            if ((rs2.Length > 1) && (rs2.Substring(rs2.Length - 2) == "; "))
            {
                rs2 = rs2.Substring(0, rs2.Length - 2);
            }
            return rs2;
        }

        public void OpenComments(BaseAppClass diff, BaseAppClass commit)
        {
            string temp = string.Empty;
            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,COMMENTS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, ApplID);
                r.Open();

                if (!r.IsEOF())
                    temp = r.GetS("COMMENTS");
                //App форма
                if (diff == null && commit == null)
                    Comment = temp;
                //Commit форма
                else if (diff == null && commit != null)
                {
                    if (string.IsNullOrEmpty(Comment) && !string.IsNullOrEmpty(temp))
                        Comment = temp;
                }
                //Diff форма
                else if (commit == null && diff != null)
                {
                    if (string.IsNullOrEmpty(Comment) && !string.IsNullOrEmpty(temp))
                        Comment = temp;
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Пердупреждение о сбросе ссылки на сайт
        /// </summary>
        protected bool ShowMessageReference(bool show)
        {
            if (show == false)
                return true;
            DialogResult dr = MessageBox.Show(
                CLocaliz.TxT("Reference to old site will be dropped, and new site will be created. Continue?"),
                  CLocaliz.TxT("Confirmation"),
                     MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            return dr == DialogResult.Yes ? true : false;
        }
        /// <summary>
        /// Предупреждение "Сайт не задан"
        /// </summary>
        protected void ShowMessageNoReference()
        {
            MessageBox.Show(CLocaliz.TxT("No reference to existing site.") + "\n" +
                            CLocaliz.TxT("To pick existing site press the button in the second coordinate cell.") + "\n" +
                            CLocaliz.TxT("To create new one press the button in the address cell.") + "\n" +
                            CLocaliz.TxT("To cancel accidental changes close the form without saving."),
                            CLocaliz.TxT("Confirmation"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        #region PARAM GRID
        //============================================================
        /// <summary>
        /// Инициализация грида параметров
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            grid.ReadOnly = ReadOnly;

            for (int r = 0; r < grid.GetRowCount(); r++)
                for (int c = 0; c < grid.GetColumnCount(r); c++)
                    InitParamGrid(grid.GetCell(r, c));
        }
        //============================================================
        /// <summary>
        /// Печать документа из заявки
        /// </summary>
        public virtual void PrintDoc()
        {
            // Проверка ни зменения статуса заявки.
            if ((gridParam.Edited || IsChangeDop) && /*(((recordID.Table != ICSMTbl.itblGSM) && (objStation.GetI("CUST_CHB1") != 1)) || (recordID.Table == ICSMTbl.itblGSM)) && */
               (MessageBox.Show(CLocaliz.TxT("You must to save this appl before printing the document. Do you want to save the appl?"), CLocaliz.TxT("Print document"), MessageBoxButtons.YesNo) == DialogResult.Yes)
              )
            {
                this.OnSaveAppl();
            }

            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr(recordID.Table, recordID.Id), (IsTechnicalUser == 1), applID, appType, false);
            if (frmReport.ShowDialog() == DialogResult.OK)
            {
                DateTime startDate = frmReport.getStartDate();
                DateTime endDate = frmReport.getEndDate();
                string docType = frmReport.getDocType();
                if (docType == DocType.DOZV_CANCEL)
                    endDate = IM.NullT;
                try
                {
                    string docName = getOutDocName(docType, "");
                    string dopFilter = getDopDilter();
                    SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
                    PrintDocs printDocs = new PrintDocs();

                    //только для технологии РБСС
                    if (Standard == CRadioTech.RBSS)
                    {
                        if (printDocs.CreateOneReport(CRadioTech.RBSS, docType, appType, startDate, endDate, PlugTbl.itblXnrfaAppl, applID, dopFilter, docName, true))
                        {
                            SaveAfterDocPrint(frmReport);
                            UpdateEventGrid();
                        }
                    }
                    else
                    {
                        if (printDocs.CreateOneReport(docType, appType, startDate, endDate, PlugTbl.itblXnrfaAppl, applID, dopFilter, docName, true))
                        {
                            SaveAfterDocPrint(frmReport);
                            UpdateEventGrid();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //============================================================
        /// <summary>
        /// Печать документа на бланке
        /// </summary>
        public virtual void PrintDocOnBlank(){}
        /// <summary>
        /// Перевірка на наявність в формі Diff даних, що змінювались
        /// </summary>
        /// <param name="objId">ID форми, що перевіряється</param>
        /// <param name="diffTableName">Таблиця Diff в якій перевіряється форма</param>
        /// <param name="objTableName">Название таблицы станции</param>
        /// <returns>Наявність змін</returns>
        public static bool IsPresent(int objId, string diffTableName, string objTableName)
        {
            bool result1 = false;
            bool result2 = false;

            using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(diffTableName, IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, objId);
                r.Open();
                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE")==0)
                        result1 = true;
                }
            }

            if (result1)
            {
                int packetId = IM.NullI;
                const string sql = "([Application.OBJ_ID1] = {0} OR [Application.OBJ_ID2] = {0} OR" +
                                   " [Application.OBJ_ID3] = {0} OR [Application.OBJ_ID4] = {0} OR" +
                                   " [Application.OBJ_ID5] = {0} OR [Application.OBJ_ID6] = {0})";
                using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.PacketToAppl, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("APPL_ID,PACKET_ID");
                    rs.SetWhere("Packet.CONTENT", IMRecordset.Operation.Like, EPacketType.PckPTKNVTV.ToStr());
                    rs.SetWhere("Application.OBJ_TABLE", IMRecordset.Operation.Like, objTableName);
                    rs.SetAdditional(string.Format(sql, objId, objTableName));
                    rs.Open();
                    if (!rs.IsEOF())
                        packetId = rs.GetI("PACKET_ID");
                }
                CPacket packet = new CPacket(packetId);
                if (packet.PacketState == StatePacket.SentToUrcp)
                    result2 = true;
            }
            return result1 & result2;
        }

        //============================================================
        /// <summary>
        /// Печать документа из пакета
        /// </summary>
        public virtual string PacketPrint(DateTime startDate, DateTime endDate, string docType, string number)
        {
            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }

            string docName = "";

            try
            {
                docName = getOutDocName(docType, number);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            string dopFilter = getDopDilter();
            //
            if (docType == DocType.DOZV)
            {
                IMRecordset rsStatLic = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadOnly);
                rsStatLic.Select("ID,APPL_ID,LIC_ID");
                rsStatLic.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applID);
                try
                {
                    DateTime LastDate = DateTime.Now.AddDays(-1).Date;
                    for (rsStatLic.Open(); !rsStatLic.IsEOF(); rsStatLic.MoveNext())
                    {
                        int IdLicence = rsStatLic.GetI("LIC_ID");
                        IMRecordset rsLic = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                        rsLic.Select("ID,STOP_DATE"); //????!
                        rsLic.SetWhere("ID", IMRecordset.Operation.Eq, IdLicence);
                        try
                        {
                            rsLic.Open();
                            if (!rsLic.IsEOF())
                            {
                                DateTime stopDate = rsLic.GetT("STOP_DATE");
                                if ((stopDate >= DateTime.Now) && ((stopDate < LastDate) || (LastDate == DateTime.Now.AddDays(-1).Date)))
                                    LastDate = stopDate;
                            }
                        }
                        finally
                        {
                            rsLic.Close();
                            rsLic.Destroy();
                        }
                    }
                    if (LastDate != DateTime.Now.AddDays(-1).Date)
                        endDate = LastDate;
                }
                finally
                {
                    rsStatLic.Close();
                    rsStatLic.Destroy();
                }
            }
            SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
            PrintDocs printDocs = new PrintDocs();
            if (docType != DocType.LYST_ZRS_CALLSIGN)
            {
                if (printDocs.CreateOneReport(docType, appType, startDate, endDate, PlugTbl.itblXnrfaAppl, applID, dopFilter, docName, false))
                    return docName;
            }
           
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static string GetTableNameFromAppl(int applId)
        {
            string retStandard = "";
            int objId = IM.NullI;
            string tableName = "";
            {
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID,OBJ_ID1,OBJ_TABLE");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                try
                {
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        objId = rs.GetI("OBJ_ID1");
                        tableName = rs.GetS("OBJ_TABLE");
                    }
                }
                finally
                {
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }
            }

            return tableName;
        }

        //============================================================
        /// <summary>
        /// Сохранение изменений в станции перед созданием отчета
        /// </summary>
        /// <param name="docType">Тип документа</param>
        protected virtual void SaveBeforeDocPrint(string docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        {
            switch (docType)
            {
                case DocType.DOZV_OPER:
                case DocType.DOZV:
                case DocType.DOZV_TRAIN:
                case DocType.DOZV_FOREIGHT:
                case DocType.DOZV_TR_SPEC:
                case DocType.DOZV_AR3_SPEC:
                case DocType.LYST_ZRS_CALLSIGN:
                case DocType.LYST_TR_CALLSIGN:
                    ApplDocuments.CreateNewPermission(ApplID, docName.ToFileNameWithoutExtension(), startDate, endDate, docName);
                    break;
                case DocType.DOZV_CANCEL:
                    ApplDocuments.CancelPermision(ApplID, startDate);
                    break;
                case DocType.VISN:
                case DocType.VISN_NR:
                    if (GetTableNameFromAppl(ApplID)!="XFA_ABONENT_STATION")
                        ApplDocuments.CreateNewConclusion(ApplID, docName.ToFileNameWithoutExtension(), startDate, endDate, docName);
                    else ApplDocuments.CreateNewConclusion_XFA_ABONENT_STATION(ApplID, docName.ToFileNameWithoutExtension(), startDate, endDate, docName);
                    break;
            }
        }

        //============================================================
        /// <summary>
        /// Сохранить измения после печати документа
        /// </summary>
        protected virtual void SaveAfterDocPrint(FormSelectOutDoc report)
        {
        }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected abstract string getOutDocName(string docType, string number);
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        public virtual void ChangeOwner(int _newOwnerID)
        {
            if (objStation == null)
                MessageBox.Show("Не можу проконтролювати відповідність Власника РЧП Власникові пакету. Повідомьте Адміністратора.", "Зауваження", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            if (objStation.GetI("OWNER_ID") != _newOwnerID)
            {
                objStation.Put("OWNER_ID", _newOwnerID);
                CJournal.CheckTable(recordID.Table, recordID.Id, objStation);
                objStation.SaveToDB();
                CJournal.CreateReport(applID, appType);
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected virtual string getDopDilter()
        {
            return "";
        }
        //============================================================
        /// <summary>
        /// Печать документа
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public abstract void InitParamGrid(Cell cell);
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon")
            {//шорота
                if (objStation != null)
                {
                    NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(objStation.GetD("X"), 0, objStation.GetS("CSYS"), "4DMS");
                    if (tmpXY.Longitude != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(tmpXY.Longitude, EnumCoordLine.Lon);
                        if (!HelpFunction.ValidateCoords(tmpXY.Longitude, EnumCoordLine.Lon))
                            ChangeColor(cell, Colors.badValue);
                        else
                            ChangeColor(cell, Colors.okvalue);
                    }
                    else
                    {
                        cell.Value = "";
                        objStation["X"] = 0.0;
                        objStation["LONGITUDE"] = 0.0;
                    }
                }
            }
            else if (cell.Key == "KLat")
            {//Долгота
                if (objStation != null)
                {
                    NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(0, objStation.GetD("Y"), objStation.GetS("CSYS"), "4DMS");
                    if (tmpXY.Latitude != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(tmpXY.Latitude, EnumCoordLine.Lat);
                        if (!HelpFunction.ValidateCoords(tmpXY.Latitude, EnumCoordLine.Lat))
                            ChangeColor(cell, Colors.badValue);
                        else
                            ChangeColor(cell, Colors.okvalue);
                    }
                    else
                    {
                        cell.Value = "";
                        objStation["Y"] = 0.0;
                        objStation["LATITUDE"] = 0.0;
                    }
                }
            }
        }
        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public virtual void OnPressButton(Cell cell, Grid grid)
        {
            System.Diagnostics.Debug.WriteLine("Press button at the cell [" + cell.Key + "]");
            //--MessageBox.Show("Press button at the cell [" + cell.Key + "]");
        }
        //============================================================
        /// <summary>
        /// Проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// //??
        public virtual void OnCellValidate(Cell cell, Grid grid)
        {

            if (cell != null)
            {
                //System.Diagnostics.Debug.Indent();
                System.Diagnostics.Debug.WriteLine("Cell validate [" + cell.Key + "]");
                //System.Diagnostics.Debug.Unindent();
                CellValidate(cell, grid);
            }
        }
        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="grid">Указатель на сетку</param>
        public virtual void CellValidate(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    //Сохраняем данные
                    objStation["CSYS"] = "4DMS";
                    objStation["X"] = lon;
                    objStation["LONGITUDE"] = NSPosition.Position.DmsToDec(lon);
                    UpdateOneParamGrid(cell, grid);
                }
            }
            else if (cell.Key == "KLat")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    //Сохраняем данные
                    objStation["CSYS"] = "4DMS";
                    objStation["Y"] = lat;
                    objStation["LATITUDE"] = NSPosition.Position.DmsToDec(lat);
                    UpdateOneParamGrid(cell, grid);
                }
            }
        }
        //============================================================
        /// <summary>
        /// Формирует струтуру грида параметров
        /// </summary>
        /// <param name="grid">Грид параметров</param>
        /*public virtual void LoadStructureParamGrid(Grid grid)
        {
           // Удаляем все строки
           while (grid.GetRowCount() > 0)
              grid.DeleteRow(grid.GetRowCount() - 1);
           string XMLParam = GetXMLParamGrid();  // Загружаем струтуру грида
           // обрабка XML і сворення структурі гріда
           XmlDocument xml = new XmlDocument();
           xml.LoadXml(GetXMLParamGrid());

           XmlNode node;
           node = xml.DocumentElement;
           XmlToParamGrid(node, grid);
        }*/
        //=============================================================
        /*private void XmlToParamGrid(XmlNode node, Grid grid)
        {
           if (node == null)
              return;

           if ((node.Name == "items") && (node.HasChildNodes))
              node = node.FirstChild;

           while(node != null)
           {
              FormatCell(node, grid);
              node = node.NextSibling;
           }
        }
        //=============================================================
        private void FormatCell(XmlNode node, Grid grid)
        {
           if (node == null)
              return;
           XmlAttributeCollection attrColl = node.Attributes;

           if (attrColl["flags"].Value.Contains("s") == false)
           {// Добавляем первый элемент
              grid.AddRow();
           }         
           // Добавляем слеюующую ячейку
           string display = "";
           if (attrColl["display"] != null)
              display = attrColl["display"].Value;
           grid.AddColumn(grid.GetRowCount() - 1, display);

           Cell newCell = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);

           if (attrColl["key"] != null)
              newCell.Key = attrColl["key"].Value;

           newCell.TextHeight = 11;

           newCell.HorizontalAlignment = HorizontalAlignment.Left;
           if (attrColl["HorizontalAlignment"] != null)
           {
              if (attrColl["HorizontalAlignment"].Value == "center")
                 newCell.HorizontalAlignment = HorizontalAlignment.Center;
           }

           if ((attrColl["ReadOnly"] != null)  && (attrColl["ReadOnly"].Value == "true"))
              newCell.CanEdit = false;
         
           if (attrColl["background"] != null)
           {
              if (attrColl["background"].Value == "DarkGray")
                 newCell.BackColor = System.Drawing.Color.DarkGray;
           }
              if (attrColl["fontColor"] != null)
              {
              if (attrColl["fontColor"].Value == "gray")
              {
                 newCell.TextColor = System.Drawing.Color.Gray;
                 newCell.FontStyle = FontStyle.Italic;
              }
              }

           // Шрифт ячейки
           if (attrColl["fontType"] != null)
           {
              if (attrColl["fontType"].Value.Contains("b"))
                 newCell.FontStyle = System.Drawing.FontStyle.Bold;
              else if (attrColl["fontType"].Value.Contains("i"))
                 newCell.FontStyle = System.Drawing.FontStyle.Italic;
           }

           if (attrColl["bold"] != null)
           {
              if (attrColl["bold"].Value == "y")
                 newCell.FontStyle = System.Drawing.FontStyle.Bold;
           }

           // Тип ячейки
           newCell.Type = "string";  //По умолчанию
           if (attrColl["type"] != null)
              newCell.Type = attrColl["type"].Value;

           newCell.DBField = "";  //По умолчанию
           if (attrColl["DBField"] != null)
                newCell.DBField = attrColl["DBField"].Value;

           newCell.TabOrder = -1;
           if (attrColl["tab"] != null)
              newCell.TabOrder = ConvertType.ToInt32(attrColl["tab"].Value, -1);
        }*/
        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public virtual void AutoFill(Cell cell, Grid grid)
        {
        }
        /// <summary>
        /// Возвращает сектор станции
        /// </summary>
        /// <returns>Сектор станции</returns>
        public virtual RecordPtr GetRecordOfSector()
        {
            return recordID;
        }

        //=============================================================
        /// <summary>
        /// Возвращает XML строку для настройки грида параметров
        /// </summary>
        /// <returns>XML строку настройки параметров грида</returns>
        protected override string GetXMLParamGrid()
        {
            string XmlString = "<items>" + "</items>";
            return XmlString;
        }
        #endregion
        //=============================================================
        #region Сохранение дозвола
        //protected string numDozv = "";
        //protected string datePrintDozv = "";
        //protected string dateValidDozv = "";
        //protected string emplDozv = "";
        //protected string dateDRVDozv = "";
        //protected string dateDozvCancel = "";
        //protected string numDozvOld = "";
        //protected string dateDRVDozvOld = "";
        //---------
        //private string NumDozv { get { return numDozv; } }
        //private string DateDozvCancel { get { return dateDozvCancel; } }
        //private string DatePrintDozv { get { return datePrintDozv; } }
        //private string DateValidDozv { get { return dateValidDozv; } }
        //private string NumDozvOld { get { return numDozvOld; } }
        //private string DateDRVDozv { get { return dateDRVDozv; } }
        //private string DateDRVDozvOld { get { return dateDRVDozvOld; } }
        //private string EmplDozv { get { return emplDozv; } }
        ////==============================================================
        ///// <summary>
        ///// Сохраняет данные о дозволе
        ///// </summary>
        ///// <param name="number">Номер дозвола</param>
        ///// <param name="printDate">Дата печати</param>
        ///// <param name="validDate">Дата действия дозвола</param>
        ///// <param name="fileName">Путь к файлу дозвола</param>
        //public void SaveDataAboutDozv(string number, DateTime printDate, DateTime validDate, string fileName)
        //{
        //    ApplDocuments.AddNewPermission(ApplID, number, printDate, validDate, fileName);
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет данные об аннулировании дозвола
        ///// </summary>
        ///// <param name="printDate">Дата печати</param>
        //public virtual void SaveDataAboutDozvCancel(DateTime printDate)
        //{
        //    SaveDataAboutDozvCancelLocal(printDate, objStation, recordID.Table, recordID.Id);
        //}
        ////==============================================================
        ///// <summary>
        ///// Возвращает дату об аннулировании дозвола
        ///// </summary>
        //public virtual DateTime LoadDataAboutDozvCancel()
        //{
        //    return LoadDataAboutDozvCancelLocal(recordID.Table, recordID.Id);
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет данные о дозволе
        ///// </summary>
        ///// <param name="number">Номер дозвола</param>
        ///// <param name="printDate">Дата печати</param>
        ///// <param name="validDate">Действительный до</param>
        //protected void SaveDataAboutDozvLocal(string number, DateTime printDate, DateTime validDate, IMObject _station, string tableName, int tableID)
        //{
        //    if ((DateDRVDozv == "")
        //        || (NumDozvOld == "")
        //        || (DateDRVDozvOld == "")
        //        || (NumDozv == "")
        //        || (DatePrintDozv == "")
        //        || (DateValidDozv == ""))
        //        return;

        //    number = number.ToFileNameWithoutExtension();
        //    IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
        //    try
        //    {
        //        r.Select(string.Format("ID,{0},{1},{2},{3},{4},{5}", DateDRVDozv, NumDozvOld, DateDRVDozvOld, NumDozv, DatePrintDozv, DateValidDozv));
        //        if (!string.IsNullOrEmpty(EmplDozv))
        //            r.Select(emplDozv);
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, tableID);
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            if (_station.GetT(DateDRVDozv) != IM.NullT)
        //            {
        //                string numD = _station.GetS(NumDozv);
        //                _station.Put(NumDozvOld, numD);
        //                r.Put(NumDozvOld, numD);
        //                DateTime dateDRV = _station.GetT(DateDRVDozv);
        //                _station.Put(DateDRVDozvOld, dateDRV);
        //                r.Put(DateDRVDozvOld, dateDRV);
        //                //Очищаем дату
        //                _station.Put(DateDRVDozv, IM.NullT);
        //                r.Put(DateDRVDozv, IM.NullT);
        //            }
        //            _station.Put(NumDozv, number);
        //            r.Put(NumDozv, number);
        //            _station.Put(DatePrintDozv, printDate);
        //            r.Put(DatePrintDozv, printDate);
        //            _station.Put(DateValidDozv, validDate);
        //            r.Put(DateValidDozv, validDate);
        //            if (!string.IsNullOrEmpty(EmplDozv))
        //            {
        //                _station.Put(EmplDozv, IM.ConnectedUser());
        //                r.Put(EmplDozv, IM.ConnectedUser());
        //            }
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет данные об аннулировании дозвола
        ///// </summary>
        ///// <param name="printDate">Дата печати</param>
        //protected void SaveDataAboutDozvCancelLocal(DateTime printDate, IMObject _station, string tableName, int tableID)
        //{
        //    if (DateDozvCancel == "")
        //        return;

        //    IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
        //    try
        //    {
        //        r.Select(string.Format("ID,{0}", DateDozvCancel));
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, tableID);
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            _station.Put(DateDozvCancel, printDate);
        //            r.Put(DateDozvCancel, printDate);
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет данные об аннулировании дозвола
        ///// </summary>
        ///// <param name="tableName">Название таблицы</param>
        ///// <param name="tableId">ID станции</param>
        //protected DateTime LoadDataAboutDozvCancelLocal(string tableName, int tableId)
        //{
        //    DateTime retVal = IM.NullT;
        //    if (DateDozvCancel == "")
        //        return retVal;

        //    IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
        //    try
        //    {
        //        r.Select(string.Format("ID,{0}", DateDozvCancel));
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, tableId);
        //        r.Open();
        //        if (!r.IsEOF())
        //            retVal = r.GetT(DateDozvCancel);
        //    }
        //    finally
        //    {
        //        if (r.IsOpen())
        //            r.Close();
        //        r.Destroy();
        //    }
        //    return retVal;
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        //public virtual void SaveDateDozvFromDRV(DateTime dateDRV)
        //{
        //    SaveDateDozvFromDRVLocal(dateDRV, objStation, recordID.Table, recordID.Id);
        //}
        ////==============================================================
        ///// <summary>
        ///// Вохвращает наличие и дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        //public virtual DateTime LoadValidDateDozvFromDRV()
        //{
        //    return LoadDateDozvValidFromDRVLocal(objStation, recordID.Table, recordID.Id);
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        ///// <param name="_station">Станция</param>
        //protected void SaveDateDozvFromDRVLocal(DateTime dateDRV, IMObject _station, string tableName, int tableID)
        //{
        //    if (dateDRVDozv == "")
        //        return;

        //    IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
        //    try
        //    {
        //        r.Select(string.Format("ID,{0}", dateDRVDozv));
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, tableID);
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            _station.Put(dateDRVDozv, dateDRV);
        //            r.Put(dateDRVDozv, dateDRV);
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}
        ////==============================================================
        ///// <summary>
        ///// Загружает дату печати дозвола в ДРВ
        ///// </summary>
        //protected DateTime LoadDateDozvValidFromDRVLocal(IMObject _station, string tableName, int tableID)
        //{
        //    DateTime dateDRV = IM.NullT;
        //    DateTime dozvCancel = IM.NullT;
        //    DateTime dozvPrint = IM.NullT;
        //    if (dateValidDozv != "")
        //    {
        //        IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
        //        try
        //        {
        //            r.Select(string.Format("ID,{0},{1},{2}", dateValidDozv, dateDozvCancel, dateDRVDozv));
        //            r.SetWhere("ID", IMRecordset.Operation.Eq, tableID);
        //            r.Open();
        //            if (!r.IsEOF())
        //            {
        //                dateDRV = r.GetT(dateValidDozv);
        //                dozvCancel = r.GetT(dateDozvCancel);
        //                dozvPrint = r.GetT(dateDRVDozv);
        //            }
        //        }
        //        finally
        //        {
        //            r.Close();
        //            r.Destroy();
        //        }
        //    }
        //    if ((dozvPrint == IM.NullT || dozvPrint > DateTime.Now) || (dozvCancel != IM.NullT &&
        //       (dozvCancel.Year < DateTime.Now.Year || (dozvCancel.Year == DateTime.Now.Year
        //       && dozvCancel.Month < DateTime.Now.Month))))
        //        dateDRV = IM.NullT;
        //    return dateDRV;
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает номер дозвола
        ///// </summary>
        ///// <returns>Возвращает номер дозвола</returns>
        //public string GetDozvNumber()
        //{
        //    if (NumDozv == "")
        //        return "";
        //    return objStation.GetS(NumDozv);
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает дату начала дозвола
        ///// </summary>
        ///// <returns>Возвращает дату начала дозвола</returns>
        //public DateTime GetStartDozvDate()
        //{
        //    if (DateDRVDozv == "")
        //        return IM.NullT;
        //    return objStation.GetT(DateDRVDozv);
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает старый номер дозвола
        ///// </summary>
        ///// <returns>Возвращает старый номер дозвола</returns>
        //public string GetOldDozvNumber()
        //{
        //    if (NumDozvOld == "")
        //        return "";
        //    return objStation.GetS(NumDozvOld);
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает дату окончания дозвола
        ///// </summary>
        ///// <returns>Возвращает дату окончания дозвола</returns>
        //public DateTime GetStopDozvDate()
        //{
        //    if (DateValidDozv == "")
        //        return IM.NullT;
        //    return objStation.GetT(DateValidDozv);
        //}
        #endregion
        //=============================================================
        #region Сохранение вісновка
        //protected string numVisn = "";
        //protected string datePrintVisn = "";
        //protected string dateValidVisn = "";
        protected int addValidVisnMonth = 0;
        ////---------
        //private string NumVisn { get { return numVisn; } }
        //private string DatePrintVisn { get { return datePrintVisn; } }
        //private string DateValidVisn { get { return dateValidVisn; } }
        //==============================================================
        ///// <summary>
        ///// Сохраняет дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        
        //public virtual void SaveDateVisnFromDRV(DateTime dateDRV)
        //{
        //    //TODO Сохранить дату высновка
        //    SaveDateVisnFromDRVLocal(dateDRV, objStation, recordID.Table, recordID.Id);
        //}
        ////==============================================================
        ///// <summary>
        ///// Сохраняет дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        ///// <param name="_station">Станция</param>
        //protected void SaveDateVisnFromDRVLocal(DateTime dateDRV, IMObject _station, string tableName, int tableID)
        //{
        //    if ((DatePrintVisn == "") || (dateValidVisn == ""))
        //        return;

        //    IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
        //    try
        //    {
        //        r.Select(string.Format("ID,{0},{1}", DatePrintVisn, dateValidVisn));
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, tableID);
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            _station.Put(DatePrintVisn, dateDRV);
        //            r.Put(DatePrintVisn, dateDRV);
        //            _station.Put(dateValidVisn, dateDRV.AddMonths(addValidVisnMonth));
        //            r.Put(dateValidVisn, dateDRV.AddMonths(addValidVisnMonth));
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает номер висновка
        ///// </summary>
        ///// <returns>Возвращает номер висновка</returns>
        //public string GetVisnNumber()
        //{
        //    if (NumVisn == "")
        //        return "";
        //    return objStation.GetS(NumVisn).ToFileNameWithoutExtension();
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает дату начала висновка
        ///// </summary>
        ///// <returns>Возвращает дату начала висновка</returns>
        //public DateTime GetStartVisnDate()
        //{
        //    if (DatePrintVisn == "")
        //        return IM.NullT;
        //    return objStation.GetT(DatePrintVisn);
        //}
        ////=============================================================
        ///// <summary>
        ///// Возвращает дату окончания висновка
        ///// </summary>
        ///// <returns>Возвращает дату окончания висновка</returns>
        //public DateTime GetStopVisnDate()
        //{
        //    if (dateValidVisn == "")
        //        return IM.NullT;
        //    return objStation.GetT(DateValidVisn);
        //}
        #endregion
        //=============================================================
        #region EVENT GRID
        //============================================================
        /// <summary>
        /// Инициализация грида событий
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void InitEventGrid(DataGridView grid)
        {
            gridEventLog = grid;
            // ID
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "chId";
            column.HeaderText = CLocaliz.TxT("ID");
            column.Width = 50;
            column.Visible = false;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // EVENT
            column = new DataGridViewTextBoxColumn();
            column.Name = "chEvent";
            column.HeaderText = CLocaliz.TxT("EVENT");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // EMPLOYEE
            column = new DataGridViewTextBoxColumn();
            column.Name = "chEmployee";
            column.HeaderText = CLocaliz.TxT("EMPLOYEE");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // DOC_DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "chDocDate";
            column.HeaderText = CLocaliz.TxT("DOC DATE");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // DOC_NUMBER
            column = new DataGridViewTextBoxColumn();
            column.Name = "chDocNumber";
            column.HeaderText = CLocaliz.TxT("DOC NUMBER");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // END_DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "chEndDate";
            column.HeaderText = CLocaliz.TxT("END DATE");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            // PATH
            column = new DataGridViewTextBoxColumn();
            column.Name = "chPath";
            column.HeaderText = CLocaliz.TxT("PATH");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridEventLog.Columns.Add(column);
            UpdateEventGrid();
        }
        //============================================================
        /// <summary>
        /// Обновить грид изменений
        /// </summary>
        public void UpdateChangeGrid()
        {
            InitChangeGrid();
        }
        //============================================================
        /// <summary>
        /// Обновить грид событий
        /// </summary>
        public virtual void UpdateEventGrid()
        {
            if (gridEventLog == null)
                return;
            gridEventLog.Rows.Clear();
            // Выбераем все события

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            List<int> idList = new List<int>();
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    if (r.GetI("OBJ_ID1") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID1"));
                    if (r.GetI("OBJ_ID2") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID2"));
                    if (r.GetI("OBJ_ID3") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID3"));
                    if (r.GetI("OBJ_ID4") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID4"));
                    if (r.GetI("OBJ_ID5") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID5"));
                    if (r.GetI("OBJ_ID6") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID6"));
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            IMRecordset r2 = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r2.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
            //   r2.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            string idString = "";
            List<int> evApIds = new List<int>();
            foreach (int id in idList)
                idString += (id.ToString() + ",");
            if (idString != "")
            {
                idString = "(" + idString.Remove(idString.Length - 1) + ")";
                r2.SetAdditional("((OBJ_ID1 in " + idString + " OR OBJ_ID2 in " + idString + " OR OBJ_ID3 in " + idString + " OR OBJ_ID4 in " + idString + " OR OBJ_ID5 in " + idString + " OR OBJ_ID6 in " + idString + ") AND (OBJ_TABLE LIKE '" + recordID.Table + "'))");

                try
                {
                    r2.Open();
                    for (; !r2.IsEOF(); r2.MoveNext())
                        evApIds.Add(r2.GetI("ID"));
                }
                finally
                {
                    r2.Close();
                    r2.Destroy();
                }
            }
            string apsIds = "";
            if (evApIds.Count > 0)
            {
                foreach (int id in evApIds)
                    apsIds += (id.ToString() + ",");
                apsIds = "(" + apsIds.Remove(apsIds.Length - 1) + ")";

                //EventLog.eri
                IMEriLOV eriData = new IMEriLOV("EventLog");

                IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadOnly);
                rsEvent.Select("ID,OBJ_ID,OBJ_TABLE,EVENT,EMPLOYEE_ID,CREATED_DATE,PATH,DOC_DATE,DOC_NUMBER,DOC_END_DATE");
                // rsEvent.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, applID);
                rsEvent.SetAdditional("OBJ_ID in " + apsIds);
                rsEvent.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.itblXnrfaAppl);
                rsEvent.SetWhere("VISIBLE", IMRecordset.Operation.Eq, 1);
                rsEvent.OrderBy("CREATED_DATE", OrderDirection.Descending);
                try
                {
                    for (rsEvent.Open(); !rsEvent.IsEOF(); rsEvent.MoveNext())
                    {
                        string descEvent = rsEvent.GetS("EVENT");
                        string freDescr = null;
                        string freBmp = null;
                        eriData.GetEntry(descEvent, ref freDescr, ref freBmp);
                        if (string.IsNullOrEmpty(freDescr) == false)
                            descEvent = freDescr;
                        string docdate = rsEvent.GetT("DOC_DATE") == IM.NullT ? "" : rsEvent.GetT("DOC_DATE").ToString("dd.MM.yyyy");
                        string docenddate = rsEvent.GetT("DOC_END_DATE") == IM.NullT ? "" : rsEvent.GetT("DOC_END_DATE").ToString("dd.MM.yyyy");
                        string[] row = new string[] { rsEvent.GetI("ID").ToString(), 
                                             descEvent,
                                             HelpFunction.getUserFIOByID(rsEvent.GetI("EMPLOYEE_ID")),
                                             docdate,
                                             rsEvent.GetS("DOC_NUMBER"),
                                             docenddate,
                                             rsEvent.GetS("PATH")
                                           };
                        gridEventLog.Rows.Add(row);
                    }
                }
                finally
                {
                    rsEvent.Close();
                    rsEvent.Destroy();
                }
            }
        }
        //============================================================
        public void ShowEventsDoc(int row, int column)
        {
            try
            {
                if (row >= 0)
                {
                    string path = gridEventLog.Rows[row].Cells[6].Value.ToString();
                    if (!string.IsNullOrEmpty(path))
                        System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
            }
        }
        #endregion
        #region CHANGE GRID
        //============================================================
        /// <summary>
        /// Инициализация грида изменений
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void InitChangeGrid(DataGridView gridLog)
        {
            gridChangeLog = gridLog;
            // ID
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "evId";
            column.HeaderText = "ID";
            column.Width = 50;
            column.Visible = false;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridChangeLog.Columns.Add(column);
            // Flag
            column = new DataGridViewTextBoxColumn();
            column.Name = "cgFlag";
            column.HeaderText = "Flag";
            column.Width = 50;
            column.Visible = false;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridChangeLog.Columns.Add(column);
            // OWNER
            column = new DataGridViewTextBoxColumn();
            column.Name = "owner";
            column.HeaderText = CLocaliz.TxT("OWNER");
            column.Width = 100;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridChangeLog.Columns.Add(column);
            // DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "evDATE";
            column.HeaderText = CLocaliz.TxT("DATE");
            column.Width = 150;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridChangeLog.Columns.Add(column);
            // LINK
            column = new DataGridViewTextBoxColumn();
            column.Name = "link";
            column.HeaderText = CLocaliz.TxT("File / Message");
            column.Width = 200;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridChangeLog.Columns.Add(column);

            gridChangeLog.CellDoubleClick += new DataGridViewCellEventHandler(dgLog_CellDoubleClick);

            // Обновляем окно событий
            InitChangeGrid();
        }

        private void dgLog_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            try
            {
                if (row >= 0)
                {
                    int idRec = gridChangeLog.Rows[e.RowIndex].Cells[0].Value.ToString().ToInt32(IM.NullI);
                    string flag = gridChangeLog.Rows[e.RowIndex].Cells[1].Value.ToString();
                    string path = gridChangeLog.Rows[e.RowIndex].Cells[4].Value.ToString();
                    switch (flag)
                    {
                        case "0":
                            if (!string.IsNullOrEmpty(path))
                                System.Diagnostics.Process.Start(path);
                            break;
                        case "1":
                            Logs.ChangeMessageForm.Show(idRec);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
            }
        }

        //============================================================
        /// <summary>
        /// Загрузка грида изменений
        /// </summary>
        protected virtual void InitChangeGrid()
        {
            if (gridChangeLog != null)
            {
                gridChangeLog.Rows.Clear();
                {
                    IMRecordset rsMsg = new IMRecordset(PlugTbl.XnChangeMsg, IMRecordset.Mode.ReadOnly);
                    rsMsg.Select("ID,DATE_CREATED,EMPLOYEE_ID,MESSAGE");
                    rsMsg.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, applID);
                    rsMsg.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.APPL);
                    rsMsg.OrderBy("DATE_CREATED", OrderDirection.Descending);
                    try
                    {
                        for (rsMsg.Open(); !rsMsg.IsEOF(); rsMsg.MoveNext())
                        {
                            string[] row = new string[]
                                               {
                                                   rsMsg.GetI("ID").ToString(),
                                                   "1",
                                                   CUsers.GetUserFio(rsMsg.GetI("EMPLOYEE_ID")),
                                                   rsMsg.GetT("DATE_CREATED").ToString(),
                                                   rsMsg.GetS("MESSAGE")
                                               };
                            gridChangeLog.Rows.Add(row);
                        }
                    }
                    finally
                    {
                        if (rsMsg.IsOpen())
                            rsMsg.Close();
                        rsMsg.Destroy();
                    }
                }
                //---
                {
                    IMRecordset rsLog = new IMRecordset(PlugTbl.itblXnrfaChangeLog, IMRecordset.Mode.ReadOnly);
                    rsLog.Select("ID,OBJ_ID,OBJ_TABLE,EMPLOYEE_ID,CREATED_DATE,PATH");
                    rsLog.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, applID);
                    rsLog.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.itblXnrfaAppl);
                    rsLog.OrderBy("CREATED_DATE", OrderDirection.Descending);
                    try
                    {
                        for (rsLog.Open(); !rsLog.IsEOF(); rsLog.MoveNext())
                        {
                            int evId = rsLog.GetI("ID");
                            if ((evId != 0) && (evId != IM.NullI))
                            {
                                string[] row = new string[]
                                                   {
                                                       rsLog.GetI("ID").ToString(),
                                                       "0",
                                                       HelpFunction.getUserFIOByID(rsLog.GetI("EMPLOYEE_ID")),
                                                       rsLog.GetT("CREATED_DATE").ToString(),
                                                       rsLog.GetS("PATH")
                                                   };
                                gridChangeLog.Rows.Add(row);
                            }
                        }
                    }
                    finally
                    {
                        rsLog.Close();
                        rsLog.Destroy();
                    }
                }
                
            }
        }
        #endregion
        #region LICENCE GRID
        //============================================================
        /// <summary>
        /// Инициализация грида лицензий
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void InitLicenceGrid(DataGridView gridLic)
        {
            gridLicence = gridLic;
            // ID
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "lisID";
            column.HeaderText = CLocaliz.TxT("ID");
            column.Width = 50;
            column.Visible = false;
            gridLicence.Columns.Add(column);
            // NAME
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisNAME";
            column.HeaderText = CLocaliz.TxT("NAME");
            column.Width = 100;
            gridLicence.Columns.Add(column);
            // START_DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisSTART_DATE";
            column.HeaderText = CLocaliz.TxT("START_DATE");
            column.Width = 100;
            column.Visible = false;
            gridLicence.Columns.Add(column);
            // STOP_DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisSTOP_DATE";
            column.HeaderText = CLocaliz.TxT("STOP_DATE");
            column.Width = 100;
            gridLicence.Columns.Add(column);
            // -------------------------
            gridLicence.PreviewKeyDown += new PreviewKeyDownEventHandler(LicencePreviewKeyDown);
            // Обновляем окно лицензий
            // -------------------------
            // Поиск лицензий связавнных с станцией

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            List<int> idList = new List<int>();
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    if (r.GetI("OBJ_ID1") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID1"));
                    if (r.GetI("OBJ_ID2") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID2"));
                    if (r.GetI("OBJ_ID3") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID3"));
                    if (r.GetI("OBJ_ID4") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID4"));
                    if (r.GetI("OBJ_ID5") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID5"));
                    if (r.GetI("OBJ_ID6") != IM.NullI)
                        idList.Add(r.GetI("OBJ_ID6"));
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            IMRecordset r2 = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r2.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r2.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, objTableName);
            string idString = "";
            string apsIds = "";
            List<int> evApIds = new List<int>();
            foreach (int id in idList)
                idString += (id.ToString() + ",");
            if (idString != "")
            {
                idString = "(" + idString.Remove(idString.Length - 1) + ")";
                r2.SetAdditional("(OBJ_ID1 in " + idString + " OR OBJ_ID2 in " + idString + " OR OBJ_ID3 in " + idString + " OR OBJ_ID4 in " + idString + " OR OBJ_ID5 in " + idString + " OR OBJ_ID6 in " + idString + ")");

                try
                {
                    r2.Open();
                    for (; !r2.IsEOF(); r2.MoveNext())
                        evApIds.Add(r2.GetI("ID"));
                }
                finally
                {
                    r2.Close();
                    r2.Destroy();
                }


                foreach (int id in evApIds)
                    apsIds += (id.ToString() + ",");
                apsIds = "(" + apsIds.Remove(apsIds.Length - 1) + ")";
            }

            if (evApIds.Count > 0)
            {
                licenceListId = new List<int>();
                IMRecordset rsLicence = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadOnly);
                rsLicence.Select("ID,APPL_ID,LIC_ID");
                // rsLicence.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applID);
                rsLicence.SetAdditional("APPL_ID in " + apsIds);
                try
                {
                    for (rsLicence.Open(); !rsLicence.IsEOF(); rsLicence.MoveNext())
                    {
                        int LicID = rsLicence.GetI("LIC_ID");
                        if ((LicID != 0) && (LicID != IM.NullI))
                            licenceListId.Add(LicID);
                    }
                }
                finally
                {
                    rsLicence.Close();
                    rsLicence.Destroy();
                }
            }
            InitLicenceGrid();
        }
        /// <summary>
        /// Обработка клавиш в гриде лицензий
        /// </summary>
        private void LicencePreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (gridLicence.CurrentRow != null)
                {
                    int idLicence = (gridLicence.CurrentRow.Cells[0].Value != null) ? gridLicence.CurrentRow.Cells[0].Value.ToString().ToInt32(IM.NullI) : IM.NullI;
                    if (licenceListId.Remove(idLicence))
                    {
                        gridLicence.Rows.Remove(gridLicence.CurrentRow);
                        IsChangeDop = true;
                        CheckTechUser(true);
                    }
                }
            }
        }

        //============================================================
        /// <summary>
        /// БЕЗОПАСНО заполняет клетку грида по ключу.
        /// Fills grid cell with value by key. SAFELY!
        /// </summary>
        //============================================================
        protected void SafeFillCellByKey(Grid grid, string Key, string Value, bool bCallCellValidate)
        {
            Cell fillCell = grid.GetCellFromKey(Key);
            if (fillCell != null)
            {
                fillCell.Value = Value;

                if (bCallCellValidate)
                    OnCellValidate(fillCell, grid);
            }
        }
        //============================================================
        /// <summary>      
        /// Write data of printing. Why do they need? UNSAFE!
        /// </summary>
        //============================================================
        public virtual void WritePrintDate()
        {
        }

        public virtual double GetBw() { throw new NotImplementedException(); }
        public virtual string GetProvince() { throw new NotImplementedException(); }
        public enum RxTx { Rx, Tx, Both }
        internal virtual CLicence.Band[] GetRxTxBands(RxTx type, string region) { throw new NotImplementedException(); }

        /// <summary>
        /// Recreates Licence Id List
        /// </summary>
        /// <param name="txBands"></param>
        /// <param name="rxBands"></param>
        protected void RefreshLicenceList()
        {
            if (FLicParams.Config() == DialogResult.OK)
            {
                bool reRfeshLicList = true;
                List<int> lcId = new List<int>();
                bool isInterrup = false; bool interactive = true;
                LicParams licParams = LicParams.GetLicParams();
                if (!licParams.AutoLinkLicences) interactive = false;

                if (GetActualLicenceList(interactive, ref lcId, ref isInterrup) && reRfeshLicList)
                {
                    if (!isInterrup)
                    {
                        //#7269
                        lcId.Sort();
                        CheckProlongatedLicences(ref lcId);
                        ShowDetailDozv();

                        licenceListId = lcId;
                        IsChangeDop = true;

                        InitLicenceGrid();

                        CheckTechUser();
                    }
                }
            }
        }

        /// <summary>
        /// Функция удаления всех пробелов в стороке
        /// </summary>
        /// <param name="in_str"></param>
        public static string AllTrim(string  in_str)
        {
            string temp = in_str;
            string tmp="";

            for (int i = 0; i < temp.Length; i++)

                if (temp[i] != ' ') tmp += temp[i];

            return tmp;
        }


        public void ShowDetailDozv()
        {
            try
            {
                string SCDozvil_ = SCDozvil;

                CompareIndex4.Clear();
                for (int kk = 0; kk < GroupLic1.Count; kk++)
                {
                    string GLSpecString = "";
                    string SpecString = "";
                    string SpecStringRecurs = "";
                    int num_index = -1;
                    for (int rr = 0; rr < GroupLic1[kk].CompareIndex.Count; rr++)
                    {

                        if ((!GetStatusInt(ref CompareIndex2, GroupLic1[kk].CompareIndex[rr])))
                        {
                            CompareIndex4.Add(GroupLic1[kk].CompareIndex[rr]);

                            if (AllTrim(GLSpecString).Trim().IndexOf(LicCompare1[GroupLic1[kk].CompareIndex[rr]].name_lic) == -1)
                            {
                                    num_index = GroupLic1[kk].CompareIndex[rr];
                                    GLSpecString += string.Format(" №{0},", LicCompare1[GroupLic1[kk].CompareIndex[rr]].name_lic);
                            }

                        }

                    }

                    if (num_index >= 0)
                    {
                        GLSpecString += string.Format(" дата видачі {0:dd.MM.yyyy}", LicCompare1[num_index].doc_start);
                    }

                    for (int rr = 0; rr < GroupLic1[kk].CompareIndex.Count; rr++)
                    {

                        if ((GetStatusInt(ref CompareIndex4, GroupLic1[kk].CompareIndex[rr])))
                        {

                            string tmn = "";
                            
                                tmn = string.Format(" №{0}, дата видачі {1:dd.MM.yyyy}", LicCompare1[GroupLic1[kk].CompareIndex[rr]].name_lic, LicCompare1[GroupLic1[kk].CompareIndex[rr]].doc_start);

                            if (AllTrim(SpecStringRecurs).Trim().IndexOf(AllTrim(tmn).Trim()) == -1)
                            {

                                SpecStringRecurs += tmn + ",";

                            }
                        }


                    }


                    if (!string.IsNullOrEmpty(SpecStringRecurs))
                    {
                        if (string.IsNullOrEmpty(GLSpecString))
                        {
                            SpecStringRecurs = SpecStringRecurs.Remove(SpecStringRecurs.LastIndexOf(","));
                        }


                        SpecString = string.Format("До {0:dd.MM.yyyy} користування РЧР здійснюється на підставі ліцензії(й): реєстраційний номер(и) ", LicCompare1[GroupLic1[kk].CompareIndex[0]].doc_stop) + SpecStringRecurs + GLSpecString;
                    }
                    else
                    {
                        SpecString = GLSpecString;
                    }





                    if (AllTrim(SCDozvil_).Trim().IndexOf(AllTrim(SpecString).Trim()) == -1)
                    {
                        SCDozvil_ = (SCDozvil_.Length > 0 ? Environment.NewLine : "") + SpecString + ".";
                        ParamsLicense GTR = new ParamsLicense();
                        GTR.doc_stop = LicCompare1[GroupLic1[kk].CompareIndex[0]].doc_stop;
                        GTR.name_lic = SCDozvil_;
                        if (GetStatusLic(ref FinalCompare,GTR.doc_start,GTR.doc_stop,GTR.name_lic) && (!string.IsNullOrEmpty(GTR.name_lic.Trim())))
                        {
                            FinalCompare.Add(GTR);
                        }
                        
                    }
                    
                }


                for (int kk = 0; kk < GroupLic2.Count; kk++)
                {

                    string SpecString = "";
                    
                    for (int rr = 0; rr < GroupLic2[kk].CompareIndex.Count; rr++)
                    {

                        if (GetStatusInt(ref CompareIndex4, GroupLic2[kk].CompareIndex[rr]))
                        {

                            if (AllTrim(SpecString).Trim().IndexOf(LicCompare2[GroupLic2[kk].CompareIndex[rr]].name_lic) == -1)
                            {
                                    SpecString += LicCompare2[GroupLic2[kk].CompareIndex[rr]].name_lic + ", ";
                            }
                        }

                    }


                    if (!string.IsNullOrEmpty(SpecString))
                    {
                        
                        SpecString = string.Format("До {0:dd.MM.yyyy} користування РЧР здійснюється на підставі ліцензії(й): реєстраційний номер(и) №{1} дата видачі {2:dd.MM.yyyy}",
                                   LicCompare2[GroupLic2[kk].CompareIndex[0]].doc_stop, SpecString, LicCompare2[GroupLic2[kk].CompareIndex[0]].doc_start);

                    }


                    if (AllTrim(SCDozvil_).Trim().IndexOf(AllTrim(SpecString).Trim()) == -1)
                    {

                        SCDozvil_ = (SCDozvil_.Length > 0 ? Environment.NewLine : "") + SpecString + ".";
                        ParamsLicense GTR = new ParamsLicense();
                        GTR.doc_stop = LicCompare2[GroupLic2[kk].CompareIndex[0]].doc_stop;
                        GTR.name_lic = SCDozvil_;
                        if (GetStatusLic(ref FinalCompare, GTR.doc_start, GTR.doc_stop, GTR.name_lic) && (!string.IsNullOrEmpty(GTR.name_lic.Trim())))
                        {
                            FinalCompare.Add(GTR);
                        }
                       
                    }


                }
                
                string totalStr = "";

                if (CompareIndex3.Count > 0)
                {
                    for (int kk = 0; kk < CompareIndex3.Count; kk++)
                    {

                            totalStr = string.Format("До {0:dd.MM.yyyy} користування РЧР здійснюється на підставі ліцензії(й): реєстраційний номер(и) №{1}, дата видачі {2:dd.MM.yyyy}",
                                        LicCompare1[CompareIndex3[kk]].doc_stop, LicCompare1[CompareIndex3[kk]].name_lic, LicCompare1[CompareIndex3[kk]].doc_start);
                            if (AllTrim(SCDozvil_).Trim().IndexOf(AllTrim(totalStr).Trim()) == -1)
                            {

                                SCDozvil_ = (SCDozvil_.Length > 0 ? Environment.NewLine : "") + totalStr + ".";
                                ParamsLicense GTR = new ParamsLicense();
                                GTR.doc_stop = LicCompare1[CompareIndex3[kk]].doc_stop;
                                GTR.name_lic = SCDozvil_;
                                if (GetStatusLic(ref FinalCompare, GTR.doc_start, GTR.doc_stop, GTR.name_lic) && (!string.IsNullOrEmpty(GTR.name_lic.Trim())))
                                {
                                    FinalCompare.Add(GTR);
                                }
                            }


                    }
                }


                if (FinalCompare.Count!=null)
                {
                    FinalCompare.Sort(delegate(ParamsLicense us1, ParamsLicense us2)
                    { return us1.doc_stop.CompareTo(us2.doc_stop); });
                   
                    for (int i = 0; i < FinalCompare.Count; i++)
                    {
                        if (AllTrim(SCDozvil).Trim().IndexOf(AllTrim(FinalCompare[i].name_lic).Trim()) == -1)
                        {
                        
                            SCDozvil += (SCDozvil.Length > 0 ? Environment.NewLine : "") + FinalCompare[i].name_lic.Trim();
                            if (OwnerWindows != null && OwnerWindows is MainAppForm)
                            {
                                (OwnerWindows as MainAppForm).textBoxDozvil.Text = SCDozvil;
                            }
                        }
                    }
                }

                GroupLic1.Clear(); GroupLic2.Clear();
                LicCompare1.Clear(); LicCompare2.Clear();
                CompareIndex1.Clear(); CompareIndex2.Clear(); CompareIndex3.Clear(); CompareIndex4.Clear();
                FinalCompare.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in method ShowDetailDozv "+ex.Message);
            }
                
        }

        public bool GetStateLicence(string oldLicName)
        {
            bool st = true;
            if (oldLicName.Length > 0)
            {


                IMRecordset rsOldLic = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsOldLic.Select("ID,NAME,STATE");
                    rsOldLic.SetWhere("NAME", IMRecordset.Operation.Eq, oldLicName);
                    rsOldLic.Open();
                    if (!rsOldLic.IsEOF())
                    {
                        string st_lic = rsOldLic.GetS("STATE").Trim();
                        if (st_lic.Equals("X") || st_lic.Equals("S") || st_lic.Equals("Z"))
                        {
                            st = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
             }
            return st;
        }

        public void CheckProlongatedLicences(ref List<int> licIdList)
        {

            List<int> licToExclude = new List<int>();
            LicParams lp = LicParams.GetLicParams();
            string scTotal = "";
           


            foreach (int licId in licIdList)
            {
                IMRecordset r = IMRecordset.ForRead(new RecordPtr(ICSMTbl.itblLicence, licId), "ID,NAME,CUST_TXT2,STOP_DATE,END_DATE,SIGNING_DATE");
                try
                {
                    if (!r.IsEOF())
                    {
                        string licName = r.GetS("NAME");
                        string oldLicName = r.GetS("CUST_TXT2").Trim();

                        int refId = IM.NullI;
                        if (oldLicName.Length > 0)
                        {


                            string newSpecCond = "";
                            string msg = "Обрана ліцензія '" + licName + "' має посилання на ліцензію '" + oldLicName + "' як попередницю";
                            IMRecordset rsOldLic = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                            try
                            {
                                rsOldLic.Select("ID,NAME,STOP_DATE,END_DATE,SIGNING_DATE,STATE");
                                rsOldLic.SetWhere("NAME", IMRecordset.Operation.Eq, oldLicName);
                                rsOldLic.Open();
                                if (!rsOldLic.IsEOF())
                                {
                                    string st_lic = rsOldLic.GetS("STATE").Trim();
                                ParamsLicense tmp_lic_cm = new ParamsLicense();
                                    DateTime endDate = rsOldLic.GetT("END_DATE");
                                    tmp_lic_cm.doc_stop = rsOldLic.GetT("END_DATE");
                                    refId = rsOldLic.GetI("ID");
                                    if (endDate == IM.NullT)
                                    {
                                        endDate = rsOldLic.GetT("STOP_DATE");
                                        tmp_lic_cm.doc_stop = rsOldLic.GetT("STOP_DATE");
                                    }



                                    newSpecCond = string.Format("До {0:dd.MM.yyyy} користування РЧР здійснюється на підставі ліцензії(й): реєстраційний номер(и) №{1}, дата видачі {2:dd.MM.yyyy}",
                                        endDate, rsOldLic.GetS("NAME"), rsOldLic.GetT("SIGNING_DATE"));
                                    tmp_lic_cm.doc_start = rsOldLic.GetT("SIGNING_DATE");
                                    tmp_lic_cm.name_lic = rsOldLic.GetS("NAME");
                                    tmp_lic_cm.index = refId;
                                    tmp_lic_cm.ID = licId;


                                    if ((!st_lic.Trim().Equals("X")) && (!st_lic.Trim().Equals("S")) && (!st_lic.Trim().Equals("Z")))
                                    {
                                        if (GetStatusLic(ref LicCompare1, tmp_lic_cm.doc_start, tmp_lic_cm.doc_stop, tmp_lic_cm.name_lic))
                                        {
                                            LicCompare1.Add(tmp_lic_cm);
                                        }
                                        if (GetStatusLic(ref LicCompare2, tmp_lic_cm.doc_start, tmp_lic_cm.doc_stop, tmp_lic_cm.name_lic))
                                        {
                                            LicCompare2.Add(tmp_lic_cm);
                                        }
                                    }

                                    tmp_lic_cm = null;


                                }
                                else
                                {
                                    if (lp.CheckProlongatedConsistency)
                                        MessageBox.Show(msg + ", але запис ліцензії з таким номером не знайдений в БД." + Environment.NewLine +
                                            "Перевірте цілісність даних."
                                            , CLocaliz.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            finally
                            {
                                rsOldLic.Destroy();

                            }
                            if (newSpecCond.Length > 0)
                                if (lp.AutoExcludeProlongated ||
                                    MessageBox.Show(msg + "." + Environment.NewLine + Environment.NewLine +
                                        "Оберіть 'Так' ('Да'), якщо хочете ігнорувати ліцензію, що подовжується, та додати в особливі умови дозволу наступну примітку: " + Environment.NewLine + Environment.NewLine +
                                        "------------------------" + Environment.NewLine +
                                        newSpecCond,
                                        CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question
                                        ) == DialogResult.Yes)
                                {
                                    scTotal += (scTotal.Length > 0 ? Environment.NewLine : "") + newSpecCond;
                                    if (refId != IM.NullI)
                                        licToExclude.Add(refId);
                                }
                        }
                        else
                        {
                           
                        }
                    }
                }
                finally
                {
                    r.Destroy();
                }
            }

            ////
/*
            foreach (int licId in licIdList)
            {
                IMRecordset r = IMRecordset.ForRead(new RecordPtr(ICSMTbl.itblLicence, licId), "ID,NAME,CUST_TXT2,STOP_DATE,END_DATE,SIGNING_DATE,STATE");
                try
                {
                    if (!r.IsEOF())
                    {
                        string licName = r.GetS("NAME");
                        string oldLicName = r.GetS("CUST_TXT2").Trim();
                        

                        int refId = IM.NullI;
                        if ((oldLicName.Length == 0) || (!GetStateLicence(oldLicName)))
                         {
                            string st_lic = r.GetS("STATE").Trim();
                            ParamsLicense tmp_lic_cm_no_zam = new ParamsLicense();
                            DateTime endDate = r.GetT("END_DATE");
                            int refId_ = IM.NullI;
                            tmp_lic_cm_no_zam.doc_stop = r.GetT("END_DATE");
                            refId_ = r.GetI("ID");
                            if (endDate == IM.NullT)
                            {
                                endDate = r.GetT("STOP_DATE");
                                tmp_lic_cm_no_zam.doc_stop = r.GetT("STOP_DATE");
                            }
                            tmp_lic_cm_no_zam.doc_start = r.GetT("SIGNING_DATE");
                            tmp_lic_cm_no_zam.name_lic = r.GetS("NAME");
                            tmp_lic_cm_no_zam.index = refId_;
                            tmp_lic_cm_no_zam.ID = licId;


                            if ((!st_lic.Trim().Equals("X")) && (!st_lic.Trim().Equals("S")) && (!st_lic.Trim().Equals("Z")))
                            {
                                if (GetStatusLic(ref LicCompare1, tmp_lic_cm_no_zam.doc_start, tmp_lic_cm_no_zam.doc_stop, tmp_lic_cm_no_zam.name_lic))
                                {
                                    LicCompare1.Add(tmp_lic_cm_no_zam);
                                }


                                if (GetStatusLic(ref LicCompare2, tmp_lic_cm_no_zam.doc_start, tmp_lic_cm_no_zam.doc_stop, tmp_lic_cm_no_zam.name_lic))
                                {
                                    LicCompare2.Add(tmp_lic_cm_no_zam);
                                }
                            }

                        }

                    }
                }
                finally
                {
                    r.Destroy();
                }

            }
            */
            foreach (int idToExclude in licToExclude)
                licIdList.Remove(idToExclude);


                GetStatusLic1(ref GroupLic1, ref LicCompare1, ref CompareIndex1);
                GetStatusLic2(ref GroupLic2, ref LicCompare2, ref CompareIndex2);
                GetFreeIndex(ref GroupLic1, ref GroupLic2, ref LicCompare1, ref LicCompare2, ref CompareIndex1, ref CompareIndex2, ref CompareIndex3);

                //ShowDetailDozv(scTotal, ref licenceListId);


        }

        internal virtual Dictionary<int, CLicence> FindLicences(string region, ref CLicence.Band[] txBands, ref CLicence.Band[] rxBands)
        {
            string standard="";
            if (objStation.Table != "EARTH_STATION")
            {
                IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                rsFreqPlan.Select("NAME");
                int PlanID = objStation.GetI("PLAN_ID");
                rsFreqPlan.SetWhere("ID", IMRecordset.Operation.Eq, PlanID);
                try
                {
                    rsFreqPlan.Open();
                    if (!rsFreqPlan.IsEOF())
                        standard = rsFreqPlan.GetS("NAME");
                }
                finally
                {
                    rsFreqPlan.Close();
                    rsFreqPlan.Destroy();
                }
            }
            return CLicence.FindLicences(objStation.GetI("OWNER_ID"), region, ref txBands, ref rxBands, Standard, standard);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        private string GetTableName(int appId)
        {
            string tableName = "";
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,OBJ_ID1,OBJ_TABLE,STANDARD");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    tableName = rs.GetS("OBJ_TABLE");
                }
                rs.Close();
            }
            finally
            {
                rs.Final();
            }

            return tableName;
        }

        private int GetIDStation(int appId)
        {
            int ID = IM.NullI;
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,OBJ_ID1,OBJ_TABLE,STANDARD");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    ID = rs.GetI("OBJ_ID1");
                }
                rs.Close();
            }
            finally
            {
                rs.Final();
            }

            return ID;
        }
        

        public bool GetActualLicenceList(bool interactive, ref List<int> lcIds, ref bool isInterrupt)
        {
            bool retVal = false;
            LicParams licParams = LicParams.GetLicParams();

            if (isInterrupt == false)
            {
                string regions = GetProvince();
                List<string> regionList = new List<string>();
                if (regions != null)
                {
                    while (regions.Length > 0)
                    {
                        string region = "";
                        int delimPos = regions.IndexOf(',');
                        if (delimPos > -1)
                        {
                            region = regions.Substring(0, delimPos);
                            regions = regions.Remove(0, delimPos + 1);
                        }
                        else
                        {
                            region = regions;
                            regions = "";
                        }
                        if (region.Length > 0 && !regionList.Contains(region))
                            regionList.Add(region);
                    }


                    lcIds = new List<int>();
                    foreach (string region in regionList)
                    {

                         CLicence.Band[] rxBands = { };
                         CLicence.Band[] txBands = { };



                         string tableName = GetTableName(ApplID);
                         if (tableName != MobStaApp.TableName)
                         {
                             rxBands = GetRxTxBands(RxTx.Rx, region);
                             txBands = GetRxTxBands(RxTx.Tx, region); 
                         }
                         


                         BaseAppClass appl = GetAppl(ApplID, false);

                         
                        /*
                         ///Доработка
                         string tableName = GetTableName(ApplID);
                         if (tableName != MobStaApp.TableName)
                         {
                             rxBands = GetRxTxBands(RxTx.Rx, region);
                             txBands = GetRxTxBands(RxTx.Tx, region);
                         }
                         else if (tableName == MobStaApp.TableName)
                         {
                             rxBands = GetRxTxBands(RxTx.Rx, region);
                             txBands = GetRxTxBands(RxTx.Tx, region);
                         }
                         /* 
                         ///

                         BaseAppClass appl = GetAppl(ApplID, false);
                        /*
                         if (appl != null)
                         {
                             if (appl.objTableName == MicrowaveApp.TableName)
                             {
                                 ///Доработка
                                 if (appl != null)
                                 {
                                     rxBands = GetRxTxBands(RxTx.Rx, region);
                                     txBands = GetRxTxBands(RxTx.Tx, region);
                                 }
                             }
                             else if (appl.objTableName == EarthApp.TableName)
                             {
                                 if (appl != null)
                                 {
                                     rxBands = GetRxTxBands(RxTx.Rx, region);
                                     txBands = GetRxTxBands(RxTx.Tx, region);
                                 }
                             }
                             else if (appl.objTableName == MobSta2App.TableName)
                             {
                                 if (appl != null)
                                 {
                                     rxBands = GetRxTxBands(RxTx.Rx, region);
                                     txBands = GetRxTxBands(RxTx.Tx, region);
                                 }
                             }
                         }
                         */

                         

                       
                            
                       

                         
                        Dictionary<int, CLicence> lics = FindLicences(region, ref txBands, ref rxBands);
                        if (lics.Count == 0)
                        {
                            if (tableName == MobStaApp.TableName)
                            {
                                //CLicence.Band[] rxBands_ = { };
                                //CLicence.Band[] txBands_ = { };
                                lics = CLicence.FindLicences(objStation.GetI("OWNER_ID"), region, ref txBands, ref rxBands, Standard, "");
                            }
                            else
                            {
                                lics = CLicence.FindLicences(objStation.GetI("OWNER_ID"), region, ref txBands, ref rxBands, Standard, "");
                            }
                        
                        }

                        if (tableName == "EARTH_STATION")
                        {
                            rxBands = GetRxTxBands(RxTx.Rx, region);
                            txBands = GetRxTxBands(RxTx.Tx, region);
                            lics = CLicence.FindLicences(objStation.GetI("OWNER_ID"), region, ref txBands, ref rxBands, Standard, "");
                        }

                        string licIdList = "";
                        foreach (KeyValuePair<int,CLicence> licId in lics)
                            licIdList += (licIdList.Length > 0 ? "," : "") + licId.Key.ToString();

                        

                        string standard = "";
                        if (tableName != "EARTH_STATION")
                        {
                            IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                            rsFreqPlan.Select("NAME");
                            int PlanID = objStation.GetI("PLAN_ID");
                            rsFreqPlan.SetWhere("ID", IMRecordset.Operation.Eq, PlanID);
                            try
                            {
                                rsFreqPlan.Open();
                                if (!rsFreqPlan.IsEOF())
                                    standard = rsFreqPlan.GetS("NAME");
                            }
                            finally
                            {
                                rsFreqPlan.Close();
                                rsFreqPlan.Destroy();
                            }
                        }


                        int ownerId = objStation.GetI("OWNER_ID");
                        
                        bool gapsFound = false;
                        foreach (CLicence.Band b in txBands)
                            if (b.GetGaps().Length > 0)
                            { gapsFound = true; break; }
                        foreach (CLicence.Band b in rxBands)
                            if (b.GetGaps().Length > 0)
                            { gapsFound = true; break; }
                         


                        if (licIdList != "")
                        {
                            List<int> vectChannel2 = new List<int>();
                            MobStaApp appMob = null;
                            appl = GetAppl(ApplID, false);
                            if (appl != null)
                            {
                                if (appl.objTableName == MobStaApp.TableName)
                                {
                                    if (appl != null)
                                    {
                                        appMob = ((MobStaApp)(appl));
                                        bool isLoad = appMob.LoadChannel(1);
                                        if (isLoad)
                                        {
                                            for (int r = 0; r < appMob.msvSectors.Count; r++)
                                            {
                                                vectChannel2 = appMob.msvSectors[r];

                                                IMRecordset rsChal2 = new IMRecordset(ICSMTbl.ChAllotments, IMRecordset.Mode.ReadOnly);
                                                try
                                                {

                                                    rsChal2.Select("ID,LIC_ID,CHANNELS");
                                                    string additional = CLicence.GetChalWhereClause(ownerId, GetProvince(), radioTech, standard, licParams.ActualDate, licParams.UseLicStatuses);
                                                    additional += " and LIC_ID in (" + licIdList + ")";
                                                    rsChal2.SetAdditional(additional);
                                                    rsChal2.OrderBy("Licence.START_DATE", OrderDirection.Ascending);
                                                    for (rsChal2.Open(); !rsChal2.IsEOF(); rsChal2.MoveNext())
                                                    {
                                                        List<int> vectTmpChannel = ConvertType.ToChanNumbList(rsChal2.GetS("CHANNELS"));
                                                        foreach (int iter_vectTmpChannel in vectTmpChannel)
                                                        {
                                                            bool blExit = false;
                                                            foreach (int vecIter in vectChannel2)
                                                                if (vecIter == iter_vectTmpChannel)
                                                                {
                                                                    blExit = true; //Нет смысла дальше проверять каналы, так как лицензия уже добавлена
                                                                    if (!licenceListId.Contains(rsChal2.GetI("LIC_ID")))
                                                                        licenceListId.Add(rsChal2.GetI("LIC_ID"));
                                                                    break;
                                                                }
                                                            if (blExit)
                                                                break;
                                                        }
                                                    }
                                                }
                                                finally
                                                {
                                                    rsChal2.Destroy();
                                                }
                                            }

                                        }

                                    }
                                }
                            }

                            if (appMob != null) appMob.Dispose();
                        }
                        licenceListId.Sort();

                        if (!licParams.IgnoreBandGaps)
                        {
                            if (appl != null)
                            {
                                //if ((appl.objTableName != MicrowaveApp.TableName) && (appl.objTableName != EarthApp.TableName) && (appl.objTableName != MobSta2App.TableName))
                                if (appl.objTableName == MobStaApp.TableName) 
                                {

                                    List<int> RemoveLic = new List<int>();
                                    foreach (int licId in lics.Keys)
                                    {
                                        if (!licenceListId.Contains(licId))
                                        {
                                            RemoveLic.Add(licId);
                                        }
                                    }

                                    foreach (int lk in RemoveLic)
                                    {
                                        lics.Remove(lk);
                                    }

                                }
                            }
                        }

                        
                            //rxBands = GetRxTxBands(RxTx.Rx, region);
                            //txBands = GetRxTxBands(RxTx.Tx, region);

                            //foreach (CLicence.Band sb in rxBands)
                            //{
                            //foreach (CLicence l in sb.RefLicences)
                            //{
                            //foreach (KeyValuePair<int, CLicence> item_ in lics)
                            //{
                            //if (item_.Key != l.Id)
                            //{
                            //break;
                            //}
                            //}
                            //}
                            //}
                        //}
                        
                       //////////////////

                       if (appl != null) appl.Dispose();
                       appl = null;
                        
                        //if (!licParams.AutoLinkLicences || !licParams.IgnoreBandGaps)
                       if (!licParams.AutoLinkLicences || !licParams.IgnoreBandGaps && gapsFound)
                        {
                            
                            FProvincesList fLicList = new FProvincesList();  // actually, this is checklist of any objects
                            fLicList.Text = CLocaliz.TxT("Select licences for region") + " '" + region + "'";
                            fLicList.lbCaption.Text = CLocaliz.TxT("Actual date") + ": " + LicParams.GetLicParams().ActualDate.ToString("d")
                                + ", APPL.ID = " + applID.ToString();
                            foreach (CLicence lic in lics.Values)
                                fLicList.chlbProvs.Items.Add(lic, true);

                            if ((txBands.Length>0) || (rxBands.Length>0))
                            {
                                fLicList.tbComment.Text =
                               CLocaliz.TxT("Tx Freqs:") + Environment.NewLine + ExtractLicInfo(txBands) + Environment.NewLine +
                               CLocaliz.TxT("Rx Freqs:") + Environment.NewLine + ExtractLicInfo(rxBands);
                                fLicList.tbComment.BorderStyle = BorderStyle.Fixed3D;
                                int heightMod = fLicList.chlbProvs.Height / 3 * 2;
                                fLicList.chlbProvs.Height = fLicList.chlbProvs.Height - heightMod;
                                fLicList.tbComment.Top = fLicList.tbComment.Top - heightMod;
                                fLicList.tbComment.Height = fLicList.tbComment.Height + heightMod;
                            }

                           /*
                            fLicList.tbComment.Text =
                                CLocaliz.TxT("Tx Freqs:") + Environment.NewLine + ExtractLicInfo(txBands) + Environment.NewLine +
                                CLocaliz.TxT("Rx Freqs:") + Environment.NewLine + ExtractLicInfo(rxBands);
                            fLicList.tbComment.BorderStyle = BorderStyle.Fixed3D;
                            int heightMod = fLicList.chlbProvs.Height / 3 * 2;
                            fLicList.chlbProvs.Height = fLicList.chlbProvs.Height - heightMod;
                            fLicList.tbComment.Top = fLicList.tbComment.Top - heightMod;
                            fLicList.tbComment.Height = fLicList.tbComment.Height + heightMod;
                            */ 
                            

                            if (!interactive)
                            {
                                //lcIds = new List<int>();
                                DialogResult res = fLicList.ShowDialog();
                                if (res == DialogResult.OK)
                                {
                                    foreach (object obj in fLicList.chlbProvs.CheckedItems)
                                        if (!lcIds.Contains(((CLicence)obj).Id))
                                            lcIds.Add(((CLicence)obj).Id);
                                    retVal = true;
                                }
                                else
                                {
                                    isInterrupt = true;
                                }
                            }
                            else
                            {
                                //lcIds = new List<int>();
                                foreach (int licId in lics.Keys)
                                    if (!lcIds.Contains(licId))
                                        lcIds.Add(licId);

                                retVal = true;
                                /*
                                if (lcIds.Count > 0)
                                {
                                    retVal = true;
                                }
                                else
                                {
                                    foreach (int licId in lics.Keys)
                                        if (!lcIds.Contains(licId))
                                            lcIds.Add(licId);

                                    retVal = true;
                                }
                                 */ 
                            }
                        }
                        else
                        {
                            if (!interactive)
                            {
                                FProvincesList fLicList = new FProvincesList();  // actually, this is checklist of any objects
                                fLicList.Text = CLocaliz.TxT("Select licences for region") + " '" + region + "'";
                                fLicList.lbCaption.Text = CLocaliz.TxT("Actual date") + ": " + LicParams.GetLicParams().ActualDate.ToString("d")
                                    + ", APPL.ID = " + applID.ToString();
                                foreach (CLicence lic in lics.Values)
                                    fLicList.chlbProvs.Items.Add(lic, true);


                                if ((txBands.Length > 0) || (rxBands.Length > 0))
                                {
                                    fLicList.tbComment.Text =
                                   CLocaliz.TxT("Tx Freqs:") + Environment.NewLine + ExtractLicInfo(txBands) + Environment.NewLine +
                                   CLocaliz.TxT("Rx Freqs:") + Environment.NewLine + ExtractLicInfo(rxBands);
                                    fLicList.tbComment.BorderStyle = BorderStyle.Fixed3D;
                                    int heightMod = fLicList.chlbProvs.Height / 3 * 2;
                                    fLicList.chlbProvs.Height = fLicList.chlbProvs.Height - heightMod;
                                    fLicList.tbComment.Top = fLicList.tbComment.Top - heightMod;
                                    fLicList.tbComment.Height = fLicList.tbComment.Height + heightMod;
                                }

                                /*

                                fLicList.tbComment.Text =
                                    CLocaliz.TxT("Tx Freqs:") + Environment.NewLine + ExtractLicInfo(txBands) + Environment.NewLine +
                                    CLocaliz.TxT("Rx Freqs:") + Environment.NewLine + ExtractLicInfo(rxBands);
                                fLicList.tbComment.BorderStyle = BorderStyle.Fixed3D;
                                int heightMod = fLicList.chlbProvs.Height / 3 * 2;
                                fLicList.chlbProvs.Height = fLicList.chlbProvs.Height - heightMod;
                                fLicList.tbComment.Top = fLicList.tbComment.Top - heightMod;
                                fLicList.tbComment.Height = fLicList.tbComment.Height + heightMod;
                                 */ 


                                //lcIds = new List<int>();
                                DialogResult res = fLicList.ShowDialog();
                                if (res == DialogResult.OK)
                                {
                                    foreach (object obj in fLicList.chlbProvs.CheckedItems)
                                        if (!lcIds.Contains(((CLicence)obj).Id))
                                            lcIds.Add(((CLicence)obj).Id);
                                    retVal = true;
                                }
                                else
                                {
                                    isInterrupt = true;
                                }
                            }
                            else
                            {
                                //lcIds = new List<int>();
                                foreach (int licId in lics.Keys)
                                    if (!lcIds.Contains(licId))
                                        lcIds.Add(licId);


                                retVal = true;
                                /*
                                if (lcIds.Count > 0)
                                {
                                    retVal = true;
                                }
                                else
                                {
                                    foreach (int licId in lics.Keys)
                                        if (!lcIds.Contains(licId))
                                            lcIds.Add(licId);

                                    retVal = true;
                                }
                                 */ 
                            }
                        }

                       lics = null;
                       GC.Collect();
                       GC.WaitForPendingFinalizers();
                    }
                }
            }

            return retVal;
        }

        internal string ExtractLicInfo(CLicence.Band[] bands)
        {
            string licInfo = "";
            foreach (CLicence.Band sb in bands)
            {
                string licList = "";
                foreach (CLicence l in sb.RefLicences) licList += (licList.Length > 0 ? "," : "") + l.Name;
                if (licList == "") licList = "<no licence>";
                string gapList = "";
                CLicence.Band[] gaps = sb.GetGaps();
                if (gaps.Length == 0)
                    gapList = "<no gaps>";
                else foreach (CLicence.Band gb in gaps)
                        gapList += (gapList.Length == 0 ? "gap" + (gaps.Length > 1 ? "s" : "") + ": " : ", ") + string.Format("{0}-{1}", gb.FLow, gb.FHigh);

                licInfo += string.Format("{0}:\t" + licList + ", " + gapList + Environment.NewLine, sb.FCenter);
            }
            return licInfo;
        }


        //============================================================
        /// <summary>
        /// Загрузка грида лицензий
        /// </summary>
        protected virtual void InitLicenceGrid()
        {
            try
            {
                gridLicence.Rows.Clear();
                foreach (int ID in licenceListId)
                {
                    IMRecordset r = IMRecordset.ForRead(new RecordPtr(ICSMTbl.itblLicence, ID), "ID,NAME,START_DATE,STOP_DATE,STANDARD");
                    try
                    {
                        if (!r.IsEOF())
                        {
                            string[] row = new string[] { r.GetI("ID").ToString(), 
                                                r.GetS("NAME"),
                                                r.GetT("START_DATE").ToString("dd.MM.yy"),
                                                r.GetT("STOP_DATE").ToString("dd.MM.yy"),
                                           };
                            try
                            {
                                gridLicence.Rows.Add(row);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error in method Add for [gridLicence.Rows] "+ex.Message);
                            }
                        }
                    }
                    finally
                    {
                        r.Destroy();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //===================================================
        /// <summary>
        /// Отображаем лицензию
        /// </summary>
        public virtual void ShowLicence(int row, int column)
        {
            try
            {
                if (row >= 0)
                {
                    int ID = Convert.ToInt32(gridLicence.Rows[row].Cells[0].Value.ToString());
                    RecordPtr recLicence = new RecordPtr(ICSMTbl.itblLicence, ID);
                    recLicence.UserEdit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
            }
        }
        /// <summary>
        /// Устареет
        /// </summary>
        /// <param name="Position"></param>
        /// <returns></returns>
        public int CopyPositionRecord(IMObject Position)
        {
            int id = IM.NullI;

            IMRecordset XnrfaPosition = null;
            try
            {
                XnrfaPosition = new IMRecordset(PlugTbl.itblXnrfaPositions, IMRecordset.Mode.ReadWrite);
                XnrfaPosition.Select("ID,LONGITUDE,LATITUDE,CREATED_BY,DATE_CREATED,CITY_ID,NAME,ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK");
                XnrfaPosition.Open();

                XnrfaPosition.AddNew();
                id = IM.AllocID(PlugTbl.itblXnrfaPositions, 1, -1);
                XnrfaPosition.Put("ID", id);
                if (Position != null)
                {
                    XnrfaPosition.Put("LONGITUDE", Position.GetD("LONGITUDE"));
                    XnrfaPosition.Put("LATITUDE", Position.GetD("LATITUDE"));
                    XnrfaPosition.Put("CREATED_BY", Position.GetS("CREATED_BY"));
                    XnrfaPosition.Put("DATE_CREATED", Position.GetT("DATE_CREATED"));
                    XnrfaPosition.Put("CITY_ID", Position.GetI("CITY_ID"));
                    XnrfaPosition.Put("NAME", Position.GetS("NAME"));
                    XnrfaPosition.Put("ADDRESS", Position.GetS("ADDRESS"));
                    XnrfaPosition.Put("PROVINCE", Position.GetS("PROVINCE"));
                    XnrfaPosition.Put("SUBPROVINCE", Position.GetS("SUBPROVINCE"));
                    XnrfaPosition.Put("CITY", Position.GetS("CITY"));
                    XnrfaPosition.Put("CUST_TXT1", Position.GetS("CUST_TXT1"));
                    XnrfaPosition.Put("REMARK", Position.GetS("REMARK"));
                }
                XnrfaPosition.Update();
            }
            finally
            {
                if (XnrfaPosition != null)
                {
                    XnrfaPosition.Close();
                    XnrfaPosition.Destroy();
                }
            }

            return id;
        }

        /// <summary>
        /// Копирует запись с POSITION-таблицы в XNRFA_POSITION
        /// Предоствляет возможность перекрыть долготу/широту
        /// </summary>      
        /// <returns></returns>
        public int CopyPositionToXNRFA(string tableName, int tableId, double longitude, double latitude)
        {
            int id = IM.NullI;

            IMRecordset XnrfaPosition = null;
            try
            {
                XnrfaPosition = new IMRecordset(PlugTbl.itblXnrfaPositions, IMRecordset.Mode.ReadWrite);
                XnrfaPosition.Select("ID,LONGITUDE,LATITUDE,CREATED_BY,DATE_CREATED,CITY_ID,NAME,ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK");
                XnrfaPosition.Open();

                XnrfaPosition.AddNew();
                id = IM.AllocID(PlugTbl.itblXnrfaPositions, 1, -1);
                XnrfaPosition.Put("ID", id);

                IMObject position = IMObject.LoadFromDB(tableName, tableId);
                if (position != null)
                {
                    if (longitude == IM.NullD && latitude == IM.NullD)
                    {
                        XnrfaPosition.Put("LONGITUDE", position.GetD("LONGITUDE"));
                        XnrfaPosition.Put("LATITUDE", position.GetD("LATITUDE"));
                    }
                    else
                    {
                        XnrfaPosition.Put("LONGITUDE", longitude);
                        XnrfaPosition.Put("LATITUDE", latitude);
                    }

                    XnrfaPosition.Put("CREATED_BY", position.GetS("CREATED_BY"));
                    XnrfaPosition.Put("DATE_CREATED", position.GetT("DATE_CREATED"));
                    XnrfaPosition.Put("CITY_ID", position.GetI("CITY_ID"));
                    XnrfaPosition.Put("NAME", position.GetS("NAME"));
                    XnrfaPosition.Put("ADDRESS", position.GetS("ADDRESS"));
                    XnrfaPosition.Put("PROVINCE", position.GetS("PROVINCE"));
                    XnrfaPosition.Put("SUBPROVINCE", position.GetS("SUBPROVINCE"));
                    XnrfaPosition.Put("CITY", position.GetS("CITY"));
                    XnrfaPosition.Put("CUST_TXT1", position.GetS("CUST_TXT1"));
                    XnrfaPosition.Put("REMARK", position.GetS("REMARK"));
                    position.Dispose();
                }
                XnrfaPosition.Update();
            }
            finally
            {
                if (XnrfaPosition != null)
                {
                    XnrfaPosition.Close();
                    XnrfaPosition.Destroy();
                }
            }

            return id;
        }

        /// <summary>
        /// Устареет
        /// </summary>
        /// <param name="XnrfaPosID"></param>
        /// <param name="tableName"></param>
        /// <param name="Longitude"></param>
        /// <param name="Latitude"></param>
        /// <returns></returns>
        public int CopyPositionRecord(int XnrfaPosID, string tableName)
        {
            int RetVal = IM.NullI;

            IMRecordset XnrfaPosition = null;
            try
            {
                XnrfaPosition = new IMRecordset(PlugTbl.itblXnrfaPositions, IMRecordset.Mode.ReadOnly);
                XnrfaPosition.Select("ID,LONGITUDE,LATITUDE,CREATED_BY,DATE_CREATED,CITY_ID,NAME,ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK");
                XnrfaPosition.SetWhere("ID", IMRecordset.Operation.Eq, XnrfaPosID);

                int CityID = IM.NullI;

                double X = IM.NullD;
                double Y = IM.NullD;

                string ConnectedUser = string.Empty;
                string NameAddr = string.Empty;
                string Addr = string.Empty;
                string Province = string.Empty;
                string Subprovince = string.Empty;
                string CityName = string.Empty;
                string Txt1 = string.Empty;
                string Remark = string.Empty;

                DateTime DataCreated = IM.NullT;

                XnrfaPosition.Open();
                if (!XnrfaPosition.IsEOF())
                {
                    X = XnrfaPosition.GetD("LONGITUDE");
                    Y = XnrfaPosition.GetD("LATITUDE");
                    ConnectedUser = XnrfaPosition.GetS("CREATED_BY");
                    DataCreated = XnrfaPosition.GetT("DATE_CREATED");
                    CityID = XnrfaPosition.GetI("CITY_ID");
                    NameAddr = XnrfaPosition.GetS("NAME");
                    Addr = XnrfaPosition.GetS("ADDRESS");
                    Province = XnrfaPosition.GetS("PROVINCE");
                    Subprovince = XnrfaPosition.GetS("SUBPROVINCE");
                    CityName = XnrfaPosition.GetS("CITY");
                    Txt1 = XnrfaPosition.GetS("CUST_TXT1");
                    Remark = XnrfaPosition.GetS("REMARK");

                    int ID_pos = IM.AllocID(tableName, 1, -1);

                    using (IMObject newPos = IMObject.New(tableName))
                    {
                        newPos.Put("ID", ID_pos);
                        newPos.Put("CODE", ID_pos.ToString());
                        newPos.Put("TABLE_NAME", tableName);
                        newPos.Put("CSYS", "4DMS");
                        newPos.Put("LONGITUDE", X.DmsToDec());
                        newPos.Put("X", X);
                        newPos.Put("LATITUDE", Y.DmsToDec());
                        newPos.Put("Y", Y);
                        newPos.Put("DATUM", 4);
                        newPos.Put("CREATED_BY", ConnectedUser);
                        newPos.Put("DATE_CREATED", DataCreated);
                        newPos.Put("CITY_ID", CityID);
                        newPos.Put("COUNTRY_ID", "UKR");
                        newPos.Put("ASL", 0);
                        newPos.Put("NAME", NameAddr);
                        newPos.Put("ADDRESS", Addr);
                        newPos.Put("PROVINCE", Province);
                        newPos.Put("SUBPROVINCE", Subprovince);
                        newPos.Put("CITY", CityName);
                        newPos.Put("CUST_TXT1", Txt1);
                        newPos.Put("REMARK", Remark);
                        newPos.SaveToDB();
                    }

                    RetVal = ID_pos;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (XnrfaPosition != null)
                {
                    XnrfaPosition.Close();
                    XnrfaPosition.Destroy();
                }
            }

            return RetVal;
        }
        #endregion
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion        

        protected void IndicateDifference(Cell cell, bool valueDiffers)
        {
            // #7236 18/10/2013 - remove indication
            //cell.TextColor = valueDiffers ? (cell.BackColor == Color.Red ? Color.Wheat : Color.Red) : SystemColors.WindowText;
            
            //cell.FontStyle = valueDiffers ? cell.FontStyle | FontStyle.Bold : cell.FontStyle & ~(FontStyle.Bold);
        }            
    }
}
