﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    internal class ERPStation : BaseStation
    {
        public class ERP
        {
            private Power _horizontal = new Power();
            private Power _vertical = new Power();
            private Power _overall = new Power();

            public Power Horizontal { get { return _horizontal; } }
            public Power Vertical { get { return _vertical; } }
            public Power Overall { get { return _overall; } }
        }

        protected Power _power = new Power();
        protected double _gain = 0.0;
        public Power Power { get { return _power; } }

        public double Gain { get { return _gain; } }

        public GainDiagramm HorizontalDiagramm { get; set; }
        public GainDiagramm VerticalDiagramm { get; set; }

        public ERP Erp { get; set; }

        public string Polarization { get; set; }

        public double FeederLength { get; set; }
        public double FeederLosses { get; set; }

        public ERPStation()
        {
            HorizontalDiagramm = new GainDiagramm();
            VerticalDiagramm = new GainDiagramm();
            Erp = new ERP();
        }

        public virtual string Direction
        {
            get
            {
                string direction = "ND";
                if (!(HorizontalDiagramm.IsEmpty && VerticalDiagramm.IsEmpty))
                {
                    if ((VerticalDiagramm.IsOmni && (HorizontalDiagramm.IsOmni || HorizontalDiagramm.IsEmpty)) ||
                          (HorizontalDiagramm.IsOmni && VerticalDiagramm.IsEmpty))
                        direction = "ND";
                    else
                        direction = "D";
                }
                return direction;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    switch (value)
                    {
                        case "D":
                            VerticalDiagramm.IsOmni = false;
                            HorizontalDiagramm.IsOmni = false;                            
                            break;
                        case "ND":
                            VerticalDiagramm.IsOmni = true;
                            HorizontalDiagramm.IsOmni = true;
                            break;
                        default:
                            throw new Exception("Bad value of direction");
                    }
                }
            }
        }

        public void CalculateERP()
        {
            double power = Power[PowerUnits.dBW];
            double feederLoss = FeederLength * FeederLosses;

            if (Polarization == "V")
            {
                List<double> diagDbl = new List<double>();
                for (double angle = 0; angle < 360.0; angle += 10.0)
                {
                    double gainAlpha = VerticalDiagramm.GetGainByAngle(angle);
                    double erp = power - feederLoss + gainAlpha;
                    diagDbl.Add(Math.Round(erp * 100.0) / 100.0);
                }
                Erp.Vertical[PowerUnits.dBW] = diagDbl.Max();
                Erp.Horizontal[PowerUnits.dBW] = IM.NullD;
                Erp.Overall[PowerUnits.dBW] = diagDbl.Max();
            }

            if (Polarization == "H")
            {
                List<double> diagDbl = new List<double>();
                for (double angle = 0; angle < 360.0; angle += 10.0)
                {
                    double gainAlpha = HorizontalDiagramm.GetGainByAngle(angle);
                    double erp = power - feederLoss + gainAlpha;
                    diagDbl.Add(Math.Round(erp * 100.0) / 100.0);
                }
                Erp.Horizontal[PowerUnits.dBW] = diagDbl.Max();
                Erp.Vertical[PowerUnits.dBW] = IM.NullD;
                Erp.Overall[PowerUnits.dBW] = diagDbl.Max();
            }

            if (Polarization == "M")
            {
                double erpMax = -1E+10;
                List<double> diagH = new List<double>();
                List<double> diagV = new List<double>();
                for (double angle = 0; angle < 360.0; angle += 10.0)
                {
                    double gainAlphaH = HorizontalDiagramm.GetGainByAngle(angle);
                    double gainAlphaV = VerticalDiagramm.GetGainByAngle(angle);
                    double erpv = power - feederLoss + gainAlphaV;
                    double erph = power - feederLoss + gainAlphaH;

                    diagH.Add(erph);
                    diagV.Add(erpv);

                    double erpvh = 10.0 * Math.Log10(Math.Pow(10.0, erpv / 10.0) + Math.Pow(10.0, erph / 10.0));

                    if (erpMax < erpvh)
                        erpMax = erpvh;

                }

                Erp.Overall[PowerUnits.dBW] = erpMax;
                Erp.Horizontal[PowerUnits.dBW] = diagH.Max();
                Erp.Vertical[PowerUnits.dBW] = diagV.Max();
            }
        }
    }
}
