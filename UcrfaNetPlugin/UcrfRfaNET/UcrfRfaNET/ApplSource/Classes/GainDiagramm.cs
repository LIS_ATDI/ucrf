﻿using System.Collections.Generic;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    //Нада будет перенести куда нибудь
    public class GainDiagramm
    {
        private string _diagrammStr;
        private double _gain = 0.0;
        private bool _omni = false;

        private Dictionary<double, double> _gainDictionary;

        public const double MinAngle = 0.0;
        public const double MaxAngle = 360.0;

        public bool IsOmni
        {
            get
            {
                return (_diagrammStr == "OMNI") || _omni;
            }
            set
            {
                _omni = value;
                if (!_omni && _diagrammStr == "OMNI")
                {
                    List<double> data = new List<double>();
                    for (double angle = MinAngle; angle < MaxAngle; angle += 10.0)
                        data.Add(_gain);

                    _diagrammStr = HelpFunction.GainedDiagToString(data, 10, _gain);
                }
            }
        }

        public bool IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(_diagrammStr);
            }

        }

        public double GetGainByAngle(double angle)
        {
            if (IsOmni)
                return _gain;

            if (_gainDictionary != null)
                if (_gainDictionary.Count > 0)
                    return _gainDictionary[angle];
            return 0.0;
        }

        public string DiagrammAsString
        {
            get
            {
                string diag = "";
                if (_omni)
                {
                    diag = "OMNI";
                }
                else
                {
                    List<double> data = new List<double>();

                    for (double angle = MinAngle; angle < MaxAngle; angle += 10.0)
                    {
                        if (_gainDictionary.Count > 0)
                            data.Add(_gainDictionary[angle]);
                        else
                            data.Add(_gain);
                    }

                    diag = HelpFunction.GainedDiagToString(data, 10, _gain);
                }
                return diag;
            }
            set
            {
                _diagrammStr = value;
                BuildDisctonary();
                _omni = (_diagrammStr == "OMNI");
            }
        }

        public double Gain
        {
            get
            {
                return _gain;
            }
            set
            {
                _gain = value;
                BuildDisctonary();
            }
        }

        public double Max
        {
            get
            {
                if (IsOmni)
                    return Gain;
                List<double> diagDbl = HelpFunction.StringToGainedDiag(_diagrammStr, _gain);
                if (diagDbl.Count > 0)
                    return diagDbl.Max();
                return IM.NullD;
            }
        }

        private void BuildDisctonary()
        {
            List<double> diagDbl;
            if (string.IsNullOrEmpty(_diagrammStr))
            {
                diagDbl = new List<double>();
                for (int i = 0; i < 36; i++)
                    diagDbl.Add(_gain);
            }
            else
            {
                diagDbl = HelpFunction.StringToGainedDiag(_diagrammStr, _gain);
            }

            _gainDictionary = new Dictionary<double, double>();

            int index = 0;
            if (diagDbl.Count > 0)
            {
                for (double angle = MinAngle; angle < MaxAngle; angle += 10.0, index++)
                    _gainDictionary[angle] = diagDbl[index];
            }
        }
    };
}
