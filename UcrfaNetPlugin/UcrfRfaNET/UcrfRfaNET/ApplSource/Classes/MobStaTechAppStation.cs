﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStaTechAppStation : BaseStation
    {
        public MobStationTechAntenna techStationAntena { get; set; }
        public Equipment techStationEquip { get; set; }
        
        public int PosId { get; set; }

        public string Standart { get; set; }
        public string Defin { get; set; }
        public string ClassStat { get; set; }

        public string NameRez { get; set; }


        private Power power = new Power();
        public Power Power { get { return power; } }

        public string DesigEmiss { get; set; }
        public string CertNumb { get; set; }
        public DateTime CertDate { get; set; }
        public string TypeAnten { get; set; }
        public double Koef { get; set; }
        public double WideAnten { get; set; }
        public double Agl { get; set; }
        public double Azimuth { get; set; }
        public double Fider { get; set; }
        public string Call { get; set; }
        
        //
        public string MMSI { get; set; }

        public double Duplex { get; set; }
        public double Accept { get; set; }
        public double Send { get; set; }
        
        //
        public string Oznach_REZ { get; set; }

        public int ObjRchp { get; set; }

        public string NAccept { get; set; }
        public string NSend { get; set; }

        public List<double> FreqRxList = new List<double>();
        public List<int> RxListId = new List<int>();
        public List<double> FreqTxList = new List<double>();
        public List<int> TxListId = new List<int>();

        public Dictionary<string, string> ClassStationDict = new Dictionary<string, string>();

        public int AntId { get; set; }
        public int EquipId { get; set; }
        //public PositionState Position { get; set; }

        public static readonly string TableName = ICSMTbl.itblMobStation;        

        public MobStaTechAppStation()
        {                        
        }

        /// <summary>
        /// Збереження даних при коміті
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,NETWORK_IDENT,NAME,POS_ID,DUPLEX_SPACING,CUST_TXT9,CUST_TXT6,CUST_TXT10,TX_LOSSES");
                r.Select("AZIMUTH,AGL,DESIG_EMISSION,EQUIP_ID,CLASS,PWR_ANT,ANT_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", Id);
                    
                    r.Put("POS_ID", Position.Id);
                    r.Put("ANT_ID", techStationAntena.Id);
                    r.Put("EQUIP_ID", techStationEquip.Id);

                    r.Put("NAME", Defin);
                    r.Put("CLASS", ClassStat);
                    r.Put("DUPLEX_SPACING", Duplex);
                    r.Put("CUST_TXT9", Call);
                    r.Put("TX_LOSSES", Fider);
                    r.Put("AZIMUTH", Azimuth);
                    r.Put("AGL", Agl);
                    r.Put("DESIG_EMISSION", DesigEmiss);
                    r.Put("PWR_ANT", techStationEquip.MaxPower[PowerUnits.dBW]);


                    //
                    r.Put("CUST_TXT6", MMSI);
                    r.Put("CUST_TXT10", Oznach_REZ);

                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Частоти            
            //TODO Что делает этот код? Не понятно.
            //IMRecordset freqRec = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadWrite);
            //try
            //{
            //    freqRec.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            //    freqRec.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            //    freqRec.Open();
            //    if (!freqRec.IsEOF())
            //    {
            //        freqRec.Edit();
            //        freqRec.Put("RX_FREQ", Accept);
            //        freqRec.Put("TX_FREQ", Send);
            //        freqRec.Update();
            //    }
            //}
            //finally
            //{
            //    if (freqRec.IsOpen())
            //        freqRec.Close();
            //    freqRec.Destroy();
            //}


            //Частоти               
            FreqRxList.Clear();
            FreqTxList.Clear();
            RxListId.Clear();
            TxListId.Clear();
            string[] rxArray = NAccept.Split(';');
            bool rxShow = true;
            for (int i = 0; i < rxArray.Length; i++)
            {
                double currDbl;
                if (double.TryParse(rxArray[i], out currDbl))
                    FreqRxList.Add(currDbl);
                else
                    rxShow = false;
            }
            string[] txArray = NSend.Split(';');
            bool txShow = true;
            for (int i = 0; i < txArray.Length; i++)
            {
                double currDbl;
                if (double.TryParse(txArray[i], out currDbl))
                    FreqTxList.Add(currDbl);
                else
                    txShow = false;
            }
            if (!rxShow || !txShow)
                MessageBox.Show("Wrong format in Freq!");


            //Заповнити таблицю з Diff...
            IMRecordset recDiffFreqs2 = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
            recDiffFreqs2.Select("ID,FREQUENCY,MOBSTA_ID,TR");
            recDiffFreqs2.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
            recDiffFreqs2.Open();
            while (!recDiffFreqs2.IsEOF())
            {
                string tr = recDiffFreqs2.GetS("TR");
                if (tr == "T")
                    TxListId.Add(Id);
                else
                    RxListId.Add(Id);
                recDiffFreqs2.MoveNext();
            }
            if (recDiffFreqs2.IsOpen())
                recDiffFreqs2.Close();
            recDiffFreqs2.Destroy();


            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);

            //Сохраняем частоты
            List<MobFreq> freqs = new List<MobFreq>();
            int maxCount = Math.Max(RxListId.Count, TxListId.Count);
            for (int indexFreq = 0; indexFreq < maxCount; indexFreq++)
            {
                MobFreq freq = new MobFreq();
                freq.RxMHz = IM.NullD;
                if (RxListId.Count > indexFreq)
                    if (FreqRxList.Count > indexFreq)
                        freq.RxMHz = FreqRxList[indexFreq];
                freq.TxMHz = IM.NullD;
                if (TxListId.Count > indexFreq)
                    if (FreqTxList.Count > indexFreq)
                        freq.TxMHz = FreqTxList[indexFreq];
                freqs.Add(freq);
            }
            MobFreqManager.SaveFreq(Id, MobFreqManager.TypeFreq.MobStation, freqs.ToArray());

            FillComments(ApplId);

            //Видалення тимчасових записів...
            /*IMRecordset delRec = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta, IMRecordset.Mode.ReadWrite);
            try
            {
                delRec.Select("ID");
                delRec.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                delRec.Open();
                if (!delRec.IsEOF())
                    delRec.Delete();
            }
            finally
            {
                if (delRec.IsOpen())
                    delRec.Close();
                delRec.Destroy();
            }

            IMRecordset delRecPos = new IMRecordset(PlugTbl.itblXnrfaPositions, IMRecordset.Mode.ReadWrite);
            try
            {
                delRecPos.Select("ID");
                delRecPos.SetWhere("ID", IMRecordset.Operation.Eq, PosId);

                for (delRecPos.Open(); !delRecPos.IsEOF(); delRecPos.MoveNext())
                    delRecPos.Delete();
            }
            finally
            {
                if (delRecPos.IsOpen())
                    delRecPos.Close();
                delRecPos.Destroy();
            }*/
        }

        /// <summary>
        /// Завантаження з БД звичайних(не змінених) значень для форми
        /// </summary>
        public override void Load()
        {
            fillClassStation();
            techStationAntena = new MobStationTechAntenna();
            techStationAntena.TableName = ICSMTbl.itblAntennaMob;

            techStationEquip = new MobStationEquipment();
            techStationEquip.TableName = ICSMTbl.itblEquipPmr;

            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NETWORK_IDENT,NAME,POS_ID,ANT_ID,DUPLEX_SPACING,CUST_TXT9,CUST_TXT6,CUST_TXT10,AGL,PWR_ANT");
                r.Select("DESIG_EMISSION,AZIMUTH,TX_LOSSES,EQUIP_ID,CLASS,CUST_TXT17");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");
                    PosId = r.GetI("POS_ID");
                    techStationAntena.Id = r.GetI("ANT_ID");
                    if (techStationAntena.Id != IM.NullI)
                        techStationAntena.Load();

                    Standart = r.GetS("NETWORK_IDENT");
                    Defin = r.GetS("NAME");
                    ClassStat = r.GetS("CLASS");
                    ObjRchp = r.GetI("ID");
                    Duplex = r.GetD("DUPLEX_SPACING");
                    Call = r.GetS("CUST_TXT9");
                    Fider = r.GetD("TX_LOSSES");
                    Azimuth = r.GetD("AZIMUTH");
                    Agl = r.GetD("AGL");
                    DesigEmiss = r.GetS("DESIG_EMISSION");
                    techStationEquip.MaxPower[PowerUnits.dBW] = r.GetD("PWR_ANT");
                    techStationEquip.Id = r.GetI("EQUIP_ID");                    
                    //Висновок
                    Finding = r.GetS("CUST_TXT17");

                    //
                    MMSI = r.GetS("CUST_TXT6");
                    Oznach_REZ = r.GetS("CUST_TXT10");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            Position = new PositionState();
            Position.LoadStatePosition(PosId, ICSMTbl.itblPositionWim);

            //Частоти      
            FreqRxList.Clear();
            FreqTxList.Clear();
            IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
            recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            recMobStaFreqs2.Open();
            while (!recMobStaFreqs2.IsEOF())
            {
                double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                int IdCurr = recMobStaFreqs2.GetI("ID");
                double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                if (RxFreq != IM.NullD)
                {
                    FreqRxList.Add(RxFreq);
                    RxListId.Add(IdCurr);
                }
                if (TxFreq != IM.NullD)
                {
                    FreqTxList.Add(TxFreq);
                    TxListId.Add(IdCurr);
                }
                recMobStaFreqs2.MoveNext();
            }
            if (recMobStaFreqs2.IsOpen())
                recMobStaFreqs2.Close();
            recMobStaFreqs2.Destroy();

            NAccept = string.Empty;
            if (FreqRxList.Count > 0)
                if (CheckValidityFrequency(FreqRxList))
                {
                    for (int i = 0; i < FreqRxList.Count; i++)
                        NAccept += FreqRxList[i].ToString() + ";";
                    NAccept = NAccept.Remove(NAccept.Count() - 1);
                }

            NSend = string.Empty;
            if (FreqTxList.Count > 0)
                if (CheckValidityFrequency(FreqTxList))
                {
                    for (int i = 0; i < FreqTxList.Count; i++)
                        NSend += FreqTxList[i] + ";";
                    NSend = NSend.Remove(NSend.Count() - 1);
                }

            //Обладнання
            IMRecordset equipRec = new IMRecordset(ICSMTbl.itblEquipPmr, IMRecordset.Mode.ReadOnly);
            try
            {
                equipRec.Select("ID,NAME,BW,MODULATION,MBITPS,CUST_TXT1,CUST_DAT1,DESIG_EMISSION");
                equipRec.SetWhere("ID", IMRecordset.Operation.Eq, techStationEquip.Id);
                equipRec.Open();
                if (!equipRec.IsEOF())
                {
                    techStationEquip.Name = equipRec.GetS("NAME");                    
                    techStationEquip.Certificate.Symbol = equipRec.GetS("CUST_TXT1");
                    techStationEquip.Certificate.Date = equipRec.GetT("CUST_DAT1");
                }
            }
            finally
            {
                if (equipRec.IsOpen())
                    equipRec.Close();
                equipRec.Destroy();
            }



            //Антена
            IMRecordset antenRec = new IMRecordset(ICSMTbl.itblAntennaMob, IMRecordset.Mode.ReadOnly);
            antenRec.Select("ID,NAME,H_BEAMWIDTH,GAIN");
            antenRec.SetWhere("ID", IMRecordset.Operation.Eq, techStationAntena.Id);
            try
            {
                antenRec.Open();
                if (!antenRec.IsEOF())
                {
                    techStationAntena.Name = antenRec.GetS("NAME");
                    techStationAntena.Gain = antenRec.GetD("GAIN");
                    techStationAntena.AntennaWidth = antenRec.GetD("H_BEAMWIDTH");
                }
            }
            finally
            {
                if (antenRec.IsOpen())
                    antenRec.Close();
                antenRec.Destroy();
            }
        }

        /// <summary>
        /// Разбивает строку на список double
        /// </summary>
        /// <param name="strList">строка</param>
        /// <returns>список значений</returns>
        public static List<double> ToDoubleList(string strList)
        {
            List<double> retLst = new List<double>();
            string[] parseList = strList.Split(new Char[] { ';' });
            foreach (string dblVal in parseList)
            {
                double tmpVal = Convert.ToDouble(dblVal);
                if (tmpVal != IM.NullD)
                    retLst.Add(tmpVal);
            }
            return retLst;
        }

        private void fillClassStation()
        {
            ClassStationDict.Add("FB", CLocaliz.TxT("Базова"));
            ClassStationDict.Add("FL", CLocaliz.TxT("Стаціонарна абонентська"));
            ClassStationDict.Add("FC", CLocaliz.TxT("Берегова"));
            ClassStationDict.Add("NL", CLocaliz.TxT("Морська радіонавігаційна сухопутна"));
        }

        private bool CheckValidityFrequency(List<double> FreqList)
        {
            foreach (double Freq in FreqList)
                if (Freq < 0.0)
                    return false;
            return true;
        }

    }
}
