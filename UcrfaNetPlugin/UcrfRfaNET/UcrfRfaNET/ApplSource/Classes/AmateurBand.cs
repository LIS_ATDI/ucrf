﻿using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public class AmateurBand
    {
        public int Id { get; set; }
        public string Band { get; set; }
        public double BandMin { get; set; }
        public double BandMax { get; set; }
        public string Channel { get; set; }
        public double Power { get; set; }

        public static bool IsPower { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public AmateurBand()
        {
            //Id = IM.NullI;
            BandMin = 0.0;
            BandMax = 0.0;
            Power = IM.NullD;
        }

        /// <summary>
        /// ID обрудования
        /// </summary>
        public int EquipId { get; set; }

        /// <summary>
        /// Сохранение частот
        /// </summary>
        public void Save()
        {
            IMRecordset r = new IMRecordset(AmateurStation.TableNameFe, IMRecordset.Mode.ReadWrite);
            r.Select("ID,SEL_EQUIP_ID,BAND_ID");
            try
            {
                r.Open();
                r.AddNew();
                r.Put("ID", IM.AllocID(AmateurStation.TableNameFe, 1, -1));
                r.Put("SEL_EQUIP_ID", EquipId);
                r.Put("BAND_ID", Id);
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>
        /// <param name="equipId">Id оборудования</param>
        public void Load(int freqId)
        {
            EquipId = freqId;
            Load();
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>
        public void Load()
        {
            IMRecordset rFreq = new IMRecordset(AmateurStation.TableNameBand, IMRecordset.Mode.ReadOnly);
            rFreq.Select("ID,BANDMIN,BANDMAX,POWER,CAT_OPER,TYPE_STAT");
            rFreq.SetWhere("ID", IMRecordset.Operation.Eq, EquipId);
            try
            {
                rFreq.Open();
                if (!rFreq.IsEOF())
                {
                    Id = EquipId;
                    BandMin = rFreq.GetD("BANDMIN");
                    BandMax = rFreq.GetD("BANDMAX");
                    Band = BandMin + " - " + BandMax;
                    Power = rFreq.GetD("POWER");                    
                }
            }
            finally
            {
                rFreq.Final();
            }

        }
        /// <summary>
        /// Возвращает строку с частотами передатчика
        /// </summary>
        /// <returns>строка с частотами передатчика</returns>
        public static string GetFreqs(List<AmateurBand> freqList)
        {
            string retVal = "";
            foreach (AmateurBand freq in freqList)
            {
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.ToString();                
            }
            return retVal;
        }


        public override string ToString()
        {            
            if (IsPower)
                return string.Format("{0} МГц / {1} Вт", Band, Power);
            return string.Format("{0} МГц ", Band);
        }
    }
}
