﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRS;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    //===============================================================
    /// <summary>
    /// класс станции ФМ
    /// </summary>
    internal class StationFM : BaseStation
    {
        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна    
        //===================================
        protected override void DisposeExec()
        {
            if (objEquip != null)
            {
                objEquip.Dispose();
                objEquip = null;
            }
            if (objAntenna != null)
            {
                objAntenna.Dispose();
                objAntenna = null;
            }
        }
    };

    class FmAnalogApp : FMTVBase
    {
        public static readonly string TableName = ICSMTbl.itblFM;

        public StationFM station = new StationFM();
        protected double Longitude = 0; //Долгота
        protected double Latitude = 0;  //Широта    
        protected bool NeedSavePosition = false;

        public FmAnalogApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, FmAnalogApp.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppR2;
            department = DepartmentType.VRS;
            departmentSector = DepartmentSectorType.VRS_SR;
            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT12");
            SCVisnovok = objStation.GetS("CUST_TXT13");
            SCNote = objStation.GetS("CUST_TXT14");

            if (newStation)
            {
                objStation.Put("ADM", "UKR");
                objStation.Put("STANDARD", _radioTech);

                IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                r.Select("ID,COD");
                r.SetWhere("COD", IMRecordset.Operation.Eq, "АЗМ1");
                r.Open();
                if (!r.IsEOF())
                {
                    int PlanId = r.GetI("ID");
                    objStation.Put("PLAN_ID", PlanId);
                    objStation.Put("CUST_TXT4", "1");
                    objStation.Put("CUST_TXT5", "O");
                }
                r.Close();
                r.Destroy();
            }
            //----
            addValidVisnMonth = 12;
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            station.Dispose();
            base.DisposeExec();
        }
        //===================================================
        /// <summary>
        /// Создание XML строки для построения грида
        /// </summary>
        /// <returns>XML строка</returns>
        protected override string GetXMLParamGrid()
        {
            string XmlString =
                "<items>"
            + "<item key='header1'    type='lat'       flags=''      tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
            + "<item key='header2'    type='lat'       flags='s'     tab=''   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LATITUDE
            + "<item key='mKLat'       type='lat'       flags=''     tab=''  display='Широта (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLat'       type='lat'       flags='s'     tab='1'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LONGITUDE
            + "<item key='mKLon'       type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLon'       type='lat'       flags='s'     tab='2'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.ASL 
            + "<item key='mkAsl'      type=''       flags=''      tab=''  display='Висота поверхні Землі, м' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='kAsl'       type='double'       flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // objPosition.GetFullAddress
            + "<item key='mkAddr'     type=''      flags=''      tab=''  display='Адреса встановлення РЕЗ' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kAddr'      type='string'      flags='s'     tab='3'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

            //objEquip.NAME
            + "<item key='mkName'     type=''      flags=''      tab=''  display='Номер РЕЗ/назва опори' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='KName'      type=''      flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='KPillarName' type=''      flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kSystem'    type='lat'      flags=''      tab=''  display='Система передачі' ReadOnly='true' background='' bold=''/>"
            + "<item key='system'     type='lat'      flags='s'     tab='4'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kEquip'    type='lat'      flags=''      tab=''  display='Назва/тип РЕЗ' ReadOnly='true' background='' bold='y'/>"
            + "<item key='equip'     type='lat'      flags='s'     tab='5'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            //PWR_ANT
            + "<item key='kPowerA'    type='lat'      flags=''      tab=''  display='Потужність передавача, Вт' ReadOnly='true' background='' bold=''/>"
            + "<item key='powera'     type='lat'      flags='s'     tab='6'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            //objEquip.BW
                //objEquip.DESIG_EMISSION
            + "<item key='kRxBand'    type='lat'      flags=''      tab=''  display='Ширина смуги, МГц/Клас випромінювання' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='rxband'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='kEMI'       type='string'    flags='s'    tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // objEquip.CUST_TXT1
                // objEquip.CUST_DAT1
            + "<item key='kCert'      type='lat'      flags=''      tab=''  display='Сертифікат відповідності (номер / дата)' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='certno'     type='string'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='certda'     type='datetime'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // 
            + "<item key='kFider'      type='lat'      flags=''   tab=''   display='Довжина фідера, м / Втрати у фідері, дБ/м' ReadOnly='true' background='' bold=''/>"
            + "<item key='fiderlen'    type='double'   flags='s'  tab='7'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='fidrloss'   type='double' flags='s'  tab='8'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='kAntennaDir'       type='lat'      flags=''      tab=''  display='Спрямованість антени/поляризація' ReadOnly='true' background='' bold='y' />"
            + "<item key='dir'        type='lat'      flags='s'     tab='9'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
            + "<item key='polar'        type='lat'      flags='s'     tab='10'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"

            + "<item key='kAntennaHeight' type='lat'      flags=''      tab=''  display='Висота антени, м / макс. ефект. висота' ReadOnly='true' background='' bold=''/>"
            + "<item key='height'         type='lat'      flags='s'     tab='11'  display='' background='' bold='' HorizontalAlignment='center'/>"
            + "<item key='maxHeight'      type='lat'      flags='s'     tab=''  display='' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kAntennaGain' type=''      flags=''      tab=''  display='Макс. коеф. підсилення антени: Г, дБ / В, дБ' background='' bold=''/>"
            + "<item key='horzGain'     type=''      flags='s'     tab='12'  display='' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='vertGain'     type=''      flags='s'     tab='13'  display='' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='kEVP' type='lat'      flags=''      tab=''  display='ЕВП: Г, дБВт / В, дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='HorzEVP'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='VertEVP'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kEVPmax' type='lat'      flags=''      tab=''  display='ЕВП макс., дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='EVPmax'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            + "<item key='kCallSign'    type='lat'      flags=''      tab=''  display='Позивний сигнал (програма мовлення)' ReadOnly='true' background='' bold=''/>"
            + "<item key='callsign'     type='lat'      flags='s'     tab='14'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            + "<item key='kState'   type='lat'      flags=''      tab=''    display='Внутрішній стан / Міжнародний стан' background='' bold='' />"
            + "<item key='homeState' type='lat'      flags='s'     tab='15'  display='' background='' bold='' HorizontalAlignment='center' />"
            + "<item key='internationalState' type='lat'      flags='s'     tab='16'  display='' background='' bold='' HorizontalAlignment='center' />"

            + "<item key='kFreq'    type='lat'      flags=''      tab=''  display='Центральна частота, МГц' ReadOnly='true' background='' bold='y'/>"
            + "<item key='freq'     type='lat'      flags='s'     tab='17'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            + "<item key='mKObj'       type=''       flags=''     tab=''  display='Об\"єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='KObj'      type=''     flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "</items>";
            return XmlString;
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            string polar = objStation.GetS("POLARIZATION");

            if (string.IsNullOrEmpty(polar))
            {
                MessageBox.Show("Не введена поляризація.");
                return false;
            }

            // Сохраняем "Особливі умови"
            objStation["CUST_TXT12"] = SCDozvil;
            objStation["CUST_TXT13"] = SCVisnovok;
            objStation["CUST_TXT14"] = SCNote;
            objStation.Put("CUST_CHB1", IsTechnicalUser);
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус

            if (newStation)
            {
                objStation.Put("DIAGA", "HH");
            }

            int EquipID = objStation.GetI("EQUIP_ID");
            int SiteID = objStation.GetI("SITE_ID");

            objStation.Put("CUST_TXT1", NumberOut);//--***         
            objStation.Put("CUST_DAT1", DateOut);//--***

            //if (NeedSavePosition)
            //    station.objPosition.Save();
            objStation.Put("SITE_ID", station.objPosition.Id);
            

            SaveLicence();
            CJournal.CheckTable(TableName, objStation.GetI("ID"), objStation);

            objStation.SaveToDB();
            return true;
        }

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (station.objPosition != null) ? station.objPosition.Province : "";
            string city = (station.objPosition != null) ? station.objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if ((docType == DocType.VISN) || (docType == DocType.VISN_NR))
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.DOZV_OPER))
                fullPath += "ЗМ-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії Дозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }

        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            System.Diagnostics.Debug.WriteLine("FmAnalogApp.UpdateOneParamGrid:" + cell.Key);
            if (cell.Key == "KLat")
            {
                int siteId = objStation.GetI("SITE_ID");
                if (siteId != IM.NullI)
                {
                    station.objPosition = new PositionState2();
                    station.objPosition.LoadStatePosition(siteId, ICSMTbl.itblPositionBro);
                    station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                }
                AutoFill(cell, grid);
            }
            else
                if (cell.Key == "system")
                {
                    Int32 transmissionSys = 0;
                    if (Int32.TryParse(objStation.GetS("TRANSMISSION_SYS"), out transmissionSys))
                    {
                        List<string> TrasmissionSystemList = CFmTransmissionSystem.getTransmissionSystemList();
                        if (transmissionSys - 1 > 0)
                            if (TrasmissionSystemList.Count > transmissionSys - 1)
                                cell.Value = TrasmissionSystemList[transmissionSys - 1];
                        AutoFill(cell, grid);
                    }
                }
                else
                    /*
                     * + "<item key='KName'      type=''      flags='s'     tab='5'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
                           + "<item key='KPillarName'      type=''      flags='s'     tab='6'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
                     * */
                    if (cell.Key == "KName")
                    {
                        cell.Value = objStation.GetS("CUST_TXT7");
                    }
                    else
                        if (cell.Key == "equip")
                        {
                            int equipId = objStation.GetI("EQUIP_ID");

                            if (equipId != IM.NullI)
                                station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipBro, equipId);

                            AutoFill(cell, grid);
                        }
                        else if (cell.Key == "height")
                        {
                            double EffectiveHeight = objStation.GetD("AGL");
                            cell.Value = EffectiveHeight.ToString("F0");
                        }
                        else if (cell.Key == "maxHeight")
                        {
                            double EffectiveHeightMax = objStation.GetD("EFHGT_MAX");
                            cell.Value = EffectiveHeightMax.ToString("F0");
                        }
                        else if (cell.Key == "fiderlen")
                        {
                            double EffectiveHeightMax = objStation.GetD("CUST_NBR1");
                            cell.Value = EffectiveHeightMax.ToString("F0");
                        }
                        else if (cell.Key == "fidrloss")
                        {
                            double EffectiveHeightMax = objStation.GetD("CUST_NBR2");
                            cell.Value = EffectiveHeightMax.ToString("F3");
                        }
                        else if (cell.Key == "dir")
                        {
                            SetDirection();
                            SelectByShort(cell, Direction);
                        }
                        else if (cell.Key == "polar")
                        {
                            SelectByShort(cell, objStation.GetS("POLARIZATION"));
                            AutoFill(cell, grid);
                        }
                        else if (cell.Key == "callsign")
                        {
                            string s = objStation.GetS("CALL_SIGN");
                            cell.Value = s;
                        }

                        else if (cell.Key == "homeState")
                        {
                            //cell.Value = objStation.GetS("CUST_TXT4");
                            SelectByShort(cell, objStation.GetS("CUST_TXT4"));
                            //cell.cellStyle = EditStyle.esPickList;
                            //cell.PickList = CFMState.getInternalStateList();
                        }
                        else if (cell.Key == "internationalState")
                        {
                            cell.Value = objStation.GetS("CUST_TXT5");
                            SelectByShort(cell, objStation.GetS("CUST_TXT5"));
                            //cell.cellStyle = EditStyle.esPickList;
                            //cell.PickList = CFMState.getForeignStateList();
                        }
                        else if (cell.Key == "freq")
                        {
                            cell.Value = objStation.GetD("FREQ").ToString("F3");
                            //cell.cellStyle = EditStyle.esPickList;
                            //cell.PickList = CFMState.getForeignStateList();
                        }
                        else if (cell.Key == "KObj")
                        {
                            cell.Value = objStation.GetI("ID").ToString();
                        }
                        else if (cell.Key == "horzGain")
                        {
                            UpdateHorzGain(cell);
                        }
                        else if (cell.Key == "vertGain")
                        {
                            UpdateVertGain(cell);
                        }
                        else if (cell.Key == "HorzEVP")
                        {
                            cell.Value = objStation.GetD("ERP_H").ToString("F3");
                        }
                        else if (cell.Key == "VertEVP")
                        {
                            cell.Value = objStation.GetD("ERP_V").ToString("F3");
                        }
                        else if (cell.Key == "powera")
                        {
                            double PwrAnt = objStation.GetD("PWR_ANT");
                            cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
                        }
                        else if (cell.Key == "kEMI")
                        {
                            string DesigEmission = objStation.GetS("DESIG_EM");
                            cell.Value = DesigEmission;
                        }
                        else if (cell.Key == "rxband")
                        {
                            double BW = objStation.GetD("BW");
                            cell.Value = (BW / 1000.0).ToString("F3");
                        }
        }

        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KLon") ||
                (cell.Key == "kAddr") ||
                (cell.Key == "equip"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
            }
            else if (cell.Key == "system")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CFmTransmissionSystem.getTransmissionSystemList();
            }
            else if (cell.Key == "polar")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Small);
            }
            else if (cell.Key == "dir")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CAntennaDirection.getAntennaDirectionList();
            }
            else if (cell.Key == "homeState")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CFMState.getInternalStateList();
            }
            else if (cell.Key == "internationalState")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CFMState.getForeignStateList();
            }
            else if ((cell.Key == "horzGain") ||
                     (cell.Key == "vertGain"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
            }
            else if ((cell.Key == "HorzEVP") ||
                      (cell.Key == "VertEVP") ||
                      (cell.Key == "EVPmax"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
                cell.ButtonText = "Розрахунок.";
            }
            else if (cell.Key == "kAsl")
            {
                cell.cellStyle = EditStyle.esEllipsis;
            }

            /*|| (cell.Key == "addr2")
                   || (cell.Key == "rez")
                   || (cell.Key == "antType1") || (cell.Key == "antType2")
                   || (cell.Key == "obj1")*/
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "equip")
            {
                // Выбор оборудования
                RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipBro, "NAME", cell.Value, true); 
                if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                {
                    station.objEquip = IMObject.LoadFromDB(recEquip);
                    objStation.Put("EQUIP_ID", recEquip.Id);
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "KLon")
            {
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionBro, 1, Longitude, Latitude);
                if (newPos != null)
                {
                    station.objPosition = newPos;
                    station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                    objStation.Put("SITE_ID", newPos.Id);
                    AutoFill(grid.GetCellFromKey("KLat"), grid);
                    NeedSavePosition = station.objPosition.IsChanged;
                }
            }
            else if (cell.Key == "kAddr")
            {
                if (ShowMessageReference(station.objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms = Longitude;
                    newPos.LatDms = Latitude;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionBro, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        station.objPosition = newPos;
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(grid.GetCellFromKey("KLat"), grid);
                        NeedSavePosition = true;
                    }
                }            
            }
            else if (cell.Key.Equals("horzGain"))
            {
                OnPresButtonHorzGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                Cell fillCell = grid.GetCellFromKey("polar");
                AutoFill(fillCell, grid);
            }
            else if (cell.Key.Equals("vertGain"))
            {
                OnPresButtonVertGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                Cell fillCell = grid.GetCellFromKey("polar");
                AutoFill(fillCell, grid);
            }
            else if (cell.Key == "HorzEVP")
            {
                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                cell.Value = CalcERPHorz(Power - FeederLoss);
            }

            else if (cell.Key == "VertEVP")
            {
                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                cell.Value = CalcERPVert(Power - FeederLoss);
            }
            else if (cell.Key == "EVPmax")
            {
                string polarization = objStation.GetS("POLARIZATION");

                double Power = objStation.GetD("PWR_ANT");
                double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                if (polarization == "V")
                {
                    cell.Value = CalcERPVert(Power - FeederLoss);
                }
                else if (polarization == "H")
                {
                    cell.Value = CalcERPHorz(Power - FeederLoss);
                }
                else if (polarization == "M")
                {
                    cell.Value = CalcERPMixed(Power - FeederLoss);
                }
            }
            else if (cell.Key == "kAsl")
            {
                if (station.objPosition != null)
                {
                    double Longitude = station.objPosition.LonDec;
                    double Latitude = station.objPosition.LatDec;
                    if (Longitude != IM.NullD && Latitude != IM.NullD)
                    {
                        double asl = IMCalculate.CalcALS(Longitude, Latitude, "4DEC");
                        if (asl != IM.NullD)
                        {
                            cell.Value = IM.RoundDeci(asl, 1).ToString();
                            OnCellValidate(cell, grid);
                        }
                    }
                }
            }
            else
            {
                base.OnPressButton(cell, grid);
            }
        }

        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            FmAnalogApp retStation = new FmAnalogApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            FmAnalogApp retStation = newAppl as FmAnalogApp;
            //(0, objStation.GetI("OWNER_ID"), packetID, radioTech);

            /*retStation.CopyFromMe(grid,
               (stationList[0].objEquip != null) ? stationList[0].objEquip.GetI("ID") : IM.NullI,
               (stationList[0].objAntenna != null) ? stationList[0].objAntenna.GetI("ID") : IM.NullI,
               (stationList[0].objStation != null) ? stationList[0].objStation.GetS("POLAR") : "",
               (stationList[0].objStation != null) ? stationList[0].objStation.GetD("PWR_ANT") : 0.0);*/
            return retStation;
        }

        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {

            if (cell.Key == "KLat")
            {
                if (station.objPosition != null)
                {
                    double latDMS = station.objPosition.LatDms;
                    double lonDMS = station.objPosition.LongDms;
                    this.Longitude = lonDMS;
                    this.Latitude = latDMS;
                    cell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                    OnCellValidate(cell, grid);

                    SafeFillCellByKey(grid, "KLon", HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon), true);
                    OnCellValidate(grid.GetCellFromKey("KLon"), grid);

                    double asl = station.objPosition.Asl;
                    //fillCell = grid.GetCellFromKey("kAsl");
                    SafeFillCellByKey(grid, "kAsl", (asl != IM.NullD) ? asl.ToString("0") : "", true);
                    //fillCell.Value = (asl != IM.NullD) ? asl.ToString("0") : "";
                    //OnCellValidate(fillCell, grid);

                    SafeFillCellByKey(grid, "kAddr", station.objPosition.FullAddressAuto, true);
                    OnCellValidate(grid.GetCellFromKey("kAddr"), grid);
                    
                    //fillCell = grid.GetCellFromKey("KPillarName");
                    SafeFillCellByKey(grid, "KPillarName", station.objPosition.Name, true);
                    //fillCell.Value = station.objPosition.GetS("NAME");
                }
                else
                {
                    //fillCell = grid.GetCellFromKey("KLon");
                    SafeFillCellByKey(grid, "KLon", string.Empty, false);
                    //fillCell.Value = "";

                    //fillCell = grid.GetCellFromKey("kAsl");
                    SafeFillCellByKey(grid, "kAsl", string.Empty, false);
                    //fillCell.Value = "";

                    //fillCell = grid.GetCellFromKey("kAddr");
                    SafeFillCellByKey(grid, "kAddr", string.Empty, false);
                    //fillCell.Value = "";
                }
            }
            else if (cell.Key == "equip")
            {
                if (station.objEquip != null)
                {// Заполяем поля значениями                    

                    cell.Value = station.objEquip.GetS("NAME");

                    //fillCell = grid.GetCellFromKey("certno");
                    SafeFillCellByKey(grid, "certno", station.objEquip.GetS("CUST_TXT1"), false);
                    //fillCell.Value = station.objEquip.GetS("CUST_TXT1");

                    //OnCellValidate(fillCell, grid);

                    //fillCell = grid.GetCellFromKey("certda");
                    DateTime dt = station.objEquip.GetT("CUST_DAT1");

                    if (dt.Year > 1990)
                    {
                        SafeFillCellByKey(grid, "certda", dt.ToString("dd.MM.yyyy"), false);
                        //fillCell.Value = dt.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        SafeFillCellByKey(grid, "certda", string.Empty, false);
                        //fillCell.Value = "";
                    }

                    //fillCell = grid.GetCellFromKey("powera");
                    double PwrAnt = objStation.GetD("PWR_ANT");
                    SafeFillCellByKey(grid, "powera", (Math.Pow(10.0, (PwrAnt) / 10.0)).ToString("F0"), false);
                    //fillCell.Value = (Math.Pow(10.0, (PwrAnt) / 10.0)).ToString("F0");

                    //fillCell = grid.GetCellFromKey("kEMI");               
                    string DesigEmission = objStation.GetS("DESIG_EM");
                    SafeFillCellByKey(grid, "kEMI", DesigEmission, false);
                    //fillCell.Value = DesigEmission;

                    //fillCell = grid.GetCellFromKey("rxband");
                    double BW = objStation.GetD("BW");
                    SafeFillCellByKey(grid, "rxband", BW.ToString(), false);
                    //fillCell.Value = (BW/1000.0).ToString("F3");         
                }
                else
                {
                    cell.Value = "";

                    //fillCell = grid.GetCellFromKey("certno");
                    SafeFillCellByKey(grid, "certno", string.Empty, false);
                    //fillCell.Value = "";

                    //fillCell = grid.GetCellFromKey("certda");
                    SafeFillCellByKey(grid, "certda", string.Empty, false);
                    //fillCell.Value = "";
                }
            }
            else if (cell.Key == "polar") //TODO нада буде шоб ваня подивився і розкоментіровать
            {
                //AutoFillPolar(cell, grid.GetCellFromKey("horzGain"), grid.GetCellFromKey("vertGain"));
            }
        }
        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    //NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(lon, 0, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (station.objPosition != null)
                    {
                        station.objPosition.LongDms = lon;
                        IndicateDifference(cell, station.objPosition.LonDiffersFromAdm);
                    }
                    Longitude = lon;
                }
            }
            else if (cell.Key == "KLat")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    //NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(0, lat, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (station.objPosition != null)
                    {
                        station.objPosition.LatDms = lat;
                        IndicateDifference(cell, station.objPosition.LatDiffersFromAdm);
                    }
                    Latitude = lat;
                }
            }
            else if (cell.Key == "kAddr")
            {
                if (station.objPosition != null)
                    IndicateDifference(cell, station.objPosition.AddrDiffersFromAdm);
            }
            if (cell.Key == "powera")
            {
                double MaxPower = 1E-99;
                if (station.objEquip != null)
                    MaxPower = station.objEquip.GetD("MAX_POWER");

                double PwrAnt = Convert.ToDouble(cell.Value);
                double ConvertedPwrAnt = 30 + 10.0 * Math.Log10(PwrAnt);
                if (MaxPower < ConvertedPwrAnt)
                {
                    double VisibleMax = Math.Pow(10, (MaxPower - 30.0) / 10.0);
                    MessageBox.Show("Занадто велике значення потужності. Не повинно перевищувати " + VisibleMax.ToString("F0"));
                }
                else
                {
                    objStation.Put("PWR_ANT", 10.0 * Math.Log10(PwrAnt));
                }
                //double PwrAnt = objStation.GetD("PWR_ANT");
                //cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
            }
            else if (cell.Key == "system")
            {
                List<string> TrasmissionSystemList = CFmTransmissionSystem.getTransmissionSystemList();
                int TransmissionSys = TrasmissionSystemList.IndexOf(cell.Value) + 1;
                objStation.Put("TRANSMISSION_SYS", TransmissionSys.ToString());
            }
            else if (cell.Key == "polar" || cell.Key == "dir")
            {
                Cell polarCell = grid.GetCellFromKey("polar");
                Cell dirCell = grid.GetCellFromKey("dir");
                Cell horzGainCell = grid.GetCellFromKey("horzGain");
                Cell vertGainCell = grid.GetCellFromKey("vertGain");

                OnValidateDirectionAndPolarization(polarCell, dirCell, horzGainCell, vertGainCell);
            }
            else if (cell.Key == "horzGain")
            {
                OnValidateGain(cell, "H");
            }
            else if (cell.Key == "vertGain")
            {
                OnValidateGain(cell, "V");
            }
            else if (cell.Key == "callsign")
            {
                objStation.Put("CALL_SIGN", cell.Value);
            }
            else if (cell.Key == "kAsl")
            {
                double asl = 0.0;
                if (Double.TryParse(cell.Value, out asl))
                {
                    station.objPosition.Asl = asl;
                    NeedSavePosition = true;
                }
            }
            else if (cell.Key == "homeState")
            {
                objStation.Put("CUST_TXT4", CFMState.getShortState(cell.Value));
            }
            else if (cell.Key == "internationalState")
            {
                objStation.Put("CUST_TXT5", CFMState.getShortState(cell.Value));
            }
            else if (cell.Key == "freq")
            {
                IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                r.Select("ID,COD,LOWER_FREQ,UPPER_FREQ");
                int PlanID = objStation.GetI("PLAN_ID");

                r.SetWhere("ID", IMRecordset.Operation.Eq, PlanID);
                r.Open();
                if (!r.IsEOF())
                {
                    //int PlanId = r.GetI("ID");
                    //objStation.Put("PLAN_ID", PlanId);

                    double LowerFreq = r.GetD("LOWER_FREQ");
                    double UpperFreq = r.GetD("UPPER_FREQ");

                    double EnteredFreq = cell.Value.ToDouble(IM.NullD);
                    //Double.TryParse(cell.Value, out EnteredFreq);

                    if (LowerFreq <= EnteredFreq && UpperFreq >= EnteredFreq)
                        objStation.Put("FREQ", EnteredFreq);
                    else
                    {
                        MessageBox.Show(cell.Value.ToString() + " не вписується в частотний план ( " + (LowerFreq).ToString("F3") + " - " + (UpperFreq).ToString("F3") + " )");
                        //double currentFreq = objStation.GetD("FREQ");
                        //cell.Value = currentFreq.ToString("F3");
                    }
                }
                else
                {
                    MessageBox.Show("Несподівана втрата даних по частотному плану");
                }
                r.Close();
                r.Destroy();



                //cell.Value = objStation.GetD("FREQ").ToString("F3");
                //cell.cellStyle = EditStyle.esPickList;
                //cell.PickList = CFMState.getForeignStateList();
            }
            else if (cell.Key == "equip")
            {
                if (station.objEquip != null)
                {
                    string DesigEmission = station.objEquip.GetS("DESIG_EMISSION");
                    double BandWidth = station.objEquip.GetD("BW");
                    double PowerAnt = station.objEquip.GetD("MAX_POWER");
                    objStation.Put("DESIG_EM", DesigEmission);
                    objStation.Put("PWR_ANT", PowerAnt - 30.0);
                    objStation.Put("BW", BandWidth);
                }
            }
            else if (cell.Key == "height")
            {
                double EffectiveHeight = 0;
                if (Double.TryParse(cell.Value, out EffectiveHeight))
                {
                    if (EffectiveHeight > 0)
                        objStation.Put("AGL", EffectiveHeight);
                    else
                    {
                        //MessageBox.Show("Висота антени повинна бути додатнім числом");
                        objStation.Put("AGL", EffectiveHeight);
                        ChangeColor(cell, Colors.badValue);
                    }
                }

            }
            else if (cell.Key == "maxHeight")
            {
                double EffectiveHeightMax = 0;
                if (Double.TryParse(cell.Value, out EffectiveHeightMax))
                {
                    double EffectiveHeight = objStation.GetD("AGL");

                    if (EffectiveHeightMax > EffectiveHeight)
                        objStation.Put("EFHGT_MAX", EffectiveHeightMax);
                    else
                        MessageBox.Show("Максимальна ефективна висота повинна бути не менше " + EffectiveHeight.ToString());
                }
            }
            else if (cell.Key == "fiderlen")
            {
                double FeederLen = 0;
                if (Double.TryParse(cell.Value, out FeederLen))
                    objStation.Put("CUST_NBR1", FeederLen);

                double FeederLoss = 0;
                FeederLoss = objStation.GetD("CUST_NBR2");
                objStation.Put("TX_LOSSES", FeederLoss * FeederLen);
            }
            else if (cell.Key == "fidrloss")
            {
                double FeederLoss = 0;
                if (Double.TryParse(cell.Value, out FeederLoss))
                    objStation.Put("CUST_NBR2", FeederLoss);

                double FeederLen = 0;
                FeederLen = objStation.GetD("CUST_NBR1");
                objStation.Put("TX_LOSSES", FeederLoss * FeederLen);
            }
            /*else if (cell.Key =="horzGain")
            {  
               string polarization = objStation.GetS("POLARIZATION");
               if (station.Direction.Equals("D"))
               {
                  if (polarization.Equals("H") && polarization.Equals("M"))
                  {                  
                     string strDiagHorizontal = objStation.GetS("DIAGH");
                     List<double> DiagDbl = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, 0.0);

                     double Minimal = DiagDbl.Min();
                     //objStation.Put("GAIN", Math.Abs(Minimal));                  
                  }                              
               }
            } else if (cell.Key == "vertGain")
            {
               string polarization = objStation.GetS("POLARIZATION");
               if (station.Direction.Equals("D"))
               {
                  if (polarization.Equals("V") || polarization.Equals("M"))
                  {
                     string strDiagHorizontal = objStation.GetS("DIAGV");
                     List<double> DiagDbl = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, 0.0);

                     double Minimal = DiagDbl.Min();
                     //objStation.Put("GAIN", Math.Abs(Minimal));
                  }
               }
            }*/
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRS formVrs = new FormVRS(recordID.Id, recordID.Table);
            formVrs.ShowDialog();
            formVrs.Dispose();
        }

        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //    r.Select("ID,CUST_DAT2,CUST_DAT3,CUST_TXT2,CUST_TXT3");
        //    r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
        //    try
        //    {
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            switch (docType)
        //            {
        //                case DocType.VISN:
        //                case DocType.VISN_NR:
        //                    r.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    objStation.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    r.Put("CUST_DAT2", startDate);
        //                    objStation.Put("CUST_DAT2", startDate);
        //                    break;
        //            }
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if (cell.Key == "kAddr")
                {
                    if (station.objPosition != null)
                    {
                        int posID = station.objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionBro, posID, (IM.TableRight(ICSMTbl.itblPositionBro) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        station.objPosition.LoadStatePosition(station.objPosition.Id, station.objPosition.TableName);
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(gridParam.GetCellFromKey("KLat"), gridParam);
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "equip") && (station.objEquip != null))
                {
                    int EquipID = station.objEquip.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(station.objEquip.GetS("TABLE_NAME"), EquipID);
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KObj") && (objStation != null))
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(TableName, ID);
                    rcPtr.UserEdit();
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        public double GetPower()
        {
            double PwrAnt = objStation.GetD("PWR_ANT");
            return Math.Pow(10.0, PwrAnt / 10.0);
        }

        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }

        public override string GetProvince()
        {
            return "";
        }
    }
}
