﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class FmAnalogEquipment : Equipment 
    {
        public override void Load()
        {
            IMRecordset eqImRecordset = new IMRecordset(ICSMTbl.itblEquipBro, IMRecordset.Mode.ReadOnly);
            try
            {
                eqImRecordset.Select("ID,NAME,CUST_TXT1,CUST_DAT1");
                eqImRecordset.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                eqImRecordset.Open();
                if (!eqImRecordset.IsEOF())
                {
                    Name = eqImRecordset.GetS("NAME");
                    Certificate.Symbol = eqImRecordset.GetS("CUST_TXT1");
                    Certificate.Date = eqImRecordset.GetT("CUST_DAT1");
                }
            }
            finally
            {
                if (eqImRecordset.IsOpen())
                    eqImRecordset.Close();
                eqImRecordset.Destroy();
            }
        }      
    }
}
