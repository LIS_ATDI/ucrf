﻿using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public class AmateurFreq
    {
        public int Id { get; set; }
        public double FreqRx { get; set; }
        public double FreqTx { get; set; }
        public string Channel { get; set; }        
        /// <summary>
        /// Конструктор
        /// </summary>
        public AmateurFreq()
        {
            Id = IM.NullI;
            FreqRx = IM.NullD;
            FreqTx = IM.NullD;
            Channel = "";                        
        }

        /// <summary>
        /// ID обрудования
        /// </summary>
        public int EquipId { get; set; }

        /// <summary>
        /// Сохранение частот
        /// </summary>
        public void Save()
        {
            IMRecordset r = new IMRecordset(AmateurStation.TableNameFreq, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CHANNEL,TX_FREQ,RX_FREQ,FREQ_ID");
            try
            {
                r.Open();
                r.AddNew();
                r.Put("ID", IM.AllocID(AmateurStation.TableNameFreq, 1, -1));                
                r.Put("FREQ_ID", EquipId);
                r.Put("TX_FREQ", FreqTx);
                r.Put("RX_FREQ", FreqRx);
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>        
        public void Load(int freqId)
        {
            EquipId = freqId;
            Load();
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>
        public void Load()
        {
                       
        }
        /// <summary>
        /// Возвращает строку с частотами передатчика
        /// </summary>
        /// <returns>строка с частотами передатчика</returns>
        public static string GetFreqs(List<AmateurFreq> freqList)
        {
            string retVal = "";
            foreach (AmateurFreq freq in freqList)
            {
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.ToString();
            }
            return retVal;
        }

        /// <summary>
        /// Возвращает строку с частотами передатчика
        /// </summary>
        /// <returns>строка с частотами передатчика</returns>
        public static string GetFreqTx(List<AmateurFreq> freqList)
        {
            string retVal = "";
            foreach (AmateurFreq freq in freqList)
            {
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.FreqTx.Round(10);
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает строку с частотами приемника
        /// </summary>
        /// <returns>строка с частотами приемника</returns>
        public static string GetFreqRx(List<AmateurFreq> freqList)
        {
            string retVal = "";
            foreach (AmateurFreq freq in freqList)
            {
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.FreqRx.Round(10);
            }
            return retVal;
        }

        public int CompareTo(AmateurFreq other)
        {
            return ((int)((other.FreqTx - FreqTx) * 1000000.0));
        }        
    }
}
