﻿using System;
using System.Windows;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    class AbonentStation : BaseStation
    {
        /// <summary>
        /// Типы эксплуатации
        /// </summary>
        public enum TypeExplAbonent
        {
            Stationary = 0,   //Стаціонарна
            Portable = 1,     //Переносна
            Movable = 2,      //Пересувна
        }
        public static bool StatusCheckDublicate { get; set; }
        public static int Count { get; set; }
        public static readonly string TableName = PlugTbl.XfaAbonentStation;
        public static readonly string TablePosition = PlugTbl.XfaPosition;
        //public static readonly string TableEquip = PlugTbl.XfaEquip;
        public static readonly string TableAnten = PlugTbl.XfaAnten;
        /// <summary>
        /// Стандарт
        /// </summary>
        public string Standard { get; set; }
        /// <summary>
        /// ID Владелеца станции
        /// </summary>
        public int AbonentResId { get; set; }
        /// <summary>
        /// Позиция
        /// </summary>
        public PositionState2 objPosition = null;//Позиция
        /// <summary>
        /// Розташування РЕЗ
        /// </summary>
        public string PositionRez { get; set; }
        /// <summary>
        /// Регіон експлуатації
        /// </summary>
        public string RegionExpl { get; protected set; }
        /// <summary>
        /// Код регіону
        /// </summary>
        public int RegionCode { get; protected set; }
        /// <summary>
        /// Тип эксплуатации
        /// </summary>
        public TypeExplAbonent ExplAbonent { get; protected set; }
        /// <summary>
        /// Оборудование
        /// </summary>
        public AbonentEquipment Equip { get; set; }
        /// <summary>
        /// Антена
        /// </summary>
        public AbonentAntenna Anten { get; set; }
        /// <summary>
        /// Мощность антенны
        /// </summary>
        public double Power { get; set; }
        /// <summary>
        /// Номер производителя
        /// </summary>
        public string ManufactureNumber { get; set; }
        /// <summary>
        /// Номер філії
        /// </summary>
        public string BrunchCode { get; set; }
        /// <summary>
        /// Позивной
        /// </summary>
        public string CallSign { get; set; }
        /// <summary>
        /// Позивной Сети
        /// </summary>
        public string NetCallSign { get; set; }
        /// <summary>
        /// Номер бланка
        /// </summary>
        public string NumBlank { get; set; }
        /// <summary>
        /// Частота TX
        /// </summary>
        public string FreqTx { get; set; }
        /// <summary>
        /// Частота RX
        /// </summary>
        public string FreqRx { get; set; }
        /// <summary>
        /// ID сети
        /// </summary>
        public int NetId { get; set; }
        /// <summary>
        /// Owner ID
        /// </summary>
        public int OwnerId { get; set; }
        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Строк действия разрешения
        /// </summary>
        public DateTime PermEndDate { get; set; }
        /// <summary>
        /// Дата выдачи разрешения
        /// </summary>
        public DateTime PermDatePrint { get; set; }
        /// <summary>
        /// Ширина полосы
        /// </summary>
        public double Bw { get; set; }
        /// <summary>
        /// Зсилання на позицію обладнання в таблиці
        /// </summary>
        public int equipId { get; set; }
        /// <summary>
        /// Зсилання на позицію сайта в таблиці
        /// </summary>
        public int posId { get; set; }
        /// <summary>
        /// Зсилання на позицію антени в таблиці
        /// </summary>
        public int antId { get; set; }
        //=======================================
        //Поля для RailwayApp
        //======================================
        /// <summary>
        /// Означення станції
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Позивний
        /// </summary>
        public string Call { get; set; }
        /// <summary>
        /// Затухання у фідерному тракті, дБ
        /// </summary>
        public double FiderLoss { get; set; }
        /// <summary>
        /// Азимут випромінювання антени, град
        /// </summary>
        public double Azimuth { get; set; }
        /// <summary>
        /// Висота антени над рівнем моря
        /// </summary>
        public double AGL { get; set; }
        /// <summary>
        /// Клас випромінювання
        /// </summary>
        public string DesigEm { get; set; }
        /// <summary>
        /// Номер дозвола
        /// </summary>
        public string PermNum { get; set; }
        /// <summary>
        /// Спецю условия дозвола
        /// </summary>
        public string SpecConditionPerm { get; set; }
        /// <summary>
        /// Спецю условия высновка
        /// </summary>
        public string SpecConditionConcl { get; set; }
        /// <summary>
        /// Замечания
        /// </summary>
        public string SpecNote { get; set; }
        //========================================

        public AbonentFreqs Freqs { get; protected set; }
        //====================================================
        public AbonentStation()
        {
            Standard = "";
            AbonentResId = IM.NullI;
            ExplAbonent = TypeExplAbonent.Stationary;
            //objPosition = new PositionState2();
            Equip = new AbonentEquipment();
            Anten = new AbonentAntenna();
            Power = IM.NullD;
            ManufactureNumber = "";
            CallSign = "";
            NetCallSign = "";
            NumBlank = "";
            PositionRez = "";
            RegionExpl = "";
            RegionCode = 99;
            FreqTx = "";
            FreqRx = "";
            NetId = IM.NullI;
            OwnerId = IM.NullI;
            PermEndDate = IM.NullT;
            PermDatePrint = IM.NullT;
            Bw = IM.NullD;
            PermNum = "";
            Freqs = new AbonentFreqs();
            SpecConditionPerm = string.Empty;
            SpecConditionConcl = string.Empty;
            SpecNote = string.Empty;
            BrunchCode = string.Empty;
        }

        /// <summary>
        /// Проверка на наличие дубликатов заводских номеров в базе по указанному региону (если запись уже есть  возвращает false)
        /// <params> FACTORY_NUM (заводской номер РЕЗ)
        /// <params> LOCATION (Размещение РЕЗ)
        /// <params> status_include (признак исключения из рассмотрения текущей открытой для редактирования записи)
        /// </summary>
        public bool GetStatus_DublicateFactory_NUM(string FACTORY_NUM, string LOCATION, int EQP_ID, bool status_include)
        {
            bool res = true;
            IMRecordset rx = new IMRecordset(AbonentStation.TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                rx.Select("ID,LOCATION,FACTORY_NUM,EQP_ID");
                rx.SetWhere("FACTORY_NUM", IMRecordset.Operation.Eq, FACTORY_NUM.ToString().TrimEnd().TrimStart());
                rx.SetWhere("EQP_ID", IMRecordset.Operation.Eq, EQP_ID);
                if (status_include) { rx.SetWhere("ID", IMRecordset.Operation.Neq, Id); }
                rx.Open();


                if (!rx.IsEOF())
                {
                    while (!rx.IsEOF())
                    {
                        if (rx.GetS("LOCATION") == LOCATION.ToString().TrimEnd().TrimStart())
                        {
                            res = false;
                            break;
                        }
                        rx.MoveNext();
                    }
                }
                else
                {
                    if (rx.IsEOF())
                    {
                        res = true;
                    }
                }

            }
            finally
            {
                rx.Final();
            }

            return res;
        }


        /// <summary>
        /// Проверка на наличие дубликатов заводских номеров в базе по указанному региону (если запись уже есть  возвращает false)
        /// <params> FACTORY_NUM (заводской номер РЕЗ)
        /// <params> LOCATION (Размещение РЕЗ)
        /// <params> status_include (признак исключения из рассмотрения текущей открытой для редактирования записи)
        /// </summary>
        public bool GetStatus_DublicateFactory_NUM(string FACTORY_NUM, string LOCATION, bool status_include)
        {
            bool res = true;
            IMRecordset rx = new IMRecordset(AbonentStation.TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                rx.Select("ID,LOCATION,FACTORY_NUM");
                rx.SetWhere("FACTORY_NUM", IMRecordset.Operation.Eq, FACTORY_NUM.ToString().TrimEnd().TrimStart());
                if (status_include) { rx.SetWhere("ID", IMRecordset.Operation.Neq, Id); }
                rx.Open();


                if (!rx.IsEOF())
                {
                    while (!rx.IsEOF())
                    {
                        if (rx.GetS("LOCATION") == LOCATION.ToString().TrimEnd().TrimStart())
                        {
                            res = false;
                            break;
                        }
                        rx.MoveNext();
                    }
                }
                else
                {
                    if (rx.IsEOF())
                    {
                        res = true;
                    }
                }

            }
            finally
            {
                rx.Final();
            }

            return res;
        }

        /// <summary>
        /// Сохраняет данные станции
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                const string selectedFields = "ID,STANDARD,ABONENT_ID,POS_ID,USE_REGION,LOCATION,REGION_CODE,EXPL_TYPE,EQP_ID,PWR_ANT,FACTORY_NUM,CALL_SIGN,BLANK_NUM,FREQ_TX_TEXT,FREQ_RX_TEXT,NET_ID,USER_ID,STATUS,EOUSE_DATE,BW,PERM_DATE_PRINT,PERM_NUM";
                r.Select(selectedFields);
                r.Select("DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                r.Select("NAME,CALL,TX_LOSSES,AZIMUTH,AGL,DESIG_EMISSION,ANT_ID,SPEC_COND_DOZV,ADM,BRANCH_OFFICE_CODE");
                r.Select("SPEC_COND_VISN,NOTE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF())
                {

                    if (!StatusCheckDublicate)
                    {
                        if (GetStatus_DublicateFactory_NUM(ManufactureNumber, PositionRez, Equip.Id, false) == false)
                        {
                            if (MessageBox.Show(CLocaliz.TxT("This serial number already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            {
                                return;
                            }
                        }
                    }

                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("STANDARD", Standard);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }
                else
                {
                    if (!StatusCheckDublicate)
                    {
                        if (GetStatus_DublicateFactory_NUM(ManufactureNumber, PositionRez, Equip.Id, true) == false)
                        {
                            if (MessageBox.Show(CLocaliz.TxT("This serial number already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            {
                                return;
                            }
                        }
                    }

                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
                r.Put("ABONENT_ID", AbonentResId);
                r.Put("LOCATION", PositionRez);
                r.Put("USE_REGION", RegionExpl);
                r.Put("REGION_CODE", RegionCode);
                r.Put("EXPL_TYPE", ((int)ExplAbonent).ToString());
                r.Put("EQP_ID", Equip.Id);
                r.Put("PWR_ANT", Power);
                r.Put("FACTORY_NUM", ManufactureNumber);
                r.Put("CALL_SIGN", CallSign);
                r.Put("BLANK_NUM", NumBlank);
                r.Put("FREQ_TX_TEXT", FreqTx);
                r.Put("FREQ_RX_TEXT", FreqRx);
                r.Put("NET_ID", NetId);
                r.Put("USER_ID", OwnerId);
                r.Put("STATUS", Status);
                r.Put("EOUSE_DATE", PermEndDate);
                r.Put("PERM_DATE_PRINT", PermDatePrint);
                r.Put("BW", Bw);
                r.Put("PERM_NUM", PermNum);
                r.Put("ADM", "UKR");
                //===================================                
                r.Put("NAME", Name);
                r.Put("CALL", Call);
                r.Put("TX_LOSSES", FiderLoss);
                r.Put("AZIMUTH", Azimuth);
                r.Put("AGL", AGL);
                r.Put("DESIG_EMISSION", DesigEm);
                r.Put("SPEC_COND_DOZV", SpecConditionPerm);
                r.Put("SPEC_COND_VISN", SpecConditionConcl);
                r.Put("NOTE", SpecNote);
                r.Put("BRANCH_OFFICE_CODE", BrunchCode);
                r.Put("STANDARD", Standard);
                
                //===================================                
                switch (ExplAbonent)
                {
                    case TypeExplAbonent.Stationary:
                        if (objPosition != null)
                        {
                            if (objPosition.IsChanged)
                                objPosition.Save(0);
                            r.Put("POS_ID", objPosition.Id);
                        }
                        if (Anten != null)
                            r.Put("ANT_ID", Anten.Id);
                        if (Equip != null)
                            r.Put("EQP_ID", Equip.Id);
                        break;
                    case TypeExplAbonent.Movable:
                    case TypeExplAbonent.Portable:
                        r.Put("POS_ID", IM.NullI);
                        break;
                }
                CJournal.CheckUcrfTable(TableName, Id, r, selectedFields);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            Freqs.StationId = Id;
            Freqs.Save();
        }
        /// <summary>
        /// Загружаем данные станции
        /// </summary>
        public override void Load(int id)
        {
            Id = id;
            Load();
        }
        /// <summary>
        /// Загружаем данные станции
        /// </summary>
        public override void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,STANDARD,ABONENT_ID,POS_ID,USE_REGION,LOCATION,REGION_CODE,EXPL_TYPE,EQP_ID,PWR_ANT,FACTORY_NUM,CALL_SIGN,BLANK_NUM");
                r.Select("FREQ_TX_TEXT,FREQ_RX_TEXT,NET_ID,USER_ID,STATUS,EOUSE_DATE,BW,PERM_DATE_PRINT,Net.CALL_SIGN");
                r.Select("NAME,CALL,TX_LOSSES,AZIMUTH,AGL,DESIG_EMISSION,ANT_ID,PERM_NUM,SPEC_COND_DOZV,BRANCH_OFFICE_CODE");
                r.Select("SPEC_COND_VISN,NOTE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Standard = r.GetS("STANDARD");
                    AbonentResId = r.GetI("ABONENT_ID");
                    PositionRez = r.GetS("LOCATION");
                    RegionExpl = r.GetS("USE_REGION");
                    RegionCode = r.GetI("REGION_CODE");
                    Power = r.GetD("PWR_ANT");

                    ManufactureNumber = r.GetS("FACTORY_NUM");
                    CallSign = r.GetS("CALL_SIGN");
                    NetCallSign = r.GetS("Net.CALL_SIGN");
                    NumBlank = r.GetS("BLANK_NUM");
                    FreqTx = r.GetS("FREQ_TX_TEXT");
                    FreqRx = r.GetS("FREQ_RX_TEXT");
                    NetId = r.GetI("NET_ID");
                    OwnerId = r.GetI("USER_ID");
                    Status = r.GetS("STATUS");
                    PermEndDate = r.GetT("EOUSE_DATE");
                    PermDatePrint = r.GetT("PERM_DATE_PRINT");
                    Bw = r.GetD("BW");
                    PermNum = r.GetS("PERM_NUM");
                    //=========================================
                    Name = r.GetS("NAME");
                    Call = r.GetS("CALL");
                    FiderLoss = r.GetD("TX_LOSSES");
                    Azimuth = r.GetD("AZIMUTH");
                    AGL = r.GetD("AGL");
                    DesigEm = r.GetS("DESIG_EMISSION");
                    SpecConditionPerm = r.GetS("SPEC_COND_DOZV");
                    SpecConditionConcl = r.GetS("SPEC_COND_VISN");
                    SpecNote = r.GetS("NOTE");
                    BrunchCode = r.GetS("BRANCH_OFFICE_CODE");
                    //=========================================
                    SetExplType(r.GetS("EXPL_TYPE"));

                    equipId = r.GetI("EQP_ID");
                    if (equipId != IM.NullI)
                    {
                        Equip.Id = equipId;
                        Equip.Load();
                    }

                    posId = r.GetI("POS_ID");
                    if (posId != IM.NullI)
                    {
                        objPosition = new PositionState2();
                        objPosition.LoadStatePosition(posId, TablePosition);
                        PositionRez = objPosition.FullAddressAuto;
                    }
                    else
                        objPosition = null;

                    antId = r.GetI("ANT_ID");
                    if (antId != IM.NullI)
                    {
                        Anten.Id = antId;
                        Anten.Load();
                    }
                    // Частоты
                    Freqs = new AbonentFreqs();
                    Freqs.Load(Id);

                    if (IsRangeFreq() == false)
                    {
                        FreqTx = Freqs.GetFreqTx();
                        FreqRx = Freqs.GetFreqRx();
                    }
                    else
                    {
                        double minFreq;
                        double maxFreq;
                        Freqs.RangeFreq(false, out minFreq, out maxFreq);
                        if ((minFreq != IM.NullD) && (maxFreq != IM.NullD))
                            FreqRx = string.Format("{0} - {1}", minFreq, maxFreq);
                        else if(minFreq != IM.NullD)
                            FreqRx = string.Format("{0}", minFreq);
                        Freqs.RangeFreq(true, out minFreq, out maxFreq);
                        if ((minFreq != IM.NullD) && (maxFreq != IM.NullD))
                            FreqTx = string.Format("{0} - {1}", minFreq, maxFreq);
                        else if(minFreq != IM.NullD)
                            FreqTx = string.Format("{0}", minFreq);
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Как 
        /// </summary>
        /// <returns></returns>
        private bool IsRangeFreq()
        {
            switch (Standard)
            {
                case CRadioTech.RPATL:
                case CRadioTech.SB:
                case CRadioTech.BAUR:
                case CRadioTech.SptRZ:
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Возвращает дубликат станции
        /// </summary>
        /// <returns>дубликат станции</returns>
        public AbonentStation GetDuplicate()
        {
            AbonentStation retStation = new AbonentStation();
            retStation.Load(Id);
            switch (retStation.ExplAbonent)
            {
                case TypeExplAbonent.Movable:
                    retStation.PositionRez = "";
                    break;
                case TypeExplAbonent.Stationary:
                    //retStation.objPosition = new PositionState2();
                    retStation.PositionRez = "";
                    retStation.RegionCode = 99;
                    retStation.RegionExpl = "";
                    break;
            }
            return retStation;
        }
        /// <summary>
        /// Возвращает описание стандарта
        /// </summary>
        /// <returns>описание стандарта</returns>
        public string GetHumanStandard()
        {
            string retVal = Standard;
            IMRecordset r = new IMRecordset(ICSMTbl.itblRadioSystem, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,DESCRIPTION");
                r.SetWhere("NAME", IMRecordset.Operation.Like, Standard);
                r.Open();
                if (!r.IsEOF())
                {
                    retVal = r.GetS("DESCRIPTION");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает имя обонента
        /// </summary>
        /// <returns>имя обонента</returns>
        public string GetNameAbonentRes()
        {
            string retVal = "";
            IMRecordset r = new IMRecordset(PlugTbl.ABONENT_OWNER, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, AbonentResId);
                r.Open();
                if (!r.IsEOF())
                {
                    retVal = r.GetS("NAME");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            return retVal;
        }
        /// <summary>
        /// Установливает тип эксплуатации
        /// </summary>
        /// <param name="type">тип эксплуатации</param>
        public void SetExplType(string type)
        {
            try
            {
                ExplAbonent = (TypeExplAbonent)Enum.Parse(typeof(TypeExplAbonent), type);
            }
            catch (Exception)
            {
                ExplAbonent = TypeExplAbonent.Stationary;
            }
        }
        /// <summary>
        /// Установливает регион эксплуатации
        /// </summary>
        /// <param name="region">регион эксплуатации</param>
        public void SetRegionExpl(string region)
        {
            RegionExpl = region;
            RegionCode = 99;
            string[] provinces = region.Trim().Split(';');
            if (provinces.Length == 1)
            {
                string prov = provinces[0].Trim();
                try
                {
                    string code = HelpFunction.getAreaCode(prov, prov);
                    int codeLocal = code.ToInt32(IM.NullI);
                    if (codeLocal != IM.NullI)
                        RegionCode = codeLocal;
                }
                catch (Exception)
                {
                    RegionCode = 99;
                }
            }
        }
        /// <summary>
        /// Возвращает список дат окончания дозвола
        /// </summary>
        /// <returns></returns>
        public DateTime[] GetEndPermDateBaseStation()
        {
            List<DateTime> endDozvBase = new List<DateTime>();
            IMRecordset rsBaseStations = new IMRecordset(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadOnly);
            rsBaseStations.Select("ID,NET_ID,STATION_ID,STATION_TABLE");
            rsBaseStations.SetWhere("NET_ID", IMRecordset.Operation.Eq, NetId);
            try
            {
                for (rsBaseStations.Open(); !rsBaseStations.IsEOF(); rsBaseStations.MoveNext())
                {
                    int id = rsBaseStations.GetI("STATION_ID");
                    string table = rsBaseStations.GetS("STATION_TABLE");

                    IMRecordset recMobSta2 = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
                    recMobSta2.Select("ID,CUST_DAT5,CUST_TXT5");
                    recMobSta2.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    try
                    {
                        recMobSta2.Open();
                        if (!recMobSta2.IsEOF())
                        {
                            DateTime date = recMobSta2.GetT("CUST_DAT5");
                            if (date != IM.NullT)
                                endDozvBase.Add(date);
                        }
                    }
                    finally
                    {
                        if (recMobSta2.IsOpen())
                            recMobSta2.Close();
                        recMobSta2.Destroy();
                    }
                }
            }
            finally
            {
                if (rsBaseStations.IsOpen())
                    rsBaseStations.Close();
                rsBaseStations.Destroy();
            }
            return endDozvBase.ToArray();
        }
        #region Static function
        /// <summary>
        /// Возвращает позывной сети
        /// </summary>
        /// <param name="idNet">ID сети</param>
        /// <returns>Позывной сети</returns>
        public static string GetCallSignNet(int idNet)
        {
            string retCall = "";
            IMRecordset rsNet = new IMRecordset(PlugTbl.XfaNet, IMRecordset.Mode.ReadOnly);
            rsNet.Select("ID,CALL_SIGN");
            rsNet.SetWhere("ID", IMRecordset.Operation.Eq, idNet);
            try
            {
                rsNet.Open();
                if (!rsNet.IsEOF())
                    retCall = rsNet.GetS("CALL_SIGN");
            }
            finally
            {
                if (rsNet.IsOpen())
                    rsNet.Close();
                rsNet.Destroy();
            }
            return retCall;
        }
        #endregion
    }
}
