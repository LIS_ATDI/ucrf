﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
//XICSM.UcrfRfaNET.ApplSource.Classes
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class ShipEquipment : Equipment
    {
        private Power _power = new Power();

        public Power Power { get { return _power; } }
        public double Quantity { get; set; }
        //public string Name { get; set; }
        //public string DesigEmission { get; set; }
        public double Bandwidth { get; set; }

        public int PmrId { get { return _equipmentPmr.Id; } }

        public ShipStation ShipStation { get; set; }

        private MobStationEquipment _equipmentPmr = new MobStationEquipment();
        
        public ShipEquipment()
        {
            Quantity = IM.NullD;
            Id = IM.NullI;
            Name = "";
            DesigEmission = "";
            Bandwidth = IM.NullD;
        }

        public enum Type
        {
            None,
            Rbss,
            Sp,
            Rps,
        }

        public string eqTypes { get; protected set; }
        public Type type { get; protected set; }

        private string[] rbssCodes = new string[] { "21", "22", "26", "28", "31" };
        private string[] spCodes = new string[] { "23", "24", "25" };
        private string[] rpsCodes = new string[] { "27", "29" };

        public override void Load()
        {    
            IMRecordset r = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,STA_ID,EQUIP_ID,POWER,REMARK,Equipment.TUNING_STEP,Equipment.CUST_TXT5,Equipment.CUST_TXT2,Equipment.CUST_TXT6");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    LoadEquipmentPmr(r.GetI("EQUIP_ID"));
                    
                    Power[PowerUnits.dBW] = r.GetD("POWER");
                    string remark = r.GetS("REMARK");

                    int quantity = 0;
                    Int32.TryParse(remark, out quantity);
                    Quantity = quantity;
                    Bandwidth = r.GetD("Equipment.TUNING_STEP");
                    if (Bandwidth != IM.NullD)
                        Bandwidth /= 1000.0;

                    string eqType = r.GetS("Equipment.CUST_TXT5");
                    eqTypes = eqType;
                    type = rbssCodes.Contains(eqType) ? Type.Rbss :
                        rpsCodes.Contains(eqType) ? Type.Rps :
                        spCodes.Contains(eqType) ? Type.Sp :
                        Type.None;
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();                
            }
        }

        public void LoadEquipmentPmr(int id)
        {
            _equipmentPmr = new MobStationEquipment();
            _equipmentPmr.Id = id;            
            _equipmentPmr.TableName = ICSMTbl.itblEquipPmr;

            if (_equipmentPmr.Id != IM.NullI)
            {
                _equipmentPmr.Load();

                eqTypes = _equipmentPmr.EqTypes;
                Name = _equipmentPmr.Name;
                Bandwidth = _equipmentPmr.Bandwidth;
                DesigEmission = _equipmentPmr.CustText2;

                if (!Power.IsValid)
                    _power = _equipmentPmr.MaxPower.Clone() as Power;
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,STA_ID,EQUIP_ID,POWER,REMARK");                
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(ICSMTbl.itblShipEqp, 1, -1));
                }
                else
                {
                    r.Edit();
                }
                r.Put("STA_ID", ShipStation.Id);
                r.Put("EQUIP_ID", _equipmentPmr.Id);
                r.Put("POWER", Power[PowerUnits.dBW]);
                r.Put("REMARK", Quantity.ToStringNullD());
                r.Update();                
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }                
    }
}
