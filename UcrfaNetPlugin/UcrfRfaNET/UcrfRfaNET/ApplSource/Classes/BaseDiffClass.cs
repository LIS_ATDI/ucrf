﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    internal abstract class BaseDiffClass : BaseAppClass
    {
        protected List<string> listFotosDst;
        protected List<string> listFotosSrc;

        public string DiffTableName { get; set; }

        public BaseDiffClass(int _id, string _tableName, int _ownerId, int _packetID, string _radioTech)
            : base(_id, _tableName, _ownerId, _packetID, _radioTech)
        {
            switch (_tableName)
            {
                case ICSMTbl.itblMicrowa:
                    DiffTableName = PlugTbl.itblXnrfaDeiffMwSta;
                    break;
                case ICSMTbl.itblEarthStation:
                    DiffTableName = PlugTbl.itblXnrfaDeiffEtSta;
                    break;
                case ICSMTbl.itblMobStation:
                    DiffTableName = PlugTbl.itblXnrfaDiffMobSta;
                    break;
                case ICSMTbl.itblMobStation2:
                    DiffTableName = PlugTbl.itblXnrfaDiffMobSta2;
                    break;
                case ICSMTbl.itblFM:
                    DiffTableName = PlugTbl.itblXnrfaDeiffFmSta;
                    break;
                case ICSMTbl.itblTV:
                    DiffTableName = PlugTbl.itblXnrfaDeiffTvSta;
                    break;
                case ICSMTbl.itblTDAB:
                    DiffTableName = PlugTbl.itblXnrfaDiffFmDigital;
                    break;
                case ICSMTbl.itblDVBT:
                    DiffTableName = PlugTbl.itblXnrfaDeiffDvbSta;
                    break;
            }            
        }

        public override void UpdateForm(MainAppForm form)
        {
            base.UpdateForm(form);            
            form.tabControl1.SelectTab(form.tabControl1.TabPages[3].Name);            
        }
      
    }
}
