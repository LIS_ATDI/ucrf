﻿using System.Collections.Generic;
using System.Linq;
using ICSM;
using IdwmNET;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    abstract class ForeighnCoord
    {
        protected int Id;
        protected string Technology;
        protected string TableName;
        protected string TableNameFreq;
        protected NearestCountryItem[] NearestCountry;
        protected List<CoordForm.FreqId> FreqStatList;
        protected List<CoordForm.CoordTable> CoordTbl;
        protected List<string> FirstStatusList;
        public ForeighnCoord(string tech, string tableName, string tableFreq)
        {
            Technology = tech;
            TableName = tableName;
            TableNameFreq = tableFreq;
        }
        
        /// <summary>
        /// Обчислення всіх можливих частот
        /// </summary>
        /// <param name="objIdList">список станцій</param>
        /// <returns></returns>
        public abstract List<CoordForm.FreqId> CalculateFreq4Station(List<int> objIdList);
        /// <summary>
        /// Вертає список статусів для форми
        /// </summary>
        /// <returns></returns>
        public abstract List<CoordForm.CoordTable> ShowStatusStation();
        /// <summary>
        /// Зберігає всі статуси станції
        /// </summary>        
        public abstract void AcceptStation();
        /// <summary>
        /// Створення початкового списку можливих адміністрацій і їх частот які є у довідниках
        /// </summary>
        public List<CoordForm.CoordTable> CreateFirstListOfRecords(NearestCountryItem[] nearCntr, List<CoordForm.FreqId> freqStatList, int id)
        {
            NearestCountry = nearCntr;
            FreqStatList = freqStatList;
            Id = id;

            List<CoordForm.CoordTable> crdTbl;
            IMRecordset r = new IMRecordset(PlugTbl.XFrecDistAdmAgre, IMRecordset.Mode.ReadOnly);
            r.Select("ID,FREQ_MIN,FREQ_MAX,D,ADMCoordination.ADM,ADMCoordination.TECHNOLOGY");

            string allSetAddStr = "";

            string country = "";
            if (nearCntr.Length > 0)
                for (int i = 0; i < nearCntr.Length; i++)
                {
                    country += string.Format("([ADMCoordination.ADM] LIKE '{0}')", nearCntr[i].country);
                    if (i < nearCntr.Length - 1)
                        country += " OR ";
                }
            if (!string.IsNullOrEmpty(country))
            {
                country = "(" + country + ")";
                allSetAddStr += country;
            }            

            
            if (!(freqStatList == null || FreqStatList.Count == 0))
            {
                string freqs = "";
                for (int i = 0; i < freqStatList.Count; i++)
                {
                    if (freqStatList[i].Freq != IM.NullD)
                    {
                        freqs += "([FREQ_MIN]<" + freqStatList[i].Freq + " AND [FREQ_MAX]>" + freqStatList[i].Freq + ")";
                        if (i < freqStatList.Count - 1)
                            freqs += " OR ";
                    }
                }
                if (!string.IsNullOrEmpty(freqs))
                {
                    freqs = "(" + freqs + ")";
                    if (string.IsNullOrEmpty(allSetAddStr))
                        allSetAddStr += freqs;
                    else
                        allSetAddStr += " AND " + freqs;
                }
                    //r.SetAdditional(freqs);
            }
           
            r.SetAdditional(allSetAddStr);
            r.SetWhere("ADMCoordination.TECHNOLOGY", IMRecordset.Operation.Eq, Technology);

            try
            {
                crdTbl = new List<CoordForm.CoordTable>();
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    CoordForm.CoordTable tmpCoord = new CoordForm.CoordTable();
                    tmpCoord.Id = id;
                    tmpCoord.Administration = r.GetS("ADMCoordination.ADM");
                    tmpCoord.Technology = r.GetS("ADMCoordination.TECHNOLOGY");
                    tmpCoord.DistanceCoord = r.GetI("D");
                    tmpCoord.FreqMax = r.GetD("FREQ_MAX");
                    tmpCoord.FreqMin = r.GetD("FREQ_MIN");
                    crdTbl.Add(tmpCoord);
                }
            }
            finally
            {
                r.Final();
            }
            CoordTbl = crdTbl;
            return crdTbl;
        }

        /// <summary>
        /// Формування остаточного списку адміністарцій для виведення            
        /// </summary>
        /// <param name="tmpCoordList">пустий тимчасовий список для збереження всіх новоутворених позицій</param>
        /// <param name="finalListCountry"></param>
        public void FormatFinishRecords(ref List<CoordForm.CoordTable> tmpCoordList, Dictionary<NearestCountryItem, int> finalListCountry)
        {
            if (finalListCountry != null && finalListCountry.Count > 0)
                NearestCountry = finalListCountry.Keys.ToArray();
            for (int i = 0; i < CoordTbl.Count; i++)
            {
                for (int j = 0; j < FreqStatList.Count; j++)
                {
                    if (CoordTbl[i].FreqMax > FreqStatList[j].Freq && CoordTbl[i].FreqMin < FreqStatList[j].Freq)
                        for (int k = 0; k < NearestCountry.Length; k++)
                        {
                            if (NearestCountry[k].country == CoordTbl[i].Administration)
                                if (NearestCountry[k].distance < CoordTbl[i].DistanceCoord)
                                {
                                    CoordForm.CoordTable tmpCord = new CoordForm.CoordTable();
                                    tmpCord.Administration = NearestCountry[k].country;
                                    tmpCord.DistanceCoord = CoordTbl[i].DistanceCoord;
                                    tmpCord.DistanceFact = NearestCountry[k].distance;
                                    tmpCord.Freq = FreqStatList[j].Freq;
                                    tmpCord.FreqMax = CoordTbl[i].FreqMax;
                                    tmpCord.FreqMin = CoordTbl[i].FreqMin;
                                    tmpCord.Id = FreqStatList[j].StaId;
                                    tmpCord.Technology = Technology;
                                    tmpCoordList.Add(tmpCord);
                                }
                        }
                }
            }
            CoordTbl = tmpCoordList;
        }



    }


}
