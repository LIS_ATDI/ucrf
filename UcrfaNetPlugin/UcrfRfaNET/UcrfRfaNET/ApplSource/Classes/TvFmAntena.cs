﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    class TvFmAntena : Antenna, ICloneable
    {        

        #region Implementation of ICloneable
        public object Clone()
        {
            TvFmAntena clone = new TvFmAntena();

            clone.Name = Name;            
            clone.Id = Id;
            clone.TableName = TableName;
            return clone;
        }
        #endregion

        public override void Load()
        {
            //ICSMTbl.itblMicrowAntenna
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,NAME,H_BEAMWIDTH,GAIN");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                    Name = r.GetS("NAME");
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}

