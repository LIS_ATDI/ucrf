﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Windows.Forms;
using System.Drawing;
using GridCtrl;

namespace XICSM.UcrfRfaNET.ApplSource
{
    public abstract class BaseGridController : IDisposable
    {
        public readonly Color warnValueColor = Color.LightSalmon;
        public readonly Color oldValueColor = Color.LightSalmon;
        public readonly Color newValueColor = Color.Yellow;
        public readonly Color badValueColor = Color.Red;
        public readonly Color okValueColor = Color.White;

        public enum Colors
        {
            newValue,  //Новое значение
            oldValue,  //Старое значение
            badValue,  //Неверное значение
            okvalue,   //Все нормально
            warnValue, //Предупреждение
        };

        public BaseGridController()
        {
        }

        /// <summary>
        /// Convert a hex string to a .NET Color object.
        /// </summary>
        /// <param name="hexColor">a hex string: "FFFFFF", "#000000"</param>
        private static Color HexStringToColor(string hexColor)
        {
            string hc = ExtractHexDigits(hexColor);
            if (hc.Length != 6)
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("hexColor is not exactly 6 digits.");
                return Color.Empty;
            }
            string r = hc.Substring(0, 2);
            string g = hc.Substring(2, 2);
            string b = hc.Substring(4, 2);
            Color color = Color.Empty;
            try
            {
                int ri
                   = Int32.Parse(r, System.Globalization.NumberStyles.HexNumber);
                int gi
                   = Int32.Parse(g, System.Globalization.NumberStyles.HexNumber);
                int bi
                   = Int32.Parse(b, System.Globalization.NumberStyles.HexNumber);
                color = Color.FromArgb(ri, gi, bi);
            }
            catch
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("Conversion failed.");
                return Color.Empty;
            }
            return color;
        }
        /// <summary>
        /// Extract only the hex digits from a string.
        /// </summary>
        private static string ExtractHexDigits(string input)
        {
            // remove any characters that are not digits (like #)
            Regex isHexDigit
               = new Regex("[abcdefABCDEF\\d]+", RegexOptions.Compiled);
            string newnum = "";
            foreach (char c in input)
            {
                if (isHexDigit.IsMatch(c.ToString()))
                    newnum += c.ToString();
            }
            return newnum;
        }


        //============================================================
        /// <summary>
        /// Формирует струтуру грида параметров
        /// </summary>
        /// <param name="grid">Грид параметров</param>
        public virtual void LoadStructureParamGrid(Grid grid)
        {
            // Удаляем все строки
            while (grid.GetRowCount() > 0)
                grid.DeleteRow(grid.GetRowCount() - 1);

            //string XMLParam = GetXMLParamGrid();  // Загружаем струтуру грида
            // обрабка XML і сворення структурі гріда*/

            XmlDocument xml = new XmlDocument();


            xml.LoadXml(GetXMLParamGrid());

            XmlNode node;
            node = xml.DocumentElement;

            XmlAttribute atr = node.Attributes["schema"];
            if ((atr != null) && (atr.Value == "UCRF.RFA 1.0"))
            {
                XmlToParamGrid2(node, grid);
            }
            else
            {
                XmlToParamGrid(node, grid);
            }
        }

       
        //=============================================================
        /// <summary>
        /// Возвращает XML строку для настройки грида параметров
        /// </summary>
        /// <returns>XML строку настройки параметров грида</returns>
        protected virtual string GetXMLParamGrid()
        {
            string XmlString = "<items>" + "</items>";
            return XmlString;
        }

        //=============================================================
        private void XmlToParamGrid(XmlNode node, Grid grid)
        {
            if (node == null)
                return;

            if ((node.Name == "items") && (node.HasChildNodes))
                node = node.FirstChild;

            while (node != null)
            {
                FormatCell(node, grid);
                node = node.NextSibling;
            }
        }

        //=============================================================
        private void XmlToParamGrid2(XmlNode node, Grid grid)
        {
            XmlNode rowsNode = node.SelectSingleNode("rows");

            XmlNode rowNode = rowsNode.FirstChild;

            while (rowNode != null)
            {
                FormatRow(rowNode, grid);
                rowNode = rowNode.NextSibling;
            }
        }
        //=============================================================
        private void FormatRow(XmlNode node, Grid grid)
        {
            grid.AddRow();
            XmlNode itemsNode = node.SelectSingleNode("items");

            XmlNode itemNode = itemsNode.FirstChild;

            while (itemNode != null)
            {
                FormatCell2(itemNode, grid);
                itemNode = itemNode.NextSibling;
            }
        }
        //=============================================================
        private void FormatCell2(XmlNode node, Grid grid)
        {
            grid.AddColumn(grid.GetRowCount() - 1, "");
            Cell newCell = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);

            XmlAttribute controlTypeAttribute = node.Attributes["controlStyle"];
            string key = node.Attributes["Key"].Value;

            string controlType = controlTypeAttribute != null ? controlTypeAttribute.Value : "";

            switch (controlType)
            {
                case "ellipsis":
                    newCell.cellStyle = EditStyle.esEllipsis;
                    break;
                case "picklist":
                    newCell.cellStyle = EditStyle.esPickList;
                    break;
            }

            int xmledTabOrder = -1;
            if (node.Attributes["tabOrder"] != null)
            {
                Int32.TryParse(node.Attributes["tabOrder"].Value, out xmledTabOrder);
            }
            newCell.TabOrder = xmledTabOrder;

            newCell.TextHeight = 11;

            if (key != null)
                newCell.Key = key;

            XmlNode readOnlyNode = node.SelectSingleNode("readOnly");
            if (readOnlyNode != null && readOnlyNode.FirstChild != null)
            {
                newCell.CanEdit = !IsSet(readOnlyNode.FirstChild.Value);
            }

            XmlNode displayNode = node.SelectSingleNode("display");
            if (displayNode != null)
            {
                XmlNode valueNode = displayNode.SelectSingleNode("value");

                if (valueNode != null && valueNode.FirstChild != null)
                {
                    newCell.Value = valueNode.FirstChild.Value;
                }

                XmlNode horizontalMarginNode = displayNode.SelectSingleNode("horizontalMargin");

                if (horizontalMarginNode != null && horizontalMarginNode.FirstChild != null)
                {
                    newCell.HorizontalMargin = Convert.ToInt32(horizontalMarginNode.FirstChild.Value);
                }

                XmlNode fontNode = displayNode.SelectSingleNode("font");
                if (fontNode != null)
                {
                    XmlAttribute weightAttribute = fontNode.Attributes["weight"];
                    if (weightAttribute != null)
                    {
                        if (weightAttribute.Value == "bold")
                            newCell.FontStyle = newCell.FontStyle | FontStyle.Bold;
                    }
                    //fontNode.Attributes["weight"].Value = 

                    XmlAttribute italicAttribute = fontNode.Attributes["italic"];
                    if (italicAttribute != null)
                    {
                        if (IsSet(italicAttribute.Value))
                            newCell.FontStyle = newCell.FontStyle | FontStyle.Italic;
                    }
                }

                XmlNode backgroundColorNode = displayNode.SelectSingleNode("backgroundColor");
                if (backgroundColorNode != null && backgroundColorNode.FirstChild != null)
                {
                    newCell.BackColor = HexStringToColor(backgroundColorNode.FirstChild.Value);
                }

                XmlNode textColorNode = displayNode.SelectSingleNode("textColor");
                if (textColorNode != null && textColorNode.FirstChild != null)
                {
                    newCell.TextColor = HexStringToColor(textColorNode.FirstChild.Value);
                }
                XmlNode horizontalAlignmentNode = displayNode.SelectSingleNode("horizontalAlignment");
                if (horizontalAlignmentNode != null && horizontalAlignmentNode.FirstChild != null)
                {
                    newCell.HorizontalAlignment = (HorizontalAlignment)Enum.Parse(typeof(HorizontalAlignment), horizontalAlignmentNode.FirstChild.Value, true);
                }
            }

            XmlNode pickListNode = node.SelectSingleNode("picklist");
            if (pickListNode != null)
            {
                newCell.KeyPickList = new KeyedPickList();
                XmlNode pickItemNode = pickListNode.FirstChild;

                while (pickItemNode != null)
                {
                    XmlAttribute storedDataAttribute = pickItemNode.Attributes["storedData"];
                    XmlAttribute visibleDataAttribute = pickItemNode.Attributes["visibleData"];

                    newCell.KeyPickList.AddToPickList(storedDataAttribute.Value, visibleDataAttribute.Value);
                    pickItemNode = pickItemNode.NextSibling;
                }
            }

            XmlNode dataBindingNode = node.SelectSingleNode("dataBinding");
            if (dataBindingNode != null)
            {
                XmlNode bindNode = dataBindingNode.FirstChild;

                while (bindNode != null)
                {
                    if (bindNode.Name == "cellProperty")
                    {
                        string controlProperty = bindNode.Attributes["controlProperty"].Value;
                        string dataSourceProperty = bindNode.Attributes["dataSourceProperty"].Value;
                        string dataSource = bindNode.Attributes["dataSource"].Value;

                        FieldInfo fi = GetType().GetField(dataSource, BindingFlags.FlattenHierarchy);
                        FieldInfo fi2 = GetType().GetField(dataSource, BindingFlags.NonPublic | BindingFlags.Instance);

                        //object dataObject = new CellStringProperty();
                        object dataObject = fi2.GetValue(this);
                        CellStringProperty c = (CellStringProperty)dataObject;
                        c.Cell = newCell;

                        dataObject = c;
                        bool canEdit = newCell.CanEdit;

                        newCell.Bind(controlProperty, dataObject, dataSourceProperty);

                        c.CanEdit = canEdit;
                    }
                    if (bindNode.Name == "event")
                    {
                        string eventType = bindNode.Attributes["type"].Value;
                        string eventHandler = bindNode.Attributes["handler"].Value;
                        if (eventType == "OnPressButton")
                            newCell.AddOnPressButtonHandler(this, eventHandler);

                        if (eventType == "OnBeforeChange")
                            newCell.AddOnBeforeChangeHandler(this, eventHandler);

                        if (eventType == "OnAfterChange")
                            newCell.AddOnAfterChangeHandler(this, eventHandler);
                    }
                    //FormatCell2(itemNode, grid);
                    bindNode = bindNode.NextSibling;
                }
            }
        }
        //=============================================================
        private void FormatCell(XmlNode node, Grid grid)
        {
            if (node == null)
                return;
            XmlAttributeCollection attrColl = node.Attributes;

            if (attrColl["flags"].Value.Contains("s") == false)
            {// Добавляем первый элемент
                grid.AddRow();
            }
            // Добавляем слеюующую ячейку
            string display = "";
            if (attrColl["display"] != null)
                display = attrColl["display"].Value;
            grid.AddColumn(grid.GetRowCount() - 1, display);

            Cell newCell = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);

            if (attrColl["key"] != null)
                newCell.Key = attrColl["key"].Value;

            newCell.TextHeight = 11;

            newCell.HorizontalAlignment = HorizontalAlignment.Left;
            if (attrColl["HorizontalAlignment"] != null)
            {
                if (attrColl["HorizontalAlignment"].Value == "center")
                    newCell.HorizontalAlignment = HorizontalAlignment.Center;
            }

            if ((attrColl["ReadOnly"] != null) && (attrColl["ReadOnly"].Value == "true"))
                newCell.CanEdit = false;

            if (attrColl["background"] != null)
            {
                if (attrColl["background"].Value == "DarkGray")
                    newCell.BackColor = System.Drawing.Color.DarkGray;
            }
            if (attrColl["fontColor"] != null)
            {
                if (attrColl["fontColor"].Value == "gray")
                {
                    newCell.TextColor = System.Drawing.Color.Gray;
                    newCell.FontStyle = FontStyle.Italic;
                }
            }

            // Шрифт ячейки
            if (attrColl["fontType"] != null)
            {
                if (attrColl["fontType"].Value.Contains("b"))
                    newCell.FontStyle = System.Drawing.FontStyle.Bold;
                else if (attrColl["fontType"].Value.Contains("i"))
                    newCell.FontStyle = System.Drawing.FontStyle.Italic;
            }

            if (attrColl["bold"] != null)
            {
                if (attrColl["bold"].Value == "y")
                    newCell.FontStyle = System.Drawing.FontStyle.Bold;
            }

            // Тип ячейки
            newCell.Type = "string";  //По умолчанию
            if (attrColl["type"] != null)
                newCell.Type = attrColl["type"].Value;

            newCell.DBField = "";  //По умолчанию
            if (attrColl["DBField"] != null)
                newCell.DBField = attrColl["DBField"].Value;

            newCell.TabOrder = -1;
            if (attrColl["tab"] != null)
                newCell.TabOrder = ConvertType.ToInt32(attrColl["tab"].Value, -1);
        }


        //============================================================
        /// <summary>
        /// Обновить все параметры грида (заполняються все поля грида)
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void UpdateGridParameters(Grid grid)
        {            
            for (int r = 0; r < grid.GetRowCount(); r++)
                for (int c = 0; c < grid.GetColumnCount(r); c++)
                    UpdateOneParamGrid(grid.GetCell(r, c), grid);            
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public virtual void UpdateOneParamGrid(Grid grid, string cellKey)
        {
            UpdateOneParamGrid(grid.GetCellFromKey(cellKey), grid);
        }

        public abstract void UpdateOneParamGrid(Cell cell, Grid grid);

        protected void SelectByShort(Cell cell, string shortText)
        {
            if (cell.PickList != null && !string.IsNullOrEmpty(shortText))
            {
                foreach (string variant in cell.PickList)
                {
                    if (variant.StartsWith(shortText))
                    {
                        cell.Value = variant;
                        return;
                    }
                }
            }
            else if (!string.IsNullOrEmpty(shortText))
                cell.Value = shortText;
            else
                cell.Value = "";
        }

        protected void InitDefaultCell(Cell cell)
        {
            cell.Value = cell.PickList[0].ToString();
            ChangeColor(cell, Colors.oldValue);
        }

        protected void OnlyChangeColor(Cell cell)
        {
            if (cell.PickList != null)
            {
                if (cell.PickList.IndexOf(cell.Value) > 0)
                    ChangeColor(cell, Colors.newValue);
                else
                    ChangeColor(cell, Colors.oldValue);
            }
        }

        private static bool IsSet(string value)
        {
            return value == "1" || value == "true" || value == "yes";
        }

        public void ChangeColor(Cell cell, Colors colors)
        {
            switch (colors)
            {
                case Colors.badValue: cell.BackColor = badValueColor; break;
                case Colors.newValue: cell.BackColor = newValueColor; break;
                case Colors.okvalue: cell.BackColor = okValueColor; break;
                case Colors.oldValue: cell.BackColor = oldValueColor; break;
                case Colors.warnValue: cell.BackColor = warnValueColor; break;
                default:
                    cell.BackColor = Color.MistyRose;
                    break;
            }
        }

        #region IDisposable Members
        /// <summary>
        /// IDisposable Members
        /// </summary>
        public void Dispose()
        {
            DisposeExec();
        }
        //===================================================
        /// <summary>
        /// Здесь необходимо удалить все
        /// </summary>
        protected virtual void DisposeExec()
        {
        }

        #endregion
    }
}
