﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class Fee
    {
        public const string TableName = PlugTbl.itblXnrfaFees;

        public int Id { get; set; }
        public string RadioSystem { get; set; }
        public string OrderSymbol { get; set; }
        public double MinimalFrequency { get; set; }
        public double MaximalFrequency { get; set; }
        public double MinimalPower { get; set; }
        public double MaximalPower { get; set; }
        public double Cost { get; set; }

        private List<int> _radioTechnologyIdList = new List<int>();
        public List<int> RadioTechnologyIdList { get { return _radioTechnologyIdList; }}


        private RadioSystemList rsl = new RadioSystemList();
            
        public Fee()
        {
            rsl.ReadRadioSystemCodeList();
        }
        
        public void Load()
        {
            using(IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,RADIO_SYSTEM_NAME,ORDER_SYMBOL,MIN_FREQUENCY,MAX_FREQUENCY,MIN_POWER,MAX_POWER,COST");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    RadioSystem = r.GetS("RADIO_SYSTEM_NAME");
                    OrderSymbol = r.GetS("ORDER_SYMBOL");
                    MinimalFrequency = r.GetD("MIN_FREQUENCY");
                    MaximalFrequency = r.GetD("MAX_FREQUENCY");
                    MinimalPower = r.GetD("MIN_POWER");
                    MaximalPower = r.GetD("MAX_POWER");
                    Cost = r.GetD("COST");

                    ReadRaditechnologies();
                }
            }            
        }

        public void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,RADIO_SYSTEM_NAME,ORDER_SYMBOL,MIN_FREQUENCY,MAX_FREQUENCY,MIN_POWER,MAX_POWER,COST");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();
                }
                else
                {
                    Id = IM.AllocID(TableName, 1, -1);
                    r.AddNew();
                    r.Put("ID", Id);
                }

                r.Put("RADIO_SYSTEM_NAME", RadioSystem);
                r.Put("ORDER_SYMBOL", OrderSymbol);
                r.Put("MIN_FREQUENCY", MinimalFrequency);
                r.Put("MAX_FREQUENCY", MaximalFrequency);
                r.Put("MIN_POWER", MinimalPower);
                r.Put("MAX_POWER", MaximalPower);
                r.Put("COST", Cost);

                r.Update();
                WriteRaditechnologies();
            }
            finally
            {
                r.Destroy();
            }
        }

        private void ReadRaditechnologies()
        {
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaFeesRadioSystems, IMRecordset.Mode.ReadOnly);
            try
            {
                string fieldsSelect = "FEE_ID,RADIO_SYSTEM_ID";
                rs.Select(fieldsSelect);
                rs.SetWhere("FEE_ID", IMRecordset.Operation.Eq, Id);
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    int val = rs.GetI("RADIO_SYSTEM_ID");
                    _radioTechnologyIdList.Add(val);
                }
            }
            finally
            {
                rs.Destroy();
            }
        }

        private void WriteRaditechnologies()
        {
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaFeesRadioSystems, IMRecordset.Mode.ReadWrite);
            try
            {
                string fieldsSelect = "FEE_ID,RADIO_SYSTEM_ID";
                rs.Select(fieldsSelect);
                rs.SetWhere("FEE_ID", IMRecordset.Operation.Eq, Id);
                rs.Open();
                bool isAdd = false;
                foreach (int val in _radioTechnologyIdList)
                {
                    if (rs.IsEOF() || (isAdd == true))
                    {
                        isAdd = true;
                        rs.AddNew();
                        rs.Put("FEE_ID", Id);

                    }
                    else
                    {
                        rs.Edit();
                    }
                    rs.Put("RADIO_SYSTEM_ID", val);
                    //CJournal.CheckTable(ICSMTbl.itblEstaAssgn, estaAssgn.GetI("ID"), estaAssgn, fieldsSelect);
                    rs.Update();
                    rs.MoveNext();
                }

                if (isAdd == false)
                    while (!rs.IsEOF())
                    {
                        rs.Delete();
                        rs.MoveNext();
                    }
            }
            finally
            {
                rs.Destroy();
            }
        }

        public string RadioTechnologyIdListAsString()
        {
            ComboBoxDictionaryList<int, string> _dictionary = rsl.GetComboBoxDictionaryList();
            string result = "";            
            foreach (int radioSystemId in _radioTechnologyIdList)
            {
                foreach (ComboBoxDictionary<int, string> kvp in _dictionary)
                {
                    if (kvp.Key == radioSystemId)
                        result = result + kvp.Description + ",";
                }
            }
            
            if (string.IsNullOrEmpty(result))
                return "";
             
            return result.Substring(0,result.Length-1);  
        }
    }
}
