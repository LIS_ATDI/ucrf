﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using LisUtility;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRR;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    /// <summary>
    /// класс станции ЗС
    /// </summary>
    internal class StationRS : BaseStation
    {
        //===================================================		
        public double Height { get; set; }  //Висота поверхні землі, м
        public double Distance { get; set; }
        public double Azimuth { get; set; }
        public double BandWidth { get; set; }
        public int Modulation { get; set; }
        public string Sertificate { get; set; }
        public string Polarization { get; set; }
        public double AntHeight { get; set; }
        public double AntDiam { get; set; }
        public double AntCoef { get; set; }
        public double MaxPower { get; set; }  //Max Мощность передатчика
        public double MinPower { get; set; }  //Min Мощность передатчика
        public double Power { get; set; }  //Мощность передатчика
        public double NomFreq { get; set; }  //частотa передатчика
        public string EmiClass { get; set; }
        /// <summary>
        /// Longitude in *DMS*
        /// </summary>
        public double Lon { get; set; }
        /// <summary>
        /// Latitude in *DMS*
        /// </summary>
        public double Lat { get; set; }

        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна
        public IMObject objStation = null;  //Станция
        public RecordPtr staID;   //Идентификатор станции


        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public StationRS()
        {// Инициализируем все перемнные
            Height = IM.NullD;  //Висота поверхні землі, м
            Distance = IM.NullD;
            Azimuth = IM.NullD;
            BandWidth = IM.NullD;
            Modulation = -1;
            Sertificate = "";
            AntHeight = IM.NullD;
            AntDiam = IM.NullD;
            AntCoef = IM.NullD;
            Polarization = "";
            MaxPower = IM.NullD;
            MinPower = IM.NullD;
            Power = IM.NullD;
            NomFreq = IM.NullD;
            EmiClass = "";
            Lon = IM.NullD;
            Lat = IM.NullD;
        }
        //==============
        //Destructor
        /*  ~StationRS()
          {
             if (objPosition != null)
                objPosition.Dispose();
             if (objEquip != null)
                objEquip.Dispose();
             if (objAntenna != null)
                objAntenna.Dispose();
             if (objStation != null)
                objStation.Dispose();
          }*/
    }
    //===============================================================
    //===============================================================
    /// <summary>
    /// Класс для обработки заявки PC
    /// </summary>
    class MicrowaveApp : BaseAppClass
    {
        private class FreqPlan
        {
            public double Freq { get; private set; }
            public double Bw { get; private set; }
            public FreqPlan(double Freq, double Bw)
            {
                this.Freq = Freq;
                this.Bw = Bw;
            }
        }

        public static readonly string TableName = ICSMTbl.itblMicrowa;
        //===================================================		

        public List<StationRS> st1 = new List<StationRS>(); // Дополнительные данные станции
        public List<StationRS> st2 = new List<StationRS>(); // Дополнительные данные станции
        public List<IMObject> links = new List<IMObject>(); // Все линки
        public int linkCount = 0;
        private FreqPlan[] ArrayFreqPlan = null;
        //===================================================

        public MicrowaveApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, MicrowaveApp.TableName, _ownerId, _packetID, _radioTech)
        {
            int mwID = objStation.GetI("ID");
            int sta1Id = IM.NullI; int sta_PosId1 = IM.NullI;
            int sta2Id = IM.NullI; int sta_PosId2 = IM.NullI;
            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
            r.Select("ID,MW_ID,ROLE,SITE_ID");
            r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
            r.Open();
            for (; !r.IsEOF(); r.MoveNext())
            {
                if (r.GetS("ROLE") == "A")
                {
                    sta1Id = r.GetI("ID");
                    sta_PosId1 = r.GetI("SITE_ID");
                }
                else if (r.GetS("ROLE") == "B")
                {
                    sta2Id = r.GetI("ID");
                    sta_PosId2 = r.GetI("SITE_ID");
                }
            }
            r.Close();
            r.Destroy();

            links.Add(objStation);
            StationRS sta1 = new StationRS();
            sta1.Position = new PositionState();
            sta1.Position.Id = sta_PosId1;
            LoadStationPosition(ref sta1);
            st1.Add(sta1);
            StationRS sta2 = new StationRS();
            sta2.Position = new PositionState();
            sta2.Position.Id = sta_PosId2;
            LoadStationPosition(ref sta2);
            st2.Add(sta2);

            RecordPtr l1 = new RecordPtr();
            l1.Id = sta1Id;
            l1.Table = ICSMTbl.itblMicrows;
            st1[0].staID = l1;
            RecordPtr l2 = new RecordPtr();
            l2.Id = sta2Id;
            l2.Table = ICSMTbl.itblMicrows;
            st2[0].staID = l2;

            if (st1[0].staID.Table == "")
                throw new IMException("TableName is empty");
            if (st1[0].staID.Id > 0 && st1[0].staID.Id != IM.NullI)
                st1[0].objStation = IMObject.LoadFromDB(st1[0].staID);
            else
            {
                st1[0].staID.Id = IM.AllocID(st1[0].staID.Table, 1, -1);
                st1[0].objStation = IMObject.New(st1[0].staID.Table);
                st1[0].objStation["ID"] = st1[0].staID.Id;
                st1[0].objStation["MW_ID"] = mwID;
                st1[0].objStation["ROLE"] = "A";
                st1[0].objStation["END_ROLE"] = "B";
            }

            if (st2[0].staID.Table == "")
                throw new IMException("TableName is empty");
            if (st2[0].staID.Id > 0 && st2[0].staID.Id != IM.NullI)
                st2[0].objStation = IMObject.LoadFromDB(st2[0].staID);
            else
            {
                st2[0].staID.Id = IM.AllocID(st2[0].staID.Table, 1, -1);
                st2[0].objStation = IMObject.New(st2[0].staID.Table);
                st2[0].objStation["ID"] = st2[0].staID.Id;
                st2[0].objStation["MW_ID"] = mwID;
                st2[0].objStation["ROLE"] = "B";
                st2[0].objStation["END_ROLE"] = "A";
            }


            appType = AppType.AppRS;
            department = DepartmentType.VFSR;
            departmentSector = DepartmentSectorType.VFSR_SRZ;
            if (((id == 0) || (id == IM.NullI)) && (objStation != null))
            {
                // Заполняем некоторые поля, которые идут по умолчанию
                objStation["NAME"] = mwID.ToString();
                objStation["ADM"] = "UKR";
                objStation["STANDARD"] = CRadioTech.RS;
                objStation["OPER_KEY"] = "РРЛ-" + links[0].GetI("ID").ToString();
                objStation["NETWORK_IDENT"] = mwID.ToString();
                objStation.Put("DEM_RECEIPT_NUM", NumberIn);
                objStation.Put("DEM_RECEIPT_DATE", DateIn);
                objStation.Put("CONFIRM_NUM", NumberOut);
                objStation.Put("CONFIRM_DATE", DateOut);
            }

            // Загрузка частот
            st2[0].NomFreq = st2[0].objStation.GetD("TX_FREQ");
            st1[0].NomFreq = st1[0].objStation.GetD("TX_FREQ");

            linkCount = 1;

            LoadSectors();


            if (st1[0].objEquip == null)
            {
                int equipId = objStation.GetI("EQPMW_ID");
                if ((equipId > 0) && (equipId != IM.NullI))
                {
                    st1[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipId);
                    st2[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipId);
                }


                /////////////

                  double MaxPower = -1;
                //Заполняем дополнитеотные поля
                  if (st1[0].objEquip != null)
                  {// Заполяем поля значениями
                      double tmpVal = st1[0].objEquip.GetD("MAX_POWER");
                      if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                      {
                          MaxPower = st1[0].objEquip.GetD("MAX_POWER");
                          st1[0].Power = (MaxPower != IM.NullD) ? MaxPower - 30 : IM.NullD;
                      }
                      if (st2[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                      {
                          MaxPower = st2[0].objEquip.GetD("MAX_POWER");
                          st2[0].Power = (MaxPower != IM.NullD) ? MaxPower - 30 : IM.NullD;
                      }

                      if (st1[0].objEquip.GetD("MIN_POWER") != IM.NullD)
                          st1[0].MinPower = st1[0].objEquip.GetD("MIN_POWER") - 30;
                      if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                          st1[0].MaxPower = st1[0].objEquip.GetD("MAX_POWER") - 30;

                      st1[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;
                      st2[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;



                      st1[0].EmiClass = st1[0].objEquip.GetS("DESIG_EMISSION");
                      st2[0].EmiClass = st2[0].objEquip.GetS("DESIG_EMISSION");

                      st1[0].Modulation = Convert.ToInt32(st1[0].objEquip.GetS("MODULATION"));
                  }


                 

                ////////////


            }



            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT14");
            SCVisnovok = objStation.GetS("CUST_TXT15");
            SCNote = objStation.GetS("CUST_TXT16");
            //----
            addValidVisnMonth = 6;
            //----
            //Загрузка частотного плана
            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly))
            {
                List<FreqPlan> list = new List<FreqPlan>();
                rs.Select("Plan.ID");
                rs.Select("FREQ");
                rs.Select("Plan.BANDWIDTH");
                rs.SetWhere("Plan.COD", IMRecordset.Operation.Like, "РРС*");
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    double freq = rs.GetD("FREQ");
                    double bw = rs.GetD("Plan.BANDWIDTH");
                    if ((freq == IM.NullD) || (bw == IM.NullD))
                        continue;
                    list.Add(new FreqPlan(freq, bw));
                }
                ArrayFreqPlan = list.ToArray();
            }

            
        }

        private void LoadStationPosition(ref StationRS Station)
        {
            RecordPtr recPosition = new RecordPtr(ICSMTbl.itblPositionMw, IM.NullI);

            // cell.Value = EquipmentName;

            if (Station.Position != null)
            {
                IMRecordset positionMw = new IMRecordset(ICSMTbl.itblPositionMw, IMRecordset.Mode.ReadOnly);
                positionMw.Select("ID");
                positionMw.SetWhere("ID", IMRecordset.Operation.Eq, Station.Position.Id);
                positionMw.Open();
                if (!positionMw.IsEOF())
                {
                    recPosition.Id = positionMw.GetI("ID");
                }
                positionMw.Close();
                positionMw.Destroy();
            }


            if (recPosition.Id != IM.NullI)
            {
                if (Station.objPosition == null)
                    Station.objPosition = new PositionState2();
                Station.objPosition.LoadStatePosition(recPosition.Id, recPosition.Table);
                Station.objPosition.GetAdminSiteInfo(Station.objPosition.AdminSiteId);
            }
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            if (st1.Count > 0)
            {
                if (st1[0].objEquip != null)
                {
                    st1[0].objEquip.Dispose();
                    st1[0].objEquip = null;
                }
                if (st1[0].objAntenna != null)
                {
                    st1[0].objAntenna.Dispose();
                    st1[0].objAntenna = null;
                }
            }
            if (st2.Count > 0)
            {
                if (st2[0].objEquip != null)
                {
                    st2[0].objEquip.Dispose();
                    st2[0].objEquip = null;
                }
                if (st2[0].objAntenna != null)
                {
                    st2[0].objAntenna.Dispose();
                    st2[0].objAntenna = null;
                }
            }
            for (int i = 1; i < links.Count; i++) //нулевой удалится в базовом классе
                links[i].Dispose();
            base.DisposeExec();
        }

        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        public override void ChangeOwner(int _newOwnerID)
        {
            foreach (IMObject link in links)
                if (link != null && link.GetI("OWNER_ID") != _newOwnerID)
                {
                    link.Put("OWNER_ID", _newOwnerID);
                    CJournal.CheckTable(recordID.Table, recordID.Id, link);
                    link.SaveToDB();
                    CJournal.CreateReport(applID, appType);
                }
        }

        public override void Coordination()
        {
            base.Coordination();
            CoordForm form = null;
            PositionState pos1 = new PositionState();
            PositionState pos2 = new PositionState();
            if (st1 != null)
                if (st1.Count > 0)
                    pos1.LoadStatePosition(st1[0].objPosition.Id, st1[0].objPosition.TableName);
            if (st2 != null)
                if (st2.Count > 0)
                    pos2.LoadStatePosition(st2[0].objPosition.Id, st2[0].objPosition.TableName);
            if (pos1.Id != IM.NullI && pos2.Id != IM.NullI)
            {
                form = new CoordForm(pos1, pos2, TableName, applID);
                if (form.ShowDialog() == DialogResult.Yes && objStation != null)
                {
                    IMRecordset rsMicrowa = new IMRecordset(objTableName, IMRecordset.Mode.ReadOnly);
                    try
                    {
                        rsMicrowa.Select("ID,HCM_STATUS,HCM_LADMS");
                        rsMicrowa.SetWhere("ID", IMRecordset.Operation.Eq, recID);
                        rsMicrowa.Open();
                        if (!rsMicrowa.IsEOF())
                        {
                            objStation.Put("HCM_STATUS", rsMicrowa.GetS("HCM_STATUS"));
                            objStation.Put("HCM_LADMS", rsMicrowa.GetS("HCM_LADMS"));
                        }
                    }
                    finally
                    {
                        rsMicrowa.Final();
                    }
                }
            }
        }

        private int LoadSectors()
        {
            IMRecordset r2 = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r2.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r2.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r2.Open();

            int Loaded = 1;
            if (!r2.IsEOF())
            {// Заявки нет     

                for (int i = 2; i <= 6; i++)
                {
                    int stationID = r2.GetI("OBJ_ID" + i.ToString());
                    if (stationID != IM.NullI)
                    {
                        int mwID = stationID;
                        int sta1Id = IM.NullI;
                        int sta2Id = IM.NullI;
                        IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,MW_ID,ROLE");
                        r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                        r.Open();
                        for (; !r.IsEOF(); r.MoveNext())
                        {
                            if (r.GetS("ROLE") == "A")
                                sta1Id = r.GetI("ID");
                            else if (r.GetS("ROLE") == "B")
                                sta2Id = r.GetI("ID");
                        }
                        r.Close();
                        r.Destroy();
                        RecordPtr linkptr = new RecordPtr(ICSMTbl.itblMicrowa, mwID);
                        links.Add(IMObject.LoadFromDB(linkptr));
                        StationRS sta1 = new StationRS();
                        st1.Add(sta1);
                        StationRS sta2 = new StationRS();
                        st2.Add(sta2);

                        RecordPtr l1 = new RecordPtr();
                        l1.Id = sta1Id;
                        l1.Table = ICSMTbl.itblMicrows;
                        st1[linkCount].staID = l1;
                        RecordPtr l2 = new RecordPtr();
                        l2.Id = sta2Id;
                        l2.Table = ICSMTbl.itblMicrows;
                        st2[linkCount].staID = l2;

                        if (st1[linkCount].staID.Table == "")
                            throw new IMException("TableName is empty");
                        if (st1[linkCount].staID.Id > 0 && st1[linkCount].staID.Id != IM.NullI)
                            st1[linkCount].objStation = IMObject.LoadFromDB(st1[linkCount].staID);
                        else
                        {
                            st1[linkCount].staID.Id = IM.AllocID(st1[linkCount].staID.Table, 1, -1);
                            st1[linkCount].objStation = IMObject.New(st1[linkCount].staID.Table);
                            st1[linkCount].objStation["ID"] = st1[linkCount].staID.Id;
                            st1[linkCount].objStation["MW_ID"] = mwID;
                            st1[linkCount].objStation["ROLE"] = "A";
                            st1[linkCount].objStation["END_ROLE"] = "B";
                        }

                        if (st2[linkCount].staID.Table == "")
                            throw new IMException("TableName is empty");
                        if (st2[linkCount].staID.Id > 0 && st2[linkCount].staID.Id != IM.NullI)
                            st2[linkCount].objStation = IMObject.LoadFromDB(st2[linkCount].staID);
                        else
                        {
                            st2[linkCount].staID.Id = IM.AllocID(st2[linkCount].staID.Table, 1, -1);
                            st2[linkCount].objStation = IMObject.New(st2[linkCount].staID.Table);
                            st2[linkCount].objStation["ID"] = st2[linkCount].staID.Id;
                            st2[linkCount].objStation["MW_ID"] = mwID;
                            st2[linkCount].objStation["ROLE"] = "B";
                            st2[linkCount].objStation["END_ROLE"] = "A";
                        }

                        // Загрузка частот
                        st2[linkCount].NomFreq = st2[linkCount].objStation.GetD("TX_FREQ");
                        st1[linkCount].NomFreq = st1[linkCount].objStation.GetD("TX_FREQ");
                        Loaded++;
                        linkCount++;
                    }
                }
            }
            r2.Close();
            r2.Destroy();

            return Loaded;
        }

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if (docType == DocType.VISN)
            {
                string prov = "";
                string city = "";
                if (st1[0].objPosition != null)
                {
                    prov = st1[0].objPosition.Province;
                    if (prov == "")
                        prov = st1[0].objPosition.City;
                    if (st2[0].objPosition != null)
                    {
                        string prov2 = st2[0].objPosition.Province;
                        if (prov2 == "")
                            prov2 = st2[0].objPosition.City;
                        if (prov != prov2)
                        {
                            using (FProvChoose form = new FProvChoose())
                            {
                                form.ProvinceList.Add(prov);
                                form.ProvinceList.Add(prov2);
                                form.ShowDialog();
                                prov = form.SelectedProvince;
                                city = form.SelectedProvince;
                            }
                        }
                    }
                }
                else
                {
                    if (st2[0].objPosition != null)
                    {
                        prov = st2[0].objPosition.Province;
                        city = st2[0].objPosition.City;
                    }
                }
                fullPath += ConvertType.DepartmetToCode(DepartmentType.VFSR) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            }
            else if (docType == DocType.DOZV)
            {
                string prov = "";
                string city = "";
                if (st1[0].objPosition != null)
                {
                    prov = st1[0].objPosition.Province;
                    if (prov == "")
                        prov = st1[0].objPosition.City;
                    if (st2[0].objPosition != null)
                    {
                        string prov2 = st2[0].objPosition.Province;
                        if (prov2 == "")
                            prov2 = st2[0].objPosition.City;
                        if (prov != prov2)
                        {
                            using (FProvChoose form = new FProvChoose())
                            {
                                form.ProvinceList.Add(prov);
                                form.ProvinceList.Add(prov2);
                                form.ShowDialog();
                                prov = form.SelectedProvince;
                                city = form.SelectedProvince;
                            }
                        }
                    }
                }
                else
                {
                    if (st2[0].objPosition != null)
                    {
                        prov = st2[0].objPosition.Province;
                        city = st2[0].objPosition.City;
                    }
                }
                fullPath += "РРЛ-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";

            }
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    //TODO Доделать для многосекторных линков
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    foreach (IMObject obj in links)
        //    {
        //        int mwID = obj.GetI("ID");
        //        string name = docName.ToFileNameWithoutExtension();
        //        if (docType == DocType.VISN)
        //        {
        //            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadWrite);
        //            r.Select("ID,MODIF_DECIS_NUM,MODIF_DEM_DATE,MODIF_DATE,MODIF_DEM_REF,DEM_RECEIPT_NUM,DEM_RECEIPT_DATE,CONFIRM_NUM,CONFIRM_DATE");
        //            r.SetWhere("ID", IMRecordset.Operation.Eq, mwID);
        //            try
        //            {
        //                r.Open();
        //                if (!r.IsEOF())
        //                {
        //                    r.Edit();

        //                    r.Put("MODIF_DECIS_NUM", name);
        //                    objStation.Put("MODIF_DECIS_NUM", name);
        //                    r.Put("MODIF_DEM_DATE", startDate);
        //                    objStation.Put("MODIF_DEM_DATE", startDate);
        //                    r.Put("MODIF_DATE", endDate);
        //                    objStation.Put("MODIF_DATE", endDate);
        //                    r.Put("MODIF_DEM_REF", IM.ConnectedUser());
        //                    objStation.Put("MODIF_DEM_REF", IM.ConnectedUser());
        //                    r.Update();
        //                }
        //            }
        //            finally
        //            {
        //                r.Close();
        //                r.Destroy();
        //            }

        //        }
        //    }
        //}

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            int SectorCount = 0;
            SelectSectorForm selectSectorForm = new SelectSectorForm();
            selectSectorForm.InitList();
            for (int i = 0; i < links.Count; i++)
            {
                SectorCount++;
                selectSectorForm.AddSector(links[i].GetI("ID"));
            }

            if (SectorCount > 1)
            {
                if (selectSectorForm.ShowDialog() == DialogResult.OK)
                {
                    FormEMS formEms = new FormEMS(recordID.Table, selectSectorForm.GetSelectedIndex());
                    formEms.ShowDialog();
                    formEms.Dispose();
                }
                selectSectorForm.Dispose();
            }
            else
            {
                FormEMS formEms = new FormEMS(recordID.Table, recordID.Id);
                formEms.ShowDialog();
                formEms.Dispose();
            }
        }

        protected override string GetXMLParamGrid()
        {
            string XmlString =
                 "<items>"
               + "<item key='header1'    type='lat'       flags=''     tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
               + "<item key='header2'    type='lat'       flags='s'    tab=''   display='РРС-1'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"
               + "<item key='header3'    type='lat'       flags='s'    tab=''   display='РРС-2'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"
                //
                   + "<item key='mKLat'      type='lat'       flags=''     tab='0'  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLat1'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='KLat2'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
                //
                   + "<item key='mKLon'      type='lat'       flags=''     tab='0'  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLon1'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='KLon2'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
                //
                   + "<item key='kAsl'       type='lat'       flags=''     tab='0'  display='Висота поверхні землі, м'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='asl1'       type='double' DBField='ASL' flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='asl2'       type='double' DBField='ASL' flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //
                   + "<item key='kDist'      type='lat'       flags=''     tab='0'  display='Відстань між РРС'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='dist1'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"

                   + "<item key='kAz'        type='lat'       flags=''     tab='0'  display='Кут азимуту випромінювання, град'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='az1'        type='double' DBField='AZIMUTH' flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='az2'        type='double' DBField='AZIMUTH' flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //
                   + "<item key='kAdr'       type='lat'       flags=''     tab='0'  display='Адреса місця встановлення РЕЗ'     ReadOnly='true' background='' bold='y' />"
               + "<item key='addr1'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='addr2'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
                //
                   + "<item key='kRez'       type='lat'       flags=''     tab='0'  display='Назва/Тип РЕЗ'     ReadOnly='true' background='' bold='y' />"
               + "<item key='rez'        type='string' DBField='INFO1' flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
                //          
                   + "<item key='kPow'       type='lat'       flags=''     tab='0'  display='Макс. потужність передавача, дБВт'     ReadOnly='true' background='' bold='' />"
               + "<item key='pow1'       type='double' DBField='POWER' flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='pow2'       type='double' DBField='POWER' flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //
                   + "<item key='kWidth'     type='lat'       flags=''     tab='0'  display='Ширина смуги, МГц'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='width1'     type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //            
                   + "<item key='kClass'     type='lat'       flags=''     tab='0'  display='Клас випромінювання'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='class1'     type='string' DBField='DESIG_EM' flags='s'     tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //            
                   + "<item key='kMod'       type='lat'       flags=''     tab='0'  display='Тип і параметри модуляції'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='mod1'       type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //            
                   + "<item key='kSert'      type='lat'       flags=''     tab='0'  display='Сертифікат відповідності(№/дата)'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='sertNom'    type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='sertDate'   type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //
                   + "<item key='kAntType'   type='lat'       flags=''     tab='0'  display='Тип антени'     ReadOnly='true' background='' bold='y' />"
               + "<item key='antType1'   type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='antType2'   type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
                //
                   + "<item key='kAntHeight' type='lat'       flags=''     tab='0'  display='Висота антени над рівнем землі, м'     ReadOnly='true' background='' bold='' />"
               + "<item key='antHeight1'  type='double' DBField='AGL1'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='antHeight2' type='double' DBField='AGL1'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //
                   + "<item key='kAntD'      type='lat'       flags=''     tab='0'  display='Діам антени, м/коеф. підсилення, дБі'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='antD1'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='antKoef1'   type='double' DBField='GAIN'  flags='s'     tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='antD2'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "<item key='antKoef2'   type='double' DBField='GAIN'  flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
                //
                   + "<item key='kPol'       type='lat'       flags=''     tab='0'  display='Тип поляризації'     ReadOnly='true' background='' bold='' />"
               + "<item key='pol1'       type='string' DBField='POLAR' flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='pol2'       type='string' DBField='POLAR' flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //
                   + "<item key='kFreqNom0'   type='lat'       flags=''     tab='0'  display='Номінал частот передавання, МГц'     ReadOnly='true' background='' bold='y' />"
               + "<item key='freqNom1_0'   type='double' DBField='TX_FREQ'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='freqNom2_0'   type='double' DBField='TX_FREQ'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"
                //
                //+ "<item key='kObj'       type='lat'       flags=''     tab='0'  display='Об`єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray' />"
                //+ "<item key='obj1'       type='lat'       flags='s'    tab='0'  display=''     ReadOnly='true' background='' bold='' fontColor='gray' HorizontalAlignment='center' />"
               + "</items>"
                //
               ;
            return XmlString;
        }
        //============================================================
        /// <summary>
        /// Initialization for entire grid
        /// It's needed for setup columns for each sector
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Grid grid)
        {
            base.InitParamGrid(grid);
            int tabord = 18;
            for (int i = 1; i < 6 && i < links.Count; i++)
            {
                StationRS Station1 = st1[i];
                StationRS Station2 = st1[i];
                {
                    Row r = grid.AddRow();
                    Cell c0 = r.AddCell("Частота передавання, МГц (ID " + links[i].GetI("ID").ToString() + ")");
                    c0.Key = "kFreqNom" + i.ToString();
                    c0.FontName = grid.GetCellFromKey("kFreqNom0").FontName;
                    c0.TextHeight = grid.GetCellFromKey("kFreqNom0").TextHeight;
                    c0.FontStyle = grid.GetCellFromKey("kFreqNom0").FontStyle;
                    c0.HorizontalAlignment = grid.GetCellFromKey("kFreqNom0").HorizontalAlignment;
                    c0.VerticalAlignment = grid.GetCellFromKey("kFreqNom0").VerticalAlignment;
                    c0.DBField = grid.GetCellFromKey("kFreqNom0").DBField;
                    c0.TextColor = grid.GetCellFromKey("kFreqNom0").TextColor;
                    c0.CanEdit = grid.GetCellFromKey("kFreqNom0").CanEdit;
                    c0.BackColor = grid.GetCellFromKey("kFreqNom0").BackColor;
                    c0.Type = grid.GetCellFromKey("kFreqNom0").Type;

                    Cell c1 = ExtendGrid(grid, r, "freqNom1_", i, "", tabord++);
                    Cell c2 = ExtendGrid(grid, r, "freqNom2_", i, "", tabord++);
                }
            }

        }
        public override void AddSector(Grid grid)
        {
            int stacount = links.Count;
            if (stacount < 6)
            {
                StationRS Station1 = new StationRS();
                StationRS Station2 = new StationRS();
                st1.Add(Station1);
                st2.Add(Station2);

                IMObject recLink = IMObject.New(ICSMTbl.itblMicrowa);
                int newid = recLink.GetI("ID");

                RecordPtr l1 = new RecordPtr();
                l1.Id = newid;
                l1.Table = ICSMTbl.itblMicrows;
                Station1.staID = l1;
                RecordPtr l2 = new RecordPtr();
                l2.Id = newid;
                l2.Table = ICSMTbl.itblMicrows;
                Station2.staID = l2;

                Station1.staID.Id = IM.AllocID(Station1.staID.Table, 1, -1);
                Station1.objStation = IMObject.New(Station1.staID.Table);
                Station1.objStation["ID"] = Station1.staID.Id;
                Station1.objStation["MW_ID"] = newid;
                Station1.objStation["ROLE"] = "A";
                Station1.objStation["END_ROLE"] = "B";
                Station1.objStation["AGL1"] = st1[0].AntHeight;
                Station1.objStation["AZIMUTH"] = st1[0].Azimuth;
                Station1.objStation["POLAR"] = st1[0].Polarization;
                Station1.objStation["POWER"] = st1[0].Power + 30;
                Station1.objStation["GAIN"] = st1[0].AntCoef;


                Station2.staID.Id = IM.AllocID(Station2.staID.Table, 1, -1);
                Station2.objStation = IMObject.New(Station2.staID.Table);
                Station2.objStation["ID"] = Station2.staID.Id;
                Station2.objStation["MW_ID"] = newid;
                Station2.objStation["ROLE"] = "B";
                Station2.objStation["END_ROLE"] = "A";
                Station2.objStation["AGL1"] = st2[0].AntHeight;
                Station2.objStation["AZIMUTH"] = st2[0].Azimuth;
                Station2.objStation["POLAR"] = st2[0].Polarization;
                Station2.objStation["POWER"] = st2[0].Power + 30;
                Station2.objStation["GAIN"] = st2[0].AntCoef;



                // Заполняем некоторые поля, которые идут по умолчанию
                recLink["NAME"] = newid.ToString();
                recLink["ADM"] = "UKR";
                recLink["STANDARD"] = CRadioTech.RS;
                recLink["OPER_KEY"] = "РРЛ-" + links[0].GetI("ID").ToString();
                recLink["NETWORK_IDENT"] = newid.ToString();
                recLink.Put("DEM_RECEIPT_NUM", NumberIn);
                recLink.Put("DEM_RECEIPT_DATE", DateIn);
                recLink.Put("CONFIRM_NUM", NumberOut);
                recLink.Put("OWNER_ID", links[0].GetI("OWNER_ID"));
                recLink.Put("DESIG_EM", st1[0].EmiClass);
                links.Add(recLink);

                int cellTabOrder = 16 + stacount * 2;

                Row r = grid.AddRow();
                Cell c0 = r.AddCell("Частота передавання, МГц (ID " + links[stacount].GetI("ID").ToString() + ")");
                c0.Key = "kFreqNom" + stacount.ToString();
                c0.FontName = grid.GetCellFromKey("kFreqNom0").FontName;
                c0.TextHeight = grid.GetCellFromKey("kFreqNom0").TextHeight;
                c0.FontStyle = grid.GetCellFromKey("kFreqNom0").FontStyle;
                c0.HorizontalAlignment = grid.GetCellFromKey("kFreqNom0").HorizontalAlignment;
                c0.VerticalAlignment = grid.GetCellFromKey("kFreqNom0").VerticalAlignment;
                c0.DBField = grid.GetCellFromKey("kFreqNom0").DBField;
                c0.TextColor = grid.GetCellFromKey("kFreqNom0").TextColor;
                c0.CanEdit = grid.GetCellFromKey("kFreqNom0").CanEdit;
                c0.BackColor = grid.GetCellFromKey("kFreqNom0").BackColor;
                c0.Type = grid.GetCellFromKey("kFreqNom0").Type;

                Cell c1 = ExtendGrid(grid, r, "freqNom1_", stacount, "", 0);
                c1.TabOrder = cellTabOrder++;
                CellValidate(c1, grid);
                Cell c2 = ExtendGrid(grid, r, "freqNom2_", stacount, "", 0);
                c2.TabOrder = cellTabOrder++;
                CellValidate(c2, grid);

                Cell ct = grid.GetCellFromKey("freqNom2_" + (links.Count - 2).ToString());
                ct.cellStyle = EditStyle.esSimple;

                grid.Focus();

                grid.CellSelect(c1);
            }
        }

        public override void RemoveSector(Grid grid)
        {
            if (links.Count > 1)
            {
                HelpClasses.Forms.FSectorRemove form = new XICSM.UcrfRfaNET.HelpClasses.Forms.FSectorRemove();
                for (int i = 1; i <= links.Count; ++i)
                    form.cbSecs.Items.Add("Лінк " + i.ToString());
                form.cbSecs.SelectedIndex = 0;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    int index = form.cbSecs.SelectedIndex;
                    if (index == 0)
                    {
                        st1[1].objPosition = st1[0].objPosition;
                        st2[1].objPosition = st2[0].objPosition;
                        st1[1].objAntenna = st1[0].objAntenna;
                        st2[1].objAntenna = st2[0].objAntenna;
                        st1[1].objEquip = st1[0].objEquip;
                        st1[1].Polarization = st1[0].Polarization;
                        st2[1].Polarization = st2[0].Polarization;
                        st2[1].AntHeight = st2[0].AntHeight;
                        st1[1].AntHeight = st1[0].AntHeight;
                        st2[1].Azimuth = st2[0].Azimuth;
                        st1[1].Azimuth = st1[0].Azimuth;
                        st2[1].Power = st2[0].Power;
                        st1[1].Power = st1[0].Power;
                        st2[1].AntCoef = st2[0].AntCoef;
                        st1[1].AntCoef = st1[0].AntCoef;
                    }

                    AddDeletedSector(links[index].GetI("ID"), recordID.Table);
                    links.RemoveAt(index);

                    st1.RemoveAt(index);
                    st2.RemoveAt(index);

                    grid.rowList.Clear();
                    LoadStructureParamGrid(grid);
                    InitParamGrid(grid);
                    UpdateGridParameters(grid);
                }
            }
        }

        //===================================================
        /// <summary>
        /// Внутрення процедура добавления ячейки для сектора.
        ///  устанавливает стили слетки.
        /// </summary>
        /// <returns>XML строка</returns>
        private Cell ExtendGrid(Grid grid, Row r, string KeyPrefix, int staNum, string value, int tab)
        {
            Cell newCell = r.AddCell(value);
            newCell.Key = KeyPrefix + staNum.ToString();
            newCell.FontName = grid.GetCellFromKey(KeyPrefix + "0").FontName;
            newCell.TextHeight = grid.GetCellFromKey(KeyPrefix + "0").TextHeight;
            newCell.FontStyle = grid.GetCellFromKey(KeyPrefix + "0").FontStyle;
            newCell.HorizontalAlignment = grid.GetCellFromKey(KeyPrefix + "0").HorizontalAlignment;
            newCell.VerticalAlignment = grid.GetCellFromKey(KeyPrefix + "0").VerticalAlignment;
            newCell.DBField = grid.GetCellFromKey(KeyPrefix + "0").DBField;
            newCell.TextColor = grid.GetCellFromKey(KeyPrefix + "0").TextColor;
            newCell.CanEdit = grid.GetCellFromKey(KeyPrefix + "0").CanEdit;
            newCell.BackColor = grid.GetCellFromKey(KeyPrefix + "0").BackColor;
            newCell.Type = grid.GetCellFromKey(KeyPrefix + "0").Type;
            newCell.Value = value;
            newCell.TabOrder = tab;
            InitParamGrid(newCell);
            return newCell;
        }
        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KLon1") || (cell.Key == "KLon2")
                || (cell.Key == "addr1") || (cell.Key == "addr2")
            || (cell.Key == "asl1") || (cell.Key == "asl2")
                || (cell.Key == "rez")
                || (cell.Key == "antType1") || (cell.Key == "antType2"))
                cell.cellStyle = EditStyle.esEllipsis;
            else if (cell.Key == "freqNom2_" + (links.Count - 1).ToString())
            {
                cell.cellStyle = EditStyle.esEllipsis;
                cell.ButtonText = "Ліц.";
            }
            else if ((cell.Key == "pol1"))
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Standard);
            }
            else if ((cell.Key == "pol2"))
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Standard);
            }
            if ((cell.Key == "obj1") || (cell.Key == "dist1") || (cell.Key == "class1") || (cell.Key == "width1")
                 || (cell.Key == "mod1") || (cell.Key == "rez"))
                cell.HorizontalAlignment = HorizontalAlignment.Center;


            if (cell.Key == "kFreqNom0")
                cell.Value = "Частота передавання, МГц (ID " + links[0].GetI("ID").ToString() + ")";

            /*	if (cell.Key == "obj1")
                    cell.TabOrder = 18;
                else */

            if (cell.Key == "freqNom2_5")
                cell.TabOrder = 27;
            else if (cell.Key == "freqNom1_5")
                cell.TabOrder = 26;
            else if (cell.Key == "freqNom2_4")
                cell.TabOrder = 25;
            else if (cell.Key == "freqNom1_4")
                cell.TabOrder = 24;
            else if (cell.Key == "freqNom2_3")
                cell.TabOrder = 23;
            else if (cell.Key == "freqNom1_3")
                cell.TabOrder = 22;
            else if (cell.Key == "freqNom2_2")
                cell.TabOrder = 21;
            else if (cell.Key == "freqNom1_2")
                cell.TabOrder = 20;
            else if (cell.Key == "freqNom2_1")
                cell.TabOrder = 19;
            else if (cell.Key == "freqNom1_1")
                cell.TabOrder = 18;
            else if (cell.Key == "freqNom2_0")
                cell.TabOrder = 17;
            else if (cell.Key == "freqNom1_0")
                cell.TabOrder = 16;
            else if (cell.Key == "pol2")
                cell.TabOrder = 15;
            else if (cell.Key == "pol1")
                cell.TabOrder = 14;
            else if (cell.Key == "antHeight2")
                cell.TabOrder = 13;
            else if (cell.Key == "antHeight1")
                cell.TabOrder = 12;
            else if (cell.Key == "antType2")
                cell.TabOrder = 11;
            else if (cell.Key == "antType1")
                cell.TabOrder = 10;
            else if (cell.Key == "pow2")
                cell.TabOrder = 9;
            else if (cell.Key == "pow1")
                cell.TabOrder = 8;
            else if (cell.Key == "rez")
                cell.TabOrder = 7;
            else if (cell.Key == "addr2")
                cell.TabOrder = 6;
            else if (cell.Key == "KLon2")
                cell.TabOrder = 5;
            else if (cell.Key == "KLat2")
                cell.TabOrder = 4;
            else if (cell.Key == "addr1")
                cell.TabOrder = 3;
            else if (cell.Key == "KLon1")
                cell.TabOrder = 2;
            else if (cell.Key == "KLat1")
                cell.TabOrder = 1;
            else
                cell.TabOrder = -1;

        }
        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения РЧП в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            if (st1[0].objPosition == null && st2[0].objPosition == null)
                throw new Exception("Не задано (обрано) жодного сайту!");

            if (st1[0].objPosition != null)
            {
                //if (st1[0].objPosition != null)
                //    st1[0].objPosition.Save();
                if (st1[0].objPosition != null)
                    foreach (StationRS st in st1)
                        st.objStation["SITE_ID"] = st1[0].objPosition.Id;				
            }

            if (st1[0].objAntenna != null)
                foreach (StationRS st in st1)
                    st.objStation["ANT_ID"] = st1[0].objAntenna.GetI("ID");

            if (st1[0].objEquip != null)
                foreach (IMObject obj in links)
                    obj["EQPMW_ID"] = st1[0].objEquip.GetI("ID");

            if (st2[0].objPosition != null)
            {
                //if (st2[0].objPosition != null)
                //    st2[0].objPosition.Save();
                if (st2[0].objPosition != null)
                    foreach (StationRS st in st2)
                        st.objStation["SITE_ID"] = st2[0].objPosition.Id;
                //MessageBox.Show(objLink1.GetI("SITE_ID").ToString());
            }
            if (st2[0].objAntenna != null)
                foreach (StationRS st in st2)
                    st.objStation["ANT_ID"] = st2[0].objAntenna.GetI("ID");

            foreach (StationRS st in st1)
                CJournal.CheckTable(ICSMTbl.itblMicrows, st.objStation.GetI("ID"), st.objStation);
            foreach (StationRS st in st2)
                CJournal.CheckTable(ICSMTbl.itblMicrows, st.objStation.GetI("ID"), st.objStation);

            // Сохраняем "Особливі умови"
            foreach (IMObject obj in links)
            {
                obj["CUST_TXT14"] = SCDozvil;
                obj["CUST_TXT15"] = SCVisnovok;
                obj["CUST_TXT16"] = SCNote;
                obj.Put("CUST_CHB1", IsTechnicalUser);
                obj.Put("STATUS", Status.ToStr()); //Сохраняем статус
                obj.Put("CHANNEL_SEP", (st1[0].BandWidth != IM.NullD) ? st1[0].BandWidth*1000.0 : IM.NullD);
                CJournal.CheckTable(ICSMTbl.itblMicrowa, obj.GetI("ID"), obj);
                obj.SaveToDB();
            }

            //Расчет Углов места (elevation)
            double f1 = st1[0].Lat.DmsToDec();
            double f2 = st2[0].Lat.DmsToDec();
            double l1 = st1[0].Lon.DmsToDec();
            double l2 = st2[0].Lon.DmsToDec();
            double h1 = (st1[0].AntHeight != IM.NullD) ? st1[0].AntHeight : 0.0;
            double h2 = (st2[0].AntHeight != IM.NullD) ? st2[0].AntHeight : 0.0;
            double alv1 = (st1[0].Height != IM.NullD) ? st1[0].Height : 0.0;
            double alv2 = (st2[0].Height != IM.NullD) ? st2[0].Height : 0.0;
            double dist = 111.0*Math.Sqrt(Math.Pow(f1 - f2, 2.0) + Math.Pow((l1 - l2)*Math.Cos(f1/2.0 + f2/2.0), 2.0));
            double alav1 = (180.0/Math.PI)*Math.Atan(((alv2 + h2 - alv1 - h1)/dist - dist/17)*0.001);
            double alav2 = (180.0/Math.PI)*Math.Atan(((alv1 + h1 - alv2 - h2)/dist - dist/17)*0.001);

            foreach (StationRS st in st1)
            {
                st.objStation.Put("ANGLE_ELEV", alav1);
                st.objStation.SaveToDB();
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadWrite);
                r.Select("ID,POLAR,MW_ID");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[st1.IndexOf(st)].GetI("ID"));
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        if (r.GetS("POLAR") != st1[0].Polarization)
                        {
                            r.Edit();
                            r["POLAR"] = st1[0].Polarization;
                            CJournal.CheckTable(ICSMTbl.itblMicrows, st.objStation.GetI("ID"), r, "POLAR");
                            r.Update();
                        }
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }

            }
            foreach (StationRS st in st2)
            {
                st.objStation.Put("ANGLE_ELEV", alav2);
                st.objStation.SaveToDB();
                IMRecordset r2 = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadWrite);
                r2.Select("POLAR,ID,MW_ID");
                r2.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[st2.IndexOf(st)].GetI("ID"));
                r2.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                try
                {
                    r2.Open();
                    if (!r2.IsEOF())
                    {
                        if (r2.GetS("POLAR") != st2[0].Polarization)
                        {
                            r2.Edit();
                            r2["POLAR"] = st1[0].Polarization;
                            CJournal.CheckTable(ICSMTbl.itblMicrows, st.objStation.GetI("ID"), r2, "POLAR");
                            r2.Update();
                        }
                    }
                }
                finally
                {
                    r2.Close();
                    r2.Destroy();
                }
            }

            IMRecordset ra = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            ra.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            ra.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            ra.Open();

            if (!ra.IsEOF())
            {
                ra.Edit();

                for (int i = 0; i < 6; i++)
                {
                    string ObjIdName = "OBJ_ID" + (i + 1).ToString();
                    if (i < links.Count)
                    {
                        ra.Put(ObjIdName, links[i].GetI("ID"));
                    }
                    else
                        ra.Put(ObjIdName, IM.NullI);
                }
                ra.Update();
            }
            ra.Close();
            ra.Destroy();

            SaveLicence();
            return true;
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            MicrowaveApp retStation = new MicrowaveApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            MicrowaveApp retStation = newAppl as MicrowaveApp;
            retStation.CopyFromMe(grid, (st1[0].objEquip != null) ? st1[0].objEquip.GetI("ID") : IM.NullI,
               (st1[0].objAntenna != null) ? st1[0].objAntenna.GetI("ID") : IM.NullI,
               (st2[0].objAntenna != null) ? st2[0].objAntenna.GetI("ID") : IM.NullI,
               st1[0].objStation.GetD("POWER") - 30,
               st2[0].objStation.GetD("POWER") - 30,
               st1[0].objStation.GetS("POLAR"),
               st2[0].objStation.GetS("POLAR"));
            return retStation;
        }
        //===========================================================
        public void CopyFromMe(Grid grid, int equipID, int antID_A, int antID_B, double maxPow_A, double maxPow_B, string PolarA, string PolarB)
        {
            Cell tmpCell = null;
            // Оборудование
            if ((equipID != 0) || (equipID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("rez");
                st1[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipID);
                st2[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipID);
                tmpCell.Value = st1[0].objEquip.GetS("NAME");
                OnCellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }

            // Антенна А
            if ((antID_A != 0) || (antID_A != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("antType1");
                st1[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblMicrowAntenna, antID_A);
                tmpCell.Value = st1[0].objAntenna.GetS("NAME");
                AutoFill(tmpCell, grid);
            }
            // Антенна B
            if ((antID_B != 0) || (antID_B != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("antType2");
                st2[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblMicrowAntenna, antID_B);
                tmpCell.Value = st2[0].objAntenna.GetS("NAME");
                AutoFill(tmpCell, grid);
            }

            // Мощность А
            tmpCell = grid.GetCellFromKey("pow1");
            tmpCell.Value = maxPow_A.ToString();
            OnCellValidate(tmpCell, grid);
            // Мощность В
            tmpCell = grid.GetCellFromKey("pow2");
            tmpCell.Value = maxPow_A.ToString();
            OnCellValidate(tmpCell, grid);
            // Поляризация
            tmpCell = grid.GetCellFromKey("pol1");
            tmpCell.Value = PolarA;
            OnCellValidate(tmpCell, grid);

            tmpCell = grid.GetCellFromKey("pol2");
            tmpCell.Value = PolarB;
            OnCellValidate(tmpCell, grid);
        }
        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            if (cell.Key == "rez")
            {
                if ((st1[0].objEquip != null) && (cell.Value != st1[0].objEquip.GetS("NAME")))
                    st1[0].objEquip = null;  //Будем искать новое оборудование, так как имя уже не совпадает

                /*if (st1.objEquip == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipMicrow, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblEquipMicrow, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME,MBITPS");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recEquip.Id = r.GetI("ID");
                        if (r.GetD("MBITPS") != IM.NullD)
                            objStation["MBITPS"] = r.GetD("MBITPS");
                    }
                    r.Close();
                    r.Destroy();
                    if (recEquip.Id > 0)
                    {
                        st1.objEquip = IMObject.LoadFromDB(recEquip);
                        st2.objEquip = IMObject.LoadFromDB(recEquip);						
                    }
                }
                AutoFill(cell, grid);*/
            }
            else if (cell.Key == "antType1")
            {
                if ((st1[0].objAntenna != null) && (cell.Value != st1[0].objAntenna.GetS("NAME")))
                    st1[0].objAntenna = null;  //Будем искать новую антенну, так как имя уже не совпадает

                /*if (st1.objAntenna == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recAnt = new RecordPtr(ICSMTbl.itblMicrowAntenna, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowAntenna, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recAnt.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recAnt.Id > 0)
                        st1.objAntenna = IMObject.LoadFromDB(recAnt);
                }
                AutoFill(cell, grid);*/
            }
            else if (cell.Key == "antType2")
            {
                if ((st2[0].objAntenna != null) && (cell.Value != st2[0].objAntenna.GetS("NAME")))
                    st2[0].objAntenna = null;  //Будем искать новую антенну, так как имя уже не совпадает

                /*if (st2.objAntenna == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recAnt = new RecordPtr(ICSMTbl.itblMicrowAntenna, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowAntenna, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recAnt.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recAnt.Id > 0)
                        st2.objAntenna = IMObject.LoadFromDB(recAnt);
                }
                AutoFill(cell, grid);*/
            }
            else if (cell.Key == "KLon2")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    st2[0].Lon = lon;
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                }
                if (st2[0].objPosition != null)
                    IndicateDifference(cell, st2[0].objPosition.LonDiffersFromAdm);
                Cell fillCell = grid.GetCellFromKey("dist1");
                UpdateOneParamGrid(fillCell, grid);
            }
            else if (cell.Key == "KLon1")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    st1[0].Lon = lon;
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                }
                if (st1[0].objPosition != null)
                    IndicateDifference(cell, st1[0].objPosition.LonDiffersFromAdm);
                Cell fillCell = grid.GetCellFromKey("dist1");
                UpdateOneParamGrid(fillCell, grid);
            }
            else if (cell.Key == "KLat2")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    st2[0].Lat = lat;
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                }
                if (st2[0].objPosition != null)
                    IndicateDifference(cell, st2[0].objPosition.LatDiffersFromAdm);
            }
            else if (cell.Key == "KLat1")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    st1[0].Lat = lat;
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                }
                if (st1[0].objPosition != null)
                    IndicateDifference(cell, st1[0].objPosition.LatDiffersFromAdm);
            }
            else if (cell.Key == "antHeight1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].AntHeight = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st1[i].objStation);
                }
                cell.Value = (st1[0].AntHeight != IM.NullD) ? st1[0].AntHeight.ToString("F0") : "";
                if (st1[0].AntHeight < 0)
                    ChangeColor(cell, Colors.badValue);
            }
            else if (cell.Key == "antHeight2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].AntHeight = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st2[i].objStation);
                }
                cell.Value = (st2[0].AntHeight != IM.NullD) ? st2[0].AntHeight.ToString("F0") : "";
                if (st2[0].AntHeight < 0)
                    ChangeColor(cell, Colors.badValue);
            }
            else if (cell.Key == "asl1")
            {
                double val = ConvertType.ToDouble(cell, IM.NullD);
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].Height = val;
                    //st1[i].objPosition.Asl = val;
                }
                //st1[0].objPosition.Asl = val;
            }
            else if (cell.Key == "asl2")
            {
                double val = ConvertType.ToDouble(cell, IM.NullD);
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].Height = val;
                    //st2[i].objPosition.Asl = val;
                }
                //st2[0].objPosition.Asl = val;
            }
            else if (cell.Key == "dist1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].Distance = ConvertType.ToDouble(cell, IM.NullD);
                }
                cell.Value = (st1[0].Distance != IM.NullD) ? st1[0].Distance.ToString("F2") : "";
                ChangeColor(cell, ((st1[0].Distance != IM.NullD) && (st1[0].Distance >= 40.0)) ? Colors.warnValue : Colors.okvalue);
            }
            else if (cell.Key == "az1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].Azimuth = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st1[i].objStation);
                }
                cell.Value = (st1[0].Azimuth != IM.NullD) ? st1[0].Azimuth.ToString("F2") : "";
            }
            else if (cell.Key == "az2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].Azimuth = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st2[i].objStation);
                }
                cell.Value = (st2[0].Azimuth != IM.NullD) ? st2[0].Azimuth.ToString("F2") : "";
            }
            else if (cell.Key == "pol1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].Polarization = CPolarization.getShortPolarization(cell.Value);
                    HelpFunction.LoadFromCellToDb(cell, st1[i].objStation);
                }
                cell.Value = st1[0].Polarization;
            }
            else if (cell.Key == "pol2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].Polarization = CPolarization.getShortPolarization(cell.Value);
                    HelpFunction.LoadFromCellToDb(cell, st2[i].objStation);
                }
                cell.Value = st2[0].Polarization;
            }
            else if (cell.Key == "pow2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].Power = ConvertType.ToDouble(cell, IM.NullD);

                    if (st2[i].Power != IM.NullD)
                        st2[i].objStation[cell.DBField] = st2[0].Power + 30;
                    else
                        st2[i].objStation[cell.DBField] = IM.NullD;
                }
                cell.Value = (st2[0].Power != IM.NullD) ? st2[0].Power.ToString() : "";
                if (((st2[0].Power > (st1[0].MaxPower + 0.0001)) || (st2[0].Power < (st1[0].MinPower - 0.0001))) && st2[0].Power != IM.NullD)
                    cell.BackColor = System.Drawing.Color.Red;
                else cell.BackColor = System.Drawing.Color.White;
            }
            else if (cell.Key == "pow1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].Power = ConvertType.ToDouble(cell, IM.NullD);
                    if (st1[i].Power != IM.NullD)
                        st1[i].objStation[cell.DBField] = st1[0].Power + 30;
                    else
                        st1[i].objStation[cell.DBField] = IM.NullD;
                }
                cell.Value = (st1[0].Power != IM.NullD) ? st1[0].Power.ToString() : "";
                if ((st1[0].Power > (st1[0].MaxPower + 0.0001) || st1[0].Power < (st1[0].MinPower - 0.0001)) && st1[0].Power != IM.NullD)
                    cell.BackColor = System.Drawing.Color.Red;
                else cell.BackColor = System.Drawing.Color.White;
            }
            else if (cell.Key == "width1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].BandWidth = ConvertType.ToDouble(cell, IM.NullD);
                }
                cell.Value = (st1[0].BandWidth != IM.NullD) ? st1[0].BandWidth.ToString("F3") : "";
            }
            else if (cell.Key == "antD1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].AntDiam = ConvertType.ToDouble(cell, IM.NullD);
                }
                cell.Value = (st1[0].AntDiam != IM.NullD) ? st1[0].AntDiam.ToString("F1") : "";
            }
            else if (cell.Key == "antD2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].AntDiam = ConvertType.ToDouble(cell, IM.NullD);
                }
                cell.Value = (st2[0].AntDiam != IM.NullD) ? st2[0].AntDiam.ToString("F1") : "";
            }
            else if (cell.Key == "antKoef2")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st2[i].AntCoef = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st2[i].objStation);
                }
                cell.Value = (st2[0].AntCoef != IM.NullD) ? st2[0].AntCoef.ToString("F1") : "";
            }
            else if (cell.Key == "antKoef1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].AntCoef = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st1[i].objStation);
                }
                cell.Value = (st1[0].AntCoef != IM.NullD) ? st1[0].AntCoef.ToString("F1") : "";
            }
            else if (cell.Key == "freqNom2_0" || cell.Key == "freqNom2_1" ||
            cell.Key == "freqNom2_2" || cell.Key == "freqNom2_3" ||
            cell.Key == "freqNom2_4" || cell.Key == "freqNom2_5")
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (index < links.Count)
                {
                    st2[index].NomFreq = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st2[index].objStation);
                    cell.Value = (st2[index].NomFreq != IM.NullD) ? st2[index].NomFreq.ToString() : "";
                    if (st2[index].NomFreq < 0)
                        ChangeColor(cell, Colors.badValue);
                    else if (st2[index].NomFreq != IM.NullD)
                        ChangeColor(cell, IsFreqCorrect(st2[index].NomFreq, st2[index].BandWidth) ? Colors.okvalue : Colors.warnValue);

                }
            }
            else if (cell.Key == "freqNom1_0" || cell.Key == "freqNom1_1" ||
            cell.Key == "freqNom1_2" || cell.Key == "freqNom1_3" ||
            cell.Key == "freqNom1_4" || cell.Key == "freqNom1_5")
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (index < links.Count)
                {
                    st1[index].NomFreq = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, st1[index].objStation);
                    cell.Value = (st1[index].NomFreq != IM.NullD) ? st1[index].NomFreq.ToString() : "";
                    if (st1[index].NomFreq < 0)
                        ChangeColor(cell, Colors.badValue);
                    else if(st1[index].NomFreq != IM.NullD)
                        ChangeColor(cell, IsFreqCorrect(st1[index].NomFreq, st1[index].BandWidth) ? Colors.okvalue : Colors.warnValue);
                }
            }
            else if (cell.Key == "class1")
            {
                for (int i = 0; i < links.Count; ++i)
                {
                    st1[i].EmiClass = cell.Value;
                    HelpFunction.LoadFromCellToDb(cell, objStation);
                }
                cell.Value = st1[0].EmiClass;
            }
            else if (cell.Key == "addr2" || cell.Key == "addr1")//добавил проверку по второму сайту #6885
            {
                if ((st1[0].objPosition != null) && (st2[0].objPosition != null))
                {
                    double Lon1 = st1[0].objPosition.LonDec;// GetD("LONGITUDE");
                    double Lon2 = st2[0].objPosition.LonDec;// GetD("LONGITUDE");
                    double Lat1 = st1[0].objPosition.LatDec;// GetD("LATITUDE");
                    double Lat2 = st2[0].objPosition.LatDec;// GetD("LATITUDE");
                    double height1 = st1[0].objPosition.Asl;
                    double height2 = st2[0].objPosition.Asl;
                    if ((Lon1 != IM.NullD) && (Lon2 != IM.NullD) && (Lat1 != IM.NullD) && (Lat2 != IM.NullD) && (height1 != IM.NullD) && (height2 != IM.NullD))
                    {
                        double azm1 = IM.RoundDeci(IMCalculate.CalcAzimuth(Lon1, Lat1, Lon2, Lat2), 2);
                        double azm2 = IM.RoundDeci(IMCalculate.CalcAzimuth(Lon2, Lat2, Lon1, Lat1), 2);
                        Cell fillCell = grid.GetCellFromKey("az1");
                        fillCell.Value = azm1.ToString("F2");
                        OnCellValidate(fillCell, grid);

                        fillCell = grid.GetCellFromKey("az2");
                        fillCell.Value = azm2.ToString("F2");
                        OnCellValidate(fillCell, grid);
                    }
                } 
                if (cell.Key == "addr2" && st2[0].objPosition != null)
                    IndicateDifference(cell, st2[0].objPosition.AddrDiffersFromAdm);
                if (cell.Key == "addr1" && st1[0].objPosition != null)
                    IndicateDifference(cell, st1[0].objPosition.AddrDiffersFromAdm);
            }
            else if (cell.Key == "az1")
            {
                double tmpVal = ConvertType.ToDouble(cell, IM.NullD);
                if (tmpVal != IM.NullD)
                    for (int i = 0; i < links.Count; ++i)
                    {
                        st1[i].objStation.Put("AZIMUTH", tmpVal);
                    }
            }
            else if (cell.Key == "az2")
            {
                double tmpVal = ConvertType.ToDouble(cell, IM.NullD);
                if (tmpVal != IM.NullD)
                    for (int i = 0; i < links.Count; ++i)
                    {
                        st2[i].objStation.Put("AZIMUTH", tmpVal);
                    }
            }
            else
                base.CellValidate(cell, grid);
        }

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "addr1"))// && (st1[0].objPosition != null))
                {
                    if (st1[0].objPosition != null)
                    {
                        Forms.AdminSiteAllTech2.Show(st1[0].objPosition.TableName, st1[0].objPosition.Id, (IM.TableRight(st1[0].objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        //position may appear be changed
                        st1[0].objPosition.LoadStatePosition(st1[0].objPosition.Id, st1[0].objPosition.TableName);
                        st1[0].objPosition.GetAdminSiteInfo(st1[0].objPosition.AdminSiteId);
                        cell.Value = st1[0].objPosition.FullAddressAuto;
                        
                        st1[0].Lon = st1[0].objPosition.LongDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon1"), gridParam);
                        st1[0].Lat = st1[0].objPosition.LatDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLat1"), gridParam);
                        Cell aCell = gridParam.GetCellFromKey("asl1");
                        aCell.Value = st1[0].objPosition.Asl.ToString();
                        OnCellValidate(aCell, gridParam);

                        OnCellValidate(cell, gridParam); // азимуты!
                    }
                    else
                        ShowMessageNoReference();

                }
                else if ((cell.Key == "antType1") && (st1[0].objAntenna != null))
                {
                    RecordPtr rcPtr = new RecordPtr(st1[0].objAntenna.GetS("TABLE_NAME"), st1[0].objAntenna.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "rez") && (st1[0].objEquip != null))
                {
                    RecordPtr rcPtr = new RecordPtr(st1[0].objEquip.GetS("TABLE_NAME"), st1[0].objEquip.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "addr2") )//&& (st2[0].objPosition != null))
                {
                    if (st2[0].objPosition != null)
                    {
                        Forms.AdminSiteAllTech2.Show(st2[0].objPosition.TableName, st2[0].objPosition.Id, (IM.TableRight(st2[0].objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        //position may appear be changed
                        st2[0].objPosition.LoadStatePosition(st2[0].objPosition.Id, st2[0].objPosition.TableName);
                        st2[0].objPosition.GetAdminSiteInfo(st2[0].objPosition.AdminSiteId);
                        cell.Value = st2[0].objPosition.FullAddressAuto;
                        
                        st2[0].Lon = st2[0].objPosition.LongDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon2"), gridParam);
                        st2[0].Lat = st2[0].objPosition.LatDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLat2"), gridParam);
                        Cell aCell = gridParam.GetCellFromKey("asl2");
                        aCell.Value = st2[0].objPosition.Asl.ToString();
                        OnCellValidate(aCell, gridParam);

                        OnCellValidate(cell, gridParam); // азимуты!
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "antType2") && (st2[0].objAntenna != null))
                {
                    RecordPtr rcPtr = new RecordPtr(st2[0].objAntenna.GetS("TABLE_NAME"), st2[0].objAntenna.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "obj1") && (objStation != null))
                {
                    ICSM.RecordPtr ptr = new RecordPtr(ICSMTbl.itblMicrowa, objStation.GetI("ID"));
                    ptr.UserEdit();
                }
                else if (cell.Key == "kFreqNom0" || cell.Key == "kFreqNom1" ||
                cell.Key == "kFreqNom2" || cell.Key == "kFreqNom3" ||
                cell.Key == "kFreqNom4" || cell.Key == "kFreqNom5")
                {
                    int index = Convert.ToInt32(cell.Key.Substring(8));
                    if (index < links.Count)
                    {
                        ICSM.RecordPtr ptr = new RecordPtr(ICSMTbl.itblMicrowa, links[index].GetI("ID"));
                        ptr.UserEdit();
                    }
                }

            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

        }

        protected void LookupAntenna1(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            // Выбераем запись из таблицы
            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach for antenna"), ICSMTbl.itblMicrowAntenna, param);
            if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
            {
                st1[0].objAntenna = IMObject.LoadFromDB(RecAnt);
                cell.Value = st1[0].objAntenna.GetS("NAME");
                //OnCellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }

        protected void LookupAntenna2(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            // Выбераем запись из таблицы
            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach for antenna"), ICSMTbl.itblMicrowAntenna, param);
            if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
            {
                st2[0].objAntenna = IMObject.LoadFromDB(RecAnt);
                cell.Value = st2[0].objAntenna.GetS("NAME");
                //OnCellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }

        protected void LookupEquipment(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipMicrow, "NAME", initialValue, true); 
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                st1[0].objEquip = IMObject.LoadFromDB(recEquip);
                st2[0].objEquip = IMObject.LoadFromDB(recEquip);
                cell.Value = st1[0].objEquip.GetS("NAME");
                OnCellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }

        public override double GetBw()
        {
            return st1.Count > 0 ? st1[0].BandWidth : 0.0;
        }

        internal override CLicence.Band[] GetRxTxBands(RxTx type, string region) 
        { 
            List<CLicence.Band> bandList = new List<CLicence.Band>();
            List<StationRS> stList = null;
            string staRegions = GetProvince();
            int delimPos = staRegions.IndexOf(',');
            string reg1 = delimPos > -1 ? staRegions.Substring(0,delimPos) : staRegions;
            string reg2 = delimPos > -1 ? staRegions.Substring(delimPos + 1, staRegions.Length - delimPos - 1) : staRegions;
            
            if (region == reg1)
                stList = type != RxTx.Rx ? st1 : st2;
            else if (region == reg2)
                stList = type != RxTx.Rx ? st2 : st1;
            else
                return bandList.ToArray();

            double BW = GetBw();

            int bandId = 0;
            foreach (StationRS sta in stList)
            {
                double freq = sta.NomFreq;
                bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(freq - BW / 2, 6), IM.RoundDeci(freq, 6), IM.RoundDeci(freq + BW / 2, 6), CLicence.Band.Parity.No));
            }
            
            return bandList.ToArray();
        }

        public override string GetProvince()
        {
            string reg1 = "";
            string reg2 = "";
            try
            {

                if (st1 != null)
                {
                    if (st1[0].objPosition != null)
                    {
                        reg1 = st1.Count > 0 ? (st1[0].objPosition.Province != "" ? st1[0].objPosition.Province : st1[0].objPosition.City) : "";
                    }
                }
                if (st2 != null)
                {
                    if (st2[0].objPosition != null)
                    {
                        reg2 = st2.Count > 0 ? (st2[0].objPosition.Province != "" ? st2[0].objPosition.Province : st2[0].objPosition.City) : "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return reg1 + (reg1.Length > 0 && reg2.Length > 0 ? "," : "") + reg2;
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "rez")
            {// Выбор оборудования
                LookupEquipment(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "addr1")
            {
                if (ShowMessageReference(st1[0].objPosition != null))
                {
                    
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms = st1[0].Lon;
                    newPos.LatDms = st1[0].Lat;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionMw, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        /////
                        st1[0].objPosition = newPos;
                        st1[0].objPosition.GetAdminSiteInfo(st1[0].objPosition.AdminSiteId);
                        cell.Value = st1[0].objPosition.FullAddressAuto;
                        CellValidate(cell, gridParam);

                        Cell longCell = grid.GetCellFromKey("KLon1");

                        double lonDMS = st1[0].objPosition.LongDms;
                        st1[0].Lon = lonDMS;
                        if (lonDMS != IM.NullD)
                        {
                            longCell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                        }
                        else
                        {
                            longCell.Value = "";
                            st1[0].Lon = 0;
                        }
                        OnCellValidate(longCell, grid);
                        AutoFill(longCell, grid);
                        if (st1[0].objPosition.Name == "")
                            st1[0].objPosition.Name = "адрес не сохранен";

                    }
                }                
            }
            else if (cell.Key == "addr2")
            {
                if (ShowMessageReference(st2[0].objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms = st2[0].Lon;
                    newPos.LatDms = st2[0].Lat;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionMw, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        //////
                        st2[0].objPosition = newPos;
                        cell.Value = st2[0].objPosition.FullAddressAuto;
                        st2[0].objPosition.GetAdminSiteInfo(st2[0].objPosition.AdminSiteId);
                        CellValidate(cell, gridParam);
                        Cell longCell = grid.GetCellFromKey("KLon2");
                        double lonDMS = st2[0].objPosition.LongDms;
                        st2[0].Lon = lonDMS;
                        if (lonDMS != IM.NullD)
                        {
                            longCell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                        }
                        else
                        {
                            longCell.Value = "";
                            st2[0].Lon = 0;
                        }

                        OnCellValidate(longCell, grid);
                        AutoFill(longCell, grid);
                        if (st2[0].objPosition.Name == "")
                            st2[0].objPosition.Name = "адрес не сохранен";

                    }
                }
            }
            else if (cell.Key == "asl1")
            {
                double lon = st1[0].Lon;
                double lat = st1[0].Lat;

                if (lon != IM.NullD && lat != IM.NullD)
                {
                    double asl = IMCalculate.CalcALS(lon, lat, "4DEC");
                    if (asl != IM.NullD)
                    {
                        cell.Value = IM.RoundDeci(asl, 1).ToString();
                        OnCellValidate(cell, grid);
                    }
                }
            }
            else if (cell.Key == "asl2")
            {
                double lon = st2[0].Lon;
                double lat = st2[0].Lat;

                if (lon != IM.NullD && lat != IM.NullD)
                {
                    double asl = IMCalculate.CalcALS(lon, lat, "4DEC");
                    if (asl != IM.NullD)
                    {
                        cell.Value = IM.RoundDeci(asl, 1).ToString();
                        OnCellValidate(cell, grid);
                    }
                }
            }
            else if ((cell.Key == "antType1"))
            {// Выбор антены
                LookupAntenna1(cell, grid, cell.Value, true);
            }
            else if ((cell.Key == "antType2"))
            {// Выбор антены
                LookupAntenna2(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "KLon2")
            {
                // Выбераем запись из таблицы
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionMw, 1, st2[0].Lon, st2[0].Lat);
                if (newPos != null)
                {
                    st2[0].objPosition = newPos;
                    st2[0].objPosition.GetAdminSiteInfo(st2[0].objPosition.AdminSiteId);
                    double lonDMS = newPos.LongDms;
                    st2[0].Lon = lonDMS;
                    if (lonDMS != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                    }
                    else
                    {
                        cell.Value = "";
                        st2[0].Lon = 0;
                    }
                    OnCellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "KLon1")
            {
                // Выбераем запись из таблицы
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionMw, 1, st1[0].Lon, st1[0].Lat);
                if (newPos != null)
                {
                    st1[0].objPosition = newPos;
                    st1[0].objPosition.GetAdminSiteInfo(st1[0].objPosition.AdminSiteId);
                    double lonDMS = st1[0].objPosition.LongDms;
                    st1[0].Lon = lonDMS;
                    if (lonDMS != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                    }
                    else
                    {
                        cell.Value = "";
                        st1[0].Lon = 0;
                    }
                    OnCellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if ((cell.Key == "obj1"))
            {
                ICSM.RecordPtr ptr = new RecordPtr(ICSMTbl.itblMicrowa, objStation.GetI("ID"));
                ptr.UserEdit();
            }
            else if (cell.Key == "freqNom2_0" || cell.Key == "freqNom2_1" ||
            cell.Key == "freqNom2_2" || cell.Key == "freqNom2_4" ||
            cell.Key == "freqNom2_3" || cell.Key == "freqNom2_5")
            {
                CellValidate(cell, grid);
                RefreshLicenceList();                
            }
            else
                base.OnPressButton(cell, grid);
        }

        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon2")
            {
                //Заполняем дополнитеотные поля
                if (st2[0].objPosition != null)
                {// Заполяем поля значениями
                    Cell fillCell = grid.GetCellFromKey("KLat2");

                    double Latitude = st2[0].objPosition.LatDms;
                    if (Latitude != IM.NullD)
                    {
                        fillCell.Value = HelpFunction.DmsToString(Latitude, EnumCoordLine.Lat);
                    }
                    else
                    {
                        fillCell.Value = "";
                        Latitude = 0;
                    }
                    st2[0].Lat = Latitude;

                    OnCellValidate(fillCell, grid);
                    fillCell = grid.GetCellFromKey("addr2");
                    fillCell.Value = st2[0].objPosition.FullAddressAuto;
                    OnCellValidate(fillCell, grid);
                    fillCell = grid.GetCellFromKey("asl2");
                    st2[0].Height = st2[0].objPosition.Asl;
                    fillCell.Value = (st2[0].Height != IM.NullD) ? st2[0].Height.ToString("F0") : "";

                    OnCellValidate(fillCell, grid);
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("KLat2");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("addr2");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("asl2");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);
                }
            }
            else if (cell.Key == "KLon1")
            {
                //Заполняем дополнитеотные поля
                if (st1[0].objPosition != null)
                {// Заполяем поля значениями
                    Cell fillCell = grid.GetCellFromKey("KLat1");

                    double Latitude = st1[0].objPosition.LatDms;
                    if (Latitude != IM.NullD)
                    {
                        fillCell.Value = HelpFunction.DmsToString(Latitude, EnumCoordLine.Lat);
                    }
                    else
                    {
                        fillCell.Value = "";
                        Latitude = 0;
                    }
                    st1[0].Lat = Latitude;

                    OnCellValidate(fillCell, grid);
                    
                    fillCell = grid.GetCellFromKey("addr1");
                    fillCell.Value = (st1[0] != null) && (st1[0].objPosition != null) ?
                        st1[0].objPosition.FullAddressAuto : "";
                    OnCellValidate(fillCell, grid);
                    
                    fillCell = grid.GetCellFromKey("asl1");
                    st1[0].Height = st1[0].objPosition.Asl;
                    fillCell.Value = (st1[0].Height != IM.NullD) ? st1[0].Height.ToString("F0") : "";

                    OnCellValidate(fillCell, grid);
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("KLat1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("addr1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("asl1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);
                }
            }
            else if (cell.Key == "rez")
            {
                double MaxPower = -1;
                //Заполняем дополнитеотные поля
                if (st1[0].objEquip != null)
                {// Заполяем поля значениями
                    double tmpVal = st1[0].objEquip.GetD("MAX_POWER");
                    if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                    {
                        MaxPower = st1[0].objEquip.GetD("MAX_POWER");
                        st1[0].Power = (MaxPower != IM.NullD) ? MaxPower - 30 : IM.NullD;
                    }
                    if (st2[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                    {
                        MaxPower = st2[0].objEquip.GetD("MAX_POWER");
                        st2[0].Power = (MaxPower != IM.NullD) ? MaxPower - 30 : IM.NullD;
                    }

                    if (st1[0].objEquip.GetD("MIN_POWER") != IM.NullD)
                        st1[0].MinPower = st1[0].objEquip.GetD("MIN_POWER") - 30;
                    if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                        st1[0].MaxPower = st1[0].objEquip.GetD("MAX_POWER") - 30;



                    Cell fillCell = null;

                    if (tmpVal > -1)
                    {
                        fillCell = grid.GetCellFromKey("pow1");
                        fillCell.Value = (st1[0].Power != IM.NullD) ? st1[0].Power.ToString("F1") : "";
                        st1[0].Power = tmpVal;
                        OnCellValidate(fillCell, grid);

                        fillCell = grid.GetCellFromKey("pow2");
                        fillCell.Value = (st1[0].Power != IM.NullD) ? st1[0].Power.ToString("F1") : "";
                        st2[0].Power = tmpVal;
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("width1");
                    st1[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;
                    st2[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;
                    fillCell.Value = (st1[0].BandWidth != IM.NullD) ? st1[0].BandWidth.ToString("F3") : "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("class1");
                    fillCell.Value = st1[0].objEquip.GetS("DESIG_EMISSION");
                    st1[0].EmiClass = st1[0].objEquip.GetS("DESIG_EMISSION");
                    st2[0].EmiClass = st2[0].objEquip.GetS("DESIG_EMISSION");
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("mod1");
                    st1[0].Modulation = Convert.ToInt32(st1[0].objEquip.GetS("MODULATION"));
                    if (st1[0].Modulation != -1)
                        fillCell.Value = CModulation.getModulationString(st1[0].Modulation);//Modulation[st1.Modulation];
                    else
                        fillCell.Value = "";

                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("sertNom");
                    fillCell.Value = st1[0].objEquip.GetS("CUST_TXT1");
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("sertDate");
                    DateTime dt = st1[0].objEquip.GetT("CUST_DAT1");
                    if (dt.Year > 1900)
                    {
                        fillCell.Value = dt.ToString("d");
                        OnCellValidate(fillCell, grid);
                    }
                    else
                    {
                        fillCell.Value = "";
                        OnCellValidate(fillCell, grid);
                    }
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("pow1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("pow2");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("class1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("mod1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("width1");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("sertNom");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("sertDate");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);
                }
            }
            else if (cell.Key == "antType1")
            {
                //Заполняем дополнитеотные поля
                if (st1[0].objAntenna != null)
                {// Заполяем поля значениями
                    Cell fillCell = grid.GetCellFromKey("antD1");
                    if (fillCell != null)
                    {
                        st1[0].AntDiam = st1[0].objAntenna.GetD("DIAMETER");
                        fillCell.Value = (st1[0].AntDiam != IM.NullD) ? st1[0].AntDiam.ToString("F1") : "";
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("antKoef1");
                    if (fillCell != null)
                    {
                        st1[0].AntCoef = st1[0].objAntenna.GetD("GAIN");
                        fillCell.Value = (st1[0].AntCoef != IM.NullD) ? st1[0].AntCoef.ToString("F1") : "";
                        OnCellValidate(fillCell, grid);
                    }
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("antD1");
                    if (fillCell != null)
                    {
                        fillCell.Value = "";
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("antKoef1");
                    if (fillCell != null)
                    {
                        fillCell.Value = "";
                        OnCellValidate(fillCell, grid);
                    }
                }
            }
            else if (cell.Key == "antType2")
            {
                //Заполняем дополнитеотные поля
                if (st2[0].objAntenna != null)
                {// Заполяем поля значениями
                    Cell fillCell = grid.GetCellFromKey("antD2");
                    if (fillCell != null)
                    {
                        st2[0].AntDiam = st2[0].objAntenna.GetD("DIAMETER");
                        fillCell.Value = (st2[0].AntDiam != IM.NullD) ? st2[0].AntDiam.ToString("F1") : "";
                        OnCellValidate(fillCell, grid);
                    }

                    if (fillCell != null)
                    {
                        fillCell = grid.GetCellFromKey("antKoef2");
                        st2[0].AntCoef = st2[0].objAntenna.GetD("GAIN");
                        SafeFillCellByKey(grid, "antKoef2", (st2[0].AntCoef != IM.NullD) ? st2[0].AntCoef.ToString("F1") : "", true);
                        //fillCell.Value = (st2[0].AntCoef != IM.NullD) ? st2[0].AntCoef.ToString("F1") : "";
                        //OnCellValidate(fillCell, grid);
                    }
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("antD2");
                    if (fillCell != null)
                    {
                        fillCell.Value = "";
                        OnCellValidate(fillCell, grid);
                    }

                    if (fillCell != null)
                    {
                        //fillCell = grid.GetCellFromKey("antKoef2");
                        //fillCell.Value = "";
                        //OnCellValidate(fillCell, grid);
                        SafeFillCellByKey(grid, "antKoef2", "", true);
                    }
                }
            }
            else
                base.AutoFill(cell, grid);
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "class1")
            {
                HelpFunction.LoadFromDbToCell(cell, objStation);
                st1[0].EmiClass = cell.Value;
            }
            else if (cell.Key == "KLon2")
            {
                if (st2[0].objPosition == null)
                {
                    int mwID = objStation.GetI("ID");
                    int posId = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("SITE_ID,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                    r.Open();
                    if (!r.IsEOF())
                        posId = r.GetI("SITE_ID");
                    r.Close();
                    r.Destroy();

                    if ((posId > 0) && (posId != IM.NullI))
                    {
                        st2[0].objPosition = new PositionState2();
                        st2[0].objPosition.LoadStatePosition(posId, ICSMTbl.itblPositionMw);
                        st2[0].objPosition.GetAdminSiteInfo(st2[0].objPosition.AdminSiteId);
                        double lonDMS = st2[0].objPosition.LongDms;
                        st2[0].Lon = lonDMS;
                        if (lonDMS != IM.NullD)
                        {
                            cell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                        }
                        else
                        {
                            cell.Value = "";
                            st2[0].Lon = 0;
                        }
                        CellValidate(cell, grid);
                        AutoFill(cell, grid);
                    }
                }
                else
                {
                    double lonDMS = st2[0].objPosition.LongDms;
                    st2[0].Lon = lonDMS;
                    if (lonDMS != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                    }
                    else
                    {
                        cell.Value = "";
                        st2[0].Lon = 0;
                    }
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "KLon1")
            {
                if (st1[0].objPosition == null)
                {
                    // попытаться загрузить
                    int mwID = objStation.GetI("ID");
                    int posId = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("SITE_ID,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                    r.Open();
                    if (!r.IsEOF())
                        posId = r.GetI("SITE_ID");
                    r.Close();
                    r.Destroy();

                    if ((posId > 0) && (posId != IM.NullI))
                    {
                        st1[0].objPosition = new PositionState2();
                        st1[0].objPosition.LoadStatePosition(posId, ICSMTbl.itblPositionMw);
                        st1[0].objPosition.GetAdminSiteInfo(st1[0].objPosition.AdminSiteId);
                    }
                }

                if (st1[0].objPosition != null)
                {
                    double lonDMS = st1[0].objPosition.LongDms;
                    st1[0].Lon = lonDMS;
                    if (lonDMS != IM.NullD)
                    {
                        cell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                    }
                    else
                    {
                        cell.Value = "";
                        st1[0].Lon = 0;
                    }
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "rez")
            {
                if (st1[0].objEquip == null)
                {
                    int equipId = objStation.GetI("EQPMW_ID");
                    if ((equipId > 0) && (equipId != IM.NullI))
                    {
                        st1[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipId);
                        st2[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMicrow, equipId);
                    }
                    else
                        return;
                }
                cell.Value = st1[0].objEquip.GetS("NAME");
                double MaxPower = -1;
                //Заполняем дополнитеотные поля
                if (st1[0].objEquip != null)
                {// Заполяем поля значениями
                    double tmpVal = st1[0].objEquip.GetD("MAX_POWER");
                    if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                    {
                        MaxPower = st1[0].objEquip.GetD("MAX_POWER") - 30;
                    }

                    if (st1[0].objEquip.GetD("MIN_POWER") != IM.NullD)
                        st1[0].MinPower = st1[0].objEquip.GetD("MIN_POWER") - 30;
                    if (st1[0].objEquip.GetD("MAX_POWER") != IM.NullD)
                        st1[0].MaxPower = st1[0].objEquip.GetD("MAX_POWER") - 30;

                    Cell fillCell = null;

                    fillCell = grid.GetCellFromKey("width1");
                    st1[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;
                    st2[0].BandWidth = st1[0].objEquip.GetD("BW") / 1000.0;
                    fillCell.Value = (st1[0].BandWidth != IM.NullD) ? st1[0].BandWidth.ToString("F3") : "";
                    OnCellValidate(fillCell, grid);

                    st1[0].Modulation = Convert.ToInt32(st1[0].objEquip.GetS("MODULATION"));
                    //st2.Modulation = st1.objEquip.GetS("MODULATION");
                    if (st1[0].Modulation != -1)
                        SafeFillCellByKey(grid, "mod1", CModulation.getModulationString(st1[0].Modulation), true);
                    else
                        SafeFillCellByKey(grid, "mod1", "", true);

                    fillCell = grid.GetCellFromKey("sertNom");
                    fillCell.Value = st1[0].objEquip.GetS("CUST_TXT1");
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("sertDate");
                    DateTime dt = st1[0].objEquip.GetT("CUST_DAT1");
                    if (dt.Year > 1900)
                    {
                        fillCell.Value = dt.ToString("d");
                        OnCellValidate(fillCell, grid);
                    }
                }
                else
                {// Обнуляем все поля
                    SafeFillCellByKey(grid, "mod1", "", true);

                    SafeFillCellByKey(grid, "width1", "", true);

                    SafeFillCellByKey(grid, "sertNom", "", true);

                    SafeFillCellByKey(grid, "sertDate", "", true);
                }
            }
            else if (cell.Key == "antType1")
            {
                if (st1[0].objAntenna == null)
                {  // Ищем антену
                    int mwID = objStation.GetI("ID");
                    int antennaID = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("ANT_ID,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                    r.Open();
                    if (!r.IsEOF())
                    {
                        antennaID = r.GetI("ANT_ID");
                        if ((antennaID > 0) && (antennaID != IM.NullI))
                        {
                            st1[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblMicrowAntenna, antennaID);
                            cell.Value = st1[0].objAntenna.GetS("NAME");
                            //Заполняем дополнитеотные поля
                            if (st1[0].objAntenna != null)
                            {// Заполяем поля значениями
                                Cell fillCell = grid.GetCellFromKey("antD1");
                                st1[0].AntDiam = st1[0].objAntenna.GetD("DIAMETER");
                                fillCell.Value = (st1[0].AntDiam != IM.NullD) ? st1[0].AntDiam.ToString("F1") : "";

                                OnCellValidate(fillCell, grid);
                            }
                            else
                            {// Обнуляем все поля
                                Cell fillCell = grid.GetCellFromKey("antD1");
                                fillCell.Value = "";
                                OnCellValidate(fillCell, grid);
                            }
                        }
                    }
                    r.Close();
                    r.Destroy();
                }
                if (st1[0].objAntenna != null)
                {
                    cell.Value = st1[0].objAntenna.GetS("NAME");
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "antType2")
            {
                if (st2[0].objAntenna == null)
                {  // Ищем антену
                    int mwID = objStation.GetI("ID");
                    int antennaID = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("ANT_ID,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                    r.Open();
                    if (!r.IsEOF())
                    {
                        antennaID = r.GetI("ANT_ID");
                        if ((antennaID > 0) && (antennaID != IM.NullI))
                        {
                            st2[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblMicrowAntenna, antennaID);
                            cell.Value = st2[0].objAntenna.GetS("NAME");
                            //Заполняем дополнитеотные поля
                            if (st2[0].objAntenna != null)
                            {// Заполяем поля значениями
                                Cell fillCell = grid.GetCellFromKey("antD2");
                                st2[0].AntDiam = st2[0].objAntenna.GetD("DIAMETER");
                                fillCell.Value = (st2[0].AntDiam != IM.NullD) ? st2[0].AntDiam.ToString("F1") : "";

                                OnCellValidate(fillCell, grid);
                            }
                            else
                            {// Обнуляем все поля
                                Cell fillCell = grid.GetCellFromKey("antD2");
                                fillCell.Value = "";
                                OnCellValidate(fillCell, grid);
                            }
                        }
                    }
                    r.Close();
                    r.Destroy();
                }
                if (st2[0].objAntenna != null)
                {
                    cell.Value = st2[0].objAntenna.GetS("NAME");
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "pol2")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("POLAR,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                r.Open();
                if (!r.IsEOF())
                {
                    st2[0].Polarization = r.GetS("POLAR");
                    cell.Value = st2[0].Polarization;
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "pol1")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("POLAR,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                r.Open();
                if (!r.IsEOF())
                {
                    st1[0].Polarization = r.GetS("POLAR");
                    cell.Value = st1[0].Polarization;
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "freqNom2_0" || cell.Key == "freqNom2_1" ||
               cell.Key == "freqNom2_2" || cell.Key == "freqNom2_3" ||
               cell.Key == "freqNom2_4" || cell.Key == "freqNom2_5")
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (index < links.Count)
                {
                    int mwID = links[index].GetI("ID");
                    //int antennaID = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("TX_FREQ,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                    r.Open();
                    if (!r.IsEOF())
                    {
                        st2[index].NomFreq = r.GetD("TX_FREQ");
                        if (st2[index].NomFreq != IM.NullD)
                            ChangeColor(cell, IsFreqCorrect(st2[index].NomFreq, st2[index].BandWidth) ? Colors.okvalue : Colors.warnValue);
                        cell.Value = (st2[index].NomFreq != IM.NullD) ? st2[index].NomFreq.ToString("F3") : "";
                    }
                    r.Close();
                    r.Destroy();
                }
            }
            else if (cell.Key == "freqNom1_0" || cell.Key == "freqNom1_1" ||
               cell.Key == "freqNom1_2" || cell.Key == "freqNom1_3" ||
               cell.Key == "freqNom1_4" || cell.Key == "freqNom1_5")
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (index < links.Count)
                {
                    int mwID = links[index].GetI("ID");
                    //int antennaID = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                    r.Select("TX_FREQ,ID,MW_ID,ROLE");
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                    r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                    r.Open();
                    if (!r.IsEOF())
                    {
                        st1[index].NomFreq = r.GetD("TX_FREQ");
                        if(st1[index].NomFreq != IM.NullD)
                            ChangeColor(cell, IsFreqCorrect(st1[index].NomFreq, st1[index].BandWidth) ? Colors.okvalue : Colors.warnValue);
                        cell.Value = (st1[index].NomFreq != IM.NullD) ? st1[index].NomFreq.ToString("F3") : "";
                    }
                    r.Close();
                    r.Destroy();
                }
            }
            else if (cell.Key == "antHeight2")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("AGL1,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                r.Open();
                if (!r.IsEOF())
                {
                    st2[0].AntHeight = r.GetD("AGL1");
                    cell.Value = (st2[0].AntHeight != IM.NullD) ? st2[0].AntHeight.ToString("F0") : "";
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "antHeight1")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("AGL1,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                r.Open();
                if (!r.IsEOF())
                {
                    st1[0].AntHeight = r.GetD("AGL1");
                    cell.Value = (st1[0].AntHeight != IM.NullD) ? st1[0].AntHeight.ToString("F0") : "";
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "az1")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("AZIMUTH,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                r.Open();
                if (!r.IsEOF())
                {
                    st1[0].Azimuth = r.GetD("AZIMUTH");
                    cell.Value = (st1[0].Azimuth != IM.NullD) ? st1[0].Azimuth.ToString("F2") : "";
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "az2")
            {
                int mwID = links[0].GetI("ID");
                //int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("AZIMUTH,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, mwID);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                r.Open();
                if (!r.IsEOF())
                {
                    st2[0].Azimuth = r.GetD("AZIMUTH");
                    cell.Value = (st2[0].Azimuth != IM.NullD) ? st2[0].Azimuth.ToString("F2") : "";
                }
                r.Close();
                r.Destroy();
            }
            else if (cell.Key == "pow1")
            {
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("POWER,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[0].GetI("ID"));
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                r.Open();
                if (!r.IsEOF())
                {
                    st1[0].Power = (r.GetD("POWER") != IM.NullD) ? r.GetD("POWER") - 30 : IM.NullD;

                }
                r.Close();
                r.Destroy();


                cell.Value = (st1[0].Power != IM.NullD) ? st1[0].Power.ToString("F1") : "";
                CellValidate(cell, grid);
            }
            else if (cell.Key == "pow2")
            {
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("POWER,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[0].GetI("ID"));
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                r.Open();
                if (!r.IsEOF())
                {
                    st2[0].Power = (r.GetD("POWER") != IM.NullD) ? r.GetD("POWER") - 30 : IM.NullD;
                }
                r.Close();
                r.Destroy();
                cell.Value = (st2[0].Power != IM.NullD) ? st2[0].Power.ToString("F1") : "";
                CellValidate(cell, grid);
            }
            else if (cell.Key == "antKoef2")
            {
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("GAIN,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[0].GetI("ID"));
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "B");
                r.Open();
                if (!r.IsEOF())
                {
                    st2[0].AntCoef = (r.GetD("GAIN") != IM.NullD) ? r.GetD("GAIN") : IM.NullD;
                }
                r.Close();
                r.Destroy();
                cell.Value = (st2[0].AntCoef != IM.NullD) ? st2[0].AntCoef.ToString("F1") : "";
            }
            else if (cell.Key == "antKoef1")
            {
                IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
                r.Select("GAIN,ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, links[0].GetI("ID"));
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
                r.Open();
                if (!r.IsEOF())
                {
                    st1[0].AntCoef = (r.GetD("GAIN") != IM.NullD) ? r.GetD("GAIN") : IM.NullD;
                }
                r.Close();
                r.Destroy();
                cell.Value = (st1[0].AntCoef != IM.NullD) ? st1[0].AntCoef.ToString("F1") : "";
            }
            else if (cell.Key == "dist1")
            {
                if (st1[0].Lat != IM.NullD && st1[0].Lon != IM.NullD && st2[0].Lat != IM.NullD && st2[0].Lon != IM.NullD)
                {
                    IMPosition a = new IMPosition(st1[0].Lon, st1[0].Lat, "4DMS");
                    IMPosition b = new IMPosition(st2[0].Lon, st2[0].Lat, "4DMS");
                    st1[0].Distance = IMPosition.Distance(a, b); //LisUtility.StationUtility.AntennaLength(, st2.Lon, st2.Lat);
                    if (st1[0].Distance < 0.0001)
                        cell.Value = "0";
                    else
                        cell.Value = st1[0].Distance.ToString("F3");
                    ChangeColor(cell, ((st1[0].Distance != IM.NullD) && (st1[0].Distance >= 40.0)) ? Colors.warnValue : Colors.okvalue);
                }
                cell.HorizontalAlignment = HorizontalAlignment.Center;
            }
            else if ((cell.Key == "obj1"))
            {
                int mwID = links[0].GetI("ID");
                cell.Value = mwID.ToString();
                cell.HorizontalAlignment = HorizontalAlignment.Center;
            }
            else
                base.UpdateOneParamGrid(cell, grid);
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            foreach (IMObject obj in links)
            {
                IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
                r.Select("ID,DEM_RECEIPT_NUM,DEM_RECEIPT_DATE,CONFIRM_NUM,CONFIRM_DATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, obj.GetI("ID"));
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r["DEM_RECEIPT_NUM"] = NumberIn;
                        r["DEM_RECEIPT_DATE"] = DateIn;
                        r["CONFIRM_NUM"] = NumberOut;
                        r["CONFIRM_DATE"] = DateOut;
                        r.Update();
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Количество частот
        /// </summary>
        public int GetFreqsCount()
        {
            int cnt = 0;
            foreach (StationRS st in st1)
                if (st.NomFreq != IM.NullD)
                    ++cnt;
            foreach (StationRS st in st2)
                if (st.NomFreq != IM.NullD)
                    ++cnt;
            if (cnt == 0)
                cnt = 1;
            return cnt;
        }

        public override int GetTxCount()
        {
            return GetFreqsCount();
        }

        //===========================================================
        /// <summary>
        /// Возвращет количество работ по заявке в ДРВ от УРЧМ
        /// </summary>
        public override int GetURCMWorksCount()
        {
            return 2;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, List<string> provs, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,StationA.Position.REMARK,StationB.Position.REMARK,StationB.Position.PROVINCE,StationA.Position.PROVINCE");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                     adress = rs.GetS("StationA.Position.REMARK") + " - " + rs.GetS("StationB.Position.REMARK");
                     if (provs != null && provs.Count > 0)
                     {
                         if (provs.Contains(rs.GetS("StationA.Position.PROVINCE")))
                             ++count;
                         if (provs.Contains(rs.GetS("StationB.Position.PROVINCE")))
                             ++count;
                     }
                     else
                         count = 2;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }

        /// <summary>
        /// Проверка попадания частоты в частотный план
        /// </summary>
        /// <param name="freq">Частота</param>
        /// <param name="bw">Ширина полосы MHz</param>
        /// <returns>TRUE - частота попадает в частотный план, иначе FALSE</returns>
        private bool IsFreqCorrect(double freq, double bw)
        {
            if ((freq == IM.NullD) || (bw == IM.NullD))
                return false;
            bw *= 1000.0;
            foreach (FreqPlan fPlan in ArrayFreqPlan)
            {
                double deltaFreq = Math.Abs(fPlan.Freq - freq);
                double deltaBw = Math.Abs(fPlan.Bw - bw);
                if ((deltaFreq <= 0.000001) && (deltaBw <= 0.000001))
                    return true;
            }
            return false;
        }
    }
}
