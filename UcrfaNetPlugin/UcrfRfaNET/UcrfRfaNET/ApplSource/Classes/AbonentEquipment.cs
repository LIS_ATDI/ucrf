﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public class AbonentEquipment : Equipment
    {
        /// <summary>
        /// Флажок редагування
        /// </summary>
        public bool IsEdit { get; set; }

        /// <summary>
        /// Мінімальна частота передавача
        /// </summary>
        public double MinFreqTx { get; set; }
        /// <summary>
        /// Максимальна частота передавача
        /// </summary>
        public double MaxFreqTx { get; set; }
        /// <summary>
        /// Мінімальна частота приймача
        /// </summary>
        public double MinFreqRx { get; set; }
        /// <summary>
        /// Максимальна частота приймача
        /// </summary>
        public double MaxFreqRx { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string CodeEquip { get; set; }

        /// <summary>
        /// Сімейство
        /// </summary>
        public string FamilyEquip { get; set; }

        /// <summary>
        /// Виробник
        /// </summary>
        public string ManufactEquip { get; set; }

        /// <summary>
        /// Технічний номер
        /// </summary>
        public string TechNoEquip { get; set; }

        /// <summary>
        /// Загрузить оборудование
        /// </summary>
        public override void Load()
        {
            _tableName = PlugTbl.XfaEquip;
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
            r.Select("ID,MAX_POWER,NAME,DESIG_EMISSION,CERTIFICATE_NUM,CERTIFICATE_DATE,BW");
            r.Select("RX_LOWER_FREQ,RX_UPPER_FREQ,LOWER_FREQ,UPPER_FREQ,CODE,FAMILY,MANUFACTURER");
            r.Select("TECH_IDNO");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    _maxPower[PowerUnits.dBm] = r.GetD("MAX_POWER");
                    _equipmentName = r.GetS("NAME");
                    _desigEmission = r.GetS("DESIG_EMISSION");
                    BandWidth = r.GetD("BW");
                    Certificate.Date = r.GetT("CERTIFICATE_DATE");
                    Certificate.Symbol = r.GetS("CERTIFICATE_NUM");
                    MinFreqRx = r.GetD("RX_LOWER_FREQ");
                    MaxFreqRx = r.GetD("RX_UPPER_FREQ");
                    MinFreqTx = r.GetD("LOWER_FREQ");
                    MaxFreqTx = r.GetD("UPPER_FREQ");
                    CodeEquip = r.GetS("CODE");
                    FamilyEquip = r.GetS("FAMILY");
                    ManufactEquip = r.GetS("MANUFACTURER");
                    TechNoEquip = r.GetS("TECH_IDNO");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Сохранить оборудование
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,MAX_POWER,NAME,DESIG_EMISSION,CERTIFICATE_NUM,CERTIFICATE_DATE,BW");
            r.Select("RX_LOWER_FREQ,RX_UPPER_FREQ,LOWER_FREQ,UPPER_FREQ,CODE,FAMILY,MANUFACTURER");
            r.Select("TECH_IDNO,DATE_MODIFIED,MODIFIED_BY,DATE_CREATED,CREATED_BY");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            r.Open();
            try
            {               
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
                else
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(_tableName, 1, -1));
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }

                r.Put("MAX_POWER", _maxPower[PowerUnits.dBm]);
                r.Put("NAME", _equipmentName);
                r.Put("DESIG_EMISSION", _desigEmission);
                r.Put("BW", BandWidth);
                r.Put("CERTIFICATE_DATE", Certificate.Date);
                r.Put("CERTIFICATE_NUM", Certificate.Symbol);
                r.Put("RX_LOWER_FREQ", MinFreqRx);
                r.Put("RX_UPPER_FREQ", MaxFreqRx);
                r.Put("LOWER_FREQ", MinFreqTx);
                r.Put("UPPER_FREQ", MaxFreqTx);
                r.Put("CODE", CodeEquip);
                r.Put("FAMILY", FamilyEquip);
                r.Put("MANUFACTURER", ManufactEquip);
                r.Put("TECH_IDNO", TechNoEquip);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}

