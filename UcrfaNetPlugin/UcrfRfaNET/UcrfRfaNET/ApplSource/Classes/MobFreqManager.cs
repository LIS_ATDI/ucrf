﻿using System;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    internal class MobFreq
    {
        public const double Precision = 0.0000001;
        public int Id { get; set; }
        public int PlanId { get; set; }
        public double TxMHz { get; set; }
        public double RxMHz { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public MobFreq()
        {
            Id = IM.NullI;
            PlanId = IM.NullI;
            TxMHz = IM.NullD;
            RxMHz = IM.NullD;
        }
    }

    internal class MobFreqManager
    {
        public enum TypeFreq { MobStation=1, MobStation2=2 };
        /// <summary>
        /// Сохраняет частоты станции
        /// </summary>
        /// <param name="stationId">ID станции</param>
        /// <param name="tableName">Название таблицы станции</param>
        /// <param name="freqs">список частот</param>
        public static void SaveFreq(string tableName, int stationId, params MobFreq[] freqs)
        {
            TypeFreq typeFreq;
            switch (tableName)
            {
                case ICSMTbl.itblMobStation:
                    typeFreq = TypeFreq.MobStation;
                    break;
                case ICSMTbl.itblMobStation2:
                    typeFreq = TypeFreq.MobStation2;
                    break;
                default:
                    throw new Exception("Error table name");
            }
            SaveFreq(stationId, typeFreq, freqs);
        }

        /// <summary>
        /// Сохраняет частоты станции
        /// </summary>
        /// <param name="stationId">ID станции</param>
        /// <param name="typeFreq">Тип станции</param>
        /// <param name="freqs">список частот</param>
        public static void SaveFreq(int stationId, TypeFreq typeFreq, params MobFreq[] freqs)
        {
            string tableName;
            switch(typeFreq)
            {
                case TypeFreq.MobStation:
                    tableName = ICSMTbl.itblMobStaFreqs;
                    break;
                case TypeFreq.MobStation2:
                    tableName = ICSMTbl.itblMobStaFreqs2;
                    break;
                default:
                    throw new Exception("Error frequency type");
            }
            const string mobStaFreqsFields = "ID,STA_ID,TX_FREQ,RX_FREQ,PLAN_ID";
            List<int> deleteFreq = new List<int>();
            {
                IMRecordset rsMobStaFreqs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsMobStaFreqs.Select(mobStaFreqsFields);
                    rsMobStaFreqs.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId);
                    for (rsMobStaFreqs.Open(); !rsMobStaFreqs.IsEOF(); rsMobStaFreqs.MoveNext())
                    {
                        bool isFound = false;
                        int id = rsMobStaFreqs.GetI("ID");
                        int planid = rsMobStaFreqs.GetI("PLAN_ID");
                        double txTmp = rsMobStaFreqs.GetD("TX_FREQ");
                        double rxTmp = rsMobStaFreqs.GetD("RX_FREQ");
                        foreach (MobFreq freq in freqs)
                        {
                            bool txIsEqual = Math.Abs(txTmp - freq.TxMHz) < MobFreq.Precision;
                            bool rxIsEqual = Math.Abs(rxTmp - freq.RxMHz) < MobFreq.Precision;
                            if (txIsEqual && rxIsEqual && ((freq.Id == IM.NullI) || (freq.Id == id)))
                            {
                                freq.Id = id;
                                freq.PlanId = planid;
                                isFound = true;
                                break;
                            }
                        }
                        if (isFound == false)
                            deleteFreq.Add(id);  //Удалить частоту
                    }
                }
                finally
                {
                    if (rsMobStaFreqs.IsOpen())
                        rsMobStaFreqs.Close();
                    rsMobStaFreqs.Destroy();
                }
            }
            if (deleteFreq.Count > 0)
            {
                string table = (typeFreq == TypeFreq.MobStation) ? ICSMTbl.itblMobStation : ICSMTbl.itblMobStation2;
                //Удалаем частоты
                IMRecordset rsMobStaFreqs = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
                try
                {
                    rsMobStaFreqs.Select(mobStaFreqsFields);
                    rsMobStaFreqs.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId);
                    for (rsMobStaFreqs.Open(); !rsMobStaFreqs.IsEOF(); rsMobStaFreqs.MoveNext())
                    {
                        int id = rsMobStaFreqs.GetI("ID");
                        foreach (int idDeletedFreq in deleteFreq)
                            if (idDeletedFreq == id)
                            {
                                {
                                    //Удаляем ссылку на частоту из координации
                                    IMRecordset rsWienCoordMob = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadWrite);
                                    try
                                    {
                                        rsWienCoordMob.Select("ID");
                                        rsWienCoordMob.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, id);
                                        rsWienCoordMob.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Like, table);
                                        rsWienCoordMob.Open();
                                        if (!rsWienCoordMob.IsEOF())
                                            rsWienCoordMob.Delete();
                                    }
                                    finally
                                    {
                                        if (rsWienCoordMob.IsOpen())
                                            rsWienCoordMob.Close();
                                        rsWienCoordMob.Destroy();
                                    }
                                }
                                rsMobStaFreqs.Delete();
                                break;
                            }
                    }
                }
                finally
                {
                    if (rsMobStaFreqs.IsOpen())
                        rsMobStaFreqs.Close();
                    rsMobStaFreqs.Destroy();
                }
            }
            //Добавляем новые частоты
            {
                IMRecordset rsMobStaFreqs = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
                try
                {
                    rsMobStaFreqs.Select(mobStaFreqsFields);
                    rsMobStaFreqs.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                    rsMobStaFreqs.Open();
                    foreach (MobFreq freq in freqs)
                    {
                        if (freq.Id != IM.NullI)
                            continue;
                        freq.Id = IM.AllocID(tableName, 1, -1);
                        rsMobStaFreqs.AddNew();
                        rsMobStaFreqs.Put("ID", freq.Id);
                        rsMobStaFreqs.Put("STA_ID", stationId);
                        rsMobStaFreqs.Put("PLAN_ID", freq.PlanId); 
                        rsMobStaFreqs.Put("RX_FREQ", freq.RxMHz);
                        rsMobStaFreqs.Put("TX_FREQ", freq.TxMHz);
                        rsMobStaFreqs.Update();
                    }
                }
                finally
                {
                    if (rsMobStaFreqs.IsOpen())
                        rsMobStaFreqs.Close();
                    rsMobStaFreqs.Destroy();
                }
            }
        }
        /// <summary>
        /// Загружает список частот
        /// </summary>
        /// <param name="stationId">ID станции</param>
        /// <param name="tableName">Название таблицы станции</param>
        /// <returns>Список частот</returns>
        public static MobFreq[] LoadFreq(int stationId, string tableName)
        {
            TypeFreq typeFreq;
            switch (tableName)
            {
                case ICSMTbl.itblMobStation:
                    typeFreq = TypeFreq.MobStation;
                    break;
                case ICSMTbl.itblMobStation2:
                    typeFreq = TypeFreq.MobStation2;
                    break;
                default:
                    throw new Exception("Error table name");
            }
            return LoadFreq(stationId, typeFreq);
        }

        /// <summary>
        /// Загружает список частот
        /// </summary>
        /// <param name="stationId">ID станции</param>
        /// <param name="typeFreq">Тип станции</param>
        /// <returns>Список частот</returns>
        public static MobFreq[] LoadFreq(int stationId, TypeFreq typeFreq)
        {
            string tableName;
            switch (typeFreq)
            {
                case TypeFreq.MobStation:
                    tableName = ICSMTbl.itblMobStaFreqs;
                    break;
                case TypeFreq.MobStation2:
                    tableName = ICSMTbl.itblMobStaFreqs2;
                    break;
                default:
                    throw new Exception("Error frequency type");
            }
            const string mobStaFreqsFields = "ID,STA_ID,TX_FREQ,RX_FREQ,PLAN_ID";
            List<MobFreq> retFreqs = new List<MobFreq>();
            {
                IMRecordset rsMobStaFreqs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsMobStaFreqs.Select(mobStaFreqsFields);
                    rsMobStaFreqs.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId);
                    rsMobStaFreqs.OrderBy("TX_FREQ", OrderDirection.Ascending);
                    for (rsMobStaFreqs.Open(); !rsMobStaFreqs.IsEOF(); rsMobStaFreqs.MoveNext())
                    {
                        MobFreq freq = new MobFreq();
                        freq.Id = rsMobStaFreqs.GetI("ID");
                        freq.PlanId = rsMobStaFreqs.GetI("PLAN_ID");
                        freq.TxMHz = rsMobStaFreqs.GetD("TX_FREQ");
                        freq.RxMHz = rsMobStaFreqs.GetD("RX_FREQ");
                        retFreqs.Add(freq);
                    }
                }
                finally
                {
                    if (rsMobStaFreqs.IsOpen())
                        rsMobStaFreqs.Close();
                    rsMobStaFreqs.Destroy();
                }
            }
            return retFreqs.ToArray();
        }
    }
}
