﻿using System;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class EmitterStation : BaseStation
    {
        internal class EmitterFreq
        {
            public double Freq;
            public double FreqMin;
            public double FreqMax;
            public double Bw;

            public override string ToString()
            {
                return Freq + " MHz (" + FreqMin + " - " + FreqMax + ") MHz, BW: " + Bw; 
            }
        }
        
        public const string TableNamePosition = "XFA_POSITION";
        private int posId;
        public bool NeedPositionSave;

        public List<EmitterFreq> EmFreqList;
        public EmitterEquipment EmEquip;

        /// <summary>
        /// Висота встановлення над рівнем Землі
        /// </summary>
        public double AGL { get; set; }

        /// <summary>
        /// Сертифікат
        /// </summary>
        public Certificate Certific { get; set; }

        /// <summary>
        /// Вид випром. пристрою
        /// </summary>
        public string EmiType { get; set; }

        /// <summary>
        /// Наявність техн. засобів
        /// </summary>
        public string DopDevice { get; set; }

        /// <summary>
        /// Потужність передавача, Вт
        /// </summary>
        public Power Pow { get; set; }
        /// <summary>
        /// Owner ID
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Стандарт
        /// </summary>
        public string Standart { get; set; }
        
        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }

        public int FreqPlanId { get; set; }
        public int FreqPlanChanId { get; set; }
        public int EquipId { get; set; }  
        public Dictionary<string, string> FreqDict = new Dictionary<string, string>();
        public string EquipName { get; set; }

        /// <summary>
        /// Частоти
        /// </summary>
        public double FreqCentr { get; set; }
        public double FreqMin { get; set; }
        public double FreqMax { get; set; }

        /// <summary>
        /// Ширина смуги
        /// </summary>
        public double Bw { get; set; }

        /// <summary>
        /// Сохраняет данные станции
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaEmitterStation, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,POS_ID,AGL,CERT_NUM,CERT_DATE,EMI_TYPE,DOP_DEVICE,POWER,STATUS,FREQPLANCHAN_ID,STANDARD,FREQ");
                r.Select("USER_ID,EQUIP_ID,EQUIP_NAME,BW,DATE_CREATED,CREATED_BY,FREQ_MIN,FREQ_MAX");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }
                else
                    r.Edit();
                r.Put("USER_ID",OwnerId);
                r.Put("POWER", Pow[PowerUnits.W]);
                r.Put("AGL", AGL);
                r.Put("CERT_DATE", Certific.Date);
                r.Put("CERT_NUM", Certific.Symbol);
                r.Put("EMI_TYPE", EmiType);
                r.Put("FREQPLANCHAN_ID", FreqPlanId);                
                r.Put("DOP_DEVICE", DopDevice);
                r.Put("STANDARD", Standart);
                r.Put("STATUS", Status);
                r.Put("FREQ", FreqCentr);
                r.Put("FREQ_MIN", FreqMin);
                r.Put("FREQ_MAX", FreqMax);
                r.Put("EQUIP_ID",EquipId);
                r.Put("EQUIP_NAME",EquipName);
                r.Put("BW", Bw);
                //Позиция
                if (Position.TableName != PlugTbl.XfaPosition)
                {
                    Position.Id = IM.NullI;
                    Position.TableName = PlugTbl.XfaPosition;
                }
                if (NeedPositionSave)
                {
                    Position.SaveToBD();
                    r.Put("POS_ID", Position.Id);
                }

                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }


            IM.RefreshQueries(PlugTbl.XfaEmitterStation);
        }

        /// <summary>
        /// Загружаем данные станции
        /// </summary>
        public override void Load()
        {
            EmFreqList=new List<EmitterFreq>();
            EmEquip = new EmitterEquipment();
            Certific = new Certificate();
            Pow = new Power();
          
            IMRecordset r = new IMRecordset(PlugTbl.XfaEmitterStation, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,AGL,CERT_NUM,CERT_DATE,EMI_TYPE,DOP_DEVICE,POWER,FREQPLANCHAN_ID,STANDARD,FREQ");
                r.Select("USER_ID,EQUIP_ID,EQUIP_NAME,BW,STATUS,FREQ_MIN,FREQ_MAX");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    posId = r.GetI("POS_ID");
                    Pow[PowerUnits.W] = r.GetD("POWER");
                    AGL = r.GetD("AGL");
                    Certific.Date = r.GetT("CERT_DATE");
                    Certific.Symbol = r.GetS("CERT_NUM");
                    EmiType = r.GetS("EMI_TYPE");
                    DopDevice = r.GetS("DOP_DEVICE");
                    FreqCentr = r.GetD("FREQ");
                    FreqMin = r.GetD("FREQ_MIN");
                    FreqMax = r.GetD("FREQ_MAX");
                    OwnerId = r.GetI("USER_ID");
                    EquipId = r.GetI("EQUIP_ID");
                    Status = r.GetS("STATUS");
                    EquipName = r.GetS("EQUIP_NAME");
                    FreqPlanChanId = r.GetI("FREQPLANCHAN_ID");
                    string tempStand = r.GetS("STANDARD");
                    if (!string.IsNullOrEmpty(tempStand))
                        Standart = tempStand;
                    Bw = r.GetD("BW");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
         
            GetCentrFreq(EmiType);
            //Позиція
            Position = new PositionState();
            Position.LoadStatePosition(posId, TableNamePosition);         
        }

        /// <summary>
        /// Отримати список всіх частот
        /// </summary>
        /// <param name="types"></param>
        public void GetCentrFreq(string types)
        {
            EmFreqList.Clear();
            IMRecordset r= new IMRecordset(PlugTbl.XfaEmitterStation,IMRecordset.Mode.ReadOnly);
            r.Select("ID,TABLE_NAME,EMI_TYPE,FREQ,FREQ_MIN,FREQ_MAX,FREQPLANCHAN_ID,BW");
            r.SetWhere("EMI_TYPE", IMRecordset.Operation.Eq, types);
            try
            {
                for (r.Open();!r.IsEOF();r.MoveNext())               
                {
                    EmitterFreq tmpFreq=new EmitterFreq();
                    tmpFreq.Freq = r.GetD("FREQ");
                    tmpFreq.FreqMin = r.GetD("FREQ_MIN");
                    tmpFreq.FreqMax = r.GetD("FREQ_MAX");
                    tmpFreq.Bw = r.GetD("BW");
                    EmFreqList.Add(tmpFreq);
                }
            }
            finally 
            {                
                r.Final();
            }
        }
    }
}
