﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using LisUtility;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRR;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    //===============================================================
    /// <summary>
    /// Класс для сотовой связи
    /// </summary>
    /// 	
    internal class MobStaTechnologicalApp : BaseAppClass
    {
        public static readonly string TableName = ICSMTbl.itblMobStation;

        protected string RadioTech;
        public PositionState2 objPosition = null;
        protected IMObject objEquip = null;
        protected IMObject objAntenna = null;

        double Lon;
        double Lat;
        double Height;
        string strFreqTX = "";
        string strFreqRX = "";

        protected List<double> TxStationFreq = new List<double>();
        protected List<double> RxStationFreq = new List<double>();
        protected string userTech = "Технологічні";
        protected string userTelecom = "Комунікації";
        private bool isDestroyed = false;  //Сергей. Поставил заглушку на CellValidate. После Destroy срабатывал CellValidate и вываливалась ошибка
        //============================================
        public MobStaTechnologicalApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, MobStaTechnologicalApp.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppTR;
            department = CUsers.GetUserDepartmentType(); //#3839
            departmentSector = DepartmentSectorType.VRR_STZ;
            objPosition = null;
            objEquip = null;
            RadioTech = _radioTech;
            RxStationFreq.Clear();

            SCDozvil = objStation.GetS("CUST_TXT16");
            SCVisnovok = objStation.GetS("CUST_TXT17");
            SCNote = objStation.GetS("CUST_TXT18");


            //MMSI = objStation.GetS("CUST_TXT6");
            //Oznach_Rez = objStation.GetS("CUST_TXT10");
            //----
            addValidVisnMonth = 6;
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            isDestroyed = true;
            if (objEquip != null)
            {
                objEquip.Dispose();
                objEquip = null;
            }
            if (objAntenna != null)
            {
                objAntenna.Dispose();
                objAntenna = null;
            }
            base.DisposeExec();
        }

        protected override string GetXMLParamGrid()
        {
            string XmlString = "";
            // если технология РБСС
            if (Standard == CRadioTech.RBSS)
            {
                XmlString =
                 "<items>"
               + "<item key='header0'    type='lat'       flags=''     tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
               + "<item key='header1'    type='lat'       flags='s'    tab=''   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"
                    //1
               + "<item key='kStandard'    type='lat'   flags=''     tab=''   display='Радіотехнологія(Стандарт)' ReadOnly='true' background='' bold=''  />"
               + "<item key='standard'    type='string'  flags='s'    tab='1'   display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
                    //2              
               + "<item key='kName' type='lat'       flags=''     tab=''  display='Означення станції/Клас станції'     ReadOnly='true' background='' bold='' />"
               + "<item key='ozn'    type='string'   DBField='NAME'    flags='s'    tab='2'   display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='Class0' type='double'   flags='s'    tab='3'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //3
               + "<item key='mKLat'      type='lat'       flags=''     tab=''  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y'/>"
               + "<item key='KLat0'      type='lat'       flags='s'    tab='4'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //4
               + "<item key='mKLon'     type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y'/>"
               + "<item key='KLon0'      type='lat'       flags='s'    tab='5'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //5
               + "<item key='kAsl'      type='lat'       flags=''     tab=''  display='Висота поверхні землі, м'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='asl'       type='lat' flags='s'    tab=''  display=''     ReadOnly='true' background='' bold=''  HorizontalAlignment='center' fontColor='gray' />"
                    //6
               + "<item key='kAdr'      type='lat'       flags=''     tab=''  display='Адреса встановлення РЕЗ'     ReadOnly='true' background='' bold='y'/>"
               + "<item key='addr'      type='lat'       flags='s'    tab='6'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
                    //7
               + "<item key='kType'      type='lat'       flags=''     tab=''  display='Назва/тип РЕЗ'     ReadOnly='true' background='' bold='y'/>"
               + "<item key='type'      type='lat'       flags='s'    tab='7'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //8            
               + "<item key='kPower'     type='lat'       flags=''     tab=''  display='Потужність передавача, Вт'     ReadOnly='true' background='' bold='' />"
               + "<item key='pow0'       type='lat'       flags='s'    tab='8'  display=''     ReadOnly='false' background='' bold=''  HorizontalAlignment='center'/>"
                    //9            
               + "<item key='kClass'    type='lat'       flags=''     tab=''  display='Клас випромінювання'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='class'     type='string' flags='s' DBField='DESIG_EMISSION' tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //10            
               + "<item key='kSert'      type='lat'       flags=''     tab=''  display='Сертифікат відповідності(№/дата)'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='sertNom'    type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
               + "<item key='sertDate'   type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //11
               + "<item key='kAntType'   type='lat'       flags=''     tab=''  display='Тип антени'     ReadOnly='true' background='' bold='y' />"
               + "<item key='antType0'   type='lat'       flags='s'    tab='9'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
                    //12
               + "<item key='kAntGain'      type='lat'       flags=''     tab=''  readOnly = 'true' display='Коеф. підсилення антени, дБі/Ширина ДС'     ReadOnly='true' background='' bold='' fontColor='gray'  />"
               + "<item key='antGain0'   type='double'  DBField='GAIN' flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
               + "<item key='antDsWdth0'   type='double'  flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //13
               + "<item key='kAntHeight' type='lat'       flags=''     tab=''  display='Висота антени над рівнем землі, м'     ReadOnly='true' background='' bold='' />"
               + "<item key='antHeight0' type='double'   flags='s' DBField='AGL' tab='10'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //14
               + "<item key='kAntAz' type='lat'       flags=''     tab=''  display='Азимут випромінювання антени, град'     ReadOnly='true' background='' bold='' />"
               + "<item key='antAz0' type='double'  flags='s'  DBField='AZIMUTH' tab='11'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //15
               + "<item key='kFiderLosses' type='lat'       flags=''  tab=''  display='Затухання у фідерному тракті, дБ'     ReadOnly='true' background='' bold='' />"
               + "<item key='fiderLosses0' type='double'  flags='s' DBField='TX_LOSSES' tab='12'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //16
               + "<item key='kCallSign'   type='lat'       flags=''     tab=''  display='Позивний'     ReadOnly='true' background='' bold='' />"
               + "<item key='callsign'    type='string'    DBField='CUST_TXT9' flags='s'    tab='13'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

                //17
               + "<item key='kCallSign1'   type='lat'       flags=''     tab=''  display='Ідентифікатор МРС (MMSI)'     ReadOnly='true' background='' bold='' />"
               + "<item key='MMSI'    type='string'    DBField='CUST_TXT6' flags='s'    tab='14'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

               //18
               + "<item key='kDD'   type='lat'       flags=''     tab=''  display='Дуплексний рознос, МГц'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='rxDD0'   type='double'      flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
                    //19
               + "<item key='kTxFreq'   type='lat'       flags=''     tab=''  display='Частоти передавання, МГц'     ReadOnly='true' background='' bold='y' />"
               + "<item key='txFreq0'   type='double'    flags='s'    tab='15'  display=''     ReadOnly='' background='' bold='y' HorizontalAlignment='center' />"
                    //20
               + "<item key='kRxFreq'   type='lat'       flags=''     tab=''  display='Частоти приймання, МГц'     ReadOnly='true' background='' bold='y' />"
               + "<item key='rxFreq0'   type='double'       flags='s'    tab='16'  display=''     ReadOnly='' background='' bold='y' HorizontalAlignment='center'/>"


                //21
               + "<item key='kCallSign2'   type='lat'       flags=''     tab=''  display='Означення РЕЗ'     ReadOnly='true' background='' bold='' />"
               + "<item key='OznachRez'    type='string'    DBField='CUST_TXT10' flags='s'    tab='17'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

               //22

               + "<item key='kObj'       type='lat'       flags=''     tab=''  display='Об`єкт(и) РЧП'     ReadOnly='true' background='' bold='' />"
               + "<item key='obj0'       type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
               + "</items>"
                    //
               ;
            }
            // если другая технология
            else
            {

                XmlString =
           "<items>"
         + "<item key='header0'    type='lat'       flags=''     tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
         + "<item key='header1'    type='lat'       flags='s'    tab=''   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"
                    //1
         + "<item key='kStandard'    type='lat'   flags=''     tab=''   display='Радіотехнологія(Стандарт)' ReadOnly='true' background='' bold=''  />"
         + "<item key='standard'    type='string'  flags='s'    tab='1'   display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
                    //2              
         + "<item key='kName' type='lat'       flags=''     tab=''  display='Означення станції/Клас станції'     ReadOnly='true' background='' bold='' />"
         + "<item key='ozn'    type='string'   DBField='NAME'    flags='s'    tab='2'   display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
         + "<item key='Class0' type='double'   flags='s'    tab='3'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

         //3
         + "<item key='mKLat'      type='lat'       flags=''     tab=''  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y'/>"
         + "<item key='KLat0'      type='lat'       flags='s'    tab='4'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //4
         + "<item key='mKLon'     type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y'/>"
         + "<item key='KLon0'      type='lat'       flags='s'    tab='5'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //5
         + "<item key='kAsl'      type='lat'       flags=''     tab=''  display='Висота поверхні землі, м'     ReadOnly='true' background='' bold='' fontColor='gray' />"
         + "<item key='asl'       type='lat' flags='s'    tab=''  display=''     ReadOnly='true' background='' bold=''  HorizontalAlignment='center' fontColor='gray' />"
                    //6
         + "<item key='kAdr'      type='lat'       flags=''     tab=''  display='Адреса встановлення РЕЗ'     ReadOnly='true' background='' bold='y'/>"
         + "<item key='addr'      type='lat'       flags='s'    tab='6'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
                    //7
         + "<item key='kType'      type='lat'       flags=''     tab=''  display='Назва/тип РЕЗ'     ReadOnly='true' background='' bold='y'/>"
         + "<item key='type'      type='lat'       flags='s'    tab='7'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                    //8            
         + "<item key='kPower'     type='lat'       flags=''     tab=''  display='Потужність передавача, Вт'     ReadOnly='true' background='' bold='' />"
         + "<item key='pow0'       type='lat'       flags='s'    tab='8'  display=''     ReadOnly='false' background='' bold=''  HorizontalAlignment='center'/>"
                    //9            
         + "<item key='kClass'    type='lat'       flags=''     tab=''  display='Клас випромінювання'     ReadOnly='true' background='' bold='' fontColor='gray' />"
         + "<item key='class'     type='string' flags='s' DBField='DESIG_EMISSION' tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //10            
         + "<item key='kSert'      type='lat'       flags=''     tab=''  display='Сертифікат відповідності(№/дата)'     ReadOnly='true' background='' bold='' fontColor='gray' />"
         + "<item key='sertNom'    type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
         + "<item key='sertDate'   type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //11
         + "<item key='kAntType'   type='lat'       flags=''     tab=''  display='Тип антени'     ReadOnly='true' background='' bold='y' />"
         + "<item key='antType0'   type='lat'       flags='s'    tab='9'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
                    //12
         + "<item key='kAntGain'      type='lat'       flags=''     tab=''  readOnly = 'true' display='Коеф. підсилення антени, дБі/Ширина ДС'     ReadOnly='true' background='' bold='' fontColor='gray'  />"
         + "<item key='antGain0'   type='double'  DBField='GAIN' flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
         + "<item key='antDsWdth0'   type='double'  flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                    //13
         + "<item key='kAntHeight' type='lat'       flags=''     tab=''  display='Висота антени над рівнем землі, м'     ReadOnly='true' background='' bold='' />"
         + "<item key='antHeight0' type='double'   flags='s' DBField='AGL' tab='10'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //14
         + "<item key='kAntAz' type='lat'       flags=''     tab=''  display='Азимут випромінювання антени, град'     ReadOnly='true' background='' bold='' />"
         + "<item key='antAz0' type='double'  flags='s'  DBField='AZIMUTH' tab='11'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //15
         + "<item key='kFiderLosses' type='lat'       flags=''  tab=''  display='Затухання у фідерному тракті, дБ'     ReadOnly='true' background='' bold='' />"
         + "<item key='fiderLosses0' type='double'  flags='s' DBField='TX_LOSSES' tab='12'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                    //16
         + "<item key='kCallSign'   type='lat'       flags=''     tab=''  display='Позивний'     ReadOnly='true' background='' bold='' />"
         + "<item key='callsign'    type='string'    DBField='CUST_TXT9' flags='s'    tab='13'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"


         //17
         + "<item key='kDD'   type='lat'       flags=''     tab=''  display='Дуплексний рознос, МГц'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
         + "<item key='rxDD0'   type='double'      flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

         //18
         + "<item key='kTxFreq'   type='lat'       flags=''     tab=''  display='Частоти передавання, МГц'     ReadOnly='true' background='' bold='y' />"
         + "<item key='txFreq0'   type='double'    flags='s'    tab='15'  display=''     ReadOnly='' background='' bold='y' HorizontalAlignment='center' />"

         //19
         + "<item key='kRxFreq'   type='lat'       flags=''     tab=''  display='Частоти приймання, МГц'     ReadOnly='true' background='' bold='y' />"
         + "<item key='rxFreq0'   type='double'       flags='s'    tab='16'  display=''     ReadOnly='' background='' bold='y' HorizontalAlignment='center'/>"

        //20

         + "<item key='kObj'       type='lat'       flags=''     tab=''  display='Об`єкт(и) РЧП'     ReadOnly='true' background='' bold='' />"
         + "<item key='obj0'       type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
         + "</items>"

         ;
            }


            return XmlString;
        }

        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KLon0")
                 || (cell.Key == "addr")
                 || (cell.Key == "type")
                 || (cell.Key == "type")
                 || (cell.Key == "antType0")
                 || (cell.Key == "obj0")
                 || (cell.Key == "asl")
                 || (cell.Key == "txFreq0"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
                if (cell.Key == "obj0")
                    cell.ButtonText = "Координуємі...";
            }
            else if (cell.Key == "Class0")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CStationClass.getlstStationClass();
            }
            else if (cell.Key == "standard")
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CStandard.getListStandard(RadioTech);
            }
            else if (cell.Key == "user")
            {
                List<string> UserList = new List<string>();
                UserList.Add(userTech);
                UserList.Add(userTelecom);

                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = UserList;
            }
        }
        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRR formEms = new FormVRR(recordID.Table, recordID.Id);
            formEms.ShowDialog();
            formEms.Dispose();
        }
        //===========================================================
        /// <summary>
        /// Добавление станции к сети
        /// </summary>
        public override void Add2Net()
        {
            if (NeedSaveAppl() == false)
                return;
            string addFilter = "";
            int[] connNets = Net.NetObject.GetConnectedNets(recordID.Id, recordID.Table);
            foreach (int netId in connNets)
            {
                if (string.IsNullOrEmpty(addFilter) == false)
                    addFilter += ",";
                addFilter += netId;
            }
            if (string.IsNullOrEmpty(addFilter) == false)
                addFilter = string.Format("([ID] NOT IN ({0}))", addFilter);
            //Выбор сети
            string param1 = string.Format("([{0}] > {1})", "ID", 0);
            string param2 = string.Format("([{0}] = {1})", "OWNER_ID", OwnerID);
            string param3 = string.Format("({0} AND {1})", (string.IsNullOrEmpty(addFilter) ? param1 : addFilter), param2);
            RecordPtr retRec = SelectRecord("Мережа", PlugTbl.XfaNet, param3,
                                            "", OrderDirection.Ascending);
            if ((retRec.Id > 0) && (retRec.Id != IM.NullI))
            {
                Net.NetStationItem station = new Net.NetStationItem();
                station.Load(recordID.Id, recordID.Table);
                station.Save(retRec.Id);
                string mess = string.Format("Станція успішно додана до мережі '{0}'.", Net.NetObject.GetNetName(retRec.Id));
                MessageBox.Show(mess, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает префикс "Дозвола на екстплуатацію"
        /// </summary>
        /// <returns></returns>
        private string getPrefixDozv()
        {
            string retPrefix = "";
            string classStation = objStation.GetS("CLASS");
            if (classStation == "FB")
                retPrefix += "БС" + ((Standard == CRadioTech.SOIPC) ? "З" : "");
            else if ((classStation == "FL") || (classStation == "FL"))
                retPrefix += "СА" + ((Standard == CRadioTech.SOIPC) ? "З" : "");
            else if (classStation == "FR")
                retPrefix += "ПР" + ((Standard == CRadioTech.SOIPC) ? "З" : "");
            // частоты
            List<double> listTmp = null;
            if (RxStationFreq.Count > 0)
                listTmp = RxStationFreq;
            else if (TxStationFreq.Count > 0)
                listTmp = TxStationFreq;

            if ((listTmp != null) && (Standard != CRadioTech.SOIPC))
            {
                if ((30.0 <= listTmp[0]) && (listTmp[0] <= 60.0))
                    retPrefix += "30";
                else if ((140.0 <= listTmp[0]) && (listTmp[0] <= 180.0))
                    retPrefix += "150";
                else if (Standard != CRadioTech.KX)
                    retPrefix += "400";
                else if (Standard == CRadioTech.KX)
                    retPrefix += "30";


            }
            return retPrefix;
        }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (objPosition != null) ? objPosition.Province : "";
            string city = (objPosition != null) ? objPosition.City : "";

            if (string.IsNullOrEmpty(number))
            {
                if (Standard == CRadioTech.SOIPC)
                    number = PrintDocs.GetListDocNumberForDozvWma(1)[0];
                else
                    number = PrintDocs.getListDocNumber(docType, 1)[0];
            }

            if ((docType == DocType.VISN) || (docType == DocType.LYST_TR_CALLSIGN))
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.LYST_TR_CALLSIGN))
            {
                switch (Standard)
                {
                    case CRadioTech.RBSS:
                    case CRadioTech.RPS:
                    case CRadioTech.RSR:
                        fullPath += string.Format("МР-{0}-{1}.rtf", HelpFunction.getAreaCode(prov, city), number);
                        break;
                    default:
                        fullPath += string.Format("{0}-{1}-{2}.rtf", getPrefixDozv(), HelpFunction.getAreaCode(prov, city), number);
                        break;
                }
            }
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else if (docType == DocType.DOZV_TR_SPEC)
            {
                string selectedProv = HelpFunction.getAreaCode(prov, city).Trim();
                string prefix = "СД";
                fullPath += prefix + "-" + selectedProv + "-" + (number.Length < 6 ? number.PadRight(6, '0') : number) + ".rtf";
            }
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    IMObject station = objStation;
        //    {
        //        IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //        r.Select("ID,CUST_TXT5,CUST_DAT4,CUST_DAT5,CUST_TXT4,CUST_TXT3,CUST_DAT2,CUST_DAT3,CUST_TXT2");
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, station.GetI("ID"));
        //        try
        //        {
        //            r.Open();
        //            if (!r.IsEOF())
        //            {
        //                r.Edit();
        //                switch (docType)
        //                {
        //                    case DocType.VISN:
        //                        r.Put("CUST_TXT3", docName.ToFileNameWithoutExtension());
        //                        station.Put("CUST_TXT3", docName.ToFileNameWithoutExtension());
        //                        r.Put("CUST_DAT2", startDate);
        //                        station.Put("CUST_DAT2", startDate);
        //                        r.Put("CUST_DAT3", endDate);
        //                        station.Put("CUST_DAT3", endDate);
        //                        r.Put("CUST_TXT2", IM.ConnectedUser());
        //                        station.Put("CUST_TXT2", IM.ConnectedUser());
        //                        break;
        //                }
        //                r.Update();
        //            }
        //        }
        //        finally
        //        {
        //            r.Close();
        //            r.Destroy();
        //        }
        //    }
        //}
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "standard")
            {
                cell.Value = objStation.GetS("NETWORK_IDENT");
                AutoFill(cell, grid);
            }
            else
                if (cell.Key == "KLon0")
                {
                    int msID = objStation.GetI("ID");

                    int posId = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,POS_ID");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, msID);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        posId = r.GetI("POS_ID");
                        if ((posId != IM.NullI) && (objPosition==null))
                        {
                            objPosition = new PositionState2();
                            objPosition.LoadStatePosition(posId, ICSMTbl.itblPositionWim);
                            objPosition.GetAdminSiteInfo(objPosition.AdminSiteId);
                        }
                    }
                    r.Close();
                    r.Destroy();

                    if (objPosition != null)
                    {
                        double lonDMS = objPosition.LongDms;
                        cell.Value = (lonDMS != IM.NullD) ? HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon) : "";
                        if (lonDMS != IM.NullD && !HelpFunction.ValidateCoords(lonDMS, EnumCoordLine.Lon))
                            ChangeColor(cell, Colors.badValue);
                        else
                            ChangeColor(cell, Colors.okvalue);
                        IndicateDifference(cell, objPosition.LonDiffersFromAdm);
                        AutoFill(cell, grid);
                    }
                }
                else if (cell.Key == "type")
                {
                    int msID = objStation.GetI("ID");
                    if (objEquip == null)
                    {
                        int equipId = IM.NullI;
                        IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,EQUIP_ID,");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, msID);
                        r.Open();
                        if (!r.IsEOF())
                        {
                            equipId = r.GetI("EQUIP_ID");
                            if (equipId != IM.NullI)
                                objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipPmr, equipId);
                        }
                        r.Close();
                        r.Destroy();
                    }

                    if (objEquip != null)
                    {
                        cell.Value = objEquip.GetS("NAME");
                        AutoFill(cell, grid);
                    }
                }
                else if (cell.Key == "antAz0")
                {
                    HelpFunction.LoadFromDbToCell(cell, objStation);
                    CellValidate(cell, grid);
                }

            if (cell.Key == "antType0")
            {
                if (objAntenna == null)
                {
                    int posId = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,ANT_ID");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, objStation.GetI("ID"));
                    r.Open();
                    if (!r.IsEOF())
                    {
                        posId = r.GetI("ANT_ID");
                        if (posId != IM.NullI)
                            objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntennaMob, posId);
                    }
                    r.Close();
                    r.Destroy();
                }

                if (objAntenna != null)
                {
                    cell.Value = objAntenna.GetS("NAME");
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "Class0")
            {
                string classStation = objStation.GetS("CLASS");
                SelectByShort(cell, classStation);
            }
            else if (cell.Key == "pow0")
            {
                double maxPower = objStation.GetD("PWR_ANT");
                if (maxPower != IM.NullD)
                {
                    double PowerAsDouble = Math.Pow(10.0, maxPower / 10.0);
                    cell.Value = PowerAsDouble.ToString("F3");
                    CellValidate(cell, grid);
                }
                else
                {
                    cell.Value = "";
                    cell.BackColor = System.Drawing.Color.White;
                }
            }
            else if ((cell.Key == "ozn") || (cell.Key == "class") || (cell.Key == "antHeight0") || (cell.Key == "callsign") || (cell.Key == "MMSI") || (cell.Key == "OznachRez"))
            {
                if (cell.Key == "MMSI")
                {
                    MMSI = cell.Value;
                }
                if (cell.Key == "OznachRez")
                {
                    Oznach_Rez = cell.Value;
                }

                HelpFunction.LoadFromDbToCell(cell, objStation);
            }
            else if (cell.Key == "obj0")
            {
                cell.Value = objStation.GetI("ID").ToString();
                CellValidate(cell, grid);
            }
            else if (cell.Key == "user")
            {
                cell.Value = objStation.GetS("CUST_TXT8");
            }
            else if (cell.Key == "txFreq0")
            {
                TxStationFreq.Clear();
                int stationId = objStation.GetI("ID");
                MobFreq[] freqs = MobFreqManager.LoadFreq(stationId, MobFreqManager.TypeFreq.MobStation);
                foreach (MobFreq freqItem in freqs)
                {
                    double txFreq = freqItem.TxMHz;
                    if (txFreq != IM.NullD)
                        TxStationFreq.Add(txFreq);
                }
                HelpFunction.ToString(cell, TxStationFreq);

                strFreqTX = cell.Value;
            }
            else if (cell.Key == "rxFreq0")
            {
                RxStationFreq.Clear();
                int stationId = objStation.GetI("ID");
                MobFreq[] freqs = MobFreqManager.LoadFreq(stationId, MobFreqManager.TypeFreq.MobStation);
                foreach (MobFreq freqItem in freqs)
                {
                    double rxFreq = freqItem.RxMHz;
                    if (rxFreq != IM.NullD)
                        RxStationFreq.Add(rxFreq);
                }
                HelpFunction.ToString(cell, RxStationFreq);
                strFreqRX = cell.Value;
            }
            else if (cell.Key == "fiderLosses0")
            {
                HelpFunction.LoadFromDbToCell(cell, objStation);
                if (string.IsNullOrEmpty(cell.Value))
                    cell.Value = "0";
                CellValidate(cell, grid);
            }
        }
        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            if (cell.Key == "standard")
            {
                Cell fillCell = grid.GetCellFromKey("rxDD0");
                double DuplexDiversion = objStation.GetD("DUPLEX_SPACING");

                List<double> textFreq = new List<double>();
                if (DuplexDiversion != IM.NullD)
                    textFreq.Add(DuplexDiversion);

                if (textFreq.Count > 0)
                    HelpFunction.ToString(fillCell, textFreq);

            }
            else if (cell.Key == "KLon0")
            {
                Cell fillCell = null;

                if (objPosition != null)
                {
                    Lat = objPosition.LatDms;
                    Lon = objPosition.LongDms;
                    double latDMS = Lat;
                    fillCell = grid.GetCellFromKey("KLat0");
                    fillCell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(latDMS, EnumCoordLine.Lat))
                        ChangeColor(fillCell, Colors.badValue);
                    else
                        ChangeColor(fillCell, Colors.okvalue);
                    OnCellValidate(fillCell, grid);

                    double asl = objPosition.Asl;
                    SafeFillCellByKey(grid, "asl", asl.ToString(), false);

                    SafeFillCellByKey(grid, "addr", objPosition.FullAddressAuto, false);
                    IndicateDifference(grid.GetCellFromKey("addr"), objPosition.AddrDiffersFromAdm);

                }
                else
                {
                    SafeFillCellByKey(grid, "KLon0", "", false);

                    SafeFillCellByKey(grid, "asl", "", false);

                    SafeFillCellByKey(grid, "addr", "", false);

                }
            }
            if (cell.Key == "type")
            {
                if (objEquip != null)
                {// Заполяем поля значениями
                    SafeFillCellByKey(grid, "sertNom", objEquip.GetS("CUST_TXT1"), false);
                    SafeFillCellByKey(grid, "class", objStation.GetS("DESIG_EMISSION"), false);
                    SafeFillCellByKey(grid, "sertDate", objEquip.GetT("CUST_DAT1").ToShortDateString(), false);

                    double maxPower = objStation.GetD("PWR_ANT");
                    if (maxPower != IM.NullD)
                    {
                        SafeFillCellByKey(grid, "pow0", (Math.Pow(10.0, maxPower / 10.0)).ToString("F3"), false);
                    }
                }
                else
                {
                    SafeFillCellByKey(grid, "sertNom", "", false);
                    SafeFillCellByKey(grid, "sertDate", "", false);
                }
            }
            if (cell.Key == "antType0")
            {
                if (objAntenna != null)
                {
                    double gain = objAntenna.GetD("GAIN");
                    SafeFillCellByKey(grid, "antGain0", (gain != IM.NullD) ? gain.ToString("F3") : "", false);
                    if (objStation != null)
                        objStation.Put("GAIN", gain);
                    SafeFillCellByKey(grid, "antDsWdth0", (objAntenna.GetD("H_BEAMWIDTH") != IM.NullD) ? objAntenna.GetD("H_BEAMWIDTH").ToString("F0") : "", false);
                }
                else
                {
                    SafeFillCellByKey(grid, "antDsWdth0", "", false);
                    SafeFillCellByKey(grid, "antGain0", "", false);
                }
            }
            else if (cell.Key == "txFreq0")
            {
                Cell fillCell = null;
                HelpFunction.ToString(cell, TxStationFreq);
                fillCell = grid.GetCellFromKey("rxFreq0");
                HelpFunction.ToString(fillCell, RxStationFreq);
            }
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "type")
            {
                // Выбор оборудования
                RecordPtr recEquip = SelectEquip("Підбір обладнання", ICSMTbl.itblEquipPmr, "NAME", cell.Value, true);
                recEquip.Table = ICSMTbl.itblEquipPmr;  //Подмена таблицы
                if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                {
                    objEquip = IMObject.LoadFromDB(recEquip);
                    cell.Value = objEquip.GetS("NAME");
                    objStation["EQUIP_ID"] = objEquip.GetI("ID");

                    double maxPower = objEquip.GetD("MAX_POWER");
                    if (maxPower != IM.NullD)
                        objStation["PWR_ANT"] = maxPower - 30.0;

                    string desigEmission = objEquip.GetS("DESIG_EMISSION");
                    if (!string.IsNullOrEmpty(desigEmission))
                        objStation["DESIG_EMISSION"] = desigEmission;
                    AutoFill(cell, grid);
                    CellValidate(grid.GetCellFromKey("pow0"), grid);
                }
            }
            else if (cell.Key == "addr")
            {
                if (ShowMessageReference(objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms = Lon;
                    newPos.LatDms = Lat;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionWim, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        objPosition = newPos;
                        UpdateOneParamGrid(grid.GetCellFromKey("KLon0"), grid);
                    }
                }
            }
            else if (cell.Key == "antType0")
            {// Выбор антены
                string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cell.Value) + "*\"}";
                // Выбераем запись из таблицы
                RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntennaMob, param);
                if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(7));
                    objAntenna = IMObject.LoadFromDB(RecAnt);
                    cell.Value = objAntenna.GetS("NAME");
                    CellValidate(cell, grid);
                }
            }
            else if (cell.Key == "KLon0")//
            {
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionWim, 1, Lon, Lat);
                if (newPos != null)
                {
                    objPosition = newPos;
                    UpdateOneParamGrid(grid.GetCellFromKey("KLon0"), grid);
                }
            }
            else if (cell.Key == "obj0")
            {
                //int index = Convert.ToInt32(cell.Key.Substring(3));
                int ID = objStation.GetI("ID");

                /*
                if (gridParam.Edited || IsChangeDop)
                {
                    if (MessageBox.Show("Схоже, в картці є незбережені зміни. " +
                        Environment.NewLine + "Зберегти дані РЧП перед продовженням?", 
                        "Підтвердження", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        gridParam.CellSelect(gridParam.GetCell(0, 0));
                        gridParam.CellSelect(cell);
                        OnSaveAppl();
                    }
                }
                 */

                IMDBList.Show("Координуємі записи (якщо нема, то нема)", ICSMTbl.WienCoordMob, "[ID] in (" +
                    " select wc.ID from %" + PlugTbl.XfaWienCrd + " wc, %" + ICSMTbl.itblMobStaFreqs + " mf " +
                    " where wc.MOBSTA_FREQ_ID = mf.ID and wc.MOBSTA_FREQ_TN = '" + ICSMTbl.itblMobStation + "' and mf.STA_ID = " + ID.ToString() +
                    " )", "TX_FREQ", OrderDirection.Ascending, 0, 0);

                //RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation, ID);
                //recStation.UserEdit();
            }
            else if (cell.Key == "txFreq0")
            {
                int OwnerID = objStation.GetI("OWNER_ID");
                string Standard = objStation.GetS("NETWORK_IDENT");
                string province = "XXX";
                List<string> ProvincesList = new List<string>();

                if (objPosition != null)
                {
                    string Province = objPosition.Province;
                    province = Province;
                    ProvincesList.Add(Province);
                    string City = objPosition.City;
                    ProvincesList.Add(City);
                }

                // Извлекаем plan_id
                int plan_id = -1;
                FreqLicence[] fr = null;
                if (objStation.GetI("PLAN_ID") != null)
                {
                    plan_id = objStation.GetI("PLAN_ID");
                }

                // получаем частоты, соответсвующие частотному плану
                if (plan_id > -1)
                {
                    fr = CLicence.GetFreqsFromPlan(plan_id);
                }

                FChannels FChannel = new FChannels();

                List<double> RxFreq = new List<double>();
                List<double> TxFreq = new List<double>();

                Hashtable FreqHash = new Hashtable();

                // Добавление в список достпуных для редактирования частот
                int i = 0;
                // Если технология РБСС или РПС тогда показать список выбора частот с частотного плана,
                if ((RadioTech == CRadioTech.RBSS) || (RadioTech == CRadioTech.RPS))
                {
                    if (fr != null)
                    {
                        foreach (var freq in fr)
                        {


                            double TxLowFreq = freq.TxCentralMHz.Round(5);
                            double RxLowFreq = freq.RxCentralMHz.Round(5);

                            //Add frequencies!!! as in Builder                  
                            bool needAdd = true;
                            if (RxStationFreq.Contains(RxLowFreq) && TxStationFreq.Contains(TxLowFreq))
                            {
                                needAdd = false;
                            }

                            string FrequencyString = string.Format("{0} / {1}", TxLowFreq, RxLowFreq);
                            //Убираем дубдикаты
                            bool flLocal = true;
                            while (FreqHash.Contains(FrequencyString))
                            {
                                if (flLocal == true)
                                {
                                    flLocal = false; //Что бы больше сюда не заходили
                                }
                                FrequencyString += " ";
                            }

                            if (needAdd) // Добавляем
                            {
                                FChannel.CLBAllChannel.Items.Add(FrequencyString);
                            }
                            else
                            {
                                FChannel.CLBSelectChannel.Items.Add(FrequencyString);
                            }

                            RxFreq.Add(RxLowFreq);
                            TxFreq.Add(TxLowFreq);
                            FreqHash.Add(FrequencyString, i);
                            i++;
                        }
                    }



                    if (FChannel.ShowDialog() == DialogResult.OK)
                    {
                        // Отображении окна редактирования и выбора частот
                        TxStationFreq.Clear();
                        RxStationFreq.Clear();

                        for (int j = 0; j < FChannel.CLBSelectChannel.Items.Count; j++)
                        {
                            string SelectedString = FChannel.CLBSelectChannel.Items[j].ToString();
                            int FrequencyIndex = Convert.ToInt32(FreqHash[SelectedString]);

                            RxStationFreq.Add(RxFreq[FrequencyIndex]);
                            TxStationFreq.Add(TxFreq[FrequencyIndex]);

                        }


                        AutoFill(cell, grid);
                        InitLicenceGrid();
                        CheckTechUser();

                        if (IsTechnicalUser == 1)
                            objStation.Put("CUST_TXT8", userTech);
                        else
                            objStation.Put("CUST_TXT8", userTelecom);
                        //Сохраняем План
                        {
                            int planId = IM.NullI;
                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,NAME");
                            r.SetWhere("NAME", IMRecordset.Operation.Like, Standard);
                            try
                            {
                                r.Open();
                                if (!r.IsEOF())
                                    planId = r.GetI("ID");
                            }
                            finally
                            {
                                r.Final();
                            }
                            objStation.Put("PLAN_ID", planId);
                        }

                        Cell RxCell = grid.GetCellFromKey("rxFreq0");
                        strFreqRX = RxCell.Value;
                        Cell TxCell = grid.GetCellFromKey("txFreq0");
                        strFreqTX = TxCell.Value;


                    }
                }
                else
                {
                    Dictionary<int, List<FreqLicence>> val = CLicence.GetLicenceWithFreqs(OwnerID, province, Standard);

                    List<int> LicList = new List<int>();
                    i = 0;
                    foreach (var lic in val)
                    {
                        string licenceName = "";
                        IMRecordset rsLicense = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                        rsLicense.Select("ID,NAME");
                        rsLicense.SetWhere("ID", IMRecordset.Operation.Eq, lic.Key);
                        try
                        {
                            rsLicense.Open();
                            if (!rsLicense.IsEOF())
                            {
                                licenceName = rsLicense.GetS("NAME");
                            }
                        }
                        finally
                        {
                            rsLicense.Final();
                        }
                        //----
                        foreach (var freq in lic.Value)
                        {
                            double TxLowFreq = freq.TxCentralMHz.Round(5);
                            double RxLowFreq = freq.RxCentralMHz.Round(5);
                            //Add frequencies!!! as in Builder                  
                            bool needAdd = true;
                            if (RxStationFreq.Contains(RxLowFreq) && TxStationFreq.Contains(TxLowFreq))
                            {
                                needAdd = false;
                            }
                            string FrequencyString = string.Format("{0} / {1}", TxLowFreq, RxLowFreq);
                            //Убираем дубдикаты
                            bool flLocal = true;
                            while (FreqHash.Contains(FrequencyString))
                            {
                                if (flLocal == true)
                                {
                                    flLocal = false; //Что бы больше сюда не заходили
                                    FrequencyString += " (" + licenceName + ")";
                                }
                                FrequencyString += " ";
                            }

                            if (needAdd) // Добавляем
                            {
                                FChannel.CLBAllChannel.Items.Add(FrequencyString);
                            }
                            else
                            {
                                FChannel.CLBSelectChannel.Items.Add(FrequencyString);
                            }

                            RxFreq.Add(RxLowFreq);
                            TxFreq.Add(TxLowFreq);
                            LicList.Add(lic.Key);
                            FreqHash.Add(FrequencyString, i);
                            i++;
                        }
                    }

                    if (FChannel.ShowDialog() == DialogResult.OK)
                    {
                        TxStationFreq.Clear();
                        RxStationFreq.Clear();
                        licenceListId.Clear();

                        for (int j = 0; j < FChannel.CLBSelectChannel.Items.Count; j++)
                        {
                            string SelectedString = FChannel.CLBSelectChannel.Items[j].ToString();
                            int FrequencyIndex = Convert.ToInt32(FreqHash[SelectedString]);

                            RxStationFreq.Add(RxFreq[FrequencyIndex]);
                            TxStationFreq.Add(TxFreq[FrequencyIndex]);

                            if (!licenceListId.Contains(LicList[FrequencyIndex]))
                                licenceListId.Add(LicList[FrequencyIndex]);
                        }
                        AutoFill(cell, grid);
                        InitLicenceGrid();
                        CheckTechUser();

                        if (IsTechnicalUser == 1)
                            objStation.Put("CUST_TXT8", userTech);
                        else
                            objStation.Put("CUST_TXT8", userTelecom);
                        //Сохраняем План
                        {
                            int planId = IM.NullI;
                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,NAME");
                            r.SetWhere("NAME", IMRecordset.Operation.Like, Standard);
                            try
                            {
                                r.Open();
                                if (!r.IsEOF())
                                    planId = r.GetI("ID");
                            }
                            finally
                            {
                                r.Final();
                            }
                            objStation.Put("PLAN_ID", planId);
                        }

                        Cell RxCell = grid.GetCellFromKey("rxFreq0");
                        strFreqRX = RxCell.Value;
                        Cell TxCell = grid.GetCellFromKey("txFreq0");
                        strFreqTX = TxCell.Value;
                    }

                }

            }
            else if (cell.Key == "asl")
            {
                if (objPosition != null)
                {
                    double lon = objPosition.LonDec;
                    double lat = objPosition.LatDec;

                    if (lon != IM.NullD && lat != IM.NullD)
                    {
                        double asl = IMCalculate.CalcALS(lon, lat, "4DEC");
                        if (asl != IM.NullD)
                        {
                            cell.Value = IM.RoundDeci(asl, 1).ToString();
                            OnCellValidate(cell, grid);
                        }
                    }
                }
            }
            else
            {
                base.OnPressButton(cell, grid);
            }
        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            if (isDestroyed == true)
                return; //Класс уже удален

            if (cell.Key == "standard")
            {
                objStation["NETWORK_IDENT"] = cell.Value;

                string Standard = objStation.GetS("NETWORK_IDENT");

                IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);

                r.Select("ID,NAME,DUPLEX_SPACING");
                r.SetWhere("NAME", IMRecordset.Operation.Like, Standard);
                r.Open();

                Cell fillCell = grid.GetCellFromKey("rxDD0");
                fillCell.Value = "";

                if (!r.IsEOF())
                {
                    List<double> textFreq = new List<double>();
                    double DuplexDiversion = r.GetD("DUPLEX_SPACING");
                    objStation.Put("DUPLEX_SPACING", DuplexDiversion);
                }
                r.Close();
                r.Destroy();

                AutoFill(cell, grid);
            }
            if (cell.Key == "KLon0")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(lon, 0, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (objPosition != null)
                    {
                        objPosition.LongDms = lon;
                        IndicateDifference(cell, objPosition.LonDiffersFromAdm);
                    }
                    Lon = lon;
                }
            }
            else if (cell.Key == "KLat0")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(0, lat, "DMS", "DEC");
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (objPosition != null)
                    {
                        objPosition.LatDms = lat;
                        IndicateDifference(cell, objPosition.LatDiffersFromAdm);
                    }
                    Lat = lat;
                }
            }
            else if ((cell.Key == "ozn") || (cell.Key == "antHeight0") || (cell.Key == "callsign") || (cell.Key == "MMSI") || (cell.Key == "OznachRez"))
            {
                if (cell.Key == "MMSI")
                {
                    MMSI = cell.Value;
                }
                if (cell.Key == "OznachRez")
                {
                    Oznach_Rez = cell.Value;
                }

                HelpFunction.LoadFromCellToDb(cell, objStation);


                if (cell.Key == "antHeight0")
                {
                    double AntHeight = ConvertType.ToDouble(cell, IM.NullD);
                    if (AntHeight < 0)
                        ChangeColor(cell, Colors.badValue);
                }
            }
            else if (cell.Key == "asl")
            {
                Height = ConvertType.ToDouble(cell, IM.NullD);
                if (objPosition != null)
                {
                    objPosition.Asl = Height;
                }
                cell.Value = (Height == IM.NullD) ? "" : Height.ToString("F0");
            }
            else if (cell.Key == "pow0")
            {
                double PwrAnt = 0;
                string cellVal = cell.Value;

                if (!cellVal.Trim().Contains(" ") && Double.TryParse(cellVal, out PwrAnt))
                {
                    double MaxPower = IM.NullD;
                    if (objEquip != null)
                        MaxPower = objEquip.GetD("MAX_POWER");
                    double RecalcMaxPower = Math.Pow(10.0, (MaxPower) / 10.0);
                    double SavedPwrAnt = 10.0 * Math.Log10(PwrAnt);

                    objStation.Put("PWR_ANT", SavedPwrAnt);

                    if (PwrAnt > 0 && (PwrAnt <= RecalcMaxPower) && MaxPower != IM.NullD)
                        cell.BackColor = System.Drawing.Color.White;
                    else
                        cell.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    if (string.IsNullOrEmpty(cell.Value.Trim()))
                    {
                        objStation.Put("PWR_ANT", IM.NullD);
                        cell.BackColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        MessageBox.Show("Введені дані не можуть бути сприйняти як потужнійсть передавача ");

                    }
                    UpdateOneParamGrid(grid.GetCellFromKey("type"), grid);
                }

            }
            else if (cell.Key == "antType0")
            {
                //int index = Convert.ToInt32(cell.Key.Substring(7));
                if ((objAntenna != null) && (cell.Value != objAntenna.GetS("NAME")))
                    objAntenna = null;  //Будем искать новую антенну, так как имя уже не совпадает

                if (objAntenna == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recAnt = new RecordPtr(ICSMTbl.itblAntennaMob, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblAntennaMob, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recAnt.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recAnt.Id > 0)
                    {
                        objAntenna = IMObject.LoadFromDB(recAnt);
                    }
                }

                if (objAntenna != null)
                {
                    objStation["ANT_ID"] = objAntenna.GetI("ID");
                    objStation["POLAR"] = objAntenna["POLARIZATION"];
                    //string s = objStation.GetS("POLAR");
                }

                AutoFill(cell, grid);
            }
            else if (cell.Key == "antHeight0")
            {
                double AntHeight = ConvertType.ToDouble(cell, IM.NullD);
                objStation["AGL"] = AntHeight;
                cell.Value = (AntHeight != IM.NullD) ? AntHeight.ToString("F0") : "";

            }
            else if (cell.Key == "antAz0")
            {
                double Azimuth = ConvertType.ToDouble(cell, IM.NullD);
                HelpFunction.LoadFromCellToDb(cell, objStation);
                cell.Value = (Azimuth != IM.NullD) ? Azimuth.ToString("F2") : "";
                //objStation["AZIMUTH"] = Azimuth;
            }
            else if (cell.Key == "antGain0")
            {
                double AntCoef = ConvertType.ToDouble(cell, IM.NullD);
                HelpFunction.LoadFromCellToDb(cell, objStation);
                cell.Value = (AntCoef != IM.NullD) ? AntCoef.ToString("F1") : "";
            }
            else if (cell.Key == "Class0")
            {
                objStation["CLASS"] = CStationClass.getShortStationClass(cell.Value);
            }
            else if (cell.Key == "user")
            {
                objStation["CUST_TXT8"] = cell.Value;
            }
            else if (cell.Key == "txFreq0")
            {
                if (ValidateFrequencies(cell, grid, true))
                {
                    //HelpFunction.ToString(RxCell, RxStationFreq);

                    strFreqTX = cell.Value;
                    Cell RxCell = grid.GetCellFromKey("rxFreq0");
                    strFreqRX = RxCell.Value;
                }
            }
            else if (cell.Key == "rxFreq0")
            {
                if (ValidateFrequencies(cell, grid, false))
                {
                    strFreqRX = cell.Value;
                    Cell TxCell = grid.GetCellFromKey("txFreq0");
                    strFreqTX = TxCell.Value;
                }
            }
            else if (cell.Key == "fiderLosses0")
            {
                HelpFunction.LoadFromCellToDb(cell, objStation);
                //cell.Value = (objStation.FiderZat != IM.NullD) ? stationList[index].FiderZat.ToString("F2") : "";
            }
        }

        private bool ValidateFrequencies(Cell cell, Grid grid, bool bTx)
        {
            string ErrorFrequenciesString = "";

            if (string.IsNullOrEmpty(cell.Value))
            {
                if (!bTx)
                    RxStationFreq = new List<double>();
                else
                    TxStationFreq = new List<double>();
            }
            else
            {
                int PlanID = IM.NullI;
                List<double> FreqList = ConvertType.ToDoubleList(cell);
                int pairing = IM.NullI;

                IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                try
                {
                    r.Select("ID,NAME,PAIRING");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, objStation.GetS("NETWORK_IDENT"));
                    r.Open();
                    if (!r.IsEOF())
                    {
                        PlanID = r.GetI("ID");
                        pairing = r.GetI("PAIRING");
                    }
                    else
                    {
                        ErrorFrequenciesString = "";
                        MessageBox.Show("Frequency plan is not found");
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }

                List<double> FrequencyList1 = new List<double>();
                List<double> FrequencyList2 = new List<double>();

                foreach (double Frequency in FreqList)
                {
                    bool blFindFreq = false;
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    try
                    {
                        r2.Select("PLAN_ID,PARITY,FREQ,CHANNEL");
                        r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, PlanID);

                        for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                        {
                            double FetchedFrequency = r2.GetD("FREQ");
                            if (Math.Abs(Frequency - FetchedFrequency) < 0.0001)
                            {
                                blFindFreq = true;
                                FrequencyList1.Add(FetchedFrequency);

                                if (pairing != 0)
                                {
                                    IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                    try
                                    {
                                        r3.Select("PLAN_ID,PARITY,FREQ,CHANNEL");
                                        r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, PlanID);
                                        r3.SetWhere("PARITY", IMRecordset.Operation.NotLike, r2.GetS("PARITY"));
                                        r3.SetWhere("CHANNEL", IMRecordset.Operation.Eq, r2.GetS("CHANNEL"));
                                        r3.Open();
                                        if (!r3.IsEOF())
                                        {
                                            FrequencyList2.Add(r3.GetD("FREQ"));
                                        }
                                    }
                                    finally
                                    {
                                        r3.Close();
                                        r3.Destroy();
                                    }
                                }
                                else
                                    FrequencyList2.Add(FetchedFrequency);
                                break;
                            }
                        }
                    }
                    finally
                    {
                        r2.Close();
                        r2.Destroy();
                    }

                    if (!blFindFreq)
                        ErrorFrequenciesString += Frequency.ToString("F6") + ";";
                }

                if (ErrorFrequenciesString != "")
                {
                    MessageBox.Show("Частоту(и) не знайдено в частотному плані: " + ErrorFrequenciesString, CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    if (bTx)
                    {
                        TxStationFreq = FrequencyList1;
                        RxStationFreq = FrequencyList2;
                    }
                    else
                    {
                        RxStationFreq = FrequencyList1;
                    }
                    objStation.Put("PLAN_ID", PlanID);
                }
                if (bTx)
                {
                    Cell RxCell = grid.GetCellFromKey("rxFreq0");
                    HelpFunction.ToString(RxCell, RxStationFreq);
                }
            }
            return true;
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            MobStaTechnologicalApp retStation = new MobStaTechnologicalApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            MobStaTechnologicalApp retStation = newAppl as MobStaTechnologicalApp;
            retStation.CopyFromMe(grid, objStation.GetI("EQUIP_ID"), objStation.GetD("PWR_ANT"),
               objStation.GetS("DESIG_EMISSION"), objStation.GetI("ANT_ID"));
            return retStation;
        }
        //===========================================================
        public void CopyFromMe(Grid grid, int EquipID, double PwrAnt, string DesigEmission, int AntID)
        {
            Cell tmpCell = null;
            objStation.Put("EQUIP_ID", EquipID);
            objStation.Put("PWR_ANT", PwrAnt);
            objStation.Put("DESIG_EMISSION", DesigEmission);
            objStation.Put("ANT_ID", AntID);

            if ((EquipID != 0) || (EquipID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("type");
                objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipPmr, EquipID);
                tmpCell.Value = objEquip.GetS("NAME");
                OnCellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }

            if ((AntID != 0) || (AntID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("antType0");
                objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntennaMob, AntID);
                tmpCell.Value = objAntenna.GetS("NAME");
                OnCellValidate(tmpCell, grid);
            }

            // Мощность
            tmpCell = grid.GetCellFromKey("pow0");
            tmpCell.Value = PwrAnt.ToString();
            OnCellValidate(tmpCell, grid);

            // Мощность
            tmpCell = grid.GetCellFromKey("class");
            tmpCell.Value = DesigEmission;
            OnCellValidate(tmpCell, grid);


            /*// Спутник
            objStation["SAT_NAME"] = SAT_NAME;
            tmpCell = grid.GetCellFromKey("KSatNet");
            CellValidate(tmpCell, grid);
            // Оборудование
            if ((equipID != 0) || (equipID != IM.NullI))
            {
               tmpCell = grid.GetCellFromKey("KName");
               st.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipEsta, equipID);
               tmpCell.Value = st.objEquip.GetS("NAME");
               OnCellValidate(tmpCell, grid);
               AutoFill(tmpCell, grid);
            }
            if ((antID != 0) || (antID != IM.NullI))
            {
               tmpCell = grid.GetCellFromKey("KAntType");
               st.objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntenna, antID);
               tmpCell.Value = st.objAntenna.GetS("NAME");
               OnCellValidate(tmpCell, grid);
            }
            // Мощность
            tmpCell = grid.GetCellFromKey("KPow");
            tmpCell.Value = maxPow.ToString();
            OnCellValidate(tmpCell, grid);
            // !!
            st.NoiseT = NoiseT;
            // Поляризация
            tmpCell = grid.GetCellFromKey("KPolarRx");
            tmpCell.Value = PolarRX;
            OnCellValidate(tmpCell, grid);

            tmpCell = grid.GetCellFromKey("KPolarTx");
            tmpCell.Value = PolarRX;
            OnCellValidate(tmpCell, grid);*/
        }
        //===========================================================
        /// <summary>
        /// Save frequencies for certaint Station.
        /// </summary>
        public void SaveFrequencies()
        {
            List<MobFreq> freqs = new List<MobFreq>();
            int stationId = objStation.GetI("ID");
            int maxCount = Math.Max(TxStationFreq.Count, RxStationFreq.Count);
            for (int indexFreq = 0; indexFreq < maxCount; indexFreq++)
            {
                MobFreq freq = new MobFreq();
                freq.RxMHz = (indexFreq < RxStationFreq.Count) ? RxStationFreq[indexFreq] : IM.NullD;
                freq.TxMHz = (indexFreq < TxStationFreq.Count) ? TxStationFreq[indexFreq] : IM.NullD;
                freqs.Add(freq);
            }
            MobFreqManager.SaveFreq(stationId, MobFreqManager.TypeFreq.MobStation, freqs.ToArray());
        }

        public override void Coordination()
        {
            base.Coordination();
            CoordForm form = null;
            PositionState pos = new PositionState();
            if (objPosition != null)
                pos.LoadStatePosition(objPosition.Id, objPosition.TableName);
            if (pos.Id != IM.NullI)
                form = new CoordForm(pos, null, TableName, applID);
            form.ShowDialog();
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            int PosID = 0;
            if (objPosition != null)
            {
                //objPosition.Save();
                PosID = objPosition.Id;
                objStation.Put("POS_ID", PosID);
            }

            double PwrAnt = objStation.GetD("PWR_ANT");
            double Gain = objStation.GetD("GAIN");
            double TxLosses = objStation.GetD("TX_LOSSES");
            double TxAddLosses = objStation.GetD("TX_ADDLOSSES");

            if (PwrAnt == IM.NullD)
                PwrAnt = 0;

            if (Gain == IM.NullD)
                Gain = 0;

            if (TxLosses == IM.NullD)
                TxLosses = 0;

            if (TxAddLosses == IM.NullD)
                TxAddLosses = 0;

            objStation.Put("STANDARD", radioTech);
            if (objAntenna != null)
                objAntenna.GetS("GAIN_TYPE");
            objStation.Put("ADM", "UKR");
            objStation.Put("CUST_CHB1", IsTechnicalUser);
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус

            // Сохраняем "Особливі умови"
            objStation["CUST_TXT16"] = SCDozvil;
            objStation["CUST_TXT17"] = SCVisnovok;
            objStation["CUST_TXT18"] = SCNote;

            // только для технологии РБСС
            if (Standard == CRadioTech.RBSS)
            {
                objStation["CUST_TXT6"] = MMSI;
                objStation["CUST_TXT10"] = Oznach_Rez;
            }

            string GainType = objStation.GetS("T_GAIN_TYPE");

            if (string.IsNullOrEmpty(GainType) && objAntenna != null)
                GainType = objAntenna.GetS("GAIN_TYPE");
            objStation.Put("T_GAIN_TYPE", GainType);

            if (GainType == "I")
                objStation.Put("POWER", PwrAnt + Gain - TxLosses - TxAddLosses);

            if (GainType == "D")
                objStation.Put("POWER", PwrAnt + Gain - TxLosses - TxAddLosses + 2.16);

            string ss = "";
            if (objAntenna != null)
                ss = objAntenna.GetS("GAIN_TYPE");

            if (newStation)
            {
                objStation.Put("CUST_TXT1", NumberOut);
                objStation.Put("CUST_DAT1", DateOut);
            }

            CJournal.CheckTable(recordID.Table, recordID.Id, objStation);

            objStation.SaveToDB();
            SaveFrequencies();

            {
                IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                r.Select("ID,CUST_TXT7,CUST_TXT13");
                r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        // Частоты на передачц (TX)
                        r.Put("CUST_TXT7", strFreqTX.Replace(',', '.'));
                        // Частоты на прием (RX)
                        r.Put("CUST_TXT13", strFreqRX.Replace(',', '.'));
                        r.Update();
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            SaveLicence();
            return true;
        }

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "addr"))// && (objPosition != null))
                {
                    if (objPosition != null)
                    {
                        int posID = objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionWim, posID, (IM.TableRight(objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        // position may appear be  changed
                        //todo: refactor this in all forms to some general "ReloadPosition()" method in base class
                        objPosition.LoadStatePosition(objPosition.Id, objPosition.TableName);
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon0"), gridParam);
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "antType0") && (objAntenna != null))
                {
                    int AntID = objAntenna.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(objAntenna.GetS("TABLE_NAME"), AntID);
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "type") && (objEquip != null))
                {
                    int EquipID = objEquip.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(objEquip.GetS("TABLE_NAME"), EquipID);
                    rcPtr.UserEdit();
                }
                else if (cell.Key == "obj0")
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation, ID);
                    recStation.UserEdit();
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CUST_TXT1,CUST_DAT1");
            r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("CUST_TXT1", NumberOut);
                    r.Put("CUST_DAT1", DateOut);
                    r.Update();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        public double GetPower()
        {
            double maxPower = objStation.GetD("PWR_ANT");
            if (maxPower != IM.NullD)
            {
                maxPower = Math.Pow(10.0, maxPower / 10.0);
            }
            return maxPower;
        }

        public string GetClass()
        {
            return objStation.GetS("CLASS");
        }

        //===========================================================
        /// <summary>
        /// Возвращет тип статьи УКХ и значение мощности/количества РЕЗ или ничего
        /// </summary>
        public LimitType GetAppTypeAndLimitValue(out double value)
        {
            LimitType type = LimitType.noLimit;
            value = IM.NullD;

            string planName = "";
            {
                IMRecordset rsPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsPlan.Select("ID,NAME");
                    rsPlan.SetWhere("ID", IMRecordset.Operation.Eq, objStation.GetI("PLAN_ID"));
                    rsPlan.Open();
                    if (!rsPlan.IsEOF())
                        planName = rsPlan.GetS("NAME");
                }
                finally
                {
                    rsPlan.Final();
                }
            }

            if (planName.ToUpper().Contains("АЛТАЙ"))
                return type;

            if (objStation.GetI("CUST_CHB1") != 1)
            {
                type = LimitType.countLimit;
                List<int> lstNet = new List<int>();
                const int maxCount = 1000000;
                value = maxCount;
                {
                    //вытаскиваем сети
                    IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                    try
                    {
                        rsAppl.Select("ID");
                        rsAppl.Select("NetSector1.NET_ID");
                        rsAppl.Select("NetSector2.NET_ID");
                        rsAppl.Select("NetSector3.NET_ID");
                        rsAppl.Select("NetSector4.NET_ID");
                        rsAppl.Select("NetSector5.NET_ID");
                        rsAppl.Select("NetSector6.NET_ID");
                        rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, ApplID);
                        for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                        {
                            for (int i = 1; i < 7; i++)
                            {
                                int netIdLocal = rsAppl.GetI(string.Format("NetSector{0}.NET_ID", i));
                                if ((netIdLocal != IM.NullI) && (lstNet.Contains(netIdLocal) == false))
                                    lstNet.Add(netIdLocal);
                            }
                        }
                    }
                    finally
                    {
                        rsAppl.Final();
                    }
                }
                foreach (int idNet in lstNet)
                {
                    bool isUsed = false;
                    int countAll = 0;
                    for (int i = 1; i < 7; i++)
                    {
                        IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rsAppl.Select("ID");
                            rsAppl.SetWhere(string.Format("NetSector{0}.NET_ID", i), IMRecordset.Operation.Eq, idNet);
                            rsAppl.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                            for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                            {
                                countAll++;
                                isUsed = true;
                            }
                        }
                        finally
                        {
                            rsAppl.Final();
                        }
                    }
                    {
                        IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rsAppl.Select("ID");
                            rsAppl.SetWhere("Abonent.NET_ID", IMRecordset.Operation.Eq, idNet);
                            rsAppl.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                            for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                            {
                                countAll++;
                                isUsed = true;
                            }
                        }
                        finally
                        {
                            rsAppl.Final();
                        }
                    }
                    if ((isUsed) && (value > countAll))
                        value = countAll;
                }
                if (value == maxCount)
                    value = 1;
                //value = 1 + (objStation.GetI("CUST_NBR5") == IM.NullI ? 0 : objStation.GetI("CUST_NBR5"));
            }
            else
            {
                type = LimitType.powerLimit;
                value = GetPower();
            }
            return type;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, string radioTech, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select(string.Format("ID,CLASS,CUST_NBR5,PLAN_ID,CUST_CHB1,Plan.NAME,Position.REMARK"));
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    if (radioTech == CRadioTech.KX)
                    {
                        Dictionary<double, double> freqs = new Dictionary<double, double>();

                        IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                        recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                        recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                        try
                        {
                            recMobStaFreqs2.Open();
                            while (!recMobStaFreqs2.IsEOF())
                            {
                                double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                                double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                                if (TxFreq != IM.NullD && RxFreq != IM.NullD)
                                {
                                    if (!freqs.Contains(new KeyValuePair<double, double>(TxFreq, RxFreq)))
                                    {
                                        count++;
                                        freqs.Add(TxFreq, RxFreq);
                                    }
                                }
                                recMobStaFreqs2.MoveNext();
                            }
                        }
                        finally
                        {
                            recMobStaFreqs2.Close();
                            recMobStaFreqs2.Destroy();
                        }
                        return count;
                    }
                    else if (radioTech == CRadioTech.TETRA || radioTech == CRadioTech.TRUNK)
                    {
                        List<string> freqs = new List<string>();

                        IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                        recMobStaFreqs2.Select("ID,STA_ID,ChannelTx.CHANNEL");
                        recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                        try
                        {
                            recMobStaFreqs2.Open();
                            while (!recMobStaFreqs2.IsEOF())
                            {
                                string TxFreq = recMobStaFreqs2.GetS("ChannelTx.CHANNEL");
                                if (TxFreq != "")
                                {
                                    if (!freqs.Contains(TxFreq))
                                    {
                                        count++;
                                        freqs.Add(TxFreq);
                                    }
                                }
                                recMobStaFreqs2.MoveNext();
                            }
                        }
                        finally
                        {
                            recMobStaFreqs2.Close();
                            recMobStaFreqs2.Destroy();
                        }
                    }
                    else if (radioTech == CRadioTech.RBSS)
                    {
                        if ((rs.GetS("CLASS") == "FC") || (rs.GetS("CLASS") == "FB"))
                        {
                            List<double> freqs = new List<double>();
                            IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                            recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                            try
                            {
                                recMobStaFreqs2.Open();
                                while (!recMobStaFreqs2.IsEOF())
                                {
                                    double txFreq = recMobStaFreqs2.GetD("TX_FREQ").Round(5);
                                    double rxFreq = recMobStaFreqs2.GetD("RX_FREQ").Round(5);

                                    if ((ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(txFreq, 6))) ||
                                    (ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(rxFreq, 6))))
                                    {
                                        if ((ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(txFreq, 6))))
                                        {
                                            recMobStaFreqs2.MoveNext();
                                            continue;
                                        }
                                        else if (ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(rxFreq, 6)))
                                        {
                                            recMobStaFreqs2.MoveNext();
                                            continue;
                                        }
                                    }

                                    if (txFreq != IM.NullD)
                                    {
                                        if (!freqs.Contains(txFreq))
                                            freqs.Add(txFreq);
                                    }
                                    if (rxFreq != IM.NullD)
                                    {
                                        if (!freqs.Contains(rxFreq))
                                            freqs.Add(rxFreq);
                                    }
                                    recMobStaFreqs2.MoveNext();
                                }
                            }
                            finally
                            {
                                recMobStaFreqs2.Close();
                                recMobStaFreqs2.Destroy();
                            }
                            count = freqs.Count();
                        }
                        else if (rs.GetS("CLASS") == "MS")
                        {
                            count = 1;
                        }
                    }
                    else if (radioTech == CRadioTech.YKX)
                    {
                        string str = rs.GetS("Plan.NAME").ToUpper();
                        if (rs.GetS("Plan.NAME").ToUpper().Contains("АЛТАЙ"))
                        {
                            List<string> freqs = new List<string>();

                            IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                            recMobStaFreqs2.Select("ID,STA_ID,ChannelTx.CHANNEL");
                            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                            try
                            {
                                recMobStaFreqs2.Open();
                                while (!recMobStaFreqs2.IsEOF())
                                {
                                    string TxFreq = recMobStaFreqs2.GetS("ChannelTx.CHANNEL");
                                    if (TxFreq != "")
                                    {
                                        if (!freqs.Contains(TxFreq))
                                        {
                                            count++;
                                            freqs.Add(TxFreq);
                                        }
                                    }
                                    recMobStaFreqs2.MoveNext();
                                }
                            }
                            finally
                            {
                                recMobStaFreqs2.Close();
                                recMobStaFreqs2.Destroy();
                            }
                        }
                        else
                        {
                            if (rs.GetI("CUST_CHB1") != 1)
                            {
                                List<double> freqs = new List<double>();
                                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                                recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                                try
                                {
                                    recMobStaFreqs2.Open();
                                    while (!recMobStaFreqs2.IsEOF())
                                    {
                                        double txFreq = recMobStaFreqs2.GetD("TX_FREQ").Round(5);
                                        double rxFreq = recMobStaFreqs2.GetD("RX_FREQ").Round(5);
                                        if (txFreq != IM.NullD)
                                        {
                                            if (!freqs.Contains(txFreq))
                                                freqs.Add(txFreq);
                                        }
                                        if (rxFreq != IM.NullD)
                                        {
                                            if (!freqs.Contains(rxFreq))
                                                freqs.Add(rxFreq);
                                        }
                                        recMobStaFreqs2.MoveNext();
                                    }
                                }
                                finally
                                {
                                    recMobStaFreqs2.Close();
                                    recMobStaFreqs2.Destroy();
                                }
                                count = freqs.Count();
                            }
                            else
                            {
                                List<double> freqs = new List<double>();
                                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                                recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                                try
                                {
                                    recMobStaFreqs2.Open();
                                    while (!recMobStaFreqs2.IsEOF())
                                    {
                                        double txFreq = recMobStaFreqs2.GetD("TX_FREQ").Round(5);
                                        double rxFreq = recMobStaFreqs2.GetD("RX_FREQ").Round(5);
                                        if (txFreq != IM.NullD)
                                        {
                                            if (!freqs.Contains(txFreq))
                                                freqs.Add(txFreq);
                                        }
                                        if (rxFreq != IM.NullD)
                                        {
                                            if (!freqs.Contains(rxFreq))
                                                freqs.Add(rxFreq);
                                        }
                                        recMobStaFreqs2.MoveNext();
                                    }
                                }
                                finally
                                {
                                    recMobStaFreqs2.Close();
                                    recMobStaFreqs2.Destroy();
                                }
                                count = freqs.Count;
                                //Тепер не враховуем кількість абонентів
                                //int stCount = 1 + (rs.GetI("CUST_NBR5") == IM.NullI ? 0 : rs.GetI("CUST_NBR5"));
                                //count *= stCount;
                            }
                        }
                    }
                    else
                        count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
        //===========================================================
        /// <summary>
        /// Возвращет количество работ по заявке в ДРВ от УРЧМ
        /// </summary>
        public override int GetURCMWorksCount()
        {

            if (this.radioTech == CRadioTech.KX)
            {
                int ID = objStation.GetI("ID");

                int count = 0;

                Dictionary<double, double> freqs = new Dictionary<double, double>();

                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                try
                {
                    recMobStaFreqs2.Open();
                    while (!recMobStaFreqs2.IsEOF())
                    {
                        double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                        double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                        if (TxFreq != IM.NullD && RxFreq != IM.NullD)
                        {
                            if (!freqs.Contains(new KeyValuePair<double, double>(TxFreq, RxFreq)))
                            {
                                count++;
                                freqs.Add(TxFreq, RxFreq);
                            }
                        }
                        recMobStaFreqs2.MoveNext();
                    }
                }
                finally
                {
                    recMobStaFreqs2.Close();
                    recMobStaFreqs2.Destroy();
                }
                return count;
            }
            else if (this.radioTech == CRadioTech.TETRA || this.radioTech == CRadioTech.TRUNK)
            {
                int ID = objStation.GetI("ID");

                int count = 0;

                List<string> freqs = new List<string>();

                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                recMobStaFreqs2.Select("ID,STA_ID,ChannelTx.CHANNEL");
                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                try
                {
                    recMobStaFreqs2.Open();
                    while (!recMobStaFreqs2.IsEOF())
                    {
                        string TxFreq = recMobStaFreqs2.GetS("ChannelTx.CHANNEL");
                        if (TxFreq != "")
                        {
                            if (!freqs.Contains(TxFreq))
                            {
                                count++;
                                freqs.Add(TxFreq);
                            }
                        }
                        recMobStaFreqs2.MoveNext();
                    }
                }
                finally
                {
                    recMobStaFreqs2.Close();
                    recMobStaFreqs2.Destroy();
                }
                return count;
            }
            else if (this.radioTech == CRadioTech.RBSS)
            {
                int ID = objStation.GetI("ID");

                int count = 0;
                if (objStation.GetS("CLASS") == "FC")
                {
                    Dictionary<double, double> freqs = new Dictionary<double, double>();

                    IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                    recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                    recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                    try
                    {
                        recMobStaFreqs2.Open();
                        while (!recMobStaFreqs2.IsEOF())
                        {
                            double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                            double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                            if (TxFreq != IM.NullD && RxFreq != IM.NullD)
                            {
                                if ((ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(TxFreq, 6))) ||
                                    (ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(RxFreq, 6))))
                                    {
                                        if ((ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(TxFreq, 6))))
                                        {
                                            recMobStaFreqs2.MoveNext();
                                            continue;
                                        }
                                        else if (ArticleLib.Abonent.CheckRemoveFreq(IM.RoundDeci(RxFreq, 6)))
                                        {
                                            recMobStaFreqs2.MoveNext();
                                            continue;
                                        }
                                    }

                                if (!freqs.Contains(new KeyValuePair<double, double>(TxFreq, RxFreq)))
                                {
                                    count++;
                                    freqs.Add(TxFreq, RxFreq);
                                }
                            }
                            recMobStaFreqs2.MoveNext();
                        }
                    }
                    finally
                    {
                        recMobStaFreqs2.Close();
                        recMobStaFreqs2.Destroy();
                    }
                }
                else if (objStation.GetS("CLASS") == "MS")
                {
                    count = 1;
                }
                return count;
            }
            else if (this.radioTech == CRadioTech.YKX)
            {
                int ID = objStation.GetI("ID");

                int count = 0;

                //TODO бред Altay
                if (objStation.GetI("PLAN_ID") == 1419)
                {
                    List<string> freqs = new List<string>();

                    IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                    recMobStaFreqs2.Select("ID,STA_ID,ChannelTx.CHANNEL");
                    recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                    try
                    {
                        recMobStaFreqs2.Open();
                        while (!recMobStaFreqs2.IsEOF())
                        {
                            string TxFreq = recMobStaFreqs2.GetS("ChannelTx.CHANNEL");
                            if (TxFreq != "")
                            {
                                if (!freqs.Contains(TxFreq))
                                {
                                    count++;
                                    freqs.Add(TxFreq);
                                }
                            }
                            recMobStaFreqs2.MoveNext();
                        }
                    }
                    finally
                    {
                        recMobStaFreqs2.Close();
                        recMobStaFreqs2.Destroy();
                    }
                    return count;
                }
                else
                {
                    //#5981
                    //if (objStation.GetI("CUST_CHB1") != 1)
                    {
                        //считаем кол-во уникальных частот
                        List<double> freqs = new List<double>();

                        IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                        recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                        recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                        try
                        {
                            for (recMobStaFreqs2.Open(); !recMobStaFreqs2.IsEOF(); recMobStaFreqs2.MoveNext())
                            {
                                double txFreq = recMobStaFreqs2.GetD("TX_FREQ");
                                double rxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                                if ((txFreq != IM.NullD) && (freqs.Contains(txFreq) == false))
                                    freqs.Add(txFreq);
                                if ((rxFreq != IM.NullD) && (freqs.Contains(rxFreq) == false))
                                    freqs.Add(rxFreq);
                            }
                        }
                        finally
                        {
                            recMobStaFreqs2.Final();
                        }
                        count = freqs.Count;
                    }
                    //else
                    //{
                    //    Dictionary<double, double> freqs = new Dictionary<double, double>();

                    //    IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                    //    recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                    //    recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                    //    try
                    //    {
                    //        recMobStaFreqs2.Open();
                    //        while (!recMobStaFreqs2.IsEOF())
                    //        {
                    //            double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                    //            double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                    //            if (TxFreq != IM.NullD && RxFreq != IM.NullD)
                    //            {
                    //                if (!freqs.Contains(new KeyValuePair<double, double>(TxFreq, RxFreq)))
                    //                {
                    //                    count++;
                    //                    freqs.Add(TxFreq, RxFreq);
                    //                }
                    //            }
                    //            recMobStaFreqs2.MoveNext();
                    //        }
                    //    }
                    //    finally
                    //    {
                    //        recMobStaFreqs2.Close();
                    //        recMobStaFreqs2.Destroy();
                    //    }
                    //    //#ХХХ
                    //    //int stCount = 1 + (objStation.GetI("CUST_NBR5") == IM.NullI ? 0 : objStation.GetI("CUST_NBR5"));
                    //    //count *= stCount;

                    //}
                    return count;
                }
            }
            return 1;
        }
    }
}
