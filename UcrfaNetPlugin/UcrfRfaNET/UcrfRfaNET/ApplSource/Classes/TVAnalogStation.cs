﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class TvAnalogStation : ERPStation
    {        
        public Antenna TvStationAntena { get; set; }
        public TvAnalogEquipment TvStationEquipment { get; set; }

        public Dictionary<String, String> PolDict = new Dictionary<String, String>();
        public Dictionary<String, String> OrientDict = new Dictionary<String, String>();
        public Dictionary<String, String> InternationalDict = new Dictionary<String, String>();
        public Dictionary<String, String> SystemDict = new Dictionary<String, String>();
        public Dictionary<String, String> HomeDict = new Dictionary<String, String>();

        public List<double> FreqVideoList = new List<double>();
        public List<string> ShiftList = new List<string>();
        public List<String> StandartList = new List<String>();
        public List<String> ChannelList = new List<String>();

        public string NumberREZ { get; set; }
        public string NameOstov { get; set; }

        public int SiteId { get; set; }

        public string Standart { get; set; }
        public string Systems { get; set; }

        public string Call { get; set; }

        private Power powA = new Power();

        public Power PowVideo
        {
            get { return _power; }
        }

        public Power PowAudio
        {
            get { return powA; }
        }

        public int ChannelId { get; set; }
        public string EmiVideo { get; set; }
        public string EmiAudio { get; set; }

        public double Bandwith { get; set; }

        public double Height { get; set; }
        public double MaxHeight { get; set; }

        public string homeState { get; set; }
        public string intState { get; set; }
        public string tvChannel { get; set; }
        public double freqVid { get; set; }
        public double freqAud { get; set; }
        public double NesivFreq { get; set; }
        public List<double> Data { get; set; }
        public List<double> DiagDbl { get; set; }
        public int ObjRchp { get; set; }

        public static readonly string TableName = ICSMTbl.itblTV;

        public TvAnalogStation()
        {            
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,SITE_ID,EQUIP_ID,PWR_ANT,PWR_RATIO_1,,DESIG_EM_V,DESIG_EM_S1,PLAN_ID,CUST_TXT7");
                r.Select("OFFSET_V_KHZ,GAIN,DIAGH,DIAGV,POLARIZATION,ERP_H,ERP_V,COLOUR_SYSTEM,TVSYS_CODE,AGL");
                r.Select("DESIG_EM,BW,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();

                    r.Put("ID", Id);
                    if (Position.TableName != ICSMTbl.itblPositionBro)
                    {
                        Position.Id = IM.NullI;
                        Position.TableName = ICSMTbl.itblPositionBro;
                        Position.SaveToBD();
                        SiteId = Position.Id;
                    }
                    //else
                    //{
                    //    SiteId = Position.Id;
                    //    Position.SaveToBD();
                    //}
                    r.Put("AGL", Height);
                    r.Put("SITE_ID", Position.Id);
                    r.Put("EQUIP_ID", TvStationEquipment.Id);
                    r.Put("DESIG_EM_V", TvStationEquipment.EVideo);
                    r.Put("DESIG_EM_S1", TvStationEquipment.EAudio);

                    r.Put("PLAN_ID", ChannelId);
                    r.Put("OFFSET_V_KHZ", NesivFreq);
                    r.Put("POLARIZATION", Polarization);
                    r.Put("TVSYS_CODE", Standart);

                    r.Put("BW", TvStationEquipment.BandWidth * 1000);
                    r.Put("DESIG_EM", TvStationEquipment.DesigEmission);

                    r.Put("COLOUR_SYSTEM", Systems);

                    double pW = PowVideo[PowerUnits.W];
                    double pA = PowAudio[PowerUnits.W];
                    r.Put("PWR_ANT", PowVideo[PowerUnits.dBW]);
                    double ratio;
                    if (pA != IM.NullD && pA!=0)
                        ratio = pW / pA;
                    else
                        ratio = 0;
                    r.Put("PWR_RATIO_1", ratio);

                    r.Put("POLARIZATION", Polarization);

                    if (Polarization != "V")
                    {
                        r.Put("DIAGH", HorizontalDiagramm.DiagrammAsString);
                        r.Put("ERP_H", Erp.Horizontal[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGH", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    if (Polarization != "H")
                    {
                        r.Put("DIAGV", VerticalDiagramm.DiagrammAsString);
                        r.Put("ERP_V", Erp.Vertical[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGV", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    r.Put("ID", ObjRchp);
                    r.Update();
                }                

                IMRecordset channelImRecordset = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadWrite);
                try
                {
                    channelImRecordset.Select("FREQ,PLAN_ID,BANDWIDTH,CHANNEL");
                    channelImRecordset.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, ChannelId);
                    channelImRecordset.Open();
                    if (!channelImRecordset.IsEOF())
                    {
                        channelImRecordset.Edit();
                        channelImRecordset.Put("BANDWIDTH", Bandwith * 1000);
                        channelImRecordset.Put("CHANNEL", tvChannel);
                        channelImRecordset.Update();
                    }
                }
                finally
                {
                    if (channelImRecordset.IsOpen())
                        channelImRecordset.Close();
                    channelImRecordset.Destroy();
                }
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);

            IMRecordset renameStatRec = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            renameStatRec.Select("REC_TAB,REC_ID,PATH");
            renameStatRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            renameStatRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);

            try
            {
                for (renameStatRec.Open(); !renameStatRec.IsEOF(); renameStatRec.MoveNext())
                {
                    renameStatRec.Edit();
                    renameStatRec.Put("REC_TAB", Position.TableName);
                    renameStatRec.Put("REC_ID", Position.Id);
                    renameStatRec.Update();
                }
            }
            finally
            {
                if (renameStatRec.IsOpen())
                    renameStatRec.Close();
                renameStatRec.Destroy();
            }
        }

        public override void Load()
        {

            TvStationAntena = new MobStationAntenna();
            TvStationAntena.TableName = ICSMTbl.itblAntennaBro;

            TvStationEquipment = new TvAnalogEquipment();
            TvStationEquipment.TableName = ICSMTbl.itblEquipBro;

            FillCombo();

            IMRecordset r = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadOnly);
            try
            {
                // Создать новые объекты
                HorizontalDiagramm = new GainDiagramm();
                VerticalDiagramm = new GainDiagramm();
                Erp = new ERP();
                Position = new PositionState();


                r.Select("ID,SITE_ID,EQUIP_ID,PWR_ANT,PWR_RATIO_1,DESIG_EM_V,DESIG_EM_S1,PLAN_ID,CUST_TXT13,DESIG_EM");
                r.Select("CUST_NBR1,CUST_NBR2,POLARIZATION,AGL,EFHGT_MAX,ERP_H,ERP_V,DIAGH,DIAGV,GAIN,CUST_TXT4");
                r.Select("CUST_TXT5,FREQ_V_CARR,DELTA_F_SND1,OFFSET_V_KHZ,COLOUR_SYSTEM,TVSYS_CODE,CUST_TXT7,BW,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");

                    SiteId = r.GetI("SITE_ID");
                    TvStationEquipment.Id = r.GetI("EQUIP_ID");

                    if (TvStationEquipment.Id != IM.NullI)
                        TvStationEquipment.Load();

                    TvStationEquipment.EVideo= r.GetS("DESIG_EM_V");
                    TvStationEquipment.EAudio = r.GetS("DESIG_EM_S1");

                    PowVideo[PowerUnits.dBW] = r.GetD("PWR_ANT");

                    double ratio = r.GetD("PWR_RATIO_1");
                    powA[PowerUnits.W] = PowVideo[PowerUnits.W] / ratio;

                    Standart = r.GetS("TVSYS_CODE");
                    Systems = r.GetS("COLOUR_SYSTEM");
                    Call = r.GetS("NAME");
                    ChannelId = r.GetI("PLAN_ID");
                    FeederLength = r.GetD("CUST_NBR1");
                    FeederLosses = r.GetD("CUST_NBR2");
                    NumberREZ = r.GetS("CUST_TXT7");
                    TvStationEquipment.BandWidth = r.GetD("BW") / 1000;
                    TvStationEquipment.DesigEmission = r.GetS("DESIG_EM");
                    Polarization = r.GetS("POLARIZATION");
                    Height = r.GetD("AGL");
                    MaxHeight = r.GetD("EFHGT_MAX");
                    NesivFreq = r.GetD("OFFSET_V_KHZ");
                    //Висновок
                    Finding = r.GetS("CUST_TXT13");
                    ObjRchp = r.GetI("ID");

                    Erp.Horizontal[PowerUnits.dBW] = r.GetD("ERP_H");
                    Erp.Vertical[PowerUnits.dBW] = r.GetD("ERP_V");
                    HorizontalDiagramm.DiagrammAsString = r.GetS("DIAGH");
                    VerticalDiagramm.DiagrammAsString = r.GetS("DIAGV");
                    HorizontalDiagramm.Gain = r.GetD("GAIN");
                    VerticalDiagramm.Gain = r.GetD("GAIN");
                    _gain = r.GetD("GAIN");

                    homeState = r.GetS("CUST_TXT4");
                    intState = r.GetS("CUST_TXT5");
                    freqVid = r.GetD("FREQ_V_CARR");
                    freqAud = r.GetD("DELTA_F_SND1") + freqVid;
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            Position = new PositionState();
            Position.LoadStatePosition(SiteId, ICSMTbl.itblPositionBro);

            IMRecordset channelImRecordset = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
            try
            {
                channelImRecordset.Select("PLAN_ID,BANDWIDTH,CHANNEL");
                channelImRecordset.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, ChannelId);
                channelImRecordset.Open();
                if (!channelImRecordset.IsEOF())
                {
                    Bandwith = channelImRecordset.GetD("BANDWIDTH") / 1000;
                    tvChannel = channelImRecordset.GetS("CHANNEL");
                }
            }
            finally
            {
                if (channelImRecordset.IsOpen())
                    channelImRecordset.Close();
                channelImRecordset.Destroy();
            }

            if (ChannelId != IM.NullI)
            {
                IMRecordset r1 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                r1.Select("PLAN_ID,CHANNEL,FREQ,OFFSET,BANDWIDTH");
                r1.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, ChannelId);
                r1.OrderBy("CHANNEL", OrderDirection.Ascending);
                for (r1.Open(); !r1.IsEOF(); r1.MoveNext())
                    ChannelList.Add(r1.GetS("CHANNEL"));
                if (r1.IsOpen())
                    r1.Close();
                r1.Destroy();
                int IndexId = ChannelList.IndexOf(tvChannel);
            }
            else
            {
                StringBuilder idChannel = new StringBuilder();
                IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                r2.Select("ID,COD");
                r2.SetWhere("COD", IMRecordset.Operation.Like, "АТМ4");
                for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                {
                    idChannel.Append(',');
                    idChannel.Append(r2.GetI("ID").ToString());
                }
                r2.Close();
                r2.Destroy();

                if (idChannel.Length == 0)
                    return;

                idChannel = idChannel.Remove(0, 1);

                r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                r2.Select("PLAN_ID,CHANNEL,FREQ,OFFSET,BANDWIDTH");
                string strPlan = "[PLAN_ID] in " + "(" + idChannel + ")";
                r2.SetAdditional(strPlan);
                r2.OrderBy("CHANNEL", OrderDirection.Ascending);
                for (r2.Open(); !r2.IsEOF(); r2.MoveNext())
                    ChannelList.Add(r2.GetS("CHANNEL"));
                int IndexId = ChannelList.IndexOf(tvChannel);
            }
        }

        private void FillCombo()
        {
            // fills Поляризація
            PolDict.Add("V", "Вертикальна");
            PolDict.Add("H", "Горизонтальна");
            PolDict.Add("M", "Змішана");

            // fills Спрямованість антени 
            OrientDict.Add("D", "спрямована");
            OrientDict.Add("ND", "неспрямована");

            // fills Зміщення несівної частоти, кГц
            ShiftList.Add("0,0");
            ShiftList.Add("1,302");
            ShiftList.Add("-1,302");
            ShiftList.Add("2,604");
            ShiftList.Add("-2,604");
            ShiftList.Add("3,906");
            ShiftList.Add("-3,906");
            ShiftList.Add("5,208");
            ShiftList.Add("-5,208");
            ShiftList.Add("6,51");
            ShiftList.Add("-6,51");
            ShiftList.Add("7,812");
            ShiftList.Add("-7,812");
            ShiftList.Add("9,115");
            ShiftList.Add("-9,115");
            ShiftList.Add("10,417");
            ShiftList.Add("-10,417");
            ShiftList.Add("11,719");
            ShiftList.Add("-11,719");
            ShiftList.Add("13,021");
            ShiftList.Add("-13,021");
            ShiftList.Add("14,323");
            ShiftList.Add("-14,323");
            ShiftList.Add("15,625");
            ShiftList.Add("-15,625");
            ShiftList.Add("16,927");
            ShiftList.Add("-16,927");
            ShiftList.Add("18,229");
            ShiftList.Add("-18,229");
            ShiftList.Add("19,531");
            ShiftList.Add("-19,531");
            ShiftList.Add("20,833");
            ShiftList.Add("-20,833");
            ShiftList.Add("22,135");
            ShiftList.Add("-22,135");
            ShiftList.Add("23,437");
            ShiftList.Add("-23,437");
            ShiftList.Add("24,71");
            ShiftList.Add("-24,71");
            ShiftList.Add("26,042");
            ShiftList.Add("-26,042");


            // fills Внутрішній стан 
            HomeDict.Clear();
            HomeDict.Add("1", "Рассматриваемый");
            HomeDict.Add("2", "Разрешенный");
            HomeDict.Add("3", "Действующий");
            HomeDict.Add("4", "Новое сост.8");
            HomeDict.Add("5", "Новое сост.2");
            HomeDict.Add("6", "Новое сост.3");
            HomeDict.Add("7", "Столб");
            HomeDict.Add("8", "Рекоммендация");
            HomeDict.Add("9", "Уточнение хар-к");
            HomeDict.Add("10", "Планируемый");
            HomeDict.Add("33", "BR IFIC ADD");
            HomeDict.Add("34", "BR IFIC REC");
            HomeDict.Add("35", "BR IFIC SUP");
            HomeDict.Add("36", "BR IFIC MOD");
            HomeDict.Add("38", "Geneva 06");

            // Міжнародний стан
            InternationalDict.Clear();
            InternationalDict.Add("A", " Скоорд.");
            InternationalDict.Add("B", "Берлин 59");
            InternationalDict.Add("C", "Чужой скоорд.");
            InternationalDict.Add("D", "Удаленный, был.");
            InternationalDict.Add("E", "Цифра, планируем.");
            InternationalDict.Add("F", "Прошел коорд.");
            InternationalDict.Add("G", "Женева 84");
            InternationalDict.Add("G06", "GENEVA 2006");
            InternationalDict.Add("H", "Цифра, план Честер");
            InternationalDict.Add("I", "Честер");
            InternationalDict.Add("K", "На коорд.");
            InternationalDict.Add("L", "Цифра, удаленный");
            InternationalDict.Add("M", " Цифра, не надо коорд.");
            InternationalDict.Add("N", "Не надо коорд.");
            InternationalDict.Add("O", "Планируемый");
            InternationalDict.Add("P", "Под крышей");
            InternationalDict.Add("Q", "Цифра, регистр.");
            InternationalDict.Add("R", " Регистр");
            InternationalDict.Add("S", "Стокгольм");
            InternationalDict.Add("T", "Цифра, скоорд.");
            InternationalDict.Add("TP", "Цифра, скоорд. на пп");
            InternationalDict.Add("U", "Цифра на коорд.");
            InternationalDict.Add("Y", "Цифра, нескоорд.");
            InternationalDict.Add("Z", "Не скоорд.");

            // fills Стандарт
            StandartList.Add("B");
            StandartList.Add("D");
            StandartList.Add("G");
            StandartList.Add("H");
            StandartList.Add("I");
            StandartList.Add("K");
            StandartList.Add("K1");
            StandartList.Add("L");
            StandartList.Add("M");
            StandartList.Add("N");
            StandartList.Add("T1");
            StandartList.Add("L1");
            StandartList.Add("B1");

            // fills system
            SystemDict.Add("P", "PAL");
            SystemDict.Add("S", "SECAM");
            SystemDict.Add("N", "NTSC");

        }

        public override void OnNewEquipment()
        {
            Bandwith = TvStationEquipment.BandWidth;
            EmiVideo = TvStationEquipment.EVideo;
            EmiAudio = TvStationEquipment.EAudio;

            PowAudio[PowerUnits.W] = TvStationEquipment.MaxPowerAudio[PowerUnits.W];
            PowVideo[PowerUnits.W] = TvStationEquipment.MaxPowerVideo[PowerUnits.W];
        }
    }
}
