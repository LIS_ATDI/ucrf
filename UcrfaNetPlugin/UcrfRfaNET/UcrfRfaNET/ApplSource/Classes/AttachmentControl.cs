﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class AttachmentControl
    {
        /// <summary>
        /// Обновляет данные документа
        /// </summary>
        public static void UpdateAttachmentData(string docNumber, DateTime datePrint, string blankNumber)
        {
            IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadWrite);
            try
            {
                rsDocFiles.Select("ID,MEMO,DOC_REF,DOC_DATE");
                rsDocFiles.SetWhere("DOC_REF", IMRecordset.Operation.Like, docNumber);
                for (rsDocFiles.Open(); !rsDocFiles.IsEOF(); rsDocFiles.MoveNext())
                {
                    rsDocFiles.Edit();
                    rsDocFiles.Put("MEMO", blankNumber);
                    rsDocFiles.Put("DOC_DATE", datePrint);
                    rsDocFiles.Update();
                }
            }
            finally
            {
                rsDocFiles.Final();
            }
        }
        /// <summary>
        /// Копирует attachments
        /// </summary>
        /// <param name="tableSrc">Исходная таблица</param>
        /// <param name="idSrc">ID исходной таблицы</param>
        /// <param name="tableDes">Таблица получатель</param>
        /// <param name="idDes">ID записи в таблице получателя</param>
        public static void CopyAttachment(string tableSrc, int idSrc, string tableDes, int idDes)
        {
            IMRecordset rsSrc = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
            try
            {
                rsSrc.Select("NAME,PATH,IS_URL,DOC_ID");
                rsSrc.SetWhere("REC_TAB", IMRecordset.Operation.Like, tableSrc);
                rsSrc.SetWhere("REC_ID", IMRecordset.Operation.Eq, idSrc);
                for (rsSrc.Open(); !rsSrc.IsEOF(); rsSrc.MoveNext())
                {
                    IMRecordset rsDes = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                    try
                    {
                        rsDes.Select("REC_ID,REC_TAB,NAME,PATH,IS_URL,DOC_ID");
                        rsDes.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                        rsDes.Open();
                        rsDes.AddNew();
                        rsDes.Put("REC_ID", idDes);
                        rsDes.Put("REC_TAB", tableDes);
                        rsDes.Put("NAME", rsSrc.Get("NAME"));
                        rsDes.Put("PATH", rsSrc.Get("PATH"));
                        rsDes.Put("IS_URL", rsSrc.Get("IS_URL"));
                        rsDes.Put("DOC_ID", rsSrc.Get("DOC_ID"));
                        rsDes.Update();
                    }
                    finally
                    {
                        rsDes.Final();
                    }
                }
            }
            finally
            {
                rsSrc.Final();
            }
        }

        private const string _defaultName = "ПТК: XICSM_UcrfRfaNET";
        private string _name = "";
        /// <summary>
        /// Описывает значение DOCLINKS.NAME которое используется для того, чтобы
        /// пометить записи, которые сделал УРЗПшник
        /// </summary>
        public string Name {get { return _name;  }}

        public List<string> FileList = new List<string>();
     
        public AttachmentControl()
        {
            _name = _defaultName;
        }
               
        public AttachmentControl(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                _name = _defaultName;
            } else {
                _name = name;            
            }
        }

        public void DetachFiles(string tableName, int id)
        {
            IMRecordset fotoRec;

            fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                fotoRec.Select("REC_TAB,REC_ID,PATH");
                fotoRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableName);
                fotoRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);                
                for (fotoRec.Open(); !fotoRec.IsEOF(); fotoRec.MoveNext())
                    fotoRec.Delete();
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            
        }

        /*public void ReadPathes(string tableName, int id)
        {
            IMRecordset fotoRec;
            FileList = new List<string>();
            
            fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            try{
                fotoRec.Select("REC_TAB,REC_ID,PATH");
                fotoRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableName);
                fotoRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);
                //fotoRec.SetWhere("PATH", IMRecordset.Operation.Eq, fileName);
                for (fotoRec.Open(); !fotoRec.IsEOF(); fotoRec.MoveNext())
                {
                    string path = fotoRec.GetS("PATH");
                    FileList.Add(path);
                }                
            }
            finally
            {
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            }                            
        }

        public void AttachPathes(string tableName, int id)
        {
            IMRecordset fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);

            try
            {
                fotoRec.Select("REC_TAB,REC_ID,PATH");
                int i = 0;
                fotoRec.Open();               
                foreach (string fileName in FileList)
                {
                    // Цепляем к станции
                    fotoRec.AddNew();
                    fotoRec.Put("REC_TAB", tableName);
                    fotoRec.Put("REC_ID", id);
                    fotoRec.Put("PATH", fileName);
                    fotoRec.Update();
                }
            }
            finally
            {
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            }
        }

        public void AttachPathesToPosition(PositionState position)
        {
            AttachPathes(position.TableName, position.Id);
            AttachPathes(ICSMTbl.itblSite, position.AdminSitesId);
        }*/

        public void AttachFiles(List<string> filesDst, List<string> filesSrc, string tableName, int id)
        {
            //Удаление пред.значений тек. таблицы
            IMRecordset fotoRec;
            foreach (string fileName in filesDst)
            {
                fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                fotoRec.Select("REC_TAB,REC_ID,PATH,NAME");
                fotoRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableName);
                fotoRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);
                fotoRec.SetWhere("NAME", IMRecordset.Operation.Eq, Name);
                for (fotoRec.Open(); !fotoRec.IsEOF(); fotoRec.MoveNext())
                    fotoRec.Delete();
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            }

            /*
            //Удаление пред.значений админ таблицы            
            foreach (string fileName in filesDst)
            {
                fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                fotoRec.Select("REC_TAB,REC_ID,PATH");
                fotoRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, ICSMTbl.itblSite);
                fotoRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, AdminSitesId);
                fotoRec.SetWhere("PATH", IMRecordset.Operation.Eq, fileName);
                for (fotoRec.Open(); !fotoRec.IsEOF(); fotoRec.MoveNext())
                    fotoRec.Delete();
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            }*/

            //Привязка фотографий к конкретной станции...
            fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            try
            {
                fotoRec.Select("REC_TAB,REC_ID,PATH,NAME");
                int i = 0;
                fotoRec.Open();
                foreach (string fileName in filesDst)
                {
                    // Цепляем к станции
                    fotoRec.AddNew();
                    fotoRec.Put("REC_TAB", tableName);
                    fotoRec.Put("REC_ID", id);
                    fotoRec.Put("PATH", fileName);
                    fotoRec.Put("NAME", Name);
                    fotoRec.Update();

                    /*//Цепляем к позиции
                    fotoRec.AddNew();
                    fotoRec.Put("REC_TAB", TableName);
                    fotoRec.Put("REC_ID", Id);
                    fotoRec.Put("PATH", fileName);
                    fotoRec.Update();

                    //Цепляем в админ-сайту
                    fotoRec.AddNew();
                    fotoRec.Put("REC_TAB", ICSMTbl.itblSite);
                    fotoRec.Put("REC_ID", AdminSitesId);
                    fotoRec.Put("PATH", fileName);
                    fotoRec.Update();*/

                    FileInfo fi = new FileInfo(filesSrc[i]);
                    if (fi.Exists)
                        fi.CopyTo(fileName, true);
                    i++;
                }
            }
            finally
            {
                if (fotoRec.IsOpen())
                    fotoRec.Close();
                fotoRec.Destroy();
            }
        }
        /// <summary>
        /// Для коммита, тиражирует связи
        /// </summary>
        /// <param name="srcTableName">Таблица, к которой сейчас все прицеплено, обычно это XNRFA_POSITION либо родная таблица позиций</param>
        /// <param name="tableId">ID, к которому сейчас все прицеплено, обычно это XNRFA_POSITION либо родная таблица позиций</param>
        /// <param name="stationTableName">Таблица станции, которая должна получить атачменты</param>
        /// <param name="stationId"></param>
        /// <param name="positionTableName">Родная таблица позиций, которая должна получить атачменты</param>
        /// <param name="positionId"></param>
        /// <param name="admSiteTableName">Родная админсайтов, которая должна получить атачменты</param>
        /// <param name="admSiteId"></param>
        public void RelocateAndCopyDocLinks(string srcTableName, int tableId, string stationTableName, int stationId, string positionTableName, int positionId, string admSiteTableName, int admSiteId)
        {
            
            IMRecordset fotoRec1 = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            IMRecordset fotoRec2 = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            try
            {
                fotoRec1.Select("REC_TAB,REC_ID,PATH,NAME");                
                fotoRec1.SetWhere("REC_TAB", IMRecordset.Operation.Eq, srcTableName);
                fotoRec1.SetWhere("REC_ID", IMRecordset.Operation.Eq, tableId);
                fotoRec1.SetWhere("NAME", IMRecordset.Operation.Eq, Name);

                fotoRec2.Select("REC_TAB,REC_ID,PATH,NAME");
                fotoRec2.SetWhere("REC_TAB", IMRecordset.Operation.Eq, srcTableName);
                fotoRec2.SetWhere("REC_ID", IMRecordset.Operation.Eq, tableId);
                fotoRec2.SetWhere("NAME", IMRecordset.Operation.Eq, Name);
                                                
                fotoRec2.Open();                    
                for (fotoRec1.Open(); !fotoRec1.IsEOF(); fotoRec1.MoveNext())
                {
                    fotoRec1.Edit();
                    string path = fotoRec1.GetS("PATH");
                    //int isURL = fotoRec.GetI("IS_URL");

                    fotoRec1.Put("REC_TAB", positionTableName);
                    fotoRec1.Put("REC_ID", positionId);
                    fotoRec1.Put("NAME", "");
                    fotoRec1.Update();

                    fotoRec2.AddNew();
                    fotoRec2.Put("REC_TAB", stationTableName);
                    fotoRec2.Put("REC_ID", stationId);
                    fotoRec2.Put("PATH", path);
                    //fotoRec.Put("IS_URL", isURL);
                    fotoRec2.Put("NAME", "");
                    fotoRec2.Update();

                    fotoRec2.AddNew();
                    fotoRec2.Put("REC_TAB", admSiteTableName);
                    fotoRec2.Put("REC_ID", admSiteId);
                    fotoRec2.Put("PATH", path);
                    //fotoRec.Put("IS_URL", isURL);
                    fotoRec2.Put("NAME", "");
                    fotoRec2.Update();
                }                                                
            }
            finally
            {
                if (fotoRec1.IsOpen())
                    fotoRec1.Close();
                fotoRec1.Destroy();

                if (fotoRec2.IsOpen())
                    fotoRec2.Close();
                fotoRec2.Destroy();
            }
        }

        /// <summary>
        /// Для коммита, тиражирует связи
        /// </summary>
        /// <param name="srcTableName">Таблица, к которой сейчас все прицеплено, обычно это XNRFA_POSITION либо родная таблица позиций</param>
        /// <param name="tableId">ID, к которому сейчас все прицеплено, обычно это XNRFA_POSITION либо родная таблица позиций</param>
        /// <param name="stationTableName">Таблица станции, которая должна получить атачменты</param>
        /// <param name="stationId"></param>
        /// <param name="positionTableName">Родная таблица позиций, которая должна получить атачменты</param>
        /// <param name="positionId"></param>
        /// <param name="admSiteTableName">Родная админсайтов, которая должна получить атачменты</param>
        /// <param name="admSiteId"></param>
        public void RelocateAndCopyDocLinks(string srcTableName, int tableId, string stationTableName, List<int> stationIdList, string positionTableName, int positionId, string admSiteTableName, int admSiteId)
        {

            IMRecordset fotoRec1 = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            IMRecordset fotoRec2 = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            try
            {
                fotoRec1.Select("REC_TAB,REC_ID,PATH,NAME");
                fotoRec1.SetWhere("REC_TAB", IMRecordset.Operation.Eq, srcTableName);
                fotoRec1.SetWhere("REC_ID", IMRecordset.Operation.Eq, tableId);
                fotoRec1.SetWhere("NAME", IMRecordset.Operation.Eq, Name);

                fotoRec2.Select("REC_TAB,REC_ID,PATH,NAME");
                fotoRec2.SetWhere("REC_TAB", IMRecordset.Operation.Eq, srcTableName);
                fotoRec2.SetWhere("REC_ID", IMRecordset.Operation.Eq, tableId);
                fotoRec2.SetWhere("NAME", IMRecordset.Operation.Eq, Name);

                fotoRec2.Open();
                for (fotoRec1.Open(); !fotoRec1.IsEOF(); fotoRec1.MoveNext())
                {
                    fotoRec1.Edit();
                    string path = fotoRec1.GetS("PATH");
                    //int isURL = fotoRec.GetI("IS_URL");

                    fotoRec1.Put("REC_TAB", positionTableName);
                    fotoRec1.Put("REC_ID", positionId);
                    fotoRec1.Put("NAME", "");
                    fotoRec1.Update();

                    foreach (int stationId in stationIdList)
                    {
                        fotoRec2.AddNew();
                        fotoRec2.Put("REC_TAB", stationTableName);
                        fotoRec2.Put("REC_ID", stationId);
                        fotoRec2.Put("PATH", path);
                        //fotoRec.Put("IS_URL", isURL);
                        fotoRec2.Put("NAME", "");
                        fotoRec2.Update();
                    }

                    fotoRec2.AddNew();
                    fotoRec2.Put("REC_TAB", admSiteTableName);
                    fotoRec2.Put("REC_ID", admSiteId);
                    fotoRec2.Put("PATH", path);
                    //fotoRec.Put("IS_URL", isURL);
                    fotoRec2.Put("NAME", "");
                    fotoRec2.Update();
                }
            }
            finally
            {
                if (fotoRec1.IsOpen())
                    fotoRec1.Close();
                fotoRec1.Destroy();

                if (fotoRec2.IsOpen())
                    fotoRec2.Close();
                fotoRec2.Destroy();
            }
        }
    }
}
