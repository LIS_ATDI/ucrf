﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    /// <summary>
    /// Клас для роботи з позицією сайта і редагуванням його властивостей для адмін. сайтів
    /// </summary>
    class PositionAdmSite : PositionState
    {
        public string _tableName = "";
        public int AdminSiteId { get; set; }
        public double AdminSiteY { get; set; }
        public double AdminSiteX { get; set; }
        public string AdminSiteCsys { get; set; }
        public string AdminSiteAddress { get; set; }

        public string SendUser { get; set; }
        public string AcceptUser { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime AcceptDate { get; set; }        

        /// <summary>
        /// Заполняем обьект данными
        /// </summary>
        public  override void LoadStatePosition(int id, string table)
        {
            _tableName = table;
            base.LoadStatePosition(id,table);
            if (Csys == "4DEC" || string.IsNullOrEmpty(Csys))
            {
                X = X.DecToDms();
                Y = Y.DecToDms();
                Csys = "4DMS";
            }
        }

        public void Save()
        {
            TableName = _tableName;
            SaveToBD();
            SaveStatus();
            IsChanged = false;
        }

        /// <summary>
        /// Вертає список фотографій для AdmSite
        /// </summary>
        /// <returns></returns>
        public List<string> GetFotoFromCities(ref List<string> tmpList)
        {
            // List<string> tmpList = new List<string>();
            IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadOnly);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    tmpList.Add(r.GetS("PATH"));
            }
            finally
            {
                r.Final();
            }
            return tmpList;
        }
        /// <summary>
        /// Вертає список фотографій через параметр ADMS_ID
        /// </summary>
        /// <returns></returns>
        public List<string> GetFotoFromCitiesNew()
        {
            List<string> tmpList = new List<string>();
            int ADMS_ID = -1;

             if ((TableName != ICSMTbl.SITES) && (TableName != ICSMTbl.Site))
            {
                IMRecordset ADMS = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                ADMS.Select("ID,ADMS_ID");
                ADMS.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                ADMS.Open();
                try
                {
                    if (!ADMS.IsEOF())
                    {
                        ADMS_ID = ADMS.GetI("ADMS_ID");
                        IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadOnly);
                        r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
                        r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, ICSMTbl.SITES);
                        r.SetWhere("REC_ID", IMRecordset.Operation.Eq, ADMS_ID);
                        try
                        {
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                                tmpList.Add(r.GetS("PATH"));
                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                }
                finally
                {
                    ADMS.Final();
                }
            }
            else if ((TableName == ICSMTbl.SITES) || (TableName == ICSMTbl.Site))
            {
                GetFotoFromCities(ref tmpList);
            }


            return tmpList;
        }


        /// <summary>
        /// Зберегти всі фото для ВРВ_САЙТИ
        /// </summary>
        /// <param name="files"></param>
        public void SaveFotoCities(List<string> files)
        {
            //Видалити всі повторюючі фото
            IMRecordset r;
            for (int i = 0; i < files.Count; i++)
            {
                r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
                r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
                r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
                r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("PATH", IMRecordset.Operation.Eq, files[i]);
                for (r.Open();!r.IsEOF();r.MoveNext())
                    r.Delete();
                r.Final();
            }

            //Запис у файл
            r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            r.Open();            
            try
            {                
                for (int i = 0; i < files.Count;i++ )
                {
                    r.AddNew();
                    r.Put("REC_TAB",TableName);
                    r.Put("REC_ID", Id);
                    r.Put("PATH",files[i]);
                    r.Update();
                }                    
            }
            finally
            {
                r.Final();
            }            
        }

        /// <summary>
        /// Видалення потчоної фотографії
        /// </summary>
        /// <param name="s"></param>
        public void DeleteRecCities(string delPhoto)
        {
            IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("PATH", IMRecordset.Operation.Eq, delPhoto);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    r.Delete();
            }
            finally
            {
                r.Final();
            }                                    
        }

        /// <summary>
        /// Повертає дані, щодо статусу сайта
        /// </summary>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        public int CheckStatusPosition(out bool b1, out bool b2, string tblName)
        {         
            string strStatus = "";
            string strSitesStatus = "";
            int admsId = IM.NullI;
            IMRecordset r;
            if (tblName != ICSMTbl.SITES)
            {
                r = new IMRecordset(_tableName, IMRecordset.Mode.ReadWrite);
                r.Select("ID,ADM_COD,TABLE_NAME,ADMS_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        admsId = r.GetI("ADMS_ID");
                        strStatus = r.GetS("ADM_COD");
                    }
                }
                finally
                {
                    r.Final();
                }
                if (admsId != IM.NullI)
                {
                    r = new IMRecordset(ICSMTbl.SITES, IMRecordset.Mode.ReadOnly);
                    r.Select("STATUS,ID");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, admsId);
                    try
                    {
                        r.Open();
                        if (!r.IsEOF())
                            strStatus = r.GetS("STATUS");
                    }
                    finally
                    {
                        r.Final();
                    }
                    if (strStatus == CAndE.Need)
                        strStatus = CAndE.PosAdmcodeEnum.NeedCheck.ToString();
                    else if (strStatus == CAndE.Chkd)
                        strStatus = CAndE.PosAdmcodeEnum.Checked.ToString();
                    else
                        strStatus = CAndE.PosAdmcodeEnum.NotCheck.ToString();
                }
                else
                    strStatus = CAndE.PosAdmcodeEnum.NeedCheck.ToString();
            }
            else
            {
                r = new IMRecordset(_tableName, IMRecordset.Mode.ReadWrite);
                r.Select("ID,STATUS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        strSitesStatus = r.GetS("STATUS");
                }
                finally
                {
                    r.Final();
                }
            }
            b1 = (strStatus != CAndE.PosAdmcodeEnum.NeedCheck.ToString());
            b2 = !b1;
            if (tblName == ICSMTbl.SITES)
            {
                b2 = (strSitesStatus == CAndE.Need);
                b1 = !b2;
            }
            r = new IMRecordset(PlugTbl.XfaPositionExt, IMRecordset.Mode.ReadOnly);
            r.Select("ID,POS_ID,POS_TABLE,SEND_DATE,SEND_USER,TABLE_NAME,CHECK_DATE,CHECK_USER");
            r.SetWhere("POS_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("POS_TABLE", IMRecordset.Operation.Eq, TableName);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    SendUser = r.GetS("SEND_USER");
                    SendDate = r.GetT("SEND_DATE");
                    AcceptUser = r.GetS("CHECK_USER");
                    AcceptDate = r.GetT("CHECK_DATE");
                }                
            }
            finally
            {
                r.Final();
            }
            return admsId;
        }

        public void GetAdminSiteInfo(int id)
        {
            IMRecordset r=new IMRecordset(ICSMTbl.SITES,IMRecordset.Mode.ReadOnly);
            r.Select("ID,REMARK,X,Y,CSYS");
            r.SetWhere("ID",IMRecordset.Operation.Eq,id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    AdminSiteId = r.GetI("ID");
                    AdminSiteCsys = r.GetS("CSYS");
                    AdminSiteX = r.GetD("X");
                    AdminSiteY = r.GetD("Y");
                    AdminSiteAddress = r.GetS("REMARK");
                }
            }
            finally 
            {
                r.Final();                
            }
            
        }

        /// <summary>
        /// Записує новий статус в БД для сайту
        /// </summary>
        /// <param name="newStatus">код статусу</param>
        public void SetNewStatus(string newStatus)
        {
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,ADM_COD,TABLE_NAME");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ADM_COD", newStatus);
                    r.Update();
                }
            }
            finally
            {
                r.Final();
            }
        }

        /// <summary>
        /// Оновити параметри користувача
        /// </summary>
        /// <param name="isCheck">пере</param>
        public void SetUserParam(bool isCheck)
        {
            if (isCheck)
            {
                SendUser = IM.ConnectedUser();
                SendDate = DateTime.Now;                
            }
            else
            {
                AcceptUser = IM.ConnectedUser();
                AcceptDate = DateTime.Now;                
            }
        }

        /// <summary>
        /// Зберегти параметри статусу
        /// </summary>
        private void SaveStatus()
        {
            IMRecordset r= new IMRecordset(PlugTbl.XfaPositionExt,IMRecordset.Mode.ReadWrite);
            r.Select("ID,POS_ID,POS_TABLE,SEND_DATE,SEND_USER,TABLE_NAME,CHECK_DATE,CHECK_USER");
            r.SetWhere("POS_ID",IMRecordset.Operation.Eq,Id);
            r.SetWhere("POS_TABLE", IMRecordset.Operation.Eq, TableName);
            try
            {
                r.Open();
                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(PlugTbl.XfaPositionExt, 1, -1));
                    r.Put("POS_ID", Id);
                    r.Put("POS_TABLE", TableName);
                    r.Put("TABLE_NAME", PlugTbl.XfaPositionExt);
                }
                else
                    r.Edit();

                r.Put("CHECK_DATE", AcceptDate);
                r.Put("CHECK_USER", AcceptUser);
                r.Put("SEND_DATE", SendDate);
                r.Put("SEND_USER", SendUser);              
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }

        public void SetStatusSites(string status)
        {
            IMRecordset r= new IMRecordset(ICSMTbl.SITES,IMRecordset.Mode.ReadWrite);
            r.Select("ID,STATUS");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("STATUS", status);
                    r.Update();
                }
            }
            finally
            {
                r.Final();
            }
        }
    }
}
