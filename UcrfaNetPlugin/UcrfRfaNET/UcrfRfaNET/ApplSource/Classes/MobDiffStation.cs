﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using NSPosition;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobDiffStation : MobStation
    {
        private bool _initialized = false;
        public int OnOff { get; set; }

        public bool IsInitialized
        {
            get { return _initialized; }
        }

        public static new readonly string TableName = PlugTbl.itblXnrfaDiffMobSta;        

        public MobDiffStation()
        {
            OnOff = 1;
            TransmitterCount = IM.NullD;
            _antenna = new MobStationAntenna();
            _equipment = new MobStationEquipment();
            _filter = new MobStationFilter();
            _plan = new MobStationPlan();
        }
        public override void Save()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,MOBSTA_ID,POS_ID,POS_TABLE,DESIG_EM,ANT_ID,EQUIP_ID,EQUIP_NBR,PWR_ANT,AZIMUTH,AGL,GAIN,CERT_NUM,CERT_DATE,ANTENNA_NAME,EQUIP_NAME,ANT_WIDTH,ONOFF");
                r.Select("COMMENTS,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("MOBSTA_ID", Id);
                }
                else
                {
                    r.Edit();
                }

                r.Put("INACTIVE", 0);
                r.Put("POS_ID", Position.Id);
                r.Put("POS_TABLE", Position.TableName);
                r.Put("DESIG_EM", DesigEmission);

                if (TransmitterCount == IM.NullD)
                    r.Put("EQUIP_NBR", 0.0);
                else
                    r.Put("EQUIP_NBR", TransmitterCount);

                r.Put("AZIMUTH", Azimuth);
                r.Put("AGL", Agl);
                r.Put("GAIN", Gain);
                r.Put("ANT_WIDTH", AntenaWidth);
                r.Put("EQUIP_ID", Equipment.Id);
                r.Put("CERT_NUM", Equipment.Certificate.Symbol);
                r.Put("CERT_DATE", Equipment.Certificate.Date);
                r.Put("EQUIP_NAME", Equipment.Name);
                r.Put("COMMENTS", StatComment);
                r.Put("ANT_ID", Antenna.Id);
                r.Put("ANTENNA_NAME", Antenna.Name);
                r.Put("PWR_ANT", Power[PowerUnits.kW]);
                r.Put("ONOFF", OnOff);

                r.Update();

                DeleteFrequencies();
                SaveFreqencies();
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            FillComments(ApplId);
        }

        private void SaveFreqencies()
        {
            IMRecordset r = null;

            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
                r.Select("ID,MOBSTA_ID,FREQUENCY,TR");
                r.Open();

                for (int j = 0; j < Plan.RxFrequency.Count; j++)
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobStaFreq, 1, -1));

                    r.Put("MOBSTA_ID", Id);
                    r.Put("FREQUENCY", Plan.RxFrequency[j]);
                    r.Put("TR", "R");
                    r.Update();
                }

                for (int j = 0; j < Plan.TxFrequency.Count; j++)
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobStaFreq, 1, -1));

                    r.Put("MOBSTA_ID", Id);
                    r.Put("FREQUENCY", Plan.TxFrequency[j]);
                    r.Put("TR", "T");
                    r.Update();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }

        }

        public override void Load()
        {
            _antenna = new MobStationAntenna();
            _antenna.TableName = ICSMTbl.itblAntennaMob;

            _equipment = new MobStationEquipment();
            _equipment.TableName = ICSMTbl.itblEquipPmr;

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,POS_TABLE,DESIG_EM,ANT_ID,EQUIP_ID,EQUIP_NBR,PWR_ANT,AZIMUTH,AGL,GAIN,CERT_NUM,CERT_DATE,EQUIP_NAME,ANTENNA_NAME,ANT_WIDTH,ONOFF");
                r.Select("COMMENTS,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF()) // , ...              
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Position = new PositionState();

                        Position.Id = r.GetI("POS_ID");
                        Position.TableName = r.GetS("POS_TABLE");
                        DesigEmission = r.GetS("DESIG_EM");

                        TransmitterCount = r.GetI("EQUIP_NBR");
                        if (TransmitterCount == 0)
                            TransmitterCount = IM.NullD;

                        Azimuth = r.GetD("AZIMUTH");

                        Antenna.Id = r.GetI("ANT_ID");
                        Equipment.Id = r.GetI("EQUIP_ID");
                        Antenna.Load();
                        Equipment.Load();

                        Agl = r.GetD("AGL");

                        Gain = r.GetD("GAIN");
                        Antenna.Gain = r.GetD("GAIN");
                        Antenna.AntennaWidth = r.GetD("ANT_WIDTH");
                        AntenaWidth = r.GetD("ANT_WIDTH");
                        StatComment = r.GetS("COMMENTS");
                        Equipment.Certificate.Symbol = r.GetS("CERT_NUM");
                        Equipment.Certificate.Date = r.GetT("CERT_DATE");
                        Power[PowerUnits.kW] = r.GetD("PWR_ANT");
                        Equipment.Name = r.GetS("EQUIP_NAME");
                        Antenna.Name = r.GetS("ANTENNA_NAME");
                        OnOff = r.GetI("ONOFF");

                        Position.LoadStatePosition(Position.Id, Position.TableName);

                        LoadFreqencies();
                        _initialized = true;
                    }
                }
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        private void LoadFreqencies()
        {
            IMRecordset r = null;

            List<double> txFreqList = new List<double>();
            List<double> rxFreqList = new List<double>();
            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
                r.Select("ID,MOBSTA_ID,TR,FREQUENCY");
                r.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                while (!r.IsEOF())
                {
                    string tr = r.GetS("TR");
                    double Frequency = IM.RoundDeci(r.GetD("FREQUENCY"), 5);

                    if (tr == "R")
                        rxFreqList.Add(Frequency);

                    if (tr == "T")
                        txFreqList.Add(Frequency);

                    r.MoveNext();
                }

                Plan.TxFrequency = txFreqList;
                Plan.RxFrequency = rxFreqList;
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }

        }

        private void DeleteFrequencies()
        {
            IMRecordset r = null;

            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
                r.Select("ID");
                r.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                while (!r.IsEOF())
                {
                    r.Delete();
                    r.MoveNext();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }

        public void Remove()
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();                    
                }

                DeleteFrequencies();
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
