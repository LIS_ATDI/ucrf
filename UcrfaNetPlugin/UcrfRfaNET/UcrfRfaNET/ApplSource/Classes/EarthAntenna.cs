﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
   class EarthAntenna : Antenna
   {

      public double Diameter { get; set; }
      public double TxGain { get; set; }
      public double RxGain { get; set; }

      public EarthAntenna()
      {
         _tableName = ICSMTbl.itblEstaAntenna;
      }

      public override void Load()
      {
         IMRecordset rr = new IMRecordset(ICSMTbl.itblAntenna, IMRecordset.Mode.ReadOnly);

         if (ID == IM.NullI)
         {
            throw new Exception("Earth Antenna ID is not initialized");
         }

         try
         {                                                
            rr.Select("ID,NAME,DIAMETER,GAIN,LOW_GAIN");
            rr.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            rr.Open();
            if (!rr.IsEOF())
            {
               Name = rr.GetS("NAME");
               Diameter = rr.GetD("DIAMETER");
               TxGain = rr.GetD("GAIN");               
               RxGain = rr.GetD("LOW_GAIN");
            }            
         }
         finally 
         {
            rr.Close();
            rr.Destroy();                     
         }         
      }
   }
}
