﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class TvAnalogEquipment : Equipment
    {
        public string EAudio { get; set; }
        public string EVideo { get; set; }
        public Power MaxPowerAudio
        {
            get { return _maxPowerAudio; }
        }

        public Power MaxPowerVideo
        {
            get { return _maxPowerVideo; }
        }

        public TvAnalogEquipment()
        {
            _tableName = ICSMTbl.itblEquipBro;
        }

        public override void Load()
        {
            IMRecordset eqImRecordset = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
            try
            {
                eqImRecordset.Select("ID,NAME,CUST_TXT1,CUST_DAT1,DESIG_EMISSION,CUST_TXT2");
                eqImRecordset.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                eqImRecordset.Open();
                if (!eqImRecordset.IsEOF())
                {
                    Name = eqImRecordset.GetS("NAME");
                    Certificate.Symbol = eqImRecordset.GetS("CUST_TXT1");
                    Certificate.Date = eqImRecordset.GetT("CUST_DAT1");
                    EVideo = eqImRecordset.GetS("DESIG_EMISSION");
                    EAudio = eqImRecordset.GetS("CUST_TXT2");
                }
            }
            finally
            {
                if (eqImRecordset.IsOpen())
                    eqImRecordset.Close();
                eqImRecordset.Destroy();
            }
        }

    }
}
