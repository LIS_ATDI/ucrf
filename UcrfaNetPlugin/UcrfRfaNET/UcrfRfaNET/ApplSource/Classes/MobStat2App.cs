﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRR;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;


namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    //===============================================================
    /// <summary>
    /// класс станции БС
    /// </summary>
    internal class StationBS : BaseStation
    {
        public bool Updated;
        public int EquipID;
        public int AntID;
        public int PosID;
        private int id = IM.NullI;
        public MobSta2App.FilterTypeInfo fti;

        public int ID
        {
            get
            {
                if (id == IM.NullI && objStation != null)
                    id = objStation.GetI("ID");
                return id;
            }
            set { id = value; }
        }

        public bool ForDelete { get; set; }
        //===================================================
        /*public double AntDiam { get; set; }  //Диаметр антенны
        public double AntGain { get; set; }//Коэффициент усиления
        public double AntGainRx { get; set; }//Коэффициент усиления приемника
        public double TxBand { get; set; } //Ширина полосы
        public double RxBand { get; set; } //ширина полосы
        public int NoiseT { get; set; } //Шумовая температура
        public string TxPolar { get; set; } //Поляризация передатчика
        public string RxPolar { get; set; } //Поляризация Приемника
        public double MinPower = -1;     // Минимальная допустимая мощность
        public double MaxPower = -1;     // Максимальная допустимая мощность*/

        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна    
        public IMObject objStation = null;

        public double Azimuth;
        public double Elevation;
        public string DesigEmission;

        public double Agl;
        public double Gain;
        public string Polar;

        public List<double> FreqTxList;
        public List<double> FreqRxList;

        public string FreqTxListAsString;
        public string FreqRxListAsString;

        //public double TxFreq;
        //public double RxFreq;

        /*public List<double> nomFreqTx = new List<double>();  //Номинальные частоты передатчика
        public List<double> nomFreqRx = new List<double>();  //Номинальные частоты приемника
        public List<string> nomTxEmi = new List<string>();  //Класс излучения передатчика
        public List<string> nomRxEmi = new List<string>();  //Класс излучения приемника
        public List<double> nomPowTx = new List<double>();  //Мощность передатчика*/
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public StationBS()
        {
            ForDelete = false;
            // Инициализируем все перемнные
            Updated = false;
            FreqTxList = new List<double>();
            FreqRxList = new List<double>();
            fti.FilterIndex = IM.NullI;
            fti.FilterName = "";
        }
        /*public void Update()
        {
            Updated = true;
        }*/
        protected override void DisposeExec()
        {
            if (objEquip != null)
                objEquip.Dispose();

            if (objAntenna != null)
                objAntenna.Dispose();

            if (objStation != null)
                objStation.Dispose();
        }
    }

    class MobSta2App : BaseAppClass
    {

        private const string EquipNameCellKey = "kName_";
        private const string AntPowerCellKey = "antPower_";
        private const string RxBandCellKey = "rxBand_";
        private const string DesigEmissionCellKey = "kEMI_";
        private const string ModulationCellKey = "mod_";
        private const string SpeedCellKey = "speed_";
        private const string CertificateNumberCellKey = "certNum_";
        private const string CertificateDateCellKey = "certDate_";
        private const string FiltType = "filtType_";

        public delegate void GridEvent(Cell cell, Grid grid);
        Hashtable InitParamGridEventTable = new Hashtable();

        public static readonly string TableName = ICSMTbl.itblMobStation2;
        protected List<StationBS> stationList = new List<StationBS>();
        protected List<IMObject> objStations = new List<IMObject>();  //Сектора
        protected List<RecordPtr> sectorIDs = new List<RecordPtr>();
        protected double Longitude = 0; //Долгота
        protected double Latitude = 0;  //Широта

        //private double lonDMS;
        //private double latDMS;      
        //private double powerAntenna;
        //private string EquipmentName;
        //private string OwnerAddress;
        //private double asl;
        //private double agl;        
        //private double bw;
        //private string AntennaType;        
        //private string Polar;

        int OwnerId;
        private List<string> Modulation;

        public struct FilterTypeInfo
        {
            public string FilterName;
            public int FilterIndex;
        };

        //protected IMObject objEquip = null;     //Оборудование
        //protected IMObject objAntenna = null;   //Антенна
        //protected IMObject objPosition = null;  //Cайт?

        public MobSta2App(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, MobSta2App.TableName, _ownerId, _packetID, _radioTech)
        {
                appType = AppType.AppBS;
                department = DepartmentType.VFSR;
                departmentSector = DepartmentSectorType.VFSR_SSRT;
                for (int i = 0; i < 6; i++)
                {
                     StationBS station = new StationBS();
                     stationList.Add(station);
                }
                int msID = objStation.GetI("ID");
                sectorIDs.Add(new RecordPtr(MobSta2App.TableName, msID));
                
                objStations.Add(objStation);

                Modulation = new List<string>();

                Modulation.Add("none");
                Modulation.Add("AM compr normal");
                Modulation.Add("FM");
                Modulation.Add("QPSK");
                Modulation.Add("4-QAM");
                Modulation.Add("8-QAM");
                Modulation.Add("16-QAM");
                Modulation.Add("32-QAM");
                Modulation.Add("64-QAM");
                Modulation.Add("128-QAM");
                Modulation.Add("256-QAM");
                Modulation.Add("512-QAM");
                Modulation.Add("DQPSK");
                Modulation.Add("QPSK 1/2");
                Modulation.Add("QPSK 2/3");
                Modulation.Add("QPSK 3/4");
                Modulation.Add("QPSK 5/6");
                Modulation.Add("QPSK 7/8");
                Modulation.Add("16-QAM 1/2 M1");
                Modulation.Add("16-QAM 2/3");
                Modulation.Add("16-QAM 3/4");
                Modulation.Add("16-QAM 5/6");
                Modulation.Add("16-QAM 7/8");
                Modulation.Add("64-QAM 1/2M2");
                Modulation.Add("64-QAM 2/3M3");
                Modulation.Add("64-QAM 3/4");
                Modulation.Add("64-QAM 5/6");
                Modulation.Add("64-QAM 7/8");
                Modulation.Add("PDM-8");
                Modulation.Add("PDM-4 1/2");
                Modulation.Add("Analog");
                Modulation.Add("TPRS");
                Modulation.Add("AM compr high");
                Modulation.Add("BFSK 1/2");
                Modulation.Add("BFSK 3/4");
                Modulation.Add("PDM-4 2/3");
                Modulation.Add("PDM-4 3/4");
                Modulation.Add("PDM-4 5/6");
                Modulation.Add("PDM-4 7/8");
                Modulation.Add("BPSK");
                Modulation.Add("GMSK");
                Modulation.Add("8PSK");
                Modulation.Add("OFDM");
                Modulation.Add("FFSK");
                Modulation.Add("FFSK2");
                Modulation.Add("FFSK4");
                Modulation.Add("FFSK4L");

                StationBS Station = stationList[0];
                OpenStation(ref Station, recordID.Id);

                /*if (Station.ID==IM.NullI)
                {                                            
                    Station.objStation = objStation;
                    int ID = objStation.GetI("ID");

                    Station.AntID = objStation.GetI("ANT_ID");
                    Station.PosID = objStation.GetI("POS_ID");
                    Station.EquipID = objStation.GetI("EQUIP_ID");

                    Station.Azimuth = objStation.GetD("AZIMUTH");
                    Station.Elevation = objStation.GetD("ELEVATION");
                    Station.Agl = objStation.GetD("AGL");
                    Station.Gain = objStation.GetD("GAIN");
                    Station.Polar = objStation.GetS("POLAR");
                }*/

                if (newStation == true)
                {
                    Station.objStation.Put("STANDARD", radioTech);
                }
                else
                {
                    radioTech = objStation.GetS("STANDARD");
                    IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
                    r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, MobSta2App.TableName);
                    r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                    try
                    {
                        r.Open();
                        if (!r.IsEOF())
                        {
                            for (int i = 2; (i <= 6) && (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI && r.GetI("OBJ_ID" + i.ToString()) > 0); ++i)
                            {
                                RecordPtr sector = new RecordPtr(MobSta2App.TableName, r.GetI("OBJ_ID" + i.ToString()));
                                try
                                {
                                    IMObject objSector = IMObject.LoadFromDB(sector);  //Dispose не делаем так как это реализовано в деструкторе
                                    objStations.Add(objSector);
                                    sectorIDs.Add(sector);
                                    StationBS temp = new StationBS();
                                    stationList.Add(temp);
                                }
                                catch
                                {
                                    CLogs.WriteError(ELogsWhat.Critical, "Не вдалося завантажити дані про сектор з ID = " + sector.Id.ToString());
                                }
                            }
                        }
                    }
                    finally
                    {
                        r.Close();
                        r.Destroy();
                    }

                }
           

            OwnerId = Station.objStation.GetI("OWNER_ID");
            stationList[0] = Station;
            int Loaded = LoadSectors();

            // Вытаскиваем "Особливі умови"
            SCDozvil = stationList[0].objStation.GetS("CUST_TXT16");
            SCVisnovok = stationList[0].objStation.GetS("CUST_TXT17");
            SCNote = stationList[0].objStation.GetS("CUST_TXT18");
            //----
            addValidVisnMonth = 6;

            var initParamGridSetEllipsisGridEvent = new GridEvent(InitParamGridSetEllipsis);

            InitParamGridEventTable.Add("KLon", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAddr", new GridEvent(InitParamGridSetEllipsis));

            for(int i = 1; i < 7;i++ )
            {
                InitParamGridEventTable.Add(string.Concat(EquipNameCellKey, i), initParamGridSetEllipsisGridEvent);
            }

            InitParamGridEventTable.Add("kAntType_1", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAntType_2", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAntType_3", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAntType_4", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAntType_5", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("kAntType_6", new GridEvent(InitParamGridSetEllipsis));

            
            InitParamGridEventTable.Add("filtType_1", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("filtType_2", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("filtType_3", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("filtType_4", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("filtType_5", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("filtType_6", new GridEvent(InitParamGridSetEllipsis));
             

          



            InitParamGridEventTable.Add("tx_freq_1", new GridEvent(InitParamGridLicense));
            InitParamGridEventTable.Add("tx_freq_2", new GridEvent(InitParamGridLicense));
            InitParamGridEventTable.Add("tx_freq_3", new GridEvent(InitParamGridLicense));
            InitParamGridEventTable.Add("tx_freq_4", new GridEvent(InitParamGridLicense));
            InitParamGridEventTable.Add("tx_freq_5", new GridEvent(InitParamGridLicense));
            InitParamGridEventTable.Add("tx_freq_6", new GridEvent(InitParamGridLicense));

            InitParamGridEventTable.Add("KObj_1", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("KObj_2", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("KObj_3", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("KObj_4", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("KObj_5", new GridEvent(InitParamGridSetEllipsis));
            InitParamGridEventTable.Add("KObj_6", new GridEvent(InitParamGridSetEllipsis));

            InitParamGridEventTable.Add("rx_pol_1", new GridEvent(InitParamGridPolarization));
            InitParamGridEventTable.Add("rx_pol_2", new GridEvent(InitParamGridPolarization));
            InitParamGridEventTable.Add("rx_pol_3", new GridEvent(InitParamGridPolarization));
            InitParamGridEventTable.Add("rx_pol_4", new GridEvent(InitParamGridPolarization));
            InitParamGridEventTable.Add("rx_pol_5", new GridEvent(InitParamGridPolarization));
            InitParamGridEventTable.Add("rx_pol_6", new GridEvent(InitParamGridPolarization));
            
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            int i;
            for (i = 0; i < stationList.Count; i++)
                stationList[i].Dispose();
            for (i = 1; i < objStations.Count; i++) //нулевой удалится в базовом классе
                objStations[i].Dispose();
            base.DisposeExec();
        }

        private int LoadSectors()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r.Open();

            int Loaded = 1;
            if (!r.IsEOF())
            {// Заявки нет     

                for (int i = 2; i <= 6; i++)
                {
                    int stationID = r.GetI("OBJ_ID" + i.ToString());
                    if (stationID != IM.NullI)
                    {
                        StationBS Station = stationList[i - 1];
                        OpenStation(ref Station, stationID);
                        stationList[i - 1] = Station;
                        Loaded++;
                    }
                }
            }
            r.Close();
            r.Destroy();

            return Loaded;
        }

        public int GetSectorCount()
        {
            int ResultCount = 0;
            for (int i = 0; i < 6; i++)
            {
                if (stationList[i].ID != IM.NullI)
                    ResultCount++;
            }

            return ResultCount;
        }

        public override int GetTxCount()
        {
            return GetSectorCount();
        }

        public int GetSectorObjIDByIndex(int Index)
        {
            return stationList[Index].ID;
        }

        private void OpenStation(ref StationBS Station, int StationID)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadOnly);

            r.Select("ID,EQUIP_ID,ANT_ID,POS_ID,DESIG_EMISSION,AZIMUTH,ELEVATION,AGL,GAIN,POLAR,AssignedFrequencies.RX_FREQ,AssignedFrequencies.TX_FREQ");

            r.SetWhere("ID", IMRecordset.Operation.Eq, StationID);

            r.Open();
            bool Loaded = false;
            if (!r.IsEOF())
            {
                Station.EquipID = r.GetI("EQUIP_ID");
                Station.AntID = r.GetI("ANT_ID");
                Station.PosID = r.GetI("POS_ID");

                Station.Azimuth = r.GetD("AZIMUTH");
                Station.Elevation = r.GetD("ELEVATION");
                Station.Agl = r.GetD("AGL");
                Station.Gain = r.GetD("GAIN");
                Station.Polar = r.GetS("POLAR");
                Station.DesigEmission = r.GetS("DESIG_EMISSION");

                //Station.RxFreq = r.GetD("AssignedFrequencies.RX_FREQ");
                //Station.TxFreq = r.GetD("AssignedFrequencies.TX_FREQ");

                RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation2, IM.NullI);
                recStation.Id = r.GetI("ID");
                Station.objStation = IMObject.LoadFromDB(recStation);

                Station.ID = StationID;
                Loaded = true;
            }
            else
            {
                //Station.ID = StationID;
                Station.objStation = objStation;

                Station.EquipID = objStation.GetI("EQUIP_ID");
                Station.AntID = objStation.GetI("ANT_ID");
                Station.PosID = objStation.GetI("POS_ID");

                Station.Azimuth = objStation.GetD("AZIMUTH");
                Station.Elevation = objStation.GetD("ELEVATION");
                Station.Agl = objStation.GetD("AGL");
                Station.Gain = objStation.GetD("GAIN");
                Station.Polar = objStation.GetS("POLAR");
                Station.DesigEmission = r.GetS("DESIG_EMISSION");

                Loaded = true;
            }

            r.Close();
            r.Destroy();

            if (Loaded)
            {
                LoadStationPosition(ref Station);
                LoadStationEquipment(ref Station);
                LoadStationAntenna(ref Station);
            }
        }

        private bool InRange(double Number, double MinBound, double MaxBound)
        {
            if (Number >= MinBound && Number <= MaxBound)
                return true;
            else
                return false;
        }


        // це ШПС (широкополосний сигнал) так/ні
        private bool IsBroadBandSignal(StationBS Station)
        {
            bool bResult = true;

            for (int i = 0; i < Station.FreqRxList.Count; i++)
            {
                if (InRange(Station.FreqRxList[i], 2400.0, 2483.5))
                    bResult = false;
            }

            for (int i = 0; i < Station.FreqTxList.Count; i++)
            {
                if (InRange(Station.FreqTxList[i], 2400.0, 2483.5))
                    bResult = false;
            }

            return !bResult;
        }

        private void CreateNewFields(ref StationBS Station)
        {
            bool SRT = true;
            bool MSR = false;
            bool MMR = false;

            for (int i = 0; i < stationList[0].FreqRxList.Count; i++)
            {
                //if (InRange(Station.FreqRxList[i], 2400.0, 2483.5))
                //  SRT = false;

                //Station
                if (InRange(stationList[0].FreqRxList[i], 40500.0, 42500.0))
                    MMR = true;

                if (InRange(stationList[0].FreqRxList[i], 3600.0, 3700.0) ||
                    InRange(stationList[0].FreqRxList[i], 10100.0, 10650.0) ||
                    InRange(stationList[0].FreqRxList[i], 12750.0, 13250.0) ||
                    InRange(stationList[0].FreqRxList[i], 24500.0, 26500.0) ||
                    InRange(stationList[0].FreqRxList[i], 27500.0, 29500.0))
                    MSR = true;
            }

            for (int i = 0; i < stationList[0].FreqTxList.Count; i++)
            {
                //if (InRange(Station.FreqTxList[i], 2400.0, 2483.5))
                //    SRT = false;

                if (InRange(stationList[0].FreqTxList[i], 40500.0, 42500.0))
                    MMR = true;

                if (InRange(stationList[0].FreqTxList[i], 3600.0, 3700.0) ||
                    InRange(stationList[0].FreqTxList[i], 10100.0, 10650.0) ||
                    InRange(stationList[0].FreqTxList[i], 12750.0, 13250.0) ||
                    InRange(stationList[0].FreqTxList[i], 24500.0, 26500.0) ||
                    InRange(stationList[0].FreqTxList[i], 27500.0, 29500.0))
                    MSR = true;
            }

            SRT = IsBroadBandSignal(stationList[0]);

            string oldName = Station.objStation.GetS("NAME");
            if (!SRT)
            {
                string name = "СРТ-" + stationList[0].objStation.GetI("ID");


                if (string.IsNullOrEmpty(oldName))
                {
                    Station.objStation["NAME"] = name;
                }

                Station.objStation["NETWORK_IDENT"] = name;
                if (MMR)
                    Station.objStation["STANDARD"] = "MмР";
                if (MSR)
                    Station.objStation["STANDARD"] = "МсР";
            }
            else
            {
                string name = "ШПС-" + stationList[0].objStation.GetI("ID");

                if (string.IsNullOrEmpty(oldName))
                {
                    Station.objStation["NAME"] = name;
                }

                Station.objStation["NETWORK_IDENT"] = name;
                Station.objStation["STANDARD"] = "ШР";
            }

            Station.objStation["ADM"] = stationList[0].objStation.GetS("ADM");

            if (string.IsNullOrEmpty(Station.DesigEmission) && stationList[0].objEquip != null)
            {
                Station.DesigEmission = stationList[0].objEquip.GetS("DESIG_EMISSION");
                Station.objStation["DESIG_EMISSION"] = Station.DesigEmission;
                double power = stationList[0].objEquip.GetD("MAX_POWER");
                if(power != IM.NullD)
                {
                    Station.objStation["PWR_ANT"] = power - 30;
                }
            }


            if (stationList[0].objAntenna != null)
            {
                Station.objStation["T_GAIN_TYPE"] = Station.objAntenna.GetS("GAIN_TYPE");
                Station.objStation["GAIN"] = Station.objAntenna.GetD("GAIN");
            }
        }

        private void CreateNewStation(ref List<IMObject> objStations, ref StationBS Station)
        {
            /*int PosID = 0;
            if (stationList[0].objPosition != null)
            {
                stationList[0].objPosition.SaveToDB();
                PosID = stationList[0].objPosition.GetI("ID");
                objStation["POS_ID"] = PosID;
            }*/

            Station.objStation = IMObject.New(ICSMTbl.itblMobStation2);
            int ID = Station.objStation.GetI("ID");

            Station.objStation["AZIMUTH"] = Station.Azimuth;
            Station.objStation["ELEVATION"] = Station.Elevation;
            Station.objStation["AGL"] = Station.Agl;
            Station.objStation["GAIN"] = Station.Gain;
            Station.objStation["POLAR"] = Station.Polar;

            Station.objStation["EQUIP_ID"] = Station.EquipID;
            Station.objStation["ANT_ID"] = Station.AntID;
            Station.objStation["POS_ID"] = Station.PosID;
            Station.objStation["OWNER_ID"] = OwnerId;
            Station.objStation["CUST_TXT1"] = NumberOut;
            Station.objStation.Put("CUST_DAT1", DateOut);
            Station.objStation["CUST_TXT6"] = NumberIn;

            CreateNewFields(ref Station);
            Station.ID = ID;
            objStations.Add(Station.objStation);
        }

        private void LoadStationPosition(ref StationBS Station)
        {
            RecordPtr recPosition = new RecordPtr(ICSMTbl.itblPositionMob2, IM.NullI);

            // cell.Value = EquipmentName;

            IMRecordset positionMob2 = new IMRecordset(ICSMTbl.itblPositionMob2, IMRecordset.Mode.ReadOnly);
            positionMob2.Select("ID");
            positionMob2.SetWhere("ID", IMRecordset.Operation.Eq, Station.PosID);
            positionMob2.Open();
            if (!positionMob2.IsEOF())
            {
                recPosition.Id = positionMob2.GetI("ID");
            }
            positionMob2.Close();
            positionMob2.Destroy();


            if (recPosition.Id != IM.NullI)
            {
                if (Station.objPosition == null)
                    Station.objPosition = new PositionState2();
                Station.objPosition.LoadStatePosition(recPosition.Id, recPosition.Table);
                Station.objPosition.GetAdminSiteInfo(Station.objPosition.AdminSiteId);
            }
        }

        private void LoadStationEquipment(ref StationBS Station)
        {
            RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipMob2, IM.NullI);

            // cell.Value = EquipmentName;

            IMRecordset equipMob2 = new IMRecordset(ICSMTbl.itblEquipMob2, IMRecordset.Mode.ReadOnly);
            equipMob2.Select("ID");
            equipMob2.SetWhere("ID", IMRecordset.Operation.Eq, Station.EquipID);
            equipMob2.Open();
            if (!equipMob2.IsEOF())
                recEquip.Id = equipMob2.GetI("ID");

            equipMob2.Close();
            equipMob2.Destroy();
            if (recEquip.Id != IM.NullI)
                Station.objEquip = IMObject.LoadFromDB(recEquip);
        }

        private void LoadStationAntenna(ref StationBS Station)
        {
            RecordPtr recAnt = new RecordPtr(ICSMTbl.itblAntennaMob2, IM.NullI);

            // cell.Value = EquipmentName;

            IMRecordset antMob2 = new IMRecordset(ICSMTbl.itblAntennaMob2, IMRecordset.Mode.ReadOnly);
            antMob2.Select("ID");
            antMob2.SetWhere("ID", IMRecordset.Operation.Eq, Station.AntID);
            antMob2.Open();
            if (!antMob2.IsEOF())
            {
                recAnt.Id = antMob2.GetI("ID");
            }
            antMob2.Close();
            antMob2.Destroy();

            if (recAnt.Id != IM.NullI)
            {
                Station.objAntenna = IMObject.LoadFromDB(recAnt);
                //cell.Value = objAntenna.GetS("NAME");
                //AutoFill(cell, grid);
            }
        }
        //===================================================
        /// <summary>
        /// Внутрення процедура добавления колонки для сектора.
        ///  устанавливает стили клетки.
        /// </summary>
        /// <returns>XML строка</returns>
        private Cell ExtendGrid(Grid grid, int rowIndex, int columnCount, string KeyPrefix)
        {
            Cell newCell = grid.AddColumn(rowIndex, "");
            newCell.Key = KeyPrefix + columnCount.ToString();
            newCell.FontName = grid.GetCellFromKey(KeyPrefix + "1").FontName;
            newCell.TextHeight = grid.GetCellFromKey(KeyPrefix + "1").TextHeight;
            newCell.FontStyle = grid.GetCellFromKey(KeyPrefix + "1").FontStyle;
            newCell.HorizontalAlignment = grid.GetCellFromKey(KeyPrefix + "1").HorizontalAlignment;
            newCell.VerticalAlignment = grid.GetCellFromKey(KeyPrefix + "1").VerticalAlignment;
            newCell.cellStyle = grid.GetCellFromKey(KeyPrefix + "1").cellStyle;
            newCell.CanEdit = grid.GetCellFromKey(KeyPrefix + "1").CanEdit;
            newCell.TextColor = grid.GetCellFromKey(KeyPrefix + "1").TextColor;

            int OldTabOrder = grid.GetCellFromKey(KeyPrefix + "1").TabOrder;

            if (OldTabOrder > 1 && rowIndex > 1)
                newCell.TabOrder = OldTabOrder + 10 * (columnCount - 1);
            else
                newCell.TabOrder = -1;

            InitParamGrid(newCell);

            return newCell;
        }

        //===================================================
        /// <summary>
        /// Создание XML строки для построения грида
        /// </summary>
        /// <returns>XML строка</returns>
        protected override string GetXMLParamGrid()
        {
            string XmlString =
                "<items>"
            + "<item key='header1'    type='lat'       flags=''      tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
            + "<item key='header2'    type='lat'       flags='s'     tab=''   display='Сектор 1'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LATITUDE
            + "<item key='mKLat'       type='lat'       flags=''     tab=''  display='Широта (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLat'       type='lat'       flags='s'     tab='1'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.LONGITUDE
            + "<item key='mKLon'       type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
            + "<item key='KLon'       type='lat'       flags='s'     tab='2'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // objPosition.ASL 
            + "<item key='mkAsl'      type=''       flags=''      tab=''  display='Висота поверхні Землі, м' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='kAsl'       type='double'  DBField='ASL'     flags='s'     tab='4'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // objPosition.GetFullAddress
            + "<item key='mkAddr'     type=''      flags=''      tab=''  display='Адреса встановлення РЕЗ' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kAddr'      type='string'      flags='s'     tab='3'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

            //objEquip.NAME
            + "<item key='mkName'     type=''      flags=''      tab=''  display='Назва/тип РЕЗ' ReadOnly='true' background='' bold='y'/>"
            + "<item key='" + EquipNameCellKey + "1'    type=''      flags='s'     tab='5'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            //PWR_ANT
            + "<item key='kPowerA'    type='lat'      flags=''      tab=''  display='Макс. потужність передавача, дБВт' ReadOnly='true' background='' bold=''/>"
            + "<item key='" + AntPowerCellKey + "1'     type='lat'      flags='s'     tab='6'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            //objEquip.BW
            + "<item key='kRxBand'    type='lat'      flags=''      tab=''  display='Ширина смуги, МГц' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='" + RxBandCellKey + "1'     type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            //objEquip.DESIG_EMISSION
            + "<item key='mkEmi'      type='lat'       flags=''      tab=''  display='Клас випромінювання' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='" + DesigEmissionCellKey + "1'  type='string'    flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            //objEquip.MODULATUIN
            + "<item key='kMod'       type='lat'      flags=''      tab=''  display='Типи і параметри модуляцій' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='" + ModulationCellKey + "1' type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            //objEquip.MBITPS
            + "<item key='kSpeed'     type='lat'      flags=''      tab=''  display='Швидкість цифрового потоку, Мбіт/c' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='" + SpeedCellKey + "1'      type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            // objEquip.CUST_TXT1
                // objEquip.CUST_DAT1
            + "<item key='kCert'      type='lat'      flags=''      tab=''  display='Сертифікат відповідності (номер / дата)' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='" + CertificateNumberCellKey + "1'     type='string'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "<item key='" + CertificateDateCellKey + "1'     type='datetime'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

             //Фильтр
            + "<item key='kFiltType'  type=''       flags=''     tab=''  display='Тип зовнішнього фільтру'     ReadOnly='true' background='' bold='' />"
            + "<item key='" + FiltType + "1'  type='string'       flags='s'    tab='7'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

            // AZIMUTH //**
            + "<item key='mkAzimuth'  type=''      flags=''      tab=''  display='Азимут макс. випромінювання, град' ReadOnly='true' background='' bold=''/>"
            + "<item key='KAzim_1'      type='double'  flags='s'     tab='8'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            //ELEVATION //**
            + "<item key='mkElevation' type=''      flags=''      tab=''  display='Кут місця макс. випромінювання, град' ReadOnly='true' background='' bold=''/>"
            + "<item key='KElev_1'       type='double'  flags='s'     tab='9'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            //objAntenna.NAME
            + "<item key='mkAntType'   type='lat'      flags=''      tab=''  display='Антена: тип' ReadOnly='true' background='' bold='y'/>"
            + "<item key='kAntType_1'    type='string'      flags='s'     tab='10'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            //AGL //**
            + "<item key='mkAgl'       type='lat'      flags=''      tab=''  display='Висота антени над рівнем землі, м' ReadOnly='true' background='' bold=''/>"
            + "<item key='KAgl_1'      type='double'   flags='s'     tab='11'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

            //objAntenna.GAIN
            + "<item key='mkGain'      type='lat'      flags=''      tab=''  display='Коефіцієнт підсилення, дБі' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='kGain_1'     type='double'   flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            //objAntenna.H_BEAMWIDTH
            + "<item key='kBandDS'     type='lat'      flags=''      tab=''  display='Ширина ДС антени (горизонт.), дБі' ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='band_ds_1'   type='lat'      flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

            //POLAR //**
            + "<item key='kRxPol'      type='lat'      flags=''      tab=''  display='Тип поляризації' ReadOnly='true' background='' bold=''/>"
             + "<item key='rx_pol_1'    type='string'   flags='s'     tab='12'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center'/>"

            // "AssignedFrequency".RX_FREQ
            + "<item key='kRxFreq'     type='lat'      flags=''      tab=''  display='Номінал частот приймання, МГц' ReadOnly='true' background='' bold='y'/>"
            + "<item key='rx_freq_1'   type='lat'      flags='s'     tab='13'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            // "AssignedFrequency".TX_FREQ
            + "<item key='kTxFreq'     type='lat'      flags=''      tab=''  display='Номінал частот передавання, МГц' ReadOnly='true' background='' bold='y'/>"
            + "<item key='tx_freq_1'   type='lat'      flags='s'     tab='14'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

            + "<item key='mKObj'       type=''       flags=''     tab=''  display='Об\"єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
            + "<item key='KObj_1'        type=''       flags='s'     tab=''  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
            + "</items>";
            return XmlString;
        }

        protected string GetPrefixFileName()
        {
            if (radioTech != CRadioTech.BNT)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (stationList[i].ID != IM.NullI)
                    {
                        if (!IsBroadBandSignal(stationList[i]))
                            return "СРТ-";
                    }
                }
                return "ШПС-";
            }
            else
            {
                return "БТ-";
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (stationList[0].objPosition != null) ? stationList[0].objPosition.Province : "";
            string city = (stationList[0].objPosition != null) ? stationList[0].objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if ((docType == DocType.VISN) || (docType == DocType.LYST_TR_CALLSIGN))
                fullPath += ConvertType.DepartmetToCode(DepartmentType.VFSR) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.LYST_TR_CALLSIGN))
                fullPath += GetPrefixFileName() + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else if (docType == DocType.DOZV_TR_SPEC)
            {
                string selectedProv = HelpFunction.getAreaCode(prov, city).Trim();
                string prefix = "СД";
                fullPath += prefix + "-" + selectedProv + "-" + (number.Length < 6 ? number.PadRight(6, '0') : number) + ".rtf";
            }
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        public override void ChangeOwner(int _newOwnerID)
        {
            for (int i = 0; i < 6; i++)
            {
                StationBS StationOne = stationList[i];
                if (StationOne.ID != IM.NullI)
                {
                    if (StationOne.objStation.GetI("OWNER_ID") != _newOwnerID)
                    {
                        StationOne.objStation.Put("OWNER_ID", _newOwnerID);
                        CJournal.CheckTable(recordID.Table, StationOne.ID, StationOne.objStation);
                        StationOne.objStation.SaveToDB();
                    }
                }
            }
            CJournal.CreateReport(applID, appType);
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            MobSta2App retStation = new MobSta2App(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            var retStation = (MobSta2App)newAppl;
            retStation.CopyFromMe(grid,
               (stationList[0].objEquip != null) ? stationList[0].objEquip.GetI("ID") : IM.NullI,
               (stationList[0].objAntenna != null) ? stationList[0].objAntenna.GetI("ID") : IM.NullI,
               (stationList[0].objStation != null) ? stationList[0].objStation.GetS("POLAR") : "",
               (stationList[0].objStation != null) ? stationList[0].objStation.GetD("PWR_ANT") : 0.0,
               (stationList[0].objStation != null) ? stationList[0].objStation.GetS("DESIG_EMISSION") : "");
            return retStation;
        }
        //===========================================================
        private void CopyFromMe(Grid grid, int equipID, int antID, string Polar, double power, string desigEmission)
        {
            Cell tmpCell = null;
            // Оборудование
            if ((equipID != 0) || (equipID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey(EquipNameCellKey + "1");
                stationList[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipMob2, equipID);
                stationList[0].EquipID = stationList[0].objEquip.GetI("ID");
                stationList[0].objStation["EQUIP_ID"] = stationList[0].EquipID;

                if (string.IsNullOrEmpty(desigEmission))
                {
                    //Возможно и не надо подтягивать..
                    stationList[0].objStation["DESIG_EMISSION"] = stationList[0].objEquip["DESIG_EMISSION"];
                }
                else
                {
                    stationList[0].objStation["DESIG_EMISSION"] = desigEmission;
                }

                stationList[0].objStation["PWR_ANT"] = stationList[0].objEquip.GetD("MAX_POWER") - 30;
                tmpCell.Value = stationList[0].objEquip.GetS("NAME");
                CellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }

            // Антенна
            if ((antID != 0) || (antID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("kAntType_1");
                stationList[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntennaMob2, antID);
                stationList[0].AntID = stationList[0].objAntenna.GetI("ID");
                stationList[0].objStation["ANT_ID"] = stationList[0].AntID;
                tmpCell.Value = stationList[0].objAntenna.GetS("NAME");
                CellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }

            // Мощность
            tmpCell = grid.GetCellFromKey(AntPowerCellKey + "1");
            tmpCell.Value = power.ToString();
            OnCellValidate(tmpCell, grid);

            // Поляризация
            tmpCell = grid.GetCellFromKey("rx_pol_1");
            tmpCell.Value = Polar;
            OnCellValidate(tmpCell, grid);
        }
        //============================================================
        /// <summary>
        /// Initialization for entire grid
        /// It's needed for setup columns for each sector
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Grid grid)
        {
            /*for (int r = 0; r < grid.GetRowCount(); r++)
                for (int c = 0; c < grid.GetColumnCount(r); c++)
                    InitParamGrid(grid.GetCell(r, c));
             */
            if (radioTech == CRadioTech.BNT)
            {
                grid.GetCellFromKey("rx_freq_1").CanEdit = false;
                grid.GetCellFromKey("rx_freq_1").row.Visible = false;
            }

            for (int i = 2; i <= 6; i++)
            {
                StationBS Station = stationList[i - 1];
                if (Station.ID != IM.NullI)
                {
                    Cell c0 = grid.AddColumn(0, "Сектор " + i.ToString());
                    c0.FontName = grid.GetCell(0, 0).FontName;
                    c0.TextHeight = grid.GetCell(0, 0).TextHeight;
                    c0.HorizontalAlignment = grid.GetCell(0, 0).HorizontalAlignment;
                    c0.VerticalAlignment = grid.GetCell(0, 0).VerticalAlignment;
                    c0.BackColor = grid.GetCell(0, 0).BackColor;
                    c0.FontStyle = grid.GetCell(0, 0).FontStyle;
                    c0.TabOrder = grid.GetCell(0, 0).TabOrder;

                    ExtendGrid(grid, 5, i, EquipNameCellKey);
                    ExtendGrid(grid, 6, i, AntPowerCellKey);
                    ExtendGrid(grid, 7, i, RxBandCellKey);
                    ExtendGrid(grid, 8, i, DesigEmissionCellKey);
                    ExtendGrid(grid, 9, i, ModulationCellKey);
                    ExtendGrid(grid, 10, i, SpeedCellKey);
                    ExtendGrid(grid, 11, i, CertificateNumberCellKey);
                    ExtendGrid(grid, 11, i, CertificateDateCellKey);
                    ExtendGrid(grid, 12, i, FiltType);
                    ExtendGrid(grid, 13, i, "KAzim_");
                    ExtendGrid(grid, 14, i, "KElev_");
                    ExtendGrid(grid, 15, i, "kAntType_");
                    ExtendGrid(grid, 16, i, "KAgl_");
                    ExtendGrid(grid, 17, i, "kGain_");
                    ExtendGrid(grid, 18, i, "band_ds_");
                    ExtendGrid(grid, 19, i, "rx_pol_");
                    ExtendGrid(grid, 20, i, "rx_freq_");
                    ExtendGrid(grid, 21, i, "tx_freq_");
                    ExtendGrid(grid, 22, i, "KObj_");
                }
            }
            base.InitParamGrid(grid);
        }

        //============================================================
        private void InitParamGridSetEllipsis(Cell cell, Grid grid)
        {
            cell.cellStyle = EditStyle.esEllipsis;
        }
        //============================================================
        private void InitParamGridLicense(Cell cell, Grid grid)
        {
            cell.cellStyle = EditStyle.esEllipsis;
            cell.ButtonText = "Ліц.";
        }
        //============================================================
        private void InitParamGridPolarization(Cell cell, Grid grid)
        {
            cell.cellStyle = EditStyle.esPickList;
            cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Standard);
        }
        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {

            if ((cell.Key == "filtType_1") || (cell.Key == "filtType_2") || (cell.Key == "filtType_3")
               || (cell.Key == "filtType_4") || (cell.Key == "filtType_5") || (cell.Key == "filtType_6"))
            {
                cell.cellStyle = EditStyle.esEllipsis;
            }

            if (!InitParamGridEventTable.ContainsKey(cell.Key))
            {
            }
            else
            {
                GridEvent delegateFunction = (GridEvent)InitParamGridEventTable[cell.Key];
                delegateFunction(cell, base.gridParam);
            }

            /*EventKeyTable[cell.] */
        }

        //===================================================
        /// <summary>
        /// Копирует общие параметры из первой станции в остальные
        /// </summary>
        /// <param name="index">номер сектора для заполнения парметрами и сохранения</param>
        void SaveParams(int index)
        {
            //-------------------------------------
            //Сохраняем фильтры
            {

                string FilterFields = "ID,OBJ_ID,OBJ_TABLE,EX_FILTER_ID,IS_TX";
                IMRecordset rsFilter = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadWrite);
                rsFilter.Select(FilterFields);
                rsFilter.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, objStations[index].GetI("ID"));
                rsFilter.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, recordID.Table);
                try
                {
                    rsFilter.Open();
                    if (!rsFilter.IsEOF())
                    {

                        //if ((stationList[index].fti.FilterIndex != 0) && (stationList[index].fti.FilterIndex != IM.NullI))
                        if (stationList[index].fti.FilterIndex != IM.NullI)
                        {
                            rsFilter.Edit();
                            rsFilter["EX_FILTER_ID"] = (stationList[index].fti.FilterIndex>0 ? stationList[index].fti.FilterIndex:IM.NullI);
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), rsFilter, FilterFields);
                            rsFilter.Update();
                        }
                        if ((stationList[index].fti.FilterIndex == 0) || (stationList[index].fti.FilterIndex == 2147483647))
                        {
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), null, FilterFields);
                            rsFilter.Delete();
                        }
                    }
                    else
                    {

                        if (((stationList[index].fti.FilterIndex != 0) || (stationList[index].fti.FilterIndex != IM.NullI)) && (stationList[index].fti.FilterIndex < 2147483647))
                        {

                            rsFilter.AddNew();
                            rsFilter["ID"] = IM.AllocID(PlugTbl._itblXnrfaStatExfltr, 1, -1);
                            rsFilter["OBJ_ID"] = objStations[index].GetI("ID");
                            rsFilter["OBJ_TABLE"] = recordID.Table;
                            rsFilter["EX_FILTER_ID"] = (stationList[index].fti.FilterIndex>0 ? stationList[index].fti.FilterIndex: IM.NullI);
                            if (radioTech.Contains("GSM"))
                                rsFilter["IS_TX"] = 0; //для GSM станций фильтер всегда на прием
                            else
                                rsFilter["IS_TX"] = 1; //Все остальные на передачу
                            //Fix for #2001
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), rsFilter, FilterFields);
                            rsFilter.Update();
                        }
                    }
                }
                finally
                {
                    rsFilter.Close();
                    rsFilter.Destroy();
                }
            }

        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            //сдвигаем сектора
            //bool needRepeat = true;
            /*while (needRepeat)
            {
               needRepeat = false;               
               for (int j = 0; j < stationList.Count-1; j++)
               {
                  //((stationList[j].ID == IM.NullI) && (stationList[j + 1].ID != IM.NullI))
                  if (stationList[j].ForDelete)
                  {
                     object tmp = stationList[j];
                     stationList[j] = stationList[j + 1];
                     stationList[j + 1] = tmp as StationBS;
                     needRepeat = true;
                  }
               }
            }*/

            if (stationList[0].objPosition == null)
                throw new Exception("Не задано (обрано) жодного сайту!");
            foreach (IMObject obj in objStations)
            {
                SaveParams(objStations.IndexOf(obj));
            }

            for (int j = 0; j < stationList.Count - 1; j++)
                for (int k = 0; k < stationList.Count - 1; k++)
                {

                    if (/*stationList[k].ID>stationList[k+1].ID ||*/ (stationList[k].ForDelete && !stationList[k + 1].ForDelete))
                    {
                        object tmp = stationList[k];
                        stationList[k] = stationList[k + 1];
                        stationList[k + 1] = tmp as StationBS;
                    }
                }


            int PosID = 0;
            if (stationList[0].objPosition != null)
            {
                //stationList[0].objPosition.Save();
                PosID = stationList[0].objPosition.Id;
                objStation["POS_ID"] = PosID;
            }
            objStation["ADM"] = "UKR";
            for (int i = 0; i < 6; i++)
            {
                if (stationList[i].ID != IM.NullI)
                {
                    if (newStation || (stationList[i].objStation.GetS("CUST_TXT1") != NumberOut || stationList[i].objStation.GetT("CUST_DAT1") != DateOut))
                    {
                        stationList[i].objStation.Put("CUST_TXT1", NumberOut);
                        stationList[i].objStation.Put("CUST_DAT1", DateOut);
                    }
                    stationList[i].objStation.Put("STANDARD", radioTech);
                    stationList[i].objStation.Put("CUST_CHB1", IsTechnicalUser);
                    stationList[i].objStation.Put("NETWORK_IDENT", stationList[0].objStation.GetS("NETWORK_IDENT"));
                    stationList[i].objStation.Put("NAME", stationList[0].objStation.GetS("NAME"));

                    CJournal.CheckTable(recordID.Table, stationList[i].ID, stationList[i].objStation);
                }
            }

            if (stationList[0].ID == IM.NullI)
            {
                StationBS Station = stationList[0];
                Station.ID = recordID.Id;
                //CreateNewFields(ref Station);
            }

            for (int i = 1; i <= 6; i++)
            {
                StationBS Station = stationList[i - 1];
                int ID = Station.ID;
                if (Station.ID != IM.NullI)
                {
                    CreateNewFields(ref Station);

                    if (i > 1)
                    {
                        objStation.Put("CUST_CHB1", IsTechnicalUser);   //???                     
                    }

                    Station.objStation["POS_ID"] = PosID;
                    // Сохраняем "Особливі умови"
                    Station.objStation["CUST_TXT16"] = SCDozvil;
                    Station.objStation["CUST_TXT17"] = SCVisnovok;
                    Station.objStation["CUST_TXT18"] = SCNote;
                    Station.objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус
                    SavePower(Station);
                    Station.objStation.SaveToDB();
                    SaveFrequencies(Station);
                }
            }

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r.Open();

            if (!r.IsEOF())
            {
                r.Edit();
                for (int i = 1; i <= 6; i++)
                {
                    StationBS Station = stationList[i - 1];
                    string ObjIdName = "OBJ_ID" + i.ToString();

                    if (Station.ForDelete)
                        r.Put(ObjIdName, IM.NullI);
                    else
                        r.Put(ObjIdName, Station.ID);
                }
                r.Update();
            }
            r.Close();
            r.Destroy();

            SaveFrequenciesToXnrfaAppl();

            // Сохраняем лицензии
            SaveLicence();
            return true;
        }

        //===========================================================
        /// <summary>
        /// Save frequencies for certaint Station.
        /// </summary>
        public void SavePower(StationBS Station)
        {
            double PowerA = 0;

            if (Station.objEquip != null)
                PowerA = Station.objEquip.GetD("MAX_POWER");

            double MobStationGain = Station.objStation.GetD("GAIN");
            double MobStationTxAddLosses = Station.objStation.GetD("TX_ADDLOSSES");

            if (PowerA == IM.NullD)
                PowerA = 0.0;

            if (MobStationTxAddLosses == IM.NullD)
                MobStationTxAddLosses = 0.0;

            if (MobStationGain == IM.NullD)
                MobStationGain = 0.0;

            Station.objStation["POWER"] = PowerA + MobStationGain - MobStationTxAddLosses - 30.0;
        }

        //===========================================================
        /// <summary>
        /// Save frequencies for certaint Station.
        /// </summary>
        public void SaveFrequencies(StationBS Station)
        {
            List<Classes.MobFreq> freqs = new List<Classes.MobFreq>();
            int countFreq = Math.Max(Station.FreqTxList.Count, Station.FreqRxList.Count);
            for (int i = 0; i < countFreq; i++)
            {
                Classes.MobFreq newFreq = new Classes.MobFreq();
                if (i < Station.FreqTxList.Count)
                    newFreq.TxMHz = Station.FreqTxList[i];
                if (i < Station.FreqRxList.Count)
                    newFreq.RxMHz = Station.FreqRxList[i];
                freqs.Add(newFreq);
            }
            Classes.MobFreqManager.SaveFreq(Station.ID, Classes.MobFreqManager.TypeFreq.MobStation2, freqs.ToArray());
        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {

            if (cell.Key == "KLon")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    //NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(lon, 0, "4DMS", "4DEC");
                    //IMPosition imPos = IMPosition.Convert(new IMPosition(lon, 0, "4DMS"), "4DEC");
                    //rxy.Latitude = rxy.Y = imPos.Lat;
                    //rxy.Longitude = rxy.X = imPos.Lon;
                    //cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (stationList[0].objPosition != null)
                    {
                        stationList[0].objPosition.LongDms = lon;
                        IndicateDifference(cell, stationList[0].objPosition.LonDiffersFromAdm);
                    }
                    Longitude = lon;
                }
            }
            else if (cell.Key == "KLat")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    NSPosition.RecordXY rxy = NSPosition.Position.convertPosition(0, lat, "4DMS", "4DEC");
                    IMPosition imPos = IMPosition.Convert(new IMPosition(0, lat, "4DMS"), "4DEC");
                    rxy.Latitude = rxy.Y = imPos.Lat;
                    rxy.Longitude = rxy.X = imPos.Lon;

                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (stationList[0].objPosition != null)
                    {
                        stationList[0].objPosition.LatDms = lat;
                        IndicateDifference(cell, stationList[0].objPosition.LatDiffersFromAdm);
                    }
                    Latitude = lat;
                }
            }
            else if (cell.Key == "kAddr")
            {
                if (stationList[0].objPosition != null)
                    IndicateDifference(cell, stationList[0].objPosition.AddrDiffersFromAdm);
            }

            if ((cell.Key == "KAzim_1")
              || (cell.Key == "KAzim_2")
              || (cell.Key == "KAzim_3")
              || (cell.Key == "KAzim_4")
              || (cell.Key == "KAzim_5")
              || (cell.Key == "KAzim_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(6));

                double Azimuth = 0.0;
                if (Double.TryParse(cell.Value, out Azimuth))
                {
                    Azimuth = Convert.ToInt32(Azimuth) % 360;
                    stationList[index - 1].Azimuth = Azimuth;
                    cell.Value = Azimuth.ToString("0");
                }
                else
                    stationList[index - 1].Azimuth = IM.NullD;

                stationList[index - 1].objStation["AZIMUTH"] = stationList[index - 1].Azimuth;
            }

            if ((cell.Key == "KElev_1")
              || (cell.Key == "KElev_2")
              || (cell.Key == "KElev_3")
              || (cell.Key == "KElev_4")
              || (cell.Key == "KElev_5")
              || (cell.Key == "KElev_6"))
            {

                int index = Convert.ToInt32(cell.Key.Substring(6));

                double Elevation = 0.0;
                if (Double.TryParse(cell.Value, out Elevation))
                {
                    Elevation = Convert.ToInt32(Elevation) % 90;
                    stationList[index - 1].Elevation = Elevation;
                    cell.Value = Elevation.ToString("0");
                }
                else
                    stationList[index - 1].Elevation = IM.NullD;

                stationList[index - 1].objStation["ELEVATION"] = stationList[index - 1].Elevation;
            }

            if ((cell.Key == "rx_pol_1")
             || (cell.Key == "rx_pol_2")
             || (cell.Key == "rx_pol_3")
             || (cell.Key == "rx_pol_4")
             || (cell.Key == "rx_pol_5")
             || (cell.Key == "rx_pol_6"))
            {
                string polarRx = CPolarization.getShortPolarization(cell.Value);

                int index = Convert.ToInt32(cell.Key.Substring(7));
                stationList[index - 1].Polar = polarRx;
                stationList[index - 1].objStation["POLAR"] = stationList[index - 1].Polar;

                SelectByShort(cell, polarRx);
            }

            if ((cell.Key == "rx_freq_1")
               || (cell.Key == "rx_freq_2")
               || (cell.Key == "rx_freq_3")
               || (cell.Key == "rx_freq_4")
               || (cell.Key == "rx_freq_5")
               || (cell.Key == "rx_freq_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                //stationList[index - 1].RxFreq = Convert.ToDouble(cell.Value);
                List<double> FreqRxList = ConvertType.ToDoubleList(cell);

                if (FreqRxList.Count > 0)
                {
                    if (CheckValidityFrequency(FreqRxList))
                    {
                        stationList[index - 1].FreqRxList = FreqRxList;
                        HelpFunction.ToString(cell, stationList[index - 1].FreqRxList);
                        stationList[index - 1].FreqRxListAsString = cell.Value;
                    }
                    else
                    {
                        //MessageBox.Show("Некорректно введеті частоти, всі частоти повинні бути додатніми числами");

                        ChangeColor(cell, Colors.badValue);
                        HelpFunction.ToString(cell, stationList[index - 1].FreqRxList);
                        stationList[index - 1].FreqRxListAsString = cell.Value;
                    }
                }
                else
                {
                    stationList[index - 1].FreqRxListAsString = "";
                    stationList[index - 1].FreqRxList = FreqRxList;
                }
            }

            if ((cell.Key == "tx_freq_1")
               || (cell.Key == "tx_freq_2")
               || (cell.Key == "tx_freq_3")
               || (cell.Key == "tx_freq_4")
               || (cell.Key == "tx_freq_5")
               || (cell.Key == "tx_freq_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                //stationList[index - 1].TxFreq = Convert.ToDouble(cell.Value);
                List<double> FreqTxList = ConvertType.ToDoubleList(cell);

                if (FreqTxList.Count > 0)
                {
                    if (CheckValidityFrequency(FreqTxList))
                    {
                        stationList[index - 1].FreqTxList = FreqTxList;
                        HelpFunction.ToString(cell, stationList[index - 1].FreqTxList);
                        stationList[index - 1].FreqTxListAsString = cell.Value;
                    }
                    else
                    {
                        //MessageBox.Show("Некорректно введеті частоти, всі частоти повинні бути додатніми числами");
                        ChangeColor(cell, Colors.badValue);
                        HelpFunction.ToString(cell, stationList[index - 1].FreqTxList);
                        stationList[index - 1].FreqTxListAsString = cell.Value;
                    }
                }
                else
                {
                    stationList[index - 1].FreqTxListAsString = "";
                    stationList[index - 1].FreqTxList = FreqTxList;
                }
            }

            if ((cell.Key == "KAgl_1")
              || (cell.Key == "KAgl_2")
              || (cell.Key == "KAgl_3")
              || (cell.Key == "KAgl_4")
              || (cell.Key == "KAgl_5")
              || (cell.Key == "KAgl_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));

                double Agl = 0;
                if (Double.TryParse(cell.Value, out Agl))
                {
                    if (Agl > 0)
                    {
                        stationList[index - 1].Agl = Agl;
                    }
                    else
                    {
                        // MessageBox.Show("Висота антени повинна бути додатнім числом");
                        stationList[index - 1].Agl = Agl;
                        ChangeColor(cell, Colors.badValue);
                    }
                }
                else
                    stationList[index - 1].Agl = IM.NullD;

                stationList[index - 1].objStation["AGL"] = stationList[index - 1].Agl;
                UpdateOneParamGrid(cell, grid);
            }

            if (cell.Key == "kAsl")
            {
                stationList[0].objPosition.Asl = cell.Value.ToDouble(IM.NullD);
                cell.Value = IM.RoundDeci(stationList[0].objPosition.Asl, 1).ToString();
            }

            if ((cell.Key == "filtType_1")
              || (cell.Key == "filtType_2")
              || (cell.Key == "filtType_3")
              || (cell.Key == "filtType_4")
              || (cell.Key == "filtType_5")
              || (cell.Key == "filtType_6"))
            {
                if (string.IsNullOrEmpty(cell.Value))
                {
                        int index = Convert.ToInt32(cell.Key.Substring(9))-1;
                        if (objStations.Count > index)
                            stationList[index].fti.FilterIndex = IM.NullI;
                }
            }
             

            if (cell.Key.StartsWith(AntPowerCellKey))
            {
                int index = GetCellIndexFromKey(cell.Key) - 1;
                double powerAnt;
                if(Double.TryParse(cell.Value, out powerAnt))
                {
                    if(stationList[index].objStation != null)
                        stationList[index].objStation["PWR_ANT"] = powerAnt;
                }
                else
                {
                    if(stationList[index].objStation != null)
                        stationList[index].objStation["PWR_ANT"] = IM.NullD;
                }
                powerAnt = stationList[index].objStation.GetD("PWR_ANT");

                if (powerAnt != IM.NullD)
                    cell.Value = powerAnt.ToString();
                else
                    cell.Value = "";

                /* для задачи 7591
                if(powerAnt != IM.NullD)
                {
                    cell.Value = powerAnt.ToString();
                    if (powerAnt>stationList[index].objEquip.GetD("MAX_POWER") || powerAnt<stationList[index].objEquip.GetD("MIN_POWER")) 
                    {
                        ChangeColor(cell, Colors.badValue);
                    }
                    else 
                    {
                        ChangeColor(cell, Colors.okvalue);
                    }
                }
                else
                  {
                    cell.Value = "";
                  }
                 */ 

///
            

            }
        }

        //============================================================
        public void LookupEquipment(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipMob2, "NAME", initialValue, true); 
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                int index = GetCellIndexFromKey(cell.Key) - 1;
                IMObject equipObj = IMObject.LoadFromDB(recEquip);
                StationBS station = stationList[index];
                station.objEquip = equipObj;
                station.EquipID = equipObj.GetI("ID");
                station.DesigEmission = equipObj.GetS("DESIG_EMISSION");
                IMObject stationObj = station.objStation;
                if(stationObj != null)
                {
                    stationObj["EQUIP_ID"] = station.EquipID;
                    stationObj["PWR_ANT"] = equipObj.GetD("MAX_POWER") - 30;
                    stationObj.Put("DESIG_EMISSION", station.DesigEmission);
                }
                cell.Value = equipObj.GetS("NAME");
                CellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }
        //============================================================
        public void LookupAntenna(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";
            // Выбераем запись из таблицы
            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Search of the antenna"), ICSMTbl.itblAntennaMob2, param);
            if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                stationList[index - 1].objAntenna = IMObject.LoadFromDB(RecAnt);
                stationList[index - 1].AntID = stationList[index - 1].objAntenna.GetI("ID");
                stationList[index - 1].objStation["ANT_ID"] = stationList[index - 1].AntID;
                cell.Value = stationList[index - 1].objAntenna.GetS("NAME");

                CellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }
        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key.StartsWith(EquipNameCellKey))
            {// Выбор оборудования
                LookupEquipment(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "kAsl")
            {
                if (stationList[0].objPosition != null)
                {
                    double lon = stationList[0].objPosition.LonDec;
                    double lat = stationList[0].objPosition.LatDec;

                    if (lon != IM.NullD && lat != IM.NullD)
                    {
                        double asl = IMCalculate.CalcALS(lon, lat, "4DEC");
                        if (asl != IM.NullD)
                        {
                            cell.Value = IM.RoundDeci(asl, 1).ToString();
                            OnCellValidate(cell, grid);
                        }
                    }
                }
            }
            else if (cell.Key == "kAddr")
            {       

                if (ShowMessageReference(stationList[0].objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms =  Longitude;
                    newPos.LatDms =  Latitude;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionMob2, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        stationList[0].objPosition = newPos;
                        stationList[0].PosID = stationList[0].objPosition.Id;
                        stationList[0].objStation["POS_ID"] = stationList[0].PosID;
                        stationList[0].objPosition.GetAdminSiteInfo(stationList[0].objPosition.AdminSiteId);
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon"), gridParam);
                    }
                }                
            }
            else if ((cell.Key == "kAntType_1")
                 || (cell.Key == "kAntType_2")
                 || (cell.Key == "kAntType_3")
                 || (cell.Key == "kAntType_4")
                 || (cell.Key == "kAntType_5")
                 || (cell.Key == "kAntType_6"))
            {// Выбор антены
                LookupAntenna(cell, grid, cell.Value, true);
            }
            else if ((cell.Key == "filtType_1")
              || (cell.Key == "filtType_2")
              || (cell.Key == "filtType_3")
              || (cell.Key == "filtType_4")
              || (cell.Key == "filtType_5")
              || (cell.Key == "filtType_6"))
            {
                string param = "{NAME=\"*" + cell.Value + "*\"}";
                // Выбераем запись из таблицы
                RecordPtr RecFlt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach for filter"), PlugTbl._itblXnrfaExternFilter, param);
                if ((RecFlt.Id > 0) && (RecFlt.Id < IM.NullI))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(9))-1;
                    //stationList[index].fti.FilterIndex = RecFlt.Id;
                    stationList[index].fti.FilterIndex = (RecFlt.Id>0 ? RecFlt.Id : IM.NullI);
                    stationList[index].fti.FilterName = "";
                    IMRecordset r = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, RecFlt.Id);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        stationList[index].fti.FilterName = r.GetS("NAME");
                    }
                    r.Close();
                    r.Destroy();

                    cell.Value = stationList[index].fti.FilterName;
                    CellValidate(cell, grid);
                }
            }
            else if (cell.Key == "KLon")
            {
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionMob2, 1, Longitude, Latitude);
                if (newPos != null)
                {
                    stationList[0].objPosition = newPos;
                    if (stationList[0].objPosition != null)
                    {
                        stationList[0].PosID = stationList[0].objPosition.Id;
                        stationList[0].objStation["POS_ID"] = stationList[0].PosID;
                        stationList[0].objPosition.GetAdminSiteInfo(stationList[0].objPosition.AdminSiteId);

                        if (stationList[0].objPosition.AdminSiteId > 0) AttachmentControl.CopyAttachment(ICSMTbl.SITES, stationList[0].objPosition.AdminSiteId, ICSMTbl.itblPositionMob2, stationList[0].objPosition.Id);

                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon"), gridParam);
                    }
                }
                //Position.Dispose();
            }
            else if ((cell.Key == "KObj_1")
                || (cell.Key == "KObj_2")
                || (cell.Key == "KObj_3")
                || (cell.Key == "KObj_4")
                || (cell.Key == "KObj_5")
                || (cell.Key == "KObj_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));
                int ID = stationList[index - 1].ID;
                RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation2, ID);
                recStation.UserEdit();
            }
            else if ((cell.Key == "tx_freq_1")
                || (cell.Key == "tx_freq_2")
                || (cell.Key == "tx_freq_3")
                || (cell.Key == "tx_freq_4")
                || (cell.Key == "tx_freq_5")
                || (cell.Key == "tx_freq_6"))
            {   
                RefreshLicenceList();
            }
            else
            {
                base.OnPressButton(cell, grid);
            }
        }

        public override double GetBw()
        {
            return stationList[0].objEquip != null ? (stationList[0].objEquip.GetD("BW")) / 1000.0 : 0.0;
        }

        public double GetBw(int Num_Sector)
        {
            return stationList[Num_Sector].objEquip != null ? (stationList[Num_Sector].objEquip.GetD("BW")) / 1000.0 : 0.0;
        }


        public override string GetProvince()
        {
            return stationList[0].objPosition != null ? stationList[0].objPosition.Province : null;
        }

        internal override CLicence.Band[] GetRxTxBands(RxTx type, string region)
        {
            // region parameter will be ignored
            //double BW = GetBw();
            List<CLicence.Band> bandList = new List<CLicence.Band>();
            for (int i = 0; i < stationList.Count; i++)
            {
                double BW = GetBw(i);
                int ID = stationList[i].ID;
                if (ID != IM.NullI)
                {
                    bool txListEmpty = stationList[i].FreqTxList.Count == 0;
                    bool rxListEmpty = stationList[i].FreqRxList.Count == 0;
                    if (txListEmpty && rxListEmpty)
                    {
                        IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                            recMobStaFreqs2.Open();
                            while (!recMobStaFreqs2.IsEOF())
                            {
                                double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                                if (RxFreq != IM.NullD)
                                    stationList[i].FreqRxList.Add(RxFreq);
                                double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                                if (TxFreq != IM.NullD)
                                    stationList[i].FreqTxList.Add(TxFreq);
                                recMobStaFreqs2.MoveNext();
                            }
                        }
                        finally
                        {
                            recMobStaFreqs2.Destroy();
                        }
                    }

                    int bandId = 0;
                    if (type != RxTx.Rx)
                    foreach (double TxFreq in stationList[i].FreqTxList)
                        bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(TxFreq - BW / 2, 6), IM.RoundDeci(TxFreq, 6), IM.RoundDeci(TxFreq + BW / 2, 6), CLicence.Band.Parity.Down));
                    if (type != RxTx.Tx)
                    foreach (double RxFreq in stationList[i].FreqRxList)
                        bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(RxFreq - BW / 2, 6), IM.RoundDeci(RxFreq, 6), IM.RoundDeci(RxFreq + BW / 2, 6), CLicence.Band.Parity.Up));
                }
            }
            return bandList.ToArray();
        }

        private bool CheckValidityFrequency(List<double> FreqList)
        {
            foreach (double Freq in FreqList)
                if (Freq <= 0.0)
                {
                    return false;
                }
            return true;
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon")
            {
                if (stationList[0].objPosition != null)
                {
                    Longitude = stationList[0].objPosition.LongDms;
                    Latitude = stationList[0].objPosition.LatDms;
                    cell.Value = (Longitude != IM.NullD) ? HelpFunction.DmsToString(Longitude, EnumCoordLine.Lon) : "";
                    OnCellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }

            else if (cell.Key == "kAsl")
            {
                if (stationList[0].objPosition != null)
                {
                    double masl = stationList[0].objPosition.Asl;

                    if (masl != IM.NullD)
                    {
                        cell.Value = Convert.ToInt32(masl).ToString("0");
                        AutoFill(cell, grid);
                    }
                }
            }

            else if (cell.Key.StartsWith(EquipNameCellKey))
            {
                int index = GetCellIndexFromKey(cell.Key) - 1;
                if(stationList[index].objEquip != null)
                {
                    cell.Value = stationList[index].objEquip.GetS("NAME");
                    AutoFill(cell, grid);
                }
            }
            else if ((cell.Key == "kAntType_1")
                 || (cell.Key == "kAntType_2")
                 || (cell.Key == "kAntType_3")
                 || (cell.Key == "kAntType_4")
                 || (cell.Key == "kAntType_5")
                 || (cell.Key == "kAntType_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (stationList[index - 1].objAntenna != null)
                {
                    cell.Value = stationList[index - 1].objAntenna.GetS("NAME");
                    AutoFill(cell, grid);
                }
            }
            else if ((cell.Key == "rx_pol_1")
              || (cell.Key == "rx_pol_2")
              || (cell.Key == "rx_pol_3")
              || (cell.Key == "rx_pol_4")
              || (cell.Key == "rx_pol_5")
              || (cell.Key == "rx_pol_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                if (stationList[index - 1].Polar != null)
                    SelectByShort(cell, stationList[index - 1].Polar);
            }

            
            if ((cell.Key == "filtType_1")
                 || (cell.Key == "filtType_2")
                 || (cell.Key == "filtType_3")
                 || (cell.Key == "filtType_4")
                 || (cell.Key == "filtType_5")
                 || (cell.Key == "filtType_6"))
            {
                
                int index = Convert.ToInt32(cell.Key.Substring(9));

                int msID = objStations[index-1].GetI("ID");

                stationList[index-1].fti = GetFilterTypeInfoByStationID(msID, ICSMTbl.itblMobStation2);

                cell.Value = stationList[index-1].fti.FilterName;
                
                  
            }
             


            if ((cell.Key == "rx_freq_1")
             || (cell.Key == "rx_freq_2")
             || (cell.Key == "rx_freq_3")
             || (cell.Key == "rx_freq_4")
             || (cell.Key == "rx_freq_5")
             || (cell.Key == "rx_freq_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                stationList[index - 1].FreqRxList = new List<double>();
                int ID = stationList[index - 1].ID;

                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
                recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                recMobStaFreqs2.Open();
                while (!recMobStaFreqs2.IsEOF())
                {
                    double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                    if (RxFreq != IM.NullD)
                        stationList[index - 1].FreqRxList.Add(RxFreq);
                    recMobStaFreqs2.MoveNext();
                }

                recMobStaFreqs2.Close();
                recMobStaFreqs2.Destroy();

                if (stationList[index - 1].FreqRxList.Count > 0)
                {
                    HelpFunction.ToString(cell, stationList[index - 1].FreqRxList);
                    stationList[index - 1].FreqRxListAsString = cell.Value;
                }
                else
                {
                    stationList[index - 1].FreqRxListAsString = "";
                }
            }

            if ((cell.Key == "tx_freq_1")
               || (cell.Key == "tx_freq_2")
               || (cell.Key == "tx_freq_3")
               || (cell.Key == "tx_freq_4")
               || (cell.Key == "tx_freq_5")
               || (cell.Key == "tx_freq_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                stationList[index - 1].FreqTxList = new List<double>();
                int ID = stationList[index - 1].ID;

                IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
                recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
                recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);
                recMobStaFreqs2.Open();
                while (!recMobStaFreqs2.IsEOF())
                {
                    double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                    if (TxFreq != IM.NullD)
                        stationList[index - 1].FreqTxList.Add(TxFreq);

                    recMobStaFreqs2.MoveNext();
                }

                recMobStaFreqs2.Close();
                recMobStaFreqs2.Destroy();

                if (stationList[index - 1].FreqTxList.Count > 0)
                {
                    HelpFunction.ToString(cell, stationList[index - 1].FreqTxList);
                    stationList[index - 1].FreqTxListAsString = cell.Value;
                }
                else
                {
                    stationList[index - 1].FreqTxListAsString = "";
                }
            }

            if ((cell.Key == "KAzim_1")
              || (cell.Key == "KAzim_2")
              || (cell.Key == "KAzim_3")
              || (cell.Key == "KAzim_4")
              || (cell.Key == "KAzim_5")
              || (cell.Key == "KAzim_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(6));
                if (stationList[index - 1].Azimuth != IM.NullD)
                    cell.Value = stationList[index - 1].Azimuth.ToString("0");
            }

            if ((cell.Key == "KElev_1")
              || (cell.Key == "KElev_2")
              || (cell.Key == "KElev_3")
              | (cell.Key == "KElev_4")
              || (cell.Key == "KElev_5")
              || (cell.Key == "KElev_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(6));

                if (stationList[index - 1].Elevation != IM.NullD)
                    cell.Value = stationList[index - 1].Elevation.ToString("0");
            }

            if ((cell.Key == "KAgl_1")
               || (cell.Key == "KAgl_2")
               || (cell.Key == "KAgl_3")
               || (cell.Key == "KAgl_4")
               || (cell.Key == "KAgl_5")
               || (cell.Key == "KAgl_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));

                if (stationList[index - 1].objStation != null)
                {
                    double magl = stationList[index - 1].objStation.GetD("AGL");
                    if (magl != IM.NullD)
                        cell.Value = Convert.ToInt32(magl).ToString();
                }
            }

            if (cell.Key.StartsWith(SpeedCellKey))
            {
                int index = GetCellIndexFromKey(cell.Key) - 1;
                if(stationList[index].objEquip != null)
                {
                    double mBitPs = stationList[index].objEquip.GetD("MBITPS");
                    if (mBitPs != IM.NullD)
                        cell.Value = mBitPs.ToString();
                }
            }

            if ((cell.Key == "KObj_1")
             || (cell.Key == "KObj_2")
             || (cell.Key == "KObj_3")
             || (cell.Key == "KObj_4")
             || (cell.Key == "KObj_5")
             || (cell.Key == "KObj_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));
                if (stationList[index - 1].objStation != null)
                    cell.Value = stationList[index - 1].objStation.GetI("ID").ToString(); //objStation.GetS("NAME");
            }

        }
        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            if (cell.Key.StartsWith(EquipNameCellKey))
            {
                int cellIndex = GetCellIndexFromKey(cell.Key);
                int index = cellIndex - 1;
                string cellIndexString = cellIndex.ToString();
                string powerCellKey = AntPowerCellKey + cellIndexString;
                string rxBandCellKey = RxBandCellKey + cellIndexString;
                string desigEmissionCellKey = DesigEmissionCellKey + cellIndexString;
                string modulationCellKey = ModulationCellKey + cellIndexString;
                string certificateNumberCellKey = CertificateNumberCellKey + cellIndexString;
                string certificateDateCellKey = CertificateDateCellKey + cellIndexString;
                if(stationList[index].objEquip != null)
                {
                    IMObject equipObj = stationList[index].objEquip;
                    SafeFillCellByKey(grid, certificateNumberCellKey, equipObj.GetS("CUST_TXT1"), false);
                    DateTime dt = stationList[0].objEquip.GetT("CUST_DAT1");
                    string certDate = dt.Year > 1990 ? dt.ToString("dd.MM.yyyy") : "";
                    SafeFillCellByKey(grid, certificateDateCellKey, certDate, false);
                    if(string.IsNullOrEmpty(stationList[index].DesigEmission))
                        stationList[index].DesigEmission = equipObj.GetS("DESIG_EMISSION");
                    SafeFillCellByKey(grid, desigEmissionCellKey, stationList[index].DesigEmission, true);
                    SafeFillCellByKey(grid, rxBandCellKey, (equipObj.GetD("BW") / 1000.0).ToString(), true);
                    SafeFillCellByKey(grid, powerCellKey, (stationList[index].objStation.GetD("PWR_ANT")).ToString(), true);
                    string strModulation = equipObj.GetS("MODULATION");
                    if (!string.IsNullOrEmpty(strModulation))
                    {
                        int modValue = Convert.ToInt32(strModulation);
                        SafeFillCellByKey(grid, modulationCellKey, Modulation[modValue], false);
                    }
                    else
                    {
                        SafeFillCellByKey(grid, modulationCellKey, "", false);
                    }
                }
                else
                {
                    SafeFillCellByKey(grid, certificateNumberCellKey, "", false);
                    SafeFillCellByKey(grid, certificateDateCellKey, "", false);
                    SafeFillCellByKey(grid, desigEmissionCellKey, "", false);
                    SafeFillCellByKey(grid, rxBandCellKey, "", false);
                    SafeFillCellByKey(grid, powerCellKey, "", false);
                    SafeFillCellByKey(grid, modulationCellKey, "", false);
                }
            }
            else if ((cell.Key == "kAntType_1")
                   || (cell.Key == "kAntType_2")
                   || (cell.Key == "kAntType_3")
                   || (cell.Key == "kAntType_4")
                   || (cell.Key == "kAntType_5")
                   || (cell.Key == "kAntType_6"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));

                if (stationList[index - 1].objAntenna != null)
                {
                    SafeFillCellByKey(grid, "kGain_" + index.ToString(), stationList[index - 1].objAntenna.GetD("GAIN").Round(2).ToString(), true);
                    //fillCell = grid.GetCellFromKey("kGain_"+index.ToString());
                    //fillCell.Value = stationList[index-1].objAntenna.GetD("GAIN").ToString("0");
                    //OnCellValidate(fillCell, grid);

                    SafeFillCellByKey(grid, "band_ds_" + index.ToString(), stationList[index - 1].objAntenna.GetD("H_BEAMWIDTH").Round(2).ToString(), true);
                    //fillCell = grid.GetCellFromKey("band_ds_" + index.ToString());
                    //fillCell.Value = stationList[index-1].objAntenna.GetD("H_BEAMWIDTH").ToString("0");
                    //OnCellValidate(fillCell, grid);

                }
                else
                {
                    SafeFillCellByKey(grid, "kGain_" + index.ToString(), "", false);
                    SafeFillCellByKey(grid, "band_ds_" + index.ToString(), "", false);
                }
            }
            if (cell.Key == "KLon")
            {
                if (stationList[0].objPosition != null)
                {
                    double latDMS = stationList[0].objPosition.LatDms;
                    SafeFillCellByKey(grid, "KLat", (latDMS != IM.NullD) ? HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat) : "", true);
                    OnCellValidate(grid.GetCellFromKey("KLat"), grid);

                    double asl = stationList[0].objPosition.Asl;
                    SafeFillCellByKey(grid, "kAsl", asl.ToStringNullD("0"), true);
                    //OnCellValidate(fillCell, grid);

                    SafeFillCellByKey(grid, "kAddr", stationList[0].objPosition.FullAddressAuto, true);
                    OnCellValidate(grid.GetCellFromKey("kAddr"), grid);

                }
                else
                {
                    SafeFillCellByKey(grid, "KLon", "", false);
                    SafeFillCellByKey(grid, "kAsl", "", false);
                    SafeFillCellByKey(grid, "kAddr", "", false);
                }
            }
        }

        public override void AddSector(Grid grid)
        {
            if (Standard == CRadioTech.BNT)
                return; //У БНТ нет секторов
            int columCount = grid.GetColumnCount(0);
            if (columCount < 7)
            {
                StationBS Station = stationList[columCount - 1];

                Station.Azimuth = IM.NullD;
                Station.Elevation = stationList[0].Elevation;
                Station.Gain = stationList[0].Gain;

                Station.AntID = stationList[0].AntID;
                Station.PosID = stationList[0].PosID;
                Station.EquipID = stationList[0].EquipID;

                Station.Azimuth = stationList[0].Azimuth;
                Station.Agl = stationList[0].Agl;
                Station.Polar = stationList[0].Polar;
                Station.fti = stationList[0].fti;

                LoadStationPosition(ref Station);
                LoadStationAntenna(ref Station);
                LoadStationEquipment(ref Station);

                //stationList.RemoveAt(columCount);
                //stationList.Insert(columCount, Station);
                stationList[columCount - 1] = Station;


                Cell c0 = grid.AddColumn(0, "Сектор " + columCount.ToString());
                c0.FontName = grid.GetCell(0, 0).FontName;
                c0.TextHeight = grid.GetCell(0, 0).TextHeight;
                c0.HorizontalAlignment = grid.GetCell(0, 0).HorizontalAlignment;
                c0.VerticalAlignment = grid.GetCell(0, 0).VerticalAlignment;
                c0.BackColor = grid.GetCell(0, 0).BackColor;
                c0.FontStyle = grid.GetCell(0, 0).FontStyle;
                c0.TabOrder = grid.GetCell(0, 0).TabOrder;


                Cell equipCell = ExtendGrid(grid, 5, columCount, EquipNameCellKey);
                Cell antPowerCell = ExtendGrid(grid, 6, columCount, AntPowerCellKey);
                ExtendGrid(grid, 7, columCount, RxBandCellKey);
                ExtendGrid(grid, 8, columCount, DesigEmissionCellKey);
                ExtendGrid(grid, 9, columCount, ModulationCellKey);
                ExtendGrid(grid, 10, columCount, SpeedCellKey);
                ExtendGrid(grid, 11, columCount, CertificateNumberCellKey);
                ExtendGrid(grid, 11, columCount, CertificateDateCellKey);

                Cell c1 = ExtendGrid(grid, 12, columCount,"filtType_");//Math.Pow(10.0, Station.MaxPower / 10.0)

                Cell azimCell = ExtendGrid(grid, 13, columCount, "KAzim_");
                Cell elevCell = ExtendGrid(grid, 14, columCount, "KElev_");
                ExtendGrid(grid, 15, columCount, "kAntType_");
                Cell aglCell = ExtendGrid(grid, 16, columCount, "KAgl_");
                ExtendGrid(grid, 17, columCount, "kGain_");
                ExtendGrid(grid, 18, columCount, "band_ds_");
                Cell rxPolarCell = ExtendGrid(grid, 19, columCount, "rx_pol_");
                Cell rxFreqCell = ExtendGrid(grid, 20, columCount, "rx_freq_");
                Cell txFreqCell = ExtendGrid(grid, 21, columCount, "tx_freq_");


                Station.fti = stationList[columCount - 1].fti;
                c1.Value = Station.fti.FilterName;
                CellValidate(c1, grid);

                CreateNewStation(ref objStations, ref Station);


                UpdateOneParamGrid(equipCell, grid);
                UpdateOneParamGrid(antPowerCell, grid);
                UpdateOneParamGrid(azimCell, grid);
                UpdateOneParamGrid(elevCell, grid);
                UpdateOneParamGrid(aglCell, grid);
                UpdateOneParamGrid(rxPolarCell, grid);
                UpdateOneParamGrid(rxFreqCell, grid);
                UpdateOneParamGrid(txFreqCell, grid);
                UpdateOneParamGrid(grid.GetCellFromKey("kAntType_" + columCount.ToString()), grid);

                Cell ca = ExtendGrid(grid, 22, columCount, "KObj_");
                UpdateOneParamGrid(ca, grid);
            }
        }

        public override void RemoveSector(Grid grid)
        {
            int countSector = GetSectorCount();
            if (countSector > 1)
            {
                HelpClasses.Forms.FSectorRemove form = new XICSM.UcrfRfaNET.HelpClasses.Forms.FSectorRemove();
                int index = 1;
                foreach (StationBS item in stationList)
                    if (item.ID != IM.NullI && !item.ForDelete)
                    {
                        form.cbSecs.Items.Add("Сектор " + index.ToString());
                        index++;
                    }
                form.cbSecs.SelectedIndex = 0;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    index = Convert.ToInt32(form.cbSecs.Text.Substring(7));
                    int indexForDelete = index;
                    int indexStation = IM.NullI;
                    for (int j = 0; j < stationList.Count; j++)
                    {
                        if (stationList[j].ID != IM.NullI)
                            index--;
                        if (index == 0)
                        {
                            indexStation = j;
                            break;
                        }
                    }

                    if (indexStation != IM.NullI)
                    {
                        AddDeletedSector(stationList[indexStation].ID, recordID.Table);
                        stationList[indexStation].ID = IM.NullI;
                        stationList[indexStation].ForDelete = true;


                        int columnCount = grid.GetColumnCount(12) - 1;
                        for (int i = indexForDelete; i < columnCount; i++)
                        {
                            string curIndex = i.ToString();
                            string nextIndex = (i+1).ToString();
                            CopyCell(EquipNameCellKey + curIndex, EquipNameCellKey + nextIndex, grid);
                            CopyCell(AntPowerCellKey + curIndex, AntPowerCellKey + nextIndex, grid);
                            CopyCell(RxBandCellKey + curIndex, RxBandCellKey + nextIndex, grid);
                            CopyCell(DesigEmissionCellKey + curIndex, DesigEmissionCellKey + nextIndex, grid);
                            CopyCell(ModulationCellKey + curIndex, ModulationCellKey+nextIndex, grid);
                            CopyCell(SpeedCellKey + curIndex, SpeedCellKey + nextIndex, grid);
                            CopyCell(CertificateNumberCellKey + curIndex, CertificateNumberCellKey + nextIndex, grid);
                            CopyCell(CertificateDateCellKey + curIndex, CertificateDateCellKey + nextIndex, grid);
                            CopyCell("filtType_" + i.ToString(), "filtType_" + (i + 1).ToString(), grid);
                            CopyCell("KAzim_" + i.ToString(), "KAzim_" + (i + 1).ToString(), grid);
                            CopyCell("KElev_" + i.ToString(), "KElev_" + (i + 1).ToString(), grid);
                            CopyCell("kAntType_" + i.ToString(), "kAntType_" + (i + 1).ToString(), grid);
                            CopyCell("KAgl_" + i.ToString(), "KAgl_" + (i + 1).ToString(), grid);
                            CopyCell("kGain_" + i.ToString(), "kGain_" + (i + 1).ToString(), grid);
                            CopyCell("band_ds_" + i.ToString(), "band_ds_" + (i + 1).ToString(), grid);
                            CopyCell("rx_pol_" + i.ToString(), "rx_pol_" + (i + 1).ToString(), grid);
                            CopyCell("rx_freq_" + i.ToString(), "rx_freq_" + (i + 1).ToString(), grid);
                            CopyCell("tx_freq_" + i.ToString(), "tx_freq_" + (i + 1).ToString(), grid);
                            CopyCell("KObj_" + i.ToString(), "KObj_" + (i + 1).ToString(), grid);
                        }
                        grid.RemoveColumn(0);
                        grid.RemoveColumn(5);
                        grid.RemoveColumn(6);
                        grid.RemoveColumn(7);
                        grid.RemoveColumn(8);
                        grid.RemoveColumn(9);
                        grid.RemoveColumn(10);
                        grid.RemoveColumn(11);
                        grid.RemoveColumn(11);
                        grid.RemoveColumn(12);
                        grid.RemoveColumn(13);
                        grid.RemoveColumn(14);
                        grid.RemoveColumn(15);
                        grid.RemoveColumn(16);
                        grid.RemoveColumn(17);
                        grid.RemoveColumn(18);
                        grid.RemoveColumn(19);
                        grid.RemoveColumn(20);
                        grid.RemoveColumn(21);
                        grid.RemoveColumn(22);
                        grid.Edited = true;
                    }

                    for (int j = 0; j < stationList.Count - 1; j++)
                        for (int k = 0; k < stationList.Count - 1; k++)
                        {

                            if (/*stationList[k].ID>stationList[k+1].ID ||*/ (stationList[k].ForDelete && !stationList[k + 1].ForDelete))
                            {
                                object tmp = stationList[k];
                                stationList[k] = stationList[k + 1];
                                stationList[k + 1] = tmp as StationBS;
                            }
                        }
                }
            }
            //int columCount = grid.GetColumnCount(0);

            //int ForDelete = columCount - 2;

            //StationBS Station = stationList[columCount - 2];
            //int StationID = Station.ID;
            //AddDeletedSector(Station.ID, recordID.Table);
            //Station.ID = IM.NullI;
            //stationList[columCount - 2] = Station;

            //if (columCount > 2)
            //{
            //   grid.RemoveColumn(0);
            //   grid.RemoveColumn(12);
            //   grid.RemoveColumn(13);
            //   grid.RemoveColumn(14);
            //   grid.RemoveColumn(15);
            //   grid.RemoveColumn(16);
            //   grid.RemoveColumn(17);
            //   grid.RemoveColumn(18);
            //   grid.RemoveColumn(19);
            //   grid.RemoveColumn(20);
            //   grid.RemoveColumn(21);

            //   /*IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadWrite);
            //   r.Select("ID");
            //   r.SetWhere("ID", IMRecordset.Operation.Eq, StationID);
            //   r.Open();

            //   while (!r.IsEOF())
            //   {
            //       r.Delete();
            //       r.MoveNext();
            //   }           
            //   r.Close();                               
            //   r.Destroy();*/
            //}
        }

        protected FilterTypeInfo GetFilterTypeInfoByStationID(int StatiobID, string StationTable)
        {
            FilterTypeInfo fti;

            fti.FilterIndex = IM.NullI;
            fti.FilterName = "";

            IMRecordset r = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadOnly);
            r.Select("OBJ_ID,OBJ_TABLE,EX_FILTER_ID");
            r.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, StatiobID);
            r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, StationTable);//ICSMTbl.itblMobStation);
            try
            {
                fti.FilterName = "";
                r.Open();
                if (!r.IsEOF())
                {
                    fti.FilterIndex = r.GetI("EX_FILTER_ID");


                    if (fti.FilterIndex != IM.NullI)
                    {
                        IMRecordset r2 = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
                        r2.Select("ID,NAME");
                        r2.SetWhere("ID", IMRecordset.Operation.Eq, fti.FilterIndex);
                        try
                        {
                            r2.Open();
                            if (!r2.IsEOF())
                            {
                                fti.FilterName = r2.GetS("NAME");
                            }
                        }
                        finally
                        {
                            r2.Close();
                            r2.Destroy();
                        }
                    }
                }
                //cell.Value = stationList[index].FilterName;
                //CellValidate(cell, grid);
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return fti;
        }

        private void CopyCell(string keyTo, string keyFrom, Grid grid)
        {
            Cell cTo = grid.GetCellFromKey(keyTo);
            Cell cFrom = grid.GetCellFromKey(keyFrom);
            //-------------------------------
            cTo.FontName = cFrom.FontName;
            cTo.TextHeight = cFrom.TextHeight;
            cTo.HorizontalAlignment = cFrom.HorizontalAlignment;
            cTo.VerticalAlignment = cFrom.VerticalAlignment;
            cTo.BackColor = cFrom.BackColor;
            cTo.FontStyle = cFrom.FontStyle;
            cTo.TabOrder = cFrom.TabOrder;
            cTo.Value = cFrom.Value;
        }

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            List<int> listId = new List<int>();
            for (int i = 0; i < stationList.Count; i++)
                if (stationList[i].ID != IM.NullI)
                    listId.Add(stationList[i].ID);
            if (listId.Count > 1)
            {
                DialogResult rez = DialogResult.None;
                int selectedId = IM.NullI;
                using (SelectSectorForm selectSectorForm = new SelectSectorForm())
                {
                    selectSectorForm.InitList();
                    foreach (int id in listId)
                    {
                        selectSectorForm.AddSector(id);
                    }
                    rez = selectSectorForm.ShowDialog();
                    selectedId = selectSectorForm.GetSelectedIndex();
                }
                switch (rez)
                {
                    case DialogResult.OK:
                        if (selectedId != IM.NullI)
                            ShowFormCalculateEms(selectedId);
                        break;
                    case DialogResult.Retry:
                        ShowFormCalculateEms(listId.ToArray());
                        break;
                }
            }
            else
            {
                ShowFormCalculateEms(listId.ToArray());
            }
        }

        private void ShowFormCalculateEms(params int[] ids)
        {
            using (FormEMS formEms = new FormEMS(recordID.Table, ids))
            {
                formEms.ShowDialog();
            }
        }

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "kAddr"))// && (stationList[0].objPosition != null))
                {
                    if (stationList[0].objPosition != null)
                    {
                        try
                        {
                            int posID = stationList[0].PosID;
                            Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionMob2, posID, (IM.TableRight(stationList[0].objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                            // position may appear be  changed
                            //todo: refactor this in all forms to some general "ReloadPosition()" method in base class
                            stationList[0].objPosition.LoadStatePosition(posID, stationList[0].objPosition.TableName);
                            stationList[0].objPosition.GetAdminSiteInfo(stationList[0].objPosition.AdminSiteId);
                            UpdateOneParamGrid(gridParam.GetCellFromKey("KLon"), gridParam);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                        ShowMessageNoReference();
                
                }
                else if ((cell.Key == "kAntType_1")
                    || (cell.Key == "kAntType_2")
                    || (cell.Key == "kAntType_3")
                    || (cell.Key == "kAntType_4")
                    || (cell.Key == "kAntType_5")
                    || (cell.Key == "kAntType_6"))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(9));
                    if (stationList[index - 1].objAntenna != null)
                    {
                        int AntID = stationList[index - 1].AntID;
                        RecordPtr rcPtr = new RecordPtr(stationList[0].objAntenna.GetS("TABLE_NAME"), AntID);
                        rcPtr.UserEdit();
                    }
                }
                else if (cell.Key.StartsWith(EquipNameCellKey))
                {
                    int index = GetCellIndexFromKey(cell.Key) - 1;
                    if (stationList[index].objEquip != null)
                    {
                        int equipId = stationList[index].EquipID;
                        string tableName = stationList[index].objEquip.GetS("TABLE_NAME");
                        var rcPtr = new RecordPtr(tableName, equipId);
                        rcPtr.UserEdit();
                    }
                }
                else if ((cell.Key == "KObj_1")
                    || (cell.Key == "KObj_2")
                    || (cell.Key == "KObj_3")
                    || (cell.Key == "KObj_4")
                    || (cell.Key == "KObj_5")
                    || (cell.Key == "KObj_6"))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(5));

                    if (stationList[index - 1].ID != IM.NullI)
                    {
                        int ID = stationList[index - 1].ID;
                        RecordPtr rcPtr = new RecordPtr(TableName, ID);
                        rcPtr.UserEdit();
                    }
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            if (Standard == CRadioTech.BNT)
                return "BNT";
            int sectorCount = 0;
            foreach (StationBS stat in stationList)
                if (stat.ID != IM.NullI)
                    sectorCount++;
            return sectorCount.ToString();
        }
        
        //============================================================
        /// <summary>
        /// Сохранение изменений в станции перед созданием отчета
        /// </summary>
        protected void SaveFrequenciesToXnrfaAppl()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
            r.Select("CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r.Open();


            if (!r.IsEOF())
            {
                r.Edit();
                for (int i = 0; i < 6; i++)
                {
                    string strFreqTX = stationList[i].FreqTxListAsString;
                    string strFreqRX = stationList[i].FreqRxListAsString;

                    string ObjIdName = "OBJ_ID" + (i + 1).ToString();
                    if (stationList[i].ID != IM.NullI)
                    {
                        //if (stationList[i].
                        //r.Put(ObjIdName, stationList[i].objStation.GetI("ID"));
                        // Каналы
                        //r.Put("CUST_TXT" + (i + 1).ToString(), stationList[i].strChannel);
                        // Частоты на передачц (TX)
                        r.Put("CUST_TXT" + (6 + i + 1).ToString(), strFreqTX.Replace(',','.'));
                        // Частоты на прием (RX)
                        r.Put("CUST_TXT" + (12 + i + 1).ToString(), strFreqRX.Replace(',', '.'));

                        //r.Put(ObjIdName, IM.NullI);
                    }
                    else
                        r.Put(ObjIdName, IM.NullI);
                }
                r.Update();
            }
            r.Close();
            r.Destroy();
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            for (int i = 0; i < 6; i++)
            {
                IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
                r.Select("ID,CUST_TXT1,CUST_DAT1,CUST_TXT6");
                r.SetWhere("ID", IMRecordset.Operation.Eq, stationList[i].ID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("CUST_TXT1", NumberOut);
                        r.Put("CUST_DAT1", DateOut);
                        r.Put("CUST_TXT6", NumberIn);
                        r.Update();
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
        }

        public override void Coordination()
        {            
            base.Coordination();
            CoordForm form = null;
            PositionState pos=new PositionState();
            if (stationList != null)
                if (stationList.Count > 0)
                    pos.LoadStatePosition(stationList[0].objPosition.Id, stationList[0].objPosition.TableName);
            if (pos.Id!=IM.NullI)
                    form = new CoordForm(stationList[0], TableName, applID);
            form.ShowDialog();
        }
        /// <summary>
        /// Возвращает сектор станции
        /// </summary>
        /// <returns>Сектор станции</returns>
        public override RecordPtr GetRecordOfSector()
        {
            RecordPtr retRec = new RecordPtr(recordID.Table, IM.NullI);
            List<int> listId = new List<int>();
            for (int i = 0; i < stationList.Count; i++)
                if (stationList[i].ID != IM.NullI)
                    listId.Add(stationList[i].ID);
            if (listId.Count > 1)
            {
                DialogResult rez = DialogResult.None;
                int selectedId = IM.NullI;
                using (SelectSectorForm selectSectorForm = new SelectSectorForm(false))
                {
                    selectSectorForm.InitList();
                    foreach (int id in listId)
                    {
                        selectSectorForm.AddSector(id);
                    }
                    rez = selectSectorForm.ShowDialog();
                    selectedId = selectSectorForm.GetSelectedIndex();
                }
                switch (rez)
                {
                    case DialogResult.OK:
                        if (selectedId != IM.NullI)
                            retRec.Id = selectedId;
                        break;
                }
            }
            else
            {
                retRec.Id = recordID.Id;
            }
            return retRec;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
    }
}
