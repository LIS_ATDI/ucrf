﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MeasureEquipmentCertificate 
    {
        //public string EquipmentType { get; set; }
        //public string Serial{ get; set; }
        //public string Note { get; set; }

        public string TableName { get; set; }
        public int Id { get; set; }        
        public string Serial { get; set; }
        public DateTime Date { get; set; }
        public string CertifiedBy { get; set; }
        public string Province { get; set; }

        public MeasureEquipmentCertificate()
        {
            TableName = PlugTbl.itblXnrfaMeasureEquipmentCertificates;
            Id = IM.NullI;
            Serial = "";
            Date = IM.NullT;
            CertifiedBy  = "";
            Province = "";
        }

        public void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,CERT_NUM,CERT_DATE,CERTIFIED_BY,PROVINCE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();                

                if (!r.IsEOF())
                {
                    Serial = r.GetS("CERT_NUM");
                    Date = r.GetT("CERT_DATE");
                    CertifiedBy = r.GetS("CERTIFIED_BY");
                    Province = r.GetS("PROVINCE");
                }
            }
            finally
            {
                if (r.IsEOF())
                    r.Close();
                r.Destroy();
            }
        }

        public void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,CERT_NUM,CERT_DATE,CERTIFIED_BY,PROVINCE,DATE_CREATED,CREATED_BY");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();
                } 
                else
                {
                    Id = IM.AllocID(TableName, 1, -1);
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }

                r.Put("CERT_NUM", Serial);
                r.Put("CERT_DATE", Date);
                r.Put("CERTIFIED_BY", CertifiedBy);
                r.Put("PROVINCE", Province);
                r.Update();
            }
            finally
            {
                if (r.IsEOF())
                    r.Close();
                r.Destroy();
            }
        }

        public string ToLongString()
        {
            if (Id != IM.NullI)
                return "Свідоцтво про атестацію №" + Serial +
                " Видане " + Date.ToShortDateStringNullT() +
                " " + CertifiedBy;
            else
                return "";
        }
    }
}
