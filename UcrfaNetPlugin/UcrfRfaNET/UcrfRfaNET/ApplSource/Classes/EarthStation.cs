﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
   class EarthStation : BaseStation, ICloneable
   {
      private EarthEquipment _equipment = new EarthEquipment();
      private EarthAntenna _antenna = new EarthAntenna();
      private Power _power = new Power();

      public string NetworkName { get; set;  }
      //public PositionState Position { get; set; }
      public double ASL { get; set; }
      public double Azimuth { get; set; }
      public double Elevation { get; set; }
      public double OrbitalPosition { get; set; }
      //public int SiteId { get; set; }

      public EarthEquipment Equipment { get { return _equipment; } }
      public Power Power { get { return _power; } }

      public double AGL { get; set; }     
      public EarthAntenna Antenna { get { return _antenna; } }

      public string TxPolarization { get; set; }
      public List<double> TxFrequencies { get; set; }
      public string RxPolarization { get; set; }
      public List<double> RxFrequencies { get; set; }

      public static readonly string TableName = ICSMTbl.itblEarthStation;
      public EarthStation()
      {                 
         TxFrequencies = new List<double>();
         RxFrequencies = new List<double>();
      }

      public object Clone()
      {
         EarthStation clone = new  EarthStation();
         return clone;
      }

      public override void Load()
      {

         IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
         
         try
         {
             r.Select("ID,SITE_ID,AGL,SAT_NAME,LONG_NOM,AZM_TO,ELEV_MIN,CUST_TXT15");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            
            r.Open();
            if (!r.IsEOF())
            {               
               AGL = r.GetD("AGL");
               NetworkName = r.GetS("SAT_NAME");
               OrbitalPosition = r.GetD("LONG_NOM");
               Azimuth = r.GetD("AZM_TO");
               Elevation = r.GetD("ELEV_MIN");
               int SiteId = r.GetI("SITE_ID");

               Position = new PositionState();
               Position.LoadStatePosition(SiteId, ICSMTbl.itblPositionEs);
               
               _equipment.Id = ReadEqipmentId(Id);
               
               _antenna.Id = ReadAntennaId(Id);

               int antennaID = ReadEstaAntennaId("E", Id);
               Power[PowerUnits.dBW] = LoadMaxPower(antennaID);
               //Висновок
               Finding = r.GetS("CUST_TXT15");

               if (_equipment.Id != IM.NullI)
                  _equipment.Load();

               if (_antenna.Id!=IM.NullI)
                  _antenna.Load();
            }
            TxFrequencies = ReadTxFrequencies(Id);
            RxFrequencies = ReadRxFrequencies(Id);

            RxPolarization = ReadPolarRX(Id);
            TxPolarization = ReadPolarTX(Id);
         }
         finally
         {
            r.Close();
            r.Destroy();
         }                           
      }

      public override void Save()
      {
         int SiteId = Position.Id;

         IMRecordset r = new IMRecordset(ICSMTbl.itblEarthStation, IMRecordset.Mode.ReadWrite);
         
         try
         {
            r.Select("ID,SITE_ID,AGL,AZM_TO,LONGITUDE,LATITUDE,X,Y,CSYS");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);

            r.Open();            
            if (!r.IsEOF())
            {
               r.Edit();
               r.Put("AGL", AGL);               
               r.Put("AZM_TO", Azimuth);
               r.Put("SITE_ID", SiteId);
               r.Put("LONGITUDE", Position.LonDec);
               r.Put("LATITUDE", Position.LatDec);
               r.Put("X", Position.LonDms);
               r.Put("Y", Position.LatDms);
               r.Put("CSYS", "4DMS");
            }

            WriteAntennaId(Id, _antenna.Id);
            WriteEqipmentId(Id, _equipment.Id);

            WritePolarTX(Id, TxPolarization);
            WritePolarRX(Id, RxPolarization);
            WriteTxFrequencies(Id, TxFrequencies);
            WriteRxFrequencies(Id, RxFrequencies);

            var antennaId = ReadEstaAntennaId("E", Id);
            SaveMaxPower(antennaId, Power[PowerUnits.dBW]);

            r.Update();

            FillComments(ApplId);
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }

      //===========================================================
      /// <summary>
      /// 
      /// </summary>
      /// <param name="rcp">R for receiver, E for transmiter</param>
      /// <param name="Id">Earth Station ID</param>
      /// <returns></returns>
      private static int ReadEstaAntennaId(string rcp, int Id)
      {
         int antennaID = IM.NullI;

         IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
         r.Select("ESTA_ID,ID,ANT_ID,EMI_RCP");
         r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, Id);
         r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, rcp);//"E");
         r.Open();
         if (!r.IsEOF())
            antennaID = r.GetI("ID");
         r.Close();
         r.Destroy();

         return antennaID;
      }
      //===========================================================
      /// <summary>
      /// Read antenna ID
      /// </summary>
      /// <param name="Id">EARTH_STATION ID</param>
      /// <returns>ANTENNA ID</returns>
      private static int ReadAntennaId(int Id)
      {
         int antennaID = IM.NullI;

         IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
         r.Select("ESTA_ID,ID,ANT_ID,EMI_RCP");
         r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, Id);
         r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
         r.Open();
         if (!r.IsEOF())
            antennaID = r.GetI("ANT_ID");
         r.Close();
         r.Destroy();

         return /*new RecordPtr(ICSMTbl.itblAntenna,*/ antennaID;
      }

      //===========================================================
      /// <summary>
      /// Write antenna ID
      /// </summary>
      /// <param name="Id">EARTH_STATION ID</param>
      /// <returns>ANTENNA ID</returns>
      private static int WriteAntennaId(int id, int antennaId)
      {
         int antennaID = IM.NullI;

         IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadWrite);
         r.Select("ESTA_ID,ID,ANT_ID,EMI_RCP");
         r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, id);
         r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
         r.Open();
         if (!r.IsEOF())
         {
            r.Edit();
            r.Put("ANT_ID", antennaId);
            r.Update();
         }

         r.Close();
         r.Destroy();

         return /*new RecordPtr(ICSMTbl.itblAntenna,*/ antennaID;
      }

      private static int ReadGroupIdByAntennaId(int antennaId)
      {
         int groupId = IM.NullI;

         IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
         group.Select("EANT_ID,ID");
         group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaId);
         group.Open();
         if (!group.IsEOF())
            groupId = group.GetI("ID");
         group.Close();
         group.Destroy();

         return groupId;
      }

      /// <summary>
      /// Read Equipment ID      
      /// </summary>
      /// <param name="Id">EARTH_STATION ID</param>
      /// <returns>EQUIP_ESTA ID</returns>
      private static int ReadEqipmentId(int Id)
      {
         int equipmentID = IM.NullI;

         int antennaID = ReadEstaAntennaId("E", Id);
         
         if (antennaID != IM.NullI)
         {
            int groupID = ReadGroupIdByAntennaId(antennaID);

            if (groupID != IM.NullI)
            {
               int emissID = IM.NullI;
               IMRecordset emiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
               emiss.Select("EGRP_ID,EQPCONF_ID");
               emiss.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
               emiss.Open();
               if (!emiss.IsEOF())
                  emissID = emiss.GetI("EQPCONF_ID");
               emiss.Close();
               emiss.Destroy();

               return emissID;
               /*if (emissID != IM.NullI)
               {
                  RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipEsta, IM.NullI);
                  IMRecordset equipEsta = new IMRecordset(ICSMTbl.itblEquipEsta, IMRecordset.Mode.ReadOnly);
                  equipEsta.Select("ID");
                  equipEsta.SetWhere("ID", IMRecordset.Operation.Eq, emissID);
                  equipEsta.Open();
                  if (!equipEsta.IsEOF())
                  {
                     equipmentID = equipEsta.GetI("ID");
                  }
                  equipEsta.Close();
                  equipEsta.Destroy();
               }*/
            }
         }
         //return new RecordPtr(ICSMTbl.itblEquipEsta, equipmentID);
         return equipmentID;
      }

      /// <summary>
      /// Read Equipment ID      
      /// </summary>
      /// <param name="Id">EARTH_STATION ID</param>
      /// <returns>EQUIP_ESTA ID</returns>
      private static void WriteEqipmentId(int id, int equipmentId)
      {         
         int antennaID = ReadEstaAntennaId("E", id);

         if (antennaID != IM.NullI)
         {
            int groupID = ReadGroupIdByAntennaId(antennaID);

            if (groupID != IM.NullI)
            {
               IMRecordset emiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadWrite);
               emiss.Select("EGRP_ID,EQPCONF_ID");
               emiss.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
               emiss.Open();
               if (!emiss.IsEOF())
               {
                  emiss.Edit();
                  emiss.Put("EQPCONF_ID", equipmentId);                  
                  emiss.Update();
               }
               emiss.Close();
               emiss.Destroy();               
            }
         }         
      }

      /// <summary>
      /// Read Transmitter Frequencies
      ///   puts it to StationZS.nomFreqTx
      /// </summary>
      private static List<double> ReadTxFrequencies(int Id)
      {
         // Частоты TX
         List<double> nomFreqTx = new List<double>();

         int antennaID = ReadEstaAntennaId("E", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = ReadGroupIdByAntennaId(antennaID);

            if (groupID != IM.NullI)
            {
               //int emissID = IM.NullI;
               IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
               rsAssgn.Select("FREQ");
               rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
               try
               {
                  for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                  {
                     double freq = rsAssgn.GetD("FREQ");
                     if (freq != IM.NullD)
                        nomFreqTx.Add(freq / 1000.0);
                  }
               }
               finally
               {
                  rsAssgn.Close();
                  rsAssgn.Destroy();
               }
            }
         }

         return nomFreqTx;
      }

      /// <summary>
      /// Read Transmitter Frequencies
      ///   puts it to StationZS.nomFreqTx
      /// </summary>
      private static void WriteTxFrequencies(int Id, List<double> frequecyList)
      {                        
         int antennaID = ReadEstaAntennaId("E", Id);

         if (antennaID != IM.NullI)
         {
            int estaGroupID = ReadGroupIdByAntennaId(antennaID);

            IMRecordset estaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadWrite);
            string fieldsSelect = "ID,EGRP_ID,FREQ";
            estaAssgn.Select(fieldsSelect);
            estaAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
            estaAssgn.Open();
            bool isAdd = false;
            foreach (double val in frequecyList)
            {
               if (estaAssgn.IsEOF() || (isAdd == true))
               {
                  isAdd = true;
                  estaAssgn.AddNew();
                  estaAssgn["ID"] = IM.AllocID(ICSMTbl.itblEstaAssgn, 1, -1);
                  estaAssgn["EGRP_ID"] = estaGroupID;
               }
               else
               {
                  estaAssgn.Edit();
               }
               estaAssgn["FREQ"] = val * 1000.0;
               //CJournal.CheckTable(ICSMTbl.itblEstaAssgn, estaAssgn.GetI("ID"), estaAssgn, fieldsSelect);
               estaAssgn.Update();
               estaAssgn.MoveNext();
            }

            if (isAdd == false)
               while (!estaAssgn.IsEOF())
               {
                  estaAssgn.Delete();
                  estaAssgn.MoveNext();
               }
            estaAssgn.Close();
            estaAssgn.Destroy();
         }         
      }

      /// <summary>      
      /// Read Receiver Frequencies
      ///   puts it to StationZS.nomFreqRx      
      /// </summary>
      private static List<double> ReadRxFrequencies(int Id)
      {
         // Частоты RX
         List<double> nomFreqRx = new List<double>();

         int antennaID = ReadEstaAntennaId("R", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = ReadGroupIdByAntennaId(antennaID);

            if (groupID != IM.NullI)
            {
               //int emissID = IM.NullI;
               IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
               rsAssgn.Select("FREQ");
               rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
               try
               {
                  for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                  {
                     double freq = rsAssgn.GetD("FREQ");
                     if (freq != IM.NullD)
                        nomFreqRx.Add(freq / 1000.0);
                  }
               }
               finally
               {
                  rsAssgn.Close();
                  rsAssgn.Destroy();
               }
            }
         }

         return nomFreqRx;
         //HelpFunction.ToString(cell, st.nomFreqRx);            
      }

      /// <summary>      
      /// Read Receiver Frequencies
      ///   puts it to StationZS.nomFreqRx      
      /// </summary>
      private static void WriteRxFrequencies(int Id, List<double> frequencyList)
      {
         // Частоты RX         
         int antennaID = ReadEstaAntennaId("R", Id);
         
         if (antennaID != IM.NullI)
         {
            int estaGroupID = ReadGroupIdByAntennaId(antennaID);

            IMRecordset estaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadWrite);
            string fieldsSelect = "ID,EGRP_ID,FREQ";
            estaAssgn.Select(fieldsSelect);
            estaAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
            estaAssgn.Open();
            bool isAdd = false;
            foreach (double val in frequencyList)
            {
               if (estaAssgn.IsEOF() || (isAdd == true))
               {
                  isAdd = true;
                  estaAssgn.AddNew();
                  estaAssgn["ID"] = IM.AllocID(ICSMTbl.itblEstaAssgn, 1, -1);
                  estaAssgn["EGRP_ID"] = estaGroupID;
               }
               else
               {
                  estaAssgn.Edit();
               }
               estaAssgn["FREQ"] = val * 1000.0;
               //CJournal.CheckTable(ICSMTbl.itblEstaAssgn, estaAssgn.GetI("ID"), estaAssgn, fieldsSelect);
               estaAssgn.Update();
               estaAssgn.MoveNext();
            }

            if (isAdd == false)
               while (!estaAssgn.IsEOF())
               {
                  estaAssgn.Delete();
                  estaAssgn.MoveNext();
               }

            estaAssgn.Close();
            estaAssgn.Destroy();
         }         
      }

      /// <summary>
      ///  Return Tx Polarizations
      /// </summary>
      /// <returns>Short letter code of polarization</returns>
      private static string ReadPolarTX(int Id)
      {
         string PolarТX = "";

         int antennaID = ReadEstaAntennaId("E", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = IM.NullI;
            IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
            group.Select("EANT_ID,POLAR_TYPE");
            group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
            group.Open();
            if (!group.IsEOF())            
               PolarТX = group.GetS("POLAR_TYPE");
               //double powr = group.GetD("PWR_MAX");                           
            group.Close();
            group.Destroy();
         }
         return PolarТX;
      }

      /// <summary>
      ///  Return Tx Polarizations
      /// </summary>
      /// <returns>Short letter code of polarization</returns>
      private static void WritePolarTX(int Id, string polarTx)
      {         
         int antennaID = ReadEstaAntennaId("E", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = IM.NullI;
            IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadWrite);
            group.Select("EANT_ID,POLAR_TYPE");
            group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
            group.Open();
            if (!group.IsEOF())
            {
               group.Edit();
               group.Put("POLAR_TYPE", polarTx);
               group.Update();
            }

            group.Close();
            group.Destroy();
         }         
      }

      /// <summary>
      ///  Return Rx Polarizations
      /// </summary>
      /// <returns>Short letter code of polarization</returns>
      private static string ReadPolarRX(int Id)
      {
         // Поляризация RX
         string PolarRX = "";
         int antennaID = ReadEstaAntennaId("R", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = IM.NullI;
            IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
            group.Select("EANT_ID,POLAR_TYPE");
            group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
            group.Open();
            if (!group.IsEOF())
               PolarRX = group.GetS("POLAR_TYPE");
            group.Close();
            group.Destroy();
         }
         return PolarRX;
      }

      /// <summary>
      ///  Return Tx Polarizations
      /// </summary>
      /// <returns>Short letter code of polarization</returns>
      private static void WritePolarRX(int Id, string polarRx)
      {
         int antennaID = ReadEstaAntennaId("R", Id);

         if (antennaID != IM.NullI)
         {
            int groupID = IM.NullI;
            IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadWrite);
            group.Select("EANT_ID,POLAR_TYPE");
            group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
            group.Open();
            if (!group.IsEOF())
            {
               group.Edit();
               group.Put("POLAR_TYPE", polarRx);
               group.Update();
            }

            group.Close();
            group.Destroy();
         }
      }

      private double LoadMaxPower(int id)
      {
         double maxPower = IM.NullD;

         IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
         group.Select("EANT_ID,PWR_MAX");
         group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, id);
         group.Open();
         if (!group.IsEOF())
            maxPower = group.GetD("PWR_MAX");
         group.Close();
         group.Destroy();

         return maxPower;         
      }

      private void SaveMaxPower(int id, double power)
      {
         double maxPower = IM.NullD;

         IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadWrite);
         group.Select("EANT_ID,PWR_MAX");
         group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, id);
         group.Open();
         if (!group.IsEOF())
         {
            group.Edit();
            group.Put("PWR_MAX", power);
            group.Update();
         }
         group.Close();
         group.Destroy();         
      }


      /// <summary>
      /// Возвратить активную полосу пропускания для земных станций 
      /// в мегагерцах
      /// </summary>
      /// <returns></returns>
      public override double GetActiveBandwidth()
      {
          return Equipment.TxBandwidth/1000.0;
      }
   }
}
