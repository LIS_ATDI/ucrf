﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStationEquipment : Equipment, ICloneable
    {
        public double Bandwidth { get; set; }
        public string Modulation { get; set; }

        public string CustText2 { get; set; }

        public override void Load()
        {
            IMRecordset equipRec = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                equipRec.Select("ID,NAME,BW,DESIG_EMISSION,MODULATION,MBITPS,MAX_POWER,CUST_TXT1,CUST_DAT1,CUST_TXT2,CUST_TXT5");
                equipRec.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                equipRec.Open();
                if (!equipRec.IsEOF())
                {
                    Name = equipRec.GetS("NAME");
                    Bandwidth = equipRec.GetD("BW");

                    if (Bandwidth != IM.NullD)
                        Bandwidth /= 1000.0;

                    _equipmentType = equipRec.GetS("CUST_TXT5");
                    DesigEmission = equipRec.GetS("DESIG_EMISSION");
                    Modulation = equipRec.GetS("MODULATION");
                    Certificate.Symbol = equipRec.GetS("CUST_TXT1");
                    Certificate.Date = equipRec.GetT("CUST_DAT1");
                    CustText2 = equipRec.GetS("CUST_TXT2");
                    MaxPower[PowerUnits.dBm] = equipRec.GetD("MAX_POWER"); 
                }
            }
            finally
            {
                if (equipRec.IsOpen())
                    equipRec.Close();
                equipRec.Destroy();
            }
        }

        public override void Save()
        {
        }

        public object Clone()
        {
            MobStationEquipment clone = new MobStationEquipment();

            //clone._maxPower = _maxPower.Clone() as Power;
            clone._equipmentName = _equipmentName;
            clone._desigEmission = _desigEmission;
            clone.Bandwidth = Bandwidth;
            clone.Certificate.Date = Certificate.Date;
            clone.Certificate.Symbol = Certificate.Symbol;
            //clone.Modulation = Modulation;
            clone.Id = Id;
            clone._tableName = _tableName;

            return clone;
        }
    }
}
