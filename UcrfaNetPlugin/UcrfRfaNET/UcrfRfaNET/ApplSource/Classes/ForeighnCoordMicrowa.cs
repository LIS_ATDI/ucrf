﻿using System.Collections.Generic;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class ForeighnCoordMicrowa : ForeighnCoord
    {
        public ForeighnCoordMicrowa(string tech, string tableName, string tableFreq)
            : base(tech, tableName, tableFreq) { }

        public override List<CoordForm.FreqId> CalculateFreq4Station(List<int> objIdList)
        {
            List<CoordForm.FreqId> freqStatList = new List<CoordForm.FreqId>();
            IMRecordset rFreq = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
            rFreq.Select("ID,ROLE,SITE_ID,TX_FREQ,MW_ID");
            string objIdStr = "";
            if (objIdList != null)
                for (int i = 0; i < objIdList.Count; i++)
                {
                    objIdStr += "[MW_ID]=" + objIdList[i];
                    if (i < objIdList.Count - 1)
                        objIdStr += " OR ";
                }
            if (!string.IsNullOrEmpty(objIdStr))
                rFreq.SetAdditional(objIdStr);
            rFreq.SetWhere("ROLE", IMRecordset.Operation.Eq, "A");
            try
            {
                for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
                    freqStatList.Add(new CoordForm.FreqId(rFreq.GetD("TX_FREQ") * 1000000, rFreq.GetI("MW_ID"), rFreq.GetI("ID")));
            }
            finally
            {
                rFreq.Final();
            }
            return freqStatList;
        }

        /// <summary>
        /// Заповнення статусу полів для Microwa
        /// </summary>
        public override List<CoordForm.CoordTable> ShowStatusStation()
        {
            if (CoordTbl != null && CoordTbl.Count > 0)
                for (int i = 0; i < CoordTbl.Count; i++)
                {
                    string status = "";
                    string administration = "";
                    IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,HCM_STATUS,HCM_LADMS");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, CoordTbl[i].Id);
                    try
                    {
                        r.Open();
                        if (!r.IsEOF())
                        {
                            status = r.GetS("HCM_STATUS");
                            administration = r.GetS("HCM_LADMS");
                        }
                    }
                    finally
                    {
                        r.Final();
                    }

                    if (string.IsNullOrEmpty(CoordTbl[i].Status))
                        CoordTbl[i].Status = "A"; //#6268 Побажання: автоматичне заповнення статусу "Координація потрібна"
                    if (!string.IsNullOrEmpty(status) && administration.Contains(CoordTbl[i].Administration))
                        CoordTbl[i].Status = status;
                }
            return CoordTbl;
        }

        /// <summary>
        /// Визначення статусу записів для таблиці microwa і збереження
        /// </summary>
        public override void AcceptStation()
        {
            //Видалити всі статуси для заданих ID станцій
            List<int> idList = CoordTbl.Select(obj => obj.Id).Distinct().ToList();
            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadWrite);
            r.Select("ID,HCM_STATUS,HCM_LADMS");
            string additSelect = "";
            for (int i = 0; i < idList.Count; i++)
            {
                additSelect += "([ID]=" + idList[i] + ")";
                if (i < idList.Count - 1)
                    additSelect += " OR ";
            }
            if (!string.IsNullOrEmpty(additSelect))
                r.SetAdditional(additSelect);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    r.Edit();
                    r.Put("HCM_STATUS", "CN");
                    r.Put("HCM_LADMS", "");
                    r.Update();
                }
            }
            finally
            {
                r.Final();
            }
            //Сформувати вихідні дані            
            for (int i = 0; i < CoordTbl.Count; i++)
            {
                int index = i;
                List<CoordForm.CoordTable> tmpCrd =
                    CoordTbl.FindAll(obj => (obj.Freq == CoordTbl[index].Freq && obj.Id == CoordTbl[index].Id));
                List<string> strAll =
                    tmpCrd.FindAll(obj => (obj.Status != "CN" && obj.Status != "A")).Select(obj => obj.Status).ToList();
                tmpCrd.ForEach(obj =>
                {
                    if (obj.Status == "A")
                        if (strAll.Count > 0)
                            obj.Status = strAll[0];
                });
            }

            //Заново записати всі статуси
            for (int i = 0; i < CoordTbl.Count; i++)
            {
                if (string.IsNullOrEmpty(CoordTbl[i].Status))
                    CoordTbl[i].Status = "CN";

                r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadWrite);
                r.Select("ID,HCM_STATUS,HCM_LADMS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, CoordTbl[i].Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        if (CoordTbl[i].Status != "CN")
                        {
                            r.Put("HCM_STATUS", CoordTbl[i].Status);
                            string admStr = r.GetS("HCM_LADMS");
                            if (string.IsNullOrEmpty(admStr))
                                admStr = CoordTbl[i].Administration;
                            else
                                admStr += "," + CoordTbl[i].Administration;
                            r.Put("HCM_LADMS", admStr);
                        }
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }
            }
        }

    }
}
