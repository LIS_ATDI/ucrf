﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class BandAmateur
    {
        Power _power = new Power();

        public const string TableName = PlugTbl.XfaBandAmateur;
        public int Id { get; set; }

        public string typeTechStr;
        public List<string> typeStr = new List<string>();
        public double BandMin { get; set; }
        public double BandMax { get; set; }
        public Power  Power { get { return _power; } }
        public string Category { get; set; }
        public string TypeStat { get; set; }

        public BandAmateur()
        {
            BandMin = 0.0;
            BandMax = 0.0;
            Power[PowerUnits.W] = IM.NullD;
            Category = "";
            TypeStat = "";                    
        }

        public void Load()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaBandAmateur, IMRecordset.Mode.ReadOnly);                        

            try
            {
                r.Select("ID,POWER,BANDMIN,BANDMAX,CAT_OPER,TYPE_STAT,CUST_TXT1");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {                    
                    Power[PowerUnits.W] = r.GetD("POWER");
                    typeTechStr = r.GetS("CUST_TXT1");
                    BandMin = r.GetD("BANDMIN");
                    BandMax = r.GetD("BANDMAX");
                    Category = r.GetS("CAT_OPER");
                    TypeStat = r.GetS("TYPE_STAT");
                    if (!string.IsNullOrEmpty(typeTechStr))
                        typeStr = typeTechStr.Split(',').ToList();
                    typeStr.RemoveAll(obj => obj.Length == 0);
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }            
        }

        public void Save()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaBandAmateur, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,POWER,BANDMIN,BANDMAX,CAT_OPER,TYPE_STAT,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,CUST_TXT1");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (r.IsEOF())
                {
                    r.AddNew();

                    if (Id==IM.NullI)
                        Id = IM.AllocID(PlugTbl.XfaBandAmateur, 1, -1);

                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());                                       
                } 
                else
                {
                    r.Edit();                    
                }

                r.Put("DATE_MODIFIED", DateTime.Now);
                r.Put("MODIFIED_BY", IM.ConnectedUser());
                 
                r.Put("POWER", Power[PowerUnits.W]);
                r.Put("BANDMIN", BandMin);
                r.Put("BANDMAX", BandMax);
                r.Put("CAT_OPER", Category);
                r.Put("TYPE_STAT", TypeStat);
                r.Put("CUST_TXT1",typeTechStr);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
