﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class TvDigitallDiffStation : BaseStation
    {
        public TvDigitallEquipment TvDgDiffEquipment { get; set; }
        public TvDigitallAntenna TvDgDiffAntenna { get; set; }
        
        public bool IsEnableDiff { get; set; }

        public int PosId { get; set; }

        public int EquipId { get; set; }
        public int AntId { get; set; }
        public int PlanId { get; set; }
        //public PositionState Position { get; set; }
        public string EquipmentName { get; set; }
                        
        public double ShiftOstov { get; set; }

        public string NameRez { get; set; }
        public double PowerTx { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }

        public string Polar { get; set; }
        public string Direct { get; set; }

        public string TvChannel { get; set; }
        public double FreqCentr { get; set; }

        public double HeightAnten { get; set; }

        public static readonly string TableName = PlugTbl.itblXnrfaDeiffDvbSta;

        public TvDigitallDiffStation()
        {            
        }

        public override void Load()
        {
            TvDgDiffEquipment = new TvDigitallEquipment();
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDeiffDvbSta, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,POS_TABLE,OFFSET_V_KHZ,PLAN_ID,CERT_DATE,CERT_NUM,DESIG_EM,BW,PWR_ANT");
                r.Select("AGL,POLAR,DIRECTION,CHANNEL,FREQ,EQUIP_ID,COMMENTS,EQUIP_NAME,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                IsEnableDiff = false;
                if (!r.IsEOF())
                {                    
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;

                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        if (tmpPosId != IM.NullI)
                        {
                            Position.LoadStatePosition(tmpPosId, tmpPosTable);
                            PlanId = r.GetI("PLAN_ID");
                            ShiftOstov = r.GetD("OFFSET_V_KHZ");
                            EquipId = r.GetI("EQUIP_ID");
                            TvDgDiffEquipment.Name = r.GetS("EQUIP_NAME");
                            TvDgDiffEquipment.Certificate.Date = r.GetT("CERT_DATE");
                            TvDgDiffEquipment.Certificate.Symbol = r.GetS("CERT_NUM");
                            TvDgDiffEquipment.DesigEmission = r.GetS("DESIG_EM");
                            TvDgDiffEquipment.Bandwidth = r.GetD("BW");
                            TvDgDiffEquipment.MaxPower[PowerUnits.dBm] = r.GetD("PWR_ANT");
                            HeightAnten = r.GetD("AGL");
                            Polar = r.GetS("POLAR");
                            Direct = r.GetS("DIRECTION");
                            TvChannel = r.GetS("CHANNEL");
                            FreqCentr = r.GetD("FREQ");
                            StatComment = r.GetS("COMMENTS");
                            //  Power = Math.Pow(10.0, (r.GetD("PWR_ANT") / 10.0)).Round(3);

                        }
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();            
            }            
            TvDgDiffEquipment.Id = EquipId;
          //  TvDgDiffEquipment.Load();
        }

        public override void Save()
        {
          
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDeiffDvbSta, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,DVB_STATION_ID,OFFSET_V_KHZ,POS_ID,POS_TABLE,EQUIP_ID,CERT_DATE,CERT_NUM,DESIG_EM,BW,PWR_ANT,AGL,INACTIVE");
                r.Select("POLAR,DIRECTION,CHANNEL,FREQ,PLAN_ID,COMMENTS,EQUIP_NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("DVB_STATION_ID", Id);
                }
                else // 
                    r.Edit();

                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }
                r.Put("INACTIVE", 0);
                r.Put("OFFSET_V_KHZ",ShiftOstov);
                r.Put("PLAN_ID",PlanId);
                r.Put("EQUIP_ID",EquipId);

                r.Put("EQUIP_NAME", TvDgDiffEquipment.Name);
                r.Put("CERT_DATE", TvDgDiffEquipment.Certificate.Date);
                r.Put("CERT_NUM", TvDgDiffEquipment.Certificate.Symbol);
                r.Put("DESIG_EM", TvDgDiffEquipment.DesigEmission);
                r.Put("BW", TvDgDiffEquipment.Bandwidth);
                r.Put("PWR_ANT", TvDgDiffEquipment.MaxPower[PowerUnits.dBm]);
                r.Put("EQUIP_ID", EquipId);
                r.Put("AGL",HeightAnten);
                r.Put("POLAR",Polar);
                r.Put("DIRECTION",Direct);
                r.Put("CHANNEL",TvChannel);
                r.Put("FREQ",FreqCentr);
                r.Put("COMMENTS",StatComment);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public void Remove()
        {
            const string tableNameLocal = PlugTbl.itblXnrfaDeiffDvbSta;

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
