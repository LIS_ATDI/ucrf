﻿using System;
using System.Collections.Generic;
using System.Text;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRS;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal class StationTV : BaseStation
    {
        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна    
        public IMObject objChannel = null; // Канал
        public string Direction = "";
        public double MaxPower;
    }

    internal class CTvAnalog : FMTVBase
    {
        // initializes combobox lists
        public CComboList _standartList = new CComboList();
        public CComboList _systemList = new CComboList();
        public CComboList _zamichenyaList = new CComboList();
        public CComboList _internalStateList = new CComboList();
        public CComboList _intnationalStateList = new CComboList();
        public CComboList _orientationList = new CComboList();
        public CComboList _polarizationList = new CComboList();
        public CComboList _channelList = new CComboList();
        //public CComboList _freqVideoList = new CComboList();
        public List<double> _bwList = new List<double>();
        public List<double> _offsetList = new List<double>();
        public List<double> _freqVideoList = new List<double>();

        public static readonly string TableName = ICSMTbl.itblTV;

        public StationTV station = new StationTV(); // Дополнительные данные станции
        private double Longitude = 0; //Долгота
        private double Latitude = 0;  //Широта
        private bool NeedSavePosition = false;

        public CTvAnalog(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, CTvAnalog.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppTV2;
            department = DepartmentType.VRS;
            departmentSector = DepartmentSectorType.VRS_ST;

            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT12");
            SCVisnovok = objStation.GetS("CUST_TXT13");
            SCNote = objStation.GetS("CUST_TXT14");

            FillComboList();

            if (newStation)
            {
                objStation.Put("PWR_RATIO_1", 10);
                objStation.Put("STANDARD", "АТМ");
                objStation.Put("ADM", "UKR");
            }

            if (objStation.GetI("PLAN_ID") != IM.NullI)
                station.objChannel = IMObject.LoadFromDB(ICSMTbl.itblFreqPlanChan, objStation.GetI("PLAN_ID"));

            if (objStation.GetI("EQUIP_ID") != IM.NullI)
                station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipBro, objStation.GetI("EQUIP_ID"));

            if (objStation.GetI("SITE_ID") != IM.NullI)
            {
                station.objPosition = new PositionState2();
                station.objPosition.LoadStatePosition(objStation.GetI("SITE_ID"), ICSMTbl.itblPositionBro);
                station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
            }
            //----
            addValidVisnMonth = 12;
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            if (station.objAntenna != null)
            {
                station.objAntenna.Dispose();
                station.objAntenna = null;
            }
            if (station.objEquip != null)
            {
                station.objEquip.Dispose();
                station.objEquip = null;
            }
            if (station.objChannel != null)
            {
                station.objChannel.Dispose();
                station.objChannel = null;
            }
            base.DisposeExec();
        }

        protected override string GetXMLParamGrid()
        {
            string XmlString =
                 "<items>"
               + "<item key='header1'    type='lat'       flags=''      tab='-1'   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
               + "<item key='header2'    type='lat'       flags='s'     tab='-1'   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"

               // objPosition.LATITUDE
               + "<item key='mKLat'      type='lat'       flags=''     tab='-1'  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLat'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"

               // objPosition.LONGITUDE
               + "<item key='mKLon'       type='lat'       flags=''     tab='-1'  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLon'       type='lat'       flags='s'     tab='1'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

               // objPosition.ASL 
               + "<item key='mkAsl'      type=''       flags=''      tab='-1'  display='Висота поверхні Землі, м' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='kAsl'       type='double'       flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               // objPosition.FullAddress
               + "<item key='mkAddr'     type=''      flags=''      tab='-1'  display='Адреса встановлення РЕЗ' ReadOnly='true' background='' bold='y'/>"
               + "<item key='kAddr'      type='string'      flags='s'     tab='2'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

               //objEquip.NAME
               + "<item key='mkName'     type=''      flags=''      tab='-1'  display='Номер РЕЗ/назва опори' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='KName'      type=''      flags='s'     tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='KPillarName' type=''      flags='s'     tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               //          
               + "<item key='kStandart'   type='lat'       flags=''     tab='-1'  display='Стандарт/ система'     ReadOnly='true' background='' bold='' />"
               + "<item key='standart'    type='' flags='s'    tab='3'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='system'      type='' flags='s'    tab='4'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kNesivFreq'     type='lat'       flags=''     tab='-1'  display='Зміщення несівної частоти, кГц'     ReadOnly='true' background='' bold=''/>"
               + "<item key='NesivFreq'     type='lat'       flags='s'    tab='5'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //            
               + "<item key='kEquip'    type='lat'      flags=''      tab='-1'  display='Назва/тип РЕЗ' ReadOnly='true' background='' bold='y'/>"
               + "<item key='equip'     type='lat'      flags='s'     tab='6'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

               //            
               + "<item key='kTXFreq'       type='lat'       flags=''     tab='-1'  display='Потужність передавача: відео, Вт / звук, Вт'     ReadOnly='true' background='' bold='' />"
               + "<item key='powVideo'       type='lat'       flags='s'    tab='7'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='powSound'       type='lat'       flags='s'    tab='8'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //objEquip.BW
               + "<item key='kRxBand'      type='lat'       flags=''     tab='-1'  display='Ширина смуги, МГц'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='rxband'    type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"

               //objEquip.DESIG_EMISSION
               + "<item key='kEmiss'   type='lat'       flags=''     tab='-1'  display='Клас випромінювання: відео / звук'     ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='emiVideo'   type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
               + "<item key='emiAudio'   type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"

               // objEquip.CUST_TXT1
                // objEquip.CUST_DAT1
               + "<item key='kCert'      type='lat'      flags=''      tab='-1'  display='Сертифікат відповідності (номер / дата)' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='certno'     type='string'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='certda'     type='datetime'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               // 
               + "<item key='kFider'      type='lat'      flags=''   tab='-1'   display='Довжина фідера, м / Втрати у фідері, дБ/м' ReadOnly='true' background='' bold=''/>"
               + "<item key='fiderlen'    type='double'   flags='s'  tab='9'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='fiderloss'   type='double' flags='s'  tab='10'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kAntennaDir'       type='lat'      flags=''      tab='-1'  display='Спрямованість антени / поляризація' ReadOnly='true' background='' bold='y' />"
               + "<item key='dir'        type='lat'      flags='s'     tab='11'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='polar'        type='lat'      flags='s'     tab='12'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"

               //
               + "<item key='kAntennaHeight' type='lat'      flags=''      tab='-1'  display='Висота антени, м / макс. ефект. висота' ReadOnly='true' background='' bold=''/>"
               + "<item key='height'         type='lat'      flags='s'     tab='13'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"
               + "<item key='maxHeight'      type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center'/>"

               //
               + "<item key='kAntennaGain' type='lat'      flags=''      tab='-1'  display='Макс. коеф. підсилення антени: Г, дБ / В, дБ' background='' bold=''/>"
               + "<item key='horzGain'     type='lat'      flags='s'     tab='14'  display='' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='vertGain'     type='lat'      flags='s'     tab='15'  display='' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kEVP' type='lat'      flags=''      tab='-1'  display='ЕВП: Г, дБВт / В, дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='HorzEVP'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='VertEVP'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               //
               + "<item key='kEVPmax' type='lat'      flags=''      tab='-1'  display='ЕВП макс., дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='EVPmax'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               //
               + "<item key='kCallSign'    type='lat'      flags=''      tab='-1'  display='Позивний сигнал (програма мовлення)' ReadOnly='true' background='' bold=''/>"
               + "<item key='callsign'     type='lat'      flags='s'     tab='16'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"

               //
               + "<item key='kState'   type='lat'      flags=''      tab='-1'    display='Внутрішній стан / Міжнародний стан' background='' bold='' />"
               + "<item key='homeState' type='lat'      flags='s'     tab='17'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='internationalState' type='lat'      flags='s'     tab='18'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kTvChan'       type='lat'       flags=''     tab='-1'  display='ТВ канал / частота: відео, МГц / звук, МГц'     ReadOnly='true' background='' bold='y' />"
               + "<item key='tvchan'       type='lat'       flags='s'    tab='19'  display=''     ReadOnly='true' background='' bold='y'  HorizontalAlignment='center' />"
               + "<item key='freqVid'       type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold=''  HorizontalAlignment='center' fontColor='gray' />"
               + "<item key='freqSnd'       type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold=''  HorizontalAlignment='center' fontColor='gray' />"

               //
               + "<item key='mKObj'       type=''       flags=''     tab='-1'  display='Об\"єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='KObj'      type=''     flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "</items>";
            //

            return XmlString;
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            // Сохраняем "Особливі умови"
            objStation.Put("CUST_TXT12", SCDozvil);
            objStation.Put("CUST_TXT13", SCVisnovok);
            objStation.Put("CUST_TXT14", SCNote);
            objStation.Put("CUST_CHB1", IsTechnicalUser);

            //if (station.objChannel != null)
            //   station.objChannel.SaveToDB();

            objStation.Put("CUST_TXT1", NumberOut);//--***         
            objStation.Put("CUST_DAT1", DateOut);//--***       
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус

            //if (NeedSavePosition)
            //    station.objPosition.Save();
            objStation.Put("SITE_ID", station.objPosition.Id);
            
            if (newStation)
            {
                objStation.Put("DIAGA", "HH");
            }

            // Сохраняем лицензии

            SaveLicence();
            CJournal.CheckTable(recordID.Table, recordID.Id, objStation);

            objStation.SaveToDB();

            return true;
        }

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (station.objPosition != null) ? station.objPosition.Province : "";
            string city = (station.objPosition != null) ? station.objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if ((docType == DocType.VISN) || (docType == DocType.VISN_NR))
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.DOZV_OPER))
                fullPath += "ТБ-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLat":
                    AutoFill(cell, grid);
                    break;

                case "system":
                    if (newStation)
                        return;
                    SelectByShort(cell, objStation.GetS("COLOUR_SYSTEM"));
                    break;

                case "KName":
                    cell.Value = objStation.GetS("CUST_TXT7");
                    break;

                case "equip":
                    {
                        AutoFill(cell, grid);
                    }
                    break;

                case "height":
                    cell.Value = objStation.GetD("AGL").ToString("F0");
                    break;

                case "maxHeight":
                    cell.Value = objStation.GetD("EFHGT_MAX").ToString("F0");
                    break;

                case "fiderlen":
                    cell.Value = objStation.GetD("CUST_NBR1").ToString("F0");
                    break;

                case "fiderloss":
                    cell.Value = objStation.GetD("CUST_NBR2").ToString("F3");
                    break;

                case "dir":
                    {
                        //-- From FMTVBase
                        SetDirection();
                        SelectByShort(cell, Direction);
                        //-- From FMTVBase
                    }
                    break;

                case "polar":
                    //-- From FMTVBase
                    SelectByShort(cell, objStation.GetS("POLARIZATION"));
                    AutoFill(cell, grid);
                    //-- From FMTVBase
                    break;

                case "callsign":
                    cell.Value = objStation.GetS("NAME");
                    break;

                case "homeState":
                    if (!newStation)
                        SelectByShort(cell, objStation.GetS("CUST_TXT4"));
                    break;

                case "internationalState":
                    if (!newStation)
                        SelectByShort(cell, objStation.GetS("CUST_TXT5"));
                    break;

                case "KObj":
                    cell.Value = objStation.GetI("ID").ToString();
                    break;

                case "HorzEVP":
                    cell.Value = objStation.GetD("ERP_H").ToString("F3");
                    break;

                case "VertEVP":
                    cell.Value = objStation.GetD("ERP_V").ToString("F3");
                    break;

                case "standart":
                    cell.Value = objStation.GetS("TVSYS_CODE");
                    break;

                case "NesivFreq":
                    cell.Value = objStation.GetD("OFFSET_V_KHZ").ToString("F3");
                    break;

                case "tvchan":
                    {
                        cell.Value = objStation.GetS("CHANNEL");

                        if (station.objChannel != null)
                        {
                            //cell.Value = station.objChannel.GetS("CHANNEL");                    
                            int planId = objStation.GetI("PLAN_ID");
                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r.Select("PLAN_ID,CHANNEL,FREQ,OFFSET,BANDWIDTH");
                            r.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, planId);
                            r.OrderBy("CHANNEL", OrderDirection.Ascending);
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _channelList.AddValue(r.GetS("CHANNEL"));

                                _offsetList.Add(r.GetD("OFFSET"));
                                _freqVideoList.Add(r.GetD("FREQ"));
                                _bwList.Add(r.GetD("BANDWIDTH") / 1000);
                            }
                            r.Close();
                            r.Destroy();

                            objStation.Put("PLAN_ID", planId);

                            cell.PickList = _channelList.GetList();

                            //cell = grid.GetCellFromKey("rxband");
                            SafeFillCellByKey(grid, "rxband", (station.objChannel.GetD("BANDWIDTH") / 1000).ToString(), false);
                            //cell.Value = (station.objChannel.GetD("BANDWIDTH") / 1000).ToString();
                        }
                        else
                        {
                            StringBuilder idChannel = new StringBuilder();
                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,COD");
                            r.SetWhere("COD", IMRecordset.Operation.Like, "АТМ4");
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                idChannel.Append(',');
                                idChannel.Append(r.GetI("ID").ToString());
                            }
                            r.Close();
                            r.Destroy();

                            if (idChannel.Length == 0)
                                return;

                            idChannel = idChannel.Remove(0, 1);
                            List<int> channelIDList = new List<int>();

                            r = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r.Select("PLAN_ID,CHANNEL,FREQ,OFFSET,BANDWIDTH");
                            string strPlan = "[PLAN_ID] in " + "(" + idChannel.ToString() + ")";
                            r.SetAdditional(strPlan);
                            r.OrderBy("CHANNEL", OrderDirection.Ascending);
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _channelList.AddValue(r.GetS("CHANNEL"));
                                channelIDList.Add(r.GetI("PLAN_ID"));

                                //double offset = 0;
                                //double freq = 0;

                                //if (r.GetD("OFFSET") != IM.NullD)
                                //   offset = r.GetD("OFFSET");

                                //if (r.GetD("FREQ") != IM.NullD)
                                //   freq = r.GetD("FREQ");

                                //freq = freq + offset / 1000;
                                //_freqVideoList.AddValue(freq.ToString());

                                _offsetList.Add(r.GetD("OFFSET"));
                                _freqVideoList.Add(r.GetD("FREQ"));
                                _bwList.Add(r.GetD("BANDWIDTH") / 1000);
                            }

                            cell.PickList = _channelList.GetList();
                            cell.Tag = channelIDList;
                        }
                        break;
                    }

                case "freqVid":
                    cell.Value = objStation.GetD("FREQ_V_CARR").ToString("F3");
                    break;

                case "freqSnd":
                    {
                        if (objStation.GetD("FREQ_V_CARR") != IM.NullD)
                        {
                            double freqVid = objStation.GetD("FREQ_V_CARR");
                            double steep = objStation.GetD("DELTA_F_SND1");
                            cell.Value = (steep + freqVid).ToString("F3");
                        }
                        else
                            cell.Value = "0.0";
                    }
                    break;

                case "powVideo":
                    {
                        double powVideo = 0;
                        if (objStation.GetD("PWR_ANT") != IM.NullD)
                        {
                            powVideo = objStation.GetD("PWR_ANT");
                            powVideo = Math.Round(Math.Pow(10.0, powVideo / 10), 3);
                            cell.Value = powVideo.ToString();

                            double ratio = objStation.GetD("PWR_RATIO_1");
                            double powSound = powVideo / ratio;

                            //cell = grid.GetCellFromKey("powSound");
                            SafeFillCellByKey(grid, "powSound", powSound.ToString(), false);
                            //cell.Value = powSound.ToString();
                        }
                        else if (station.objEquip != null)
                        {
                            if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                            {
                                double maxPow = (station.objEquip.GetD("MAX_POWER") - 30.0) / 10;
                                powVideo = Math.Round(Math.Pow(10, maxPow), 3);
                                cell.Value = powVideo.ToString();
                                //===========================================
                                objStation.Put("PWR_ANT", 10.0 * Math.Log10(powVideo));

                                double ratio = objStation.GetD("PWR_RATIO_1");
                                double powSound = powVideo / ratio;

                                //cell = grid.GetCellFromKey("powSound");
                                SafeFillCellByKey(grid, "powSound", powSound.ToString(), false);
                                //cell.Value = powSound.ToString();
                            }
                        }
                    }
                    break;

                //case "powSound":
                //   {
                //      double powSound = 0;
                //      double powVideo = objStation.GetD("PWR_ANT");

                //      if (objStation.GetD("PWR_RATIO_1") != IM.NullD)
                //      {
                //         powSound = objStation.GetD("PWR_RATIO_1");
                //         powSound = Math.Round(powVideo / powSound, 3);
                //         cell.Value = powSound.ToString();
                //      }
                //      else if (station.objEquip != null)
                //      {
                //         if (station.objEquip.GetD("MIN_POWER") != IM.NullD)
                //         {
                //            double minPow = (station.objEquip.GetD("MIN_POWER") - 30.0) / 10;
                //            powSound = Math.Round(Math.Pow(10, minPow), 3);
                //            cell.Value = powSound.ToString();
                //            //===========================================                        
                //            objStation.Put("PWR_RATIO_1", powVideo / powSound);
                //         }
                //      }
                //   }
                //   break;

                //case "rxband":
                //   {
                //      double BW = objStation.GetD("BW");
                //      cell.Value = (BW / 1000.0).ToString("F3");
                //   }
                //   break;

                case "emiVideo":
                    cell.Value = objStation.GetS("DESIG_EM_V");
                    break;

                case "emiAudio":
                    if (!string.IsNullOrEmpty(cell.Value.TrimEnd().TrimStart()))
                    {
                        objStation.Put("DESIG_EM_S1", cell.Value);
                    }
                     cell.Value = objStation.GetS("DESIG_EM_S1");

                    break;

                case "horzGain":
                    //-- From FMTVBase
                    UpdateHorzGain(cell);
                    //-- From FMTVBase
                    break;

                case "vertGain":
                    //-- From FMTVBase
                    UpdateVertGain(cell);
                    //-- From FMTVBase
                    break;
            }
        }

        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            switch (cell.Key)
            {
                case "KLon":
                case "kAddr":
                case "equip":
                case "kAsl":
                case "horzGain":
                case "vertGain":
                    cell.cellStyle = EditStyle.esEllipsis;
                    break;

                case "HorzEVP":
                case "VertEVP":
                case "EVPmax":
                    cell.cellStyle = EditStyle.esEllipsis;
                    cell.ButtonText = "Розрахунок.";
                    break;

                case "system":
                    cell.PickList = _systemList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "polar":
                    cell.PickList = _polarizationList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "dir":
                    cell.PickList = _orientationList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "homeState":
                    cell.PickList = _internalStateList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    cell.Value = _internalStateList.GetList()[0];
                    break;

                case "internationalState":
                    cell.PickList = _intnationalStateList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    cell.Value = _intnationalStateList.GetList()[14];
                    break;

                case "tvchan":
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "standart":
                    cell.PickList = _standartList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "NesivFreq":
                    cell.PickList = _zamichenyaList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;
            }
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "equip":
                    {
                        // Выбор оборудования
                        RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipBro, "NAME", cell.Value, true);
                        if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                        {
                            station.objEquip = IMObject.LoadFromDB(recEquip);
                            objStation.Put("EQUIP_ID", recEquip.Id);
                            CellValidate(cell, grid);
                            AutoFill(cell, grid);
                        }
                    }
                    break;

                case "KLon":
                    {
                        CellValidate(cell, grid);
                        PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionBro, 1, Longitude, Latitude);
                        if (newPos != null)
                        {
                            station.objPosition = newPos;
                            station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                            objStation.Put("SITE_ID", newPos.Id);
                            AutoFill(grid.GetCellFromKey("KLat"), grid);
                            NeedSavePosition = station.objPosition.IsChanged;                            
                        }
                    }
                    break;

                case "kAddr":
                    {
                        if (ShowMessageReference(station.objPosition != null))
                        {
                            PositionState2 newPos = new PositionState2();
                            newPos.LongDms =  Longitude;
                            newPos.LatDms = Latitude;
                            DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionBro, ref newPos, OwnerWindows);
                            if (dr == DialogResult.OK)
                            {
                                station.objPosition = newPos;
                                station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                                AutoFill(grid.GetCellFromKey("KLat"), grid);
                                NeedSavePosition = true;
                            }
                        }                        
                    }
                    break;

                case "horzGain":
                    {
                        //-- From FMTVBase
                        OnPresButtonHorzGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                        Cell fillCell = grid.GetCellFromKey("polar");
                        AutoFill(fillCell, grid);
                        //-- From FMTVBase
                    }
                    break;

                case "vertGain":
                    {
                        //-- From FMTVBase
                        OnPresButtonVertGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                        Cell fillCell = grid.GetCellFromKey("polar");
                        AutoFill(fillCell, grid);
                        //-- From FMTVBase
                    }
                    break;

                case "HorzEVP":
                    {
                        //-- From FMTVBase
                        double Power = objStation.GetD("PWR_ANT");
                        double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
                        cell.Value = CalcERPHorz(Power - FeederLoss);
                        //-- From FMTVBase
                    }
                    break;

                case "VertEVP":
                    {
                        double Power = objStation.GetD("PWR_ANT");
                        double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
                        cell.Value = CalcERPVert(Power - FeederLoss);
                        //-- From FMTVBase               
                    }
                    break;

                case "EVPmax":
                    {
                        string polarization = objStation.GetS("POLARIZATION");

                        double Power = objStation.GetD("PWR_ANT");
                        double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                        //-- From FMTVBase
                        if (polarization == "V")
                        {
                            cell.Value = CalcERPVert(Power - FeederLoss);
                        }
                        else if (polarization == "H")
                        {
                            cell.Value = CalcERPHorz(Power - FeederLoss);
                        }
                        else if (polarization == "M")
                        {
                            cell.Value = CalcERPMixed(Power - FeederLoss);
                        }
                        //-- From FMTVBase
                    }
                    break;

                case "kAsl":
                    {
                        if (station.objPosition != null)
                        {
                            double Longitude = station.objPosition.LonDec;
                            double Latitude = station.objPosition.LatDec;

                            if (Longitude != IM.NullD && Latitude != IM.NullD)
                            {
                                double asl = IMCalculate.CalcALS(Longitude, Latitude, "4DEC");
                                if (asl != IM.NullD)
                                {
                                    cell.Value = IM.RoundDeci(asl, 1).ToString();
                                    OnCellValidate(cell, grid);
                                }
                            }
                        }
                    }
                    break;

                default:
                    base.OnPressButton(cell, grid);
                    break;
            }
        }

        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            CTvAnalog retStation = new CTvAnalog(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            CTvAnalog retStation = newAppl as CTvAnalog;// new EarthApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);

            //retStation.CopyFromMe(grid, station.objEquip != null ? station.objEquip.GetI("ID") : IM.NullI,            
            //   station.objChannel != null ? station.objChannel.GetI("ID") : IM.NullI);         
            //string jj = retStation.radioTech;
            return retStation;
        }

        //private void CopyFromMe(Grid grid, int equipID, int channelID)
        //{
        //   Cell tmpCell = null;
        //   objStation.Put("PWR_RATIO_1", 10);

        //   // Оборудование
        //   if (equipID != 0 && equipID != IM.NullI)
        //   {
        //      tmpCell = grid.GetCellFromKey("KName");
        //      station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipEsta, equipID);
        //      tmpCell.Value = station.objEquip.GetS("NAME");
        //      OnCellValidate(tmpCell, grid);
        //      AutoFill(tmpCell, grid);
        //   }

        //   // канал
        //   if (channelID != 0 && channelID != IM.NullI)
        //   {
        //      objStation.Put("PLAN_ID", channelID);
        //      station.objChannel = IMObject.LoadFromDB(ICSMTbl.itblFreqPlanChan, channelID);
        //   }
        //}

        //============================================================
        /// <summary>
        /// Автоматически заполнить поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            //try
            //{
            Cell fillCell = null;
            switch (cell.Key)
            {
                case "KLat":
                    {
                        if (station.objPosition != null)
                        {
                            double latDMS = station.objPosition.LatDms;
                            double lonDMS = station.objPosition.LongDms;
                            this.Longitude = lonDMS;
                            this.Latitude = latDMS;
                            cell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                            OnCellValidate(cell, grid);

                            fillCell = grid.GetCellFromKey("KLon");
                            fillCell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                            OnCellValidate(fillCell, grid);

                            double asl = station.objPosition.Asl;
                            //fillCell = grid.Ge0CellFromKey("kAsl");
                            SafeFillCellByKey(grid, "kAsl", (asl != IM.NullD) ? asl.ToString("0") : "", true);
                            //fillCell.Value = (asl != IM.NullD) ? asl.ToString("0") : "";
                            //OnCellValidate(fillCell, grid);

                            //fillCell = grid.GetCellFromKey("kAddr");
                            SafeFillCellByKey(grid, "kAddr", station.objPosition.FullAddressAuto, true);
                            OnCellValidate(grid.GetCellFromKey("kAddr"), grid);

                            //fillCell = grid.GetCellFromKey("KPillarName");
                            SafeFillCellByKey(grid, "KPillarName", station.objPosition.Name, true);
                            //fillCell.Value = station.objPosition.GetS("NAME");
                        }
                        else
                        {
                            //fillCell = grid.GetCellFromKey("KLon");
                            SafeFillCellByKey(grid, "KLon", "", false);
                            //fillCell.Value = "";

                            //fillCell = grid.GetCellFromKey("kAsl");
                            SafeFillCellByKey(grid, "kAsl", "", false);
                            //fillCell.Value = "";

                            //fillCell = grid.GetCellFromKey("kAddr");
                            SafeFillCellByKey(grid, "kAddr", "", false);
                            //fillCell.Value = "";
                        }
                    }
                    break;

                case "equip":
                    {
                        if (station.objEquip != null)
                        {
                            // Заполяем поля значениями                    
                            cell.Value = station.objEquip.GetS("NAME");

                            //fillCell = grid.GetCellFromKey("certno");
                            SafeFillCellByKey(grid, "certno", station.objEquip.GetS("CUST_TXT1").ToString(), false);
                            //fillCell.Value = station.objEquip.GetS("CUST_TXT1").ToString();

                            fillCell = grid.GetCellFromKey("certda");
                            DateTime dt = station.objEquip.GetT("CUST_DAT1");

                            if (fillCell != null)
                            {
                                if (dt.Year > 1990)
                                    fillCell.Value = dt.ToString("dd.MM.yyyy");
                                else
                                    fillCell.Value = "";
                            }

                            fillCell = grid.GetCellFromKey("powVideo");
                            if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                            {
                                double maxPow = (station.objEquip.GetD("MAX_POWER") - 30.0) / 10;
                                station.MaxPower = Math.Round(Math.Pow(10, maxPow), 3);
                                fillCell.Value = station.MaxPower.ToString();

                                if (objStation.GetD("PWR_ANT") == IM.NullD)
                                    objStation.Put("PWR_ANT", 10.0 * Math.Log10(station.MaxPower));

                                double ratio = objStation.GetD("PWR_RATIO_1");

                                fillCell = grid.GetCellFromKey("powSound");
                                fillCell.Value = (station.MaxPower / ratio).ToString();
                            }


                            objStation.Put("DESIG_EM_S1", station.objEquip.GetS("CUST_TXT2"));
                            objStation.SaveToDB();


                            SafeFillCellByKey(grid, "emiVideo", station.objEquip.GetS("DESIG_EMISSION"), false);


                            fillCell = grid.GetCellFromKey("emiAudio");
                            if (fillCell != null)
                            {
                                fillCell.Value = station.objEquip.GetS("CUST_TXT2").TrimEnd().TrimStart();
                            }
                            OnCellValidate(fillCell, grid);

     
                            
                            //fillCell = grid.GetCellFromKey("rxband");
                            //double BW = station.objEquip.GetD("BW");
                            //fillCell.Value = (BW / 1000.0).ToString("F3");
                        }
                        else
                        {
                            cell.Value = "";

                            //fillCell = grid.GetCellFromKey("certno");
                            SafeFillCellByKey(grid, "certno", "", false);
                            //fillCell.Value = "";

                            //fillCell = grid.GetCellFromKey("certda");
                            SafeFillCellByKey(grid, "certda", "", false);
                            //fillCell.Value = "";
                        }
                    }
                    break;

                case "polar": //TODO нада буде шоб ваня подивився і розкоментіровать
                    //-- From FMTVBase
                    //AutoFillPolar(cell, grid.GetCellFromKey("horzGain"), grid.GetCellFromKey("vertGain")); 
                    //-- From FMTVBase
                    break;

            }
            //}
            //catch(Exception ex)
            //{
            //   string msg = ex.Message;
            //}

        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLon":
                    {
                        double lon = ConvertType.StrToDMS(cell.Value);
                        if (lon != IM.NullD)
                        {
                            cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                            if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                            if (station.objPosition != null)
                            {
                                station.objPosition.LongDms = lon;
                                IndicateDifference(cell, station.objPosition.LonDiffersFromAdm);
                            }
                            Longitude = lon;
                        }
                    }
                    break;

                case "KLat":
                    {
                        double lat = ConvertType.StrToDMS(cell.Value);
                        if (lat != IM.NullD)
                        {
                            cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat); 
                            if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                            if (station.objPosition != null)
                            {
                                station.objPosition.LatDms = lat;
                                IndicateDifference(cell, station.objPosition.LatDiffersFromAdm);
                            }
                            Latitude = lat;
                        }
                    }
                    break;
                case "kAddr":
                    if (station.objPosition != null)
                        IndicateDifference(cell, station.objPosition.AddrDiffersFromAdm);
                    break;
                case "system":
                    {
                        objStation.Put("COLOUR_SYSTEM", _systemList.GetShortValue(cell.Value));
                    }
                    break;

                case "polar":
                case "dir":
                    {
                        Cell polarCell = grid.GetCellFromKey("polar");
                        Cell dirCell = grid.GetCellFromKey("dir");
                        Cell horzGainCell = grid.GetCellFromKey("horzGain");
                        Cell vertGainCell = grid.GetCellFromKey("vertGain");

                        OnValidateDirectionAndPolarization(polarCell, dirCell, horzGainCell, vertGainCell);
                    }
                    break;

                case "callsign":
                    objStation.Put("NAME", cell.Value);
                    break;

                case "kAsl":
                    {
                        if (station.objPosition == null)
                            break;

                        double asl = ConvertType.ToDouble(cell.Value, IM.NullD);

                        if (asl == IM.NullD)
                        {
                            ShowErrorDigit();
                            return;
                        }
                        else
                        {
                            station.objPosition.Asl = asl;
                            NeedSavePosition = true;
                        }
                    }
                    break;

                case "homeState":
                    {
                        objStation.Put("CUST_TXT4", _internalStateList.GetShortValue(cell.Value));
                    }
                    break;

                case "internationalState":
                    {
                        objStation.Put("CUST_TXT5", _intnationalStateList.GetShortValue(cell.Value));
                    }
                    break;

                case "powVideo":
                    {
                        ChangePowVideo(grid);
                    }
                    break;

                case "equip":
                    {
                        if (station.objEquip != null)
                        {
                            string desigEmission = station.objEquip.GetS("DESIG_EMISSION");
                            //double BandWidth = station.objEquip.GetD("BW");
                            objStation.Put("DESIG_EM", desigEmission);
                            objStation.Put("DESIG_EM_V", desigEmission);
                            //objStation.Put("BW", BandWidth);
                        }
                    }
                    break;

                case "standart":
                    {
                        objStation.Put("TVSYS_CODE", _standartList.GetShortValue(cell.Value));
                    }
                    break;

                case "NesivFreq":
                    {
                        double NesivFreq = ConvertType.ToDouble(cell.Value, IM.NullD);

                        if (NesivFreq == IM.NullD)
                        {
                            ShowErrorDigit();
                            UpdateOneParamGrid(cell, grid);
                        }
                        else
                        {
                            objStation.Put("OFFSET_V_KHZ", NesivFreq);
                        }
                    }
                    break;

                case "fiderlen":
                    {
                        double FeederLen = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if (FeederLen == IM.NullD || FeederLen < 0.0)
                        {
                            ShowErrorDigit();
                            UpdateOneParamGrid(cell, grid);
                        }
                        else
                        {
                            objStation.Put("CUST_NBR1", FeederLen);
                        }

                        double FeederLoss = objStation.GetD("CUST_NBR2");
                        objStation.Put("TX_LOSSES", FeederLoss * FeederLen);
                    }
                    break;

                case "fiderloss":
                    {
                        double FeederLoss = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if (FeederLoss == IM.NullD || FeederLoss < 0.0)
                        {
                            ShowErrorDigit();
                            UpdateOneParamGrid(cell, grid);
                        }
                        else
                        {
                            objStation.Put("CUST_NBR2", FeederLoss);
                        }

                        double FeederLen = objStation.GetD("CUST_NBR1");
                        objStation.Put("TX_LOSSES", FeederLoss * FeederLen);
                    }
                    break;

                case "tvchan":
                    {
                        if (_channelList.GetList().Count == 0 && !newStation)
                            break;

                        int index = 0;

                        objStation.Put("CHANNEL", _channelList.GetShortValue(cell.Value));
                        if (station.objChannel != null)
                        {
                            //station.objChannel.Put("CHANNEL", _channelList.GetShortValue(cell.Value));
                            index = _channelList.GetList().IndexOf(cell.Value);
                        }
                        else if (newStation)
                        {
                            index = cell.PickList.IndexOf(cell.Value);
                            List<int> idChannelList = (List<int>)cell.Tag;
                            int id = idChannelList[index];

                            station.objChannel = IMObject.LoadFromDB(ICSMTbl.itblFreqPlanChan, id);
                            //station.objChannel.Put("CHANNEL", _channelList.GetShortValue(cell.Value));
                            objStation.Put("PLAN_ID", id);
                        }

                        //string video = string.Empty;
                        double bw = 0;
                        double freqVideo = 0;
                        double freq = 0;

                        try
                        {
                            freq = _freqVideoList[index];
                            freqVideo = _freqVideoList[index] + _offsetList[index] / 1000;
                            bw = _bwList[index];
                        }
                        catch (ArgumentNullException ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);

                            return;
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);

                            return;
                        }

                        //=======================================================================
                        //bw
                        cell = grid.GetCellFromKey("rxband");
                        cell.Value = bw.ToString("F3");

                        //=======================================================================
                        //freq Video
                        cell = grid.GetCellFromKey("freqVid");
                        cell.Value = freqVideo.ToString();
                        objStation.Put("FREQ_V_CARR", freqVideo);

                        cell = grid.GetCellFromKey("standart");
                        string shortSt = _standartList.GetShortValue(cell.Value);
                        //=======================================================================
                        // freq sound
                        double freqSound = 0;
                        double steep = 0;
                        if (shortSt == "B" || shortSt == "G" || shortSt == "H")
                        {
                            steep = 5.5;
                        }
                        else if (shortSt == "D" || shortSt == "K" || shortSt == "L" ||
                           shortSt == "D1")
                        {
                            steep = 6.5;
                        }
                        else if (shortSt == "І")
                        {
                            steep = 6.0;
                        }

                        freqSound = freqVideo + steep;
                        objStation.Put("DELTA_F_SND1", steep);
                        objStation.Put("FREQ", freq);
                        cell = grid.GetCellFromKey("freqSnd");
                        cell.Value = freqSound.ToString();
                    }
                    break;

                case "height":
                    {
                        double height = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if (height != IM.NullD)
                        {
                            if (height >= 0.0)
                            {
                                objStation.Put("AGL", height);
                            }
                            else
                            {
                                //MessageBox.Show("The height of antenna must be greater");
                                //UpdateOneParamGrid(cell, grid);
                                objStation.Put("AGL", height);
                                ChangeColor(cell, Colors.badValue);
                            }
                        }
                        else
                        {
                            ShowErrorDigit();
                            UpdateOneParamGrid(cell, grid);
                        }
                    }
                    break;

                //case "maxHeight":
                //   {
                //      double maxheight = ConvertType.ToDouble(cell.Value, IM.NullD);
                //      if (maxheight == IM.NullD)
                //      {
                //         ShowErrorDigit();
                //         return;
                //      }
                //      else
                //      {
                //         objStation.Put("EFHGT_MAX", ConvertType.ToDouble(cell.Value, 0.0));
                //      }
                //   }
                //   break;

                case "emiVideo":
                    objStation.Put("DESIG_EM", cell.Value);
                    objStation.Put("DESIG_EM_V", cell.Value);
                    break;

                case "emiAudio":
                    if (!string.IsNullOrEmpty(cell.Value.TrimEnd().TrimStart()))
                    {
                        objStation.Put("DESIG_EM_S1", cell.Value);
                        objStation.SaveToDB();
                    }

                    break;

                case "horzGain":
                    OnValidateGain(cell, "H");
                    break;

                case "vertGain":
                    OnValidateGain(cell, "V");
                    break;
            }
        }
        private void ShowErrorDigit()
        {
            //MessageBox.Show(CLocaliz.TxT("Please input only digit value"),
            //   CLocaliz.TxT("Please input only digit value"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool ChangePowVideo(Grid grid)
        {
            Cell cell = grid.GetCellFromKey("powVideo");
            double powVideo = ConvertType.ToDouble(cell.Value, IM.NullD);
            cell.BackColor = System.Drawing.Color.White;

            if (powVideo == IM.NullD)
            {
                cell.BackColor = System.Drawing.Color.Red;
                return false;
            }

            if (powVideo > station.MaxPower && station.MaxPower != 0)
            {
                cell.BackColor = System.Drawing.Color.Red;
                return false;
            }

            objStation.Put("PWR_ANT", 10.0 * Math.Log10(powVideo));
            double ratio = objStation.GetD("PWR_RATIO_1");

            cell = grid.GetCellFromKey("powSound");
            cell.Value = (powVideo / ratio).ToString();

            return true;
        }

        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        { }

        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //    r.Select("ID,CUST_DAT2,CUST_DAT3,CUST_TXT2,CUST_TXT3");
        //    r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
        //    try
        //    {
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            switch (docType)
        //            {
        //                case DocType.VISN:
        //                case DocType.VISN_NR:
        //                    r.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    objStation.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    r.Put("CUST_DAT2", startDate);
        //                    objStation.Put("CUST_DAT2", startDate);
        //                    break;
        //            }
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "kAddr"))// && (station.objPosition != null))
                {
                    if (station.objPosition != null)
                    {
                        int posID = station.objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionBro, posID, (IM.TableRight(ICSMTbl.itblPositionBro) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        station.objPosition.LoadStatePosition(station.objPosition.Id, station.objPosition.TableName);
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(gridParam.GetCellFromKey("KLat"), gridParam);
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "equip") && (station.objEquip != null))
                {
                    int EquipID = station.objEquip.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(station.objEquip.GetS("TABLE_NAME"), EquipID);
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KObj") && (objStation != null))
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(TableName, ID);
                    rcPtr.UserEdit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRS formVRS = new FormVRS(recordID.Id, recordID.Table);
            formVRS.ShowDialog();
            formVRS.Dispose();
        }

        private void FillComboList()
        {
            // fills Поляризація
            _polarizationList.AddValueL("V", "Вертикальна");
            _polarizationList.AddValueL("H", "Горизонтальна");
            _polarizationList.AddValueL("M", "Змішана");

            // fills Спрямованість антени 
            _orientationList.AddValue("D", "спрямована");
            _orientationList.AddValue("ND", "неспрямована");

            // fills Стандарт
            _standartList.AddValueL("B", "B");
            _standartList.AddValueL("D", "D");
            _standartList.AddValueL("G", "G");
            _standartList.AddValueL("H", "H");
            _standartList.AddValueL("I", "I");
            _standartList.AddValueL("K", "K");
            _standartList.AddValueL("K1", "K1");
            _standartList.AddValueL("L", "L");
            _standartList.AddValueL("M", "M");
            _standartList.AddValueL("N", "N");
            _standartList.AddValueL("T1", "T1");
            _standartList.AddValueL("L1", "L1");
            _standartList.AddValueL("B1", "B1");

            // fills system
            _systemList.AddValueL("P", "PAL");
            _systemList.AddValueL("S", "SECAM");
            _systemList.AddValueL("N", "NTSC");

            // fills Зміщення несівної частоти, кГц
            _zamichenyaList.AddValue("0,0");
            _zamichenyaList.AddValue("1,302");
            _zamichenyaList.AddValue("-1,302");
            _zamichenyaList.AddValue("2,604");
            _zamichenyaList.AddValue("-2,604");
            _zamichenyaList.AddValue("3,906");
            _zamichenyaList.AddValue("-3,906");
            _zamichenyaList.AddValue("5,208");
            _zamichenyaList.AddValue("-5,208");
            _zamichenyaList.AddValue("6,51");
            _zamichenyaList.AddValue("-6,51");
            _zamichenyaList.AddValue("7,812");
            _zamichenyaList.AddValue("-7,812");
            _zamichenyaList.AddValue("9,115");
            _zamichenyaList.AddValue("-9,115");
            _zamichenyaList.AddValue("10,417");
            _zamichenyaList.AddValue("-10,417");
            _zamichenyaList.AddValue("11,719");
            _zamichenyaList.AddValue("-11,719");
            _zamichenyaList.AddValue("13,021");
            _zamichenyaList.AddValue("-13,021");
            _zamichenyaList.AddValue("14,323");
            _zamichenyaList.AddValue("-14,323");
            _zamichenyaList.AddValue("15,625");
            _zamichenyaList.AddValue("-15,625");
            _zamichenyaList.AddValue("16,927");
            _zamichenyaList.AddValue("-16,927");
            _zamichenyaList.AddValue("18,229");
            _zamichenyaList.AddValue("-18,229");
            _zamichenyaList.AddValue("19,531");
            _zamichenyaList.AddValue("-19,531");
            _zamichenyaList.AddValue("20,833");
            _zamichenyaList.AddValue("-20,833");
            _zamichenyaList.AddValue("22,135");
            _zamichenyaList.AddValue("-22,135");
            _zamichenyaList.AddValue("23,437");
            _zamichenyaList.AddValue("-23,437");
            _zamichenyaList.AddValue("24,71");
            _zamichenyaList.AddValue("-24,71");
            _zamichenyaList.AddValue("26,042");
            _zamichenyaList.AddValue("-26,042");

            // fills Внутрішній стан 
            _internalStateList.AddValueL("1", "Рассматриваемый");
            _internalStateList.AddValueL("2", "Разрешенный");
            _internalStateList.AddValueL("3", "Действующий");
            _internalStateList.AddValueL("4", "Новое сост.8");
            _internalStateList.AddValueL("5", "Новое сост.2");
            _internalStateList.AddValueL("6", "Новое сост.3");
            _internalStateList.AddValueL("7", "Столб");
            _internalStateList.AddValueL("8", "Рекоммендация");
            _internalStateList.AddValueL("9", "Уточнение хар-к");
            _internalStateList.AddValueL("10", "Планируемый");
            _internalStateList.AddValueL("33", "BR IFIC ADD");
            _internalStateList.AddValueL("34", "BR IFIC REC");
            _internalStateList.AddValueL("35", "BR IFIC SUP");
            _internalStateList.AddValueL("36", "BR IFIC MOD");
            _internalStateList.AddValueL("38", "Geneva 06");

            // Міжнародний стан
            _intnationalStateList.AddValueL("A", "Скоорд.");
            _intnationalStateList.AddValueL("B", "Берлин 59");
            _intnationalStateList.AddValueL("C", "Чужой скоорд.");
            _intnationalStateList.AddValueL("D", "Удаленный, был.");
            _intnationalStateList.AddValueL("E", "Цифра, планируем.");
            _intnationalStateList.AddValueL("F", "Прошел коорд.");
            _intnationalStateList.AddValueL("G", "Женева 84");
            _intnationalStateList.AddValueL("G06", "GENEVA 2006");
            _intnationalStateList.AddValueL("H", "Цифра, план Честер");
            _intnationalStateList.AddValueL("I", "Честер");
            _intnationalStateList.AddValueL("K", "На коорд.");
            _intnationalStateList.AddValueL("L", "Цифра, удаленный");
            _intnationalStateList.AddValueL("M", "Цифра, не надо коорд.");
            _intnationalStateList.AddValueL("N", "Не надо коорд.");
            _intnationalStateList.AddValueL("O", "Планируемый");
            _intnationalStateList.AddValueL("P", "Под крышей");
            _intnationalStateList.AddValueL("Q", "Цифра, регистр.");
            _intnationalStateList.AddValueL("R", "Регистр");
            _intnationalStateList.AddValueL("S", "Стокгольм");
            _intnationalStateList.AddValueL("T", "Цифра, скоорд.");
            _intnationalStateList.AddValueL("TP", "Цифра, скоорд. на пп");
            _intnationalStateList.AddValueL("U", "Цифра на коорд.");
            _intnationalStateList.AddValueL("Y", "Цифра, нескоорд.");
            _intnationalStateList.AddValueL("Z", "Не скоорд.");
        }

        public double GetPower()
        {
            double powVideo = 0;
            if (objStation.GetD("PWR_ANT") != IM.NullD)
            {
                powVideo = objStation.GetD("PWR_ANT");
                powVideo = Math.Round(Math.Pow(10.0, powVideo / 10), 3);
                return powVideo;
            }
            /*    else if (station.objEquip != null)
                {
                   if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                   {
                      return station.objEquip.GetD("MAX_POWER");
                   }
                }*/
            return powVideo;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }

    }
}


