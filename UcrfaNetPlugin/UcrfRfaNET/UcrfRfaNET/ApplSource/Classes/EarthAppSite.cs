﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using System.Windows.Forms;
using XICSM.UcrfRfaNET;
using XICSM.UcrfRfaNET.StationsStatus;
using XICSM.UcrfRfaNET.Documents;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    //===============================================================
    /// <summary>
    /// класс станции ЗС
    /// </summary>
    internal class StationEarthStation : BaseStation
    {
        //===================================================
        public double LonDms { get; set; }  //Долгота
        public double LatDms { get; set; }  //Широта
        public double AntDiam { get; set; }  //Диаметр антенны
        public double AntGain { get; set; }//Коэффициент усиления
        public double AntGainRx { get; set; }//Коэффициент усиления приемника
        public double TxBand { get; set; } //Ширина полосы
        public double RxBand { get; set; } //ширина полосы
        public int NoiseT { get; set; } //Шумовая температура
        public double MinPower = -1;     // Минимальная допустимая мощность
        public double MaxPower = -1;     // Максимальная допустимая мощность
        public double CurPower = -1;     // Первая мощность
        public int ModulRx = 0;          // Модуляция приемника
        public int ModulTx = 0;          // Модуляция передатчика
        public string PolarRX = "";      // Поляризация приемника
        public string PolarТX = "";      // Поляризация передатчика
        public new PositionState2 Position { get; set; } //Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна
        public List<double> nomFreqTx = new List<double>();  //Номинальные частоты передатчика
        public List<double> nomFreqRx = new List<double>();  //Номинальные частоты приемника
        public List<string> nomTxEmi = new List<string>();  //Класс излучения передатчика
        public List<string> nomRxEmi = new List<string>();  //Класс излучения приемника
        public List<double> nomPowTx = new List<double>();  //Мощность передатчика
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public StationEarthStation()
        {// Инициализируем все перемнные
            AntDiam = 0;
            AntGain = 0;
            AntGainRx = IM.NullD;
            TxBand = 0;
            RxBand = 0;
            NoiseT = IM.NullI;
            Position = null;
        }
    }
    //===============================================================
    //===============================================================
    /// <summary>
    /// Класс для обработки заявки ЗC
    /// </summary>
    class EarthAppSite : BaseAppClass
    {
        public static class Helper
        {
            //===========================================================
            /// <summary>
            /// 
            /// </summary>
            /// <param name="rcp">R for receiver, E for transmiter</param>
            /// <param name="Id">Earth Station ID</param>
            /// <returns></returns>
            public static int ReadEstaAntennaID(string rcp, int Id)
            {
                int antennaID = IM.NullI;

                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID,ANT_ID,EMI_RCP");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, rcp);//"E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();

                return antennaID;
            }
            //===========================================================
            /// <summary>
            /// Read antenna ID
            /// </summary>
            /// <param name="Id">EARTH_STATION ID</param>
            /// <returns>ANTENNA ID</returns>
            public static RecordPtr ReadAntennaID(int Id)
            {
                int antennaID = IM.NullI;

                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID,ANT_ID,EMI_RCP");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ANT_ID");
                r.Close();
                r.Destroy();

                return new RecordPtr(ICSMTbl.itblAntenna, antennaID);
            }

            /// <summary>
            /// Read Equipment ID      
            /// </summary>
            /// <param name="Id">EARTH_STATION ID</param>
            /// <returns>EQUIP_ESTA ID</returns>
            public static RecordPtr ReadEqipmentID(int Id)
            {
                int equipmentID = IM.NullI;

                int antennaID = ReadEstaAntennaID("E", Id);
                /*IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                   antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();*/

                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        int emissID = IM.NullI;
                        IMRecordset emiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
                        emiss.Select("EGRP_ID,EQPCONF_ID");
                        emiss.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        emiss.Open();
                        if (!emiss.IsEOF())
                            emissID = emiss.GetI("EQPCONF_ID");
                        emiss.Close();
                        emiss.Destroy();

                        if (emissID != IM.NullI)
                        {
                            RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipEsta, IM.NullI);
                            IMRecordset equipEsta = new IMRecordset(ICSMTbl.itblEquipEsta, IMRecordset.Mode.ReadOnly);
                            equipEsta.Select("ID");
                            equipEsta.SetWhere("ID", IMRecordset.Operation.Eq, emissID);
                            equipEsta.Open();
                            if (!equipEsta.IsEOF())
                            {
                                equipmentID = equipEsta.GetI("ID");
                            }
                            equipEsta.Close();
                            equipEsta.Destroy();
                        }
                    }
                }
                return new RecordPtr(ICSMTbl.itblEquipEsta, equipmentID);
            }

            /// <summary>
            /// Read Transmitter Frequencies
            ///   puts it to StationZS.nomFreqTx
            /// </summary>
            public static List<double> ReadTxFrequencies(int Id)
            {
                // Частоты TX
                List<double> nomFreqTx = new List<double>();

                int antennaID = ReadEstaAntennaID("E", Id);

                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        //int emissID = IM.NullI;
                        IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                        rsAssgn.Select("FREQ");
                        rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        try
                        {
                            for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                            {
                                double freq = rsAssgn.GetD("FREQ");
                                if (freq != IM.NullD)
                                    nomFreqTx.Add(freq / 1000.0);
                            }
                        }
                        finally
                        {
                            rsAssgn.Close();
                            rsAssgn.Destroy();
                        }
                    }
                }

                return nomFreqTx;
            }

            /// <summary>      
            /// Read Receiver Frequencies
            ///   puts it to StationZS.nomFreqRx      
            /// </summary>
            public static List<double> ReadRxFrequencies(int Id)
            {
                // Частоты RX
                List<double> nomFreqRx = new List<double>();

                int antennaID = ReadEstaAntennaID("R", Id);

                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        //int emissID = IM.NullI;
                        IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                        rsAssgn.Select("FREQ");
                        rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        try
                        {
                            for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                            {
                                double freq = rsAssgn.GetD("FREQ");
                                if (freq != IM.NullD)
                                    nomFreqRx.Add(freq / 1000.0);
                            }
                        }
                        finally
                        {
                            rsAssgn.Close();
                            rsAssgn.Destroy();
                        }
                    }
                }

                return nomFreqRx;
                //HelpFunction.ToString(cell, st.nomFreqRx);            
            }

            /// <summary>
            ///  Return Tx Polarizations
            /// </summary>
            /// <returns>Short letter code of polarization</returns>
            public static string ReadPolarTX(int Id)
            {
                string PolarТX = "";

                int antennaID = ReadEstaAntennaID("E", Id);

                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,POLAR_TYPE");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        PolarТX = group.GetS("POLAR_TYPE");
                    group.Close();
                    group.Destroy();
                }
                return PolarТX;
            }

            /// <summary>
            ///  Return Rx Polarizations
            /// </summary>
            /// <returns>Short letter code of polarization</returns>
            public static string ReadPolarRX(int Id)
            {
                // Поляризация RX
                string PolarRX = "";
                int antennaID = ReadEstaAntennaID("R", Id);

                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,POLAR_TYPE");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        PolarRX = group.GetS("POLAR_TYPE");
                    group.Close();
                    group.Destroy();
                }
                return PolarRX;
            }

        };

        public static readonly string TableName = ICSMTbl.itblEarthStation;
        public StationEarthStation st = new StationEarthStation(); // Дополнительные данные станции
        //===================================================
        public EarthAppSite(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, EarthAppSite.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppZS;
            department = DepartmentType.VFSR;
            departmentSector = DepartmentSectorType.VFSR_SSZ;
            if (newStation == true)
            {
                // Заполняем некоторые поля, которые идут по умолчанию
                objStation["NAME"] = "ЗС-" + recordID.Id.ToString();
                objStation["ADM"] = "UKR";
                objStation["STANDARD"] = _radioTech;
            }
            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT14");
            SCVisnovok = objStation.GetS("CUST_TXT15");
            SCNote = objStation.GetS("CUST_TXT16");
            //----
            addValidVisnMonth = 6;
            if (st != null && st.Position != null)
                st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            if (st.objEquip != null)
            {
                st.objEquip.Dispose();
                st.objEquip = null;
            }
            if (st.objAntenna != null)
            {
                st.objAntenna.Dispose();
                st.objAntenna = null;
            }
            base.DisposeExec();
        }
        //===================================================
        /// <summary>
        /// Создание XML строки для построения грида
        /// </summary>
        /// <returns>XML строка</returns>
        protected override string GetXMLParamGrid()
        {
            string XmlString =
                 "<items>"
               + "<item key='header1'      fontType='b' type=''       flags=''  display='Параметри' ReadOnly='true' background='DarkGray' HorizontalAlignment='center' />"
               + "<item key='header2'      fontType='b' type=''       flags='s' display='Значення'     ReadOnly='true' background='DarkGray' HorizontalAlignment='center'/>"

               + "<item key='mKSatNet'     fontType='b' type=''       flags='' display='Назва супутникової мережі'     ReadOnly='true' background=''/>"
               + "<item key='KSatNet'      fontType='b' type='string' DBField='SAT_NAME' flags='s' tab='1'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKOrbit'      fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Орбітальна позиція'     ReadOnly='true' background=''/>"
               + "<item key='KOrbit'       fontType='i' fontColor='gray' type='double' DBField='LONG_NOM'      flags='s' display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKLat'        fontType='b' type=''       flags=''     tab=''  display='Широта (гр.хв.сек.)'     ReadOnly='true' background=''/>"
               + "<item key='KLat'         fontType='b' type=''       flags='s'     tab='2'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKLon'        fontType='b' type=''       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background=''/>"
               + "<item key='KLon'         fontType='b' type=''       flags='s'     tab='3'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKAsl'        fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Висота поверхні землі'     ReadOnly='true' background=''/>"
               + "<item key='KAsl'         fontType='i' fontColor='gray' type='double' DBField='ASL'      flags='s'     tab='5'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKAzim'       fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Кути азимуту/елевації випромінювання, град'     ReadOnly='false' background=''/>"
               + "<item key='KAzim'        fontType='i' fontColor='gray' type='double' DBField='AZM_TO'      flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"
               + "<item key='KElev'        fontType='i' fontColor='gray' type='double' DBField='ELEV_MIN'   flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKAddr'      fontType='b' type=''       flags=''     tab=''  display='Адреса місця встановлення РЕЗ'     ReadOnly='true' background='' />"
               + "<item key='KAddr'       fontType='b' type=''       flags='s'     tab='4'  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKName'      fontType='b' type=''       flags=''     tab=''  display='Назва/Тип РЕЗ'     ReadOnly='true' background=''/>"
               + "<item key='KName'       fontType='b' type=''       flags='s'     tab='6'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKPow'       type=''       flags=''     tab=''  display='Макс. потужність передавача на вх. антени, дБВт'     ReadOnly='true' background='' />"
               + "<item key='KPow'       type=''       flags='s'     tab='7'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKTxBand'    fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Ширина смуги, кГц; клас випромінювання передавача'     ReadOnly='true' background=''/>"
               + "<item key='KTxBand'     fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"
               + "<item key='KTxEmi'      fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKTxMod'     fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Тип і параметри модуляції випром., що передаються'     ReadOnly='true' background=''/>"
               + "<item key='KTxMod'      fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKRxBand'    fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Ширина смуги; клас випромінювання, що приймається'     ReadOnly='true' background=''/>"
               + "<item key='KRxBand'     fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"
               + "<item key='KRxEmi'      fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKRxMod'     fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Тип і параметри модуляції випром., що приймається'     ReadOnly='true' background=''/>"
               + "<item key='KRxMod'      fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKCertNo'    fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Сертифікат відповідності (номер / дата)'     ReadOnly='true' background='' />"
               + "<item key='KCertNo'     fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"
               + "<item key='KCertDa'     fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKNoise'     fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Шумова температура приймальної системи, К'     ReadOnly='true' background=''/>"
               + "<item key='KNoise'      fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKAntType'   fontType='b' type=''       flags=''     tab=''  display='Тип антени'     ReadOnly='true' background=''/>"
               + "<item key='KAntType'    fontType='b' type=''       flags='s'     tab='8'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKAgl'       type=''       flags=''     tab=''  display='Висота антени над рівнем землі, м'     ReadOnly='true' background=''/>"
               + "<item key='KAgl' type='double' DBField='AGL'       flags='s'     tab='9'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKDiam'      fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Діаметр антени, м'     ReadOnly='true' background=''/>"
               + "<item key='KDiam'       fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKKtx'       fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Коефіцієнт підсилення (передавання/приймання), дБі'     ReadOnly='true' background=''/>"
               + "<item key='KKtx'        fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"
               + "<item key='KKrx'        fontType='i' fontColor='gray' type=''       flags='s'     tab=''  display=''     ReadOnly='true' background='' HorizontalAlignment='center'/>"

               + "<item key='mKPolarRx'       type=''       flags=''     tab=''  display='Тип поляризації: у режимі приймання'     ReadOnly='true' background=''/>"
               + "<item key='KPolarRx'       type=''       flags='s'     tab='10'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKNomRx'      fontType='b' type=''       flags=''     tab=''  display='Номінали робочих частот у режимі приймання, ГГц'     ReadOnly='true' background=''/>"
               + "<item key='KNomRx'       fontType='b' type=''       flags='s'     tab='11'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKPolarTx'       type=''       flags=''     tab=''  display='Тип поляризації: у режимі передавання'     ReadOnly='true' background=''/>"
               + "<item key='KPolarTx'       type=''       flags='s'     tab='12'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKNomTx'      fontType='b' type=''       flags=''     tab=''  display='Номінали робочих частот у режимі передав., ГГц'     ReadOnly='true' background=''/>"
               + "<item key='KNomTx'       fontType='b' type=''       flags='s'     tab='13'  display=''     ReadOnly='false' background='' HorizontalAlignment='center'/>"

               + "<item key='mKObj'       fontType='i' fontColor='gray' type=''       flags=''     tab=''  display='Об\"єкт(и) РЧП'     ReadOnly='true' background=''/>"
               + "<item key='KObj'        fontType='i' fontColor='gray' type=''       flags='s'     tab='14'  display='' ReadOnly='true' HorizontalAlignment='center'/>"
               + "</items>"
               ;
            return XmlString;
        }
        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KSatNet") || (cell.Key == "KLon")
               || (cell.Key == "KAddr") || (cell.Key == "KName")
               || (cell.Key == "KAntType") || (cell.Key == "KObj")
               || (cell.Key == "KAsl"))
                cell.cellStyle = EditStyle.esEllipsis;
            else if (cell.Key == "KNomTx")
            {
                cell.cellStyle = EditStyle.esEllipsis;
                cell.ButtonText = "Ліц.";
            }
            else if ((cell.Key == "KPolarRx") || (cell.Key == "KPolarTx"))
            {
                cell.PickList = CPolarization.getPolarizationList(PolarizarionType.Standard);
                cell.cellStyle = EditStyle.esPickList;
            }
        }
        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormEMS formEms = new FormEMS(recordID.Table, recordID.Id);
            formEms.ShowDialog();
            formEms.Dispose();
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            EarthAppSite retStation = new EarthAppSite(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            EarthAppSite retStation = newAppl as EarthAppSite;
            retStation.CopyFromMe(grid, objStation.GetS("SAT_NAME"),
               (st.objEquip != null) ? st.objEquip.GetI("ID") : IM.NullI,
               (st.objAntenna != null) ? st.objAntenna.GetI("ID") : IM.NullI,
               (st.nomPowTx.Count > 0) ? st.nomPowTx[0] : -1, st.NoiseT,
               st.PolarRX, st.PolarТX);
            return retStation;
        }
        //===========================================================
        public void CopyFromMe(Grid grid, string SAT_NAME, int equipID, int antID, double maxPow, int NoiseT, string PolarRX, string PolarTX)
        {
            Cell tmpCell = null;
            // Спутник
            objStation["SAT_NAME"] = SAT_NAME;
            tmpCell = grid.GetCellFromKey("KSatNet");
            tmpCell.Value = SAT_NAME;
            CellValidate(tmpCell, grid);
            // Оборудование
            if ((equipID != 0) && (equipID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("KName");
                st.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipEsta, equipID);
                tmpCell.Value = st.objEquip.GetS("NAME");
                OnCellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }
            if ((antID != 0) && (antID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("KAntType");
                st.objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntenna, antID);
                tmpCell.Value = st.objAntenna.GetS("NAME");
                OnCellValidate(tmpCell, grid);
            }
            // Мощность
            tmpCell = grid.GetCellFromKey("KPow");
            tmpCell.Value = maxPow.ToString();
            OnCellValidate(tmpCell, grid);
            // !!
            st.NoiseT = NoiseT;
            // Поляризация
            tmpCell = grid.GetCellFromKey("KPolarRx");
            tmpCell.Value = PolarRX;
            OnCellValidate(tmpCell, grid);

            tmpCell = grid.GetCellFromKey("KPolarTx");
            tmpCell.Value = PolarRX;
            OnCellValidate(tmpCell, grid);
        }
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "KAddr"))// && (st.Position != null))
                {
                    if (st.Position != null)
                    {
                        Forms.AdminSiteAllTech2.Show(st.Position.TableName, st.Position.Id, (IM.TableRight(st.Position.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        //position may appear be changed
                        st.Position.LoadStatePosition(st.Position.Id, st.Position.TableName);
                        st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
                        cell.Value = st.Position.FullAddressAuto;
                        st.LonDms = st.Position.LongDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon"), gridParam);
                        st.LatDms = st.Position.LatDms;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLat"), gridParam);
                        Cell aCell = gridParam.GetCellFromKey("KAsl");
                        aCell.Value = st.Position.Asl.ToString();
                        OnCellValidate(aCell, gridParam);
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "KAntType") && (st.objAntenna != null))
                {
                    RecordPtr rcPtr = new RecordPtr(st.objAntenna.GetS("TABLE_NAME"), st.objAntenna.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KName") && (st.objEquip != null))
                {
                    RecordPtr rcPtr = new RecordPtr(st.objEquip.GetS("TABLE_NAME"), st.objEquip.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KObj") && (objStation != null))
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(TableName, ID);
                    rcPtr.UserEdit();
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            if (st.Position != null)
            {
                //st.Position.Save();
                objStation["SITE_ID"] = st.Position.Id;
                // Координаты
                objStation["CSYS"] = st.Position.Csys;
                objStation["X"] = st.Position.X;
                objStation["Y"] = st.Position.Y;
                objStation["LONGITUDE"] = st.Position.LonDec;
                objStation["LATITUDE"] = st.Position.LatDec;
            }
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус
            // Сохраняем "Особливі умови"
            objStation["CUST_TXT14"] = SCDozvil;
            objStation["CUST_TXT15"] = SCVisnovok;
            objStation["CUST_TXT16"] = SCNote;
            objStation.Put("CUST_CHB1", IsTechnicalUser);
            objStation["ALTITUDE"] = objStation.GetD("ASL") + objStation.GetD("AGL");
            CJournal.CheckTable(recordID.Table, recordID.Id, objStation);
            objStation.SaveToDB();
            if (newStation)
            {
                // Якогось хера не зберігаеться вище
                // Полів CUST_TXT14 - CUST_TXT16 нема в EARTH_STATION гага три рази
                IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
                r.Select("ID,DEM_RECEIPT_NUM,DEM_RECEIPT_DATE,CONFIRM_NUM,CONFIRM_DATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r["DEM_RECEIPT_NUM"] = NumberOut;//--***
                        r["DEM_RECEIPT_DATE"] = DateOut;//--***
                        r["CONFIRM_NUM"] = NumberIn;
                        r["CONFIRM_DATE"] = DateIn;
                        r.Update();
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            // Сохраняем лицензии
            SaveLicence();
            // Сохраняем данне по антенне
            // --- Передатчик
            {
                // Антенна
                int estaAntID = 0;
                {
                    IMRecordset estaAnt = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,ESTA_ID,EMI_RCP,BEAM_NAME,ANT_TYPE,DIAMETER,GAIN,ANT_ID";
                    estaAnt.Select(fieldsSelect);
                    estaAnt.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                    estaAnt.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                    estaAnt.Open();
                    bool tmp = estaAnt.IsEOF();
                    if (estaAnt.IsEOF())
                    {
                        estaAnt.AddNew();
                        estaAnt["ID"] = IM.AllocID(ICSMTbl.itblEstaAntenna, 1, -1);
                        estaAnt["ESTA_ID"] = recordID.Id;
                        estaAnt["EMI_RCP"] = "E";
                        estaAnt["BEAM_NAME"] = "1";
                        estaAnt["ANT_TYPE"] = "REC-580";  //TODO: parameter
                    }
                    else
                    {
                        estaAnt.Edit();
                    }
                    estaAntID = estaAnt.GetI("ID");
                    estaAnt["DIAMETER"] = st.AntDiam;
                    estaAnt["GAIN"] = st.AntGain;
                    if (st.objAntenna != null)
                        estaAnt["ANT_ID"] = st.objAntenna.GetI("ID");
                    CJournal.CheckTable(ICSMTbl.itblEstaAntenna, estaAntID, estaAnt, fieldsSelect);
                    estaAnt.Update();
                    estaAnt.Close();
                    estaAnt.Destroy();
                }
                // -------
                // Группа
                int estaGroupID = 0;
                {
                    IMRecordset estaGroup = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,EANT_ID,SERV_TYPE,BDWDTH,POLAR_TYPE,PWR_MAX";
                    estaGroup.Select(fieldsSelect);
                    estaGroup.SetWhere("EANT_ID", IMRecordset.Operation.Eq, estaAntID);
                    estaGroup.Open();
                    if (estaGroup.IsEOF())
                    {
                        estaGroup.AddNew();
                        estaGroup["ID"] = IM.AllocID(ICSMTbl.itblEstaGroup, 1, -1);
                        estaGroup["EANT_ID"] = estaAntID;
                    }
                    else
                    {
                        estaGroup.Edit();
                    }
                    estaGroupID = estaGroup.GetI("ID");
                    estaGroup["POLAR_TYPE"] = st.PolarТX;
                    estaGroup["PWR_MAX"] = st.CurPower;
                    estaGroup["SERV_TYPE"] = 8;  //TODO: parameter
                    estaGroup["BDWDTH"] = st.TxBand;
                    CJournal.CheckTable(ICSMTbl.itblEstaGroup, estaGroupID, estaGroup, fieldsSelect);
                    estaGroup.Update();
                    estaGroup.Close();
                    estaGroup.Destroy();
                }
                // Частоты
                {
                    IMRecordset estaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,EGRP_ID,FREQ";
                    estaAssgn.Select(fieldsSelect);
                    estaAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
                    estaAssgn.Open();
                    bool isAdd = false;
                    foreach (double val in st.nomFreqTx)
                    {
                        if (estaAssgn.IsEOF() || (isAdd == true))
                        {
                            isAdd = true;
                            estaAssgn.AddNew();
                            estaAssgn["ID"] = IM.AllocID(ICSMTbl.itblEstaAssgn, 1, -1);
                            estaAssgn["EGRP_ID"] = estaGroupID;
                        }
                        else
                        {
                            estaAssgn.Edit();
                        }
                        estaAssgn["FREQ"] = val * 1000.0;
                        CJournal.CheckTable(ICSMTbl.itblEstaAssgn, estaAssgn.GetI("ID"), estaAssgn, fieldsSelect);
                        estaAssgn.Update();
                        estaAssgn.MoveNext();
                    }

                    if (isAdd == false)
                        while (!estaAssgn.IsEOF())
                        {
                            estaAssgn.Delete();
                            estaAssgn.MoveNext();
                        }

                    estaAssgn.Close();
                    estaAssgn.Destroy();
                }
                // ----
                {
                    if (st.nomTxEmi.Count != st.nomPowTx.Count)
                    {
                        throw new IMException("There are different counts of the emissions and powers. Check the transmitters group.");
                    }

                    IMRecordset estaEmi = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "EGRP_ID,DESIG_EMISSION,EQPCONF_ID,EMI_TYPE,PWR_TOT_PK,PWR_DS_MAX,PWR_MIN_PK,PWR_DS_MIN,MBITPS,C_TO_N"; //??
                    estaEmi.Select(fieldsSelect);
                    estaEmi.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
                    estaEmi.SetWhere("EMI_TYPE", IMRecordset.Operation.Like, "N");
                    estaEmi.Open();
                    bool isAdd = false;
                    for (int i = 0; i < st.nomTxEmi.Count; i++)
                    {
                        if (estaEmi.IsEOF() || (isAdd == true))
                        {
                            isAdd = true;
                            estaEmi.AddNew();
                            estaEmi["EGRP_ID"] = estaGroupID;
                            estaEmi["EMI_TYPE"] = "N";
                        }
                        else
                        {
                            estaEmi.Edit();
                        }
                        estaEmi["DESIG_EMISSION"] = st.nomTxEmi[i];
                        double pwr = st.nomPowTx[i] + 30.0;
                        estaEmi["PWR_TOT_PK"] = pwr;

                        //double pwd = 10.0 * Math.Log10(Math.Pow(10.0,pwr/10.0) / bw);
                        //    if (maxpwd < pwd) maxpwd = pwd;
                        //    emiDs->FieldByName("PWR_DS_MAX")->AsFloat       = pwd;

                        if (st.objEquip != null)
                        {
                            estaEmi.Put("EQPCONF_ID", st.objEquip.GetI("ID"));
                        }

                        estaEmi.Update();
                        estaEmi.MoveNext();
                    }

                    if (isAdd == false)
                        while (!estaEmi.IsEOF())
                        {
                            estaEmi.Delete();
                            estaEmi.MoveNext();
                        }

                    estaEmi.Close();
                    estaEmi.Destroy();
                }
            }
            // --- Приемние
            {
                // Антенна
                int estaAntID = 0;
                {
                    IMRecordset estaAnt = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,ESTA_ID,EMI_RCP,BEAM_NAME,ANT_TYPE,DIAMETER,GAIN,ANT_ID";
                    estaAnt.Select(fieldsSelect);
                    estaAnt.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                    estaAnt.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "R");
                    estaAnt.Open();
                    bool tmp = estaAnt.IsEOF();
                    if (estaAnt.IsEOF())
                    {
                        estaAnt.AddNew();
                        estaAnt["ID"] = IM.AllocID(ICSMTbl.itblEstaAntenna, 1, -1);
                        estaAnt["ESTA_ID"] = recordID.Id;
                        estaAnt["EMI_RCP"] = "R";
                        estaAnt["BEAM_NAME"] = "1";
                        estaAnt["ANT_TYPE"] = "REC-580";  //TODO: parameter
                    }
                    else
                    {
                        estaAnt.Edit();
                    }
                    estaAntID = estaAnt.GetI("ID");
                    estaAnt["DIAMETER"] = st.AntDiam;
                    estaAnt["GAIN"] = (st.AntGainRx != IM.NullD) ? st.AntGainRx : st.AntGain;
                    if (st.objAntenna != null)
                        estaAnt["ANT_ID"] = st.objAntenna.GetI("ID");
                    CJournal.CheckTable(ICSMTbl.itblEstaAntenna, estaAntID, estaAnt, fieldsSelect);
                    estaAnt.Update();
                    estaAnt.Close();
                    estaAnt.Destroy();
                }
                // -------
                // Группа
                int estaGroupID = 0;
                {
                    IMRecordset estaGroup = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,EANT_ID,SERV_TYPE,BDWDTH,NOISE_T,POLAR_TYPE,PWR_MAX";
                    estaGroup.Select(fieldsSelect);
                    estaGroup.SetWhere("EANT_ID", IMRecordset.Operation.Eq, estaAntID);
                    estaGroup.Open();
                    if (estaGroup.IsEOF())
                    {
                        estaGroup.AddNew();
                        estaGroup["ID"] = IM.AllocID(ICSMTbl.itblEstaGroup, 1, -1);
                        estaGroup["EANT_ID"] = estaAntID;
                    }
                    else
                    {
                        estaGroup.Edit();
                    }
                    estaGroupID = estaGroup.GetI("ID");
                    estaGroup["POLAR_TYPE"] = st.PolarRX;
                    estaGroup["PWR_MAX"] = st.CurPower;
                    estaGroup["NOISE_T"] = st.NoiseT;
                    estaGroup["SERV_TYPE"] = 8;  //TODO: parameter
                    estaGroup["BDWDTH"] = st.RxBand;
                    CJournal.CheckTable(ICSMTbl.itblEstaGroup, estaGroupID, estaGroup, fieldsSelect);
                    estaGroup.Update();
                    estaGroup.Close();
                    estaGroup.Destroy();
                }
                // Частоты
                {
                    IMRecordset estaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadWrite);
                    string fieldsSelect = "ID,EGRP_ID,FREQ";
                    estaAssgn.Select(fieldsSelect);
                    estaAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
                    estaAssgn.Open();
                    bool isAdd = false;
                    foreach (double val in st.nomFreqRx)
                    {
                        if (estaAssgn.IsEOF() || (isAdd == true))
                        {
                            isAdd = true;
                            estaAssgn.AddNew();
                            estaAssgn["ID"] = IM.AllocID(ICSMTbl.itblEstaAssgn, 1, -1);
                            estaAssgn["EGRP_ID"] = estaGroupID;
                        }
                        else
                        {
                            estaAssgn.Edit();
                        }
                        estaAssgn["FREQ"] = val * 1000.0;
                        CJournal.CheckTable(ICSMTbl.itblEstaAssgn, estaAssgn.GetI("ID"), estaAssgn, fieldsSelect);
                        estaAssgn.Update();
                        estaAssgn.MoveNext();
                    }

                    if (isAdd == false)
                        while (!estaAssgn.IsEOF())
                        {
                            estaAssgn.Delete();
                            estaAssgn.MoveNext();
                        }

                    estaAssgn.Close();
                    estaAssgn.Destroy();
                }
                // ----
                {
                    IMRecordset estaEmi = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadWrite);
                    estaEmi.Select("EGRP_ID,DESIG_EMISSION,EMI_TYPE,PWR_TOT_PK,PWR_DS_MAX,EQPCONF_ID");
                    estaEmi.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, estaGroupID);
                    estaEmi.Open();
                    bool isAdd = false;
                    for (int i = 0; i < st.nomTxEmi.Count; i++)
                    {
                        if (estaEmi.IsEOF() || (isAdd == true))
                        {
                            isAdd = true;
                            estaEmi.AddNew();
                            estaEmi["EGRP_ID"] = estaGroupID;
                        }
                        else
                        {
                            estaEmi.Edit();
                        }
                        estaEmi["DESIG_EMISSION"] = st.nomTxEmi[i];
                        estaEmi.Update();
                        estaEmi.MoveNext();
                    }

                    if (isAdd == false)
                        while (!estaEmi.IsEOF())
                        {
                            estaEmi.Delete();
                            estaEmi.MoveNext();
                        }

                    estaEmi.Close();
                    estaEmi.Destroy();
                }
            }
            return true;
        }

        public void LookupEquipment(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipEsta, "NAME", initialValue, true);
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                if (st.objEquip != null)
                    st.objEquip.Dispose();

                st.objEquip = IMObject.LoadFromDB(recEquip);
                cell.Value = st.objEquip.GetS("NAME");

                OnCellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }

        public void LookupAntenna(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntenna, param);
            if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
            {
                if (st.objAntenna != null)
                    st.objAntenna.Dispose();

                st.objAntenna = IMObject.LoadFromDB(RecAnt);
                cell.Value = st.objAntenna.GetS("NAME");
                OnCellValidate(cell, grid);
            }
        }

        public override double GetBw()
        {
            if (st.objEquip == null)
            {
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        int emissID = IM.NullI;
                        IMRecordset emiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
                        emiss.Select("EGRP_ID,EQPCONF_ID");
                        emiss.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        emiss.Open();
                        if (!emiss.IsEOF())
                            emissID = emiss.GetI("EQPCONF_ID");
                        emiss.Close();
                        emiss.Destroy();

                        if (emissID != IM.NullI)
                        {
                            RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipEsta, IM.NullI);
                            IMRecordset equipEsta = new IMRecordset(ICSMTbl.itblEquipEsta, IMRecordset.Mode.ReadOnly);
                            equipEsta.Select("ID");
                            equipEsta.SetWhere("ID", IMRecordset.Operation.Eq, emissID);
                            equipEsta.Open();
                            if (!equipEsta.IsEOF())
                            {
                                recEquip.Id = equipEsta.GetI("ID");
                            }
                            equipEsta.Close();
                            equipEsta.Destroy();
                            if (recEquip.Id != IM.NullI)
                            {
                                st.objEquip = IMObject.LoadFromDB(recEquip);

                            }
                        }
                    }
                }
                int ID_Stat = recordID.Id;
                st.nomFreqRx = EarthAppSite.Helper.ReadRxFrequencies(ID_Stat);
                st.nomFreqTx = EarthAppSite.Helper.ReadTxFrequencies(ID_Stat);
            }

            return st.objEquip.GetD("BW") / 1000.0;
        }

        internal override CLicence.Band[] GetRxTxBands(RxTx type, string region)
        {
            //region parameter will be ignored
            List<CLicence.Band> bandList = new List<CLicence.Band>();
            double BW = GetBw();
            int bandId = 0;

            if (type != RxTx.Rx)
                foreach (double freq in st.nomFreqTx)
                {
                    double fMHz = freq * 1000.0;
                    bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(fMHz - BW / 2, 6), IM.RoundDeci(fMHz, 6), IM.RoundDeci(fMHz + BW / 2, 6), CLicence.Band.Parity.No));
                }

            if (type != RxTx.Tx)
                foreach (double freq in st.nomFreqRx)
                {
                    double fMHz = freq * 1000.0;
                    bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(fMHz - BW / 2, 6), IM.RoundDeci(fMHz, 6), IM.RoundDeci(fMHz + BW / 2, 6), CLicence.Band.Parity.No));
                }

            return bandList.ToArray();
        }

        public override string GetProvince()
        {
            if (st.Position == null)
            {
                int posId = objStation.GetI("SITE_ID");
                if ((posId > 0) && (posId != IM.NullI))
                {
                    st.Position = new PositionState2();
                    st.Position.LoadStatePosition(posId, ICSMTbl.itblPositionEs);
                    st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
                }
            }
            return st.Position.Province != "" ? st.Position.Province : st.Position.City;
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "KSatNet")
            {  // Выбераем запись из таблицы
                FSelectSatellite frm = new FSelectSatellite(HelpFunction.ReplaceQuotaSumbols(cell.Value));
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    //RecordPtr satellite = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the satellite"), ICSMTbl.itblSatellites, "");
                    RecordPtr satellite = new RecordPtr(ICSMTbl.itblSatellites, frm.GetID());
                    // Заносим новые данные в ячейки
                    string satelName = "";
                    IMRecordset r = new IMRecordset(ICSMTbl.itblSatellites, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,SAT_NAME,LONG_NOM");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, satellite.Id);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        satelName = r.GetS("SAT_NAME");
                    }
                    r.Close();
                    r.Destroy();

                    cell.Value = satelName;
                    OnCellValidate(cell, grid);
                    double lon = st.LonDms;
                    double lat = st.LatDms;
                    Cell fillCell = grid.GetCellFromKey("KOrbit");
                    double orbita = ConvertType.ToDouble(fillCell, IM.NullD);

                    if (lon != IM.NullD && lat != IM.NullD && lon != 0 && lat != 0)
                    {
                        if (orbita != IM.NullD)
                        {
                            double azim = IMCalculate.CalcAzimSat(lon, lat, orbita);
                            double curAzim = objStation.GetD("AZM_TO");
                            if (azim != IM.NullD && curAzim != azim)
                            {
                                fillCell = grid.GetCellFromKey("KAzim");
                                fillCell.Value = IM.RoundDeci(azim, 1).ToString();
                                OnCellValidate(fillCell, grid);
                            }
                        }
                        fillCell = grid.GetCellFromKey("KAgl");
                        double agl = ConvertType.ToDouble(fillCell, IM.NullD);
                        fillCell = grid.GetCellFromKey("KAsl");
                        double asl = ConvertType.ToDouble(fillCell, IM.NullD);
                        if (agl != IM.NullD && asl != IM.NullD && orbita != IM.NullD)
                        {
                            double elev = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1);
                            double curElev = objStation.GetD("ELEV_MIN");
                            if (curElev != elev)
                            {
                                fillCell = grid.GetCellFromKey("KElev");
                                fillCell.Value = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1).ToString();
                                OnCellValidate(fillCell, grid);
                            }
                        }
                    }
                }
                frm.Dispose();
            }
            else if (cell.Key == "KAddr")
            {
                if (ShowMessageReference(st.Position != null))
                {
                    PositionState2 newPos = new PositionState2();
                    //newPos.LoadStatePosition(st.Position);
                    //newPos.Csys = "4DMS";
                    newPos.LongDms = st.LonDms;
                    newPos.LatDms = st.LatDms;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionEs, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        st.Position = newPos;
                        st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
                        cell.Value = st.Position.FullAddressAuto;
                        st.LonDms = st.Position.LongDms;
                        st.LatDms = st.Position.LatDms;
                        //---
                        grid.GetCellFromKey("KLon").Value = HelpFunction.DmsToString(st.Position.LongDms, EnumCoordLine.Lon);
                        grid.GetCellFromKey("KLat").Value = HelpFunction.DmsToString(st.Position.LatDms, EnumCoordLine.Lat);
                        OnCellValidate(grid.GetCellFromKey("KLon"), grid);
                        OnCellValidate(grid.GetCellFromKey("KLat"), grid);
                        OnCellValidate(grid.GetCellFromKey("KAddr"), grid);

                        Cell fillCell = grid.GetCellFromKey("KAsl");
                        fillCell.Value = st.Position.Asl.ToString();
                        OnCellValidate(fillCell, grid);
                    }
                }
            }
            else if (cell.Key == "KName")
            {// Выбор оборудования
                /*string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cell.Value) + "*\"}";
                // Выбераем запись из таблицы
                RecordPtr RecEquip = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipEsta, param);
                if ((RecEquip.Id > 0) && (RecEquip.Id < IM.NullI))
                {
                    st.objEquip = IMObject.LoadFromDB(RecEquip);
                    cell.Value = st.objEquip.GetS("NAME");                    

                    OnCellValidate(cell, grid);
                    AutoFill(cell, grid);
                }*/
                LookupEquipment(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "KAntType")
            {// Выбор антены
                /*string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cell.Value) + "*\"}";
                // Выбераем запись из таблицы
                RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntenna, param);
                if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
                {
                    st.objAntenna = IMObject.LoadFromDB(RecAnt);
                    cell.Value = st.objAntenna.GetS("NAME");
                    OnCellValidate(cell, grid);
                }*/
                LookupAntenna(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "KLon")
            {
                // Выбераем запись из таблицы
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionEs, 1, st.LonDms, st.LatDms);
                if (newPos != null)
                {
                    st.Position = newPos;
                    st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
                    st.LonDms = st.Position.LongDms;
                    st.LatDms = st.Position.LatDms;
                    grid.GetCellFromKey("KLon").Value = HelpFunction.DmsToString(st.Position.LongDms, EnumCoordLine.Lon);
                    grid.GetCellFromKey("KLat").Value = HelpFunction.DmsToString(st.Position.LatDms, EnumCoordLine.Lat);
                    OnCellValidate(grid.GetCellFromKey("KLon"), grid);
                    OnCellValidate(grid.GetCellFromKey("KLat"), grid);
                    Cell fillCell = grid.GetCellFromKey("KAddr");
                    fillCell.Value = st.Position.FullAddressAuto;
                    OnCellValidate(fillCell, grid);
                    fillCell = grid.GetCellFromKey("KAsl");
                    fillCell.Value = st.Position.Asl.ToString();
                    OnCellValidate(fillCell, grid);
                }
            }
            else if (cell.Key == "KNomTx")
            {
                RefreshLicenceList();
            }
            else if (cell.Key == "KObj")
            {
                recordID.UserEdit();
            }
            else if (cell.Key == "KAsl")
            {
                double lon = st.LonDms;
                double lat = st.LatDms;
                Cell fillCell = grid.GetCellFromKey("KOrbit");
                double orbita = ConvertType.ToDouble(fillCell, IM.NullD);

                if (lon != IM.NullD && lat != IM.NullD)
                {
                    double asl = IMCalculate.CalcALS(lon, lat, "4DEC");
                    if (asl != IM.NullD)
                    {
                        cell.Value = IM.RoundDeci(asl, 1).ToString();
                        OnCellValidate(cell, grid);
                    }

                    if (orbita != IM.NullD)
                    {
                        double azim = IMCalculate.CalcAzimSat(lon, lat, orbita);
                        double curAzim = objStation.GetD("AZM_TO");
                        if (azim != IM.NullD && curAzim == IM.NullD)
                        {
                            fillCell = grid.GetCellFromKey("KAzim");
                            fillCell.Value = IM.RoundDeci(azim, 1).ToString();
                            OnCellValidate(fillCell, grid);
                        }
                    }
                }
            }
            else
                base.OnPressButton(cell, grid);
        }
        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLon":
                    {
                        double lon = ConvertType.StrToDMS(cell.Value);
                        if (lon != IM.NullD)
                        {
                            st.LonDms = lon;
                            if (!HelpFunction.ValidateCoords(st.LonDms, EnumCoordLine.Lon))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                            double lonDec = st.LonDms.DmsToDec();
                            double latDec = st.LatDms.DmsToDec();
                            Cell fillCell = grid.GetCellFromKey("KOrbit");
                            double orbita = ConvertType.ToDouble(fillCell, IM.NullD);
                            if (lonDec != IM.NullD && latDec != IM.NullD && lonDec != 0 && latDec != 0)
                            {
                                if (orbita != IM.NullD)
                                {
                                    double azim = IMCalculate.CalcAzimSat(lonDec, latDec, orbita);
                                    double curAzim = objStation.GetD("AZM_TO");
                                    if (azim != IM.NullD && curAzim != azim)
                                    {
                                        fillCell = grid.GetCellFromKey("KAzim");
                                        fillCell.Value = IM.RoundDeci(azim, 1).ToString();
                                        OnCellValidate(fillCell, grid);
                                    }
                                }
                            }
                        }
                        if (st != null && st.Position != null)
                            IndicateDifference(cell, st.Position.LonDiffersFromAdm);
                    }
                    break;
                case "KLat":
                    {
                        double lat = ConvertType.StrToDMS(cell.Value);
                        if (lat != IM.NullD)
                        {
                            st.LatDms = lat;
                            if (!HelpFunction.ValidateCoords(st.LatDms, EnumCoordLine.Lat))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                        }
                        if (st != null && st.Position != null)
                            IndicateDifference(cell, st.Position.LatDiffersFromAdm);
                    }
                    break;
                case "KAddr":
                    if (st != null && st.Position != null)
                        IndicateDifference(cell, st.Position.AddrDiffersFromAdm);
                    break;
            }

            if (cell.Key == "KSatNet")
            {
                HelpFunction.LoadFromCellToDb(cell, objStation);
                objStation["TYPE"] = "S";
                objStation["STANDARD"] = CRadioTech.ZS;
                AutoFill(cell, grid);
            }
            else if ((cell.Key == "KOrbit")
                    || (cell.Key == "KAzim")
                    || (cell.Key == "KElev")
                    || (cell.Key == "KAsl"))
            {
                HelpFunction.LoadFromCellToDb(cell, objStation);
                HelpFunction.LoadFromDbToCell(cell, objStation);
            }
            else if (cell.Key == "KAgl")
            {
                HelpFunction.LoadFromCellToDb(cell, objStation);
                HelpFunction.LoadFromDbToCell(cell, objStation);
                AutoFill(cell, grid);
            }
            else if (cell.Key == "KName")
            {
                if ((st.objEquip != null) && (cell.Value != st.objEquip.GetS("NAME")))
                    st.objEquip = null;  //Будем искать новое оборудование, так как имя уже не совпадает

                if (st.objEquip == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipEsta, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblEquipEsta, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recEquip.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recEquip.Id > 0)
                    {
                        st.objEquip = IMObject.LoadFromDB(recEquip);
                    }
                }
            }
            else if (cell.Key == "KAntType")
            {
                if ((st.objAntenna != null) && (cell.Value != st.objAntenna.GetS("NAME")))
                    st.objAntenna = null;  //Будем искать новую антенну, так как имя уже не совпадает

                if (st.objAntenna == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recAnt = new RecordPtr(ICSMTbl.itblAntenna, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblAntenna, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recAnt.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recAnt.Id > 0)
                        st.objAntenna = IMObject.LoadFromDB(recAnt);
                }
                AutoFill(cell, grid);
            }
            else if (cell.Key == "KNomTx")
            {
                st.nomFreqTx = ConvertType.ToDoubleList(cell);
                HelpFunction.ToString(cell, st.nomFreqTx);
            }
            else if (cell.Key == "KNomRx")
            {
                st.nomFreqRx = ConvertType.ToDoubleList(cell);
                HelpFunction.ToString(cell, st.nomFreqRx);
            }
            else if (cell.Key == "KTxEmi")
            {
                st.nomTxEmi = ConvertType.ToStringList(cell);
                HelpFunction.ToString<string>(cell, st.nomTxEmi);
            }
            else if (cell.Key == "KRxEmi")
            {
                st.nomRxEmi = ConvertType.ToStringList(cell);
                HelpFunction.ToString<string>(cell, st.nomRxEmi);
            }
            else if (cell.Key == "KPow")
            {
                st.nomPowTx = ConvertType.ToDoubleList(cell);
                st.CurPower = 0.0;
                if (st.nomPowTx.Count > 0)
                    st.CurPower = st.nomPowTx[0];
                HelpFunction.ToString(cell, st.nomPowTx);
                if ((st.nomPowTx.Count > 0) && ((st.nomPowTx[0] > st.MaxPower) || (st.nomPowTx[0] < st.MinPower)))
                    cell.BackColor = System.Drawing.Color.Red;
                else cell.BackColor = System.Drawing.Color.White;
            }
            else if (cell.Key == "KDiam")
            {
                st.AntDiam = ConvertType.ToDouble(cell, 0);
                cell.Value = st.AntDiam.ToString();
            }
            else if (cell.Key == "KKtx")
            {
                st.AntGain = ConvertType.ToDouble(cell, 0);
                cell.Value = st.AntGain.ToString();
            }
            else if (cell.Key == "KKrx")
            {
                st.AntGainRx = ConvertType.ToDouble(cell, IM.NullD);
                if (st.AntGainRx == IM.NullD)
                    cell.Value = "";
                else cell.Value = st.AntGainRx.ToString();
            }
            else if (cell.Key == "KTxBand")
            {
                st.TxBand = ConvertType.ToDouble(cell, 0);
                cell.Value = st.TxBand.ToString();
            }
            else if (cell.Key == "KRxBand")
            {
                st.RxBand = ConvertType.ToDouble(cell, 0);
                cell.Value = st.RxBand.ToString();
            }
            else if (cell.Key == "KPolarRx")
            {
                st.PolarRX = CPolarization.getShortPolarization(cell.Value);
                cell.Value = st.PolarRX;
            }
            else if (cell.Key == "KPolarTx")
            {
                st.PolarТX = CPolarization.getShortPolarization(cell.Value);
                cell.Value = st.PolarТX;
            }
            //???
            //else
            //   base.CellValidate(cell, grid);
            //-----
            //Здесь так надо. Долгота обрабатываеться в базовом класе
            //if (cell.Key == "KLon")
            //{
            //   double lon = objStation.GetD("LONGITUDE");
            //   double lat = objStation.GetD("LATITUDE");
            //   Cell fillCell = grid.GetCellFromKey("KOrbit");
            //   double orbita = ConvertType.ToDouble(fillCell, IM.NullD);
            //   if (lon != IM.NullD && lat != IM.NullD && lon != 0 && lat != 0)
            //   {
            //      if (orbita != IM.NullD)
            //      {
            //         double azim = IMCalculate.CalcAzimSat(lon, lat, orbita);
            //         double curAzim = objStation.GetD("AZM_TO");
            //         if (azim != IM.NullD && curAzim != azim)
            //         {
            //            fillCell = grid.GetCellFromKey("KAzim");
            //            fillCell.Value = IM.RoundDeci(azim, 1).ToString();
            //            OnCellValidate(fillCell, grid);
            //         }
            //      }
            //   }
            //}
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLon":
                    cell.Value = HelpFunction.DmsToString(st.LonDms, EnumCoordLine.Lon);
                    OnCellValidate(cell, grid);
                    break;
                case "KLat":
                    if ((st.Position == null) && (objStation != null))
                    {
                        int posId = objStation.GetI("SITE_ID");
                        if ((posId > 0) && (posId != IM.NullI))
                        {
                            st.Position = new PositionState2();
                            st.Position.LoadStatePosition(posId, ICSMTbl.itblPositionEs);
                            st.Position.GetAdminSiteInfo(st.Position.AdminSiteId);
                            st.LonDms = st.Position.LongDms;
                            st.LatDms = st.Position.LatDms;
                        }
                    }
                    cell.Value = HelpFunction.DmsToString(st.LatDms, EnumCoordLine.Lat);
                    OnCellValidate(cell, grid);
                    break;
            }

            if ((cell.Key == "KOrbit")
               || (cell.Key == "KAzim")
               || (cell.Key == "KElev")
               || (cell.Key == "KAsl")
               || (cell.Key == "KSatNet")   // Вытаскиваем имя спутника
               || (cell.Key == "KAgl"))
            {
                HelpFunction.LoadFromDbToCell(cell, objStation);
            }
            else if (cell.Key == "KName")
            {
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        int emissID = IM.NullI;
                        IMRecordset emiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
                        emiss.Select("EGRP_ID,EQPCONF_ID");
                        emiss.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        emiss.Open();
                        if (!emiss.IsEOF())
                            emissID = emiss.GetI("EQPCONF_ID");
                        emiss.Close();
                        emiss.Destroy();

                        if (emissID != IM.NullI)
                        {
                            RecordPtr recEquip = new RecordPtr(ICSMTbl.itblEquipEsta, IM.NullI);
                            IMRecordset equipEsta = new IMRecordset(ICSMTbl.itblEquipEsta, IMRecordset.Mode.ReadOnly);
                            equipEsta.Select("ID");
                            equipEsta.SetWhere("ID", IMRecordset.Operation.Eq, emissID);
                            equipEsta.Open();
                            if (!equipEsta.IsEOF())
                            {
                                recEquip.Id = equipEsta.GetI("ID");
                            }
                            equipEsta.Close();
                            equipEsta.Destroy();
                            if (recEquip.Id != IM.NullI)
                            {
                                st.objEquip = IMObject.LoadFromDB(recEquip);
                                cell.Value = st.objEquip.GetS("NAME");
                                AutoFill(cell, grid);
                            }
                        }
                    }
                }
            }
            else if (cell.Key == "KAntType")
            {
                if (st.objAntenna == null)
                {  // Ищем антену
                    int antennaID = IM.NullI;
                    IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                    r.Select("ESTA_ID,DIAMETER,GAIN,ANT_ID");
                    r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                    r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                    r.Open();
                    if (!r.IsEOF())
                        antennaID = r.GetI("ANT_ID");
                    r.Close();
                    r.Destroy();

                    if ((antennaID > 0) && (antennaID != IM.NullI))
                        st.objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntenna, antennaID);
                }
                if (st.objAntenna != null)
                {
                    cell.Value = st.objAntenna.GetS("NAME");
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "KAddr")
            {
                if (st.Position != null)
                {
                    cell.Value = st.Position.FullAddressAuto;
                    OnCellValidate(cell, grid);
                }
            }
            else if (cell.Key == "KNomTx")
            {// Частоты TX
                st.nomFreqTx.Clear();
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        //int emissID = IM.NullI;
                        IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                        rsAssgn.Select("FREQ");
                        rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        try
                        {
                            for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                            {
                                double freq = rsAssgn.GetD("FREQ");
                                if (freq != IM.NullD)
                                    st.nomFreqTx.Add(freq / 1000.0);
                            }
                        }
                        finally
                        {
                            rsAssgn.Close();
                            rsAssgn.Destroy();
                        }
                    }
                }
                HelpFunction.ToString(cell, st.nomFreqTx);
            }
            else if (cell.Key == "KNomRx")
            {// Частоты RX
                st.nomFreqRx.Clear();
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "R");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,ID");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        groupID = group.GetI("ID");
                    group.Close();
                    group.Destroy();

                    if (groupID != IM.NullI)
                    {
                        //int emissID = IM.NullI;
                        IMRecordset rsAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                        rsAssgn.Select("FREQ");
                        rsAssgn.SetWhere("EGRP_ID", IMRecordset.Operation.Eq, groupID);
                        try
                        {
                            for (rsAssgn.Open(); !rsAssgn.IsEOF(); rsAssgn.MoveNext())
                            {
                                double freq = rsAssgn.GetD("FREQ");
                                if (freq != IM.NullD)
                                    st.nomFreqRx.Add(freq / 1000.0);
                            }
                        }
                        finally
                        {
                            rsAssgn.Close();
                            rsAssgn.Destroy();
                        }
                    }
                }
                HelpFunction.ToString(cell, st.nomFreqRx);
            }
            else if (cell.Key == "KPolarTx")
            {// Поляризация TX
                st.PolarТX = "";
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,POLAR_TYPE,PWR_MAX");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                    {
                        st.PolarТX = group.GetS("POLAR_TYPE");
                        double powr = group.GetD("PWR_MAX");
                        if (powr != IM.NullD)
                        {
                            Cell fillCell = grid.GetCellFromKey("KPow");
                            if (fillCell != null)
                            {
                                fillCell.Value = powr.Round(1).ToString();
                                CellValidate(fillCell, grid);
                            }
                        }
                    }
                    group.Close();
                    group.Destroy();
                }
                cell.Value = st.PolarТX;
            }
            else if (cell.Key == "KPolarRx")
            {// Поляризация RX
                st.PolarRX = "";
                int antennaID = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
                r.Select("ESTA_ID,ID");
                r.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "R");
                r.Open();
                if (!r.IsEOF())
                    antennaID = r.GetI("ID");
                r.Close();
                r.Destroy();
                if (antennaID != IM.NullI)
                {
                    int groupID = IM.NullI;
                    IMRecordset group = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
                    group.Select("EANT_ID,POLAR_TYPE,PWR_MAX");
                    group.SetWhere("EANT_ID", IMRecordset.Operation.Eq, antennaID);
                    group.Open();
                    if (!group.IsEOF())
                        st.PolarRX = group.GetS("POLAR_TYPE");
                    group.Close();
                    group.Destroy();
                }
                cell.Value = st.PolarRX;
            }
            else if (cell.Key == "KObj")
                cell.Value = objStation.GetI("ID").ToString();
            //???
            //else
            //    base.UpdateOneParamGrid(cell, grid);
            //----------------------------------
            // Дополнительная загрузка
            //if (cell.Key == "KLon")
            //{
            //    if (st.Position == null)
            //    {
            //        int posId = objStation.GetI("SITE_ID");
            //        if ((posId > 0) && (posId != IM.NullI))
            //        {
            //            st.Position = new PositionState();
            //            st.Position.LoadStatePosition(posId, ICSMTbl.itblPositionEs);
            //        }
            //    }
            //}
        }
        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            if (cell.Key == "KSatNet")
            {// Вытаскиваем поле LONG_NOM
                double long_nom = IM.NullD;
                IMRecordset r = new IMRecordset(ICSMTbl.itblSatellites, IMRecordset.Mode.ReadOnly);
                r.Select("ID,SAT_NAME,LONG_NOM");
                r.SetWhere("SAT_NAME", IMRecordset.Operation.Like, cell.Value);
                r.Open();
                if (!r.IsEOF())
                {
                    long_nom = r.GetD("LONG_NOM");
                }
                r.Close();
                r.Destroy();
                Cell cellLongNom = grid.GetCellFromKey("KOrbit");
                if (long_nom != IM.NullD)
                    cellLongNom.Value = long_nom.ToString();
                else cellLongNom.Value = "";
                OnCellValidate(cellLongNom, grid);
            }
            else if (cell.Key == "KName")
            {
                st.MinPower = -1;
                st.MaxPower = -1;
                //Заполняем дополнитеотные поля
                if (st.objEquip != null)
                {// Заполяем поля значениями
                    if (st.objEquip.GetD("MIN_POWER") != IM.NullD)
                        st.MinPower = st.objEquip.GetD("MIN_POWER") - 30;
                    if (st.objEquip.GetD("MAX_POWER") != IM.NullD)
                        st.MaxPower = st.objEquip.GetD("MAX_POWER") - 30;

                    Cell fillCell = null;

                    if ((st.MinPower > -1) || (st.MaxPower > -1))
                    {
                        fillCell = grid.GetCellFromKey("KPow");
                        fillCell.Value = Math.Max(st.MinPower, st.MaxPower).ToString();
                        OnCellValidate(fillCell, grid);
                    }

                    double bw = st.objEquip.GetD("BW");
                    if (bw != IM.NullD)
                    {
                        fillCell = grid.GetCellFromKey("KTxBand");
                        if (fillCell != null)
                        {
                            fillCell.Value = bw.ToString();
                            OnCellValidate(fillCell, grid);
                        }
                    }

                    fillCell = grid.GetCellFromKey("KTxEmi");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objEquip.GetS("DESIG_EMISSION");
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KTxMod");
                    if (fillCell != null)
                    {
                        fillCell.Value = CModulation.getModulationString(st.objEquip.GetS("MODULATION"));
                        OnCellValidate(fillCell, grid);
                    }

                    bw = st.objEquip.GetD("BW");
                    if (bw != IM.NullD)
                    {
                        fillCell = grid.GetCellFromKey("KRxBand");
                        if (fillCell != null)
                        {
                            fillCell.Value = bw.ToString();
                            OnCellValidate(fillCell, grid);
                        }
                    }

                    fillCell = grid.GetCellFromKey("KRxEmi");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objEquip.GetS("DESIG_EMISSION");
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KRxMod");
                    if (fillCell != null)
                    {
                        fillCell.Value = CModulation.getModulationString(st.objEquip.GetS("MODULATION"));
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KCertNo");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objEquip.GetS("CUST_TXT1");
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KCertDa");
                    if (fillCell != null)
                    {
                        DateTime dt = st.objEquip.GetT("CUST_DAT1");
                        if (dt.Year > 1900)
                        {
                            fillCell.Value = dt.ToString("dd.MM.yyyy");
                            OnCellValidate(fillCell, grid);
                        }
                    }

                    double noise_f = st.objEquip.GetD("NOISE_F");
                    if (noise_f != IM.NullD)
                    {
                        double noise_k = Math.Pow(10, (noise_f / 10));
                        double noiseTmp = 290 * (noise_k - 1) + 50;
                        st.NoiseT = (int)(noiseTmp + 1);

                        fillCell = grid.GetCellFromKey("KNoise");
                        if (fillCell != null)
                        {
                            fillCell.Value = st.NoiseT.ToString();
                            OnCellValidate(fillCell, grid);
                        }
                    }
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("KPow");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KTxBand");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KTxEmi");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KTxMod");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KRxBand");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KRxEmi");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KRxMod");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KCertNo");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KCertDa");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KNoise");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);
                    st.NoiseT = IM.NullI;
                }
            }
            else if (cell.Key == "KAntType")
            {
                //Заполняем дополнитеотные поля
                if (st.objAntenna != null)
                {// Заполяем поля значениями
                    Cell fillCell = grid.GetCellFromKey("KDiam");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objAntenna.GetD("DIAMETER").ToString();
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KKtx");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objAntenna.GetD("GAIN").ToString();
                        OnCellValidate(fillCell, grid);
                    }

                    fillCell = grid.GetCellFromKey("KKrx");
                    if (fillCell != null)
                    {
                        fillCell.Value = st.objAntenna.GetD("LOW_GAIN").ToString();
                        OnCellValidate(fillCell, grid);
                    }
                }
                else
                {// Обнуляем все поля
                    Cell fillCell = grid.GetCellFromKey("KDiam");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KKtx");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);

                    fillCell = grid.GetCellFromKey("KKrx");
                    fillCell.Value = "";
                    OnCellValidate(fillCell, grid);
                }
            }
            else if (cell.Key == "KAgl")
            {//Автоматически расчитваем elevation
                double agl = ConvertType.ToDouble(cell, IM.NullD);
                Cell fillCell = grid.GetCellFromKey("KAsl");
                double asl = ConvertType.ToDouble(fillCell, IM.NullD);
                double lon = st.LonDms;
                double lat = st.LatDms;
                fillCell = grid.GetCellFromKey("KOrbit");
                double orbita = ConvertType.ToDouble(fillCell, IM.NullD);
                if (agl != IM.NullD && asl != IM.NullD && lon != IM.NullD && lat != IM.NullD && orbita != IM.NullD)
                {
                    double elev = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1);
                    double curElev = objStation.GetD("ELEV_MIN");
                    if (curElev == IM.NullD)
                    {
                        fillCell = grid.GetCellFromKey("KElev");
                        fillCell.Value = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1).ToString();
                        OnCellValidate(fillCell, grid);
                    }
                }
            }
            else
                base.AutoFill(cell, grid);
        }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (st.Position != null) ? st.Position.Province : "";
            string city = (st.Position != null) ? st.Position.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if (docType == DocType.VISN)
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DOZV)
                fullPath += "ЗС-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
               || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //    r.Select("ID,DEM_RECEIPT_NUM,DEM_RECEIPT_DATE,CONFIRM_NUM,CONFIRM_DATE,SUPPR_DECIS_NUM,SUPPR_DEM_DATE,SUPPR_DATE,SUPPR_DEM_REF,MODIF_DECIS_NUM,MODIF_DEM_DATE,MODIF_DATE,MODIF_DEM_REF");
        //    r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
        //    try
        //    {
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            switch (docType)
        //            {
        //                case DocType.VISN:
        //                    r.Put("MODIF_DECIS_NUM", docName.ToFileNameWithoutExtension());
        //                    objStation.Put("MODIF_DECIS_NUM", docName.ToFileNameWithoutExtension());
        //                    r.Put("MODIF_DEM_DATE", startDate);
        //                    objStation.Put("MODIF_DEM_DATE", startDate);
        //                    r.Put("MODIF_DATE", endDate);
        //                    objStation.Put("MODIF_DATE", endDate);
        //                    r.Put("MODIF_DEM_REF", IM.ConnectedUser());
        //                    objStation.Put("MODIF_DEM_REF", IM.ConnectedUser());
        //                    break;
        //            }
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
            r.Select("ID,DEM_RECEIPT_NUM,DEM_RECEIPT_DATE,CONFIRM_NUM,CONFIRM_DATE");
            r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r["DEM_RECEIPT_NUM"] = NumberOut;
                    r["DEM_RECEIPT_DATE"] = DateOut;
                    r["CONFIRM_NUM"] = NumberIn;
                    r["CONFIRM_DATE"] = DateIn;
                    r.Update();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
    }
}
