﻿using System.Collections.Generic;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HCM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class ForeignCoordMobStationX : ForeighnCoord
    {
        public ForeignCoordMobStationX(string tech, string tableName, string tableFreq)
            : base(tech, tableName, tableFreq) { }

        public override List<CoordForm.FreqId> CalculateFreq4Station(List<int> objIdList)
        {
            List<CoordForm.FreqId> freqStatList = new List<CoordForm.FreqId>();
            string tableFreq = ICSMTbl.itblMobStaFreqs;
            if (TableName == ICSMTbl.itblMobStation2)
                tableFreq += "2";
            IMRecordset rFreq = new IMRecordset(tableFreq, IMRecordset.Mode.ReadOnly);
            rFreq.Select("TX_FREQ,STA_ID,ID");
            string objIdStr = "";
            for (int i = 0; i < objIdList.Count; i++)
            {
                objIdStr += "[STA_ID]=" + objIdList[i];
                if (i < objIdList.Count - 1)
                    objIdStr += " OR ";
            }
            if (!string.IsNullOrEmpty(objIdStr))
                rFreq.SetAdditional(objIdStr);
            try
            {
                for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
                    freqStatList.Add(new CoordForm.FreqId(rFreq.GetD("TX_FREQ") * 1000000, rFreq.GetI("STA_ID"), rFreq.GetI("ID")));
            }
            finally
            {
                rFreq.Final();
            }
            List<CoordForm.FreqId> tmpFreq = new List<CoordForm.FreqId>();
            for (int i = 0; i < freqStatList.Count; i++)
                if (!tmpFreq.Select(obj => obj.Freq).ToList().Contains(freqStatList[i].Freq))
                    tmpFreq.Add(freqStatList[i]);
            return tmpFreq;
        }

        public override List<CoordForm.CoordTable> ShowStatusStation()
        {
            FirstStatusList = new List<string>();
            for (int i = 0; i < CoordTbl.Count; i++)
            {
                //Визначити Id запису для WIEN_ANS_MOB
                int wienCoordId = IM.NullI;
                int index = i;
                CoordForm.FreqId tmpFreq = FreqStatList.Find(obj => (obj.Freq == CoordTbl[index].Freq));

                IMRecordset rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
                rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
                rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
                try
                {
                    rXfaWien.Open();
                    if (!rXfaWien.IsEOF())
                        wienCoordId = rXfaWien.GetI("ID");
                }
                finally
                {
                    rXfaWien.Final();
                }

                //Визначення основних параметрів таблиці WienAnsMob
                string statusAns = "";
                string statusReq = "";
                int wienHeadreq = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadOnly);
                r.Select("ID,CO_LINK_ID,COUNTRY_DEST_ID,COORD_STAT_ANS,COORD_STAT_REQ,HEADREQ_ID");
                r.SetWhere("CO_LINK_ID", IMRecordset.Operation.Eq, wienCoordId);
                r.SetWhere("COUNTRY_DEST_ID", IMRecordset.Operation.Eq, CoordTbl[i].Administration);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        statusAns = r.GetS("COORD_STAT_ANS");
                        statusReq = r.GetS("COORD_STAT_REQ");
                        wienHeadreq = r.GetI("HEADREQ_ID");
                    }
                }
                finally
                {
                    r.Final();
                }

                if (string.IsNullOrEmpty(statusAns) && statusReq == "B")
                    CoordTbl[i].Status = "A";
                if (!string.IsNullOrEmpty(statusAns))
                    CoordTbl[i].Status = statusAns;
                if ((wienHeadreq != IM.NullI) && (CoordTbl[i].Status == "A"))
                    CoordTbl[i].Status = "CA";
                FirstStatusList.Add(CoordTbl[i].Status);
                //#6268 Побажання: автоматичне заповнення статусу "Координація потрібна"
                //>>>>
                if (string.IsNullOrEmpty(CoordTbl[i].Status))
                    CoordTbl[i].Status = "A";
                //<<<<

            }
            return CoordTbl;
        }

        /// <summary>
        /// Визначення статусу записів для таблиць mobstationX і збереження
        /// </summary>
        public override void AcceptStation()
        {
            for (int i = 0; i < CoordTbl.Count; i++)
            {
                if ((string.IsNullOrEmpty(FirstStatusList[i]) && CoordTbl[i].Status == "A") ||
                    (FirstStatusList[i] == "CN" && CoordTbl[i].Status == "A"))
                    MobEmptyA(i);
                else if (FirstStatusList[i] == "A" && ((CoordTbl[i].Status == "CN") || (string.IsNullOrEmpty(CoordTbl[i].Status))))
                    MobAcn(i);
                else if (string.IsNullOrEmpty(FirstStatusList[i]) && ((CoordTbl[i].Status == "CN") || (string.IsNullOrEmpty(CoordTbl[i].Status))))
                    MobEmptyCn(i);
                FirstStatusList[i] = CoordTbl[i].Status;
            }

            AfterSaveMobStation();
        }

        /// <summary>
        /// Зміни в таблицях після збереження параметрів
        /// </summary>
        private void AfterSaveMobStation()
        {
            # region comment
            //for (int i = 0; i < CoordTbl.Count; i++)
            //{
            //    int wienCoordId = IM.NullI;
            //    string status = "";
            //    List<string> wienAnsList = new List<string>();
            //    List<string> wienAnsAdmList = new List<string>();
            //    int index = i;
            //    CoordForm.FreqId tmpFreq = FreqStatList.Find(obj => (obj.Freq == CoordTbl[index].Freq));

            //    //Визначення id для таблиці coord
            //    IMRecordset rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
            //    rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
            //    rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            //    rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
            //    try
            //    {
            //        rXfaWien.Open();
            //        if (!rXfaWien.IsEOF())
            //            wienCoordId = rXfaWien.GetI("ID");
            //    }
            //    finally
            //    {
            //        rXfaWien.Final();
            //    }

            //    IMRecordset r;
            //    if (wienCoordId != IM.NullI)
            //    {
            //        r = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadOnly);
            //        r.Select("ID,COUNTRY_DEST_ID,CO_LINK_ID,COORD_STAT_ANS");
            //        r.SetWhere("CO_LINK_ID", IMRecordset.Operation.Eq, wienCoordId);
            //        try
            //        {
            //            for (r.Open(); !r.IsEOF(); r.MoveNext())
            //            {
            //                string stat = r.GetS("COORD_STAT_ANS");
            //                wienAnsList.Add(stat);
            //                if (!string.IsNullOrEmpty(stat) && stat != "CN")
            //                    wienAnsAdmList.Add(r.GetS("COUNTRY_DEST_ID"));
            //            }
            //        }
            //        finally
            //        {
            //            r.Final();
            //        }
            //        if (wienAnsList.Exists(obj => (obj == "CN" || string.IsNullOrEmpty(obj))))
            //            status = "CN";
            //        if (wienAnsList.Exists(obj => (obj == "C")))
            //            status = "C";
            //        if (wienAnsList.Exists(obj => (obj == "Z")))
            //            status = "Z";
            //        if (wienAnsList.Exists(obj => (obj == "CA")))
            //            status = "CA";
            //        if (wienAnsList.Exists(obj => (obj == "A")))
            //            status = "A";

            //        int idCoord = 0;
            //        rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
            //        rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
            //        rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            //        rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
            //        try
            //        {
            //            rXfaWien.Open();
            //            if (!rXfaWien.IsEOF())
            //                idCoord = rXfaWien.GetI("ID");
            //        }
            //        finally
            //        {
            //            rXfaWien.Final();
            //        }

            //        r = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
            //        r.Select("INT_COORD_STAT,WCM_NBR1,ID");
            //        r.SetWhere("ID", IMRecordset.Operation.Eq, idCoord);
            //        try
            //        {
            //            r.Open();
            //            if (!r.IsEOF())
            //            {
            //                r.Edit();
            //                r.Put("INT_COORD_STAT", status);
            //                r.Update();
            //            }
            //        }
            //        finally
            //        {
            //            r.Final();
            //        }

            //        string admins = "";
            //        for (int j = 0; j < wienAnsAdmList.Count; j++)
            //            admins += wienAnsAdmList[j] + ",";
            //        if (!string.IsNullOrEmpty(admins) && admins.Length > 0)
            //            admins = admins.Remove(admins.Length - 1);
            //        r = new IMRecordset(TableNameFreq, IMRecordset.Mode.ReadWrite);
            //        r.Select("ID,CUST_TXT5,CUST_TXT4");
            //        r.SetWhere("ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            //        try
            //        {
            //            r.Open();
            //            if (!r.IsEOF())
            //            {
            //                r.Edit();
            //                r.Put("CUST_TXT5", status);
            //                r.Put("CUST_TXT4", admins);
            //                r.Update();
            //            }
            //        }
            //        finally
            //        {
            //            r.Final();
            //        }
            //    }
            //}
            //string coordNeed = "-";
            //List<string> admAllList = new List<string>();
            //for (int i = 0; i < FreqStatList.Count; i++)
            //{
            //    IMRecordset r = new IMRecordset(TableNameFreq, IMRecordset.Mode.ReadOnly);
            //    r.Select("ID,CUST_TXT5,CUST_TXT4");
            //    r.SetWhere("ID", IMRecordset.Operation.Eq, FreqStatList[i].Id);
            //    try
            //    {
            //        r.Open();
            //        if (!r.IsEOF())
            //        {
            //            string tmpStat = r.GetS("CUST_TXT5");
            //            List<string> tmpAdm = r.GetS("CUST_TXT4").Split(',').ToList();
            //            admAllList.AddRange(tmpAdm);
            //            if (tmpStat == "CA" || tmpStat == "A")
            //                coordNeed = "+";
            //        }
            //    }
            //    finally
            //    {
            //        r.Final();
            //    }
            //}
            //admAllList = admAllList.Distinct().ToList();
            //string[] admStr = { "" };
            //admAllList.ForEach(obj => admStr[0] += obj + ",");
            //if (!string.IsNullOrEmpty(admStr[0]) && admStr[0].Length > 0)
            //    admStr[0] = admStr[0].Remove(admStr[0].Length - 1);
            //IMRecordset rS = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            //rS.Select("ID,COORD_NEEDED,COORD_LADMS");
            //rS.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            //try
            //{
            //    rS.Open();
            //    if (!rS.IsEOF())
            //    {
            //        rS.Put("COORD_NEEDED", coordNeed);
            //        rS.Put("COORD_LADMS", admStr[0]);
            //    }
            //}
            //finally
            //{
            //    rS.Final();
            //}
            #endregion

            //1Формування списку ID для станції таблиці WIEN_COORD_MOB
            List<int> xfaFreqList = new List<int>();
            List<int> tmpFreqList = new List<int>();
            List<int> freqList = new List<int>();
            tmpFreqList = FreqStatList.Select(obj => obj.Id).Distinct().ToList();
            string tmpStr = "";
            for (int i = 0; i < tmpFreqList.Count; i++)
            {
                tmpStr += "([MOBSTA_FREQ_ID]=" + tmpFreqList[i] + ")";
                if (i < tmpFreqList.Count - 1)
                    tmpStr += " OR ";
            }

            IMRecordset r = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
            r.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
            r.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
            r.SetAdditional(tmpStr);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    xfaFreqList.Add(r.GetI("ID"));
            }
            finally
            {
                r.Final();
            }

            if (xfaFreqList.Count == 0)
                return;

            //Визначення статусів для конкретної частоти
            string tblName = "";
            string tblNameFreq = "MOBSTA_FREQS";
            for (int i = 0; i < xfaFreqList.Count; i++)
            {
                List<string> statusList = new List<string>();
                List<string> admList = new List<string>();
                IMRecordset rAns = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadOnly);
                rAns.Select("ID,CO_LINK_ID,COORD_STAT_ANS,COUNTRY_DEST_ID");
                rAns.SetWhere("CO_LINK_ID", IMRecordset.Operation.Eq, xfaFreqList[i]);
                try
                {
                    for (rAns.Open(); !rAns.IsEOF(); rAns.MoveNext())
                    {
                        statusList.Add(rAns.GetS("COORD_STAT_ANS"));
                        admList.Add(rAns.GetS("COUNTRY_DEST_ID"));
                    }
                }
                finally
                {
                    rAns.Final();
                }
                //Формування статуса int_coord_stat
                string status = "CN";
                if (statusList.Contains("CN"))
                    status = "CN";
                if (statusList.Contains("C"))
                    status = "C";
                if (statusList.Contains("Z"))
                    status = "Z";
                if (statusList.Contains("CA"))
                    status = "CA";
                if (statusList.Contains("A"))
                    status = "A";
                admList = admList.Distinct().ToList();
                IMRecordset rWienCoord = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
                rWienCoord.Select("ID,TABLE_NAME,C3,INT_COORD_STAT");
                rWienCoord.SetWhere("ID", IMRecordset.Operation.Eq, xfaFreqList[i]);
                try
                {
                    rWienCoord.Open();
                    if (!rWienCoord.IsEOF())
                    {
                        rWienCoord.Edit();
                        rWienCoord.Put("INT_COORD_STAT", status);
                        rWienCoord.Update();
                    }
                }
                finally
                {
                    rWienCoord.Final();
                }
                //Визначити поточний ID для mobsta_freqX
                int freqId = IM.NullI;

                IMRecordset rXfa = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
                rXfa.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                rXfa.SetWhere("ID", IMRecordset.Operation.Eq, xfaFreqList[i]);
                rXfa.Open();
                try
                {
                    if (!rXfa.IsEOF())
                    {
                        tblName = rXfa.GetS("MOBSTA_FREQ_TN");
                        freqId = rXfa.GetI("MOBSTA_FREQ_ID");
                        freqList.Add(freqId);
                    }
                }
                finally
                {
                    rXfa.Final();
                }
                if (tblName.Contains("2") && i == 0)
                    tblNameFreq += "2";
                string administr = "";
                for (int j = 0; j < admList.Count; j++)
                {
                    administr += admList[j];
                    if (j < admList.Count - 1)
                        administr += ",";
                }
                IMRecordset rFreq = new IMRecordset(tblNameFreq, IMRecordset.Mode.ReadWrite);
                rFreq.Select("ID,CUST_TXT5,CUST_TXT4");
                rFreq.SetWhere("ID", IMRecordset.Operation.Eq, freqId);
                rFreq.Open();
                try
                {
                    if (!rFreq.IsEOF())
                    {
                        rFreq.Edit();
                        rFreq.Put("CUST_TXT5", status);
                        rFreq.Put("CUST_TXT4", administr);
                        rFreq.Update();
                    }
                }
                finally
                {
                    rFreq.Final();
                }
            }
            int stationID = IM.NullI;
            int coord_needed = 0;
            string coord_ladms = "";
            List<string> crdNeedList = new List<string>();
            List<string> crdLadmsList = new List<string>();
            //Збереження параметрів для станцій
            for (int i = 0; i < freqList.Count; i++)
            {
                IMRecordset rStat = new IMRecordset(tblNameFreq, IMRecordset.Mode.ReadOnly);
                rStat.Select("ID,STA_ID,CUST_TXT4,CUST_TXT5");
                rStat.SetWhere("ID", IMRecordset.Operation.Eq, freqList[i]);
                try
                {
                    rStat.Open();
                    if (!rStat.IsEOF())
                    {
                        crdLadmsList.AddRange(rStat.GetS("CUST_TXT4").Split(','));
                        crdNeedList.AddRange(rStat.GetS("CUST_TXT5").Split(','));
                        if (i >= freqList.Count - 1)
                            stationID = rStat.GetI("STA_ID");
                    }
                }
                finally
                {
                    rStat.Final();
                }
            }
            crdNeedList = crdNeedList.Distinct().ToList();
            crdLadmsList = crdLadmsList.Distinct().ToList();
            if (crdNeedList.Contains("CA") || crdNeedList.Contains("A"))
                coord_needed = 1;
            for (int i = 0; i < crdLadmsList.Count; i++)
            {
                coord_ladms += crdLadmsList[i];
                if (i < crdLadmsList.Count - 1)
                    coord_ladms += ",";
            }
            //Запис параметрів в mobstationX
            IMRecordset rStation = new IMRecordset(tblName, IMRecordset.Mode.ReadWrite);
            rStation.Select("ID,COORD_NEEDED,COORD_LADMS");
            rStation.SetWhere("ID", IMRecordset.Operation.Eq, stationID);
            try
            {
                rStation.Open();
                if (!rStation.IsEOF())
                {
                    rStation.Edit();
                    rStation.Put("COORD_LADMS", coord_ladms);
                    rStation.Put("COORD_NEEDED", coord_needed);
                    rStation.Update();
                }
            }
            finally
            {
                rStation.Final();
            }
        }

        /// <summary>
        /// Збереження запису для заяв MobStationX, якщо " "->CN
        /// </summary>
        private void MobEmptyCn(int i)
        {
            CoordForm.FreqId tmpFreq = FreqStatList.Find(obj => (obj.Freq == CoordTbl[i].Freq));
            IMRecordset r = new IMRecordset(TableNameFreq, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CUST_TXT5");
            r.SetWhere("ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("CUST_TXT5", "CN");
                    r.Update();
                }
            }
            finally
            {
                r.Final();
            }
        }

        /// <summary>
        /// Збереження запису для заяв MobStationX, якщо "A"->"CN"
        /// </summary>
        /// <param name="i"></param>
        private void MobAcn(int i)
        {
            int wienCoordId = 0;
            bool isLastAnsMob = false;
            CoordForm.FreqId tmpFreq = FreqStatList.Find(obj => (obj.Freq == CoordTbl[i].Freq));

            //Витягуємо номер позиції для ANSMOB           
            IMRecordset rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
            rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
            rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
            try
            {
                rXfaWien.Open();
                if (!rXfaWien.IsEOF())
                    wienCoordId = rXfaWien.GetI("ID");
            }
            finally
            {
                rXfaWien.Final();
            }

            //Видалити запис для даної адміністрації, частоти, станції
            IMRecordset r = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadWrite);
            r.Select("ID,COUNTRY_DEST_ID,CO_LINK_ID");
            r.SetWhere("COUNTRY_DEST_ID", IMRecordset.Operation.Eq, CoordTbl[i].Administration);
            r.SetWhere("CO_LINK_ID", IMRecordset.Operation.Eq, wienCoordId);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    r.Delete();
            }
            finally
            {
                r.Final();
            }
            //Визначити чи це не останній запис для даної станції і частоти
            r = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadWrite);
            r.Select("ID,COUNTRY_DEST_ID,CO_LINK_ID");
            r.SetWhere("CO_LINK_ID", IMRecordset.Operation.Eq, wienCoordId);
            try
            {
                r.Open();
                if (r.IsEOF())
                    isLastAnsMob = true;
            }
            finally
            {
                r.Final();
            }
            //якщо запис останній, то видалити WIEN_COORD_MOB
            if (isLastAnsMob)
            {
                int idCoord = IM.NullI;
                rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
                rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
                rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
                try
                {
                    rXfaWien.Open();
                    if (!rXfaWien.IsEOF())
                        idCoord = rXfaWien.GetI("ID");
                }
                finally
                {
                    rXfaWien.Final();
                }
                r = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
                r.Select("INT_COORD_STAT,WCM_NBR1,ID,WCM_TXT4");
                r.SetWhere("ID", IMRecordset.Operation.Eq, idCoord);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        r.Delete();
                }
                finally
                {
                    r.Final();
                }
            }
        }

        /// <summary>
        /// Збереження запису для заяв MobStationX, якщо " "->A
        /// </summary>
        /// <param name="i"></param>
        private void MobEmptyA(int i)
        {
            bool isCreateWienCoord = false;
            //Якщо необхідно, то додаємо запис в таблицю WIEN_COORD_MOB
            CoordForm.FreqId tmpFreq = FreqStatList.Find(obj => (obj.Freq == CoordTbl[i].Freq));

            IMRecordset rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
            rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
            rXfaWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
            rXfaWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
            try
            {
                rXfaWien.Open();
                if (rXfaWien.IsEOF())
                    isCreateWienCoord = true;
            }
            finally
            {
                rXfaWien.Final();
            }

            int wnId = IM.NullI;
            if (isCreateWienCoord)
            {
                wnId = HcmMobStation.AddMobStationToWien(tmpFreq.StaId, TableNameFreq, tmpFreq.Id);
                rXfaWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadWrite);
                rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                rXfaWien.SetWhere("ID", IMRecordset.Operation.Eq, wnId);
                try
                {
                    rXfaWien.Open();
                    if (!rXfaWien.IsEOF())
                    {
                        rXfaWien.Edit();
                        rXfaWien.Put("MOBSTA_FREQ_ID", tmpFreq.Id);
                        rXfaWien.Put("MOBSTA_FREQ_TN", TableName);
                        rXfaWien.Update();
                    }
                }
                finally
                {
                    rXfaWien.Final();
                }
                IMRecordset rd = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
                rd.Select("ID,C3,WCM_NBR1,WCM_TXT4");
                rd.SetWhere("ID", IMRecordset.Operation.Eq, wnId);
                try
                {
                    rd.Open();
                    if (!rd.IsEOF())
                    {
                        rd.Edit();
                        rd.Put("C3", "UKR");
                        rd.Update();
                    }
                }
                finally
                {
                    rd.Final();
                }
            }
            if (wnId == IM.NullI)
            {
                IMRecordset rWien = new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadOnly);
                rWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                rWien.SetWhere("MOBSTA_FREQ_ID", IMRecordset.Operation.Eq, tmpFreq.Id);
                rWien.SetWhere("MOBSTA_FREQ_TN", IMRecordset.Operation.Eq, TableName);
                try
                {
                    rWien.Open();
                    if (!rWien.IsEOF())
                        wnId = rWien.GetI("ID");
                }
                finally
                {
                    rWien.Final();
                }
            }
            //Формуємо запис WIEN_ANS_MOB
            IMRecordset r = new IMRecordset(ICSMTbl.WienAnsMob, IMRecordset.Mode.ReadWrite);
            r.Select("ID,COUNTRY_DEST_ID,CO_LINK_ID,COORD_STAT_ANS,DATE_CREATED,CREATED_BY");
            try
            {
                r.Open();
                r.AddNew();
                r.Put("ID", IM.AllocID(ICSMTbl.WienAnsMob, 1, -1));
                if (wnId != IM.NullI && r.GetI("CO_LINK_ID") != wnId)
                    r.Put("CO_LINK_ID", wnId);
                r.Put("COUNTRY_DEST_ID", CoordTbl[i].Administration);
                //r.Put("COORD_STAT_ANS", CoordTbl[i].Status);
                r.Put("DATE_CREATED", System.DateTime.Now);
                r.Put("CREATED_BY", IM.ConnectedUser());
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }

    }
}
