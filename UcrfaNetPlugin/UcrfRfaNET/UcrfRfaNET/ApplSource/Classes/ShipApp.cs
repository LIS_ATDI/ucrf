﻿using System;
using System.Text;
using System.Collections.Generic;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;

namespace XICSM.UcrfRfaNET.ApplSource
{    
    class ShipApp : BaseAppClass
    {
        public static readonly string TableName = ICSMTbl.itblShip;

        private ShipStation _shipStation = new ShipStation();
        
        #region CELL
        private CellStringProperty _cellType = new CellStringProperty();
        private CellIntegerProperty _cellObjectID = new CellIntegerProperty();
        private CellStringProperty _cellName = new CellStringProperty();
        private CellStringProperty _cellNameLatin = new CellStringProperty();
        private CellStringProperty _cellPort = new CellStringProperty();
        private CellStringProperty _cellPortLatin = new CellStringProperty();
        private CellStringProperty _cellCategory = new CellStringProperty();
        private CellStringProperty _cellNavigationArea = new CellStringProperty();

        private CellStringProperty _cellOwner = new CellStringProperty();
        private CellStringProperty _cellOwnerLatin = new CellStringProperty();
        private CellStringProperty _cellShipOwner = new CellStringProperty();
        private CellStringProperty _cellShipOwnerLatin = new CellStringProperty();
        private CellStringProperty _cellRegistrator = new CellStringProperty();
        private CellStringProperty _cellRegistratorLatin = new CellStringProperty();
        private CellStringProperty _cellCharter = new CellStringProperty();

        private CellStringProperty _cellRadioPhone = new CellStringProperty();
        private CellStringProperty _cellRadioPhoneLatin = new CellStringProperty();
        private CellStringProperty _cellRadioTelegraph = new CellStringProperty();
        private CellStringProperty _cellRadioTelegraphLatin = new CellStringProperty();

        private CellStringProperty _cellSelectiveCallsign = new CellStringProperty();
        private CellStringProperty _cellMaritimeMobId = new CellStringProperty();
        private CellStringProperty _cellInmarsat1 = new CellStringProperty();
        private CellStringProperty _cellInmarsat2 = new CellStringProperty();
        private CellStringProperty _cellKospasSarsat1 = new CellStringProperty();
        private CellStringProperty _cellKospasSarsat2 = new CellStringProperty();
        private CellStringProperty _cellKospasSarsat3 = new CellStringProperty();


        private CellStringProperty _cellEquipmentGroup = new CellStringProperty();
        private CellStringProperty _cellEquipmentName = new CellStringProperty();
        private CellDoubleNullableProperty _cellEquipmentNumber = new CellDoubleNullableProperty();
        private CellDoubleProperty _cellEquipmentPower = new CellDoubleProperty();
        private CellStringProperty _cellDesigEmission = new CellStringProperty();
        private CellDoubleProperty _cellEquipmentBw = new CellDoubleProperty();
        private CellStringProperty _cellBandwidth = new CellStringProperty();
        private CellStringProperty _cellCodeATIS = new CellStringProperty();

        private List<CellStringProperty> _listCellEquipmentGroup = new List<CellStringProperty>();
        private List<CellStringProperty> _listCellEquipmentName = new List<CellStringProperty>();
        private List<CellDoubleNullableProperty> _listCellEquipmentNumber = new List<CellDoubleNullableProperty>();
        private List<CellDoubleProperty> _listCellEquipmentPower = new List<CellDoubleProperty>();
        private List<CellStringProperty> _listCellDesigEmission = new List<CellStringProperty>();
        private List<CellDoubleProperty> _listCellEquipmentBw = new List<CellDoubleProperty>();

        #endregion
        
        public ShipApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, ShipApp.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppZRS;
            IsTechUser = true;
            IsTechnicalUser = 1;

            if (_ownerId != 0)
                objStation["OWNER_ID"] = _ownerId;
            //else
            //    OwnerID = objStation.GetI("OWNER_ID");

            _cellEquipmentBw.FormatString = "N4";

            _shipStation.Id = objStation.GetI("ID");            
            _shipStation.Load();

            _listCellEquipmentGroup.Add(_cellEquipmentGroup);
            _listCellEquipmentName.Add(_cellEquipmentName);
            _listCellEquipmentNumber.Add(_cellEquipmentNumber);
            _listCellEquipmentPower.Add(_cellEquipmentPower);
            _listCellDesigEmission.Add(_cellDesigEmission);
            _listCellEquipmentBw.Add(_cellEquipmentBw);
        }

        #region Overrides of BaseAppClass
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {                                    
        }
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.GetListDocNumberForDozvWma(1)[0];
            
            if (docType == DocType.DOZV)
            {
                fullPath += "МС-" + CUsers.GetUserAreaCode() + "-" + number + ".rtf";
            }
            else if (docType == DocType.LYST_ZRS_CALLSIGN)
            {
                fullPath += "МС-" + CUsers.GetUserAreaCode() + "-" + number + ".rtf";
            }
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        /// <summary>
        /// Возвращает дополнительный фильтр шаблона
        /// </summary>
        /// <returns></returns>
        protected override string getDopDilter()
        {
            if (_shipStation.StationType == "RS")
                return "SHIP_RS";
            else if (_shipStation.StationType == "NS")
                return "SHIP_NS";
            return "";
        }
        
        public override void InitParamGrid(Cell cell)
        {
            throw new NotImplementedException();            
        }

        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            StringBuilder errorDesctption = new StringBuilder();

            _shipStation.Id = _cellObjectID.IntValue;

            _shipStation.StationType = _cellType.Value;

            if (string.IsNullOrEmpty(_shipStation.StationType))
                errorDesctption.AppendLine("Тип станції не вибрано");

            _shipStation.Name = _cellName.Value;

            if (string.IsNullOrEmpty(_shipStation.Name))
                errorDesctption.AppendLine("Назва корабля не введена");

            if (_shipStation.Owner.ziID==IM.NullI)
                errorDesctption.AppendLine("Назва власника не введена");

            if (_shipStation.ShipOwner.ziID==IM.NullI)
                errorDesctption.AppendLine("Назва судновласника не введена");
            
            if (_shipStation.RegisterDepartment.ziID==IM.NullI)
                errorDesctption.AppendLine("Назва органу державної реєстраціх не введена");                
            
            _shipStation.NameLatin = _cellNameLatin.Value;

            if (string.IsNullOrEmpty(_shipStation.Port))
                errorDesctption.AppendLine("Назва порту приписки не введена");

            _shipStation.Category = _cellCategory.Value;

            _shipStation.RadioPhone = _cellRadioPhone.Value;
            _shipStation.RadioPhoneLatin = _cellRadioPhoneLatin.Value;

            _shipStation.RadioTelegraph = _cellRadioTelegraph.Value;
            _shipStation.RadioTelegraphLatin = _cellRadioTelegraphLatin.Value;

            _shipStation.SelectiveCallsign = _cellSelectiveCallsign.Value;
            _shipStation.MaritimeMobId =  _cellMaritimeMobId.Value;
            _shipStation.CodeATIS = _cellCodeATIS.Value;
            _shipStation.Inmarsat1 = _cellInmarsat1.Value;
            _shipStation.Inmarsat2 = _cellInmarsat2.Value;
            _shipStation.KospasSarsat1 =  _cellKospasSarsat1.Value;
            _shipStation.KospasSarsat2 =  _cellKospasSarsat2.Value;
            _shipStation.KospasSarsat3 =  _cellKospasSarsat3.Value;
            _shipStation.CodeATIS = _cellCodeATIS.Value;

            double bwTotal = 0.0;
            foreach (ShipEquipment eq in _shipStation.Equipment)
                bwTotal += eq.Bandwidth;
            _shipStation.Bandwidth = bwTotal;

            //_cellEquipmentName.Cell.Tag = _shipStation.Equipment[0];

            bool bNotEntered = false;
            for (int i = 0; i < _shipStation.Equipment.Count; i++)
                if (_shipStation.Equipment[i].PmrId == IM.NullI)
                    bNotEntered = true;

            if (_shipStation.Equipment.Count == 0 || bNotEntered)
                errorDesctption.AppendLine("Корабельне обладнання не введено");

            if (errorDesctption.ToString().Length == 0)
            {
                int i = 0;
                foreach (ShipEquipment equipment in _shipStation.Equipment)
                {
                    equipment.Quantity = _listCellEquipmentNumber[i].DoubleValue;
                    equipment.Power[PowerUnits.kW] = _listCellEquipmentPower[i].DoubleValue;

                    i++;
                }

                _shipStation.Save();

                IMRecordset r = new IMRecordset(ICSMTbl.itblShip, IMRecordset.Mode.ReadWrite);
                r.Select("ID,STATUS,OWNER_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _shipStation.Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("STATUS", Status.StatusAsString);
                    r.Put("OWNER_ID", OwnerID);
                    r.Update();
                }
                r.Close();
                r.Destroy();
                return true;
            } else
            {
                MessageBox.Show(errorDesctption.ToString(),"Помилка",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
            }

            
        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.ShipApp;
        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            InitStationObject();            
        }

        public override void RemoveSector(Grid grid)
        {
            if (_shipStation.Equipment.Count > 1)
            {
                RemoveShipEquipment frm = new RemoveShipEquipment();
                for(int i=1;i<=_shipStation.Equipment.Count;i++)
                {
                    frm.cbEquipment.Items.Add("Обладнання " + i);                    
                }
                frm.cbEquipment.SelectedIndex = -1;

                if (frm.ShowDialog()==DialogResult.OK)
                {
                    if (frm.cbEquipment.SelectedIndex >= 0)
                    {
                        int row1 = gridParam.rowList.IndexOf(_cellEquipmentName.Cell.row);                     
                        grid.rowList.RemoveAt(row1 + frm.cbEquipment.SelectedIndex);

                        //_shipStation.Equipment[frm.cbEquipment.SelectedIndex].Remove();
                        _shipStation.Equipment.RemoveAt(frm.cbEquipment.SelectedIndex);

                        grid.Edited = true;
                        grid.Invalidate();
                    }
                }
            }
        }

        private Cell ExtendGrid()
        {
            Cell returnCell = null;
            Grid grid = gridParam;

            Row row = new Row(grid, grid.rowList[0].Size);
            row.grid = grid;
            grid.rowList.Insert(grid.rowList.Count - 2, row);

            for (int i = 0; i <= 6; i++)
            {
                Cell cell = null;
                Cell pivotCell = grid.GetCell(1, 1);

                Cell titleCell = grid.GetCellFromKey("mkEquipId1");

                switch (i)
                {
                    case 0:
                        cell = row.AddCell(titleCell.Value, EditStyle.esSimple);
                        cell.HorizontalAlignment = titleCell.HorizontalAlignment;
                        cell.Value2 = "";
                        cell.CanEdit = false;
                        break;
                    case 1:
                        cell = row.AddCell("", EditStyle.esSimple);
                        cell.HorizontalAlignment = HorizontalAlignment.Left;
                        cell.CanEdit = false;
                        break;
                    case 2:
                        cell = row.AddCell("", EditStyle.esEllipsis);
                        cell.HorizontalAlignment = HorizontalAlignment.Left;
                        cell.CanEdit = true;
                        returnCell = cell;
                        break;
                    default:
                        cell = row.AddCell("", EditStyle.esSimple);
                        cell.HorizontalAlignment = HorizontalAlignment.Left;
                        cell.CanEdit = true;
                        break;
                }

                cell.FontName = pivotCell.FontName;
                cell.FontStyle = pivotCell.FontStyle;
                cell.TextHeight = pivotCell.TextHeight;                
            }

            CellStringProperty newCellEquipmentGroup = new CellStringProperty();
            CellStringProperty newCellEquipmentName = new CellStringProperty();
            CellDoubleNullableProperty newCellEquipmentNumber = new CellDoubleNullableProperty();
            CellDoubleProperty newCellEquipmentPower = new CellDoubleProperty();
            CellStringProperty newCellDesigEmission = new CellStringProperty();
            CellDoubleProperty newCellEquipmentBw = new CellDoubleProperty();
            newCellEquipmentBw.FormatString = "N4";

            row.cellList[1].Bind("Value", newCellEquipmentGroup, "Value");
            row.cellList[2].Bind("Value", newCellEquipmentName, "Value");
            row.cellList[3].Bind("Value", newCellEquipmentNumber, "Value");
            row.cellList[4].Bind("Value", newCellEquipmentPower, "Value");
            row.cellList[5].Bind("Value", newCellDesigEmission, "Value");
            row.cellList[6].Bind("Value", newCellEquipmentBw, "Value");
            
            row.cellList[2].AddOnPressButtonHandler(this, "OnPressEquipment");

            int equipmentCount = _shipStation.Equipment.Count;
            row.cellList[2].Tag = _shipStation.Equipment[equipmentCount-1];

            newCellEquipmentGroup.Cell = row.cellList[1];
            newCellEquipmentName.Cell = row.cellList[2];
            newCellEquipmentNumber.Cell = row.cellList[3];
            newCellEquipmentPower.Cell = row.cellList[4];
            newCellDesigEmission.Cell = row.cellList[5];
            newCellEquipmentBw.Cell = row.cellList[6];
            row.cellList[1].CanEdit = false;
            row.cellList[5].CanEdit = false;
            row.cellList[6].CanEdit = false;


            _listCellEquipmentGroup.Add(newCellEquipmentGroup);
            _listCellEquipmentName.Add(newCellEquipmentName);
            _listCellEquipmentNumber.Add(newCellEquipmentNumber);
            _listCellEquipmentPower.Add(newCellEquipmentPower);
            _listCellDesigEmission.Add(newCellDesigEmission);
            _listCellEquipmentBw.Add(newCellEquipmentBw);

            return returnCell;
        }


        public override void AddSector(Grid grid)
        {
            _shipStation.AddEquipment();            
            OnPressEquipment(ExtendGrid());
        }

        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if (cell.Key == "KObjID")
                {
                    RecordPtr ptr = new RecordPtr(ICSMTbl.itblShip, _shipStation.Id);
                    ptr.UserEdit();
                }
                if (cell.Key == "KOwnerUkr")
                {
                    FAbonentOwner fAOwn=new FAbonentOwner(_shipStation.Owner.ziID);
                    fAOwn.ShowDialog();                    
                }
                if (cell.Key == "KShipOwnerUkr")
                {
                    FAbonentOwner fAOwn = new FAbonentOwner(_shipStation.ShipOwner.ziID);
                    fAOwn.ShowDialog();                    
                }
                if (cell.Tag!=null)
                {
                    ShipEquipment _equipment = cell.Tag as ShipEquipment;
                    RecordPtr ptr = new RecordPtr(ICSMTbl.itblEquipPmr, _equipment.PmrId);
                    ptr.UserEdit();
                }                            
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        #endregion 

        private void InitStationObject()
        {
            _cellObjectID.IntValue = _shipStation.Id;
            _cellName.Value = _shipStation.Name;
            _cellNameLatin.Value = _shipStation.NameLatin;

            _cellPort.Value = _shipStation.Port;
            _cellPortLatin.Value = _shipStation.PortLatin;

            _cellOwner.Value = _shipStation.Owner.zsNAME;
            _cellOwnerLatin.Value = _shipStation.Owner.zsNAME_LATIN;
            _cellShipOwner.Value = _shipStation.ShipOwner.zsNAME;
            _cellShipOwnerLatin.Value = _shipStation.ShipOwner.zsNAME_LATIN;            
            _cellRegistrator.Value = _shipStation.RegisterDepartment.zsNAME;
            _cellRegistratorLatin.Value = _shipStation.RegisterDepartment.zsNAME_LATIN;
            _cellCharter.Value = _shipStation.Charter.zsNAME;
            
            _cellNavigationArea.Value = _shipStation.NavigationArea.ToString();

            _cellRadioPhone.Value = _shipStation.RadioPhone;
            _cellRadioPhoneLatin.Value = _shipStation.RadioPhoneLatin;

            _cellRadioTelegraph.Value = _shipStation.RadioTelegraph;
            _cellRadioTelegraphLatin.Value = _shipStation.RadioTelegraphLatin;

            _cellSelectiveCallsign.Value = _shipStation.SelectiveCallsign;
            _cellMaritimeMobId.Value = _shipStation.MaritimeMobId;
            _cellInmarsat1.Value = _shipStation.Inmarsat1;
            _cellInmarsat2.Value = _shipStation.Inmarsat2;
            _cellKospasSarsat1.Value = _shipStation.KospasSarsat1;
            _cellKospasSarsat2.Value = _shipStation.KospasSarsat2;
            _cellKospasSarsat3.Value = _shipStation.KospasSarsat3;
            _cellCodeATIS.Value = _shipStation.CodeATIS;

            _cellType.Cell.KeyPickList = new KeyedPickList();
            Dictionary<string, string> _shipGenClass = ComboFiles.GetEriCodeAndDescr("ShipType");
            foreach (KeyValuePair<string, string> pair in _shipGenClass)
            {
                _cellType.Cell.KeyPickList.AddToPickList(pair.Key, pair.Value);
            }
            _cellType.Value = _shipStation.StationType;            

            int i = 0;
            foreach(ShipEquipment equipment in _shipStation.Equipment)
            {
                if (i>0)
                    ExtendGrid();

                _listCellEquipmentGroup[i].Value = equipment.eqTypes;
                _listCellEquipmentName[i].Value = equipment.Name;
                _listCellEquipmentNumber[i].DoubleValue = equipment.Quantity;
                _listCellEquipmentPower[i].DoubleValue = equipment.Power[PowerUnits.kW];
                _listCellDesigEmission[i].Value = equipment.DesigEmission;
                _listCellEquipmentName[i].Cell.Tag = equipment;
                _listCellEquipmentBw[i].DoubleValue = equipment.Bandwidth;

                i++;
            }

            _cellBandwidth.Value = _shipStation.BwText;
            _cellCategory.Value = _shipStation.Category;
        }

        public void OnPressButtonCategory(Cell cell)
        {
            FCorrespondencyCategory categoryForm = new FCorrespondencyCategory();
            categoryForm.CategoriesAsString = _cellCategory.Value;
            if (categoryForm.ShowDialog()==DialogResult.OK)            
            {
                _cellCategory.Value = categoryForm.CategoriesAsString;                
            }                                      
        }

        public void OnPressButtonNavigationArea(Cell cell)
        {
            FNavigationArea navigationAreaForm = new FNavigationArea();
            navigationAreaForm.A1 = _shipStation.NavigationArea.A1;
            navigationAreaForm.A2 = _shipStation.NavigationArea.A2;
            navigationAreaForm.A3 = _shipStation.NavigationArea.A3;
            navigationAreaForm.DomescticWaterWays = _shipStation.NavigationArea.DomesticWaterWays;
            navigationAreaForm.NonConvention = _shipStation.NavigationArea.NonConvention;

            if (navigationAreaForm.ShowDialog()==DialogResult.OK)
            {
                _shipStation.NavigationArea.A1 = navigationAreaForm.A1;
                _shipStation.NavigationArea.A2 = navigationAreaForm.A2;
                _shipStation.NavigationArea.A3 = navigationAreaForm.A3;
                _shipStation.NavigationArea.DomesticWaterWays = navigationAreaForm.DomescticWaterWays;
                _shipStation.NavigationArea.NonConvention = navigationAreaForm.NonConvention;

                _cellNavigationArea.Value = _shipStation.NavigationArea.ToString();
            }                        
        }

        /// <summary>
        /// Выбор нового владельца
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonOwner(Cell cell)
        {
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(_cellOwner.Value) + "*\"}";
           
            //RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
            //RecordPtr recOwner = SelectEquip(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, "ID", _shipStation.Owner.ziID.ToString(), true);
            RecordPtr recOwner = SelectOwner(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, "ReferenceTable.NAME", HelpFunction.ReplaceQuotaSumbols(_cellOwner.Value), true);
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {                
                _shipStation.Owner.Read(recOwner.Id);
                _cellOwner.Value = _shipStation.Owner.zsNAME;
                _cellOwnerLatin.Value = _shipStation.Owner.zsNAME_LATIN;
            }
        }

        public void OnPressButtonShipOwner(Cell cell)
        {
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(_cellShipOwner.Value) + "*\"}";
            //RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
           // RecordPtr recOwner = SelectEquip(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, "ID", _shipStation.ShipOwner.ziID.ToString(), true);
            RecordPtr recOwner = SelectOwner(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, "ReferenceTable.NAME", HelpFunction.ReplaceQuotaSumbols(_cellShipOwner.Value), true);
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {
                _shipStation.ShipOwner.Read(recOwner.Id);
                _cellShipOwner.Value = _shipStation.ShipOwner.zsNAME;
                _cellShipOwnerLatin.Value = _shipStation.ShipOwner.zsNAME_LATIN;
            }
        }

        public void OnPressButtonRegistryPort(Cell cell)
        {
            string criteria = HelpFunction.ReplaceQuotaSumbols(cell.Value);
            string param = string.Format("([{0}] LIKE '%{1}%')", "NAME_UKR", criteria);
            RecordPtr retRec = SelectRecord(CLocaliz.TxT("Select port of registry"), PlugTbl.RegistryPort, param, "NAME_UKR", OrderDirection.Ascending);
            if ((retRec.Id > 0) && (retRec.Id < IM.NullI))
            {
                _shipStation.RegistryPort.Load(retRec.Id);
                _cellPort.Value = _shipStation.Port;
                _cellPortLatin.Value = _shipStation.PortLatin;
            }
        }

        public void OnPressButtonRegistrator(Cell cell)
        {
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(_cellRegistrator.Value) + "*\"}";
            //RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
            //RecordPtr recOwner = SelectEquip(CLocaliz.TxT("Seach of the Registrator"), PlugTbl.XvAbonentOwner, "ID", _shipStation.RegisterDepartment.ziID.ToString(), true);
            RecordPtr recOwner = SelectOwner(CLocaliz.TxT("Seach of the Registrator"), PlugTbl.XvAbonentOwner, "ReferenceTable.NAME", HelpFunction.ReplaceQuotaSumbols(_cellRegistrator.Value), true);
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {
                _shipStation.RegisterDepartment.Read(recOwner.Id);
                _cellRegistrator.Value = _shipStation.RegisterDepartment.zsNAME;
                _cellRegistratorLatin.Value = _shipStation.RegisterDepartment.zsNAME_LATIN;
            }
        }

        public void OnPressButtonCharter(Cell cell)
        {
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(_cellCharter.Value) + "*\"}";
            //RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
            //RecordPtr recOwner = SelectEquip(CLocaliz.TxT("Seach of the Charter"), PlugTbl.XvAbonentOwner, "ID", _shipStation.Charter.ziID.ToString(), true);
            RecordPtr recOwner = SelectOwner(CLocaliz.TxT("Seach of the Charter"), PlugTbl.XvAbonentOwner, "ReferenceTable.NAME", HelpFunction.ReplaceQuotaSumbols(_cellCharter.Value), true);
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {
                _shipStation.Charter.Read(recOwner.Id);
                _cellCharter.Value = _shipStation.Charter.zsNAME;                
            }
        }

        public void OnAfterChangeName(Cell cell)
        {
            if (string.IsNullOrEmpty(_cellRadioPhone.Value))
                _cellRadioPhone.Value = cell.Value;
            
        }

        public void OnAfterChangeNameLatin(Cell cell)
        {
            if (string.IsNullOrEmpty(_cellRadioPhoneLatin.Value))
                _cellRadioPhoneLatin.Value = cell.Value;
        }

        public void OnPressEquipment(Cell cell)
        {
            RecordPtr recEquip = SelectEquip("Підбір обладнання", ICSMTbl.itblEquipPmr, "NAME", cell.Value, true);
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                ShipEquipment equipment = cell.Tag as ShipEquipment;
                if (equipment == null)
                {
                    _shipStation.AddEquipment();
                    equipment = _shipStation.Equipment[_shipStation.Equipment.Count-1];
                    cell.Tag = equipment;
                }

                equipment.Power[PowerUnits.dBW] = IM.NullD;                
                equipment.LoadEquipmentPmr(recEquip.Id);

                if (equipment.Quantity == IM.NullD)
                {
                    equipment.Quantity = 1.0;                    
                }

                _shipStation.CalculateBandwidth();

                int row1 = gridParam.rowList.IndexOf(_cellEquipmentName.Cell.row);
                int row2 = gridParam.rowList.IndexOf(cell.row);

                int index = row2 - row1;

                _listCellEquipmentGroup[index].Value = equipment.eqTypes;
                _listCellEquipmentName[index].Value = equipment.Name;
                _listCellEquipmentNumber[index].DoubleValue = equipment.Quantity;
                _listCellEquipmentPower[index].DoubleValue = equipment.Power[PowerUnits.kW];
                _listCellDesigEmission[index].Value = equipment.DesigEmission;
                _listCellEquipmentBw[index].DoubleValue = equipment.Bandwidth;

                _cellBandwidth.Value = _shipStation.BwText;
            } 
        }

        public override string GetProvince()
        {
            return "";
        }
    }
}
