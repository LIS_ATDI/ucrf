﻿using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public class EmiterBand
    {
        public int Id { get; set; }
        public string TypeVP { get; set; }
        public string Band { get; set; }
        public double BandMin { get; set; }
        public double BandMax { get; set; }
        public double FREQ { get; set; }
        public double FREQ_P { get; set; }
        public double FREQ_M { get; set; }
        public static bool IsPower { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public EmiterBand()
        {
            BandMin = 0.0;
            BandMax = 0.0;
            FREQ = IM.NullD;
            FREQ_P = IM.NullD;
            FREQ_M = IM.NullD;
        }

        /// <summary>
        /// Сохранение линков
        /// </summary>
        /// <param name="Lst_bands"></param>
        /// <param name="emi_id"></param>
        public void Save(List<EmiterBand> Lst_bands, int emi_id)
        {
            Remove(emi_id);
            foreach (EmiterBand it in Lst_bands)
            {
                IMRecordset rs_emi_to_band = new IMRecordset("XFA_EMI_TO_BAND", IMRecordset.Mode.ReadWrite);
                rs_emi_to_band.Select("EMI_ID,BAND_ID");
                rs_emi_to_band.SetWhere("EMI_ID", IMRecordset.Operation.Eq, emi_id);
                rs_emi_to_band.SetWhere("BAND_ID", IMRecordset.Operation.Eq, it.Id);
                rs_emi_to_band.Open();
                if (rs_emi_to_band.IsEOF())
                {
                    rs_emi_to_band.AddNew();
                    rs_emi_to_band.Put("EMI_ID", emi_id);
                    rs_emi_to_band.Put("BAND_ID", it.Id);
                    rs_emi_to_band.Update();
                }
                rs_emi_to_band.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="band_id"></param>
        /// <param name="emi_id"></param>
        public void Remove(int emi_id)
        {
                IMRecordset rs_emi_to_band = new IMRecordset("XFA_EMI_TO_BAND", IMRecordset.Mode.ReadWrite);
                rs_emi_to_band.Select("EMI_ID,BAND_ID");
                rs_emi_to_band.SetWhere("EMI_ID", IMRecordset.Operation.Eq, emi_id);
                for (rs_emi_to_band.Open(); !rs_emi_to_band.IsEOF();rs_emi_to_band.MoveNext())
                {
                    rs_emi_to_band.Delete();
                }
                rs_emi_to_band.Close();
        }
    
        /// <summary>
        /// Сохранение частот
        /// </summary>
        public void Save(int band_id, int emi_id)
        {
            IMRecordset rs_emi_to_band = new IMRecordset("XFA_EMI_TO_BAND", IMRecordset.Mode.ReadWrite);
            rs_emi_to_band.Select("EMI_ID,BAND_ID");
            rs_emi_to_band.SetWhere("EMI_ID", IMRecordset.Operation.Eq, emi_id);
            rs_emi_to_band.SetWhere("BAND_ID", IMRecordset.Operation.Eq, band_id);
            rs_emi_to_band.Open();
            if (rs_emi_to_band.IsEOF())
            {
                rs_emi_to_band.AddNew();
                rs_emi_to_band.Put("EMI_ID", emi_id);
                rs_emi_to_band.Put("BAND_ID", band_id);
                rs_emi_to_band.Update();
            }
            rs_emi_to_band.Close();
        }
        /// <summary>
        /// Получить все бэнды по типу
        /// </summary>
        /// <param name="emi_id"></param>
        /// <returns></returns>
        public List<EmiterBand> Load(string emi_type)
        {
            List<EmiterBand> Lst_bands = new List<EmiterBand>();
                    IMRecordset rs = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                    rs.SetWhere("EMI_TYPE", IMRecordset.Operation.Eq, emi_type);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        EmiterBand band = new EmiterBand();
                        band.Id = rs.GetI("ID");
                        band.BandMin = rs.GetD("BANDMIN");
                        band.BandMax = rs.GetD("BANDMAX");
                        band.FREQ = rs.GetD("FREQ");
                        band.FREQ_M = rs.GetD("OFFSET_MINUS");
                        band.FREQ_P = rs.GetD("OFFSET_PLUS");
                        band.TypeVP = rs.GetS("EMI_TYPE");
                        band.Band = BandMin.ToString() + " - " + BandMax.ToString() + (FREQ_M == FREQ_P ? " ±" + FREQ_M.ToString() : (" +" + FREQ_P.ToString() + " -" + FREQ_M.ToString()));
                        Lst_bands.Add(band);
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
            
            return Lst_bands;
        }

        /// <summary>
        /// Все линки прикрепленные к данному emission
        /// </summary>
        public List<EmiterBand> Load(int emi_id)
        {
            List<EmiterBand> Lst_bands = new List<EmiterBand>();
            IMRecordset rs_emi_to_band = new IMRecordset("XFA_EMI_TO_BAND", IMRecordset.Mode.ReadWrite);
            rs_emi_to_band.Select("EMI_ID,BAND_ID");
            rs_emi_to_band.SetWhere("EMI_ID", IMRecordset.Operation.Eq, emi_id);
            for (rs_emi_to_band.Open(); !rs_emi_to_band.IsEOF(); rs_emi_to_band.MoveNext())
            {

                IMRecordset rs_allstations = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID");
                rs_allstations.SetWhere("ID", IMRecordset.Operation.Eq, rs_emi_to_band.GetI("BAND_ID"));
                rs_allstations.Open();
                if (!rs_allstations.IsEOF())
                {
                    IMRecordset rs = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID"));
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        EmiterBand band = new EmiterBand();
                        band.Id = rs.GetI("ID");
                        band.BandMin = rs.GetD("BANDMIN");
                        band.BandMax = rs.GetD("BANDMAX");
                        band.FREQ = rs.GetD("FREQ");
                        band.FREQ_M = rs.GetD("OFFSET_MINUS");
                        band.FREQ_P = rs.GetD("OFFSET_PLUS");
                        band.TypeVP = rs.GetS("EMI_TYPE");
                        band.Band = band.BandMin.ToStringNullD() + " - " + band.BandMax.ToStringNullD();
                        Lst_bands.Add(band);
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }

            }
            rs_emi_to_band.Close();
            return Lst_bands;
        }
       
        public static string GetAllBand(List<EmiterBand> all_band)
        {
            string Out = "";
            foreach(EmiterBand item in all_band)
            {
                Out += item.Band+";";
            }
            return Out;
        }

        public static string GetAllBandExt(List<EmiterBand> all_band)
        {
            string Out = "";
            foreach (EmiterBand item in all_band)
            {
                if (all_band.Count > 0)
                    Out += item.BandMin.ToStringNullD() + " - " + item.BandMax.ToStringNullD() + "; ";
                else
                    Out += item.BandMin.ToStringNullD() + " - " + item.BandMax.ToStringNullD();
            }
            return Out;
        }

        public static string GetAllCentralFreq(List<EmiterBand> all_band)
        {
            string Out = "";
            foreach (EmiterBand item in all_band)
            {
                if (all_band.Count>0)
                    Out += item.FREQ.ToStringNullD() + "" + (item.FREQ_M == item.FREQ_P ? " ±" + item.FREQ_M.ToStringNullD() : (" +" + item.FREQ_P.ToStringNullD() + " -" + item.FREQ_M.ToStringNullD())) + "; ";
                else
                    Out += item.FREQ.ToStringNullD() + "" + (item.FREQ_M == item.FREQ_P ? " ±" + item.FREQ_M.ToStringNullD() : (" +" + item.FREQ_P.ToStringNullD() + " -" + item.FREQ_M.ToStringNullD()));
            }
            return Out;
        }

        public override string ToString()
        {
            return Band = BandMin.ToStringNullD() + " - " + BandMax.ToStringNullD() + ";" + (FREQ_M == FREQ_P ? " ±" + FREQ_M.ToStringNullD() : (" +" + FREQ_P.ToStringNullD() + " -" + FREQ_M.ToStringNullD())); 
        }
       
    }
}
