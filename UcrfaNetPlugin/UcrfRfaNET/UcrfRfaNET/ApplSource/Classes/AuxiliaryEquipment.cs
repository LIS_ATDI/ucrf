﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{    
    class AuxiliaryEquipment : Equipment
    {
        public string EquipmentType { get; set; }
        //public string Serial { get; set; }
        public string Note { get; set; }
        public string Province { get; set; }

        public AuxiliaryEquipment()
        {
            TableName = PlugTbl.AuxiliaryEquipment;
            Id = IM.NullI;
            Name = "";
            Province = "";

            EquipmentType = "";
            //Serial = "";
            Note = "";
        }

        public override void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,NAME,DEV_TYPE,NOTES,PROVINCE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    Name = r.GetS("NAME");
                    EquipmentType = r.GetS("DEV_TYPE");
                    //Serial = r.GetS("SERIAL");
                    Note = r.GetS("NOTES");
                    Province = r.GetS("PROVINCE");
                }
            }
            finally
            {
                if (r.IsEOF())
                    r.Close();
                r.Destroy();
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,NAME,DEV_TYPE,NOTES,PROVINCE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();
                }
                else
                {
                    Id = IM.AllocID(TableName, 1, -1);
                    r.AddNew();
                    r.Put("ID", Id);
                }

                r.Put("NAME", Name);
                r.Put("DEV_TYPE", EquipmentType);
                // r.Put("SERIAL", Serial);
                r.Put("NOTES", Note);
                r.Put("PROVINCE", Province);
                r.Update();
            }
            finally
            {
                if (r.IsEOF())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
