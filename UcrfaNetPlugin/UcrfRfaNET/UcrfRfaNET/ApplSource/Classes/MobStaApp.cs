﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using LisUtility;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRR;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    /// <summary>
    /// класс станции PP
    /// </summary>
    internal class StationRR : BaseStation
    {
        //===================================================		
        public string OznRez { get; set; }
        public double Height { get; set; }
        public string EmiClass { get; set; }
        public string Sertificate { get; set; }
        public DateTime SertDate { get; set; }
        public int TrCount { get; set; }
        public int ID { get; set; }
        //public int FilterType { get; set; }
        //public string FilterName { get; set; }
        public MobStaApp.FilterTypeInfo fti;
        public double MaxPower { get; set; }
        public double AntHeight { get; set; }
        public double Azimuth { get; set; }
        public double AntCoef { get; set; }
        public double AntDSWidth { get; set; }
        public double AntAngle { get; set; }
        public double FiderZat { get; set; }
        public double Count { get; set; }
        public List<int> Channels { get; set; }
        public List<double> TxFreqs { get; set; }
        public List<double> RxFreqs { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public string strChannel = "";
        public string strFreqTX = "";
        public string strFreqRX = "";

        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public StationRR()
        {// Инициализируем все перемнные
            OznRez = "";
            Height = IM.NullD;
            EmiClass = "";
            Sertificate = "";
            TrCount = IM.NullI;
            //FilterType = IM.NullI;
            fti.FilterIndex = IM.NullI;
            fti.FilterName = "";
            MaxPower = IM.NullD;
            AntHeight = IM.NullD;
            //FilterName = "";
            Azimuth = IM.NullD;
            AntCoef = IM.NullD;
            AntDSWidth = IM.NullD;
            AntAngle = IM.NullD;
            FiderZat = 3;
            Channels = new List<int>();
            TxFreqs = new List<double>();
            RxFreqs = new List<double>();
            Lon = IM.NullD;
            Lat = IM.NullD;
            ID = IM.NullI;
            Count = IM.NullD;
        }
        //========
        protected override void DisposeExec()
        {
            if (objEquip != null)
            {
                objEquip.Dispose();
                objEquip = null;
            }
            if (objAntenna != null)
            {
                objAntenna.Dispose();
                objAntenna = null;
            }
            base.DisposeExec();
        }
    }
    //===============================================================
    //===============================================================
    /// <summary>
    /// Класс для обработки заявки PP
    /// </summary>
    internal class MobStaApp : BaseAppClass
    {
        public static readonly string TableName = ICSMTbl.itblMobStation;
        //===================================================	
        protected List<StationRR> stationList = new List<StationRR>(); // Дополнительные данные станции
        protected List<IMObject> objStations = new List<IMObject>();  //Сектора
        protected List<RecordPtr> sectorIDs = new List<RecordPtr>();
        public List<List<int>> msvSectors = new List<List<int>>(); //массив секторов
        private string standard = "";   //Стандарт
        private const string strCount = "згідно комплектації базової станції (або згідно заяви)";

        public struct FilterTypeInfo
        {
            public string FilterName;
            public int FilterIndex;
        };

        public MobStaApp(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, MobStaApp.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppRR;
            department = DepartmentType.VRR;
            departmentSector = DepartmentSectorType.VRR_SSZ;
            int msID = objStation.GetI("ID");
            sectorIDs.Add(new RecordPtr(MobStaApp.TableName, msID));
            StationRR st = new StationRR();
            stationList.Add(st);
            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT16");
            SCVisnovok = objStation.GetS("CUST_TXT17");
            SCNote = objStation.GetS("CUST_TXT18");

            objStations.Add(objStation);
            if (newStation)
            {
                objStation["ADM"] = "UKR";
                objStation["CLASS"] = "FB";
                objStation["TX_ADDLOSSES"] = 0;
                objStation["STANDARD"] = radioTech;
                objStation.Put("ELEVATION", -1);
            }
            else
            {
                radioTech = objStation.GetS("STANDARD");
                IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
                r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, MobStaApp.TableName);
                r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        for (int i = 2; (i <= 6) && (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI && r.GetI("OBJ_ID" + i.ToString()) > 0); ++i)
                        {
                            RecordPtr sector = new RecordPtr(MobStaApp.TableName, r.GetI("OBJ_ID" + i.ToString()));
                            try
                            {
                                IMObject objSector = IMObject.LoadFromDB(sector);  //Dispose не делаем так как это реализовано в деструкторе
                                objStations.Add(objSector);
                                sectorIDs.Add(sector);
                                StationRR temp = new StationRR();
                                stationList.Add(temp);
                            }
                            catch
                            {
                                CLogs.WriteError(ELogsWhat.Critical, "Не вдалося завантажити дані про сектор з ID = " + sector.Id.ToString());
                            }
                        }
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
                //---------------------
                {// Вытаскиваем стандарт
                    IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                    rsFreqPlan.Select("NAME");
                    int PlanID = objStation.GetI("PLAN_ID");
                    rsFreqPlan.SetWhere("ID", IMRecordset.Operation.Eq, PlanID);
                    try
                    {
                        rsFreqPlan.Open();
                        if (!rsFreqPlan.IsEOF())
                            standard = rsFreqPlan.GetS("NAME");
                    }
                    finally
                    {
                        rsFreqPlan.Close();
                        rsFreqPlan.Destroy();
                    }
                }
            }
            LoadChannel(stationList.Count);
            LoadPosition(id);
            LoadEquipment(id);

            for (int i = 0; i < objStations.Count; i++)
            {
                int StationID = objStations[i].GetI("ID");
                LoadAntenna(i, StationID);
            }
            //----
            addValidVisnMonth = 6;
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            int i;
            for (i = 0; i < stationList.Count; i++)
                stationList[i].Dispose();
            for (i = 1; i < objStations.Count; i++) //нулевой удалится в базовом классе
                objStations[i].Dispose();
            base.DisposeExec();
        }

        public override void Coordination()
        {
            base.Coordination();
            CoordForm form = null;
            PositionState pos = new PositionState();
            if (stationList != null)
                if (stationList.Count > 0)
                    pos.LoadStatePosition(stationList[0].objPosition.Id, stationList[0].objPosition.TableName);
            if (pos.Id != IM.NullI)
            {
                form = new CoordForm(stationList[0], TableName, applID);

                if (form.ShowDialog() == DialogResult.Yes && objStations!=null && objStations.Count>0)
                {
                    //IMRecordset rsMobsta = new IMRecordset(objTableName, IMRecordset.Mode.ReadOnly);
                    //try
                    //{
                    //    rsMobsta.Select("ID,COORD_NEEDED,COORD_LADMS");
                    //    rsMobsta.SetWhere("ID", IMRecordset.Operation.Eq, recID);
                    //    rsMobsta.Open();
                    //    if (!rsMobsta.IsEOF())
                    //    {
                    //        objStation.Put("COORD_NEEDED", rsMobsta.GetS("COORD_NEEDED"));
                    //        objStation.Put("COORD_LADMS", rsMobsta.GetS("COORD_LADMS"));
                    //    }
                    //}
                    //finally
                    //{
                    //    rsMobsta.Final();
                    //}
                }
            }
        }

        private void LoadAntenna(int index, int msID)
        {
            if (stationList[index].objAntenna == null)
            {
                int posId = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                r.Select("ANT_ID,ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, msID);
                r.Open();
                if (!r.IsEOF())
                {
                    posId = r.GetI("ANT_ID");
                    if ((posId != 0) && (posId != IM.NullI))
                        stationList[index].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntennaMob, posId);
                }
                r.Close();
                r.Destroy();
            }
        }

        private void LoadPosition(int StationID)
        {
            if (stationList[0].objPosition == null)
            {
                int posId = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                r.Select("POS_ID,ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, StationID);
                r.Open();
                if (!r.IsEOF())
                {
                    posId = r.GetI("POS_ID");
                    stationList[0].objPosition = new PositionState2();
                    stationList[0].objPosition.LoadStatePosition(posId, ICSMTbl.itblPositionWim);
                }
                r.Close();
                r.Destroy();
            }
        }

        private void LoadEquipment(int StationID)
        {
            if (stationList[0].objEquip == null)
            {
                int posId = IM.NullI;
                IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                r.Select("EQUIP_ID,ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, StationID);
                r.Open();
                if (!r.IsEOF())
                {
                    posId = r.GetI("EQUIP_ID");
                    if (posId != IM.NullI)
                        stationList[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipPmr, posId);
                    else
                        stationList[0].objEquip = null;
                }
                r.Close();
                r.Destroy();
            }
        }


        public int GetSectorCount()
        {
            return stationList.Count;
        }

        public override int GetTxCount()
        {
            return stationList.Count;
        }

        public int GetSectorObjIDByIndex(int Index)
        {
            return objStations[Index].GetI("ID");
        }

        protected override string GetXMLParamGrid()
        {
            string XmlString =
                  "<items>"
                + "<item key='header0'    type='' flags=''     tab=''   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
                + "<item key='header1'    type='' flags='s'    tab=''   display='Сектор 1'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"
                //0
            + "<item key='mKStand'       type='' flags=''     tab=''   display='Стандарт' ReadOnly='true' background='' bold=''  />"
            + "<item key='KStand'        type='' flags='s'    tab='1'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //1
                + "<item key='kOzn'       type='' flags=''     tab=''   display='Означення РЕЗ (або ІD БС)' ReadOnly='true' background='' bold=''  />"
                + "<item key='ozn'        type='string'   DBField='NAME'       flags='s'    tab='2'   display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //2
                + "<item key='mKLat'      type='lat'       flags=''     tab=''  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y'/>"
                + "<item key='KLat0'      type='lat'       flags='s'    tab='3'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                //3
                + "<item key='mKLon'     type='lat'       flags=''     tab=''  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y'/>"
                + "<item key='KLon0'      type='lat'       flags='s'    tab='4'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                //4
            + "<item key='kAsl'      type='lat'       flags=''     tab=''  display='Висота поверхні землі, м'     ReadOnly='true' background='' bold='' fontColor='gray' />"
            + "<item key='asl'       type='lat' flags='s'    tab=''  display=''     ReadOnly='true' background='' bold=''  HorizontalAlignment='center' fontColor='gray' />"
                //5
                + "<item key='kAdr'      type='lat'       flags=''     tab=''  display='Адреса встановлення РЕЗ'     ReadOnly='true' background='' bold='y'/>"
                + "<item key='addr'      type='lat'       flags='s'    tab='5'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
                //6
                + "<item key='kType'      type='lat'       flags=''     tab=''  display='Тип базової станції'     ReadOnly='true' background='' bold='y'/>"
                + "<item key='type'      type='lat'       flags='s'    tab='6'  display=''     ReadOnly='false' background='' bold='y'  HorizontalAlignment='center'/>"
                //7            
            + "<item key='kClass'    type='lat'       flags=''     tab=''  display='Клас випромінювання'     ReadOnly='true' background='' bold='' fontColor='gray' />"
            + "<item key='class'     type='string' DBField='DESIG_EMISSION' flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                //8            
            + "<item key='kSert'      type='lat'       flags=''     tab=''  display='Сертифікат відповідності(№/дата)'     ReadOnly='true' background='' bold='' fontColor='gray' />"
            + "<item key='sertNom'    type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
            + "<item key='sertDate'   type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                //9       
                + "<item key='kCount'     type='lat'       flags=''     tab=''  display='Кількість передавачів'     ReadOnly='true' background='' bold='' />"
                + "<item key='count'      type='double'  DBField='EQUIP_NBR'  flags='s'    tab='7'  display=''     ReadOnly='false' background='' bold=''  HorizontalAlignment='center'/>"
                //10
                + "<item key='kFiltType'  type='lat'       flags=''     tab=''  display='Тип фільтру'     ReadOnly='true' background='' bold='' />"
                + "<item key='filtType0'  type='lat'       flags='s'    tab='8'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //11           
                + "<item key='kPower'     type='lat'       flags=''     tab=''  display='Потужність передавача, Вт'     ReadOnly='true' background='' bold='' />"
                + "<item key='pow0'       type='lat'       flags='s'    tab='9'  display=''     ReadOnly='false' background='' bold=''  HorizontalAlignment='center'/>"
                //12
                + "<item key='kAntType'   type='lat'       flags=''     tab=''  display='Тип антени'     ReadOnly='true' background='' bold='y' />"
                + "<item key='antType0'   type='lat'       flags='s'    tab='10'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"
                //13
                + "<item key='kAntHeight' type='lat'       flags=''     tab=''  display='Висота антени над рівнем землі, м'     ReadOnly='true' background='' bold='' />"
                + "<item key='antHeight0' type='double'  DBField='AGL'  flags='s'    tab='11'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //14
                + "<item key='kAntAz' type='lat'       flags=''     tab=''  display='Азимут випромінювання антени, град'     ReadOnly='true' background='' bold='' />"
                + "<item key='antAz0' type='double'  DBField='AZIMUTH'  flags='s'    tab='12'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //15
                + "<item key='kAntGain'      type='lat'       flags=''     tab=''  display='Коеф. підсилення антени, дБі'     ReadOnly='true' background='' bold='' fontColor='gray'  />"
            + "<item key='antGain0'   type='double' DBField='GAIN'  flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                //16
            + "<item key='kAntDsWidth'      type='lat'       flags=''     tab=''  display='Ширина ДС антени, град'     ReadOnly='true' background='' bold='' fontColor='gray' />"
            + "<item key='antDsWdth0'   type='double' DBField='GAIN'  flags='s'     tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                //17
            + "<item key='kAntAngle'      type='lat'       flags=''     tab=''  display='Сумарний кут нахилу ДСА, град'     ReadOnly='true' background='' bold=''  />"
            + "<item key='antAngle0'   type='double' DBField='ELEVATION'  flags='s'     tab=''  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //18
                + "<item key='kFiderLoss'      type='lat'       flags=''     tab=''  display='Затухання у фідерному тракті, дБ'     ReadOnly='true' background='' bold='' />"
                + "<item key='fiderLoss0'   type='double' DBField='TX_LOSSES'  flags='s'     tab=''  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
                //19
                + "<item key='kChanNum'      type='lat'       flags=''     tab=''  display='Номери каналів'     ReadOnly='true' background='' bold='' />"
            + "<item key='chan0'   type='double' DBField='GAIN'  flags='s'     tab='13'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center'/>"
                //20
                + "<item key='kRxFreq'   type='lat'       flags=''     tab=''  display='Частоти приймання, МГц'     ReadOnly='true' background='' bold='y' />"
            + "<item key='rxFreq0'   type='double' DBField='TX_FREQ'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"
                //21
                + "<item key='kTxFreq'   type='lat'       flags=''     tab=''  display='Частоти передавання, МГц'     ReadOnly='true' background='' bold='y' />"
            + "<item key='txFreq0'   type='double' DBField='TX_FREQ'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
                //22
                + "<item key='kObj'       type='lat'       flags=''     tab=''  display='Об`єкт(и) РЧП'     ReadOnly='true' background='' bold='' />"
                + "<item key='obj0'       type='lat'       flags='s'    tab=''  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
                + "</items>"
                //
                ;
            return XmlString;
        }

        //============================================================
        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            if ((cell.Key == "KLon0")
                || (cell.Key == "addr")
            || (cell.Key == "type") || (cell.Key == "asl")
                || (cell.Key == "chan0") || (cell.Key == "chan1") || (cell.Key == "chan2")
                || (cell.Key == "chan3") || (cell.Key == "chan4") || (cell.Key == "chan5")
                || (cell.Key == "filtType0") || (cell.Key == "filtType1") || (cell.Key == "filtType2")
                || (cell.Key == "filtType3") || (cell.Key == "filtType4") || (cell.Key == "filtType5")
                || (cell.Key == "antType0") || (cell.Key == "antType1") || (cell.Key == "antType2")
                || (cell.Key == "antType3") || (cell.Key == "antType4") || (cell.Key == "antType5")
                || (cell.Key == "obj0") || (cell.Key == "obj1") || (cell.Key == "obj2")
                || (cell.Key == "obj3") || (cell.Key == "obj4") || (cell.Key == "obj5"))
                cell.cellStyle = EditStyle.esEllipsis;
            else if ((cell.Key == "KStand"))
            {
                cell.cellStyle = EditStyle.esPickList;
                cell.PickList = CStandard.getListStandard(radioTech);
            }
        }
        //============================================================
        /// <summary>
        /// Initialization for entire grid
        /// It's needed for setup columns for each sector
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Grid grid)
        {
            for (int i = 1; i < 6 && i < stationList.Count; i++)
            {
                StationRR Station = stationList[i];
                //if (Station.ID != IM.NullI)
                {
                    Cell c0 = grid.AddColumn(0, "Сектор " + (i + 1).ToString());
                    c0.FontName = grid.GetCell(0, 0).FontName;
                    c0.TextHeight = grid.GetCell(0, 0).TextHeight;
                    c0.HorizontalAlignment = grid.GetCell(0, 0).HorizontalAlignment;
                    c0.VerticalAlignment = grid.GetCell(0, 0).VerticalAlignment;
                    c0.BackColor = grid.GetCell(0, 0).BackColor;
                    c0.FontStyle = grid.GetCell(0, 0).FontStyle;

                    int cellTabOrder = 9 + i * 4;

                    Cell c1 = ExtendGrid(grid, i, "filtType", "", -1);
                    Cell c2 = ExtendGrid(grid, i, "pow", "", -1);
                    Cell c3 = ExtendGrid(grid, i, "antType", "", cellTabOrder++);
                    Cell c4 = ExtendGrid(grid, i, "antHeight", "", cellTabOrder++);
                    Cell c5 = ExtendGrid(grid, i, "antAz", "", cellTabOrder++);
                    Cell c6 = ExtendGrid(grid, i, "antGain", "", -1);
                    Cell c7 = ExtendGrid(grid, i, "antDsWdth", "", -1);
                    Cell c8 = ExtendGrid(grid, i, "antAngle", "", -1);
                    Cell c9 = ExtendGrid(grid, i, "fiderLoss", "", -1);
                    Cell c10 = ExtendGrid(grid, i, "chan", "", cellTabOrder++);
                    Cell c11 = ExtendGrid(grid, i, "rxFreq", "", -1);
                    Cell c12 = ExtendGrid(grid, i, "txFreq", "", -1);
                    Cell c13 = ExtendGrid(grid, i, "obj", "", -1);
                }
            }
            base.InitParamGrid(grid);
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            MobStaApp retStation = new MobStaApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            MobStaApp retStation = newAppl as MobStaApp;
            retStation.CopyFromMe(grid,
               (stationList[0].objEquip != null) ? stationList[0].objEquip.GetI("ID") : IM.NullI,
               (stationList[0].objAntenna != null) ? stationList[0].objAntenna.GetI("ID") : IM.NullI,
               stationList[0].MaxPower,
               stationList[0].AntHeight,
               stationList[0].FiderZat,
               msvSectors[0], standard);
            return retStation;
        }
        //===========================================================
        public void CopyFromMe(Grid grid, int equipID, int antID, double maxPow, double _asl, double _fider, List<int> cannels, string _stand)
        {
            Cell tmpCell = null;
            // Оборудование
            if ((equipID != 0) && (equipID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("type");
                stationList[0].objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipPmr, equipID);
                tmpCell.Value = stationList[0].objEquip.GetS("NAME");
                OnCellValidate(tmpCell, grid);
                AutoFill(tmpCell, grid);
            }
            if ((antID != 0) && (antID != IM.NullI))
            {
                tmpCell = grid.GetCellFromKey("antType0");
                stationList[0].objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntennaMob, antID);
                tmpCell.Value = stationList[0].objAntenna.GetS("NAME");
                OnCellValidate(tmpCell, grid);
            }
            // Стандарт
            tmpCell = grid.GetCellFromKey("KStand");
            tmpCell.Value = _stand;
            OnCellValidate(tmpCell, grid);
            // Мощность
            tmpCell = grid.GetCellFromKey("pow0");
            tmpCell.Value = maxPow.ToString();
            OnCellValidate(tmpCell, grid);
            // ALS
            tmpCell = grid.GetCellFromKey("antHeight0");
            tmpCell.Value = _asl.ToString();
            OnCellValidate(tmpCell, grid);
            // Фидер
            tmpCell = grid.GetCellFromKey("fiderLoss0");
            tmpCell.Value = _fider.ToString();
            OnCellValidate(tmpCell, grid);
            //Каналы
            msvSectors[0] = cannels;
            tmpCell = grid.GetCellFromKey("chan0");
            UpdateOneParamGrid(tmpCell, grid);
        }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (stationList[0].objPosition != null) ? stationList[0].objPosition.Province : "";
            string city = (stationList[0].objPosition != null) ? stationList[0].objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            string cid = objStations[0].GetS("NAME");

            if ((docType == DocType.VISN) || (docType == DocType.LYST_TR_CALLSIGN))
            {
                string filter = "";
                switch (radioTech)
                {
                    case CRadioTech.GSM_1800:
                    case CRadioTech.GSM_900:
                    case CRadioTech.CDMA_450:
                    case CRadioTech.CDMA_800:
                    case CRadioTech.UMTS:
                    case CRadioTech.DAMPS:
                        filter = cid;
                        break;
                    default:
                        filter = string.Format("{0}{1}", ConvertType.DepartmetToCode(department), HelpFunction.getUserNumber());
                        break;
                }
                fullPath += string.Format("{0}-{1}-{2}.rtf", filter, HelpFunction.getAreaCode(prov, city), number);
            }
            else if ((docType == DocType.DOZV) || (docType == DocType.LYST_TR_CALLSIGN))
            {
                string filter = "";
                if (radioTech == CRadioTech.GSM_1800)
                    filter += "БС1800-" + cid;
                else if (radioTech == CRadioTech.GSM_900)
                    filter += "БС900-" + cid;
                else if (radioTech == CRadioTech.CDMA_450)
                    filter += "БС450-" + cid;
                else if (radioTech == CRadioTech.CDMA_800)
                    filter += "БС800-" + cid;
                else if (radioTech == CRadioTech.UMTS)
                    filter += "БС2100-" + cid;
                else if (radioTech == CRadioTech.DECT)
                    filter += "БС1880";
                else if (radioTech == CRadioTech.DAMPS)
                    filter += "БС880-" + cid;

                fullPath += string.Format("{0}-{1}-{2}.rtf", filter, HelpFunction.getAreaCode(prov, city), number);
            }
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else if (docType == DocType.DOZV_TR_SPEC)
            {
                string selectedProv = HelpFunction.getAreaCode(prov, city).Trim();
                string prefix = "СД";
                fullPath += prefix + "-" + selectedProv + "-" + (number.Length < 6 ? number.PadRight(6, '0') : number) + ".rtf";
            }
            else
                throw new IMException("Error document type");

            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        public override void ChangeOwner(int _newOwnerID)
        {
            foreach (IMObject obj in objStations)
            {
                if (obj.GetI("OWNER_ID") != _newOwnerID)
                {
                    obj.Put("OWNER_ID", _newOwnerID);
                    CJournal.CheckTable(recordID.Table, obj.GetI("ID"), obj);
                    obj.SaveToDB();
                }
            }
            CJournal.CreateReport(applID, appType);
        }
        ////==============================================================
        ///// <summary>
        ///// Сохраняет дату печати дозвола в ДРВ
        ///// </summary>
        ///// <param name="dateDRV">Дата печати дозвола в ДРВ</param>
        //public override void SaveDateVisnFromDRV(DateTime dateDRV)
        //{
        //    foreach (IMObject station in objStations)
        //    {
        //        SaveDateVisnFromDRVLocal(dateDRV, station, recordID.Table, station.GetI("ID"));
        //    }
        //}
        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    foreach (IMObject station in objStations)
        //    {
        //        IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //        r.Select("ID,CUST_TXT5,CUST_DAT4,CUST_DAT5,CUST_TXT4,CUST_TXT3,CUST_DAT2,CUST_DAT3,CUST_TXT2");
        //        r.SetWhere("ID", IMRecordset.Operation.Eq, station.GetI("ID"));
        //        try
        //        {
        //            r.Open();
        //            if (!r.IsEOF())
        //            {
        //                r.Edit();
        //                switch (docType)
        //                {
        //                    case DocType.VISN:
        //                        r.Put("CUST_TXT3", docName.ToFileNameWithoutExtension());
        //                        station.Put("CUST_TXT3", docName.ToFileNameWithoutExtension());
        //                        r.Put("CUST_DAT2", startDate);
        //                        station.Put("CUST_DAT2", startDate);
        //                        r.Put("CUST_DAT3", endDate);
        //                        station.Put("CUST_DAT3", endDate);
        //                        r.Put("CUST_TXT2", IM.ConnectedUser());
        //                        station.Put("CUST_TXT2", IM.ConnectedUser());
        //                        break;
        //                }
        //                r.Update();
        //            }
        //        }
        //        finally
        //        {
        //            r.Close();
        //            r.Destroy();
        //        }
        //    }
        //}
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            return objStations.Count.ToString();
        }
        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            //string DesigEmission = objStation.GetS("DESIG_EMISSION");

            if (stationList[0].objPosition == null)
                throw new Exception("Не задано (обрано) жодного сайту!");
            foreach (IMObject obj in objStations)
            {
                SaveParams(objStations.IndexOf(obj));
            }

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
            r.Select("CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r.Open();

            if (!r.IsEOF())
            {
                r.Edit();

                for (int i = 0; i < 6; i++)
                {
                    string ObjIdName = "OBJ_ID" + (i + 1).ToString();
                    if (i < objStations.Count)
                    {
                        r.Put(ObjIdName, objStations[i].GetI("ID"));
                        // Каналы
                        r.Put("CUST_TXT" + (i + 1).ToString(), NormalizeNumberList(stationList[i].strChannel).Replace(',', '.'));
                        // Частоты на передачц (TX)
                        r.Put("CUST_TXT" + (6 + i + 1).ToString(), NormalizeNumberList(stationList[i].strFreqTX).Replace(',', '.'));
                        // Частоты на прием (RX)
                        r.Put("CUST_TXT" + (12 + i + 1).ToString(), NormalizeNumberList(stationList[i].strFreqRX).Replace(',', '.'));
                    }
                    else
                        r.Put(ObjIdName, IM.NullI);
                }
                r.Update();
            }
            r.Close();
            r.Destroy();

            SaveLicence();
            return true;
        }

        //============================================================
        /// <summary>
        /// Автоматически заполнить неоюходимые поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon0")
            {
                Cell fillCell = null;

                if (stationList[0].objPosition != null)
                {
                    double latDMS = stationList[0].objPosition.LatDms;
                    fillCell = grid.GetCellFromKey("KLat0");
                    fillCell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                    OnCellValidate(fillCell, grid);

                    double asl = stationList[0].objPosition.Asl;
                    stationList[0].Height = asl;
                    SafeFillCellByKey(grid, "asl", (asl != IM.NullD) ? asl.ToString("F0") : "", false);
                    OnCellValidate(grid.GetCellFromKey("asl"), grid);

                    SafeFillCellByKey(grid, "addr", stationList[0].objPosition.FullAddressAuto, false);
                    OnCellValidate(grid.GetCellFromKey("addr"), grid);
                }
                else
                {
                    SafeFillCellByKey(grid, "KLon0", "", false);

                    SafeFillCellByKey(grid, "asl", "", false);

                    SafeFillCellByKey(grid, "addr", "", false);
                }
            }
            if (cell.Key == "type")
            {
                if (stationList[0].objEquip != null)
                {// Заполяем поля значениями
                    Cell fillCell = null;

                    SafeFillCellByKey(grid, "sertNom", stationList[0].objEquip.GetS("CUST_TXT1"), false);
                    fillCell = grid.GetCellFromKey("sertNom");
                    OnCellValidate(fillCell, grid);

                    DateTime dt = stationList[0].objEquip.GetT("CUST_DAT1");

                    if (dt.Year > 1990)
                        SafeFillCellByKey(grid, "sertDate", dt.ToString("dd MM yyyy"), false);
                    else
                        SafeFillCellByKey(grid, "sertDate", "", false);
                    fillCell = grid.GetCellFromKey("sertDate");
                    OnCellValidate(fillCell, grid);

                    SafeFillCellByKey(grid, "class", objStation.GetS("DESIG_EMISSION"), true);

                    //fillCell.Value = stationList[0].objEquip.GetS("DESIG_EMISSION").ToString();              

                    double maxPower = stationList[0].objEquip.GetD("MAX_POWER");
                    string pow = "";
                    if (maxPower != IM.NullD)
                    {
                        maxPower = Math.Pow(10.0, ((maxPower - 30) / 10.0));
                        pow = maxPower.ToString("F3");
                    }
                    for (int i = 0; i < stationList.Count; ++i)
                    {
                        SafeFillCellByKey(grid, "pow" + i.ToString(), pow, true);
                        stationList[i].MaxPower = maxPower;
                    }
                }
                else
                {
                    SafeFillCellByKey(grid, "sertNom", "", false);

                    SafeFillCellByKey(grid, "sertDate", "", false);

                    SafeFillCellByKey(grid, "class", "", false);

                    for (int i = 0; i < stationList.Count; ++i)
                    {
                        SafeFillCellByKey(grid, "pow" + i.ToString(), "", false);
                    }
                }
            }
            else if ((cell.Key == "antType0")
                  || (cell.Key == "antType1")
                  || (cell.Key == "antType2")
                  || (cell.Key == "antType3")
                  || (cell.Key == "antType4")
                  || (cell.Key == "antType5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                if (stationList[index].objAntenna != null)
                {
                    SafeFillCellByKey(grid, "antGain" + index.ToString(), (stationList[index].objAntenna.GetD("GAIN") != IM.NullD) ? stationList[index].objAntenna.GetD("GAIN").ToString("F3") : "", true);

                    SafeFillCellByKey(grid, "antDsWdth" + index.ToString(), (stationList[index].objAntenna.GetD("H_BEAMWIDTH") != IM.NullD) ? stationList[index].objAntenna.GetD("H_BEAMWIDTH").ToString("F3") : "", true);

                    //Фидер
                    if (stationList[index].FiderZat == IM.NullD)
                    {
                        SafeFillCellByKey(grid, "fiderLoss" + index.ToString(), "3", true);
                    }
                }
                else
                {
                    SafeFillCellByKey(grid, "antGain" + index.ToString(), "", false);

                    SafeFillCellByKey(grid, "antDsWdth" + index.ToString(), "", false);
                }
            }
        }

        public override void AddSector(Grid grid)
        {
            int columCount = stationList.Count;
            if (columCount < 6)
            {
                StationRR Station = new StationRR();

                Station.fti.FilterIndex = IM.NullI;
                Station.MaxPower = stationList[columCount - 1].MaxPower;
                Station.fti.FilterName = "";
                Station.Azimuth = IM.NullD;
                Station.TxFreqs = stationList[columCount - 1].TxFreqs.GetRange(0, stationList[columCount - 1].TxFreqs.Count);
                Station.RxFreqs = stationList[columCount - 1].RxFreqs.GetRange(0, stationList[columCount - 1].RxFreqs.Count);
                if (stationList[columCount - 1].objAntenna != null)
                {
                    RecordPtr ant = new RecordPtr(ICSMTbl.itblAntennaMob, stationList[columCount - 1].objAntenna.GetI("ID"));
                    Station.objAntenna = IMObject.LoadFromDB(ant);
                }
                stationList.Add(Station);
                List<int> chans = msvSectors[msvSectors.Count - 1];
                msvSectors.Add(chans);

                IMObject recMobStation = IMObject.New(ICSMTbl.itblMobStation);
                int newid = recMobStation.GetI("ID");
                recMobStation.Put("CLASS", "FB");
                recMobStation.Put("ADM", "UKR");
                recMobStation.Put("TX_ADDLOSSES", 0);
                recMobStation.Put("OWNER_ID", objStations[columCount - 1].GetI("OWNER_ID"));
                recMobStation.Put("STANDARD", radioTech);

                Station.ID = newid;
                objStations.Add(recMobStation);

                int cellTabOrder = 9 + columCount * 4;

                Cell c0 = ExtendGrid(grid, columCount, "header", "Сектор " + (columCount + 1).ToString(), -1);
                Cell c1 = ExtendGrid(grid, columCount, "filtType", Station.fti.FilterName, -1);//Math.Pow(10.0, Station.MaxPower / 10.0)
                Cell c2 = ExtendGrid(grid, columCount, "pow", (Station.MaxPower == IM.NullD) ? "" : Station.MaxPower.ToString("F3"), -1);
                Cell c3 = ExtendGrid(grid, columCount, "antType", (Station.objAntenna == null) ? "" : Station.objAntenna.GetS("NAME"), cellTabOrder++);
                Cell c4 = ExtendGrid(grid, columCount, "antHeight", (Station.AntHeight == IM.NullD) ? "" : Station.AntHeight.ToString("F3"), cellTabOrder++);
                Cell c5 = ExtendGrid(grid, columCount, "antAz", "", cellTabOrder++);
                Cell c6 = ExtendGrid(grid, columCount, "antGain", (Station.AntCoef == IM.NullD) ? "" : Station.AntCoef.ToString("F3"), -1);
                Cell c13 = ExtendGrid(grid, columCount, "antDsWdth", (Station.AntDSWidth == IM.NullD) ? "" : Station.AntDSWidth.ToString("F3"), -1);
                Cell c7 = ExtendGrid(grid, columCount, "antAngle", (Station.AntAngle == IM.NullD) ? "" : Station.AntAngle.ToString("F3"), -1);
                Cell c8 = ExtendGrid(grid, columCount, "fiderLoss", (Station.FiderZat == IM.NullD) ? "" : Station.FiderZat.ToString("F3"), -1);
                Cell c9 = ExtendGrid(grid, columCount, "chan", "", cellTabOrder++);
                Cell c11 = ExtendGrid(grid, columCount, "txFreq", "", -1);
                Cell c10 = ExtendGrid(grid, columCount, "rxFreq", "", -1);
                UpdateOneParamGrid(c9, grid);
                Cell c12 = ExtendGrid(grid, columCount, "obj", newid.ToString(), -1);

                CellValidate(c2, grid);
                CellValidate(c3, grid);
                Station.AntCoef = stationList[columCount - 1].AntCoef;
                Station.AntDSWidth = stationList[columCount - 1].AntDSWidth;
                Station.AntAngle = stationList[columCount - 1].AntAngle;
                Station.FiderZat = stationList[columCount - 1].FiderZat;
                Station.AntHeight = stationList[columCount - 1].AntHeight;
                Station.AntDSWidth = stationList[columCount - 1].AntDSWidth;
                Station.fti = stationList[columCount - 1].fti;
                c1.Value = Station.fti.FilterName;
                CellValidate(c1, grid);

                c4.Value = (Station.AntHeight == IM.NullD) ? "" : Station.AntHeight.ToString("F3");
                c5.Value = "";
                c6.Value = (Station.AntCoef == IM.NullD) ? "" : Station.AntCoef.ToString("F3");
                c7.Value = (Station.AntAngle == IM.NullD) ? "" : Station.AntAngle.ToString("F3");
                c8.Value = (Station.FiderZat == IM.NullD) ? "" : Station.FiderZat.ToString("F3");
                c13.Value = (Station.AntDSWidth == IM.NullD) ? "" : Station.AntDSWidth.ToString("F3");
                CellValidate(c4, grid);
                CellValidate(c5, grid);
                CellValidate(c6, grid);
                CellValidate(c7, grid);
                CellValidate(c8, grid);
                CellValidate(c9, grid);
                CellValidate(c10, grid);
                CellValidate(c11, grid);
                CellValidate(c12, grid);
                CellValidate(c13, grid);

            }
        }

        public override void RemoveSector(Grid grid)
        {
            if (objStations.Count > 1)
            {
                HelpClasses.Forms.FSectorRemove form = new XICSM.UcrfRfaNET.HelpClasses.Forms.FSectorRemove();
                for (int i = 1; i <= stationList.Count; ++i)
                    form.cbSecs.Items.Add("Сектор " + i.ToString());
                form.cbSecs.SelectedIndex = 0;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    int index = Convert.ToInt32(form.cbSecs.Text.Substring(7)) - 1;
                    if (index == 0)
                    {
                        stationList[1].OznRez = stationList[0].OznRez;
                        stationList[1].objPosition = stationList[0].objPosition;
                        stationList[1].objEquip = stationList[0].objEquip;
                    }

                    AddDeletedSector(objStations[index].GetI("ID"), recordID.Table);
                    stationList.RemoveAt(index);
                    objStations.RemoveAt(index);
                    msvSectors.RemoveAt(index);

                    grid.rowList.Clear();
                    LoadStructureParamGrid(grid);
                    InitParamGrid(grid);
                    UpdateGridParameters(grid);
                }
            }

            /*
            int columCount = grid.GetColumnCount(0);

            int ForDelete = columCount - 2;

            StationBS Station = stationList[columCount - 2];
            int StationID = Station.ID;
            Station.ID = IM.NullI;
            stationList[columCount - 2] = Station;

            if (columCount > 2)
            {
               grid.RemoveColumn(0);
               grid.RemoveColumn(12);
               grid.RemoveColumn(13);
               grid.RemoveColumn(14);
               grid.RemoveColumn(15);
               grid.RemoveColumn(16);
               grid.RemoveColumn(17);
               grid.RemoveColumn(18);
               grid.RemoveColumn(19);
               grid.RemoveColumn(20);
               grid.RemoveColumn(21);

               IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadWrite);
               r.Select("ID");
               r.SetWhere("ID", IMRecordset.Operation.Eq, StationID);
               r.Open();

               while (!r.IsEOF())
               {
                  r.Delete();
                  r.MoveNext();
               }
               r.Close();
               r.Destroy();
            }*/
        }

        protected FilterTypeInfo GetFilterTypeInfoByStationID(int StatiobID, string StationTable)
        {
            FilterTypeInfo fti;

            fti.FilterIndex = IM.NullI;
            fti.FilterName = "";

            IMRecordset r = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadOnly);
            r.Select("OBJ_ID,OBJ_TABLE,EX_FILTER_ID");
            r.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, StatiobID);
            r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, StationTable);//ICSMTbl.itblMobStation);
            try
            {
                fti.FilterName = "";
                r.Open();
                if (!r.IsEOF())
                {
                    fti.FilterIndex = r.GetI("EX_FILTER_ID");

                    if (fti.FilterIndex != IM.NullI)
                    {
                        IMRecordset r2 = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
                        r2.Select("ID,NAME");
                        r2.SetWhere("ID", IMRecordset.Operation.Eq, fti.FilterIndex);
                        try
                        {
                            r2.Open();
                            if (!r2.IsEOF())
                            {
                                fti.FilterName = r2.GetS("NAME");
                            }
                        }
                        finally
                        {
                            r2.Close();
                            r2.Destroy();
                        }
                    }
                }
                //cell.Value = stationList[index].FilterName;
                //CellValidate(cell, grid);
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return fti;
        }

        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            if (cell.Key == "KLon0")
            {
                int msID = objStation.GetI("ID");

                if (stationList[0].objPosition != null)
                {
                    stationList[0].objPosition.GetAdminSiteInfo(stationList[0].objPosition.AdminSiteId);
                    double lonDMS = stationList[0].objPosition.LongDms;
                    stationList[0].Lon = stationList[0].objPosition.LongDms;
                    stationList[0].Lat = stationList[0].objPosition.LatDms;
                    cell.Value = (lonDMS != IM.NullD) ? HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon) : "";
                    OnCellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            else if (cell.Key == "ozn")
            {
                HelpFunction.LoadFromDbToCell(cell, objStation);
                stationList[0].OznRez = cell.Value;
            }
            else if (cell.Key == "class")
            {
                HelpFunction.LoadFromDbToCell(cell, objStation);
            }
            else if (cell.Key == "asl")
            {
                if (stationList[0].objPosition != null)
                {
                    double masl = stationList[0].objPosition.Asl;

                    if (masl != IM.NullD)
                    {
                        cell.Value = Convert.ToInt32(masl).ToString("F3");
                        CellValidate(cell, grid);
                        AutoFill(cell, grid);
                    }
                    else
                        cell.Value = "";
                }
            }
            else if (cell.Key == "type")
            {
                if (stationList[0].objEquip != null)
                {
                    cell.Value = stationList[0].objEquip.GetS("NAME");
                    if (stationList[0].objEquip != null)
                    {// Заполяем поля значениями

                        SafeFillCellByKey(grid, "sertNom", stationList[0].objEquip.GetS("CUST_TXT1"), true);


                        DateTime dt = stationList[0].objEquip.GetT("CUST_DAT1");

                        if (dt.Year > 1990)
                            SafeFillCellByKey(grid, "sertDate", dt.ToString("dd MM yyyy"), true);
                        else
                            SafeFillCellByKey(grid, "sertDate", "", true);

                        SafeFillCellByKey(grid, "class", objStation.GetS("DESIG_EMISSION"), false);

                        SafeFillCellByKey(grid, "class", stationList[0].objEquip.GetS("DESIG_EMISSION"), true);

                        double maxPower = stationList[0].objEquip.GetD("MAX_POWER");
                        string pow = "";
                        if (maxPower != IM.NullD)
                        {
                            maxPower = Math.Pow(10.0, ((maxPower - 30) / 10.0));
                            pow = maxPower.ToString("F3");
                        }
                    }
                    else
                    {
                        SafeFillCellByKey(grid, "sertNom", "", false);

                        SafeFillCellByKey(grid, "sertDate", "", false);

                        SafeFillCellByKey(grid, "class", "", false);

                        for (int i = 0; i < stationList.Count; ++i)
                            SafeFillCellByKey(grid, "pow" + i.ToString(), "", false);

                    }
                }
            }
            else if (cell.Key == "count")
            {
                //   objStations[0].Put("EQUIP_NBR", stationList[0].Count);
                stationList[0].Count = objStations[0].GetD("EQUIP_NBR");
                if (stationList[0].Count == IM.NullD)
                {
                    cell.Value = "Згідно комплектації базової станії";
                }
                else
                    cell.Value = stationList[0].Count.ToString();
            }
            else if ((cell.Key == "pow0") || (cell.Key == "pow1") || (cell.Key == "pow2") ||
                        (cell.Key == "pow3") || (cell.Key == "pow4") || (cell.Key == "pow5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(3));
                if (objStations.Count > index)
                {
                    double maxPower = objStations[index].GetD("PWR_ANT");
                    //string pow = "";
                    /*	if (maxPower != IM.NullD)
                        {
                            maxPower = Math.Pow(10.0, ((maxPower) / 10.0));
                            pow = maxPower.ToString("F3");
                        }*/
                    double PowerAsDouble = IM.NullD;

                    if (maxPower != IM.NullD)
                    {
                        PowerAsDouble = Math.Pow(10.0, maxPower / 10.0);
                        cell.Value = PowerAsDouble.ToString("F3");
                        CellValidate(cell, grid);
                    }
                    stationList[index].MaxPower = PowerAsDouble;
                    //cell.Value = pow;
                    //  CellValidate(cell, grid);
                }
            }
            if ((cell.Key == "antType0")
                  || (cell.Key == "antType1")
                  || (cell.Key == "antType2")
                  || (cell.Key == "antType3")
                  || (cell.Key == "antType4")
                  || (cell.Key == "antType5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));

                if (stationList[index].objAntenna != null)
                {
                    cell.Value = stationList[index].objAntenna.GetS("NAME");
                    CellValidate(cell, grid);
                    AutoFill(cell, grid);
                }
            }
            if ((cell.Key == "filtType0")
                  || (cell.Key == "filtType1")
                  || (cell.Key == "filtType2")
                  || (cell.Key == "filtType3")
                  || (cell.Key == "filtType4")
                  || (cell.Key == "filtType5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));

                int msID = objStations[index].GetI("ID");

                stationList[index].fti = GetFilterTypeInfoByStationID(msID, ICSMTbl.itblMobStation);

                cell.Value = stationList[index].fti.FilterName;
            }
            else if ((cell.Key == "fiderLoss0")
                 || (cell.Key == "fiderLoss1")
                 || (cell.Key == "fiderLoss2")
                 || (cell.Key == "fiderLoss3")
                 || (cell.Key == "fiderLoss4")
                 || (cell.Key == "fiderLoss5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (objStations.Count > index)
                {
                    HelpFunction.LoadFromDbToCell(cell, objStations[index]);
                    if (string.IsNullOrEmpty(cell.Value))
                        cell.Value = "3";
                }
                CellValidate(cell, grid);
            }
            else if ((cell.Key == "antGain0")
                 || (cell.Key == "antGain1")
                 || (cell.Key == "antGain2")
                 || (cell.Key == "antGain3")
                 || (cell.Key == "antGain4")
                 || (cell.Key == "antGain5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                if (objStations.Count > index)
                    HelpFunction.LoadFromDbToCell(cell, objStations[index]);
                CellValidate(cell, grid);
            }
            else if ((cell.Key == "antAz0")
                 || (cell.Key == "antAz1")
                 || (cell.Key == "antAz2")
                 || (cell.Key == "antAz3")
                 || (cell.Key == "antAz4")
                 || (cell.Key == "antAz5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));
                if (objStations.Count > index)
                    HelpFunction.LoadFromDbToCell(cell, objStations[index]);
                CellValidate(cell, grid);
            }
            else if ((cell.Key == "antAngle0")
                 || (cell.Key == "antAngle1")
                 || (cell.Key == "antAngle2")
                 || (cell.Key == "antAngle3")
                 || (cell.Key == "antAngle4")
                 || (cell.Key == "antAngle5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                if (objStations.Count > index)
                {
                    double tmpVal = objStations[index].GetD(cell.DBField);
                    if (tmpVal != IM.NullD)
                        cell.Value = (-tmpVal).ToString();
                    CellValidate(cell, grid);
                }
            }
            else if ((cell.Key == "antHeight0")
                 || (cell.Key == "antHeight1")
                 || (cell.Key == "antHeight2")
                 || (cell.Key == "antHeight3")
                 || (cell.Key == "antHeight4")
                 || (cell.Key == "antHeight5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (objStations.Count > index)
                    HelpFunction.LoadFromDbToCell(cell, objStations[index]);
                CellValidate(cell, grid);
            }
            else if ((cell.Key == "obj0")
             || (cell.Key == "obj1")
             || (cell.Key == "obj2")
             || (cell.Key == "obj3")
             || (cell.Key == "obj4")
             || (cell.Key == "obj5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(3));
                if (objStations[index] != null)
                    cell.Value = objStations[index].GetI("ID").ToString();
                CellValidate(cell, grid);
            }
            else if ((cell.Key == "chan0")
             || (cell.Key == "chan1")
             || (cell.Key == "chan2")
             || (cell.Key == "chan3")
             || (cell.Key == "chan4")
             || (cell.Key == "chan5")

             || (cell.Key == "rxFreq0")
             || (cell.Key == "rxFreq1")
             || (cell.Key == "rxFreq2")
             || (cell.Key == "rxFreq3")
             || (cell.Key == "rxFreq4")
             || (cell.Key == "rxFreq5")

             || (cell.Key == "txFreq0")
             || (cell.Key == "txFreq1")
             || (cell.Key == "txFreq2")
             || (cell.Key == "txFreq3")
             || (cell.Key == "txFreq4")
             || (cell.Key == "txFreq5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(cell.Key.Length - 1));
                if (ChangeFindChannel())
                    UpdateCellChannelRr2_UMTS(index, grid);
                else
                {
                    string RxParity = "D";
                    string TxParity = "U";
                    if (Standard == CRadioTech.DECT)
                    {
                        RxParity = "N";
                        TxParity = "N";
                    }                    
                    UpdateCellChannelRr2Boosted(index, RxParity, TxParity, grid);                    
                }
            }
            else if (cell.Key == "KStand")
            {
                cell.Value = standard;
            }
        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {

            if (cell.Key == "KLon0")
            {
                double lon = ConvertType.StrToDMS(cell.Value);
                if (lon != IM.NullD)
                {
                    cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                    if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (stationList[0].objPosition != null)
                    {
                        stationList[0].objPosition.LongDms = lon;
                        IndicateDifference(cell, stationList[0].objPosition.LonDiffersFromAdm);
                    }
                    stationList[0].Lon = lon;
                }
            }
            else if (cell.Key == "KLat0")
            {
                double lat = ConvertType.StrToDMS(cell.Value);
                if (lat != IM.NullD)
                {
                    cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                    if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                        ChangeColor(cell, Colors.badValue);
                    else
                        ChangeColor(cell, Colors.okvalue);
                    if (stationList[0].objPosition != null)
                    {
                        stationList[0].objPosition.LatDms = lat;
                        IndicateDifference(cell, stationList[0].objPosition.LatDiffersFromAdm);
                    }
                    stationList[0].Lat = lat;
                }
            }
            else if (cell.Key == "addr")
            {
                if (stationList[0].objPosition != null)
                    IndicateDifference(cell, stationList[0].objPosition.AddrDiffersFromAdm);
            }
            else if (cell.Key == "class")
            {
                foreach (IMObject obj in objStations)
                    HelpFunction.LoadFromCellToDb(cell, obj);
            }
            else if (cell.Key == "count")
            {
                double Count = ConvertType.ToDouble(cell, IM.NullD);
                if ((Count != IM.NullD) && (Count < 0))
                {
                    MessageBox.Show("Кількість передавачів має бути більше 0", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Count = 0;
                    cell.Value = "0";
                }
                stationList[0].Count = Count;

                /*      if (stationList[0].Count == IM.NullD)
                      {
                         stationList[0].Count = stationList.Count;              
                      }*/
                objStations[0].Put("EQUIP_NBR", stationList[0].Count);

            }
            else if (cell.Key == "ozn")
            {
                HelpFunction.LoadFromCellToDb(cell, objStations[0]);
                stationList[0].OznRez = cell.Value;
            }
            else if (cell.Key == "asl")
            {
                stationList[0].Height = ConvertType.ToDouble(cell, IM.NullD);
                if (stationList[0].objPosition != null)
                {
                    stationList[0].objPosition.Asl = stationList[0].Height;
                }
                cell.Value = (stationList[0].Height == IM.NullD) ? "" : stationList[0].Height.ToString("F0");
            }
            else if (cell.Key == "type")
            {
                if ((stationList[0].objEquip != null) && (cell.Value != stationList[0].objEquip.GetS("NAME")))
                    stationList[0].objEquip = null;  //Будем искать новое оборудование, так как имя уже не совпадает

            }
            else if ((cell.Key == "pow0") || (cell.Key == "pow1") || (cell.Key == "pow2") ||
                     (cell.Key == "pow3") || (cell.Key == "pow4") || (cell.Key == "pow5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(3));
                if (objStations.Count > index)
                {
                    double maxPower = ConvertType.ToDouble(cell, IM.NullD);
                    stationList[index].MaxPower = maxPower;
                    string pow = "";
                    if (maxPower != IM.NullD)
                    {

                        pow = maxPower.ToString("F3");
                        cell.Value = pow;
                        maxPower = 10.0 * Math.Log10(maxPower);
                        if (stationList[0].objEquip != null)
                        {
                            double temp = stationList[0].objEquip.GetD("MAX_POWER");
                            if ((stationList[0].objEquip.GetD("MAX_POWER") + 0.0001) < (maxPower + 30))
                                cell.BackColor = System.Drawing.Color.Red;
                            else cell.BackColor = System.Drawing.Color.White;
                        }
                    }
                    else
                        cell.Value = "";
                    objStations[index]["PWR_ANT"] = maxPower;
                }
            }
            else if ((cell.Key == "antType0")
                 || (cell.Key == "antType1")
                 || (cell.Key == "antType2")
                 || (cell.Key == "antType3")
                 || (cell.Key == "antType4")
                 || (cell.Key == "antType5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                if ((stationList[index].objAntenna != null) && (cell.Value != stationList[index].objAntenna.GetS("NAME")))
                    stationList[index].objAntenna = null;  //Будем искать новую антенну, так как имя уже не совпадает

                if (stationList[index].objAntenna == null)
                {// Ищем станцию, похожую по названию
                    RecordPtr recAnt = new RecordPtr(ICSMTbl.itblAntennaMob, 0);
                    IMRecordset r = new IMRecordset(ICSMTbl.itblAntennaMob, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("NAME", IMRecordset.Operation.Like, cell.Value);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        recAnt.Id = r.GetI("ID");
                    }
                    r.Close();
                    r.Destroy();
                    if (recAnt.Id > 0)
                        stationList[index].objAntenna = IMObject.LoadFromDB(recAnt);
                }
                AutoFill(cell, grid);
            }
            else if ((cell.Key == "antHeight0")
                    || (cell.Key == "antHeight1")
                    || (cell.Key == "antHeight2")
                    || (cell.Key == "antHeight3")
                    || (cell.Key == "antHeight4")
                    || (cell.Key == "antHeight5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (objStations.Count > index)
                {
                    double height = ConvertType.ToDouble(cell, IM.NullD);
                    /*     if ((height != IM.NullD) && (height < 0))
                         {
                            MessageBox.Show("Висота антени над рівнем землі має бути більше 0", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            height = 0;
                         }*/
                    stationList[index].AntHeight = height;
                    objStations[index]["AGL"] = stationList[index].AntHeight;
                    cell.Value = (stationList[index].AntHeight != IM.NullD) ? stationList[index].AntHeight.ToString("F0") : "";
                }
            }
            else if ((cell.Key == "antAz0")
                 || (cell.Key == "antAz1")
                 || (cell.Key == "antAz2")
                 || (cell.Key == "antAz3")
                 || (cell.Key == "antAz4")
                 || (cell.Key == "antAz5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(5));
                if (objStations.Count > index)
                {
                    double angle = ConvertType.ToDouble(cell, IM.NullD);
                    if ((angle != IM.NullD) && ((angle > 360) || (angle < 0)))
                    {
                        MessageBox.Show("Азимут випромінювання антени має бути в межах від 0 до 360", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        angle = 0;
                    }

                    stationList[index].Azimuth = angle;
                    HelpFunction.LoadFromCellToDb(cell, objStations[index]);
                }
                cell.Value = (stationList[index].Azimuth != IM.NullD) ? stationList[index].Azimuth.ToString("F2") : "";
            }
            else if ((cell.Key == "antGain0")
                 || (cell.Key == "antGain1")
                 || (cell.Key == "antGain2")
                 || (cell.Key == "antGain3")
                 || (cell.Key == "antGain4")
                 || (cell.Key == "antGain5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                if (objStations.Count > index)
                {
                    stationList[index].AntCoef = ConvertType.ToDouble(cell, IM.NullD);
                    HelpFunction.LoadFromCellToDb(cell, objStations[index]);
                }
                cell.Value = (stationList[index].AntCoef != IM.NullD) ? stationList[index].AntCoef.ToString("F1") : "";
            }
            if ((cell.Key == "antDsWdth0")
              || (cell.Key == "antDsWdth1")
              || (cell.Key == "antDsWdth2")
              || (cell.Key == "antDsWdth3")
              || (cell.Key == "antDsWdth4")
              || (cell.Key == "antDsWdth5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (objStations.Count > index)
                {
                    stationList[index].AntDSWidth = ConvertType.ToDouble(cell, IM.NullD);
                    //HelpFunction.LoadFromCellToDb(cell, objStations[index]);
                }
                cell.Value = (stationList[index].AntDSWidth != IM.NullD) ? stationList[index].AntDSWidth.ToString("F1") : "";
            }
            else if ((cell.Key == "antAngle0")
                    || (cell.Key == "antAngle1")
                    || (cell.Key == "antAngle2")
                    || (cell.Key == "antAngle3")
                    || (cell.Key == "antAngle4")
                    || (cell.Key == "antAngle5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(8));
                if (objStations.Count > index)
                {
                    double angle = ConvertType.ToDouble(cell, IM.NullD);
                    if ((angle != IM.NullD) && (angle > 180 || angle < -180))
                    {
                        MessageBox.Show("Сумарний кут ДСА має бути в межах від -180 до 180", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        angle = 0;
                    }
                    stationList[index].AntAngle = angle;

                    if (stationList[index].AntAngle != IM.NullD)
                        objStations[index][cell.DBField] = -stationList[index].AntAngle;
                    //HelpFunction.LoadFromCellToDb(cell, );
                }
                cell.Value = (stationList[index].AntAngle != IM.NullD) ? stationList[index].AntAngle.ToString("F2") : "";
            }
            else if ((cell.Key == "fiderLoss0")
                 || (cell.Key == "fiderLoss1")
                 || (cell.Key == "fiderLoss2")
                 || (cell.Key == "fiderLoss3")
                 || (cell.Key == "fiderLoss4")
                 || (cell.Key == "fiderLoss5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(9));
                if (objStations.Count > index)
                {
                    double angle = ConvertType.ToDouble(cell, IM.NullD);
                    if (angle == IM.NullD || angle < 0)
                    {
                        MessageBox.Show("Затухання в фідрному тракті  має бути більше 0", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        angle = 0;
                    }
                    stationList[index].FiderZat = angle;
                    HelpFunction.LoadFromCellToDb(cell, objStations[index]);
                }
                cell.Value = (stationList[index].FiderZat != IM.NullD) ? stationList[index].FiderZat.ToString("F2") : "";
            }
            else if ((cell.Key == "filtType0")
              || (cell.Key == "filtType1")
              || (cell.Key == "filtType2")
              || (cell.Key == "filtType3")
              || (cell.Key == "filtType4")
              || (cell.Key == "filtType5"))
            {
                if (string.IsNullOrEmpty(cell.Value))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(8));
                    if (objStations.Count > index)
                        stationList[index].fti.FilterIndex = IM.NullI;
                }
            }
            else if (cell.Key == "KStand")
            {
                standard = cell.Value;
            }
            else if ((cell.Key == "chan0")
                || (cell.Key == "chan1")
                || (cell.Key == "chan2")
                || (cell.Key == "chan3")
                || (cell.Key == "chan4")
                || (cell.Key == "chan5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(4));
                if (objStations.Count > index)
                    stationList[index].strChannel = cell.Value;
            }
            else if ((cell.Key == "rxFreq0")
                || (cell.Key == "rxFreq1")
                || (cell.Key == "rxFreq2")
                || (cell.Key == "rxFreq3")
                || (cell.Key == "rxFreq4")
                || (cell.Key == "rxFreq5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(6));
                if (objStations.Count > index)
                    stationList[index].strFreqRX = cell.Value;
            }
            else if ((cell.Key == "txFreq0")
                || (cell.Key == "txFreq1")
                || (cell.Key == "txFreq2")
                || (cell.Key == "txFreq3")
                || (cell.Key == "txFreq4")
                || (cell.Key == "txFreq5"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(6));
                if (objStations.Count > index)
                    stationList[index].strFreqTX = cell.Value;
            }
        }

        //============================================================
        public void LookupEquipment(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Підбір обладнання", ICSMTbl.itblEquipPmr, "NAME", initialValue, true);
            recEquip.Table = ICSMTbl.itblEquipPmr;  //Подмена таблицы
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                stationList[0].objEquip = IMObject.LoadFromDB(recEquip);
                string desigEmission = stationList[0].objEquip.GetS("DESIG_EMISSION");
                if (!string.IsNullOrEmpty(desigEmission))
                    objStations[0]["DESIG_EMISSION"] = desigEmission;
                cell.Value = stationList[0].objEquip.GetS("NAME");
                CellValidate(cell, grid);
                AutoFill(cell, grid);
            }
        }
        //============================================================
        public void LookupAntenna(Cell cell, Grid grid, string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cell.Value) + "*\"}";
            // Выбераем запись из таблицы
            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntennaMob, param);
            if ((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI))
            {
                int index = Convert.ToInt32(cell.Key.Substring(7));
                stationList[index].objAntenna = IMObject.LoadFromDB(RecAnt);
                cell.Value = stationList[index].objAntenna.GetS("NAME");
                CellValidate(cell, grid);
            }
        }
        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            if (cell.Key == "type")
            {// Выбор оборудования
                LookupEquipment(cell, grid, cell.Value, true);
            }
            else if (cell.Key == "asl")
            {
                double lon = stationList[0].Lon;
                double lat = stationList[0].Lat;

                if (lon != IM.NullD && lat != IM.NullD)
                {
                    double asl = IMCalculate.CalcALS(lon, lat, "4DMS");
                    if (asl != IM.NullD)
                    {
                        cell.Value = IM.RoundDeci(asl, 1).ToString();
                        stationList[0].Height = IM.RoundDeci(asl, 1);
                        OnCellValidate(cell, grid);
                    }
                }
            }
            else if (cell.Key == "addr")
            {                
                if (ShowMessageReference(stationList[0].objPosition != null))
                {
                    PositionState2 newPos = new PositionState2();
                    newPos.LongDms =  stationList[0].Lon;
                    newPos.LatDms =  stationList[0].Lat;
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionWim, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {
                        stationList[0].objPosition = newPos;
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon0"), gridParam);
                    }
                }
            }
            else if ((cell.Key == "antType0")
                  || (cell.Key == "antType1")
                  || (cell.Key == "antType2")
                  || (cell.Key == "antType3")
                  || (cell.Key == "antType4")
                  || (cell.Key == "antType5"))
            {// Выбор антены
                LookupAntenna(cell, grid, cell.Value, true);
            }
            else if ((cell.Key == "filtType0")
                  || (cell.Key == "filtType1")
                  || (cell.Key == "filtType2")
                  || (cell.Key == "filtType3")
                  || (cell.Key == "filtType4")
                  || (cell.Key == "filtType5"))
            {
                string param = "{NAME=\"*" + cell.Value + "*\"}";
                // Выбераем запись из таблицы
                RecordPtr RecFlt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach for filter"), PlugTbl._itblXnrfaExternFilter, param);
                if ((RecFlt.Id > 0) && (RecFlt.Id < IM.NullI))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(8));
                    stationList[index].fti.FilterIndex = RecFlt.Id;
                    stationList[index].fti.FilterName = "";
                    IMRecordset r = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, RecFlt.Id);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        stationList[index].fti.FilterName = r.GetS("NAME");
                    }
                    r.Close();
                    r.Destroy();

                    cell.Value = stationList[index].fti.FilterName;
                    CellValidate(cell, grid);
                }
            }
            else if (cell.Key == "KLon0")
            {                
                CellValidate(cell, grid);
                PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionWim, 1, stationList[0].Lon, stationList[0].Lat);
                if (newPos != null)
                {
                    stationList[0].objPosition = newPos;
                    UpdateOneParamGrid(grid.GetCellFromKey("KLon0"), grid);
                }
            }
            else if ((cell.Key == "obj1")
                 || (cell.Key == "obj2")
                 || (cell.Key == "obj3")
                 || (cell.Key == "obj4")
                 || (cell.Key == "obj5")
                 || (cell.Key == "obj0"))
            {
                int index = Convert.ToInt32(cell.Key.Substring(3));
                int ID = objStations[index].GetI("ID");
                RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation, ID);
                recStation.UserEdit();
            }
            else if ((cell.Key == "chan0")
                 || (cell.Key == "chan1")
                 || (cell.Key == "chan2")
                 || (cell.Key == "chan3")
                 || (cell.Key == "chan4")
                 || (cell.Key == "chan5"))
            {
                int ownerId = objStation.GetI("OWNER_ID");
                if (ownerId == IM.NullI)
                    throw new Exception("Не вибрано заявника!");

                if (FLicParams.Config() == DialogResult.OK)
                {
                    CLicence.Band[] rxBands = {};// = GetRxTxBands(RxTx.Rx);
                    CLicence.Band[] txBands = {};// = GetRxTxBands(RxTx.Tx);

                    //CLicence.Band[] rxBands = GetRxTxBands(RxTx.Rx, GetProvince());
                    //CLicence.Band[] txBands = GetRxTxBands(RxTx.Tx, GetProvince());

                    LicParams licParams = LicParams.GetLicParams();

                    Dictionary<int, CLicence> lics = CLicence.FindLicences(ownerId, GetProvince(), ref txBands, ref rxBands, Standard, standard);

                    if (lics.Count == 0)
                        MessageBox.Show(CLocaliz.TxT("No licences found"), CLocaliz.TxT("Info"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                    {
                        List<int> selectedLicences = new List<int>();
                        string licIdList = "";
                        
                        FProvincesList fLicList = new FProvincesList();  // actually, this is checklist of any objects
                        fLicList.Text = CLocaliz.TxT("Select licences");
                        foreach (CLicence lic in lics.Values)
                            fLicList.chlbProvs.Items.Add(lic, true);
                        if (fLicList.ShowDialog() == DialogResult.OK)
                        {
                            foreach (object obj in fLicList.chlbProvs.CheckedItems)
                                selectedLicences.Add(((CLicence)obj).Id);

                           // CheckProlongatedLicences(ref selectedLicences);

                            foreach (int licId in selectedLicences)
                                licIdList += (licIdList.Length > 0 ? "," : "") + licId.ToString();
                        }

                        if (licIdList.Length > 0)
                        {
                            //   throw new Exception(CLocaliz.TxT("No licence selected"));

                            List<int> vectChannels = new List<int>();

                            IMRecordset rsChal = new IMRecordset(ICSMTbl.ChAllotments, IMRecordset.Mode.ReadOnly);
                            try
                            {
                                rsChal.Select("ID,LIC_ID,Licence.NAME,TYPE,CHANNELS,PLAN_ID,Plan.NAME");
                                string additional = CLicence.GetChalWhereClause(ownerId, GetProvince(), radioTech, standard, licParams.ActualDate, licParams.UseLicStatuses);
                                additional += " and LIC_ID in (" + licIdList + ")";
                                rsChal.SetAdditional(additional);
                                rsChal.OrderBy("Licence.START_DATE", OrderDirection.Ascending);
                                for (rsChal.Open(); !rsChal.IsEOF(); rsChal.MoveNext())
                                {
                                    int allId = rsChal.GetI("ID");

                                    // following I leave almost unchanged - no time for refactoring

                                    List<int> vectTmpChannel = ConvertType.ToChanNumbList(rsChal.GetS("CHANNELS"));

                                    foreach (int iter_vectTmpChannel in vectTmpChannel)
                                    {
                                        bool needAdd = true;
                                        if (vectChannels.Contains(iter_vectTmpChannel))
                                        {
                                            needAdd = false;
                                            continue;
                                        }
                                        if (needAdd)// Добавляем
                                        {
                                            if (ChangeFindChannel())
                                            {// Проверка канала
                                                IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                                r2.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                                r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, rsChal.GetI("PLAN_ID"));
                                                r2.SetWhere("CHANNEL", IMRecordset.Operation.Eq, iter_vectTmpChannel);
                                                r2.SetWhere("PARITY", IMRecordset.Operation.Like, "D");
                                                try
                                                {
                                                    r2.Open();
                                                    if (!rsChal.IsEOF())
                                                    {
                                                        IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                                        r3.Select("PLAN_ID,PARITY,CHANNEL");
                                                        r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, rsChal.GetI("PLAN_ID"));
                                                        r3.SetWhere("CHANNEL", IMRecordset.Operation.Eq, iter_vectTmpChannel + 950);
                                                        r3.SetWhere("PARITY", IMRecordset.Operation.Like, "U");
                                                        try
                                                        {
                                                            r3.Open();
                                                            if (!r3.IsEOF())
                                                            {
                                                                vectChannels.Add(iter_vectTmpChannel);
                                                            }
                                                            else
                                                                MessageBox.Show("Не можу знайти канал №" + (iter_vectTmpChannel + 950).ToString() +
                                                                    " паритета \"U\" для частотного плана \"" + rsChal.GetS("Plan.NAME") + "\"");
                                                        }
                                                        finally
                                                        {
                                                            r3.Close();
                                                            r3.Destroy();
                                                        }
                                                    }
                                                }
                                                finally
                                                {
                                                    r2.Close();
                                                    r2.Destroy();
                                                }
                                            }
                                            else
                                                vectChannels.Add(iter_vectTmpChannel);
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                rsChal.Destroy();
                            }

                            vectChannels.Sort();
                            int index = Convert.ToInt32(cell.Key.Substring(4));
                            int numChannel = index;
                            FChannels FChannel = new FChannels();
                            FChannel.Text = CLocaliz.TxT("Select channels");
                            List<int> vectChannel = msvSectors[numChannel];
                            foreach (int iter_findChannel in vectChannels)
                            {
                                string strChannel;
                                if (ChangeFindChannel())
                                    strChannel = iter_findChannel.ToString() + " / " + (iter_findChannel + 950).ToString();
                                else
                                    strChannel = iter_findChannel.ToString();
                                if (!vectChannel.Contains(ConvertType.ToInt32(strChannel, IM.NullI)))
                                    FChannel.CLBAllChannel.Items.Add(strChannel);
                            }

                            foreach (int vecIter in vectChannel)
                                FChannel.CLBSelectChannel.Items.Add(ChangeFindChannel() ? vecIter.ToString() + " / " + (vecIter + 950).ToString() : vecIter.ToString());

                            if (FChannel.ShowDialog() == DialogResult.OK)
                            {// были изменения
                                vectChannel.Clear();
                                for (int i = 0; i < FChannel.CLBSelectChannel.Items.Count; i++)
                                {
                                    string[] tmpChannel = FChannel.CLBSelectChannel.Items[i].ToString().Split();
                                    int nCannel = 0;
                                    if (tmpChannel.Length > 0)
                                        nCannel = Convert.ToInt32((string.IsNullOrEmpty(tmpChannel[0]) == false) ? tmpChannel[0] : "0");
                                    if (nCannel >= 0) vectChannel.Add(nCannel);
                                }
                                vectChannel.Sort();
                                if (ChangeFindChannel())
                                    UpdateCellChannelRr2_UMTS(numChannel, grid);
                                else
                                {
                                    string RxParity = "D";
                                    string TxParity = "U";
                                    if (Standard == CRadioTech.DECT)
                                    {
                                        RxParity = "N";
                                        TxParity = "N";
                                    }
                                    UpdateCellChannelRr2(numChannel, RxParity, TxParity, grid);
                                }

                                // Обновляем лицензии
                                List<int> vectLicence = new List<int>();
                                licenceListId.Clear();
                                int it = 0;
                                for (it = 0; it < stationList.Count; it++)
                                {
                                    if (it >= msvSectors.Count) //Заглушка на всякий случай
                                        break;
                                    List<int> vectChannel2 = msvSectors[it];

                                    IMRecordset rsChal2 = new IMRecordset(ICSMTbl.ChAllotments, IMRecordset.Mode.ReadOnly);
                                    try
                                    {
                                        rsChal2.Select("ID,LIC_ID,CHANNELS");
                                        string additional = CLicence.GetChalWhereClause(ownerId, GetProvince(), radioTech, standard, licParams.ActualDate, licParams.UseLicStatuses);
                                        additional += " and LIC_ID in (" + licIdList + ")";
                                        rsChal2.SetAdditional(additional);
                                        rsChal2.OrderBy("Licence.START_DATE", OrderDirection.Ascending);
                                        for (rsChal2.Open(); !rsChal2.IsEOF(); rsChal2.MoveNext())
                                        {
                                            List<int> vectTmpChannel = ConvertType.ToChanNumbList(rsChal2.GetS("CHANNELS"));
                                            foreach (int iter_vectTmpChannel in vectTmpChannel)
                                            {
                                                bool blExit = false;
                                                foreach (int vecIter in vectChannel)
                                                    if (vecIter == iter_vectTmpChannel)
                                                    {
                                                        blExit = true; //Нет смысла дальше проверять каналы, так как лицензия уже добавлена
                                                        if (!licenceListId.Contains(rsChal2.GetI("LIC_ID")))
                                                            licenceListId.Add(rsChal2.GetI("LIC_ID"));
                                                        break;
                                                    }
                                                if (blExit)
                                                    break;
                                            }
                                        }
                                    }
                                    finally
                                    {
                                        rsChal2.Destroy();
                                    }
                                }
                                licenceListId.Sort();
                                CheckProlongatedLicences(ref licenceListId);


                                ShowDetailDozv();
                                InitLicenceGrid();
                                CheckTechUser();
                                IsChangeDop = true;
                            }
                        }
                    }
                }
            }
            else
            {
                base.OnPressButton(cell, grid);
            }
        }

        public override double GetBw()
        {
            return stationList[0].objEquip != null ? (stationList[0].objEquip.GetD("BW")) / 1000.0 : 0.0;
        }

        public override string GetProvince()
        {
            return stationList[0].objPosition != null ? (!string.IsNullOrEmpty(stationList[0].objPosition.Province) ? stationList[0].objPosition.Province : stationList[0].objPosition.City) : null;
        }

        internal override CLicence.Band[] GetRxTxBands(RxTx type, string region)
        {
            // region parameter will be ignored
            double BW = GetBw();
            List<CLicence.Band> bandList = new List<CLicence.Band>();
            int bandId = 0;
            for (int i = 0; i < sectorIDs.Count; i++)
            {
                IMRecordset freqRs = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                try
                {
                    freqRs.Select("TX_FREQ,RX_FREQ");
                    freqRs.SetWhere("STA_ID", IMRecordset.Operation.Eq, sectorIDs[i].Id);
                    for (freqRs.Open(); !freqRs.IsEOF(); freqRs.MoveNext())
                    {
                        double TxFreq = freqRs.GetD("TX_FREQ");
                        double RxFreq = freqRs.GetD("RX_FREQ");
                        if (type != RxTx.Rx && TxFreq != IM.NullD)
                            bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(TxFreq - BW / 2, 6), IM.RoundDeci(TxFreq, 6), IM.RoundDeci(TxFreq + BW / 2, 6), CLicence.Band.Parity.Up));
                        if (type != RxTx.Tx && RxFreq != IM.NullD)
                            bandList.Add(new CLicence.Band(++bandId, IM.RoundDeci(RxFreq - BW / 2, 6), IM.RoundDeci(RxFreq, 6), IM.RoundDeci(RxFreq + BW / 2, 6), CLicence.Band.Parity.Down));
                    }
                }
                finally
                {
                    freqRs.Destroy();
                }
            }
            return bandList.ToArray();
        }
        
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if ((cell.Key == "addr") )//&& (stationList[0].objPosition != null))
                {
                    if (stationList[0].objPosition != null)
                    {
                        Forms.AdminSiteAllTech2.Show(stationList[0].objPosition.TableName, stationList[0].objPosition.Id, (IM.TableRight(stationList[0].objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        // position may appear be  changed
                        //todo: refactor this in all forms to some general "ReloadPosition()" method in base class
                        stationList[0].objPosition.LoadStatePosition(stationList[0].objPosition.Id, stationList[0].objPosition.TableName);
                        UpdateOneParamGrid(gridParam.GetCellFromKey("KLon0"), gridParam);
                    }
                    else
                        ShowMessageNoReference();
                
                }
                else if ((cell.Key == "antType0")
                  || (cell.Key == "antType1")
                  || (cell.Key == "antType2")
                  || (cell.Key == "antType3")
                  || (cell.Key == "antType4")
                  || (cell.Key == "antType5"))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(7));
                    if (stationList[index].objAntenna != null)
                    {
                        RecordPtr rcPtr = new RecordPtr(stationList[index].objAntenna.GetS("TABLE_NAME"), stationList[index].objAntenna.GetI("ID"));
                        rcPtr.UserEdit();
                    }
                }
                else if ((cell.Key == "type") && (stationList[0].objEquip != null))
                {
                    RecordPtr rcPtr = new RecordPtr(stationList[0].objEquip.GetS("TABLE_NAME"), stationList[0].objEquip.GetI("ID"));
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "obj1")
             || (cell.Key == "obj2")
             || (cell.Key == "obj3")
             || (cell.Key == "obj4")
             || (cell.Key == "obj5")
             || (cell.Key == "obj0"))
                {
                    int index = Convert.ToInt32(cell.Key.Substring(3));
                    int ID = objStations[index].GetI("ID");
                    RecordPtr recStation = new RecordPtr(ICSMTbl.itblMobStation, ID);
                    recStation.UserEdit();
                }
            }
            catch (IMException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

        }

        //===================================================
        /// <summary>
        /// Внутрення процедура добавления колонки для сектора.
        ///  устанавливает стили слетки.
        /// </summary>
        /// <returns>XML строка</returns>
        private Cell ExtendGrid(Grid grid, int columnCount, string KeyPrefix, string value, int tab)
        {
            Row r = grid.GetCellFromKey(KeyPrefix + "0").row;
            Cell newCell = r.AddCell("");
            newCell.Key = KeyPrefix + columnCount.ToString();
            newCell.FontName = grid.GetCellFromKey(KeyPrefix + "0").FontName;
            newCell.TextHeight = grid.GetCellFromKey(KeyPrefix + "0").TextHeight;
            newCell.FontStyle = grid.GetCellFromKey(KeyPrefix + "0").FontStyle;
            newCell.HorizontalAlignment = grid.GetCellFromKey(KeyPrefix + "0").HorizontalAlignment;
            newCell.VerticalAlignment = grid.GetCellFromKey(KeyPrefix + "0").VerticalAlignment;
            newCell.DBField = grid.GetCellFromKey(KeyPrefix + "0").DBField;
            newCell.TextColor = grid.GetCellFromKey(KeyPrefix + "0").TextColor;
            newCell.CanEdit = grid.GetCellFromKey(KeyPrefix + "0").CanEdit;
            newCell.BackColor = grid.GetCellFromKey(KeyPrefix + "0").BackColor;
            newCell.Type = grid.GetCellFromKey(KeyPrefix + "0").Type;
            newCell.Value = value;
            newCell.TabOrder = tab;
            InitParamGrid(newCell);

            return newCell;
        }
        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRR formEms = new FormVRR(recordID.Table, recordID.Id);
            formEms.ShowDialog();
            formEms.Dispose();
        }
        //===================================================
        /// <summary>
        /// Изменить алгоритм поиска каналов
        /// </summary>
        /// <returns>yes / no</returns>
        bool ChangeFindChannel()
        {
            string strFreqPlan = standard;
            if (strFreqPlan.Contains("UMTS"))
                return true;
            return false;
        }

        //===================================================
        /// <summary>
        /// Обновить ячейку каналов в заявке PP2 для UMTS
        /// </summary>
        /// <param name="numChannel">Номер сектора</param>
        void UpdateCellChannelRr2_UMTS(int numChannel, Grid grid)
        {
            if (numChannel < 0 || numChannel >= 6)
                return;
            // Вытаскиваем номер "плана"
            string strRadioTech = standard;
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            int IDFreqPlan = IM.NullI;
            r.Select("ID,NAME");
            r.SetWhere("NAME", IMRecordset.Operation.Like, strRadioTech);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    IDFreqPlan = r.GetI("ID");
                }
                else
                    return;
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            stationList[numChannel].TxFreqs.Clear();
            stationList[numChannel].RxFreqs.Clear();
            // Формируем список частот и каналов
            List<int> vectChannel = msvSectors[numChannel];
            string strChannel = "";
            string strTX = "";
            string strRX = "";
            // Создаем список сгрупированных каналов
            foreach (int vecIter in vectChannel)
            {
                // RX
                {
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r2.Select("PLAN_ID,PARITY,FREQ");
                    r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                    r2.SetWhere("PARITY", IMRecordset.Operation.Like, "D");
                    r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, vecIter.ToString());
                    try
                    {
                        r2.Open();
                        if (!r2.IsEOF())
                        {
                            if (strRX != "")
                                strRX += ";";
                            double freq = r2.GetD("FREQ");
                            strRX += freq.ToString("F3");
                            stationList[numChannel].RxFreqs.Add(freq);
                            // Каналы
                            if (strChannel != "")
                                strChannel += ";";
                            strChannel += vecIter.ToString() + "/" + (vecIter + 950).ToString();
                        }
                    }
                    finally
                    {
                        r2.Close();
                        r2.Destroy();
                    }
                }
                {
                    IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r3.Select("PLAN_ID,PARITY,FREQ");
                    r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                    r3.SetWhere("PARITY", IMRecordset.Operation.Like, "U");
                    r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, (vecIter + 950).ToString());
                    try
                    {
                        r3.Open();
                        if (!r3.IsEOF())
                        {
                            if (strTX != "")
                                strTX += ";";
                            double freq = r3.GetD("FREQ");
                            strTX += freq.ToString("F3");
                            stationList[numChannel].TxFreqs.Add(freq);
                        }
                    }
                    finally
                    {
                        r3.Close();
                        r3.Destroy();
                    }
                }
            }
            // Заполняем поля
            string nameFieldChannel = "chan";
            string nameFieldRX = "rxFreq";
            string nameFieldTX = "txFreq";
            if (numChannel >= 0)
            {// Формируем название разширеных полей
                nameFieldChannel += (numChannel).ToString();
                nameFieldRX += (numChannel).ToString();
                nameFieldTX += (numChannel).ToString();
            }

            Cell cell = grid.GetCellFromKey(nameFieldChannel);
            if (cell != null)
            {
                cell.Value = strChannel;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldRX);
            if (cell != null)
            {
                cell.Value = strRX;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldTX);
            if (cell != null)
            {
                cell.Value = strTX;
                CellValidate(cell, grid);
            }
        }


        //===================================================
        /// <summary>
        /// Загрузить значение каналов для всех секторов из таблицы 
        /// </summary>
        /// <param name="toChannel">кол-во каналов (должно быть >= нулю) Если == 0, то все сектора будут удаленны</param>
        /// <returns>TRUE  - Все ОК, FALSE - Ошибка</returns>
       public bool LoadChannel(int toChannel)
        {
            for (int i = 0; i < toChannel; i++)
            {
                List<double> freqs = new List<double>();
                {
                    int stationId = objStations[i].GetI("ID");
                    MobFreq[] freqStation = MobFreqManager.LoadFreq(stationId, MobFreqManager.TypeFreq.MobStation);
                    foreach (MobFreq freqItem in freqStation)
                    {
                        double txf = freqItem.TxMHz;
                        double rxf = freqItem.RxMHz;
                        if (txf != IM.NullD && txf > 0 && !freqs.Contains(txf))
                            freqs.Add(txf);
                        if (rxf != IM.NullD && rxf > 0 && !freqs.Contains(rxf))
                            freqs.Add(rxf);
                    }
                }

                List<int> chans = new List<int>();
                if (freqs.Count > 0)
                {
                    int planId = objStations[i].GetI("PLAN_ID");

                    if (planId != IM.NullI)
                    {
                        IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                        r3.Select("FREQ,PLAN_ID,CHANNEL");
                        r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, planId);
                        try
                        {
                            for (r3.Open(); !r3.IsEOF(); r3.MoveNext())
                            {
                                int ch = ConvertType.ToInt32(r3.GetS("CHANNEL"), IM.NullI);
                                double f = r3.GetD("FREQ");
                                if (freqs.Contains(f) && ch != IM.NullI && ch >= 0 && !chans.Contains(ch))
                                    chans.Add(ch);
                            }
                        }
                        finally
                        {
                            r3.Close();
                            r3.Destroy();
                        }
                    }
                    chans.Sort();
                }
                msvSectors.Add(chans); //Добавляем сектор
            }
            if (msvSectors.Count > 0)
                return true;
            else
            {
                msvSectors.Add(new List<int>());
                return false;
            }
        }

        //===================================================
        /// <summary>
        /// Копирует общие параметры из первой станции в остальные
        /// </summary>
        /// <param name="index">номер сектора для заполнения парметрами и сохранения</param>
        void SaveParams(int index)
        {
            objStations[index].Put("STATUS", Status.ToStr()); //Сохраняем статус
            objStations[index].Put("CUST_CHB1", IsTechnicalUser);
            if (index != 0)
            {
                objStations[index]["NAME"] = stationList[0].OznRez;
                //???objStations[index]["DESIG_EMISSION"] = stationList[0].EmiClass;
                // #3388 - задача про запис кількості обладняння лише в 1-й сектор
                //objStations[index]["EQUIP_NBR"] = stationList.Count;
                // #3979 - тепер записую передатчики у всі сектора (конфліктує з #3388)
                objStations[index]["EQUIP_NBR"] = stationList.Count;
                objStations[index]["STANDARD"] = radioTech;
                objStations[index]["DESIG_EMISSION"] = objStations[0].GetS("DESIG_EMISSION");
                // #3388 - задача про запис кількості обладняння лише в 1-й сектор
                //objStations[index].Put("EQUIP_NBR", stationList[0].Count);
                // #3979 - тепер записую передатчики у всі сектора (конфліктує з #3388)
                objStations[index].Put("EQUIP_NBR", stationList[0].Count);
                //            objStations[index].Put("CUST_CHB1", IsTechnicalUser);
            }
            if (stationList[0].objPosition != null)
            {
                //if (stationList[0].objPosition != null)
                //    stationList[0].objPosition.Save();
                if (stationList[0].objPosition != null)
                    objStations[index]["POS_ID"] = stationList[0].objPosition.Id;
            }
            if (stationList[0].objEquip != null)
                objStations[index]["EQUIP_ID"] = stationList[0].objEquip.GetI("ID");

            if (stationList[index].objAntenna != null)
            {
                int id = stationList[index].objAntenna.GetI("ID");
                objStations[index]["ANT_ID"] = stationList[index].objAntenna.GetI("ID");
                objStations[index]["T_GAIN_TYPE"] = stationList[index].objAntenna.GetS("GAIN_TYPE");
                if ((stationList[index].objAntenna.GetS("GAIN_TYPE") == "I") && (stationList[index].MaxPower > 0))
                    objStations[index]["POWER"] = 10.0 * Math.Log10(stationList[index].MaxPower) + objStations[index].GetD("GAIN") -
                                                                     objStations[index].GetD("TX_LOSSES") -
                                                                     objStations[index].GetD("TX_ADDLOSSES");
                objStations[index]["POLAR"] = stationList[index].objAntenna.GetS("POLARIZATION");
            }

            // Сохраняем "Особливі умови"
            objStations[index]["CUST_TXT16"] = SCDozvil;
            objStations[index]["CUST_TXT17"] = SCVisnovok;
            objStations[index]["CUST_TXT18"] = SCNote;
            // План
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            int IDFreqPlan = IM.NullI;
            r.Select("ID,NAME,DUPLEX_SPACING");
            r.SetWhere("NAME", IMRecordset.Operation.Like, standard);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    objStations[index]["DUPLEX_SPACING"] = r.GetD("DUPLEX_SPACING");
                    IDFreqPlan = r.GetI("ID");
                    objStations[index]["PLAN_ID"] = IDFreqPlan;
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            if (newStation || (objStations[index].GetS("CUST_TXT1") != NumberOut || objStations[index].GetT("CUST_DAT1") != DateOut))
            {
                objStations[index].Put("CUST_TXT1", NumberOut);
                objStations[index].Put("CUST_DAT1", DateOut);
            }


            CJournal.CheckTable(ICSMTbl.itblMobStation, objStations[index].GetI("ID"), objStations[index]);
            objStations[index].SaveToDB();

            //-------------------------------------
            //Сохраняем фильтры
            {
                string FilterFields = "ID,OBJ_ID,OBJ_TABLE,EX_FILTER_ID,IS_TX";
                IMRecordset rsFilter = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadWrite);
                rsFilter.Select(FilterFields);
                rsFilter.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, objStations[index].GetI("ID"));
                rsFilter.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, recordID.Table);
                try
                {
                    rsFilter.Open();
                    if (!rsFilter.IsEOF())
                    {
                        if ((stationList[index].fti.FilterIndex != 0) && (stationList[index].fti.FilterIndex != IM.NullI))
                        {
                            rsFilter.Edit();
                            rsFilter["EX_FILTER_ID"] = stationList[index].fti.FilterIndex;
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), rsFilter, FilterFields);
                            rsFilter.Update();
                        }
                        else
                        {
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), null, FilterFields);
                            rsFilter.Delete();
                        }
                    }
                    else
                    {
                        if ((stationList[index].fti.FilterIndex != 0) || (stationList[index].fti.FilterIndex != IM.NullI))
                        {
                            rsFilter.AddNew();
                            rsFilter["ID"] = IM.AllocID(PlugTbl._itblXnrfaStatExfltr, 1, -1);
                            rsFilter["OBJ_ID"] = objStations[index].GetI("ID");
                            rsFilter["OBJ_TABLE"] = recordID.Table;
                            rsFilter["EX_FILTER_ID"] = stationList[index].fti.FilterIndex;
                            if ((radioTech.Contains("GSM")) || (radioTech.Contains("UMTS")))
                                rsFilter["IS_TX"] = 0; //для GSM и UMTS станций фильтер всегда на прием
                            else
                                rsFilter["IS_TX"] = 1; //Все остальные на передачу
                            //Fix for #2001
                            CJournal.CheckUcrfTable(PlugTbl._itblXnrfaStatExfltr, rsFilter.GetI("ID"), rsFilter, FilterFields);
                            rsFilter.Update();
                        }
                    }
                }
                finally
                {
                    rsFilter.Close();
                    rsFilter.Destroy();
                }
            }
            {
                stationList[index].RxFreqs.Sort();
                stationList[index].TxFreqs.Sort();
                List<MobFreq> freqs = new List<MobFreq>();
                int countFreq = Math.Max(stationList[index].TxFreqs.Count, stationList[index].RxFreqs.Count);
                for (int i = 0; i < countFreq; i++)
                {
                    MobFreq newFreq = new MobFreq();
                    if (i < stationList[index].TxFreqs.Count)
                        newFreq.TxMHz = stationList[index].TxFreqs[i];
                    if (i < stationList[index].RxFreqs.Count)
                        newFreq.RxMHz = stationList[index].RxFreqs[i];
                    newFreq.PlanId = IDFreqPlan;
                    freqs.Add(newFreq);
                }
                MobFreqManager.SaveFreq(objStations[index].GetI("ID"), MobFreqManager.TypeFreq.MobStation, freqs.ToArray());
            }
        }

        private class ChannelInfo
        {
            public int ChannelNo { get; set; }
            public double RxFrequency { get; set; }
            public double TxFrequency { get; set; }
        }

        void UpdateCellChannelRr2Boosted(int numChannel, string RxParity, string TxParity, Grid grid)
        {
            if (numChannel < 0 || numChannel >= 6)
                return;
            // Вытаскиваем номер "плана"
            string strRadioTech = standard;
            
            List<List<ChannelInfo>> listGroupChannel = new List<List<ChannelInfo>>();   //массив секторов
            int LastVal = 0;
            List<int> vectChannel = msvSectors[numChannel];
            stationList[numChannel].TxFreqs.Clear();
            stationList[numChannel].RxFreqs.Clear();

            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            int IDFreqPlan = IM.NullI;
            r.Select("ID,NAME");
            r.SetWhere("NAME", IMRecordset.Operation.Like, strRadioTech);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    IDFreqPlan = r.GetI("ID");
                }
                else
                    return;
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            Dictionary<int, ChannelInfo> channelMap = new Dictionary<int, ChannelInfo>();

            IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
            r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
            r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
            
            for (r3.Open(); !r3.IsEOF(); r3.MoveNext())
            {
                int currentChannel = r3.GetS("CHANNEL").ToInt32(IM.NullI);
                string currentParity = r3.GetS("PARITY");
                double currentFrequency = r3.GetD("FREQ");

                if (!channelMap.ContainsKey(currentChannel))
                {
                    ChannelInfo channelInfo = new ChannelInfo();
                    channelInfo.ChannelNo = currentChannel;

                    channelMap.Add(currentChannel, channelInfo);
                }

                if (channelMap.ContainsKey(currentChannel))
                {
                    if (currentParity == TxParity)
                    {
                        channelMap[currentChannel].TxFrequency = currentFrequency;
                        if (vectChannel.Contains(currentChannel))
                            stationList[numChannel].TxFreqs.Add(currentFrequency);
                    }

                    if (currentParity == RxParity)
                    {
                        channelMap[currentChannel].RxFrequency = currentFrequency;

                        if (vectChannel.Contains(currentChannel))
                            stationList[numChannel].RxFreqs.Add(currentFrequency);
                    }
                }
                else
                {
                    throw new Exception("----");
                }
            }
            r3.Final();


            foreach (int vecIter in vectChannel)
            {
                ChannelInfo channelFromVector = channelMap[vecIter];

                int currentChannel = channelFromVector.ChannelNo;
                if (currentChannel == vectChannel[0])
                {
                    // Первый элемент
                    LastVal = currentChannel;
                    
                    List<ChannelInfo> groupChannel = new List<ChannelInfo>();
                    groupChannel.Add(channelFromVector);
                    listGroupChannel.Add(groupChannel);                                            
                }
                else
                {
                    // Не первый элемент
                    if ((LastVal + 1) == currentChannel)
                    {
                        List<ChannelInfo>  groupChannel = listGroupChannel[listGroupChannel.Count - 1];                            
                        groupChannel.Add(channelFromVector);                                                    
                    }
                    else
                    {                        
                        List<ChannelInfo> groupChannel = new List<ChannelInfo>();
                        
                        groupChannel.Add(channelFromVector);
                        listGroupChannel.Add(groupChannel);                                                    
                    }
                    LastVal = currentChannel;
                }
            }

            
            // Формируем список частот
            string strChannel = "";
            string strTX = "";
            string strRX = "";

            foreach (List<ChannelInfo> channelInfoOldList in listGroupChannel)
            {
                ChannelInfo[] channelInfoList = channelInfoOldList.ToArray();
                //List<Channel> vectGroup = vecDrpsIter;
                switch (channelInfoList.Length)
                {
                    case 0://Ничего не делаем
                        break;
                    case 1://Одна запись
                        {
                            strChannel += channelInfoList[channelInfoList.Length - 1].ChannelNo.ToString();
                            strTX += channelInfoList[channelInfoList.Length - 1].TxFrequency.Round(3).ToString("F3");
                            strRX += channelInfoList[channelInfoList.Length - 1].RxFrequency.Round(3).ToString("F3");                            
                            break;
                        }
                    case 2://Две запись
                        {
                            strChannel += channelInfoList[0].ChannelNo + ";";
                            strChannel += channelInfoList[channelInfoList.Length - 1].ChannelNo.ToString();

                            strTX += channelInfoList[0].TxFrequency.Round(3).ToString("F3");
                            strRX += channelInfoList[0].RxFrequency.Round(3).ToString("F3");

                            strTX += ";";
                            strRX += ";";

                            strTX += channelInfoList[channelInfoList.Length - 1].TxFrequency.Round(3).ToString("F3");
                            strRX += channelInfoList[channelInfoList.Length - 1].RxFrequency.Round(3).ToString("F3");
                            
                            break;
                        }
                    default://Много записей

                        strChannel += channelInfoList[0].ChannelNo + "-";
                        strChannel += channelInfoList[channelInfoList.Length - 1].ChannelNo.ToString();

                        strTX += channelInfoList[0].TxFrequency.Round(3).ToString("F3");
                        strRX += channelInfoList[0].RxFrequency.Round(3).ToString("F3");

                        strTX += "-";
                        strRX += "-";

                        strTX += channelInfoList[channelInfoList.Length - 1].TxFrequency.Round(3).ToString("F3");
                        strRX += channelInfoList[channelInfoList.Length - 1].RxFrequency.Round(3).ToString("F3");
                        
                        break;
                }
                if (channelInfoOldList != listGroupChannel.Last())
                {
                    strChannel += ";";
                    strTX += ";";
                    strRX += ";";
                }
            }

            // Заполняем поля
            string nameFieldChannel = "chan";
            string nameFieldRX = "rxFreq";
            string nameFieldTX = "txFreq";
            if (numChannel >= 0)
            {// Формируем название разширеных полей
                nameFieldChannel += numChannel.ToString();
                nameFieldRX += numChannel.ToString();
                nameFieldTX += numChannel.ToString();
            }

            Cell cell = grid.GetCellFromKey(nameFieldChannel);
            if (cell != null)
            {
                cell.Value = strChannel;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldRX);
            if (cell != null)
            {
                cell.Value = strRX;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldTX);
            if (cell != null)
            {
                cell.Value = strTX;
                CellValidate(cell, grid);
            }            
        }
        //===================================================
        /// <summary>
        /// Обновить ячейку каналов в заявке PP2 
        /// </summary>
        /// <param name="numChannel">Номер сектора</param>
        void UpdateCellChannelRr2(int numChannel, string RxParity, string TxParity, Grid grid)
        {
            if (numChannel < 0 || numChannel >= 6)
                return;
            // Вытаскиваем номер "плана"
            string strRadioTech = standard;
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            int IDFreqPlan = IM.NullI;
            r.Select("ID,NAME");
            r.SetWhere("NAME", IMRecordset.Operation.Like, strRadioTech);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    IDFreqPlan = r.GetI("ID");
                }
                else
                    return;
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            List<List<int>> listGroupChannel = new List<List<int>>();   //массив секторов
            int LastVal = 0;
            List<int> vectChannel = msvSectors[numChannel];
            stationList[numChannel].TxFreqs.Clear();
            stationList[numChannel].RxFreqs.Clear();
            // Создаем список сгрупированных каналов
            foreach (int vecIter in vectChannel)
            {
                if (vecIter == vectChannel[0])
                {// Первый элемент
                    LastVal = vecIter;
                    List<int> groupChannel = new List<int>();
                    groupChannel.Add(LastVal);
                    listGroupChannel.Add(groupChannel);
                }
                else
                {// Не первый элемент
                    if ((LastVal + 1) == vecIter)
                    {
                        List<int> groupChannel = listGroupChannel[listGroupChannel.Count - 1];
                        groupChannel.Add(vecIter);
                    }
                    else
                    {
                        List<int> groupChannel = new List<int>();
                        groupChannel.Add(vecIter);
                        listGroupChannel.Add(groupChannel);
                    }
                    LastVal = vecIter;
                }
                // Обновляем частоты
                string strChannel1 = vecIter.ToString();
                {
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r2.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                    r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                    r2.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                    r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                    try
                    {                        
                        r2.Open();
                        if (!r2.IsEOF())
                            stationList[numChannel].TxFreqs.Add(r2.GetD("FREQ"));
                    }
                    finally
                    {
                        r2.Close();
                        r2.Destroy();
                    }
                }
                {
                    IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                    r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                    r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                    r3.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                    r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                    try
                    {
                        r3.Open();
                        if (!r3.IsEOF())
                            stationList[numChannel].RxFreqs.Add(r3.GetD("FREQ"));
                    }
                    finally
                    {
                        r3.Close();
                        r3.Destroy();
                    }
                }
            }

            // Формируем список частот
            string strChannel = "";
            string strTX = "";
            string strRX = "";

            foreach (List<int> vecDrpsIter in listGroupChannel)
            {
                List<int> vectGroup = vecDrpsIter;
                switch (vectGroup.Count)
                {
                    case 0://Ничего не делаем
                        break;
                    case 1://Одна запись
                        {
                            strChannel += vectGroup[vectGroup.Count - 1].ToString();
                            string strChannel1 = vectGroup[vectGroup.Count - 1].ToString();
                            {
                                IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                r2.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r2.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                                r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                                try
                                {
                                    r2.Open();
                                    if (!r2.IsEOF())
                                    {
                                        strTX += r2.GetD("FREQ").ToString("F3");
                                    }
                                }
                                finally
                                {
                                    r2.Close();
                                    r2.Destroy();
                                }
                            }
                            {
                                IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r3.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                                r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                                try
                                {
                                    r3.Open();
                                    if (!r3.IsEOF())
                                    {
                                        strRX += r3.GetD("FREQ").ToString("F3");
                                    }
                                }
                                finally
                                {
                                    r3.Close();
                                    r3.Destroy();
                                }
                            }
                            break;
                        }
                    case 2://Две запись
                        {
                            strChannel += (vectGroup[0].ToString() + ";");
                            strChannel += vectGroup[vectGroup.Count - 1].ToString();
                            string strChannel1 = vectGroup[0].ToString();
                            string strChannel2 = vectGroup[vectGroup.Count - 1].ToString();
                            // TX
                            {
                                IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);

                                r2.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r2.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                                r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                                try
                                {
                                    r2.Open();
                                    if (!r2.IsEOF())
                                    {
                                        strTX += r2.GetD("FREQ").ToString("F3");
                                    }
                                }
                                finally
                                {
                                    r2.Close();
                                    r2.Destroy();
                                }
                            }
                            {
                                IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r3.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                                r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel1);
                                try
                                {
                                    r3.Open();
                                    if (!r3.IsEOF())
                                    {
                                        strRX += r3.GetD("FREQ").ToString("F3");
                                    }
                                }
                                finally
                                {
                                    r3.Close();
                                    r3.Destroy();
                                }
                            }
                            strTX += ";";
                            strRX += ";";
                            {
                                IMRecordset r4 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                r4.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r4.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r4.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                                r4.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel2);
                                try
                                {
                                    r4.Open();
                                    if (!r4.IsEOF())
                                    {
                                        strTX += r4.GetD("FREQ").ToString("F3");
                                    }
                                    else
                                        return;
                                }
                                finally
                                {
                                    r4.Close();
                                    r4.Destroy();
                                }
                            }
                            {
                                IMRecordset r5 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                                r5.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                                r5.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                                r5.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                                r5.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel2);
                                try
                                {
                                    r5.Open();
                                    if (!r5.IsEOF())
                                    {
                                        strRX += r5.GetD("FREQ").ToString("F3");
                                    }
                                }
                                finally
                                {
                                    r5.Close();
                                    r5.Destroy();
                                }
                            }
                            break;
                        }
                    default://Много записей
                        strChannel += (vectGroup[0].ToString() + "-");
                        strChannel += vectGroup[vectGroup.Count - 1].ToString();
                        string strChannel11 = vectGroup[0].ToString();
                        string strChannel22 = vectGroup[vectGroup.Count - 1].ToString();
                        // TX
                        {
                            IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);

                            r2.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                            r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                            r2.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                            r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel11);
                            try
                            {
                                r2.Open();
                                if (!r2.IsEOF())
                                {
                                    strTX += r2.GetD("FREQ").ToString("F3");
                                }
                            }
                            finally
                            {
                                r2.Close();
                                r2.Destroy();
                            }
                        }
                        {
                            IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                            r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                            r3.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                            r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel11);
                            try
                            {
                                r3.Open();
                                if (!r3.IsEOF())
                                {
                                    strRX += r3.GetD("FREQ").ToString("F3");
                                }
                            }
                            finally
                            {
                                r3.Close();
                                r3.Destroy();
                            }
                        }
                        strTX += "-";
                        strRX += "-";
                        {
                            IMRecordset r4 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r4.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                            r4.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                            r4.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
                            r4.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel22);
                            try
                            {
                                r4.Open();
                                if (!r4.IsEOF())
                                {
                                    strTX += r4.GetD("FREQ").ToString("F3");
                                }
                            }
                            finally
                            {
                                r4.Close();
                                r4.Destroy();
                            }
                        }
                        {
                            IMRecordset r5 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r5.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
                            r5.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                            r5.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity);
                            r5.SetWhere("CHANNEL", IMRecordset.Operation.Like, strChannel22);
                            try
                            {
                                r5.Open();
                                if (!r5.IsEOF())
                                {
                                    strRX += r5.GetD("FREQ").ToString("F3");
                                }
                            }
                            finally
                            {
                                r5.Close();
                                r5.Destroy();
                            }
                        }
                        break;
                }
                if (vecDrpsIter != listGroupChannel[listGroupChannel.Count - 1])
                {
                    strChannel += ";";
                    strTX += ";";
                    strRX += ";";
                }
            }
            // Заполняем поля
            string nameFieldChannel = "chan";
            string nameFieldRX = "rxFreq";
            string nameFieldTX = "txFreq";
            if (numChannel >= 0)
            {// Формируем название разширеных полей
                nameFieldChannel += numChannel.ToString();
                nameFieldRX += numChannel.ToString();
                nameFieldTX += numChannel.ToString();
            }

            Cell cell = grid.GetCellFromKey(nameFieldChannel);
            if (cell != null)
            {
                cell.Value = strChannel;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldRX);
            if (cell != null)
            {
                cell.Value = strRX;
                CellValidate(cell, grid);
            }

            cell = grid.GetCellFromKey(nameFieldTX);
            if (cell != null)
            {
                cell.Value = strTX;
                CellValidate(cell, grid);
            }
        }
        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            foreach (IMObject obj in objStations)
            {
                IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
                r.Select("ID,CUST_TXT1,CUST_DAT1");
                r.SetWhere("ID", IMRecordset.Operation.Eq, obj.GetI("ID"));
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("CUST_TXT1", NumberOut);
                        r.Put("CUST_DAT1", DateOut);
                        r.Update();
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
        }
        public LimitType GetAppTypeAndLimitValueGSM()
        {
            LimitType type = LimitType.noLimit;
            List<int> listAllChannel = new List<int>(); //массив секторов
            for (int numChannel = 0; numChannel < msvSectors.Count; ++numChannel)
            {
                //int LastVal = 0;
                List<int> vectChannel = msvSectors[numChannel];
                // Создаем список сгрупированных каналов
                foreach (int vecIter in vectChannel)
                {
                    if (!listAllChannel.Contains(vecIter))
                        listAllChannel.Add(vecIter);
                }
            }
            listAllChannel.Sort();

            int countChannel = 1;
            int maxCountChannel = (listAllChannel.Count > 0) ? 1 : 0;
            for (int i = 1; i < listAllChannel.Count; ++i)
            {
                if (listAllChannel[i] == (listAllChannel[i - 1] + 1))
                    countChannel++;
                else
                    countChannel = 1;

                if (maxCountChannel < countChannel)
                    maxCountChannel = countChannel;
            }

            if ((maxCountChannel == 0) || (maxCountChannel > 3))
            {// НЕ 26.1
                if (GetTransCount() == IM.NullD)
                {
                    type = LimitType.RRaccToBSLimit;
                }
                else
                {
                    type = LimitType.RRtrCountLimit;
                }
            }
            else
            {
                type = LimitType.RRnoFreqHop;
            }

            return type;
        }

        //===========================================================
        /// <summary>
        /// Возвращает количество передатчиков
        /// </summary>
        public double GetTransCount()
        {
            return objStations[0].GetD("EQUIP_NBR");
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(List<int> ID, string radioTech, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                if (radioTech == CRadioTech.GSM_900 || radioTech == CRadioTech.GSM_1800)
                {
                    rs.Select("ID,Position.REMARK,EQUIP_NBR,AssignedFrequencies.ChannelRx.CHANNEL");
                    string ids = "";
                    foreach (int i in ID)
                    {
                        ids += i.ToString() + ",";
                    }
                    ids = ids.Remove(ids.Length - 1, 1);
                    rs.SetAdditional(string.Format("[ID] in ({0})", ids));
                    rs.SetWhere("ID", IMRecordset.Operation.Gt, 0);
                    bool find = false;
                    List<int> chans = new List<int>();
                    int eqnbr = IM.NullI;
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (!find && rs.GetI("ID") == ID[0])
                        {
                            find = true;
                            adress = rs.GetS("Position.REMARK");
                            eqnbr = rs.GetI("EQUIP_NBR");
                        }
                        int ch = ConvertType.ToInt32(rs.GetS("AssignedFrequencies.ChannelRx.CHANNEL"), IM.NullI);
                        if (ch != IM.NullI && !chans.Contains(ch))
                            chans.Add(ch);
                    }
                    chans.Sort();

                    int countChannel = 1;
                    int maxCountChannel = (chans.Count > 0) ? 1 : 0;
                    for (int i = 1; i < chans.Count; ++i)
                    {
                        if (chans[i] == (chans[i - 1] + 1))
                            countChannel++;
                        else
                            countChannel = 1;

                        if (maxCountChannel < countChannel)
                            maxCountChannel = countChannel;
                    }

                    if ((maxCountChannel == 0) || (maxCountChannel > 3))
                    {// НЕ 26.1
                        if (eqnbr != IM.NullI)
                            count = eqnbr;
                        else
                            count = 1;
                    }
                    else //статья 26.1
                        count = chans.Count;
                }
                else
                {
                    rs.Select("ID,Position.REMARK,EQUIP_NBR,AssignedFrequencies.ChannelRx.CHANNEL");
                    string ids = "";
                    foreach (int i in ID)
                    {
                        ids += i.ToString() + ",";
                    }
                    ids = ids.Remove(ids.Length - 1, 1);
                    rs.SetAdditional(string.Format("[ID] in ({0})", ids));
                    rs.SetWhere("ID", IMRecordset.Operation.Gt, 0);
                    // rs.OrderBy("ID", OrderDirection.Ascending);
                    bool find = false;
                    List<int> chans = new List<int>();
                    int eqnbr = IM.NullI;
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (!find && rs.GetI("ID") == ID[0])
                        {
                            find = true;
                            adress = rs.GetS("Position.REMARK");
                        }
                        {
                            int ch = ConvertType.ToInt32(rs.GetS("AssignedFrequencies.ChannelRx.CHANNEL"), IM.NullI);
                            if (ch != IM.NullI && !chans.Contains(ch))
                                chans.Add(ch);
                        }
                    }
                    if (radioTech == CRadioTech.DAMPS || radioTech == CRadioTech.UMTS ||
                        radioTech == CRadioTech.CDMA_450 || radioTech == CRadioTech.CDMA_800)
                        count = chans.Count;
                    else
                        count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
    }
}
