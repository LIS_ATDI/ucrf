﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
   class EarthEquipment : Equipment//, ICloneable
   {
      private Power _minPower = new Power();
      private double _noise_freq;

      public double RxBandwidth { get; set; }
      public double TxBandwidth { get; set; }
      public string RxDesigEmission { get; set; }
      public string TxDesigEmission { get; set; }
      public string RxModulation { get; set; }
      public string TxModulation { get; set; }
      
      public Power MinPower { get { return _minPower; }}
      public double NoiseTemperature
      {
         get { return GetNoiseTemperature(); }
         set { SetNoiseTemperature(value); }
      }

      public EarthEquipment()
      {
         _tableName = ICSMTbl.itblEquipEsta;
         RxDesigEmission = "";
         TxDesigEmission = "";
         RxModulation = "";
         TxModulation = "";
      }

      private double GetNoiseTemperature()
      {
         double noise_k = Math.Pow(10, (_noise_freq / 10));
         double noiseTmp = 290 * (noise_k - 1) + 50;         
         return IM.RoundDeci((noiseTmp + 1),0);
      }

      private void SetNoiseTemperature(double newNoiseFrequency)
      {
         //_noise_freq = newNoiseFrequency;
      }   

      public override void Load()
      {
         if (ID == IM.NullI)
         {
            throw new Exception("Earth Equipment ID is not initialized");
         }

         IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
         r.Select("ID,NAME,MIN_POWER,MAX_POWER,BW,DESIG_EMISSION,MODULATION,NOISE_F,CUST_TXT1,CUST_DAT1");         
         r.SetWhere("ID", IMRecordset.Operation.Eq, Id);            
         r.Open();
         if (!r.IsEOF())
         {
            Name = r.GetS("NAME");

            MinPower[PowerUnits.dBm] = r.GetD("MIN_POWER");
            MaxPower[PowerUnits.dBm] = r.GetD("MAX_POWER");
                                       
            RxBandwidth = r.GetD("BW");
            TxBandwidth = r.GetD("BW");
            TxDesigEmission = _desigEmission = r.GetS("DESIG_EMISSION");
            RxDesigEmission = _desigEmission = r.GetS("DESIG_EMISSION");            
            TxModulation = r.GetS("MODULATION");
            RxModulation = r.GetS("MODULATION");

            _certificate.Symbol = r.GetS("CUST_TXT1");
            _certificate.Date = r.GetT("CUST_DAT1");
            _noise_freq = r.GetD("NOISE_F");               
         }
      }
   }
}
