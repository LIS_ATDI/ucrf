﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using NSPosition;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStation : BaseStation
    {
       public class MobStationFilter
       {
          public string Name { get; set;}
          public int Id { get; set;}
       };

       public class MobStationFrequency
       {
          public double RxFrequency;
          public double TxFrequency;          
       }

       public class MobStationPlan
       {
          public int Source { get; set;} 

          private List<int> _channelList;
          private Dictionary<int, MobStationFrequency> _frequencies = new Dictionary<int, MobStationFrequency>();
          private List<double> _rxFreqnuency = new List<double>();
          private List<double> _txFreqnuency = new List<double>();

          public int Id { get; set; }
          public int FreqPlanId { get; set; }

          public string Standard
          {
             get
             {
                return GetStandardByPlanID(Id);
             }             
          }

          public MobStationPlan()
          {             
          }
          
          public List<int> ChannelList
          {
             get
             {
                return _channelList;
             }
             set
             {
                _channelList = value;
             }
          }

           public List<double> RxFrequency
           {
               get
               {
                   return _rxFreqnuency;
               }
               set
               {
                   Source = 1;
                   _rxFreqnuency = value;                   
               }
           }

           public List<double> TxFrequency
           {
               get
               {
                   return _txFreqnuency;
               }
               set
               {
                   Source = 1;
                   _txFreqnuency = value;                   
               }
           }           

          public double GetRxFrequencyByChannel(int channel)
          {
             return _frequencies[channel].RxFrequency;
          }

          public double GetTxFrequencyByChannel(int channel)
          {
             return _frequencies[channel].TxFrequency;
          }

          public void SetRxFrequencyByChannel(int channel, double frequency)
          {
             if (!_frequencies.ContainsKey(channel))
             {
                MobStationFrequency newFrequency = new MobStationFrequency();
                _frequencies.Add(channel, newFrequency);                
             }

             _frequencies[channel].RxFrequency = frequency;
          }

          public void SetTxFrequencyByChannel(int channel, double frequency)
          {
             if (!_frequencies.ContainsKey(channel))
             {
                MobStationFrequency newFrequency = new MobStationFrequency();
                _frequencies.Add(channel, newFrequency);                
             }

             _frequencies[channel].TxFrequency = frequency;
          }
          

          public string ChannelListAsString
          {
             get
             {
                return getChannelListAsString();
             }
             set
             {
                setChannelListAsString(value);
             }
          }

          public string RxFrequencyListAsString
          {
             get
             {
                  return getRxFrequencyListAsString();
             }
          }

          public string TxFrequencyListAsString
          {
             get
             {
                return getTxFrequencyListAsString();
             }
             //set
             //{
             //    setPlanByTxFrequency(value);
             //}
          }

          private List<List<int>> BuildListGroupChannel()
          {
             int LastVal = 0;
             List<List<int>> listGroupChannel = new List<List<int>>();

             foreach (int vecIter in _channelList)
             {
                if (vecIter == _channelList[0])
                {
                   // Первый элемент
                   LastVal = vecIter;
                   List<int> groupChannel = new List<int>();
                   groupChannel.Add(LastVal);
                   listGroupChannel.Add(groupChannel);
                }
                else
                {
                   // Не первый элемент
                   if ((LastVal + 1) == vecIter)
                   {
                      List<int> groupChannel = listGroupChannel[listGroupChannel.Count - 1];
                      groupChannel.Add(vecIter);
                   }
                   else
                   {
                      List<int> groupChannel = new List<int>();
                      groupChannel.Add(vecIter);
                      listGroupChannel.Add(groupChannel);
                   }
                   LastVal = vecIter;
                }
             }
             return listGroupChannel;
          }

          protected string getChannelListAsString()
          {
             List<List<int>> listGroupChannel = BuildListGroupChannel();             
             // Создаем список сгрупированных каналов
             
             string strChannel = "";
             foreach (List<int> vecDrpsIter in listGroupChannel)
             {
                List<int> vectGroup = vecDrpsIter;
                switch (vectGroup.Count)
                {
                   case 0:
                      break;
                   case 1:
                      strChannel += vectGroup[vectGroup.Count - 1].ToString();
                      break;
                   case 2: //Две запись                      
                      strChannel += (vectGroup[0].ToString() + ";");
                      strChannel += vectGroup[vectGroup.Count - 1].ToString();
                      break;
                   default:
                      strChannel += (vectGroup[0].ToString() + "-");
                      strChannel += vectGroup[vectGroup.Count - 1].ToString();
                      break;
                }

                if (vecDrpsIter != listGroupChannel[listGroupChannel.Count - 1])
                   strChannel += "; ";
             }

             return strChannel;
          }

          protected void setChannelListAsString(string value)
          {              
          }

          protected string getRxFrequencyListAsString()
          {             
             if (Source == 0)
             {
                 List<List<int>> listGroupChannel = BuildListGroupChannel();
                 // Создаем список сгрупированных каналов

                 string strFrequency = "";
                 int i1, i2;
                 foreach (List<int> vecDrpsIter in listGroupChannel)
                 {
                     List<int> vectGroup = vecDrpsIter;
                     switch (vectGroup.Count)
                     {
                         case 0:
                             break;
                         case 1:
                             i1 = vectGroup[vectGroup.Count - 1];
                             strFrequency += GetRxFrequencyByChannel(i1).ToString("F5");
                             break;
                         case 2: //Две запись                      
                             //strChannel += (vectGroup[0].ToString() + ";");
                             //strChannel += vectGroup[vectGroup.Count - 1].ToString();
                             i1 = vectGroup[0];
                             i2 = vectGroup[vectGroup.Count - 1];

                             strFrequency += GetRxFrequencyByChannel(i1).ToString("F5") + ";";
                             strFrequency += GetRxFrequencyByChannel(i2).ToString("F5");
                             break;
                         default:
                             i1 = vectGroup[0];
                             i2 = vectGroup[vectGroup.Count - 1];

                             strFrequency += GetRxFrequencyByChannel(i1).ToString("F5") + "-";
                             strFrequency += GetRxFrequencyByChannel(i2).ToString("F5");
                             break;
                     }

                     if (vecDrpsIter != listGroupChannel[listGroupChannel.Count - 1])
                         strFrequency += "; ";
                 }

                 return strFrequency;
             } else
             {
                 return HelpFunction.ToString(RxFrequency);
             }
          }

          private string getTxFrequencyListAsString()
          {                            
              if (Source == 0)
              {
                  List<List<int>> listGroupChannel = BuildListGroupChannel();
                  // Создаем список сгрупированных каналов

                  string strFrequency = "";

                  int i1, i2;
                  foreach (List<int> vecDrpsIter in listGroupChannel)
                  {
                      List<int> vectGroup = vecDrpsIter;
                      switch (vectGroup.Count)
                      {
                          case 0:
                              break;
                          case 1:
                              i1 = vectGroup[vectGroup.Count - 1];
                              strFrequency += GetTxFrequencyByChannel(i1).ToString("F5");
                              break;
                          case 2: //Две запись                      
                              //strChannel += (vectGroup[0].ToString() + ";");
                              //strChannel += vectGroup[vectGroup.Count - 1].ToString();
                              i1 = vectGroup[0];
                              i2 = vectGroup[vectGroup.Count - 1];

                              strFrequency += GetTxFrequencyByChannel(i1).ToString("F5") + ";";
                              strFrequency += GetTxFrequencyByChannel(i2).ToString("F5");
                              break;
                          default:
                              i1 = vectGroup[0];
                              i2 = vectGroup[vectGroup.Count - 1];

                              strFrequency += GetTxFrequencyByChannel(i1).ToString("F5") + "-";
                              strFrequency += GetTxFrequencyByChannel(i2).ToString("F5");
                              break;
                      }

                      if (vecDrpsIter != listGroupChannel[listGroupChannel.Count - 1])
                          strFrequency += "; ";
                  }
                  return strFrequency;
              } else
              {
                  return HelpFunction.ToString(TxFrequency);
              }
          }

          private void setPlanByTxFrequency(string value)
          {
              int planId = GetFreqPlanID(Standard);           
          }
       }

       protected MobStationFilter GetFilterTypeInfoByStationID(int StatiobID, string StationTable)
       {
          MobStationFilter fti = new MobStationFilter();

          fti.Id = IM.NullI;
          fti.Name = "";

          IMRecordset r = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadOnly);
          r.Select("OBJ_ID,OBJ_TABLE,EX_FILTER_ID");
          r.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, StatiobID);
          r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, StationTable);//ICSMTbl.itblMobStation);
          try
          {
             fti.Name = "";
             r.Open();
             if (!r.IsEOF())
             {
                fti.Id = r.GetI("EX_FILTER_ID");

                if (fti.Id != IM.NullI)
                {
                   IMRecordset r2 = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
                   r2.Select("ID,NAME");
                   r2.SetWhere("ID", IMRecordset.Operation.Eq, fti.Id);
                   try
                   {
                      r2.Open();
                      if (!r2.IsEOF())
                      {
                         fti.Name = r2.GetS("NAME");
                      }
                   }
                   finally
                   {
                      r2.Close();
                      r2.Destroy();
                   }
                }
             }
             //cell.Value = stationList[index].FilterName;
             //CellValidate(cell, grid);
          }
          finally
          {
             r.Close();
             r.Destroy();
          }
          return fti;
       }

        private Power _power = new Power();
        //public int PosId { get; set; }
        //public int AntId { get; set; }
        //public int EquipId { get; set; }
        /// <summary>
        /// Стандарт
        /// </summary> 
        public string Standard { get; set; }
        //public PositionState Position { get; set; }
        public double ASL { get; set; }
        /// <summary>
        /// Тип базової станції
        /// </summary>
        public string CellBaseType { get; set; }
        /// <summary>
        /// Максимальна потужність передавача, дБВт        
        /// </summary>
        //public double PowerTr { get; set; }
        public double Bandwith { get; set; }
        public string TxDesignEmission { get; set; }
        //public List<string> ModulationList = new List<string>();
        public double BaseType { get; set; }
        public string Rez { get; set; }
        public double TransmitterCount { get; set; }       
        public Power Power { get { return _power; } }
        //public string CertificateNumber { get; set; }
        //public string CertificateDate { get; set; }
        public double Agl { get; set; }
        ///<summary>
        ///Азимут випромінювання антени, град
        /// </summary>
        //public double Emission { get; set; }
        public double Azimuth { get; set; }
        //public double PlaceSite { get; set; }
        //public string AntennaType { get; set; }
        public double HeightMax { get; set; }
        public double Gain { get; set; }
        public double AntenaWidth { get; set; }
        //public Dictionary<string, string> PolarAntDict = new Dictionary<string, string>();
        //public string PolarAnt { get; set; }
        ///// <summary>
        /////  Частоти приймання, МГц
        ///// </summary>
        //public double CellTxFrequency { get; set; }
        ///// <summary>
        /////Частоти передавання, МГц
        ///// </summary>
        //public double CellFrequencyTransfer { get; set; }
        public double Elevation { get; set; }
        public double TxLosses { set; get; }
        //public double ChannelNumber { set; get; }
        public string Name { get; set; }
        /// <summary>
        /// Клас випромінювання
        /// </summary>
        public string DesigEmission { get; set; }

        protected MobStationAntenna _antenna;
        protected MobStationEquipment _equipment;
        protected MobStationFilter _filter;
        protected MobStationPlan _plan; 
       
          public MobStationAntenna Antenna { get { return _antenna; }}
          public MobStationEquipment Equipment { get { return _equipment; } }
          public MobStationFilter Filter { get { return _filter; } }
          public MobStationPlan Plan { get { return _plan; } }

        public static readonly string TableName = ICSMTbl.itblMobStation;        

        public MobStation()
        {        
        }
               
        public override void Save()          
        {
           /*_antenna = new MobStationAntenna();
           _antenna.TableName = ICSMTbl.itblAntennaMob;

           _equipment = new MobStationEquipment();
           _equipment.TableName = ICSMTbl.itblEquipPmr;

           Position = new PositionState();

           _plan = new MobStationPlan();
           _filter = new MobStationFilter();*/

           IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadWrite);
           try
           {
              r.Select("ID,POS_ID,EQUIP_ID,ANT_ID,PWR_ANT,STANDARD,NAME,DESIG_EMISSION,EQUIP_NBR,AGL,AZIMUTH,GAIN,TX_LOSSES,ELEVATION,NAME");
              r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
              r.Open();
              if (!r.IsEOF())
              {
                 r.Edit();
                 //Id = r.GetI("ID");
                 //PosId = r.GetI("POS_ID");
                 //EquipId = r.GetI("EQUIP_ID");

                 r.Put("EQUIP_ID", Equipment.Id);
                 r.Put("ANT_ID", Antenna.Id);
                 r.Put("POS_ID", Position.Id);
                 r.Put("PWR_ANT", Power[PowerUnits.dBW]);
                 r.Put("STANDARD", Standard);
                 r.Put("NAME", Name);
                 r.Put("DESIG_EMISSION", DesigEmission);                  
                 r.Put("EQUIP_NBR", TransmitterCount);
                 r.Put("AGL", Agl);
                 r.Put("AZIMUTH", Azimuth);
                 r.Put("GAIN", Gain);
                 r.Put("TX_LOSSES", TxLosses);
                 r.Put("ELEVATION", Elevation);
                 
                 //Plan.Id = r.GetI("PLAN_ID");

                 //Position.LoadStatePosition(r.GetI("POS_ID"), ICSMTbl.itblPositionWim);
                 //Antenna.Load();
                 //Equipment.Load();

                 //if (string.IsNullOrEmpty(DesigEmission) && !string.IsNullOrEmpty(Equipment.DesigEmission))
                 //   DesigEmission = Equipment.DesigEmission;
                 //_filter = GetFilterTypeInfoByStationID(Id, ICSMTbl.itblMobStation);
                 //LoadChannelInfo();
                 r.Update();
              }
           }
           finally
           {
              if (r.IsOpen())
                 r.Close();
              r.Destroy();
           }

           FillComments(ApplId);
        }

        public override void Load()
        {
            _antenna = new MobStationAntenna();
            _antenna.TableName = ICSMTbl.itblAntennaMob;

            _equipment = new MobStationEquipment();
            _equipment.TableName = ICSMTbl.itblEquipPmr;

            Position = new PositionState();

            _plan = new MobStationPlan();
            _filter = new MobStationFilter();
           
            IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,PLAN_ID,EQUIP_ID,ANT_ID,PWR_ANT,STANDARD,NAME,DESIG_EMISSION,EQUIP_NBR,AGL,AZIMUTH,GAIN,TX_LOSSES,ELEVATION,NAME");
                r.Select("CUST_TXT17");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    //Id = r.GetI("ID");
                    //PosId = r.GetI("POS_ID");
                    //EquipId = r.GetI("EQUIP_ID");

                    Equipment.Id = r.GetI("EQUIP_ID");
                    Antenna.Id = r.GetI("ANT_ID");
                    Power[PowerUnits.dBW] = r.GetD("PWR_ANT");                    
                    Standard = r.GetS("STANDARD");
                    Rez = r.GetS("NAME");
                    DesigEmission = r.GetS("DESIG_EMISSION");
                    TransmitterCount = r.GetD("EQUIP_NBR");
                    Agl = r.GetD("AGL");
                    Azimuth = r.GetD("AZIMUTH");
                    Gain = r.GetD("GAIN");
                    TxLosses = r.GetD("TX_LOSSES");
                    Elevation = r.GetD("ELEVATION");
                    Name = r.GetS("NAME");
                    Plan.Id = r.GetI("PLAN_ID");
                    //Висновок
                    Finding = r.GetS("CUST_TXT17");
                    Position.LoadStatePosition(r.GetI("POS_ID"), ICSMTbl.itblPositionWim);            
                    Antenna.Load();
                    Equipment.Load();                   

                    if (string.IsNullOrEmpty(DesigEmission) && !string.IsNullOrEmpty(Equipment.DesigEmission))
                       DesigEmission = Equipment.DesigEmission;
 
                    _filter = GetFilterTypeInfoByStationID(Id, ICSMTbl.itblMobStation);

                    LoadChannelInfo();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }                                   
        }

        private void LoadChannelInfo()
        {
           LoadChannels();           

           if (ChangeFindChannel())
           {
              LoadFrequenciesUMTS();
           }
           else
           {              
              if (Standard == CRadioTech.DECT)
              {                 
                 //UpdateCellChannelRr2("N","N");
                 LoadFrequenciesRR2("N", "N");
              }
              else
              {
                 //UpdateCellChannelRr2("D","U");
                 LoadFrequenciesRR2("D", "U");
              }
           }
        }

       public void CalculateChannels()
       {
           //List<double> freqs = new List<double>();

           //foreach()           
       }

       private void LoadChannels()
       {
          List<double> freqs = new List<double>();
          {
              MobFreq[] freqStation = MobFreqManager.LoadFreq(Id, MobFreqManager.TypeFreq.MobStation);
              foreach (MobFreq freqItem in freqStation)
              {
                  double txf = freqItem.TxMHz.Round(5);
                  double rxf = freqItem.RxMHz.Round(5);
                  if (txf != IM.NullD && txf > 0 && !freqs.Contains(txf))
                  {
                      Plan.TxFrequency.Add(txf);
                      freqs.Add(txf);
                  }
                  if (rxf != IM.NullD && rxf > 0 && !freqs.Contains(rxf))
                  {
                      Plan.RxFrequency.Add(rxf);
                      freqs.Add(rxf);
                  }
              }
          }

          CalculateChannels();
           
          List<int> chans = new List<int>();
          if (freqs.Count > 0)
          {             
             if (Plan.Id != IM.NullI)
             {
                IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                r3.Select("FREQ,PLAN_ID,CHANNEL");
                r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, Plan.Id);
                try
                {
                   for (r3.Open(); !r3.IsEOF(); r3.MoveNext())
                   {
                      int ch = ConvertType.ToInt32(r3.GetS("CHANNEL"), IM.NullI);
                      double f = r3.GetD("FREQ");
                      if (freqs.Contains(f) && ch != IM.NullI && ch >= 0 && !chans.Contains(ch))
                         chans.Add(ch);
                   }
                }
                finally
                {
                   r3.Close();
                   r3.Destroy();
                }
             }
             chans.Sort();
          }
          Plan.ChannelList = chans;
       }

       private static string GetStandardByPlanID(int planId)
       {
          string standard = "";
          IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
          rsFreqPlan.Select("NAME");
          
          rsFreqPlan.SetWhere("ID", IMRecordset.Operation.Eq, planId);
          try
          {
             rsFreqPlan.Open();
             if (!rsFreqPlan.IsEOF())
                standard = rsFreqPlan.GetS("NAME");
          }
          finally
          {
             rsFreqPlan.Close();
             rsFreqPlan.Destroy();
          }
          return standard;
       }

       internal static int GetFreqPlanID(string strRadioTech)
       {
           int IDFreqPlan = IM.NullI;

           //string strRadioTech = Plan.Standard;
           IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);

           r.Select("ID,NAME");
           r.SetWhere("NAME", IMRecordset.Operation.Like, strRadioTech);
           try
           {
               r.Open();
               if (!r.IsEOF())
               {
                   IDFreqPlan = r.GetI("ID");
               }
           }
           finally
           {
               r.Close();
               r.Destroy();
           }
           return IDFreqPlan;           
       }

       private int GetFreqPlanID()
       {
           return GetFreqPlanID(Plan.Standard);
       }              

       private void LoadFrequenciesRR2(string RxParity, string TxParity)
       {
          Plan.FreqPlanId = GetFreqPlanID();
          
          foreach (int channel in Plan.ChannelList)
          {
             IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
             r2.Select("PLAN_ID,PARITY,FREQ");
             r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, Plan.FreqPlanId);
             r2.SetWhere("PARITY", IMRecordset.Operation.Like, RxParity); //"D");
             r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, channel.ToString());
             try
             {
                r2.Open();
                if (!r2.IsEOF())
                {
                   double freq = r2.GetD("FREQ");
                   Plan.SetRxFrequencyByChannel(channel, freq);
                }
             }
             finally
             {
                r2.Close();
                r2.Destroy();
             }

             IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
             r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
             r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, Plan.FreqPlanId);
             r3.SetWhere("PARITY", IMRecordset.Operation.Like, TxParity);
             r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, channel.ToString());
             try
             {
                r3.Open();
                if (!r3.IsEOF())
                {
                   double freq = r3.GetD("FREQ");
                   Plan.SetTxFrequencyByChannel(channel, freq);                   
                }
             }
             finally
             {
                r3.Close();
                r3.Destroy();
             }
          }          
       }

       private void LoadFrequenciesUMTS()
       {
          Plan.FreqPlanId = GetFreqPlanID();

          foreach (int channel in Plan.ChannelList)
          {
             IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
             r2.Select("PLAN_ID,PARITY,FREQ");
             r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, Plan.FreqPlanId);
             r2.SetWhere("PARITY", IMRecordset.Operation.Like, "D");
             r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, channel.ToString());
             try
             {
                r2.Open();
                if (!r2.IsEOF())
                {
                   double freq = r2.GetD("FREQ");
                   Plan.SetRxFrequencyByChannel(channel, freq);
                   // ошибка возникала из=за отсутствия данных по каналу channel + 950 (не находило в словаре)
                   Plan.SetRxFrequencyByChannel(channel + 950, freq);
                }
             }
             finally
             {
                r2.Close();
                r2.Destroy();
             }

             IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
             r3.Select("PLAN_ID,PARITY,CHANNEL,FREQ");
             r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, Plan.FreqPlanId);
             r3.SetWhere("PARITY", IMRecordset.Operation.Like, "U");
             r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, (channel + 950).ToString());
             try
             {
                r3.Open();
                if (!r3.IsEOF())
                {
                   double freq = r3.GetD("FREQ");
                   Plan.SetTxFrequencyByChannel(channel, freq);
                   // ошибка возникала из=за отсутствия данных по каналу channel + 950 (не находило в словаре)
                   Plan.SetTxFrequencyByChannel(channel + 950, freq);
                }
             }
             finally
             {
                r3.Close();
                r3.Destroy();
             }
          }          
          
       }

       //===================================================
        /// <summary>
        /// Изменить алгоритм поиска каналов
        /// </summary>
        /// <returns>yes / no</returns>
        bool ChangeFindChannel()
        {
           string strFreqPlan = Standard;
           if (strFreqPlan.Contains("UMTS"))
              return true;
           return false;
        }

        //===================================================
        /// <summary>
        /// Обновить ячейку каналов в заявке PP2 для UMTS
        /// </summary>
        /// <param name="numChannel">Номер сектора</param>
        void UpdateCellChannelRr2_UMTS()
        {           
           // Вытаскиваем номер "плана"

           int FreqPlanId = GetFreqPlanID();

           //stationList[numChannel].TxFreqs.Clear();
           //stationList[numChannel].RxFreqs.Clear();
           // Формируем список частот и каналов
           //List<int> vectChannel = msvSectors[numChannel];
           //string strChannel = "";
           //string strTX = "";
           //string strRX = "";
           // Создаем список сгрупированных каналов

          /* foreach (int vecIter in vectChannel)
           {
              // RX
              {
                 IMRecordset r2 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                 r2.Select("PLAN_ID,PARITY,FREQ");
                 r2.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                 r2.SetWhere("PARITY", IMRecordset.Operation.Like, "D");
                 r2.SetWhere("CHANNEL", IMRecordset.Operation.Like, vecIter.ToString());
                 try
                 {
                    r2.Open();
                    if (!r2.IsEOF())
                    {
                       if (strRX != "")
                          strRX += ";";
                       double freq = r2.GetD("FREQ");
                       strRX += freq.ToString("F3");
                       stationList[numChannel].RxFreqs.Add(freq);
                       // Каналы
                       if (strChannel != "")
                          strChannel += ";";
                       strChannel += vecIter.ToString() + "/" + (vecIter + 950).ToString();
                    }
                 }
                 finally
                 {
                    r2.Close();
                    r2.Destroy();
                 }
              }
              {
                 IMRecordset r3 = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                 r3.Select("PLAN_ID,PARITY,FREQ");
                 r3.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, IDFreqPlan);
                 r3.SetWhere("PARITY", IMRecordset.Operation.Like, "U");
                 r3.SetWhere("CHANNEL", IMRecordset.Operation.Like, (vecIter + 950).ToString());
                 try
                 {
                    r3.Open();
                    if (!r3.IsEOF())
                    {
                       if (strTX != "")
                          strTX += ";";
                       double freq = r3.GetD("FREQ");
                       strTX += freq.ToString("F3");
                       stationList[numChannel].TxFreqs.Add(freq);
                    }
                 }
                 finally
                 {
                    r3.Close();
                    r3.Destroy();
                 }
              }
           }
           // Заполняем поля
           string nameFieldChannel = "chan";
           string nameFieldRX = "rxFreq";
           string nameFieldTX = "txFreq";
           if (numChannel >= 0)
           {// Формируем название разширеных полей
              nameFieldChannel += (numChannel).ToString();
              nameFieldRX += (numChannel).ToString();
              nameFieldTX += (numChannel).ToString();
           }

           Cell cell = grid.GetCellFromKey(nameFieldChannel);
           if (cell != null)
           {
              cell.Value = strChannel;
              CellValidate(cell, grid);
           }

           cell = grid.GetCellFromKey(nameFieldRX);
           if (cell != null)
           {
              cell.Value = strRX;
              CellValidate(cell, grid);
           }

           cell = grid.GetCellFromKey(nameFieldTX);
           if (cell != null)
           {
              cell.Value = strTX;
              CellValidate(cell, grid);
           }*/
        }

       //===================================================
		/// <summary>
		/// Обновить ячейку каналов в заявке PP2 
		/// </summary>
		/// <param name="numChannel">Номер сектора</param>
      private void UpdateCellChannelRr2(string RxParity, string TxParity)
      {          
          // Вытаскиваем номер "плана"
          string strRadioTech = Standard;
          IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
          int IDFreqPlan = IM.NullI;
          r.Select("ID,NAME");
          r.SetWhere("NAME", IMRecordset.Operation.Like, strRadioTech);
          try
          {
              r.Open();
              if (!r.IsEOF())
              {
                  IDFreqPlan = r.GetI("ID");
              }
              else
                  return;
          }
          finally
          {
              r.Close();
              r.Destroy();
          }          
       }

       public void SetDeletedStatus()
       {
          IMRecordset rd = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadWrite);
          rd.Select("ID,STATUS");
          rd.SetWhere("ID", IMRecordset.Operation.Eq, Id);
          try
          {
             rd.Open();
             if (!rd.IsEOF())
             {
                rd.Edit();                
                rd.Put("STATUS", "Z");
                rd.Update();
             }
          }
          finally
          {
             rd.Close();
             rd.Destroy();
          }
       }
    }
}
