﻿using System;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.HelpClasses;
//XICSM.UcrfRfaNET.ApplSource.Classes
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class EmitterEquipment : Equipment
    {
        private Power _power = new Power();

        public Power Pow { get; set; }
        //public int Number;
        public Certificate cert = null;
        public Power Power { get { return _power; } }
        public double Quantity { get; set; }
        public double Bandwidth { get; set; }
        public string PlantNumb { get; set; }
        public string Devi { get; set; }
        public string Passing { get; set; }

        //Cмуги частот
        public List<AmateurBand> AmBand { get; set; }
        //Частоти
        public List<AmateurFreq> AmFreq { get; set; }

        public EmitterEquipment()
        {
            Pow = new Power();
            cert = new Certificate();
            Id = IM.NullI;
            Name = "";
            cert.Date = DateTime.MinValue;
            cert.Symbol = "";
        }

        public void Load(int id)
        {
            Id = id;
            Load();
        }

        public override void Load()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaEquip, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME,DESIG_EMISSION,CERTIFICATE_DATE,CERTIFICATE_NUM,MAX_POWER");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            if (!r.IsEOF())
            {
                Name = r.GetS("NAME");                
                Certificate.Date = r.GetT("CERTIFICATE_DATE");
                Certificate.Symbol = r.GetS("CERTIFICATE_NUM");
                Pow[PowerUnits.dBm] = r.GetD("MAX_POWER");
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaEquip, IMRecordset.Mode.ReadWrite);
            r.Select("ID,NAME,DESIG_EMISSION,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
                else
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(_tableName, 1, -1));
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }
                r.Put("NAME", Name);
                r.Put("DESIG_EMISSION", DesigEmission);
                r.Update();
            }
            finally
            {
                r.Final();
            }

        }
    }
}
