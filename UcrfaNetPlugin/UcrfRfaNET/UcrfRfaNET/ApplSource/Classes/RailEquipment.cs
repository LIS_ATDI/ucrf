﻿using System;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.HelpClasses;
//XICSM.UcrfRfaNET.ApplSource.Classes
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    /// <summary>
    /// Класс обладнання для залізниці
    /// </summary>
    class RailEquipment : Equipment
    {
        private Power _power = new Power();

        public int Number{ get; set;}

        public Power Power { get { return _power; } }
        public double Quantity { get; set; }        
        public string PlantNumb { get; set; }
        public string Devi { get; set; }
        public string Passing { get; set; }

       
        //Частоти
        public List<AmateurFreq> AmFreq { get; set; }

        public RailEquipment()
        {
            Id = IM.NullI;
            Name = "";
            DesigEmission = "";
            PlantNumb = "";           
            AmFreq=new List<AmateurFreq>();
        }

        /// <summary>
        /// Завантажити дані з таблиці PlugTbl.XfaEquip по визначеному Id
        /// </summary>
        /// <param name="id"> id </param>
        public void Load(int id)
        {
            Id = id;
            Load();
        }

        /// <summary>
        /// Завантажити дані з таблиці PlugTbl.XfaEquip
        /// </summary>
        public override void Load()
        {
            _tableName = PlugTbl.XfaEquip;
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
            r.Select("ID,MAX_POWER,NAME,DESIG_EMISSION,CERTIFICATE_NUM,CERTIFICATE_DATE,BW");
            r.Select("RX_LOWER_FREQ,RX_UPPER_FREQ,LOWER_FREQ,UPPER_FREQ,CODE,FAMILY,MANUFACTURER");
            r.Select("TECH_IDNO");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    _maxPower[PowerUnits.dBm] = r.GetD("MAX_POWER");
                    _equipmentName = r.GetS("NAME");
                    _desigEmission = r.GetS("DESIG_EMISSION");
                    BandWidth = r.GetD("BW");
                    Certificate.Date = r.GetT("CERTIFICATE_DATE");
                    Certificate.Symbol = r.GetS("CERTIFICATE_NUM");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Зберегти дані в таблиці
        /// </summary>
        public override void Save()
        {
           
        }
    }
}
