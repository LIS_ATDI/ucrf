﻿using System;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    internal class EmployeeList
    {
        // Класс для списака пользователей.
        // приспособлен для биндинга с комьбобоксом
        private readonly string vrzString = Enum.GetName(DepartmentType.VRZ.GetType(), DepartmentType.VRZ);
        private ComboBoxDictionaryList<int, string> _overallDictonary = new ComboBoxDictionaryList<int, string>();

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadEntireEmployeeList()
        {
            _overallDictonary.Clear();
            
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                else // оставлю так, пусть и идиотско - SK, 21/11/12
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, vrzString);
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");

                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");

                    string employee = lastName + " " + firstName;

                    _overallDictonary.Add(new ComboBoxDictionary<int, string>(ID, employee));


                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        public void ReadEntireEmployeeList(string province, string employeeType)
        {
            _overallDictonary.Clear();

            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME,PROVINCE");
                //if (string.IsNullOrEmpty(employeeType))
                //    r.SetWhere("TYPE", IMRecordset.Operation.Eq, vrzString);
                //else
                r.SetWhere("TYPE", IMRecordset.Operation.Eq, employeeType);

                r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, province);
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");

                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");

                    string employee = lastName + " " + firstName;

                    _overallDictonary.Add(new ComboBoxDictionary<int, string>(ID, employee));


                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }            
        }

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadEntireEmployeeList(string province)
        {
            ReadEntireEmployeeList(province, (CUsers.CurDepartment == UcrfDepartment.Branch) ? "" : vrzString);
        }
        
        /// <summary>
        /// Получить рещультатт в виде удобном бля биндинга
        /// </summary>
        /// <returns></returns>
        public ComboBoxDictionaryList<int, string> GetComboBoxDictionaryList()
        {
            return _overallDictonary;
        }
    }
}
