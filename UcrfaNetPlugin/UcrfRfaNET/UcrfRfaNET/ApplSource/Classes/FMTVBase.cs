﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    public abstract class FMTVBase : BaseAppClass
    {
        public string Direction = "";

        public FMTVBase(int _id, string _tableName, int _ownerId, int _packetID, string _radioTech)
            : base(
               _id, _tableName, _ownerId, _packetID, _radioTech)
        {
            //numDozv = "CUST_TXT3";
            //datePrintDozv = "CUST_DAT3";
            //dateValidDozv = "CUST_DAT7";
            //numDozvOld = "CUST_TXT15";
            //dateDRVDozv = "CUST_DAT6";
            //dateDRVDozvOld = "CUST_DAT8";
        }

        /// <summary>
        /// Gets Licence (if Name of Licence is valid)
        /// </summary>
        /// <param name="value">name of licence</param>      
        private void ProcessingLicence(string nameLicence)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME,START_DATE,STOP_DATE,STANDARD");
            r.SetWhere("NAME", IMRecordset.Operation.Like, nameLicence);
            r.OrderBy("ID", OrderDirection.Descending);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    string standard = r.GetS("STANDARD");
                    int licenceId = r.GetI("ID");
                    if (standard != radioTech)
                        MessageBox.Show("Дана ліцензія має невірну радіотехнологію!", "Помилка вибору ліцензії",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (licenceListId.Contains(licenceId))
                    {
                        MessageBox.Show(string.Format(CLocaliz.TxT("The licence '{0}' already was added"), nameLicence), CLocaliz.TxT("The licence already exist"),
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        gridLicence.CurrentRow.Cells[0].Value = r.GetI("ID");
                        gridLicence.CurrentRow.Cells[1].Value = r.GetS("NAME");
                        gridLicence.CurrentRow.Cells[1].Tag = r.GetS("NAME");
                        gridLicence.CurrentRow.Cells[2].Value = r.GetT("START_DATE").ToString("dd.MM.yy");
                        gridLicence.CurrentRow.Cells[3].Value = r.GetT("STOP_DATE").ToString("dd.MM.yy");

                        licenceListId.Add(licenceId);
                        gridLicence.Rows.Add();
                        gridLicence.BeginEdit(true);
                        IsChangeDop = true;
                    }
                }
                else if (gridLicence.CurrentRow != null)
                {
                    int idLicence = (gridLicence.CurrentRow.Cells[0].Value != null) ? gridLicence.CurrentRow.Cells[0].Value.ToString().ToInt32(IM.NullI) : IM.NullI;
                    if (licenceListId.Remove(idLicence))
                    {
                        gridLicence.Rows.Remove(gridLicence.CurrentRow);
                        IsChangeDop = true;
                    }
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Инициализацыя Грида
        /// </summary>
        /// <param name="gridLic"></param>
        public override void InitLicenceGrid(DataGridView gridLic)
        {
            base.InitLicenceGrid(gridLic);
            gridLicence.ReadOnly = false;
            gridLicence.EditMode = DataGridViewEditMode.EditOnEnter;

            gridLicence.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(gridLicence_EditingControlShowing);
                        
            gridLicence.Rows.Add();
        }

        /// <summary>
        /// подписываемся на событие появление редактируемого контрола.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gridLicence_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            // проверка что это TextBox с колонки номер 1
            if (gridLicence.CurrentCell.ColumnIndex == 1)
            {
                if (e.Control.Tag == null)
                {
                    e.Control.PreviewKeyDown += new PreviewKeyDownEventHandler(Control_PreviewKeyDown);
                    e.Control.Tag = true;
                }
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            string retVal = "";
            if (objStation.GetS("POLARIZATION") == "M")
                retVal = "M";
            return retVal;
        }
        /// <summary>
        /// Обработка нажатия ENTER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Control_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // не получилось потянуть значение с CELL 
                TextBox text = gridLicence.EditingControl as TextBox;
                if (text != null)
                {
                    ProcessingLicence(text.Text);
                }
            }
        }
        //----------------------------------------------------------------------------------------
        //========================================================================================
        protected void AutoFillPolar(Cell polarizationCell, Cell horzGainCell, Cell vertGainCell)
        {
            if (polarizationCell == null || horzGainCell == null || vertGainCell == null)
            {
                return;
            }
            string polar = CPolarization.getShortPolarization(polarizationCell.Value);

            if (polar == "V")
            {
                polarizationCell.CanEdit = true;
                FillVerticalPolar(vertGainCell);
                //Cell fillCell = grid.GetCellFromKey("horzGain");
                horzGainCell.Value = "——";
                horzGainCell.CanEdit = false;
                return;
            }

            if (polar == "H")
            {
                polarizationCell.CanEdit = true;
                FillHorizontalPolar(horzGainCell);
                vertGainCell.Value = "——";
                vertGainCell.CanEdit = false;
                return;
            }

            if (polar == "M")
            {
                FillMixedPolar(horzGainCell, vertGainCell);
            }
            horzGainCell.CanEdit = true;
            vertGainCell.CanEdit = true;
        }
        //========================================================================================
        protected void FillVerticalPolar(Cell vertGainCell)
        {
            string strDiagVertical = objStation.GetS("DIAGV");
            double Gain = objStation.GetD("GAIN");

            if (strDiagVertical != "OMNI")
            {
                List<double> DiagDbl = HelpClasses.HelpFunction.StringToGainedDiag(strDiagVertical, Gain);

                if (DiagDbl.Count > 0)
                    vertGainCell.Value = DiagDbl.Max().ToString("F3");
            }
            else
            {
                vertGainCell.Value = Gain.ToString("F3");
            }

            vertGainCell.CanEdit = true;
        }
        //========================================================================================
        private void FillHorizontalPolar(Cell horzGainCell)
        {
            string strDiagHorizontal = objStation.GetS("DIAGH");
            double Gain = objStation.GetD("GAIN");

            if (strDiagHorizontal != "OMNI")
            {
                List<double> DiagDbl = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, Gain);
                if (DiagDbl.Count > 0)
                    horzGainCell.Value = DiagDbl.Max().ToString("F3");
            }
            else
            {
                horzGainCell.Value = Gain.ToString("F3");
            }

            horzGainCell.CanEdit = true;
            //fillCell = grid.GetCellFromKey("vertGain");
            //fillCell.Value = "——";
            //fillCell.CanEdit = false;
        }
        //================================================================================
        protected void FillMixedPolar(Cell horzGainCell, Cell vertGainCell)
        {
            FillVerticalPolar(vertGainCell);
            FillHorizontalPolar(horzGainCell);
        }
        //================================================================================
        protected void OnPresButtonHorzGain(Cell gainCell, Cell dirCell, Cell polarCell)
        {
            string strDiagVertical = objStation.GetS("DIAGV");
            string strDiagHorizontal = objStation.GetS("DIAGH");
            double Gain = objStation.GetD("GAIN");

            //Cell dirCell = grid.GetCellFromKey("dir");
            Direction = CAntennaDirection.getShortDirection(dirCell.Value);

            if (Direction == "D")
            {
                string Polarization = objStation.GetS("POLARIZATION");
                if (Polarization != "V")
                {
                    FEnterEVP f = new FEnterEVP("Ввести коефіцієнт підсилення антени (Г)",
                       "Азимут, град", "Коеф. підсил., дБ");

                    List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiag(strDiagVertical, Gain);
                    List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, Gain);
                    f.Data = DiagDblH;

                    f.ShowDialog();
                    if (f.DialogResult == DialogResult.OK)
                    {
                        List<double> data = f.Data;
                        double max = data.Max();

                        StoreDiagrams(data, DiagDblV);
                        //string DiagH = HelpClasses.HelpFunction.GainedDiagToString(data, 10, Gain);
                        //objStation.Put("DIAGH", DiagH);
                        gainCell.Value = max.ToString("F3");
                    }
                    f.Dispose();
                }
            }
        }
        //================================================================================
        protected void OnPresButtonVertGain(Cell gainCell, Cell dirCell, Cell polarCell)
        {
            string strDiagVertical = objStation.GetS("DIAGV");
            string strDiagHorizontal = objStation.GetS("DIAGH");
            double Gain = objStation.GetD("GAIN");

            //Cell dirCell = grid.GetCellFromKey("dir");
            Direction = CAntennaDirection.getShortDirection(dirCell.Value);

            if (Direction == "D")
            {
                string Polarization = objStation.GetS("POLARIZATION");
                if (Polarization != "H")
                {
                    FEnterEVP f = new FEnterEVP("Ввести коефіцієнт підсилення антени (В)",
                       "Азимут, град", "Коеф. підсил., дБ");

                    List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiag(strDiagVertical, Gain);
                    List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, Gain);
                    f.Data = DiagDblV;

                    f.ShowDialog();
                    if (f.DialogResult == DialogResult.OK)
                    {
                        List<double> data = f.Data;
                        double max = data.Max();

                        StoreDiagrams(DiagDblH, data);
                        //string DiagH = HelpClasses.HelpFunction.GainedDiagToString(data, 10, Gain);
                        //objStation.Put("DIAGH", DiagH);
                        gainCell.Value = max.ToString("F3");
                    }
                    f.Dispose();
                }
            }
        }
        //====================================================================================
        protected void StoreDiagrams(List<double> HorizontalData, List<double> VerticalData)
        {
            double VertMax = 0.0;
            double HorzMax = 0.0;
            string DiagH = "";
            string DiagV = "";

            if (HorizontalData.Count == 0)
            {
                if (VerticalData.Count > 0)
                {
                    VertMax = VerticalData.Max();
                    objStation.Put("GAIN", VertMax);

                    DiagV = HelpClasses.HelpFunction.GainedDiagToString(VerticalData, 10, VertMax);
                    objStation.Put("DIAGV", DiagV);

                }
                return;
            }
            if (VerticalData.Count == 0)
            {
                if (HorizontalData.Count > 0)
                {
                    HorzMax = HorizontalData.Max();
                    objStation.Put("GAIN", HorzMax);

                    DiagH = HelpClasses.HelpFunction.GainedDiagToString(HorizontalData, 10, HorzMax);
                    objStation.Put("DIAGH", DiagH);
                }
                return;
            }

            VertMax = VerticalData.Max();
            HorzMax = HorizontalData.Max();
            double Gain = Math.Max(HorzMax, VertMax);
            objStation.Put("GAIN", Gain);

            DiagH = HelpClasses.HelpFunction.GainedDiagToString(HorizontalData, 10, Gain);
            DiagV = HelpClasses.HelpFunction.GainedDiagToString(VerticalData, 10, Gain);
            objStation.Put("DIAGH", DiagH);
            objStation.Put("DIAGV", DiagV);
        }
        //-------------------------------------------------------------------------------------
        protected string CalcERPHorz(double BasePower)
        {
            string polarization = objStation.GetS("POLARIZATION");

            if (polarization != "V")
            {
                FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (Г)",
                                     "Азимут, град", "ЕВП., дБВт");

                string strDiagVertical = objStation.GetS("DIAGV");
                string strDiagHorizontal = objStation.GetS("DIAGH");
                double Gain = objStation.GetD("GAIN");

                List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagVertical, Gain);
                List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, Gain);

                double ERPHMax = -1E+10;
                List<double> DiagDbl = new List<double>();

                //double Power = objStation.GetD("PWR_ANT");
                //double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                foreach (double GainAlpha in DiagDblH)
                {
                    double ERP = BasePower/*Power - FeederLoss*/ + GainAlpha;
                    if (ERPHMax < ERP)
                        ERPHMax = ERP;
                    DiagDbl.Add(Math.Round(ERP * 100.0) / 100.0);
                }

                f.Data = DiagDbl;
                f.ShowDialog();

                if (ERPHMax != -1E+10)
                {
                    objStation.Put("ERP_H", ERPHMax);
                    return ERPHMax.ToString("F3");
                }
                else
                {
                    objStation.Put("ERP_H", IM.NullD);
                    return "";
                }
            }
            else
            {
                objStation.Put("ERP_H", IM.NullD);
                return "не застосовується";
            }
        }
        //-------------------------------------------------------------------------------------
        protected string CalcERPVert(double BasePower)
        {
            string polarization = objStation.GetS("POLARIZATION");

            if (polarization != "H")
            {
                FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (В)",
                                     "Азимут, град", "ЕВП., дБВт");

                string strDiagVertical = objStation.GetS("DIAGV");
                string strDiagHorizontal = objStation.GetS("DIAGH");
                double Gain = objStation.GetD("GAIN");

                List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagVertical, Gain);
                List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, Gain);

                double ERPVMax = -1E+10;
                List<double> DiagDbl = new List<double>();

                //double Power = objStation.GetD("PWR_ANT");
                //double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                foreach (double GainAlpha in DiagDblV)
                {
                    double ERP = BasePower/*Power - FeederLoss*/ + GainAlpha;
                    if (ERPVMax < ERP)
                        ERPVMax = ERP;

                    DiagDbl.Add(Math.Round(ERP * 100.0) / 100.0);
                }

                f.Data = DiagDbl;
                f.ShowDialog();

                //objStation.Put("ERP_V", ERPVMax);            
                //return ERPVMax.ToString("F3");

                if (ERPVMax != -1E+10)
                {
                    objStation.Put("ERP_V", ERPVMax);
                    return ERPVMax.ToString("F3");
                }
                else
                {
                    objStation.Put("ERP_V", IM.NullD);
                    return "";
                }
            }
            else
            {
                objStation.Put("ERP_V", IM.NullD);
                return "не застосовується";
            }
        }
        //-------------------------------------------------------------------------------------
        protected string CalcERPMixed(double BasePower)
        {
            FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (М)",
                                 "Азимут, град", "ЕВП., дБВт");

            string polarization = objStation.GetS("POLARIZATION");

            string strDiagVertical = objStation.GetS("DIAGV");
            string strDiagHorizontal = objStation.GetS("DIAGH");
            double Gain = objStation.GetD("GAIN");
            List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagVertical, Gain);
            List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, Gain);

            //double Power = objStation.GetD("PWR_ANT");
            //double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
            List<double> DiagDbl = new List<double>();

            int Count = DiagDblV.Count;
            double ERPVHmax = -1E+10;
            for (int i = 0; i < Count; i++)
            {
                double ERPV = BasePower/*Power - FeederLoss*/ + DiagDblV[i];
                double ERPH = BasePower/*Power - FeederLoss*/ + DiagDblH[i];

                double ERPVH = 10.0 * Math.Log10(Math.Pow(10.0, ERPV / 10.0) + Math.Pow(10.0, ERPH / 10.0));

                if (ERPVHmax < ERPVH)
                    ERPVHmax = ERPVH;

                DiagDbl.Add(Math.Round(ERPVH * 100.0) / 100.0);
            }

            f.Data = DiagDbl;
            f.ShowDialog();

            return ERPVHmax.ToString("F3");
        }
        //----------------------------------------------------------------------------------------
        protected void UpdateHorzGain(Cell cell)
        {
            string DiagH = objStation.GetS("DIAGH");
            double Gain = objStation.GetD("GAIN");
            string Polarization = objStation.GetS("POLARIZATION");

            if (!string.IsNullOrEmpty(DiagH) && Polarization != "V")
            {
                if (DiagH.Equals("OMNI"))
                    cell.Value = Gain.ToString("F3");
                else
                {
                    List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiag(DiagH, Gain);
                    if (DiagDblH.Count > 0)
                        cell.Value = (DiagDblH.Max()).ToString("F3");
                }
            }
            else
                cell.Value = "не застосовується";
        }
        //----------------------------------------------------------------------------------------
        protected void UpdateVertGain(Cell cell)
        {
            string DiagV = objStation.GetS("DIAGV");
            double Gain = objStation.GetD("GAIN");
            string Polarization = objStation.GetS("POLARIZATION");

            if (!string.IsNullOrEmpty(DiagV) && Polarization != "H")
            {
                if (DiagV.Equals("OMNI"))
                    cell.Value = Gain.ToString("F3");
                else
                {
                    List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiag(DiagV, Gain);
                    if (DiagDblV.Count > 0)
                        cell.Value = (DiagDblV.Max()).ToString("F3");
                }
            }
            else
                cell.Value = "не застосовується";
        }
        //----------------------------------------------------------------------------------------
        public void OnValidateDirectionAndPolarization(Cell polarCell, Cell dirCell, Cell horzGainCell, Cell vertGainCell)
        {
            string dir = CAntennaDirection.getShortDirection(dirCell.Value);
            SelectByShort(dirCell, dir);
            Direction = dir;

            string polar = CPolarization.getShortPolarization(polarCell.Value);
            if (objStation.GetS("POLARIZATION") == polar)
                return;

            objStation.Put("POLARIZATION", polar);
            SelectByShort(polarCell, polar);

            if (dir.Equals("ND"))
            {
                if (polar.Equals("V"))
                {
                    objStation.Put("DIAGV", "OMNI");
                    objStation.Put("DIAGH", "");
                }

                if (polar.Equals("H"))
                {
                    objStation.Put("DIAGH", "OMNI");
                    objStation.Put("DIAGV", "");
                }

                if (polar.Equals("M"))
                {
                    objStation.Put("DIAGH", "OMNI");
                    objStation.Put("DIAGV", "OMNI");
                }
            }
            else
            {
                double Gain = objStation.GetD("GAIN");

                if (polar.Equals("V"))
                {
                    string strDiagV = objStation.GetS("DIAGV");
                    if (string.IsNullOrEmpty(strDiagV) || strDiagV.Equals("OMNI"))
                    {
                        List<double> values = new List<double>();
                        for (int i = 0; i < 36; i++)
                            values.Add(Gain);

                        strDiagV = HelpFunction.GainedDiagToString(values, 10, Gain);
                        objStation.Put("DIAGV", strDiagV);
                        objStation.Put("DIAGH", "");
                        objStation.Put("ERP_H", IM.NullD);
                    }
                }

                if (polar.Equals("H"))
                {
                    string strDiagH = objStation.GetS("DIAGH");
                    if (string.IsNullOrEmpty(strDiagH) || strDiagH.Equals("OMNI"))
                    {
                        List<double> values = new List<double>();
                        for (int i = 0; i < 36; i++)
                            values.Add(Gain);

                        strDiagH = HelpFunction.GainedDiagToString(values, 10, Gain);
                        objStation.Put("DIAGH", strDiagH);
                        objStation.Put("DIAGV", "");
                        objStation.Put("ERP_V", IM.NullD);
                    }
                }

                if (polar.Equals("M"))
                {
                    List<double> values = new List<double>();
                    for (int i = 0; i < 36; i++)
                        values.Add(Gain);

                    string strDiag = HelpFunction.GainedDiagToString(values, 10, Gain);
                    objStation.Put("DIAGH", strDiag);
                    objStation.Put("DIAGV", strDiag);
                }
            }
            AutoFillPolar(polarCell, horzGainCell, vertGainCell);
        }

        public void OnValidateGain(Cell cell, string GainType)
        {
            string strDiagVertical = objStation.GetS("DIAGV");
            string strDiagHorizontal = objStation.GetS("DIAGH");
            string Polarization = objStation.GetS("POLARIZATION");

            if (!Polarization.Equals("M"))
            {
                double Gain = 0;
                if (Double.TryParse(cell.Value, out Gain))
                {
                    List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiag(strDiagVertical, Gain);
                    List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiag(strDiagHorizontal, Gain);

                    if (DiagDblH.Count > 0 && DiagDblV.Count > 0)
                        StoreDiagrams(DiagDblH, DiagDblV);
                    else
                    {
                        objStation.Put("GAIN", Gain);
                    }
                }
            }
            else
            {
                double Gain = objStation.GetD("GAIN");
                double NewGain = 0;
                if (Double.TryParse(cell.Value, out NewGain))
                {
                    if (Gain != NewGain)
                    {
                        List<double> DiagDblV = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagVertical, Gain);
                        List<double> DiagDblH = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, Gain);

                        if (GainType.Equals("H"))
                        {
                            if (NewGain < Gain)
                            {
                                for (int i = 0; i < DiagDblH.Count(); i++)
                                    DiagDblH[i] = Math.Min(DiagDblH[i], NewGain);

                                if (DiagDblH.Count > 0)
                                {
                                    double Difference = NewGain - DiagDblH.Max();

                                    if (Difference > 0)
                                    {
                                        for (int i = 0; i < DiagDblH.Count(); i++)
                                            DiagDblH[i] += Difference;
                                    }
                                }
                            }
                            else
                            {
                                DiagDblH.Clear();
                                DiagDblH = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, NewGain);
                            }
                        }
                        else if (GainType.Equals("V"))
                        {
                            if (NewGain < Gain)
                            {
                                for (int i = 0; i < DiagDblV.Count(); i++)
                                    DiagDblV[i] = Math.Min(DiagDblV[i], NewGain);

                                if (DiagDblV.Count > 0)
                                {
                                    double Difference = NewGain - DiagDblV.Max();
                                    if (Difference > 0)
                                    {
                                        for (int i = 0; i < DiagDblV.Count(); i++)
                                            DiagDblV[i] += Difference;
                                    }
                                }
                            }
                            else
                            {
                                DiagDblV.Clear();
                                DiagDblV = HelpClasses.HelpFunction.StringToGainedDiagEx(strDiagHorizontal, NewGain);
                            }
                        }
                        StoreDiagrams(DiagDblH, DiagDblV);
                    }
                }
            }
        }

        public void SetDirection()
        {
            Direction = "";

            string DiagH = objStation.GetS("DIAGH");
            string DiagV = objStation.GetS("DIAGV");

            if (!(string.IsNullOrEmpty(DiagH) && string.IsNullOrEmpty(DiagV)))
            {
                if ((DiagV.Equals("OMNI") && (DiagH.Equals("OMNI") || string.IsNullOrEmpty(DiagH))) ||
                      (DiagH.Equals("OMNI") && string.IsNullOrEmpty(DiagV)))
                {
                    Direction = "ND";
                }
                else
                {
                    Direction = "D";
                }
            }
        }

        public override string GetProvince()
        {
            return "";
        }
    }
}
