﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    /// <summary>
    /// Клас для роботи з позицією сайта і редагуванням його властивостей для адмін. сайтів
    /// </summary>
    //??
    [Obsolete("Пересмотреть, этот класс используеться для создания новго сайта")]
    class PositionAdmSite2 : PositionState2
    {
        public string SendUser { get; set; }
        public string AcceptUser { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime AcceptDate { get; set; }

        public PositionAdmSite2()
            : base()
        {
            SendDate = IM.NullT;
            AcceptDate = IM.NullT;
        }

        public new void Save(int Status_Nic)
        {
            base.Save(Status_Nic);
            IsChanged = false;
        }

        /// <summary>
        /// Вертає список фотографій для AdmSite
        /// </summary>
        /// <returns></returns>
        public List<string> GetFotoFromCities(ref List<string> tmpList)
        {
            //List<string> tmpList = new List<string>();
            IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadOnly);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    tmpList.Add(r.GetS("PATH"));
            }
            finally
            {
                r.Final();
            }
            return tmpList;
        }

        /// <summary>
        /// Вертає список фотографій через параметр ADMS_ID
        /// </summary>
        /// <returns></returns>
        public List<string> GetFotoFromCitiesNew()
        {
            List<string> tmpList = new List<string>();
            int ADMS_ID = -1;

            if ((TableName != ICSMTbl.SITES) && (TableName != ICSMTbl.Site))
            {
                IMRecordset ADMS = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                ADMS.Select("ID,ADMS_ID");
                ADMS.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                ADMS.Open();
                try
                {
                    if (!ADMS.IsEOF())
                    {
                        ADMS_ID = ADMS.GetI("ADMS_ID");
                        IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadOnly);
                        r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
                        r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, ICSMTbl.SITES);
                        r.SetWhere("REC_ID", IMRecordset.Operation.Eq, ADMS_ID);
                        try
                        {
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                                tmpList.Add(r.GetS("PATH"));
                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                }
                finally
                {
                    ADMS.Final();
                }
            }
            else if ((TableName == ICSMTbl.SITES) || (TableName == ICSMTbl.Site))
            {
                GetFotoFromCities(ref tmpList);
            }


            return tmpList;
        }

        /// <summary>
        /// Зберегти всі фото для ВРВ_САЙТИ
        /// </summary>
        /// <param name="files"></param>
        public void SaveFotoCities(List<string> files)
        {
            //Видалити всі повторюючі фото
            IMRecordset r;
            for (int i = 0; i < files.Count; i++)
            {
                r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
                r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
                r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
                r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("PATH", IMRecordset.Operation.Eq, files[i]);
                for (r.Open();!r.IsEOF();r.MoveNext())
                    r.Delete();
                r.Final();
            }

            //Запис у файл
            r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            r.Open();            
            try
            {                
                for (int i = 0; i < files.Count;i++ )
                {
                    r.AddNew();
                    r.Put("REC_TAB",TableName);
                    r.Put("REC_ID", Id);
                    r.Put("PATH",files[i]);
                    r.Update();
                }                    
            }
            finally
            {
                r.Final();
            }            
        }

        /// <summary>
        /// Видалення потчоної фотографії
        /// </summary>
        /// <param name="s"></param>
        public void DeleteRecCities(string delPhoto)
        {
            IMRecordset r = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            r.Select("REC_TAB,REC_ID,NAME,PATH,DOC_ID");
            r.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            r.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("PATH", IMRecordset.Operation.Eq, delPhoto);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    r.Delete();
            }
            finally
            {
                r.Final();
            }                                    
        }

        /// <summary>
        /// Повертає дані, щодо статусу сайта
        /// </summary>
        /// <param name="requestAllowed"></param>
        /// <param name="confirmAllowed"></param>
        public string CheckStatus(out bool requestAllowed, out bool confirmAllowed)
        {         
            string strAdmStatus = "";
            int admsId = IM.NullI;
            IMRecordset r;
            int id = (TableName == ICSMTbl.SITES) ? Id : AdminSiteId;
            if (id != IM.NullI)
            {
                r = new IMRecordset(ICSMTbl.SITES, IMRecordset.Mode.ReadWrite);
                r.Select("ID,STATUS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        strAdmStatus = r.GetS("STATUS");
                }
                finally
                {
                    r.Final();
                }
            }
            
            confirmAllowed = (strAdmStatus == CAndE.Need || strAdmStatus == CAndE.ReqP);
            requestAllowed = !confirmAllowed && strAdmStatus != CAndE.Del;

            r = new IMRecordset(PlugTbl.XfaPositionExt, IMRecordset.Mode.ReadOnly);
            r.Select("ID,POS_ID,POS_TABLE,SEND_DATE,SEND_USER,TABLE_NAME,CHECK_DATE,CHECK_USER");
            r.SetWhere("POS_ID", IMRecordset.Operation.Eq, id);
            r.SetWhere("POS_TABLE", IMRecordset.Operation.Eq, ICSMTbl.SITES);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    SendUser = r.GetS("SEND_USER");
                    SendDate = r.GetT("SEND_DATE");
                    AcceptUser = r.GetS("CHECK_USER");
                    AcceptDate = r.GetT("CHECK_DATE");
                }                
            }
            finally
            {
                r.Final();
            }
            return strAdmStatus;
        }

        /// <summary>
        /// Зберегти параметри статусу
        /// </summary>
        public void SaveStatusAudit()
        {
            int id = (TableName == ICSMTbl.SITES) ? Id : AdminSiteId;
            if (id != IM.NullI)
            {
                IMRecordset r = new IMRecordset(PlugTbl.XfaPositionExt, IMRecordset.Mode.ReadWrite);
                r.Select("ID,POS_ID,POS_TABLE,SEND_DATE,SEND_USER,TABLE_NAME,CHECK_DATE,CHECK_USER");
                r.SetWhere("POS_ID", IMRecordset.Operation.Eq, id);
                r.SetWhere("POS_TABLE", IMRecordset.Operation.Eq, ICSMTbl.SITES);
                try
                {
                    r.Open();
                    if (r.IsEOF())
                    {
                        r.AddNew();
                        r.Put("ID", IM.AllocID(PlugTbl.XfaPositionExt, 1, -1));
                        r.Put("POS_ID", id);
                        r.Put("POS_TABLE", ICSMTbl.SITES);
                        r.Put("TABLE_NAME", PlugTbl.XfaPositionExt);
                    }
                    else
                        r.Edit();

                    r.Put("CHECK_DATE", AcceptDate);
                    r.Put("CHECK_USER", AcceptUser);
                    r.Put("SEND_DATE", SendDate);
                    r.Put("SEND_USER", SendUser);
                    r.Update();
                }
                finally
                {
                    r.Final();
                }
            }
        }

        public void SetStatusSites(string status)
        {
            IMRecordset r= new IMRecordset(ICSMTbl.SITES,IMRecordset.Mode.ReadWrite);
            r.Select("ID,STATUS");
            int id = (TableName == ICSMTbl.SITES) ? Id : AdminSiteId;
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("STATUS", status);
                    r.Update();

                    Status = status;

                    SaveStatusAudit();
                }
            }
            finally
            {
                r.Final();
            }
        }
    }
}
