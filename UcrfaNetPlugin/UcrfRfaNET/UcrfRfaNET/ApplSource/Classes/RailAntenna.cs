﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    public class RailAntenna : Antenna
    {
        /// <summary>
        /// Коефіцієнт підсилення
        /// </summary>
        public double Gain { get; set; }

        /// <summary>
        /// Ширина ДС
        /// </summary>
        public double BeamWidth { get; set; }

        /// <summary>
        /// Загрузить
        /// </summary>
        public override void Load()
        {
            _tableName = PlugTbl.XfaAnten;
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,GAIN,H_BEAMWIDTH");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    Name = r.GetS("NAME");
                    Gain = r.GetD("GAIN");
                    BeamWidth = r.GetD("H_BEAMWIDTH");
                }
            }
            finally
            {
                r.Final();
            }
        }

        /// <summary>
        /// Сохранить антену
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(PlugTbl.XfaAnten, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,NAME,GAIN,H_BEAMWIDTH");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                    r.Edit();
                else
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(PlugTbl.XfaAnten, 1, -1));
                }
                r.Put("NAME", Name);
                r.Put("GAIN", Gain);
                r.Put("H_BEAMWIDTH", BeamWidth);
                r.Update();
            }
            finally
            {
                r.Final();
            }

        }
    }
}
