﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows.Forms;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    //===============================================================
    //===============================================================
    /// <summary>
    /// Класс для залізниці
    /// </summary>
    /// 	
    internal class RailwayAppl : BaseAppClass
    {
        public static readonly string TableName = PlugTbl.XfaAbonentStation;

        protected string RadioTech;

        private RailwayApplStation railStation;
        private List<RailwayApplStation> railStatList = new List<RailwayApplStation>();

        private List<int> idStatList;

        private int countSector = 1;

        #region _cell

        /// <summary>
        /// Сектор
        /// </summary>
        private CellStringProperty _cellParam1 = new CellStringProperty();
        private CellStringProperty _cellParam2 = new CellStringProperty();
        private CellStringProperty[] _cellParamArray = new CellStringProperty[2];

        /// <summary>
        /// Радіотехнологія(Стандарт)
        /// </summary>
        private CellStringComboboxAplyProperty _cellRadioTech1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty _cellRadioTech2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty[] _cellRadioTechArray = new CellStringComboboxAplyProperty[2];


        /// <summary>
        /// Означення станції
        /// </summary>
        private CellStringComboboxAplyProperty _cellNetClassStat = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Клас станції
        /// </summary>
        private CellStringComboboxAplyProperty _cellClassStat = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty _cellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty _cellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty _cellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса встановлення РЕЗ
        /// </summary>
        private CellStringProperty _cellAddress = new CellStringProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty _cellEquipmentName1 = new CellStringProperty();
        private CellStringProperty _cellEquipmentName2 = new CellStringProperty();
        private CellStringProperty[] _cellEquipmentNameArray = new CellStringProperty[2];

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleProperty _cellPowerTr1 = new CellDoubleProperty();
        private CellDoubleProperty _cellPowerTr2 = new CellDoubleProperty();
        private CellDoubleProperty[] _cellPowerTrArray = new CellDoubleProperty[2];

        /// <summary>
        /// Клас випромінювання               
        /// </summary>        
        private CellStringProperty _cellTxDesignEmission1 = new CellStringProperty();
        private CellStringProperty _cellTxDesignEmission2 = new CellStringProperty();
        private CellStringProperty[] _cellTxDesignEmissionArray = new CellStringProperty[2];

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty _cellCertificateNumber1 = new CellStringProperty();
        private CellStringProperty _cellCertificateNumber2 = new CellStringProperty();
        private CellStringProperty[] _cellCertificateNumberArray = new CellStringProperty[2];
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty _cellCertificateDate1 = new CellDateNullableProperty();
        private CellDateNullableProperty _cellCertificateDate2 = new CellDateNullableProperty();
        private CellDateNullableProperty[] _cellCertificateDateArray = new CellDateNullableProperty[2];
        /// <summary>
        /// Тип антени
        /// </summary>        
        private CellStringProperty _cellAntennaType = new CellStringProperty();

        /// <summary>
        /// Коефіцієнт підсилення антени, дБі
        /// </summary>        
        private CellDoubleBoundedProperty _cellMaxKoef = new CellDoubleBoundedProperty();
        /// <summary>
        /// Ширина ДС 
        /// </summary>        
        private CellDoubleBoundedProperty _cellWidthAntenna = new CellDoubleBoundedProperty();

        /// <summary>
        /// Висота антени над рівнем землі, м
        /// </summary>                
        private CellDoubleProperty _cellHighAntenna = new CellDoubleProperty();

        /// <summary>
        /// Азимут випромінювання антени, град 
        /// </summary>
        private CellDoubleBoundedProperty _cellAglMaxEmission = new CellDoubleBoundedProperty();

        /// <summary>
        /// Позивний
        /// </summary>
        private CellStringProperty _cellCall = new CellStringProperty();

        /// <summary>
        /// Частоти передавання, МГц
        /// </summary>
        private CellStringProperty _cellNominSend1 = new CellStringProperty();
        private CellStringProperty _cellNominSend2 = new CellStringProperty();
        private CellStringProperty[] _cellNominSendArray = new CellStringProperty[2];

        /// <summary>
        /// Частоти приймання, МГц
        /// </summary>
        private CellStringProperty _cellNominAccept1 = new CellStringProperty();
        private CellStringProperty _cellNominAccept2 = new CellStringProperty();
        private CellStringProperty[] _cellNominAcceptArray = new CellStringProperty[2];

        private CellStringProperty _cellObj1 = new CellStringProperty();
        private CellStringProperty _cellObj2 = new CellStringProperty();
        private CellStringProperty[] _cellObjArray = new CellStringProperty[2];

        #endregion

        protected List<double> TxStationFreq = new List<double>();
        protected List<double> RxStationFreq = new List<double>();
        protected string UserTech = "Технологічні";
        protected string UserTelecom = "Комунікації";

        public RailwayAppl(int id, int ownerId, int packetID, string radioTech)
            : base(id, TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTR_З;
            department = CUsers.GetUserDepartmentType(); //#3839
            departmentSector = DepartmentSectorType.VMA_TR3;
            RadioTech = radioTech;

            FillArrayOfSector();

            idStatList = RailwayApplStation.GetIdStat(applID);

            if (idStatList.Count < 1)
            {
                idStatList.Add(recID);
                RailwayApplStation tempRailStat = new RailwayApplStation();
                if (idStatList.Count > 0)
                    tempRailStat.Id = idStatList[0];
                tempRailStat.Load();
                railStatList.Add(tempRailStat);
            }
            else
            {
                for (int i = 0; i < idStatList.Count; i++)
                {
                    RailwayApplStation tempRailStat = new RailwayApplStation();
                    tempRailStat.Id = idStatList[i];
                    tempRailStat.Load();
                    railStatList.Add(tempRailStat);
                }
            }
            _cellCall.Value = "";
            ReadData();

            Status.StatusAsString = railStation.Status;

            if ((applID != 0) && (applID != IM.NullI))
                railStation.APPL_ID = applID;


            if ((ownerId != 0) && (ownerId != IM.NullI))
                railStation.OwnerId = ownerId;

            if(railStatList.Count > 0)
            {
                SCVisnovok = railStatList[0].ScVisnovok;
                SCDozvil = railStatList[0].ScDozvil;
                SCNote = railStatList[0].ScNote;
            }
            
        }

        /// <summary>
        /// Початкове завантаження даних
        /// </summary>
        private void ReadData()
        {
            _cellClassStat.Value = "FL - Стаціонарна";
            _cellParam1.Value = "РЕЗ 1";
            _cellMaxKoef.Value = railStatList[0].RailStationAntena.Gain.ToStringNullD();
            _cellWidthAntenna.Value = railStatList[0].RailStationAntena.BeamWidth.ToStringNullD();

            railStation = railStatList[0];
            if (RadioTech == "КХ")
            {
                _cellRadioTech1.Value = railStation.StandartList[0];
                _cellRadioTech2.Value = railStation.StandartList[0];
            }
            else if (RadioTech == "УКХ")
            {
                _cellRadioTech1.Value = railStation.StandartList[1];
                _cellRadioTech2.Value = railStation.StandartList[1];
            }
            _cellRadioTech1.Items.AddRange(railStation.StandartList);
            _cellRadioTech2.Items.AddRange(railStation.StandartList);
            _cellLatitude.DoubleValue = railStation.objPosition != null ? railStation.objPosition.LatDms : IM.NullD;
            _cellLongitude.DoubleValue = railStation.objPosition != null ? railStation.objPosition.LongDms : IM.NullD;
            _cellASL.DoubleValue = railStation.objPosition != null ? railStation.objPosition.Asl : IM.NullD;
            _cellAddress.Value = railStation.objPosition != null ? railStation.objPosition.FullAddressAuto : "";
            if (railStation != null)
            {
                _cellCall.Value = railStation.Call;
                _cellAglMaxEmission.DoubleValue = railStation.Azimuth;
                _cellHighAntenna.DoubleValue = railStation.Agl;
                if (railStation.BrunchCode != null) { _cellNetClassStat.Value = railStation.BrunchCode; }
            }

            if (_cellAglMaxEmission.Value == "0" || string.IsNullOrEmpty(_cellAglMaxEmission.Value))
                _cellAglMaxEmission.Value = "ND";
            _cellObj1.Value = recordID.Id.ToString();
           if (railStatList.Count>1)
            _cellObj2.Value = railStatList[1].Id.ToString();
            RxStationFreq.Clear();
            TxStationFreq.Clear();
        }

        /// <summary>
        /// Створення масивів комірок для роботи з ними поіндексно
        /// </summary>
        private void FillArrayOfSector()
        {
            _cellCertificateDateArray[0] = _cellCertificateDate1;
            _cellCertificateDateArray[1] = _cellCertificateDate2;
            _cellCertificateNumberArray[0] = _cellCertificateNumber1;
            _cellCertificateNumberArray[1] = _cellCertificateNumber2;
            _cellEquipmentNameArray[0] = _cellEquipmentName1;
            _cellEquipmentNameArray[1] = _cellEquipmentName2;
            _cellNominAcceptArray[0] = _cellNominAccept1;
            _cellNominAcceptArray[1] = _cellNominAccept2;
            _cellNominSendArray[0] = _cellNominSend1;
            _cellNominSendArray[1] = _cellNominSend2;
            _cellParamArray[0] = _cellParam1;
            _cellParamArray[1] = _cellParam2;
            _cellPowerTrArray[0] = _cellPowerTr1;
            _cellPowerTrArray[1] = _cellPowerTr2;
            _cellRadioTechArray[0] = _cellRadioTech1;
            _cellRadioTechArray[1] = _cellRadioTech2;
            _cellTxDesignEmissionArray[0] = _cellTxDesignEmission1;
            _cellTxDesignEmissionArray[1] = _cellTxDesignEmission2;
            _cellObjArray[0] = _cellObj1;
            _cellObjArray[1] = _cellObj2;
        }

        /// <summary>
        /// Показує додатковий сектор
        /// </summary>
        private void ShowSector2()
        {
            _cellParam1.Value = "РЕЗ 1";
            _cellParam2.Value = "РЕЗ 2";
            _cellParam2.Cell.Visible = true;
            //_cellParam2.Cell.TabOrder = 12;
            _cellCertificateDate2.Cell.Visible = true;
            _cellCertificateNumber2.Cell.Visible = true;
            _cellEquipmentName2.Cell.Visible = true;
            _cellEquipmentName2.Cell.TabOrder = 12;
            _cellNominAccept2.Cell.Visible = true;            
            _cellNominSend2.Cell.Visible = true;
            _cellNominSend2.Cell.TabOrder = 13;
            _cellPowerTr2.Cell.Visible = true;
            //_cellPowerTr2.Cell.TabOrder = 14;
            _cellRadioTech2.Cell.Visible = true;
            _cellTxDesignEmission2.Cell.Visible = true;
            _cellObj2.Cell.Visible = true;
            countSector = 2;
        }

        /// <summary>
        /// Повертає XML форми
        /// </summary>
        /// <returns></returns>
        protected override string GetXMLParamGrid() { return Properties.Resources.RailwayAppl; }
        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (railStation.objPosition != null) ? railStation.objPosition.Province : "";
            string city = (railStation.objPosition != null) ? railStation.objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.GetListDocNumberForDozvWma(1)[0];

            if (docType == DocType.DOZV )
                fullPath += string.Format("САЗ-{0}-{1}.rtf", HelpFunction.getAreaCode(prov, city), number);
            else if (docType == DocType.VISN)
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            return "TRAIN";
        }
        //============================================================
        /// <summary>
        /// Реакція на вибір меню "Додати сектор"
        /// </summary>
        /// <param name="grid"></param>
        public override void AddSector(Grid grid)
        {
            _cellRadioTech2.Value = _cellRadioTech1.Value;
            _cellPowerTr2.Value = "";
            _cellCertificateDate2.Value = "";
            _cellCertificateNumber2.Value = "";
            _cellEquipmentName2.Value = "";
            _cellTxDesignEmission2.Value = "";
            _cellObj2.Value = "";
            if (railStatList.Count == 1)
            {
                railStatList.Add(new RailwayApplStation());
                railStatList[railStatList.Count - 1].Load();
            }
            _cellCertificateDateArray[1] = _cellCertificateDate2;
            _cellCertificateNumberArray[1] = _cellCertificateNumber2;
            _cellEquipmentNameArray[1] = _cellEquipmentName2;
            _cellNominAcceptArray[1] = _cellNominAccept2;
            _cellNominSendArray[1] = _cellNominSend2;
            _cellParamArray[1] = _cellParam2;
            _cellPowerTrArray[1] = _cellPowerTr2;
            _cellRadioTechArray[1] = _cellRadioTech2;
            _cellTxDesignEmissionArray[1] = _cellTxDesignEmission2;
            _cellObjArray[1] = _cellObj2;
            _cellObjArray[0].Value = railStatList[0].Id.ToString();
            _cellObjArray[1].Value = railStatList[1].Id.ToString();
            ShowSector2();
            grid.Invalidate();
        }



        /// <summary>
        /// Реакція на вибір меню "Видалити сектор"
        /// </summary>
        /// <param name="grid"></param>
        public override void RemoveSector(Grid grid)
        {
            List<string> sAr = new List<string>();
            Row rowParam = grid.GetRow(0) as Row;
            if (rowParam != null)
            {
                foreach (Cell cell in rowParam.cellList)
                {
                    if (cell.Value.StartsWith("РЕЗ"))
                        sAr.Add(cell.Value);
                }

                RemoveSectorForm rSm = new RemoveSectorForm(sAr.ToArray());
                rSm.ShowDialog();
                if (rSm.DialogResult == DialogResult.OK)
                {
                    if (rSm.cmbBoxRemove.SelectedIndex == 0)
                    {
                        _cellCertificateDate1.Value = _cellCertificateDate2.Value;
                        _cellCertificateNumber1.Value = _cellCertificateNumber2.Value;
                        _cellEquipmentName1.Value = _cellEquipmentName2.Value;
                        _cellNominAccept1.Value = _cellNominAccept2.Value;
                        _cellNominSend1.Value = _cellNominSend2.Value;
                        _cellPowerTr1.Value = _cellPowerTr2.Value;
                        _cellRadioTech1.Value = _cellRadioTech2.Value;
                        _cellTxDesignEmission1.Value = _cellTxDesignEmission2.Value;
                        _cellObj1.Value = _cellObj2.Value;
                    }
                }
            }
            HideSector2();
            grid.Invalidate();
        }
        /// <summary>        
        /// Формування адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            CellValidate(cell, gridParam);
            PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, PlugTbl.XfaPosition, 1, _cellLongitude.DoubleValue, _cellLatitude.DoubleValue);
            if (newPos != null)
            {
                railStation.objPosition = newPos;
                _cellAddress.Value = railStation.objPosition.FullAddressAuto;
                _cellLongitude.DoubleValue = railStation.objPosition.LongDms;
                _cellLatitude.DoubleValue = railStation.objPosition.LatDms;
                _cellASL.DoubleValue = railStation.objPosition.Asl;
            
            }
        }

        /// <summary>        
        /// Створення нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            if (ShowMessageReference(railStation.objPosition != null))
            {
                PositionState2 newPos = new PositionState2();
                newPos.LongDms = _cellLongitude.DoubleValue;
                newPos.LatDms = _cellLatitude.DoubleValue;
                DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(PlugTbl.XfaPosition, ref newPos, OwnerWindows);
                if (dr == DialogResult.OK)
                {
                    railStation.objPosition = newPos;
                    _cellAddress.Value = railStation.objPosition.FullAddressAuto;
                    _cellLatitude.DoubleValue = railStation.objPosition.LatDms;
                    _cellLongitude.DoubleValue = railStation.objPosition.LongDms;
                    _cellASL.DoubleValue = railStation.objPosition.Asl;
            
                }
            }
        }

        /// <summary>
        /// Перевірка на число частоти
        /// </summary>
        /// <param name="freqList"></param>
        /// <returns></returns>
        private bool CheckValidityFrequency(List<double> freqList)
        {
            foreach (double freq in freqList)
                if (freq <= 0.0)
                    return false;
            return true;
        }

        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            RailwayAppl rwAppl = new RailwayAppl(0, railStation.OwnerId, packetID, radioTech);
            for (int i = 0; i < this.railStatList.Count; i++)
            {
                RailwayApplStation srcSt = this.railStatList[i];
                while (rwAppl.railStatList.Count < i + 1)
                    rwAppl.railStatList.Add(new RailwayApplStation());
                while (rwAppl.idStatList.Count < i + 1)
                    rwAppl.idStatList.Add(IM.NullI);
                RailwayApplStation dstSt = rwAppl.railStatList[i];
                if (i > 0) // 0 уже загружен
                    dstSt.Load(IM.NullI); // так надо, не спрашивай

                dstSt.Agl = srcSt.Agl;
                dstSt.AntId = srcSt.AntId;
                dstSt.ApplId = srcSt.ApplId;
                dstSt.Azimuth = srcSt.Azimuth;
                dstSt.Call = srcSt.Call;
                dstSt.CertDate = srcSt.CertDate;
                dstSt.CertNumb = srcSt.CertNumb;
                dstSt.ClassStat = srcSt.ClassStat;
                dstSt.ClassStationDict = srcSt.ClassStationDict;
                dstSt.Defin = srcSt.Defin;
                dstSt.DesigEmiss = srcSt.DesigEmiss;
                dstSt.Duplex = srcSt.Duplex;
                dstSt.EquipId = srcSt.EquipId;
                dstSt.Fider = srcSt.Fider;
                dstSt.Finding = srcSt.Finding;
                dstSt.FreqRxList = srcSt.FreqRxList;
                dstSt.FreqTxList = srcSt.FreqTxList;
                dstSt.Koef = srcSt.Koef;
                dstSt.NAccept = srcSt.NAccept;
                dstSt.NameRez = srcSt.NameRez;
                //dstSt.NSend = srcSt.NSend;
                //dstSt.NumBlank = srcSt.NumBlank;
                //dstSt.ObjRchp = srcSt.ObjRchp;
                dstSt.OwnerId = srcSt.OwnerId;
                //dstSt.PosId = srcSt.PosId;
                //dstSt.objPosition = srcSt.objPosition;
                dstSt.RailStationAntena = srcSt.RailStationAntena;
                dstSt.RailStationEquip = srcSt.RailStationEquip;
                dstSt.RxListId = srcSt.RxListId;
                dstSt.ScNote = srcSt.ScNote;
                dstSt.Standart = srcSt.Standart;
                dstSt.TypeAnten = srcSt.TypeAnten;
                dstSt.UserId = srcSt.UserId;
                dstSt.WideAnten = srcSt.WideAnten;
            }
            rwAppl.ReadData();
            return rwAppl;
        }

        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            RailwayAppl rwAppl = newAppl as RailwayAppl;
            return newAppl;
        }
        
        /// <summary>
        /// Перевірка на правильне введення висоти
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="val"></param>
        public void OnBeforeChangeHigh(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal))
                val = cell.Value;
        }



        /// <summary>
        /// Перевірка на правильне введення частоти
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="val"></param>
        public void OnBeforeChangeFreq(Cell cell, ref string val)
        {
            ChangeColor(cell, Colors.okvalue);
            List<double> freqList = ConvertType.ToDoubleList(val);

            if (freqList.Count > 0)
                if (CheckValidityFrequency(freqList))
                    val = HelpFunction.ToString(freqList);
                else
                {
                    ChangeColor(cell, Colors.badValue);
                    val = HelpFunction.ToString(freqList);
                }
            else
                val = cell.Value;
        }

        /// <summary>
        /// Формування частот приймання на основі частот передавання
        /// </summary>
        /// <param name="cell"></param>
        public void OnAfterChangeFreq(Cell cell)
        {
            int columnNumb = cell.Col;
            _cellNominAcceptArray[columnNumb - 1].Value = _cellNominSendArray[columnNumb - 1].Value;
        }


        public void OnBeforeChangeCall(Cell cell, ref string val)
        {
            if (string.IsNullOrEmpty(val))
                val = cell.Value;
        }


        ///// <summary>
        ///// Перевірка на правильне введення потужності
        ///// </summary>
        ///// <param name="cell"></param>
        ///// <param name="val"></param>
        public void OnBeforeChangePower(Cell cell, ref string val)
        {
            double newVal1;
            if (Double.TryParse(val, out newVal1))
            {
                if (val.ToDouble(IM.NullD).Round(0) > railStatList[cell.Col - 1].RailStationEquip.MaxPower[PowerUnits.W].Round(0))
                    cell.BackColor = Color.Pink;
                else
                    cell.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Перевірка на правильне введення азимуту
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="val"></param>
        public void OnBeforeChangeAzimuth(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0 || newVal >= 360)
                val = cell.Value;
            if (val == "0")
                val = "ND";
        }

        /// <summary>
        /// Вибір нової антени
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressAntena(Cell cell)
        {
            string tableAnten = PlugTbl.XfaAnten;
            IMTableRight rights = IM.TableRight(tableAnten);
            if (!(((rights & IMTableRight.Insert) == IMTableRight.Insert) &&
                 ((rights & IMTableRight.Update) == IMTableRight.Update)))
                tableAnten = PlugTbl.XvAnten;
            string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cell.Value) + "*\"}";
            RecordPtr recAnt = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), tableAnten, param);
            if ((recAnt.Id > 0) && (recAnt.Id < IM.NullI))
            {
                railStation.RailStationAntena = new RailAntenna();
                railStation.RailStationAntena.Id = recAnt.Id;
                railStation.RailStationAntena.TableName = recAnt.Table;
                railStation.RailStationAntena.Load();
                railStation.AntId = railStation.RailStationAntena.Id;
                _cellAntennaType.Value = railStation.RailStationAntena.Name;
                _cellMaxKoef.Value = railStation.RailStationAntena.Gain.ToStringNullD();
                _cellWidthAntenna.Value = railStation.RailStationAntena.BeamWidth.ToStringNullD();
            }
        }

        /// <summary>
        /// Вибір нового обладнання
        /// </summary>
        /// <param name="cell">Комірка</param>
        public void OnPressEquipment(Cell cell)
        {
            int id = cell.Col;
            string tableEquip = PlugTbl.XfaEquip;
            IMTableRight rights = IM.TableRight(tableEquip);
            if (!(((rights & IMTableRight.Insert) == IMTableRight.Insert) &&
                 ((rights & IMTableRight.Update) == IMTableRight.Update)))
                tableEquip = PlugTbl.XvEquip;

            RecordPtr recEquip = SelectEquip("Seach of the equipment", tableEquip, "NAME", cell.Value, false);
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                if (railStatList.Count > id - 1)
                {
                    railStatList[id - 1].RailStationEquip.Id = recEquip.Id;
                    railStatList[id - 1].RailStationEquip.Load();
                    _cellEquipmentNameArray[id - 1].Value = railStatList[id - 1].RailStationEquip.Name;
                    _cellTxDesignEmissionArray[id - 1].Value = railStatList[id - 1].RailStationEquip.DesigEmission;
                    _cellPowerTrArray[id - 1].DoubleValue = railStatList[id - 1].RailStationEquip.MaxPower[PowerUnits.W];
                    _cellCertificateDateArray[id - 1].DateValue = railStatList[id - 1].RailStationEquip.Certificate.Date;
                    _cellCertificateNumberArray[id - 1].Value = railStatList[id - 1].RailStationEquip.Certificate.Symbol;
                }
            }
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            railStation.Status = Status.StatusAsString;
            //Формування запису для таблиці станцій
            if (countSector > idStatList.Count)
            {
               // railStatList[railStatList.Count - 1].Id = RailwayApplStation.GetNewId();
                // railStatList[railStatList.Count - 1].Load();
                idStatList.Add(railStatList[railStatList.Count - 1].Id);
            }
            else if (countSector < idStatList.Count)
            {
                RailwayApplStation.RemoveRecFromTable(railStatList.Count, ApplID);
                railStatList.RemoveAt(railStatList.Count - 1);
                idStatList.RemoveAt(countSector);
            }
            
            for (int i = 0; i < countSector; i++)
            {
                railStatList[i].DesigEmiss = _cellTxDesignEmissionArray[i].Value;
                railStatList[i].Standart = _cellRadioTechArray[i].Value;
                railStatList[i].PosId = railStation.objPosition != null ? railStation.objPosition.Id : IM.NullI;
                railStatList[i].EquipId = railStatList[i].RailStationEquip.Id;
                railStatList[i].AntId = railStation.RailStationAntena.Id;
                railStatList[i].objPosition = railStation.objPosition;
                railStatList[i].ClassStat = "0";
                if (string.IsNullOrEmpty(_cellCall.Value))
                    _cellCall.Value = "";
                railStatList[i].Call = _cellCall.Value;
                railStatList[i].Agl = _cellHighAntenna.DoubleValue;
                railStatList[i].BrunchCode = _cellNetClassStat.Value;
               

                if (string.IsNullOrEmpty(_cellAglMaxEmission.Value) || _cellAglMaxEmission.Value=="ND")
                    railStatList[i].Azimuth = 0;
                else
                    railStatList[i].Azimuth = _cellAglMaxEmission.DoubleValue;
                railStatList[i].NAccept = _cellNominAcceptArray[i].Value;
                railStatList[i].NSend = _cellNominSendArray[i].Value;
                railStatList[i].Power[PowerUnits.W] = _cellPowerTrArray[i].DoubleValue;
                railStatList[i].ScVisnovok = SCVisnovok;
                railStatList[i].ScDozvil = SCDozvil;
                railStatList[i].ScNote = SCNote;
                railStatList[i].Save();
            }
            //Формування зсилки для XNRFA_APPL
            RailwayApplStation.RemoveAllRec(applID);
            for (int i = 0; i < countSector; i++)
                RailwayApplStation.SetRecToTable(applID, i, railStatList[i].Id);

            return true;
        }

        protected override void ShowObject(Cell cell)
        {
            int currId = cell.Col;
            if (cell.Key == "NewKey-KNameRez1" || cell.Key == "NewKey-KNameRez2")
            {
                using (AbonentEquipForm frm = new AbonentEquipForm(railStatList[currId - 1].RailStationEquip.Id))
                    if (frm.ShowDialog() == DialogResult.OK)
                        frm.Save();
            }

            if (cell.Key=="NewKey-KAntenType")
            {
                using (RailAntenForm frm=new RailAntenForm(railStation.AntId))
                    if (frm.ShowDialog() == DialogResult.OK)
                        frm.Save();
            }

            if (cell.Key == "KAddr")
            {
                if (railStation.objPosition != null)
                {
                    Forms.AdminSiteAllTech2.Show(railStation.objPosition.TableName, railStation.objPosition.Id, (IM.TableRight(railStation.objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                    railStation.objPosition.LoadStatePosition(railStation.objPosition.Id, railStation.objPosition.TableName);
                    _cellAddress.Value = railStation.objPosition.FullAddressAuto;
                    _cellLongitude.DoubleValue = railStation.objPosition.LongDms;
                    _cellLatitude.DoubleValue = railStation.objPosition.LatDms;
                    _cellASL.DoubleValue = railStation.objPosition.Asl;
                }
            }
        }
        //===========================================================
        /// <summary>
        /// Обновление данных поля РДПО владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerRDPO(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,REGIST_NUM");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, railStation.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("REGIST_NUM");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }

        //===========================================================
        /// <summary>
        /// Отобразить данные о владельце
        /// </summary>
        public override void ShowOperator()
        {
            if ((railStation.OwnerId > 0) && (railStation.OwnerId != IM.NullI))
            {
                RecordPtr rcPtr = new RecordPtr(ICSMTbl.itblUsers, railStation.OwnerId);
                rcPtr.UserEdit();
            }
        }

        //===========================================================
        /// <summary>
        /// Обновление данных поля Названия владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerName(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,NAME");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, railStation.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("NAME");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }

        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
            IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CUST_TXT1,CUST_DAT1");
            r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("CUST_TXT1", NumberOut);
                    r.Put("CUST_DAT1", DateOut);
                    r.Update();
                }
            }
            finally
            {
                r.Final();
            }
        }

        /// <summary>
        /// Сховати додатковий сектор
        /// </summary>
        private void HideSector2()
        {
            _cellParam2.Cell.Visible = false;
            _cellParam2.Cell.TabOrder = 0;
            _cellCertificateDate2.Cell.Visible = false;
            _cellCertificateDate2.Cell.TabOrder = 0;
            _cellCertificateNumber2.Cell.Visible = false;
            _cellCertificateNumber2.Cell.TabOrder = 0;
            _cellEquipmentName2.Cell.Visible = false;
            _cellEquipmentName2.Cell.TabOrder = 0;
            _cellNominAccept2.Cell.Visible = false;
            _cellNominAccept2.Cell.TabOrder = 0;
            _cellNominSend2.Cell.Visible = false;
            _cellNominSend2.Cell.TabOrder = 0;
            _cellPowerTr2.Cell.Visible = false;
            _cellPowerTr2.Cell.TabOrder = 0;
            _cellRadioTech2.Cell.Visible = false;
            _cellRadioTech2.Cell.TabOrder = 0;
            _cellTxDesignEmission2.Cell.Visible = false;
            _cellTxDesignEmission2.Cell.TabOrder = 0;
            _cellObj2.Cell.Visible = false;
            _cellObj2.Cell.TabOrder = 0;
            countSector = 1;
        }

        /// <summary>
        /// Ініціалізація додаткових парметрів(після того, як сформовані всі клітинки)
        /// </summary>
        /// <param name="grid"></param>
        public override void InitParamGrid(Grid grid)
        {
            countSector = 2;
            if (idStatList.Count == 1)
            {
                HideSector2();
                countSector = 1;
            }

            for (int i = 0; i < idStatList.Count; i++)
            {
                if (!string.IsNullOrEmpty(railStatList[i].Standart))
                    _cellRadioTechArray[i].Value = railStatList[i].Standart;
                _cellParamArray[i].Value = "РЕЗ " + (i + 1);
                _cellEquipmentNameArray[i].Value = railStatList[i].RailStationEquip.Name;
                _cellTxDesignEmissionArray[i].Value = railStatList[i].RailStationEquip.DesigEmission;
                _cellPowerTrArray[i].DoubleValue = railStatList[i].RailStationEquip.MaxPower[PowerUnits.W];
                _cellCertificateDateArray[i].DateValue = railStatList[i].RailStationEquip.Certificate.Date;
                _cellCertificateNumberArray[i].Value = railStatList[i].RailStationEquip.Certificate.Symbol;
                _cellNominAcceptArray[i].Value = railStatList[i].NAccept;
                _cellNominSendArray[i].Value = railStatList[i].NSend;
                _cellPowerTrArray[i].DoubleValue = railStatList[i].Power[PowerUnits.W];
            }
            _cellAntennaType.Value = railStation.RailStationAntena.Name;
            gridParam = grid;
        }

        public override void InitParamGrid(Cell cell) { }

        //============================================================
        /// <summary>
        /// Печать документа из заявки
        /// </summary>
        public override void PrintDoc()
        {
            // Проверка ни зменения статуса заявки.
            if ((gridParam.Edited || IsChangeDop) &&
               (MessageBox.Show(CLocaliz.TxT("You must to save this appl before printing the document. Do you want to save the appl?"), CLocaliz.TxT("Print document"), MessageBoxButtons.YesNo) == DialogResult.Yes)
              )
            {
                this.OnSaveAppl();
            }

            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr(recordID.Table, recordID.Id), (IsTechnicalUser == 1), applID, appType, false);
            if (frmReport.ShowDialog() == DialogResult.OK)
            {
                string docType = frmReport.getDocType();
                string docName = getOutDocName(docType, "");
                //Получить имя файла
                if (docType == DocType.DOZV)
                {
                    string docNameEmpty = docName.ToFileNameWithoutExtension();                    
                    SaveFileNumber(docNameEmpty, recordID.Table, recordID.Id);
                }

                DateTime startDate = frmReport.getStartDate();
                DateTime endDate = frmReport.getEndDate();
                
                if (docType == DocType.DOZV_CANCEL)
                    endDate = IM.NullT;
                try
                {
                    
                    string dopFilter = getDopDilter();
                    SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
                    PrintDocs printDocs = new PrintDocs();
                    if (printDocs.CreateOneReport(docType, appType, startDate, endDate, PlugTbl.itblXnrfaAppl, applID, dopFilter, docName, true))
                    {
                        SaveAfterDocPrint(frmReport);
                        UpdateEventGrid();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="_newOwnerID">ID нового владельца</param>
        public override void ChangeOwner(int _newOwnerID)
        {
            foreach (RailwayApplStation obj in railStatList)
            {
                if (obj.OwnerId != _newOwnerID)
                {
                    obj.OwnerId = _newOwnerID;
                    //CJournal.CheckTable(recordID.Table, obj.Id, obj);
                    obj.Save();
                }
            }
            //CJournal.CreateReport(applID, appType);
        }

        public override string GetProvince()
        {
            return "";
        }
   
    }
}
