﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{

    class StationSectorCollection<T> where T : new() //: ICollection<BaseStation>
    {
        private List<T> _sectors = new List<T>();
        public int ApplID { get; set; }        

        public StationSectorCollection()//CreateSectorDelegate createSectorProcedure)
        {
        }

        public T GetSectorByIndex(int sectorIndex)
        {
            if (sectorIndex >= 0 && sectorIndex < _sectors.Count)
                return _sectors[sectorIndex];

            throw new Exception("Bad sector index");
        }

        public int GetSectorCount()
        {
            return _sectors.Count;
        }

        public void Add(T newStation)
        {
            _sectors.Add(newStation);
        }

        public void Clear()
        {
            _sectors.Clear();
        }

        public void LoadSectors()
        {
            _sectors.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ApplID);
            r.Open();

            //int Loaded = 1;
            if (!r.IsEOF())
            {// Заявки нет     

                for (int i = 1; i <= 6; i++)
                {
                    int sectorID = r.GetI("OBJ_ID" + i.ToString());
                    if (sectorID != IM.NullI)
                    {
                        T sector = new T();//_createSectorProcedure();
                        BaseStation station = sector as BaseStation;
                        if (station != null)
                        {
                            station.Id = sectorID;                            
                            station.Load();
                            _sectors.Add(sector);
                        }
                        else
                        {
                            throw new Exception("Malformed template type for StationSectorCollection");
                        }
                    }
                }
            }
            r.Close();
            r.Destroy();
        }

        public void LoadSectorsOrSingle(int stationId)
        {
            LoadSectors();

            if (_sectors.Count == 0)
            {
                T sector = new T();
                BaseStation station = sector as BaseStation;
                if (station != null)
                {
                    station.Id = stationId;
                    station.Load();
                    _sectors.Add(sector);
                }
                else
                {
                    throw new Exception("Malformed template type for StationSectorCollection");
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UpdateStationData">FALSE - если надо одновить только заявку</param>
        public bool SaveSectors(bool updateStationData)
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ApplID);
            r.Open();

            bool result = false;

            if (!r.IsEOF())
            {
                // Заявки нет     
                r.Edit();
                for (int i = 1; i <= 6; i++)
                {
                    if (_sectors.Count >= i)
                    {
                        BaseStation baseStation = _sectors[i - 1] as BaseStation;                           
                        r.Put("OBJ_ID" + i.ToString(), baseStation.Id);

                        if (updateStationData)
                            baseStation.Save();
                    }
                    else
                    {
                        r.Put("OBJ_ID" + i.ToString(), IM.NullI);
                    }
                }
                r.Update();
                result = true;
            }

            r.Close();
            r.Destroy();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UpdateStationData">FALSE - если надо одновить только заявку</param>
        public void SaveSectorsOrSingle(bool updateStationData)
        {
            if (!SaveSectors(updateStationData))
            {
                if (_sectors.Count == 1)
                {
                    BaseStation baseStation = _sectors[0] as BaseStation;
                    if (baseStation != null)
                    {
                        baseStation.Save();
                    }
                    else
                    {
                        throw new Exception("Malformed template type for StationSectorCollection");
                    }
                }
            }
        }   
  
        public List<int> GetIdList()
        {
            List<int> resultList = new List<int>();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                r.SetWhere("ID", IMRecordset.Operation.Eq, ApplID);
                r.Open();

                //int Loaded = 1;
                if (!r.IsEOF())
                {
                    // Заявки нет     

                    for (int i = 1; i <= 6; i++)
                    {
                        int sectorID = r.GetI("OBJ_ID" + i.ToString());
                        if (sectorID != IM.NullI)
                        {
                            resultList.Add(sectorID);
                            /*T sector = new T();//_createSectorProcedure();
                            BaseStation station = sector as BaseStation;
                            if (station != null)
                            {
                                station.Id = sectorID;
                                station.Load();
                                _sectors.Add(sector);
                            }
                            else
                            {
                                throw new Exception("Malformed template type for StationSectorCollection");
                            }
                        }*/
                        }
                    }
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            return resultList;
        }
    }
}
