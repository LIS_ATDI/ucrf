﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
   class EarthDiffStation : EarthStation
   {
      private bool _initialized = false;

      public bool IsInitialized
      {
         get { return _initialized; }
      }

      public static new readonly string TableName = PlugTbl.itblXnrfaDeiffEtSta;

      public EarthDiffStation()
      {          
      }

       public override void Load()
      {
         IMRecordset r = null;
         try
         {            
            const string fieldsNameLocal =
               "ID,EQUIP_ID,ANT_ID,PWR_ANT,AGL,AZM_TO,EVEL_MIN,RX_DESIG_EM,TX_DESIG_EM,TX_BW,RX_BW,CERT_NUM,CERT_DATE,DIAMETER,POLAR_TX,POLAR_RX,POS_ID,POS_TABLE,SATTELITE,ANTENNA_NAME,EQUIP_NAME,RXMODULATION,TXMODULATION,TXGAIN,RXGAIN,COMMENTS,INACTIVE";
            r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            r.Select(fieldsNameLocal);
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();

            if (!r.IsEOF())
            {
                if (r.GetI("INACTIVE") != 1)
                {
                    Equipment.TableName = ICSMTbl.itblEquipEsta;
                    Equipment.Id = r.GetI("EQUIP_ID");
                    Equipment.Load();

                    Position = new PositionState();
                    Position.TableName = r.GetS("POS_TABLE");
                    Position.Id = r.GetI("POS_ID");
                    Position.LoadStatePosition(r.GetI("POS_ID"), r.GetS("POS_TABLE"));

                    Antenna.Id = r.GetI("ANT_ID");

                    Power[PowerUnits.dBW] = r.GetD("PWR_ANT");
                    AGL = r.GetD("AGL");
                    TxPolarization = r.GetS("POLAR_TX");
                    RxPolarization = r.GetS("POLAR_RX");

                    Azimuth = r.GetD("AZM_TO");
                    Equipment.RxDesigEmission = r.GetS("RX_DESIG_EM");
                    Equipment.TxDesigEmission = r.GetS("TX_DESIG_EM");

                    Equipment.TxBandwidth = r.GetD("TX_BW");
                    Equipment.RxBandwidth = r.GetD("RX_BW");
                    Equipment.Certificate.Symbol = r.GetS("CERT_NUM");
                    Equipment.Certificate.Date = r.GetT("CERT_DATE");
                    Equipment.TxModulation = r.GetS("TXMODULATION");
                    Equipment.RxModulation = r.GetS("RXMODULATION");

                    Antenna.Diameter = r.GetD("DIAMETER");
                    Antenna.RxGain = r.GetD("RXGAIN");
                    Antenna.TxGain = r.GetD("TXGAIN");
                    StatComment = r.GetS("COMMENTS");
                    //r.Put("SATTELITE", NetworkName);
                    Antenna.Name = r.GetS("ANTENNA_NAME");
                    Equipment.Name = r.GetS("EQUIP_NAME");

                    LoadFreq();
                    _initialized = true;
                }
            }
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Destroy();
            }
         }
      }



      public override void Save()
      {
         //SaveFreq(); Перенет ниже

         IMRecordset r = null;
         try
         {
            const string tableNameLocal = PlugTbl.itblXnrfaDeiffEtSta;
            const string fieldsNameLocal =
               "ID,ET_STATION_ID,COMMENTS,EQUIP_ID,ANT_ID,PWR_ANT,AGL,AZM_TO,EVEL_MIN,RX_DESIG_EM,TX_DESIG_EM,TX_BW,RX_BW,CERT_NUM,CERT_DATE,DIAMETER,POLAR_TX,POLAR_RX,POS_ID,POS_TABLE,SATTELITE,ANTENNA_NAME,EQUIP_NAME,RXMODULATION,TXMODULATION,RXGAIN,TXGAIN,INACTIVE";
            r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
            r.Select(fieldsNameLocal);
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();

            if (r.IsEOF())
            {
               r.AddNew();
               r.Put("ID", Id);
               r.Put("ET_STATION_ID", Id);
            }
            else
            {
               r.Edit();
            }

            //Сохраняет координаты.
            //SavePosition();

            r.Put("INACTIVE", 0);
            r.Put("EQUIP_ID", Equipment.Id);
            r.Put("COMMENTS",StatComment);
            r.Put("POS_TABLE", Position.TableName);
            r.Put("POS_ID", Position.Id);
            r.Put("ANT_ID", Antenna.Id);            
            r.Put("PWR_ANT", Power[PowerUnits.dBW]);

            r.Put("AGL", AGL);
            r.Put("POLAR_TX", TxPolarization);
            r.Put("POLAR_RX", RxPolarization);

            r.Put("AZM_TO", Azimuth);
            //r.Put("EVEL_MIN", Elevation);
            r.Put("RX_DESIG_EM", Equipment.RxDesigEmission);
            r.Put("TX_DESIG_EM", Equipment.TxDesigEmission);
            r.Put("TX_BW", Equipment.TxBandwidth);
            r.Put("RX_BW", Equipment.RxBandwidth);
            r.Put("RXMODULATION", Equipment.RxModulation);
            r.Put("TXMODULATION", Equipment.TxModulation);
            r.Put("CERT_NUM", Equipment.Certificate.Symbol);
            r.Put("CERT_DATE", Equipment.Certificate.Date);

            r.Put("DIAMETER", Antenna.Diameter);
            r.Put("RXGAIN", Antenna.RxGain);
            r.Put("TXGAIN", Antenna.TxGain);
            
            r.Put("SATTELITE", NetworkName);
            r.Put("ANTENNA_NAME", Antenna.Name);
            r.Put("EQUIP_NAME", Equipment.Name);

            //CJournal.TestFieldsOfPluginTable(tableNameLocal, ID, r, fieldsNameLocal);

            r.Update();
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Destroy();
            }
         }
         SaveFreq();

          FillComments(ApplId);
      }

      /// <summary>
      /// Read frequencies (both Tx and Rx) from XNRFA_DIFF_ET_FREQ
      /// </summary>
      private bool LoadFreq()
      {
         RxFrequencies.Clear();
         TxFrequencies.Clear();
         
         IMRecordset r = null;
         try
         {
            r = new IMRecordset(PlugTbl.itblXnrfaEtFreq, IMRecordset.Mode.ReadOnly);
            r.Select("ET_ID,FREQ,TX");
            r.SetWhere("ET_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("TX", IMRecordset.Operation.Like, "T");
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               TxFrequencies.Add(r.GetD("FREQ"));
            }
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Dispose();
            }
         }

         try
         {
            r = new IMRecordset(PlugTbl.itblXnrfaEtFreq, IMRecordset.Mode.ReadOnly);
            r.Select("ET_ID,FREQ,TX");
            r.SetWhere("ET_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("TX", IMRecordset.Operation.Like, "R");
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               RxFrequencies.Add(r.GetD("FREQ"));
            }
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Dispose();
            }
         }
         return true;
      }

      private bool SaveFreq()
      {
         IMRecordset r = null;
         try
         {
            const string tableNameLocal = PlugTbl.itblXnrfaEtFreq;
            const string fieldNameLocal = "ID,ET_ID,FREQ,TX";
            r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
            r.Select(fieldNameLocal);
            r.SetWhere("ET_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("TX", IMRecordset.Operation.Like, "T");
            r.Open();
            bool isAdd = false;

            foreach (double val in TxFrequencies)
            {
               if (r.IsEOF() || (isAdd == true))
               {
                  isAdd = true;
                  r.AddNew();
                  int localId = IM.AllocID(tableNameLocal, 1, -1);
                  r.Put("ID", localId);
                  r.Put("ET_ID", Id);
                  r.Put("TX", "T");
               }
               else
               {
                  r.Edit();
               }
               r.Put("FREQ", val);
               //CJournal.TestFieldsOfPluginTable(tableNameLocal, r.GetI("ID"), r, fieldNameLocal);
               r.Update();
               r.MoveNext();
            }

            if (isAdd == false)
            {
               while (!r.IsEOF())
               {
                  //CJournal.DeleteRecordOfPluginTable(tableNameLocal, r.GetI("ID"), fieldNameLocal);
                  r.Delete();
                  r.MoveNext();
               }
            }

            if (r != null)
            {
               r.Close();
               r.Destroy();
               r = null;
            }

            //////////////////////////////////////////////////////////////////////////

            r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
            r.Select(fieldNameLocal);
            r.SetWhere("ET_ID", IMRecordset.Operation.Eq, Id);
            r.SetWhere("TX", IMRecordset.Operation.Like, "R");
            r.Open();
            isAdd = false;

            foreach (double val in RxFrequencies)
            {
               if (r.IsEOF() || (isAdd == true))
               {
                  isAdd = true;
                  r.AddNew();
                  r.Put("ID", IM.AllocID(tableNameLocal, 1, -1));
                  r.Put("ET_ID", Id);
                  r.Put("TX", "R");
               }
               else
               {
                  r.Edit();
               }
               r.Put("FREQ", val);
               //CJournal.TestFieldsOfPluginTable(tableNameLocal, r.GetI("ID"), r, fieldNameLocal);
               r.Update();
               r.MoveNext();
            }

            if (isAdd == false)
               while (!r.IsEOF())
               {
                  //CJournal.DeleteRecordOfPluginTable(tableNameLocal, r.GetI("ID"), fieldNameLocal);
                  r.Delete();
                  r.MoveNext();
               }

            return true;
         }
         catch (Exception ex)
         {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            return false;
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Destroy();
            }
         }
      }
 
      public void Remove()
      {
         const string tableNameLocal = PlugTbl.itblXnrfaDeiffEtSta;

         IMRecordset r = null;
         try
         {
            r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
            r.Select("ID,INACTIVE");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("INACTIVE",1);
                r.Update();
            }            
         }
         finally
         {
            if (r != null)
            {
               r.Close();
               r.Destroy();
               r = null;
            }
         }
      }
   }
}
