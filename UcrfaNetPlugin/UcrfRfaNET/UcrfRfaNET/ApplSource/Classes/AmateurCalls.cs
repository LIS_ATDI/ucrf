﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class AmateurCalls
    {
        public const string TableName = "XFA_CALL_AMATEUR";
        public int StaId { get; set; }
        public string NumberDocument { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        public string NumberBlank { get; set; }
        public string TypeCall { get; set; }
        public string Call { get; set; }
        public string GoalUse { get; set; }
        public string ObjDoc { get; set; }
        public string PathFile { get; set; }

        public void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
            r.SetWhere("STA_ID", IMRecordset.Operation.Eq, StaId);
            r.SetWhere("CALL", IMRecordset.Operation.Eq, Call);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    r.Edit();
                else
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(TableName, 1, -1));
                    r.Put("CALL", Call);
                    r.Put("STA_ID", StaId);
                }
                r.Put("TABLE_NAME", TableName);
                r.Put("TYPE_CALL", TypeCall);
                r.Put("GOAL_USED", GoalUse);
                r.Put("DATE_IN", DateIn);
                r.Put("DATE_OUT", DateOut);
                r.Put("NUMBER_DOZV", NumberDocument);
                r.Put("NUMB", NumberBlank);
                r.Put("PATH", PathFile);
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }

        public void Load()
        {
        }
    }
}
