﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class RadioSystemList
    {        
        private ComboBoxDictionaryList<int, string> _overallDictionary = new ComboBoxDictionaryList<int, string>();

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadRadioSystemList()
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(ICSMTbl.itblRadioSystem, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,DESCRIPTION");
                
                r.Open();

                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");
                    string description = r.GetS("DESCRIPTION");
                    
                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name+" - "+description));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        public void ReadRadioSystemCodeList()
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(ICSMTbl.itblRadioSystem, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME");

                r.Open();

                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");
                    
                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Получить рещультатт в виде удобном бля биндинга
        /// </summary>
        /// <returns></returns>
        public ComboBoxDictionaryList<int, string> GetComboBoxDictionaryList()
        {
            return _overallDictionary;
        }
    }
}
