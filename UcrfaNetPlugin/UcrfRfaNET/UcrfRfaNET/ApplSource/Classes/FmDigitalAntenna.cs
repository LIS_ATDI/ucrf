﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class FmDigitallAntenna:Antenna
    {
       
        public override void Load()
        {
            TableName = ICSMTbl.itblAntennaBro;
            //ICSMTbl.itblMicrowAntenna
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            if (Id == IM.NullI)
            {
                throw new Exception("FmDigitall antenna ID is nit initialized");
            }

            try
            {
                r.Select("ID,NAME,DIAMETER,GAIN");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {                    
                    Name = r.GetS("NAME");                                       
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }         
        }

        public override void Save()
        {

        }
    }
}
