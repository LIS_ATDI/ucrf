﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MeasureEquipmentList
    {
        // Класс для списка сертификатов измерительного оборудования
        // приспособлен для биндинга с комбобоксом
        //private readonly string vrzString = Enum.GetName(DepartmentType.VRZ.GetType(), DepartmentType.VRZ);
        private ComboBoxDictionaryList<int, string> _overallDictionary = new ComboBoxDictionaryList<int, string>();

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadMeasurementEquipmentList(int certificateId)
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.MeasureEquipment, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,CERT_ID");

                if (certificateId!=IM.NullI)
                    r.SetWhere("CERT_ID", IMRecordset.Operation.Eq, certificateId);
                
                r.Open();

                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");

                    /*DateTime when = r.GetT("CERT_DATE");
                    string certifiedBy = r.GetS("CERTIFIED_BY");

                    string certificateText =
                        "Свідоцтво про атестацію №" + number +
                        " Видане " + when.ToShortDateStringNullT() +
                        " " + certifiedBy;*/

                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Вытащить список с базы данных, отфильтровать по провинции.
        /// </summary>
        public void ReadMeasurementEquipmentList(string province)
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.MeasureEquipment, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,Certificate.PROVINCE");

                if (!string.IsNullOrEmpty(province))
                    r.SetWhere("Certificate.PROVINCE", IMRecordset.Operation.Eq, province);

                r.Open();

                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");

                    /*DateTime when = r.GetT("CERT_DATE");
                    string certifiedBy = r.GetS("CERTIFIED_BY");

                    string certificateText =
                        "Свідоцтво про атестацію №" + number +
                        " Видане " + when.ToShortDateStringNullT() +
                        " " + certifiedBy;*/

                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Получить результат в виде удобном бля биндинга
        /// </summary>
        /// <returns></returns>
        public ComboBoxDictionaryList<int, string> GetComboBoxDictionaryList()
        {
            return _overallDictionary;
        }
    }
}
