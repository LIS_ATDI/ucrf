﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using GridCtrl;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.CalculationVRS;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal class StationTvDigital : BaseStation
    {
        public PositionState2 objPosition = null;//Позиция
        public IMObject objEquip = null; //Оборудование
        public IMObject objAntenna = null;//Антенна    
        public IMObject objStation = null;
        public IMObject objChannel = null; // Канал
        public IMObject objAsign = null;
        public string Direction = "";
        public double MaxPower;
        public string Code = string.Empty;
    }

    internal class CTvDigital : FMTVBase
    {
        // initializes combobox lists
        public CComboList _konturList = new CComboList();
        public CComboList _numberRezList = new CComboList();

        public CComboList _polarizationList = new CComboList();
        public CComboList _numKontList = new CComboList();
        public CComboList _vidilenNameList = new CComboList();
        public CComboList _ocmNameList = new CComboList();
        public CComboList _intnationalStateList = new CComboList();
        public CComboList _internalStateList = new CComboList();
        public CComboList _nesivCountList = new CComboList();
        public CComboList _protecIntervalList = new CComboList();
        private Dictionary<string, string> _spectMaskTypeList = new Dictionary<string, string>();
        public CComboList _priemnTypeList = new CComboList();
        public CComboList _standKomprList = new CComboList();
        public CComboList _orientationList = new CComboList();
        public CComboList _channelList = new CComboList();
        public CComboList _freqList = new CComboList();
        public CComboList _nesivVariantList = new CComboList();

        public static readonly string TableName = ICSMTbl.itblDVBT;
        public StationTvDigital station = new StationTvDigital(); // Дополнительные данные станции

        private double Longitude = 0; //Долгота
        private double Latitude = 0;  //Широта
        private bool NeedSavePosition = false;

        public CTvDigital(int id, int _ownerId, int _packetID, string _radioTech)
            : base(id, CTvDigital.TableName, _ownerId, _packetID, _radioTech)
        {
            appType = AppType.AppTV2d;
            department = DepartmentType.VRS;
            departmentSector = DepartmentSectorType.VRS_ST;

            // Вытаскиваем "Особливі умови"
            SCDozvil = objStation.GetS("CUST_TXT8");
            SCVisnovok = objStation.GetS("CUST_TXT9");
            SCNote = objStation.GetS("CUST_TXT10");

            FillComboList();

            if (objStation.GetI("PLAN_ID") != IM.NullI)
            {
                station.objChannel = IMObject.LoadFromDB(ICSMTbl.itblFreqPlanChan, objStation.GetI("PLAN_ID"));
            }

            if (objStation.GetI("ASSGN_ID") != IM.NullI)
            {
                station.objAsign = IMObject.LoadFromDB(ICSMTbl.itblFmtvAssign, objStation.GetI("ASSGN_ID"));
            }

            if (objStation.GetI("SITE_ID") != IM.NullI)
            {
                station.objPosition = new PositionState2();
                station.objPosition.LoadStatePosition(objStation.GetI("SITE_ID"), ICSMTbl.itblPositionBro);
                station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
            }

            if (newStation)
            {
                objStation.Put("ADM", "UKR");
                objStation.Put("STANDARD", _radioTech == CRadioTech.DVBT2 ? CRadioTech.DVBT2 : CRadioTech.DVBT);
            }
            //----
            addValidVisnMonth = 12;
        }
        //===================================================
        // Реализация метода IDispose
        //===================================================
        protected override void DisposeExec()
        {
            base.DisposeExec();
        }

        protected override string GetXMLParamGrid()
        {
            string XmlString =
                "<items>"
               + "<item key='header1'    type='lat'       flags=''      tab='-1'   display='Параметри' ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center' />"
               + "<item key='header2'    type='lat'       flags='s'     tab='-1'   display='Значення'     ReadOnly='true' background='DarkGray' bold='y' HorizontalAlignment='center'/>"

               // objPosition.LATITUDE
               + "<item key='mKLat'      type='lat'       flags=''     tab='-1'  display='Широта (гр. хв. сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLat'      type='lat'       flags='s'    tab='0'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center' />"

               // objPosition.LONGITUDE
               + "<item key='mKLon'       type='lat'       flags=''     tab='-1'  display='Довгота (гр.хв.сек.)'     ReadOnly='true' background='' bold='y' />"
               + "<item key='KLon'       type='lat'       flags='s'     tab='1'  display=''     ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

               // objPosition.ASL 
               + "<item key='mkAsl'      type=''       flags=''      tab='-1'  display='Висота поверхні Землі, м' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='kAsl'       type='double'       flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               // objPosition.GetFullAddress
               + "<item key='mkAddr'     type=''      flags=''      tab='-1'  display='Адреса встановлення РЕЗ' ReadOnly='true' background='' bold='y'/>"
               + "<item key='kAddr'      type='string'      flags='s'     tab='2'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center'/>"

               //objEquip.NAME
               + "<item key='mkName'     type=''      flags=''      tab='-1'  display='Номер РЕЗ/назва опори' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='KName'      type=''      flags='s'     tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='KPillarName' type=''      flags='s'     tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               // 
               + "<item key='kNumbKont' type=''    flags=''     tab='-1'  display='Номер контуру / номер РЕЗ в синхронній мережі'     ReadOnly='true' background='' bold='' />"
               + "<item key='NumbKont' type='' flags='s'    tab='3'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='numbSinchNetwork' type='' flags='s'    tab='4'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kVidilenName'     type='lat'       flags=''     tab='-1'  display='Назва виділення / Назва ОЧМ '     ReadOnly='true' background='' bold=''/>"
               + "<item key='VidilenName'     type='lat'       flags='s'    tab='5'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='OcmName'     type='lat'       flags='s'    tab='6'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //            
               + "<item key='kNesivVariant'     type='lat'       flags=''     tab='-1'  display='Варіант системи / кількість несівних / захисний інтервал'     ReadOnly='true' background='' bold=''/>"
               + "<item key='NesivVariant'     type='lat'       flags='s'    tab='7'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='NesivCount'     type='lat'       flags='s'    tab='8'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='ProtecInterval'     type='lat'       flags='s'    tab='9'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kVtratiFreq'     type='lat'       flags=''     tab='-1'  display='Втрати у фільтрі / тип спектр. маски фільтра / тип прм'     ReadOnly='true' background='' bold=''/>"
               + "<item key='VtratiFreq'     type='lat'       flags='s'    tab='10'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='SpectMaskType'     type='lat'       flags='s'    tab='11'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='PriemnType'     type='lat'       flags='s'    tab='12'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kStandKompr'     type='lat'       flags=''     tab='-1'  display='Стандарт компресії / Зміщення несівної частоти, Гц'     ReadOnly='true' background='' bold=''/>"
               + "<item key='StandKompr'     type='lat'       flags='s'    tab='13'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='NesivFreq'     type='lat'       flags='s'    tab='14'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //            
               + "<item key='kEquip'    type='lat'      flags=''      tab='-1'  display='Назва/тип РЕЗ' ReadOnly='true' background='' bold='y'/>"
               + "<item key='equip'     type='lat'      flags='s'     tab='15'  display='' ReadOnly='false' background='' bold='y' HorizontalAlignment='center'/>"

               //            
               + "<item key='kTXFreq'       type='lat'       flags=''     tab='-1'  display='Потужність передавача, Вт'     ReadOnly='true' background='' bold='' />"
               + "<item key='powVideo'       type='lat'       flags='s'    tab='16'  display=''     ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //            
               + "<item key='kRxBand'      type='lat'       flags=''     tab='-1' display='Ширина смуги, МГц / клас випромінювання ' ReadOnly='true' background='' bold='' fontColor='gray' />"
               + "<item key='rxband'    type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"
               + "<item key='desigEmi'   type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"

               // objEquip.CUST_TXT1
                // objEquip.CUST_DAT1
               + "<item key='kCert'      type='lat'      flags=''      tab='-1'  display='Сертифікат відповідності (номер / дата)' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='certno'     type='string'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='certda'     type='datetime'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               // 
               + "<item key='kFider'      type='lat'      flags=''   tab='-1'   display='Довжина фідера, м / Втрати у фідері, дБ/м' ReadOnly='true' background='' bold=''/>"
               + "<item key='fiderlen'    type='double'   flags='s'  tab='17'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='fiderloss'   type='double' flags='s'  tab='18'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kAntennaDir'       type='lat'      flags=''      tab='-1'  display='Спрямованість антени / поляризація' ReadOnly='true' background='' bold='y' />"
               + "<item key='dir'        type='lat'      flags='s'     tab='19'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='polar'        type='lat'      flags='s'     tab='20'  display='' ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"

               //
               + "<item key='kAntennaHeight' type='lat'      flags=''      tab='-1'  display='Висота антени, м / макс. ефект. висота' ReadOnly='true' background='' bold=''/>"
               + "<item key='height'         type='lat'      flags='s'     tab='21'  display='' ReadOnly='false' background='' bold='' HorizontalAlignment='center'/>"
               + "<item key='maxHeight'      type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center'/>"

               //
               + "<item key='kAntennaGain' type='lat'      flags=''      tab='-1'  display='Макс. коеф. підсилення антени: Г, дБ / В, дБ' background='' bold=''/>"
               + "<item key='horzGain'     type='lat'      flags='s'     tab='22'  display='' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='vertGain'     type='lat'      flags='s'     tab='23'  display='' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kEVP' type='lat'      flags=''      tab='-1'  display='ЕВП: Г, дБВт / В, дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='HorzEVP'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "<item key='VertEVP'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               //
               + "<item key='kEVPmax' type='lat'      flags=''      tab='-1'  display='ЕВП макс., дБВт' ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='EVPmax'     type='lat'      flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"

               //
               + "<item key='kState'   type='lat'      flags=''      tab='-1'    display='Внутрішній стан / Міжнародний стан' background='' bold='' />"
               + "<item key='homeState' type='lat'      flags='s'     tab='24'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' />"
               + "<item key='internationalState' type='lat'      flags='s'     tab='25' ReadOnly='true' display='' background='' bold='' HorizontalAlignment='center' />"

               //
               + "<item key='kTvChan'       type='lat'       flags=''     tab='-1'  display='ТВ канал / центральна частота МГц' ReadOnly='true' background='' bold='y' />"
               + "<item key='tvchan'       type='lat'       flags='s'    tab='26'  display=''     ReadOnly='true' background='' bold='y' HorizontalAlignment='center' />"
               + "<item key='freqVid'       type='lat'       flags='s'    tab='-1'  display=''     ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray' />"

               //
               + "<item key='mKObj'       type=''       flags=''     tab='-1'  display='Об\"єкт(и) РЧП'     ReadOnly='true' background='' bold='' fontColor='gray'/>"
               + "<item key='KObj'      type=''     flags='s'     tab='-1'  display='' ReadOnly='true' background='' bold='' HorizontalAlignment='center' fontColor='gray'/>"
               + "</items>";

            return XmlString;
        }

        //===========================================================
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            // Сохраняем "Особливі умови"
            objStation.Put("CUST_TXT8", SCDozvil);
            objStation.Put("CUST_TXT9", SCVisnovok);
            objStation.Put("CUST_TXT10", SCNote);
            objStation.Put("CUST_CHB1", IsTechnicalUser);

            if (station.objChannel != null)
                station.objChannel.SaveToDB();

            objStation.Put("CUST_TXT1", NumberOut);//--***         
            objStation.Put("CUST_DAT1", DateOut);//--***
            objStation.Put("STATUS", Status.ToStr()); //Сохраняем статус
            Power power = new Power();
            power[PowerUnits.W] = gridParam.GetCellFromKey("powVideo").Value.ToDouble(IM.NullD).Round(3);

            objStation.Put("PWR_ANT", power[PowerUnits.dBW]);

            //if (NeedSavePosition)
            //    station.objPosition.Save();
            objStation.Put("SITE_ID", station.objPosition.Id);

            if (newStation)
            {
                objStation.Put("DIAGA", "HH");
            }

            SaveFMTVAssign();

            // Сохраняем лицензии
            SaveLicence();
            CJournal.CheckTable(recordID.Table, recordID.Id, objStation);

            objStation.SaveToDB();

            var r = new IMRecordset(objTableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,CODE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, recID);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", recID);
                    r.Put("CODE", station.Code);
                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            return true;
        }

        private void SaveFMTVAssign()
        {
            if (station.objAsign != null)
            {
                station.objAsign.Put("SITE_ALT", station.objPosition.Asl);
                station.objAsign.Put("LONGITUDE", Longitude);
                station.objAsign.Put("LATITUDE", Latitude);
                station.objAsign.Put("PLAN_ID", objStation.GetI("PLAN_ID"));
                station.objAsign.Put("SITE_NAME", station.objPosition.Name);
                station.objAsign.Put("ERP_H", objStation.GetD("ERP_H"));
                station.objAsign.Put("ERP_V", objStation.GetD("ERP_V"));

                station.objAsign.Put("POLARIZATION", objStation.GetS("POLARIZATION"));
                station.objAsign.Put("ANT_DIR", _orientationList.GetShortValue(station.Direction));
                station.objAsign.Put("ATTN_H", objStation.GetS("DIAGH"));
                station.objAsign.Put("ATTN_V", objStation.GetS("DIAGV"));

                station.objAsign.Put("DESIG_EMISSION", objStation.GetS("DESIG_EM"));
                station.objAsign.Put("BW", objStation.GetD("BW"));
                station.objAsign.Put("FREQ", objStation.GetD("FREQ"));
                station.objAsign.Put("CHANNEL", objStation.GetS("CHANNEL"));
                station.objAsign.Put("AGL", objStation.GetD("AGL"));
                station.objAsign.Put("A_NAME", objStation.GetS("ADM_KEY"));
                station.objAsign.Put("EFHGT_MAX", objStation.GetD("EFHGT_MAX"));


                //   station.objAsign.Put("SPECT_MASK", objStation.GetI("HM_CRITICAL").ToString());//Не находит поле в БД  29.03.11  
                station.objAsign.Put("DVBT_SYS_CODE", objStation.GetS("DVBT_SYS_CODE"));
                station.objAsign.Put("NB_CARR", objStation.GetS("NB_CARR"));
                station.objAsign.Put("GUARD_INTERVAL", objStation.GetD("GUARD_INTERVAL"));
                station.objAsign.Put("DVBT_OFFSET", objStation.GetD("OFFSET"));
                station.objAsign.Put("SFN_IDENT", objStation.GetS("SFN_IDENT"));

                station.objAsign.SaveToDB();
            }
        }

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            string prov = (station.objPosition != null) ? station.objPosition.Province : "";
            string city = (station.objPosition != null) ? station.objPosition.City : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];

            if ((docType == DocType.VISN) || (docType == DocType.VISN_NR))
                fullPath += ConvertType.DepartmetToCode(department) + HelpFunction.getUserNumber() + "-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.DOZV_OPER))
                fullPath += "ЦТБ-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії лозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Обновить один параметр грида
        /// </summary>
        /// <param name="grid">Грид</param>
        public override void UpdateOneParamGrid(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLat":
                    AutoFill(cell, grid);
                    break;

                case "system":
                    SelectByShort(cell, objStation.GetS("COLOUR_SYSTEM"));
                    break;

                case "KName":
                    cell.Value = objStation.GetS("CUST_TXT7");
                    break;

                case "equip":
                    {
                        int equipId = objStation.GetI("EQUIP_ID");

                        if (equipId != IM.NullI)
                            station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipBro, equipId);

                        AutoFill(cell, grid);
                    }
                    break;

                case "height":
                    cell.Value = objStation.GetD("AGL").ToString("F0");
                    break;

                case "maxHeight":
                    cell.Value = objStation.GetD("EFHGT_MAX").ToString("F0");
                    break;

                case "fiderlen":
                    cell.Value = objStation.GetD("CUST_NBR1").ToString("F0");
                    break;

                case "fiderloss":
                    cell.Value = objStation.GetD("CUST_NBR2").ToString("F3");
                    break;

                case "dir":
                    {
                        string DiagH = objStation.GetS("DIAGH");
                        string DiagV = objStation.GetS("DIAGV");

                        if ((DiagV.Equals("OMNI") && (DiagH.Equals("OMNI") || string.IsNullOrEmpty(DiagH))) ||
                              (DiagH.Equals("OMNI") && string.IsNullOrEmpty(DiagV)))
                        {
                            SelectByShort(cell, "ND");
                        }
                        else
                        {
                            SelectByShort(cell, "D");
                        }
                        station.Direction = cell.Value;
                    }
                    break;

                case "polar":
                    SelectByShort(cell, objStation.GetS("POLARIZATION"));
                    AutoFill(cell, grid);
                    break;

                case "callsign":
                    cell.Value = objStation.GetS("CALL_SIGN");
                    break;

                case "homeState":
                    SelectByShort(cell, objStation.GetS("CUST_TXT4"));
                    break;

                case "internationalState":
                    SelectByShort(cell, objStation.GetS("CUST_TXT5"));
                    break;

                case "KObj":
                    cell.Value = objStation.GetI("ID").ToString();
                    break;

                case "HorzEVP":
                    cell.Value = objStation.GetD("ERP_H").ToString("F3");
                    break;

                case "VertEVP":
                    cell.Value = objStation.GetD("ERP_V").ToString("F3");
                    break;

                case "NesivFreq":
                    cell.Value = objStation.GetD("OFFSET").ToString("F3");
                    break;

                case "tvchan":
                    {
                        cell.Value = objStation.GetS("CHANNEL");

                        if (station.objChannel != null)
                        {
                            //cell.Value = station.objChannel.GetS("CHANNEL");

                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r.Select("PLAN_ID,CHANNEL,FREQ,OFFSET");
                            r.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, objStation.GetI("PLAN_ID"));
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _channelList.AddValue(r.GetS("CHANNEL"));

                                double offset = 0;
                                double freq = 0;

                                if (r.GetD("OFFSET") != IM.NullD)
                                    offset = r.GetD("OFFSET");

                                if (r.GetD("FREQ") != IM.NullD)
                                    freq = r.GetD("FREQ");

                                freq = freq + offset / 1000;

                                _freqList.AddValue(freq.ToString());
                            }
                            r.Close();
                            r.Destroy();

                            cell.PickList = _channelList.GetList();

                            int index = _channelList.GetList().IndexOf(cell.Value);
                            string freVideo = string.Empty;
                            try
                            {
                                freVideo = _freqList.GetList()[index];
                            }
                            catch (ArgumentOutOfRangeException ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.Message);
                            }

                            cell = grid.GetCellFromKey("freqVid");
                            if (cell != null)
                                cell.Value = freVideo;
                        }
                        else
                        {
                            StringBuilder idChannel = new StringBuilder();
                            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,COD");
                            r.SetWhere("COD", IMRecordset.Operation.Like, "*DVB-T*");
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                idChannel.Append(',');
                                idChannel.Append(r.GetI("ID").ToString());
                            }
                            r.Close();
                            r.Destroy();

                            if (idChannel.Length == 0)
                                return;

                            idChannel = idChannel.Remove(0, 1);
                            List<int> channelIDList = new List<int>();

                            r = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                            r.Select("PLAN_ID,CHANNEL,FREQ,OFFSET");
                            r.SetAdditional("[PLAN_ID] in " + "(" + idChannel.ToString() + ")");
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _channelList.AddValue(r.GetS("CHANNEL"));
                                channelIDList.Add(r.GetI("PLAN_ID"));

                                double offset = 0;
                                double freq = 0;

                                if (r.GetD("OFFSET") != IM.NullD)
                                    offset = r.GetD("OFFSET");

                                if (r.GetD("FREQ") != IM.NullD)
                                    freq = r.GetD("FREQ");

                                freq = freq - offset;

                                _freqList.AddValue(freq.ToString());
                            }

                            cell.PickList = _channelList.GetList();
                            cell.Tag = channelIDList;

                            r.Close();
                            r.Destroy();
                        }
                        break;
                    }

                case "powVideo":
                    {
                        double powVideo = 0;
                        if (objStation.GetD("PWR_ANT") != IM.NullD)
                        {
                            //powVideo = objStation.GetD("PWR_ANT");
                            //powVideo = Math.Round(Math.Pow(10.0, powVideo / 10), 3);
                            //cell.Value = powVideo.ToString();
                            Power powV = new Power();
                            powV[PowerUnits.dBW] = objStation.GetD("PWR_ANT");
                            cell.Value = powV[PowerUnits.W].Round(3).ToString();

                        }
                        else if (station.objEquip != null)
                        {
                            if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                            {
                                double maxPow = (station.objEquip.GetD("MAX_POWER") - 30.0) / 10;
                                powVideo = Math.Round(Math.Pow(10, maxPow), 3);
                                cell.Value = powVideo.ToString();
                                //===========================================
                                objStation.Put("PWR_ANT", 10.0 * Math.Log10(powVideo));
                            }
                        }
                    }
                    break;

                case "NumbKont":
                    {
                        IMRecordset r = new IMRecordset(ICSMTbl.itblItuContour, IMRecordset.Mode.ReadOnly);
                        r.Select("NUM,ADM");
                        r.SetWhere("ADM", IMRecordset.Operation.Like, "UKR");
                        r.OrderBy("NUM", OrderDirection.Ascending);
                        try
                        {
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _numKontList.AddValue(r.GetI("NUM").ToString());
                            }
                            cell.PickList = _numKontList.GetList();
                            if (station.objAsign != null)
                                cell.Value = station.objAsign.GetS("INDX");
                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                    break;

                case "VidilenName":
                    {
                        Cell c = grid.GetCellFromKey("NumbKont");
                        if (string.IsNullOrEmpty(c.Value))
                            return;

                        string aName = c.Value;
                        switch (aName.Length)
                        {
                            case 1:
                                aName = "000" + aName;
                                break;
                            case 2:
                                aName = "00" + aName;
                                break;
                            case 3:
                                aName = "0" + aName;
                                break;

                        }
 

                        IMRecordset r = null;
                        try
                        {
                            r = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadOnly);
                            r.Select("IS_ALLOTM,CLASS,A_NAME");
                            r.SetWhere("CLASS", IMRecordset.Operation.Like, "BT");
                            r.SetWhere("IS_ALLOTM", IMRecordset.Operation.Like, "Y");
                            r.SetWhere("A_NAME", IMRecordset.Operation.Like, "UKR" +"*"+ aName);
                            _vidilenNameList.AddValue("   ".ToString());
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _vidilenNameList.AddValue(r.GetS("A_NAME"));
                            }
                        }
                        finally
                        {
                            r.Close();
                            r.Destroy();
                        }

                        cell.Value = objStation.GetS("ADM_KEY");
                        cell.PickList = _vidilenNameList.GetList();
                    }
                    break;

                case "desigEmi":
                    cell.Value = objStation.GetS("DESIG_EM");
                    break;

                case "SpectMaskType":
                    {
                        string val = objStation.GetS("SPECT_MASK");
                        if (_spectMaskTypeList.ContainsKey(val))
                            cell.Value = _spectMaskTypeList[val];
                    }
                    break;

                case "NesivVariant":
                    SelectByShort(cell, objStation.GetS("DVBT_SYS_CODE"));
                    break;

                case "NesivCount":
                    cell.Value = objStation.GetS("NB_CARR");
                    break;

                case "ProtecInterval":
                    SelectByShort(cell, objStation.GetI("GUARD_INTERVAL").ToString());
                    break;

                case "rxband":
                    {
                        double BW = objStation.GetD("BW");
                        cell.Value = (BW / 1000.0).ToString("F3");
                    }
                    break;

                case "PriemnType":
                    if (station.objAsign != null)
                    {
                        SelectByShort(cell, station.objAsign.GetS("RX_MODE"));
                    }
                    else
                    {
                        station.objAsign = IMObject.New(ICSMTbl.itblFmtvAssign);
                        objStation.Put("ASSGN_ID", station.objAsign.GetI("ID"));

                        station.objAsign.Put("STN_ID", objStation.GetI("ID").ToString());
                        station.objAsign.Put("CLASS", "BT");
                        station.objAsign.Put("DIGITAL", 1);
                        station.objAsign.Put("ADM", "UKR");
                        station.objAsign.Put("COUNTRY", "UKR");
                        station.objAsign.Put("IS_ALLOTM", "N");
                        station.objAsign.Put("ATTN_H", "0");
                        station.objAsign.Put("ASSGN_CODE", "S");
                    }
                    break;

                case "StandKompr":
                    cell.Value = objStation.GetS("CUST_TXT6");
                    break;

                case "OcmName":
                    cell.Value = objStation.GetS("SFN_IDENT");
                    break;

                case "VtratiFreq":
                    {
                        double txLosses = objStation.GetD("TX_LOSSES");
                        double cust1 = objStation.GetD("CUST_NBR1");
                        double cust2 = objStation.GetD("CUST_NBR2");

                        cell.Value = (txLosses - cust1 * cust2).ToString("F3");
                    }
                    break;

                case "numbSinchNetwork":
                    {
                        var stationRec = new IMRecordset(objTableName, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            stationRec.Select("CODE");
                            stationRec.SetWhere("ID", IMRecordset.Operation.Eq, recID);
                            stationRec.Open();
                            if (!stationRec.IsEOF())
                            {
                                station.Code = stationRec.GetS("CODE");
                                cell.Value = station.Code;
                            }
                        }
                        finally
                        {
                            stationRec.Close();
                            stationRec.Destroy();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Инициализация ячейки грида параметров
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {
            switch (cell.Key)
            {
                case "KLon":
                case "kAddr":
                case "equip":
                case "kAsl":
                case "horzGain":
                case "vertGain":
                    cell.cellStyle = EditStyle.esEllipsis;
                    break;

                case "HorzEVP":
                case "VertEVP":
                case "EVPmax":
                    cell.cellStyle = EditStyle.esEllipsis;
                    cell.ButtonText = "Розрахунок.";
                    break;

                //case "powera":
                //   {
                //      double PwrAnt = objStation.GetD("PWR_ANT");
                //      cell.Value = (Math.Pow(10.0, PwrAnt / 10.0)).ToString("F3");
                //   }
                //   break;

                case "polar":
                    cell.PickList = _polarizationList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "dir":
                    cell.PickList = _orientationList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "homeState":
                    cell.PickList = _internalStateList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "internationalState":
                    cell.PickList = _intnationalStateList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "tvchan":
                case "NumbKont":
                case "VidilenName":
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "NesivVariant":
                    cell.PickList = _nesivVariantList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "NesivCount":
                    cell.PickList = _nesivCountList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "ProtecInterval":
                    cell.PickList = _protecIntervalList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "SpectMaskType":
                    cell.PickList = new List<string>();
                    foreach (KeyValuePair<string, string> pair in _spectMaskTypeList)
                        cell.PickList.Add(pair.Value);
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "PriemnType":
                    cell.PickList = _priemnTypeList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;

                case "StandKompr":
                    cell.PickList = _standKomprList.GetList();
                    cell.cellStyle = EditStyle.esPickList;
                    break;
            }
        }

        //============================================================
        /// <summary>
        /// Нажата кнопка в ячейке
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void OnPressButton(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "equip":
                    {
                        // Выбор оборудования
                        RecordPtr recEquip = SelectEquip("Seach of the equipment", ICSMTbl.itblEquipBro, "NAME", cell.Value, true);
                        if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                        {
                            station.objEquip = IMObject.LoadFromDB(recEquip);
                            objStation.Put("EQUIP_ID", recEquip.Id);
                            CellValidate(cell, grid);
                            AutoFill(cell, grid);
                        }
                    }
                    break;

                case "KLon":
                    {
                        CellValidate(cell, grid);
                        PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, ICSMTbl.itblPositionBro, 1, Longitude, Latitude);
                        if (newPos != null)
                        {
                            station.objPosition = newPos;
                            station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                            objStation.Put("SITE_ID", newPos.Id);
                            AutoFill(grid.GetCellFromKey("KLat"), grid);
                            NeedSavePosition = station.objPosition.IsChanged;
                        }
                    }
                    break;

                case "kAddr":
                    {
                        if (ShowMessageReference(station.objPosition != null))
                        {
                            PositionState2 newPos = new PositionState2();
                            newPos.LongDms = Longitude;
                            newPos.LatDms = Latitude;
                            DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(ICSMTbl.itblPositionBro, ref newPos, OwnerWindows);
                            if (dr == DialogResult.OK)
                            {
                                station.objPosition = newPos;
                                station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                                AutoFill(grid.GetCellFromKey("KLat"), grid);
                                NeedSavePosition = true;
                            }
                        }
                    }
                    break;

                case "horzGain":
                    {
                        //-- From FMTVBase
                        OnPresButtonHorzGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                        Cell fillCell = grid.GetCellFromKey("polar");
                        AutoFill(fillCell, grid);
                        //-- From FMTVBase
                    }
                    break;

                case "vertGain":
                    {
                        //-- From FMTVBase
                        OnPresButtonVertGain(cell, grid.GetCellFromKey("dir"), grid.GetCellFromKey("polar"));
                        Cell fillCell = grid.GetCellFromKey("polar");
                        AutoFill(fillCell, grid);
                        //-- From FMTVBase
                    }
                    break;

                case "HorzEVP":
                    {
                        //-- From FMTVBase
                        double Power = objStation.GetD("PWR_ANT");

                        double tx_losses = objStation.GetD("TX_LOSSES");
                        cell.Value = CalcERPHorz(Power - tx_losses);
                        //-- From FMTVBase
                    }
                    break;

                case "VertEVP":
                    {
                        double Power = objStation.GetD("PWR_ANT");
                        double tx_losses = objStation.GetD("TX_LOSSES");
                        cell.Value = CalcERPVert(Power - tx_losses);
                        //-- From FMTVBase               
                    }
                    break;

                case "EVPmax":
                    {
                        string polarization = objStation.GetS("POLARIZATION");

                        double Power = objStation.GetD("PWR_ANT");
                        double tx_losses = objStation.GetD("TX_LOSSES");

                        //-- From FMTVBase
                        if (polarization == "V")
                        {
                            cell.Value = CalcERPVert(Power - tx_losses);
                        }
                        else if (polarization == "H")
                        {
                            cell.Value = CalcERPHorz(Power - tx_losses);
                        }
                        else if (polarization == "M")
                        {
                            cell.Value = CalcERPMixed(Power - tx_losses);
                        }
                        //-- From FMTVBase
                    }
                    break;

                case "kAsl":
                    {
                        if (station.objPosition != null)
                        {
                            double Longitude = station.objPosition.LonDec;
                            double Latitude = station.objPosition.LatDec;

                            if (Longitude != IM.NullD && Latitude != IM.NullD)
                            {
                                double asl = IMCalculate.CalcALS(Longitude, Latitude, "4DEC");
                                if (asl != IM.NullD)
                                {
                                    cell.Value = IM.RoundDeci(asl, 1).ToString();
                                    OnCellValidate(cell, grid);
                                }
                            }
                        }
                    }
                    break;

                default:
                    base.OnPressButton(cell, grid);
                    break;
            }
        }

        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            CTvDigital retStation = new CTvDigital(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            return retStation;
        }

        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            CTvDigital retStation = newAppl as CTvDigital;// new EarthApp(0, objStation.GetI("OWNER_ID"), packetID, radioTech);
            //retStation.CopyFromMe(grid, (station.objEquip != null) ? station.objEquip.GetI("ID") : IM.NullI,
            //   (station.objAntenna != null) ? station.objAntenna.GetI("ID") : IM.NullI);
            //(station.nomPowTx.Count > 0) ? station.nomPowTx[0] : -1, station.NoiseT,
            //station.PolarRX, station.PolarТX);
            return retStation;
        }

        //private void CopyFromMe(Grid grid, int equipID, int antID)//, double maxPow, int NoiseT, string PolarRX, string PolarTX)
        //{
        //   Cell tmpCell = null;

        //   // Оборудование
        //   if ((equipID != 0) || (equipID != IM.NullI))
        //   {
        //      tmpCell = grid.GetCellFromKey("KName");
        //      station.objEquip = IMObject.LoadFromDB(ICSMTbl.itblEquipEsta, equipID);
        //      tmpCell.Value = station.objEquip.GetS("NAME");
        //      OnCellValidate(tmpCell, grid);
        //      AutoFill(tmpCell, grid);
        //   }
        //   if ((antID != 0) || (antID != IM.NullI))
        //   {
        //      tmpCell = grid.GetCellFromKey("KAntType");
        //      station.objAntenna = IMObject.LoadFromDB(ICSMTbl.itblAntenna, antID);
        //      tmpCell.Value = station.objAntenna.GetS("NAME");
        //      OnCellValidate(tmpCell, grid);
        //   }
        //   //// Мощность
        //   //tmpCell = grid.GetCellFromKey("KPow");
        //   //tmpCell.Value = maxPow.ToString();
        //   //OnCellValidate(tmpCell, grid);
        //   //// !!
        //   //st.NoiseT = NoiseT;
        //   //// Поляризация
        //   //tmpCell = grid.GetCellFromKey("KPolarRx");
        //   //tmpCell.Value = PolarRX;
        //   //OnCellValidate(tmpCell, grid);

        //   //tmpCell = grid.GetCellFromKey("KPolarTx");
        //   //tmpCell.Value = PolarRX;
        //   //OnCellValidate(tmpCell, grid);
        //}

        //============================================================
        /// <summary>
        /// Автоматически заполнить поля
        /// </summary>
        /// <param name="cell">Ячейка, от которой отталкиваемся</param>
        /// <param name="grid">Грид</param>
        public override void AutoFill(Cell cell, Grid grid)
        {
            Cell fillCell = null;
            switch (cell.Key)
            {
                case "KLat":
                    {
                        if (station.objPosition != null)
                        {
                            double latDMS = station.objPosition.LatDms;
                            double lonDMS = station.objPosition.LongDms;
                            this.Longitude = lonDMS;
                            this.Latitude = latDMS;
                            cell.Value = HelpFunction.DmsToString(latDMS, EnumCoordLine.Lat);
                            OnCellValidate(cell, grid);

                            fillCell = grid.GetCellFromKey("KLon");
                            fillCell.Value = HelpFunction.DmsToString(lonDMS, EnumCoordLine.Lon);
                            OnCellValidate(fillCell, grid);

                            double asl = station.objPosition.Asl;
                            fillCell = grid.GetCellFromKey("kAsl");
                            if (fillCell != null)
                            {
                                fillCell.Value = (asl != IM.NullD) ? asl.ToString("0") : "";
                                OnCellValidate(fillCell, grid);
                            }

                            fillCell = grid.GetCellFromKey("kAddr");
                            fillCell.Value = station.objPosition.FullAddressAuto;
                            OnCellValidate(fillCell, grid);

                            fillCell = grid.GetCellFromKey("KPillarName");
                            if (fillCell != null)
                                fillCell.Value = station.objPosition.Name;
                        }
                        else
                        {
                            fillCell = grid.GetCellFromKey("KLon");
                            fillCell.Value = "";

                            fillCell = grid.GetCellFromKey("kAsl");
                            if (fillCell != null)
                                fillCell.Value = "";

                            fillCell = grid.GetCellFromKey("kAddr");
                            fillCell.Value = "";
                        }
                    }
                    break;

                case "equip":
                    {
                        if (station.objEquip != null)
                        {
                            // Заполяем поля значениями                    
                            cell.Value = station.objEquip.GetS("NAME");

                            fillCell = grid.GetCellFromKey("certno");
                            if (fillCell != null)
                                fillCell.Value = station.objEquip.GetS("CUST_TXT1").ToString();

                            fillCell = grid.GetCellFromKey("certda");
                            if (fillCell != null)
                            {
                                DateTime dt = station.objEquip.GetT("CUST_DAT1");

                                if (dt.Year > 1990)
                                    fillCell.Value = dt.ToString("dd.MM.yyyy");
                                else
                                    fillCell.Value = "";
                            }

                            fillCell = grid.GetCellFromKey("powVideo");
                            if (objStation.GetD("PWR_ANT") != IM.NullD)
                            // if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                            {
                                Power maxPower = new Power();
                                // double maxPower = (station.objEquip.GetD("MAX_POWER") - 30.0) / 10;
                                maxPower[PowerUnits.dBW] = objStation.GetD("PWR_ANT");
                                station.MaxPower = maxPower[PowerUnits.W];//Math.Round(Math.Pow(10, maxPower), 3);
                                fillCell.Value = station.MaxPower.ToString();

                                objStation.Put("PWR_ANT", maxPower[PowerUnits.dBW]);
                                // objStation.Put("PWR_ANT", 10.0 * Math.Log10(station.MaxPower));
                            }

                            fillCell = grid.GetCellFromKey("desigEmi");
                            if (fillCell != null)
                            {
                                string DesigEmission = objStation.GetS("DESIG_EM");
                                fillCell.Value = DesigEmission;
                            }

                            fillCell = grid.GetCellFromKey("rxband");
                            if (fillCell != null)
                            {
                                double BW = station.objEquip.GetD("BW");
                                fillCell.Value = (BW / 1000.0).ToString("F3");
                            }
                        }
                        else
                        {
                            cell.Value = "";

                            fillCell = grid.GetCellFromKey("certno");
                            if (fillCell != null)
                                fillCell.Value = "";

                            fillCell = grid.GetCellFromKey("certda");
                            if (fillCell != null)
                                fillCell.Value = "";
                        }
                    }
                    break;

                case "polar":
                    //-- From FMTVBase
                    AutoFillPolar(cell, grid.GetCellFromKey("horzGain"), grid.GetCellFromKey("vertGain"));
                    //-- From FMTVBase
                    break;
            }
        }

        //============================================================
        /// <summary>
        /// Внутреняя проверка содержимого ячейки
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void CellValidate(Cell cell, Grid grid)
        {
            switch (cell.Key)
            {
                case "KLon":
                    {
                        double lon = ConvertType.StrToDMS(cell.Value);
                        if (lon != IM.NullD)
                        {
                            cell.Value = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
                            if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                            if (station.objPosition != null)
                            {
                                station.objPosition.LongDms = lon;
                                IndicateDifference(cell, station.objPosition.LonDiffersFromAdm);
                            }
                            Longitude = lon;
                        }
                    }
                    break;

                case "KLat":
                    {
                        double lat = ConvertType.StrToDMS(cell.Value);
                        if (lat != IM.NullD)
                        {
                            cell.Value = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
                            if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                                ChangeColor(cell, Colors.badValue);
                            else
                                ChangeColor(cell, Colors.okvalue);
                            if (station.objPosition != null)
                            {
                                station.objPosition.LatDms = lat;
                                IndicateDifference(cell, station.objPosition.LatDiffersFromAdm);
                            }
                            Latitude = lat;
                        }
                    }
                    break;
                case "kAddr":
                    if (station.objPosition != null)
                        IndicateDifference(cell, station.objPosition.AddrDiffersFromAdm);
                    break;
                case "polar":
                case "dir":
                    {
                        Cell polarCell = grid.GetCellFromKey("polar");
                        Cell dirCell = grid.GetCellFromKey("dir");
                        Cell horzGainCell = grid.GetCellFromKey("horzGain");
                        Cell vertGainCell = grid.GetCellFromKey("vertGain");

                        OnValidateDirectionAndPolarization(polarCell, dirCell, horzGainCell, vertGainCell);
                    }
                    break;

                case "callsign":
                    objStation.Put("CALL_SIGN", cell.Value);
                    break;

                case "kAsl":
                    {
                        if (station.objPosition != null)
                        {
                            double asl = ConvertType.ToDouble(cell.Value, 0.0);
                            station.objPosition.Asl = asl;
                            NeedSavePosition = true;
                        }
                    }
                    break;

                case "homeState":
                    objStation.Put("CUST_TXT4", _internalStateList.GetShortValue(cell.Value));
                    break;

                case "internationalState":
                    objStation.Put("CUST_TXT5", _intnationalStateList.GetShortValue(cell.Value));
                    break;

                case "freq":
                    {
                        double tmp = ConvertType.ToDouble(cell.Value, 0.0);
                        objStation.Put("FREQ", tmp);
                        if (tmp < 0)
                            ChangeColor(cell, Colors.badValue);
                    }
                    break;

                case "powVideo":
                    {
                        if (!ChangePowVideo(grid))
                        {
                            UpdateOneParamGrid(cell, grid);
                        }
                    }
                    break;

                case "equip":
                    {
                        if (station.objEquip != null)
                        {
                            string DesigEmission = station.objEquip.GetS("DESIG_EMISSION");
                            double BandWidth = station.objEquip.GetD("BW");
                            objStation.Put("DESIG_EM", DesigEmission);
                            objStation.Put("BW", BandWidth);
                        }
                    }
                    break;

                case "fiderlen":
                    {
                        ChangeColor(cell, Colors.okvalue);
                        double FeederLen = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if (FeederLen == IM.NullD || FeederLen < 0.0)
                        {
                            ChangeColor(cell, Colors.badValue);
                        }
                        else
                        {
                            objStation.Put("CUST_NBR1", FeederLen);
                            ChangeVtratiFreq(grid);
                        }
                    }
                    break;

                case "fiderloss":
                    {
                        ChangeColor(cell, Colors.okvalue);
                        double FeederLoss = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if (FeederLoss == IM.NullD || FeederLoss < 0.0)
                        {
                            ChangeColor(cell, Colors.badValue);
                        }
                        else
                        {
                            objStation.Put("CUST_NBR2", FeederLoss);
                            ChangeVtratiFreq(grid);
                        }
                    }
                    break;

                case "tvchan":
                    {
                        if (_channelList.GetList().Count == 0 && !newStation)
                            return;

                        int index = 0;

                        if (string.IsNullOrEmpty(cell.Value))
                            return;

                        objStation.Put("CHANNEL", _channelList.GetShortValue(cell.Value));

                        if (station.objChannel != null)
                        {
                            //station.objChannel.Put("CHANNEL", _channelList.GetShortValue(cell.Value));
                            index = _channelList.GetList().IndexOf(cell.Value);
                        }
                        else if (newStation)
                        {
                            index = cell.PickList.IndexOf(cell.Value);
                            List<int> idChannelList = (List<int>)cell.Tag;
                            int id = idChannelList[index];

                            station.objChannel = IMObject.LoadFromDB(ICSMTbl.itblFreqPlanChan, id);
                            //station.objChannel.Put("CHANNEL", _channelList.GetShortValue(cell.Value));
                            objStation.Put("PLAN_ID", id);
                        }

                        string freq = string.Empty;
                        double freqVideo = 0;

                        try
                        {
                            freq = _freqList.GetList()[index];
                            freqVideo = ConvertType.ToDouble(freq, 0.0);
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                        //=======================================================================
                        //freq Video
                        cell = grid.GetCellFromKey("freqVid");
                        cell.Value = freq;
                        objStation.Put("FREQ", freqVideo);
                    }
                    break;

                case "height":
                    double height = ConvertType.ToDouble(cell.Value, IM.NullD);
                    if (height != IM.NullD)
                    {
                        if (height >= 0.0)
                        {
                            objStation.Put("AGL", height);
                        }
                        else
                        {
                            MessageBox.Show("The height of antenna must be greater");
                            UpdateOneParamGrid(cell, grid);
                        }
                    }
                    else
                    {
                        ShowErrorDigit();
                        UpdateOneParamGrid(cell, grid);
                    }
                    break;

                //case "maxHeight":
                //   objStation.Put("EFHGT_MAX", ConvertType.ToDouble(cell.Value, 0.0));
                //   break;

                case "NumbKont":
                    {
                        string aName = cell.Value;

                        if (station.objAsign != null)
                            station.objAsign.Put("INDX", aName);
                        _vidilenNameList.GetList().Clear();

                        switch (aName.Length)
                        {
                            case 1:
                                aName = "000" + aName;
                                break;
                            case 2:
                                aName = "00" + aName;
                                break;
                            case 3:
                                aName = "0" + aName;
                                break;

                        }

                        IMRecordset r = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadOnly);
                        r.Select("IS_ALLOTM,CLASS,A_NAME");
                        r.SetWhere("CLASS", IMRecordset.Operation.Like, "BT");
                        r.SetWhere("IS_ALLOTM", IMRecordset.Operation.Like, "Y");
                        r.SetWhere("A_NAME", IMRecordset.Operation.Like, "UKR" +"*"+ aName );
                        try
                        {
                            _vidilenNameList.AddValue("   ".ToString());
                            for (r.Open(); !r.IsEOF(); r.MoveNext())
                            {
                                _vidilenNameList.AddValue(r.GetS("A_NAME"));
                            }


                            Cell c = grid.GetCellFromKey("VidilenName");
                            c.Value = string.Empty;
                            c.PickList = _vidilenNameList.GetList();

                            if (_vidilenNameList.GetList().Count > 0)
                            {
                                c.Value = _vidilenNameList.GetList()[0];
                                objStation.Put("ADM_KEY", c.Value);
                            }

                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                    break;

                case "VidilenName":
                    objStation.Put("ADM_KEY", cell.Value);
                    break;

                case "SpectMaskType":
                    {
                        objStation.Put("SPECT_MASK", "");
                        if (string.IsNullOrEmpty(cell.Value) == false)
                        {
                            foreach (KeyValuePair<string, string> pair in _spectMaskTypeList)
                            {
                                if (pair.Value == cell.Value)
                                {
                                    objStation.Put("SPECT_MASK", pair.Key);
                                    break;
                                }
                            }
                        }
                    }
                    break;

                case "NesivVariant":
                    objStation.Put("DVBT_SYS_CODE", _nesivVariantList.GetShortValue(cell.Value));
                    break;

                case "NesivCount":
                    objStation.Put("NB_CARR", _nesivCountList.GetShortValue(cell.Value));
                    break;

                case "ProtecInterval":
                    {
                        string shortValue = _protecIntervalList.GetShortValue(cell.Value);
                        int value;
                        if (!int.TryParse(shortValue, out value))
                        {
                            UpdateOneParamGrid(cell, grid);
                            return;
                        }

                        objStation.Put("GUARD_INTERVAL", value);
                    }
                    break;

                case "NesivFreq":
                    {
                        double offset = ConvertType.ToDouble(cell.Value, 0.0);
                        objStation.Put("OFFSET", offset);
                    }
                    break;

                case "PriemnType":
                    // FMTV_ASSIGN.RX_MODE                  
                    station.objAsign.Put("RX_MODE", _priemnTypeList.GetShortValue(cell.Value));
                    break;

                case "StandKompr":
                    objStation.Put("CUST_TXT6", _standKomprList.GetShortValue(cell.Value));
                    break;

                case "OcmName":
                    objStation.Put("SFN_IDENT", cell.Value);
                    break;

                case "VtratiFreq":
                    {
                        ChangeColor(cell, Colors.okvalue);
                        double txLosses = ConvertType.ToDouble(cell.Value, IM.NullD);
                        if ((txLosses != IM.NullD) && (txLosses >= 0.0))
                        {
                            double cust1 = objStation.GetD("CUST_NBR1");
                            double cust2 = objStation.GetD("CUST_NBR2");
                            objStation.Put("TX_LOSSES", txLosses + cust1 * cust2);
                        }
                        else
                            ChangeColor(cell, Colors.badValue);
                    }
                    break;

                case "horzGain":
                    OnValidateGain(cell, "H");
                    break;

                case "vertGain":
                    OnValidateGain(cell, "V");
                    break;

                case "numbSinchNetwork":
                    station.Code = cell.Value;
                    break;
            }
        }

        private void ChangeVtratiFreq(Grid grid)
        {
            Cell c = grid.GetCellFromKey("VtratiFreq");

            double txLosses = ConvertType.ToDouble(c.Value, 0.0);
            double cust1 = objStation.GetD("CUST_NBR1");
            double cust2 = objStation.GetD("CUST_NBR2");

            objStation.Put("TX_LOSSES", txLosses + cust1 * cust2);
        }

        private bool ChangePowVideo(Grid grid)
        {
            Cell cell = grid.GetCellFromKey("powVideo");

            if (string.IsNullOrEmpty(cell.Value))
                return false;
            Power pow = new Power();
            pow[PowerUnits.W] = cell.Value.ToDouble(IM.NullD);
            //double powVideo = ConvertType.ToDouble(cell.Value, IM.NullD);
            if (pow[PowerUnits.W] != IM.NullD)
            {
                if (pow[PowerUnits.W] > station.MaxPower && station.MaxPower != 0)
                {
                    MessageBox.Show("Please enter less value then your Maximum Power " + pow[PowerUnits.W].ToString(),
                       "Please enter less value then your Maximum Power " + pow[PowerUnits.W].ToString(),
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                objStation.Put("PWR_ANT", pow[PowerUnits.dBW]);
                return true;
            }
            return false;
        }

        //===========================================================
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        ////============================================================
        ///// <summary>
        ///// Сохранение изменений в станции перед созданием отчета
        ///// </summary>
        //protected override void SaveBeforeDocPrint(DocType docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        //{
        //    base.SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
        //    IMRecordset r = new IMRecordset(recordID.Table, IMRecordset.Mode.ReadWrite);
        //    r.Select("ID,CUST_DAT2,CUST_DAT3,CUST_TXT2,CUST_TXT3");
        //    r.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
        //    try
        //    {
        //        r.Open();
        //        if (!r.IsEOF())
        //        {
        //            r.Edit();
        //            switch (docType)
        //            {
        //                case DocType.VISN:
        //                case DocType.VISN_NR:
        //                    r.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    objStation.Put("CUST_TXT2", docName.ToFileNameWithoutExtension());
        //                    r.Put("CUST_DAT2", startDate);
        //                    objStation.Put("CUST_DAT2", startDate);
        //                    break;
        //            }
        //            r.Update();
        //        }
        //    }
        //    finally
        //    {
        //        r.Close();
        //        r.Destroy();
        //    }
        //}

        private void ShowErrorDigit()
        {
            //MessageBox.Show(CLocaliz.TxT("Please input only digit value"),
            //   CLocaliz.TxT("Please input only digit value"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            try
            {
                if (cell.Key == "kAddr")
                {
                    if (station.objPosition != null)
                    {
                        int posID = station.objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(ICSMTbl.itblPositionBro, posID, (IM.TableRight(ICSMTbl.itblPositionBro) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        station.objPosition.LoadStatePosition(station.objPosition.Id, station.objPosition.TableName);
                        station.objPosition.GetAdminSiteInfo(station.objPosition.AdminSiteId);
                        AutoFill(gridParam.GetCellFromKey("KLat"), gridParam);
                    }
                    else
                        ShowMessageNoReference();
                }
                else if ((cell.Key == "equip") && (station.objEquip != null))
                {
                    int EquipID = station.objEquip.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(station.objEquip.GetS("TABLE_NAME"), EquipID);
                    rcPtr.UserEdit();
                }
                else if ((cell.Key == "KObj") && (objStation != null))
                {
                    int ID = objStation.GetI("ID");
                    RecordPtr rcPtr = new RecordPtr(TableName, ID);
                    rcPtr.UserEdit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        //============================================================
        /// <summary>
        /// Вызов формы для предварительных расчетов ЕМС
        /// </summary>
        public override void CalculateEMS()
        {
            FormVRS formVRS = new FormVRS(recordID.Id, recordID.Table);
            formVRS.ShowDialog();
            formVRS.Dispose();
        }

        private void FillComboList()
        {
            //DvbtGuardInt.ERI - Захисний інтервал 
            //DvbtNbCarr.ERI - Кількість несучих 
            //DvbtSys.ERI - Варіант системи 

            // Поляризація
            _polarizationList.AddValueL("V", "Вертикальна");
            _polarizationList.AddValueL("H", "Горизонтальна");
            _polarizationList.AddValueL("M", "Змішана");

            // Спрямованість антени 
            _orientationList.AddValue("D", "спрямована");
            _orientationList.AddValue("ND", "неспрямована");

            // fills Внутрішній стан 

            _internalStateList.AddValueL("1", "Рассматриваемый");
            _internalStateList.AddValueL("2", "Разрешенный");
            _internalStateList.AddValueL("3", "Действующий");
            _internalStateList.AddValueL("4", "Новое сост.8");
            _internalStateList.AddValueL("5", "Новое сост.2");
            _internalStateList.AddValueL("6", "Новое сост.3");
            _internalStateList.AddValueL("7", "Столб");
            _internalStateList.AddValueL("8", "Рекоммендация");
            _internalStateList.AddValueL("9", "Уточнение хар-к");
            _internalStateList.AddValueL("10", "Планируемый");
            _internalStateList.AddValueL("33", "BR IFIC ADD");
            _internalStateList.AddValueL("34", "BR IFIC REC");
            _internalStateList.AddValueL("35", "BR IFIC SUP");
            _internalStateList.AddValueL("36", "BR IFIC MOD");
            _internalStateList.AddValueL("38", "Geneva 06");

            // Міжнародний стан
            _intnationalStateList.AddValueL("A", "Скоорд.");
            _intnationalStateList.AddValueL("B", "Берлин 59");
            _intnationalStateList.AddValueL("C", "Чужой скоорд.");
            _intnationalStateList.AddValueL("D", "Удаленный, был.");
            _intnationalStateList.AddValueL("E", "Цифра, планируем.");
            _intnationalStateList.AddValueL("F", "Прошел коорд.");
            _intnationalStateList.AddValueL("G", "Женева 84");
            _intnationalStateList.AddValueL("G06", "GENEVA 2006");
            _intnationalStateList.AddValueL("H", "Цифра, план Честер");
            _intnationalStateList.AddValueL("I", "Честер");
            _intnationalStateList.AddValueL("K", "На коорд.");
            _intnationalStateList.AddValueL("L", "Цифра, удаленный");
            _intnationalStateList.AddValueL("M", "Цифра, не надо коорд.");
            _intnationalStateList.AddValueL("N", "Не надо коорд.");
            _intnationalStateList.AddValueL("O", "Планируемый");
            _intnationalStateList.AddValueL("P", "Под крышей");
            _intnationalStateList.AddValueL("Q", "Цифра, регистр.");
            _intnationalStateList.AddValueL("R", "Регистр");
            _intnationalStateList.AddValueL("S", "Стокгольм");
            _intnationalStateList.AddValueL("T", "Цифра, скоорд.");
            _intnationalStateList.AddValueL("TP", "Цифра, скоорд. на пп");
            _intnationalStateList.AddValueL("U", "Цифра на коорд.");
            _intnationalStateList.AddValueL("Y", "Цифра, нескоорд.");
            _intnationalStateList.AddValueL("Z", "Не скоорд.");

            //DvbtNbCarr.ERI - Кількість несучих
            _nesivCountList.AddRangeValue("DvbtNbCarr");

            //DvbtGuardInt.ERI - Захисний інтервал 
            _protecIntervalList.AddRangeValueDict("DvbtGuardInt");

            // тип спектр. маски фільтра 
            _spectMaskTypeList = EriFiles.GetEriCodeAndDescr("RrcSpectMaskBt");

            // тип прийома
            _priemnTypeList.AddValueL("MO", "мобільний");
            _priemnTypeList.AddValueL("FX", "фіксований");
            _priemnTypeList.AddValueL("PO", "портат. зовн.");
            _priemnTypeList.AddValueL("PI", "портат. внутр.");

            //DvbtSys.ERI - Варіант системи 
            _nesivVariantList.AddRangeValueDictL("DvbtSys");

            _standKomprList.AddValue("MPEG2");
            _standKomprList.AddValue("MPEG4");
        }

        public double GetPower()
        {
            Power pow = new Power();
            double powVideo = 0;
            if (objStation.GetD("PWR_ANT") != IM.NullD)
            {
                //powVideo = objStation.GetD("PWR_ANT");
                //powVideo = Math.Round(Math.Pow(10.0, powVideo / 10), 3);
                pow[PowerUnits.dBW] = objStation.GetD("PWR_ANT");
                return pow[PowerUnits.W];
            }
            /* else if (station.objEquip != null)
             {
                if (station.objEquip.GetD("MAX_POWER") != IM.NullD)
                {
                   return station.objEquip.GetD("MAX_POWER");
                }
             }*/
            return powVideo;
        }

        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            if (Standard == CRadioTech.DVBT2)
                return "DVBT2";
            return "";
        }

        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                rs.Select("ID,Position.REMARK");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    adress = rs.GetS("Position.REMARK");
                    count = 1;
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return count;
        }
    }
}
