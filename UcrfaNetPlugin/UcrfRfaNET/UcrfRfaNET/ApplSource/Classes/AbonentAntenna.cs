﻿using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class AbonentAntenna : Antenna
    {
        /// <summary>
        /// Загрузить
        /// </summary>
        public override void Load()
        {
            _tableName = PlugTbl.XfaAnten;
            //ICSMTbl.itblMicrowAntenna
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                    Name = r.GetS("NAME");
            }
            finally
            {
                r.Final();
            }
        }
        /// <summary>
        /// Сохранить 
        /// </summary>
        public override void Save()
        {
        }
    }
}
