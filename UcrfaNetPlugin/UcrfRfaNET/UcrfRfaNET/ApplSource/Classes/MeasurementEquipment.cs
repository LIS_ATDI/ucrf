﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MeasurementEquipment : Equipment
    {
        public string EquipmentType { get; set; }
        public string Serial{ get; set; }
        public string Note { get; set; }
        public DateTime Checkdate { get; set; }

        private new MeasureEquipmentCertificate _certificate = new MeasureEquipmentCertificate();
        public new MeasureEquipmentCertificate Certificate{ get { return _certificate; } }

        public MeasurementEquipment()
        {
            TableName = PlugTbl.MeasureEquipment;
            Id = IM.NullI;
            Name = "";

            _certificate.Id = IM.NullI;
            EquipmentType = "";
            Serial= "";
            Note = "";
            Checkdate = IM.NullT;
        }

        public override void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,NAME,DEV_TYPE,SERIAL,NOTES,CERT_ID,CHECKDATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                _certificate = new MeasureEquipmentCertificate();

                if (!r.IsEOF())
                {
                    Name = r.GetS("NAME");
                    EquipmentType = r.GetS("DEV_TYPE");
                    Serial= r.GetS("SERIAL");
                    Note = r.GetS("NOTES");
                    Checkdate = r.GetT("CHECKDATE");

                    _certificate.Id = r.GetI("CERT_ID");

                    if (_certificate.Id!=IM.NullI)
                        _certificate.Load();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,NAME,DEV_TYPE,SERIAL,NOTES,CERT_ID,CHECKDATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();
                } 
                else
                {
                    Id = IM.AllocID(TableName, 1, -1);
                    r.AddNew();
                    r.Put("ID", Id);
                }

                r.Put("NAME", Name);
                r.Put("DEV_TYPE", EquipmentType);
                r.Put("SERIAL", Serial);
                r.Put("NOTES", Note);
                r.Put("CERT_ID", _certificate.Id);
                r.Put("CHECKDATE", Checkdate);

                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
