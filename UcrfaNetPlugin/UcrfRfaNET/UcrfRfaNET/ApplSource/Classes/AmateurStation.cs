﻿using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class  AmateurStation : BaseStation
    {
        public const string TableName = "XFA_AMATEUR";
        public const string TableNamePosition = "XFA_POSITION";
        public const string TableNameEs = "XFA_ES_AMATEUR";
        public const string TableNameEquip = "XFA_EQUIP_AMATEUR";
        public const string TableNameFe = "XFA_FE_AMATEUR";
        public const string TableNameFreq = "XFA_FREQ_AMATEUR";
        public const string TableNameBand = "XFA_BAND_AMATEUR";

        public PositionState2 objPosition = null;//Позиция

        public static int Count { get; set; }
        public static int Number { get; set; }
        //список обладнання
        public List<AmateurEquipment> AmEquip;

        //Список обладнання 
        public List<AmateurCalls> AmCallsSps = new List<AmateurCalls>();
        public List<AmateurCalls> AmCallsUps = new List<AmateurCalls>();

        /// <summary>
        /// Id власника
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Id Власник РЕЗ
        /// </summary>
        public int AbonentResId { get; set; }
        /// <summary>
        /// Id позиції
        /// </summary>
        private int posId = IM.NullI;

        /// <summary>
        /// Тип станції
        /// </summary>
        public string TypeStat { get; set; }

        /// <summary>
        /// Власник РЕЗ
        /// </summary>
        public string NameOwner { get; set; }

        /// <summary>
        /// Категорія оператора
        /// </summary>
        public string CategOper { get; set; }

        /// <summary>
        /// Категорія оператора
        /// </summary>
        public string Call { get; set; }

        /// <summary>
        /// Тип користування АРС
        /// </summary>
        public string Apc { get; set; }

        /// <summary>
        /// Висота антени над рівнем Землі, м
        /// </summary>
        public double AGL { get; set; }

        /// <summary>
        /// Тип гарм-го екзам-го сертифікату
        /// </summary>
        public string Cert { get; set; }

        /// <summary>
        /// Номер бланку
        /// </summary>
        public string NumbBlank { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Спецю условия дозвола
        /// </summary>
        public string SpecConditionPerm { get; set; }
        /// <summary>
        /// Спецю условия высновка
        /// </summary>
        public string SpecConditionConcl { get; set; }
        /// <summary>
        /// Замечания
        /// </summary>
        public string SpecNote { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public AmateurStation()
        {
            SpecConditionPerm = string.Empty;
            SpecConditionConcl = string.Empty;
            SpecNote = string.Empty;
        }
        /// <summary>
        /// Проверка на наличие указанного позывного в базе (если запись уже есть  возвращает false)
        /// <params> CALL_SIGN (позывной для проверки)
        /// <params> status_include (признак исключения из рассмотрения текущей открытой для редактирования записи)
        /// </summary>
        public bool GetStatus_DublicateCallSign(string CALL_SIGN, int OWNER_ID, bool status_include)
        {
            bool res = true;
            IMRecordset rx = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                rx.Select("ID,OWNER_ID,CALL_SIGN");
                rx.SetWhere("CALL_SIGN", IMRecordset.Operation.Eq , CALL_SIGN);
                if (status_include) { rx.SetWhere("ID", IMRecordset.Operation.Neq, Id); }
                rx.Open();


                if (!rx.IsEOF())
                {
                    while (!rx.IsEOF())
                    {
                        if (rx.GetI("OWNER_ID") != OWNER_ID)
                        {
                            res = false;
                            break;
                        }
                        rx.MoveNext();
                    }
                }
                else
                {
                    if (rx.IsEOF())
                    {
                        res = true;
                    }
                }

            }
            finally
            {
                rx.Final();
            }

            return res;
        }
        /// <summary>
        /// Сохраняет данные станции
        /// 
        /// </summary>
        public override void Save()
        {

             
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,TABLE_NAME,POS_ID,OWNER_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                r.Select("TYPE_STAT,OWNER_ID,USER_ID,CATEG_OPER,CALL_SIGN,APC,AGL,CERT,NUMB_BLANK");
                r.Select("STATUS");
                r.Select("SPEC_COND_DOZV,SPEC_COND_VISN,NOTE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (r.IsEOF())
                {
                    if (GetStatus_DublicateCallSign(Call.ToString().TrimEnd(),OwnerId, false) == false)
                    {

                        if (MessageBox.Show(CLocaliz.TxT("This callsign already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            return;
                        }

                        
                    }
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                    r.Put("TABLE_NAME", TableName);
                }
                else
                {
                    if (GetStatus_DublicateCallSign(Call.ToString().TrimEnd(), OwnerId,true) == false)
                    {
                        if (MessageBox.Show(CLocaliz.TxT("This callsign already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            return;
                        }
                    }

                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
                r.Put("USER_ID", AbonentResId);
                r.Put("OWNER_ID", OwnerId);
                r.Put("TYPE_STAT", TypeStat);
                r.Put("CATEG_OPER", CategOper);
                r.Put("CALL_SIGN", Call);
                r.Put("STATUS",Status);
                r.Put("APC", Apc);
                r.Put("AGL", AGL);
                r.Put("CERT", Cert);
                r.Put("SPEC_COND_DOZV", SpecConditionPerm);
                r.Put("SPEC_COND_VISN", SpecConditionConcl);
                r.Put("NOTE", SpecNote);

                r.Put("POS_ID", objPosition != null ? objPosition.Id : IM.NullI);

                r.Update();
            }
            finally
            {
                r.Final();
            }

            //Обладнання
            IMRecordset rEquip = new IMRecordset(TableNameEs, IMRecordset.Mode.ReadWrite);
            rEquip.Select("ID,STA_ID,EQUIP_ID,NUMB,PLANT_NUMB,POWER,DEVI,PASSING");
            rEquip.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            for (rEquip.Open(); !rEquip.IsEOF(); rEquip.MoveNext())
            {
                int refId = rEquip.GetI("ID");

                /*
                //delete bands (if record still exists - bands will be recreated)
                IMRecordset rBand = new IMRecordset(TableNameFe, IMRecordset.Mode.ReadWrite);
                rBand.Select("ID,SEL_EQUIP_ID");
                rBand.SetWhere("SEL_EQUIP_ID", IMRecordset.Operation.Eq, refId);
                for (rBand.Open(); !rBand.IsEOF(); rBand.MoveNext())
                    rBand.Delete();
                rBand.Final();

                //delete freqs  (if record still exists - freqs will be recreated)
                IMRecordset rFreq = new IMRecordset(TableNameFreq, IMRecordset.Mode.ReadWrite);
                rFreq.Select("ID,CHANNEL,TX_FREQ,RX_FREQ,FREQ_ID");
                rFreq.SetWhere("FREQ_ID", IMRecordset.Operation.Eq, refId);
                for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
                    rFreq.Delete();
                rFreq.Final();
                 * */
                
                //find equipment
                AmateurEquipment amEq = AmEquip.FirstOrDefault(ae => ae.RefId == refId);
                if (amEq != null)
                {
                    //and update it
                    amEq.Number = AmEquip.IndexOf(amEq) + 1;
                    amEq.SaveRef(rEquip);
                }
                else
                {
                    //else delete record from db
                    rEquip.Delete();
                }
            }
            
            // new entries, not yet saved in DB
            foreach (AmateurEquipment amEq in AmEquip.FindAll(ae => ae.RefId == IM.NullI))
            {
                amEq.Number = AmEquip.IndexOf(amEq) + 1;
                amEq.RefId = IM.AllocID(TableNameEs, 1, -1);
                amEq.StaId = Id;
                amEq.SaveRef(rEquip, true);
            }

            SetUpsSps();

            rEquip.Destroy();

            IM.RefreshQueries(TableName);
        }


        /// <summary>
        /// Метод, выполняющий подсчет общего числа записей таблицы XfaCallAmateur для различных типов (СПС или УПС)
        /// </summary>
        /// <param name="Id_">номер STA_ID</param>
        /// <param name="status_">Искомый тип (УПС или СПС)</param>
        /// <returns>Количество найденных записей</returns>
        public int GetCountRecordInBase(int Id_, string status_)
        {
            IMRecordset rSp = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadWrite);
            rSp.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
            rSp.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id_);
            rSp.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
            rSp.SetWhere("TYPE_CALL", IMRecordset.Operation.Eq, status_);
            rSp.OrderBy("ID", OrderDirection.Ascending);

            int index_S = 0;

            try
            {
            rSp.Open();
            while (!rSp.IsEOF())
                 {
                    index_S++;
                     rSp.MoveNext();
                 }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            return index_S;
        }

        /// <summary>
        /// Выполняем операцию сохранения
        /// </summary>

        private void SetUpsSps()
        {

            IMRecordset rSps2 = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadWrite);
            rSps2.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
            rSps2.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            rSps2.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
            rSps2.SetWhere("TYPE_CALL", IMRecordset.Operation.Eq, "СПС");
            rSps2.OrderBy("ID",OrderDirection.Ascending);

            IMRecordset rSps3 = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadWrite);
            rSps3.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
            rSps3.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            rSps3.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
            rSps3.SetWhere("TYPE_CALL", IMRecordset.Operation.Eq, "УПС");
            rSps3.OrderBy("ID",OrderDirection.Ascending);


            try
            {

               rSps2.Open();

                    if ((AmCallsSps != null) && (AmCallsSps.Count() > 0))
                    {
                        List<int> LstIndex = new List<int>();
                        LstIndex.Clear();
                        int cnt_record_inbase = 0;
                        cnt_record_inbase = GetCountRecordInBase(Id, "СПС");
                        int sub_value = Math.Abs(cnt_record_inbase - AmCallsSps.Count());

                        if (sub_value>0)
                        {

                            for (int nn = 0; nn < sub_value; nn++)
                            {
                                rSps2.AddNew();
                                rSps2["ID"] = IM.AllocID(PlugTbl.XfaCallAmateur, 1, -1);
                                rSps2["STA_ID"] = Id;
                                rSps2.Put("TYPE_CALL", "СПС");
                                rSps2.Put("CALL", "");
                                rSps2.Put("GOAL_USED", "");
                                rSps2.Put("DATE_IN", DateTime.Now.Date);
                                rSps2.Put("DATE_OUT", DateTime.Now.AddMonths(1));
                                rSps2.Update();
                            }
                        }
                    }
                    rSps2.Close();
                    rSps3.Open();
                        if ((AmCallsUps != null) && (AmCallsUps.Count() > 0))
                        {
                            List<int> LstIndex = new List<int>();
                            LstIndex.Clear();

                            int cnt_record_inbase = 0;
                            cnt_record_inbase = GetCountRecordInBase(Id, "УПС");
                            int sub_value = Math.Abs(cnt_record_inbase - AmCallsUps.Count());
                            if (sub_value > 0)
                            {

                                for (int nn = 0; nn < sub_value; nn++)
                                {
                                    rSps3.AddNew();
                                    rSps3["ID"] = IM.AllocID(PlugTbl.XfaCallAmateur, 1, -1);
                                    rSps3["STA_ID"] = Id;
                                    rSps3.Put("TYPE_CALL", "УПС");
                                    rSps3.Put("CALL", "");
                                    rSps3.Put("GOAL_USED", "");
                                    rSps3.Put("DATE_IN", DateTime.Now.Date);
                                    rSps3.Put("DATE_OUT", DateTime.Now.AddMonths(1));


                                    rSps3.Update();
                                }
                            }
                        }


                    rSps3.Close();

                    //////////////////////////////////////////////////////////////////////////////////////////////

                    IMRecordset rSps = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadWrite);
                    rSps.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
                    rSps.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
                    rSps.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
                    rSps.OrderBy("ID", OrderDirection.Ascending);



                    rSps.Open();

                    int index_Sps = 0; int index_Ups = 0;

                    while (!rSps.IsEOF())
                    {
                        string type;
                  
                        if ((AmCallsSps != null) && (AmCallsSps.Count() > 0))
                        {
                        
                            if (index_Sps < AmCallsSps.Count())
                            {
                                 type = rSps.GetS("TYPE_CALL");

                                if (type == "СПС")
                                {

                                    rSps.Edit();
                                    rSps.Put("TYPE_CALL", AmCallsSps[index_Sps].TypeCall);
                                    rSps.Put("CALL", AmCallsSps[index_Sps].Call);
                                    rSps.Put("GOAL_USED", AmCallsSps[index_Sps].GoalUse);
                                    index_Sps++;
                                    rSps.Update();
                                }

                            }
                        }
                    

                        if ((AmCallsUps != null) && (AmCallsUps.Count()>0))
                        {
                            if (index_Ups < AmCallsSps.Count())
                            {
                                 type = rSps.GetS("TYPE_CALL");
                                if (type == "УПС")
                                {
                                    rSps.Edit();
                                    rSps.Put("TYPE_CALL", AmCallsUps[index_Ups].TypeCall);
                                    rSps.Put("CALL", AmCallsUps[index_Ups].Call);
                                    rSps.Put("GOAL_USED", AmCallsUps[index_Ups].GoalUse);
                                    index_Ups++;
                                    rSps.Update();
                                }

                            }
                        }
                    

                        rSps.MoveNext();
                    }
                    rSps.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }



        }



        /// <summary>
        /// Загружаем данные станции
        /// </summary>
        public override void Load()
        {
            AmEquip = new List<AmateurEquipment>();
            //Основна станція
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TABLE_NAME,POS_ID,OWNER_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                r.Select("TYPE_STAT,USER_ID,CATEG_OPER,CALL_SIGN,APC,AGL,CERT,NUMB_BLANK");
                r.Select("STATUS");
                r.Select("SPEC_COND_DOZV,SPEC_COND_VISN,NOTE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Status = r.GetS("STATUS");
                    posId = r.GetI("POS_ID");
                    OwnerId = r.GetI("OWNER_ID");
                    AbonentResId = r.GetI("USER_ID");
                    TypeStat = r.GetS("TYPE_STAT");
                    CategOper = r.GetS("CATEG_OPER");
                    Call = r.GetS("CALL_SIGN");
                    Apc = r.GetS("APC");
                    AGL = r.GetD("AGL");
                    Cert = r.GetS("CERT");
                    SpecConditionPerm = r.GetS("SPEC_COND_DOZV");
                    SpecConditionConcl = r.GetS("SPEC_COND_VISN");
                    SpecNote = r.GetS("NOTE");
                }
            }
            finally
            {
                r.Final();
            }
            //Позиція
            LoadPosition();
            //Власник
            NameOwner = GetNameAbonentRes();
            //Обладнання            
            GetEquipmentList();
            //УПС СПС
            GetUpsSps();
        }

        private void GetUpsSps()
        {
            IMRecordset rSps = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadOnly);
            rSps.Select("ID,STA_ID,TABLE_NAME,TYPE_CALL,CALL,GOAL_USED,DATE_IN,DATE_OUT,NUMBER_DOZV,NUMB,PATH");
            rSps.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            rSps.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
            for (rSps.Open(); !rSps.IsEOF(); rSps.MoveNext())
            {
                string type = rSps.GetS("TYPE_CALL");
                if (type == "СПС")
                {
                    AmateurCalls tempSps = new AmateurCalls();
                    tempSps.StaId = Id;
                    tempSps.Call = rSps.GetS("CALL");
                    tempSps.GoalUse = rSps.GetS("GOAL_USED");
                    tempSps.DateIn = rSps.GetT("DATE_IN");
                    tempSps.DateOut = rSps.GetT("DATE_OUT");
                    tempSps.NumberDocument = rSps.GetS("NUMBER_DOZV");
                    tempSps.NumberBlank = rSps.GetS("NUMB");
                    tempSps.PathFile = rSps.GetS("PATH");
                    tempSps.TypeCall = type;
                    AmCallsSps.Add(tempSps);
                }
                else if (type == "УПС")
                {
                    AmateurCalls tempUps = new AmateurCalls();
                    tempUps.StaId = Id;
                    tempUps.Call = rSps.GetS("CALL");
                    tempUps.GoalUse = rSps.GetS("GOAL_USED");
                    tempUps.DateIn = rSps.GetT("DATE_IN");
                    tempUps.DateOut = rSps.GetT("DATE_OUT");
                    tempUps.NumberDocument = rSps.GetS("NUMBER_DOZV");
                    tempUps.NumberBlank = rSps.GetS("NUMB");
                    tempUps.PathFile = rSps.GetS("PATH");
                    tempUps.TypeCall = type;
                    AmCallsUps.Add(tempUps);
                }
            }
        }

        private void GetEquipmentList()
        {
            IMRecordset r = new IMRecordset(TableNameEs, IMRecordset.Mode.ReadOnly);
            r.Select("ID,STA_ID,EQUIP_ID,NUMB,PLANT_NUMB,POWER,DEVI,PASSING");
            r.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            r.OrderBy("NUMB", OrderDirection.Ascending);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
                Count++;
                AmateurEquipment eqp = new AmateurEquipment();
                eqp.RefId = r.GetI("ID");
                eqp.StaId = r.GetI("STA_ID");
                eqp.Load(r.GetI("EQUIP_ID"));
                eqp.Number = r.GetI("NUMB");
                eqp.PlantNumb = r.GetS("PLANT_NUMB");
                eqp.Power[PowerUnits.W] = r.GetD("POWER");
                eqp.Devi = r.GetS("DEVI");
                eqp.Passing = r.GetS("PASSING");
                IMRecordset rBand = new IMRecordset(TableNameFe, IMRecordset.Mode.ReadOnly);
                try
                {
                    rBand.Select("ID,SEL_EQUIP_ID,BAND_ID");
                    rBand.SetWhere("SEL_EQUIP_ID", IMRecordset.Operation.Eq, eqp.RefId);
                    for (rBand.Open(); !rBand.IsEOF(); rBand.MoveNext())
                    {
                        AmateurBand tempBand = new AmateurBand();
                        int bandId = rBand.GetI("BAND_ID");
                        tempBand.Load(bandId);
                        eqp.AmBand.Add(tempBand);
                    }
                }
                finally
                {
                    rBand.Final();
                }

                IMRecordset rFreq = new IMRecordset(TableNameFreq, IMRecordset.Mode.ReadOnly);
                try
                {
                    rFreq.Select("ID,CHANNEL,TX_FREQ,RX_FREQ,FREQ_ID");
                    rFreq.SetWhere("FREQ_ID", IMRecordset.Operation.Eq, eqp.RefId);
                    for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
                    {
                        AmateurFreq tempFreq = new AmateurFreq();
                        tempFreq.FreqRx = rFreq.GetD("RX_FREQ");
                        tempFreq.FreqTx = rFreq.GetD("TX_FREQ");
                        tempFreq.Id = rFreq.GetI("ID");
                        tempFreq.EquipId = eqp.RefId;
                        eqp.AmFreq.Add(tempFreq);
                    }
                }
                finally
                {
                    rFreq.Final();
                }

                AmEquip.Add(eqp);
            }
            r.Final();
            if (AmEquip.Count == 0)
                AmEquip.Add(new AmateurEquipment());

            AmEquip = (from x in AmEquip
                       orderby x.Number
                       select x).ToList();
        }

        private void LoadPosition()
        {
            if (posId != IM.NullI)
            {
                objPosition = new PositionState2();
                objPosition.LoadStatePosition(posId, TableNamePosition);
            }
            else
                objPosition = null;
        }

        /// <summary>
        /// Возвращает имя обонента
        /// </summary>
        /// <returns>имя обонента</returns>
        public string GetNameAbonentRes()
        {
            string retVal = "";
            IMRecordset r = new IMRecordset(PlugTbl.ABONENT_OWNER, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, OwnerId);
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetS("NAME");
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            return retVal;
        }       

        public static string TypeCert(string table, int id)
        {           
            IMRecordset r = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,CERT");
                r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                r.Open();
                if (!r.IsEOF())
                {
                    string cert = r.GetS("CERT");
                    if (!string.IsNullOrEmpty(cert))
                        return cert;                    
                }
                return "";
            }
            finally
            {
                r.Final();
            }
        }
        
        public void SaveMoveStat(DateTime date1,DateTime date2, string docName, string path,int numberEquip)
        {
            IMRecordset r = new IMRecordset(TableNameEs, IMRecordset.Mode.ReadWrite);
            r.Select("ID,NUMB,STA_ID,NUMB_DOZV,DATE_PRINT_DOZV,DATE_EXPIR_DOZV,PATH");
            r.SetWhere("STA_ID",IMRecordset.Operation.Eq,Id);
            r.SetWhere("NUMB", IMRecordset.Operation.Eq, numberEquip);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("NUMB_DOZV", docName);
                    r.Put("DATE_PRINT_DOZV", date1);
                    r.Put("DATE_EXPIR_DOZV", date2);
                    r.Put("PATH", path);
                    r.Update();
                }
            }
            finally 
            {
                r.Final();
            }
        }
        /// <summary>
        /// Сохранить данные документа сертификата (генерация документа)
        /// </summary>
        /// <param name="dateFrom">Дата от</param>
        /// <param name="dateTo">Дата до</param>
        /// <param name="path">Полный путь к файлу</param>
        public void SaveCertificate(DateTime dateFrom, DateTime dateTo, string path)
        {
            using(Icsm.LisRecordSet rs = new Icsm.LisRecordSet(TableName, IMRecordset.Mode.ReadWrite))
            {
                rs.Select("ID");
                rs.Select("CERTIF_NUMB");
                rs.Select("CERTIF_DATE_FROM");
                rs.Select("CERTIF_DATE_TO");
                rs.Select("CERTIF_PATH");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                rs.Open();
                if(!rs.IsEOF())
                {
                    rs.Edit();
                    rs.Put("CERTIF_NUMB", path.ToFileNameWithoutExtension());
                    rs.Put("CERTIF_DATE_FROM", dateFrom);
                    rs.Put("CERTIF_DATE_TO", dateTo);
                    rs.Put("CERTIF_PATH", path);
                    rs.Update();
                }
            }
        }
        /// <summary>
        /// Печать сертификата
        /// </summary>
        /// <param name="datePrint">Дата печати</param>
        public void SavePrintCertificate(DateTime datePrint)
        {
            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(TableName, IMRecordset.Mode.ReadWrite))
            {
                rs.Select("ID");
                rs.Select("CERTIF_DATE_PRINT");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                rs.Open();
                if (!rs.IsEOF())
                {
                    rs.Edit();
                    rs.Put("CERTIF_DATE_PRINT", datePrint);
                    rs.Update();
                }
            }
        }
        /// <summary>
        /// Выдача сертификата
        /// </summary>
        /// <param name="dateDelivery">Дата выдачи</param>
        public void SaveDeliveryCertificate(DateTime dateDelivery)
        {
            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(TableName, IMRecordset.Mode.ReadWrite))
            {
                rs.Select("ID");
                rs.Select("CERTIF_DELIVERY");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                rs.Open();
                if (!rs.IsEOF())
                {
                    rs.Edit();
                    rs.Put("CERTIF_DELIVERY", dateDelivery);
                    rs.Update();
                }
            }
        }
        /// <summary>
        /// Вертає Id таблиці xfa_es_amateur по станції і номер РЕЗ
        /// </summary>
        /// <param name="id">станція</param>
        /// <param name="numberEquip">Номер оборудования по порядку</param>
        /// <returns></returns>
        public int GetXfaEsId(int id, int numberEquip)
        {
            int ret = IM.NullI;
            IMRecordset r=new IMRecordset(PlugTbl.XfaEsAmateur,IMRecordset.Mode.ReadOnly);
            r.Select("ID,STA_ID,NUMB");
            r.SetWhere("STA_ID",IMRecordset.Operation.Eq,id);
            r.SetWhere("NUMB", IMRecordset.Operation.Eq, numberEquip);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    ret = r.GetI("ID");
                }
            }
            finally 
            {
               r.Final();                
            }
            return ret;
        }

        /// <summary>
        /// Вертає Id таблиці xfa_call_amateur по станції і значенню
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="value">СПС/УПС</param>
        /// <param name="s">значення СПС/УПС</param>
        /// <returns></returns>
        public int GetXfaCallId(int id, string value, string s)
        {
            int ret = 0;
            IMRecordset r = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadOnly);
            r.Select("ID,STA_ID,TYPE_CALL,CALL,DATE_OUT");
            r.SetWhere("STA_ID", IMRecordset.Operation.Eq, id);
            r.SetWhere("TYPE_CALL", IMRecordset.Operation.Eq, value);
            r.SetWhere("CALL", IMRecordset.Operation.Eq, s);
            r.SetWhere("DATE_OUT", IMRecordset.Operation.Ge, DateTime.Now.Date);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    ret = r.GetI("ID");
                }
            }
            finally
            {
                r.Final();
            }
            return ret;
        }
    }
}
