﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MicrowaveStation : ICloneable
    {
        public int Id { get; set; }
        private MicrowaveAntenna _antenna = new MicrowaveAntenna();
        private Power _power = new Power();

        public PositionState Position { get; set; }
        public double Azimuth { get; set; }

        public Power Power { get { return _power; } }

        //public int AntennaId { get; set; }
        public double ASL { get; set; }
        //public double Diameter { get; set; }
        public double AGL { get; set; }
        public string Polarization { get; set; }
        public double Gain { get; set; }

        public MicrowaveAntenna Antenna { get { return _antenna; } }

        public double Frequency { get; set; }

        public MicrowaveStation()
        {
            Id = IM.NullI;                                
        }

        public object Clone()
        {
            MicrowaveStation clone = new MicrowaveStation();

            clone._antenna = _antenna.Clone() as MicrowaveAntenna;
            clone._power = _power.Clone() as Power;

            clone.Id = Id;
            clone.Position = Position.Clone() as PositionState;
            clone.Azimuth = Azimuth;

            clone.ASL = ASL;

            clone.AGL = AGL;
            clone.Polarization = Polarization;
            clone.Gain = Gain;
            clone.Frequency = Frequency;

            return clone;
        }
    }

    class MicrowaveLink : BaseStation, ICloneable
    {
        protected double _range = 0.0;
        protected MicrowaveEquipment _equipment = new MicrowaveEquipment();
        public string DesigEmission { get; set; }

        //public int EquipmentId { get; set; }
        public double Range { get { return _range; } }

        //public string EquipmentName { get; set; }
        //public double Bandwith { get; set; }
        //public string DesigEmission { get; set; }
        //public string CertificateNumber { get; set; }
        //public string CertificateDate { get; set; }
        public MicrowaveEquipment Equipment { get { return _equipment; } }

        /// <summary>
        /// Позиция первой станции, нужна большей частью для определения региона.
        /// Только для чтения.
        /// </summary>
        public override PositionState Position
        {
            get { return Stations[0].Position;  }            
            set { }
        }

        public static readonly string TableName = ICSMTbl.itblMicrowa;

        protected MicrowaveStation[] _stations = new MicrowaveStation[2];

        public MicrowaveStation[] Stations
        {
            get { return _stations; }
        }

        public MicrowaveLink()
        {            
            _stations[0] = new MicrowaveStation();
            _stations[1] = new MicrowaveStation();
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            if (r.IsOpen())
                r.Close();
            r.Destroy();
            SaveEquipmentId(Equipment.Id);

            SaveStation(_stations[0]);
            SaveStation(_stations[1]);
            FillComments(ApplId);
        }

        public override void Load()
        {
            _stations[0].Id = GetMicrowsIdByRole("A");
            LoadStationByMicrowsId(_stations[0]);

            _stations[1].Id = GetMicrowsIdByRole("B");
            LoadStationByMicrowsId(_stations[1]);

            LoadLinkData();
            if (Equipment.Id != IM.NullI)
            {
                Equipment.Load();
            }

            _stations[0].Antenna.TableName = ICSMTbl.itblMicrowAntenna;
            if (_stations[0].Antenna.Id != IM.NullI)
            {
                _stations[0].Antenna.Load();
            }

            _stations[1].Antenna.TableName = ICSMTbl.itblMicrowAntenna;
            if (_stations[1].Antenna.Id != IM.NullI)
            {
                _stations[1].Antenna.Load();
            }

            IMPosition a = new IMPosition(_stations[1].Position.LonDms, _stations[1].Position.LatDms, "4DMS");
            IMPosition b = new IMPosition(_stations[0].Position.LonDms, _stations[0].Position.LatDms, "4DMS");
            _range = IMPosition.Distance(a, b);
        }

        protected void LoadStationByMicrowsId(MicrowaveStation station)
        {
            station.Position = new PositionState();

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);

            if (station.Id == IM.NullI)
            {
                throw new Exception("Microwave station ID is nit initialized");
            }

            try
            {
                r.Select("ID,SITE_ID,AZIMUTH,ANT_ID,AGL1,GAIN,POWER,POLAR,TX_FREQ");
                r.SetWhere("ID", IMRecordset.Operation.Eq, station.Id);
                r.Open();

                if (!r.IsEOF())
                {
                    //if (r.GetS("ROLE") == roleValue)
                    station.Position.Id = r.GetI("SITE_ID");
                    station.Azimuth = r.GetD("AZIMUTH");
                    station.Antenna.Id = r.GetI("ANT_ID");
                    station.AGL = r.GetD("AGL1");
                    station.Gain = r.GetD("GAIN");
                    station.Power[PowerUnits.dBm] = r.GetD("POWER");
                    station.Polarization = r.GetS("POLAR");
                    station.Frequency = r.GetD("TX_FREQ");
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            station.Position.LoadStatePosition(station.Position.Id, ICSMTbl.itblPositionMw);
        }

        protected void SaveStation(MicrowaveStation station)
        {
            //station.Position = new PositionState();

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadWrite);

            if (station.Id == IM.NullI)
            {
                throw new Exception("Microwave station ID is not initialized");
            }

            try
            {
                if (station.Position.TableName != ICSMTbl.itblPositionMw)
                {
                    station.Position.Id = IM.NullI;
                    station.Position.TableName = ICSMTbl.itblPositionMw;
                    station.Position.SaveToBD();
                }

                r.Select("ID,SITE_ID,AZIMUTH,ANT_ID,AGL1,GAIN,POWER,POLAR,TX_FREQ");
                r.SetWhere("ID", IMRecordset.Operation.Eq, station.Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();
                    //if (r.GetS("ROLE") == roleValue)
                    r.Put("SITE_ID", station.Position.Id);
                    r.Put("AZIMUTH", station.Azimuth);
                    r.Put("ANT_ID", station.Antenna.Id);
                    r.Put("AGL1", station.AGL);
                    r.Put("GAIN", station.Gain);
                    r.Put("POWER", station.Power[PowerUnits.dBm]);
                    r.Put("POLAR", station.Polarization);
                    r.Put("TX_FREQ", station.Frequency);
                    r.Update();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        private int GetMicrowsIdByRole(string roleValue)
        {
            int resultValue = IM.NullI;

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,MW_ID,ROLE");
                r.SetWhere("MW_ID", IMRecordset.Operation.Eq, Id);
                r.SetWhere("ROLE", IMRecordset.Operation.Eq, roleValue);
                r.Open();
                for (; !r.IsEOF(); r.MoveNext())
                {
                    resultValue = r.GetI("ID");
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            return resultValue;
        }

        private int GetEquipmentId()
        {
            int resultValue = IM.NullI;

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,EQPMW_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                for (; !r.IsEOF(); r.MoveNext())
                {
                    resultValue = r.GetI("EQPMW_ID");
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }

            return resultValue;
        }

        private void LoadLinkData()
        {
            int resultValue = IM.NullI;

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,EQPMW_ID,DESIG_EM,CUST_TXT14,CUST_TXT15,CUST_TXT16");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                for (; !r.IsEOF(); r.MoveNext())
                {
                    Equipment.Id = r.GetI("EQPMW_ID");
                    DesigEmission = r.GetS("DESIG_EM");

                    // Вытаскиваем "Особливі умови"

                    /*Permis
                    SCDozvil = objStation.GetS("CUST_TXT14");*/
                    Finding = r.GetS("CUST_TXT15");                    
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        private void SaveEquipmentId(int newEquipmentId)
        {
            int resultValue = IM.NullI;

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadWrite);

            try
            {
                r.Select("ID,EQPMW_ID,DESIG_EM");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("EQPMW_ID", newEquipmentId);
                    r.Put("DESIG_EM", DesigEmission);
                    r.Update();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        public override void OnNewEquipment()
        {
            if (!string.IsNullOrEmpty(Equipment.DesigEmission))
                DesigEmission = Equipment.DesigEmission;

            Stations[0].Power[PowerUnits.dBW] = Equipment.MaxPower[PowerUnits.dBW];
            Stations[1].Power[PowerUnits.dBW] = Equipment.MaxPower[PowerUnits.dBW];
        }

        public object Clone()
        {
            MicrowaveLink clone = new MicrowaveLink();
            clone._range = _range;
            clone._equipment = _equipment.Clone() as MicrowaveEquipment;

            clone.Id = Id;

            clone._stations[0] = _stations[0].Clone() as MicrowaveStation;
            clone._stations[1] = _stations[1].Clone() as MicrowaveStation;
            clone.DesigEmission = DesigEmission;

            return clone;
        }
    }

    class MicrowaveLinkDiff : MicrowaveLink
    {
        private bool _initialized = false;

        public bool IsInitialized
        {
            get { return _initialized; }
        }

        public static new readonly string TableName = PlugTbl.itblXnrfaDeiffMwSta;

        public MicrowaveLinkDiff()
        {            
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                const string fieldsNameLocal = "ID,MW_STATION_ID,EQUIP_ID1,EQUIP_ID2,ANTEN_ID1,ANTEN_ID2,POWER1,POWER2,AGL1,AGL2,TX_FREQ1,TX_FREQ2,POS_ID1,POS_ID2,POS_TABLE1,POS_TABLE2,POLAR1,POLAR2,DESIG_EM,BW,CERT_NUM,CERT_DATE,AZIMUTH_1,AZIMUTH_2,BW,DIAMETER_1,DIAMETER_2,ANTENNA1_NAME,ANTENNA2_NAME,EQUIPMENT_NAME,GAIN1,GAIN2,MODULATION,COMMENTS,INACTIVE";
                r.Select(fieldsNameLocal);
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("MW_STATION_ID", Id);
                }
                else
                {
                    r.Edit();
                }

                r.Put("INACTIVE", 0);
                r.Put("EQUIP_ID1", Equipment.Id);
                r.Put("EQUIP_ID2", Equipment.Id);

                r.Put("POWER1", Stations[0].Power[PowerUnits.dBm]);
                r.Put("POWER2", Stations[1].Power[PowerUnits.dBm]);

                r.Put("AGL1", Stations[0].AGL);
                r.Put("AGL2", Stations[1].AGL);

                r.Put("TX_FREQ1", Stations[0].Frequency);
                r.Put("TX_FREQ2", Stations[1].Frequency);

                r.Put("ANTEN_ID1", Stations[0].Antenna.Id);
                r.Put("ANTEN_ID2", Stations[1].Antenna.Id);

                r.Put("POS_ID1", Stations[0].Position.Id);
                r.Put("POS_ID2", Stations[1].Position.Id);
                r.Put("POS_TABLE1", Stations[0].Position.TableName);
                r.Put("POS_TABLE2", Stations[1].Position.TableName);

                r.Put("POLAR1", Stations[0].Polarization);
                r.Put("POLAR2", Stations[1].Polarization);

                r.Put("GAIN1", Stations[0].Gain);
                r.Put("GAIN2", Stations[1].Gain);
                r.Put("MODULATION", Equipment.Modulation);

                r.Put("DESIG_EM", Equipment.DesigEmission);
                r.Put("CERT_NUM", Equipment.Certificate.Symbol);
                r.Put("CERT_DATE", Equipment.Certificate.Date);
                r.Put("AZIMUTH_1", Stations[0].Azimuth);
                r.Put("AZIMUTH_2", Stations[1].Azimuth);
                r.Put("DIAMETER_1", Stations[0].Antenna.Diameter);
                r.Put("DIAMETER_2", Stations[1].Antenna.Diameter);
                r.Put("BW", Equipment.Bandwidth);

                r.Put("ANTENNA1_NAME", Stations[0].Antenna.Name);
                r.Put("ANTENNA2_NAME", Stations[1].Antenna.Name);
                r.Put("EQUIPMENT_NAME", Equipment.Name);

                r.Put("COMMENTS", StatComment);

                //CJournal.TestFieldsOfPluginTable(tableNameLocal, Id, r, fieldsNameLocal);

                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
                
            }
        }

        public override void Load()
        {
            const string tableNameLocal = PlugTbl.itblXnrfaDeiffMwSta;
            //
            //MODULATION
            const string fieldsNameLocal = "ID,EQUIP_ID1,EQUIP_ID2,ANTEN_ID1,ANTEN_ID2,POWER1,POWER2,AGL1,AGL2,TX_FREQ1,TX_FREQ2,POS_ID1,POS_ID2,POS_TABLE1,POS_TABLE2,POLAR1,POLAR2,DESIG_EM,BW,CERT_NUM,CERT_DATE,AZIMUTH_1,AZIMUTH_2,BW,MODULATION,ANTENNA1_NAME,ANTENNA2_NAME,EQUIPMENT_NAME,DIAMETER_1,DIAMETER_2,GAIN1,GAIN2,COMMENTS,INACTIVE";

            Stations[0].Position = new PositionState();
            Stations[1].Position = new PositionState();

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadOnly);
                r.Select(fieldsNameLocal);
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        _initialized = true;
                        Equipment.Id = r.GetI("EQUIP_ID1");

                        Stations[0].Power[PowerUnits.dBm] = r.GetD("POWER1");
                        Stations[1].Power[PowerUnits.dBm] = r.GetD("POWER2");

                        Stations[0].AGL = r.GetD("AGL1");
                        Stations[1].AGL = r.GetD("AGL2");

                        Stations[0].Frequency = r.GetD("TX_FREQ1");
                        Stations[1].Frequency = r.GetD("TX_FREQ2");

                        Stations[0].Antenna.Id = r.GetI("ANTEN_ID1");
                        Stations[1].Antenna.Id = r.GetI("ANTEN_ID2");

                        Stations[0].Position.LoadStatePosition(r.GetI("POS_ID1"), r.GetS("POS_TABLE1"));
                        Stations[1].Position.LoadStatePosition(r.GetI("POS_ID2"), r.GetS("POS_TABLE2"));

                        Stations[0].Polarization = r.GetS("POLAR1");
                        Stations[1].Polarization = r.GetS("POLAR2");

                        Stations[0].Gain = r.GetD("GAIN1");
                        Stations[1].Gain = r.GetD("GAIN2");

                        Equipment.DesigEmission = r.GetS("DESIG_EM");
                        Equipment.Certificate.Symbol = r.GetS("CERT_NUM");
                        Equipment.Certificate.Date = r.GetT("CERT_DATE");
                        Equipment.Modulation = r.GetS("MODULATION");

                        Stations[0].Azimuth = r.GetD("AZIMUTH_1");
                        Stations[1].Azimuth = r.GetD("AZIMUTH_2");
                        Stations[0].Antenna.Diameter = r.GetD("DIAMETER_1");
                        Stations[1].Antenna.Diameter = r.GetD("DIAMETER_2");
                        Equipment.Bandwidth = r.GetD("BW");

                        Stations[0].Antenna.Name = r.GetS("ANTENNA1_NAME");
                        Stations[1].Antenna.Name = r.GetS("ANTENNA2_NAME");
                        Equipment.Name = r.GetS("EQUIPMENT_NAME");

                        StatComment = r.GetS("COMMENTS");
                    }
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }

        public new object Clone()
        {
            MicrowaveLinkDiff clone = new MicrowaveLinkDiff();
            clone._range = _range;
            clone._equipment = _equipment.Clone() as MicrowaveEquipment;

            clone.Id = Id;

            clone._stations[0] = _stations[0].Clone() as MicrowaveStation;
            clone._stations[1] = _stations[1].Clone() as MicrowaveStation;

            return clone;
        }

        public void Remove()
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaDeiffMwSta, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();
                }                                
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
