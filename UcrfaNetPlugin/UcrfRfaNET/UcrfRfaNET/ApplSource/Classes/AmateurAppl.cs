﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.StationsStatus;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class AmateurAppl : BaseAppClass
    {
        private AmateurStation amateurStat;
        public const string TableName = "XFA_AMATEUR";
        private int[] arrayFiledsSector;
        private Dictionary<string,string> spsDict =new Dictionary<string ,string >();
        private Dictionary<string, string> upsDict = new Dictionary<string, string>();
        private List<Object> objDocuments = new List<object>();

        public static List<object> ObjSpsList = new List<object>();
        public static List<object> ObjRezList = new List<object>();

        private Row rowSps;
        private Row rowUps;
        private int spsCount = 1;
        private int upsCount = 1;
        private string objDocString = "";
        #region Cells

        /// <summary>
        /// Тип станції
        /// </summary>
        private CellStringComboboxAplyProperty cellTypeStation = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty cellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty cellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty cellAddress = new CellStringProperty();

        /// <summary>
        /// Власник РЕЗ
        /// </summary>
        private CellStringProperty cellOwner = new CellStringProperty();

        /// <summary>
        /// Категорія оператора
        /// </summary>
        private CellStringComboboxAplyProperty cellCatOper = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Позивний сигнал
        /// </summary>
        private CellStringProperty cellCall = new CellStringComboboxAplyProperty();


        /// <summary>
        /// Тип користування АРС
        /// </summary>                   
        private CellStringComboboxAplyProperty cellAPC = new CellStringComboboxAplyProperty();

        /// <summary>
        ///  СПС Позивний сигнал
        /// </summary>
        private CellStringProperty cellSPSCall = new CellStringComboboxAplyProperty();
        private List<CellStringProperty> listCellSpsCall = new List<CellStringProperty>();
        /// <summary>
        ///  СПС Мета використання
        /// </summary>
        private CellStringProperty cellSPSUse = new CellStringComboboxAplyProperty();
        private List<CellStringProperty> listCellSpsUse = new List<CellStringProperty>();

        /// <summary>
        ///  УПС Позивний сигнал
        /// </summary>
        private CellStringProperty cellUPSCall = new CellStringComboboxAplyProperty();
        private List<CellStringProperty> listCellUpsCall = new List<CellStringProperty>();
        /// <summary>
        ///  УПС Мета використання
        /// </summary>
        private CellStringProperty cellUPSUse = new CellStringComboboxAplyProperty();
        private List<CellStringProperty> listCellUpsUse = new List<CellStringProperty>();
        /// <summary>
        /// Тип гарм-го екзам-го сертифікату
        /// </summary>
        private CellStringComboboxAplyProperty cellGarm = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Висота антени над рівнем Землі, м
        /// </summary>
        private CellDoubleProperty CellHighAntenn = new CellDoubleProperty();

        /// <summary>
        /// Назва/тип РЕЗ
        /// </summary>
        private CellStringProperty cellName = new CellStringProperty();
        private List<CellStringProperty> listCellName = new List<CellStringProperty>();
        private List<CellStringProperty> listCellParam = new List<CellStringProperty>();

        /// <summary>
        /// Смуга випромінювання. МГц
        /// </summary>
        private CellStringBrowsableProperty cellDesigEmi = new CellStringBrowsableProperty();
        private List<CellStringBrowsableProperty> listCellDesigEmission = new List<CellStringBrowsableProperty>();
        /// <summary>
        /// Заводський номер
        /// </summary>
        private CellStringProperty cellNumber = new CellStringProperty();
        private List<CellStringProperty> listCellNumber = new List<CellStringProperty>();

        /// <summary>
        /// Потужність передавача, Вт
        /// </summary>
        private CellDoubleBoundedProperty cellPower = new CellDoubleBoundedProperty();
        private List<CellDoubleBoundedProperty> listCellPower = new List<CellDoubleBoundedProperty>();
        /// <summary>
        /// Девіація частоти передавача
        /// </summary>
        private CellStringProperty cellDevi = new CellStringProperty();
        private List<CellStringProperty> listCellDevi = new List<CellStringProperty>();

        /// <summary>
        /// Смуги радіочастот
        /// </summary>
        private CellStringProperty cellBandfreqs = new CellStringProperty();
        private List<CellStringProperty> listCellBandfreqs = new List<CellStringProperty>();

        /// <summary>
        /// Рознесення між каналами
        /// </summary>
        private CellStringComboboxAplyProperty cellChannel = new CellStringComboboxAplyProperty();
        private List<CellStringComboboxAplyProperty> listCellChannel = new List<CellStringComboboxAplyProperty>();

        /// <summary>
        /// Частоти передавання, МГц
        /// </summary>
        private CellStringProperty cellSend = new CellStringProperty();
        private List<CellStringProperty> listCellSend = new List<CellStringProperty>();

        /// <summary>
        /// Частоти передавання, МГц
        /// </summary>
        private CellStringProperty cellAccept = new CellStringProperty();
        private List<CellStringProperty> listCellAccept = new List<CellStringProperty>();

        /// <summary>
        /// Об'єкти(и) РЧП
        /// </summary>
        private  CellStringProperty cellObj=new CellStringProperty();

        private string spsValue;

        #endregion

        public override int OwnerID
        {
            get
            {
                return 0;
                // return amateurStat.OwnerId;
            }
        }

       private Dictionary<string, string> dictAmateurType = EriFiles.GetEriCodeAndDescr("AmateurType");
       private Dictionary<string, string> dictAmateurCateg = EriFiles.GetEriCodeAndDescr("AmateurCateg");
       private Dictionary<string, string> dictAmateurApc = EriFiles.GetEriCodeAndDescr("AmateurAPC");
       private Dictionary<string, string> dictAmateurCert = EriFiles.GetEriCodeAndDescr("AmateurCert");
       private Dictionary<string, string> dictAmateurPass = EriFiles.GetEriCodeAndDescr("AmateurPassing");
        

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public AmateurAppl(int id, int ownerId, int packetID, string radioTech)
            : base(id, TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppA3;
            amateurStat = new AmateurStation();
            amateurStat.Id = recID;
            arrayFiledsSector = new[] { 13, 14, 15, 16, 17, 18, 19, 20, 21, 0 };
            AmateurStation.Count = 0;
            amateurStat.Load();
            department = DepartmentType.VMA;
            departmentSector = DepartmentSectorType.VMA_NULL;
            
            Status.StatusAsString = amateurStat.Status;
            if ((ownerId != 0) && (ownerId != IM.NullI))
                amateurStat.AbonentResId = ownerId;
            SCDozvil = amateurStat.SpecConditionPerm;
            SCVisnovok = amateurStat.SpecConditionConcl;
            SCNote = amateurStat.SpecNote;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public AmateurAppl(int id, int ownerId, int packetID, string radioTech, StatusType statusx)
            : base(id, TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppA3;
            amateurStat = new AmateurStation();
            amateurStat.Id = recID;
            arrayFiledsSector = new[] { 13, 14, 15, 16, 17, 18, 19, 20, 21, 0 };
            AmateurStation.Count = 0;
            amateurStat.Load();
            department = DepartmentType.VMA;
            departmentSector = DepartmentSectorType.VMA_NULL;

            Status.StatusAsString = statusx.StatusAsString;
            if ((ownerId != 0) && (ownerId != IM.NullI))
                amateurStat.AbonentResId = ownerId;
            SCDozvil = amateurStat.SpecConditionPerm;
            SCVisnovok = amateurStat.SpecConditionConcl;
            SCNote = amateurStat.SpecNote;
        }

        public override void InitParamGrid(Grid grid)
        {
            rowSps = grid.rowList[8] as Row;
            rowUps = grid.rowList[9] as Row;

            FillLists();

            amateurStat.AmCallsSps.RemoveAll(obj => obj.DateOut <= DateTime.Now);
            amateurStat.AmCallsUps.RemoveAll(obj => obj.DateOut <= DateTime.Now);

            if (amateurStat.AmCallsSps != null)
                if (amateurStat.AmCallsSps.Count > 1)
                {
                    for (int i = 1; i < amateurStat.AmCallsSps.Count; i++)
                        NewSps(grid);
                    for (int j = 0; j < spsCount; j++)
                    {
                        Row row1 = ((Row)(grid.rowList[8 + j]));
                        if (row1.cellList.Count > 1)
                            row1.cellList[1].Value = amateurStat.AmCallsSps[j].Call;
                        if (row1.cellList.Count > 2)
                            row1.cellList[2].Value = amateurStat.AmCallsSps[j].GoalUse;
                    }
                }
                else if (amateurStat.AmCallsSps.Count == 1)
                {
                    Row row1 = ((Row)(grid.rowList[8]));
                    if (row1.cellList.Count > 1)
                        row1.cellList[1].Value = amateurStat.AmCallsSps[0].Call;
                    if (row1.cellList.Count > 2)
                        row1.cellList[2].Value = amateurStat.AmCallsSps[0].GoalUse;
                }

            if (amateurStat.AmCallsUps != null)
                if (amateurStat.AmCallsUps.Count > 1)
                {
                    for (int i = 1; i < amateurStat.AmCallsUps.Count; i++)
                        NewUps(grid);
                    for (int j = 0; j < upsCount; j++)
                    {
                        Row row1 = ((Row)(grid.rowList[8 + spsCount + j]));
                        if (row1.cellList.Count > 1)
                            row1.cellList[1].Value = amateurStat.AmCallsUps[j].Call;
                        if (row1.cellList.Count > 2)
                            row1.cellList[2].Value = amateurStat.AmCallsUps[j].GoalUse;
                    }
                }
                else if (amateurStat.AmCallsUps.Count == 1)
                {
                    Row row1 = ((Row)(grid.rowList[8 + spsCount]));
                    if (row1.cellList.Count > 1)
                        row1.cellList[1].Value = amateurStat.AmCallsUps[0].Call;
                    if (row1.cellList.Count > 2)
                        row1.cellList[2].Value = amateurStat.AmCallsUps[0].GoalUse;
                }

            for (int i = 0; i < arrayFiledsSector.Length - 1; i++)
                arrayFiledsSector[i] += spsCount + upsCount - 2;

           
                if (AmateurStation.Count > 1)
                    for (int i = 1; i < AmateurStation.Count; i++)
                        ReBuildGrid(grid, i);

            ReadData();

            //Сектори
            for (int i = 0; i < AmateurStation.Count; i++)
            {
                listCellName[i].Value = amateurStat.AmEquip[i].Name;
                listCellDesigEmission[i].Value = amateurStat.AmEquip[i].DesigEmission;
                listCellNumber[i].Value = amateurStat.AmEquip[i].PlantNumb;
                listCellPower[i].Value = amateurStat.AmEquip[i].Power[PowerUnits.W].ToStringNullD();
                listCellDevi[i].Value = amateurStat.AmEquip[i].Devi;
                
                if (!dictAmateurPass.ContainsKey(amateurStat.AmEquip[i].Passing))
                    amateurStat.AmEquip[i].Passing = "";
                if (dictAmateurPass.ContainsKey(amateurStat.AmEquip[i].Passing))
                    listCellChannel[i].Value = dictAmateurPass[amateurStat.AmEquip[i].Passing];

                listCellBandfreqs[i].Value = AmateurBand.GetFreqs(amateurStat.AmEquip[i].AmBand);

                string tempAccept = "";
                string tempSend = "";
                for (int j = 0; j < amateurStat.AmEquip[i].AmFreq.Count; j++)
                {
                    if (amateurStat.AmEquip[i].AmFreq[j].FreqRx != IM.NullD)
                        tempAccept += amateurStat.AmEquip[i].AmFreq[j].FreqRx.ToStringNullD() + ";";
                    if (amateurStat.AmEquip[i].AmFreq[j].FreqTx != IM.NullD)
                        tempSend += amateurStat.AmEquip[i].AmFreq[j].FreqTx.ToStringNullD() + ";";
                }
                if (tempAccept.Length > 0)
                    tempAccept = tempAccept.Remove(tempAccept.Count() - 1);
                if (tempSend.Length > 0)
                    tempSend = tempSend.Remove(tempSend.Count() - 1);

                listCellAccept[i].Value = tempAccept;
                listCellSend[i].Value = tempSend;

            }

            gridParam = grid;
            CheckStationType();
            FillTabOrderSector(grid);
        }

        private void FillLists()
        {
            listCellName.Clear();
            listCellDesigEmission.Clear();
            listCellNumber.Clear();
            listCellPower.Clear();
            listCellDevi.Clear();
            listCellBandfreqs.Clear();
            listCellChannel.Clear();
            listCellSend.Clear();
            listCellAccept.Clear();
            listCellSpsUse.Clear();
            listCellSpsCall.Clear();
            listCellUpsUse.Clear();
            listCellUpsCall.Clear();

            listCellName.Add(cellName);
            listCellDesigEmission.Add(cellDesigEmi);
            listCellNumber.Add(cellNumber);
            listCellPower.Add(cellPower);
            listCellDevi.Add(cellDevi);
            listCellBandfreqs.Add(cellBandfreqs);
            cellChannel.Items.AddRange(dictAmateurPass.Values.ToArray());
            listCellChannel.Add(cellChannel);
            listCellSend.Add(cellSend);
            listCellAccept.Add(cellAccept);
            listCellSpsUse.Add(cellSPSUse);
            listCellSpsCall.Add(cellSPSCall);
            listCellUpsUse.Add(cellUPSUse);
            listCellUpsCall.Add(cellUPSCall);
        }


        /// <summary>
        /// Зчитування даних
        /// </summary>
        private void ReadData()
        {
           
            List<string> typeStatList = EriFiles.GetEriDescrList("AmateurType");
            List<string> typeStatListCode = EriFiles.GetEriCodeList("AmateurType");
            if (typeStatListCode.Count > 0)
                if (!typeStatListCode.Contains(amateurStat.TypeStat))
                    amateurStat.TypeStat = typeStatListCode[0];
            cellTypeStation.Items.AddRange(typeStatList);

            if (!string.IsNullOrEmpty(amateurStat.TypeStat))
                if (dictAmateurType.ContainsKey(amateurStat.TypeStat))
                    cellTypeStation.Value = dictAmateurType[amateurStat.TypeStat];

            cellOwner.Value = amateurStat.NameOwner;
            
            cellCatOper.Items.AddRange(dictAmateurCateg.Values.ToArray());
            cellCatOper.Value = "";
            if (!string.IsNullOrEmpty(amateurStat.CategOper))
                if (dictAmateurCateg.ContainsKey(amateurStat.CategOper))
                    cellCatOper.Value = dictAmateurCateg[amateurStat.CategOper];

            if (!string.IsNullOrEmpty(amateurStat.Call))
                cellCall.Value = amateurStat.Call;

            if (!string.IsNullOrEmpty(amateurStat.Apc))
                if (!dictAmateurApc.ContainsKey(amateurStat.Apc))
                    if (dictAmateurApc.Count > 0)
                        amateurStat.Apc = dictAmateurApc.Keys.ToList()[0];
            cellAPC.Items.AddRange(dictAmateurApc.Values.ToList());
            cellAPC.Value = "";
            if (!string.IsNullOrEmpty(amateurStat.Apc))
                if (dictAmateurApc.ContainsKey(amateurStat.Apc))
                    cellAPC.Value = dictAmateurApc[amateurStat.Apc];


            CellHighAntenn.DoubleValue = amateurStat.AGL;

            if (amateurStat.objPosition != null)
            {
                cellLongitude.DoubleValue = amateurStat.objPosition.LongDms;
                cellLatitude.DoubleValue = amateurStat.objPosition.LatDms;
                cellAddress.Value = amateurStat.objPosition.FullAddressAuto;
            }
            
            cellGarm.Items.AddRange(dictAmateurCert.Values.ToArray());
            if (!string.IsNullOrEmpty(amateurStat.Cert))
                if (dictAmateurCert.ContainsKey(amateurStat.Cert))
                    cellGarm.Value = dictAmateurCert[amateurStat.Cert];
            cellObj.Value = amateurStat.Id.ToString();
        }

        /// <summary>
        /// Обновление данных поля РДПО владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerRDPO(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,REGIST_NUM");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, amateurStat.AbonentResId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("REGIST_NUM");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }

        //===========================================================
        /// <summary>
        /// Обновление данных поля Названия владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerName(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,NAME");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, amateurStat.AbonentResId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("NAME");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="newOwnerId">ID нового владельца</param>
        public override void ChangeOwner(int newOwnerId)
        {
            if (amateurStat.AbonentResId != newOwnerId)
            {
                amateurStat.AbonentResId = newOwnerId;
                amateurStat.Save();
            }
        }

        /// <summary>
        /// Реакция на нажатия кнопки оформления позиции
        /// Формирование адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            CellValidate(cell, gridParam);
            PositionState2 newPos = FormSelectExistPosition.SelectPositionState(OwnerWindows, PlugTbl.XfaPosition, 1, cellLongitude.DoubleValue, cellLatitude.DoubleValue);
            if (newPos != null)
            {
                amateurStat.objPosition = newPos;
                amateurStat.objPosition.GetAdminSiteInfo(amateurStat.objPosition.AdminSiteId);
                cellAddress.Value = amateurStat.objPosition.FullAddressAuto;
                cellLongitude.DoubleValue = amateurStat.objPosition.LongDms;
                cellLatitude.DoubleValue = amateurStat.objPosition.LatDms;
                //amateurStat.NeedPositionSave = amateurStat.objPosition.IsChanged;
            }
        }

        /// <summary>
        /// Реакция на нажатие кнопки создания адреса
        /// Создание нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            if (ShowMessageReference(amateurStat.objPosition != null))
            {
                PositionState2 newPos = new PositionState2();
                newPos.LongDms = cellLongitude.DoubleValue;
                newPos.LatDms = cellLatitude.DoubleValue;

                DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(AbonentStation.TablePosition, ref newPos, OwnerWindows);
                if (dr == DialogResult.OK)
                {
                    amateurStat.objPosition = newPos;
                    amateurStat.objPosition.GetAdminSiteInfo(amateurStat.objPosition.AdminSiteId);
                    cellAddress.Value = amateurStat.objPosition.FullAddressAuto;
                    cellLongitude.DoubleValue = amateurStat.objPosition.LongDms;
                    cellLatitude.DoubleValue = amateurStat.objPosition.LatDms;
                }
            }
        }

        public void OnBeforeAddress(Cell cell, ref string newV)
        {

        }

        /// <summary>
        /// Callback when use select Add Sector
        /// </summary>
        public override void AddSector(Grid grid)
        {
            ReBuildGrid(grid, -1);
            AmateurStation.Count++;

            FillTabOrderSector(grid);

        }

        private void FillTabOrderSector(Grid grid)
        {
            int[] arrTab = new[] { 13, 15, 16, 17, 18, 19, 20, 21 };
            for (int k = 0; k < arrTab.Length; k++)
                arrTab[k] += spsCount + upsCount - 2;
            for (int i = 0; i < arrTab.Length; i++)
            {
                if (grid.rowList.Count > arrTab.Length)
                {
                    Row rowTemp = ((Row)grid.rowList[arrTab[i]]);
                    if (rowTemp != null)
                    {
                        for (int j = 1; j < rowTemp.cellList.Count; j++)
                        {
                            Cell cellTemp = rowTemp.cellList[j];
                            if (cellTemp != null)
                                cellTemp.TabOrder = 2 + i + j * arrTab.Length;
                        }
                    }
                }
            }
            grid.Invalidate();
            grid.Focus();
        }

        /// <summary>
        /// Печать документа из заявки
        /// </summary>
        public override void PrintDoc()
        {
            //amateurStat.AmCallsSps.Clear();
            //amateurStat.AmCallsUps.Clear();

            //don't rewrite, just add recently added (and not saved yet)

            for (int i = 0; i < listCellSpsCall.Count; i++)
            {
                string call = listCellSpsCall[i].Value;
                if (string.IsNullOrEmpty(call))
                    continue;
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsSps where (item.Call == call) select item;
                if (calls == null || calls.Count() == 0)
                {
                    AmateurCalls acS = new AmateurCalls();
                    acS.Call = call;
                    acS.StaId = amateurStat.Id;
                    acS.GoalUse = listCellSpsUse[i].Value;
                    amateurStat.AmCallsSps.Add(acS);
                }
            }
            for (int i = 0; i < listCellUpsCall.Count; i++)
            {
                string call = listCellUpsCall[i].Value;
                if (string.IsNullOrEmpty(call))
                    continue;
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsUps where (item.Call == call) select item;
                if (calls == null || calls.Count() == 0)
                {
                    AmateurCalls ucS = new AmateurCalls();
                    ucS.StaId = amateurStat.Id;
                    ucS.Call = call;
                    ucS.GoalUse = listCellUpsUse[i].Value;
                    amateurStat.AmCallsUps.Add(ucS);
                }
        }
            //Формування списка для Об'єктів документу
            objDocuments.Clear();
            if (amateurStat.AmCallsSps != null)
                for (int i = 0; i < amateurStat.AmCallsSps.Count; i++)
                {
                    objDocuments.Add(amateurStat.AmCallsSps[i].Call);
                    if (!spsDict.ContainsKey(amateurStat.AmCallsSps[i].Call))
                        spsDict.Add(amateurStat.AmCallsSps[i].Call, "СПС");
                }
            if (amateurStat.AmCallsUps != null)
                for (int i = 0; i < amateurStat.AmCallsUps.Count; i++)
                {
                    objDocuments.Add(amateurStat.AmCallsUps[i].Call);
                    if (!upsDict.ContainsKey(amateurStat.AmCallsUps[i].Call))
                        upsDict.Add(amateurStat.AmCallsUps[i].Call, "УПС");
                }

            ObjSpsList.Clear();
            for (int i = 0; i < objDocuments.Count; i++)
                ObjSpsList.Add(objDocuments[i].ToString());

            objDocuments.Add("РЕЗ 1");
            if (listCellParam != null)
                if (listCellParam.Count > 0)
                    for (int i = 0; i < listCellParam.Count; i++)
                        objDocuments.Add(listCellParam[i].Value);

            ObjRezList.Clear();
            for (int i = ObjSpsList.Count; i < objDocuments.Count; i++)
                ObjRezList.Add(objDocuments[i].ToString());

            // Проверка ни зменения статуса заявки.
            if (gridParam.Edited || IsChangeDop)
                OnSaveAppl();

            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr(recordID.Table, recordID.Id), (IsTechnicalUser == 1), applID, appType, false, cellGarm.Value, cellTypeStation.Value);
            //frmReport.ShowBlankNumBox = false;

            if (objDocuments != null)
                if (objDocuments.Count > 0)
                    frmReport.SetObj(objDocuments);

            if (frmReport.ShowDialog() == DialogResult.OK)
            {
                //Сохранение в БД
                objDocString = frmReport.getObjDoc();
                DateTime startDate = frmReport.getStartDate();
                DateTime endDate = frmReport.getEndDate();
                string docType = frmReport.getDocType();
                if (docType == DocType.DOZV_CANCEL)
                    endDate = IM.NullT;
                //Определение СПС или УПС
                if (docType==DocType.DOZV_SPS)
                {
                    spsValue = "СПС";
                    if (upsDict.ContainsKey(frmReport.cmbBxDocument.Text.Trim()))
                        spsValue = "УПС";
                }

                if (docType == DocType.DOZV_MOVE)
                    spsValue = frmReport.cmbBxDocument.Text.Substring(3);

                try
                {
                    string docName = getOutDocName(docType, "");
                    string dopFilter = getDopDilter();
                    if (docType == DocType.DOZV_SPS)
                    {
                        if (amateurStat.AmCallsSps != null)
                            foreach (AmateurCalls acSps in amateurStat.AmCallsSps)
                                if (acSps.Call == objDocString)
                                {
                                    acSps.StaId = amateurStat.Id;
                                    acSps.DateIn = startDate;
                                    acSps.DateOut = endDate;
                                    acSps.TypeCall = "СПС";
                                    acSps.NumberDocument = docName.ToFileNameWithoutExtension();
                                    acSps.PathFile = docName;
                                    acSps.NumberBlank = frmReport.GetNumber();
                                    acSps.Save();
                                }

                        if (amateurStat.AmCallsUps != null)
                            foreach (AmateurCalls ucSps in amateurStat.AmCallsUps)
                                if (ucSps.Call == objDocString)
                                {
                                    ucSps.StaId = amateurStat.Id;
                                    ucSps.DateIn = startDate;
                                    ucSps.DateOut = endDate;
                                    ucSps.TypeCall = "УПС";
                                    ucSps.NumberDocument = docName.ToFileNameWithoutExtension();
                                    ucSps.PathFile = docName;
                                    ucSps.NumberBlank = frmReport.GetNumber();
                                    ucSps.Save();
                                }
                    }
                    //Дозвіл на рухому станцію
                    if (docType == DocType.DOZV_MOVE)
                    {
                        int numberEquip = objDocString.Remove(0, 3).ToInt32(IM.NullI);
                        amateurStat.SaveMoveStat(startDate, endDate, docName.ToFileNameWithoutExtension(), docName, numberEquip);
                    }
                    if (((docType == DocType.GARM_CERT) || ((docType == DocType.NOVIC_CERT))) && (!frmReport.IsEnableDateEnd))
                    {
                        endDate = IM.NullT;
                    }

                    SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
                    PrintDocs printDocs = new PrintDocs();
                    //Визначення id таблиці друку і її назва
                    int idPrint = applID;
                    string tblName = PlugTbl.itblXnrfaAppl;
                    if (docType == DocType.DOZV_MOVE)
                    {
                        int numberEquip = objDocString.Remove(0, 3).ToInt32(IM.NullI);
                        tblName = PlugTbl.XfaEsAmateur;
                        idPrint = amateurStat.GetXfaEsId(recordID.Id, numberEquip);
                        CEventLog.AddApplEvent(applID, EDocEvent.evMOVE, DateTime.Now, docName.ToFileNameWithoutExtension(), endDate, docName);
                    }
                    else if (docType == DocType.DOZV_SPS)
                    {
                        tblName = PlugTbl.XfaCallAmateur;
                        idPrint = amateurStat.GetXfaCallId(recordID.Id, spsValue, objDocString);
                        CEventLog.AddApplEvent(applID, EDocEvent.evSPS, DateTime.Now, docName.ToFileNameWithoutExtension(), endDate, docName);                        
                    }
                    else if ((docType == DocType.GARM_CERT) || (docType == DocType.NOVIC_CERT))
                    {
                        amateurStat.SaveCertificate(startDate, endDate, docName);
                    }
                    //створення друку
                    if (printDocs.CreateOneReport(docType, appType, startDate, endDate, tblName, idPrint, dopFilter, docName, true))
                        UpdateEventGrid();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //============================================================
       /* /// <summary>
        /// Печать документа на бланке
        /// </summary>
        public void SetNumBlank(string blankNumber)
        {
            IMRecordset r = null;
            r = IMRecordset.ForRead(new RecordPtr(PlugTbl.APPL, applID), "OBJ_ID1");
            int staId = r.GetI("OBJ_ID1");
            r.Destroy();
            r = IMRecordset.ForWrite(new RecordPtr(PlugTbl.XfaAmateur, staId), "NUMB_BLANK");
            r.Edit();
            r.Put("NUMB_BLANK", blankNumber.ToString());
            r.Update();
            r.Destroy();
            r = null;
        }
        */ 
        //============================================================
        /// <summary>
        /// Печать документа на бланке
        /// </summary>
        public override void PrintDocOnBlank()
        {
            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // Проверка ни зменения статуса заявки.
            if (gridParam.Edited || IsChangeDop)
                OnSaveAppl();

            //Формування списка для Об'єктів документу
            objDocuments.Clear();
            if (amateurStat.AmCallsSps != null)
                for (int i = 0; i < amateurStat.AmCallsSps.Count; i++)
                {
                    if (amateurStat.AmCallsSps[i].DateOut.Date > DateTime.Now.Date)
                        objDocuments.Add(amateurStat.AmCallsSps[i].Call);
                }
            if (amateurStat.AmCallsUps != null)
                for (int i = 0; i < amateurStat.AmCallsUps.Count; i++)
                {
                    if (amateurStat.AmCallsUps[i].DateOut.Date > DateTime.Now.Date)
                        objDocuments.Add(amateurStat.AmCallsUps[i].Call);
                }

            ObjSpsList.Clear();
            for (int i = 0; i < objDocuments.Count; i++)
                ObjSpsList.Add(objDocuments[i].ToString());

            objDocuments.Add("РЕЗ 1");
            if (listCellParam != null)
                if (listCellParam.Count > 0)
                    for (int i = 0; i < listCellParam.Count; i++)
                        objDocuments.Add(listCellParam[i].Value);

            ObjRezList.Clear();
            for (int i = ObjSpsList.Count; i < objDocuments.Count; i++)
                ObjRezList.Add(objDocuments[i].ToString());

            FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr(recordID.Table, recordID.Id), (IsTechnicalUser == 1), applID, appType, false, cellGarm.Value, cellTypeStation.Value);
            frmReport.Text = "Друк документа на бланку";

            if (objDocuments != null)
                if (objDocuments.Count > 0)
                    frmReport.SetObj(objDocuments);

            if (frmReport.ShowDialog() == DialogResult.OK)
            {
                //Сохранение в БД
                objDocString = frmReport.getObjDoc();
                DateTime startDate = frmReport.getStartDate();
                DateTime endDate = frmReport.getEndDate();
                string docType = frmReport.getDocType();
                string docNumber = frmReport.cmbBxDocument.Text.Trim();
                string blankNumber = frmReport.GetNumber();
                //Определение СПС или УПС
                string pathFile = "";
                endDate = IM.NullT;
                string eventDocNumber = "";
                EDocEvent evt = EDocEvent.evUnknown;
                switch (docType)
                {
                    default:
                        MessageBox.Show("Документ такого типу не може бути роздрукований тут", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case DocType.DOZV_SPS:
                        {
                            if (amateurStat.AmCallsSps != null)
                                foreach (AmateurCalls acSps in amateurStat.AmCallsSps)
                                    if (acSps.Call == docNumber)
                                    {
                                        acSps.NumberBlank = blankNumber;
                                        endDate = acSps.DateOut;
                                        pathFile = acSps.PathFile;
                                        eventDocNumber = acSps.NumberDocument;
                                        acSps.Save();
                                        evt = EDocEvent.evDRVPrintDozv;
                                        break;
                                        
                                    }

                            if (amateurStat.AmCallsUps != null)
                                foreach (AmateurCalls ucSps in amateurStat.AmCallsUps)
                                    if (ucSps.Call == docNumber)
                                    {
                                        ucSps.NumberBlank = blankNumber;
                                        endDate = ucSps.DateOut;
                                        pathFile = ucSps.PathFile;
                                        eventDocNumber = ucSps.NumberDocument;
                                        ucSps.Save();
                                        evt = EDocEvent.evDRVPrintDozv;
                                         break;
                                    }
                          //  SetNumBlank(blankNumber.ToString());
                        }
                        break;
                    case DocType.DOZV_MOVE:
                        {
                            int numberEquip = docNumber.Remove(0, 3).ToInt32(IM.NullI);
                            int idEqAmateur = amateurStat.GetXfaEsId(recordID.Id, numberEquip);
                            {
                                IMRecordset r = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadWrite);
                                r.Select("ID,DATE_EXPIR_DOZV,NUMB_DOZV,PATH,NUM_BLANK");
                                r.SetWhere("ID", IMRecordset.Operation.Eq, idEqAmateur);
                                try
                                {
                                    r.Open();
                                    if (!r.IsEOF())
                                    {
                                        r.Edit();
                                        endDate = r.GetT("DATE_EXPIR_DOZV");
                                        pathFile = r.GetS("PATH");
                                        eventDocNumber = r.GetS("NUMB_DOZV");
                                        r.Put("NUM_BLANK", blankNumber);
                                        r.Update();
                                        evt = EDocEvent.evDRVPrintDozv;
                                    }
                                }
                                finally
                                {
                                    r.Final();
                                }
                            }
                        }
                        break;
                        /* #7264 - основные документы печатаются из пакета
                    
                         case DocType.DOZV_APC:
                        {
                            string newDozvNum;
                            DateTime dateFrom, dateTo;
                            ApplDocuments.GetNewPermData(applID, out newDozvNum, out dateFrom, out dateTo, out pathFile);
                            ApplDocuments.PrintPermision(applID, newDozvNum, startDate, "");
                            CEventLog.AddApplEvent(applID, EDocEvent.evDRVPrintDozv, DateTime.Now, newDozvNum, endDate, pathFile);
                            UpdateEventGrid();
                        }
                        break;
                         */
                    case DocType.GARM_CERT:
                    case DocType.NOVIC_CERT:
                        {
                            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(TableName, IMRecordset.Mode.ReadOnly))
                            {
                                rs.Select("ID");
                                rs.Select("CERTIF_NUMB");
                                rs.Select("CERTIF_PATH");
                                rs.SetWhere("ID", IMRecordset.Operation.Eq, recordID.Id);
                                rs.Open();
                                if (!rs.IsEOF())
                                {
                                    eventDocNumber = rs.GetS("CERTIF_NUMB");
                                    pathFile = rs.GetS("CERTIF_PATH");
                                }
                            }
                            amateurStat.SavePrintCertificate(startDate);

                            evt = EDocEvent.evPrintCertificate;
                            UpdateEventGrid();
                        }
                        break;
                }

                /*
                if (docType == DocType.DOZV) 
                {
                    SetNumBlank(blankNumber.ToString());
                }
                 */ 
                

                if (evt != EDocEvent.evUnknown) 
                {
                    ApplDocuments.SendToPrinter(pathFile, false);
                    // CEventLog.AddApplEvent(applID, evt, DateTime.Now, eventDocNumber, endDate, pathFile);
                    //CEventLog.AddApplEventWithoutChangeStatus(applID, evt, DateTime.Now, eventDocNumber, endDate, pathFile);

                   /* IMRecordset r = null;
                    r = IMRecordset.ForRead(new RecordPtr(PlugTbl.APPL, applID), "OBJ_ID1");
                    int staId = r.GetI("OBJ_ID1");
                    r.Destroy();
                    r = IMRecordset.ForWrite(new RecordPtr(PlugTbl.XfaAmateur, staId), "NUMB_BLANK");
                    r.Edit();
                    r.Put("NUMB_BLANK", blankNumber.ToString());
                    r.Update();
                    r.Destroy();
                    r = null;
                    */ 


                    ApplDocuments.PrintPermision(applID, eventDocNumber, DateTime.Now, blankNumber.ToString());
                    CEventLog.AddApplEventWithoutChangeStatus(applID, evt, DateTime.Now, eventDocNumber, IM.NullT, pathFile);
                    UpdateEventGrid();
                }
                else
                    MessageBox.Show("Документ не знайдено", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Callback when use select Add Sector SPS
        /// </summary>
        public override void AddSectorSps(Grid grid)
        {
            NewSps(grid);
            for (int i = 0; i < arrayFiledsSector.Length - 1; i++)
                arrayFiledsSector[i]++;
        }

        private void NewSps(Grid grid)
        {
            Cell cell;
            Row row = new Row(grid, grid.rowList[0].Size);
            row.grid = grid;
            grid.rowList.Insert(8 + spsCount, row);
            cell = row.AddCell("СПС(позивний / мета використання)", EditStyle.esSimple);
            cell.HorizontalAlignment = HorizontalAlignment.Left;
            cell.TextHeight = rowSps.cellList[0].TextHeight;
            cell.CanEdit = false;
            Cell cell1 = row.AddCell("", EditStyle.esSimple);
            cell1.HorizontalAlignment = HorizontalAlignment.Center;
            cell1.TextHeight = rowSps.cellList[0].TextHeight;
            cell1.CanEdit = true;

            Cell cell2 = row.AddCell("", EditStyle.esSimple);
            cell2.HorizontalAlignment = HorizontalAlignment.Center;
            cell2.TextHeight = rowSps.cellList[0].TextHeight;
            cell2.CanEdit = true;
            spsCount++;

            CellStringProperty newCellCall = new CellStringProperty();
            cell1.Bind("Value", newCellCall, "Value");
            newCellCall.Cell = cell1;
            listCellSpsCall.Add(newCellCall);

            CellStringProperty newCellGoal = new CellStringProperty();
            cell2.Bind("Value", newCellGoal, "Value");
            newCellGoal.Cell = cell2;
            listCellSpsUse.Add(newCellGoal);

            grid.Focus();
            grid.Invalidate();
        }

        /// <summary>
        /// Callback when use select Add Sector SPS
        /// </summary>
        public override void AddSectorUps(Grid grid)
        {
            NewUps(grid);
            for (int i = 0; i < arrayFiledsSector.Length - 1; i++)
                arrayFiledsSector[i]++;
        }

        /// <summary>
        /// Створення нового рядка УПС для грід
        /// </summary>
        /// <param name="grid"></param>
        private void NewUps(Grid grid)
        {
            Cell cell;
            Row row = new Row(grid, grid.rowList[0].Size);
            row.grid = grid;
            grid.rowList.Insert(8 + spsCount + upsCount, row);
            cell = row.AddCell("УПС(позивний / мета використання)", EditStyle.esSimple);
            cell.HorizontalAlignment = HorizontalAlignment.Left;
            cell.TextHeight = rowUps.cellList[0].TextHeight;
            cell.CanEdit = false;
            Cell cell1 = row.AddCell("", EditStyle.esSimple);
            cell1.HorizontalAlignment = HorizontalAlignment.Center;
            cell1.TextHeight = rowUps.cellList[0].TextHeight;
            cell1.CanEdit = true;
            Cell cell2 = row.AddCell("", EditStyle.esSimple);
            cell2.HorizontalAlignment = HorizontalAlignment.Center;
            cell2.TextHeight = rowUps.cellList[0].TextHeight;
            cell2.CanEdit = true;
            upsCount++;

            CellStringProperty newCellCall = new CellStringProperty();
            cell1.Bind("Value", newCellCall, "Value");
            newCellCall.Cell = cell1;
            listCellUpsCall.Add(newCellCall);

            CellStringProperty newCellGoal = new CellStringProperty();
            cell2.Bind("Value", newCellGoal, "Value");
            newCellGoal.Cell = cell2;
            listCellUpsUse.Add(newCellGoal);

            grid.Focus();
            grid.Invalidate();
        }

        /// <summary>
        /// Видалення сектору
        /// </summary>
        /// <param name="grid"></param>
        public override void RemoveSector(Grid grid)
        {
            List<string> sAr = new List<string>();
            Row rowParam = grid.GetRow(arrayFiledsSector.Last()) as Row;
            if (rowParam != null)
            {
                foreach (Cell cell in rowParam.cellList)
                    if (cell.Value.StartsWith("РЕЗ"))
                        sAr.Add(cell.Value);

                if (rowParam.cellList.Count > 2)
                {
                    RemoveSectorForm rSm = new RemoveSectorForm(sAr.ToArray());
                    rSm.ShowDialog();
                    if (rSm.DialogResult == DialogResult.OK)
                    {
                        if (AmateurStation.Count > 1)
                        {
                            listCellName.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellDesigEmission.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellNumber.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellPower.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellDevi.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellBandfreqs.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellChannel.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellSend.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            listCellAccept.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                            amateurStat.AmEquip.RemoveAt(rSm.cmbBoxRemove.SelectedIndex);
                        }

                        for (int i = 0; i < arrayFiledsSector.Length; i++)
                        {
                            Row row = grid.GetRow(arrayFiledsSector[i]) as Row;
                            if (row != null)
                                if (row.cellList.Count > 2)
                                    row.cellList.RemoveAt(rSm.cmbBoxRemove.SelectedIndex + 1);
                        }

                        grid.Edited = true;
                        grid.Invalidate();
                        grid.Focus();
                        AmateurStation.Count--;
                    }
                }
                foreach (Cell cells in rowParam.cellList)
                {
                    if (cells.Value.StartsWith("РЕЗ"))
                    {
                        cells.Value = "РЕЗ " + cells.Col;
                        cells.Selected = true;
                        grid.Update();
                    }
                }
            }
            FillTabOrderSector(grid);
        }

        private void ReBuildGrid(Grid grid, int cnt)
        {
            for (int i = 0; i < arrayFiledsSector.Length; i++)
            {
                Row row = grid.GetRow(arrayFiledsSector[i]) as Row;
                if (row != null) row.grid = grid;
                grid.rowList.RemoveAt(arrayFiledsSector[i]);
                grid.rowList.Insert(arrayFiledsSector[i], row);
                if (row != null)
                {
                    Cell originalCell = row.GetCell(1);

                    Cell cell = row.AddCell(originalCell.Value);

                    cell.HorizontalAlignment = HorizontalAlignment.Center;
                    cell.Value2 = "";
                    cell.CanEdit = false;
                    cell.cellStyle = originalCell.cellStyle;
                    cell.FontName = originalCell.FontName;
                    cell.FontStyle = originalCell.FontStyle;
                    cell.TextHeight = originalCell.TextHeight;
                    cell.Value = "";
                    cell.CanEdit = originalCell.CanEdit;
                }

                switch (i)
                {
                    case 0:
                        CellStringProperty newCellEquipmentName = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newCellEquipmentName, "Value");
                            newCellEquipmentName.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellName.Add(newCellEquipmentName);
                        if (row != null)
                            row.GetCell(row.cellList.Count - 1).AddOnPressButtonHandler(this, "OnPressEquipment");
                        break;
                    case 1:
                        CellStringBrowsableProperty newCellDesigEmmision = new CellStringBrowsableProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newCellDesigEmmision, "Value");
                            newCellDesigEmmision.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellDesigEmission.Add(newCellDesigEmmision);
                        break;
                    case 2:
                        CellStringProperty newCellNumber = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newCellNumber, "Value");
                            newCellNumber.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellNumber.Add(newCellNumber);
                        break;
                    case 3:
                        CellDoubleBoundedProperty newPower = new CellDoubleBoundedProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newPower, "Value");
                            newPower.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellPower.Add(newPower);
                        break;
                    case 4:
                        CellStringProperty newDevi = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newDevi, "Value");
                            newDevi.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellDevi.Add(newDevi);
                        break;
                    case 5:
                        CellStringProperty newBandFreq = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newBandFreq, "Value");
                            newBandFreq.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellBandfreqs.Add(newBandFreq);
                        if (row != null)
                            row.GetCell(row.cellList.Count - 1).AddOnPressButtonHandler(this, "OnPressButtonEditFreq");
                        break;
                    case 6:
                        CellStringComboboxAplyProperty newChannel = new CellStringComboboxAplyProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newChannel, "Value");
                            newChannel.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        newChannel.Items.Clear();
                        //if (cnt < 0)
                        newChannel.Items.AddRange(dictAmateurPass.Values.ToArray());
                        listCellChannel.Add(newChannel);
                        break;
                    case 7:
                        CellStringProperty newSend = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newSend, "Value");
                            newSend.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellSend.Add(newSend);
                        break;
                    case 8:
                        CellStringProperty newAccept = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newAccept, "Value");
                            newAccept.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        listCellAccept.Add(newAccept);
                        break;
                    case 9:
                        CellStringProperty newParam = new CellStringProperty();
                        if (row != null)
                        {
                            row.GetCell(row.cellList.Count - 1).Bind("Value", newParam, "Value");
                            newParam.Cell = row.GetCell(row.cellList.Count - 1);
                        }
                        newParam.Cell.Value = "РЕЗ " + newParam.Cell.Col;
                        if (row != null) newParam.Cell.BackColor = row.GetCell(0).BackColor;
                        listCellParam.Add(newParam);
                        break;
                    default:
                        break;
                }
            }
            if (cnt <= 0)
            {
                amateurStat.AmEquip.Add(new AmateurEquipment());
                amateurStat.AmEquip.Last().Number = amateurStat.AmEquip.Count;
            }

            grid.Focus();
            grid.Invalidate();
        }

        public void OnAfterTypeStation(Cell cell)
        {
            if (gridParam != null)
                CheckStationType();
        }

        private void CheckStationType()
        {
            List< string> _typeStatList = dictAmateurType.Values.ToList();
            Row rowFreq1 = gridParam.GetRow(arrayFiledsSector[7]) as Row;
            Row rowFreq2 = gridParam.GetRow(arrayFiledsSector[8]) as Row;
            Row rowFreq3 = gridParam.GetRow(arrayFiledsSector[3]) as Row;
            Row rowFreq4 = gridParam.GetRow(arrayFiledsSector[4]) as Row;
            Row rowFreq5 = gridParam.GetRow(arrayFiledsSector[6]) as Row;
            Row rowFreq6 = gridParam.GetRow(6) as Row;  
            Row rowApc = (Row)(gridParam.rowList[9 + spsCount + upsCount]);

            if (rowFreq1 != null)
                for (int i = 1; i < rowFreq1.cellList.Count; i++)
                {
                    bool isRetrOrSport=false;
                    if (_typeStatList.Count > 2)
                        isRetrOrSport = (cellTypeStation.Value == _typeStatList[1] ||
                                              cellTypeStation.Value == _typeStatList[2]);
                    rowFreq1.cellList[i].CanEdit = isRetrOrSport;
                    if (rowFreq2 != null)
                        rowFreq2.cellList[i].CanEdit = isRetrOrSport;
                    if (rowFreq3 != null)
                        rowFreq3.cellList[i].CanEdit = isRetrOrSport;
                }
            if (rowFreq4 != null)
            {
                for (int i = 1; i < rowFreq4.cellList.Count; i++)
                    if (_typeStatList.Count > 1)
                        rowFreq4.cellList[i].CanEdit = (cellTypeStation.Value ==
                                                        _typeStatList[1]);
                if (rowFreq5 != null)
                    for (int i = 1; i < rowFreq5.cellList.Count; i++)
                    {
                        if (_typeStatList.Count > 1)
                            rowFreq5.cellList[i].CanEdit = (cellTypeStation.Value ==
                                                            _typeStatList[1]);
                        if (!rowFreq5.cellList[i].CanEdit)
                            rowFreq5.cellList[i].cellStyle = EditStyle.esSimple;
                        else
                            rowFreq5.cellList[i].cellStyle = EditStyle.esPickList;
                    }
            }

            if (rowFreq6 != null)
                for (int i = 1; i < rowFreq6.cellList.Count; i++)
                {
                    if (_typeStatList.Count > 3)
                        rowFreq6.cellList[i].CanEdit =
                            (cellTypeStation.Value == _typeStatList[0] || cellTypeStation.Value == _typeStatList[3]);
                    if (!rowFreq6.cellList[i].CanEdit)
                        rowFreq6.cellList[i].cellStyle = EditStyle.esSimple;
                    else rowFreq6.cellList[i].cellStyle = EditStyle.esPickList;
                    rowFreq6.cellList[i].CanEdit = false;
                }

           if (rowApc!=null)
            for (int i = 1; i < rowApc.cellList.Count; i++)
            {
                if (_typeStatList.Count > 3)
                    rowApc.cellList[i].CanEdit =
                        (cellTypeStation.Value == _typeStatList[0] || cellTypeStation.Value == _typeStatList[3]);
                if (!rowApc.cellList[i].CanEdit)
                    rowApc.cellList[i].cellStyle = EditStyle.esSimple;
                else rowApc.cellList[i].cellStyle = EditStyle.esPickList;
                rowApc.cellList[i].CanEdit = false;
            }
        }

        /// <summary>
        /// Выбор БС
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonEditFreq(Cell cell)
        {
            List<string> _typeStatList = dictAmateurType.Values.ToList();
            if (string.IsNullOrEmpty(cellCatOper.Value))
            {
                MessageBox.Show("Необхідно обрати категорію оператора");
                return;                
            }
            if (cellTypeStation.Value != _typeStatList[2])
                EditFreqs(cell.Col);
        }

        /// <summary>
        /// Отримати частоти аматорів
        /// </summary>
        /// <returns></returns>
        public List<AmateurBand> GetFreqAmStations(string typeOper, string catOper)
        {
            int type = dictAmateurType.Values.ToList().IndexOf(typeOper) + 1;           
            List<AmateurBand> tempAmFrList = new List<AmateurBand>();
            IMRecordset r = new IMRecordset(AmateurStation.TableNameBand, IMRecordset.Mode.ReadOnly);
            r.Select("ID,POWER,BANDMIN,BANDMAX,CAT_OPER,TYPE_STAT");
            if (type < 3 || type == 4)
                r.SetWhere("TYPE_STAT", IMRecordset.Operation.Eq, typeOper);
            if (type == 1 || type == 4)
                r.SetWhere("CAT_OPER", IMRecordset.Operation.Eq, catOper);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
                AmateurBand tmpGreq = new AmateurBand();
                tmpGreq.Band = r.GetD("BANDMIN") + " - " + r.GetD("BANDMAX");
                tmpGreq.Id = r.GetI("ID");
                tmpGreq.Power = r.GetD("POWER");
                tempAmFrList.Add(tmpGreq);
            }            
            tempAmFrList = (tempAmFrList.OrderBy(t => Convert.ToDouble(t.Band.Split('-')[0].Trim())).ThenBy(t => t.Power)).ToList();
            return tempAmFrList;
        }

        /// <summary>
        /// Редактирование списка частот
        /// </summary>
        private void EditFreqs(int id)
        {
            List<string> _typeStatList = dictAmateurType.Values.ToList();
            using (FChannels fChannel = new FChannels("Вибір смуг частот", "Смуги частот / потужність"))
            {
                List<AmateurBand> listFreq = GetFreqAmStations(cellTypeStation.Value,cellCatOper.Value);

                foreach (AmateurBand freq in listFreq)
                {
                    AmateurBand aF = new AmateurBand();
                    aF.Band = freq.Band;
                    aF.Power = freq.Power;
                    aF.Id = freq.Id;
                    AmateurBand.IsPower = (cellTypeStation.Value == _typeStatList[0] ||
                                           cellTypeStation.Value == _typeStatList[3]);
                    if (amateurStat.AmEquip[id - 1].AmBand.Contains(aF))
                        fChannel.CLBSelectChannel.Items.Add(aF);
                    else
                        fChannel.CLBAllChannel.Items.Add(aF);
                }

                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    List<AmateurBand> amBnd = new List<AmateurBand>();
                    amateurStat.AmEquip[id - 1].AmBand.Clear();
                    foreach (object selectedItem in fChannel.CLBSelectChannel.Items)
                    {
                        AmateurBand freq = selectedItem as AmateurBand;
                        if (freq != null)
                        {
                            AmateurBand.IsPower = (cellTypeStation.Value == _typeStatList[0] || cellTypeStation.Value == _typeStatList[3]);                            
                            amBnd.Add(freq);
                        }
                    }
                    if (gridParam != null)
                    {
                        Row rowBand = gridParam.GetRow(arrayFiledsSector[5]) as Row;
                        if (rowBand != null)
                            rowBand.cellList[id].Value = AmateurBand.GetFreqs(amBnd);                        
                    }
                    amateurStat.AmEquip[id - 1].AmBand = amBnd;
                }
            }
        }

        /// <summary>
        /// Выбор нового владельца
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonOwner(Cell cell)
        {
            //string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(cellOwner.Value) + "*\"}";
            //RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
            RecordPtr recOwner = SelectOwner("Підбір Власника", PlugTbl.XvAbonentOwner, "NAME", HelpFunction.ReplaceQuotaSumbols(cellOwner.Value), true);
            if (recOwner.Id == -1)
            {
                using (FAbonentOwner form = new FAbonentOwner(IM.NullI))
                {
                    if (form.ShowDialog() == DialogResult.OK && form.dAbonentOwner.ziID != IM.NullI && form.dAbonentOwner.ziID != 0)
                        recOwner.Id = form.dAbonentOwner.ziID;
                }
            }
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {
                amateurStat.OwnerId = recOwner.Id;
                cellOwner.Value = amateurStat.GetNameAbonentRes();
            }
        }

        /// <summary>
        /// Выбор нового владельца
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonName(Cell cell) { }

        /// <summary>
        /// Выбор нового владельца
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressEquipment(Cell cell)
        {
            int id = cell.Col;
            RecordPtr recEquip = SelectEquip("Підбір обладнання", PlugTbl.XvEquipAmateur, "NAME", cell.Value, false);
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                amateurStat.AmEquip[id - 1].Id = recEquip.Id;
                amateurStat.AmEquip[id - 1].Load();
                cell.Value = amateurStat.AmEquip[id - 1].Name;
                Cell cellDesEmi = ((Row)gridParam.rowList[arrayFiledsSector[1]]).GetCell(id);
                cellDesEmi.Value = amateurStat.AmEquip[id - 1].DesigEmission;
            }
        }

        public void OnBeforeChangeFreq(Cell cell, ref string val)
        {
            ChangeColor(cell, Colors.okvalue);
            List<double> freqList = ConvertType.ToDoubleList(val);

            if (freqList.Count > 0)
            {
                if (CheckValidityFrequency(freqList))
                    val = HelpFunction.ToString(freqList);
                else
                {
                    ChangeColor(cell, Colors.badValue);
                    val = HelpFunction.ToString(freqList);
                }
            }
            else
                val = cell.Value;
        }

        private bool CheckValidityFrequency(List<double> freqList)
        {
            foreach (double freq in freqList)
                if (freq <= 0.0)
                    return false;
            return true;
        }

        #region Overrides of BaseAppClass

        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            //Область сотрудника 
            
            //string oblUser = CUsers.GetUserAreaCode();

            string prov = (amateurStat.objPosition != null) ? amateurStat.objPosition.Province : "";
            string city = (amateurStat.objPosition != null) ? amateurStat.objPosition.City : "";
            string oblUser = HelpFunction.getAreaCode(prov, city);

            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            //номер            
            if (string.IsNullOrEmpty(number))
                number = PrintDocs.GetListDocNumberForDozvWma(1)[0];

            if (docType == DocType.DOZV_RETR || docType == DocType.DOZV_APC || docType == DocType.DOZV_APC50 || docType == DocType.DOZV_MOVE)
                fullPath += "AC" + "-" + oblUser + "-" + number + ".rtf";

            else if (docType == DocType.GARM_CERT || docType == DocType.NOVIC_CERT)
            {
                string typeCert = cellGarm.Value.Trim().Remove(3);                
                fullPath += "AC" + "-" + typeCert + "-" + number + ".rtf";                
            }
            
            else if (docType == DocType.DOZV_SPS)
                fullPath += "AC" + "-" + spsValue + "-" + number + ".rtf";
                        
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid() { return Properties.Resources.AmateurAppl; }
        public override void UpdateToNewPacket() { }

        #endregion

        public override void CellValidate(Cell cell, Grid grid) { }

        /// <summary>
        /// Сохранение изменений в станции перед созданием отчета
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="dopFilter"></param>
        protected override void SaveBeforeDocPrint(string docType, DateTime startDate, DateTime endDate, string docName, string dopFilter)
        {
            if (docType == DocType.DOZV_RETR || docType == DocType.DOZV_APC || docType == DocType.DOZV_APC50)
            {
                ApplDocuments.CreateNewPermission(ApplID, docName.ToFileNameWithoutExtension(), startDate, endDate, docName);
            }
            if (docType==DocType.DOZV_MOVE)
            {
                IMRecordset r = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadWrite);
                r.Select("ID,STA_ID,NUMB,DATE_PRINT_DOZV,DATE_EXPIR_DOZV,NUMB_DOZV,PATH");
                r.SetWhere("STA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("NUMB", IMRecordset.Operation.Eq, spsValue.ToInt32(IM.NullI));
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("NUMB_DOZV", docName.ToFileNameWithoutExtension());
                        r.Put("DATE_PRINT_DOZV", startDate);
                        r.Put("DATE_EXPIR_DOZV", endDate);
                        r.Put("PATH", docName);
                        r.Update();
                    }
                }
                finally 
                {
                   r.Final();                    
                }                
            }
            if (docType==DocType.DOZV_SPS)
            {
                IMRecordset r = new IMRecordset("XFA_CALL_AMATEUR", IMRecordset.Mode.ReadWrite);
                r.Select("ID,STA_ID,TYPE_CALL,CALL,NUMBER_DOZV");
                r.SetWhere("STA_ID", IMRecordset.Operation.Eq, recordID.Id);
                r.SetWhere("TYPE_CALL", IMRecordset.Operation.Eq, spsValue);
                r.SetWhere("CALL", IMRecordset.Operation.Eq, objDocString);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("NUMBER_DOZV", docName.ToFileNameWithoutExtension());                        
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }   
            }
        }
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            Row row = gridParam.GetRow(arrayFiledsSector[0]) as Row;
            if ((row != null) && (row.cellList.Contains(cell)))
            {
                int col = cell.Col;
                using(AmateurEquipForm form = new AmateurEquipForm(amateurStat.AmEquip[col - 1].Id))
                    if (form.ShowDialog() == DialogResult.OK)
                        form.Save();
            }
            else
            {
                switch(cell.Key)
                {
                    case "NewKey-KOwner":
                        if (amateurStat.OwnerId == IM.NullI || amateurStat.OwnerId == 0)
                            MessageBox.Show("Власник не заданий. Для вибору існуючого / створення нового запису натисніть кнопку в комірці.");
                        else
                            using(FAbonentOwner form = new FAbonentOwner(amateurStat.OwnerId))
                            if (form.ShowDialog() == DialogResult.OK)
                                cellOwner.Value = amateurStat.GetNameAbonentRes();
                        break;
                    case "NewKey-KAddr":
                        if (amateurStat.objPosition != null)
                        {
                            Forms.AdminSiteAllTech2.Show(amateurStat.objPosition.TableName, amateurStat.objPosition.Id, (IM.TableRight(amateurStat.objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                            amateurStat.objPosition.LoadStatePosition(amateurStat.objPosition.Id, amateurStat.objPosition.TableName);
                            amateurStat.objPosition.GetAdminSiteInfo(amateurStat.objPosition.AdminSiteId);
                            cellAddress.Value = amateurStat.objPosition.FullAddressAuto;
                            cellLongitude.DoubleValue = amateurStat.objPosition.LongDms;
                            cellLatitude.DoubleValue = amateurStat.objPosition.LatDms;
                        }
                        else
                            ShowMessageNoReference();
                        break;
                }
            }
        }

        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            amateurStat.SpecConditionPerm = SCDozvil;
            amateurStat.SpecConditionConcl = SCVisnovok;
            amateurStat.SpecNote = SCNote;
            amateurStat.Status = Status.StatusAsString;
            
            int selItem = dictAmateurType.Values.ToList().IndexOf(cellTypeStation.Value);
            if (selItem >= 0 && dictAmateurType.Count > selItem)
                amateurStat.TypeStat = dictAmateurType.Keys.ToList()[selItem];

            if (!string.IsNullOrEmpty(cellCatOper.Value))
            {
                selItem = dictAmateurCateg.Values.ToList().IndexOf(cellCatOper.Value);
                if (selItem >= 0 && dictAmateurCateg.Count > selItem)
                    amateurStat.CategOper = dictAmateurCateg.Keys.ToList()[selItem];
            }
            if (!string.IsNullOrEmpty(cellCall.Value))
                amateurStat.Call = cellCall.Value;
            if (!string.IsNullOrEmpty(cellAPC.Value))
            {
                selItem = dictAmateurApc.Values.ToList().IndexOf(cellAPC.Value);
                if (selItem >= 0 && dictAmateurApc.Count > selItem)
                    amateurStat.Apc = dictAmateurApc.Keys.ToList()[selItem];
            }

            selItem = dictAmateurCert.Values.ToList().IndexOf(cellGarm.Value);
            if (selItem >= 0 && dictAmateurCert.Count > selItem)
                amateurStat.Cert = dictAmateurCert.Keys.ToList()[selItem];

            amateurStat.AGL = CellHighAntenn.DoubleValue;

            for (int i = 0; i < amateurStat.AmEquip.Count; i++)
            {
                amateurStat.AmEquip[i].PlantNumb = ((Row)gridParam.rowList[arrayFiledsSector[2]]).cellList[i + 1].Value;
                amateurStat.AmEquip[i].Power[PowerUnits.W] = ((Row)gridParam.rowList[arrayFiledsSector[3]]).cellList[i + 1].Value.ToDouble(IM.NullD);
                amateurStat.AmEquip[i].Devi = ((Row)gridParam.rowList[arrayFiledsSector[4]]).cellList[i + 1].Value;

                string strItem = ((Row)gridParam.rowList[arrayFiledsSector[6]]).cellList[i + 1].Value;
                if (!dictAmateurPass.ContainsValue(strItem))
                    amateurStat.AmEquip[i].Passing = "";
                else
                {
                    selItem = dictAmateurPass.Values.ToList().IndexOf(strItem);
                    amateurStat.AmEquip[i].Passing = dictAmateurPass.Keys.ToList()[selItem];
                }
                string[] acceptList = listCellAccept[i].Value.Split(';');
                string[] sendList = listCellSend[i].Value.Split(';');

                int maxFreq = Math.Max(acceptList.Count(), sendList.Count());
                amateurStat.AmEquip[i].AmFreq.Clear();
                for (int j = 0; j < maxFreq; j++)
                    amateurStat.AmEquip[i].AmFreq.Add(new AmateurFreq());
                for (int j = 0; j < acceptList.Count(); j++)
                    amateurStat.AmEquip[i].AmFreq[j].FreqRx = acceptList[j].ToDouble(IM.NullD);
                for (int j = 0; j < sendList.Count(); j++)
                    amateurStat.AmEquip[i].AmFreq[j].FreqTx = sendList[j].ToDouble(IM.NullD);
            }

            //исправление
            //исправление от 26.03.2015
            amateurStat.AmCallsSps.Clear();
            for (int i = 0; i < listCellSpsCall.Count; i++)
            {
                string call = listCellSpsCall[i].Value;
               
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsSps where (item.Call == call) select item;
               
                    AmateurCalls acS = new AmateurCalls();
                    acS.Call = call;
                    acS.StaId = amateurStat.Id;
                    acS.TypeCall = "СПС";
                    acS.GoalUse = listCellSpsUse[i].Value;
                    amateurStat.AmCallsSps.Add(acS);
               
            }

            //исправление от 26.03.2015
            amateurStat.AmCallsUps.Clear();

            for (int i = 0; i < listCellUpsCall.Count; i++)
            {
                string call = listCellUpsCall[i].Value;
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsUps where (item.Call == call) select item;
                    AmateurCalls ucS = new AmateurCalls();
                    ucS.StaId = amateurStat.Id;
                    ucS.Call = call;
                    ucS.TypeCall = "УПС";
                    ucS.GoalUse = listCellUpsUse[i].Value;
                    amateurStat.AmCallsUps.Add(ucS);
               
            }
            
/*
            for (int i = 0; i < listCellSpsCall.Count; i++)
            {
                string call = listCellSpsCall[i].Value;
                if (string.IsNullOrEmpty(call))
                    continue;
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsSps where (item.Call == call) select item;
                if (calls == null || calls.Count() == 0)
                {
                    AmateurCalls acS = new AmateurCalls();
                    acS.Call = call;
                    acS.StaId = amateurStat.Id;
                    acS.TypeCall = "СПС";
                    acS.GoalUse = listCellSpsUse[i].Value;
                    amateurStat.AmCallsSps.Add(acS);
                }
            }
            for (int i = 0; i < listCellUpsCall.Count; i++)
            {
                string call = listCellUpsCall[i].Value;
                if (string.IsNullOrEmpty(call))
                    continue;
                IEnumerable<AmateurCalls> calls =
                    from item in amateurStat.AmCallsUps where (item.Call == call) select item;
                if (calls == null || calls.Count() == 0)
                {
                    AmateurCalls ucS = new AmateurCalls();
                    ucS.StaId = amateurStat.Id;
                    ucS.Call = call;
                    ucS.TypeCall = "УПС";
                    ucS.GoalUse = listCellUpsUse[i].Value;
                    amateurStat.AmCallsUps.Add(ucS);
                }
            }
 */ 

            amateurStat.Save();

            gridParam.Edited = false;
            IsChangeDop = false;

            return true;
        }

        public override string GetProvince()
        {
            return "";
        }
    }
}
