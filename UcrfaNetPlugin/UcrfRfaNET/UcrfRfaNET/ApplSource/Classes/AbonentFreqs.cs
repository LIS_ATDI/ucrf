﻿using System;
using System.Collections;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    internal class AbonentFreqs
    {
        public class Freq : IEqualityComparer<Freq>, IComparable<Freq>, IComparable, IComparer
        {
            public int Id { get; set; }
            public double FreqRx { get; set; }
            public double FreqTx { get; set; }
            public string ChannelRx { get; set; }
            public string ChannelTx { get; set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public Freq()
            {
                Id = IM.NullI;
                FreqRx = IM.NullD;
                FreqTx = IM.NullD;
                ChannelRx = "";
                ChannelTx = "";
            }

            #region Implementation of IEqualityComparer<Freq>

            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            /// <param name="y">The second object of type T to compare.</param><param name="x">The first object of type T to compare.</param>
            public bool Equals(Freq x, Freq y)
            {
                if (x == y)
                    return true;
                if ((Math.Abs(x.FreqRx - y.FreqRx) < 0.0000001) && (Math.Abs(x.FreqTx - y.FreqTx) < 0.0000001) &&
                    (x.ChannelRx == y.ChannelRx) && (x.ChannelTx == y.ChannelTx))
                    return true;
                return false;
            }

            /// <summary>
            /// Returns a hash code for the specified object.
            /// </summary>
            /// <returns>
            /// A hash code for the specified object.
            /// </returns>
            /// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param><exception cref="T:System.ArgumentNullException">The type of obj is a reference type and obj is null.</exception>
            public int GetHashCode(Freq obj)
            {
                return obj.ToString().GetHashCode();
            }

            #endregion

            public override bool Equals(object obj)
            {
                if (obj == null)
                    return false;
                return (obj is Freq) != false && Equals(this, (Freq)obj);
            }

            public override int GetHashCode()
            {
                return this.ToString().GetHashCode();
            }

            /// <summary>
            /// Compares the current instance with another object of the same type.
            /// </summary>
            /// <returns>
            /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than obj. Zero This instance is equal to obj. Greater than zero This instance is greater than obj. 
            /// </returns>
            /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException">obj is not the same type as this instance. </exception><filterpriority>2</filterpriority>
            public int CompareTo(object obj)
            {
                Freq x1 = (Freq)obj;
                if (x1 == null)
                    return 0;
                return CompareTo(x1);
            }

            /// <summary>
            /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
            /// </summary>
            /// <returns>
            /// Value Condition Less than zero x is less than y. Zero x equals y. Greater than zero x is greater than y. 
            /// </returns>
            /// <param name="y">The second object to compare. </param><param name="x">The first object to compare. </param><exception cref="T:System.ArgumentException">Neither x nor y implements the <see cref="T:System.IComparable"/> interface.-or- x and y are of different types and neither one can handle comparisons with the other. </exception><filterpriority>2</filterpriority>
            public int Compare(object x, object y)
            {
                Freq x1 = (Freq)x;
                Freq y1 = (Freq)y;
                if ((x1 == null) || (y1 == null))
                    return 0;
                return x1.CompareTo(y1);
            }

            /// <summary>
            /// Compares the current object with another object of the same type.
            /// </summary>
            /// <returns>
            /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the other parameter.Zero This object is equal to other. Greater than zero This object is greater than other. 
            /// </returns>
            /// <param name="other">An object to compare with this object.</param>
            public int CompareTo(Freq other)
            {
                return ((int)((FreqTx - other.FreqTx) * 1000000.0));
            }

            public override string ToString()
            {
                return string.Format("{0} / {1}", FreqTx.ToStringNullD(), FreqRx.ToStringNullD());
            }
        }
        /// <summary>
        /// ID станции
        /// </summary>
        public int StationId { get; set; }
        /// <summary>
        /// Список частот
        /// </summary>
        private List<Freq> _freqs = new List<Freq>();
        public List<Freq> Freqs { get { return _freqs; } }
        /// <summary>
        /// Список частот
        /// </summary>
        private List<Freq> _freqsDeleted = new List<Freq>();
        public List<Freq> FreqsDeleted { get { return _freqsDeleted; } }
        /// <summary>
        /// Сохранение частот
        /// </summary>
        public void Save()
        {
            //Удаляем частоты
            foreach (Freq freq in FreqsDeleted)
            {
                const string tableName = PlugTbl.XfaFreq;
                const string selectedFields = "ID";
                IMRecordset rsFreq = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
                rsFreq.Select(selectedFields);
                rsFreq.SetWhere("ID", IMRecordset.Operation.Eq, freq.Id);
                try
                {
                    rsFreq.Open();
                    if (!rsFreq.IsEOF())
                    {
                        rsFreq.Delete();
                        CJournal.CheckUcrfTable(tableName, freq.Id, rsFreq, selectedFields);
                    }
                }
                finally
                {
                    if (rsFreq.IsOpen())
                        rsFreq.Close();
                    rsFreq.Destroy();
                }
            }
            FreqsDeleted.Clear();            
            //Обновляем частоты
            foreach (Freq freq in Freqs)
            {
                //Удаляем частоты
                const string tableName = PlugTbl.XfaFreq;
                const string selectedFields = "STA_ID,ID,TX_FREQ,RX_FREQ,TX_CHANNEL,RX_CHANNEL,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY";
                const string checkFields    = "STA_ID,ID,TX_FREQ,RX_FREQ,TX_CHANNEL,RX_CHANNEL,DATE_CREATED,CREATED_BY,MODIFIED_BY";
                IMRecordset rsFreq = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
                rsFreq.Select(selectedFields);
                rsFreq.SetWhere("ID", IMRecordset.Operation.Eq, freq.Id);
                try
                {
                    rsFreq.Open();
                    if (!rsFreq.IsEOF())
                    {
                        rsFreq.Edit();
                        rsFreq.Put("DATE_MODIFIED", DateTime.Now);
                        rsFreq.Put("MODIFIED_BY", IM.ConnectedUser());
                    }
                    else
                    {
                        rsFreq.AddNew();
                        if (freq.Id == IM.NullI)
                            freq.Id = IM.AllocID(tableName, 1, -1);
                        rsFreq.Put("ID", freq.Id);
                        rsFreq.Put("STA_ID", StationId);
                        rsFreq.Put("DATE_CREATED", DateTime.Now);
                        rsFreq.Put("CREATED_BY", IM.ConnectedUser());
                    }
                    rsFreq.Put("RX_FREQ", freq.FreqRx);
                    rsFreq.Put("TX_FREQ", freq.FreqTx);
                    rsFreq.Put("RX_CHANNEL", freq.ChannelRx);
                    rsFreq.Put("TX_CHANNEL", freq.ChannelTx);
                    CJournal.CheckUcrfTable(tableName, freq.Id, rsFreq, checkFields);
                    rsFreq.Update();
                }
                finally
                {
                    if (rsFreq.IsOpen())
                        rsFreq.Close();
                    rsFreq.Destroy();
                }
            }
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>
        /// <param name="stationId">Id станции</param>
        public void Load(int stationId)
        {
            StationId = stationId;
            Load();
        }
        /// <summary>
        /// Загрузка частот
        /// </summary>
        public void Load()
        {
            Freqs.Clear();
            IMRecordset rsFreq = new IMRecordset(PlugTbl.XfaFreq, IMRecordset.Mode.ReadOnly);
            rsFreq.Select("STA_ID,ID,TX_FREQ,RX_FREQ,TX_CHANNEL,RX_CHANNEL");
            rsFreq.SetWhere("STA_ID", IMRecordset.Operation.Eq, StationId);
            try
            {
                for (rsFreq.Open(); !rsFreq.IsEOF(); rsFreq.MoveNext())
                {
                    Freq fr = new Freq();
                    fr.Id = rsFreq.GetI("ID");
                    fr.FreqRx = rsFreq.GetD("RX_FREQ");
                    fr.FreqTx = rsFreq.GetD("TX_FREQ");
                    fr.ChannelRx = rsFreq.GetS("RX_CHANNEL");
                    fr.ChannelTx = rsFreq.GetS("TX_CHANNEL");
                    Freqs.Add(fr);
                }
            }
            finally
            {
                if (rsFreq.IsOpen())
                    rsFreq.Close();
                rsFreq.Destroy();
            }
        }
        /// <summary>
        /// Очищает ID частот
        /// </summary>
        public void ClearId()
        {
            foreach (Freq freq in Freqs)
                freq.Id = IM.NullI;
        }
        /// <summary>
        /// Возвращает строку с частотами передатчика
        /// </summary>
        /// <returns>строка с частотами передатчика</returns>
        public string GetFreqTx()
        {
            string retVal = "";
            var sortFreqs = new List<Freq>();
            sortFreqs.AddRange(Freqs);
            sortFreqs.Sort();
            foreach (Freq freq in sortFreqs)
            {
                if (freq.FreqTx == IM.NullD)
                    continue;
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.FreqTx.Round(10);
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает строку с частотами приемника
        /// </summary>
        /// <returns>строка с частотами приемника</returns>
        public string GetFreqRx()
        {
            string retVal = "";
            var sortFreqs = new List<Freq>();
            sortFreqs.AddRange(Freqs);
            sortFreqs.Sort();
            foreach (Freq freq in sortFreqs)
            {
                if (freq.FreqRx == IM.NullD)
                    continue;
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += "; ";
                retVal += freq.FreqRx.Round(10);
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает диапазон частот
        /// </summary>
        public void RangeFreq(bool isTx, out double minFreq, out double maxFreq)
        {
            minFreq = 999999999.0;
            maxFreq = 0;
            foreach (Freq freq in Freqs)
            {
                double curFreq = (isTx) ? freq.FreqTx : freq.FreqRx;
                minFreq = Math.Min(curFreq, minFreq);
                if(Freqs.Count > 1)
                   maxFreq = Math.Max(curFreq, maxFreq);
            }
            if (maxFreq == 0)
                maxFreq = IM.NullD;
            if (minFreq == 999999999.0)
                minFreq = IM.NullD;
        }
        /// <summary>
        /// Возвращает список уникальных частот
        /// </summary>
        /// <returns>Список уникальных частот</returns>
        public double[] GteUniceFreqs()
        {
            List<double> retVal = new List<double>();
            HashSet<long> freqTmp = new HashSet<long>();
            foreach (Freq freq in Freqs)
            {
                long tmpFreqLong = (long)(freq.FreqRx*1000000.0);
                if(freqTmp.Contains(tmpFreqLong) == false)
                {
                    retVal.Add(freq.FreqRx);
                    freqTmp.Add(tmpFreqLong);
                }
                tmpFreqLong = (long)(freq.FreqTx * 1000000.0);
                if (freqTmp.Contains(tmpFreqLong) == false)
                {
                    retVal.Add(freq.FreqTx);
                    freqTmp.Add(tmpFreqLong);
                }
            }
            return retVal.ToArray();
        }
        /// <summary>
        /// Удалить все частоты
        /// </summary>
        public void Clear()
        {
            _freqsDeleted.AddRange(Freqs);
            Freqs.Clear();
        }
        /// <summary>
        /// Удалить частоту
        /// </summary>
        public void Delete(Freq freq)
        {
            FreqsDeleted.Add(freq);
            Freqs.Remove(freq);
        }
    }
}
