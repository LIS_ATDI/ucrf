﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class ShipStation : BaseStation//, ICloneable
    {

        public class NavigationAreaDataType
        {
            public bool A1 { get; set; }
            public bool A2 { get; set; }
            public bool A3 { get; set; }
            public bool DomesticWaterWays { get; set; }
            public string NonConvention { get; set; }

            public override string ToString()
            {
                StringBuilder resultString = new StringBuilder();

                if (A1)
                    resultString.Append("A1,");
                
                if (A2)
                    resultString.Append("A2,");

                if (A3)
                    resultString.Append("A3,");

                if (DomesticWaterWays)
                    resultString.Append("ВВШУ,");

                if (!string.IsNullOrEmpty(NonConvention))
                    resultString.Append("НРП: "+NonConvention);

                if (resultString.Length > 0)
                    return resultString.ToString().Substring(0, resultString.Length - 1);
                return "";
            }
        };
       
        private NavigationAreaDataType _area = new NavigationAreaDataType();

        public const string TableName = ICSMTbl.itblShip;

        private CAbonentOwner _owner = new CAbonentOwner();
        private CAbonentOwner _shipOwner = new CAbonentOwner();
        private CAbonentOwner _charter = new CAbonentOwner();
        private CAbonentOwner _registerDepartment = new CAbonentOwner();

        public string StationType { get; set; }

        public string Name { get; set; }
        public string NameLatin { get; set; }
        public CAbonentOwner Owner { get { return _owner; } set { _owner = value; } }
        public CAbonentOwner ShipOwner { get { return _shipOwner; } set { _shipOwner = value; } }
        public CAbonentOwner Charter { get { return _charter; } set { _charter = value; } }
        public CAbonentOwner RegisterDepartment { get { return _registerDepartment; } set { _registerDepartment = value; } }

        public NavigationAreaDataType NavigationArea { get { return _area; } }

        public Forms.RegistryPort RegistryPort { get; set; }
        public string Port { get{ return (RegistryPort == null) ? "" : RegistryPort.NameUkr; }}
        public string PortLatin { get { return (RegistryPort == null) ? "" : RegistryPort.NameEng; } }

        public string Category { get; set; }

        public string NumBlank { get; set; }
        public string RadioPhone { get; set; }
        public string RadioPhoneLatin { get; set; }
        public string RadioTelegraph { get; set; }
        public string RadioTelegraphLatin { get; set; }

        public string SelectiveCallsign { get; set; }
        public string MaritimeMobId { get; set; }
        public string CodeATIS { get; set; }

        public string Inmarsat1 { get; set; }
        public string Inmarsat2 { get; set; }
        public string KospasSarsat1 { get; set; }
        public string KospasSarsat2 { get; set; }
        public string KospasSarsat3 { get; set; }

        private List<ShipEquipment> _equipmentList = new List<ShipEquipment>();
        public List<ShipEquipment> Equipment { get{ return _equipmentList; } }
        
        public double Bandwidth { get; set; }// { return getBandwidth(); } }
        public string BwText
        {
            get
            {
                var bwRez = 
                    from eq in Equipment
                    group eq by eq.type into g
                    orderby g.Key
                    select g;

                string retVal = "";
                foreach (ShipEquipment.Type type in Enum.GetValues(typeof(ShipEquipment.Type)))
                //foreach (IGrouping<ShipEquipment.Type, ShipEquipment> eqGr in bwRez)
                {
                    if (type == ShipEquipment.Type.None)
                        continue;

                    IGrouping<ShipEquipment.Type, ShipEquipment> eqGr = bwRez.FirstOrDefault(elt => elt.Key == type);
                    retVal += (retVal.Length > 0 ? Environment.NewLine : "") +
                        string.Format(
                            " {0} ({1}) ",
                            eqGr != null ? (eqGr.Sum(eq => (eq.Bandwidth != IM.NullD ? eq.Bandwidth : 0.0))) : 0.0,
                            type == ShipEquipment.Type.Rbss ? "радіозв’язок фіксованої, рухомої сухопутної та морської радіослужби" :
                                type == ShipEquipment.Type.Sp ? "радіозв’язок супутникової рухомої та фіксованої радіослужби" :
                                type == ShipEquipment.Type.Rps ? "радіолокаційна та радіонавігаційна радіослужби" :
                            "???"
                            );
                }

                return retVal;
            }
        }
                

        public ShipStation()
        {
            _owner.CreateNew = false;
            _shipOwner.CreateNew = false;
            _charter.CreateNew = false;
            _registerDepartment.CreateNew = false;

            // Нужно все строковые данные хотиь чем-то инциализировать,
            // иначе биндинг будет тупить, проще присвоить пустые строки,

            StationType = "";

            Name = "";
            NameLatin = "";
                        
            Category = "";
            RadioPhone = "";
            RadioPhoneLatin = "";
            RadioTelegraph = "";
            RadioTelegraphLatin = "";

            SelectiveCallsign = "";        
            MaritimeMobId = "";        
            Inmarsat1 = "";
            Inmarsat2 = "";
            KospasSarsat1 = "";
            KospasSarsat2 = "";
            KospasSarsat3 = "";
            Bandwidth = IM.NullD;
            NumBlank = "";
            RegistryPort = new Forms.RegistryPort();
        }

        public override void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,SHIP_TYPE,NAME,NAME_LATIN,ITM_NUM,TRGBY_ID,DELRQ_ID,NB_LIFE_BOATS,REG_PORT,REG_NUM,NAT_SRV,CUST_CHB1,CUST_CHB2,CUST_CHB3,CUST_CHB4,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT6,CUST_TXT8,CUST_TXT9,CALL_SIGN,SEL_CALL_1,MMSI_NUM,EPIRB_ID_CODE,MANUFACTURER,MODEL,OWNER_ID,ATIS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    StationType = r.GetS("SHIP_TYPE");
                    Name = r.GetS("NAME");
                    NameLatin = r.GetS("NAME_LATIN");
                    
                    LoadExtendedData();

                    Category = r.GetS("NAT_SRV");

                    NavigationArea.A1 = r.GetI("CUST_CHB1") != 0;
                    NavigationArea.A2 = r.GetI("CUST_CHB2") != 0;
                    NavigationArea.A3 = r.GetI("CUST_CHB3") != 0;
                    NavigationArea.DomesticWaterWays = r.GetI("CUST_CHB4") != 0;
                    NavigationArea.NonConvention = r.GetS("CUST_TXT6");

                    RadioPhone = r.GetS("CUST_TXT2");
                    RadioPhoneLatin = r.GetS("CUST_TXT3");
                    NumBlank = r.GetS("CUST_TXT4");

                    RadioTelegraph = "";
                    RadioTelegraphLatin = "";

                    string CallSign = r.GetS("CALL_SIGN");
                    if (!string.IsNullOrEmpty(CallSign))
                    {
                        string[] radioTelegraph = CallSign.Split('/');

                        if (radioTelegraph.Length>0)
                            RadioTelegraph = radioTelegraph[0];

                        if (radioTelegraph.Length > 1)
                            RadioTelegraphLatin = radioTelegraph[1];
                    }


                    SelectiveCallsign = r.GetS("SEL_CALL_1");
                    MaritimeMobId = r.GetS("MMSI_NUM");
                    Inmarsat1 = r.GetS("CUST_TXT8");
                    Inmarsat2 = r.GetS("CUST_TXT9");
                    CodeATIS = r.GetS("ATIS");           
               
                    ReadEqupments();
                }
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();

                r.Destroy();
            }
        }

        /// <summary>
        /// Проверка на наличие указанного позывного в базе (если запись уже есть  возвращает false)
        /// <params> CALL_SIGN (позывной для проверки)
        /// <params> status_include (признак исключения из рассмотрения текущей открытой для редактирования записи)
        /// </summary>
        public string GetStatus_DublicateCallSign(List<KeyValuePair<string, string>> L_CallSign, int OWNER_ID, bool status_include)
        {
            string All_Messages = "";            
            foreach (KeyValuePair<string, string> item in L_CallSign)
            {
                if ((item.Key == "-") || (item.Key == ""))
                    continue;

                bool isCheck = true;
                string Val = "";
                IMRecordset rx = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rx.Select("ID,OWNER_ID");
                    rx.Select(item.Value);
                    rx.SetWhere(item.Value, IMRecordset.Operation.Eq, item.Key);
                    if (status_include) { rx.SetWhere("ID", IMRecordset.Operation.Neq, Id); }
                    rx.Open();
                    if (!rx.IsEOF()) {
                        while (!rx.IsEOF()) {
                            if (rx.GetI("OWNER_ID") != OWNER_ID) {
                                isCheck = false;
                                Val = rx.GetS(item.Value);
                                break;
                            }
                            rx.MoveNext();
                        }
                    }
                    else{
                        if (rx.IsEOF()){
                            isCheck = true;
                        }
                    }
                }
                finally
                {
                    rx.Final();
                }

                if (!isCheck) {
                    if (item.Value == "CALL_SIGN") {
                        All_Messages += string.Format("Радіотелеграфія - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "SEL_CALL_1") {
                        All_Messages += string.Format("Номер вибіркового виклику - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "MMSI_NUM") {
                        All_Messages += string.Format("Ідентифікатор морської служби - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "ATIS") {
                        All_Messages += string.Format("Код ATIS - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "CUST_TXT8") {
                        All_Messages += string.Format("Ідент. номери ст-цій сис-ми(1) - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "CUST_TXT9") {
                        All_Messages += string.Format("Ідент. номери ст-цій сис-ми(2) - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "EPIRB_ID_CODE") {
                        All_Messages += string.Format("КОСПАС-САРСАТ(1) - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "MANUFACTURER") {
                        All_Messages += string.Format("КОСПАС-САРСАТ(2) - {0}", Val) + Environment.NewLine;
                    }
                    else if (item.Value == "MODEL") {
                        All_Messages += string.Format("КОСПАС-САРСАТ(3) - {0}", Val) + Environment.NewLine;
                    }
                }
            }
            return All_Messages;
        }
        void LoadExtendedData()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaShipExt, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,OWNER_ID,SHIPOWNER_ID,CHARTER_ID,REGISTRATOR_ID,COSPAS_SARSAT_IDENT_1,COSPAS_SARSAT_IDENT_2,COSPAS_SARSAT_IDENT_3,BANDWIDTH");
                r.Select("REGISTRY_PORT_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Owner.Read(r.GetI("OWNER_ID"));
                    ShipOwner.Read(r.GetI("SHIPOWNER_ID"));
                    Charter.Read(r.GetI("CHARTER_ID"));
                    RegisterDepartment.Read(r.GetI("REGISTRATOR_ID"));
                    RegistryPort.Load(r.GetI("REGISTRY_PORT_ID"));
                    KospasSarsat1 = r.GetS("COSPAS_SARSAT_IDENT_1");
                    KospasSarsat2 = r.GetS("COSPAS_SARSAT_IDENT_2");
                    KospasSarsat3 = r.GetS("COSPAS_SARSAT_IDENT_3");
                    Bandwidth = r.GetD("BANDWIDTH");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }            
        }

        void SaveExtendedData()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaShipExt, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,OWNER_ID,SHIPOWNER_ID,CHARTER_ID,REGISTRATOR_ID,COSPAS_SARSAT_IDENT_1,COSPAS_SARSAT_IDENT_2,COSPAS_SARSAT_IDENT_3,BANDWIDTH");
                r.Select("REGISTRY_PORT_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                } else
                {
                    r.AddNew();
                    r.Put("ID", Id);
                }

                r.Put("OWNER_ID", Owner.ziID);
                r.Put("SHIPOWNER_ID", ShipOwner.ziID);
                r.Put("CHARTER_ID", Charter.ziID);
                r.Put("REGISTRATOR_ID", RegisterDepartment.ziID);
                r.Put("REGISTRY_PORT_ID", RegistryPort.Id);

                r.Put("COSPAS_SARSAT_IDENT_1", KospasSarsat1);
                r.Put("COSPAS_SARSAT_IDENT_2", KospasSarsat2);
                r.Put("COSPAS_SARSAT_IDENT_3", KospasSarsat3);
                r.Put("BANDWIDTH", Bandwidth);

                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        private double getBandwidth()
        {
            double res = 0;

            foreach(ShipEquipment equipment in _equipmentList)
            {
                if (equipment.Bandwidth!=IM.NullD)
                    res += equipment.Bandwidth * equipment.Quantity;
            }

            if (res>0)
                return res;                        

            return IM.NullD;            
        }

        public void CalculateBandwidth()
        {
            double _bandwidth = getBandwidth();

            if (_bandwidth!=IM.NullD)
                Bandwidth = _bandwidth;
        }

        private void ReadEqupments()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,STA_ID");
                r.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
                                
                for(r.Open();!r.IsEOF();r.MoveNext())
                {
                    ShipEquipment shipEquipment = new ShipEquipment();
                    shipEquipment.Id = r.GetI("ID");
                    shipEquipment.TableName = ICSMTbl.itblEquipPmr;
                    shipEquipment.Load();

                    AddEquipment(shipEquipment);
                }
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();

                r.Destroy();
            }

            if (_equipmentList.Count==0)
            {
                AddEquipment();                
            }        
        }

        public void AddEquipment()
        {
            ShipEquipment shipEquipment = new ShipEquipment();
            AddEquipment(shipEquipment);            
        }

        private  void AddEquipment(ShipEquipment shipEquipment)
        {
            shipEquipment.ShipStation = this;
            _equipmentList.Add(shipEquipment);
        }

        private void SaveEquipment()
        {
            List<int> listId = new List<int>();

            IMRecordset r = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,STA_ID");
                r.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    listId.Add(r.GetI("ID"));
                }
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();

                r.Destroy();
            }


            foreach (ShipEquipment equipment in _equipmentList)
            {
                equipment.ShipStation = this;
                equipment.Save();
                listId.Remove(equipment.Id);
            }

            foreach (int removed in listId)
            {
                IMRecordset rd = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadWrite);

                try
                {
                    rd.Select("ID,STA_ID");
                    rd.SetWhere("ID", IMRecordset.Operation.Eq, removed);
                    rd.Open();                    
                    rd.Delete();
                }
                finally
                {
                    if (rd.IsOpen())
                        rd.Close();
                    rd.Destroy();
                }                
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                string CallSign = RadioTelegraph + "/" + RadioTelegraphLatin;
                List<KeyValuePair<string, string>> L_CallSign = new List<KeyValuePair<string, string>>();
                /*CALL_SIGN*/
                KeyValuePair<string, string> RadioTelegraph_call = new KeyValuePair<string, string>(CallSign, "CALL_SIGN"); L_CallSign.Add(RadioTelegraph_call);
                /*SEL_CALL_1*/
                KeyValuePair<string, string> SelectiveCallsign_call = new KeyValuePair<string, string>(SelectiveCallsign, "SEL_CALL_1"); L_CallSign.Add(SelectiveCallsign_call);
                /*MMSI_NUM*/
                KeyValuePair<string, string> MaritimeMobId_call = new KeyValuePair<string, string>(MaritimeMobId, "MMSI_NUM"); L_CallSign.Add(MaritimeMobId_call);
                /*ATIS*/
                KeyValuePair<string, string> CodeATIS_call = new KeyValuePair<string, string>(CodeATIS, "ATIS"); L_CallSign.Add(CodeATIS_call);
                /*CUST_TXT8*/
                KeyValuePair<string, string> Inmarsat1_call = new KeyValuePair<string, string>(Inmarsat1, "CUST_TXT8"); L_CallSign.Add(Inmarsat1_call);
                /*CUST_TXT9*/
                KeyValuePair<string, string> Inmarsat2_call = new KeyValuePair<string, string>(Inmarsat2, "CUST_TXT9"); L_CallSign.Add(Inmarsat2_call);
                /*EPIRB_ID_CODE*/
                KeyValuePair<string, string> KospasSarsat1_call = new KeyValuePair<string, string>(KospasSarsat1, "EPIRB_ID_CODE"); L_CallSign.Add(KospasSarsat1_call);
                /*MANUFACTURER*/
                KeyValuePair<string, string> KospasSarsat2_call = new KeyValuePair<string, string>(KospasSarsat2, "MANUFACTURER"); L_CallSign.Add(KospasSarsat2_call);
                /*MODEL*/
                KeyValuePair<string, string> KospasSarsat3_call = new KeyValuePair<string, string>(KospasSarsat3, "MODEL"); L_CallSign.Add(KospasSarsat3_call);
                
                r.Select("ID,SHIP_TYPE,NAME,NAME_LATIN,ITM_NUM,TRGBY_ID,DELRQ_ID,NB_LIFE_BOATS,REG_PORT,REG_NUM,NAT_SRV,CUST_CHB1,CUST_CHB2,CUST_CHB3,CUST_CHB4,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT6,CUST_TXT8,CUST_TXT9,CALL_SIGN,SEL_CALL_1,EPIRB_ID_CODE,MMSI_NUM,MANUFACTURER,MODEL,OWNER_ID,ATIS");
                r.Select("DATE_MODIFIED,MODIFIED_BY,DATE_CREATED,CREATED_BY");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
               
                if (!r.IsEOF()) {
                    string Mess =  GetStatus_DublicateCallSign(L_CallSign, Owner.ziID, true);
                    if (!string.IsNullOrEmpty(Mess)){
                        if (MessageBox.Show(CLocaliz.TxT("This callsign already exists in the database! Insert a record into the database (Yes/No)") + Environment.NewLine + Mess, "", MessageBoxButton.YesNo) == MessageBoxResult.No){
                            return;
                        }
                    }
                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                } 
                else
                {
                    string Mess = GetStatus_DublicateCallSign(L_CallSign, Owner.ziID, false);
                    if (!string.IsNullOrEmpty(Mess)) {
                        if (MessageBox.Show(CLocaliz.TxT("This callsign already exists in the database! Insert a record into the database (Yes/No)") + Environment.NewLine + Mess, "", MessageBoxButton.YesNo) == MessageBoxResult.No) {
                            return;
                        }
                    }

                    r.AddNew();
                    if (Id==IM.NullI)
                        Id = IM.AllocID(TableName, 1, -1);
                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());                    
                }

                r.Put("SHIP_TYPE", StationType);
                r.Put("NAME", Name);
                r.Put("NAME_LATIN", NameLatin);

                r.Put("REG_PORT", Port);
                r.Put("REG_NUM", PortLatin);

                //r.Put("ITM_NUM", Owner.ziID);
                //r.Put("TRGBY_ID", ShipOwner.ziID);
                //r.Put("DELRQ_ID", Charter.ziID);
                //r.Put("NB_LIFE_BOATS", RegisterDepartment.ziID);

                SaveExtendedData();

                r.Put("NAT_SRV", Category);

                r.Put("CUST_CHB1", NavigationArea.A1 ? 1 : 0);
                r.Put("CUST_CHB2", NavigationArea.A2 ? 1 : 0);
                r.Put("CUST_CHB3", NavigationArea.A3 ? 1 : 0);
                r.Put("CUST_CHB4", NavigationArea.DomesticWaterWays ? 1 : 0);

                r.Put("CUST_TXT2", RadioPhone);
                r.Put("CUST_TXT3", RadioPhoneLatin);
                r.Put("CUST_TXT4", NumBlank);
                r.Put("CUST_TXT6", NavigationArea.NonConvention);

                
                r.Put("CALL_SIGN", CallSign);

                r.Put("SEL_CALL_1", SelectiveCallsign);

                r.Put("MMSI_NUM", MaritimeMobId);
                r.Put("CUST_TXT8", Inmarsat1);
                r.Put("CUST_TXT9", Inmarsat2);
                
                r.Put("EPIRB_ID_CODE", KospasSarsat1);
                r.Put("MANUFACTURER", KospasSarsat2);
                r.Put("MODEL", KospasSarsat3);
                r.Put("ATIS", CodeATIS);                
                
                r.Update();

                SaveEquipment();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
