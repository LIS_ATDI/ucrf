﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Linq;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses.Forms;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal class AbonentAppl : BaseAppClass
    {

        /// <summary>
        /// Вспомагательный класс сети
        /// </summary>
        internal class AuxiliaryNet
        {
            /// <summary>
            /// ID
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// НАзвание сети
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Позывной сети
            /// </summary>
            public string CallSign { get; set; }
            /// <summary>
            /// Формирование строки
            /// </summary>
            /// <returns>Строка сети</returns>
            public override string ToString()
            {
                string retStr = "(М) " + Name;
                if(string.IsNullOrEmpty(CallSign) == false)
                    retStr += " / " + CallSign;
                return retStr;
            }
        }
        /// <summary>
        /// Вспомагательный класс
        /// </summary>
        internal class BaseStationInfo : IComparable
        {
            public class FreqStructure : IEqualityComparer<FreqStructure>/*, IComparable<FreqStructure>, IComparable, IComparer, IEquatable<FreqStructure>*/
            {
                public double FreqTx;
                public double FreqRx;
                #region Implementation of IEqualityComparer

                /// <summary>
                /// Determines whether the specified objects are equal.
                /// </summary>
                /// <returns>
                /// true if the specified objects are equal; otherwise, false.
                /// </returns>
                /// <param name="y">The second object to compare.</param><param name="x">The first object to compare.</param><exception cref="T:System.ArgumentException">x and y are of different types and neither one can handle comparisons with the other.</exception>
                public bool Equals(FreqStructure x, FreqStructure y)
                {
                    FreqStructure x1 = x;
                    FreqStructure y1 = y;
                    if ((x1 == null) || (y1 == null))
                        return false;
                    if ((Math.Abs(x1.FreqRx - y1.FreqRx) < 0.0000001) && (Math.Abs(x1.FreqTx - y1.FreqTx) < 0.0000001))
                        return true;
                    return false;
                }
                /// <summary>
                /// Returns a hash code for the specified object.
                /// </summary>
                /// <returns>
                /// A hash code for the specified object.
                /// </returns>
                /// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param><exception cref="T:System.ArgumentNullException">The type of obj is a reference type and obj is null.</exception>
                public int GetHashCode(FreqStructure obj)
                {
                    return obj.ToString().GetHashCode();
                }
                /// <summary>
                /// To string
                /// </summary>
                /// <returns></returns>
                public override string ToString()
                {
                    return string.Format("{0} / {1}", FreqTx.Round(3), FreqRx.Round(3));
                }
                #endregion
            }
            public int Id { get; set; }
            public int OwnerId { get; set; }
            public string TableName;
            //public DateTime Date1;
            //public DateTime Date2;
            //public DateTime Date3;
            public DateTime DateDozvTo;
            public string Caption;
            public string NetName { get; set; }
            public string Standard { get; set; }
            public List<FreqStructure> Freqs { get; set; }

            public BaseStationInfo()
            {
                NetName = "";
                Standard = "";
                Freqs = new List<FreqStructure>();
            }

            public int CompareTo(object obj)
            {
                BaseStationInfo otherBaseStationInfo = obj as BaseStationInfo;
                if (otherBaseStationInfo != null)
                    return this.Id.CompareTo(otherBaseStationInfo.Id);
                else
                    throw new ArgumentException("Object is not BaseStationInfo");
            }

            public override string ToString()
            {
                string retVal = "(С) " + Caption;
                switch (Standard)
                {
                    case CRadioTech.SHR:
                    case CRadioTech.MsR:
                    case CRadioTech.MmR:
                    case CRadioTech.ПРІ:
                        retVal += " (";
                        bool isFirst = true;
                        foreach (FreqStructure freq in Freqs)
                        {
                            if (isFirst == false)
                                retVal += "; ";
                            isFirst = false;
                            retVal += freq.FreqTx.Round(4).ToStringNullD();
                        }
                        retVal += ")";
                        break;
                }
                return retVal;
            }
            /// <summary>
            /// Возвращает название сети к которой привязана станция
            /// </summary>
            /// <returns>название сети к которой привязана станция</returns>
            public static string GetNetOfBaseStation(int id, string table)
            {
                string retVal = "";
                IMRecordset rsNet = new IMRecordset(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadOnly);
                rsNet.Select("ID,Net.NAME");
                rsNet.SetWhere("STATION_ID", IMRecordset.Operation.Eq, id);
                rsNet.SetWhere("STATION_TABLE", IMRecordset.Operation.Eq, table);
                try
                {
                    rsNet.Open();
                    if (!rsNet.IsEOF())
                        retVal = rsNet.GetS("Net.NAME");
                }
                finally
                {
                    if (rsNet.IsOpen())
                        rsNet.Close();
                    rsNet.Destroy();
                }
                return retVal;
            }
            /// <summary>
            /// Возвращает частоты станции
            /// </summary>
            /// <returns>список частот передачи станции</returns>
            public static List<FreqStructure> GetFreqsOfBaseStation(int id, string table)
            {
                MobFreq[] freqs = MobFreqManager.LoadFreq(id, table);
                List<FreqStructure> retVal = new List<FreqStructure>();
                foreach (MobFreq freqItem in freqs)
                {
                    FreqStructure freq = new FreqStructure();
                    freq.FreqRx = freqItem.RxMHz;
                    freq.FreqTx = freqItem.TxMHz;
                    retVal.Add(freq);
                }
                return retVal;
            }
        };

        private int[] arrayFiledsSector;

        protected List<AbonentStation> stationList = new List<AbonentStation>();
        protected new AbonentStation Station { get; set; }
        /// <summary>
        /// Радиотехнология
        /// </summary>
        private CellStringProperty _cellStandart = new CellStringProperty();
        /// <summary>
        /// Власник РЕЗ
        /// </summary>
        private CellStringProperty _cellOwner = new CellStringProperty();
        private List<CellStringProperty> listcellOwner = new List<CellStringProperty>();
        /// <summary>
        /// Код філії
        /// </summary>
        private CellStringProperty _cellNetBrunch = new CellStringProperty();
        /// <summary>
        /// Спосіб експлуатації
        /// </summary>
        private CellStringProperty _cellTypeExpluat = new CellStringProperty();
        /// <summary>
        /// Розташування РЕЗ
        /// </summary>
        private CellStringProperty _cellPosition = new CellStringProperty();
        /// <summary>
        /// Регіон експлуатації
        /// </summary>
        private CellStringProperty _cellRegion = new CellStringProperty();
        /// <summary>
        /// Название оборудования
        /// </summary>
        private CellStringProperty _cellEquipmentName = new CellStringProperty();
        /// <summary>
        /// Мощность
        /// </summary>
        private CellDoubleBoundedProperty _cellPower = new CellDoubleBoundedProperty();
        /// <summary>
        /// Класс излучения
        /// </summary>
        private CellStringProperty _cellDesignEmission = new CellStringProperty();
        /// <summary>
        /// Номер сертификата
        /// </summary>
        private CellStringProperty _cellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертификата
        /// </summary>
        private CellDateProperty _cellCertificateDate = new CellDateProperty();
        /// <summary>
        /// Номер производителя
        /// </summary>
        private CellStringProperty _cellManufactureNumber = new CellStringProperty();
        /// <summary>
        /// Позивной
        /// </summary>
        private CellStringProperty _cellCallSign = new CellStringProperty();
        /// <summary>
        /// Позивной
        /// </summary>
        private CellStringProperty _cellNetCallSign = new CellStringProperty();
        /// <summary>
        /// Номер бланка
        /// </summary>
        private CellStringProperty _cellNumBlank = new CellStringProperty();
        /// <summary>
        /// OBJ1
        /// </summary>
        private CellStringProperty _cellObj1 = new CellStringProperty();
        /// <summary>
        /// Дата дії дозвола
        /// </summary>
        private CellDateProperty _cellPermDate = new CellDateProperty();
        /// <summary>
        /// Ширина смуги
        /// </summary>
        private CellDoubleBoundedProperty _cellBw = new CellDoubleBoundedProperty();
        /// <summary>
        /// Частоты приема
        /// </summary>
        private CellStringProperty _cellFreqRx = new CellStringProperty();
        /// <summary>
        /// Частоты передачи
        /// </summary>
        private CellStringProperty _cellFreqTx = new CellStringProperty();
        // Список допустимих частот
        private Dictionary<string, List<double>> _allowedTxFreq = new Dictionary<string, List<double>>();
        private Dictionary<string, List<double>> _allowedRxFreq = new Dictionary<string, List<double>>();
        //=============================================================
        public override int OwnerID
        {
            get
            {
                return Station.OwnerId;
            }
        }

        //=============================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public AbonentAppl(int id, int ownerId, int packetId, string radioTech, AppType tp)
            : base(id, AbonentStation.TableName, ownerId, packetId, radioTech)
        {
            arrayFiledsSector = new[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16,17,1 };
            appType = tp;
            using (IMRecordset r = new IMRecordset("XNRFA_PACKET", IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,TYPE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, packetId);
                r.Open();
                if (!r.IsEOF())
                {
                    if (!string.IsNullOrEmpty(r.GetS("TYPE")))
                    {
                        if (tp == AppType.AppUnknown) appType = (AppType)Enum.Parse(typeof(AppType), r.GetS("TYPE"));
                    }
                }
                r.Close();
            }
            //if ((tp == AppType.AppUnknown) && (appType== AppType.AppUnknown)) appType = AppType.AppAR_3;
            
            //base.appType = appType;
            department = DepartmentType.VMA;
            departmentSector = DepartmentSectorType.VMA_NULL;
            Station = new AbonentStation();

            if (recID!=0) 
                Station.Load(recID);
            else
                Station.Load(IM.NullI);

            Status.StatusAsString = Station.Status;
            if ((ownerId != 0) && (ownerId != IM.NullI))
                Station.OwnerId = ownerId;

            //---------
            if (newStation)
            {
                //_cellStandart.Value = Station.Standard;
                Station.Standard = Standard;
                //_cellTypeExpluat.Value = Station.Standard;
            }
            //---------
            //_cellStandart.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(СellRadioTech_PropertyChanged);
            _cellTypeExpluat.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(СellTypeExpluat_PropertyChanged);
            _cellFreqRx.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(СellFreqRx_PropertyChanged);
            _cellFreqTx.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(СellFreqTx_PropertyChanged);
            _cellManufactureNumber.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(СellManufactureNumber_PropertyChanged);
            //---------
            _cellBw.LowerBound = 0.0;
            _cellPower.LowerBound = 0.0;
            if ((Station.Equip != null) && (Station.Equip.MaxPower.IsValid))
            {
                _cellPower.UpperBound = Station.Equip.MaxPower[PowerUnits.W];
            }
            CAbonentOwner dAbonentOwner = new CAbonentOwner(); dAbonentOwner.Read(Station.AbonentResId);
            _cellNetBrunch.Value = dAbonentOwner.zsBranchCode;
            InitCells();
            //-----
            SCDozvil = Station.SpecConditionPerm;
            SCVisnovok = Station.SpecConditionConcl;
            SCNote = Station.SpecNote;
            stationList.Add(Station);

            if (newStation)
            {
              
            }
            else
            {
                IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
                r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.XfaAbonentStation);
                r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        for (int i = 2; (i <= 6) && (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI && r.GetI("OBJ_ID" + i.ToString()) > 0); ++i)
                        {
                           RecordPtr sector = new RecordPtr(PlugTbl.XfaAbonentStation, r.GetI("OBJ_ID" + i.ToString()));
                           AbonentStation temp = new AbonentStation();
                           temp.Id = sector.Id;
                           temp.Load();
                           stationList.Add(temp);
                         
                        }
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
        }

      
        //============================================================
        /// <summary>
        /// Initialization for entire grid
        /// It's needed for setup columns for each sector
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Grid grid)
        {
            if (gridParam == null) gridParam = grid;
            if (gridParam != null)
            {
                int columCount = stationList.Count;
                for (int i = 1; i < stationList.Count; i++)
                {
                    AbonentStation Station = stationList[i];
                    ReBuildGrid(grid, -1);

               

                    Cell cell7 = ((Row)gridParam.rowList[arrayFiledsSector[5]]).GetCell(columCount);
                    cell7.Value = Station.Power.dBw_To_W().Round(4).ToString();
                    Cell cell8 = ((Row)gridParam.rowList[arrayFiledsSector[6]]).GetCell(columCount);
                    cell8.Value = Station.Equip.DesigEmission;
                    Cell cell12 = ((Row)gridParam.rowList[arrayFiledsSector[10]]).GetCell(columCount);
                    cell12.Value = Station.Bw.ToString();
                    Cell cell14 = ((Row)gridParam.rowList[arrayFiledsSector[12]]).GetCell((columCount));
                    cell14.Key = "NewKey-FreqRx" + columCount;
                    cell14.AddOnAfterChangeHandler(this, "OnPressButtonEditFreq2");
                    cell14.Value = Station.FreqRx;
                    Cell cell15 = ((Row)gridParam.rowList[arrayFiledsSector[13]]).GetCell((columCount));
                    cell15.Key = "NewKey-FreqTx" + columCount;
                    cell15.AddOnAfterChangeHandler(this, "OnPressButtonEditFreq2");
                    cell15.Value = Station.FreqTx;

                    Cell cell17 = ((Row)gridParam.rowList[arrayFiledsSector[15]]).GetCell(columCount);
                    cell17.Value = Station.Id.ToString();

                    Cell cell18 = ((Row)gridParam.rowList[arrayFiledsSector[16]]).GetCell(columCount);
                    cell18.Value = Station.Standard.ToString();

                    grid.Refresh();
                    grid.Invalidate();

                }
                base.InitParamGrid(grid);
            }
        }

        public void SaveDeletedSectors()
        {
            foreach (RecordPtr rec in DeletedIds)
            {
                int DeletedID = rec.Id;
                IMRecordset rd = new IMRecordset(rec.Table, IMRecordset.Mode.ReadWrite);
                rd.Select("ID,STATUS");
                rd.SetWhere("ID", IMRecordset.Operation.Eq, DeletedID);
                try
                {
                    rd.Open();
                    if (!rd.IsEOF())
                    {
                        rd.Delete();
                    }
                }
                finally
                {
                    rd.Close();
                    rd.Destroy();
                }
            }
        }
        public override void RemoveSector(Grid grid)
        {
            if (stationList.Count > 1)
            {
                HelpClasses.Forms.FSectorRemove form = new XICSM.UcrfRfaNET.HelpClasses.Forms.FSectorRemove();
                for (int i = 1; i <= stationList.Count; ++i)
                    form.cbSecs.Items.Add("Сектор " + i.ToString());
                form.cbSecs.SelectedIndex = 0;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    int index = Convert.ToInt32(form.cbSecs.Text.Substring(7)) - 1;
                    AddDeletedSector(stationList[index].Id, recordID.Table);
                    stationList.RemoveAt(index);
                    AbonentStation.Count--;
                    grid.rowList.Clear();
                    LoadStructureParamGrid(grid);
                    InitParamGrid(grid);
                    UpdateGridParameters(grid);
                }
            }
        }
        public override void AddSector(Grid grid)
        {
            if (ApplType == AppType.AppAP3)
            {
                if (gridParam == null) gridParam = grid;
                int columCount = stationList.Count;
                if (columCount < 2)
                {
                    ReBuildGrid(grid, -1);
                    AbonentStation.Count++;


                    //// копирование с 1-го сектора
                    stationList[columCount - 1].Standard = _cellStandart.Value;
                    stationList[columCount - 1].SetExplType(_cellTypeExpluat.Value);
                    stationList[columCount - 1].SetRegionExpl(_cellRegion.Value);
                    stationList[columCount - 1].PositionRez = _cellPosition.Value;
                    stationList[columCount - 1].Equip.Name = _cellEquipmentName.Value;
                    stationList[columCount - 1].Equip.DesigEmission = _cellEquipmentName.Value;
                    stationList[columCount - 1].Equip.Certificate.Symbol = _cellCertificateNumber.Value;
                    stationList[columCount - 1].Equip.Certificate.Date = _cellCertificateDate.DateValue;
                    stationList[columCount - 1].Power = _cellPower.DoubleValue;
                    stationList[columCount - 1].ManufactureNumber = _cellManufactureNumber.Value;
                    stationList[columCount - 1].CallSign = _cellCallSign.Value;
                    stationList[columCount - 1].NetCallSign = _cellNetCallSign.Value;
                    stationList[columCount - 1].NumBlank = _cellNumBlank.Value;
                    stationList[columCount - 1].PermEndDate = _cellPermDate.DateValue;
                    stationList[columCount - 1].Bw = _cellBw.DoubleValue;
                    CAbonentOwner dAbonentOwner_ = new CAbonentOwner();
                    dAbonentOwner_.Read(stationList[columCount - 1].AbonentResId);
                    stationList[columCount - 1].BrunchCode = dAbonentOwner_.zsBranchCode;


                    ////
                    AbonentStation Station = new AbonentStation();
                    Station.Id = IM.AllocID(PlugTbl.XfaAbonentStation, 1, -1);
                    Station.AbonentResId = stationList[columCount - 1].AbonentResId;
                    Station.AGL = stationList[columCount - 1].AGL;
                    Station.antId = stationList[columCount - 1].antId;
                    Station.ApplId = stationList[columCount - 1].ApplId;
                    Station.Azimuth = stationList[columCount - 1].Azimuth;
                    Station.BrunchCode = stationList[columCount - 1].BrunchCode;
                    Station.Bw = stationList[columCount - 1].Bw;
                    Station.Call = stationList[columCount - 1].Call;
                    Station.CallSign = stationList[columCount - 1].CallSign;
                    Station.equipId = stationList[columCount - 1].Equip.Id;
                    Station.FiderLoss = stationList[columCount - 1].FiderLoss;
                    Station.Finding = stationList[columCount - 1].Finding;
                    Station.ManufactureNumber = stationList[columCount - 1].ManufactureNumber;
                    Station.Name = stationList[columCount - 1].Name;
                    Station.NetCallSign = stationList[columCount - 1].NetCallSign;
                    Station.NumBlank = stationList[columCount - 1].NumBlank;
                    Station.OwnerId = stationList[columCount - 1].OwnerId;
                    Station.PermDatePrint = stationList[columCount - 1].PermDatePrint;
                    Station.PermEndDate = stationList[columCount - 1].PermEndDate;
                    Station.PermNum = stationList[columCount - 1].PermNum;
                    Station.posId = stationList[columCount - 1].posId;
                    Station.PositionRez = stationList[columCount - 1].PositionRez;
                    Station.Power = stationList[columCount - 1].Power;
                    Station.SpecConditionConcl = stationList[columCount - 1].SpecConditionConcl;
                    Station.Standard = stationList[columCount - 1].Standard;
                    Station.StatComment = stationList[columCount - 1].StatComment;
                    Station.Status = stationList[columCount - 1].Status;
                    Station.SetRegionExpl(stationList[columCount - 1].RegionExpl);
                    Station.SetExplType(stationList[columCount - 1].ExplAbonent.ToString());




                    if (stationList[columCount - 1].Position != null)
                    {
                        RecordPtr pos = new RecordPtr(PlugTbl.XfaAbonentStation, stationList[columCount - 1].Position.Id);
                        PositionState pos_R = new PositionState();
                        pos_R.Id = stationList[columCount - 1].posId;
                        pos_R.TableName = PlugTbl.XfaAbonentStation;
                        pos_R.LoadStatePosition(pos.Id, pos.Table);
                        Station.Position = pos_R;
                    }

                    if (stationList[columCount - 1].Equip != null)
                    {
                        RecordPtr eqp = new RecordPtr(PlugTbl.XfaAbonentStation, stationList[columCount - 1].equipId);
                        AbonentEquipment eqp_R = new AbonentEquipment();
                        eqp_R.Id = stationList[columCount - 1].Equip.Id;
                        eqp_R.TableName = PlugTbl.XfaAbonentStation;
                        eqp_R.Load();
                        Station.Equip = eqp_R;
                    }

                    if (stationList[columCount - 1].Anten != null)
                    {
                        RecordPtr ant = new RecordPtr(PlugTbl.XfaAbonentStation, stationList[columCount - 1].antId);
                        AbonentAntenna ant_R = new AbonentAntenna();
                        ant_R.Id = ant.Id;
                        ant_R.TableName = PlugTbl.XfaAbonentStation;
                        ant_R.Load();
                        Station.Anten = ant_R;
                    }


                    Cell cell7 = ((Row)gridParam.rowList[arrayFiledsSector[5]]).GetCell((columCount + 1));
                    cell7.Key = "NewKey-Power" + (columCount + 1).ToString();
                    cell7.Value = Station.Equip.MaxPower[PowerUnits.W].Round(4).ToString();
                    Cell cell8 = ((Row)gridParam.rowList[arrayFiledsSector[6]]).GetCell((columCount + 1));
                    cell8.Key = "NewKey-KTxEmi" + (columCount + 1).ToString();
                    cell8.Value = Station.Equip.DesigEmission;
                    Cell cell12 = ((Row)gridParam.rowList[arrayFiledsSector[10]]).GetCell((columCount + 1));
                    cell12.Key = "NewKey-BW" + (columCount + 1).ToString();
                    cell12.Value = Station.Equip.BandWidth.ToString();
                    Cell cell14 = ((Row)gridParam.rowList[arrayFiledsSector[12]]).GetCell((columCount + 1));
                    cell14.Key = "NewKey-FreqRx" + (columCount + 1).ToString();
                    cell14.AddOnAfterChangeHandler(this, "OnPressButtonEditFreq2");
                    cell14.Value = Station.FreqRx;
                    Cell cell15 = ((Row)gridParam.rowList[arrayFiledsSector[13]]).GetCell((columCount + 1));
                    cell15.Key = "NewKey-FreqTx" + (columCount + 1).ToString();
                    cell15.AddOnAfterChangeHandler(this, "OnPressButtonEditFreq2");
                    cell15.Value = Station.FreqTx;

                    Cell cell17 = ((Row)gridParam.rowList[arrayFiledsSector[15]]).GetCell((columCount + 1));
                    cell17.Key = "KObj1" + (columCount + 1).ToString();
                    cell17.Value = Station.Id.ToString();


                    Cell cell18 = ((Row)gridParam.rowList[arrayFiledsSector[16]]).GetCell((columCount + 1));
                    cell18.Key = "NewKey-Standard" + (columCount + 1).ToString();
                    cell18.Value = Station.Standard;


                    stationList.Add(Station);
                    grid.Refresh();
                    grid.Invalidate();

                }
            }
        }


        private void ReBuildGrid(Grid grid, int cnt)
        {
            
            for (int i = 0; i < arrayFiledsSector.Length; i++)
            {
                if ((i == 5) || (i == 6) || (i == 10) || (i == 12) || (i == 13) || (i == 15) || (i == 16))
                {

                        Row row = grid.GetRow(arrayFiledsSector[i]) as Row;
                        if (row != null) row.grid = grid;
                        grid.rowList.RemoveAt(arrayFiledsSector[i]);
                        grid.rowList.Insert(arrayFiledsSector[i], row);
                        if (row != null)
                        {

                            Cell originalCell = row.GetCell(1);

                            if (i == 16)
                            {
                                Cell cell = row.AddCell(originalCell.Value);
                                cell.HorizontalAlignment = HorizontalAlignment.Center;
                                cell.Value2 = "";
                                if (appType == AppType.AppAP3)
                                    cell.cellStyle = EditStyle.esPickList;
                                cell.KeyPickList = new KeyedPickList();
                                Dictionary<string, string> dictStandard = EriFiles.GetEriCodeAndDescr("TRFAAppl_AP_3");
                                foreach (KeyValuePair<string, string> pair in dictStandard)
                                    cell.KeyPickList.AddToPickList(pair.Key, pair.Value);
                                //cell.Value = ((int)Station.ExplAbonent).ToString();
                                cell.CanEdit = false;
                                cell.FontName = originalCell.FontName;
                                cell.FontStyle = originalCell.FontStyle;
                                cell.TextHeight = originalCell.TextHeight;
                                cell.Value = "";
                                cell.CanEdit = originalCell.CanEdit;
                            }
                            else
                            {
                               

                                Cell cell = row.AddCell(originalCell.Value);
                                cell.HorizontalAlignment = HorizontalAlignment.Center;
                                cell.Value2 = "";
                                cell.CanEdit = false;
                                cell.cellStyle = originalCell.cellStyle;
                                cell.FontName = originalCell.FontName;
                                cell.FontStyle = originalCell.FontStyle;
                                cell.TextHeight = originalCell.TextHeight;
                                cell.Value = "";
                                cell.CanEdit = originalCell.CanEdit;
                            }

                        }
                }
            }


            grid.Focus();
            grid.Invalidate();
        }

        /// <summary>
        /// Инициализация ячеек
        /// </summary>
        /// 
        private void InitCells()
        {

            _cellStandart.Value = Station.GetHumanStandard();
            _cellOwner.Value = Station.GetNameAbonentRes();
            _cellTypeExpluat.Value = ((int)Station.ExplAbonent).ToString();
            _cellRegion.Value = Station.RegionExpl;
            _cellPosition.Value = Station.PositionRez;
            _cellEquipmentName.Value = Station.Equip.Name;
            _cellDesignEmission.Value = Station.Equip.DesigEmission;
            _cellCertificateNumber.Value = Station.Equip.Certificate.Symbol;
            _cellCertificateDate.DateValue = Station.Equip.Certificate.Date;
            _cellPower.DoubleValue = Station.Power.dBw_To_W().Round(4);
            _cellManufactureNumber.Value = Station.ManufactureNumber;
            _cellCallSign.Value = Station.CallSign;
            _cellNetCallSign.Value = Station.NetCallSign;
            _cellNumBlank.Value = Station.NumBlank;
            _cellObj1.Value = Station.Id.ToString();
            _isUpdatingFreq = true;
            _cellFreqTx.Value = Station.FreqTx;
            _cellFreqRx.Value = Station.FreqRx;
            _isUpdatingFreq = false;
            _cellPermDate.DateValue = Station.PermEndDate;
            _cellBw.DoubleValue = Station.Bw;
            CAbonentOwner dAbonentOwner = new CAbonentOwner(); dAbonentOwner.Read(Station.AbonentResId);
            _cellNetBrunch.Value = dAbonentOwner.zsBranchCode;
           
        }

        public override void UpdateToNewPacket()
        {
            //throw new NotImplementedException();
        }


        public void GetData(Grid grid)
        {

             int columCount = stationList.Count;
             for (int i = 1; i < stationList.Count; i++)
             {

                 Cell cell7 = ((Row)gridParam.rowList[arrayFiledsSector[5]]).GetCell(columCount);
                 CellValidate(cell7, grid); 
                 double res=IM.NullD;
                 if (double.TryParse(cell7.Value, out res))
                     stationList[i].Power = res;


                 Cell cell8 = ((Row)gridParam.rowList[arrayFiledsSector[6]]).GetCell((columCount));
                 CellValidate(cell8, grid);
                 cell8.Value = cell8.Value;
                 Cell cell12 = ((Row)gridParam.rowList[arrayFiledsSector[10]]).GetCell(columCount);
                 CellValidate(cell12, grid);
                 double res_bw = IM.NullD;
                 if (double.TryParse(cell12.Value, out res_bw))
                     stationList[i].Bw = res_bw;
                               
                 Cell cell14 = ((Row)gridParam.rowList[arrayFiledsSector[12]]).GetCell((columCount));
                 CellValidate(cell14, grid);
                 stationList[i].FreqRx = cell14.Value;

                 Cell cell15 = ((Row)gridParam.rowList[arrayFiledsSector[13]]).GetCell((columCount));
                 CellValidate(cell15, grid);
                 stationList[i].FreqTx = cell15.Value;

                 Cell cell17 = ((Row)gridParam.rowList[arrayFiledsSector[15]]).GetCell(columCount);
                 CellValidate(cell17, grid);
                 int rs_w = IM.NullI;
                 if (int.TryParse(cell17.Value, out rs_w))
                     stationList[i].Id = rs_w;


                 Cell cell18 = ((Row)gridParam.rowList[arrayFiledsSector[16]]).GetCell(columCount);
                 CellValidate(cell18, grid);
                 stationList[i].Standard  = cell18.Value;
             }
            
        }

        protected override bool SaveAppl()
        {
            AbonentStation.StatusCheckDublicate = false;
            Station.BrunchCode = _cellNetBrunch.Value;
            Station.Status = Status.StatusAsString;
            Station.SetExplType(_cellTypeExpluat.Value);
            Station.SetRegionExpl(_cellRegion.Value);
            Station.PositionRez = _cellPosition.Value;
            Station.Power = _cellPower.DoubleValue.W_To_dBW();
            Station.ManufactureNumber = _cellManufactureNumber.Value;
            Station.CallSign = _cellCallSign.Value;
            Station.FreqTx = _cellFreqTx.Value;
            Station.FreqRx = _cellFreqRx.Value;
            Station.PermEndDate = _cellPermDate.DateValue;
            Station.Bw = _cellBw.DoubleValue;
            Station.NumBlank = _cellNumBlank.Value;
            Station.SpecConditionPerm = SCDozvil;
            Station.SpecConditionConcl = SCVisnovok;
            Station.SpecNote = SCNote;
            if (_cellStandart.Value == "Аналоговий короткохвильовий радіозв'язок")  {  Station.Standard = "КХ"; }
            else if (_cellStandart.Value == "Аналоговий ультракороткохвильовий радіотелефонний зв'язок") { Station.Standard = "УКХ"; }
            //======
            if ((_allowedTxFreq != null) && (_allowedTxFreq.ContainsKey(_cellFreqTx.Value)) &&
                (_allowedRxFreq != null) && (_allowedRxFreq.ContainsKey(_cellFreqRx.Value)))
            {
                List<double> freqRx = _allowedRxFreq[_cellFreqRx.Value];
                List<double> freqTx = _allowedTxFreq[_cellFreqTx.Value];
                Station.Freqs.Clear();
                int indexMax = (freqTx.Count > freqRx.Count)
                                   ? freqRx.Count
                                   : freqTx.Count;
                for (int i = 0; i < indexMax; i++)
                {
                    AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                    newFreq.FreqRx = freqRx[i];
                    newFreq.FreqTx = freqTx[i];
                    Station.Freqs.Freqs.Add(newFreq);
                }
            }
            //======
            if (Station.Id == IM.NullI){int max_ID = IM.AllocID(PlugTbl.XfaAbonentStation, 1, -1); Station.Id = max_ID; }
            Station.Save();
            AbonentStation.StatusCheckDublicate = true;

            foreach (RecordPtr rec in DeletedIds)
            {
                int DeletedID = rec.Id;
                IMRecordset r1 = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                r1.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                r1.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
                r1.Select("CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
                r1.SetWhere("OBJ_ID2", IMRecordset.Operation.Eq, DeletedID);
                r1.Open();

                if (!r1.IsEOF())
                {
                        r1.Edit();
                        r1.Put("OBJ_ID2", IM.NullI);
                        r1.Update();
                }
                r1.Close();
                r1.Destroy();

            
            }

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
            r.Select("CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
            r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
            r.Open();

            if (!r.IsEOF())
            {
                if (Station.Id > 0)
                {
                    r.Edit();
                    string ObjIdName = "OBJ_ID1";
                    r.Put(ObjIdName, Station.Id);
                    r.Update();
                }
            }
            r.Close();
            r.Destroy();
            
            GetData(gridParam);
            for (int u = 1; u < stationList.Count(); u++)
            {
                if ((_allowedTxFreq != null) && (_allowedTxFreq.ContainsKey(stationList[u].FreqTx)) &&
                (_allowedRxFreq != null) && (_allowedRxFreq.ContainsKey(stationList[u].FreqRx)))
                {
                    List<double> freqRx = _allowedRxFreq[stationList[u].FreqRx];
                    List<double> freqTx = _allowedTxFreq[stationList[u].FreqTx];
                    stationList[u].Freqs.Clear();
                    int indexMax = (freqTx.Count > freqRx.Count)
                                       ? freqRx.Count
                                       : freqTx.Count;
                    for (int i = 0; i < indexMax; i++)
                    {
                        AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                        newFreq.FreqRx = freqRx[i];
                        newFreq.FreqTx = freqTx[i];
                        stationList[u].Freqs.Freqs.Add(newFreq);
                    }
                }
                if (stationList[u].Id == IM.NullI) { int max_ID = IM.AllocID(PlugTbl.XfaAbonentStation, 1, -1); stationList[u].Id = max_ID;  }
                stationList[u].Power = stationList[u].Power.W_To_dBW();
                stationList[u].Save();

                r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                r.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
                r.Select("CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
                r.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                r.Open();

                if (!r.IsEOF())
                {
                    if (stationList[u].Id > 0)
                    {
                        r.Edit();
                        string ObjIdName = "OBJ_ID" + (u + 1).ToString();
                        r.Put(ObjIdName, stationList[u].Id);
                        r.Update();
                    }
                }
                r.Close();
                r.Destroy();

            }
            SaveDeletedSectors();
            return true;
        }
        //===========================================================
        /// <summary>
        /// Отобразить обьект
        /// </summary>
        /// <param name="cell">ячейка</param>
        protected override void ShowObject(Cell cell)
        {
            switch(cell.Key)
            {
                case "NewKey-Owner":
                    using(FAbonentOwner form = new FAbonentOwner(Station.AbonentResId))
                        form.ShowDialog();
                    break;
                case "NewKey-KNameRez":
                    using (AbonentEquipForm frm = new AbonentEquipForm(Station.Equip.Id))
                        if (frm.ShowDialog() == DialogResult.OK)
                            frm.Save();                    
                    break;
                case "NewKey-Position":
                    if (Station.objPosition != null)
                    {
                        int posID = Station.objPosition.Id;
                        Forms.AdminSiteAllTech2.Show(Station.objPosition.TableName, posID, (IM.TableRight(Station.objPosition.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                        Station.objPosition.LoadStatePosition(Station.objPosition.Id, Station.objPosition.TableName);
                        Station.objPosition.GetAdminSiteInfo(Station.objPosition.AdminSiteId);
                        _cellPosition.Value = Station.objPosition.FullAddressAuto;
                        _cellRegion.Value = Station.objPosition.Province;
                    }
                    else
                        ShowMessageNoReference();
                    break;
            }
            
        }
        /// <summary>
        /// Генерирует название документа
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="number">Номер документа</param>
        /// <returns>Название документа</returns>
        protected override string getOutDocName(string docType, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.GetListDocNumberForDozvWma(1)[0];

            if ((docType == DocType.DOZV) || (docType == DocType.DOZV_FOREIGHT) || (docType == DocType.DOZV_TRAIN ) || (docType == DocType.LYST_TR_CALLSIGN))
            {
                string codeProv = "";
                string[] provList = _cellRegion.Value.Split(';');
                if (provList.Length > 1)
                {
                    using (FProvChoose form = new FProvChoose())
                    {
                        form.ProvinceList.Add("");
                        foreach (string province in provList)
                        {
                            form.ProvinceList.Add(province.Trim());
                        }
                        form.ShowDialog();
                        string selectedProv = form.SelectedProvince;
                        if (string.IsNullOrEmpty(selectedProv))
                            codeProv = "99";
                        else
                            codeProv = HelpFunction.getAreaCode(selectedProv, selectedProv);
                    }
                }
                else if (provList.Length == 1)
                {
                    string selectedProv = provList[0].Trim();
                    codeProv = HelpFunction.getAreaCode(selectedProv, selectedProv);
                }
                string prefix = Standard == CRadioTech.RBSS ? "МР" : "АР";
                fullPath += prefix + "-" + codeProv + "-" + number + ".rtf";
            }
            else if (docType == DocType.DOZV_AR3_SPEC)
            {
                string codeProv = "";
                string[] provList = _cellRegion.Value.Split(';');
                if (provList.Length > 1)
                {
                    using (FProvChoose form = new FProvChoose())
                    {
                        form.ProvinceList.Add("");
                        foreach (string province in provList)
                        {
                            form.ProvinceList.Add(province.Trim());
                        }
                        form.ShowDialog();
                        string selectedProv = form.SelectedProvince;
                        if (string.IsNullOrEmpty(selectedProv))
                            codeProv = "99";
                        else
                            codeProv = HelpFunction.getAreaCode(selectedProv, selectedProv);
                    }
                }
                else if (provList.Length == 1)
                {
                    string selectedProv = provList[0].Trim();
                    codeProv = HelpFunction.getAreaCode(selectedProv, selectedProv);
                }
                string prefix = "СТД";
                fullPath += prefix + "-" + codeProv + "-" + (number.Length<6 ? number.PadRight(6,'0') : number) +  ".rtf";
            }
            else
                throw new IMException("Error document type");
            return fullPath;
        }
        //============================================================
        /// <summary>
        /// Изменяем владельца РЧП (Использовать только из пакета)
        /// </summary>
        /// <param name="newOwnerId">ID нового владельца</param>
        public override void ChangeOwner(int newOwnerId)
        {
            if (Station.OwnerId != newOwnerId)
            {
                Station.OwnerId = newOwnerId;

                if (Station.Id == IM.NullI) { int max_ID = IM.AllocID(PlugTbl.XfaAbonentStation, 1, -1); Station.Id = max_ID; }
                Station.Save();
            }
        }
        //============================================================
        /// <summary>
        /// Возвращает дополнительный фильтр для IRP файла
        /// </summary>
        protected override string getDopDilter()
        {
            string retVal = "";
            if (Standard == CRadioTech.SB)
                retVal += "SB";
            if (Station.NetId != IM.NullI)
                retVal += "BASE";
            return retVal;
        }



        /// <summary>
        /// Инициализация грида
        /// </summary>
        /// <param name="cell">ячейка</param>
        public override void InitParamGrid(Cell cell)
        {
            if (cell == null)
                return;

            switch (cell.Key)
            {
                case "NewKey-FreqRx":
                    switch (Standard)
                    {
                        case CRadioTech.BCUR:
                        case CRadioTech.SB:
                        case CRadioTech.BAUR:
                        case CRadioTech.SptRZ:
                        case CRadioTech.RPATL:
                            _allowedRxFreq = GetAllowedFreq(Standard, false);
                            cell.PickList = new List<string>();
                            foreach (KeyValuePair<string, List<double>> pair in _allowedRxFreq)
                            {
                                cell.PickList.Add(pair.Key);
                            }
                            _cellFreqRx.CanEdit = false;
                            if (cell.PickList.Count == 1)
                                _cellFreqRx.Value = cell.PickList[0];
                            else
                                cell.cellStyle = EditStyle.esPickList;
                            break;
                        default:
                            cell.CanEdit = ((Station.NetId == IM.NullI) || (Standard == CRadioTech.ПРІ) || (Standard == CRadioTech.KX) || (Standard == CRadioTech.RRK));
                            cell.cellStyle = (Station.NetId == IM.NullI) ? EditStyle.esSimple : EditStyle.esEllipsis;
                            break;
                    }
                    break;
                case "NewKey-FreqTx":
                    switch (Standard)
                    {
                        case CRadioTech.BCUR:
                        case CRadioTech.SB:
                        case CRadioTech.BAUR:
                        case CRadioTech.SptRZ:
                        case CRadioTech.RPATL:
                            _allowedTxFreq = GetAllowedFreq(Standard, true);
                            cell.PickList = new List<string>();
                            foreach (KeyValuePair<string, List<double>> pair in _allowedTxFreq)
                            {
                                cell.PickList.Add(pair.Key);
                            }
                            _cellFreqTx.CanEdit = false;
                            if (cell.PickList.Count == 1)
                                _cellFreqTx.Value = cell.PickList[0];
                            else
                                cell.cellStyle = EditStyle.esPickList;
                            break;
                        default:
                            cell.CanEdit = ((Station.NetId == IM.NullI) || (Standard == CRadioTech.KX) || (Standard == CRadioTech.ПРІ) || (Standard == CRadioTech.RRK));
                            break;
                    }
                    break;
                case "NewKey-PermDate":
                    _cellPermDate.Cell.cellStyle = (CanSelectBaseStation()) ? EditStyle.esEllipsis : EditStyle.esSimple;
                    break;
                case "NewKey-Expluat":
                    if (_cellTypeExpluat.Cell != null)
                    {
                        _cellTypeExpluat.Cell.KeyPickList = new KeyedPickList();
                        Dictionary<string, string> dictExplType = EriFiles.GetEriCodeAndDescr("TRFAExplType");
                        foreach (KeyValuePair<string, string> pair in dictExplType)
                            _cellTypeExpluat.Cell.KeyPickList.AddToPickList(pair.Key, pair.Value);
                        _cellTypeExpluat.Value = ((int)Station.ExplAbonent).ToString();
                    }
                    break;
                case "NewKey-Standard":
                    if (_cellStandart.Cell != null)
                    {
                        if (appType== AppType.AppAP3)
                            _cellStandart.Cell.cellStyle = EditStyle.esPickList;
                        _cellStandart.Cell.KeyPickList = new KeyedPickList();
                        Dictionary<string, string> dictStandard = EriFiles.GetEriCodeAndDescr("TRFAAppl_AP_3");
                        foreach (KeyValuePair<string, string> pair in dictStandard)
                            _cellStandart.Cell.KeyPickList.AddToPickList(pair.Key, pair.Value);
                        _cellStandart.Value = Station.GetHumanStandard();
                    }
                 
                    break;
            }
        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.AbonentAppl;
        }
        //===========================================================
        /// <summary>
        /// Обновление данных поля РДПО владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerRDPO(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,REGIST_NUM");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, Station.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("REGIST_NUM");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }
        //===========================================================
        /// <summary>
        /// Обновление данных поля Названия владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerName(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,NAME");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, Station.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("NAME");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }
        //===========================================================
        /// <summary>
        /// Созадем такующе заявк
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass CreateStation()
        {
            AbonentAppl retStation = new AbonentAppl(0, OwnerID, packetID, radioTech, appTp);
            return retStation;
        }
        //===========================================================
        /// <summary>
        /// Дублирует станцию
        /// </summary>
        /// <returns>Новую заявку</returns>
        public override BaseAppClass DuplicateStation(Grid grid, BaseAppClass newAppl)
        {
            AbonentAppl retStation = newAppl as AbonentAppl;
            if (retStation == null)
                return newAppl;
            retStation.Station = Station.GetDuplicate();
            retStation.Station.Id = retStation.recID;
            retStation.Station.Freqs.ClearId();
            retStation.InitCells();
            retStation.InitLicenceGrid();
            return retStation;
        }
        /// <summary>
        /// Печать документа из заявки
        /// </summary>
        public override void PrintDoc()
        {
            // Проверка ни зменения статуса заявки.
            if (gridParam.Edited || IsChangeDop)
                this.OnSaveAppl();

            if ((applID == 0) || (applID == IM.NullI))
            {
                MessageBox.Show(CLocaliz.TxT("Can't create document because this station doesn't have relation with application."), CLocaliz.TxT("Creating document"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr(recordID.Table, recordID.Id), (IsTechnicalUser == 1), applID, appType,  false);
            if (frmReport.ShowDialog() == DialogResult.OK)
            {
                SaveFileNumber(frmReport.FileNumber, recordID.Table, recordID.Id);                 
                DateTime startDate = frmReport.getStartDate();
                DateTime endDate = frmReport.getEndDate();
                string docType = frmReport.getDocType();
                if (docType == DocType.DOZV_CANCEL)
                    endDate = IM.NullT;
                try
                {
                    string docName = getOutDocName(docType, "");
                    string dopFilter = getDopDilter();
                    SaveBeforeDocPrint(docType, startDate, endDate, docName, dopFilter);
                    PrintDocs printDocs = new PrintDocs();
                    if (printDocs.CreateOneReport(docType, appType, startDate, endDate, PlugTbl.itblXnrfaAppl, applID, dopFilter, docName, true))
                    {
                        SaveAfterDocPrint(frmReport);
                        UpdateEventGrid();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
            
        //============================================================
        /// <summary>
        /// Сохранить измения после печати документа
        /// </summary>
        protected override void SaveAfterDocPrint(FormSelectOutDoc report)
        {
            Station.NumBlank = report.FileNumber.ToString();
            if (Station.Id == IM.NullI) { int max_ID = IM.AllocID(PlugTbl.XfaAbonentStation, 1, -1); Station.Id = max_ID; }
            Station.Save();
            _cellNumBlank.Value = Station.NumBlank;
        }
        //===========================================================
        /// <summary>
        /// Возвращет все данные для заявки в ДРВ от УРЧМ
        /// </summary>
        public static int GetDataForURCM(int ID, ref string adress)
        {
            IMRecordset rs = new IMRecordset(AbonentStation.TableName, IMRecordset.Mode.ReadOnly);
            int count = 0;
            try
            {
                adress = "";
                rs.Select("ID,POS_ID,Position.REMARK,USE_REGION");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rs.Open();
                if (!rs.IsEOF())
                {
                    if (rs.GetI("POS_ID") != IM.NullI)
                        adress = rs.GetS("Position.REMARK");
                    else
                        adress = rs.GetS("USE_REGION");
                    count = 1;
                }
            }
            finally
            {
                if(rs.IsOpen())
                    rs.Close();
                rs.Destroy();
            }
            return count;
        }
        /// <summary>
        /// Проверяет на возможность редактировать Дату разрешения
        /// </summary>
        /// <returns>TRUE - можна, иначе НЕТ</returns>
        private bool CanSelectBaseStation()
        {
            switch (Standard)
            {
                case CRadioTech.ЦУКХ:
                case CRadioTech.YKX:
                case CRadioTech.PD:
                case CRadioTech.TRUNK:
                case CRadioTech.TETRA:
                case CRadioTech.SHR:
                case CRadioTech.MsR:
                case CRadioTech.MmR:
                case CRadioTech.RUZO:
                case CRadioTech.ROPS:
                case CRadioTech.RRK:
                case CRadioTech.RBSS:
                case CRadioTech.KX:
                case CRadioTech.ПРІ:

                    return true;
            }
            return false;
        }

        #region OnPressButton
        /// <summary>
        /// Выбор нового владельца
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonOwner(Cell cell)
        {

            string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(_cellOwner.Value) + "*\"}";
            RecordPtr recOwner = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the owner"), PlugTbl.XvAbonentOwner, param);
            if ((recOwner.Id > 0) && (recOwner.Id < IM.NullI))
            {
               
                if (cell.Key.Contains("NewKey-Owner"))
                {

                    if (cell.Key =="NewKey-Owner")
                    {
                        Station.AbonentResId = recOwner.Id;
                        if (stationList.Count>1)
                            stationList[1].AbonentResId = recOwner.Id;
                        _cellOwner.Value = Station.GetNameAbonentRes();
                        CAbonentOwner CA = new CAbonentOwner();
                        CA.Read(Station.AbonentResId);
                        _cellNetBrunch.Value = CA.zsBranchCode;         
                    }
                    AutoFill(cell, gridParam);
                }
              
               //int id = 1;
               //Cell cellDes = ((Row)gridParam.rowList[arrayFiledsSector[1]]).GetCell(id);
               //cellDes.Value = listcellOwner[id - 1].Value;
            }
            
        
             
        }

        /// <summary>
        /// Выбор позиции
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonPosition(Cell cell)
        {
            if (_cellTypeExpluat.Value != ((int)AbonentStation.TypeExplAbonent.Stationary).ToString())
            {
                MessageBox.Show(CLocaliz.TxT("In order to set position set the exploitation type to 'fixed'"), "",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;  //Для нестационарнных рез не выбераем позиию
            }

            if (ShowMessageReference(Station.objPosition != null))
            {
                PositionState2 newPos = new PositionState2();
                DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(AbonentStation.TablePosition, ref newPos, OwnerWindows);
                if (dr == DialogResult.OK)
                {
                   

                    if (cell.Key.Contains("NewKey-Position"))
                    {
                        if (cell.Key=="NewKey-Position")
                        {
                            Station.objPosition = newPos;
                            Station.objPosition.GetAdminSiteInfo(Station.objPosition.AdminSiteId);
                            _cellPosition.Value = Station.objPosition.FullAddressAuto;
                            _cellRegion.Value = Station.objPosition.Province;
                            if (stationList.Count > 1)
                            {
                                stationList[1].objPosition = newPos;
                                stationList[1].objPosition.GetAdminSiteInfo(stationList[1].objPosition.AdminSiteId);
                            }
                        }
                        AutoFill(cell, gridParam);
                    }
                   
                }
            }                        
        }
        /// <summary>
        /// Выбор Региона
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonRegion(Cell cell)
        {
            //============
            if (cell.Key == "NewKey-Region")
            {
                if (_cellTypeExpluat.Value == ((int)AbonentStation.TypeExplAbonent.Stationary).ToString())
                {
                    MessageBox.Show(CLocaliz.TxT("For selection the region set the exploitation type to not stationary"), "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return; //Для стационарнных рез не выбераем регион
                }

                using (MaintenanceRegion maintenanceRegionForm = new MaintenanceRegion(_cellRegion.Value))
                {
                    if (maintenanceRegionForm.ShowDialog() == DialogResult.OK)
                    {
                        _cellRegion.Value = maintenanceRegionForm.RegionTextBox.Text;
                         if (stationList.Count > 1)
                               stationList[1].SetRegionExpl(maintenanceRegionForm.RegionTextBox.Text);
                        AutoFill(cell, gridParam);

                    }
                }
            }
        }
        /// <summary>
        /// Выбор оборудования
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonEquipmentName(Cell cell)
        {
            string tableEquip = PlugTbl.XfaEquip;
            IMTableRight rights = IM.TableRight(tableEquip);
            if (!(((rights & IMTableRight.Insert) == IMTableRight.Insert) &&
                 ((rights & IMTableRight.Update) == IMTableRight.Update)))
                tableEquip = PlugTbl.XvEquip;

            if (cell.Key == "NewKey-KNameRez")
            {
                RecordPtr recEquip = SelectEquip("Підбір обладнання", tableEquip, "NAME", _cellEquipmentName.Value, true);
                if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
                {

                    if (cell.Key.Contains("NewKey-KNameRez"))
                    {
                        if (cell.Key == "NewKey-KNameRez")
                        {
                            Station.Equip.Id = recEquip.Id;
                            Station.Equip.Load();
                            _cellEquipmentName.Value = Station.Equip.Name;
                            _cellDesignEmission.Value = Station.Equip.DesigEmission;
                            _cellCertificateNumber.Value = Station.Equip.Certificate.Symbol;
                            _cellCertificateDate.DateValue = Station.Equip.Certificate.Date;
                            _cellPower.DoubleValue = Station.Equip.MaxPower[PowerUnits.W];
                            _cellPower.UpperBound = _cellPower.DoubleValue;
                            _cellBw.DoubleValue = Station.Equip.BandWidth;
                            if (stationList.Count > 1)
                            {
                                stationList[1].Equip.Id = recEquip.Id;
                                stationList[1].Equip.Load();
                                Cell cell8 = ((Row)gridParam.rowList[arrayFiledsSector[6]]).GetCell(2);
                                cell8.Value = stationList[1].Equip.DesigEmission;
                                Cell cell7 = ((Row)gridParam.rowList[arrayFiledsSector[5]]).GetCell(2);
                                cell7.Value = stationList[1].Equip.MaxPower[PowerUnits.W].ToStringNullD();
                                Cell cell12 = ((Row)gridParam.rowList[arrayFiledsSector[10]]).GetCell(2);
                                cell12.Value = stationList[1].Equip.BandWidth.ToStringNullD();

                            }
                        }
                        AutoFill(cell, gridParam);
                    }
                }
            }
           
        }
        /// <summary>
        /// Выбор БС
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonPermDate(Cell cell)
        {
            //if (cell.Key == "NewKey-PermDate2")
            //{
                //Cell cell11 = ((Row)gridParam.rowList[arrayFiledsSector[9]]).GetCell((2));
                //cell11.cellStyle = (CanSelectBaseStation()) ? EditStyle.esEllipsis : EditStyle.esSimple;
            //}
            //else
            //{
                SelectExpireDate();
            //}
              
        }
        /// <summary>
        /// Выбор БС
        /// </summary>
        /// <param name="cell">Ячейка</param>
        public void OnPressButtonEditFreq(Cell cell)
        {
            if (Standard == CRadioTech.MsR)
            {
                EditFreqsMcR();
            }
            else
            {
                EditFreqs();
            }
        }

       
        #endregion

        #region PropertyChanged
        /// <summary>
        /// Изменилась заводской номер
        /// </summary>
        void СellManufactureNumber_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                IMRecordset rs = new IMRecordset(AbonentStation.TableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("ID");
                    rs.SetWhere("FACTORY_NUM", IMRecordset.Operation.Like, _cellManufactureNumber.Value);
                    rs.SetWhere("ID", IMRecordset.Operation.Neq, Station.Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        _cellManufactureNumber.BackColor = Color.PeachPuff;
                    }
                    else
                    {
                        _cellManufactureNumber.BackColor =
                            CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);
                    }
                }
                finally
                {
                    rs.Final();
                }
            }
        }

        public void CellValidateManufNumber(Cell cell)
        {
            Cell cell10 = ((Row)gridParam.rowList[arrayFiledsSector[8]]).GetCell(2);
                IMRecordset rs = new IMRecordset(AbonentStation.TableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("ID");
                    rs.SetWhere("FACTORY_NUM", IMRecordset.Operation.Like, cell10.Value);
                    rs.SetWhere("ID", IMRecordset.Operation.Neq, stationList[1].Id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        cell10.BackColor = Color.PeachPuff;
                    }
                    else
                    {
                        cell10.BackColor =
                            CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);
                    }
                }
                finally
                {
                    rs.Final();
                }
         
        }

        /*
        void СellRadioTech_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                if (_cellStandart.Value == "Аналоговий короткохвильовий радіозв'язок")
                {
                    Station.Standard = "КХ";
                    
                }
                else if (_cellStandart.Value == "Аналоговий ультракороткохвильовий радіотелефонний зв'язок")
                {
                    Station.Standard = "УКХ";
                }
                else
                {
                    _cellStandart.Value = Station.GetHumanStandard();
                }
            }
        }
         */ 

        /// <summary>
        /// Изменилась Региона экспл
        /// </summary>
        void СellTypeExpluat_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                if (_cellTypeExpluat.Value != "")
                {
                    Station.SetExplType(_cellTypeExpluat.Value);
                    if (stationList.Count > 1)
                    {
                        stationList[1].SetExplType(_cellTypeExpluat.Value);
                    }
                }
                    switch (Standard)
                    {
                        case CRadioTech.RPATL:
                            _cellFreqRx.Value = "";
                            if (_cellFreqRx.Cell != null)
                                InitParamGrid(_cellFreqRx.Cell);
                            _cellFreqTx.Value = "";
                            if (_cellFreqTx.Cell != null)
                                InitParamGrid(_cellFreqTx.Cell);
                            break;
                    }
                    //#5062
                    if (Station.ExplAbonent == AbonentStation.TypeExplAbonent.Portable)
                    {
                        if ((_cellTypeExpluat.Cell != null) && (_cellTypeExpluat.Cell.KeyPickList != null))
                        {
                            _cellPosition.Value = _cellTypeExpluat.Cell.KeyPickList.GetValueByKey(((int)Station.ExplAbonent).ToString());

                            if (stationList.Count > 1)
                            {
                                stationList[1].PositionRez = _cellPosition.Value;
                            }
                        }
                    
                }
            }
        }

     
        /// <summary>
        /// Изменилась частота приема
        /// </summary>
        public void СellFreqRx_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                switch (Standard)
                {
                    case CRadioTech.RPATL:
                    case CRadioTech.SB:
                    case CRadioTech.BAUR:
                    case CRadioTech.SptRZ:
                    case CRadioTech.BCUR:
                        if ((_allowedRxFreq != null) && (_allowedTxFreq != null) &&
                           (_cellFreqTx.Cell != null) && (_allowedRxFreq.ContainsKey(_cellFreqRx.Value)) && (_cellFreqTx.Cell.PickList != null))
                        {
                            int index = _allowedRxFreq.Keys.ToList().IndexOf(_cellFreqRx.Value);
                            if((index > -1) && (index < _cellFreqTx.Cell.PickList.Count))
                                _cellFreqTx.Value = _cellFreqTx.Cell.PickList[index];
                        }
                        break;
                    default:
                        UpdateFreqs();
                        InitParamGrid(_cellFreqRx.Cell);
                        //UpdateFreqs2();
                        break;
                }
            }
        }

        public void OnPressButtonEditFreq2(Cell cell)
        {
            UpdateFreqs2();
        }
        /// <summary>
        /// Изменилась частота приема
        /// </summary>
        public void СellFreqTx_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                switch (Standard)
                {
                    case CRadioTech.RPATL:
                    case CRadioTech.SB:
                    case CRadioTech.BAUR:
                    case CRadioTech.SptRZ:
                    case CRadioTech.BCUR:
                        if ((_allowedRxFreq != null) && (_allowedTxFreq != null) &&
                           (_cellFreqRx.Cell != null) && (_allowedTxFreq.ContainsKey(_cellFreqTx.Value)) && (_cellFreqRx.Cell.PickList != null))
                        {
                            int index = _allowedTxFreq.Keys.ToList().IndexOf(_cellFreqTx.Value);
                            if ((index > -1) && (index < _cellFreqRx.Cell.PickList.Count))
                                _cellFreqRx.Value = _cellFreqRx.Cell.PickList[index];
                        }
                        break;
                    default:
                        UpdateFreqs();
                        InitParamGrid(_cellFreqTx.Cell);
                        //UpdateFreqs2();
                        break;
                }
            }
        }

        bool _isUpdatingFreq = false; //Флаг, для предотвращения зацыкливания
        bool _isUpdatingFreq2 = false; //Флаг, для предотвращения зацыкливания
        /// <summary>
        /// Изменились частоты
        /// </summary>
        private void UpdateFreqs()
        {
            if((_isUpdatingFreq) || (Station.NetId != IM.NullI))
                return;
            _isUpdatingFreq = true;
            bool isBadTx = false;
            bool isBadRx = false;
            double[] freqsTx = _cellFreqTx.Value.ToDoubleArray(';');
            double[] freqsRx = _cellFreqRx.Value.ToDoubleArray(';');
            if(freqsRx.Length != freqsTx.Length)
            {
                isBadTx = true;
                isBadRx = true;
            }
            int maxLength = (freqsRx.Length > freqsTx.Length) ? freqsRx.Length : freqsTx.Length;
            Station.Freqs.Clear();
            for (int i = 0; i < maxLength; i++)
            {
                double freqTx = (i < freqsTx.Length) ? freqsTx[i] : IM.NullD;
                double freqRx = (i < freqsRx.Length) ? freqsRx[i] : IM.NullD;
                AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                newFreq.FreqRx = freqRx;
                newFreq.FreqTx = freqTx; 
                Station.Freqs.Freqs.Add(newFreq);
            }
            _cellFreqTx.Value = freqsTx.ToStringWithSeparator(";", 4);
            _cellFreqRx.Value = freqsRx.ToStringWithSeparator(";", 4);
            _cellFreqTx.BackColor = isBadTx
                                        ? CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.BadColor)
                                        : CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);
            _cellFreqRx.BackColor = isBadRx
                                        ? CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.BadColor)
                                        : CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);
            _isUpdatingFreq = false;
        }

        private void UpdateFreqs2()
        {
            if ((_isUpdatingFreq2) || (Station.NetId != IM.NullI))
                return;
            _isUpdatingFreq2 = true;
            bool isBadTx = false;
            bool isBadRx = false;
            Cell cell14 = ((Row)gridParam.rowList[arrayFiledsSector[12]]).GetCell((2));
            Cell cell15 = ((Row)gridParam.rowList[arrayFiledsSector[13]]).GetCell((2));

            double[] freqsTx = cell15.Value.ToDoubleArray(';');
            double[] freqsRx = cell14.Value.ToDoubleArray(';');
            if (freqsRx.Length != freqsTx.Length)
            {
                isBadTx = true;
                isBadRx = true;
            }
            int maxLength = (freqsRx.Length > freqsTx.Length) ? freqsRx.Length : freqsTx.Length;
            stationList[1].Freqs.Clear();
            for (int i = 0; i < maxLength; i++)
            {
                double freqTx = (i < freqsTx.Length) ? freqsTx[i] : IM.NullD;
                double freqRx = (i < freqsRx.Length) ? freqsRx[i] : IM.NullD;
                AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                newFreq.FreqRx = freqRx;
                newFreq.FreqTx = freqTx;
                stationList[1].Freqs.Freqs.Add(newFreq);
            }

            

            cell14.Value = freqsRx.ToStringWithSeparator(";", 4); 
            cell14.BackColor = isBadRx
                                        ? CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.BadColor)
                                        : CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);

            
            cell15.Value = freqsTx.ToStringWithSeparator(";", 4);
            cell15.BackColor = isBadTx
                                        ? CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.BadColor)
                                        : CellStringProperty.ColorBgr(CellStringProperty.ColorIndex.Default);
         
            _isUpdatingFreq2 = false;
        }

        #endregion

        //============================================================
        private string GetTableNameByStandard(string standard)
        {
            Dictionary<string, string> dc = new Dictionary<string, string>();

            dc.Add(CRadioTech.ЦУКХ, "MOB_STATION");
            dc.Add(CRadioTech.KX, "MOB_STATION");
            dc.Add(CRadioTech.YKX, "MOB_STATION");
            dc.Add(CRadioTech.ПРІ, "MOB_STATION");
            dc.Add(CRadioTech.PD, "MOB_STATION");
            dc.Add(CRadioTech.TRUNK, "MOB_STATION");
            dc.Add(CRadioTech.TETRA, "MOB_STATION");
            dc.Add(CRadioTech.SHR, "MOB_STATION2");
            dc.Add(CRadioTech.MsR, "MOB_STATION2");
            dc.Add(CRadioTech.MmR, "MOB_STATION2");
            dc.Add(CRadioTech.RUZO, "MOB_STATION");
            dc.Add(CRadioTech.ROPS, "MOB_STATION");
            dc.Add(CRadioTech.RRK, "MOB_STATION");
            dc.Add(CRadioTech.RBSS, "MOB_STATION");

            if (dc.ContainsKey(standard))
                return dc[standard];
            else
                return "";
        }
        //==========================================================
        private Dictionary<string, List<double>> GetAllowedFreq(string standard, bool isTxFreq)
        {
            Dictionary<string, List<double>> retDict = new Dictionary<string, List<double>>();
            switch (standard)
            {
                case CRadioTech.BCUR:
                    {
                        List<double> lstFreq = new List<double> {446.1, 446.2};
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    break;
                case CRadioTech.SB:
                    {
                        List<double> lstFreq = new List<double> {26.965, 27.405};
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    break;
                case CRadioTech.BAUR:
                    {
                        List<double> lstFreq = new List<double> {446.0, 446.1};
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> {446.3, 446.4};
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    break;
                case CRadioTech.SptRZ:
                    if (isTxFreq)
                    {
                        List<double> lstFreq = new List<double> {1626.5, 1660.5};
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> {1610, 1626.5};
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> {1621.35, 1626.5};
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> { 1621.5, 1626.5 };
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    else
                    {
                        List<double> lstFreq = new List<double> {1525, 1559};
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> {2483.5, 2500};
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> {1621.35, 1626.5};
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> { 1626.5, 1660.5 };
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double> { 1621.5, 1626.5 };
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    break;
                case CRadioTech.RPATL:
                    if (isTxFreq)
                    {
                        bool isStationary = Station.ExplAbonent == AbonentStation.TypeExplAbonent.Stationary;
                        List<double> lstFreq = new List<double>();
                        if (isStationary)
                        {
                            lstFreq.Add(379.5);
                            lstFreq.Add(380.5);
                        }
                        else
                        {
                            lstFreq.Add(253.5);
                            lstFreq.Add(254.5);
                        }
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double>();
                        if (isStationary)
                        {
                            lstFreq.Add(393.95);
                            lstFreq.Add(394.95);
                        }
                        else
                        {
                            lstFreq.Add(263.95);
                            lstFreq.Add(264.95);
                        }
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    else
                    {
                        bool isStationary = Station.ExplAbonent == AbonentStation.TypeExplAbonent.Stationary;
                        List<double> lstFreq = new List<double>();
                        if (isStationary)
                        {
                            lstFreq.Add(253.5);
                            lstFreq.Add(254.5);
                        }
                        else
                        {
                            lstFreq.Add(379.5);
                            lstFreq.Add(380.5);
                        }
                        string code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                        lstFreq = new List<double>();
                        if (isStationary)
                        {
                            lstFreq.Add(263.95);
                            lstFreq.Add(264.95);
                        }
                        else
                        {
                            lstFreq.Add(393.95);
                            lstFreq.Add(394.95);
                        }
                        code = string.Format("{0} - {1}", lstFreq.Min(), lstFreq.Max());
                        retDict.Add(code, lstFreq);
                    }
                    break;
            }
            return retDict;
        }

        public void OnBeforeDate(Cell cell, ref string newV)
        {
            DateTime newVal;
            if (!DateTime.TryParse(newV, out newVal))
                cell.BackColor = Color.Pink;
            else
                if (newVal < DateTime.Now)
                    cell.BackColor = Color.Pink;
                else
                    cell.BackColor = Color.White;
        }

        //==========================================================
        private void SelectExpireDate()
        {
            int localOwnerId = OwnerID;
            {
                // Выбераем нового владельца
                string ownerName = "";
                using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID,NAME");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, localOwnerId);
                    rs.Open();
                    if (!rs.IsEOF())
                        ownerName = rs.GetS("NAME");
                }
                string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(ownerName) + "*\"}";
                RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Seaching of an owner"), ICSMTbl.itblUsers, param);
                if (user.Id != IM.NullI)
                    localOwnerId = user.Id;
            }

            List<AuxiliaryNet> lstNet = new List<AuxiliaryNet>();
            List<BaseStationInfo> lstBaseStation = new List<BaseStationInfo>();

            //исправление #7437

            string region = _cellRegion.Value;
            bool restrictByReg = false;

            CustomDLG dlg = new CustomDLG("Виводити список БС по вказаних регіонах (" + region + ") або по ВСІХ регіонах України?" + Environment.NewLine + Environment.NewLine
              + "\tТАК\t- тільки " + region + Environment.NewLine
              + "\tНІ\t- ВСІ регіони", CLocaliz.TxT("Question"));

            if ((!string.IsNullOrEmpty(region) && region.ToLower().Contains("україна") == false))
            {
                if (dlg.GetStatusPress() == DialogResultMessageBoxButtons.OK)
                {
                    restrictByReg = true;
                }
            }

           
            using (LisProgressBar pb = new LisProgressBar("Пошук БС"))
            {
                pb.SetBig("Завантаження БС...");
                pb.SetProgress(0, 100);
                //---
                //Формируем список всех БС
                using (Icsm.LisRecordSet recMobSta2 = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                {
                    string tableName = GetTableNameByStandard(Standard);
                    string refer = (tableName == ICSMTbl.itblMobStation) ? "MobileSector1." : "BaseSector1.";
                    string refer2 = (tableName == ICSMTbl.itblMobStation) ? "MobileSector2." : "BaseSector2.";
                    string refer3 = (tableName == ICSMTbl.itblMobStation) ? "MobileSector3." : "BaseSector3.";
                    string refer4 = (tableName == ICSMTbl.itblMobStation) ? "MobileSector4." : "BaseSector4.";
                    string refer5 = (tableName == ICSMTbl.itblMobStation) ? "MobileSector5." : "BaseSector5.";
                    string refer6 = (tableName == ICSMTbl.itblMobStation) ? "MobileSector6." : "BaseSector6.";
                    recMobSta2.Select("ID");
                    recMobSta2.Select("DOZV_NUM");
                    recMobSta2.Select("DOZV_DATE_TO");
                    recMobSta2.Select(refer + "ID");
                    recMobSta2.Select(refer2 + "ID");
                    recMobSta2.Select(refer3 + "ID");
                    recMobSta2.Select(refer4 + "ID");
                    recMobSta2.Select(refer5 + "ID");
                    recMobSta2.Select(refer6 + "ID");
                    recMobSta2.Select(refer + "STANDARD");
                    recMobSta2.Select(refer + "OWNER_ID");
                    recMobSta2.Select(refer + "Position.PROVINCE");
                    recMobSta2.Select(refer + "STATUS");
                    recMobSta2.SetWhere(refer + "STANDARD", IMRecordset.Operation.Like, Standard);
                    recMobSta2.SetWhere(refer + "OWNER_ID", IMRecordset.Operation.Eq, localOwnerId);
                    recMobSta2.OrderBy("DOZV_NUM", OrderDirection.Ascending);
                    for (recMobSta2.Open(); !recMobSta2.IsEOF(); recMobSta2.MoveNext())
                    {
                        if (StationsStatus.StatusDivision.CanUseForAbonentStation(recMobSta2.GetS(refer + "STATUS")) == false)
                            continue; //Отбрасываем ненужные станции

                        pb.Increment(true);

                        if (restrictByReg)
                        {
                            string curRegion = recMobSta2.GetS(refer + "Position.PROVINCE");
                            if (string.IsNullOrEmpty(curRegion))
                                curRegion = "empty";
                            if (region.Contains(curRegion) == false)
                                continue;
                        }

                        DateTime date = recMobSta2.GetT("DOZV_DATE_TO");
                        string eXpire = recMobSta2.GetS("DOZV_NUM");
                        string localStandard = recMobSta2.GetS(refer + "STANDARD");
                        int localOwnerIdStation = recMobSta2.GetI(refer + "OWNER_ID");
                        if ((date != IM.NullT) && (!string.IsNullOrEmpty(eXpire)))
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                string refStr = refer;
                                switch(i)
                                {
                                    case 0: refStr = refer;
                                        break;
                                    case 1: refStr = refer2;
                                        break;
                                    case 2: refStr = refer3;
                                        break;
                                    case 3: refStr = refer4;
                                        break;
                                    case 4: refStr = refer5;
                                        break;
                                    case 5: refStr = refer6;
                                        break;
                                }
                                int localStationId = recMobSta2.GetI(refStr + "ID");
                                if(localStationId == IM.NullI)
                                    continue;

                                BaseStationInfo baseStationInfo = new BaseStationInfo();
                                baseStationInfo.Id = localStationId;
                                baseStationInfo.OwnerId = localOwnerIdStation;
                                baseStationInfo.Standard = localStandard;
                                baseStationInfo.DateDozvTo = date;
                                baseStationInfo.Caption = eXpire + " #" + recMobSta2.GetI("ID");
                                baseStationInfo.TableName = tableName;
                                baseStationInfo.NetName = BaseStationInfo.GetNetOfBaseStation(baseStationInfo.Id,
                                                                                              baseStationInfo.TableName);
                                List<BaseStationInfo.FreqStructure> freqsLst =
                                    BaseStationInfo.GetFreqsOfBaseStation(baseStationInfo.Id, baseStationInfo.TableName);
                                baseStationInfo.Freqs = freqsLst.Distinct().ToList();
                                lstBaseStation.Add(baseStationInfo);
                                if ((localStandard != CRadioTech.SHR) && (localStandard != CRadioTech.MmR) && (localStandard != CRadioTech.MsR))
                                    break;
                            }
                        }
                    }
                }
                //---
                // Формируем список сетей
                pb.SetBig("Завантаження мереж...");
                {
                    HashSet<int> addedIdNet = new HashSet<int>();
                    using (Icsm.LisRecordSet rsNet = new Icsm.LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadOnly))
                    {
                        rsNet.Select("ID,Net.ID,Net.NAME,Net.CALL_SIGN,BaseMobStation.ID,BaseMobStation2.ID");
                        rsNet.SetWhere("Net.NAME", IMRecordset.Operation.NotEmpty, "");
                        rsNet.OrderBy("Net.NAME", OrderDirection.Ascending);
                        for (rsNet.Open(); !rsNet.IsEOF(); rsNet.MoveNext())
                        {
                            int idNet = rsNet.GetI("Net.ID");
                            if (addedIdNet.Contains(idNet))
                                continue;
                            pb.Increment(true);
                            int idBase1 = rsNet.GetI("BaseMobStation.ID");
                            int idBase2 = rsNet.GetI("BaseMobStation2.ID");
                            foreach (BaseStationInfo baseStation in lstBaseStation)
                            {
                                if ((idBase1 == baseStation.Id) || (idBase2 == baseStation.Id))
                                {
                                    addedIdNet.Add(idNet);
                                    AuxiliaryNet net = new AuxiliaryNet();
                                    net.Id = idNet;
                                    net.Name = rsNet.GetS("Net.NAME");
                                    net.CallSign = rsNet.GetS("Net.CALL_SIGN");
                                    lstNet.Add(net);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //---
            // Отображаем окно выбора сети
            FChannels fChannel = new FChannels(CLocaliz.TxT("Select the base stations are connected"), CLocaliz.TxT("Permit number BS / network name"));
            // Добавляем сети
            foreach (AuxiliaryNet net in lstNet)
            {
                if(Station.NetId == net.Id)
                    fChannel.CLBSelectChannel.Items.Add(net);
                else
                    fChannel.CLBAllChannel.Items.Add(net);
            }
            //Добавляем БС
            foreach (BaseStationInfo baseStation in lstBaseStation)
            {
                fChannel.CLBAllChannel.Items.Add(baseStation);
            }

            bool isRepeated = false;
            do
            {
                isRepeated = false;
                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    int netId = IM.NullI;
                    if (fChannel.CLBSelectChannel.Items.Count == 1)
                    {
                        AuxiliaryNet net = fChannel.CLBSelectChannel.Items[0] as AuxiliaryNet;
                        if(net != null)
                        {
                            netId = net.Id;
                        }
                        else
                        {
                            BaseStationInfo baseStationInfo = fChannel.CLBSelectChannel.Items[0] as BaseStationInfo;
                            if (baseStationInfo != null)
                                netId = CreateNet(new List<BaseStationInfo> { baseStationInfo }.ToArray(), localOwnerId);
                        }
                        if (netId == IM.NullI)
                        {
                            isRepeated = true; //По новому кругу
                            continue;
                        }
                    }
                    else if (fChannel.CLBSelectChannel.Items.Count > 1)
                    {
                        List<BaseStationInfo> lstAddBase = new List<BaseStationInfo>();
                        int countNets = 0;
                        foreach (object selectedItem in fChannel.CLBSelectChannel.Items)
                        {
                            AuxiliaryNet net = selectedItem as AuxiliaryNet;
                            if (net != null)
                            {
                                countNets++;
                                continue;
                            }
                            BaseStationInfo baseStation = selectedItem as BaseStationInfo;
                            if (baseStation != null)
                            {
                                lstAddBase.Add(baseStation);
                                continue;
                            }
                        }
                        if(countNets > 1)
                        {
                            MessageBox.Show("Вибрано більше ніж одну мережу");
                            isRepeated = true; //По новому кругу
                            continue;
                        }
                        if((countNets != fChannel.CLBSelectChannel.Items.Count) && (countNets > 0))
                        {
                            MessageBox.Show("Заборонено вибір мережі та БС одночасно");
                            isRepeated = true; //По новому кругу
                            continue;
                        }
                        //-----
                        // Створюємо нову мережу
                        netId = CreateNet(lstAddBase.ToArray(), localOwnerId);
                        if (netId == IM.NullI)
                        {
                            isRepeated = true; //По новому кругу
                            continue;
                        }
                    }
                    Station.NetId = netId;
                    Station.Freqs.Clear();
                    BaseStationInfo.FreqStructure sortFreq = new BaseStationInfo.FreqStructure();
                    List<BaseStationInfo.FreqStructure> distinctedFreqs = GetFreqsBaseStation().Distinct(sortFreq).ToList();
                    foreach (BaseStationInfo.FreqStructure freq in distinctedFreqs)
                    {
                        AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                        newFreq.FreqRx = freq.FreqTx;
                        newFreq.FreqTx = freq.FreqRx;
                        Station.Freqs.Freqs.Add(newFreq);
                    }
                    _cellPermDate.DateValue = IM.NullT;
                    DateTime[] endDateDozvList = Station.GetEndPermDateBaseStation();
                    if (endDateDozvList.Length > 0)
                        _cellPermDate.DateValue = endDateDozvList.Min();
                    _cellBw.DoubleValue = IM.NullD;
                    if ((Station.Equip != null) && (Station.Equip.BandWidth != IM.NullD))
                        _cellBw.DoubleValue = (Station.Freqs.GteUniceFreqs().Length * Station.Equip.BandWidth).Round(3);
                    _isUpdatingFreq = true;
                    _cellFreqTx.Value = Station.Freqs.GetFreqTx();
                    _cellFreqRx.Value = Station.Freqs.GetFreqRx();
                    _isUpdatingFreq = false;
                    _cellNetCallSign.Value = AbonentStation.GetCallSignNet(Station.NetId);
                    UpdatePermisions();
                }
            } while (isRepeated);
        }
        /// <summary>
        /// Создает сеть базовых станций
        /// </summary>
        /// <param name="arrayBaseStation">Список БС</param>
        /// <param name="ownerId">ID владельца</param>
        /// <returns>ID новой сети</returns>
        private int CreateNet(BaseStationInfo[] arrayBaseStation, int ownerId)
        {
            int retNetId = IM.NullI;
            IMDialog dialogNewNet = new IMDialog();
            dialogNewNet.Add("txtNetName", "", "", CLocaliz.TxT("Net name"));
            dialogNewNet.Add("txtCallName", "", "", CLocaliz.TxT("Call name"));
            if (dialogNewNet.Enter() == false)
                return retNetId;

            string nameNet = dialogNewNet.Get("txtNetName") as string;
            string netCallSign = dialogNewNet.Get("txtCallName") as string;
            {
                bool isPresent = false;
                //Проверка на дублирование сети
                using (Icsm.LisRecordSet rsNet = new Icsm.LisRecordSet(PlugTbl.XfaNet, IMRecordset.Mode.ReadOnly))
                {
                    rsNet.Select("ID,NAME");
                    rsNet.SetWhere("NAME", IMRecordset.Operation.Like, nameNet);
                    rsNet.Open();
                    if (!rsNet.IsEOF())
                        isPresent = true;
                }
                if(isPresent && (MessageBox.Show(string.Format("Мережа з іменем '{0}' вже існує. Продовжити створення мережі?", nameNet), "", MessageBoxButtons.YesNo) != DialogResult.Yes))
                    return IM.NullI;
            }
            {
                //Создаем новую сеть
                IMRecordset rsNet = new IMRecordset(PlugTbl.XfaNet, IMRecordset.Mode.ReadWrite);
                rsNet.Select("ID,NAME,CALL_SIGN,DATE_CREATED,CREATED_BY,OWNER_ID");
                rsNet.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                try
                {
                    rsNet.Open();
                    rsNet.AddNew();
                    retNetId = IM.AllocID(PlugTbl.XfaNet, 1, -1);
                    rsNet.Put("ID", retNetId);
                    rsNet.Put("NAME", nameNet);
                    rsNet.Put("OWNER_ID", ownerId);
                    rsNet.Put("CALL_SIGN", netCallSign);
                    rsNet.Put("DATE_CREATED", DateTime.Now);
                    rsNet.Put("CREATED_BY", IM.ConnectedUser());
                    rsNet.Update();
                }
                finally
                {
                    if (rsNet.IsOpen())
                        rsNet.Close();
                    rsNet.Destroy();
                }
                //Добавляем БС в сеть
                IMRecordset rsBaseStations = new IMRecordset(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite);
                rsBaseStations.Select("ID,NET_ID,STATION_ID,STATION_TABLE");
                rsBaseStations.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                try
                {
                    rsBaseStations.Open();
                    foreach (BaseStationInfo baseStationInfo in arrayBaseStation)
                    {
                        rsBaseStations.AddNew();
                        int id = IM.AllocID(PlugTbl.XfaRefBaseStation, 1, -1);
                        rsBaseStations.Put("ID", id);
                        rsBaseStations.Put("NET_ID", retNetId);
                        rsBaseStations.Put("STATION_ID", baseStationInfo.Id);
                        rsBaseStations.Put("STATION_TABLE", baseStationInfo.TableName);
                        rsBaseStations.Update();
                    }
                }
                finally
                {
                    if (rsBaseStations.IsOpen())
                        rsBaseStations.Close();
                    rsBaseStations.Destroy();
                }
            }
            return retNetId;
        }


        /// <summary>
        /// Редактирование списка частот
        /// </summary>
        private void EditFreqsMcR()
        {

            using (FChannels fChannel = new FChannels(CLocaliz.TxT("Select the frequency"), CLocaliz.TxT("Permit number BS / network name")))
            {
                BaseStationInfo.FreqStructure sortFreq = new BaseStationInfo.FreqStructure();
                List<BaseStationInfo.FreqStructure> freqsBase = GetFreqsBaseStation().Distinct(sortFreq).ToList();
                //-----
                foreach (BaseStationInfo.FreqStructure freq in freqsBase)
                {

                    AbonentFreqs.Freq newFreq = null;
                    if ((Convert.ToInt32(freq.FreqTx) > 0) && (Convert.ToInt32(freq.FreqRx) > 0))
                    {
                        newFreq = new AbonentFreqs.Freq();
                        newFreq.FreqRx = freq.FreqTx;
                        newFreq.FreqTx = IM.NullD;


                        if (Station.Freqs.Freqs.Contains(newFreq))
                            fChannel.CLBSelectChannel.Items.Add(Station.Freqs.Freqs[Station.Freqs.Freqs.IndexOf(newFreq)]);
                        else
                            fChannel.CLBAllChannel.Items.Add(newFreq);

                        if (Convert.ToInt32(freq.FreqRx) > 0)
                        {
                            newFreq = null;
                            newFreq = new AbonentFreqs.Freq();
                            newFreq.FreqRx = IM.NullD;
                            newFreq.FreqTx = freq.FreqRx;

                            if (Station.Freqs.Freqs.Contains(newFreq))
                                fChannel.CLBSelectChannel.Items.Add(Station.Freqs.Freqs[Station.Freqs.Freqs.IndexOf(newFreq)]);
                            else
                                fChannel.CLBAllChannel.Items.Add(newFreq);

                        }

                    }
                    else
                    {

                        if (((freq.FreqTx > 0) && ((freq.FreqRx == 0) || (freq.FreqRx == IM.NullD))) || (((freq.FreqTx == 0) || (freq.FreqTx == IM.NullD)) && (freq.FreqRx > 0)))
                        {
                            newFreq = null;
                            newFreq = new AbonentFreqs.Freq();
                            newFreq.FreqRx = freq.FreqTx;
                            newFreq.FreqTx = freq.FreqRx;


                            if (Station.Freqs.Freqs.Contains(newFreq))
                                fChannel.CLBSelectChannel.Items.Add(Station.Freqs.Freqs[Station.Freqs.Freqs.IndexOf(newFreq)]);
                            else
                                fChannel.CLBAllChannel.Items.Add(newFreq);

                        }
                    }


                }
                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    Station.Freqs.Clear();
                    foreach (object selectedItem in fChannel.CLBSelectChannel.Items)
                    {
                        AbonentFreqs.Freq freq = selectedItem as AbonentFreqs.Freq;
                        if (freq == null)
                            continue;
                        Station.Freqs.Freqs.Add(freq);
                    }
                    if ((Station.Equip != null) && (Station.Equip.BandWidth != IM.NullD))
                        _cellBw.DoubleValue = (Station.Freqs.Freqs.Count * Station.Equip.BandWidth).Round(3);
                    _isUpdatingFreq = true;
                    _cellFreqTx.Value = Station.Freqs.GetFreqTx();
                    _cellFreqRx.Value = Station.Freqs.GetFreqRx();
                    _isUpdatingFreq = false;
                }
            }
        }


        /// <summary>
        /// Редактирование списка частот
        /// </summary>
        private void EditFreqs()
        {

            using (FChannels fChannel = new FChannels(CLocaliz.TxT("Select the frequency"), CLocaliz.TxT("Permit number BS / network name")))
            {
                BaseStationInfo.FreqStructure sortFreq = new BaseStationInfo.FreqStructure();
                List<BaseStationInfo.FreqStructure> freqsBase = GetFreqsBaseStation().Distinct(sortFreq).ToList();
                //-----
                foreach (BaseStationInfo.FreqStructure freq in freqsBase)
                {
                    AbonentFreqs.Freq newFreq = new AbonentFreqs.Freq();
                    newFreq.FreqRx = freq.FreqTx;
                    newFreq.FreqTx = freq.FreqRx;

                    if (Station.Freqs.Freqs.Contains(newFreq))
                        fChannel.CLBSelectChannel.Items.Add(Station.Freqs.Freqs[Station.Freqs.Freqs.IndexOf(newFreq)]);
                    else
                        fChannel.CLBAllChannel.Items.Add(newFreq);
                }
                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    Station.Freqs.Clear();
                    foreach (object selectedItem in fChannel.CLBSelectChannel.Items)
                    {
                        AbonentFreqs.Freq freq = selectedItem as AbonentFreqs.Freq;
                        if (freq == null)
                            continue;
                        Station.Freqs.Freqs.Add(freq);
                    }
                    if ((Station.Equip != null) && (Station.Equip.BandWidth != IM.NullD))
                        _cellBw.DoubleValue = (Station.Freqs.Freqs.Count * Station.Equip.BandWidth).Round(3);
                    _isUpdatingFreq = true;
                    _cellFreqTx.Value = Station.Freqs.GetFreqTx();
                    _cellFreqRx.Value = Station.Freqs.GetFreqRx();
                    _isUpdatingFreq = false;
                }
            }
        }
        /// <summary>
        /// Возвращает список частот базовых станций
        /// </summary>
        /// <returns></returns>
        private List<BaseStationInfo.FreqStructure> GetFreqsBaseStation()
        {
            List<BaseStationInfo.FreqStructure> freqsBase = new List<BaseStationInfo.FreqStructure>();
            {
                IMRecordset rsBaseStations = new IMRecordset(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadOnly);
                rsBaseStations.Select("ID,NET_ID,STATION_ID,STATION_TABLE");
                rsBaseStations.SetWhere("NET_ID", IMRecordset.Operation.Eq, Station.NetId);
                try
                {
                    for (rsBaseStations.Open(); !rsBaseStations.IsEOF(); rsBaseStations.MoveNext())
                    {
                        int id = rsBaseStations.GetI("STATION_ID");
                        string table = rsBaseStations.GetS("STATION_TABLE");
                        freqsBase.AddRange(BaseStationInfo.GetFreqsOfBaseStation(id, table));
                    }
                }
                finally
                {
                    if (rsBaseStations.IsOpen())
                        rsBaseStations.Close();
                    rsBaseStations.Destroy();
                }
            }
            return freqsBase;
        }
        /// <summary>
        /// Обновляет список дозволов
        /// </summary>
        private void UpdatePermisions()
        {
            gridLicence.Rows.Clear();
            IMRecordset rsBaseStations = new IMRecordset(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadOnly);
            rsBaseStations.Select("ID,NET_ID,STATION_ID,STATION_TABLE");
            rsBaseStations.SetWhere("NET_ID", IMRecordset.Operation.Eq, Station.NetId);
            try
            {
                for (rsBaseStations.Open(); !rsBaseStations.IsEOF(); rsBaseStations.MoveNext())
                {
                    int id = rsBaseStations.GetI("STATION_ID");
                    string table = rsBaseStations.GetS("STATION_TABLE");

                    IMRecordset recMobSta2 = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
                    recMobSta2.Select("ID,CUST_DAT5,CUST_TXT5");
                    recMobSta2.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    try
                    {
                        recMobSta2.Open();
                        if (!recMobSta2.IsEOF())
                        {
                            DateTime date = recMobSta2.GetT("CUST_DAT5");
                            string eXpire = recMobSta2.GetS("CUST_TXT5");
                            string[] row = new string[] { recMobSta2.GetI("ID").ToString(),
                                                   table,
                                                   eXpire,
                                                   date.ToString("dd.MM.yyyy"),
                                           };
                            gridLicence.Rows.Add(row);
                        }
                    }
                    finally
                    {
                        if (recMobSta2.IsOpen())
                            recMobSta2.Close();
                        recMobSta2.Destroy();
                    }
                }
            }
            finally
            {
                if (rsBaseStations.IsOpen())
                    rsBaseStations.Close();
                rsBaseStations.Destroy();
            }
        }


        #region LICENCE GRID
        //============================================================
        /// <summary>
        /// Инициализация грида лицензий
        /// </summary>
        /// <param name="gridLic">Грид</param>
        public override void InitLicenceGrid(DataGridView gridLic)
        {
            gridLicence = gridLic;
            // ID
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "lisID";
            column.HeaderText = CLocaliz.TxT("ID");
            column.Width = 50;
            column.Visible = false;
            gridLicence.Columns.Add(column);
            // NAME
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisTableName";
            column.HeaderText = "TableName";
            column.Width = 100;
            column.Visible = false;
            gridLicence.Columns.Add(column);
            // NAME
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisNAME";
            column.HeaderText = CLocaliz.TxT("Permision number");
            column.Width = 100;
            gridLicence.Columns.Add(column);
            // STOP_DATE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisSTOP_DATE";
            column.HeaderText = CLocaliz.TxT("STOP_DATE");
            column.Width = 100;
            gridLicence.Columns.Add(column);
            InitLicenceGrid();
        }
        //============================================================
        /// <summary>
        /// Загрузка грида лицензий
        /// </summary>
        protected override void InitLicenceGrid()
        {
            UpdatePermisions();
        }
        //===================================================
        /// <summary>
        /// Отображаем лицензию
        /// </summary>
        public override void ShowLicence(int row, int column)
        {
            if (row >= 0)
            {
                int id = gridLicence.Rows[row].Cells[0].Value.ToString().ToInt32(IM.NullI);
                string table = gridLicence.Rows[row].Cells[1].Value.ToString();
                if (id != IM.NullI)
                {
                    BaseAppClass obj = BaseAppClass.GetStation(id, table, 0, false);
                    obj.ReadOnly = true; //Не смотрим на права доступа
                    using (MainAppForm formAppl = new MainAppForm(obj))
                    {
                        formAppl.ShowDialog();
                    }
                }
            }
        }


        public override string GetProvince()
        {
            return "";
        }

        #endregion
    }
}
