﻿using System;
using System.Collections.Generic;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class RailwayApplStation : BaseStation
    {
        public RailAntenna RailStationAntena { get; set; }
        public RailEquipment RailStationEquip { get; set; }

        public int PosId { get; set; }

        public PositionState2 objPosition = null;

        public string Standart { get; set; }
        public string Status { get; set; }
        public string Defin { get; set; }
        public string ClassStat { get; set; }

        public string NameRez { get; set; }
        
        private Power power = new Power();
        public Power Power { get { return power; } }

        public string DesigEmiss { get; set; }
        public string CertNumb { get; set; }
        public DateTime CertDate { get; set; }
        public string TypeAnten { get; set; }
        public string BrunchCode { get; set; }
        public double Koef { get; set; }
        public double WideAnten { get; set; }
        public double Agl { get; set; }
        public double Azimuth { get; set; }
        public double Fider { get; set; }
        public string Call { get; set; }
        public double Duplex { get; set; }
        public double Accept { get; set; }
        public double Send { get; set; }
        public int ObjRchp { get; set; }
        /// <summary>
        /// Номер бланка
        /// </summary>
        public string NumBlank { get; set; }
        
        public string NAccept { get; set; }
        public string NSend { get; set; }

        public List<double> FreqRxList = new List<double>();
        public List<int> RxListId = new List<int>();
        public List<double> FreqTxList = new List<double>();
        public List<int> TxListId = new List<int>();

        public List<string> StandartList=new List<string>();

        public Dictionary<string, string> ClassStationDict = new Dictionary<string, string>();

        public int AntId { get; set; }
        public int EquipId { get; set; }
        public int OwnerId { get; set; }
        public int UserId { get; set; }
        public string ScVisnovok { get; set; }  //Особливі умови висновка
        public string ScDozvil { get; set; }    //Особливі умови дозвола
        public string ScNote { get; set; }      //Примітки
        public int APPL_ID { get; set; }  

        public static readonly string TableName = PlugTbl.XfaAbonentStation;
        public static readonly string TablePosition = PlugTbl.XfaPosition;
        //public static readonly string TableEquip = PlugTbl.XfaEquip;
        //public static readonly string TableAnten = PlugTbl.XfaAnten;
        public static readonly string TableFreq = PlugTbl.XfaFreq;
       
        public RailwayApplStation()
        {
            Call = "";
            Standart = "";
            OwnerId = IM.NullI;
            PosId = IM.NullI;
        }

        /// <summary>
        /// Збереження даних при коміті
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {             
                r.Select("ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,STANDARD,CALL");
                r.Select("AZIMUTH,AGL,PWR_ANT,POS_ID,EQP_ID,ANT_ID,USER_ID,STATUS,DESIG_EMISSION");
                r.Select("SPEC_COND_DOZV,SPEC_COND_VISN,NOTE");
                r.Select("FREQ_TX_TEXT,FREQ_RX_TEXT,BW,BLANK_NUM,EXPL_TYPE");

                if (TableName == PlugTbl.XfaAbonentStation) r.Select("BRANCH_OFFICE_CODE");

                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }
                else
                {
                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
              
                r.Put("STANDARD",Standart);
                r.Put("CALL", Call);
                r.Put("AZIMUTH", Azimuth);
                r.Put("AGL", Agl);
                r.Put("PWR_ANT",Power[PowerUnits.dBW]);
                r.Put("USER_ID", OwnerId);
                r.Put("STATUS", Status);
                r.Put("DESIG_EMISSION", DesigEmiss);
                r.Put("SPEC_COND_DOZV", ScDozvil);
                r.Put("SPEC_COND_VISN", ScVisnovok);
                r.Put("NOTE", ScNote);
                r.Put("FREQ_TX_TEXT", NSend);
                r.Put("FREQ_RX_TEXT", NAccept);
                r.Put("BLANK_NUM", NumBlank);
                r.Put("EXPL_TYPE", ClassStat);
                r.Put("POS_ID", objPosition != null ? objPosition.Id : IM.NullI);
                if (TableName == PlugTbl.XfaAbonentStation)
                {
                    r.Put("BRANCH_OFFICE_CODE", BrunchCode);
                }

                //Обладнання 
                if (RailStationEquip != null)
                {
                    r.Put("EQP_ID", RailStationEquip.Id);
                    r.Put("BW", RailStationEquip.BandWidth);
                }
                //Антена
                if (RailStationAntena != null)
                    r.Put("ANT_ID", RailStationAntena.Id);
                r.Update();
            }
            finally
            {
                r.Final();
            }
            //Частоти
            FreqRxList.Clear();
            FreqTxList.Clear();
            //Видалити всі частоти які застаріли
            IMRecordset rFreq = new IMRecordset(PlugTbl.XfaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            rFreq.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
                rFreq.Delete();
            rFreq.Final();
            //Встановити нові частоти
            string[] tempStrTx = NSend.Split(';').ToArray();
            string[] tempStrRx = NAccept.Split(';').ToArray();
            for (int i = 0; i < Math.Max(tempStrRx.Length, tempStrTx.Length); i++)
            {
                if (tempStrRx.Length > i)
                    FreqRxList.Add(tempStrRx[i].ToDouble(IM.NullD));
                if (tempStrTx.Length > i)
                    FreqTxList.Add(tempStrTx[i].ToDouble(IM.NullD));
            }
            rFreq = new IMRecordset(PlugTbl.XfaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            rFreq.Open();
            for (int i = 0; i < Math.Max(FreqTxList.Count, FreqRxList.Count); i++)
            {
                rFreq.AddNew();
                rFreq.Put("ID", IM.AllocID(PlugTbl.XfaFreq, 1, -1));
                rFreq.Put("STA_ID", Id);
                if (FreqTxList.Count > i)
                    rFreq.Put("TX_FREQ", FreqTxList[i]);
                if (FreqRxList.Count > i)
                    rFreq.Put("RX_FREQ", FreqRxList[i]);
                rFreq.Update();
            }
            rFreq.Final();

        }
        /// <summary>
        /// Загружаем данные станции
        /// </summary>
        public override void Load(int id)
        {
            Id = id;
            Load();
        }
        /// <summary>
        /// Завантаження з БД  значень для форми
        /// </summary>
        public override void Load()
        {
            RailStationAntena = new RailAntenna();
            RailStationAntena.TableName = PlugTbl.XfaAnten;

            RailStationEquip = new RailEquipment();
            RailStationEquip.TableName = PlugTbl.XfaEquip;

            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,STANDARD,ABONENT_ID,PWR_ANT,POS_ID,USE_REGION,LOCATION,REGION_CODE,EXPL_TYPE,EQP_ID,PWR_ANT,FACTORY_NUM,CALL_SIGN,BLANK_NUM");
                r.Select("FREQ_TX_TEXT,FREQ_RX_TEXT,NET_ID,USER_ID,STATUS,EOUSE_DATE,BW,PERM_DATE_PRINT,Net.CALL_SIGN");
                r.Select("NAME,CALL,TX_LOSSES,AZIMUTH,AGL,DESIG_EMISSION,ANT_ID,PERM_NUM,EQP_ID,ANT_ID,STATUS");
                r.Select("USER_ID");
                r.Select("SPEC_COND_DOZV,SPEC_COND_VISN,NOTE");
                if (TableName == PlugTbl.XfaAbonentStation) r.Select("BRANCH_OFFICE_CODE");

                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");
                    PosId = r.GetI("POS_ID");
                    EquipId = r.GetI("EQP_ID");
                    AntId = r.GetI("ANT_ID");
                    if (TableName == PlugTbl.XfaAbonentStation) BrunchCode = r.GetS("BRANCH_OFFICE_CODE");
                    RailStationAntena.Id = AntId;
                    if (RailStationAntena.Id != IM.NullI)
                        RailStationAntena.Load();
                    //Обладнання                    
                    if (EquipId != IM.NullI)
                    {
                        RailStationEquip.Id = EquipId;
                        RailStationEquip.Load();
                    }
                    OwnerId = r.GetI("USER_ID");
                    Call = r.GetS("CALL");
                    Azimuth = r.GetD("AZIMUTH");
                    Agl = r.GetD("AGL");
                    Standart = r.GetS("STANDARD");
                    Power[PowerUnits.dBW] = r.GetD("PWR_ANT");
                    Status = r.GetS("STATUS");
                    DesigEmiss = r.GetS("DESIG_EMISSION");
                    ScDozvil = r.GetS("SPEC_COND_DOZV");
                    ScVisnovok = r.GetS("SPEC_COND_VISN");
                    ScNote = r.GetS("NOTE");
                    ClassStat = r.GetS("EXPL_TYPE");
                }
                else if (Id == 0 || Id == IM.NullI)
                    Id = IM.AllocID(TableName, 1, -1);
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            if (PosId != IM.NullI)
            {
                objPosition = new PositionState2();
                objPosition.LoadStatePosition(PosId, PlugTbl.XfaPosition);
            }
            else
                objPosition = null; 

            //Частоти      
            FreqRxList.Clear();
            FreqTxList.Clear();
            IMRecordset recMobStaFreqs2 = new IMRecordset(PlugTbl.XfaFreq, IMRecordset.Mode.ReadOnly);
            recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            recMobStaFreqs2.Open();
            while (!recMobStaFreqs2.IsEOF())
            {
                double rxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                int idCurr = recMobStaFreqs2.GetI("ID");
                double txFreq = recMobStaFreqs2.GetD("TX_FREQ");
                if (rxFreq != IM.NullD)
                {
                    FreqRxList.Add(rxFreq);
                    RxListId.Add(idCurr);
                }
                if (txFreq != IM.NullD)
                {
                    FreqTxList.Add(txFreq);
                    TxListId.Add(idCurr);
                }
                recMobStaFreqs2.MoveNext();
            }
            if (recMobStaFreqs2.IsOpen())
                recMobStaFreqs2.Close();
            recMobStaFreqs2.Destroy();

            NAccept = string.Empty;
            if (FreqRxList.Count > 0)
                if (CheckValidityFrequency(FreqRxList))
                {
                    for (int i = 0; i < FreqRxList.Count; i++)
                        NAccept += FreqRxList[i] + ";";
                    NAccept = NAccept.Remove(NAccept.Count() - 1);
                }

            NSend = string.Empty;
            if (FreqTxList.Count > 0)
                if (CheckValidityFrequency(FreqTxList))
                {
                    for (int i = 0; i < FreqTxList.Count; i++)
                        NSend += FreqTxList[i] + ";";
                    NSend = NSend.Remove(NSend.Count() - 1);
                }
            FillListStandart();
        }

        private void FillListStandart()
        {
            StandartList.Add("КХ");
            StandartList.Add("УКХ");
        }

        /// <summary>
        /// Разбивает строку на список double
        /// </summary>
        /// <param name="strList">строка</param>
        /// <returns>список значений</returns>
        public static List<double> ToDoubleList(string strList)
        {
            List<double> retLst = new List<double>();
            string[] parseList = strList.Split(new[] { ';' });
            foreach (string dblVal in parseList)
            {
                double tmpVal = Convert.ToDouble(dblVal);
                if (tmpVal != IM.NullD)
                    retLst.Add(tmpVal);
            }
            return retLst;
        }

        private bool CheckValidityFrequency(List<double> FreqList)
        {
            foreach (double Freq in FreqList)
                if (Freq < 0.0)
                    return false;
            return true;
        }

        public object Clone()
        {
            RailwayApplStation clone = new RailwayApplStation();
            clone.Position = Position;
            clone.PosId = PosId;
            clone.Id = Id;
            return clone;
        }
        /// <summary>
        /// Повертає список всіх OBJ_ID таблиці APPL для заданого Id
        /// </summary>
        /// <param name="idAppl"></param>
        /// <returns></returns>
        public static List<int> GetIdStat(int idAppl)
        {
            List<int> tempList = new List<int>();
            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
            r.Open();
            if (!r.IsEOF())
            {
                for (int i = 1; i < 7; i++)
                {
                    int objInt = r.GetI("OBJ_ID" + i);
                    if (objInt != IM.NullI)
                        tempList.Add(objInt);
                }
            }
            return tempList;
        }

        /// <summary>
        /// Повертає значення терміну дії з XNRFA_APPL
        /// </summary>
        /// <param name="idAppl"></param>
        /// <returns></returns>
        public static DateTime Get_EOUSE_DATE(int idAppl)
        {
            DateTime PermEndDate = IM.NullT;

            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            r.Select("ID,OBJ_ID1,OBJ_TABLE,DOZV_NUM_NEW,DOZV_DATE_TO_NEW,DOZV_DATE_TO,DOZV_NUM");
            r.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
            r.Open();
            if (!r.IsEOF())
            {
                if (r.GetS("OBJ_TABLE") == PlugTbl.XfaAbonentStation)
                {
                    if (!string.IsNullOrEmpty(r.GetS("DOZV_NUM_NEW")))
                    {
                        PermEndDate = r.GetT("DOZV_DATE_TO_NEW");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(r.GetS("DOZV_NUM")))
                        {
                            PermEndDate = r.GetT("DOZV_DATE_TO");
                        }
                    }
                }


            }
            return PermEndDate;
        }    

        /// <summary>
        /// Видаляє  OBJ_IDx таблиці APPL для заданого Id та x
        /// </summary>
        /// <param name="count"></param>
        /// <param name="id"></param>
        public static void RemoveRecFromTable(int count, int id)
        {
            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("OBJ_ID" + count, IM.NullI);
                r.Update();
            }
            r.Final();
        }
        /// <summary>
        /// Очищення запису від сатнцій
        /// </summary>
        /// <param name="id"></param>
        public static void RemoveAllRec(int id)
        {
            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                for (int i = 2; i < 7; i++)
                    r.Put("OBJ_ID" + i, IM.NullI);
                r.Update();
            }
            r.Final();
        }

        /// <summary>
        /// Зписує в таблицю  APPL по визначеному Id і OBJID необхідне значення
        /// </summary>     
        public static void SetRecToTable(int id, int index, int val)
        {
            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("OBJ_ID" + (index + 1), val);
                r.Update();
            }
            r.Final();
        }       
    }
}
