﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class FmAnalogDiffStation : FmAnalogStation
    {
        public bool IsEnableDiff { get; set; }
        public string Positiv { get; set; }

        public double Powers { get; set; }


        private string _direction = "ND";
        public override string Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    switch (value)
                    {
                        case "D":
                            _direction = value;
                            break;
                        case "ND":
                            _direction = value;
                            break;
                        default:
                            throw new Exception("Bad value of direction");
                    }
                }
            }
        }

        public static new readonly string TableName = PlugTbl.DifffmStat;

        public FmAnalogDiffStation()
        {
        }

        public override void Save()
        {
            bool status_new_record = false;
            string Fm_Statu_in_desig_em = ""; string Xnrfa_Statu_in_desig_em = "";
            Fm_Statu_in_desig_em = GetDesig_Em(ICSMTbl.itblFM, Id);
            Xnrfa_Statu_in_desig_em = GetDesig_Em(PlugTbl.DifffmStat, Id);
            if (Position.TableName == PlugTbl.itblXnrfaPositions)
                Position.SaveToBD();
            IMRecordset r = new IMRecordset(PlugTbl.DifffmStat, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,FM_STATION_ID,POS_ID,POS_TABLE,CERT_NUM,CERT_DATE,POS_TABLE,FREQ,DESIG_EM,PWR_ANT,AGL,FREQ,INACTIVE");
                r.Select("POLAR,DIRECTION,EQUIP_ID,BW,MAX_KOEF_G,MAX_KOEF_V,COMMENTS,CALL_SIGN,EQUIP_NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    status_new_record = true;
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("FM_STATION_ID", Id);
                    r.Put("DESIG_EM", Fm_Statu_in_desig_em);
                }
                else // 
                    r.Edit();

                r.Put("INACTIVE", 0);
                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }
                r.Put("PWR_ANT", Equipment.MaxPower[PowerUnits.kW]);
                r.Put("FREQ", CenterFreq);
                r.Put("AGL", AGL);
                if (!status_new_record)
                {
                    if (!string.IsNullOrEmpty(ClassEmission)) r.Put("DESIG_EM", ClassEmission);
                    if (string.IsNullOrEmpty(Xnrfa_Statu_in_desig_em)) r.Put("DESIG_EM", Fm_Statu_in_desig_em);
                }


                r.Put("CERT_NUM", Equipment.Certificate.Symbol);
                r.Put("CERT_DATE", Equipment.Certificate.Date);
                r.Put("POLAR", Polarization);
                r.Put("DIRECTION", Direction);
                r.Put("EQUIP_ID", Equipment.Id);
                r.Put("EQUIP_NAME", Equipment.Name);
                r.Put("BW", Bandwith);
                r.Put("CALL_SIGN", Positiv);
                r.Put("COMMENTS", StatComment);
                //write to Journal             
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Получение класса излучения по ID для таблицы FM_STATION и XNRFA_DIFF_FM_STAT
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string GetDesig_Em(string TableNames, int Id)
        {
            string Temp_ClassEmission = "";
            //IMRecordset r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadOnly);
            IMRecordset r = new IMRecordset(TableNames, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,DESIG_EM");
                if (TableNames == ICSMTbl.itblFM) r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                if (TableNames == PlugTbl.DifffmStat) r.SetWhere("FM_STATION_ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Temp_ClassEmission = r.GetS("DESIG_EM");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            return Temp_ClassEmission;
        }

        public override void Load()
        {

            Equipment = new FmAnalogEquipment();
            Equipment.TableName = ICSMTbl.itblEquipBro;

            IMRecordset r = new IMRecordset(PlugTbl.DifffmStat, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,EQUIP_ID,POS_TABLE,CERT_NUM,CERT_DATE,POS_TABLE,FREQ,DESIG_EM,PWR_ANT");
                r.Select("POLAR,DIRECTION,AGL,BW,MAX_KOEF_G,MAX_KOEF_V,COMMENTS,CALL_SIGN,EQUIP_NAME,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                IsEnableDiff = false;

                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;

                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        Position.LoadStatePosition(tmpPosId, tmpPosTable);

                        Equipment.MaxPower[PowerUnits.kW] = r.GetD("PWR_ANT");
                        StatComment = r.GetS("COMMENTS");
                        AGL = r.GetD("AGL");
                        Bandwith = r.GetD("BW");
                        CenterFreq = r.GetD("FREQ");
                        ClassEmission = r.GetS("DESIG_EM");
                        Polarization = r.GetS("POLAR");
                        Direction = r.GetS("DIRECTION");
                        Positiv = r.GetS("CALL_SIGN");
                        Equipment.Id = r.GetI("EQUIP_ID");
                        Equipment.Load();
                        Equipment.Certificate.Symbol = r.GetS("CERT_NUM");
                        Equipment.Certificate.Date = r.GetT("CERT_DATE");
                        Equipment.Name = r.GetS("EQUIP_NAME");
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public void Remove()
        {
            const string tableNameLocal = PlugTbl.DifffmStat;

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
