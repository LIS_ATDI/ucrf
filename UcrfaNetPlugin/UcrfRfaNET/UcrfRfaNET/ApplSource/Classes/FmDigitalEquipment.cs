﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class FmDigitallEquipment : Equipment
    {
        public double Bandwidth { get; set; }
        public override void Load()
        {
            _tableName = ICSMTbl.itblEquipBro;
            IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);

            if (ID == IM.NullI)
            {
                throw new Exception("FmDigitall equipment ID is not initialized");
            }

            try
            {
                r.Select("ID,MAX_POWER,NAME,BW,DESIG_EMISSION,CUST_TXT1,CUST_DAT1");
                r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                r.Open();

                if (!r.IsEOF())
                {
                    _maxPower[PowerUnits.dBm] = r.GetD("MAX_POWER");
                    _equipmentName = r.GetS("NAME");
                    _desigEmission = r.GetS("DESIG_EMISSION");
                    Bandwidth = r.GetD("BW");
                    Certificate.Date = r.GetT("CUST_DAT1");
                    Certificate.Symbol = r.GetS("CUST_TXT1");
                    // Modulation = r.GetS("MODULATION");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public override void Save()
        {
        }
    }
}
