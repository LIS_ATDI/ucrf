﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class TvAnalogDiffStation : BaseStation
    {

        public bool IsEnableDiff { get; set; }

        public Antenna TvDiffStationAntena { get; set; }
        public TvAnalogEquipment TvDiffStationEquipment { get; set; }

        public int SiteId { get; set; }
        public int EquipId { get; set; }
        public Double PowVideo { get; set; }
        public Double PowAudio { get; set; }
        public int ChannelId { get; set; }
        public string EmiVideo { get; set; }
        public string EmiAudio { get; set; }
        //public PositionState Position { get; set; }
        public double Bandwith { get; set; }
        public string EquipmentName { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }
        public string Channel { get; set; }
        public string Polar { get; set; }
        public string Direction { get; set; }
        public double Height { get; set; }
        public double NesivFreq { get; set; }
        public double FreqAudio { get; set; }
        public double FreqVideo { get; set; }

        public static readonly string TableName = PlugTbl.itblXnrfaDeiffTvSta;

        public TvAnalogDiffStation()
        {            
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,TV_STATION_ID,POLAR,POS_ID,POS_TABLE,EQUIP,POWVIDEO,POWAUDIO,RXBAND,EMIVIDEO,EMIAUDIO,CERTNO,CERTDA,CHANNEL,POLAR,DIRECTION,OFFSET_V_KHZ");
                r.Select("AGL,FREQ_V_CARR,FREQ,COMMENTS,INACTIVE");

                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("TV_STATION_ID", Id);                        
                }
                else // 
                    r.Edit();
                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }
                r.Put("INACTIVE", 0);
                r.Put("EQUIP", EquipmentName);
                r.Put("POWVIDEO", PowVideo);
                r.Put("POWAUDIO", PowAudio);
                r.Put("RXBAND", Bandwith);
                r.Put("EMIVIDEO", EmiVideo);
                r.Put("EMIAUDIO", EmiAudio);
                r.Put("CERTNO", TvDiffStationEquipment.Certificate.Symbol);
                r.Put("CERTDA", TvDiffStationEquipment.Certificate.Date);
                r.Put("POLAR", Polar);
                r.Put("DIRECTION", Direction);
                r.Put("AGL", Height);
                r.Put("CHANNEL", Channel);
                r.Put("OFFSET_V_KHZ", NesivFreq);
                r.Put("FREQ", FreqAudio);
                r.Put("FREQ_V_CARR", FreqVideo);
                r.Put("COMMENTS", StatComment);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public override void Load()
        {
            TvDiffStationAntena = new MobStationAntenna();
            TvDiffStationAntena.TableName = ICSMTbl.itblAntennaBro;

            TvDiffStationEquipment = new TvAnalogEquipment();
            TvDiffStationEquipment.TableName = ICSMTbl.itblEquipBro;

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDeiffTvSta, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_TABLE,POS_ID,EQUIP,POWVIDEO,POWAUDIO,RXBAND,EMIVIDEO,EMIAUDIO,CERTNO,CERTDA,CHANNEL,POLAR,DIRECTION,OFFSET_V_KHZ");
                r.Select("AGL,FREQ_V_CARR,FREQ,COMMENTS,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                IsEnableDiff = false;
                r.Open();
                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;

                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        if (tmpPosId != IM.NullI)
                        {
                            Position.LoadStatePosition(tmpPosId, tmpPosTable);
                            EquipmentName = r.GetS("EQUIP");
                            PowVideo = r.GetD("POWVIDEO");
                            PowAudio = r.GetD("POWAUDIO");
                            Bandwith = r.GetD("RXBAND");
                            EmiVideo = r.GetS("EMIVIDEO");
                            EmiAudio = r.GetS("EMIAUDIO");
                            TvDiffStationEquipment.Certificate.Symbol = r.GetS("CERTNO");
                            TvDiffStationEquipment.Certificate.Date = r.GetT("CERTDA");
                            NesivFreq = r.GetD("OFFSET_V_KHZ");
                            Height = r.GetD("AGL");
                            Direction = r.GetS("DIRECTION");
                            Polar = r.GetS("POLAR");
                            Channel = r.GetS("CHANNEL");
                            FreqAudio = r.GetD("FREQ");
                            FreqVideo = r.GetD("FREQ_V_CARR");
                            StatComment = r.GetS("COMMENTS");
                        }
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public void Remove()
        {
            const string tableNameLocal = PlugTbl.itblXnrfaDeiffTvSta;

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
