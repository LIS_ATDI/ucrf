﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStaTechAppDiffStation : BaseStation
    {
        private Power _power = new Power();
        /// <summary>
        /// Таблиця Diff
        /// </summary>
        public static readonly string DiffTableName = PlugTbl.itblXnrfaDiffMobSta;

        public Antenna techDiffStationAntena;
        public Equipment techDiffStationEquip;

        /// <summary>
        /// Дозвіл на виведення даних DIFF
        /// </summary>
        public bool IsEnableDiff { get; set; }

        /// <summary>
        /// позиція
        /// </summary>
        //public PositionState Position { get; set; }
        public int PosId { get; set; }
        public int EquipId { get; set; }

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        public double Asl { get; set; }
        /// <summary>
        /// Потужність передавача, Вт 
        /// </summary>
        //public double Power { get; set; }
        public Power Power
        {
            get { return _power; }
        }
        /// <summary>
        /// Клас випромінювання      
        /// </summary>
        public string DesigEmiss { get; set; }
        /// <summary>
        /// Радіотехнологія(Стандарт)
        /// </summary>
        public string Standart { get; set; }
        /// <summary>
        /// Означення станції
        /// </summary>
        public string Defin { get; set; }
        /// <summary>
        /// Клас станції
        /// </summary>
        public string ClassStat { get; set; }
        /// <summary>
        /// Номер сертифіката 
        /// </summary>
        public string CertNumb { get; set; }
        /// <summary>
        /// Дата сертифіката 
        /// </summary>
        public DateTime CertDate { get; set; }
        /// <summary>
        /// Позивний
        /// </summary>
        public string Call { get; set; }


        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        public string NameRez { get; set; }

        /// <summary>
        /// Тип антени
        /// </summary>
        public string TypeAnt { get; set; }

        /// <summary>
        /// Азимут випромінювання антени, град 
        /// </summary>
        public double Azimuth { get; set; }

        /// <summary>
        /// Висота антени над рівнем Землі, м
        /// </summary>
        public double HeightAnt { get; set; }

        /// <summary>
        /// Коефіцієнт підсилення антени, дБі
        /// </summary>
        public double Koef { get; set; }
        /// <summary>
        /// Ширина ДС
        /// </summary>
        public double WidthAnt { get; set; }

        /// <summary>
        /// Позивний
        /// </summary>
        public string Called { get; set; }

        /// <summary>
        /// Частоти приймання
        /// </summary>
        public string NomReceive { get; set; }
        /// <summary>
        /// Частоти передавання
        /// </summary>
        public string NomSend { get; set; }
        /// <summary>
        /// Частоти приймання, список
        /// </summary>
        public List<double> NomReceiveList = new List<double>();
        /// <summary>
        /// Частоти передавання, список
        /// </summary>
        public List<double> NomSendList = new List<double>();

        public static readonly string TableName = PlugTbl.itblXnrfaDiffMobSta;        

        public MobStaTechAppDiffStation()
        {                    
        }

        /// <summary>
        /// Збереження тимчасових даних в таблицю DIFF 
        /// </summary>
        public override void Save()
        {            
            IMRecordset r = new IMRecordset(DiffTableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,MOBSTA_ID,NETWORK_IDENT,CLASS,NAME,POS_ID,POS_TABLE,PWR_ANT,AZIMUTH,CALL_SIGN,ASL,TX_LOSSES");
                r.Select("EQUIP_ID,TX_LOSSES,AZIMUTH,CERT_NUM,CERT_DATE,DESIG_EM,PWR_ANT,ASL");
                r.Select("AGL,TYPE,EQUIP_NAME,ANT_WIDTH,GAIN,COMMENTS,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("MOBSTA_ID", Id);
                }
                else // 
                    r.Edit();

                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }

                r.Put("INACTIVE", 0);
                r.Put("EQUIP_ID", EquipId);
                r.Put("NETWORK_IDENT", Standart);
                r.Put("NAME", Defin);
                r.Put("CLASS", ClassStat);
                r.Put("CALL_SIGN", Call);
                r.Put("AGL", HeightAnt);
                r.Put("TYPE", TypeAnt);
                r.Put("EQUIP_NAME", NameRez);
                r.Put("ANT_WIDTH", WidthAnt);
                r.Put("GAIN", Koef);
                //r.Put("TX_LOSSES", Fider);
                r.Put("AZIMUTH", Azimuth);

                r.Put("CERT_NUM", techDiffStationEquip.Certificate.Symbol);                
                r.Put("CERT_DATE", techDiffStationEquip.Certificate.Date);
                r.Put("DESIG_EM", DesigEmiss);
                r.Put("PWR_ANT", Power[PowerUnits.dBm]);
                r.Put("ASL", Asl);
                r.Put("COMMENTS", StatComment);
                //r.Put("BW", Bandwith * 1000);

                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Частоти Diff
            string[] nomReceiveArray = NomReceive.Split(';');
            string[] nomSendArray = NomSend.Split(';');

            //Видалення старих даних
            IMRecordset rFreq = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA_ID,TR");
            rFreq.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                rFreq.Delete();
            }
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();

            rFreq = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA_ID,TR");
            rFreq.Open();
            for (int i = 0; i < nomReceiveArray.Length; i++)
            {
                rFreq.AddNew();
                rFreq.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobStaFreq, 1, -1));
                rFreq.Put("MOBSTA_ID", Id);
                double currReceive;
                if (double.TryParse(nomReceiveArray[i], out currReceive))
                    rFreq.Put("FREQUENCY", currReceive);
                else
                    rFreq.Put("FREQUENCY", 0.0);
                rFreq.Put("TR", "R");
                rFreq.Update();
            }
            for (int i = 0; i < nomSendArray.Length; i++)
            {
                rFreq.AddNew();
                rFreq.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMobStaFreq, 1, -1));
                rFreq.Put("MOBSTA_ID", Id);
                double currSend;
                if (double.TryParse(nomSendArray[i], out currSend))
                    rFreq.Put("FREQUENCY", currSend);
                else
                    rFreq.Put("FREQUENCY", 0.0);
                rFreq.Put("TR", "T");
                rFreq.Update();
            }
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();
        }

        /// <summary>
        /// Завантаження з БД тимчасових значень для форми
        /// </summary>
        public override void Load()
        {
            techDiffStationAntena = new MobStationAntenna();
            techDiffStationAntena.TableName = ICSMTbl.itblAntennaMob;

            techDiffStationEquip = new MobStationEquipment();
            techDiffStationEquip.TableName = ICSMTbl.itblEquipPmr;

            IMRecordset r = new IMRecordset(DiffTableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NETWORK_IDENT,CLASS,NAME,POS_ID,POS_TABLE,PWR_ANT,AZIMUTH,CALL_SIGN,ASL,TX_LOSSES");
                r.Select("AZIMUTH,CERT_NUM,CERT_DATE,PWR_ANT,EQUIP_ID,ASL,DESIG_EM");
                r.Select("AGL,TYPE,EQUIP_NAME,ANT_WIDTH,GAIN,COMMENTS,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                IsEnableDiff = false;
                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Id = r.GetI("ID");
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;
                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        if (tmpPosId != IM.NullI)
                        {
                            Position.LoadStatePosition(tmpPosId, tmpPosTable);
                            Position.Id = tmpPosId;
                            PosId = tmpPosId;
                            EquipId = r.GetI("EQUIP_ID");
                            Position.TableName = tmpPosTable;
                            Standart = r.GetS("NETWORK_IDENT");
                            Defin = r.GetS("NAME");
                            ClassStat = r.GetS("CLASS");
                            Call = r.GetS("CALL_SIGN");
                            HeightAnt = r.GetD("AGL");
                            TypeAnt = r.GetS("TYPE");
                            NameRez = r.GetS("EQUIP_NAME");
                            WidthAnt = r.GetD("ANT_WIDTH");
                            Koef = r.GetD("GAIN");
                            //  Fider = r.GetD("TX_LOSSES");
                            Azimuth = r.GetD("AZIMUTH");
                            techDiffStationEquip.Certificate.Symbol = r.GetS("CERT_NUM");
                            techDiffStationEquip.Certificate.Date = r.GetT("CERT_DATE");
                            DesigEmiss = r.GetS("DESIG_EM");
                            Power[PowerUnits.dBm] = r.GetD("PWR_ANT");
                            Asl = r.GetD("ASL");
                            StatComment = r.GetS("COMMENTS");
                        }
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            //Завантаження частот
            IMRecordset rFreq = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA_ID,TR");
            rFreq.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
            rFreq.SetWhere("TR", IMRecordset.Operation.Eq, "R");
            NomReceive = string.Empty;
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                double currDouble = rFreq.GetD("FREQUENCY");
                NomReceive += currDouble + ";";
            }
            if (NomReceive.Length > 0)
                NomReceive = NomReceive.Remove(NomReceive.Length - 1);
            if (NomReceive == "0") NomReceive = "";
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();

            rFreq = new IMRecordset(PlugTbl.itblXnrfaMobStaFreq, IMRecordset.Mode.ReadWrite);
            rFreq.Select("ID,FREQUENCY,MOBSTA_ID,TR");
            rFreq.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, Id);
            rFreq.SetWhere("TR", IMRecordset.Operation.Eq, "T");
            NomSend = string.Empty;
            for (rFreq.Open(); !rFreq.IsEOF(); rFreq.MoveNext())
            {
                double currDouble = rFreq.GetD("FREQUENCY");
                NomSend += currDouble + ";";
            }

            if (NomSend.Length > 0)
                NomSend = NomSend.Remove(NomSend.Length - 1);
            if (NomSend == "0") NomSend = "";
            if (rFreq.IsOpen())
                rFreq.Close();
            rFreq.Destroy();
        }

        public void Remove()
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset(DiffTableName, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();                    
                }                
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
