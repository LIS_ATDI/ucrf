﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using System.Drawing;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public abstract class BaseCommitClass : BaseAppClass
    {        
        public BaseCommitClass(int _id, string _tableName, int _ownerId, int _packetID, string _radioTech)
            : base(_id, _tableName, _ownerId, _packetID, _radioTech) { }
       
        public override void UpdateForm(MainAppForm form)
        {
            base.UpdateForm(form);
            form.tabControl1.SelectTab(form.tabControl1.TabPages[3].Name);
        }

        public override bool OnAfterSave()
        {            
            if (gridParam!=null)
            {
                foreach (Row row in gridParam.rowList)
                {
                    foreach(Cell cell in row.cellList)
                    {                        
                        cell.UnbindAll();                     
                    }                    
                }                
            }
            return true;
        }

        protected int SaveNewPosition(PositionState newPos, PositionState oldPos, string[] assgnReferenceTables)
        {
            string msg = "Обрано новий сайт, що був створений за результатами ПТК." + Environment.NewLine + Environment.NewLine;

            //discover whether there are more references to this site,
            //if so, add this info to message.
            string assignmentsInfo = "";
            foreach (string tn in assgnReferenceTables)
            {
                int count = 0;
                if (IM.ExecuteScalar(ref count, string.Format("select count(ID) from %{0} where {1} = {2}", tn, posFieldNames[tn], oldPos.Id)))
                    assignmentsInfo += string.Format("{0}: {1} присвоєнь{2}", tn, count, Environment.NewLine);
            }

            msg += string.Format("Існуючий сайт {0}: {1}", oldPos.TableName, oldPos.Id);
            if (assignmentsInfo.Length > 0)
                msg += (", на який посилаються записи з таблиць:" + Environment.NewLine + assignmentsInfo + ",");
            msg += " буде збережено." + Environment.NewLine;

            //msg += Environment.NewLine + "Продовжити?";

            //if (
            MessageBox.Show(msg,
                //CLocaliz.Question, 
                CLocaliz.Information,
                MessageBoxButtons.OK, MessageBoxIcon.Information)
                //== DialogResult.No)
                //return false
            ;

            //if admin site exists with other references (not only this one),
            //then ask user whether to isolate it and mark as invalid
            //if no connections except this, don't ask, just do it
            int curAdmsId = oldPos.AdminSitesId;
            if (curAdmsId != IM.NullI)
            {
                string posInfo = "";
                foreach (string tn in posTables)
                {
                    int count = 0;
                    if (IM.ExecuteScalar(ref count, string.Format("select count(ID){2} from %{0} where ADMS_ID = {1}", tn, curAdmsId, tn == oldPos.TableName ? "-1" : "")) && count != 0)
                        posInfo += string.Format("{0}: {1} посилань{2}", tn, count, Environment.NewLine);
                }
                string confirmMsg = (posInfo.Length == 0) ?
                    "Існуючий адмінсайт (ID = {0}) не має посилань (крім поточного сайту)."
                        + Environment.NewLine + "Помітити цей адмінсайт на видалення?"
                    :
                    "На існуючий адмінсайт (ID = {0}) є посилання (крім поточного сайту) з наступних таблиць:"
                        + Environment.NewLine
                        + posInfo
                        + Environment.NewLine
                        + "Скинути їх та помітити цей адмінсайт на видалення?";
                if (MessageBox.Show(string.Format(confirmMsg, curAdmsId), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //isolate
                    foreach (string tn in posTables)
                        IM.Execute(string.Format("update %{0} set ADMS_ID = NULL where ADMS_ID = {1}", tn, curAdmsId));
                    //mark for deletion
                    IM.Execute(string.Format("update %SITES set STATUS = '{0}' where ID = {1}", CAndE.Del, curAdmsId));
                }
            }

            newPos.Id = IM.NullI;
            newPos.AdminSitesId = IM.NullI;
            newPos.NeedAddAdminSite = true;
            newPos.TableName = oldPos.TableName;
            newPos.SaveToBD(); // admin site will be created as well

            PositionAdmSite2 admSite = new PositionAdmSite2();
            admSite.LoadStatePosition(newPos.AdminSitesId, ICSMTbl.SITES); // here we can just load it from selectedPosition, if selectedPosition will be PositionState2
            admSite.SendDate = DateTime.Now;
            admSite.SendUser = IM.ConnectedUser();
            admSite.SetStatusSites(CAndE.ReqP);

            return newPos.Id;
        }
    }
}
