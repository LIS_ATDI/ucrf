﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MobStation2AppStation : BaseStation, ICloneable
    {
        public MobStationAntenna mobStationAntena { get; set; }
        public MobStationEquipment mobStationEquipment { get; set; }

        public int PosId { get; set; }
        public int AntId { get; set; }
        public int EquipId { get; set; }
        //public PositionState Position { get; set; }
        public double ASL { get; set; }
        public string NameRez { get; set; }
        public double Power { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public List<string> ModulationList = new List<string>();
        public List<string> OnOffList = new List<string>();
        public List<double> FreqRxList = new List<double>();
        public List<int> RxListId = new List<int>();
        public List<double> FreqTxList = new List<double>();
        public List<int> TxListId = new List<int>();
        public int Modulation { get; set; }
        public double SpeedThread { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }
        public double Azimuth { get; set; }
        public double PlaceSite { get; set; }
        public string TypeAnten { get; set; }
        public double AGL { get; set; }
        public double Koef { get; set; }
        public double WideAnten { get; set; }
        public Dictionary<string, string> PolarAntDict = new Dictionary<string, string>();
        public string PolarAnt { get; set; }

        public int OnOff { get; set; }

        public string NominAccept { get; set; }
        public string NominSend { get; set; }

        public int ObjRchp { get; set; }

        public static readonly string TableName = ICSMTbl.MobStation2;

        public MobStation2AppStation()
        {
        }

        public object Clone()
        {
            MobStation2AppStation clone = new MobStation2AppStation();
            clone.Id = Id;
            clone.PosId = PosId;
            clone.AntId = AntId;
            clone.EquipId = EquipId;
            clone.Position = Position;
            clone.ASL = ASL;
            clone.NameRez = NameRez;
            clone.Power = Power;
            clone.Bandwith = Bandwith;
            clone.ClassEmission = ClassEmission;
            clone.ModulationList = ModulationList;
            clone.OnOffList = OnOffList;
            clone.FreqRxList = FreqRxList;
            clone.RxListId = RxListId;
            clone.FreqTxList = FreqTxList;
            clone.TxListId = TxListId;
            clone.Modulation = Modulation;
            clone.SpeedThread = SpeedThread;
            clone.CertificateNumber = CertificateNumber;
            clone.CertificateDate = CertificateDate;
            clone.Azimuth = Azimuth;
            clone.PlaceSite = PlaceSite;
            clone.TypeAnten = TypeAnten;
            clone.AGL = AGL;
            clone.Koef = Koef;
            clone.WideAnten = WideAnten;
            clone.PolarAntDict = PolarAntDict;
            clone.PolarAnt = PolarAnt;
            clone.StatComment = StatComment;
            clone.OnOff = OnOff;

            clone.NominAccept = NominAccept;
            clone.NominSend = NominSend;

            clone.ObjRchp = ObjRchp;
            clone.mobStationAntena = mobStationAntena;
            clone.mobStationEquipment = mobStationEquipment;

            //MobStationAntenna clone.mobStationAntena = new MobStationAntenna();
            //MobStationEquipment clone.mobStationEquipment = new MobStationEquipment();
            clone.mobStationAntena.TableName = mobStationAntena.TableName;
            clone.mobStationAntena.Id = mobStationAntena.Id;
            clone.mobStationAntena.AntennaWidth = mobStationAntena.AntennaWidth;
            clone.mobStationAntena.Gain = mobStationAntena.Gain;

            clone.mobStationEquipment.Id = mobStationEquipment.Id;
            clone.mobStationEquipment.TableName = mobStationEquipment.TableName;
            clone.mobStationEquipment.Bandwidth = mobStationEquipment.Bandwidth;
            clone.mobStationEquipment.Certificate.Date = mobStationEquipment.Certificate.Date;
            clone.mobStationEquipment.Certificate.Symbol = mobStationEquipment.Certificate.Symbol;
            clone.mobStationEquipment.DesigEmission = mobStationEquipment.DesigEmission;
            clone.mobStationEquipment.MaxPower[PowerUnits.W] = mobStationEquipment.MaxPower[PowerUnits.W];
            clone.mobStationEquipment.Modulation = mobStationEquipment.Modulation;

            return clone;
        }



        /// <summary>
        /// Збереження даних при коміті
        /// </summary>
        public override void Save()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.MobStation2, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,POS_ID,EQUIP_ID,ANT_ID,PLAN_ID,PWR_ANT,AZIMUTH,ELEVATION,POLAR,AZIMUTH,ELEVATION,AGL,POLAR");
                r.Select("DESIG_EMISSION");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", Id);
                    if (Position.TableName != ICSMTbl.itblPositionMob2)
                    {
                        Position.Id = IM.NullI;
                        Position.TableName = ICSMTbl.itblPositionMob2;
                        Position.SaveToBD();
                        PosId = Position.Id;
                    }
                    //else
                    //{
                    //    PosId = Position.Id;
                    //    Position.SaveToBD();
                    //}
                    r.Put("POS_ID", Position.Id);
                    r.Put("EQUIP_ID", mobStationEquipment.Id);
                    r.Put("ANT_ID", mobStationAntena.Id);
                    r.Put("PWR_ANT", Power);
                    r.Put("AZIMUTH", Azimuth);
                    r.Put("ELEVATION", PlaceSite);
                    r.Put("DESIG_EMISSION", mobStationEquipment.DesigEmission);
                    //   r.Put("AGL", HeightMax);
                    r.Put("POLAR", PolarAnt);
                    r.Put("AZIMUTH", Azimuth);
                    r.Put("ELEVATION", PlaceSite);
                    r.Put("AGL", AGL);
                    r.Put("POLAR", PolarAnt);
                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Частоти               
            FreqRxList.Clear();
            FreqTxList.Clear();
            RxListId.Clear();
            TxListId.Clear();
            string[] rxArray = NominAccept.Split(';');
            bool rxShow = true;
            for (int i = 0; i < rxArray.Length; i++)
            {
                double currDbl;
                if (double.TryParse(rxArray[i], out currDbl))
                    FreqRxList.Add(currDbl);
                else
                    rxShow = false;
            }
            string[] txArray = NominSend.Split(';');
            bool txShow = true;
            for (int i = 0; i < txArray.Length; i++)
            {
                double currDbl;
                if (double.TryParse(txArray[i], out currDbl))
                    FreqTxList.Add(currDbl);
                else
                    txShow = false;
            }
            if (!rxShow || !txShow)
                MessageBox.Show("Wrong format in Freq!");

            //Заповнити таблицю з Diff...
            IMRecordset recDiffFreqs2 = new IMRecordset(PlugTbl.itblXnrfaMobSta2Freq, IMRecordset.Mode.ReadWrite);
            recDiffFreqs2.Select("ID,FREQUENCY,MOBSTA2_ID,TR");
            recDiffFreqs2.SetWhere("MOBSTA2_ID", IMRecordset.Operation.Eq, Id);
            recDiffFreqs2.Open();
            while (!recDiffFreqs2.IsEOF())
            {
                string tr = recDiffFreqs2.GetS("TR");
                if (tr == "T")
                    TxListId.Add(Id);
                else
                    RxListId.Add(Id);
                recDiffFreqs2.MoveNext();
            }
            if (recDiffFreqs2.IsOpen())
                recDiffFreqs2.Close();
            recDiffFreqs2.Destroy();

            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);

            //Сохраняем частоты
            List<MobFreq> freqs = new List<MobFreq>();
            int maxCount = Math.Max(RxListId.Count, TxListId.Count);
            for (int indexFreq = 0; indexFreq < maxCount; indexFreq++)
            {
                MobFreq freq = new MobFreq();
                freq.RxMHz = IM.NullD;
                if (RxListId.Count > indexFreq)
                    if (FreqRxList.Count > indexFreq)
                        freq.RxMHz = FreqRxList[indexFreq];
                freq.TxMHz = IM.NullD;
                if (TxListId.Count > indexFreq)
                    if (FreqTxList.Count > indexFreq)
                        freq.TxMHz = FreqTxList[indexFreq];
                freqs.Add(freq);
            }
            MobFreqManager.SaveFreq(Id, MobFreqManager.TypeFreq.MobStation2, freqs.ToArray());

            FillComments(ApplId);            
        }

        public override void Load(int currId)
        {
            Id = currId;
            Load();
        }

        public override void Load()
        {
            mobStationAntena = new MobStationAntenna();
            mobStationAntena.TableName = ICSMTbl.itblAntennaMob2;

            mobStationEquipment = new MobStationEquipment();
            mobStationEquipment.TableName = ICSMTbl.itblEquipMob2;

            AddPolar();
            AddModulation();
            AddOnOff();

            IMRecordset r = new IMRecordset(ICSMTbl.MobStation2, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,POS_ID,EQUIP_ID,ANT_ID,PWR_ANT,AZIMUTH,ELEVATION,POLAR,AGL,CUST_TXT17");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");
                    PosId = r.GetI("POS_ID");
                    mobStationEquipment.Id = r.GetI("EQUIP_ID");
                    if (mobStationEquipment.Id != IM.NullI)
                        mobStationEquipment.Load();
                    mobStationAntena.Id = r.GetI("ANT_ID");
                    if (mobStationAntena.Id != IM.NullI)
                        mobStationAntena.Load();
                    Power = r.GetD("PWR_ANT");
                    AGL = r.GetD("AGL");
                    Azimuth = r.GetD("AZIMUTH");
                    PlaceSite = r.GetD("ELEVATION");
                    PolarAnt = r.GetS("POLAR");
                    ObjRchp = r.GetI("ID");
                    //Висновок
                    Finding = r.GetS("CUST_TXT17");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            Position = new PositionState();
            Position.LoadStatePosition(PosId, ICSMTbl.itblPositionMob2);
            //Частоти      
            FreqRxList.Clear();
            FreqTxList.Clear();
            IMRecordset recMobStaFreqs2 = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
            recMobStaFreqs2.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
            recMobStaFreqs2.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
            recMobStaFreqs2.Open();
            while (!recMobStaFreqs2.IsEOF())
            {
                double RxFreq = recMobStaFreqs2.GetD("RX_FREQ");
                int IdCurr = recMobStaFreqs2.GetI("ID");
                double TxFreq = recMobStaFreqs2.GetD("TX_FREQ");
                if (RxFreq != IM.NullD)
                {
                    FreqRxList.Add(RxFreq);
                    RxListId.Add(IdCurr);
                }
                if (TxFreq != IM.NullD)
                {
                    FreqTxList.Add(TxFreq);
                    TxListId.Add(IdCurr);
                }
                recMobStaFreqs2.MoveNext();
            }
            if (recMobStaFreqs2.IsOpen())
                recMobStaFreqs2.Close();
            recMobStaFreqs2.Destroy();

            NominAccept = string.Empty;
            if (FreqRxList.Count > 0)
                if (CheckValidityFrequency(FreqRxList))
                {
                    for (int i = 0; i < FreqRxList.Count; i++)
                        NominAccept += FreqRxList[i].ToString() + ";";
                    NominAccept = NominAccept.Remove(NominAccept.Count() - 1);
                }

            NominSend = string.Empty;
            if (FreqTxList.Count > 0)
                if (CheckValidityFrequency(FreqTxList))
                {
                    for (int i = 0; i < FreqTxList.Count; i++)
                        NominSend += FreqTxList[i].ToString() + ";";
                    NominSend = NominSend.Remove(NominSend.Count() - 1);
                }

            //Обладнання
            IMRecordset equipRec = new IMRecordset(ICSMTbl.itblEquipMob2, IMRecordset.Mode.ReadOnly);
            try
            {
                equipRec.Select("ID,NAME,BW,MODULATION,MBITPS,CUST_TXT1,CUST_DAT1,DESIG_EMISSION");
                equipRec.SetWhere("ID", IMRecordset.Operation.Eq, EquipId);
                equipRec.Open();
                if (!equipRec.IsEOF())
                {
                    NameRez = equipRec.GetS("NAME");
                    mobStationEquipment.Bandwidth = equipRec.GetD("BW") / 1000;
                    mobStationEquipment.DesigEmission = equipRec.GetS("DESIG_EMISSION");
                    mobStationEquipment.Modulation = equipRec.GetS("MODULATION");
                    SpeedThread = equipRec.GetD("MBITPS");
                    mobStationEquipment.Certificate.Symbol = equipRec.GetS("CUST_TXT1");
                    mobStationEquipment.Certificate.Date = equipRec.GetT("CUST_DAT1");
                }
            }
            finally
            {
                if (equipRec.IsOpen())
                    equipRec.Close();
                equipRec.Destroy();
            }
            //Антена
            IMRecordset antenRec = new IMRecordset(ICSMTbl.itblAntennaMob2, IMRecordset.Mode.ReadOnly);
            antenRec.Select("ID,NAME,H_BEAMWIDTH,GAIN");
            antenRec.SetWhere("ID", IMRecordset.Operation.Eq, AntId);
            try
            {
                antenRec.Open();
                if (!antenRec.IsEOF())
                {
                    TypeAnten = antenRec.GetS("NAME");
                    Koef = antenRec.GetD("GAIN");
                    WideAnten = antenRec.GetD("H_BEAMWIDTH");
                }
            }
            finally
            {
                if (antenRec.IsOpen())
                    antenRec.Close();
                antenRec.Destroy();
            }
        }


        /// <summary>
        /// Разбивает строку на список double
        /// </summary>
        /// <param name="strList">строка</param>
        /// <returns>список значений</returns>
        public static List<double> ToDoubleList(string strList)
        {
            List<double> retLst = new List<double>();
            string[] parseList = strList.Split(new Char[] { ';' });
            foreach (string dblVal in parseList)
            {
                double tmpVal = Convert.ToDouble(dblVal);
                if (tmpVal != IM.NullD)
                    retLst.Add(tmpVal);
            }
            return retLst;
        }

        private bool CheckValidityFrequency(List<double> FreqList)
        {
            foreach (double Freq in FreqList)
                if (Freq < 0.0)
                    return false;
            return true;
        }


        private void AddModulation()
        {
            ModulationList.Clear();
            ModulationList.Add("none");
            ModulationList.Add("AM compr normal");
            ModulationList.Add("FM");
            ModulationList.Add("QPSK");
            ModulationList.Add("4-QAM");
            ModulationList.Add("8-QAM");
            ModulationList.Add("16-QAM");
            ModulationList.Add("32-QAM");
            ModulationList.Add("64-QAM");
            ModulationList.Add("128-QAM");
            ModulationList.Add("256-QAM");
            ModulationList.Add("512-QAM");
            ModulationList.Add("DQPSK");
            ModulationList.Add("QPSK 1/2");
            ModulationList.Add("QPSK 2/3");
            ModulationList.Add("QPSK 3/4");
            ModulationList.Add("QPSK 5/6");
            ModulationList.Add("QPSK 7/8");
            ModulationList.Add("16-QAM 1/2 M1");
            ModulationList.Add("16-QAM 2/3");
            ModulationList.Add("16-QAM 3/4");
            ModulationList.Add("16-QAM 5/6");
            ModulationList.Add("16-QAM 7/8");
            ModulationList.Add("64-QAM 1/2M2");
            ModulationList.Add("64-QAM 2/3M3");
            ModulationList.Add("64-QAM 3/4");
            ModulationList.Add("64-QAM 5/6");
            ModulationList.Add("64-QAM 7/8");
            ModulationList.Add("PDM-8");
            ModulationList.Add("PDM-4 1/2");
            ModulationList.Add("Analog");
            ModulationList.Add("TPRS");
            ModulationList.Add("AM compr high");
            ModulationList.Add("BFSK 1/2");
            ModulationList.Add("BFSK 3/4");
            ModulationList.Add("PDM-4 2/3");
            ModulationList.Add("PDM-4 3/4");
            ModulationList.Add("PDM-4 5/6");
            ModulationList.Add("PDM-4 7/8");
            ModulationList.Add("BPSK");
            ModulationList.Add("GMSK");
            ModulationList.Add("8PSK");
            ModulationList.Add("OFDM");
            ModulationList.Add("FFSK");
            ModulationList.Add("FFSK2");
            ModulationList.Add("FFSK4");
            ModulationList.Add("FFSK4L");
        }

        private void AddPolar()
        {
            PolarAntDict.Clear();
            PolarAntDict.Add("V", "Вертикальна");
            PolarAntDict.Add("H", "Горизонтальна");
            PolarAntDict.Add("CL", "Лівокругова");
            PolarAntDict.Add("CR", "Правокругова");
            PolarAntDict.Add("D", "Дуальна");
            PolarAntDict.Add("L", "Лінійна");
            PolarAntDict.Add("M", "Змішана");
            PolarAntDict.Add("SL", "Лівостороння");
            PolarAntDict.Add("SR", "Правостороння");
        }

        private void AddOnOff()
        {
            OnOffList.Clear();
            OnOffList.Add("Вимкнено");
            OnOffList.Add("Ввімкнено");
        }

        public List<string> FillPolar()
        {
            List<string> tempListPolar = new List<string>();
            foreach (KeyValuePair<string, string> list in PolarAntDict)
                tempListPolar.Add(list.Key + " - " + list.Value);
            return tempListPolar;
        }

        protected void StoreDiagrams(IMRecordset r, List<double> HorizontalData, List<double> VerticalData)
        {

        }
    }
}
