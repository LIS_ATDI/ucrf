﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class FmDigitallDiffStation : BaseStation
    {
        public FmDigitallEquipment FmDgDiffEquipment { get; set; }
        public FmDigitallAntenna FmDgDiffAntenna { get; set; }

        public bool IsEnableDiff { get; set; }

        public int PosId { get; set; }
        
        public int AntId { get; set; }
        //public PositionState Position { get; set; }
        public string EquipmentName { get; set; }

        public double ShiftOstov { get; set; }

        public double HeightAnten { get; set; }
        public string NameRez { get; set; }
        public double PowerTx { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }

        public string Polar { get; set; }
        public string Direct { get; set; }

        public String Block { get; set; }
        public Double Centr { get; set; }
        public Power Power { get; set; }

        public static readonly string TableName = PlugTbl.itblXnrfaDiffFmDigital;

        public FmDigitallDiffStation()
        {            
        }

        public override void Load()
        {
            Power = new Power();

            FmDgDiffEquipment = new FmDigitallEquipment();

            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {                
                r.Select("ID,POS_ID,EQUIP_ID,ANT_ID,ANT_DIR,POS_ID,POS_TABLE,DESIG_EM");
                r.Select("CERT_DATE,CERT_NUM,BW,BLOCK_STR,OFFSET,COMMENTS");
                r.Select("POLAR,ANT_DIR,ANT_HEIGHT,REMARK,POWER,FREQS_CENTRAL,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                IsEnableDiff = false;
                if (!r.IsEOF())
                {
                    if (r.GetI("INACTIVE") != 1)
                    {
                        Position = new PositionState();
                        int tmpPosId = r.GetI("POS_ID");
                        string tmpPosTable = r.GetS("POS_TABLE");
                        if (string.IsNullOrEmpty(tmpPosTable))
                            tmpPosTable = PlugTbl.itblXnrfaPositions;

                        Position.Id = tmpPosId;
                        Position.TableName = tmpPosTable;
                        if (tmpPosId != IM.NullI)
                        {
                            Position.LoadStatePosition(tmpPosId, tmpPosTable);

                            FmDgDiffEquipment.Id = r.GetI("EQUIP_ID");
                            if (FmDgDiffEquipment.Id != IM.NullI)
                                FmDgDiffEquipment.Load();

                            ShiftOstov = r.GetD("OFFSET");
                            FmDgDiffEquipment.Certificate.Date = r.GetT("CERT_DATE");
                            FmDgDiffEquipment.Certificate.Symbol = r.GetS("CERT_NUM");
                            FmDgDiffEquipment.DesigEmission = r.GetS("DESIG_EM");
                            FmDgDiffEquipment.Bandwidth = r.GetD("BW");
                            Power[PowerUnits.dBm] = r.GetD("POWER");

                            HeightAnten = r.GetD("ANT_HEIGHT");
                            Polar = r.GetS("POLAR");
                            Direct = r.GetS("ANT_DIR");
                            Block = r.GetS("BLOCK_STR");
                            Centr = r.GetD("FREQS_CENTRAL");
                            NameRez = r.GetS("REMARK");
                            StatComment = r.GetS("COMMENTS");
                            //  Power = Math.Pow(10.0, (r.GetD("PWR_ANT") / 10.0)).Round(3);

                        }
                        IsEnableDiff = true;
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }            
            //  FmDgDiffEquipment.Load();
        }

        public override void Save()
        {
           
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaDiffFmDigital, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,FM_DIGITAL_ID, POS_ID,EQUIP_ID,ANT_ID,ANT_DIR,POS_ID,POS_TABLE,CERT_DATE,CERT_NUM,DESIG_EM,BW,INACTIVE");
                r.Select("POLAR,ANT_DIR,ANT_HEIGHT,REMARK,POWER,BLOCK_STR,OFFSET,FREQS_CENTRAL,COMMENTS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    r.Put("ID", Id);
                    r.Put("FM_DIGITAL_ID", Id);
                }
                else // 
                    r.Edit();

                r.Put("INACTIVE", 0);
                if (Position != null)
                {
                    r.Put("POS_ID", Position.Id);
                    r.Put("POS_TABLE", Position.TableName);
                }

                r.Put("EQUIP_ID", FmDgDiffEquipment.Id);

                r.Put("OFFSET", ShiftOstov);

                r.Put("CERT_DATE", FmDgDiffEquipment.Certificate.Date);
                r.Put("CERT_NUM", FmDgDiffEquipment.Certificate.Symbol);
                r.Put("DESIG_EM", FmDgDiffEquipment.DesigEmission);
                r.Put("BW", FmDgDiffEquipment.Bandwidth);
                r.Put("POWER", Power[PowerUnits.dBm]);
                r.Put("POLAR", Polar);
                r.Put("ANT_DIR", Direct);
                r.Put("COMMENTS",StatComment);
                r.Put("ANT_HEIGHT", HeightAnten);
                r.Put("BLOCK_STR", Block);
                r.Put("FREQS_CENTRAL", Centr);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        public void Remove()
        {
            const string tableNameLocal = PlugTbl.itblXnrfaDiffFmDigital;

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(tableNameLocal, IMRecordset.Mode.ReadWrite);
                r.Select("ID,INACTIVE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("INACTIVE", 1);
                    r.Update();
                }
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                    r = null;
                }
            }
        }
    }
}
