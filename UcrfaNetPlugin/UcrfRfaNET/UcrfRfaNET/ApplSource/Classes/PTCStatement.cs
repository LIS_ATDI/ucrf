﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource
{
    // Primary Technical Control Statement
    // Акт ПТК    
    internal class PTCStatement
    {
        // група.
        public class Team : ICloneable
        {
            public int ChiefID { get; set; }
            public int[] MembersID = new int[3];

            public Team()
            {
                ChiefID = IM.NullI;
                ClearMembers();
            }

            public bool IsMemberID(int id)
            {
                return MembersID.Contains(id);
            }

            public object Clone()
            {
                Team clone = new Team();
                clone.ChiefID = ChiefID;
                clone.MembersID = MembersID.Clone() as int[];
                return clone;                
            }

            public void ClearMembers()
            {
                for (int i = 0; i < MembersID.Length; i++)
                    MembersID[i] = IM.NullI;
            }

            public override bool Equals(object team)
            {
                Team objTeam = team as Team;
                if (objTeam != null)
                {
                    if (objTeam.ChiefID != ChiefID)
                        return false;

                    if (MembersID.Length!=objTeam.MembersID.Length)
                        return false;

                    for (int i = 0; i < MembersID.Length;i++)
                        if (objTeam.MembersID[i]!=MembersID[i])                        
                            return false;

                    return true;
                } 
                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        };

        private Team _team = new Team();

        public Team Comittee { get { return _team; } }
        public int Id { get; set; }
        public string TableName { get; set; }

        // Особливы умови
        public string Specials { get;  set; }

        public string ResultsNV { get; set; }
        public string ResultsTV { get; set; }
        public string ResultsParams { get; set; }
        public string ResultsInstrumental { get; set; }

        public string GeneralTotal { get; set; }

        public string ProtocolParamsLink { get; set; }
        public string ProtocolInstrumentalLink { get; set; }
        public string LastLink { get; set; }        

        public void Load()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    Specials = r.GetS("TOTAL");
                    
                    Comittee.ChiefID = r.GetI("CHIEF_ID");
                    Comittee.MembersID[0] = r.GetI("MEMBER1_ID");
                    Comittee.MembersID[1] = r.GetI("MEMBER2_ID");
                    Comittee.MembersID[2] = r.GetI("MEMBER3_ID");
                    ResultsNV = r.GetS("RNV");
                    ResultsTV = r.GetS("RTV");
                    ResultsParams = r.GetS("RMP");
                    ResultsInstrumental = r.GetS("RITMP");
                    GeneralTotal = r.GetS("GENERAL");
                    ProtocolInstrumentalLink = r.GetS("PATH1");
                    ProtocolParamsLink = r.GetS("PATH2");
                    LastLink = r.GetS("LASTPATH");   
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();

                r.Destroy();
            }
            
        }

        public void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {                
                r.Select("ID,TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    r.Edit();                    
                    r.Put("TOTAL", Specials);
                    r.Put("CHIEF_ID", Comittee.ChiefID);
                    r.Put("MEMBER1_ID", Comittee.MembersID[0]);
                    r.Put("MEMBER2_ID", Comittee.MembersID[1]);
                    r.Put("MEMBER3_ID", Comittee.MembersID[2]);
                    r.Put("RNV", ResultsNV);
                    r.Put("RTV", ResultsTV);
                    r.Put("RMP", ResultsParams);
                    r.Put("RITMP", ResultsInstrumental);
                    r.Put("GENERAL", GeneralTotal);
                    r.Put("PATH1", ProtocolInstrumentalLink);
                    r.Put("PATH2", ProtocolParamsLink);
                    r.Put("LASTPATH", LastLink);
                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();                
            }             
        }
    }
}
