﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class AuxiliaryEquipmentList
    {        
        // Класс для списка сертификатов дополнительного оборудования
        // приспособлен для биндинга с комбобоксом
            
        private ComboBoxDictionaryList<int, string> _overallDictionary = new ComboBoxDictionaryList<int, string>();

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadAuxiliatyEquipmentList()
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.AuxiliaryEquipment, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME");                
                r.Open();
                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");
                                            
                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Вытащить список с базы данных, отфильтровать по провинции
        /// </summary>
        public void ReadAuxiliatyEquipmentList(string province)
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.AuxiliaryEquipment, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,PROVINCE");

                if (!string.IsNullOrEmpty(province))
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, province);

                r.Open();
                
                while (!r.IsEOF())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");

                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(id, name));

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Получить рещультатт в виде удобном бля биндинга
        /// </summary>
        /// <returns></returns>
        public ComboBoxDictionaryList<int, string> GetComboBoxDictionaryList()
        {
            return _overallDictionary;
        }
    }
}
