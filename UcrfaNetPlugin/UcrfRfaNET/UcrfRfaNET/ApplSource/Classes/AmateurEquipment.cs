﻿using System;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.HelpClasses;
//XICSM.UcrfRfaNET.ApplSource.Classes
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    public class AmateurEquipment : Equipment
    {
        private Power _power = new Power();

        public int Number;

        public Power Power { get { return _power; } }
        public double Quantity { get; set; }        
        public double Bandwidth { get; set; }
        public string PlantNumb { get; set; }
        public string Devi { get; set; }
        public string Passing { get; set; }

        public int RefId { get; set; }  // Amateur Station reference record ID
        public int StaId { get; set; }  // Amateur Station record ID

        //Cмуги частот
        public List<AmateurBand> AmBand { get; set; }
        //Частоти
        public List<AmateurFreq> AmFreq { get; set; }

        public AmateurEquipment()
        {
            Id = IM.NullI;
            RefId = IM.NullI;
            StaId = IM.NullI;
            Number = 0;
            Name = "";
            DesigEmission = "";
            PlantNumb = "";
            AmBand = new List<AmateurBand>();
            AmFreq=new List<AmateurFreq>();
        }

        public void Load(int id)
        {
            Id = id;
            Load();
        }

        public override void Load()
        {
            IMRecordset r = new IMRecordset(AmateurStation.TableNameEquip, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME,DESIG_EMISSION");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            if (!r.IsEOF())
            {
                Name = r.GetS("NAME");
                DesigEmission = r.GetS("DESIG_EMISSION");
            }
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(AmateurStation.TableNameEquip, IMRecordset.Mode.ReadWrite);
            r.Select("ID,NAME,DESIG_EMISSION,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("DATE_MODIFIED", DateTime.Now);
                    r.Put("MODIFIED_BY", IM.ConnectedUser());
                }
                else
                {
                    r.AddNew();
                    r.Put("ID", IM.AllocID(AmateurStation.TableNameEquip, 1, -1));
                    r.Put("DATE_CREATED", DateTime.Now);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                }
                r.Put("NAME", Name);
                r.Put("DESIG_EMISSION", DesigEmission);
                r.Update();
            }
            finally
            {
                r.Final();
            }

        }

        internal void SaveRef(IMRecordset rEquip, bool forceNew = false)
        {
            if (rEquip.IsEOF() || forceNew)
                rEquip.AddNew();
            else
            {
                rEquip.Edit();

                //public const string TableNameFe = "";
                //public const string TableNameFreq = "";
                IM.Execute("delete from %XFA_FE_AMATEUR where SEL_EQUIP_ID = " + RefId.ToString());

                //delete freqs  (if record still exists - freqs will be recreated)
                IM.Execute("delete from %XFA_FREQ_AMATEUR where FREQ_ID = " + RefId.ToString());
            }

            rEquip.Put("ID", RefId);
            rEquip.Put("STA_ID", StaId);
            rEquip.Put("NUMB", Number);
            rEquip.Put("EQUIP_ID", Id);
            rEquip.Put("PLANT_NUMB", PlantNumb);
            rEquip.Put("POWER", Power[PowerUnits.W]);
            rEquip.Put("DEVI", Devi);
            rEquip.Put("PASSING", Passing);
            rEquip.Update();

            // new bands       
            for (int j = 0; j < AmBand.Count; j++)
            {
                AmBand[j].EquipId = RefId;
                AmBand[j].Save();
            }
            
            // new freqs
            for (int j = 0; j < AmFreq.Count; j++)
            {
                AmFreq[j].EquipId = RefId;
                AmFreq[j].Save();
            }
        }
    }
}
