﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    /// <summary>
    /// Класс для роботи з ЦОБ
    /// </summary>
    class XmlAllStation
    {
        public Dictionary<string,string> _fieldsDict = new Dictionary<string,string>();
        public List<string> _naborList = new List<string>();
        public List<string> _entityList = new List<string>();
        public List<string> _entityCreateList = new List<string>();

        public List<string> checkList = new List<string>();      

        /// <summary>
        /// Конструктор
        /// </summary>
        public XmlAllStation()
        {            
            IMRecordset r = new IMRecordset(PlugTbl.XfaNabEnt, IMRecordset.Mode.ReadOnly);
            r.Select("ID,EXPORT_ID,ENTITY,NAME");            
            _naborList.Clear();
            _entityCreateList.Clear();            
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
                _naborList.Add(r.GetS("NAME"));
                _entityCreateList.Add(r.GetS("ENTITY"));                
            }
            r.Final();
        }
        
        public string Nabor { get; set; }
        public string Fields { get; set; }
        public string Entity { get; set; }

        /// <summary>
        /// Отримати список всіх полів з таблиці
        /// </summary>
        /// <param name="tableName">назва таблиці</param>
        /// <returns>список полів</returns>
        public void GetAllFields(string tableName)
        {
            Dictionary<string,string> tempDict = new Dictionary<string,string>();
            OrmTable ormTab = OrmSchema.Table(tableName);
            OrmField[] fldMas = ormTab.Fields;

            IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            rs.Select("ID");
            foreach (OrmField fldCicle in fldMas)
            {
                if (fldCicle == null)
                    continue;
                if (fldCicle is OrmFieldF)
                {
                    OrmFieldF fld = fldCicle as OrmFieldF;
                    if ((fld.DDesc != null) &&
                        (fld.IsCompiledInEdition) &&
                        (fld.Nature == OrmFieldNature.Column) &&
                        ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                        (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                        (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)))
                    {
                        try
                        {
                            tempDict.Add(fld.Name, CLocaliz.TxT(fld.Name));
                        }
                        catch (Exception ex)
                        {
                            CLogs.WriteError(ELogsWhat.AutoXml, ex);
                        }
                    }
                }
            }
            _fieldsDict = tempDict;
        }

        /// <summary>
        /// Завантаження всіх пар "набір/сутність"
        /// </summary>
        /// <returns>словник пар "набір/сутність"</returns>
        public Dictionary<string, string> LoadNabor()
        {
            Dictionary<string, string> tempDict = new Dictionary<string, string>();
            for (int i = 0; i < _entityCreateList.Count; i++)
                tempDict.Add(_naborList[i], _entityCreateList[i]);
            return tempDict;
        }

        /// <summary>
        /// Збереження набору в БД
        /// </summary>
        public void SaveNabor()
        {
            int id = 0;
            //1. Збереження відмічених полів
            IMRecordset r = new IMRecordset(PlugTbl.XfaExportRchp, IMRecordset.Mode.ReadWrite);
            r.Select("ID,VALUES");
            r.Open();
            try
            {
                r.AddNew();
                id = IM.AllocID(PlugTbl.XfaExportRchp, 1, -1);
                r.Put("ID", id);
                r.Put("VALUES", Fields);
                r.Update();
            }
            finally
            {
                r.Final();
            }
            //2. Збереження набору
            r = new IMRecordset(PlugTbl.XfaNabEnt, IMRecordset.Mode.ReadWrite);
            r.Select("ID,EXPORT_ID,NAME,ENTITY");
            r.Open();
            try
            {
                r.AddNew();
                r.Put("ID", IM.AllocID(PlugTbl.XfaNabEnt, 1, -1));
                r.Put("EXPORT_ID", id);
                r.Put("NAME", Nabor);
                r.Put("ENTITY", Entity);
                r.Update();
            }
            finally
            {
                r.Final();
            }
        }

        //Завантаження всіх полів, які будуть формуватися у XML файл(всі відмічені)
        public void LoadNaborChecked()
        {
            int idExp = IM.NullI;            
            IMRecordset r = new IMRecordset(PlugTbl.XfaNabEnt, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME,EXPORT_ID,ENTITY");
            r.SetWhere("NAME", IMRecordset.Operation.Eq, Nabor);
            r.Open();
            try
            {                
                if (!r.IsEOF())
                {
                    Entity = r.GetS("ENTITY");
                    idExp = r.GetI("EXPORT_ID");
                }
            }
            finally
            {
                r.Final();
            }
            checkList.Clear();
            r = new IMRecordset(PlugTbl.XfaExportRchp, IMRecordset.Mode.ReadOnly);
            r.Select("ID,VALUES");
            r.SetWhere("ID", IMRecordset.Operation.Eq, idExp);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    checkList.AddRange(r.GetS("VALUES").Split(',').ToList());
                }
            }
            finally
            {
                r.Final();
            }            
        }

        public void DeleteNabor()
        {
            int idDel = IM.NullI;
            IMRecordset r = new IMRecordset(PlugTbl.XfaNabEnt, IMRecordset.Mode.ReadWrite);
            r.Select("ID,NAME,EXPORT_ID,ENTITY");
            r.SetWhere("NAME", IMRecordset.Operation.Eq, Nabor);
            r.Open();
            try
            {
                if (!r.IsEOF())
                {
                    idDel = r.GetI("EXPORT_ID");
                    r.Delete();
                }
            }
            finally
            {
                r.Final();
            }

            r=new IMRecordset(PlugTbl.XfaExportRchp,IMRecordset.Mode.ReadWrite);
            r.Select("ID");
            r.SetWhere("ID",IMRecordset.Operation.Eq,idDel);
            r.Open();
            if (!r.IsEOF())
                r.Delete();
            r.Final();
        }
    }
}
