﻿using System;
using System.Linq;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{

    class FmAnalogStation : ERPStation
    {        
        public Antenna fmStationAntena { get; set; }
        public FmAnalogEquipment Equipment { get; set; }

        public List<string> SystSendList = new List<string>();
        public Dictionary<string, string> InternalStateDict = new Dictionary<string, string>();
        public Dictionary<string, string> ForeignStateDict = new Dictionary<string, string>();

        public int SiteId { get; set; }
      
        public int TransmissionSyst { get; set; }
        //public PositionState Position { get; set; }
        public double ASL { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public double AGL { get; set; }
        public double HeightMax { get; set; }

        public string PositivSignal { get; set; }
        public string InnerState { get; set; }
        public string OuterState { get; set; }
        public double CenterFreq { get; set; }
        public string NumberRez { get; set; }
        public string NameRez { get; set; }

        public List<double> Data { get; set; }
        public List<double> DiagDbl { get; set; }

        public static readonly string TableName = ICSMTbl.itblFM;

        public FmAnalogStation()
        {            
        }

        public override void Save()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadWrite);
            try
            {
                string selectStr = "ID,SITE_ID,EQUIP_ID,PWR_ANT,DESIG_EM,CUST_TXT4,CUST_TXT5,FREQ";
                selectStr += ",CALL_SIGN,ERP_H,ERP_V,AGL,EFHGT_MAX,CUST_NBR1,CUST_NBR2,CUST_TXT7,NAME,POLARIZATION";
                selectStr += ",DIAGH,DIAGV,TRANSMISSION_SYS,BW,ERP_H,ERP_V";
                r.Select(selectStr);
                
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", Id);
                    if (Position.TableName != ICSMTbl.itblPositionBro)
                    {
                        Position.Id = IM.NullI;
                        Position.TableName = ICSMTbl.itblPositionBro;
                        Position.SaveToBD();
                        SiteId = Position.Id;
                    }
                    //else
                    //{
                    //    SiteId = Position.Id;
                    //    Position.SaveToBD();
                    //}
                    r.Put("SITE_ID", Position.Id);
                    r.Put("EQUIP_ID", Equipment.Id);
                    
                    r.Put("DESIG_EM", ClassEmission);                                   

                    r.Put("CUST_TXT4", InnerState);
                    r.Put("CUST_TXT5", OuterState);
                    r.Put("FREQ", CenterFreq);
                    r.Put("CALL_SIGN", PositivSignal);
                    r.Put("AGL", AGL);
                    r.Put("TRANSMISSION_SYS", TransmissionSyst.ToString());
                    r.Put("EFHGT_MAX", HeightMax);
                    r.Put("CUST_NBR1", FeederLength);
                    r.Put("CUST_NBR2", FeederLosses);
                    r.Put("CUST_TXT7", NumberRez);

                    r.Put("PWR_ANT", _power[PowerUnits.dBW]);
                    r.Put("POLARIZATION", Polarization);
                    
                    if (Polarization != "V")
                    {
                        r.Put("DIAGH", HorizontalDiagramm.DiagrammAsString);
                        r.Put("ERP_H", Erp.Horizontal[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGH", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    if (Polarization != "H")
                    {
                        r.Put("DIAGV", VerticalDiagramm.DiagrammAsString);
                        r.Put("ERP_V", Erp.Vertical[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGV", "");
                        r.Put("ERP_V", IM.NullD);
                    }                                        

                    r.Put("BW", Bandwith);
                    r.Put("NAME", NameRez);
                    CJournal.CheckTable(ICSMTbl.itblFM, Id, r, selectStr);                    
                    r.Update();                    
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);

            FillComments(ApplId);            
        }

        public override void OnNewEquipment()
        {
            Bandwith = Equipment.BandWidth;
            ClassEmission = Equipment.DesigEmission;

            //InitEquipment();
            Power[PowerUnits.W] = Equipment.MaxPower[PowerUnits.W];
        }

        public override void Load()
        {
            fmStationAntena = new MobStationAntenna();
            fmStationAntena.TableName = ICSMTbl.itblAntennaBro;

            Equipment = new FmAnalogEquipment();
            Equipment.TableName = ICSMTbl.itblEquipBro;

            fillCombo();

            IMRecordset r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,SITE_ID,EQUIP_ID,PWR_ANT,DESIG_EM,CUST_TXT4,CUST_TXT5,FREQ");
                r.Select("CALL_SIGN,ERP_H,ERP_V,AGL,EFHGT_MAX,CUST_NBR1,CUST_NBR2,CUST_TXT7,NAME,POLARIZATION");
                r.Select("DIAGH,DIAGV,GAIN,TRANSMISSION_SYS,BW,CUST_TXT13");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    HorizontalDiagramm = new GainDiagramm();
                    VerticalDiagramm = new GainDiagramm();
                    Erp = new ERP();

                    Id = r.GetI("ID");
                    SiteId = r.GetI("SITE_ID");

                    Equipment.Id = r.GetI("EQUIP_ID");
                    if (Equipment.Id != IM.NullD)
                        Equipment.Load();

                    string transm = r.GetS("TRANSMISSION_SYS");
                    TransmissionSyst = string.IsNullOrEmpty(transm) ? 0 : Int32.Parse(transm);

                    _power[PowerUnits.dBW] = r.GetD("PWR_ANT");
                    
                    ClassEmission = r.GetS("DESIG_EM");
                    InnerState = r.GetS("CUST_TXT4");
                    OuterState = r.GetS("CUST_TXT5");
                    CenterFreq = r.GetD("FREQ");
                    PositivSignal = r.GetS("CALL_SIGN");                    

                    AGL = r.GetD("AGL");
                    HeightMax = r.GetD("EFHGT_MAX");
                    FeederLength = r.GetD("CUST_NBR1");
                    FeederLosses = r.GetD("CUST_NBR2");
                    NumberRez = r.GetS("CUST_TXT7");
                    NameRez = r.GetS("NAME");

                    Erp.Horizontal[PowerUnits.dBW] = r.GetD("ERP_H");
                    Erp.Vertical[PowerUnits.dBW] = r.GetD("ERP_V");
                    HorizontalDiagramm.DiagrammAsString = r.GetS("DIAGH");
                    VerticalDiagramm.DiagrammAsString = r.GetS("DIAGV");
                    HorizontalDiagramm.Gain = r.GetD("GAIN");
                    VerticalDiagramm.Gain = r.GetD("GAIN");
                    _gain = r.GetD("GAIN");
                    //Висновок
                    Finding = r.GetS("CUST_TXT13");
                    Polarization = r.GetS("POLARIZATION");
                    Bandwith = r.GetD("BW");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            Position = new PositionState();
            Position.LoadStatePosition(SiteId, ICSMTbl.itblPositionBro);
        }

        private void fillCombo()
        {
            SystSendList = CFmTransmissionSystem.getTransmissionSystemList();           

            // Внутрішній стан
            InternalStateDict.Clear();
            InternalStateDict.Add("1", "Рассматриваемый");
            InternalStateDict.Add("2", "Разрешенный");
            InternalStateDict.Add("3", "Действующий");
            InternalStateDict.Add("4", "Новое сост.8");
            InternalStateDict.Add("5", "Новое сост.2");
            InternalStateDict.Add("6", "Новое сост.3");
            InternalStateDict.Add("7", "Столб");
            InternalStateDict.Add("8", "Рекоммендация");
            InternalStateDict.Add("9", "Уточнение хар-к");
            InternalStateDict.Add("10", "Планируемый");
            InternalStateDict.Add("33", "BR IFIC ADD");
            InternalStateDict.Add("34", "BR IFIC REC");
            InternalStateDict.Add("35", "BR IFIC SUP");
            InternalStateDict.Add("36", "BR IFIC MOD");
            InternalStateDict.Add("38", "Geneva 06");

            // Міжнародний стан           
            ForeignStateDict.Clear();
            ForeignStateDict.Add("A", "Скоорд.");
            ForeignStateDict.Add("B", "Берлин 59");
            ForeignStateDict.Add("C", "Чужой скоорд.");
            ForeignStateDict.Add("D", "Удаленный, был.");
            ForeignStateDict.Add("E", "Цифра, планируем.");
            ForeignStateDict.Add("F", "Прошел коорд.");
            ForeignStateDict.Add("G", "Женева 84");
            ForeignStateDict.Add("G06", "GENEVA 2006");
            ForeignStateDict.Add("H", "Цифра, план Честер");
            ForeignStateDict.Add("I", "Честер");
            ForeignStateDict.Add("K", "На коорд.");
            ForeignStateDict.Add("L", "Цифра, удаленный");
            ForeignStateDict.Add("M", "Цифра, не надо коорд.");
            ForeignStateDict.Add("N", "Не надо коорд.");
            ForeignStateDict.Add("O", "Планируемый");
            ForeignStateDict.Add("P", "Под крышей");
            ForeignStateDict.Add("Q", "Цифра, регистр.");
            ForeignStateDict.Add("R", "Регистр");
            ForeignStateDict.Add("S", "Стокгольм");
            ForeignStateDict.Add("T", "Цифра, скоорд.");
            ForeignStateDict.Add("TP", "Цифра, скоорд. на пп");
            ForeignStateDict.Add("U", "Цифра на коорд.");
            ForeignStateDict.Add("Y", "Цифра, нескоорд.");
            ForeignStateDict.Add("Z", "Не скоорд.");
        }
    }
}
