﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using Lis.CommonLib.Extensions;
using Lis.CommonLib.Binding; 

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class MeasureEquipmentCertificateList
    {
        // Класс для списка сертификатов измерительного оборудования
        // приспособлен для биндинга с комбобоксом
        //private readonly string vrzString = Enum.GetName(DepartmentType.VRZ.GetType(), DepartmentType.VRZ);
        private ComboBoxDictionaryList<int, string> _overallDictionary = new ComboBoxDictionaryList<int, string>();

        /// <summary>
        /// Вытащить список с базы данных, нечего более
        /// </summary>
        public void ReadMeasurementCertificateList(string province)
        {
            _overallDictionary.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaMeasureEquipmentCertificates, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,CERT_NUM,CERT_DATE,CERTIFIED_BY,PROVINCE");

                if (!string.IsNullOrEmpty(province))
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, province);
                                
                //r.OrderBy("LASTNAME", OrderDirection.Ascending);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");

                    string number = r.GetS("CERT_NUM");
                    DateTime when = r.GetT("CERT_DATE");
                    string certifiedBy = r.GetS("CERTIFIED_BY");

                    string certificateText =
                        "Свідоцтво про атестацію №" + number +
                        " Видане " + when.ToShortDateStringNullT() +
                        " " + certifiedBy;
                                                
                    _overallDictionary.Add(new ComboBoxDictionary<int, string>(ID, certificateText));


                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Получить рещультатт в виде удобном бля биндинга
        /// </summary>
        /// <returns></returns>
        public ComboBoxDictionaryList<int, string> GetComboBoxDictionaryList()
        {
            return _overallDictionary;
        }
    }
}
