﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class TvDigitallStation : ERPStation
    {
        public List<string> SystSendList = new List<string>();
        public Dictionary<string, string> InnerStateDict = new Dictionary<string, string>();
        public Dictionary<string, string> OuterStateDict = new Dictionary<string, string>();
        public List<string> CountNesivList = new List<string>();
        public Dictionary<string, string> SafeIntervDict = new Dictionary<string, string>();
        public List<string> SpectrMaskList = new List<string>();
        public Dictionary<string, string> TypeReceiveDict = new Dictionary<string, string>();
        public Dictionary<string, string> VariantSystDict = new Dictionary<string, string>();
        public List<string> StandComprList = new List<string>();
        public List<string> ChannelList = new List<string>();
        public List<Double> FreqList = new List<Double>();

        public List<string> ContourList = new List<string>();
        public List<string> NameSelectList = new List<string>();

        //public static readonly string TableName = ICSMTbl.itblDVBT;

        //public PositionState Position { get; set; }
        public TvDigitallEquipment TvDgEquipment { get; set; }
        public TvDigitallAntenna TvDgAntenna { get; set; }

        public int SiteId { get; set; }
        public int AssgnId { get; set; }
        public int EquipId { get; set; }
        public int PlanId { get; set; }
        public string NameNumber { get; set; }
        public string Countur { get; set; }
        public string NameSynchr { get; set; }
        public string NameSelect { get; set; }
        public string NameOchm { get; set; }
        public string NameVariant { get; set; }
        public string CountOstov { get; set; }
        public int SafeInterv { get; set; }
        public double LoseFiltr { get; set; }
        public string SpectrMaskFiltr { get; set; }
        public string TypeReceive { get; set; }
        public string StandCompr { get; set; }
        public double ShiftOstov { get; set; }

        public string NameRez { get; set; }
        public double PowerTx { get; set; }
        public double Bandwith { get; set; }
        public string ClassEmission { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CertificateDate { get; set; }

        public double HeightAnten { get; set; }
        public double MaxHigh { get; set; }

        public string InnerState { get; set; }
        public string OuterState { get; set; }

        public string TvChannel { get; set; }
        public double FreqCentr { get; set; }

        public int ObjRCHP { get; set; }

        public static readonly string TableName = ICSMTbl.itblDVBT;

        public TvDigitallStation()
        {            
        }

        public override void Load()
        {
            //Заповнення комбо боксів
            FillCombo();

            TvDgEquipment = new TvDigitallEquipment();         

            IMRecordset r = new IMRecordset(ICSMTbl.itblDVBT, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,SITE_ID,CUST_TXT7,CODE,ADM_KEY,SFN_IDENT,DVBT_SYS_CODE,PWR_ANT");                
                r.Select("NB_CARR,GUARD_INTERVAL,TX_LOSSES,CUST_NBR1,CUST_NBR2,ASSGN_ID,EQUIP_ID");
                r.Select("CUST_NBR1,CUST_NBR2,POLARIZATION,ERP_H,ERP_V,DIAGH,DIAGV,AGL,EFHGT_MAX");
                r.Select("PLAN_ID,CHANNEL,FREQ,CUST_TXT4,CUST_TXT5,EQUIP_ID,OFFSET,GAIN,CUST_TXT9");
                r.Select("PWR_ANT,BW,DESIG_EM");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    Id = r.GetI("ID");
                    SiteId = r.GetI("SITE_ID");
                    PlanId = r.GetI("PLAN_ID");
                    AssgnId = r.GetI("ASSGN_ID");
                    TvDgEquipment.Id = r.GetI("EQUIP_ID");
                    if (TvDgEquipment.Id != IM.NullI)
                        TvDgEquipment.Load();
                    TvDgEquipment.DesigEmission = r.GetS("DESIG_EM");
                    TvDgEquipment.MaxPower[PowerUnits.dBW] = r.GetD("PWR_ANT");
                    TvDgEquipment.Bandwidth = r.GetD("BW")/1000;
                    NameNumber = r.GetS("CUST_TXT7");
                    Countur = r.GetS("ADM_KEY");
                    if (string.IsNullOrEmpty(Countur))
                        Countur = "0";
                    NameSynchr = r.GetS("CODE");                   
                    NameSelect = Countur;
                    NameOchm = r.GetS("SFN_IDENT");
                    NameVariant = r.GetS("DVBT_SYS_CODE");
                    CountOstov = r.GetS("NB_CARR");
                    SafeInterv = r.GetI("GUARD_INTERVAL");
                    LoseFiltr = r.GetD("TX_LOSSES") - r.GetD("CUST_NBR1") * r.GetD("CUST_NBR2");
                    StandCompr = r.GetS("CUST_TXT7");
                    FeederLength = r.GetD("CUST_NBR1");
                    FeederLosses = r.GetD("CUST_NBR2");
                    Polarization = r.GetS("POLARIZATION");
                    ShiftOstov = r.GetD("OFFSET");

                    Power[PowerUnits.dBW] = r.GetD("PWR_ANT");

                    Erp.Horizontal[PowerUnits.dBW] = r.GetD("ERP_H");
                    Erp.Vertical[PowerUnits.dBW] = r.GetD("ERP_V");
                    HorizontalDiagramm.DiagrammAsString = r.GetS("DIAGH");
                    VerticalDiagramm.DiagrammAsString = r.GetS("DIAGV");
                    HorizontalDiagramm.Gain = r.GetD("GAIN");
                    VerticalDiagramm.Gain = r.GetD("GAIN");
                    _gain = r.GetD("GAIN");

                    HeightAnten = r.GetD("AGL");
                    MaxHigh = r.GetD("EFHGT_MAX");

                    TvChannel = r.GetS("CHANNEL");
                    FreqCentr = r.GetD("FREQ");
                    InnerState = r.GetS("CUST_TXT4");
                    OuterState = r.GetS("CUST_TXT5");
                    ObjRCHP = r.GetI("ID");
                    //Висновок
                    Finding = r.GetS("CUST_TXT9");
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            Position = new PositionState();
            Position.LoadStatePosition(SiteId, ICSMTbl.itblPositionBro);
            NameRez = Position.Name;

            //Формування номеру контуру
            IMRecordset rCountur = null;
            try
            {
                rCountur = new IMRecordset(ICSMTbl.itblItuContour, IMRecordset.Mode.ReadOnly);
                rCountur.Select("NUM,ADM");
                rCountur.SetWhere("ADM", IMRecordset.Operation.Like, "UKR");
                rCountur.OrderBy("NUM", OrderDirection.Ascending);
                for (rCountur.Open(); !rCountur.IsEOF(); rCountur.MoveNext())
                {
                    ContourList.Add(rCountur.GetI("NUM").ToString());
                }
                Countur = ADMParseNumber(Countur);

            }
            finally
            {
                if (rCountur.IsOpen())
                    rCountur.Close();
                rCountur.Destroy();
            }

            //Формування Назви виділення
            if (!string.IsNullOrEmpty(NameSynchr))
            {
                string a_name = NameSynchr;
                IMRecordset rSelect = null;
                try
                {
                    rSelect = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadOnly);
                    rSelect.Select("IS_ALLOTM,CLASS,A_NAME");
                    rSelect.SetWhere("CLASS", IMRecordset.Operation.Like, "BT");
                    rSelect.SetWhere("IS_ALLOTM", IMRecordset.Operation.Like, "Y");
                    rSelect.SetWhere("A_NAME", IMRecordset.Operation.Like, "UKR" + a_name + "*");
                    for (rSelect.Open(); !rSelect.IsEOF(); rSelect.MoveNext())
                    {
                        NameSelectList.Add(rSelect.GetS("A_NAME"));
                    }
                }
                finally
                {
                    if (rSelect.IsOpen())
                        rSelect.Close();
                    rSelect.Destroy();
                }
            }
            //ASSIGN
            IMRecordset rAssgn = new IMRecordset(ICSMTbl.itblFmtvAssign, IMRecordset.Mode.ReadOnly);
            try
            {
                rAssgn.Select("ID,RX_MODE");
                rAssgn.SetWhere("ID", IMRecordset.Operation.Eq, AssgnId);
                rAssgn.Open();
                if (!rAssgn.IsEOF())
                {
                    TypeReceive = rAssgn.GetS("RX_MODE");
                }
            }
            finally
            {
                if (rAssgn.IsOpen())
                    rAssgn.Close();
                rAssgn.Destroy();
            }
            //Обладнання 

            NameRez = TvDgEquipment.Name;

            PowerTx = TvDgEquipment.MaxPower[PowerUnits.dBm];
            ClassEmission = TvDgEquipment.DesigEmission;
            Bandwith = TvDgEquipment.Bandwidth;
            CertificateDate = TvDgEquipment.Certificate.Date;
            CertificateNumber = TvDgEquipment.Certificate.Symbol;


            //ТВ канал
            if (PlanId != IM.NullI)
            {
                //cell.Value = station.objChannel.GetS("CHANNEL");

                IMRecordset rChann = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                rChann.Select("PLAN_ID,CHANNEL,FREQ,OFFSET");
                rChann.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, PlanId);
                for (rChann.Open(); !rChann.IsEOF(); rChann.MoveNext())
                {
                    ChannelList.Add(rChann.GetS("CHANNEL"));

                    double offset = 0;
                    double freq = 0;

                    if (rChann.GetD("OFFSET") != IM.NullD)
                        offset = rChann.GetD("OFFSET");

                    if (rChann.GetD("FREQ") != IM.NullD)
                        freq = rChann.GetD("FREQ");

                    freq = freq + offset / 1000;

                    FreqList.Add(freq);
                }
                if (rChann.IsOpen())
                    rChann.Close();
                rChann.Destroy();


                int index = ChannelList.IndexOf(TvChannel);
                double freVideo = 0.0;
                try
                {
                    freVideo = FreqList[index];
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                FreqCentr = freVideo;
            }
            else
            {
                StringBuilder idChannel = new StringBuilder();
                IMRecordset rChann = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                rChann.Select("ID,COD");
                rChann.SetWhere("COD", IMRecordset.Operation.Like, "*DVB-T*");
                for (rChann.Open(); !rChann.IsEOF(); rChann.MoveNext())
                {
                    idChannel.Append(',');
                    idChannel.Append(rChann.GetI("ID").ToString());
                }
                if (rChann.IsOpen())
                    rChann.Close();
                rChann.Destroy();

                if (idChannel.Length == 0)
                    return;

                idChannel = idChannel.Remove(0, 1);
                List<int> channelIDList = new List<int>();

                rChann = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                rChann.Select("PLAN_ID,CHANNEL,FREQ,OFFSET");
                rChann.SetAdditional("[PLAN_ID] in " + "(" + idChannel.ToString() + ")");
                for (rChann.Open(); !rChann.IsEOF(); rChann.MoveNext())
                {
                    ChannelList.Add(rChann.GetS("CHANNEL"));
                    channelIDList.Add(rChann.GetI("PLAN_ID"));

                    double offset = 0;
                    double freq = 0;

                    if (rChann.GetD("OFFSET") != IM.NullD)
                        offset = rChann.GetD("OFFSET");

                    if (rChann.GetD("FREQ") != IM.NullD)
                        freq = rChann.GetD("FREQ");

                    freq = freq - offset;

                    FreqList.Add(freq);
                }

                if (rChann.IsOpen())
                    rChann.Close();
                rChann.Destroy();
            }


        }

        //Визначення спрямованості антени
        public string SetDirection(string diagH, string diagV)
        {
            string direction = "ND";
            if (!(string.IsNullOrEmpty(diagH) && string.IsNullOrEmpty(diagV)))
            {
                if ((diagV.Equals("OMNI") && (diagH.Equals("OMNI") || string.IsNullOrEmpty(diagH))) ||
                      (diagH.Equals("OMNI") && string.IsNullOrEmpty(diagV)))
                    direction = "ND";
                else
                    direction = "D";
            }
            return direction;
        }

        private void FillCombo()
        {
            // fills Внутрішній стан 
            InnerStateDict.Clear();
            InnerStateDict.Add("1", "Рассматриваемый");
            InnerStateDict.Add("2", "Разрешенный");
            InnerStateDict.Add("3", "Действующий");
            InnerStateDict.Add("4", "Новое сост.8");
            InnerStateDict.Add("5", "Новое сост.2");
            InnerStateDict.Add("6", "Новое сост.3");
            InnerStateDict.Add("7", "Столб");
            InnerStateDict.Add("8", "Рекоммендация");
            InnerStateDict.Add("9", "Уточнение хар-к");
            InnerStateDict.Add("10", "Планируемый");
            InnerStateDict.Add("33", "BR IFIC ADD");
            InnerStateDict.Add("34", "BR IFIC REC");
            InnerStateDict.Add("35", "BR IFIC SUP");
            InnerStateDict.Add("36", "BR IFIC MOD");
            InnerStateDict.Add("38", "Geneva 06");

            // Міжнародний стан
            OuterStateDict.Clear();
            OuterStateDict.Add("A", "Скоорд.");
            OuterStateDict.Add("B", "Берлин 59");
            OuterStateDict.Add("C", "Чужой скоорд.");
            OuterStateDict.Add("D", "Удаленный, был.");
            OuterStateDict.Add("E", "Цифра, планируем.");
            OuterStateDict.Add("F", "Прошел коорд.");
            OuterStateDict.Add("G", "Женева 84");
            OuterStateDict.Add("G06", "GENEVA 2006");
            OuterStateDict.Add("H", "Цифра, план Честер");
            OuterStateDict.Add("I", "Честер");
            OuterStateDict.Add("K", "На коорд.");
            OuterStateDict.Add("L", "Цифра, удаленный");
            OuterStateDict.Add("M", "Цифра, не надо коорд.");
            OuterStateDict.Add("N", "Не надо коорд.");
            OuterStateDict.Add("O", "Планируемый");
            OuterStateDict.Add("P", "Под крышей");
            OuterStateDict.Add("Q", "Цифра, регистр.");
            OuterStateDict.Add("R", "Регистр");
            OuterStateDict.Add("S", "Стокгольм");
            OuterStateDict.Add("T", "Цифра, скоорд.");
            OuterStateDict.Add("TP", "Цифра, скоорд. на пп");
            OuterStateDict.Add("U", "Цифра на коорд.");
            OuterStateDict.Add("Y", "Цифра, нескоорд.");
            OuterStateDict.Add("Z", "Не скоорд.");

            // кількість несівних 
            CountNesivList.Clear();
            CountNesivList = CComboList.GetListValuesFromEri("DvbtNbCarr");


            // захисний інтервал
            SafeIntervDict.Clear();
            SafeIntervDict = CComboList.GetDictValuesFromEri("DvbtGuardInt");

            // тип спектр. маски фільтра 
            SpectrMaskList.Clear();
            SpectrMaskList.Add("не критична");
            SpectrMaskList.Add("критична");

            // тип прийома
            TypeReceiveDict.Clear();
            TypeReceiveDict.Add("MO", "мобільний");
            TypeReceiveDict.Add("FX", "фіксований");
            TypeReceiveDict.Add("PO", "портат. зовн.");
            TypeReceiveDict.Add("PI", "портат. внутр.");

            // Варіант системи   
            VariantSystDict.Clear();
            VariantSystDict = CComboList.GetDictValuesFromEri("DvbtSys");

            //Стандарт компресії
            StandComprList.Clear();
            StandComprList.Add("MPEG2");
            StandComprList.Add("MPEG4");
        }

        private string ADMParseNumber(string admKey)
        {
            if (string.IsNullOrEmpty(admKey))
                return string.Empty;

            int lenght = admKey.LastIndexOf('0') - 3;
            if (lenght <= 0)
                return string.Empty;

            string number = admKey.Substring(3, lenght);

            return number;
        }



        public override void Save()
        {
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,SITE_ID,CUST_TXT7,CODE,ADM_KEY,SFN_IDENT,DVBT_SYS_CODE,CHANNEL,FREQ");
                r.Select("NB_CARR,GUARD_INTERVAL,TX_LOSSES,CUST_NBR1,CUST_NBR2,POLARIZATION,EFHGT_MAX");
                r.Select("CUST_TXT4,CUST_TXT5,EQUIP_ID,OFFSET,PWR_ANT,DESIG_EM,BW");
                r.Select("DIAGV,ERP_V,DIAGH,ERP_H,AGL");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("ID", Id);
                    if (Position.TableName != ICSMTbl.itblPositionBro)
                    {
                        Position.Id = IM.NullI;
                        Position.TableName = ICSMTbl.itblPositionBro;
                        Position.SaveToBD();
                        SiteId = Position.Id;
                    }
                    //else
                    //{
                    //    SiteId = Position.Id;
                    //    Position.SaveToBD();
                    //}
                    r.Put("SITE_ID", Position.Id);
                    r.Put("EQUIP_ID", TvDgEquipment.Id);
                    r.Put("CUST_TXT7", NameNumber);
                    r.Put("CODE", NameSynchr);
                    r.Put("ADM_KEY", NameSelect);
                    r.Put("SFN_IDENT", NameOchm);
                    r.Put("DVBT_SYS_CODE", NameVariant);
                    r.Put("NB_CARR", CountOstov);
                    r.Put("GUARD_INTERVAL", SafeInterv);
                    //r.Put("TX_LOSSES", tx_losses + cust_nbr1 * cust_nbr2);
                    r.Put("CUST_NBR1", FeederLength);
                    r.Put("CUST_NBR2", FeederLosses);
                    r.Put("DESIG_EM",TvDgEquipment.DesigEmission);
                    r.Put("BW",TvDgEquipment.Bandwidth*1000);
                    r.Put("PWR_ANT", _power[PowerUnits.dBW]);
                    r.Put("POLARIZATION", Polarization);

                    if (Polarization != "V")
                    {
                        r.Put("DIAGH", HorizontalDiagramm.DiagrammAsString);
                        r.Put("ERP_H", Erp.Horizontal[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGH", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    if (Polarization != "H")
                    {
                        r.Put("DIAGV", VerticalDiagramm.DiagrammAsString);
                        r.Put("ERP_V", Erp.Vertical[PowerUnits.dBW]);
                    }
                    else
                    {
                        r.Put("DIAGV", "");
                        r.Put("ERP_V", IM.NullD);
                    }

                    r.Put("AGL", HeightAnten);
                    r.Put("EFHGT_MAX", MaxHigh);
                    r.Put("CUST_TXT4", InnerState);
                    r.Put("CUST_TXT5", OuterState);
                    r.Put("OFFSET", ShiftOstov);
                    r.Put("CHANNEL", TvChannel);
                    r.Put("FREQ", FreqCentr);
                    r.Put("ID", ObjRCHP);
                    r.Update();
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            //Перейменування станції в DOCLINK...
            RenameStationInTheDoclink(TableName, Id, Position.TableName, Position.Id);

            /*IMRecordset renameStatRec = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            renameStatRec.Select("REC_TAB,REC_ID,PATH");
            renameStatRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, TableName);
            renameStatRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, Id);
            try
            {
                for (renameStatRec.Open(); !renameStatRec.IsEOF(); renameStatRec.MoveNext())
                {
                    renameStatRec.Edit();
                    renameStatRec.Put("REC_TAB", Position.TableName);
                    renameStatRec.Put("REC_ID", Position.Id);
                    renameStatRec.Update();
                }
            }
            finally
            {
                if (renameStatRec.IsOpen())
                    renameStatRec.Close();
                renameStatRec.Destroy();
            }*/

            FillComments(ApplId);

            //Удаление временной строчки...
        }
    }
}
