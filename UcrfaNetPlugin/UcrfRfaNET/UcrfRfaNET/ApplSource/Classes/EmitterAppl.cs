﻿using System.Collections.Generic;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.ApplSource.Forms;

namespace XICSM.UcrfRfaNET.ApplSource.Classes
{
    class EmitterAppl : BaseAppClass
    {

        private EmitterStation emitterStat;
        public const string TableName = PlugTbl.XfaEmitterStation;
        #region Cells

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty cellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty cellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty cellAddress = new CellStringProperty();

        /// <summary>
        /// Висота встановлення над рівнем землі, м
        /// </summary>
        private CellDoubleProperty cellAGL = new CellDoubleProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellDoubleProperty cellName = new CellDoubleProperty();

        /// <summary>
        /// Номер сертифіката
        /// </summary>
        private CellStringProperty cellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката
        /// </summary>
        private CellDateNullableProperty cellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Вид(призначення) випром. пристрою
        /// </summary>
        private CellStringComboboxAplyProperty cellTypeEmi = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Наявність технічних засобів, що зменшують рівень завадового впливу
        /// </summary>
        private CellStringProperty cellTechGadget = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт
        /// </summary>
        private CellDoubleBoundedProperty cellPower = new CellDoubleBoundedProperty();

        /// <summary>
        /// Номінал(и) центральної(их) частот(и), МГц
        /// </summary>
        private CellStringComboboxAplyProperty cellNomCentr = new CellStringComboboxAplyProperty();


        /// <summary>
        /// Ширина смуги випромінювання. МГц
        /// </summary>
        private CellStringProperty cellWideEmi = new CellStringProperty();

        /// <summary>
        /// Смуга випромінювання. МГц
        /// </summary>
        private CellStringProperty cellDesigEmi = new CellStringProperty();

        /// <summary>
        /// Об'єкти РЧП
        /// </summary>
        private CellStringProperty cellObj = new CellStringProperty();
        /// <summary>
        /// 
        /// </summary>
        private List<EmiterBand> listFreq_Sel2 = new List<EmiterBand>();
        private List<EmiterBand> listFreq_All = new List<EmiterBand>();
        private List<EmiterBand> listFreq_Sel = new List<EmiterBand>();

        

        #endregion

        public int ID { get; set; }

        public override int OwnerID
        {
            get
            {
                return emitterStat.OwnerId;
            }
        }

        public EmitterAppl(int id, int ownerId, int packetID, string radioTech)
            : base(id, TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppVP;
            ID = id;
            //if ((ID == 0) || (ID == IM.NullI)) { ID = IM.AllocID(TableName, 1, - 1); }
            emitterStat = new EmitterStation();
            emitterStat.Id = recID;
            emitterStat.Standart = radioTech;
            emitterStat.Load();

            Status.StatusAsString = emitterStat.Status;

            if ((ownerId != 0) && (ownerId != IM.NullI))
                emitterStat.OwnerId = ownerId;
            ReadData();

        }
        public void OnPressButtonChangeFreq(Cell cell)
        {
            FillCentrFreq2();  
        }
        



        private void ReadData()
        {

            cellLongitude.DoubleValue = emitterStat.Position.LonDms;
            cellLatitude.DoubleValue = emitterStat.Position.LatDms;
            cellAddress.Value = emitterStat.Position.FullAddress;

            cellAGL.DoubleValue = emitterStat.AGL;
            if (string.IsNullOrEmpty(emitterStat.Certific.Symbol))
                emitterStat.Certific.Symbol = "";
            cellCertificateNumber.Value = emitterStat.Certific.Symbol;
            cellCertificateDate.DateValue = emitterStat.Certific.Date;
            cellWideEmi.Value = emitterStat.Bw.ToStringNullD();
            cellName.Value = emitterStat.EquipName;

            if (!EriFiles.GetEriCodeList("EmitterType").Contains(emitterStat.EmiType))
                emitterStat.EmiType = EriFiles.GetEriCodeList("EmitterType")[0];
            cellTypeEmi.Items.AddRange(EriFiles.GetEriDescrList("EmitterType"));
            cellTypeEmi.Value = EriFiles.GetEriCodeAndDescr("EmitterType")[emitterStat.EmiType];

            FillCentrFreq();

            cellNomCentr.Value = emitterStat.FreqCentr.ToStringNullD();
            //cellDesigEmi.Value = emitterStat.FreqMin.ToStringNullD() + " - " + emitterStat.FreqMax.ToStringNullD();
            cellWideEmi.Value = emitterStat.Bw.ToStringNullD();

            if (string.IsNullOrEmpty(emitterStat.DopDevice))
                emitterStat.DopDevice = "";
            cellTechGadget.Value = emitterStat.DopDevice;
            cellPower.DoubleValue = emitterStat.Pow[PowerUnits.W].Round(3);
            cellObj.Value = recordID.Id.ToString();
            LoadBand(recordID.Id);
        }


        /// <summary>
        /// Обновление данных поля РДПО владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerRDPO(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,REGIST_NUM");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, emitterStat.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("REGIST_NUM");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }

        //===========================================================
        /// <summary>
        /// Обновление данных поля Названия владельца сайта
        /// </summary>
        /// <param name="textBox">TextBox</param>
        public override void UpdateOwnerName(TextBox textBox)
        {
            IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,NAME");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, emitterStat.OwnerId);
            rs.Open();
            if (!rs.IsEOF())
                textBox.Text = rs.GetS("NAME");
            if (rs.IsOpen())
                rs.Close();
            rs.Destroy();
        }

        /// <summary>
        /// Реакция на нажатия кнопки оформления позиции
        /// Формирование адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(cellLongitude.DoubleValue, cellLatitude.DoubleValue, "4DMS", "4DMS");

            PositionState objPosition = frmSelectPosition.SelectPositionState(PlugTbl.XfaPosition, 1, tmpXy.Longitude, tmpXy.Latitude);

            if (objPosition != null)
            {
                emitterStat.Position = objPosition;
                if (!string.IsNullOrEmpty(emitterStat.Position.FullAddress))
                    cellAddress.Value = emitterStat.Position.FullAddress;
                cellLongitude.DoubleValue = emitterStat.Position.LonDms;
                cellLatitude.DoubleValue = emitterStat.Position.LatDms;
            }/**/
            emitterStat.NeedPositionSave = true;
        }

        /// <summary>
        /// Оборудование 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressEquipment(Cell cell)
        {
            RecordPtr recEquip = SelectEquip("Seach of the equipment", PlugTbl.XvEquip, "NAME", cell.Value, false);
            if ((recEquip.Id > 0) && (recEquip.Id < IM.NullI))
            {
                emitterStat.EmEquip.Id = recEquip.Id;
                emitterStat.EmEquip.Load();
                cellName.Value = emitterStat.EmEquip.Name;
                cellPower.Value = emitterStat.EmEquip.Pow[PowerUnits.W].Round(3).ToStringNullD();
                cellCertificateDate.DateValue = emitterStat.EmEquip.Certificate.Date;
                cellCertificateNumber.Value = emitterStat.EmEquip.Certificate.Symbol;
            }
        }

        public override void DoubleClickOnGrid(Cell cell)
        {
            OnPressButtonAddressName(cell);
        }

        /// <summary>
        /// Реакция на нажатие кнопки создания адреса
        /// Создание нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            //if (EPacketType.PckPTKNVTV != PacketType)
            UcrfDepartment curDepart = CUsers.GetCurDepartment();
            if (curDepart != UcrfDepartment.URZP)
            {
                PositionState2 newPos = new PositionState2();
                int posID = emitterStat.Position.Id;
                if ((emitterStat.Position != null) && (posID != IM.NullI))
                {
                    Forms.AdminSiteAllTech2.Show(emitterStat.Position.TableName, posID, (IM.TableRight(emitterStat.Position.TableName) & IMTableRight.Update) != IMTableRight.Update, OwnerWindows);
                    emitterStat.Position.LoadStatePosition(emitterStat.Position.Id, emitterStat.Position.TableName);

                    if (!string.IsNullOrEmpty(emitterStat.Position.FullAddress))
                        cellAddress.Value = emitterStat.Position.FullAddress;
                    cellLongitude.DoubleValue = emitterStat.Position.LonDms;
                    cellLatitude.DoubleValue = emitterStat.Position.LatDms;
                    emitterStat.NeedPositionSave = true;
                }
               
            }
            else
            {
                
                if (emitterStat.Position != null)
                {
                    FormNewPosition frm = new FormNewPosition(emitterStat.Position.CityId);
                    frm.CanAttachPhoto = false;
                    frm.CanEditPosition = true;
                    frm.CanEditStreet = true;
                    frm.SetLongitudeAsDms(cellLongitude.DoubleValue);
                    frm.SetLatitudeAsDms(cellLatitude.DoubleValue);

                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        emitterStat.Position = frm.LoadNewPosition(PlugTbl.XfaPosition, cellLongitude.DoubleValue, cellLatitude.DoubleValue);
                        cellAddress.Value = emitterStat.Position.FullAddress;
                        cellLatitude.DoubleValue = emitterStat.Position.LatDms;
                        cellLongitude.DoubleValue = emitterStat.Position.LonDms;
                        emitterStat.NeedPositionSave = true;
                    }
                }
            }

        }

        public void OnPressButtonAddressNameCreate(Cell cell)
        {
            //if (EPacketType.PckPTKNVTV != PacketType)
            UcrfDepartment curDepart = CUsers.GetCurDepartment();
            if (curDepart != UcrfDepartment.URZP)
            {
                PositionState2 newPos = new PositionState2();
                if (ShowMessageReference(emitterStat.Position != null))
                {
                    DialogResult dr = Forms.AdminSiteAllTech2.CreateNewPosition(PlugTbl.XfaPosition, ref newPos, OwnerWindows);
                    if (dr == DialogResult.OK)
                    {

                        PositionState stat = new PositionState();
                        stat.Address = newPos.AddressAuto;
                        stat.FullAddress = newPos.FullAddressAuto;
                        stat.LonDms = newPos.LongDms;
                        stat.LatDms = newPos.LatDms;
                        emitterStat.Position = stat;
                        stat.City = newPos.City;
                        stat.CityId = newPos.CityId;
                        stat.Csys = newPos.Csys;
                        stat.Datum = newPos.Datum;
                        stat.DistBorder = newPos.DistBorder;
                        stat.CountryId = newPos.CountryId;
                        stat.CitiesName = newPos.CitiesName;
                        stat.CitiesProvince = newPos.CitiesProvince;
                        stat.CitiesSubprovince = newPos.CitiesSubprovince;
                        stat.CitiesCode = newPos.CitiesCode;
                        stat.BuildingNumber = newPos.BuildingNumber;
                        stat.ASl = newPos.Asl;
                        stat.Province = newPos.Province;
                        stat.Remark = newPos.Remark;
                        stat.Status = newPos.Status;
                        stat.StreetName = newPos.StreetName;
                        stat.StreetType = newPos.StreetType;
                        stat.Subprovince = newPos.Subprovince;
                        stat.TypeCity = newPos.TypeCity;
                        stat.X = newPos.X;
                        stat.Y = newPos.Y;
                        stat.TableName = newPos.TableName;
                        stat.Id = newPos.Id;


                        if (!string.IsNullOrEmpty(emitterStat.Position.FullAddress))
                            cellAddress.Value = emitterStat.Position.FullAddress;
                        cellLongitude.DoubleValue = emitterStat.Position.LonDms;
                        cellLatitude.DoubleValue = emitterStat.Position.LatDms;
                        emitterStat.NeedPositionSave = true;
                    }
                }
                
            }
            else
            {
               
                if (ShowMessageReference(emitterStat.Position != null))
                {
                    FormNewPosition frm = new FormNewPosition(IM.NullI);
                    frm.CanAttachPhoto = false;
                    frm.CanEditPosition = true;
                    frm.CanEditStreet = true;
                    frm.SetLongitudeAsDms(cellLongitude.DoubleValue);
                    frm.SetLatitudeAsDms(cellLatitude.DoubleValue);

                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        emitterStat.Position = frm.LoadNewPosition(PlugTbl.XfaPosition, cellLongitude.DoubleValue, cellLatitude.DoubleValue);
                        cellAddress.Value = emitterStat.Position.FullAddress;
                        cellLatitude.DoubleValue = emitterStat.Position.LatDms;
                        cellLongitude.DoubleValue = emitterStat.Position.LonDms;
                        emitterStat.NeedPositionSave = true;
                    }
                }
            }

        }

        public void OnBeforeAddress(Cell cell, string newV)
        {

        }

        /// <summary>
        /// Реакція при виборі частоти
        /// </summary>
        /// <param name="cell"></param>
        public void OnAfterCentrFreq(Cell cell)
        {
            int numb = cell.PickList.IndexOf(cell.Value);
            //cellNomCentr.Value = emitterStat.EmFreqList[numb].Freq.ToStringNullD();
            if (numb >= 0)
            {
                cellNomCentr.Value = emitterStat.EmFreqList[numb].Freq.ToStringNullD();
                cellWideEmi.Value = emitterStat.EmFreqList[numb].Bw.ToStringNullD();
                //cellDesigEmi.Value = emitterStat.EmFreqList[numb].FreqMin + " - " + emitterStat.EmFreqList[numb].FreqMax;
            }
        }

        public void OnBeforeCentrFreq(Cell cell, string newV)
        {
            double val;
            if (!double.TryParse(cell.Value, out val))
                newV = cell.Value;
        }

        /// <summary>
        /// Зміна виду випром. пристроїв
        /// </summary>
        /// <param name="cell"></param>
        public void OnAfterTypeEmi(Cell cell)
        {
            listFreq_All = new List<EmiterBand>();
            listFreq_Sel = new List<EmiterBand>();
            listFreq_Sel2 = new List<EmiterBand>();
            FillCentrFreq();
            FillCentrFreq2();
        }

        public void LoadBand(int emit_id)
        {
            EmiterBand bn = new EmiterBand();
            listFreq_Sel2 = bn.Load(emit_id);
            listFreq_Sel = bn.Load(emit_id);
            if (listFreq_Sel2.Count > 0)
            {
                cellNomCentr.Value = EmiterBand.GetAllCentralFreq(listFreq_Sel2);
                cellDesigEmi.Value = EmiterBand.GetAllBandExt(listFreq_Sel2);
            }
        }
                


        private void FillCentrFreq2()
        {

            using (FChannels fChannel = new FChannels("Вибір смуг частот", "Смуги частот"))
            {
                EmiterBand em = new EmiterBand();
                listFreq_All = em.Load(cellTypeEmi.Value);
                foreach (EmiterBand freq in listFreq_All)
                {
                    fChannel.CLBAllChannel.Items.Add(freq);
                }

                foreach (EmiterBand freq in listFreq_Sel)
                {
                    fChannel.CLBSelectChannel.Items.Add(freq);
                    if (listFreq_All.Find(r => r.Id == freq.Id && r.BandMax == freq.BandMax && r.BandMin==freq.BandMin && r.FREQ==freq.FREQ && r.FREQ_M==freq.FREQ_M && r.FREQ_P==freq.FREQ_P) != null)
                    {
                        EmiterBand bnd = listFreq_All.Find(r => r.Id == freq.Id && r.BandMax == freq.BandMax && r.BandMin == freq.BandMin && r.FREQ == freq.FREQ && r.FREQ_M == freq.FREQ_M && r.FREQ_P == freq.FREQ_P);
                        fChannel.CLBAllChannel.Items.Remove(bnd);
                    }
                }

                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    listFreq_Sel2 = new List<EmiterBand>();
                    if (fChannel.CLBSelectChannel.Items.Count > 0)
                    {
                        for (int i = 0; i < fChannel.CLBSelectChannel.Items.Count; i++)
                        {
                            listFreq_Sel2.Add(((EmiterBand)(fChannel.CLBSelectChannel.Items[i])));
                        }
                        cellNomCentr.Value = EmiterBand.GetAllCentralFreq(listFreq_Sel2);
                        cellDesigEmi.Value = EmiterBand.GetAllBandExt(listFreq_Sel2);
                        listFreq_Sel = listFreq_Sel2;
                    }
                    else
                    {
                        listFreq_Sel = new List<EmiterBand>();
                        listFreq_Sel2 = new List<EmiterBand>();
                        cellNomCentr.Value = "";
                        cellDesigEmi.Value = "";
                    }
                }

            }
        }

        /// <summary>
        /// Заповнити список центр.частот
        /// </summary>
        private void FillCentrFreq()
        {
            
            cellNomCentr.Items.Clear();
            emitterStat.GetCentrFreq(EriFiles.GetEriCode(cellTypeEmi.Value, "EmitterType"));
            foreach (EmitterStation.EmitterFreq emfreq in emitterStat.EmFreqList)
                cellNomCentr.Items.Add(emfreq.ToString());
            if (emitterStat.EmFreqList.Count > 0)
            {
                cellNomCentr.Value = emitterStat.EmFreqList[0].Freq.ToStringNullD();
                cellWideEmi.Value = emitterStat.EmFreqList[0].Bw.ToStringNullD();
                //cellDesigEmi.Value = emitterStat.EmFreqList[0].FreqMin + " - " + emitterStat.EmFreqList[0].FreqMax;
            }
        }

        public void OnAfterWideFreq(Cell cell) { }

        public void OnBeforeWideFreq(Cell cell, string newV)
        {
            double val;
            if (!double.TryParse(cell.Value, out val))
                newV = cell.Value;
        }

        #region Overrides of BaseAppClass

        //===========================================================
        /// <summary>
        /// Отобразить данные о владельце
        /// </summary>
        public override void ShowOperator()
        {
            if ((emitterStat.OwnerId > 0) && (emitterStat.OwnerId != IM.NullI))
            {
                RecordPtr rcPtr = new RecordPtr(ICSMTbl.itblUsers, emitterStat.OwnerId);
                rcPtr.UserEdit();
            }
        }
        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket() { }

        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            EmiterBand band = new EmiterBand();
            if (listFreq_Sel2.Count > 0) { band.Save(listFreq_Sel2, emitterStat.Id); } else { band.Remove(emitterStat.Id); }
            emitterStat.Status = Status.StatusAsString;
            emitterStat.EquipName = emitterStat.EmEquip.Name;
            emitterStat.EquipId = emitterStat.EmEquip.Id;
            emitterStat.Position.LonDms = cellLongitude.DoubleValue;
            emitterStat.Position.LatDms = cellLatitude.DoubleValue;
            emitterStat.Position.FullAddress = !string.IsNullOrEmpty(cellAddress.Value) ? cellAddress.Value : "";
            emitterStat.AGL = cellAGL.DoubleValue;
            emitterStat.Certific.Symbol = cellCertificateNumber.Value;
            emitterStat.Certific.Date = cellCertificateDate.DateValue;
            foreach (KeyValuePair<string, string> s in EriFiles.GetEriCodeAndDescr("EmitterType"))
                if (s.Value.Trim() == cellTypeEmi.Value.Trim())
                {
                    emitterStat.EmiType = s.Key;
                    break;
                }
            emitterStat.FreqCentr = cellNomCentr.Value.ToDouble(IM.NullD);
            emitterStat.DopDevice = cellTechGadget.Value;
            emitterStat.Pow[PowerUnits.W] = cellPower.DoubleValue.Round(3);
            emitterStat.Bw = cellWideEmi.Value.ToDouble(IM.NullD);
            string[] desStr = cellDesigEmi.Value.Split('-');
            emitterStat.FreqMin = desStr[0].Trim().ToDouble(IM.NullD);
            emitterStat.FreqMax = desStr[desStr.Length-1].Trim().ToDouble(IM.NullD);
            emitterStat.Save();
            return true;
        }


        //============================================================
        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            /*
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            //string prov = (station.objPosition != null) ? station.objPosition.GetS("PROVINCE") : "";
            //string city = (station.objPosition != null) ? station.objPosition.GetS("CITY") : "";

            if (string.IsNullOrEmpty(number))
                number = PrintDocs.getListDocNumber(docType, 1)[0];
              

            if ((docType == DocType.VISN) || (docType == DocType.VISN_NR))
                fullPath += HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.DOZV) || (docType == DocType.DOZV_OPER))
                fullPath += "ЦТБ-" + "-" + number + ".rtf";
            else if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA) || (docType == DocType.PCR2)
                  || (docType == DocType.MESS_DOZV_CANCEL))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(department) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                throw new IMException("Анулювання дії дозволу можна здійснювати лише з пакету!");
            else
                throw new IMException("Error document type");
            return fullPath;
            */

            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";


            string prov = (emitterStat.Position != null) ? emitterStat.Position.Province : "";
            string city = (emitterStat.Position != null) ? emitterStat.Position.City : "";


            if (string.IsNullOrEmpty(number))
                number = PrintDocs.GetListDocNumberForDozvWma(1)[0];

            if (docType == DocType.DOZV)
            {
                fullPath += "ВП-" + HelpFunction.getAreaCode(prov, city) + "-" + number + ".rtf";
            }
            else
                throw new IMException("Error document type");
            return fullPath;

        }

        /// <summary>
        /// Печать документа
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {

        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.EmitterAppl;
        }

        #endregion

        public override string GetProvince()
        {
            return "";
        }

    }
}
