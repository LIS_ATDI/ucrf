﻿namespace XICSM.UcrfRfaNET.ApplSource
{
    partial class DiffReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiffReportForm));
            this.lblSpecials = new System.Windows.Forms.Label();
            this.txtSpecials = new System.Windows.Forms.TextBox();
            this.lblChief = new System.Windows.Forms.Label();
            this.lblMembers = new System.Windows.Forms.Label();
            this.cbChief = new System.Windows.Forms.ComboBox();
            this.lbMembers = new System.Windows.Forms.ListBox();
            this.btnChoose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblActualTestResults = new System.Windows.Forms.Label();
            this.txtActialTestResults = new System.Windows.Forms.TextBox();
            this.btnLoopupActualTestResults = new System.Windows.Forms.Button();
            this.btnLookupTestResults = new System.Windows.Forms.Button();
            this.txtTestResults = new System.Windows.Forms.TextBox();
            this.lblTestResults = new System.Windows.Forms.Label();
            this.grpReviesAdditions = new System.Windows.Forms.GroupBox();
            this.lblEquipParamMeasure = new System.Windows.Forms.Label();
            this.txtEquipParamMeasure = new System.Windows.Forms.TextBox();
            this.txtEquipParameters = new System.Windows.Forms.TextBox();
            this.lblEquipParameters = new System.Windows.Forms.Label();
            this.btnTestParams = new System.Windows.Forms.Button();
            this.txtTestParams = new System.Windows.Forms.TextBox();
            this.lblTestParams = new System.Windows.Forms.Label();
            this.btnParamMeasurement = new System.Windows.Forms.Button();
            this.txtParamMeasurement = new System.Windows.Forms.TextBox();
            this.lblParamMeasurement = new System.Windows.Forms.Label();
            this.lblGeneralSummary = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLookupGeneralSummary = new System.Windows.Forms.Button();
            this.txtGeneralSummary = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.icon = new System.Windows.Forms.PictureBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.lblLastSavedReport = new System.Windows.Forms.Label();
            this.txtLastSaved = new System.Windows.Forms.TextBox();
            this.grpReviesAdditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSpecials
            // 
            this.lblSpecials.AutoSize = true;
            this.lblSpecials.Location = new System.Drawing.Point(9, 6);
            this.lblSpecials.Name = "lblSpecials";
            this.lblSpecials.Size = new System.Drawing.Size(201, 13);
            this.lblSpecials.TabIndex = 0;
            this.lblSpecials.Text = "Особливі умови Висновки щодо ЕМС: ";
            // 
            // txtSpecials
            // 
            this.txtSpecials.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSpecials.Location = new System.Drawing.Point(12, 27);
            this.txtSpecials.Multiline = true;
            this.txtSpecials.Name = "txtSpecials";
            this.txtSpecials.Size = new System.Drawing.Size(566, 63);
            this.txtSpecials.TabIndex = 1;
            // 
            // lblChief
            // 
            this.lblChief.AutoSize = true;
            this.lblChief.Location = new System.Drawing.Point(12, 106);
            this.lblChief.Name = "lblChief";
            this.lblChief.Size = new System.Drawing.Size(82, 13);
            this.lblChief.TabIndex = 2;
            this.lblChief.Text = "Голова комісії:";
            // 
            // lblMembers
            // 
            this.lblMembers.AutoSize = true;
            this.lblMembers.Location = new System.Drawing.Point(12, 132);
            this.lblMembers.Name = "lblMembers";
            this.lblMembers.Size = new System.Drawing.Size(78, 13);
            this.lblMembers.TabIndex = 4;
            this.lblMembers.Text = "Члени комісії:";
            // 
            // cbChief
            // 
            this.cbChief.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChief.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChief.FormattingEnabled = true;
            this.cbChief.Location = new System.Drawing.Point(100, 102);
            this.cbChief.Name = "cbChief";
            this.cbChief.Size = new System.Drawing.Size(478, 21);
            this.cbChief.TabIndex = 6;
            this.cbChief.SelectedIndexChanged += new System.EventHandler(this.cbChief_SelectedIndexChanged);
            // 
            // lbMembers
            // 
            this.lbMembers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMembers.FormattingEnabled = true;
            this.lbMembers.Location = new System.Drawing.Point(100, 132);
            this.lbMembers.Name = "lbMembers";
            this.lbMembers.Size = new System.Drawing.Size(478, 43);
            this.lbMembers.TabIndex = 7;
            // 
            // btnChoose
            // 
            this.btnChoose.Location = new System.Drawing.Point(12, 152);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(75, 23);
            this.btnChoose.TabIndex = 8;
            this.btnChoose.Text = "Вибрати";
            this.btnChoose.UseVisualStyleBackColor = true;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.WindowText;
            this.panel1.Location = new System.Drawing.Point(10, 186);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(570, 2);
            this.panel1.TabIndex = 9;
            // 
            // lblActualTestResults
            // 
            this.lblActualTestResults.AutoSize = true;
            this.lblActualTestResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblActualTestResults.Location = new System.Drawing.Point(12, 201);
            this.lblActualTestResults.Name = "lblActualTestResults";
            this.lblActualTestResults.Size = new System.Drawing.Size(100, 13);
            this.lblActualTestResults.TabIndex = 11;
            this.lblActualTestResults.Text = "Результати НВ:";
            // 
            // txtActialTestResults
            // 
            this.txtActialTestResults.Location = new System.Drawing.Point(12, 227);
            this.txtActialTestResults.Multiline = true;
            this.txtActialTestResults.Name = "txtActialTestResults";
            this.txtActialTestResults.Size = new System.Drawing.Size(276, 59);
            this.txtActialTestResults.TabIndex = 12;
            // 
            // btnLoopupActualTestResults
            // 
            this.btnLoopupActualTestResults.Location = new System.Drawing.Point(213, 196);
            this.btnLoopupActualTestResults.Name = "btnLoopupActualTestResults";
            this.btnLoopupActualTestResults.Size = new System.Drawing.Size(75, 23);
            this.btnLoopupActualTestResults.TabIndex = 13;
            this.btnLoopupActualTestResults.Text = "Вибрати";
            this.btnLoopupActualTestResults.UseVisualStyleBackColor = true;
            this.btnLoopupActualTestResults.Click += new System.EventHandler(this.btnLoopupActualTestResults_Click);
            // 
            // btnLookupTestResults
            // 
            this.btnLookupTestResults.Location = new System.Drawing.Point(503, 196);
            this.btnLookupTestResults.Name = "btnLookupTestResults";
            this.btnLookupTestResults.Size = new System.Drawing.Size(75, 23);
            this.btnLookupTestResults.TabIndex = 16;
            this.btnLookupTestResults.Text = "Вибрати";
            this.btnLookupTestResults.UseVisualStyleBackColor = true;
            this.btnLookupTestResults.Click += new System.EventHandler(this.btnLookupTestResults_Click);
            // 
            // txtTestResults
            // 
            this.txtTestResults.Location = new System.Drawing.Point(300, 227);
            this.txtTestResults.Multiline = true;
            this.txtTestResults.Name = "txtTestResults";
            this.txtTestResults.Size = new System.Drawing.Size(280, 59);
            this.txtTestResults.TabIndex = 15;
            // 
            // lblTestResults
            // 
            this.lblTestResults.AutoSize = true;
            this.lblTestResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTestResults.Location = new System.Drawing.Point(297, 201);
            this.lblTestResults.Name = "lblTestResults";
            this.lblTestResults.Size = new System.Drawing.Size(99, 13);
            this.lblTestResults.TabIndex = 14;
            this.lblTestResults.Text = "Результати ТВ:";
            // 
            // grpReviesAdditions
            // 
            this.grpReviesAdditions.Controls.Add(this.lblEquipParamMeasure);
            this.grpReviesAdditions.Controls.Add(this.txtEquipParamMeasure);
            this.grpReviesAdditions.Controls.Add(this.txtEquipParameters);
            this.grpReviesAdditions.Controls.Add(this.lblEquipParameters);
            this.grpReviesAdditions.Location = new System.Drawing.Point(12, 298);
            this.grpReviesAdditions.Name = "grpReviesAdditions";
            this.grpReviesAdditions.Size = new System.Drawing.Size(566, 76);
            this.grpReviesAdditions.TabIndex = 17;
            this.grpReviesAdditions.TabStop = false;
            this.grpReviesAdditions.Text = "Перегляд додатків до акту ПТК:";
            // 
            // lblEquipParamMeasure
            // 
            this.lblEquipParamMeasure.AutoSize = true;
            this.lblEquipParamMeasure.Location = new System.Drawing.Point(288, 22);
            this.lblEquipParamMeasure.Name = "lblEquipParamMeasure";
            this.lblEquipParamMeasure.Size = new System.Drawing.Size(209, 13);
            this.lblEquipParamMeasure.TabIndex = 18;
            this.lblEquipParamMeasure.Text = "Протокол вимірювання параметрів РЕЗ";
            // 
            // txtEquipParamMeasure
            // 
            this.txtEquipParamMeasure.Location = new System.Drawing.Point(291, 41);
            this.txtEquipParamMeasure.Name = "txtEquipParamMeasure";
            this.txtEquipParamMeasure.Size = new System.Drawing.Size(262, 20);
            this.txtEquipParamMeasure.TabIndex = 19;
            // 
            // txtEquipParameters
            // 
            this.txtEquipParameters.Location = new System.Drawing.Point(11, 41);
            this.txtEquipParameters.Name = "txtEquipParameters";
            this.txtEquipParameters.Size = new System.Drawing.Size(262, 20);
            this.txtEquipParameters.TabIndex = 18;
            // 
            // lblEquipParameters
            // 
            this.lblEquipParameters.AutoSize = true;
            this.lblEquipParameters.Location = new System.Drawing.Point(8, 22);
            this.lblEquipParameters.Name = "lblEquipParameters";
            this.lblEquipParameters.Size = new System.Drawing.Size(228, 13);
            this.lblEquipParameters.TabIndex = 0;
            this.lblEquipParameters.Text = "Протокол інстр. оцінки парам. випром. РЕЗ";
            // 
            // btnTestParams
            // 
            this.btnTestParams.Location = new System.Drawing.Point(503, 385);
            this.btnTestParams.Name = "btnTestParams";
            this.btnTestParams.Size = new System.Drawing.Size(75, 23);
            this.btnTestParams.TabIndex = 23;
            this.btnTestParams.Text = "Вибрати";
            this.btnTestParams.UseVisualStyleBackColor = true;
            this.btnTestParams.Click += new System.EventHandler(this.btnTestParams_Click);
            // 
            // txtTestParams
            // 
            this.txtTestParams.Location = new System.Drawing.Point(300, 416);
            this.txtTestParams.Multiline = true;
            this.txtTestParams.Name = "txtTestParams";
            this.txtTestParams.Size = new System.Drawing.Size(280, 59);
            this.txtTestParams.TabIndex = 22;
            // 
            // lblTestParams
            // 
            this.lblTestParams.AutoSize = true;
            this.lblTestParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTestParams.Location = new System.Drawing.Point(297, 390);
            this.lblTestParams.Name = "lblTestParams";
            this.lblTestParams.Size = new System.Drawing.Size(194, 13);
            this.lblTestParams.TabIndex = 21;
            this.lblTestParams.Text = "Рез. інстр. оцінки парам. випр.";
            // 
            // btnParamMeasurement
            // 
            this.btnParamMeasurement.Location = new System.Drawing.Point(213, 385);
            this.btnParamMeasurement.Name = "btnParamMeasurement";
            this.btnParamMeasurement.Size = new System.Drawing.Size(75, 23);
            this.btnParamMeasurement.TabIndex = 20;
            this.btnParamMeasurement.Text = "Вибрати";
            this.btnParamMeasurement.UseVisualStyleBackColor = true;
            this.btnParamMeasurement.Click += new System.EventHandler(this.btnParamMeasurement_Click);
            // 
            // txtParamMeasurement
            // 
            this.txtParamMeasurement.Location = new System.Drawing.Point(12, 416);
            this.txtParamMeasurement.Multiline = true;
            this.txtParamMeasurement.Name = "txtParamMeasurement";
            this.txtParamMeasurement.Size = new System.Drawing.Size(276, 59);
            this.txtParamMeasurement.TabIndex = 19;
            // 
            // lblParamMeasurement
            // 
            this.lblParamMeasurement.AutoSize = true;
            this.lblParamMeasurement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblParamMeasurement.Location = new System.Drawing.Point(12, 390);
            this.lblParamMeasurement.Name = "lblParamMeasurement";
            this.lblParamMeasurement.Size = new System.Drawing.Size(201, 13);
            this.lblParamMeasurement.TabIndex = 18;
            this.lblParamMeasurement.Text = "Результати вимірювання парам.";
            // 
            // lblGeneralSummary
            // 
            this.lblGeneralSummary.AutoSize = true;
            this.lblGeneralSummary.Location = new System.Drawing.Point(9, 504);
            this.lblGeneralSummary.Name = "lblGeneralSummary";
            this.lblGeneralSummary.Size = new System.Drawing.Size(152, 13);
            this.lblGeneralSummary.TabIndex = 24;
            this.lblGeneralSummary.Text = "Загальні висновки акту ПТК";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.WindowText;
            this.panel2.Location = new System.Drawing.Point(10, 489);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(570, 2);
            this.panel2.TabIndex = 25;
            // 
            // btnLookupGeneralSummary
            // 
            this.btnLookupGeneralSummary.Location = new System.Drawing.Point(503, 499);
            this.btnLookupGeneralSummary.Name = "btnLookupGeneralSummary";
            this.btnLookupGeneralSummary.Size = new System.Drawing.Size(75, 23);
            this.btnLookupGeneralSummary.TabIndex = 26;
            this.btnLookupGeneralSummary.Text = "Вибрати";
            this.btnLookupGeneralSummary.UseVisualStyleBackColor = true;
            this.btnLookupGeneralSummary.Click += new System.EventHandler(this.btnLookupGeneralSummary_Click);
            // 
            // txtGeneralSummary
            // 
            this.txtGeneralSummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGeneralSummary.Location = new System.Drawing.Point(12, 528);
            this.txtGeneralSummary.Multiline = true;
            this.txtGeneralSummary.Name = "txtGeneralSummary";
            this.txtGeneralSummary.Size = new System.Drawing.Size(566, 63);
            this.txtGeneralSummary.TabIndex = 27;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 607);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 28;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(93, 607);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 23);
            this.btnPrint.TabIndex = 29;
            this.btnPrint.Text = "Друк акту ПТК";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.Location = new System.Drawing.Point(505, 607);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "Вийти";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // icon
            // 
            this.icon.Image = ((System.Drawing.Image)(resources.GetObject("icon.Image")));
            this.icon.Location = new System.Drawing.Point(12, 645);
            this.icon.Name = "icon";
            this.icon.Size = new System.Drawing.Size(48, 48);
            this.icon.TabIndex = 31;
            this.icon.TabStop = false;
            // 
            // lblHint
            // 
            this.lblHint.Location = new System.Drawing.Point(66, 663);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(443, 30);
            this.lblHint.TabIndex = 32;
            this.lblHint.Text = "для перегляду і редагування попередньої збереженої версії акту ПТК − виконати под" +
                "війний клік лівою клавішею миші в цьому полі";
            // 
            // lblLastSavedReport
            // 
            this.lblLastSavedReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLastSavedReport.Location = new System.Drawing.Point(7, 701);
            this.lblLastSavedReport.Name = "lblLastSavedReport";
            this.lblLastSavedReport.Size = new System.Drawing.Size(116, 29);
            this.lblLastSavedReport.TabIndex = 33;
            this.lblLastSavedReport.Text = "Останній варіант акту ПТК";
            // 
            // txtLastSaved
            // 
            this.txtLastSaved.Location = new System.Drawing.Point(119, 707);
            this.txtLastSaved.Name = "txtLastSaved";
            this.txtLastSaved.ReadOnly = true;
            this.txtLastSaved.Size = new System.Drawing.Size(459, 20);
            this.txtLastSaved.TabIndex = 34;
            this.txtLastSaved.DoubleClick += new System.EventHandler(this.txtLastSaved_DoubleClick);
            // 
            // DiffReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 744);
            this.Controls.Add(this.txtLastSaved);
            this.Controls.Add(this.lblLastSavedReport);
            this.Controls.Add(this.lblHint);
            this.Controls.Add(this.icon);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtGeneralSummary);
            this.Controls.Add(this.btnLookupGeneralSummary);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblGeneralSummary);
            this.Controls.Add(this.btnTestParams);
            this.Controls.Add(this.txtTestParams);
            this.Controls.Add(this.lblTestParams);
            this.Controls.Add(this.btnParamMeasurement);
            this.Controls.Add(this.txtParamMeasurement);
            this.Controls.Add(this.lblParamMeasurement);
            this.Controls.Add(this.grpReviesAdditions);
            this.Controls.Add(this.btnLookupTestResults);
            this.Controls.Add(this.txtTestResults);
            this.Controls.Add(this.lblTestResults);
            this.Controls.Add(this.btnLoopupActualTestResults);
            this.Controls.Add(this.txtActialTestResults);
            this.Controls.Add(this.lblActualTestResults);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.lbMembers);
            this.Controls.Add(this.cbChief);
            this.Controls.Add(this.lblMembers);
            this.Controls.Add(this.lblChief);
            this.Controls.Add(this.txtSpecials);
            this.Controls.Add(this.lblSpecials);
            this.Name = "DiffReportForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Генерація акту ПТК";
            this.Load += new System.EventHandler(this.DiffReportForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DiffReportForm_FormClosing);
            this.grpReviesAdditions.ResumeLayout(false);
            this.grpReviesAdditions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSpecials;
        private System.Windows.Forms.TextBox txtSpecials;
        private System.Windows.Forms.Label lblChief;
        private System.Windows.Forms.Label lblMembers;
        private System.Windows.Forms.ComboBox cbChief;
        private System.Windows.Forms.ListBox lbMembers;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblActualTestResults;
        private System.Windows.Forms.TextBox txtActialTestResults;
        private System.Windows.Forms.Button btnLoopupActualTestResults;
        private System.Windows.Forms.Button btnLookupTestResults;
        private System.Windows.Forms.TextBox txtTestResults;
        private System.Windows.Forms.Label lblTestResults;
        private System.Windows.Forms.GroupBox grpReviesAdditions;
        private System.Windows.Forms.Label lblEquipParameters;
        private System.Windows.Forms.TextBox txtEquipParameters;
        private System.Windows.Forms.Label lblEquipParamMeasure;
        private System.Windows.Forms.TextBox txtEquipParamMeasure;
        private System.Windows.Forms.Button btnTestParams;
        private System.Windows.Forms.TextBox txtTestParams;
        private System.Windows.Forms.Label lblTestParams;
        private System.Windows.Forms.Button btnParamMeasurement;
        private System.Windows.Forms.TextBox txtParamMeasurement;
        private System.Windows.Forms.Label lblParamMeasurement;
        private System.Windows.Forms.Label lblGeneralSummary;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnLookupGeneralSummary;
        private System.Windows.Forms.TextBox txtGeneralSummary;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox icon;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.Label lblLastSavedReport;
        private System.Windows.Forms.TextBox txtLastSaved;
    }
}