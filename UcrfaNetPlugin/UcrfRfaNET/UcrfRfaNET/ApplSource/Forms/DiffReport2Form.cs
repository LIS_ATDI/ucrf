﻿using System;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal partial class DiffReport2Form : Form
    {
        private DiffReport2Object _repObject;
        public DiffReport2Form(Classes.BaseDiffClass diffObject)
        {
            InitializeComponent();
            _repObject = new DiffReport2Object(diffObject);
        }
        /// <summary>
        /// Загружаем форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiffReport2Form_Load(object sender, EventArgs e)
        {
            // Делаем связки
            //Дата 1
            dateMeasurement.DataBindings.Add("Value", _repObject, "Date1", true);
            timeFrom.DataBindings.Add("Value", _repObject, "TimeFrom1", true);
            timeTo.DataBindings.Add("Value", _repObject, "TimeTo1", true);
            timeTo.DataBindings.Add("MinDate", _repObject, "TimeFrom1", true);
            cbDate1.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            cbDate1.DataBindings.Add("Visible", _repObject, "IsEnableDate2", true);
            //Дата 2
            dateMeasurement2.DataBindings.Add("Value", _repObject, "Date2", true);
            timeFrom2.DataBindings.Add("Value", _repObject, "TimeFrom2", true);
            timeTo2.DataBindings.Add("Value", _repObject, "TimeTo2", true);
            timeTo2.DataBindings.Add("MinDate", _repObject, "TimeFrom2", true);
            cbDate2.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            cbDate2.DataBindings.Add("Visible", _repObject, "IsEnableDate2", true);
            dateMeasurement2.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            timeFrom2.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            timeTo2.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            lblDate2.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            lblTimeFrom.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            lblTimeTo.DataBindings.Add("Enabled", _repObject, "IsEnableDate2", true);
            //Линк
            lblLink.DataBindings.Add("Text", _repObject, "FileLink", true);
            //Устройство
            _repObject.DeviceLst.InitComboBox(cbDevice);
            cbDevice.DataBindings.Add("SelectedValue", _repObject, "MonitorEquipId", true);
            //Ползователи 1
            _repObject.ViseUserLst.InitComboBox(cbViseUser);
            cbViseUser.DataBindings.Add("SelectedValue", _repObject, "ViseUserId", true);
            //Ползователи 2
            _repObject.ExecutorUserLst.InitComboBox(cbExecutorUser);
            cbExecutorUser.DataBindings.Add("SelectedValue", _repObject, "ExecutorUserId", true);
            //----
            cbDate1.DataBindings.Add("Checked", _repObject, "IsCheckedDate1", true);
            cbDate2.DataBindings.Add("Checked", _repObject, "IsCheckedDate2", true);
        }
        /// <summary>
        /// Сохраняем данные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            _repObject.Save();
        }
        /// <summary>
        /// Печатаем документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            _repObject.Print();
        }
        /// <summary>
        /// Открываем линк
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblLink_DoubleClick(object sender, EventArgs e)
        {
            _repObject.OpenLink();
        }
    }
}
