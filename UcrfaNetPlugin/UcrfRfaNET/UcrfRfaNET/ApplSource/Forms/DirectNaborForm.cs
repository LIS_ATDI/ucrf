﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.GlobalDB;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class DirectNaborForm : Form
    {
       
        public Dictionary<string, string> NaborDict = new Dictionary<string, string>();

        private XmlAllStation xAll;
        /// <summary>
        /// Конструктор
        /// </summary>
        public DirectNaborForm()
        {
            InitializeComponent();
        }

        //Завантаження даних
        public DirectNaborForm(string tableName)
            : this()
        {
            ReadData(tableName);
        }

        /// <summary>
        /// Початкове формування списків даних
        /// </summary>
        /// <param name="tableName">Назва таблиці</param>
        private void ReadData(string tableName)
        {                     
            ReadFields(tableName);

            ReadEntity();

            ReadNabor();            
        }

        /// <summary>
        /// Формування наборів
        /// </summary>
        private void ReadNabor()
        {
           NaborDict = xAll.LoadNabor();
            for (int i = 0; i < NaborDict.Count; i++)
                lstBoxNaborDirect.Items.Add(NaborDict.Keys.ToList()[i] + "/" + NaborDict.Values.ToList()[i]);
        }

        /// <summary>
        /// Формування списку полів для експорту даних
        /// </summary>        
        /// <param name="tableName"></param>
        private void ReadFields(string tableName)
        {
            xAll = new XmlAllStation();
            xAll.GetAllFields(tableName);
            for (int i = 0; i < xAll._fieldsDict.Count; i++)
                chkdLstBoxFieldDirect.Items.Add(xAll._fieldsDict.Values.ToList()[i]);                   
        }

        /// <summary>
        /// Формування списку сутностей
        /// </summary>
        private void ReadEntity()
        {
            CGlobalDB cDb = new CGlobalDB();
            cDb.OpenConnection();
            List<string> tmp = cDb.ReadEssences();
            cDb.CloseConnection();
            cmbBoxEntityDirect.Items.AddRange(tmp.ToArray());          
        }

        /// <summary>
        /// Реакція на зміну прапорця "Вибрати все"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkBoxSelectAllDirectCheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < chkdLstBoxFieldDirect.Items.Count; i++)
                chkdLstBoxFieldDirect.SetItemChecked(i, chkBoxSelectAllDirect.Checked);
        }

        /// <summary>
        /// Збереження набору
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveNaborDirectClick(object sender, EventArgs e)
        {
            //Перевірка на правильність вводу
            if (NaborDict.ContainsKey(txtBoxNaborDirect.Text))
            {
                MessageBox.Show("Назва набору вже існує в БД!");
                return;                
            }
            if (cmbBoxEntityDirect.SelectedIndex < 0)
            {
                MessageBox.Show("Виберіть сутність!");
                return;
            }
            //Формування списків
            NaborDict.Add(txtBoxNaborDirect.Text, cmbBoxEntityDirect.SelectedItem.ToString());            
            lstBoxNaborDirect.Items.Add(txtBoxNaborDirect.Text + "/" + cmbBoxEntityDirect.SelectedItem);
            xAll._entityCreateList.Add(cmbBoxEntityDirect.SelectedItem.ToString());
            xAll._naborList.Add(txtBoxNaborDirect.Text);
            xAll.Nabor = txtBoxNaborDirect.Text;
            xAll.Entity = cmbBoxEntityDirect.SelectedItem.ToString();
            xAll.Fields = "";
            for (int i = 0; i < chkdLstBoxFieldDirect.Items.Count; i++)
                if (chkdLstBoxFieldDirect.GetItemChecked(i))
                    xAll.Fields += i + ",";
            if (xAll.Fields.Length>0)
            xAll.Fields = xAll.Fields.Remove(xAll.Fields.Length - 1);
                //Збереження набору в БД
            xAll.SaveNabor();
        }

        /// <summary>
        /// Завантаження набору
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadNab_Click(object sender, EventArgs e)
        {
            if (lstBoxNaborDirect.SelectedIndex<0)
            {
                MessageBox.Show("Оберіть набір для завантаження");
                return;
            }
            if (xAll._naborList.Count > lstBoxNaborDirect.SelectedIndex)
                xAll.Nabor = xAll._naborList[lstBoxNaborDirect.SelectedIndex];
            xAll.LoadNaborChecked();
            for (int i = 0; i < xAll._fieldsDict.Count; i++)
                chkdLstBoxFieldDirect.SetItemChecked(i, false);
            for (int i = 0; i < xAll.checkList.Count; i++)
                chkdLstBoxFieldDirect.SetItemChecked(xAll.checkList[i].ToInt32(IM.NullI), true);
        }

        /// <summary>
        /// Видалення набору
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        private void btnDelNab_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви впевнені?", "Видалення", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (lstBoxNaborDirect.SelectedIndex < 0)
                {
                    MessageBox.Show("Оберіть набір для видалення");
                    return;
                }
                if (xAll._naborList.Count > lstBoxNaborDirect.SelectedIndex)
                    xAll.Nabor = xAll._naborList[lstBoxNaborDirect.SelectedIndex];
                xAll.DeleteNabor();
                xAll._naborList.RemoveAt(lstBoxNaborDirect.SelectedIndex);
                xAll._entityCreateList.RemoveAt(lstBoxNaborDirect.SelectedIndex);
                lstBoxNaborDirect.Items.RemoveAt(lstBoxNaborDirect.SelectedIndex);                
            }
        }
    }
}
