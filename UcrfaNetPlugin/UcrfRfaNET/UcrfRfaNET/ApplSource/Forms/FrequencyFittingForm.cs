﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class FrequencyFittingForm : Form
    {
        private sealed class ChannelPlan
        {
            public sealed class Details : IComparable
            {
                public double TxFrequency{ get; set;}
                public double RxFrequency { get; set; }
                public double Bandwidth { get; set; }
                public int Channel { get; set; }

                public override string ToString()
                {
                    return ("TX:"+TxFrequency.ToStringNullD() + " ; RX:" + RxFrequency.ToStringNullD());
                }

                public int CompareTo(object obj)
                {
                    if (obj == null) return 1;

                    Details otherDetails = obj as Details;

                    if (TxFrequency>otherDetails.TxFrequency)
                    {
                        return 1;
                    }
                    if (TxFrequency < otherDetails.TxFrequency)
                    {
                        return -1;
                    }
                    if (RxFrequency > otherDetails.RxFrequency)
                    {
                        return 1;
                    }
                    if (RxFrequency < otherDetails.RxFrequency)
                    {
                        return -1;
                    }
                    if (Bandwidth > otherDetails.Bandwidth)
                    {
                        return 1;
                    }
                    if (Bandwidth < otherDetails.Bandwidth)
                    {
                        return -1;
                    }

                    return 0;
                }
            };


            public int Id { get; set; }
            public string Name { get; set; }
            private Dictionary<int, Details> _details = new Dictionary<int, Details>();            

            public Dictionary<int, Details> Channels{ get { return _details;}}
            
            private void ReadFromDb(int id)
            {
                IMRecordset rs = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("ID,NAME");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        Name = rs.GetS("NAME");
                        Id = id;
                    }
                }
                finally
                {
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }                
            }

            public void ExtractDetails()
            {
                _details = new Dictionary<int, Details>();

                IMRecordset rs = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("Plan.ID,CHANNEL,FREQ,PARITY,BANDWIDTH");
                    rs.SetWhere("Plan.ID", IMRecordset.Operation.Eq, Id);

                    for(rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int _channel;
                        Int32.TryParse(rs.GetS("CHANNEL"), out _channel);
                        double _freq = rs.GetD("FREQ");
                        string _parity = rs.GetS("PARITY");
                        double _bandwidth = rs.GetD("BANDWIDTH");

                        Details detailRecord = null;
                        if (_details.ContainsKey(_channel))
                        {
                            detailRecord = _details[_channel];
                        }else
                        {
                            detailRecord = new Details();
                            detailRecord.Channel = _channel;
                            _details.Add(_channel, detailRecord);
                        }
                        detailRecord.Bandwidth = _bandwidth;
                        switch(_parity)
                        {
                            case "N":
                                detailRecord.RxFrequency = _freq;
                                detailRecord.TxFrequency = _freq;
                                break;
                            case "D":
                            case "L":
                                detailRecord.TxFrequency = _freq;
                                break;
                            case "U":
                            case "H":
                                detailRecord.RxFrequency = _freq;
                                break;
                            default:
                                break;                            
                        }
                    }
                }
                finally
                {
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }                
            }

            public override string ToString()
            {
                return Name;                
            }

            public ChannelPlan()
            {
                
            }

            public ChannelPlan(int id)
            {
                ReadFromDb(id);
            }
        };

        private int _tableID;
        private string _tableName = string.Empty;

        public int ApplId { get; set; }

        public FrequencyFittingForm(string tableName, int TablID)
        {
            _tableName = tableName;
            _tableID = TablID;         
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lbChannelList.SelectedIndex>=0)
                lbChannelList.Items.Remove(lbChannelList.SelectedItem);
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            //string criteria = HelpFunction.ReplaceQuotaSumbols(initialValue);
            //string param = string.Format("([{0}] LIKE '%{1}%')", fieldFilterName, criteria);
            //if (string.IsNullOrEmpty(dopFilter) == false)
            //param = string.Format("{0} AND ({1})", param, dopFilter);
            //RecordPtr retRec = SelectRecord(title, tableName, param, fieldFilterName, OrderDirection.Ascending);
            RecordPtr selectedChannel = RecordPtr.UserSelect(CLocaliz.TxT("Select channel plan"), ICSMTbl.itblFreqPlan,
                                                             "", "ID", OrderDirection.Ascending);
            if (selectedChannel.Id != IM.NullI)
            {
                ChannelPlan chplan = new ChannelPlan(selectedChannel.Id);
                lbChannelList.Items.Add(chplan);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string strError = "";
            if (lbChannelList.Items.Count == 0)
            {
                strError += "Потрібно вибрати хоча б один канальний план\r\n";
            }

            if (!checkMicrowave.Checked && !checkMobStation2.Checked && !checkEarthStation.Checked)
            {
                strError += "Потрібно вибрати хоча б один тип станції\r\n";
            }

            if (string.IsNullOrEmpty(strError))
            {
                RunFrequencyFitting();
            }
            else
            {
                MessageBox.Show(strError);                
            }
        }

        private void RunFrequencyFitting()
        {
            Station station = new Station(_tableName, _tableID);
            List<ViborkaEMS.Results> resultsList = new List<ViborkaEMS.Results>();

            int channelCount = lbChannelList.Items.Count;
            int frequencyCount = 0;
            foreach (ChannelPlan plan in lbChannelList.Items)
            {
                if (plan != null)
                {
                    plan.ExtractDetails();
                    frequencyCount += plan.Channels.Count;
                }
            }

            int currentChannel = 0;
            int currentFrequency = 0;

            HashSet<ChannelPlan.Details> doneChannels = new HashSet<ChannelPlan.Details>();

            bool _stop = false;
            foreach(ChannelPlan plan in lbChannelList.Items)
            {
                currentChannel++;
                if (plan!=null)
                {
                    //plan.ExtractDetails();
                    
                    foreach (KeyValuePair<int, ChannelPlan.Details> channel in plan.Channels)
                    {
                        currentFrequency++;
                        bool bFoundChannel = false;
                        foreach(ChannelPlan.Details b in doneChannels)
                        {
                            bFoundChannel |= (b.CompareTo(channel.Value)==0);
                        }

                        if (bFoundChannel)
                            continue;

                        doneChannels.Add(channel.Value);

                        //station._show = true;
                        double radius = Convert.ToDouble(txtMaxRange.Text);
                        double freqDiapazon = (channel.Value.Bandwidth/1000.0)*3.0;
                        double compareTD = Convert.ToDouble(tbThreshold.Text);
                        
                        if (_tableName == ICSMTbl.itblMicrowa)
                        {
                            station.STFrequncyRX.Clear();
                            station.STFrequncyTX.Clear();
                            station.STFrequncyRX.Add(channel.Value.RxFrequency);
                            station.STFrequncyTX.Add(channel.Value.TxFrequency);

                            station.stationA.STFrequncyRX.Clear();
                            station.stationA.STFrequncyTX.Clear();
                            station.stationB.STFrequncyRX.Clear();
                            station.stationB.STFrequncyTX.Clear();
                            station.stationA.STFrequncyTX.Add(channel.Value.TxFrequency);
                            station.stationA.STFrequncyRX.Add(channel.Value.RxFrequency);
                            station.stationB.STFrequncyTX.Add(channel.Value.RxFrequency);
                            station.stationB.STFrequncyRX.Add(channel.Value.TxFrequency);

                            System.Diagnostics.Debug.WriteLine(channel.Value.ToString());


                            ViborkaEMS viborkaEMS1 = new ViborkaEMS(station, radius, freqDiapazon, compareTD,
                                                                   checkMobStation2.Checked, checkEarthStation.Checked,
                                                                   checkMicrowave.Checked,
                                                                   cbExcludeCertainOperator.Checked,
                                                                   false);
                            //channel.Value.RxFrequency, 
                            //channel.Value.TxFrequency);

                            string strTitle = string.Format(
                                "Підбір частот (канальних планів: {0} з {1}, частот: {2} з {3})", currentChannel,
                                channelCount, currentFrequency, frequencyCount);
                            viborkaEMS1.Calculate(strTitle);

                            ViborkaEMS.Results results1 = viborkaEMS1.GetResults();
                            resultsList.Add(results1);

                            if (viborkaEMS1.IsCancelled)
                            {
                                _stop = true;
                                break;
                            }
                            else
                            {
                                station.STFrequncyRX.Clear();
                                station.STFrequncyTX.Clear();
                                station.STFrequncyRX.Add(channel.Value.TxFrequency);
                                station.STFrequncyTX.Add(channel.Value.RxFrequency);

                                station.stationA.STFrequncyRX.Clear();
                                station.stationA.STFrequncyTX.Clear();
                                station.stationB.STFrequncyRX.Clear();
                                station.stationB.STFrequncyTX.Clear();
                                station.stationA.STFrequncyTX.Add(channel.Value.RxFrequency);
                                station.stationA.STFrequncyRX.Add(channel.Value.TxFrequency);
                                station.stationB.STFrequncyTX.Add(channel.Value.TxFrequency);
                                station.stationB.STFrequncyRX.Add(channel.Value.RxFrequency);

                                System.Diagnostics.Debug.WriteLine(channel.Value.ToString());


                                ViborkaEMS viborkaEMS2 = new ViborkaEMS(station, radius, freqDiapazon, compareTD,
                                                                        checkMobStation2.Checked,
                                                                        checkEarthStation.Checked,
                                                                        checkMicrowave.Checked,
                                                                        cbExcludeCertainOperator.Checked,
                                                                        false);
                                //channel.Value.RxFrequency, 
                                //channel.Value.TxFrequency);
                                viborkaEMS2.Calculate(strTitle);

                                ViborkaEMS.Results results2 = viborkaEMS2.GetResults();
                                resultsList.Add(results2);

                                if (viborkaEMS2.IsCancelled)
                                {
                                    _stop = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            station.STFrequncyRX.Clear();
                            station.STFrequncyTX.Clear();
                            station.STFrequncyRX.Add(channel.Value.RxFrequency);
                            station.STFrequncyTX.Add(channel.Value.TxFrequency);

                            System.Diagnostics.Debug.WriteLine(channel.Value.ToString());


                            ViborkaEMS viborkaEMS = new ViborkaEMS(station, radius, freqDiapazon, compareTD,
                                                                   checkMobStation2.Checked, checkEarthStation.Checked,
                                                                   checkMicrowave.Checked,
                                                                   cbExcludeCertainOperator.Checked,
                                                                   false);
                            //channel.Value.RxFrequency, 
                            //channel.Value.TxFrequency);

                            string strTitle = string.Format(
                                "Підбір частот (канальних планів: {0} з {1}, частот: {2} з {3})", currentChannel,
                                channelCount, currentFrequency, frequencyCount);
                            viborkaEMS.Calculate(strTitle);

                            ViborkaEMS.Results results = viborkaEMS.GetResults();
                            resultsList.Add(results);

                            if (viborkaEMS.IsCancelled)
                            {
                                _stop = true;
                                break;
                            }
                        }
                    }                    
                }
                if (_stop)
                    break;
            }

            double _radius = Convert.ToDouble(txtMaxRange.Text);            
            double _compareTD = Convert.ToDouble(tbThreshold.Text);

            FrequencyFittingResultForm ffrf = new FrequencyFittingResultForm();
            ffrf.Owner = this;

            string ownerRemark = "";
            Station.GetOwnerData(station, ref ownerRemark);
            ffrf.StationOwner = ownerRemark;
            ffrf.TableName = _tableName;
            ffrf.Id = _tableID;
            ffrf.ApplId = ApplId;            

            if (_tableName == ICSMTbl.itblMicrowa)
            {
                ffrf.PositionA = station.stationA._longitude.LongDECToString() + " " +
                                 station.stationA._latitude.LongDECToString();

                ffrf.PositionB = station.stationB._longitude.LongDECToString() + " " +
                                 station.stationB._latitude.LongDECToString();
            }
            else
            {
                ffrf.PositionA = station._longitude.LongDECToString() + " " +
                                 station._latitude.LongDECToString();

                ffrf.PositionB = station._longitude.LongDECToString() + " " +
                                 station._latitude.LongDECToString();
            }
            ffrf.MicrovaweStations = checkMicrowave.Checked;
            ffrf.MobSta2Stations = checkMobStation2.Checked;
            ffrf.EarthStations = checkEarthStation.Checked;

            if (_tableName == ICSMTbl.itblMicrowa)
            {
                ffrf.FrequencySuffix = " A / B";
            }
            else
            {
                ffrf.FrequencySuffix = " Пд / Пр";
            }


            ffrf.Range = _radius;
            ffrf.Threshold = _compareTD;
            ffrf.SetData(resultsList);
            if (ffrf.ShowDialog()==DialogResult.OK)
            {                
            }
        }

        private void FrequencyFittingForm_Load(object sender, EventArgs e)
        {
            Station station = new Station(_tableName, _tableID);

            if (_tableName==ICSMTbl.itblMicrowa)
            {
                checkMicrowave.Checked = true;
                checkMobStation2.Checked = false;
                checkEarthStation.Checked = false;
                tbThreshold.Text = "3";

                double Frequency1 = 40E+12;
                double Frequency2 = 40E+12;
                
                if (station.stationA.STFrequncyRX.Count > 0)
                    Frequency1 = station.stationA.STFrequncyRX[0];
                if (station.stationA.STFrequncyTX.Count > 0)
                    Frequency2 = station.stationA.STFrequncyTX[0];

                if (Frequency1<40000 || Frequency2<40000)
                {
                    txtMaxRange.Text = "50";
                }

                if (Frequency1 < 25000 || Frequency2 < 25000)
                {
                    txtMaxRange.Text = "100";
                }

                if (Frequency1 < 17000 || Frequency2 < 17000)
                {
                    txtMaxRange.Text = "150";
                }

                if (Frequency1 < 12000 || Frequency2 < 12000)
                {
                    txtMaxRange.Text = "200";
                }

                if (Frequency1 < 7000 || Frequency2 < 7000)
                {
                    txtMaxRange.Text = "250";
                }
            }

            if (_tableName == ICSMTbl.itblMobStation2)
            {
                checkMicrowave.Checked = false;
                checkMobStation2.Checked = true;
                checkEarthStation.Checked = false;
                tbThreshold.Text = "20";
                txtMaxRange.Text = "50";
            }
        }
    }
}
