﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Extension;
using XICSM.UcrfRfaNET.Reports;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Класс генерации отчета для ФОРМА 1
    /// </summary>
    class TaxPayerForm1New : ITaxPayerForm
    {
        private const string FormType = "Форма 1";
        /// <summary>
        /// Данные для записи
        /// </summary>
        class DataItem
        {
            public string LicNum { get; set; }
            public string OwnerName { get; set; }
            public string OwnerRegNum { get; set; }
            public string OwnerAddress { get; set; }
            public DateTime LicDateFrom { get; set; }
            public DateTime LicDateTo { get; set; }
            public DateTime DozvDateCancel { get; set; }
            public string OrderCode { get; set; }
            public string OrderSymbol { get; set; }
            public List<string> Province { get; private set; }
            public double Bw { get; private set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public DataItem()
            {
                LicNum = "";
                OwnerName = "";
                OwnerRegNum = "";
                OwnerAddress = "";
                LicDateFrom = IM.NullT;
                LicDateTo = IM.NullT;
                DozvDateCancel = IM.NullT;
                OrderCode = "";
                OrderSymbol = "";
                Province = new List<string>();
                Bw = 0.0;
            }
            /// <summary>
            /// Обновить запись
            /// </summary>
            /// <param name="province"></param>
            /// <param name="bw"></param>
            public void Add(string province, double bw)
            {
                string[] spl = province.Split(new Char[] { ' ', ',', ';', '/' });
                foreach (string prov in spl)
                    if (Province.Contains(prov.Trim()) == false)
                        Province.Add(prov.Trim());
                if (TaxPayerReportParams.Get().DataFrom == TaxPayerReportParams.F1Method.Allot)
                    Bw += (bw != IM.NullD ? bw : 0);
                else
                    Bw = bw;
            }
        }
        /// <summary>
        /// Класс работы с данными
        /// </summary>
        class WriteData
        {
            private Dictionary<string, DataItem> _data;
            /// <summary>
            /// Конструктор
            /// </summary>
            public WriteData()
            {
                _data = new Dictionary<string, DataItem>();
            }
            /// <summary>
            /// Добавить запись
            /// </summary>
            public string[] Add(Dictionary<string, string> radioSystem,
                            int recId,
                            string licNum,
                            string ownerName,
                            string ownerRegNum,
                            string ownerAddress,
                            DateTime licDateFrom,
                            DateTime licDateTo,
                            string orderSymbol,
                            string province,
                            double bw,
                            DateTime DozvDateCancel,
                            int licId,
                            int chalId)
            {
                List<string> errorList = new List<string>();

                if (licDateFrom == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата початку ліцензії: ID ліцензії = {0}", licId, FormType));
                if (licDateTo == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата закінчення ліцензії: ID ліцензії = {0}", licId, FormType));
                if (string.IsNullOrEmpty(orderSymbol))
                    errorList.Add(string.Format("{1}: Недостатньо інформації для визначення позиції Тарифів: ID ліцензії = {0}, ID виділення = {2}", licId, FormType, chalId ==  IM.NullI ?  "NULL" : chalId.ToString()));
                else if (radioSystem.ContainsKey(orderSymbol) == false)
                    errorList.Add(string.Format("{1}: Відсутній опис для коду позиції Тарифів '{0}': ID ліцензії = {2}", orderSymbol, FormType, licId));
                if (string.IsNullOrEmpty(province))
                    errorList.Add(string.Format("{1}: Відсутній регіон використання: ID ліцензії = {0}", licId, FormType));
                if (bw == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня загальна ширина смуги радіочастот: ID ліцензії = {0}, ID виділення = {2}", licId, FormType, chalId == IM.NullI ? "NULL" : chalId.ToString()));

                //if (errorList.Count > 0)
                //    return errorList.ToArray();

                string key = licNum + "_" + orderSymbol;
                DataItem di = null;
                if (_data.ContainsKey(key))
                {
                    di = _data[key];
                    //di.DozvDateCancel = IM.NullT;
                }
                else
                {
                    di = new DataItem();
                    _data.Add(key, di);
                    di.LicNum = licNum;
                    di.OwnerName = ownerName;
                    di.OwnerRegNum = ownerRegNum;
                    di.OwnerAddress = ownerAddress;
                    di.LicDateFrom = licDateFrom;
                    di.LicDateTo = licDateTo;
                    di.OrderSymbol = orderSymbol;
                    di.DozvDateCancel = DozvDateCancel;
                    if (radioSystem.ContainsKey(orderSymbol))
                        di.OrderCode = radioSystem[di.OrderSymbol];
                }
                di.Add(province, bw);
                return errorList.ToArray();
            }
            /// <summary>
            /// Сохранить в файл
            /// </summary>
            /// <param name="path"></param>
            /// <param name="province"></param>
            /// <param name="repType"></param>
            public void Save(string path, string province, ReportType repType)
            {
                if (_data.Count == 0)
                    return;

                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Запис в файл")))
                {
                    pb.SetBig("Запис в файл...");
                    using (IReport rep = Report.CreateReport(repType))
                    {
                        rep.Init(Path.Combine(path, string.Format("форма1{0}{1}", string.IsNullOrEmpty(province) ? "" : "-", province)), "Page1","");
                        List<string> line = new List<string>();
                        line.Add("№ з/п");
                        line.Add("Найменування користувача РЧР /ПІБ для користувача РЧР фізичної особи");
                        line.Add("Код ЄДРПОУ/Реєстраційний номер облікової картки платника податків для фізичних осіб суб’єктів господарювання");
                        line.Add("Місцезнаходження користувача – юридичної особи /Місце проживання фізичної особи");
                        line.Add("Ліцензія на користування РЧР – Номер");
                        line.Add("Ліцензія на користування РЧР – дата видачі дд.мм.рррр");
                        line.Add("Ліцензія на користування РЧР – термін дії до мм.рррр");
                        line.Add("Вид радіозв’язку");
                        line.Add("Номер позиції статті 320 Податку кодексу України, якою встановлено ставки збору за користування РЧР");
                        line.Add("Назва регіонів користування РЧР");
                        line.Add("Загальна ширина смуги радіочастот, зазначена в ліцензії (МГц)");
                        line.Add("Дата анулювання ліцензії дд.мм.рррр");
                        line.Add("Примітки");
                        rep.WriteLine(line.ToArray());
                        line.Clear();

                        int counter = 1;
                        foreach (KeyValuePair<string, DataItem> item in _data)
                        {
                            pb.SetSmall(counter, _data.Count);
                            line.Clear();
                            DataItem di = item.Value;
                            line.Add(string.Format("{0}", counter++));
                            line.Add(di.OwnerName);
                            line.Add(di.OwnerRegNum);
                            line.Add(di.OwnerAddress);
                            line.Add(di.LicNum);
                            line.Add(di.LicDateFrom.ToStringNullT("dd.MM.yyyy"));
                            line.Add(di.LicDateTo.ToStringNullT("dd.MM.yyyy"));
                            line.Add(di.OrderCode);
                            line.Add(di.OrderSymbol);
                            StringBuilder provinceData = new StringBuilder(500);
                            foreach (string prov in di.Province)
                            {
                                if (provinceData.Length != 0)
                                    provinceData.Append(" ");
                                provinceData.Append(prov);
                            }
                            line.Add(provinceData.ToString());
                            line.Add(di.Bw == IM.NullD ? "" : di.Bw.ToString());

                            line.Add(di.DozvDateCancel.ToStringNullT("dd.MM.yyyy"));
                           
                           
                            rep.WriteLine(line.ToArray());

                            if (pb.UserCanceled())
                                break;
                        }
                        if (!pb.UserCanceled())
                            rep.Save();
                    }
                }
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="date">Дата формирования</param>
        /// <param name="path">Путь к файлам</param>
        public TaxPayerForm1New()
        {
        }
        /// <summary>
        /// Начать процесс генерации документов
        /// </summary>
        /// <returns>список ошибок</returns>
        public string[] Generate(ReportType repType, bool isOneFile)
        {
            TaxPayerReportParams _params = TaxPayerReportParams.Get();
            
            List<string> errorList = new List<string>();
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування переліку користувачів РЧР (Форма 1)")))
            {
                // empty licences
                IMRecordset rs = new IMRecordset("LICENCE", IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("ID,NAME");
                    if (_params.F1StatusPOnly)
                        rs.SetWhere("STATE", IMRecordset.Operation.Eq, "P");
                    string additional = "[STANDARD] not in ('АТМ','АЗМ','DVB-T','DVB-T2','БНТ')";
                    if (!(_params.F1StatusPOnly && _params.F1IgnoreTimePeriod))
                        additional += string.Format(
                        " and [START_DATE] < TO_DATE(\'{1:dd-MM-yyyy}\') and [STOP_DATE] >= TO_DATE(\'{0:dd-MM-yyyy}\')" +
                        " and ([END_DATE] is null or [END_DATE] >= TO_DATE(\'{0:dd-MM-yyyy}\'))"
                        , _params.DateBeg, _params.DateEnd);
                    additional += " and (select count(*) from %CH_ALLOTMENTS where LIC_ID = [ID]) = 0";
                    rs.SetAdditional(additional);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        errorList.Add(string.Format("Форма 1: Ліцензія '{0}' (ID={1}) пуста: жодного канального виділення", rs.GetS("NAME"), rs.GetI("ID")));
                }
                finally
                {
                    rs.Destroy();
                }

                Dictionary<string, string> radioSystem = EriFiles.GetEriCodeAndDescr("Fees");
                pb.SetProgress(0, 50000);
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                string lastProvince = "";
                WriteData wd = new WriteData();
                using (LisRecordSet lrs = new LisRecordSet(PlugTbl.XvTaxPayerForm1, IMRecordset.Mode.ReadOnly))
                {
                    lrs.Select("ID");
                    lrs.Select("OWNER_ID");
                    lrs.Select("Owner.PROVINCE");
                    lrs.Select("Owner.NAME");
                    lrs.Select("Owner.REGIST_NUM");
                    lrs.Select("OWNER_ADDRESS");
                    lrs.Select("Owner.ADDRESS");
                    lrs.Select("Owner.PROVINCE");
                    lrs.Select("Owner.SUBPROVINCE");
                    lrs.Select("Owner.POSTCODE");

                    lrs.Select("Owner.CITY");
                    lrs.Select("LICENCE_ID,CHAL_ID");
                    lrs.Select("Licence.ID");
                    lrs.Select("Licence.STANDARD");
                    lrs.Select("Licence.CUST_NBR3");
                    lrs.Select("Licence.STATE");
                    lrs.Select("LIC_NUM");
                    lrs.Select("LIC_DATE_FROM");
                    lrs.Select("LIC_DATE_TO");
                    lrs.Select("FEES_ID");
                    lrs.Select("Fees.ORDER_SYMBOL");
                    lrs.Select("PROVINCE");

                    lrs.Select("BW");
                    lrs.Select("Licence.STOP_DATE");
                    lrs.Select("Licence.END_DATE");
                    if (_params.F1StatusPOnly)
                        lrs.SetWhere("Licence.STATE", IMRecordset.Operation.Eq, "P");
                    if (!(_params.F1StatusPOnly && _params.F1IgnoreTimePeriod))
                    {
                        lrs.SetWhere("LIC_DATE_FROM", IMRecordset.Operation.Lt, _params.DateEnd);
                        lrs.SetWhere("LIC_DATE_TO", IMRecordset.Operation.Ge, _params.DateBeg);
                    }
                    
                    lrs.OrderBy("LIC_NUM", OrderDirection.Ascending);
                    if (isOneFile == false)
                        lrs.OrderBy("Owner.PROVINCE", OrderDirection.Ascending);
                    for (lrs.Open(); !lrs.IsEOF(); lrs.MoveNext())
                    {
                        if (pb.UserCanceled())
                        {
                            errorList.Add("Перервано користувачем.");
                            break;
                        }
                        pb.Increment(true);
                        int recId = lrs.GetI("ID");

                        if (!(_params.F1StatusPOnly && _params.F1IgnoreTimePeriod))
                        {
                            DateTime dtLicEnd = lrs.GetT("Licence.END_DATE");
                            if ((dtLicEnd != IM.NullT) && (dtLicEnd < _params.DateBeg))
                                continue;
                        }

                        int licId = lrs.GetI("LICENCE_ID");
                        int chalId = lrs.GetI("CHAL_ID");
                        string provinceOwner = lrs.GetS("Owner.PROVINCE");
                        if (string.IsNullOrEmpty(provinceOwner))
                        {
                            //нет обоасти
                            errorList.Add(string.Format("Форма 1: Відсутня область власника. ID власника = {0}, ID ліцензії = {1}", lrs.GetI("OWNER_ID"), licId));
                            provinceOwner = "(без регіону)";
                        }

                        
                        if ((provinceOwner != lastProvince) && (isOneFile == false))
                        {
                            if (string.IsNullOrEmpty(lastProvince) == false)
                            {
                                pb.SetBig("Запис в файл...");
                                wd.Save(_params.FilePath, lastProvince, repType);
                            }
                            lastProvince = provinceOwner;
                            wd = new WriteData();
                            pb.SetBig(lastProvince);
                        }

                  // сборка данных Область, район, населенный пункт, адрес
                        String Temp="";

                        if (lrs.GetS("Owner.POSTCODE").Trim() != "")
                        {
                            Temp = lrs.GetS("Owner.POSTCODE") + ", ";
                        }

                        if ((lrs.GetS("Owner.PROVINCE").Trim() != "") && (lrs.GetS("Owner.PROVINCE").Trim() != "АР Крим"))
                        {
                            Temp = Temp + lrs.GetS("Owner.PROVINCE") + " обл.";
                        }
                        else
                        {
                            Temp = lrs.GetS("Owner.PROVINCE");
                        }
                        if (lrs.GetS("Owner.SUBPROVINCE").Trim() != "")
                        {
                            if (lrs.GetS("Owner.PROVINCE").Trim() != "")
                            {
                                Temp = Temp + ", "+lrs.GetS("Owner.SUBPROVINCE") + " р-н.";
                            }
                            if (lrs.GetS("Owner.PROVINCE").Trim() == "")
                            {
                                Temp = Temp +  lrs.GetS("Owner.SUBPROVINCE") + " р-н.";
                            }
                        }
                        if (lrs.GetS("Owner.CITY").Trim() != "")
                        {
                            if ((lrs.GetS("Owner.SUBPROVINCE").Trim() == "") && (Temp.Trim() != ""))
                            {
                                Temp = Temp + ", м." + lrs.GetS("Owner.CITY") + ".";
                            }
                            if ((lrs.GetS("Owner.SUBPROVINCE").Trim() == "") && (Temp.Trim() == ""))
                            {
                                Temp = Temp + "м." + lrs.GetS("Owner.CITY") + ".";
                            }

                            if ((lrs.GetS("Owner.SUBPROVINCE").Trim() != "") && (Temp.Trim() != ""))
                            {
                                Temp = Temp + ", с." + lrs.GetS("Owner.CITY") + ".";
                            }
                            if ((lrs.GetS("Owner.SUBPROVINCE").Trim() != "") && (Temp.Trim() == ""))
                            {
                                Temp = Temp + ", с." + lrs.GetS("Owner.CITY") + ".";
                            }
                        }
                        if (lrs.GetS("Owner.ADDRESS").Trim() != "")
                        {
                            if (Temp.Trim() != "")
                            {
                                Temp = Temp + "," +lrs.GetS("Owner.ADDRESS");
                            }
                            if (Temp.Trim() == "")
                            {
                                Temp =  lrs.GetS("Owner.ADDRESS");
                            }
                        }

                        string[] error = wd.Add(radioSystem, recId, lrs.GetS("LIC_NUM"), lrs.GetS("Owner.NAME"),
                                                lrs.GetS("Owner.REGIST_NUM"), Temp,
                                                lrs.GetT("LIC_DATE_FROM"), lrs.GetT("LIC_DATE_TO"),
                                                lrs.GetS("Fees.ORDER_SYMBOL"), 
                                                lrs.GetS("PROVINCE"), 
                                                _params.DataFrom == TaxPayerReportParams.F1Method.Lic ?
                                                    lrs.GetD("Licence.CUST_NBR3")/10 : lrs.GetD("BW"),
                                                    lrs.GetT("Licence.END_DATE"),
                                                licId, chalId);
                        errorList.AddRange(error);
                    }
                    if (!pb.UserCanceled())
                    {
                        if (string.IsNullOrEmpty(lastProvince) == false)
                            wd.Save(_params.FilePath, lastProvince, repType);
                        else if (isOneFile)
                            wd.Save(_params.FilePath, "", repType);
                    }
                }
            }
            return errorList.ToArray();
        }
    }
}
