﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    class TaxPayerReportParams
    {
        public DateTime DateBeg { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime DateStan { get; set; }
        public string FilePath { get; set; }
        public bool F1StatusPOnly { get; set; }
        public bool F1IgnoreTimePeriod { get; set; }
        public enum F1Method { Allot, Lic }
        public F1Method DataFrom { get; set; }

        public bool DoF1 { get; set; }
        public bool DoF2 { get; set; }
        public bool DoF3 { get; set; }

        protected TaxPayerReportParams()
        {
            int m = DateTime.Today.Month;
            int y = DateTime.Today.Year;
            DateBeg = new DateTime((m == 1 ? y - 1 : y), ((m == 1) || (m > 8) ? 7 : 1), 1);
            DateEnd = (m == 1) || (m == 7) ? new DateTime(y, m, 1) : DateTime.Today;
            DateStan = new DateTime(y, m, 1);
            FilePath = "";
            F1StatusPOnly = false;
            F1IgnoreTimePeriod = false;
            DataFrom = F1Method.Allot;
            DoF1 = false;
            DoF2 = false;
            DoF3 = false;
        }

        protected static TaxPayerReportParams _p = null;

        public static TaxPayerReportParams Get()
        {
            if (_p == null)
                _p = new TaxPayerReportParams();
            return _p;
        }
    }
}
