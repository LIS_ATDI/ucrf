﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class AuxiliaryEquipmentForm : Form
    {
        private AuxiliaryEquipment _equipment = new AuxiliaryEquipment();
        private ComboBoxDictionaryList<string, string> _overallDictonary = new ComboBoxDictionaryList<string, string>();

        // Для Binding ComboBox
        private ComboBoxDictionaryList<string, string> _listProvince;
        /// <summary>
        /// Список провинцей
        /// </summary>
        public List<string> ProvinceList { get; set; }
        

        public AuxiliaryEquipmentForm(string tableName, int id)
        {
            InitializeComponent();

            _equipment.TableName = tableName;
            _equipment.Id = id;
            _equipment.Load();

            Dictionary<string,string> eri = EriFiles.GetEriCodeAndDescr("AuxiliaryEquipmentType");

            SortedDictionary<string, string> eri2 = new SortedDictionary<string, string>();
            foreach (KeyValuePair<string, string> kvp in eri)
            {
                eri2.Add(kvp.Value, kvp.Key);
            }

            foreach (KeyValuePair<string, string> kvp in eri2)
            {
                _overallDictonary.Add(new ComboBoxDictionary<string,string>(kvp.Value, kvp.Key));
            }

            _overallDictonary.InitComboBox(cbEquipmentType);
            cbEquipmentType.DataBindings.Add("SelectedValue", _equipment, "EquipmentType", true);

            txtName.DataBindings.Add("Text", _equipment, "Name", true);           
            txtNote.DataBindings.Add("Text", _equipment, "Note", true);
        }

        public void SaveAppl()
        {            
            _equipment.Save();
        }

        private void AuxiliaryEquipmentForm_Load(object sender, EventArgs e)
        {
            _listProvince = new ComboBoxDictionaryList<string, string>();
            // Загрузка списка областей
            HashSet<string> hashProvince = new HashSet<string>();
            IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            rsArea.Select("ID,NAME");
            rsArea.OrderBy("NAME", OrderDirection.Ascending);

            cbRegion.Items.Clear();
            try
            {
                for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                {
                    string province = rsArea.GetS("NAME");
                    if (!hashProvince.Contains(province))
                    {
                        hashProvince.Add(province);
                        //cbRegion.Items.Add(province);
                        _listProvince.Add(new ComboBoxDictionary<string, string>(province, province));
                    }
                }
            }
            finally
            {
                if (rsArea.IsOpen())
                    rsArea.Close();
                rsArea.Destroy();
            }

            _listProvince.InitComboBox(cbRegion);
            cbRegion.DataBindings.Add("SelectedValue", _equipment, "Province", true);
        }
    }
}
