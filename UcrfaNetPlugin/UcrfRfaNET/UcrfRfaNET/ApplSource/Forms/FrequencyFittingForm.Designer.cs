﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class FrequencyFittingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkMicrowave = new System.Windows.Forms.CheckBox();
            this.checkMobStation2 = new System.Windows.Forms.CheckBox();
            this.checkEarthStation = new System.Windows.Forms.CheckBox();
            this.cbExcludeCertainOperator = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaxRange = new System.Windows.Forms.TextBox();
            this.tbThreshold = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.btRemove = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbChannelList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // checkMicrowave
            // 
            this.checkMicrowave.AutoSize = true;
            this.checkMicrowave.Location = new System.Drawing.Point(12, 12);
            this.checkMicrowave.Name = "checkMicrowave";
            this.checkMicrowave.Size = new System.Drawing.Size(48, 17);
            this.checkMicrowave.TabIndex = 0;
            this.checkMicrowave.Text = "РРЛ";
            this.checkMicrowave.UseVisualStyleBackColor = true;
            // 
            // checkMobStation2
            // 
            this.checkMobStation2.AutoSize = true;
            this.checkMobStation2.Location = new System.Drawing.Point(66, 12);
            this.checkMobStation2.Name = "checkMobStation2";
            this.checkMobStation2.Size = new System.Drawing.Size(47, 17);
            this.checkMobStation2.TabIndex = 1;
            this.checkMobStation2.Text = "СРТ";
            this.checkMobStation2.UseVisualStyleBackColor = true;
            // 
            // checkEarthStation
            // 
            this.checkEarthStation.AutoSize = true;
            this.checkEarthStation.Location = new System.Drawing.Point(119, 12);
            this.checkEarthStation.Name = "checkEarthStation";
            this.checkEarthStation.Size = new System.Drawing.Size(40, 17);
            this.checkEarthStation.TabIndex = 2;
            this.checkEarthStation.Text = "ЗС";
            this.checkEarthStation.UseVisualStyleBackColor = true;
            // 
            // cbExcludeCertainOperator
            // 
            this.cbExcludeCertainOperator.AutoSize = true;
            this.cbExcludeCertainOperator.Location = new System.Drawing.Point(12, 35);
            this.cbExcludeCertainOperator.Name = "cbExcludeCertainOperator";
            this.cbExcludeCertainOperator.Size = new System.Drawing.Size(237, 17);
            this.cbExcludeCertainOperator.TabIndex = 3;
            this.cbExcludeCertainOperator.Text = "Не враховувати станції даного оператора";
            this.cbExcludeCertainOperator.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Відстань пошуку завад,  км";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Граничне значення порогу (TD), дБ";
            // 
            // txtMaxRange
            // 
            this.txtMaxRange.Location = new System.Drawing.Point(210, 75);
            this.txtMaxRange.Name = "txtMaxRange";
            this.txtMaxRange.Size = new System.Drawing.Size(63, 20);
            this.txtMaxRange.TabIndex = 6;
            // 
            // tbThreshold
            // 
            this.tbThreshold.Location = new System.Drawing.Point(210, 104);
            this.tbThreshold.Name = "tbThreshold";
            this.tbThreshold.Size = new System.Drawing.Size(63, 20);
            this.tbThreshold.TabIndex = 7;
            this.tbThreshold.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(15, 224);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(177, 23);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Розпочати підбір частот";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(201, 162);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(75, 23);
            this.btAdd.TabIndex = 10;
            this.btAdd.Text = "Додати";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btRemove
            // 
            this.btRemove.Location = new System.Drawing.Point(201, 191);
            this.btRemove.Name = "btRemove";
            this.btRemove.Size = new System.Drawing.Size(75, 23);
            this.btRemove.TabIndex = 11;
            this.btRemove.Text = "Видалити";
            this.btRemove.UseVisualStyleBackColor = true;
            this.btRemove.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Канальні плани:";
            // 
            // lbChannelList
            // 
            this.lbChannelList.FormattingEnabled = true;
            this.lbChannelList.HorizontalScrollbar = true;
            this.lbChannelList.Location = new System.Drawing.Point(15, 162);
            this.lbChannelList.Name = "lbChannelList";
            this.lbChannelList.Size = new System.Drawing.Size(177, 56);
            this.lbChannelList.TabIndex = 13;
            // 
            // FrequencyFittingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 261);
            this.Controls.Add(this.lbChannelList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btRemove);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tbThreshold);
            this.Controls.Add(this.txtMaxRange);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbExcludeCertainOperator);
            this.Controls.Add(this.checkEarthStation);
            this.Controls.Add(this.checkMobStation2);
            this.Controls.Add(this.checkMicrowave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrequencyFittingForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Підбір частот";
            this.Load += new System.EventHandler(this.FrequencyFittingForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkMicrowave;
        private System.Windows.Forms.CheckBox checkMobStation2;
        private System.Windows.Forms.CheckBox checkEarthStation;
        private System.Windows.Forms.CheckBox cbExcludeCertainOperator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaxRange;
        private System.Windows.Forms.TextBox tbThreshold;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btRemove;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbChannelList;
    }
}