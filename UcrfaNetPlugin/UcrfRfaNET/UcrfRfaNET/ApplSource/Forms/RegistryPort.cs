﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    internal class RegistryPort
    {
        private const string TableName = PlugTbl.RegistryPort;
        /// <summary>
        /// ID
        /// </summary>
        public int Id {get; protected set;}
        public const string FieldNameUkr = "NameUkr";
        /// <summary>
        /// Укр название
        /// </summary>
        public string NameUkr { get; set; }
        public const string FieldNameEng = "NameEng";
        /// <summary>
        /// Кирилица название
        /// </summary>
        public string NameEng { get; set; }
        public const string FieldProvince = "Province";
        /// <summary>
        /// Область прописки
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// Кто создал
        /// </summary>
        public string CreatedBy { get; protected set; }
        /// <summary>
        /// Когда создал
        /// </summary>
        public DateTime CreatedDate { get; protected set; }
        /// <summary>
        /// Кто изменил
        /// </summary>
        public string ModifiedBy { get; protected set; }

        public const string FieldModifiedDate = "ModifiedDate";
        /// <summary>
        /// Когда изменил
        /// </summary>
        public DateTime ModifiedDate { get; protected set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public RegistryPort()
        {
            Id = IM.NullI;
            NameUkr = "";
            NameEng = "";
            Province = "";
            CreatedBy = "";
            CreatedDate = IM.NullT;
            ModifiedBy = "";
            ModifiedDate = IM.NullT;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">Id записи</param>
        public RegistryPort(int id) : this()
        {
            Load(id);
        }
        /// <summary>
        /// Загрузить запись
        /// </summary>
        /// <param name="id">Id записи</param>
        public void Load(int id)
        {
            IMRecordset rsPort = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                rsPort.Select("ID");
                rsPort.Select("NAME_UKR");
                rsPort.Select("NAME_ENG");
                rsPort.Select("PROVINCE");
                rsPort.Select("CREATED_BY");
                rsPort.Select("CREATED_DATE");
                rsPort.Select("MODIFIED_BY");
                rsPort.Select("MODIFIED_DATE");
                rsPort.SetWhere("ID", IMRecordset.Operation.Eq, id);
                rsPort.Open();
                if(!rsPort.IsEOF())
                {
                    Id = rsPort.GetI("ID");
                    NameUkr = rsPort.GetS("NAME_UKR");
                    NameEng = rsPort.GetS("NAME_ENG");
                    Province = rsPort.GetS("PROVINCE");
                    CreatedBy = rsPort.GetS("CREATED_BY");
                    CreatedDate = rsPort.GetT("CREATED_DATE");
                    ModifiedBy = rsPort.GetS("MODIFIED_BY");
                    ModifiedDate = rsPort.GetT("MODIFIED_DATE");
                }
            }
            finally
            {
                rsPort.Final();
            }
        }
        /// <summary>
        /// Созранить запись
        /// </summary>
        public void Save()
        {
            IMRecordset rsPort = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                rsPort.Select("ID");
                rsPort.Select("NAME_UKR");
                rsPort.Select("NAME_ENG");
                rsPort.Select("PROVINCE");
                rsPort.Select("CREATED_BY");
                rsPort.Select("CREATED_DATE");
                rsPort.Select("MODIFIED_BY");
                rsPort.Select("MODIFIED_DATE");
                rsPort.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                rsPort.Open();
                if (rsPort.IsEOF())
                {
                    rsPort.AddNew();
                    Id = IM.AllocID(TableName, 1, -1);
                    CreatedBy = IM.ConnectedUser();
                    CreatedDate = DateTime.Now;
                    rsPort.Put("ID", Id);
                    rsPort.Put("CREATED_BY", CreatedBy);
                    rsPort.Put("CREATED_DATE", CreatedDate);
                }
                else
                {
                    rsPort.Edit();
                    ModifiedBy = IM.ConnectedUser();
                    ModifiedDate = DateTime.Now;
                    rsPort.Put("MODIFIED_BY", ModifiedBy);
                    rsPort.Put("MODIFIED_DATE", ModifiedDate);
                }
                rsPort.Put("NAME_UKR", NameUkr);
                rsPort.Put("NAME_ENG", NameEng);
                rsPort.Put("PROVINCE", Province);
                rsPort.Update();
            }
            finally
            {
                rsPort.Final();
            }
        }
    }
}
