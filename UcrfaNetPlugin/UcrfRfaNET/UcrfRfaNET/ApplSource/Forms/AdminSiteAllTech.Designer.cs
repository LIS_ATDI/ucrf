﻿using System.ComponentModel;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class AdminSiteAllTech
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxLatLow = new System.Windows.Forms.TextBox();
            this.txtBoxLongLow = new System.Windows.Forms.TextBox();
            this.lblLatitude = new System.Windows.Forms.Label();
            this.lblLongitude = new System.Windows.Forms.Label();
            this.grpBoxKoord = new System.Windows.Forms.GroupBox();
            this.lblLong = new System.Windows.Forms.Label();
            this.lblLat = new System.Windows.Forms.Label();
            this.btnSaveExit = new System.Windows.Forms.Button();
            this.btnCancelExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnICSMSite = new System.Windows.Forms.Button();
            this.lblDataCoatuu = new System.Windows.Forms.Label();
            this.txtBoxKoatuu = new System.Windows.Forms.TextBox();
            this.lblAddres = new System.Windows.Forms.Label();
            this.txtBoxAddress = new System.Windows.Forms.TextBox();
            this.lblAsl = new System.Windows.Forms.Label();
            this.btnMapToAsl = new System.Windows.Forms.Button();
            this.lblLimit = new System.Windows.Forms.Label();
            this.txtBoxLimit = new System.Windows.Forms.TextBox();
            this.grpBoxAddress = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pnlAddress = new System.Windows.Forms.Panel();
            this.txtBoxAsl = new System.Windows.Forms.TextBox();
            this.grpBoxMap = new System.Windows.Forms.GroupBox();
            this.btnFull = new System.Windows.Forms.Button();
            this.grpBoxPhoto = new System.Windows.Forms.GroupBox();
            this.pctBox = new System.Windows.Forms.PictureBox();
            this.btndel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbBoxPhoto = new System.Windows.Forms.ComboBox();
            this.txtBoxAdmSite = new System.Windows.Forms.TextBox();
            this.btnChangeAdmSite = new System.Windows.Forms.Button();
            this.btnOffAdmSite = new System.Windows.Forms.Button();
            this.grpBoxAdmSite = new System.Windows.Forms.GroupBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.lblCheckName = new System.Windows.Forms.Label();
            this.lblCheckDate = new System.Windows.Forms.Label();
            this.lblAcceptDate = new System.Windows.Forms.Label();
            this.lblAcceptName = new System.Windows.Forms.Label();
            this.grpBoxCheck = new System.Windows.Forms.GroupBox();
            this.btnError = new System.Windows.Forms.Button();
            this.lblAcceptNameVal = new System.Windows.Forms.Label();
            this.lblAcceptDateVal = new System.Windows.Forms.Label();
            this.lblCheckDateVal = new System.Windows.Forms.Label();
            this.lblCheckNamVal = new System.Windows.Forms.Label();
            this.txtBoxRemark = new System.Windows.Forms.TextBox();
            this.gbNote = new System.Windows.Forms.GroupBox();
            this.grpBoxKoord.SuspendLayout();
            this.grpBoxAddress.SuspendLayout();
            this.grpBoxMap.SuspendLayout();
            this.grpBoxPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).BeginInit();
            this.grpBoxAdmSite.SuspendLayout();
            this.grpBoxCheck.SuspendLayout();
            this.gbNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBoxLatLow
            // 
            this.txtBoxLatLow.Location = new System.Drawing.Point(65, 46);
            this.txtBoxLatLow.Name = "txtBoxLatLow";
            this.txtBoxLatLow.Size = new System.Drawing.Size(128, 20);
            this.txtBoxLatLow.TabIndex = 2;
            this.txtBoxLatLow.TextChanged += new System.EventHandler(this.txtBoxLatLow_TextChanged);
            // 
            // txtBoxLongLow
            // 
            this.txtBoxLongLow.Location = new System.Drawing.Point(281, 46);
            this.txtBoxLongLow.Name = "txtBoxLongLow";
            this.txtBoxLongLow.Size = new System.Drawing.Size(122, 20);
            this.txtBoxLongLow.TabIndex = 3;
            this.txtBoxLongLow.TextChanged += new System.EventHandler(this.txtBoxLongLow_TextChanged);
            // 
            // lblLatitude
            // 
            this.lblLatitude.AutoSize = true;
            this.lblLatitude.Location = new System.Drawing.Point(8, 23);
            this.lblLatitude.Name = "lblLatitude";
            this.lblLatitude.Size = new System.Drawing.Size(48, 13);
            this.lblLatitude.TabIndex = 0;
            this.lblLatitude.Text = "Широта:";
            // 
            // lblLongitude
            // 
            this.lblLongitude.AutoSize = true;
            this.lblLongitude.Location = new System.Drawing.Point(212, 23);
            this.lblLongitude.Name = "lblLongitude";
            this.lblLongitude.Size = new System.Drawing.Size(53, 13);
            this.lblLongitude.TabIndex = 0;
            this.lblLongitude.Text = "Довгота:";
            // 
            // grpBoxKoord
            // 
            this.grpBoxKoord.Controls.Add(this.lblLong);
            this.grpBoxKoord.Controls.Add(this.lblLat);
            this.grpBoxKoord.Controls.Add(this.lblLongitude);
            this.grpBoxKoord.Controls.Add(this.lblLatitude);
            this.grpBoxKoord.Controls.Add(this.txtBoxLongLow);
            this.grpBoxKoord.Controls.Add(this.txtBoxLatLow);
            this.grpBoxKoord.Location = new System.Drawing.Point(8, 3);
            this.grpBoxKoord.Name = "grpBoxKoord";
            this.grpBoxKoord.Size = new System.Drawing.Size(420, 75);
            this.grpBoxKoord.TabIndex = 1;
            this.grpBoxKoord.TabStop = false;
            this.grpBoxKoord.Text = "Географічні координати";
            // 
            // lblLong
            // 
            this.lblLong.BackColor = System.Drawing.SystemColors.Control;
            this.lblLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLong.Location = new System.Drawing.Point(281, 23);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(123, 15);
            this.lblLong.TabIndex = 0;
            // 
            // lblLat
            // 
            this.lblLat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLat.Location = new System.Drawing.Point(65, 23);
            this.lblLat.Name = "lblLat";
            this.lblLat.Size = new System.Drawing.Size(129, 15);
            this.lblLat.TabIndex = 0;
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveExit.Location = new System.Drawing.Point(13, 698);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Size = new System.Drawing.Size(110, 23);
            this.btnSaveExit.TabIndex = 16;
            this.btnSaveExit.Text = "Зберегти та вийти";
            this.btnSaveExit.UseVisualStyleBackColor = true;
            this.btnSaveExit.Click += new System.EventHandler(this.btnSaveExit_Click);
            // 
            // btnCancelExit
            // 
            this.btnCancelExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelExit.Location = new System.Drawing.Point(132, 698);
            this.btnCancelExit.Name = "btnCancelExit";
            this.btnCancelExit.Size = new System.Drawing.Size(111, 23);
            this.btnCancelExit.TabIndex = 17;
            this.btnCancelExit.Text = "Відмінити та вийти";
            this.btnCancelExit.UseVisualStyleBackColor = true;
            this.btnCancelExit.Click += new System.EventHandler(this.btnCancelExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(249, 698);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Зберегти зміни";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnICSMSite
            // 
            this.btnICSMSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnICSMSite.Location = new System.Drawing.Point(351, 698);
            this.btnICSMSite.Name = "btnICSMSite";
            this.btnICSMSite.Size = new System.Drawing.Size(77, 23);
            this.btnICSMSite.TabIndex = 19;
            this.btnICSMSite.Text = "ICSM Сайт";
            this.btnICSMSite.UseVisualStyleBackColor = true;
            this.btnICSMSite.Click += new System.EventHandler(this.btnICSMSite_Click);
            // 
            // lblDataCoatuu
            // 
            this.lblDataCoatuu.AutoSize = true;
            this.lblDataCoatuu.Location = new System.Drawing.Point(9, 227);
            this.lblDataCoatuu.Name = "lblDataCoatuu";
            this.lblDataCoatuu.Size = new System.Drawing.Size(81, 13);
            this.lblDataCoatuu.TabIndex = 100;
            this.lblDataCoatuu.Text = "Дані КОАТУУ:";
            // 
            // txtBoxKoatuu
            // 
            this.txtBoxKoatuu.Location = new System.Drawing.Point(96, 227);
            this.txtBoxKoatuu.Multiline = true;
            this.txtBoxKoatuu.Name = "txtBoxKoatuu";
            this.txtBoxKoatuu.ReadOnly = true;
            this.txtBoxKoatuu.Size = new System.Drawing.Size(311, 45);
            this.txtBoxKoatuu.TabIndex = 100;
            // 
            // lblAddres
            // 
            this.lblAddres.AutoEllipsis = true;
            this.lblAddres.AutoSize = true;
            this.lblAddres.Location = new System.Drawing.Point(6, 256);
            this.lblAddres.Name = "lblAddres";
            this.lblAddres.Size = new System.Drawing.Size(41, 13);
            this.lblAddres.TabIndex = 100;
            this.lblAddres.Text = "Адрес:";
            // 
            // txtBoxAddress
            // 
            this.txtBoxAddress.Location = new System.Drawing.Point(11, 276);
            this.txtBoxAddress.Multiline = true;
            this.txtBoxAddress.Name = "txtBoxAddress";
            this.txtBoxAddress.Size = new System.Drawing.Size(396, 32);
            this.txtBoxAddress.TabIndex = 6;
            // 
            // lblAsl
            // 
            this.lblAsl.AutoSize = true;
            this.lblAsl.Location = new System.Drawing.Point(6, 313);
            this.lblAsl.Name = "lblAsl";
            this.lblAsl.Size = new System.Drawing.Size(96, 13);
            this.lblAsl.TabIndex = 100;
            this.lblAsl.Text = "Над рівнем моря:";
            // 
            // btnMapToAsl
            // 
            this.btnMapToAsl.Location = new System.Drawing.Point(138, 327);
            this.btnMapToAsl.Name = "btnMapToAsl";
            this.btnMapToAsl.Size = new System.Drawing.Size(40, 23);
            this.btnMapToAsl.TabIndex = 8;
            this.btnMapToAsl.Text = "<=";
            this.btnMapToAsl.UseVisualStyleBackColor = true;
            this.btnMapToAsl.Click += new System.EventHandler(this.btnMapToAsl_Click);
            // 
            // lblLimit
            // 
            this.lblLimit.AutoSize = true;
            this.lblLimit.Location = new System.Drawing.Point(294, 312);
            this.lblLimit.Name = "lblLimit";
            this.lblLimit.Size = new System.Drawing.Size(115, 13);
            this.lblLimit.TabIndex = 100;
            this.lblLimit.Text = "Відстань від кордону:";
            // 
            // txtBoxLimit
            // 
            this.txtBoxLimit.Location = new System.Drawing.Point(305, 329);
            this.txtBoxLimit.Name = "txtBoxLimit";
            this.txtBoxLimit.ReadOnly = true;
            this.txtBoxLimit.Size = new System.Drawing.Size(102, 20);
            this.txtBoxLimit.TabIndex = 100;
            // 
            // grpBoxAddress
            // 
            this.grpBoxAddress.Controls.Add(this.label1);
            this.grpBoxAddress.Controls.Add(this.textBox1);
            this.grpBoxAddress.Controls.Add(this.pnlAddress);
            this.grpBoxAddress.Controls.Add(this.txtBoxAsl);
            this.grpBoxAddress.Controls.Add(this.txtBoxLimit);
            this.grpBoxAddress.Controls.Add(this.lblLimit);
            this.grpBoxAddress.Controls.Add(this.btnMapToAsl);
            this.grpBoxAddress.Controls.Add(this.lblAsl);
            this.grpBoxAddress.Controls.Add(this.txtBoxAddress);
            this.grpBoxAddress.Controls.Add(this.lblAddres);
            this.grpBoxAddress.Controls.Add(this.txtBoxKoatuu);
            this.grpBoxAddress.Controls.Add(this.lblDataCoatuu);
            this.grpBoxAddress.Location = new System.Drawing.Point(7, 79);
            this.grpBoxAddress.Name = "grpBoxAddress";
            this.grpBoxAddress.Size = new System.Drawing.Size(420, 355);
            this.grpBoxAddress.TabIndex = 4;
            this.grpBoxAddress.TabStop = false;
            this.grpBoxAddress.Text = "Адреса встановлення";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 102;
            this.label1.Text = "Повна адреса:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(96, 19);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(313, 46);
            this.textBox1.TabIndex = 101;
            this.textBox1.Text = "Поки не доступна.\r\nФункціонал розробляється.";
            // 
            // pnlAddress
            // 
            this.pnlAddress.Location = new System.Drawing.Point(11, 71);
            this.pnlAddress.Name = "pnlAddress";
            this.pnlAddress.Size = new System.Drawing.Size(396, 150);
            this.pnlAddress.TabIndex = 5;
            // 
            // txtBoxAsl
            // 
            this.txtBoxAsl.Location = new System.Drawing.Point(11, 329);
            this.txtBoxAsl.Name = "txtBoxAsl";
            this.txtBoxAsl.Size = new System.Drawing.Size(102, 20);
            this.txtBoxAsl.TabIndex = 7;
            // 
            // grpBoxMap
            // 
            this.grpBoxMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxMap.Controls.Add(this.btnFull);
            this.grpBoxMap.Location = new System.Drawing.Point(433, 4);
            this.grpBoxMap.Name = "grpBoxMap";
            this.grpBoxMap.Size = new System.Drawing.Size(365, 381);
            this.grpBoxMap.TabIndex = 20;
            this.grpBoxMap.TabStop = false;
            this.grpBoxMap.Text = "Мапа";
            this.grpBoxMap.SizeChanged += new System.EventHandler(this.grpBoxMap_SizeChanged);
            // 
            // btnFull
            // 
            this.btnFull.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFull.Location = new System.Drawing.Point(6, 358);
            this.btnFull.Name = "btnFull";
            this.btnFull.Size = new System.Drawing.Size(355, 23);
            this.btnFull.TabIndex = 25;
            this.btnFull.Text = "Показати карту в окремому вікні";
            this.btnFull.UseVisualStyleBackColor = true;
            this.btnFull.Click += new System.EventHandler(this.btnFull_Click);
            // 
            // grpBoxPhoto
            // 
            this.grpBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxPhoto.Controls.Add(this.pctBox);
            this.grpBoxPhoto.Controls.Add(this.btndel);
            this.grpBoxPhoto.Controls.Add(this.btnAdd);
            this.grpBoxPhoto.Controls.Add(this.cmbBoxPhoto);
            this.grpBoxPhoto.Location = new System.Drawing.Point(433, 390);
            this.grpBoxPhoto.Name = "grpBoxPhoto";
            this.grpBoxPhoto.Size = new System.Drawing.Size(362, 337);
            this.grpBoxPhoto.TabIndex = 21;
            this.grpBoxPhoto.TabStop = false;
            this.grpBoxPhoto.Text = "Фото";
            // 
            // pctBox
            // 
            this.pctBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pctBox.ErrorImage = null;
            this.pctBox.Location = new System.Drawing.Point(6, 50);
            this.pctBox.Name = "pctBox";
            this.pctBox.Size = new System.Drawing.Size(347, 281);
            this.pctBox.TabIndex = 3;
            this.pctBox.TabStop = false;
            this.pctBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pctBox_MouseDoubleClick);
            // 
            // btndel
            // 
            this.btndel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btndel.Location = new System.Drawing.Point(278, 14);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(75, 23);
            this.btndel.TabIndex = 24;
            this.btndel.Text = "Вилучити";
            this.btndel.UseVisualStyleBackColor = true;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(175, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 23;
            this.btnAdd.Text = "Додати";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbBoxPhoto
            // 
            this.cmbBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxPhoto.FormattingEnabled = true;
            this.cmbBoxPhoto.Location = new System.Drawing.Point(4, 16);
            this.cmbBoxPhoto.Name = "cmbBoxPhoto";
            this.cmbBoxPhoto.Size = new System.Drawing.Size(130, 21);
            this.cmbBoxPhoto.TabIndex = 22;
            // 
            // txtBoxAdmSite
            // 
            this.txtBoxAdmSite.Location = new System.Drawing.Point(11, 19);
            this.txtBoxAdmSite.Multiline = true;
            this.txtBoxAdmSite.Name = "txtBoxAdmSite";
            this.txtBoxAdmSite.ReadOnly = true;
            this.txtBoxAdmSite.Size = new System.Drawing.Size(396, 40);
            this.txtBoxAdmSite.TabIndex = 0;
            // 
            // btnChangeAdmSite
            // 
            this.btnChangeAdmSite.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnChangeAdmSite.Location = new System.Drawing.Point(11, 60);
            this.btnChangeAdmSite.Name = "btnChangeAdmSite";
            this.btnChangeAdmSite.Size = new System.Drawing.Size(110, 23);
            this.btnChangeAdmSite.TabIndex = 10;
            this.btnChangeAdmSite.Text = "Змінити";
            this.btnChangeAdmSite.UseVisualStyleBackColor = true;
            this.btnChangeAdmSite.Click += new System.EventHandler(this.btnChangeAdmSite_Click);
            // 
            // btnOffAdmSite
            // 
            this.btnOffAdmSite.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOffAdmSite.Location = new System.Drawing.Point(298, 60);
            this.btnOffAdmSite.Name = "btnOffAdmSite";
            this.btnOffAdmSite.Size = new System.Drawing.Size(110, 23);
            this.btnOffAdmSite.TabIndex = 11;
            this.btnOffAdmSite.Text = "Відключити";
            this.btnOffAdmSite.UseVisualStyleBackColor = true;
            this.btnOffAdmSite.Click += new System.EventHandler(this.btnOffAdmSite_Click);
            // 
            // grpBoxAdmSite
            // 
            this.grpBoxAdmSite.Controls.Add(this.btnOffAdmSite);
            this.grpBoxAdmSite.Controls.Add(this.btnChangeAdmSite);
            this.grpBoxAdmSite.Controls.Add(this.txtBoxAdmSite);
            this.grpBoxAdmSite.Location = new System.Drawing.Point(6, 440);
            this.grpBoxAdmSite.Name = "grpBoxAdmSite";
            this.grpBoxAdmSite.Size = new System.Drawing.Size(420, 87);
            this.grpBoxAdmSite.TabIndex = 9;
            this.grpBoxAdmSite.TabStop = false;
            this.grpBoxAdmSite.Text = "Адміністративний сайт";
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnAccept.Location = new System.Drawing.Point(270, 15);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(135, 23);
            this.btnAccept.TabIndex = 14;
            this.btnAccept.Text = "Перевірка проведена";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCheck.Location = new System.Drawing.Point(9, 16);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(123, 23);
            this.btnCheck.TabIndex = 13;
            this.btnCheck.Text = "Потрібна перевірка";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // lblCheckName
            // 
            this.lblCheckName.AutoSize = true;
            this.lblCheckName.Location = new System.Drawing.Point(10, 44);
            this.lblCheckName.Name = "lblCheckName";
            this.lblCheckName.Size = new System.Drawing.Size(93, 13);
            this.lblCheckName.TabIndex = 40;
            this.lblCheckName.Text = "Запит перевірки:";
            // 
            // lblCheckDate
            // 
            this.lblCheckDate.AutoSize = true;
            this.lblCheckDate.Location = new System.Drawing.Point(10, 66);
            this.lblCheckDate.Name = "lblCheckDate";
            this.lblCheckDate.Size = new System.Drawing.Size(36, 13);
            this.lblCheckDate.TabIndex = 41;
            this.lblCheckDate.Text = "Дата:";
            // 
            // lblAcceptDate
            // 
            this.lblAcceptDate.AutoSize = true;
            this.lblAcceptDate.Location = new System.Drawing.Point(270, 66);
            this.lblAcceptDate.Name = "lblAcceptDate";
            this.lblAcceptDate.Size = new System.Drawing.Size(36, 13);
            this.lblAcceptDate.TabIndex = 43;
            this.lblAcceptDate.Text = "Дата:";
            // 
            // lblAcceptName
            // 
            this.lblAcceptName.AutoSize = true;
            this.lblAcceptName.Location = new System.Drawing.Point(270, 44);
            this.lblAcceptName.Name = "lblAcceptName";
            this.lblAcceptName.Size = new System.Drawing.Size(65, 13);
            this.lblAcceptName.TabIndex = 42;
            this.lblAcceptName.Text = "Перевірив :";
            // 
            // grpBoxCheck
            // 
            this.grpBoxCheck.Controls.Add(this.btnError);
            this.grpBoxCheck.Controls.Add(this.lblAcceptNameVal);
            this.grpBoxCheck.Controls.Add(this.lblAcceptDateVal);
            this.grpBoxCheck.Controls.Add(this.lblCheckDateVal);
            this.grpBoxCheck.Controls.Add(this.lblCheckNamVal);
            this.grpBoxCheck.Controls.Add(this.lblAcceptDate);
            this.grpBoxCheck.Controls.Add(this.lblAcceptName);
            this.grpBoxCheck.Controls.Add(this.lblCheckDate);
            this.grpBoxCheck.Controls.Add(this.lblCheckName);
            this.grpBoxCheck.Controls.Add(this.btnAccept);
            this.grpBoxCheck.Controls.Add(this.btnCheck);
            this.grpBoxCheck.Location = new System.Drawing.Point(7, 533);
            this.grpBoxCheck.Name = "grpBoxCheck";
            this.grpBoxCheck.Size = new System.Drawing.Size(421, 89);
            this.grpBoxCheck.TabIndex = 12;
            this.grpBoxCheck.TabStop = false;
            this.grpBoxCheck.Text = "Перевірка координат та адреси встановлення";
            // 
            // btnError
            // 
            this.btnError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnError.Image = global::XICSM.UcrfRfaNET.Properties.Resources.cancel;
            this.btnError.Location = new System.Drawing.Point(180, 14);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(35, 24);
            this.btnError.TabIndex = 48;
            this.btnError.UseVisualStyleBackColor = true;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // lblAcceptNameVal
            // 
            this.lblAcceptNameVal.AutoSize = true;
            this.lblAcceptNameVal.Location = new System.Drawing.Point(341, 44);
            this.lblAcceptNameVal.Name = "lblAcceptNameVal";
            this.lblAcceptNameVal.Size = new System.Drawing.Size(0, 13);
            this.lblAcceptNameVal.TabIndex = 47;
            // 
            // lblAcceptDateVal
            // 
            this.lblAcceptDateVal.AutoSize = true;
            this.lblAcceptDateVal.Location = new System.Drawing.Point(314, 66);
            this.lblAcceptDateVal.Name = "lblAcceptDateVal";
            this.lblAcceptDateVal.Size = new System.Drawing.Size(0, 13);
            this.lblAcceptDateVal.TabIndex = 46;
            // 
            // lblCheckDateVal
            // 
            this.lblCheckDateVal.AutoSize = true;
            this.lblCheckDateVal.Location = new System.Drawing.Point(52, 66);
            this.lblCheckDateVal.Name = "lblCheckDateVal";
            this.lblCheckDateVal.Size = new System.Drawing.Size(0, 13);
            this.lblCheckDateVal.TabIndex = 45;
            // 
            // lblCheckNamVal
            // 
            this.lblCheckNamVal.AutoSize = true;
            this.lblCheckNamVal.Location = new System.Drawing.Point(109, 44);
            this.lblCheckNamVal.Name = "lblCheckNamVal";
            this.lblCheckNamVal.Size = new System.Drawing.Size(0, 13);
            this.lblCheckNamVal.TabIndex = 44;
            // 
            // txtBoxRemark
            // 
            this.txtBoxRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBoxRemark.Location = new System.Drawing.Point(3, 16);
            this.txtBoxRemark.Multiline = true;
            this.txtBoxRemark.Name = "txtBoxRemark";
            this.txtBoxRemark.Size = new System.Drawing.Size(414, 47);
            this.txtBoxRemark.TabIndex = 15;
            // 
            // gbNote
            // 
            this.gbNote.Controls.Add(this.txtBoxRemark);
            this.gbNote.Location = new System.Drawing.Point(9, 626);
            this.gbNote.Name = "gbNote";
            this.gbNote.Size = new System.Drawing.Size(420, 66);
            this.gbNote.TabIndex = 45;
            this.gbNote.TabStop = false;
            this.gbNote.Text = "Примітка";
            // 
            // AdminSiteAllTech
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 741);
            this.Controls.Add(this.gbNote);
            this.Controls.Add(this.grpBoxCheck);
            this.Controls.Add(this.grpBoxAdmSite);
            this.Controls.Add(this.grpBoxPhoto);
            this.Controls.Add(this.grpBoxMap);
            this.Controls.Add(this.grpBoxAddress);
            this.Controls.Add(this.btnICSMSite);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancelExit);
            this.Controls.Add(this.btnSaveExit);
            this.Controls.Add(this.grpBoxKoord);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(720, 665);
            this.Name = "AdminSiteAllTech";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Форма ВРВ_Сайти";
            this.grpBoxKoord.ResumeLayout(false);
            this.grpBoxKoord.PerformLayout();
            this.grpBoxAddress.ResumeLayout(false);
            this.grpBoxAddress.PerformLayout();
            this.grpBoxMap.ResumeLayout(false);
            this.grpBoxPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).EndInit();
            this.grpBoxAdmSite.ResumeLayout(false);
            this.grpBoxAdmSite.PerformLayout();
            this.grpBoxCheck.ResumeLayout(false);
            this.grpBoxCheck.PerformLayout();
            this.gbNote.ResumeLayout(false);
            this.gbNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxLatLow;
        private System.Windows.Forms.TextBox txtBoxLongLow;
        private System.Windows.Forms.Label lblLatitude;
        private System.Windows.Forms.Label lblLongitude;
        private System.Windows.Forms.GroupBox grpBoxKoord;
        private System.Windows.Forms.Button btnSaveExit;
        private System.Windows.Forms.Button btnCancelExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnICSMSite;
        private System.Windows.Forms.Label lblDataCoatuu;
        private System.Windows.Forms.TextBox txtBoxKoatuu;
        private System.Windows.Forms.Label lblAddres;
        private System.Windows.Forms.TextBox txtBoxAddress;
        private System.Windows.Forms.Label lblAsl;
        private System.Windows.Forms.Button btnMapToAsl;
        private System.Windows.Forms.Label lblLimit;
        private System.Windows.Forms.TextBox txtBoxLimit;
        private System.Windows.Forms.GroupBox grpBoxAddress;
        private System.Windows.Forms.TextBox txtBoxAsl;
        private System.Windows.Forms.Label lblLong;
        private System.Windows.Forms.Label lblLat;
        private System.Windows.Forms.GroupBox grpBoxMap;
        private System.Windows.Forms.GroupBox grpBoxPhoto;
        private System.Windows.Forms.PictureBox pctBox;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbBoxPhoto;
        private System.Windows.Forms.Panel pnlAddress;
        private System.Windows.Forms.TextBox txtBoxAdmSite;
        private System.Windows.Forms.Button btnChangeAdmSite;
        private System.Windows.Forms.Button btnOffAdmSite;
        private System.Windows.Forms.GroupBox grpBoxAdmSite;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label lblCheckName;
        private System.Windows.Forms.Label lblCheckDate;
        private System.Windows.Forms.Label lblAcceptDate;
        private System.Windows.Forms.Label lblAcceptName;
        private System.Windows.Forms.GroupBox grpBoxCheck;
        private System.Windows.Forms.TextBox txtBoxRemark;
        private System.Windows.Forms.Button btnFull;
        private System.Windows.Forms.Label lblAcceptNameVal;
        private System.Windows.Forms.Label lblAcceptDateVal;
        private System.Windows.Forms.Label lblCheckDateVal;
        private System.Windows.Forms.Label lblCheckNamVal;
        private System.Windows.Forms.Button btnError;
        private System.Windows.Forms.GroupBox gbNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;        
    }
}