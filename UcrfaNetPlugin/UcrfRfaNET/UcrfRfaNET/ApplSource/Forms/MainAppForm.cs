﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using GridCtrl;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;


namespace XICSM.UcrfRfaNET.ApplSource
{
    public partial class MainAppForm : FBaseForm
    {
        /// <summary>
        /// Отображает ВРВ станции
        /// </summary>
        /// <param name="table">Название таблицы станции</param>
        /// <param name="id">ID станции</param>
        /// <param name="readOnly">режим доступа</param>
        public static void ShowApplForm(string table, int id, bool readOnly)
        {
            BaseAppClass obj = BaseAppClass.GetStation(id, table, 0, false);
            if (readOnly)
                obj.ReadOnly = readOnly; //Не смотрим на права доступа
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }
        /// <summary>
        /// Отображает форму заявки с базовой станцией только на чтение
        /// </summary>
        /// <param name="applId">ID заявки</param>
        public static void ShowApplFormBase(int applId, bool readOnly)
        {
            BaseAppClass obj = BaseAppClass.GetAppl(applId, true);
            obj.ReadOnly = readOnly;
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }

        //===============================================================
        //===============================================================
        //                        Переменные
        //===============================================================
        //===============================================================
        //private bool disposed = false;
        public static bool isShowDialog = false;
        private Grid gridParams;
        protected BaseAppClass baseApplObj = null;   // Обьект, который управляет заявкой
        public static int StatNumApplIdOpened = -1;
        public bool isDublicate = false;
        //===============================================================
        //===============================================================
        //                          Функции
        //===============================================================
        //===============================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_baseApplObj">ссылка на обьект, который будет управляет заявкой</param>
        public MainAppForm(BaseAppClass _baseApplObj)
        {
            if (_baseApplObj == null)
                throw new IMException("The base object can't be NULL.");
            baseApplObj = _baseApplObj;
            baseApplObj.OwnerWindows = this;
            CallBackFunction.callbackEventHandler = new CallBackFunction.callbackEvent(this.UpdateVisnovok);
            InitializeApp();
            isDublicate = false;
            //textBoxDozvil.DataBindings.Add("Text", baseApplObj, "SCDozvil");
        }

        /// <summary>
        /// Обновление висновка
        /// </summary>
        /// <param name="str"></param>
        private void UpdateVisnovok(string str)
        {
            textBoxVisnovok.Text = str;
        }


        private void InitializeApp()
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading the application form...")))
            {
                
                pb.SetBig("Init components...");
                InitializeComponent();
                CLocaliz.TxT(this);
                FormId = baseApplObj.recID;
                SetTitle(string.Format("{0} ({1}) (Packet-{2})", CLocaliz.TxT(baseApplObj.objTableName), baseApplObj.Standard, baseApplObj.PacketID));
                pb.SetBig("Init grids...");
                InitGrids();
                InitApplication(baseApplObj);
                //---------------------------
                pb.SetBig("Init auxiliary data...");
                textBoxDozvil.Text = baseApplObj.SCDozvil;
                textBoxVisnovok.Text = baseApplObj.SCVisnovok;
                textBoxNote.Text = baseApplObj.SCNote;
                //---------------------------
                txtOwnerName.Select();
                baseApplObj.IsChangeDop = false; // last txtbx.Text assignments mark the baseApplObj as changed (TODO : DataBind them with corresponding baseApplObj properties)
                StatNumApplIdOpened = baseApplObj.ApplID;
            }
        }

        void InitGrids()
        {
            this.gridParams = new GridCtrl.Grid();
            gbParamGrid.Controls.Add(this.gridParams);
            this.gridParams.AllowDrop = true;
            this.gridParams.BackColor = System.Drawing.SystemColors.Window;
            this.gridParams.EditMode = true;
            this.gridParams.FirstColumnWidth = 225;
            this.gridParams.GridLine = GridCtrl.Grid.GridLineStyle.Both;
            this.gridParams.GridLineColor = System.Drawing.Color.Black;
            this.gridParams.GridOpacity = 100;
            this.gridParams.Image = null;
            this.gridParams.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;
            this.gridParams.Location = new System.Drawing.Point(2, 3);
            this.gridParams.LockUpdates = false;
            this.gridParams.Name = "gridParams";
            this.gridParams.OtherColumnsWidth = 230;
            this.gridParams.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;
            this.gridParams.Size = new System.Drawing.Size(267, 133);
            this.gridParams.TabIndex = 0;
            this.gridParams.ToolTips = true;
            this.gridParams.Parent = gbParamGrid;
            this.gridParams.OnCellButtonClick += new GridCtrl.Grid.CellButtonClickDelegate(ParamCellButtonClick);
            this.gridParams.OnDataValidate += new GridCtrl.Grid.DataValidateDelegate(ParamCellValidate);
            this.gridParams.MouseDoubleClick += new MouseEventHandler(MouseDblClick);
            this.gridParams.ReadOnly = baseApplObj.ReadOnly;
            ResizeGrid(this.gridParams);
        }
        //============================================================
        /// <summary>
        /// Изменения размера Грида в соотвествии с его родителем
        /// </summary>
        /// <param name="grid">Грид</param>
        private void ResizeGrid(Grid grid)
        {
            if (grid != null && grid.Parent != null)
            {
                grid.Top = 20;
                grid.Left = 7;
                grid.Width = grid.Parent.Width - grid.Left * 2 + 1;
                grid.Height = grid.Parent.Height - (grid.Top + grid.Left);
                grid.Invalidate();
            }
        }
        /// <summary>
        /// Форма отобразилась
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainAppForm_Shown(object sender, EventArgs e)
        {
            isShowDialog = true;
            gbParamGrid.Enabled = true;  //Даем доступ к гриду
        }

        #region EVENTS
        //============================================================
        /// <summary>
        /// Событие для изменения грида параметров
        /// </summary>
        private void gbParametersGrid_Resize(object sender, EventArgs e)
        {
            txtOwnerName.Focus();
            ResizeGrid(this.gridParams);
        }
        //===========================================================
        /// <summary>
        /// Нажата кнопка в ячейки грида с параметрами
        /// </summary>
        /// <param name="cell">Ячейка</param>
        private void ParamCellButtonClick(Cell cell)
        {
            baseApplObj.OnPressButton(cell, gridParams);
        }
        //===========================================================
        /// <summary>
        /// Проверка параметров ячейки
        /// </summary>
        /// <param name="cell">Ячейка</param>
        private void ParamCellValidate(Cell cell)
        {
            try
            {
                baseApplObj.OnCellValidate(cell, gridParams);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        //===========================================================
        /// <summary>
        /// Сохранение изменений
        /// </summary>
        public void SaveToBase()
        {
            txtOwnerName.Focus();
            baseApplObj.OnSaveAppl();
            if (isDublicate)
            {
                List<LinkDocFiles> ListDocAttachPachet = CPacket.GetAllLinkDocsFromIDPacket(baseApplObj.PacketID);
                PrintDocs prntDoc = new PrintDocs();
                foreach (LinkDocFiles item_ in ListDocAttachPachet)
                {
                    prntDoc.LoadDocLink(item_.DOC_TYPE, item_.DOC_REF, item_.PATH, item_.DATE_CREATED, item_.EXP_DATE, new List<int> { baseApplObj.ApplID }, false);
                }
                isDublicate = false;
                InitApplication(baseApplObj);
            }
            if (baseApplObj.OnAfterSave())
            {
                baseApplObj = BaseAppClass.GetStation(baseApplObj.recID, baseApplObj.objTableName, baseApplObj.ApplID, false);
                InitApplication(baseApplObj);
            }
        }
        //===========================================================
        /// <summary>
        /// Обработка меню Save
        /// </summary>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtOwnerName.Focus();
            baseApplObj.OnSaveAppl();
            if (isDublicate)
            {
                List<LinkDocFiles> ListDocAttachPachet = CPacket.GetAllLinkDocsFromIDPacket(baseApplObj.PacketID);
                PrintDocs prntDoc = new PrintDocs();
                foreach (LinkDocFiles item_ in ListDocAttachPachet)
                {
                    prntDoc.LoadDocLink(item_.DOC_TYPE, item_.DOC_REF, item_.PATH, item_.DATE_CREATED, item_.EXP_DATE, new List<int> { baseApplObj.ApplID }, false);
                }
                isDublicate = false;
                InitApplication(baseApplObj);
            }
            if (baseApplObj.OnAfterSave())
            {
                baseApplObj = BaseAppClass.GetStation(baseApplObj.recID, baseApplObj.objTableName, baseApplObj.ApplID, false);
                InitApplication(baseApplObj);
                SetTitle(string.Format("{0} ({1}) (Packet-{2})", CLocaliz.TxT(baseApplObj.objTableName), baseApplObj.Standard, baseApplObj.PacketID));
            }
        }

        #endregion

        private void AddSectorMenuItem_Click(object sender, EventArgs e)
        {
            baseApplObj.AddSector(this.gridParams);
        }

        private void RemoveSectorMenuItem_Click(object sender, EventArgs e)
        {
            baseApplObj.RemoveSector(this.gridParams);
        }
        //=================================================
        /// <summary>
        /// Печать документа
        /// </summary>
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            baseApplObj.PrintDoc();
        }
        /// <summary>
        /// Печать на бланке
        /// </summary>
        private void printOnBlankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            baseApplObj.PrintDocOnBlank();
        }        
        //=================================================
        /// <summary>
        /// Предварительные расчеты EMS для УРЧП
        /// </summary>
        private void URCPEmsCalc_Click(object sender, EventArgs e)
        {
            baseApplObj.CalculateEMS();
        }
        //=================================================
        /// <summary>
        /// Отобразить лицензию
        /// </summary>
        private void dataGridLicence_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            baseApplObj.ShowLicence(e.RowIndex, e.ColumnIndex);
        }
        //=================================================
        /// <summary>
        /// Нажали меню "Выход"
        /// </summary>
        private void MenuEXIT_Click(object sender, EventArgs e)
        {
            Close();
        }
        //=================================================
        /// <summary>
        /// Обработка DoubleClick на гриде параметров
        /// </summary>
        private void MouseDblClick(object sender, MouseEventArgs e)
        {
            this.Enabled = false;
            baseApplObj.GridDoubleClick(this.gridParams);
            this.Enabled = true;
        }
        //=================================================
        /// <summary>
        /// Отобразить данные об операторе
        /// </summary>
        private void txtOwnerName_DoubleClick(object sender, EventArgs e)
        {
            this.Enabled = false;
            baseApplObj.ShowOperator();
            this.Enabled = true;
        }
        //=================================================
        /// <summary>
        /// Изменился текст дозвола
        /// </summary>
        private void textBoxDozvil_TextChanged(object sender, EventArgs e)
        {
            baseApplObj.SCDozvil = textBoxDozvil.Text;
            baseApplObj.IsChangeDop = true;
        }
        //=================================================
        /// <summary>
        /// Изменился текст висновка
        /// </summary>
        private void textBoxVisnovok_TextChanged(object sender, EventArgs e)
        {
            baseApplObj.SCVisnovok = textBoxVisnovok.Text;
            baseApplObj.IsChangeDop = true;
        }
        //=================================================
        /// <summary>
        /// Изменился текст Примечания
        /// </summary>
        private void textBoxNote_TextChanged(object sender, EventArgs e)
        {
            baseApplObj.SCNote = textBoxNote.Text;
            baseApplObj.IsChangeDop = true;
        }
        //=================================================
        /// <summary>
        /// Вставить Вывод ЕМС
        /// </summary>
        private void UCRFEMSConclusion_Click(object sender, EventArgs e)
        {
            FSelectSCPattern frm = new FSelectSCPattern(baseApplObj.department, baseApplObj.departmentSector, DocType.VISN);
            if (frm.ShowDialog() == DialogResult.OK)
                textBoxVisnovok.Text = frm.GetPatternText();
            frm.Dispose();
        }
        //=================================================
        /// <summary>
        /// Вставить Разрешение ЕМС
        /// </summary>
        private void eMSPermissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FSelectSCPattern frm = new FSelectSCPattern(baseApplObj.department, baseApplObj.departmentSector, DocType.DOZV);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //string oldText = Environment.NewLine + textBoxDozvil.Text;
                //if (oldText.Trim().Length > 0)
                    //if (MessageBox.Show("В особливих умовах вже щось є. Зберегти старий текст, додаючі новий?", CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    //oldText = "";
                    //textBoxDozvil.Text = frm.GetPatternText() + Environment.NewLine + oldText;
                    textBoxDozvil.Text = frm.GetPatternText();
            }
            frm.Dispose();
        }
        //=================================================
        /// <summary>
        /// Просмотреть документ события
        /// </summary>
        private void dataGridEventLog_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            baseApplObj.ShowEventsDoc(e.RowIndex, e.ColumnIndex);
        }
        //=================================================
        /// <summary>
        /// Дубдировать заявку
        /// </summary>
        private void URCPDuplicateStation_Click(object sender, EventArgs e)
        {
            BaseAppClass DuplicateObj = baseApplObj.CreateStation();
            if (DuplicateObj == null)
                return;
            InitApplication(DuplicateObj);
            DuplicateObj = baseApplObj.DuplicateStation(this.gridParams, DuplicateObj);
            baseApplObj = DuplicateObj;
            isDublicate = true;
            textBoxDozvil.Text = "";
            textBoxVisnovok.Text = "";
            textBoxNote.Text = "";
        }
        //=================================================
        /// <summary>
        /// Инициализация заявки
        /// </summary>
        private void InitApplication(BaseAppClass StationObj)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading the application form...")))
            {
                pb.SetBig(CLocaliz.TxT("Updating components..."));
                const int maxElem = 20;
                tbStatus.DataBindings.Clear();                
                //tbStatus.DataBindings.Add("Text", StationObj.Status, "Status.StatusFromEri");
                tbStatus.DataBindings.Add("Text", StationObj, "Status");
                //-----
                lblTechUser.DataBindings.Clear();
                lblTechUser.DataBindings.Add("Visible", StationObj, "IsTechUser");
                //-----
                dataGridLicence.DataBindings.Clear();
                dataGridLicence.DataBindings.Add("Visible", StationObj, "IsNotTechUser");
                //-----
                textBoxComments.DataBindings.Clear();
                textBoxComments.DataBindings.Add("Text", StationObj, "Comment");
                textBoxComments.Text = StationObj.Comment;

                int index = 0;
                // Инициализация гридов
                pb.SetProgress(index++, maxElem);
                StationObj.InitMenu(this.MainMenu.Items);
                pb.SetProgress(index++, maxElem);
                StationObj.LoadStructureParamGrid(this.gridParams);
                pb.SetProgress(index++, maxElem);
                StationObj.InitParamGrid(this.gridParams);
                pb.SetProgress(index++, maxElem);
                StationObj.UpdateGridParameters(this.gridParams);
                // Выравниваем первую колонку
                pb.SetProgress(index++, maxElem);

                Classes.BaseCommitClass comm = StationObj as Classes.BaseCommitClass;
                Classes.BaseDiffClass diff = StationObj as Classes.BaseDiffClass;
                //Загрузить коментарий
                StationObj.OpenComments(diff, comm);
               
                if ((comm == null) && (diff == null))
                    tabControl1.SelectTab(tabControl1.TabPages[1].Name);
                else
                    tabControl1.SelectTab(tabControl1.TabPages[3].Name);
               
                int curWidth = 0;
                for (int r = 0; r < this.gridParams.GetRowCount(); r++)
                {
                    int width = this.gridParams.widthCellText(this.gridParams.GetCell(r, 0));
                    if (curWidth < width)
                        curWidth = width;
                }
                if (StationObj is AmateurAppl)
                {
                    AddSectorUps.Enabled = true;
                    AddSectorUps.Visible = AddSectorUps.Enabled;
                    AddSectorSps.Enabled = true;
                    AddSectorSps.Visible = AddSectorSps.Enabled;
                    printOnBlank.Enabled = true;
                    printOnBlank.Visible = printOnBlank.Enabled;
                }

                if (StationObj is RailwayAppl)
                {
                    URCPEmsCalc.Visible = false;
                    URCPEmsCalc.Enabled = false;
                }
                const int maxWidth = 300;
                if (curWidth > maxWidth)
                    curWidth = maxWidth;
                this.gridParams.FirstColumnWidth = curWidth + 20;
                pb.SetProgress(index++, maxElem);
                StationObj.InitEventGrid(dataGridEventLog);
                pb.SetProgress(index++, maxElem);
                StationObj.InitLicenceGrid(dataGridLicence);
                pb.SetProgress(index++, maxElem);
                StationObj.InitChangeGrid(dgLog);
                pb.SetProgress(index++, maxElem);
                StationObj.UpdateForm(this);
                pb.SetProgress(index++, maxElem);
                StationObj.UpdateOwnerRDPO(txtOwnerRDPO);
                pb.SetProgress(index++, maxElem);
                StationObj.UpdateOwnerName(txtOwnerName);
                pb.SetProgress(index, maxElem);
                UpdateNets(StationObj);
                UpdateArticle(StationObj);
                this.gridParams.Edited = false;
            }
        }
        /// <summary>
        /// Обновляет список сетей
        /// </summary>
        /// <param name="stationObj"></param>
        private void UpdateNets(BaseAppClass stationObj)
        {
            string str = "";
            int index = 0;
            string[] netName = stationObj.GetNetNames();
            foreach (string name in netName)
            {
                if (string.IsNullOrEmpty(str) == false)
                    str += "; ";
                if (++index > 4)
                {
                    str += ">>>";
                    break;
                }
                str += (string.IsNullOrEmpty(name)) ? "<без назви>" : name;
            }
            if (string.IsNullOrEmpty(str) == false)
                str = "Мережа:" + str;
            tsNet.Text = str;
        }

        /// <summary>
        /// Обновляет статью и кол-во работ
        /// </summary>
        /// <param name="stationObj"></param>
        private void UpdateArticle(BaseAppClass stationObj)
        {
            string article1 = "";
            int workCount1 = IM.NullI;
            string article2 = "";
            int workCount2 = IM.NullI;
            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.ApplID);
                rs.Open();
                if(!rs.IsEOF())
                {
                    article1 = rs.GetS("Price.ARTICLE");
                    article2 = rs.GetS("Price2.ARTICLE");
                    workCount1 = rs.GetI("WORKS_COUNT");
                    workCount2 = rs.GetI("WORKS_COUNT2");
                }
            }
            if (string.IsNullOrEmpty(article2) == false)
                article1 += " / " + article2;
            string article = (string.IsNullOrEmpty(article1) == false) ? article1 : "<пусто>";
            string workCount = (workCount1 != IM.NullI) ? workCount1.ToString() : "<пусто>";
            if (workCount2 != IM.NullI)
                workCount += " / " + workCount2;
            tsArticle.Text = "Стаття: " + article;
            tsWorkCount.Text = "Кіл-ть робіт: " + workCount;
        }

        private void MainAppForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            isShowDialog = false;
            if (baseApplObj.ReadOnly == false)
            {
                if (gridParams.Edited || baseApplObj.IsChangeDop)
                {
                    // Заставить явно вызваться все CellValidate-ы, некоторые из них явно пишут в IMObject
                    // причем тогда, когда форма закрыватеся и фокус сооветвенно теряется.
                    //  это будет самый быстрый путь.
                    gridParams.CellSelect(gridParams.GetCell(0, 0));

                    DialogResult dResult = MessageBox.Show("Ви бажаєте зберегти зміни?", "Підтвердження", MessageBoxButtons.YesNoCancel);
                    switch (dResult)
                    {
                        case DialogResult.Yes:
                            e.Cancel = true; // this should prevent form from closing when an error occurs
                            if (baseApplObj.OnSaveAppl())
                                e.Cancel = false;
                            break;
                        case DialogResult.No:
                            e.Cancel = false;
                            break;
                        case DialogResult.Cancel:
                            e.Cancel = true;
                            break;
                    }
                }
            }
            if (e.Cancel == false)
                StatNumApplIdOpened = -1;
                baseApplObj.Dispose();
        }

        private void CheckoutMenuItem_Click(object sender, EventArgs e)
        {
            txtOwnerName.Focus();
            baseApplObj.OnSaveAppl();
        }
        //===================================================
        /// <summary>
        /// Меню "Приєднати документ"
        /// </summary>
        private void URCP_AttachDoc_Click(object sender, EventArgs e)
        {
            List<int> listID = new List<int>();
            if ((baseApplObj.ApplID != 0) || (baseApplObj.ApplID != IM.NullI))
                listID.Add(baseApplObj.ApplID);

            if (listID.Count == 0)
                return;
            //--------------
            using (FAttachDoc frm = new FAttachDoc(baseApplObj.PacketID, listID.ToArray()))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    PrintDocs prntDoc = new PrintDocs();
                    prntDoc.LoadDocLink(frm.DocTypeCode, frm.docRef, frm.fileName, frm.docDate, frm.expDate, listID,true);
                    baseApplObj.UpdateEventGrid();
                    MessageBox.Show(CLocaliz.TxT(string.Format("The document \"{0}\" is attached.", frm.fileName)), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void miArticle_Click(object sender, EventArgs e)
        {
            baseApplObj.SelectArticle();
            UpdateArticle(baseApplObj);
        }
        /// <summary>
        /// Изменить статус заявки с "Технологического пользователя" на "Не технологического пользователя"
        /// </summary>
        private void lblTechUser_DoubleClick(object sender, EventArgs e)
        {
            if ((baseApplObj != null) &&
               (baseApplObj.IsTechUser == true) &&
               (MessageBox.Show(CLocaliz.TxT("Do you want to change state of application from \"Technical user\" to \"Not technical user\""), CLocaliz.TxT("Changing the state"), MessageBoxButtons.YesNo) == DialogResult.Yes)
               )
            {
                baseApplObj.IsTechnicalUser = 0;
            }
        }

        private void MainMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        /// <summary>
        /// генерация отчета Акту ПТК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiffReportGenerationMenuItem_Click(object sender, EventArgs e)
        {
            txtOwnerName.Focus();  //Убираем фокус из грида, чтоб сработало событие CellValidating
            // Если заявка не сохранилась, уже будет что-то выведено...
            if (baseApplObj.OnSaveAppl())
            {
                DiffReportForm frm = new DiffReportForm((BaseDiffClass) baseApplObj);
                frm.ShowDialog();
                baseApplObj.UpdateEventGrid();
            }
        }
        /// <summary>
        /// Генерація протоколу інструментальної оцінки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuUrzpPtk2_Click(object sender, EventArgs e)
        {
            if (baseApplObj.OnSaveAppl())
            {
                DiffReport2Form frm = new DiffReport2Form((BaseDiffClass)baseApplObj);
                frm.ShowDialog();
                baseApplObj.UpdateEventGrid();
            }
        }

        private void menuUrzpPtk3_Click(object sender, EventArgs e)
        {
            if (baseApplObj.OnSaveAppl())
            {
                DiffReport3Form frm = new DiffReport3Form((BaseDiffClass)baseApplObj);
                frm.ShowDialog();
                baseApplObj.UpdateEventGrid();
            }
        }
        /// <summary>
        /// Изменить филию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeFilialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IMTableRight rights = IM.TableRight(PlugTbl.APPL);
            if ((rights & IMTableRight.Update) != IMTableRight.Update)
            {
                MessageBox.Show(CLocaliz.TxT("You don't have rights."));
                return;
            }

            int applId = baseApplObj.ApplID;
            if ((applId == 0) || (applId == IM.NullI))
            {
                MessageBox.Show("Can't identify Appl ID");
                return;
            }
            using (ChangeMonitorProvince frm = new ChangeMonitorProvince(applId))
            {
                frm.Owner = this;
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    CEventLog.AddApplEvent(applId, EDocEvent.evChangeProvinceMoni, frm.AppObject.ChangeDocDate, frm.AppObject.ChangeDocNum, IM.NullT, frm.AppObject.ChangeFilePos);
                    baseApplObj.UpdateEventGrid();
                }
            }
        }

        private void AddSectorSps_Click(object sender, EventArgs e)
        {
            baseApplObj.AddSectorSps(this.gridParams);
        }

        private void AddSectorUps_Click(object sender, EventArgs e)
        {
            baseApplObj.AddSectorUps(this.gridParams);
        }
        /// <summary>
        /// Координація
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripCoord_Click(object sender, EventArgs e)
        {
            baseApplObj.Coordination();
            baseApplObj.UpdateForeingMkStatus();
        }

        private void UrcpFindFrequencies_Click(object sender, EventArgs e)
        {
            RecordPtr rec = baseApplObj.GetRecordOfSector();
            if ((rec.Id != IM.NullI) && (rec.Id != 0) && (!string.IsNullOrEmpty(rec.Table)))
            {
                using (FrequencyFittingForm frequencyFittingForm = new FrequencyFittingForm(rec.Table, rec.Id))
                {
                    frequencyFittingForm.ApplId = baseApplObj.ApplID;
                    frequencyFittingForm.ShowDialog(this);
                    baseApplObj.UpdateEventGrid();
                }
            }
        }

        private void MenuAdd2Net_Click(object sender, EventArgs e)
        {
            baseApplObj.Add2Net();
            UpdateNets(baseApplObj);
        }
    }
}
