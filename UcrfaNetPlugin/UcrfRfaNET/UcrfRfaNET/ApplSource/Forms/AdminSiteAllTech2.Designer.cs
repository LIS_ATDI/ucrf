﻿using System.ComponentModel;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class AdminSiteAllTech2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxLatLow = new System.Windows.Forms.TextBox();
            this.txtBoxLongLow = new System.Windows.Forms.TextBox();
            this.lblLatitude = new System.Windows.Forms.Label();
            this.lblLongitude = new System.Windows.Forms.Label();
            this.grpBoxKoord = new System.Windows.Forms.GroupBox();
            this.btCoordLikeAdmSiteHas = new System.Windows.Forms.Button();
            this.lblLong = new System.Windows.Forms.Label();
            this.lblLat = new System.Windows.Forms.Label();
            this.btnSaveExit = new System.Windows.Forms.Button();
            this.btnCancelExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblDataCoatuu = new System.Windows.Forms.Label();
            this.txtBoxKoatuu = new System.Windows.Forms.TextBox();
            this.lblAddres = new System.Windows.Forms.Label();
            this.lblAsl = new System.Windows.Forms.Label();
            this.btnMapToAsl = new System.Windows.Forms.Button();
            this.lblLimit = new System.Windows.Forms.Label();
            this.txtBoxLimit = new System.Windows.Forms.TextBox();
            this.grpBoxAddress = new System.Windows.Forms.GroupBox();
            this.btAddrLikeAdmSiteHas = new System.Windows.Forms.Button();
            this.pnlShortAddress = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFullAddress = new System.Windows.Forms.TextBox();
            this.pnlAddress = new System.Windows.Forms.Panel();
            this.txtBoxAsl = new System.Windows.Forms.TextBox();
            this.grpBoxMap = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_set_layer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_number_building = new System.Windows.Forms.TextBox();
            this.txt_owner = new System.Windows.Forms.TextBox();
            this.TypeObject = new System.Windows.Forms.ComboBox();
            this.Cust_nbr2 = new System.Windows.Forms.TextBox();
            this.Cust_nbr1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpBoxPhoto = new System.Windows.Forms.GroupBox();
            this.pctBox = new System.Windows.Forms.PictureBox();
            this.btndel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbBoxPhoto = new System.Windows.Forms.ComboBox();
            this.txtBoxAdmSite = new System.Windows.Forms.TextBox();
            this.btnChangeAdmSite = new System.Windows.Forms.Button();
            this.btnOffAdmSite = new System.Windows.Forms.Button();
            this.grpBoxAdmSite = new System.Windows.Forms.GroupBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.lblCheckName = new System.Windows.Forms.Label();
            this.lblCheckDate = new System.Windows.Forms.Label();
            this.lblAcceptDate = new System.Windows.Forms.Label();
            this.lblAcceptName = new System.Windows.Forms.Label();
            this.grpBoxCheck = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox_special_check = new System.Windows.Forms.CheckBox();
            this.picAdmStatus = new System.Windows.Forms.PictureBox();
            this.btnError = new System.Windows.Forms.Button();
            this.lblCheckDateVal = new System.Windows.Forms.Label();
            this.lblCheckNamVal = new System.Windows.Forms.Label();
            this.lblAcceptNameVal = new System.Windows.Forms.Label();
            this.lblAcceptDateVal = new System.Windows.Forms.Label();
            this.gbNote = new System.Windows.Forms.GroupBox();
            this.tbOldAddress = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.gbxLog = new System.Windows.Forms.GroupBox();
            this.txtModifiedDate = new System.Windows.Forms.Label();
            this.difieddate = new System.Windows.Forms.Label();
            this.txtModifiedBy = new System.Windows.Forms.Label();
            this.o = new System.Windows.Forms.Label();
            this.txtCreatedDate = new System.Windows.Forms.Label();
            this.lbCreatedDate = new System.Windows.Forms.Label();
            this.txtCreatedBy = new System.Windows.Forms.Label();
            this.lbCreatedBy = new System.Windows.Forms.Label();
            this.btmIcsmSite = new System.Windows.Forms.Button();
            this.grpBoxKoord.SuspendLayout();
            this.grpBoxAddress.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBoxPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).BeginInit();
            this.grpBoxAdmSite.SuspendLayout();
            this.grpBoxCheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAdmStatus)).BeginInit();
            this.gbNote.SuspendLayout();
            this.gbxLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBoxLatLow
            // 
            this.txtBoxLatLow.Location = new System.Drawing.Point(65, 32);
            this.txtBoxLatLow.Name = "txtBoxLatLow";
            this.txtBoxLatLow.Size = new System.Drawing.Size(180, 20);
            this.txtBoxLatLow.TabIndex = 2;
            this.txtBoxLatLow.TextChanged += new System.EventHandler(this.txtBoxLatLow_TextChanged);
            this.txtBoxLatLow.Validated += new System.EventHandler(this.txtBoxLatLow_Validated);
            // 
            // txtBoxLongLow
            // 
            this.txtBoxLongLow.Location = new System.Drawing.Point(370, 30);
            this.txtBoxLongLow.Name = "txtBoxLongLow";
            this.txtBoxLongLow.Size = new System.Drawing.Size(150, 20);
            this.txtBoxLongLow.TabIndex = 5;
            this.txtBoxLongLow.TextChanged += new System.EventHandler(this.txtBoxLongLow_TextChanged);
            this.txtBoxLongLow.Validated += new System.EventHandler(this.txtBoxLongLow_Validated);
            // 
            // lblLatitude
            // 
            this.lblLatitude.AutoSize = true;
            this.lblLatitude.Location = new System.Drawing.Point(8, 14);
            this.lblLatitude.Name = "lblLatitude";
            this.lblLatitude.Size = new System.Drawing.Size(48, 13);
            this.lblLatitude.TabIndex = 0;
            this.lblLatitude.Text = "Широта:";
            // 
            // lblLongitude
            // 
            this.lblLongitude.AutoSize = true;
            this.lblLongitude.Location = new System.Drawing.Point(301, 13);
            this.lblLongitude.Name = "lblLongitude";
            this.lblLongitude.Size = new System.Drawing.Size(53, 13);
            this.lblLongitude.TabIndex = 3;
            this.lblLongitude.Text = "Довгота:";
            // 
            // grpBoxKoord
            // 
            this.grpBoxKoord.Controls.Add(this.btCoordLikeAdmSiteHas);
            this.grpBoxKoord.Controls.Add(this.lblLong);
            this.grpBoxKoord.Controls.Add(this.lblLat);
            this.grpBoxKoord.Controls.Add(this.lblLongitude);
            this.grpBoxKoord.Controls.Add(this.lblLatitude);
            this.grpBoxKoord.Controls.Add(this.txtBoxLongLow);
            this.grpBoxKoord.Controls.Add(this.txtBoxLatLow);
            this.grpBoxKoord.Location = new System.Drawing.Point(8, 3);
            this.grpBoxKoord.Name = "grpBoxKoord";
            this.grpBoxKoord.Size = new System.Drawing.Size(526, 54);
            this.grpBoxKoord.TabIndex = 0;
            this.grpBoxKoord.TabStop = false;
            this.grpBoxKoord.Text = " Географічні координати ";
            // 
            // btCoordLikeAdmSiteHas
            // 
            this.btCoordLikeAdmSiteHas.Location = new System.Drawing.Point(266, 28);
            this.btCoordLikeAdmSiteHas.Name = "btCoordLikeAdmSiteHas";
            this.btCoordLikeAdmSiteHas.Size = new System.Drawing.Size(101, 23);
            this.btCoordLikeAdmSiteHas.TabIndex = 13;
            this.btCoordLikeAdmSiteHas.Text = "Як адм. сайт";
            this.btCoordLikeAdmSiteHas.UseVisualStyleBackColor = true;
            this.btCoordLikeAdmSiteHas.Click += new System.EventHandler(this.btCoordLikeAdmSiteHas_Click);
            // 
            // lblLong
            // 
            this.lblLong.BackColor = System.Drawing.SystemColors.Control;
            this.lblLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLong.Location = new System.Drawing.Point(370, 12);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(150, 15);
            this.lblLong.TabIndex = 4;
            // 
            // lblLat
            // 
            this.lblLat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLat.Location = new System.Drawing.Point(65, 14);
            this.lblLat.Name = "lblLat";
            this.lblLat.Size = new System.Drawing.Size(180, 15);
            this.lblLat.TabIndex = 1;
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSaveExit.Location = new System.Drawing.Point(142, 718);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Size = new System.Drawing.Size(137, 23);
            this.btnSaveExit.TabIndex = 22;
            this.btnSaveExit.Text = "Зберегти та вийти (F8)";
            this.btnSaveExit.UseVisualStyleBackColor = true;
            this.btnSaveExit.Click += new System.EventHandler(this.btnSaveExit_Click);
            // 
            // btnCancelExit
            // 
            this.btnCancelExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelExit.Location = new System.Drawing.Point(289, 718);
            this.btnCancelExit.Name = "btnCancelExit";
            this.btnCancelExit.Size = new System.Drawing.Size(111, 23);
            this.btnCancelExit.TabIndex = 24;
            this.btnCancelExit.Text = "Відмінити та вийти";
            this.btnCancelExit.UseVisualStyleBackColor = true;
            this.btnCancelExit.Click += new System.EventHandler(this.btnCancelExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(10, 718);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Зберегти зміни (F5)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblDataCoatuu
            // 
            this.lblDataCoatuu.AutoSize = true;
            this.lblDataCoatuu.Location = new System.Drawing.Point(8, 181);
            this.lblDataCoatuu.Name = "lblDataCoatuu";
            this.lblDataCoatuu.Size = new System.Drawing.Size(81, 13);
            this.lblDataCoatuu.TabIndex = 3;
            this.lblDataCoatuu.Text = "Дані КОАТУУ:";
            // 
            // txtBoxKoatuu
            // 
            this.txtBoxKoatuu.Location = new System.Drawing.Point(96, 178);
            this.txtBoxKoatuu.Multiline = true;
            this.txtBoxKoatuu.Name = "txtBoxKoatuu";
            this.txtBoxKoatuu.ReadOnly = true;
            this.txtBoxKoatuu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxKoatuu.Size = new System.Drawing.Size(424, 45);
            this.txtBoxKoatuu.TabIndex = 4;
            this.txtBoxKoatuu.TabStop = false;
            // 
            // lblAddres
            // 
            this.lblAddres.AutoEllipsis = true;
            this.lblAddres.AutoSize = true;
            this.lblAddres.Location = new System.Drawing.Point(10, 210);
            this.lblAddres.Name = "lblAddres";
            this.lblAddres.Size = new System.Drawing.Size(41, 13);
            this.lblAddres.TabIndex = 5;
            this.lblAddres.Text = "Адрес:";
            // 
            // lblAsl
            // 
            this.lblAsl.AutoSize = true;
            this.lblAsl.Location = new System.Drawing.Point(10, 320);
            this.lblAsl.Name = "lblAsl";
            this.lblAsl.Size = new System.Drawing.Size(96, 13);
            this.lblAsl.TabIndex = 7;
            this.lblAsl.Text = "Над рівнем моря:";
            // 
            // btnMapToAsl
            // 
            this.btnMapToAsl.BackgroundImage = global::XICSM.UcrfRfaNET.Properties.Resources.ok;
            this.btnMapToAsl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnMapToAsl.Location = new System.Drawing.Point(175, 311);
            this.btnMapToAsl.Name = "btnMapToAsl";
            this.btnMapToAsl.Size = new System.Drawing.Size(23, 23);
            this.btnMapToAsl.TabIndex = 10;
            this.btnMapToAsl.UseVisualStyleBackColor = true;
            this.btnMapToAsl.Click += new System.EventHandler(this.btnMapToAsl_Click);
            // 
            // lblLimit
            // 
            this.lblLimit.AutoSize = true;
            this.lblLimit.Location = new System.Drawing.Point(212, 320);
            this.lblLimit.Name = "lblLimit";
            this.lblLimit.Size = new System.Drawing.Size(84, 13);
            this.lblLimit.TabIndex = 8;
            this.lblLimit.Text = "Відст. до корд.:";
            // 
            // txtBoxLimit
            // 
            this.txtBoxLimit.Location = new System.Drawing.Point(295, 313);
            this.txtBoxLimit.Name = "txtBoxLimit";
            this.txtBoxLimit.ReadOnly = true;
            this.txtBoxLimit.Size = new System.Drawing.Size(225, 20);
            this.txtBoxLimit.TabIndex = 11;
            // 
            // grpBoxAddress
            // 
            this.grpBoxAddress.Controls.Add(this.btAddrLikeAdmSiteHas);
            this.grpBoxAddress.Controls.Add(this.pnlShortAddress);
            this.grpBoxAddress.Controls.Add(this.label1);
            this.grpBoxAddress.Controls.Add(this.tbFullAddress);
            this.grpBoxAddress.Controls.Add(this.pnlAddress);
            this.grpBoxAddress.Controls.Add(this.txtBoxAsl);
            this.grpBoxAddress.Controls.Add(this.txtBoxLimit);
            this.grpBoxAddress.Controls.Add(this.lblLimit);
            this.grpBoxAddress.Controls.Add(this.btnMapToAsl);
            this.grpBoxAddress.Controls.Add(this.lblAsl);
            this.grpBoxAddress.Controls.Add(this.lblAddres);
            this.grpBoxAddress.Controls.Add(this.txtBoxKoatuu);
            this.grpBoxAddress.Controls.Add(this.lblDataCoatuu);
            this.grpBoxAddress.Location = new System.Drawing.Point(8, 59);
            this.grpBoxAddress.Name = "grpBoxAddress";
            this.grpBoxAddress.Size = new System.Drawing.Size(526, 336);
            this.grpBoxAddress.TabIndex = 2;
            this.grpBoxAddress.TabStop = false;
            this.grpBoxAddress.Text = " Адреса встановлення ";
            // 
            // btAddrLikeAdmSiteHas
            // 
            this.btAddrLikeAdmSiteHas.Location = new System.Drawing.Point(9, 32);
            this.btAddrLikeAdmSiteHas.Name = "btAddrLikeAdmSiteHas";
            this.btAddrLikeAdmSiteHas.Size = new System.Drawing.Size(81, 23);
            this.btAddrLikeAdmSiteHas.TabIndex = 12;
            this.btAddrLikeAdmSiteHas.Text = "Як адм. сайт ->";
            this.btAddrLikeAdmSiteHas.UseVisualStyleBackColor = true;
            this.btAddrLikeAdmSiteHas.Click += new System.EventHandler(this.btAddrLikeAdmSiteHas_Click);
            // 
            // pnlShortAddress
            // 
            this.pnlShortAddress.Location = new System.Drawing.Point(10, 226);
            this.pnlShortAddress.Name = "pnlShortAddress";
            this.pnlShortAddress.Size = new System.Drawing.Size(510, 81);
            this.pnlShortAddress.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Повна адреса:";
            // 
            // tbFullAddress
            // 
            this.tbFullAddress.BackColor = System.Drawing.SystemColors.Control;
            this.tbFullAddress.Location = new System.Drawing.Point(96, 16);
            this.tbFullAddress.Multiline = true;
            this.tbFullAddress.Name = "tbFullAddress";
            this.tbFullAddress.ReadOnly = true;
            this.tbFullAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFullAddress.Size = new System.Drawing.Size(424, 46);
            this.tbFullAddress.TabIndex = 1;
            this.tbFullAddress.TabStop = false;
            this.tbFullAddress.Text = "Поки не доступна.\r\nФункціонал розробляється.";
            // 
            // pnlAddress
            // 
            this.pnlAddress.Location = new System.Drawing.Point(11, 65);
            this.pnlAddress.Name = "pnlAddress";
            this.pnlAddress.Size = new System.Drawing.Size(509, 110);
            this.pnlAddress.TabIndex = 2;
            // 
            // txtBoxAsl
            // 
            this.txtBoxAsl.Location = new System.Drawing.Point(109, 313);
            this.txtBoxAsl.Name = "txtBoxAsl";
            this.txtBoxAsl.Size = new System.Drawing.Size(60, 20);
            this.txtBoxAsl.TabIndex = 9;
            // 
            // grpBoxMap
            // 
            this.grpBoxMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxMap.Location = new System.Drawing.Point(542, 4);
            this.grpBoxMap.Name = "grpBoxMap";
            this.grpBoxMap.Size = new System.Drawing.Size(513, 380);
            this.grpBoxMap.TabIndex = 12;
            this.grpBoxMap.TabStop = false;
            this.grpBoxMap.Text = " Мапа ";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.button_set_layer);
            this.groupBox2.Location = new System.Drawing.Point(542, 379);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 46);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            // 
            // button_set_layer
            // 
            this.button_set_layer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_set_layer.Location = new System.Drawing.Point(326, 14);
            this.button_set_layer.Name = "button_set_layer";
            this.button_set_layer.Size = new System.Drawing.Size(178, 26);
            this.button_set_layer.TabIndex = 30;
            this.button_set_layer.Text = "Зберегти налаштування ";
            this.button_set_layer.UseVisualStyleBackColor = true;
            this.button_set_layer.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txt_number_building);
            this.groupBox1.Controls.Add(this.txt_owner);
            this.groupBox1.Controls.Add(this.TypeObject);
            this.groupBox1.Controls.Add(this.Cust_nbr2);
            this.groupBox1.Controls.Add(this.Cust_nbr1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(8, 396);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(526, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Характеристика споруди";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(335, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Номер споруди";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Власник";
            // 
            // txt_number_building
            // 
            this.txt_number_building.Location = new System.Drawing.Point(425, 9);
            this.txt_number_building.Name = "txt_number_building";
            this.txt_number_building.Size = new System.Drawing.Size(95, 20);
            this.txt_number_building.TabIndex = 7;
            // 
            // txt_owner
            // 
            this.txt_owner.Location = new System.Drawing.Point(190, 9);
            this.txt_owner.Name = "txt_owner";
            this.txt_owner.Size = new System.Drawing.Size(137, 20);
            this.txt_owner.TabIndex = 6;
            // 
            // TypeObject
            // 
            this.TypeObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypeObject.FormattingEnabled = true;
            this.TypeObject.Location = new System.Drawing.Point(9, 32);
            this.TypeObject.Name = "TypeObject";
            this.TypeObject.Size = new System.Drawing.Size(118, 21);
            this.TypeObject.TabIndex = 5;
            this.TypeObject.SelectedIndexChanged += new System.EventHandler(this.TypeObject_SelectedIndexChanged);
            // 
            // Cust_nbr2
            // 
            this.Cust_nbr2.Location = new System.Drawing.Point(455, 32);
            this.Cust_nbr2.Name = "Cust_nbr2";
            this.Cust_nbr2.Size = new System.Drawing.Size(65, 20);
            this.Cust_nbr2.TabIndex = 4;
            // 
            // Cust_nbr1
            // 
            this.Cust_nbr1.Location = new System.Drawing.Point(237, 32);
            this.Cust_nbr1.Name = "Cust_nbr1";
            this.Cust_nbr1.Size = new System.Drawing.Size(49, 20);
            this.Cust_nbr1.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(286, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Макс. висота підвісу антени, м";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(133, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Висота споруди, м";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Тип споруди";
            // 
            // grpBoxPhoto
            // 
            this.grpBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxPhoto.Controls.Add(this.pctBox);
            this.grpBoxPhoto.Controls.Add(this.btndel);
            this.grpBoxPhoto.Controls.Add(this.btnAdd);
            this.grpBoxPhoto.Controls.Add(this.cmbBoxPhoto);
            this.grpBoxPhoto.Location = new System.Drawing.Point(542, 427);
            this.grpBoxPhoto.Name = "grpBoxPhoto";
            this.grpBoxPhoto.Size = new System.Drawing.Size(513, 295);
            this.grpBoxPhoto.TabIndex = 14;
            this.grpBoxPhoto.TabStop = false;
            this.grpBoxPhoto.Text = "Фото";
            // 
            // pctBox
            // 
            this.pctBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pctBox.ErrorImage = null;
            this.pctBox.Location = new System.Drawing.Point(6, 50);
            this.pctBox.Name = "pctBox";
            this.pctBox.Size = new System.Drawing.Size(498, 239);
            this.pctBox.TabIndex = 3;
            this.pctBox.TabStop = false;
            this.pctBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pctBox_MouseDoubleClick);
            // 
            // btndel
            // 
            this.btndel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btndel.Location = new System.Drawing.Point(429, 14);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(75, 23);
            this.btndel.TabIndex = 2;
            this.btndel.Text = "Вилучити";
            this.btndel.UseVisualStyleBackColor = true;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(326, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Додати";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbBoxPhoto
            // 
            this.cmbBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxPhoto.FormattingEnabled = true;
            this.cmbBoxPhoto.Location = new System.Drawing.Point(4, 16);
            this.cmbBoxPhoto.Name = "cmbBoxPhoto";
            this.cmbBoxPhoto.Size = new System.Drawing.Size(281, 21);
            this.cmbBoxPhoto.TabIndex = 0;
            // 
            // txtBoxAdmSite
            // 
            this.txtBoxAdmSite.Location = new System.Drawing.Point(11, 15);
            this.txtBoxAdmSite.Multiline = true;
            this.txtBoxAdmSite.Name = "txtBoxAdmSite";
            this.txtBoxAdmSite.ReadOnly = true;
            this.txtBoxAdmSite.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxAdmSite.Size = new System.Drawing.Size(343, 44);
            this.txtBoxAdmSite.TabIndex = 0;
            this.txtBoxAdmSite.TabStop = false;
            this.txtBoxAdmSite.TextChanged += new System.EventHandler(this.txtBoxAdmSite_TextChanged);
            // 
            // btnChangeAdmSite
            // 
            this.btnChangeAdmSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChangeAdmSite.Location = new System.Drawing.Point(9, 59);
            this.btnChangeAdmSite.Name = "btnChangeAdmSite";
            this.btnChangeAdmSite.Size = new System.Drawing.Size(81, 23);
            this.btnChangeAdmSite.TabIndex = 1;
            this.btnChangeAdmSite.Text = "Змінити";
            this.btnChangeAdmSite.UseVisualStyleBackColor = true;
            this.btnChangeAdmSite.Click += new System.EventHandler(this.btnChangeAdmSite_Click);
            // 
            // btnOffAdmSite
            // 
            this.btnOffAdmSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOffAdmSite.Location = new System.Drawing.Point(96, 59);
            this.btnOffAdmSite.Name = "btnOffAdmSite";
            this.btnOffAdmSite.Size = new System.Drawing.Size(81, 23);
            this.btnOffAdmSite.TabIndex = 2;
            this.btnOffAdmSite.Text = "Відключити";
            this.btnOffAdmSite.UseVisualStyleBackColor = true;
            this.btnOffAdmSite.Click += new System.EventHandler(this.btnOffAdmSite_Click);
            // 
            // grpBoxAdmSite
            // 
            this.grpBoxAdmSite.Controls.Add(this.btnOffAdmSite);
            this.grpBoxAdmSite.Controls.Add(this.btnChangeAdmSite);
            this.grpBoxAdmSite.Controls.Add(this.txtBoxAdmSite);
            this.grpBoxAdmSite.Location = new System.Drawing.Point(8, 458);
            this.grpBoxAdmSite.Name = "grpBoxAdmSite";
            this.grpBoxAdmSite.Size = new System.Drawing.Size(362, 88);
            this.grpBoxAdmSite.TabIndex = 4;
            this.grpBoxAdmSite.TabStop = false;
            this.grpBoxAdmSite.Text = " Адміністративний сайт ";
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAccept.Location = new System.Drawing.Point(380, 16);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(116, 23);
            this.btnAccept.TabIndex = 2;
            this.btnAccept.Text = "Перевірено";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCheck.Location = new System.Drawing.Point(9, 16);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(123, 23);
            this.btnCheck.TabIndex = 0;
            this.btnCheck.Text = "Запитати перевірку";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // lblCheckName
            // 
            this.lblCheckName.AutoSize = true;
            this.lblCheckName.Location = new System.Drawing.Point(4, 44);
            this.lblCheckName.Name = "lblCheckName";
            this.lblCheckName.Size = new System.Drawing.Size(93, 13);
            this.lblCheckName.TabIndex = 3;
            this.lblCheckName.Text = "Запит перевірки:";
            // 
            // lblCheckDate
            // 
            this.lblCheckDate.AutoSize = true;
            this.lblCheckDate.Location = new System.Drawing.Point(4, 57);
            this.lblCheckDate.Name = "lblCheckDate";
            this.lblCheckDate.Size = new System.Drawing.Size(36, 13);
            this.lblCheckDate.TabIndex = 4;
            this.lblCheckDate.Text = "Дата:";
            // 
            // lblAcceptDate
            // 
            this.lblAcceptDate.AutoSize = true;
            this.lblAcceptDate.Location = new System.Drawing.Point(224, 57);
            this.lblAcceptDate.Name = "lblAcceptDate";
            this.lblAcceptDate.Size = new System.Drawing.Size(36, 13);
            this.lblAcceptDate.TabIndex = 8;
            this.lblAcceptDate.Text = "Дата:";
            // 
            // lblAcceptName
            // 
            this.lblAcceptName.AutoSize = true;
            this.lblAcceptName.Location = new System.Drawing.Point(224, 44);
            this.lblAcceptName.Name = "lblAcceptName";
            this.lblAcceptName.Size = new System.Drawing.Size(62, 13);
            this.lblAcceptName.TabIndex = 7;
            this.lblAcceptName.Text = "Перевірив:";
            // 
            // grpBoxCheck
            // 
            this.grpBoxCheck.Controls.Add(this.label7);
            this.grpBoxCheck.Controls.Add(this.checkBox_special_check);
            this.grpBoxCheck.Controls.Add(this.picAdmStatus);
            this.grpBoxCheck.Controls.Add(this.btnError);
            this.grpBoxCheck.Controls.Add(this.lblAcceptDate);
            this.grpBoxCheck.Controls.Add(this.lblAcceptName);
            this.grpBoxCheck.Controls.Add(this.lblCheckDate);
            this.grpBoxCheck.Controls.Add(this.lblCheckName);
            this.grpBoxCheck.Controls.Add(this.btnAccept);
            this.grpBoxCheck.Controls.Add(this.btnCheck);
            this.grpBoxCheck.Controls.Add(this.lblCheckDateVal);
            this.grpBoxCheck.Controls.Add(this.lblCheckNamVal);
            this.grpBoxCheck.Controls.Add(this.lblAcceptNameVal);
            this.grpBoxCheck.Controls.Add(this.lblAcceptDateVal);
            this.grpBoxCheck.Location = new System.Drawing.Point(8, 550);
            this.grpBoxCheck.Name = "grpBoxCheck";
            this.grpBoxCheck.Size = new System.Drawing.Size(526, 76);
            this.grpBoxCheck.TabIndex = 8;
            this.grpBoxCheck.TabStop = false;
            this.grpBoxCheck.Text = "Перевірка координат та адреси встановлення";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(275, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "непідконтрольний";
            // 
            // checkBox_special_check
            // 
            this.checkBox_special_check.AutoSize = true;
            this.checkBox_special_check.Location = new System.Drawing.Point(276, 14);
            this.checkBox_special_check.Name = "checkBox_special_check";
            this.checkBox_special_check.Size = new System.Drawing.Size(82, 17);
            this.checkBox_special_check.TabIndex = 12;
            this.checkBox_special_check.Text = "Тимчасово";
            this.checkBox_special_check.UseVisualStyleBackColor = true;
            this.checkBox_special_check.CheckedChanged += new System.EventHandler(this.checkBox_special_check_CheckedChanged);
            // 
            // picAdmStatus
            // 
            this.picAdmStatus.Location = new System.Drawing.Point(156, 16);
            this.picAdmStatus.Name = "picAdmStatus";
            this.picAdmStatus.Size = new System.Drawing.Size(25, 25);
            this.picAdmStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAdmStatus.TabIndex = 11;
            this.picAdmStatus.TabStop = false;
            // 
            // btnError
            // 
            this.btnError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnError.Enabled = false;
            this.btnError.Location = new System.Drawing.Point(203, 16);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(69, 23);
            this.btnError.TabIndex = 1;
            this.btnError.Text = "Відхилити";
            this.btnError.UseVisualStyleBackColor = true;
            this.btnError.Visible = false;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // lblCheckDateVal
            // 
            this.lblCheckDateVal.Location = new System.Drawing.Point(95, 57);
            this.lblCheckDateVal.Name = "lblCheckDateVal";
            this.lblCheckDateVal.Size = new System.Drawing.Size(123, 13);
            this.lblCheckDateVal.TabIndex = 5;
            this.lblCheckDateVal.Text = "123456";
            // 
            // lblCheckNamVal
            // 
            this.lblCheckNamVal.Location = new System.Drawing.Point(95, 44);
            this.lblCheckNamVal.Name = "lblCheckNamVal";
            this.lblCheckNamVal.Size = new System.Drawing.Size(134, 13);
            this.lblCheckNamVal.TabIndex = 6;
            this.lblCheckNamVal.Text = "lblCheckNamVal";
            // 
            // lblAcceptNameVal
            // 
            this.lblAcceptNameVal.Location = new System.Drawing.Point(286, 44);
            this.lblAcceptNameVal.Name = "lblAcceptNameVal";
            this.lblAcceptNameVal.Size = new System.Drawing.Size(130, 13);
            this.lblAcceptNameVal.TabIndex = 9;
            this.lblAcceptNameVal.Text = "123456";
            // 
            // lblAcceptDateVal
            // 
            this.lblAcceptDateVal.Location = new System.Drawing.Point(286, 57);
            this.lblAcceptDateVal.Name = "lblAcceptDateVal";
            this.lblAcceptDateVal.Size = new System.Drawing.Size(130, 15);
            this.lblAcceptDateVal.TabIndex = 10;
            this.lblAcceptDateVal.Text = "123456";
            // 
            // gbNote
            // 
            this.gbNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbNote.Controls.Add(this.tbOldAddress);
            this.gbNote.Location = new System.Drawing.Point(7, 624);
            this.gbNote.Name = "gbNote";
            this.gbNote.Size = new System.Drawing.Size(529, 89);
            this.gbNote.TabIndex = 10;
            this.gbNote.TabStop = false;
            this.gbNote.Text = "Примітка";
            // 
            // tbOldAddress
            // 
            this.tbOldAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbOldAddress.Location = new System.Drawing.Point(3, 16);
            this.tbOldAddress.Multiline = true;
            this.tbOldAddress.Name = "tbOldAddress";
            this.tbOldAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOldAddress.Size = new System.Drawing.Size(523, 70);
            this.tbOldAddress.TabIndex = 0;
            this.tbOldAddress.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(971, 718);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(871, 718);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 26;
            this.btnOk.Text = "OK (F5)";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Visible = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // gbxLog
            // 
            this.gbxLog.Controls.Add(this.txtModifiedDate);
            this.gbxLog.Controls.Add(this.difieddate);
            this.gbxLog.Controls.Add(this.txtModifiedBy);
            this.gbxLog.Controls.Add(this.o);
            this.gbxLog.Controls.Add(this.txtCreatedDate);
            this.gbxLog.Controls.Add(this.lbCreatedDate);
            this.gbxLog.Controls.Add(this.txtCreatedBy);
            this.gbxLog.Controls.Add(this.lbCreatedBy);
            this.gbxLog.Location = new System.Drawing.Point(374, 455);
            this.gbxLog.Name = "gbxLog";
            this.gbxLog.Size = new System.Drawing.Size(160, 88);
            this.gbxLog.TabIndex = 6;
            this.gbxLog.TabStop = false;
            this.gbxLog.Text = "Аудит";
            // 
            // txtModifiedDate
            // 
            this.txtModifiedDate.AutoSize = true;
            this.txtModifiedDate.Location = new System.Drawing.Point(36, 69);
            this.txtModifiedDate.Name = "txtModifiedDate";
            this.txtModifiedDate.Size = new System.Drawing.Size(81, 13);
            this.txtModifiedDate.TabIndex = 8;
            this.txtModifiedDate.Text = "txtModifiedDate";
            // 
            // difieddate
            // 
            this.difieddate.AutoSize = true;
            this.difieddate.Location = new System.Drawing.Point(2, 69);
            this.difieddate.Name = "difieddate";
            this.difieddate.Size = new System.Drawing.Size(36, 13);
            this.difieddate.TabIndex = 7;
            this.difieddate.Text = "Дата:";
            // 
            // txtModifiedBy
            // 
            this.txtModifiedBy.AutoSize = true;
            this.txtModifiedBy.Location = new System.Drawing.Point(63, 52);
            this.txtModifiedBy.Name = "txtModifiedBy";
            this.txtModifiedBy.Size = new System.Drawing.Size(70, 13);
            this.txtModifiedBy.TabIndex = 6;
            this.txtModifiedBy.Text = "txtModifiedBy";
            // 
            // o
            // 
            this.o.AutoSize = true;
            this.o.Location = new System.Drawing.Point(2, 52);
            this.o.Name = "o";
            this.o.Size = new System.Drawing.Size(57, 13);
            this.o.TabIndex = 5;
            this.o.Text = "Змінений:";
            // 
            // txtCreatedDate
            // 
            this.txtCreatedDate.AutoSize = true;
            this.txtCreatedDate.Location = new System.Drawing.Point(36, 35);
            this.txtCreatedDate.Name = "txtCreatedDate";
            this.txtCreatedDate.Size = new System.Drawing.Size(78, 13);
            this.txtCreatedDate.TabIndex = 4;
            this.txtCreatedDate.Text = "txtCreatedDate";
            // 
            // lbCreatedDate
            // 
            this.lbCreatedDate.AutoSize = true;
            this.lbCreatedDate.Location = new System.Drawing.Point(2, 35);
            this.lbCreatedDate.Name = "lbCreatedDate";
            this.lbCreatedDate.Size = new System.Drawing.Size(36, 13);
            this.lbCreatedDate.TabIndex = 3;
            this.lbCreatedDate.Text = "Дата:";
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.AutoSize = true;
            this.txtCreatedBy.Location = new System.Drawing.Point(63, 18);
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Size = new System.Drawing.Size(67, 13);
            this.txtCreatedBy.TabIndex = 2;
            this.txtCreatedBy.Text = "txtCreatedBy";
            // 
            // lbCreatedBy
            // 
            this.lbCreatedBy.AutoSize = true;
            this.lbCreatedBy.Location = new System.Drawing.Point(2, 18);
            this.lbCreatedBy.Name = "lbCreatedBy";
            this.lbCreatedBy.Size = new System.Drawing.Size(64, 13);
            this.lbCreatedBy.TabIndex = 1;
            this.lbCreatedBy.Text = "Створений:";
            // 
            // btmIcsmSite
            // 
            this.btmIcsmSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmIcsmSite.Location = new System.Drawing.Point(433, 718);
            this.btmIcsmSite.Name = "btmIcsmSite";
            this.btmIcsmSite.Size = new System.Drawing.Size(103, 23);
            this.btmIcsmSite.TabIndex = 29;
            this.btmIcsmSite.Text = "Картка ICSm";
            this.btmIcsmSite.UseVisualStyleBackColor = true;
            this.btmIcsmSite.Click += new System.EventHandler(this.btmIcsmSite_Click);
            // 
            // AdminSiteAllTech2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelExit;
            this.ClientSize = new System.Drawing.Size(1062, 750);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btmIcsmSite);
            this.Controls.Add(this.gbxLog);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gbNote);
            this.Controls.Add(this.grpBoxCheck);
            this.Controls.Add(this.grpBoxAdmSite);
            this.Controls.Add(this.grpBoxPhoto);
            this.Controls.Add(this.grpBoxMap);
            this.Controls.Add(this.grpBoxAddress);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancelExit);
            this.Controls.Add(this.btnSaveExit);
            this.Controls.Add(this.grpBoxKoord);
            this.MinimumSize = new System.Drawing.Size(800, 726);
            this.Name = "AdminSiteAllTech2";
            this.ShowInTaskbar = true;
            this.Text = "Форма ВРВ сайти";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminSiteAllTech2_FormClosing);
            this.Load += new System.EventHandler(this.AdminSiteAllTech_Load);
            this.Shown += new System.EventHandler(this.AdminSiteAllTech2_Shown);
            this.grpBoxKoord.ResumeLayout(false);
            this.grpBoxKoord.PerformLayout();
            this.grpBoxAddress.ResumeLayout(false);
            this.grpBoxAddress.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBoxPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).EndInit();
            this.grpBoxAdmSite.ResumeLayout(false);
            this.grpBoxAdmSite.PerformLayout();
            this.grpBoxCheck.ResumeLayout(false);
            this.grpBoxCheck.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAdmStatus)).EndInit();
            this.gbNote.ResumeLayout(false);
            this.gbNote.PerformLayout();
            this.gbxLog.ResumeLayout(false);
            this.gbxLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxLatLow;
        private System.Windows.Forms.TextBox txtBoxLongLow;
        private System.Windows.Forms.Label lblLatitude;
        private System.Windows.Forms.Label lblLongitude;
        private System.Windows.Forms.GroupBox grpBoxKoord;
        private System.Windows.Forms.Button btnSaveExit;
        private System.Windows.Forms.Button btnCancelExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblDataCoatuu;
        private System.Windows.Forms.TextBox txtBoxKoatuu;
        private System.Windows.Forms.Label lblAddres;
        private System.Windows.Forms.Label lblAsl;
        private System.Windows.Forms.Button btnMapToAsl;
        private System.Windows.Forms.Label lblLimit;
        private System.Windows.Forms.TextBox txtBoxLimit;
        private System.Windows.Forms.GroupBox grpBoxAddress;
        private System.Windows.Forms.TextBox txtBoxAsl;
        private System.Windows.Forms.Label lblLong;
        private System.Windows.Forms.Label lblLat;
        private System.Windows.Forms.GroupBox grpBoxMap;
        private System.Windows.Forms.GroupBox grpBoxPhoto;
        private System.Windows.Forms.PictureBox pctBox;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbBoxPhoto;
        private System.Windows.Forms.Panel pnlAddress;
        private System.Windows.Forms.TextBox txtBoxAdmSite;
        private System.Windows.Forms.Button btnChangeAdmSite;
        private System.Windows.Forms.Button btnOffAdmSite;
        private System.Windows.Forms.GroupBox grpBoxAdmSite;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label lblCheckName;
        private System.Windows.Forms.Label lblCheckDate;
        private System.Windows.Forms.Label lblAcceptDate;
        private System.Windows.Forms.Label lblAcceptName;
        private System.Windows.Forms.GroupBox grpBoxCheck;
        private System.Windows.Forms.Label lblAcceptNameVal;
        private System.Windows.Forms.Label lblAcceptDateVal;
        private System.Windows.Forms.Label lblCheckDateVal;
        private System.Windows.Forms.Label lblCheckNamVal;
        private System.Windows.Forms.Button btnError;
        private System.Windows.Forms.GroupBox gbNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFullAddress;
        private System.Windows.Forms.Panel pnlShortAddress;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox tbOldAddress;
        private System.Windows.Forms.GroupBox gbxLog;
        private System.Windows.Forms.Label txtModifiedDate;
        private System.Windows.Forms.Label difieddate;
        private System.Windows.Forms.Label txtModifiedBy;
        private System.Windows.Forms.Label o;
        private System.Windows.Forms.Label txtCreatedDate;
        private System.Windows.Forms.Label lbCreatedDate;
        private System.Windows.Forms.Label txtCreatedBy;
        private System.Windows.Forms.Label lbCreatedBy;
        private System.Windows.Forms.Button btmIcsmSite;
        private System.Windows.Forms.PictureBox picAdmStatus;
        private System.Windows.Forms.Button btAddrLikeAdmSiteHas;
        private System.Windows.Forms.Button btCoordLikeAdmSiteHas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox TypeObject;
        private System.Windows.Forms.TextBox Cust_nbr2;
        private System.Windows.Forms.TextBox Cust_nbr1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_number_building;
        private System.Windows.Forms.TextBox txt_owner;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_set_layer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_special_check;        
    }
}