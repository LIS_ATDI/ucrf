﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class RegistryPortForm : Form
    {
        /// <summary>
        /// Отобразить форму
        /// </summary>
        /// <param name="id">Id записи</param>
        /// <returns>True - внесли изменения, иначе False</returns>
        public static bool ShowForm(int id)
        {
            bool retVal = false;
            using (RegistryPortForm frm = new RegistryPortForm(id))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                    retVal = true;
            }
            return retVal;
        }
        //===========================================================
        private ComboBoxDictionaryList<string, string> _listProvince;
        private RegistryPort _port;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">Id записи</param>
        public RegistryPortForm(int id)
        {
            InitializeComponent();
            this.Text = CLocaliz.TxT("Port of registry");
            label1.Text = CLocaliz.TxT("Port of registry")+"(укр)";
            label2.Text = CLocaliz.TxT("Port of registry")+"(лат)";
            label3.Text = CLocaliz.TxT("Region, where the port");
            btnOk.Text = CLocaliz.TxT("Save");
            btnCancel.Text = CLocaliz.TxT("Cancel");
            //----
            _port = new RegistryPort(id);
            //----
            _listProvince = new ComboBoxDictionaryList<string, string>();
            IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            try
            {
                HashSet<string> hashProvince = new HashSet<string>();
                rsArea.Select("ID,NAME");
                rsArea.OrderBy("NAME", OrderDirection.Ascending);
                for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                {
                    string province = rsArea.GetS("NAME");
                    if (!hashProvince.Contains(province))
                    {
                        hashProvince.Add(province);
                        _listProvince.Add(new ComboBoxDictionary<string, string>(province, province));
                    }
                }
            }
            finally
            {
                if (rsArea.IsOpen())
                    rsArea.Close();
                rsArea.Destroy();
            }
            
            //----
            tbNameUkr.DataBindings.Add("Text", _port, RegistryPort.FieldNameUkr);
            tbNameEng.DataBindings.Add("Text", _port, RegistryPort.FieldNameEng);
            _listProvince.InitComboBox(cbProvince);
            cbProvince.DataBindings.Add("SelectedValue", _port, RegistryPort.FieldProvince);

        }
        /// <summary>
        /// Сохранить изменения
        /// </summary>
        private void btnOk_Click(object sender, EventArgs e)
        {
            _port.Save();
        }
    }
}
