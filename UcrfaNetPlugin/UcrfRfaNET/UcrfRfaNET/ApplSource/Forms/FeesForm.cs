﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource
{
    public partial class FeesForm : Form
    {
        private class RadioTechnology
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }


        //Запомнить последнюю введеную позициб статьи, чтобы пользователю меньше щелкать было
        //private static string _lastPosition = "";

        //private ComboBoxDictionaryList<int, string> _listRadioSystem;
        private ComboBoxDictionaryList<string, string> _listRadioComm;
        private Fee _fee = new Fee();

        public FeesForm(string tableName, int id)
        {
            InitializeComponent();

            _fee.Id = id;
            _fee.Load();
            
            _listRadioComm = new ComboBoxDictionaryList<string, string>();                        
            foreach (KeyValuePair<string, string> kvp in EriFiles.GetEriCodeAndDescr("Fees"))
            {
                _listRadioComm.Add(new ComboBoxDictionary<string, string>(kvp.Key, kvp.Key+" "+kvp.Value));                
            }            
            
            _listRadioComm.InitComboBox(cbRadioCommunication);
            
            tbMinFrequency.DataBindings.Add("Text", _fee, "MinimalFrequency", true);
            tbMaxFrequency.DataBindings.Add("Text", _fee, "MaximalFrequency", true);
            tbMinPower.DataBindings.Add("Text", _fee, "MinimalPower", true);
            tbMaxPower.DataBindings.Add("Text", _fee, "MaximalPower", true);
            tbFee.DataBindings.Add("Text", _fee, "Cost", true);

            cbRadioCommunication.DataBindings.Add("SelectedValue", _fee, "OrderSymbol", true);
            tbRadiotechnology.Text = _fee.RadioTechnologyIdListAsString();
        }


        public void SaveAppl()
        {            
            _fee.Save();
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            FChannels frm = new FChannels();
            frm.Text = "Вибрати радіотехнології";
            frm.AllChannelsCaption = "Наявні радіотехнології";
            frm.SelectChannelsCaption = "Вибрані радіотехнології";

            RadioSystemList rsl = new RadioSystemList();
            rsl.ReadRadioSystemList();
            ComboBoxDictionaryList<int, string> _listRadioSystem = rsl.GetComboBoxDictionaryList();

            for (int i = 0; i < _listRadioSystem.Count; i++)
            {
                RadioTechnology radioTechnology = new RadioTechnology();
                radioTechnology.Id = _listRadioSystem[i].Key;
                radioTechnology.Name = _listRadioSystem[i].Description;

                if (_fee.RadioTechnologyIdList.Contains(radioTechnology.Id))
                    frm.CLBSelectChannel.Items.Add(radioTechnology);
                else
                    frm.CLBAllChannel.Items.Add(radioTechnology);
            }

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _fee.RadioTechnologyIdList.Clear();
                foreach (object selectedObject in frm.CLBSelectChannel.Items)
                {
                    RadioTechnology radioTechnology = selectedObject as RadioTechnology;
                    _fee.RadioTechnologyIdList.Add(radioTechnology.Id);                    
                }
                tbRadiotechnology.Text = _fee.RadioTechnologyIdListAsString();
            }
        }

        private void cbRadioCommunication_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }
    }
}
