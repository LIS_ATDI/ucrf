﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class AmateurEquipForm : Form, INotifyPropertyChanged
    {
        public AmateurEquipForm()
        {
            InitializeComponent();
        }

        public AmateurEquipment AmatEquip;

        #region Implementation of INotifyPropertyChanged
        /// <summary>
        /// Имя
        /// </summary>       
        public string NameEquip
        {
            get
            {
                return AmatEquip.Name;
            }
            set
            {
                if (value != AmatEquip.Name)
                {
                    AmatEquip.Name = value;
                    NotifyPropertyChanged("NameEquip");
                }
            }
        }
        /// <summary>
        /// Класс излучения
        /// </summary>      
        public string ClassEmission
        {
            get
            {
                return AmatEquip.DesigEmission;
            }
            set
            {
                if (value != AmatEquip.DesigEmission)
                {
                    AmatEquip.DesigEmission = value;
                    NotifyPropertyChanged("ClassEmission");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public AmateurEquipForm(int id)
        {
            AmatEquip = new AmateurEquipment();
            AmatEquip.Id = id;
            AmatEquip.Load();
            InitializeComponent();
            Text = id != IM.NullI ? "Редагування запису за номером ID: " + id : "Створення нового запису";
            //Привязка 
            txtBoxName.DataBindings.Add("Text", this, "NameEquip");
            txtBoxDesEmi.DataBindings.Add("Text", this, "ClassEmission");
        }
        
        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void Save()
        {
            if (AmatEquip != null)
                AmatEquip.Save();
        }

        private void btnDesisEm_Click(object sender, EventArgs e)
        {
            string desEmi = txtBoxDesEmi.Text;
            if (IMAuxForms.EditDesigEmiss(ref desEmi, false))
            {
                txtBoxDesEmi.Text = desEmi;
                AmatEquip.DesigEmission = desEmi;             
            }            

        }          
    }
}
