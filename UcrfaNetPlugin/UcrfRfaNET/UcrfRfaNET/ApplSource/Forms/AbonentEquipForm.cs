﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class AbonentEquipForm : Form, INotifyPropertyChanged
    {
        public Classes.AbonentEquipment AbonEquip;
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Мощность
        /// </summary>        
        public string Power
        {
            get
            {
                return AbonEquip.MaxPower[PowerUnits.W].Round(3).ToStringNullD();
            }
            set
            {
                if (value != AbonEquip.MaxPower[PowerUnits.W].ToStringNullD())
                {
                    AbonEquip.MaxPower[PowerUnits.W] = value.ToDouble(IM.NullD).Round(3);
                    NotifyPropertyChanged("Power");
                }
            }
        }

        /// <summary>
        /// Имя
        /// </summary>       
        public string NameEquip
        {
            get
            {
                return AbonEquip.Name;
            }
            set
            {
                if (value != AbonEquip.Name)
                {
                    AbonEquip.Name = value;
                    NotifyPropertyChanged("NameEquip");
                }
            }
        }

        /// <summary>
        /// Номер сертификата
        /// </summary>       
        public string CertNumb
        {
            get
            {
                return AbonEquip.Certificate.Symbol;
            }
            set
            {
                if (value != AbonEquip.Certificate.Symbol)
                {
                    AbonEquip.Certificate.Symbol = value;
                    NotifyPropertyChanged("CertNumb");
                }
            }
        }

        /// <summary>
        /// Дата сертификата
        /// </summary>       
        public DateTime CertDate
        {
            get
            {               
                return AbonEquip.Certificate.Date;
            }
            set
            {
                if (value != AbonEquip.Certificate.Date)
                {
                    AbonEquip.Certificate.Date = value;
                    NotifyPropertyChanged("CertDate");
                }
            }
        }

        /// <summary>
        /// Класс излучения
        /// </summary>      
        public string ClassEmission
        {
            get
            {
                return AbonEquip.DesigEmission;
            }
            set
            {
                if (value != AbonEquip.DesigEmission)
                {
                    AbonEquip.DesigEmission = value;
                    NotifyPropertyChanged("ClassEmission");
                }
            }
        }

        /// <summary>
        /// Мінімальна частота передавача
        /// </summary>
        public double MinTxFreq
        {
            get
            {
                return AbonEquip.MinFreqTx;
            }
            set
            {
                if (value != AbonEquip.MinFreqTx)
                {
                    AbonEquip.MinFreqTx = value;
                    NotifyPropertyChanged("MinTxFreq");
                }
            }
        }

        /// <summary>
        /// Максимальна частота передавача
        /// </summary>
        public double MaxTxFreq
        {
            get
            {
                return AbonEquip.MaxFreqTx;
            }
            set
            {
                if (value != AbonEquip.MaxFreqTx)
                {
                    AbonEquip.MaxFreqTx = value;
                    NotifyPropertyChanged("MaxTxFreq");
                }
            }
        }

        /// <summary>
        /// Максимальна частота приймача
        /// </summary>
        public double MaxRxFreq
        {
            get
            {
                return AbonEquip.MaxFreqRx;
            }
            set
            {
                if (value != AbonEquip.MaxFreqRx)
                {
                    AbonEquip.MaxFreqRx = value;
                    NotifyPropertyChanged("MaxRxFreq");
                }
            }
        }

        /// <summary>
        /// Мінімальна частота приймача
        /// </summary>
        public double MinRxFreq
        {
            get
            {
                return AbonEquip.MinFreqRx;
            }
            set
            {
                if (value != AbonEquip.MinFreqRx)
                {
                    AbonEquip.MinFreqRx = value;
                    NotifyPropertyChanged("MinRxFreq");
                }
            }
        }

        /// <summary>
        /// Ширина пропускания
        /// </summary>        
        public string Bw
        {
            get
            {
                if (AbonEquip.BandWidth == 0.0)
                    return string.Empty;
                return AbonEquip.BandWidth.ToStringNullD();
            }
            set
            {
                if (value != AbonEquip.BandWidth.ToString())
                {
                    AbonEquip.BandWidth = Convert.ToDouble(value);
                    NotifyPropertyChanged("Bw");
                }
            }
        }

        /// <summary>
        /// Код
        /// </summary>
        public string Code
        {
            get
            {
                return AbonEquip.CodeEquip;
            }
            set
            {
                if (value != AbonEquip.CodeEquip)
                {
                    AbonEquip.CodeEquip = value;
                    NotifyPropertyChanged("Code");
                }
            }
        }

        /// <summary>
        /// Сімейство
        /// </summary>
        public string Family
        {
            get
            {
                return AbonEquip.FamilyEquip;
            }
            set
            {
                if (value != AbonEquip.FamilyEquip)
                {
                    AbonEquip.FamilyEquip = value;
                    NotifyPropertyChanged("Family");
                }
            }
        }

        /// <summary>
        /// Виробник
        /// </summary>
        public string Manufacturer
        {
            get
            {
                return AbonEquip.ManufactEquip;
            }
            set
            {
                if (value != AbonEquip.ManufactEquip)
                {
                    AbonEquip.ManufactEquip = value;
                    NotifyPropertyChanged("Manufacturer");
                }
            }
        }
        public string TechNo
        {
            get
            {
                return AbonEquip.TechNoEquip;
            }
            set
            {
                if (value != AbonEquip.TechNoEquip)
                {
                    AbonEquip.TechNoEquip = value;
                    NotifyPropertyChanged("TechNo");
                }
            }
        }

        #endregion

        public AbonentEquipForm(int id, bool copyRecord = false)
        {
            AbonEquip = new AbonentEquipment();
            AbonEquip.Id = id;
            AbonEquip.Load();
            if(copyRecord)
            {
               AbonEquip.Id = IM.NullI;
            }
            InitializeComponent();
            Text = id != IM.NullI ? "Редагування запису за номером ID: " + id : "Створення нового запису";
            //Привязка 
            txtBoxName.DataBindings.Add("Text", this, "NameEquip");
            txtBoxPower.DataBindings.Add("Text", this, "Power");

            txtBoxDate.DataBindings.Add("Text", this, "CertDate", true);
            txtBoxCertNumb.DataBindings.Add("Text", this, "CertNumb");

            txtBoxDesEmi.DataBindings.Add("Text", this, "ClassEmission");
            txtBoxBw.DataBindings.Add("Text", this, "Bw");

            txtBoxMaxFreqRx.DataBindings.Add("Text", this, "MaxRxFreq");
            txtBoxMinFreqRx.DataBindings.Add("Text", this, "MinRxFreq");
            txtBoxMaxFreqTx.DataBindings.Add("Text", this, "MaxTxFreq");
            txtBoxMinFreqTx.DataBindings.Add("Text", this, "MinTxFreq");
            txtBoxCode.DataBindings.Add("Text", this, "Code");
            txtBoxFamily.DataBindings.Add("Text", this, "Family");
            txtBoxManuf.DataBindings.Add("Text", this, "Manufacturer");
            txtBoxTechNo.DataBindings.Add("Text", this, "TechNo");            
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void Save()
        {           
            if (AbonEquip != null)
                AbonEquip.Save();
        }

        private void btnDesEmi_Click(object sender, EventArgs e)
        {
            string desEm = txtBoxDesEmi.Text;
            if(IMAuxForms.EditDesigEmiss(ref desEm, false))
            {
                txtBoxDesEmi.Text = desEm;
                AbonEquip.DesigEmission = desEm;
            }
        }
    }
}
