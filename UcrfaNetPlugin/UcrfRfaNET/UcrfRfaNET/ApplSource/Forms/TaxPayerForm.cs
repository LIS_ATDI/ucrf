﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET;
using OrmCs;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class TaxPayerForm : Form
    {
        private ComboBoxDictionaryList<string, string> _tableNameList = new ComboBoxDictionaryList<string, string>();
        
        TaxPayerReportParams _params = TaxPayerReportParams.Get();

        public String TableName { get; set; }

        public TaxPayerForm()
        {
            InitializeComponent();
            TableName = "";
            _tableNameList.InitComboBox(cbTableName);
            cbTableName.DataBindings.Add("SelectedValue", this, "TableName", true);

            System.Windows.Forms.Binding b = new System.Windows.Forms.Binding("Checked", taxPayerReportParamsBindingSource, "DataFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            b.Format += new ConvertEventHandler((s, e) => { e.Value = ((TaxPayerReportParams.F1Method)e.Value == TaxPayerReportParams.F1Method.Allot); });
            b.Parse += new ConvertEventHandler((s, e) => { if ((bool)e.Value) e.Value = TaxPayerReportParams.F1Method.Allot; });
            rbtF1Allotments.DataBindings.Add(b);
            b = new System.Windows.Forms.Binding("Checked", taxPayerReportParamsBindingSource, "DataFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            b.Format += new ConvertEventHandler((s, e) => { e.Value = ((TaxPayerReportParams.F1Method)e.Value == TaxPayerReportParams.F1Method.Lic); });
            b.Parse += new ConvertEventHandler((s, e) => { if ((bool)e.Value) e.Value = TaxPayerReportParams.F1Method.Lic; });
            rbtF1Whole.DataBindings.Add(b);
            
            taxPayerReportParamsBindingSource.Add(_params);
        }

        private void TaxPayerForm_Load(object sender, EventArgs e)
        {
            int d = DateTime.Today.Day;
            int m = DateTime.Today.Month;
            int y = DateTime.Today.Year;
            dtpBeg.Value = new DateTime((m == 1 ? y - 1 : y), ((m == 1) || (m > 8) ? 7 : 1), 1);
            dtpEnd.Value = (m == 1) || (m == 7) ? new DateTime(y,m,1) : DateTime.Today;
            dtpValueStan.Value = new DateTime(y, m, 1);
            SetOnState();
            //Заполняем список таблиц
            _tableNameList.Add(new ComboBoxDictionary<string, string>("", "Всі"));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(ICSMTbl.itblMobStation, ICSMTbl.itblMobStation));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(ICSMTbl.itblMobStation2, ICSMTbl.itblMobStation2));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(ICSMTbl.itblEarthStation, ICSMTbl.itblEarthStation));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(ICSMTbl.itblMicrowa, ICSMTbl.itblMicrowa));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(PlugTbl.XfaAbonentStation, PlugTbl.XfaAbonentStation));
            _tableNameList.Add(new ComboBoxDictionary<string, string>(PlugTbl.Ship, PlugTbl.Ship));


            //-----------------------------------------
           // LisLocalizationLib.Localization valLocal = null;
           // valLocal = new LisLocalizationLib.Localization(Environment.CurrentDirectory, "XICSM_UcrfRfaNET");
            
           // lbBeg.Text = valLocal.Translate("START_PERIOD");
           // CLocaliz.TxT();
            //------------------------------------------
            lbBeg.Text = CLocaliz.TxT("START_PERIOD");
            lbEnd.Text = CLocaliz.TxT("END_PERIOD");
            label2.Text = CLocaliz.TxT("PATH_SAVE_REPORT");
            groupBox1.Text = CLocaliz.TxT("FORM_REPORT");
            chbF1Status.Text = CLocaliz.TxT("CHECK_REAL");
            chbF1IgnoreTimePeriod.Text = CLocaliz.TxT("CHECK_IGNORE");
            gbxF1Method.Text = CLocaliz.TxT("RADIO_TYPE");
            rbtF1Allotments.Text = CLocaliz.TxT("RADIO_BUTTON1");
            rbtF1Whole.Text = CLocaliz.TxT("RADIO_BUTTON2");
            gbFileType.Text = CLocaliz.TxT("TYPE_FILE");
            cbOneFile.Text = CLocaliz.TxT("CHECK_ONE_FILE");
            lbF2SelectedTable.Text = CLocaliz.TxT("CAPTION_COMBO");
            btnGenerateNew.Text = CLocaliz.TxT("BUTTON_GENERATE");
            btnCancel.Text = CLocaliz.TxT("BUTTON_CANCEL");
            Text = CLocaliz.TxT("CAPTION_FORM_RCR");
        }

        // Устанавливает кнопку OK в нужное состояние
        private void SetOnState()
        {
            if (tbPath.Text.Length>0 && (cbForm1.Checked || cbForm2.Checked || cbForm3.Checked))
                btnGenerateNew.Enabled = true;                
            else
                btnGenerateNew.Enabled = false;
        }

        private void cbForm1_CheckedChanged(object sender, EventArgs e)
        {
            SetOnState();
            chbF1IgnoreTimePeriod.Enabled = cbForm1.Checked && chbF1Status.Checked;
        }        

        private void cbForm2_CheckedChanged(object sender, EventArgs e)
        {
            SetOnState();
            lbF2SelectedTable.Enabled = cbForm2.Checked;
            cbTableName.Enabled = cbForm2.Checked;
        }

        private void cbForm3_CheckedChanged(object sender, EventArgs e)
        {
            SetOnState();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Виберіть папку для формування звітів";

            if (fbd.ShowDialog()==DialogResult.OK)
            {
                tbPath.Text = fbd.SelectedPath;
                SetOnState();
            }
        }

        private void btnGenerateNew_Click(object sender, EventArgs e)
        {
            Reports.ReportType repType = (rbRepTypeExcel.Checked) ? Reports.ReportType.Excel : Reports.ReportType.Csv;
            List<string> errors = new List<string>();
            try
            {
                if (cbForm1.Checked)
                {
                    ITaxPayerForm tp = TaxPayerFormFactory.CreateTaxPayerForm(_params, "", TaxPayerFormType.Type1);
                    string[] error = tp.Generate(Reports.ReportType.ExcelFast, cbOneFile.Checked);
                    errors.AddRange(error);
                }
                if (cbForm2.Checked)
                {
                    ITaxPayerForm tp = TaxPayerFormFactory.CreateTaxPayerForm(_params, TableName, TaxPayerFormType.Type2);
                    string[] error = tp.Generate(Reports.ReportType.ExcelFast, cbOneFile.Checked);
                    errors.AddRange(error);
                }
                if (cbForm3.Checked)
                {
                    ITaxPayerForm tp = TaxPayerFormFactory.CreateTaxPayerForm(_params, "", TaxPayerFormType.Type3);
                    string[] error = tp.Generate(Reports.ReportType.ExcelFast, cbOneFile.Checked);
                    errors.AddRange(error);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            // Отображаем ошибки
            if (errors.Count > 0)
            {
                ICSM.IMLogFile log = new ICSM.IMLogFile();
                log.Create("TaxPayer.txt");
                foreach (string s in errors)
                    log.Warning(string.Format("{0}{1}", s, Environment.NewLine));
                log.Display("Tax payer");
                log.Destroy();
            }
        }

        private void chbF1Status_CheckedChanged(object sender, EventArgs e)
        {
            chbF1IgnoreTimePeriod.Enabled = cbForm1.Checked && chbF1Status.Checked;
        }
    }
}
