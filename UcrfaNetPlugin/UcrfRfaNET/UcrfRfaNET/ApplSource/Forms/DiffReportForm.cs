﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource
{    
    internal partial class DiffReportForm : FBaseForm
    {
        private PTCStatement _statement = new PTCStatement();

        private PTCStatement.Team oldComittee;

        protected BaseDiffClass diffObject = null;   // Обьект, который управляет заявкой

        private ComboBoxDictionaryList<int, string> _employeeDictonary = new ComboBoxDictionaryList<int, string>();
        private ComboBoxDictionaryList<int, string> _overallDictonary = new ComboBoxDictionaryList<int, string>();

        private readonly string vrzString = Enum.GetName(DepartmentType.VRZ.GetType(), DepartmentType.VRZ);
        
        //===============================================================
        //===============================================================
        //                          Функции
        //===============================================================
        //===============================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="baseApplObj">ссылка на обьект, который будет управляет заявкой</param>
        public DiffReportForm(BaseDiffClass baseApplObj)        
        {
            diffObject = baseApplObj;
            InitializeComponent();
        }

        private void DiffReportForm_Load(object sender, EventArgs e)
        {
            
            _statement.Id = diffObject.Station.Id;
            _statement.TableName = diffObject.DiffTableName;
            _statement.Load();

            if (string.IsNullOrEmpty(_statement.Specials))
                _statement.Specials = diffObject.Station.Finding;

            int chiefID = _statement.Comittee.ChiefID;            
            txtSpecials.Text = _statement.Specials;
            txtActialTestResults.Text = _statement.ResultsNV;
            txtTestResults.Text = _statement.ResultsTV;
            txtParamMeasurement.Text = _statement.ResultsParams;
            txtTestParams.Text = _statement.ResultsInstrumental;

            txtGeneralSummary.Text = _statement.GeneralTotal;

            txtEquipParamMeasure.Text = _statement.ProtocolParamsLink;
            txtEquipParameters.Text = _statement.ProtocolInstrumentalLink;
            txtLastSaved.Text = _statement.LastLink;

            FillListByComittee();

            EmployeeList employeeList = new EmployeeList();
            employeeList.ReadEntireEmployeeList();
            _overallDictonary = employeeList.GetComboBoxDictionaryList();
            
            ReadEmployee();
            _employeeDictonary.InitComboBox(cbChief);
            cbChief.DataBindings.Add("SelectedValue", _statement.Comittee, "ChiefID", true);

            oldComittee = _statement.Comittee.Clone() as PTCStatement.Team;
        }
        
        /// <summary>
        /// Прочитать юзеров (employee) в ComboBoxDictionaryList
        /// </summary>
        private void ReadEmployee()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                else
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, vrzString);
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");

                    List<int> list1 = new List<int>()
                                          {
                                              _statement.Comittee.MembersID[0],
                                              _statement.Comittee.MembersID[1],
                                              _statement.Comittee.MembersID[2]
                                          };

                    if (!list1.Contains(ID))
                    {
                        string title = r.GetS("TITLE");
                        string firstName = r.GetS("FIRSTNAME");
                        string lastName = r.GetS("LASTNAME");

                        string employee = lastName + " " + firstName;
                     
                        _employeeDictonary.Add(new ComboBoxDictionary<int, string>(ID, employee));
                    }

                    r.MoveNext();
                }
            }
            finally
            {                
                r.Close();
                r.Destroy();
            }                        
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FillListByComittee()
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);

            lbMembers.Items.Clear();
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                r.SetWhere("TYPE", IMRecordset.Operation.Eq, vrzString);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");

                    string employee = lastName + " " + firstName;
                    //_employeeDictonary.Add(new ComboBoxDictionary<int,string>(ID, employee));
                    if (_statement.Comittee.IsMemberID(ID))
                        lbMembers.Items.Add(employee);                    

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            FChannels frm = new FChannels();
            frm.Text = "Вибрати членів комісії";
            frm.AllChannelsCaption = "Наявні співробітники";
            frm.SelectChannelsCaption = "Члени комісії";

            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                else
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, vrzString);
                r.Open();

                while (!r.IsEOF())
                {
                    int ID = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");

                    string employee = lastName + " " + firstName;

                    if (ID != _statement.Comittee.ChiefID)
                    {
                        if (_statement.Comittee.IsMemberID(ID))
                        {
                            frm.CLBSelectChannel.Items.Add(employee, false);
                        }
                        else
                        {
                            frm.CLBAllChannel.Items.Add(employee, false);
                        }
                    }

                    r.MoveNext();
                }

                frm.CLBSelectChannel.Sorted = true;
                frm.CLBAllChannel.Sorted = true;
            }
            finally
            {                
                r.Close();
                r.Destroy();
            }

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.CLBSelectChannel.Items.Count > _statement.Comittee.MembersID.Length)
                {
                    MessageBox.Show("Не можна вибрати більше " + _statement.Comittee.MembersID.Length + " членів комісії");
                    return;
                }

                lbMembers.Items.Clear();
                _statement.Comittee.ClearMembers();
                for (int i = 0; i < _overallDictonary.Count; i++)
                {
                    for(int j=0; j<frm.CLBSelectChannel.Items.Count; j++)
                    {
                        if (_overallDictonary[i].Description == frm.CLBSelectChannel.Items[j].ToString())
                        {
                            _statement.Comittee.MembersID[j] = _overallDictonary[i].Key;
                            lbMembers.Items.Add(_overallDictonary[i].Description);
                        }
                    }
                }

                cbChief.DataBindings.Clear();
                cbChief.DataSource = null;
                cbChief.Items.Clear();
                _employeeDictonary.Clear();

                ReadEmployee();

                _employeeDictonary.InitComboBox(cbChief);
                cbChief.DataBindings.Add("SelectedValue", _statement.Comittee, "ChiefID", true);
            }
        }
        /// <summary>
        /// Отображает форму для вибора шаблона
        /// </summary>
        /// <param name="selectedText">Возвращает выбранный текст</param>
        /// <param name="docType">Тип документа</param>
        /// <returns>TRUE - выбор подтвержден, иначе FALSE</returns>
        private bool SelectTextOfDoc(out string selectedText, string docType)
        {
            bool retVal = false;
            selectedText = "";
            using (FSelectSCPattern frm = new FSelectSCPattern(DepartmentType.VRZ, DepartmentSectorType.VZR_VZR, docType))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    selectedText = frm.GetPatternText();
                    retVal = true;
                }
            }
            return retVal;
        }

        private void btnLoopupActualTestResults_Click(object sender, EventArgs e)
        {
            string outString;
            if(SelectTextOfDoc(out outString, DocType.NV))
                txtActialTestResults.Text = outString;
        }

        private void btnLookupTestResults_Click(object sender, EventArgs e)
        {
            string outString;
            if (SelectTextOfDoc(out outString, DocType.TV))
                txtTestResults.Text = outString;
        }

        private void btnParamMeasurement_Click(object sender, EventArgs e)
        {
            string outString;
            if (SelectTextOfDoc(out outString, DocType.MP))
                txtParamMeasurement.Text = outString;
        }

        private void btnTestParams_Click(object sender, EventArgs e)
        {
            string outString;
            if (SelectTextOfDoc(out outString, DocType.ITMP))
                txtTestParams.Text = outString;
        }

        private void btnLookupGeneralSummary_Click(object sender, EventArgs e)
        {
            string outString;
            if (SelectTextOfDoc(out outString, DocType.GENERAL))
                txtGeneralSummary.Text = outString;
        }

        private void SavePTCStatement()
        {
            _statement.Specials = txtSpecials.Text;
            _statement.ResultsNV = txtActialTestResults.Text;
            _statement.ResultsTV = txtTestResults.Text;
            _statement.ResultsParams = txtParamMeasurement.Text;
            _statement.ResultsInstrumental = txtTestParams.Text;

            _statement.GeneralTotal = txtGeneralSummary.Text;

            _statement.ProtocolParamsLink = txtEquipParamMeasure.Text;
            _statement.ProtocolInstrumentalLink = txtEquipParameters.Text;
            _statement.LastLink = txtLastSaved.Text;

            _statement.Id = diffObject.Station.Id;
            _statement.TableName = diffObject.DiffTableName;
            _statement.Save();    
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SavePTCStatement();            
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string regionCode = HelpFunction.getAreaCode(diffObject.Station.Position.Province, diffObject.Station.Position.City);
            string applIdStr = diffObject.ApplID.ToString();
            string docSymbol = string.Format("{0}\\{1}", PluginSetting.PluginFolderSetting.FolderAktPtk, string.Format("{0}-{1}-{2}-ПТК.RTF", regionCode, applIdStr, diffObject.PacketID));

            PrintDocs printDocs = new PrintDocs();
            if (printDocs.CreateOneReport(DocType.AKT_PTK, diffObject.ApplType, DateTime.Now, DateTime.Now, PlugTbl.itblXnrfaAppl, diffObject.ApplID, "", docSymbol, true))
            {
                txtLastSaved.Text = docSymbol;
            }
        }

        private void DiffReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((_statement.Specials != txtSpecials.Text) ||
                (_statement.ResultsNV != txtActialTestResults.Text) ||
                (_statement.ResultsTV != txtTestResults.Text) ||
                (_statement.ResultsParams != txtParamMeasurement.Text) ||
                (_statement.ResultsInstrumental != txtTestParams.Text) ||
                (_statement.GeneralTotal != txtGeneralSummary.Text) ||
                (_statement.ProtocolParamsLink != txtEquipParamMeasure.Text) ||
                (_statement.ProtocolInstrumentalLink != txtEquipParameters.Text) ||
                (_statement.LastLink != txtLastSaved.Text) ||
                !_statement.Comittee.Equals(oldComittee))
            {
                DialogResult response = MessageBox.Show("Ви хочете зберегти дані перед закриттям?", "Підтвердження",
                                                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                if (response == DialogResult.Cancel)
                    e.Cancel = true;
                else if (response == DialogResult.Yes)
                    SavePTCStatement();
            }
        }
        /// <summary>
        /// открыть документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLastSaved_DoubleClick(object sender, EventArgs e)
        {
            string fileName = txtLastSaved.Text;
            if (System.IO.File.Exists(fileName))
            {
                System.Diagnostics.Process.Start(fileName);
            }
        }

        private void cbChief_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
