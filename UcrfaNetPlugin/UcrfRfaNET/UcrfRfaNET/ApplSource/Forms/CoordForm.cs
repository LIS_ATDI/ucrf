﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using IdwmNET;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class CoordForm : Form
    {

        public List<int> Mass_G { get; set; }

        /// <summary>
        /// Допоміжні структури даних
        /// </summary>
        public class CoordTable
        {
            public int Id { get; set; }
            public string Administration { get; set; }
            public string Technology { get; set; }
            public string Status { get; set; }
            public double FreqMin { get; set; }
            public double Freq { get; set; }
            public double FreqMax { get; set; }
            public double DistanceCoord { get; set; }
            public double DistanceFact { get; set; }
            public CoordTable() { Administration = ""; Technology = ""; Status = ""; }
        }

        public struct FreqId
        {
            public double Freq { get; set; }
            public int StaId { get; set; }
            public int Id { get; set; }
            public FreqId(double f, int staid, int id)
                : this()
            {
                Freq = f;
                StaId = staid;
                Id = id;
            }
        }

        /// <summary>
        /// приватні змінні
        /// </summary>
        private ForeighnCoord forCoord;
        private Grid gridEx;
        private Grid gridExtended;
        private int id;
        private Idwm idwmVal;        
        private string tableNameMobFreq = ICSMTbl.itblMobStaFreqs;
        private const int Distance = 500;
        private NearestCountryItem[] nearestCountry;
        private string technology;
        private string tableName;
        private List<CoordTable> coordTbl = new List<CoordTable>();
        private Dictionary<NearestCountryItem, int> finalListCountry;

        /// <summary>
        /// Конструктои
        /// </summary>
        public CoordForm(BaseStation station, string tblName, int idStat)
            : this()
        {
            PositionState pos = new PositionState();
            if (station != null)
            {
                if (tblName == ICSMTbl.MobStation2)
                    pos.LoadStatePosition(((StationBS)station).objPosition.Id, ((StationBS)station).objPosition.TableName);
                else if (tblName == ICSMTbl.itblMobStation)
                    pos.LoadStatePosition(((StationRR)station).objPosition.Id, ((StationRR)station).objPosition.TableName);
            }
            if (pos.Id != IM.NullI)
                Initializing(pos, null, tblName, idStat);

            Mass_G = new List<int>();
        }

        public CoordForm()
        {
            InitializeComponent();
            Mass_G = new List<int>();
        }

        public CoordForm(PositionState pos1, PositionState pos2, string tblName, int idStat)
            : this()
        {
            Initializing(pos1, pos2, tblName, idStat);
            Mass_G = new List<int>();
        }

        /// <summary>
        /// Ініціалізація для Станцій
        /// </summary>
        /// <param name="pos1"></param>
        /// <param name="pos2"></param>
        /// <param name="tblName"></param>
        /// <param name="idStat"></param>      
        private void Initializing(PositionState pos1, PositionState pos2, string tblName, int idStat)
        {                                    
            tableName = tblName;
            technology = GetTech(tblName);
            id = idStat;            
            InitParamFromBd(pos1, pos2);


            if (technology != "MICROWA")
            {
                grpBoxExtended.Visible = false;

                grpBox.Height = 490;
                grpBox.Width = 847;

                InitGridCoord();
                InitGridStructure();
            }

            if (technology == "MICROWA")
            {
                grpBoxExtended.Visible = true;
                InitGridCoord();
                InitGridStructure();

                InitGridExtendedCoord();
                InitGridExtendedStructure();
            }

        }

        /// <summary>
        /// Вертає назву технології по таблиці
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string GetTech(string name)
        {
            switch (name)
            {
                case ICSMTbl.itblMobStation2:
                    return "MOBSTATION2";
                case ICSMTbl.itblMobStation:
                    return "MOBSTATION";
                case ICSMTbl.itblMicrowa:
                    return "MICROWA";
                default:
                    return name;
            }
        }

        /// <summary>
        /// Зчитування даних з БД
        /// </summary>
        private void InitParamFromBd(PositionState pos, PositionState pos1)
        {
            //Обчислення списку країн
            idwmVal = new Idwm();
            bool isNotInit = !idwmVal.Init(11);
            if (isNotInit)
                return;
            nearestCountry = GetListOfCountries(pos);
            //Якщо вказано 2 позицію(для Microwa)
            if (pos1 != null)
                GetMinDistanceFor2Stat(pos1);
                         
            //Обчислення списку частот
            if (technology == "MOBSTATION2")
                tableNameMobFreq += "2";

            if (technology == "MICROWA")
                forCoord = new ForeighnCoordMicrowa(technology,tableName,tableNameMobFreq);
            else if (technology.Contains("MOBSTATION"))
                forCoord = new ForeignCoordMobStationX(technology, tableName, tableNameMobFreq);

            List<int> objIdList;
            List<FreqId> freqStatList;            
            objIdList = HelpFunction.GetListIdAppl(id);            
            freqStatList = forCoord.CalculateFreq4Station(objIdList);
            coordTbl= forCoord.CreateFirstListOfRecords(nearestCountry, freqStatList, id);
            List<CoordTable> tmpCoordList = new List<CoordTable>();
            forCoord.FormatFinishRecords(ref tmpCoordList, finalListCountry);            
            coordTbl = tmpCoordList;

            forCoord.ShowStatusStation();            
        }

        /// <summary>
        /// Обчислює мінімальну відстань до кордону для двох станцій одночасно
        /// </summary>
        /// <param name="pos1"></param>
        private void GetMinDistanceFor2Stat(PositionState pos1)
        {
            List<NearestCountryItem> nearCountry1 = nearestCountry.ToList();
            List<NearestCountryItem> nearCountry2 = GetListOfCountries(pos1).ToList();
            finalListCountry = new Dictionary<NearestCountryItem, int>();
            //Формування списка з мінімальними координац. відстанями для                         
            for (int i = 0; i < nearCountry1.Count; i++)
                for (int j = 0; j < nearCountry2.Count; j++)
                {
                    if (nearCountry1[i].country == nearCountry2[j].country)
                    {
                        if (nearCountry1[i].distance > nearCountry2[j].distance)
                            finalListCountry.Add(nearCountry2[j], 2);
                        else
                            finalListCountry.Add(nearCountry1[i], 1);
                    }
                }

            nearCountry1.RemoveAll(obj => (finalListCountry.Keys.ToList().Exists(fobj => (fobj.country == obj.country))));
            nearCountry2.RemoveAll(obj => (finalListCountry.Keys.ToList().Exists(fobj => (fobj.country == obj.country))));
            for (int i = 0; i < nearCountry1.Count; i++)
                finalListCountry.Add(nearCountry1[i], 1);
            for (int i = 0; i < nearCountry2.Count; i++)
                finalListCountry.Add(nearCountry2[i], 2);
        }

        /// <summary>
        /// Вертає список 4 країн до яких відстань від заданої позиції найменша(крім України)
        /// </summary>
        /// <param name="pos">позиція</param>
        /// <returns>список країн з додатковими параметрами</returns>
        private NearestCountryItem[] GetListOfCountries(PositionState pos)
        {
            NearestCountryItem[] nearCountry;
            float xDec = (float)pos.X;
            float yDec = (float)pos.Y;

            if (pos.Csys == "4DMS")
            {
                xDec = (float)IMPosition.Dms2Dec(pos.X);
                yDec = (float)IMPosition.Dms2Dec(pos.Y);
            }
            string[] exclude = { "UKR" };
            nearCountry = idwmVal.GetNearestCountries(Idwm.DecToRadian(xDec), Idwm.DecToRadian(yDec), Distance, exclude, 4);
            //перевірка якщо станція знаходиться не в Україні, то йде обчислення відстані 
            //до кордону з Україною і виведення відстані у від'ємному форматі
            if (nearCountry.ToList().Exists(obj => (obj.distance <= 0)))
            {
                exclude[0] = "";
                NearestCountryItem[] tmpCntries = idwmVal.GetNearestCountries(Idwm.DecToRadian(xDec), Idwm.DecToRadian(yDec), Distance, exclude, 4);
                NearestCountryItem tmpCntr = tmpCntries.ToList().Find(obj => (obj.country == "UKR"));
                float ukrDist;
                if (tmpCntr != null)
                {
                    ukrDist = tmpCntr.distance;
                    nearCountry.ToList().Find(obj => (obj.distance <= 0)).distance = -ukrDist;
                }
            }
            return nearCountry;
        }
     
        /// <summary>
        /// Формування структури гріда
        /// </summary>
        private void InitGridStructure()
        {            
            Cell cAdm = null;
            Cell cFreq = null;
            Cell cDFact = null;
            Cell cDCoord = null;
            Cell cStatus = null;
            for (int i = -1; i < coordTbl.Count; i++)
            {

                if (i == -1)
                {
                    Row r = gridEx.AddRow();
                    cAdm = FillCell(r, "Адміністрація");
                    cFreq = FillCell(r, "Частота");
                    cDFact = FillCell(r, "Відстань фактична");
                    cDCoord = FillCell(r, "Відстань координаційна");
                    cStatus = FillCell(r, "Статус");
                }
                else
                {
                    Row r = gridEx.AddRow();
                    cAdm = FillCell(r, coordTbl[i].Administration);
                    cFreq = FillCell(r, (coordTbl[i].Freq / 1000000).Round(3).ToStringNullD()+" MHz ");
                    cDFact = FillCell(r, coordTbl[i].DistanceFact.Round(3).ToStringNullD());
                    cDCoord = FillCell(r, coordTbl[i].DistanceCoord.Round(3).ToStringNullD());
                    if (string.IsNullOrEmpty(coordTbl[i].Status))
                        coordTbl[i].Status = string.Empty;
                    cStatus = FillCell(r, coordTbl[i].Status);
                    cStatus.Key = "Status" + i;
                    cStatus.cellStyle = EditStyle.esEllipsis;
                    cStatus.AddOnPressButtonHandler(this, "OnPressEquipment");
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void CopyToFixMobNotice(int ID)
        {

            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadWrite);
            r.Select("ID,StationA.ID, StationB.ID, StationA.TX_FREQ,StationB.TX_FREQ , StationA.Position.NAME, StationB.Position.NAME , StationA.Position.X , StationB.Position.X, StationA.POWER, StationB.POWER,StationA.GAIN, StationB.GAIN, StationA.RADOM_ATTN, StationB.RADOM_ATTN ");
            r.Select("StationA.Position.Y , StationB.Position.Y , StationA.Position.CSYS, StationB.Position.CSYS , DESIG_EM, BW, StationA.Position.ASL , StationB.Position.ASL , StationA.POWER, StationB.POWER , StationA.GAIN , StationA.RADOM_ATTN, StationB.GAIN , StationB.RADOM_ATTN");
            r.Select("StationA.AZIMUTH , StationB.AZIMUTH , StationA.Ant1.H_BEAMWIDTH , StationB.Ant1.H_BEAMWIDTH , StationA.ANGLE_ELEV , StationB.ANGLE_ELEV , StationA.POLAR , StationB.POLAR , StationA.AGL1 , StationB.AGL1 , StationB.Position.NAME , StationA.Position.NAME ,  ");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {

                    DateTime D_INUSE = IM.NullT;
                    IMRecordset rs_Appl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
                    rs_Appl.Select("ID,OBJ_ID1, DOZV_DATE_TO");
                    rs_Appl.SetWhere("OBJ_ID1", IMRecordset.Operation.Eq, ID);
                    rs_Appl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "MICROWA");
                    rs_Appl.Open();
                    if (!rs_Appl.IsEOF())
                    {
                        D_INUSE = rs_Appl.GetT("DOZV_DATE_TO");
                    }
                    rs_Appl.Close();
                    rs_Appl.Destroy();


                    ///
                    // Save A
                                                       IMRecordset rsFixMobNotice_A = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadWrite);
                                                       rsFixMobNotice_A.Select("ID,ADM,FMN_NBR1,T_NOTICE_TYPE,T_D_ADM_NTC,T_FRAGMENT,T_PROV,T_IS_RESUB,T_ACTION,T_FREQ_ASSGN,TX_FREQ,T_D_INUSE,T_GEO_TYPE,NAME,COUNTRY_ID,X,Y,CSYS,T_STN_CLS,NAT_SRV,DESIG_EMISSION,BW,ASL,T_OP_HH_FR,T_OP_HH_TO,T_ADDR_CODE");
                                                       rsFixMobNotice_A.Open();
                                                       rsFixMobNotice_A.AddNew();
                                                       rsFixMobNotice_A.Put("ID", IM.AllocID(ICSMTbl.FixMobNotice, 1, -1));
                                                       rsFixMobNotice_A.Put("ADM","UKR");
                                                       rsFixMobNotice_A.Put("T_NOTICE_TYPE","T11");
                                                       rsFixMobNotice_A.Put("T_D_ADM_NTC", DateTime.Now);
                                                       DateTime D_ = DateTime.Now.AddMonths(2);
                                                       rsFixMobNotice_A.Put("T_D_INUSE", D_);                                                       
                                                       rsFixMobNotice_A.Put("T_FRAGMENT","NTFD_RR");                                                       
                                                       rsFixMobNotice_A.Put("T_PROV", "RR11.2");
                                                       rsFixMobNotice_A.Put("T_IS_RESUB",0);
                                                       rsFixMobNotice_A.Put("T_ACTION", "ADD");
                                                       rsFixMobNotice_A.Put("T_FREQ_ASSGN", r.Get("StationA.TX_FREQ"));
                                                       rsFixMobNotice_A.Put("TX_FREQ", r.Get("StationA.TX_FREQ"));
                                                       //rsFixMobNotice_A.Put("T_D_INUSE", D_INUSE);
                                                       rsFixMobNotice_A.Put("T_GEO_TYPE", "POINT");
                                                       rsFixMobNotice_A.Put("NAME", r.Get("StationA.Position.NAME"));
                                                       rsFixMobNotice_A.Put("COUNTRY_ID", "UKR");
                                                       rsFixMobNotice_A.Put("X", r.Get("StationA.Position.X"));
                                                       rsFixMobNotice_A.Put("Y", r.Get("StationA.Position.Y"));
                                                       rsFixMobNotice_A.Put("CSYS", r.Get("StationA.Position.CSYS"));
                                                       rsFixMobNotice_A.Put("T_STN_CLS", "FX");
                                                       rsFixMobNotice_A.Put("NAT_SRV", "CP");
                                                       rsFixMobNotice_A.Put("DESIG_EMISSION", r.Get("DESIG_EM"));
                                                       rsFixMobNotice_A.Put("BW", r.Get("BW"));
                                                       rsFixMobNotice_A.Put("ASL", r.Get("StationA.Position.ASL"));
                                                       rsFixMobNotice_A.Put("T_OP_HH_FR", 0.0);
                                                       rsFixMobNotice_A.Put("T_OP_HH_TO", 24.0);
                                                       rsFixMobNotice_A.Put("T_ADDR_CODE", "A");
                                                       rsFixMobNotice_A.Put("FMN_NBR1", r.Get("StationA.ID"));
                                                       rsFixMobNotice_A.Update();




                                                       IMRecordset rs_antenna_A = new IMRecordset(ICSMTbl.T1nAntennas, IMRecordset.Mode.ReadWrite );
                                                       rs_antenna_A.Select("ID,NOT_ID, T_PWR_XYZ, T_PWR_ANT, T_PWR_EIV, T_PWR_DBW, T_ANT_DIR, T_AZM_MAX_E, T_BMWDTH, T_GAIN_TYPE, T_GAIN_MAX, T_ELEV, T_POLAR, T_HGT_AGL ");
                                                       rs_antenna_A.Open();
                                                                                                              
                                                           rs_antenna_A.AddNew();
                                                           rs_antenna_A.Put("ID", IM.AllocID(ICSMTbl.T1nAntennas, 1, -1));



                                                           rs_antenna_A.Put("NOT_ID", rsFixMobNotice_A.GetI("ID"));
                                                           rs_antenna_A.Put("T_PWR_XYZ", "Y");
                                                           rs_antenna_A.Put("T_PWR_ANT", r.GetD("StationA.POWER") - 30);
                                                           rs_antenna_A.Put("T_PWR_EIV", "I");
                                                           rs_antenna_A.Put("T_PWR_DBW", r.GetD("StationA.POWER") + r.GetD("StationA.GAIN") - ( r.GetD("StationA.RADOM_ATTN")==IM.NullD ? 0 : r.GetD("StationA.RADOM_ATTN")) - 30);
                                                            rs_antenna_A.Put("T_ANT_DIR", "D");
                                                            rs_antenna_A.Put("T_AZM_MAX_E", r.Get("StationA.AZIMUTH"));
                                                            rs_antenna_A.Put("T_BMWDTH", r.Get("StationA.Ant1.H_BEAMWIDTH") );
                                                            rs_antenna_A.Put("T_GAIN_TYPE", "I");
                                                            rs_antenna_A.Put("T_GAIN_MAX", r.Get("StationA.GAIN"));
                                                            rs_antenna_A.Put("T_ELEV", r.Get("StationA.ANGLE_ELEV") );
                                                            rs_antenna_A.Put("T_POLAR", r.Get("StationA.POLAR"));
                                                            rs_antenna_A.Put("T_HGT_AGL", r.Get("StationA.AGL1"));
                                                            rs_antenna_A.Update();





                                                            IMRecordset rs_antenna_radius_A = new IMRecordset(ICSMTbl.T1nRxTxStations, IMRecordset.Mode.ReadWrite);
                                                            rs_antenna_radius_A.Select("ID,ANT_ID,NAME, COUNTRY_ID, X, Y,CSYS, T_GEO_TYPE, ROLE_TX, ROLE_RX ");
                                                           rs_antenna_radius_A.Open();
                                                           rs_antenna_radius_A.AddNew();
                                                           rs_antenna_radius_A.Put("ID", IM.AllocID(ICSMTbl.T1nRxTxStations, 1, -1));
                                                           rs_antenna_radius_A.Put("ANT_ID", rs_antenna_A.GetI("ID"));
                                                           rs_antenna_radius_A.Put("NAME",  r.Get("StationB.Position.NAME"));
                                                           rs_antenna_radius_A.Put("COUNTRY_ID","UKR");
                                                           rs_antenna_radius_A.Put("X", r.Get("StationB.Position.X"));
                                                           rs_antenna_radius_A.Put("Y", r.Get("StationB.Position.Y"));
                                                           rs_antenna_radius_A.Put("CSYS", r.Get("StationB.Position.CSYS") );
                                                           rs_antenna_radius_A.Put("T_GEO_TYPE", "POINT");
                                                           rs_antenna_radius_A.Put("ROLE_TX", "A");
                                                           rs_antenna_radius_A.Put("ROLE_RX", "B");
                                                           rs_antenna_radius_A.Update();




                    rs_antenna_radius_A.Close();
                    rs_antenna_radius_A.Dispose();

                   
                    rs_antenna_A.Close();
                    rs_antenna_A.Dispose();

                    rsFixMobNotice_A.Close();
                    rsFixMobNotice_A.Dispose();





                    // Save B


                 
                     
                                                       IMRecordset rsFixMobNotice_B = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadWrite);
                                                       rsFixMobNotice_B.Select("ID,ADM,FMN_NBR1,T_NOTICE_TYPE,T_D_ADM_NTC,T_FRAGMENT,T_PROV,T_IS_RESUB,T_ACTION,T_FREQ_ASSGN,TX_FREQ,T_D_INUSE,T_GEO_TYPE,NAME,COUNTRY_ID,X,Y,CSYS,T_STN_CLS,NAT_SRV,DESIG_EMISSION,BW,ASL,T_OP_HH_FR,T_OP_HH_TO,T_ADDR_CODE,T_D_INUSE");
                                                       rsFixMobNotice_B.Open();
                                                       rsFixMobNotice_B.AddNew();
                                                       rsFixMobNotice_B.Put("ID", IM.AllocID(ICSMTbl.FixMobNotice, 1, -1));
                                                       rsFixMobNotice_B.Put("ADM","UKR");
                                                       rsFixMobNotice_B.Put("T_NOTICE_TYPE","T11");
                                                       rsFixMobNotice_B.Put("T_D_ADM_NTC", DateTime.Now);
                                                       D_ = DateTime.Now.AddMonths(2);
                                                       rsFixMobNotice_B.Put("T_D_INUSE", D_);                                                       
                                                       rsFixMobNotice_B.Put("T_FRAGMENT","NTFD_RR");                                                       
                                                       rsFixMobNotice_B.Put("T_PROV", "RR11.2");
                                                       rsFixMobNotice_B.Put("T_IS_RESUB",0);
                                                       rsFixMobNotice_B.Put("T_ACTION", "ADD");
                                                       rsFixMobNotice_B.Put("T_FREQ_ASSGN", r.Get("StationB.TX_FREQ"));
                                                       rsFixMobNotice_B.Put("TX_FREQ", r.Get("StationB.TX_FREQ"));
                                                       //rsFixMobNotice_B.Put("T_D_INUSE", D_INUSE);
                                                       rsFixMobNotice_B.Put("T_GEO_TYPE", "POINT");
                                                       rsFixMobNotice_B.Put("NAME", r.Get("StationB.Position.NAME"));
                                                       rsFixMobNotice_B.Put("COUNTRY_ID", "UKR");
                                                       rsFixMobNotice_B.Put("X", r.Get("StationB.Position.X"));
                                                       rsFixMobNotice_B.Put("Y", r.Get("StationB.Position.Y"));
                                                       rsFixMobNotice_B.Put("CSYS", r.Get("StationB.Position.CSYS"));
                                                       rsFixMobNotice_B.Put("T_STN_CLS", "FX");
                                                       rsFixMobNotice_B.Put("NAT_SRV", "CP");
                                                       rsFixMobNotice_B.Put("DESIG_EMISSION", r.Get("DESIG_EM"));
                                                       rsFixMobNotice_B.Put("BW", r.Get("BW"));
                                                       rsFixMobNotice_B.Put("ASL", r.Get("StationB.Position.ASL"));
                                                       rsFixMobNotice_B.Put("T_OP_HH_FR", 0.0);
                                                       rsFixMobNotice_B.Put("T_OP_HH_TO", 24.0);
                                                       rsFixMobNotice_B.Put("T_ADDR_CODE", "A");
                                                       rsFixMobNotice_B.Put("FMN_NBR1", r.Get("StationB.ID"));
                                                       rsFixMobNotice_B.Update();


                                                       IMRecordset rs_antenna_B = new IMRecordset(ICSMTbl.T1nAntennas, IMRecordset.Mode.ReadWrite);
                                                       rs_antenna_B.Select("ID,NOT_ID, T_PWR_XYZ, T_PWR_ANT, T_PWR_EIV, T_PWR_DBW, T_ANT_DIR, T_AZM_MAX_E, T_BMWDTH, T_GAIN_TYPE, T_GAIN_MAX, T_ELEV, T_POLAR, T_HGT_AGL ");
                                                       rs_antenna_B.Open();
                                                                                                              
                                                           rs_antenna_B.AddNew();
                                                           rs_antenna_B.Put("ID", IM.AllocID(ICSMTbl.T1nAntennas, 1, -1));
                                                           rs_antenna_B.Put("NOT_ID", rsFixMobNotice_B.GetI("ID"));
                                                           rs_antenna_B.Put("T_PWR_XYZ", "Y");
                                                           rs_antenna_B.Put("T_PWR_ANT", r.GetD("StationB.POWER") - 30);
                                                           rs_antenna_B.Put("T_PWR_EIV", "I");
                                                           rs_antenna_B.Put("T_PWR_DBW", r.GetD("StationB.POWER") + r.GetD("StationB.GAIN") - ( r.GetD("StationB.RADOM_ATTN")==IM.NullD ? 0 : r.GetD("StationB.RADOM_ATTN")) - 30);
                                                            rs_antenna_B.Put("T_ANT_DIR", "D");
                                                            rs_antenna_B.Put("T_AZM_MAX_E", r.Get("StationB.AZIMUTH"));
                                                            rs_antenna_B.Put("T_BMWDTH", r.Get("StationB.Ant1.H_BEAMWIDTH") );
                                                            rs_antenna_B.Put("T_GAIN_TYPE", "I");
                                                            rs_antenna_B.Put("T_GAIN_MAX", r.Get("StationB.GAIN"));
                                                            rs_antenna_B.Put("T_ELEV", r.Get("StationB.ANGLE_ELEV") );
                                                            rs_antenna_B.Put("T_POLAR", r.Get("StationB.POLAR"));
                                                            rs_antenna_B.Put("T_HGT_AGL", r.Get("StationB.AGL1"));
                                                            rs_antenna_B.Update();
                                                         




                                                           IMRecordset rs_antenna_radius_B = new IMRecordset(ICSMTbl.T1nRxTxStations, IMRecordset.Mode.ReadWrite);
                                                           rs_antenna_radius_B.Select("ID,ANT_ID,NAME, COUNTRY_ID, X, Y,CSYS, T_GEO_TYPE, ROLE_TX, ROLE_RX ");
                                                           rs_antenna_radius_B.Open();
                                                           rs_antenna_radius_B.AddNew();
                                                           rs_antenna_radius_B.Put("ID", IM.AllocID(ICSMTbl.T1nRxTxStations, 1, -1));
                                                           rs_antenna_radius_B.Put("ANT_ID", rs_antenna_B.GetI("ID"));
                                                           rs_antenna_radius_B.Put("NAME", r.Get("StationA.Position.NAME"));
                                                           rs_antenna_radius_B.Put("COUNTRY_ID", "UKR");
                                                           rs_antenna_radius_B.Put("X", r.Get("StationA.Position.X"));
                                                           rs_antenna_radius_B.Put("Y", r.Get("StationA.Position.Y"));
                                                           rs_antenna_radius_B.Put("CSYS", r.Get("StationA.Position.CSYS"));
                                                           rs_antenna_radius_B.Put("T_GEO_TYPE", "POINT");
                                                           rs_antenna_radius_B.Put("ROLE_TX", "B");
                                                           rs_antenna_radius_B.Put("ROLE_RX", "A");
                                                           rs_antenna_radius_B.Update();
                
    
                    rs_antenna_radius_B.Close();
                    rs_antenna_radius_B.Dispose();

                   
                    rs_antenna_B.Close();
                    rs_antenna_B.Dispose();

                    rsFixMobNotice_B.Close();
                    rsFixMobNotice_B.Dispose();

                }

            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private bool isAdded(int ID)
        {
            bool isCheck = false;
            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadOnly);
            r.Select("ID,StationA.ID, StationB.ID");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {

                    IMRecordset rsFixMobNotice_B = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadOnly);
                    rsFixMobNotice_B.Select("ID,PUB_NO,ADM,T_NOTICE_TYPE,T_D_ADM_NTC,T_FRAGMENT,T_PROV,T_IS_RESUB,T_ACTION,T_FREQ_ASSGN,TX_FREQ,T_D_INUSE,T_GEO_TYPE,NAME,COUNTRY_ID,X,Y,CSYS,T_STN_CLS,NAT_SRV,DESIG_EMISSION,BW,ASL,T_OP_HH_FR,T_OP_HH_TO,T_ADDR_CODE,FMN_NBR1");
                    rsFixMobNotice_B.SetAdditional(string.Format("[FMN_NBR1]={0} OR [FMN_NBR1]={1}", r.GetI("StationA.ID"), r.GetI("StationB.ID")));
                    rsFixMobNotice_B.Open();
                    if (!rsFixMobNotice_B.IsEOF())
                    {
                        isCheck = true;  
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return isCheck;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private string isNotification(int ID)
        {
            string Notif = "";
            IMRecordset r = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadOnly);
            r.Select("ID,StationA.ID, StationB.ID");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {

                    IMRecordset rsFixMobNotice_A = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadOnly);
                    rsFixMobNotice_A.Select("ID,PUB_NO,ADM,T_NOTICE_TYPE,T_D_ADM_NTC,T_FRAGMENT,T_PROV,T_IS_RESUB,T_ACTION,T_FREQ_ASSGN,TX_FREQ,T_D_INUSE,T_GEO_TYPE,NAME,COUNTRY_ID,X,Y,CSYS,T_STN_CLS,NAT_SRV,DESIG_EMISSION,BW,ASL,T_OP_HH_FR,T_OP_HH_TO,T_ADDR_CODE,FMN_NBR1");
                    rsFixMobNotice_A.SetAdditional(string.Format("[FMN_NBR1]={0}", r.GetI("StationA.ID")));
                    rsFixMobNotice_A.Open();
                    if (!rsFixMobNotice_A.IsEOF())
                    {
                        if (rsFixMobNotice_A.GetS("PUB_NO").Trim() != "")
                            Notif = rsFixMobNotice_A.GetS("PUB_NO");
                    }
                    rsFixMobNotice_A.Close();
                    rsFixMobNotice_A.Destroy();

                    IMRecordset rsFixMobNotice_B = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadOnly);
                    rsFixMobNotice_B.Select("ID,PUB_NO,ADM,T_NOTICE_TYPE,T_D_ADM_NTC,T_FRAGMENT,T_PROV,T_IS_RESUB,T_ACTION,T_FREQ_ASSGN,TX_FREQ,T_D_INUSE,T_GEO_TYPE,NAME,COUNTRY_ID,X,Y,CSYS,T_STN_CLS,NAT_SRV,DESIG_EMISSION,BW,ASL,T_OP_HH_FR,T_OP_HH_TO,T_ADDR_CODE,FMN_NBR1");
                    rsFixMobNotice_B.SetAdditional(string.Format("[FMN_NBR1]={0}", r.GetI("StationB.ID")));
                    //rsFixMobNotice_B.SetAdditional(string.Format("[FMN_NBR1]={0} OR [FMN_NBR1]={1}", r.GetI("StationA.ID"), r.GetI("StationB.ID")));
                    rsFixMobNotice_B.Open();
                    if (!rsFixMobNotice_B.IsEOF())
                    {
                        if (rsFixMobNotice_B.GetS("PUB_NO").Trim()!="")
                            Notif+=", "+ rsFixMobNotice_B.GetS("PUB_NO");
                    }
                    rsFixMobNotice_B.Close();
                    rsFixMobNotice_B.Destroy();
                }
            }
            catch (Exception ex)
            {

            }
            return Notif;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitGridExtendedStructure()
        {
            Cell cFreq = null;
            Cell cDFact = null;
            Cell cDCoord = null;
            Cell cStatus = null;

            List<CoordTable> LTbl = new List<CoordTable>();
            Dictionary<double, string> Freq_Lst = new Dictionary<double, string>();
            Dictionary<double, string> Width_Lst = new Dictionary<double, string>();
            Dictionary<double, double> Distance = new Dictionary<double, double>();

            List<double> Lst_Freq = new List<double>();


            for (int i = 0; i < coordTbl.Count; i++)
            {
                if (!Freq_Lst.ContainsKey(coordTbl[i].Freq))
                    Freq_Lst.Add(coordTbl[i].Freq, coordTbl[i].Administration);

                if (!Distance.ContainsKey(coordTbl[i].Freq))
                    Distance.Add(coordTbl[i].Freq, coordTbl[i].DistanceFact);

                if (!Lst_Freq.Contains(coordTbl[i].Freq))
                    Lst_Freq.Add(coordTbl[i].Freq);



            }

            foreach (double it in Lst_Freq)
            {

                List<double> Lst_w = new List<double>();
                foreach (KeyValuePair<double, double> item in Distance)
                {
                    List<CoordTable> cb = coordTbl.FindAll(r => r.Freq == item.Key);
                    if (cb != null)
                    {
                        foreach (CoordTable t in cb)
                        {
                            if (t.Freq == it)
                            {
                                if (!Lst_w.Contains(t.DistanceFact))
                                    Lst_w.Add(t.DistanceFact);
                            }
                        }
                    }
                 
                }
                if (Lst_w.Count > 0)
                {
                    Lst_w.Sort();
                    double tx = Lst_w[0];

                     CoordTable cb = coordTbl.Find(r => r.Freq == it && r.DistanceFact == tx);
                     if (cb != null)
                     {
                         LTbl.Add(cb);
                     }
                   
                }
            }


           

            for (int i = -1; i < LTbl.Count; i++)
            {

                if (i == -1)
                {
                    Row r = gridExtended.AddRow();
                    cFreq = FillCell(r, "Частота");
                    cDFact = FillCell(r, "Мінімальна відстань");
                    cDCoord = FillCell(r, "Відстань координаційна");
                    cStatus = FillCell(r, "Статус");
                }
                else
                {
                    Row r = gridExtended.AddRow();
                    cFreq = FillCell(r, (LTbl[i].Freq / 1000000).Round(3).ToStringNullD() + " MHz ");
                    cDFact = FillCell(r, LTbl[i].DistanceFact.Round(3).ToStringNullD());
                    cDCoord = FillCell(r, LTbl[i].DistanceCoord.Round(3).ToStringNullD());
                    if (string.IsNullOrEmpty(LTbl[i].Status))
                        LTbl[i].Status = string.Empty;
                   // cStatus = FillCell(r, coordTbl[i].Status);



                    if (isNotification(LTbl[i].Id) != "") 
                    {
                        cStatus = FillCell(r, "Нотифіковано: " + isNotification(LTbl[i].Id)); cStatus.cellStyle = EditStyle.esSimple; 
                    }
                    else if (isAdded(LTbl[i].Id))
                    {
                        cStatus = FillCell(r, "Додано до таблиці"); cStatus.cellStyle = EditStyle.esSimple;
                    }
                    else 
                    {
                        cStatus = FillCell(r, "Додати"); cStatus.cellStyle = EditStyle.esEllipsis;  //cStatus.ButtonText = "Додати";
                    }
                    cStatus.Key = "Status" + i;



                    //cStatus.cellStyle  = EditStyle.esButton;
                    cStatus.HorizontalAlignment = HorizontalAlignment.Center;
                    cStatus.AddOnPressButtonHandler(this, "OnPressChangeStatus");
                }
            }
        }

        /// <summary>
        /// Реакція на зміну статуса
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressEquipment(ref Cell cell)
        {
            int row = Convert.ToInt32(cell.Key.Replace("Status", string.Empty));
            if (string.IsNullOrEmpty(cell.Value))
                cell.Value = "A";
            else if (cell.Value == "A")
                cell.Value = "CN";
            else if (cell.Value == "CN")
                cell.Value = "A";
            coordTbl[row].Status = cell.Value;
        }


        public void OnPressChangeStatus(ref Cell cell)
        {
            int row = Convert.ToInt32(cell.Key.Replace("Status", string.Empty));
            if (Mass_G == null) Mass_G = new List<int>();
            if (!Mass_G.Contains(coordTbl[row].Id))
            {
               Mass_G.Add(coordTbl[row].Id);
            }

            if (cell.Value == "Додано до таблиці")
            {
                cell.Value = "Додати";
                Mass_G.Remove(coordTbl[row].Id);
            }
            else if (cell.Value == "Додати")
            {
                cell.Value = "Додано до таблиці";
            }
            
        }

        /// <summary>
        /// Заповнення комірки з даним іменем
        /// </summary>
        /// <param name="r"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private Cell FillCell(Row r, string name)
        {
            Cell cTmp = r.AddCell(name);
            cTmp.TextHeight = 12;
            cTmp.Key = name;
            cTmp.CanEdit = false;
            return cTmp;
        }

        /// <summary>
        /// Початкова ініціалізація Гріда
        /// </summary>
        private void InitGridCoord()
        {
            gridEx = new Grid();
            grpBox.Controls.Add(gridEx);
            gridEx.AllowDrop = true;
            gridEx.BackColor = SystemColors.Window;
            gridEx.EditMode = true;
            gridEx.FirstColumnWidth = 225;
            gridEx.GridLine = Grid.GridLineStyle.Both;
            gridEx.GridLineColor = Color.Black;
            gridEx.GridOpacity = 100;
            gridEx.Image = null;
            gridEx.ImageStyle = Grid.ImageStyleType.Stretch;
            gridEx.Location = new Point(2, 3);
            gridEx.LockUpdates = false;
            gridEx.Name = "gridEx";
            gridEx.OtherColumnsWidth = 230;
            gridEx.SelectionMode = Grid.SelectionModeType.CellSelect;
            gridEx.Size = new Size(267, 133);
            gridEx.TabIndex = 0;
            gridEx.ToolTips = true;
            gridEx.Parent = grpBox;
            ResizedGrid();
        }

        private void InitGridExtendedCoord()
        {
            gridExtended = new Grid();
            grpBoxExtended.Controls.Add(gridExtended);
            gridExtended.AllowDrop = true;
            gridExtended.BackColor = SystemColors.Window;
            gridExtended.EditMode = true;
            gridExtended.FirstColumnWidth = 225;
            gridExtended.GridLine = Grid.GridLineStyle.Both;
            gridExtended.GridLineColor = Color.Black;
            gridExtended.GridOpacity = 100;
            gridExtended.Image = null;
            gridExtended.ImageStyle = Grid.ImageStyleType.Stretch;
            gridExtended.Location = new Point(2, 3);
            gridExtended.LockUpdates = false;
            gridExtended.Name = "gridExtended";
            gridExtended.OtherColumnsWidth = 230;
            gridExtended.SelectionMode = Grid.SelectionModeType.CellSelect;
            gridExtended.Size = new Size(267, 133);
            gridExtended.TabIndex = 0;
            gridExtended.ToolTips = true;
            gridExtended.Parent = grpBoxExtended;
            ResizedGridExtended();
        }

        private void grpBox_SizeChanged(object sender, EventArgs e)
        {
            ResizedGrid();
        }

        private void ResizedGrid()
        {
            btnAccept.Focus();
            if (gridEx != null && gridEx.Parent != null)
            {
                gridEx.Top = 20;
                gridEx.Left = 7;
                gridEx.Width = gridEx.Parent.Width - gridEx.Left * 2 + 1;
                gridEx.Height = gridEx.Parent.Height - (gridEx.Top + gridEx.Left);
                gridEx.Invalidate();
            }
        }

        private void ResizedGridExtended()
        {
            btnAccept.Focus();
            if (gridExtended != null && gridExtended.Parent != null)
            {
                gridExtended.Top = 20;
                gridExtended.Left = 7;
                gridExtended.Width = gridExtended.Parent.Width - gridExtended.Left * 2 + 1;
                gridExtended.Height = gridExtended.Parent.Height - (gridExtended.Top + gridEx.Left);
                gridExtended.Invalidate();
            }
        }

        /// <summary>
        /// Реакція на кнопку Прийняти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (Mass_G != null)
            {
                foreach (int item in Mass_G)
                    CopyToFixMobNotice(item);
            }
            forCoord.AcceptStation();
            Mass_G.Clear();
            Close();
        }
            
        /// <summary>
        /// Закрити форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
