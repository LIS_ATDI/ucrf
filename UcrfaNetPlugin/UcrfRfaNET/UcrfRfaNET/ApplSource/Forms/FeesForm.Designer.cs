﻿namespace XICSM.UcrfRfaNET.ApplSource
{
    partial class FeesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblRadiotechnology = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbMinFrequency = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbMaxFrequency = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbMinPower = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMaxPower = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFee = new System.Windows.Forms.TextBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.cbRadioCommunication = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbRadiotechnology = new System.Windows.Forms.TextBox();
            this.btnChoose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(289, 259);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(370, 259);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "Так";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblRadiotechnology
            // 
            this.lblRadiotechnology.AutoSize = true;
            this.lblRadiotechnology.Location = new System.Drawing.Point(12, 14);
            this.lblRadiotechnology.Name = "lblRadiotechnology";
            this.lblRadiotechnology.Size = new System.Drawing.Size(95, 13);
            this.lblRadiotechnology.TabIndex = 9;
            this.lblRadiotechnology.Text = "Вид радіозв\'язку:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Мінімальна частота, MГц:";
            // 
            // tbMinFrequency
            // 
            this.tbMinFrequency.Location = new System.Drawing.Point(336, 74);
            this.tbMinFrequency.Name = "tbMinFrequency";
            this.tbMinFrequency.Size = new System.Drawing.Size(109, 20);
            this.tbMinFrequency.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Максимальна частота. МГц:";
            // 
            // tbMaxFrequency
            // 
            this.tbMaxFrequency.Location = new System.Drawing.Point(336, 100);
            this.tbMaxFrequency.Name = "tbMaxFrequency";
            this.tbMaxFrequency.Size = new System.Drawing.Size(110, 20);
            this.tbMaxFrequency.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Мінімальна потужність, Вт:";
            // 
            // tbMinPower
            // 
            this.tbMinPower.Location = new System.Drawing.Point(336, 126);
            this.tbMinPower.Name = "tbMinPower";
            this.tbMinPower.Size = new System.Drawing.Size(110, 20);
            this.tbMinPower.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Максимальна потужність, Вт:";
            // 
            // tbMaxPower
            // 
            this.tbMaxPower.Location = new System.Drawing.Point(336, 155);
            this.tbMaxPower.Name = "tbMaxPower";
            this.tbMaxPower.Size = new System.Drawing.Size(110, 20);
            this.tbMaxPower.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Ставка збору:";
            // 
            // tbFee
            // 
            this.tbFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbFee.Location = new System.Drawing.Point(336, 214);
            this.tbFee.Name = "tbFee";
            this.tbFee.Size = new System.Drawing.Size(109, 20);
            this.tbFee.TabIndex = 19;
            // 
            // lblHint
            // 
            this.lblHint.AutoSize = true;
            this.lblHint.Location = new System.Drawing.Point(12, 184);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(410, 13);
            this.lblHint.TabIndex = 20;
            this.lblHint.Text = "Залиште обидві потужності в 0, якщо ставка збору не залежить від потужності ";
            // 
            // cbRadioCommunication
            // 
            this.cbRadioCommunication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRadioCommunication.FormattingEnabled = true;
            this.cbRadioCommunication.Location = new System.Drawing.Point(118, 11);
            this.cbRadioCommunication.Name = "cbRadioCommunication";
            this.cbRadioCommunication.Size = new System.Drawing.Size(328, 21);
            this.cbRadioCommunication.TabIndex = 21;
            this.cbRadioCommunication.SelectedIndexChanged += new System.EventHandler(this.cbRadioCommunication_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Радіотехнології:";
            // 
            // tbRadiotechnology
            // 
            this.tbRadiotechnology.Location = new System.Drawing.Point(118, 44);
            this.tbRadiotechnology.Name = "tbRadiotechnology";
            this.tbRadiotechnology.ReadOnly = true;
            this.tbRadiotechnology.Size = new System.Drawing.Size(246, 20);
            this.tbRadiotechnology.TabIndex = 23;
            // 
            // btnChoose
            // 
            this.btnChoose.Location = new System.Drawing.Point(371, 42);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(75, 23);
            this.btnChoose.TabIndex = 24;
            this.btnChoose.Text = "Вибрати...";
            this.btnChoose.UseVisualStyleBackColor = true;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // FeesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 321);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.tbRadiotechnology);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbRadioCommunication);
            this.Controls.Add(this.lblHint);
            this.Controls.Add(this.tbFee);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbMaxPower);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbMinPower);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbMaxFrequency);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbMinFrequency);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblRadiotechnology);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FeesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ставки збору";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblRadiotechnology;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbMinFrequency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbMaxFrequency;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMinPower;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMaxPower;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFee;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.ComboBox cbRadioCommunication;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbRadiotechnology;
        private System.Windows.Forms.Button btnChoose;
    }
}