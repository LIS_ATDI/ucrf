﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Classes;
using XICSM.UcrfRfaNET.UtilityClass;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource
{
    // Скопированро с DiffReport2Object

    class DiffReport3Object : NotifyPropertyChanged
    {
        private string _fileLink;
        private DateTime _timeFrom;        
        private DateTime _timeTo;
        private List<int> _measureEquipmentList = new List<int>();
        private List<int> _auxiliaryEquipmentList = new List<int>();
        private List<int> _noteList = new List<int>();

        /// <summary>
        /// Область, де розміщена станція - радіорелейні лінки зазвичай 
        /// завжди в межах одної області.
        /// </summary>
        public string Province { get { return GetProvince(); } }

        /// <summary>
        /// ID запису Diff
        /// </summary>
        public int DiffId { get { return DiffObject.Station.Id; } }
        /// <summary>
        /// Таблица запису Diff
        /// </summary>
        public string TableName { get { return DiffObject.DiffTableName; } }

        /// <summary>
        /// Список пользовотелей которые утверждают
        /// </summary>
        public ComboBoxDictionaryList<int, string> ViseUserLst = new ComboBoxDictionaryList<int, string>();
        /// <summary>
        /// Список пользователей исполнителей
        /// </summary>
        public ComboBoxDictionaryList<int, string> ExecutorUserLst = new ComboBoxDictionaryList<int, string>();


        /// <summary>
        /// Час вимірювання "від"
        /// </summary>
        public DateTime TimeFrom
        {
            get { return _timeFrom; }
            set
            {
                if (value != _timeFrom)
                {
                    _timeFrom = value;
                    InvokeNotifyPropertyChanged("TimeFrom");
                }
            }
        }
        /// <summary>
        /// Час вимірювання "до"
        /// </summary>
        public DateTime TimeTo
        {
            get { return _timeTo; }
            set
            {
                if (value != _timeTo)
                {
                    _timeTo = value;
                    InvokeNotifyPropertyChanged("TimeTo");
                }
            }
        }

        /// <summary>
        /// Лінк на файл протоколу
        /// </summary>
        public string FileLink
        {
            get { return _fileLink; }
            set
            {
                if (value != _fileLink)
                {
                    _fileLink = value;
                    InvokeNotifyPropertyChanged("FileLink");
                }
            }
        }

        /// <summary>
        /// Сертифікат (свідоцтво про атестацію)
        /// </summary>
        public int CertificateId { get; set; }

        /// <summary>
        /// ID того, что затверджує
        /// </summary>
        public int ApprovePersonId { get; set; }

        /// <summary>
        /// ID виконавця
        /// </summary>
        public int PerformPersonId { get; set; }

        public List<int> MeasureEquipmentID { get { return _measureEquipmentList; } }
        public List<int> AuxiliaryEquipmentID { get { return _auxiliaryEquipmentList; } }
        public List<int> NoteID { get { return _noteList; } }

        /// <summary>
        /// Обьект станции
        /// </summary>
        protected BaseDiffClass DiffObject = null;   // Обьект, который управляет заявкой
        //=======================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="diffObject">Объект Diff станции</param>
        public DiffReport3Object(BaseDiffClass diffObject)
        {
            DiffObject = diffObject;
           
            TimeFrom = DateTime.Now;
            TimeTo = DateTime.Now;
            
            ApprovePersonId = IM.NullI;
            PerformPersonId = IM.NullI;
            FileLink = "";

            UpdateViseUser();
            //Загрузка
            UpdateExecutorUser();
            
            Load();
        }

        private string GetProvince()
        {
            try
            {
                return DiffObject.Station.Position.Province;
            }
            catch (Exception)
            {
                return "";
            }            
        }

        //=======================================
        /// <summary>
        /// Сохраняет данные в таблицу
        /// </summary>
        public void Save()
        {
            IMRecordset rsStationDiff = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            rsStationDiff.Select("ID,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
            rsStationDiff.SetWhere("ID", IMRecordset.Operation.Eq, DiffId);
            try
            {
                rsStationDiff.Open();
                if (!rsStationDiff.IsEOF())
                {
                    rsStationDiff.Edit();
                    rsStationDiff.Put("MSREQ_CERT_ID", CertificateId);
                    rsStationDiff.Put("MEASURE_TIME_FROM3", TimeFrom);
                    rsStationDiff.Put("MEASURE_TIME_TO3", TimeTo);
                    rsStationDiff.Put("MEASURE_APPROVED_BY", ApprovePersonId);
                    rsStationDiff.Put("MEASURED_BY", PerformPersonId);
                    rsStationDiff.Put("LINK_IE3", FileLink);
                    rsStationDiff.Update();
                }

                WriteMeasureEquipment();
                WriteAuxiliaryEquipment();
                WriteNotes();
            }
            finally
            {
                if (rsStationDiff.IsOpen())
                    rsStationDiff.Close();
                rsStationDiff.Destroy();
            }
        }



        /// <summary>
        /// Печатаем документ
        /// </summary>        
        /// <returns>Удалось ли напечатать?(TRUE - если да)</returns>
        public bool Print()
        {
            string regionCode = HelpFunction.getAreaCode(DiffObject.Station.Position.Province, DiffObject.Station.Position.City);
            string packetIdStr = DiffObject.PacketID.ToString("D5");
            string applIdStr = DiffObject.ApplID.ToString("D4");

            string fileName = string.Format("{0}\\{1}", PluginSetting.PluginFolderSetting.FolderAktPtk, string.Format("{0}-{1}-{2}-ПВП", regionCode, applIdStr, packetIdStr));
            string rtfFileName = string.Format("{0}.{1}", fileName, "rtf");
            string docFileName = string.Format("{0}.{1}", fileName, "docx");
            if (System.IO.File.Exists(docFileName))
            {
                string message = string.Format("Файл '{0}' існує. Ви дійсно бажаєте перезаписати його?", docFileName);
                if (MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return false;
            }
            PrintDocs printDocs = new PrintDocs();

            if (printDocs.CreateOneReport(DocType.PARAM_REZ, DiffObject.ApplType, TimeTo, IM.NullT, PlugTbl.itblXnrfaAppl, DiffObject.ApplID, "", rtfFileName, true, docFileName))
            {
                FunctionConvertToDOCX doc = new FunctionConvertToDOCX();
                doc.ConvertDOCToDOCX(docFileName);
                FileLink = docFileName;
                Save();
                OpenLink();
                return true;
            }
            return false;
        }

        public void Load()
        {
            IMRecordset rsStationDiff = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            rsStationDiff.Select("ID,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
            rsStationDiff.SetWhere("ID", IMRecordset.Operation.Eq, DiffId);
            try
            {
                rsStationDiff.Open();
                if (!rsStationDiff.IsEOF())
                {
                    CertificateId = rsStationDiff.GetI("MSREQ_CERT_ID");                    
                    TimeFrom = rsStationDiff.GetT("MEASURE_TIME_FROM3");
                    TimeTo = rsStationDiff.GetT("MEASURE_TIME_TO3");

                    if (TimeFrom == IM.NullT)
                        TimeFrom = DateTime.Now;

                    if (TimeTo == IM.NullT)
                        TimeTo = DateTime.Now;

                    ApprovePersonId = rsStationDiff.GetI("MEASURE_APPROVED_BY");
                    PerformPersonId = rsStationDiff.GetI("MEASURED_BY");
                    FileLink = rsStationDiff.GetS("LINK_IE3");

                    ReadMeasureEquipment();
                    ReadAuxiliaryEquipment();
                    ReadNotes();
                }
            }
            finally
            {
                if (rsStationDiff.IsOpen())
                    rsStationDiff.Close();
                rsStationDiff.Destroy();
            }                        
        }

        /// <summary>
        /// Открываем документ
        /// </summary>
        public void OpenLink()
        {
            if (string.IsNullOrEmpty(FileLink) || System.IO.File.Exists(FileLink) == false)
            {
                MessageBox.Show(string.Format("Can't open file '{0}'", FileLink));
                return;
            }
            System.Diagnostics.Process.Start(FileLink);
        }

        private void SetWhere(IMRecordset r)
        {
            r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, TableName);

            switch (TableName)
            {
                case PlugTbl.itblXnrfaDiffMobSta2:
                    r.SetWhere("MOBSTA2_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDiffMobSta:
                    r.SetWhere("MOBSTA_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffMwSta:
                    r.SetWhere("MW_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffEtSta:
                    r.SetWhere("ET_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffTvSta:
                    r.SetWhere("TV_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffFmSta:
                    r.SetWhere("FM_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDiffFmDigital:
                    r.SetWhere("TDAB_ID", IMRecordset.Operation.Eq, DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffDvbSta:
                    r.SetWhere("TDVB_ID", IMRecordset.Operation.Eq, DiffId);
                    break;
            }
        }

        private void CreateNew(IMRecordset r)
        {
            r.Put("OBJ_TABLE", TableName);

            switch (TableName)
            {
                case PlugTbl.itblXnrfaDiffMobSta2:
                    r.Put("MOBSTA2_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDiffMobSta:
                    r.Put("MOBSTA_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffMwSta:
                    r.Put("MW_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffEtSta:
                    r.Put("ET_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffTvSta:
                    r.Put("TV_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffFmSta:
                    r.Put("FM_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDiffFmDigital:
                    r.Put("TDAB_ID", DiffId);
                    break;

                case PlugTbl.itblXnrfaDeiffDvbSta:
                    r.Put("TDVB_ID", DiffId);
                    break;
            }            
        }

        public void ReadMeasureEquipment()
        {
            _measureEquipmentList.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaMeasureEquipmentList, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,MEASURE_EQ_ID");
                SetWhere(r);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    _measureEquipmentList.Add(r.GetI("MEASURE_EQ_ID"));
                }
            }
            finally
            {    
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }            
        }

        public void WriteMeasureEquipment()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaMeasureEquipmentList, IMRecordset.Mode.ReadWrite);            
            r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,MEASURE_EQ_ID");
            SetWhere(r);
            
            r.Open();
            bool isAdd = false;
            foreach (int val in _measureEquipmentList)
            {
                if (r.IsEOF() || (isAdd == true))
                {
                    isAdd = true;
                    
                    r.AddNew();
                    r.Put("ID", IM.AllocID(PlugTbl.itblXnrfaMeasureEquipmentList, 1, -1));

                    CreateNew(r);                    
                }
                else
                {
                    r.Edit();
                }
                r.Put("MEASURE_EQ_ID", val);
                
                r.Update();
                r.MoveNext();
            }

            if (isAdd == false)
                while (!r.IsEOF())
                {
                    r.Delete();
                    r.MoveNext();
                }

            r.Close();
            r.Destroy();            
        }

        private void ReadAuxiliaryEquipment()
        {
            _auxiliaryEquipmentList.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAuxiliaryEquipmentList, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,AUXILIARY_EQ_ID");
                SetWhere(r);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    _auxiliaryEquipmentList.Add(r.GetI("AUXILIARY_EQ_ID"));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        private void WriteAuxiliaryEquipment()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAuxiliaryEquipmentList, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,AUXILIARY_EQ_ID");
                SetWhere(r);

                r.Open();
                bool isAdd = false;
                foreach (int val in _auxiliaryEquipmentList)
                {
                    if (r.IsEOF() || (isAdd == true))
                    {
                        isAdd = true;

                        r.AddNew();
                        r.Put("ID", IM.AllocID(PlugTbl.itblXnrfaAuxiliaryEquipmentList, 1, -1));

                        CreateNew(r);
                    }
                    else
                    {
                        r.Edit();
                    }
                    r.Put("AUXILIARY_EQ_ID", val);

                    r.Update();
                    r.MoveNext();
                }

                if (isAdd == false)
                    while (!r.IsEOF())
                    {
                        r.Delete();
                        r.MoveNext();
                    }
            }
            finally
            {
                r.Close();
                r.Destroy();                
            }                        
        }

        private void ReadNotes()
        {
            _noteList.Clear();

            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaParamRezNote, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,SC_ID");
                SetWhere(r);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    _noteList.Add(r.GetI("SC_ID"));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        private void WriteNotes()
        {
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaParamRezNote, IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("ID,MOBSTA2_ID,MOBSTA_ID,MW_ID,ET_ID,TV_ID,FM_ID,TDVB_ID,TDAB_ID,OBJ_TABLE,SC_ID");
                SetWhere(r);

                r.Open();
                bool isAdd = false;
                foreach (int val in _noteList)
                {
                    if (r.IsEOF() || (isAdd == true))
                    {
                        isAdd = true;

                        r.AddNew();
                        r.Put("ID", IM.AllocID(PlugTbl.itblXnrfaParamRezNote, 1, -1));

                        CreateNew(r);
                    }
                    else
                    {
                        r.Edit();
                    }
                    r.Put("SC_ID", val);

                    r.Update();
                    r.MoveNext();
                }

                if (isAdd == false)
                    while (!r.IsEOF())
                    {
                        r.Delete();
                        r.MoveNext();
                    }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// Обновляет пользователей-начальников
        /// </summary>
        private void UpdateViseUser()
        {
            ViseUserLst.Clear();
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                {
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                }
                else if (CUsers.CurDepartment == UcrfDepartment.URZP)
                {
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.VRZ.ToString());
                }
                else
                {
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.FILIA.ToString());
                }

                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    int id = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");
                    string employee = lastName + " " + firstName;
                    ViseUserLst.Add(new ComboBoxDictionary<int, string>(id, employee));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Обновляет пользователей-исполнителей
        /// </summary>
        private void UpdateExecutorUser()
        {
            ExecutorUserLst.Clear();
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                {
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                }
                else if (CUsers.CurDepartment == UcrfDepartment.URZP)
                {
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.VRZ.ToString());
                }
                else
                {
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.FILIA.ToString());
                }
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    int id = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");
                    string employee = lastName + " " + firstName;
                    ExecutorUserLst.Add(new ComboBoxDictionary<int, string>(id, employee));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
