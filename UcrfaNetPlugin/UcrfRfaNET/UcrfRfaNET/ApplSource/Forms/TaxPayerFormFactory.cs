﻿using System;
using XICSM.UcrfRfaNET.Reports;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Типы отчетов в налоговую
    /// </summary>
    enum TaxPayerFormType
    {
        Type1,
        Type2,
        Type3,
    }
    /// <summary>
    /// фабрика создания классов для генерации отчетов в налоговую
    /// </summary>
    class TaxPayerFormFactory
    {
        public static ITaxPayerForm CreateTaxPayerForm(TaxPayerReportParams _params, string tableName, TaxPayerFormType type)
        {
            switch(type)
            {
                case TaxPayerFormType.Type1:
                    return new TaxPayerForm1();
                case TaxPayerFormType.Type2:
                    return new TaxPayerForm2(_params, tableName);
                case TaxPayerFormType.Type3:
                    return new TaxPayerForm3(_params);
                default:
                    throw new Exception("Unknown Tax Payer Form Type: " + type);
            }
        }
    }
}
