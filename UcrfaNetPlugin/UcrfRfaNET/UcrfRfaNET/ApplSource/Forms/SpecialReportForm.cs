﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class SpecialReportForm : Form
    {
        public DateTime StartDate { get; set;}
        public DateTime DueDate { get; set; }

        private SpecialReportProducer report = new SpecialReportProducer();

        public SpecialReportForm(int id)
        {
            report.Load(id);
            StartDate = report.StartDate;
            DueDate = report.DueDate;
            InitializeComponent();

            btnCreateReport.Enabled = false;
            btnPrint.Enabled = true;

            startDatePicker.Enabled = false;
            endDatePicker.Enabled = false;
        }

        public SpecialReportForm()
        {
            StartDate = DateTime.Now - new TimeSpan(7,0,0,0);
            DueDate = DateTime.Now;

            InitializeComponent();  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                report.CreateReport(StartDate, DueDate);
                btnPrint.Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SpecialReportForm_Load(object sender, EventArgs e)
        {
            startDatePicker.DataBindings.Add("Value", this, "StartDate");
            endDatePicker.DataBindings.Add("Value", this, "DueDate");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //SpecialReportProducer report = new SpecialReportProducer();
            report.Show();
        }

    }
}
