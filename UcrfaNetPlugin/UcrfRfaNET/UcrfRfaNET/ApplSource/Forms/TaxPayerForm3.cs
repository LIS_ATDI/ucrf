﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSM;
using XICSM.UcrfRfaNET.Extension;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Reports;
using System.Windows;
using System.Globalization;
using System.Linq;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    class TaxPayerForm3 : ITaxPayerForm
    {
        private const string FormType = "Форма 3";
        Dictionary<string, string> GlobalArea = new Dictionary<string, string>();
        /// <summary>
        /// Данные для записи
        /// </summary>
        class DataItem
        {
            public string Owner_Name { get; set; }
            public string Owner_Code { get; set; }
            public string Owner_Address { get; set; }
            public string Owner_Province { get; set; }
            public string Lic_Num { get; set; }
            public string Lic_Date_From { get; set; }
            public string Lic_Date_To { get; set; }
            public string Doc_Num { get; set; }
            public DateTime Doc_Date_From { get; set; }
            public DateTime Doc_Date_To { get; set; }
            public string Power { get; set; }
            public string Address { get; set; }
            public string OrderCode { get; set; }
            public string Fees_Txt2 { get; set; }
            public string Fees_Txt { get; set; }
            public string Province { get; set; }
            public string Province_id { get; set; }
            public double Bw { get; set; }
            public DateTime Doc_Date_Cancel { get; set; }
            public DateTime Doc_Date_Stop { get; set; }
            public string Frequency { get; set; }
            public string Channel { get; set; }
           
            
            /// <summary>
            /// Конструктор
            /// </summary>
            public DataItem()
            {
                Owner_Name = "";
                Owner_Code = "";
                Owner_Address = "";
                Owner_Province = "";
                Doc_Num = "";
                Lic_Num = "";
                Lic_Date_From = "";
                Lic_Date_To = "";
                Doc_Date_From = IM.NullT;
                Doc_Date_To = IM.NullT;
                Doc_Date_Cancel = IM.NullT;
                Doc_Date_Stop = IM.NullT;
                Address = "";
                Province = "";
                Fees_Txt2 = "";
                Bw = IM.NullD;
                //Power = IM.NullD;
                Power = "";
                OrderCode = "";
                Fees_Txt = "";
                Frequency = "";
                Channel = "";
                Province_id ="";
                                
            }
        }
        /// <summary>
        /// Класс работы с данными
        /// </summary>
        class WriteData
        {
            private Dictionary<string, DataItem> _data;

            /// <summary>
            /// Конструктор
            /// </summary>
            public WriteData()
            {
                _data = new Dictionary<string, DataItem>();
            }
            /// <summary>
            /// Добавить запись
            /// </summary>
            public string[] Add(Dictionary<string, string> radioSystem,
                            int recId,
                            string Owner_Name, 
                            string Owner_Code, 
                            string Owner_Address,
                            string Owner_Province, 
                            string Lic_Num, 
                            string Lic_Date_From, 
                            string Doc_Num, 
                            DateTime Doc_Date_From, 
                            DateTime Doc_Date_To, 
                            string Power, 
                            string Address,
                            string Fees_Txt,
                            string Fees_Txt2, 
                            string Province, 
                            double Bw,
                            string Tx_Freq, 
                            DateTime Doc_Date_Cancel,
                            DateTime Doc_Date_Stop,
                            string Frequency,
                            string Channel,
                            string Prov_ID
                ) 
               {
                List<string> errorList = new List<string>();

               
                if (Doc_Date_From == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата початку дозволу: ID запису = {0}", recId, FormType));
                if (Doc_Date_To == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата закінчення дозволу: ID запису = {0}", recId, FormType));
                if (string.IsNullOrEmpty(Fees_Txt))
                    errorList.Add(string.Format("{1}: Недостатньо інформації для визначення позиції Тарифів: ID запису = {0}", recId, FormType));
                else if (radioSystem.ContainsKey(Fees_Txt) == false)
                    errorList.Add(string.Format("{1}: Відсутній опис для коду позиції Тарифів '{0}': ID запису = {2}", Fees_Txt, FormType, recId));
                if (string.IsNullOrEmpty(Province))
                    errorList.Add(string.Format("{1}: Відсутній регіон використання: ID запису = {0}", recId, FormType));
               // if (Power == IM.NullD)
               //     errorList.Add(string.Format("{1}: Відсутня потужність: ID запису = {0}", recId, FormType));
               // if (Bw == IM.NullD)
               //     errorList.Add(string.Format("{1}: Відсутня загальна ширина смуги радіочастот: ID запису = {0}", recId, FormType));
               // if (Bw == IM.NullD)
               //     errorList.Add(string.Format("{1}: Відсутня частота: ID запису:{0}", recId, FormType));

                if (errorList.Count > 0)
                    return errorList.ToArray();

                string key = Owner_Name + "_" + Province + "_" + Doc_Num + "_" + Tx_Freq + "_" + Fees_Txt + "_" + Bw.ToStringNullD();
                if (_data.ContainsKey(key) == false)
                {
    
                    DataItem di = new DataItem();
                    _data.Add(key, di);
                    di.Owner_Name = Owner_Name;
                    di.Owner_Code = Owner_Code;
                    di.Owner_Address = Owner_Address;
                    di.Owner_Province = Owner_Province;
                    di.Lic_Num = Lic_Num;
                    di.Lic_Date_From = Lic_Date_From;
                    di.Doc_Num = Doc_Num;
                    di.Doc_Date_From = Doc_Date_From;
                    di.Doc_Date_To = Doc_Date_To;
                    di.Power = Power;
                    di.Address = Address;
                    di.Fees_Txt = Fees_Txt;
                    di.Frequency = Frequency;
                    di.Channel = Channel;
                    di.Province_id = Prov_ID;
                    
                        if (radioSystem.ContainsKey(Fees_Txt))
                        {
                            di.OrderCode = radioSystem[di.Fees_Txt];

                            try
                            {
                                if ((!string.IsNullOrEmpty(di.OrderCode) && di.OrderCode.IndexOf("(") > 0))
                                {
                                    di.OrderCode = di.OrderCode.Substring(0, di.OrderCode.IndexOf("(") - 1);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message + "|");
                            }

                        }
                    
                    di.Fees_Txt2  = Fees_Txt2;
                    di.Province = Province;
                    di.Bw = Bw;
                    di.Doc_Date_Cancel = Doc_Date_Cancel;
                    di.Doc_Date_Stop = Doc_Date_Stop;
                    
                }
                return errorList.ToArray();
            }
            /// <summary>
            /// Сохранить в файл
            /// </summary>
            /// <param name="path"></param>
            /// <param name="province"></param>
            /// <param name="repType"></param>
            public void Save(string path, string province, ReportType repType, DateTime dateStan, DateTime dateBeg, DateTime dateEnd)
            {
                if (_data.Count == 0)
                    return;


                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Запис в файл")))
                {
                    pb.SetBig("Запис в файл...");
                    using (IReport rep = Report.CreateReport(repType))
                    {
                        //string DefaultFontName = "Times New Roman";
                        double[] ListHeadColumnWidth = {5.57,36.29,9,26,7.14,9.57,13,9.86,9.71,6.71,38.86,17.43,6.71,13.86,5.14,8.71,5.14 };
                        string[] ListHead = {"№ з/п",
                                            "Найменування користувача РЧР/ ПІБ для користувача РЧР фізичної особи",
                                            "Код ЄДРПОУ /реєстраційний номер облікової картки платника податків  1 ",
                                            "Місцезнаходження користувача - юридичної особи/ фізичної особи ",
                                            "Ліцензія на мовлення",
                                            "Номер",
                                            "дата видачі дд.мм.рррр",
                                            "Дозвіл на експлуатацію РЕЗ мовлення",
                                            "Номер",
                                            "дата видачі дд.мм.рррр",
                                            "термін дії до    дд.мм.рррр",
                                            "Потужність , кВт",
                                            "адреса місця встановлення РЕЗ мовлення",
                                            "Вид радіозв`язку",
                                            "Номер позиції пункту 254.4 Податкового кодексу України, якою встановлено ставки рентної плати за користування РЧР",
                                            "Назва регіонів користу-вання РЧР",
                                            "Загальна ширина смуги радіочастот, зазначена в дозволі (МГц)",
                                            "Дата припинення користування РЧР 2  дд.мм.рррр",
                                            "Частота",
                                            "Канал",
                                            "Примітки"};
                        rep.Init(Path.Combine(path, string.Format("форма3{0}{1}", string.IsNullOrEmpty(province) ? "" : "-", province)), "Page1","");
                        List<string> line = new List<string>();


                        // Производим объединение ячеек M1:Q1
                        //rep.UnionCell(12, 0, 16, 0, rep.GetCellStyleDefault(), "Додаток - ф.3");
                        rep.UnionCell(15, 0, 15, 0, rep.GetCellStyleDefault(), "Форма 3");
                        //rep.SetRowHeight(13,1,24.75);
                        //for (int i = 0; i <= 4;i++ ) rep.SetBorderStyle(13+i, 1, 1);

                        // Производим объединение ячеек A2:Q2
                        rep.UnionCell(0, 1, 18, 1, rep.GetCellStyleBackgroundDefaultColor(), "Перелік користувачів радіочастотного ресурсу - платників рентної плати за користування радіочастотним ресурсом України (РЧР), \t\n" +
                                  //"які мають дозволи на експлуатацію радіоелектронного засобу (РЕЗ) мовлення, видані відповідно до ліцензій на мовлення, станом на " + dateStan.ToStringNullT("dd.MM.yyyy"));
                                  "які мають дозволи на експлуатацію радіоелектронного засобу (РЕЗ) мовлення, видані відповідно до ліцензій на мовлення, станом на ______________");
                        rep.UnionCell(0, 2, 0, 3, rep.GetCellStyleHead(), ListHead[0]); rep.SetCellStyle(0, 3, rep.GetCellStyleHead());
                        rep.UnionCell(1, 2, 1, 3, rep.GetCellStyleHead(), ListHead[1]);
                        rep.UnionCell(2, 2, 2, 3, rep.GetCellStyleHead(), ListHead[2]);
                        rep.UnionCell(3, 2, 3, 3, rep.GetCellStyleHead(), ListHead[3]);
                        rep.UnionCell(4, 2, 5, 2, rep.GetCellStyleHead(), ListHead[4]); rep.UnionCell(4, 3, 4, 3, rep.GetCellStyleHead(), ListHead[5]); rep.UnionCell(5, 3, 5, 3, rep.GetCellStyleHead(), ListHead[6]);

                        rep.UnionCell(6, 2, 10, 2, rep.GetCellStyleHead(), ListHead[7]); rep.UnionCell(6, 3, 6, 3, rep.GetCellStyleHead(), ListHead[8]); rep.UnionCell(7, 3, 7, 3, rep.GetCellStyleHead(), ListHead[9]); rep.UnionCell(8, 3, 8, 3, rep.GetCellStyleHead(), ListHead[10]); rep.UnionCell(9, 3, 9, 3, rep.GetCellStyleHead(), ListHead[11]); rep.UnionCell(10, 3, 10, 3, rep.GetCellStyleHead(), ListHead[12]);                      
                        rep.UnionCell(11, 2, 11, 3, rep.GetCellStyleHead(), ListHead[13]);
                        rep.UnionCell(12, 2, 12, 3, rep.GetCellStyleHead(), ListHead[14]);
                        rep.UnionCell(13, 2, 13, 3, rep.GetCellStyleHead(), ListHead[15]);
                        rep.UnionCell(14, 2, 14, 3, rep.GetCellStyleHead(), ListHead[16]);
                        rep.UnionCell(15, 2, 15, 3, rep.GetCellStyleHead(), ListHead[17]);

                        rep.UnionCell(16, 2, 16, 3, rep.GetCellStyleHead(), ListHead[18]);
                        rep.UnionCell(17, 2, 17, 3, rep.GetCellStyleHead(), ListHead[19]);

                        rep.UnionCell(18, 2, 18, 3, rep.GetCellStyleHead(), ListHead[20]);

                        //rep.UnionCell(16, 2, 16, 3, rep.GetCellStyleBorder(), ListHead[18]);

                        for (int i = 0; i < ListHeadColumnWidth.Length; i++)
                        {
                            rep.SetColumnWidth(i, (int)ListHeadColumnWidth[i]);

                        }


                        Dictionary<string, string> fprov = new Dictionary<string, string>();
                        
                        IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                        rsArea.OrderBy("CODE", OrderDirection.Ascending);
                        rsArea.Select("NAME,CODE");
                        try
                        {
                            for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                                fprov.Add(rsArea.GetS("CODE"),rsArea.GetS("NAME"));
                        }
                        finally
                        {
                            rsArea.Close();
                            rsArea.Destroy();
                        }

                        int curr_num = rep.GetIndexRow() + 4;

                        rep.SetIndexRow(curr_num);


                        int counter = 1;

                        foreach (KeyValuePair<string, string> item_prov in fprov)
                        {
                            List<DataItem> owner_name = new List<DataItem>();
                             bool status = false;
                            
                            foreach (KeyValuePair<string, DataItem> item in _data)
                            {
                                DataItem di = item.Value;
                                if (di.Owner_Province.Equals(item_prov.Value.Trim())) { status = true; owner_name.Add(di); }
                            }

                            owner_name.Sort(delegate(DataItem us1, DataItem us2)
                            { return us1.Owner_Name.CompareTo(us2.Owner_Name); });


                            var out_list = from u in owner_name
                                              orderby u.Owner_Name,u.OrderCode,u.Province_id,u.Frequency,u.Channel 
                                              select u;

                           
                                if (status)
                                {
                                    string temp_item_prov ="";


                                    if ((item_prov.Value.ToString() != "Київ") && (item_prov.Value.ToString() != "Севастополь") && (item_prov.Value.ToString() != "АР Крим"))
                                    {
                                        //temp_item_prov+=item_prov.Value.ToString() + " обл.";
                                        temp_item_prov+=item_prov.Value.ToString() + " область";
                                    }
                                    else if (item_prov.Value.ToString() == "АР Крим")
                                    {
                                        temp_item_prov+=item_prov.Value.ToString();
                                    }
                                    else
                                    {
                                        temp_item_prov+="м. " + item_prov.Value.ToString();
                                    }


                                    curr_num = rep.GetIndexRow();
                                    rep.UnionCell(0, curr_num, 18, curr_num, rep.GetCellStyleBackgroundColor(), temp_item_prov);
                                    curr_num = rep.GetIndexRow() + 1;
                                    rep.SetIndexRow(curr_num);
                                    status = false;
                                }

                            
                            
                            foreach (DataItem di in out_list)
                            {
                             
                                pb.SetSmall(counter, _data.Count);
                                line.Clear();
                                if (di.Owner_Province.Equals(item_prov.Value.Trim()))
                                {
                                    
                                    string tem_=""; 
                                    string date_from = di.Lic_Date_From.Trim();
                                    string[] strarray = date_from.Split(';');
                                    if (strarray!=null)
                                    {
                                        for (int i = 0; i < strarray.Length; i++)
                                        {
                                            if (!string.IsNullOrEmpty(strarray[i].Trim()))
                                            {
                                                DateTime temp = IM.NullT;
                                                temp = Convert.ToDateTime(strarray[i].Trim());
                                                tem_ += temp.ToStringNullT("dd.MM.yyyy") + ";" + Environment.NewLine;
                                            }

                                        }
                                    }

                                    if (!string.IsNullOrEmpty(tem_))
                                    {
                                        tem_ = tem_.Trim().Remove(tem_.Trim().Length - 1);
                                    }

                                    
                                    double out_fees = 0;
                                    if (Double.TryParse(di.Fees_Txt2, out out_fees))
                                    {
                                        out_fees = Math.Floor(out_fees);
                                    }
                                    else
                                    {
                                        if (Double.TryParse(di.Fees_Txt2.Replace(".", ","), out out_fees))
                                        {
                                            out_fees = Math.Floor(out_fees);
                                        }
                                        else
                                        {
                                            if (Double.TryParse(di.Fees_Txt2.Replace(",", "."), out out_fees))
                                            {
                                                out_fees = Math.Floor(out_fees);
                                            }
                                            else
                                            {
                                                MessageBox.Show("Error convert Fees_Txt to double");
                                                break;
                                            }
                                        }
                                        
                                    }

                                    string Lic_Num = di.Lic_Num;
                                    if (Lic_Num.IndexOf(';')>-1)
                                    {
                                        if (Lic_Num[Lic_Num.IndexOf(';') - 1] == ' ')
                                        {
                                            Lic_Num = Lic_Num.Remove(Lic_Num.IndexOf(';') - 1, 1);
                                        }
                                    }

                                    
                                  
                                                                       
                                    line.Add(string.Format("{0}", counter++));
                                    line.Add(di.Owner_Name);
                                    line.Add(di.Owner_Code);
                                    line.Add(di.Owner_Address);
                                    //line.Add(di.Lic_Num);
                                    line.Add(Lic_Num);
                                    //line.Add(di.Lic_Date_From);
                                    line.Add(tem_);
                                    line.Add(di.Doc_Num);
                                    line.Add(di.Doc_Date_From.ToStringNullT("dd.MM.yyyy"));
                                    line.Add(di.Doc_Date_To.ToStringNullT("dd.MM.yyyy"));
                                    line.Add(di.Power.ToString());
                                    line.Add(di.Address);
                                    line.Add(di.OrderCode);
                                  //  line.Add(!string.IsNullOrEmpty(di.Fees_Txt2) ? Math.Truncate(Convert.ToDouble(di.Fees_Txt2.Replace(".", ","))).ToString() : "");
                                    //line.Add(di.Fees_Txt2);
                                    line.Add(out_fees.ToString());

                                    if ((di.Province != "Київ") && (di.Province != "Севастополь") && (di.Province != "АР Крим"))
                                    {
                                        line.Add(di.Province + " обл.");
                                    }
                                    else if (di.Province == "АР Крим")
                                    {
                                        line.Add(di.Province);
                                    }
                                    else
                                    {
                                        line.Add("м. " + di.Province);
                                    }
                                    line.Add(di.Bw.Round(5).ToStringNullD());

                                    if (!string.IsNullOrEmpty(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy")) && !string.IsNullOrEmpty(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy")))
                                    {
                                        bool n1=false;bool n2=false;
                                        if ((di.Doc_Date_Stop >= dateBeg) && (di.Doc_Date_Stop <= dateEnd))
                                        {
                                            n1=true;
                                        }
                                        if ((di.Doc_Date_Cancel >= dateBeg) && (di.Doc_Date_Cancel <= dateEnd))
                                        {
                                            n2=true;
                                        }


                                        if ((n1 == true) && (n2 == true))
                                        {
                                            line.Add((di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy") + ";" + Environment.NewLine + di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy")));
                                        }
                                        else if ((n1 == true) && (n2 == false))
                                        {
                                            line.Add(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy"));
                                        }
                                        else if ((n1 == false) && (n2 == true))
                                        {
                                            line.Add(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy"));
                                        }
                                        else
                                        {
                                            line.Add("");
                                        }
                                       

                                    }
                                    if (!string.IsNullOrEmpty(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy")) && string.IsNullOrEmpty(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy")))
                                    {
                                        if ((di.Doc_Date_Stop >= dateBeg) && (di.Doc_Date_Stop <= dateEnd))
                                        {
                                            line.Add(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy"));
                                        }
                                        else
                                        {
                                            line.Add("");
                                        }
                                    }
                                    if (string.IsNullOrEmpty(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy")) && !string.IsNullOrEmpty(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy")))
                                    {
                                        if ((di.Doc_Date_Cancel >= dateBeg) && (di.Doc_Date_Cancel <= dateEnd))
                                        {
                                            line.Add(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy"));
                                        }
                                        else
                                        {
                                            line.Add("");
                                        }

                                    }
                                    if (string.IsNullOrEmpty(di.Doc_Date_Stop.ToStringNullT("dd.MM.yyyy")) && string.IsNullOrEmpty(di.Doc_Date_Cancel.ToStringNullT("dd.MM.yyyy")))
                                    {
                                        line.Add("");
                                    }
                                    line.Add(di.Frequency);
                                    line.Add(di.Channel);
                                    line.Add("");
                                    rep.WriteLine(line.ToArray());
                                
                                    
                                }
                                    
                                

                                if (pb.UserCanceled())
                                    break;
                            }

                                //rep.SetBorderStyle(1, 1, 17, rep.GetIndexRow()-1,1);

                                owner_name.Clear();
                                //out_list.Clear();
                                out_list = null;
                        }
                        if (!pb.UserCanceled())
                            rep.Save();
                    }
                }
            }
        }
        public DateTime _dateBeg;
        public DateTime _dateEnd;
        public DateTime _dateStan;
        private string _path;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="date">Дата формирования</param>
        /// <param name="path">Путь к файлам</param>
        public TaxPayerForm3(TaxPayerReportParams _params)
        {
             GetCode();
            _dateBeg = _params.DateBeg;
            _dateEnd = _params.DateEnd;
            _dateStan = _params.DateStan;
            _path = _params.FilePath;
        }

         /// <summary>
        /// Возвращает значение поля CODE по значению области
        /// </summary>
        /// <param name="NameProv"></param>
        /// <returns></returns>
        public string FindCode(string NameProv)
        {
            string tmp = "";
            foreach (KeyValuePair<string, string> item in GlobalArea)
            {
                if (item.Value == NameProv)
                {
                    tmp = item.Key;
                }
            }
            return tmp;
        }

        /// <summary>
        /// Возвращает значение поля CODE по значению области
        /// </summary>
        /// <param name="NameProv"></param>
        /// <returns></returns>
        public void GetCode()
        {
            GlobalArea.Clear();
            IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            rsArea.OrderBy("CODE", OrderDirection.Ascending);
            rsArea.Select("NAME,CODE");
            try
            {
                
                for (rsArea.Open();!rsArea.IsEOF();rsArea.MoveNext())
                {
                    GlobalArea.Add(rsArea.GetS("CODE"), rsArea.GetS("NAME"));
                }
            }
            finally
            {
                rsArea.Close();
                rsArea.Destroy();
            }
            
        }

        public double GetCountFreqMultBW(string freq, double bw, string Standard)
        {

            double res = 0;
            int count_freq = 0;
            int count_digit = 0;


            if (Standard == "БНТ")
            {
                string bw_str = bw.ToString();
                bw_str = bw_str.Substring(bw_str.IndexOf(',') + 1);
                if (!string.IsNullOrEmpty(bw_str))
                {
                    count_digit = 0;
                    foreach (char ch in bw_str)
                    {
                        count_digit++;
                    }

                    if (count_digit == 0)
                    {
                        bw_str = bw_str.Substring(bw_str.IndexOf('.') + 1);
                        if (!string.IsNullOrEmpty(bw_str))
                        {
                            count_digit = 0;
                            foreach (char ch in bw_str)
                            {
                                count_digit++;
                            }
                        }

                    }
                }
                


                if (!string.IsNullOrEmpty(freq))
                {
                    count_freq = 1;
                    foreach (char ch in freq)
                    {
                        if (ch == ';')
                        {
                            count_freq++;
                        }
                    }

                }

                if (count_digit > 0)
                {
                    res = Math.Round(bw * count_freq, count_digit);
                }
                else
                {
                    res = bw * count_freq;
                }

            }
            else
            {
                res = (bw != 0 ? bw : 0);
            }
            return res;

        }

        /// <summary>
        /// Начать процесс генерации документов
        /// </summary>
        /// <returns>список ошибок</returns>
        public string[] Generate(ReportType repType, bool isOneFile)
        {
            string additional = "[STANDARD] in ('БНТ','АТМ','АЗМ','DVB-T','DVB-T2')";
            List<string> errorList = new List<string>();
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування переліку користувачів РЧР (Форма 3)")))
            {
                Dictionary<string, string> radioSystem = EriFiles.GetEriCodeAndDescr("Fees");
                pb.SetProgress(0, 50000);
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                

                string lastProvince = "";
                WriteData wd = new WriteData();

                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.XnrfaDocs, IMRecordset.Mode.ReadOnly))
                    {

                        rs.Select("ID");
                        rs.Select("OWNER_ID");
                        rs.Select("OWNER_NAME");
                        rs.Select("OWNER_CODE");
                        rs.Select("OWNER_ADDRESS");
                        rs.Select("OWNERPROVINCE");

                        rs.Select("Owner.ADDRESS");
                        rs.Select("Owner.PROVINCE");
                        rs.Select("Owner.SUBPROVINCE");
                        rs.Select("Owner.POSTCODE");
                        rs.Select("Owner.CITY");
                        rs.Select("Owner.City.CUST_TXT4");
                        

                        rs.Select("STANDARD");
                        rs.Select("DOC_NUM");
                        rs.Select("LICENCE_ID");
                        rs.Select("LIC_NUM");
                        rs.Select("TX_FREQ");
                        rs.Select("LIC_DATE_FROM");
                        rs.Select("LIC_DATE_TO");
                        rs.Select("DOC_DATE_FROM");
                        rs.Select("DOC_DATE_TO");
                        rs.Select("DOC_DATE_CANCEL");
                        rs.Select("DOC_DATE_STOP");
                        rs.Select("ADDRESS");
                        rs.Select("PROVINCE");
                        rs.Select("FEES_ID");
                        rs.Select("APPL_ID");
                        rs.Select("FEES_TXT");
                        rs.Select("BW");
                        rs.Select("POWER");
                        rs.Select("CREATED_BY");
                        rs.Select("CUSTOM_TXT1");
                        rs.Select("CUSTOM_TXT2");
                        rs.Select("CUSTOM_TXT3");
                        rs.Select("CUSTOM_DATE1");
                        rs.Select("CUSTOM_DATE2");
                        rs.Select("CUSTOM_DATE3");
                        rs.Select("LIC_COUNT");
                        rs.Select("CHANEL");
                        rs.SetWhere("DOC_DATE_FROM", IMRecordset.Operation.Lt, _dateEnd);
                        rs.SetWhere("DOC_DATE_TO", IMRecordset.Operation.Ge, _dateBeg);
                        rs.SetAdditional(additional);
                        rs.OrderBy("PROVINCE", OrderDirection.Ascending);

                            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                            {
                               
                                    if (pb.UserCanceled())
                                    {
                                        errorList.Add("Перервано користувачем.");
                                        break;
                                   }


                                   pb.Increment(true);
                                    int recId = rs.GetI("ID");

                                    DateTime cancelDate = rs.GetT("DOC_DATE_CANCEL");
                                    DateTime StopDate = rs.GetT("DOC_DATE_STOP");
                                    if (((cancelDate != IM.NullT) && (cancelDate < _dateBeg)) || ((StopDate != IM.NullT) && (StopDate < _dateBeg)))
                                    {
                                        continue;
                                    }

                                    string provinceOwner = rs.GetS("OWNERPROVINCE");
                                    if (string.IsNullOrEmpty(provinceOwner))
                                    {
                                        //нет обоасти
                                        errorList.Add(string.Format("{1}: Відсутня область власника: ID запису = {0}, ID власника = {2}", recId, FormType, rs.GetI("OWNER_ID")));
                                        provinceOwner = "(без регіону)";
                                    }

                                    if ((provinceOwner != lastProvince) && (isOneFile == false))
                                    {
                                        if (string.IsNullOrEmpty(lastProvince) == false)
                                        {
                                            pb.SetBig("Запис в файл...");
                                            wd.Save(_path, lastProvince, repType, _dateStan, _dateBeg, _dateEnd);
                                        }
                                        lastProvince = provinceOwner;
                                        wd = new WriteData();
                                        pb.SetBig(lastProvince);
                                    }

                                      try
                                        {


                                            // сборка данных Область, район, населенный пункт, адрес
                                            String Temp = "";

                                            if (rs.GetS("Owner.POSTCODE").Trim() != "")
                                            {
                                                Temp = rs.GetS("Owner.POSTCODE");
                                            }

                                            if ((rs.GetS("Owner.PROVINCE").Trim() != "") && (rs.GetS("Owner.PROVINCE").Trim() != "АР Крим") && (rs.GetS("Owner.PROVINCE").Trim() != "Севастополь") && (rs.GetS("Owner.PROVINCE").Trim() != "Київ"))
                                            {
                                                Temp = Temp + ", " + rs.GetS("Owner.PROVINCE") + " обл.";
                                            }
                                            //else if ((rs.GetS("Owner.PROVINCE").Trim() != "") && ((rs.GetS("Owner.PROVINCE").Trim() == "Київська") || (rs.GetS("Owner.PROVINCE").Trim() == "Севастополь") || (rs.GetS("Owner.PROVINCE").Trim() == "Київ") || (rs.GetS("Owner.PROVINCE").Trim() == "АР Крим")))
                                            //{
                                                
                                            //}
                                            else if ((rs.GetS("Owner.PROVINCE").Trim() != "") && (rs.GetS("Owner.PROVINCE").Trim() == "АР Крим"))
                                            {
                                                Temp = Temp + ", " + rs.GetS("Owner.PROVINCE");
                                            }
                                            if (rs.GetS("Owner.SUBPROVINCE").Trim() != "")
                                            {
                                                if (rs.GetS("Owner.PROVINCE").Trim() != "")
                                                {
                                                    Temp = Temp + ", " + rs.GetS("Owner.SUBPROVINCE") + " р-н";
                                                }
                                                if (rs.GetS("Owner.PROVINCE").Trim() == "")
                                                {
                                                    Temp = Temp + rs.GetS("Owner.SUBPROVINCE") + " р-н";
                                                }
                                            }
                                            if (rs.GetS("Owner.CITY").Trim() != "")
                                            {
                                                if ((rs.GetS("Owner.SUBPROVINCE").Trim() == "") && (Temp.Trim() != ""))
                                                {
                                                    Temp = Temp + ", м." + rs.GetS("Owner.CITY") + "";
                                                }
                                                if ((rs.GetS("Owner.SUBPROVINCE").Trim() == "") && (Temp.Trim() == ""))
                                                {
                                                    Temp = Temp + "м." + rs.GetS("Owner.CITY") + "";
                                                }

                                                if ((rs.GetS("Owner.SUBPROVINCE").Trim() != "") && (Temp.Trim() != ""))
                                                {
                                                    Temp = Temp + ", с." + rs.GetS("Owner.CITY") + "";
                                                }
                                                if ((rs.GetS("Owner.SUBPROVINCE").Trim() != "") && (Temp.Trim() == ""))
                                                {
                                                    Temp = Temp + ", с." + rs.GetS("Owner.CITY") + "";
                                                }
                                            }
                                            if (rs.GetS("Owner.ADDRESS").Trim() != "")
                                            {
                                                if (Temp.Trim() != "")
                                                {
                                                    Temp = Temp + ", " + rs.GetS("Owner.ADDRESS");
                                                }
                                                if (Temp.Trim() == "")
                                                {
                                                    Temp = rs.GetS("Owner.ADDRESS");
                                                }
                                            }


                                            CultureInfo cl = new CultureInfo("en-US", false);
                                            cl.NumberFormat.NumberDecimalDigits = 0;
                                            string uiSep = "";
                                            if ((rs.GetD("POWER").ToString().IndexOf('E') > 0) || (rs.GetD("POWER").ToString().IndexOf('e') > 0))
                                            {
                                                //cl.NumberFormat.NumberDecimalSeparator = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
                                                cl.NumberFormat.NumberDecimalDigits = Int32.Parse(rs.GetD("POWER").ToString()[rs.GetD("POWER").ToString().Length-1].ToString());
                                            }

                                            string[] error = wd.Add(radioSystem,
                                                recId,
                                                (rs.GetS("OWNER_NAME")!=null?rs.GetS("OWNER_NAME"):" "),
                                                (rs.GetS("OWNER_CODE")!= null? rs.GetS("OWNER_CODE") : " "),
                                                //(rs.GetS("OWNER_ADDRESS")!= null ? rs.GetS("OWNER_ADDRESS") : " "),
                                                Temp,
                                                (rs.GetS("OWNERPROVINCE")!= null? rs.GetS("OWNERPROVINCE") : " "),
                                                (rs.GetS("LIC_NUM")!= null? rs.GetS("LIC_NUM") : " "),
                                                (rs.GetS("LIC_DATE_FROM")!= null? rs.GetS("LIC_DATE_FROM") : " "),
                                                (rs.GetS("DOC_NUM")!= null? rs.GetS("DOC_NUM") : ""),
                                                rs.GetT("DOC_DATE_FROM"),
                                                rs.GetT("DOC_DATE_TO"),


                                                (rs.GetD("POWER") != 0 ? ((cl.NumberFormat.NumberDecimalDigits > 0) ? rs.GetD("POWER").ToString("F", cl).Replace(",", ".") : rs.GetD("POWER").ToString().Replace(",", ".")) : 0.ToString("F", cl)),
                                                (rs.GetS("ADDRESS")!= null? rs.GetS("ADDRESS") : " "),
                                                rs.GetS("FEES_TXT"),
                                                //rs.GetI("FEES_ID"),
                                                rs.GetS("FEES_TXT"),
                                                (rs.GetS("PROVINCE")!= null ? rs.GetS("PROVINCE"):" "),
                                                //(rs.GetD("BW") != 0 ? rs.GetD("BW") : 0),
                                                GetCountFreqMultBW(rs.GetS("TX_FREQ"), (rs.GetD("BW") != 0 ? rs.GetD("BW") : 0), rs.GetS("STANDARD")),
                                               (rs.GetS("TX_FREQ")!= null? rs.GetS("TX_FREQ") : " "),
                                                rs.GetT("DOC_DATE_CANCEL"),
                                                rs.GetT("DOC_DATE_STOP"),
                                                (rs.GetS("TX_FREQ")!= null? rs.GetS("TX_FREQ") : " "),
                                                (rs.GetS("CHANEL")!= null? rs.GetS("CHANEL") : " "),
                                                FindCode(rs.GetS("PROVINCE"))
                                                );


                                        
                                        errorList.AddRange(error);
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message);
                                        }
                            }

                        
                        if (!pb.UserCanceled())
                        {
                            if (string.IsNullOrEmpty(lastProvince) == false)
                                wd.Save(_path, lastProvince, repType, _dateStan, _dateBeg, _dateEnd);
                            else if (isOneFile)
                                wd.Save(_path, "", repType, _dateStan, _dateBeg, _dateEnd);
                        }
                    }
            }
            GlobalArea = null;
            return errorList.ToArray();
        }
    }
}
