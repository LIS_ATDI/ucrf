﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class MeasureEquipmentCertificateForm : Form
    {
        private MeasureEquipmentCertificate _certificate = new MeasureEquipmentCertificate();

        // Для Binding ComboBox
        private ComboBoxDictionaryList<string, string> _listProvince;
        /// <summary>
        /// Список провинцей
        /// </summary>
        public List<string> ProvinceList { get; set; }
        

        public MeasureEquipmentCertificateForm(string tableName, int id)
        {
            InitializeComponent();

            _certificate.TableName = tableName;
            _certificate.Id = id;
            _certificate.Load();

            if (_certificate.Date == IM.NullT)
                _certificate.Date = new DateTime(1900, 1, 1);

            txtSymbol.DataBindings.Add("Text", _certificate, "Serial", true);
            dtpDate.DataBindings.Add("Value", _certificate, "Date", true);
            txtCertifiedBy.DataBindings.Add("Text", _certificate, "CertifiedBy", true);

            ProvinceList = new List<string>();
            _listProvince = new ComboBoxDictionaryList<string, string>(); 
            //----            
        }

        public void SaveAppl()
        {
            if (_certificate.Date.Year < 1900)
                _certificate.Date = IM.NullT;

            _certificate.Save();
        }

        private void MeasureEquipmentCertificateForm_Load(object sender, EventArgs e)
        {
            // Загрузка списка областей
            HashSet<string> hashProvince = new HashSet<string>();
            IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            rsArea.Select("ID,NAME");
            rsArea.OrderBy("NAME", OrderDirection.Ascending);

            cbRegion.Items.Clear();
            try
            {
                for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                {
                    string province = rsArea.GetS("NAME");
                    if (!hashProvince.Contains(province))
                    {
                        hashProvince.Add(province);
                        //cbRegion.Items.Add(province);
                        _listProvince.Add(new ComboBoxDictionary<string, string>(province, province));
                    }
                }
            }
            finally
            {
                if (rsArea.IsOpen())
                    rsArea.Close();
                rsArea.Destroy();
            }
            
            _listProvince.InitComboBox(cbRegion);
            cbRegion.DataBindings.Add("SelectedValue", _certificate, "Province", true);
        }
    }
}

