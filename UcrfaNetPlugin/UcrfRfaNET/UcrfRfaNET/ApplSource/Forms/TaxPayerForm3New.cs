﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSM;
using XICSM.UcrfRfaNET.Extension;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Reports;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    class TaxPayerForm3New : ITaxPayerForm
    {
        private const string FormType = "Форма 3";
        /// <summary>
        /// Данные для записи
        /// </summary>
        class DataItem
        {
            public string OwnerName { get; set; }
            public string OwnerRegNum { get; set; }
            public string OwnerAddress { get; set; }
            public string LicNum { get; set; }
            public string LicDateFromStr { get; set; }
            public string DozvNum { get; set; }
            public DateTime DozvDateFrom { get; set; }
            public DateTime DozvDateTo { get; set; }
            public DateTime DozvDateCancel { get; set; }
            public string DozvAddress { get; set; }
            public double Power { get; set; }
            public string OrderCode { get; set; }
            public string OrderSymbol { get; set; }
            public string Province { get; set; }
            public double Bw { get; set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public DataItem()
            {
                OwnerName = "";
                OwnerRegNum = "";
                OwnerAddress = "";
                LicNum = "";
                LicDateFromStr = "";
                DozvNum = "";
                DozvDateFrom = IM.NullT;
                DozvDateTo = IM.NullT;
                DozvDateCancel = IM.NullT;
                DozvAddress = "";
                Power = IM.NullD;
                OrderCode = "";
                OrderSymbol = "";
                Province = "";
                Bw = 0.0;
            }
        }
        /// <summary>
        /// Класс работы с данными
        /// </summary>
        class WriteData
        {
            private Dictionary<string, DataItem> _data;
            /// <summary>
            /// Конструктор
            /// </summary>
            public WriteData()
            {
                _data = new Dictionary<string, DataItem>();
            }
            /// <summary>
            /// Добавить запись
            /// </summary>
            public string[] Add(Dictionary<string, string> radioSystem,
                            int recId,
                            string ownerName,
                            string ownerRegNum,
                            string ownerAddress,
                            string licNum,
                            string licDateFrom,
                            string dozvNum,
                            DateTime dozvDateFrom,
                            DateTime dozvDateTo,
                            string dozvAddress,
                            double power,
                            string orderSymbol,
                            string province,
                            double bw,
                            double freq,
                            DateTime dozvDateCancel)
            {
                List<string> errorList = new List<string>();

                //#6591
                //if (string.IsNullOrEmpty(licDateFrom))
                //    errorList.Add(string.Format("Відсутня дата початку ліцензії: ID запису:{0}", recId));
                if (dozvDateFrom == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата початку дозволу: ID запису = {0}", recId, FormType));
                if (dozvDateTo == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата закінчення дозволу: ID запису = {0}", recId, FormType));
                if (string.IsNullOrEmpty(orderSymbol))
                    errorList.Add(string.Format("{1}: Недостатньо інформації для визначення позиції Тарифів: ID запису = {0}", recId, FormType));
                else if (radioSystem.ContainsKey(orderSymbol) == false)
                    errorList.Add(string.Format("{1}: Відсутній опис для коду позиції Тарифів '{0}': ID запису = {2}", orderSymbol, FormType, recId));
                if (string.IsNullOrEmpty(province))
                    errorList.Add(string.Format("{1}: Відсутній регіон використання: ID запису = {0}", recId, FormType));
                if (power == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня потужність: ID запису = {0}", recId, FormType));
                if (bw == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня загальна ширина смуги радіочастот: ID запису = {0}", recId, FormType));
                if (bw == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня частота: ID запису:{0}", recId, FormType));

                if (errorList.Count > 0)
                    return errorList.ToArray();

                string key = ownerRegNum + "_" + province + "_" + freq.ToStringNullD() + "_" + orderSymbol + "_" + bw.ToStringNullD();
                if (_data.ContainsKey(key) == false)
                {
                    DataItem di = new DataItem();
                    _data.Add(key, di);
                    di.OwnerName = ownerName;
                    di.OwnerRegNum = ownerRegNum;
                    di.OwnerAddress = ownerAddress;
                    di.LicNum = licNum;
                    di.LicDateFromStr = licDateFrom;
                    di.DozvNum = dozvNum;
                    di.DozvDateFrom = dozvDateFrom;
                    di.DozvDateTo = dozvDateTo;
                    di.DozvAddress = dozvAddress;
                    di.DozvDateCancel = dozvDateCancel;
                    di.Power = power;
                    di.Province = province;
                    di.Bw = bw;
                    di.OrderSymbol = orderSymbol;
                    if (radioSystem.ContainsKey(orderSymbol))
                        di.OrderCode = radioSystem[di.OrderSymbol];
                }
                return errorList.ToArray();
            }
            /// <summary>
            /// Сохранить в файл
            /// </summary>
            /// <param name="path"></param>
            /// <param name="province"></param>
            /// <param name="repType"></param>
            public void Save(string path, string province, ReportType repType)
            {
                if (_data.Count == 0)
                    return;

                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Запис в файл")))
                {
                    pb.SetBig("Запис в файл...");
                    using (IReport rep = Report.CreateReport(repType))
                    {
                        rep.Init(Path.Combine(path, string.Format("форма3{0}{1}", string.IsNullOrEmpty(province) ? "" : "-", province)), "Page1","");
                        List<string> line = new List<string>();
                        line.Add("№ з/п");
                        line.Add("Найменування користувача РЧР /ПІБ для користувача РЧР фізичної особи");
                        line.Add("Код ЄДРПОУ/Реєстраційний номер облікової картки платника податків для фізичних осіб суб’єктів господарювання");
                        line.Add("Місцезнаходження користувача – юридичної особи /Місце проживання фізичної особи");
                        line.Add("Ліцензія на мовлення – номер");
                        line.Add("Ліцензія на мовлення – дата видачі дд.мм.рррр");
                        line.Add("Дозвіл на експлуатацію РЕЗ мовлення – номер");
                        line.Add("Дозвіл на експлуатацію РЕЗ мовлення – дата видачі дд.мм.рррр");
                        line.Add("Дозвіл на експлуатацію РЕЗ мовлення – термін дії до мм.рррр");
                        line.Add("Дозвіл на експлуатацію РЕЗ мовлення – потужність, кВт");
                        line.Add("Дозвіл на експлуатацію РЕЗ мовлення – адреса місця встановлення РЕЗ мовлення");
                        line.Add("Вид радіозв’язку");
                        line.Add("Номер та строка (абзац, пункт) позиції статті 320 Податку кодексу України, якою встановлено ставки збору за користування РЧР");
                        line.Add("Назва регіонів користування РЧР");
                        line.Add("Загальна ширина смуги радіочастот, зазначена в дозволі (МГц)");
                        line.Add("Дата анулювання дозволу дд.мм.рррр");
                        line.Add("Примітки");
                        rep.WriteLine(line.ToArray());
                        
                        
                        int counter = 1;
                        foreach (KeyValuePair<string, DataItem> item in _data)
                        {
                            pb.SetSmall(counter, _data.Count);
                            line.Clear();
                            DataItem di = item.Value;
                            line.Add(string.Format("{0}", counter++));
                            line.Add(di.OwnerName);
                            line.Add(di.OwnerRegNum);
                            line.Add(di.OwnerAddress);
                            line.Add(di.LicNum);
                            line.Add(di.LicDateFromStr);
                            line.Add(di.DozvNum);
                            line.Add(di.DozvDateFrom.ToStringNullT("dd.MM.yyyy"));
                            line.Add(di.DozvDateTo.ToStringNullT("dd.MM.yyyy"));
                            line.Add(di.Power.Round(3).ToStringNullD());
                            line.Add(di.DozvAddress);
                            line.Add(di.OrderCode);
                            line.Add(di.OrderSymbol);
                            line.Add(di.Province);
                            line.Add(di.Bw.ToStringNullD());
                            line.Add(di.DozvDateCancel.ToStringNullT("dd.MM.yyyy"));
                            rep.WriteLine(line.ToArray());
                            if (pb.UserCanceled())
                                break;
                        }
                        if (!pb.UserCanceled())
                            rep.Save();
                    }
                }
            }
        }
        private DateTime _dateBeg;
        private DateTime _dateEnd;
        private string _path;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="date">Дата формирования</param>
        /// <param name="path">Путь к файлам</param>
        public TaxPayerForm3New(TaxPayerReportParams _params)
        {
            _dateBeg = _params.DateBeg;
            _dateEnd = _params.DateEnd;
            _path = _params.FilePath;
        }
        /// <summary>
        /// Начать процесс генерации документов
        /// </summary>
        /// <returns>список ошибок</returns>
        public string[] Generate(ReportType repType, bool isOneFile)
        {
            List<string> errorList = new List<string>();
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування переліку користувачів РЧР (Форма 3)")))
            {
                Dictionary<string, string> radioSystem = EriFiles.GetEriCodeAndDescr("Fees");
                pb.SetProgress(0, 50000);
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                string lastProvince = "";
                WriteData wd = new WriteData();
                using (LisRecordSet rs = new LisRecordSet(PlugTbl.XvTaxPaeyrForm3, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID");
                    rs.Select("OWNER_ID");
                    rs.Select("OWNER_PROVINCE");
                    rs.Select("OWNER_NAME");
                    rs.Select("OWNER_CODE");
                    rs.Select("OWNER_ADDRESS");
                    rs.Select("LIC_NUM");
                    rs.Select("LIC_DATE_FROM");
                    rs.Select("DOC_NUM");
                    rs.Select("DOC_DATE_FROM");
                    rs.Select("DOC_DATE_TO");
                    rs.Select("POWER");
                    rs.Select("ADDRESS");
                    rs.Select("FEES_ID");
                    rs.Select("Fees.ORDER_SYMBOL");
                    rs.Select("PROVINCE");
                    rs.Select("BW");
                    rs.Select("TX_FREQ_TXT");
                    rs.Select("DOZV_DATE_CANCEL");
                    rs.Select("Appl.DOZV_NUM");
                    rs.Select("Appl.DOZV_DATE_FROM");
                    rs.Select("Appl.DOZV_DATE_TO");
                    rs.Select("Appl.DOZV_DATE_CANCEL");
                    rs.Select("Licence.NAME");
                    rs.Select("Licence.START_DATE");
                    rs.Select("Licence.STOP_DATE");
                    rs.Select("Licence.END_DATE");
                    rs.SetWhere("Appl.DOZV_DATE_FROM", IMRecordset.Operation.Lt, _dateEnd);
                    rs.SetWhere("Appl.DOZV_DATE_TO", IMRecordset.Operation.Ge, _dateBeg);
                    rs.OrderBy("OWNER_PROVINCE", OrderDirection.Ascending);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (pb.UserCanceled())
                        {
                            errorList.Add("Перервано користувачем.");
                            break;
                        }
                        pb.Increment(true);
                        int recId = rs.GetI("ID");

                        DateTime cancelDate = rs.GetT("Appl.DOZV_DATE_CANCEL");
                        if ((cancelDate != IM.NullT) && (cancelDate < _dateBeg))
                            continue;

                        string provinceOwner = rs.GetS("OWNER_PROVINCE");
                        if (string.IsNullOrEmpty(provinceOwner))
                        {
                            //нет обоасти
                            errorList.Add(string.Format("{1}: Відсутня область власника: ID запису = {0}, ID власника = {2}", recId, FormType, rs.GetI("OWNER_ID")));
                            provinceOwner = "(без регіону)";
                        }

                        if ((provinceOwner != lastProvince) && (isOneFile == false))
                        {
                            if (string.IsNullOrEmpty(lastProvince) == false)
                            {
                                pb.SetBig("Запис в файл...");
                                wd.Save(_path, lastProvince, repType);
                            }
                            lastProvince = provinceOwner;
                            wd = new WriteData();
                            pb.SetBig(lastProvince);
                        }

                        string[] error = wd.Add(radioSystem,
                            recId,
                            rs.GetS("OWNER_NAME"),
                            rs.GetS("OWNER_CODE"),
                            rs.GetS("OWNER_ADDRESS"),
                            rs.GetS("Licence.NAME"),
                            rs.GetT("Licence.START_DATE").ToStringNullT(),
                            rs.GetS("Appl.DOZV_NUM"),
                            rs.GetT("Appl.DOZV_DATE_FROM"),
                            rs.GetT("Appl.DOZV_DATE_TO"),
                            rs.GetS("ADDRESS"),
                            rs.GetD("POWER"),
                            rs.GetS("Fees.ORDER_SYMBOL"),
                            rs.GetS("PROVINCE"),
                            rs.GetD("BW"),
                            ConvertType.ToDouble(rs.GetS("TX_FREQ_TXT"), IM.NullD),
                            cancelDate);
                        errorList.AddRange(error);
                    }
                    if (!pb.UserCanceled())
                    {
                        if (string.IsNullOrEmpty(lastProvince) == false)
                            wd.Save(_path, lastProvince, repType);
                        else if (isOneFile)
                            wd.Save(_path, "", repType);
                    }
                }
            }
            return errorList.ToArray();
        }
    }
}
