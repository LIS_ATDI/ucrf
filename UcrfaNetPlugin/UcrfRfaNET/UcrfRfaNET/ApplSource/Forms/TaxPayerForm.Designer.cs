﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class TaxPayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxPayerForm));
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.taxPayerReportParamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lbEnd = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpValueStan = new System.Windows.Forms.DateTimePicker();
            this.chbF1IgnoreTimePeriod = new System.Windows.Forms.CheckBox();
            this.gbxF1Method = new System.Windows.Forms.GroupBox();
            this.rbtF1Whole = new System.Windows.Forms.RadioButton();
            this.rbtF1Allotments = new System.Windows.Forms.RadioButton();
            this.chbF1Status = new System.Windows.Forms.CheckBox();
            this.lbF2SelectedTable = new System.Windows.Forms.Label();
            this.cbTableName = new System.Windows.Forms.ComboBox();
            this.cbForm3 = new System.Windows.Forms.CheckBox();
            this.cbForm2 = new System.Windows.Forms.CheckBox();
            this.cbForm1 = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnGenerateNew = new System.Windows.Forms.Button();
            this.gbFileType = new System.Windows.Forms.GroupBox();
            this.rbRepTypeExcel = new System.Windows.Forms.RadioButton();
            this.rbRepTypeCsv = new System.Windows.Forms.RadioButton();
            this.cbOneFile = new System.Windows.Forms.CheckBox();
            this.lbBeg = new System.Windows.Forms.Label();
            this.dtpBeg = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.taxPayerReportParamsBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbxF1Method.SuspendLayout();
            this.gbFileType.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpEnd
            // 
            this.dtpEnd.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.taxPayerReportParamsBindingSource, "DateEnd", true));
            this.dtpEnd.Location = new System.Drawing.Point(369, 12);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(132, 20);
            this.dtpEnd.TabIndex = 3;
            // 
            // taxPayerReportParamsBindingSource
            // 
            this.taxPayerReportParamsBindingSource.DataSource = typeof(XICSM.UcrfRfaNET.ApplSource.Forms.TaxPayerReportParams);
            // 
            // lbEnd
            // 
            this.lbEnd.AutoSize = true;
            this.lbEnd.Location = new System.Drawing.Point(280, 16);
            this.lbEnd.Name = "lbEnd";
            this.lbEnd.Size = new System.Drawing.Size(83, 13);
            this.lbEnd.TabIndex = 2;
            this.lbEnd.Text = "Кінець періоду:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chbF1IgnoreTimePeriod);
            this.groupBox1.Controls.Add(this.gbxF1Method);
            this.groupBox1.Controls.Add(this.chbF1Status);
            this.groupBox1.Controls.Add(this.lbF2SelectedTable);
            this.groupBox1.Controls.Add(this.cbTableName);
            this.groupBox1.Controls.Add(this.cbForm3);
            this.groupBox1.Controls.Add(this.cbForm2);
            this.groupBox1.Controls.Add(this.cbForm1);
            this.groupBox1.Location = new System.Drawing.Point(20, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 229);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Форми звіту:";
            // 
            // dtpValueStan
            // 
            this.dtpValueStan.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.taxPayerReportParamsBindingSource, "DateStan", true));
            this.dtpValueStan.Location = new System.Drawing.Point(384, 303);
            this.dtpValueStan.Name = "dtpValueStan";
            this.dtpValueStan.Size = new System.Drawing.Size(216, 20);
            this.dtpValueStan.TabIndex = 8;
            // 
            // chbF1IgnoreTimePeriod
            // 
            this.chbF1IgnoreTimePeriod.AutoSize = true;
            this.chbF1IgnoreTimePeriod.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.taxPayerReportParamsBindingSource, "F1IgnoreTimePeriod", true));
            this.chbF1IgnoreTimePeriod.Enabled = false;
            this.chbF1IgnoreTimePeriod.Location = new System.Drawing.Point(126, 38);
            this.chbF1IgnoreTimePeriod.Name = "chbF1IgnoreTimePeriod";
            this.chbF1IgnoreTimePeriod.Size = new System.Drawing.Size(181, 17);
            this.chbF1IgnoreTimePeriod.TabIndex = 7;
            this.chbF1IgnoreTimePeriod.Text = "Ігнорувати обмеження по часу";
            this.chbF1IgnoreTimePeriod.UseVisualStyleBackColor = true;
            // 
            // gbxF1Method
            // 
            this.gbxF1Method.Controls.Add(this.rbtF1Whole);
            this.gbxF1Method.Controls.Add(this.rbtF1Allotments);
            this.gbxF1Method.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.taxPayerReportParamsBindingSource, "DoF1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gbxF1Method.Enabled = false;
            this.gbxF1Method.Location = new System.Drawing.Point(102, 60);
            this.gbxF1Method.Name = "gbxF1Method";
            this.gbxF1Method.Size = new System.Drawing.Size(230, 70);
            this.gbxF1Method.TabIndex = 6;
            this.gbxF1Method.TabStop = false;
            this.gbxF1Method.Text = "Загальна смуга частот";
            // 
            // rbtF1Whole
            // 
            this.rbtF1Whole.AutoSize = true;
            this.rbtF1Whole.Location = new System.Drawing.Point(14, 45);
            this.rbtF1Whole.Name = "rbtF1Whole";
            this.rbtF1Whole.Size = new System.Drawing.Size(193, 17);
            this.rbtF1Whole.TabIndex = 1;
            this.rbtF1Whole.Text = "З поля CUST_NBR3 (BW) ліцензії";
            this.rbtF1Whole.UseVisualStyleBackColor = true;
            // 
            // rbtF1Allotments
            // 
            this.rbtF1Allotments.AutoSize = true;
            this.rbtF1Allotments.Location = new System.Drawing.Point(14, 21);
            this.rbtF1Allotments.Name = "rbtF1Allotments";
            this.rbtF1Allotments.Size = new System.Drawing.Size(175, 17);
            this.rbtF1Allotments.TabIndex = 0;
            this.rbtF1Allotments.Text = "З канальних виділень ліцензії";
            this.rbtF1Allotments.UseVisualStyleBackColor = true;
            // 
            // chbF1Status
            // 
            this.chbF1Status.AutoSize = true;
            this.chbF1Status.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.taxPayerReportParamsBindingSource, "F1StatusPOnly", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbF1Status.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.taxPayerReportParamsBindingSource, "DoF1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbF1Status.Enabled = false;
            this.chbF1Status.Location = new System.Drawing.Point(111, 19);
            this.chbF1Status.Name = "chbF1Status";
            this.chbF1Status.Size = new System.Drawing.Size(152, 17);
            this.chbF1Status.TabIndex = 5;
            this.chbF1Status.Text = "Тільки зі станом \'Дійсна\'";
            this.chbF1Status.UseVisualStyleBackColor = true;
            this.chbF1Status.CheckedChanged += new System.EventHandler(this.chbF1Status_CheckedChanged);
            // 
            // lbF2SelectedTable
            // 
            this.lbF2SelectedTable.AutoSize = true;
            this.lbF2SelectedTable.Enabled = false;
            this.lbF2SelectedTable.Location = new System.Drawing.Point(108, 144);
            this.lbF2SelectedTable.Name = "lbF2SelectedTable";
            this.lbF2SelectedTable.Size = new System.Drawing.Size(104, 13);
            this.lbF2SelectedTable.TabIndex = 4;
            this.lbF2SelectedTable.Text = "Тільки для таблиці:";
            // 
            // cbTableName
            // 
            this.cbTableName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTableName.Enabled = false;
            this.cbTableName.FormattingEnabled = true;
            this.cbTableName.Location = new System.Drawing.Point(102, 161);
            this.cbTableName.Name = "cbTableName";
            this.cbTableName.Size = new System.Drawing.Size(219, 21);
            this.cbTableName.TabIndex = 3;
            // 
            // cbForm3
            // 
            this.cbForm3.AutoSize = true;
            this.cbForm3.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.taxPayerReportParamsBindingSource, "DoF3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbForm3.Location = new System.Drawing.Point(9, 206);
            this.cbForm3.Name = "cbForm3";
            this.cbForm3.Size = new System.Drawing.Size(72, 17);
            this.cbForm3.TabIndex = 2;
            this.cbForm3.Text = "Форма 3";
            this.cbForm3.UseVisualStyleBackColor = true;
            this.cbForm3.CheckedChanged += new System.EventHandler(this.cbForm3_CheckedChanged);
            // 
            // cbForm2
            // 
            this.cbForm2.AutoSize = true;
            this.cbForm2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.taxPayerReportParamsBindingSource, "DoF2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbForm2.Location = new System.Drawing.Point(9, 143);
            this.cbForm2.Name = "cbForm2";
            this.cbForm2.Size = new System.Drawing.Size(72, 17);
            this.cbForm2.TabIndex = 1;
            this.cbForm2.Text = "Форма 2";
            this.cbForm2.UseVisualStyleBackColor = true;
            this.cbForm2.CheckedChanged += new System.EventHandler(this.cbForm2_CheckedChanged);
            // 
            // cbForm1
            // 
            this.cbForm1.AutoSize = true;
            this.cbForm1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.taxPayerReportParamsBindingSource, "DoF1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbForm1.Location = new System.Drawing.Point(9, 19);
            this.cbForm1.Name = "cbForm1";
            this.cbForm1.Size = new System.Drawing.Size(72, 17);
            this.cbForm1.TabIndex = 0;
            this.cbForm1.Text = "Форма 1";
            this.cbForm1.UseVisualStyleBackColor = true;
            this.cbForm1.CheckedChanged += new System.EventHandler(this.cbForm1_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(539, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Закрити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // tbPath
            // 
            this.tbPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.taxPayerReportParamsBindingSource, "FilePath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbPath.Location = new System.Drawing.Point(20, 62);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(563, 20);
            this.tbPath.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Шлях збереження звітів:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(589, 60);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(25, 23);
            this.btnBrowse.TabIndex = 6;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnGenerateNew
            // 
            this.btnGenerateNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateNew.Location = new System.Drawing.Point(369, 339);
            this.btnGenerateNew.Name = "btnGenerateNew";
            this.btnGenerateNew.Size = new System.Drawing.Size(149, 23);
            this.btnGenerateNew.TabIndex = 10;
            this.btnGenerateNew.Text = "Формувати звіти";
            this.btnGenerateNew.UseVisualStyleBackColor = true;
            this.btnGenerateNew.Click += new System.EventHandler(this.btnGenerateNew_Click);
            // 
            // gbFileType
            // 
            this.gbFileType.Controls.Add(this.rbRepTypeExcel);
            this.gbFileType.Controls.Add(this.rbRepTypeCsv);
            this.gbFileType.Location = new System.Drawing.Point(369, 97);
            this.gbFileType.Name = "gbFileType";
            this.gbFileType.Size = new System.Drawing.Size(245, 50);
            this.gbFileType.TabIndex = 8;
            this.gbFileType.TabStop = false;
            this.gbFileType.Text = "Тип файлу";
            // 
            // rbRepTypeExcel
            // 
            this.rbRepTypeExcel.AutoSize = true;
            this.rbRepTypeExcel.Checked = true;
            this.rbRepTypeExcel.Location = new System.Drawing.Point(15, 19);
            this.rbRepTypeExcel.Name = "rbRepTypeExcel";
            this.rbRepTypeExcel.Size = new System.Drawing.Size(51, 17);
            this.rbRepTypeExcel.TabIndex = 1;
            this.rbRepTypeExcel.TabStop = true;
            this.rbRepTypeExcel.Text = "Excel";
            this.rbRepTypeExcel.UseVisualStyleBackColor = true;
            // 
            // rbRepTypeCsv
            // 
            this.rbRepTypeCsv.AutoSize = true;
            this.rbRepTypeCsv.Location = new System.Drawing.Point(130, 19);
            this.rbRepTypeCsv.Name = "rbRepTypeCsv";
            this.rbRepTypeCsv.Size = new System.Drawing.Size(46, 17);
            this.rbRepTypeCsv.TabIndex = 0;
            this.rbRepTypeCsv.Text = "CSV";
            this.rbRepTypeCsv.UseVisualStyleBackColor = true;
            // 
            // cbOneFile
            // 
            this.cbOneFile.AutoSize = true;
            this.cbOneFile.Checked = true;
            this.cbOneFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOneFile.Location = new System.Drawing.Point(384, 164);
            this.cbOneFile.Name = "cbOneFile";
            this.cbOneFile.Size = new System.Drawing.Size(89, 17);
            this.cbOneFile.TabIndex = 9;
            this.cbOneFile.Text = "В один файл";
            this.cbOneFile.UseVisualStyleBackColor = true;
            // 
            // lbBeg
            // 
            this.lbBeg.AutoSize = true;
            this.lbBeg.Location = new System.Drawing.Point(17, 16);
            this.lbBeg.Name = "lbBeg";
            this.lbBeg.Size = new System.Drawing.Size(92, 13);
            this.lbBeg.TabIndex = 0;
            this.lbBeg.Text = "Початок періоду:";
            // 
            // dtpBeg
            // 
            this.dtpBeg.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.taxPayerReportParamsBindingSource, "DateBeg", true));
            this.dtpBeg.Location = new System.Drawing.Point(122, 12);
            this.dtpBeg.Name = "dtpBeg";
            this.dtpBeg.Size = new System.Drawing.Size(132, 20);
            this.dtpBeg.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(381, 287);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Звіт станом на:";
            // 
            // TaxPayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(632, 374);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbBeg);
            this.Controls.Add(this.dtpValueStan);
            this.Controls.Add(this.dtpBeg);
            this.Controls.Add(this.cbOneFile);
            this.Controls.Add(this.gbFileType);
            this.Controls.Add(this.btnGenerateNew);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbEnd);
            this.Controls.Add(this.dtpEnd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaxPayerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Формування переліку користувачів РЧР";
            this.Load += new System.EventHandler(this.TaxPayerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.taxPayerReportParamsBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxF1Method.ResumeLayout(false);
            this.gbxF1Method.PerformLayout();
            this.gbFileType.ResumeLayout(false);
            this.gbFileType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label lbEnd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbForm3;
        private System.Windows.Forms.CheckBox cbForm2;
        private System.Windows.Forms.CheckBox cbForm1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnGenerateNew;
        private System.Windows.Forms.ComboBox cbTableName;
        private System.Windows.Forms.GroupBox gbFileType;
        private System.Windows.Forms.RadioButton rbRepTypeExcel;
        private System.Windows.Forms.RadioButton rbRepTypeCsv;
        private System.Windows.Forms.CheckBox cbOneFile;
        private System.Windows.Forms.Label lbBeg;
        private System.Windows.Forms.DateTimePicker dtpBeg;
        private System.Windows.Forms.GroupBox gbxF1Method;
        private System.Windows.Forms.RadioButton rbtF1Whole;
        private System.Windows.Forms.RadioButton rbtF1Allotments;
        private System.Windows.Forms.CheckBox chbF1Status;
        private System.Windows.Forms.Label lbF2SelectedTable;
        private System.Windows.Forms.BindingSource taxPayerReportParamsBindingSource;
        private System.Windows.Forms.CheckBox chbF1IgnoreTimePeriod;
        private System.Windows.Forms.DateTimePicker dtpValueStan;
        private System.Windows.Forms.Label label1;
    }
}