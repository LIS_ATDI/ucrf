﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Objects;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    internal partial class ChangeMonitorProvince : Form
    {
        public ApplObject AppObject;
        //-----
        public ChangeMonitorProvince(int idAppl)
        {
            InitializeComponent();
            CLocaliz.TxT(this);
            //----
            AppObject = new ApplObject(idAppl);
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeMonitorProvince_Load(object sender, EventArgs e)
        {
            // Загрузка списка областей
            HashSet<string> hashProvince = new HashSet<string>();
            IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            rsArea.Select("ID,NAME");
            rsArea.OrderBy("NAME", OrderDirection.Ascending);
            try
            {
                for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                {
                    string province = rsArea.GetS("NAME");
                    if(!hashProvince.Contains(province))
                    {
                        hashProvince.Add(province);
                        cbProvince.Items.Add(province);
                    }
                }
            }
            finally
            {
                if(rsArea.IsOpen())
                    rsArea.Close();
                rsArea.Destroy();
            }
            //------
            // Привязка данных
            tbFile.DataBindings.Add("Text", AppObject, "ChangeFilePos", true, DataSourceUpdateMode.OnPropertyChanged);
            tbDocNum.DataBindings.Add("Text", AppObject, "ChangeDocNum", true, DataSourceUpdateMode.OnPropertyChanged);
            cbProvince.DataBindings.Add("SelectedItem", AppObject, "ChangeProvince", true, DataSourceUpdateMode.OnPropertyChanged);
            dtDocDate.DataBindings.Add("Value", AppObject, "ChangeDocDate", true, DataSourceUpdateMode.OnPropertyChanged);
            btnOk.DataBindings.Add("Enabled", AppObject, "IsChanged", true);
        }
        /// <summary>
        /// Сохраняем изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AppObject.ChangeFilePos))
            {
                MessageBox.Show(CLocaliz.TxT("Document was't selected."), "", MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                btnOk.Focus();
                return;
            }
            else if (System.IO.File.Exists(AppObject.ChangeFilePos) == false)
            {
                MessageBox.Show(string.Format("{0} '{1}'", CLocaliz.TxT("Can't find file"), AppObject.ChangeFilePos),
                                "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                btnOk.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(AppObject.ChangeDocNum))
            {
                MessageBox.Show(CLocaliz.TxT("Document number can't be empty"), "", MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                tbDocNum.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(AppObject.ChangeProvince))
            {
                MessageBox.Show(CLocaliz.TxT("Province can't be empty"), "", MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                cbProvince.Focus();
                return;
            }
            AppObject.Save();
            DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// Выбераем файл
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                AppObject.ChangeFilePos = openFileDialog.FileName;
            }
        }
    }
}
