﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class AmateurBandForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblBandwidth = new System.Windows.Forms.Label();
            this.lblPower = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBandwidth1 = new System.Windows.Forms.TextBox();
            this.txtPower = new System.Windows.Forms.TextBox();
            this.txtBandwidth2 = new System.Windows.Forms.TextBox();
            this.lbldef = new System.Windows.Forms.Label();
            this.cmbBoxCatOper = new System.Windows.Forms.ComboBox();
            this.cmbBxTypeStat = new System.Windows.Forms.ComboBox();
            this.chckLstBox = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(155, 302);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(264, 302);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "Зберегти";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblBandwidth
            // 
            this.lblBandwidth.AutoSize = true;
            this.lblBandwidth.Location = new System.Drawing.Point(12, 15);
            this.lblBandwidth.Name = "lblBandwidth";
            this.lblBandwidth.Size = new System.Drawing.Size(139, 13);
            this.lblBandwidth.TabIndex = 0;
            this.lblBandwidth.Text = "Смуга пропускання, МГц: ";
            // 
            // lblPower
            // 
            this.lblPower.AutoSize = true;
            this.lblPower.Location = new System.Drawing.Point(12, 41);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(85, 13);
            this.lblPower.TabIndex = 0;
            this.lblPower.Text = "Потужність, Вт:";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(12, 95);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(115, 13);
            this.lblCategory.TabIndex = 0;
            this.lblCategory.Text = "Категорія оператора:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Тип станції:";
            // 
            // txtBandwidth1
            // 
            this.txtBandwidth1.Location = new System.Drawing.Point(155, 12);
            this.txtBandwidth1.Name = "txtBandwidth1";
            this.txtBandwidth1.Size = new System.Drawing.Size(75, 20);
            this.txtBandwidth1.TabIndex = 1;
            this.txtBandwidth1.Leave += new System.EventHandler(this.txtBandwidth1_Leave);
            // 
            // txtPower
            // 
            this.txtPower.Location = new System.Drawing.Point(155, 38);
            this.txtPower.Name = "txtPower";
            this.txtPower.Size = new System.Drawing.Size(184, 20);
            this.txtPower.TabIndex = 3;
            // 
            // txtBandwidth2
            // 
            this.txtBandwidth2.Location = new System.Drawing.Point(264, 12);
            this.txtBandwidth2.Name = "txtBandwidth2";
            this.txtBandwidth2.Size = new System.Drawing.Size(75, 20);
            this.txtBandwidth2.TabIndex = 2;
            this.txtBandwidth2.Leave += new System.EventHandler(this.txtBandwidth2_Leave);
            // 
            // lbldef
            // 
            this.lbldef.AutoSize = true;
            this.lbldef.Location = new System.Drawing.Point(236, 15);
            this.lbldef.Name = "lbldef";
            this.lbldef.Size = new System.Drawing.Size(10, 13);
            this.lbldef.TabIndex = 11;
            this.lbldef.Text = "-";
            // 
            // cmbBoxCatOper
            // 
            this.cmbBoxCatOper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxCatOper.FormattingEnabled = true;
            this.cmbBoxCatOper.Location = new System.Drawing.Point(155, 92);
            this.cmbBoxCatOper.Name = "cmbBoxCatOper";
            this.cmbBoxCatOper.Size = new System.Drawing.Size(184, 21);
            this.cmbBoxCatOper.TabIndex = 5;
            // 
            // cmbBxTypeStat
            // 
            this.cmbBxTypeStat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxTypeStat.FormattingEnabled = true;
            this.cmbBxTypeStat.Location = new System.Drawing.Point(155, 65);
            this.cmbBxTypeStat.Name = "cmbBxTypeStat";
            this.cmbBxTypeStat.Size = new System.Drawing.Size(184, 21);
            this.cmbBxTypeStat.TabIndex = 4;
            this.cmbBxTypeStat.SelectedIndexChanged += new System.EventHandler(this.cmbBxTypeStat_SelectedIndexChanged);
            // 
            // chckLstBox
            // 
            this.chckLstBox.FormattingEnabled = true;
            this.chckLstBox.Location = new System.Drawing.Point(15, 142);
            this.chckLstBox.Name = "chckLstBox";
            this.chckLstBox.Size = new System.Drawing.Size(324, 154);
            this.chckLstBox.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Типи частот:";
            // 
            // AmateurBandForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 337);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chckLstBox);
            this.Controls.Add(this.cmbBxTypeStat);
            this.Controls.Add(this.cmbBoxCatOper);
            this.Controls.Add(this.lbldef);
            this.Controls.Add(this.txtBandwidth2);
            this.Controls.Add(this.txtPower);
            this.Controls.Add(this.txtBandwidth1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.lblPower);
            this.Controls.Add(this.lblBandwidth);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AmateurBandForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Смуги частот аматорів";
            this.Load += new System.EventHandler(this.AmateurBandForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblBandwidth;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBandwidth1;
        private System.Windows.Forms.TextBox txtPower;
        private System.Windows.Forms.TextBox txtBandwidth2;
        private System.Windows.Forms.Label lbldef;
        private System.Windows.Forms.ComboBox cmbBoxCatOper;
        private System.Windows.Forms.ComboBox cmbBxTypeStat;
        private System.Windows.Forms.CheckedListBox chckLstBox;
        private System.Windows.Forms.Label label1;
    }
}