﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Documents;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    internal partial class DiffReport3Form : FBaseForm
    {
        private class MeasureEquipment
        {
            public int Id { get; set; }
            public string Name { get; set; }
            
            public override string ToString()
            {
                return Name;
            }
        }

        private DiffReport3Object _repObject;

        private ComboBoxDictionaryList<int, string> _overallDictonary1 = new ComboBoxDictionaryList<int, string>();
        private ComboBoxDictionaryList<int, string> _overallDictonary2 = new ComboBoxDictionaryList<int, string>();

        private ComboBoxDictionaryList<int, string> _measureEquipmentCertificateDictionary = new ComboBoxDictionaryList<int, string>();
        private ComboBoxDictionaryList<int, string> _measureEquipmentDictionary = new ComboBoxDictionaryList<int, string>();
        private ComboBoxDictionaryList<int, string> _auxiliaryEquipmentDictionary = new ComboBoxDictionaryList<int, string>();
        
        // Для Binding ComboBox
        private ComboBoxDictionaryList<string, string> _listProvince;
        /// <summary>
        /// Список провинцей
        /// </summary>
        public List<string> ProvinceList { get; set; }

        /// <summary>
        /// Находится ли в солстоянии сброса список областей
        /// Нужен, чтобы комбобокс второй раз меседжббокс не бросал.
        /// </summary>
        private bool _resetting;

        private string _province;
        public string Province
        {
            get { return _province; }
            set
            {
                if (_province != value)
                {
                    _province = value;
                    InvokeNotifyPropertyChanged("Province");                    
                }
            }
        }

        public DiffReport3Form(BaseDiffClass diffObject)
        {           
            InitializeComponent();
            _repObject = new DiffReport3Object(diffObject);            
        }

        private void DiffReport3Form_Load(object sender, EventArgs e)
        {                       
            dtpStartTime.DataBindings.Add("Value", _repObject, "TimeFrom", true);
            dtpEndTime.DataBindings.Add("Value", _repObject, "TimeTo", true);
            dtpEndTime.DataBindings.Add("MinDate", _repObject, "TimeFrom", true);

            cbCertificate.DataBindings.Add("SelectedValue", _repObject, "CertificateId", true);

            MeasureEquipmentList mel = new MeasureEquipmentList();
            mel.ReadMeasurementEquipmentList(Province);// _repObject.CertificateId);
            _measureEquipmentDictionary = mel.GetComboBoxDictionaryList();

            BuildMeasureEquipmentListBox();

            AuxiliaryEquipmentList ael = new AuxiliaryEquipmentList();
            ael.ReadAuxiliatyEquipmentList();
            _auxiliaryEquipmentDictionary = ael.GetComboBoxDictionaryList();

            BuildAuxiliaryEquipmentListBox();

            BuildAdditionalCheckListBox();

            if (!string.IsNullOrEmpty(_repObject.FileLink))
            {
                lblSaved.Text = _repObject.FileLink;                
                lblSaved.Visible = true;
            }
            LoadProvinces();
            FillEmployeeList();


            //cbRegion_SelectedValueChanged(this, e);
        }

        private void LoadProvinces()
        {
            HashSet<string> hashProvince = new HashSet<string>();

            _listProvince = new ComboBoxDictionaryList<string, string>();            
            //cbRegion.DataBindings.Clear();

            string userProvince = CUsers.GetUserProvince();            
            //userProvince = "Житомирська";

            if (string.IsNullOrEmpty(userProvince))
            {                
                // Загрузка списка областей
                
                IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                rsArea.Select("ID,NAME");
                rsArea.OrderBy("NAME", OrderDirection.Ascending);

                cbRegion.Items.Clear();
                try
                {
                    for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                    {
                        string province = rsArea.GetS("NAME");
                        if (!hashProvince.Contains(province))
                        {
                            hashProvince.Add(province);
                            //cbRegion.Items.Add(province);
                            _listProvince.Add(new ComboBoxDictionary<string, string>(province, province));
                        }
                    }
                }
                finally
                {
                    if (rsArea.IsOpen())
                        rsArea.Close();
                    rsArea.Destroy();
                }
            }
            else
            {
                hashProvince.Add(userProvince);
                //cbRegion.Items.Add(province);
                _listProvince.Add(new ComboBoxDictionary<string, string>(userProvince, userProvince));
           }

            _listProvince.InitComboBox(cbRegion);
            //cbRegion.DataBindings.Add("SelectedValue", this, "Province", true);

            if (cbRegion.Items.Count == 1)
            {
                Province = userProvince;
                cbRegion.SelectedValue = userProvince;
                //InvokeNotifyPropertyChanged("Province");
                //string eee = cbRegion.SelectedValue.ToString();                
            } else
            {
                Province = "";
                cbRegion.SelectedValue = "";
            }
        }

        private bool CheckDates()
        {
            return (_repObject.TimeFrom <= _repObject.TimeTo);                            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckDates())
            {
                _repObject.NoteID.Clear();
                foreach (object note in clbAdditionals.CheckedItems)
                {
                    MeasureEquipment equipment = note as MeasureEquipment;
                    _repObject.NoteID.Add(equipment.Id);
                }

                _repObject.Save();

            }
            else
            {
                MessageBox.Show("Дата \"з\" не повинна перевищувати дату \"по\"");

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (CheckDates())
            {
                if (_repObject.Print())
                {
                    lblSaved.Text = _repObject.FileLink;
                    lblSaved.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Дата \"з\" не повинна перевищувати дату \"по\"");

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbCertificate_SelectedIndexChanged(object sender, EventArgs e)
        {                        
        }

        private void btnChooseMeasureEquipment_Click(object sender, EventArgs e)
        {
            MeasureEquipmentList mel = new MeasureEquipmentList();
            mel.ReadMeasurementEquipmentList(Province);//  _repObject.CertificateId);
            _measureEquipmentDictionary = mel.GetComboBoxDictionaryList();

            FChannels frm = new FChannels();
            frm.Text = "Вибрати вимірювальне обладнаня";
            frm.AllChannelsCaption = "Наявне обладнання";
            frm.SelectChannelsCaption = "Вибране обладнання";
            
            for (int i = 0; i < _measureEquipmentDictionary.Count; i++)
            {
                MeasureEquipment equipment = new MeasureEquipment();
                equipment.Id = _measureEquipmentDictionary[i].Key;
                equipment.Name = _measureEquipmentDictionary[i].Description;

                if (_repObject.MeasureEquipmentID.Contains(equipment.Id))
                    frm.CLBSelectChannel.Items.Add(equipment);
                else
                    frm.CLBAllChannel.Items.Add(equipment);
            }            

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _repObject.MeasureEquipmentID.Clear();                
                foreach(object selectedObject in frm.CLBSelectChannel.Items)
                {
                    MeasureEquipment equipment = selectedObject as MeasureEquipment;
                    _repObject.MeasureEquipmentID.Add(equipment.Id);
                }
                BuildMeasureEquipmentListBox();
            }            
        }

        private void BuildMeasureEquipmentListBox()
        {
            lblMeasurementEquipment.Items.Clear();

            for (int i = 0; i < _measureEquipmentDictionary.Count; i++)
            {

                if (_repObject.MeasureEquipmentID.Contains(_measureEquipmentDictionary[i].Key))
                {
                    lblMeasurementEquipment.Items.Add(_measureEquipmentDictionary[i].Description);
                }
            }
        }

        private void BuildAuxiliaryEquipmentListBox()
        {
            lblAuxiliaryEquipment.Items.Clear();

            for (int i = 0; i < _auxiliaryEquipmentDictionary.Count; i++)
            {

                if (_repObject.AuxiliaryEquipmentID.Contains(_auxiliaryEquipmentDictionary[i].Key))
                {
                    lblAuxiliaryEquipment.Items.Add(_auxiliaryEquipmentDictionary[i].Description);
                }
            }            
        }

        private void btnChooseAuxiliaryEquipment_Click(object sender, EventArgs e)
        {
            AuxiliaryEquipmentList ael = new AuxiliaryEquipmentList();
            ael.ReadAuxiliatyEquipmentList(Province);
            _auxiliaryEquipmentDictionary = ael.GetComboBoxDictionaryList();
            
            FChannels frm = new FChannels();
            frm.Text = "Вибрати допоміжне обладнаня";
            frm.AllChannelsCaption = "Наявне обладнання";
            frm.SelectChannelsCaption = "Вибране обладнання";

            for (int i = 0; i < _auxiliaryEquipmentDictionary.Count; i++)
            {
                MeasureEquipment equipment = new MeasureEquipment();
                equipment.Id = _auxiliaryEquipmentDictionary[i].Key;
                equipment.Name = _auxiliaryEquipmentDictionary[i].Description;

                if (_repObject.AuxiliaryEquipmentID.Contains(equipment.Id))
                    frm.CLBSelectChannel.Items.Add(equipment);
                else
                    frm.CLBAllChannel.Items.Add(equipment);
            }

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _repObject. AuxiliaryEquipmentID.Clear();
                foreach (object selectedObject in frm.CLBSelectChannel.Items)
                {
                    MeasureEquipment equipment = selectedObject as MeasureEquipment;
                    _repObject.AuxiliaryEquipmentID.Add(equipment.Id);
                }
                BuildAuxiliaryEquipmentListBox();
            }
        }

        private void BuildAdditionalCheckListBox()
        {            
            IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaSCPattern, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME,TYPE");
                r.SetWhere("TYPE", IMRecordset.Operation.Eq,  DocType.PARAM_REZ.ToString());

                r.Open();
                while (!r.IsEOF())
                {                    
                    MeasureEquipment equipment = new MeasureEquipment();
                    equipment.Id = r.GetI("ID");
                    equipment.Name = r.GetS("NAME");

                    int itemIndex = clbAdditionals.Items.Add(equipment);

                    if (_repObject.NoteID.Contains(equipment.Id))                                                                    
                        clbAdditionals.SetItemCheckState(itemIndex, CheckState.Checked);

                    r.MoveNext();
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }

        private void lblSaved_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            _repObject.OpenLink();
        }

        /*private void FillCertificateList()
        {
            MeasureEquipmentCertificateList mecl = new MeasureEquipmentCertificateList();

            if (!string.IsNullOrEmpty(Province))
                mecl.ReadMeasurementCertificateList(Province);

            _measureEquipmentCertificateDictionary = mecl.GetComboBoxDictionaryList();
            cbCertificate.DataBindings.Clear();                
            _measureEquipmentCertificateDictionary.InitComboBox(cbCertificate);            
            cbCertificate.DataBindings.Add("SelectedValue", _repObject, "CertificateId", true);
        }*/

        private void cbRegion_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_resetting)
                return;

            if (_repObject.CertificateId != IM.NullI ||
                        _repObject.MeasureEquipmentID.Count > 0 ||
                        _repObject.AuxiliaryEquipmentID.Count > 0)
            {
                if (MessageBox.Show("Очистити список вибраного обладнання?", "Підтвердження", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
                {
                    Province = cbRegion.SelectedValue.ToString();

                    _repObject.CertificateId = IM.NullI;
                    _repObject.MeasureEquipmentID.Clear();
                    _repObject.AuxiliaryEquipmentID.Clear();
                    lblMeasurementEquipment.Items.Clear();
                    lblAuxiliaryEquipment.Items.Clear();                    
                }  else
                {
                    _resetting = true;
                    cbRegion.SelectedValue = Province;
                    _resetting = false;
                    return;
                }
            } else
            {
                if (cbRegion.SelectedValue != null)
                    Province = cbRegion.SelectedValue.ToString();
                else
                    Province = "";
            }
            _repObject.CertificateId = GetFirstCertificateId();
            //FillEmployeeList();
            //FillCertificateList();
        }

        /*private void cbRegion_SelectedValueChanged(object sender, EventArgs e)
        {            
            //cbCertificate.DataBindings.Add("SelectedValue", _repObject, "CertificateId", true);
        }*/

        private int GetFirstCertificateId()
        {
            MeasureEquipmentCertificateList mecl = new MeasureEquipmentCertificateList();

            if (!string.IsNullOrEmpty(Province))
                mecl.ReadMeasurementCertificateList(Province);

            _measureEquipmentCertificateDictionary = mecl.GetComboBoxDictionaryList();

            if (_measureEquipmentCertificateDictionary.Count > 0)
                return _measureEquipmentCertificateDictionary.First().Key;
            else
                return IM.NullI;
        }

        private void FillEmployeeList()
        {
            EmployeeList employeeList1 = new EmployeeList();
            EmployeeList employeeList2 = new EmployeeList();
            employeeList1.ReadEntireEmployeeList(CUsers.GetUserProvince(), "MANAGEMENT");
            employeeList2.ReadEntireEmployeeList(CUsers.GetUserProvince());

            _overallDictonary1 = employeeList1.GetComboBoxDictionaryList();
            _overallDictonary2 = employeeList2.GetComboBoxDictionaryList();

            _overallDictonary1.InitComboBox(cbApprovePerson);
            _overallDictonary2.InitComboBox(cbPerformPerson);

            cbApprovePerson.DataBindings.Clear();
            cbPerformPerson.DataBindings.Clear();

            _repObject.ViseUserLst.InitComboBox(cbApprovePerson);
            cbApprovePerson.DataBindings.Add("SelectedValue", _repObject, "ApprovePersonId", true);
            _repObject.ExecutorUserLst.InitComboBox(cbPerformPerson);
            cbPerformPerson.DataBindings.Add("SelectedValue", _repObject, "PerformPersonId", true);
        }
    }
}
