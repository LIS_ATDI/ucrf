﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class MeasureEquipmentForm : Form
    {
        private MeasurementEquipment _equipment = new MeasurementEquipment();
        private ComboBoxDictionaryList<string, string> _overallDictonary = new ComboBoxDictionaryList<string, string>();

        public MeasureEquipmentForm(string tableName, int id)
        {
            InitializeComponent();

            _equipment.TableName = tableName;
            _equipment.Id = id;
            _equipment.Load();

            Dictionary<string, string> eri = EriFiles.GetEriCodeAndDescr("MeasureEquipmentType");

            foreach (KeyValuePair<string, string> kvp in eri)
            {
                _overallDictonary.Add(new ComboBoxDictionary<string, string>(kvp.Key, kvp.Value));
            }

            _overallDictonary.InitComboBox(cbEquipmentType);
            cbEquipmentType.DataBindings.Add("SelectedValue", _equipment, "EquipmentType", true);

            dtpCheckdate.DataBindings.Add("Value", _equipment, "Checkdate", true);
            txtName.DataBindings.Add("Text", _equipment, "Name", true);
            tbSerial.DataBindings.Add("Text", _equipment, "Serial", true);
            txtNote.DataBindings.Add("Text", _equipment, "Note", true);

            tbCertificate.Text = _equipment.Certificate.ToLongString();
        }

        public void SaveAppl()
        {
            _equipment.Save();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _equipment.Certificate.Id = IM.NullI;
            tbCertificate.Text = "";
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            string param = string.Format("([{0}] LIKE '%{1}%')", "SYMBOL", "");
            //if (string.IsNullOrEmpty(dopFilter) == false)
            //     param = string.Format("{0} AND ({1})", param, dopFilter);
            RecordPtr recordPtr = RecordPtr.UserSelect(CLocaliz.TxT("Select certificate for measure equipment"), PlugTbl.XvMsreqCert, "", "", OrderDirection.Ascending);
            if (recordPtr.Id == -1)
                MessageBox.Show("Добавление новых записей производите в соответствующем справочнике", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (recordPtr.Id != IM.NullI && recordPtr.Id != 0)
            {
                _equipment.Certificate.Id = recordPtr.Id;
                _equipment.Certificate.Load();

                tbCertificate.Text = _equipment.Certificate.ToLongString();
            }
            else
                MessageBox.Show("Невірний вибір", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);

            //return retRec;            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DateTime date2 = DateTime.Now.AddMonths(13);
            if (_equipment.Checkdate >= DateTime.Now && _equipment.Checkdate <= date2)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                if (
                    MessageBox.Show("перевірте правильність введення «Дати перевірки»", "Нагадування",
                                    MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                {
                    DialogResult = DialogResult.None;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
