﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class RailAntenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtBoxKoef = new System.Windows.Forms.TextBox();
            this.lblKoef = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtBoxWide = new System.Windows.Forms.TextBox();
            this.lblWide = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtBoxName
            // 
            this.txtBoxName.Location = new System.Drawing.Point(131, 12);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(130, 20);
            this.txtBoxName.TabIndex = 24;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 13);
            this.lblName.TabIndex = 23;
            this.lblName.Text = "Назва";
            // 
            // txtBoxKoef
            // 
            this.txtBoxKoef.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxKoef.Location = new System.Drawing.Point(131, 38);
            this.txtBoxKoef.Name = "txtBoxKoef";
            this.txtBoxKoef.Size = new System.Drawing.Size(130, 20);
            this.txtBoxKoef.TabIndex = 22;
            // 
            // lblKoef
            // 
            this.lblKoef.AutoSize = true;
            this.lblKoef.Location = new System.Drawing.Point(5, 41);
            this.lblKoef.Name = "lblKoef";
            this.lblKoef.Size = new System.Drawing.Size(120, 13);
            this.lblKoef.TabIndex = 21;
            this.lblKoef.Text = "Коефіцієнт підсилення";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(148, 90);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(40, 89);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 25;
            this.btnOk.Text = "Прийняти";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // txtBoxWide
            // 
            this.txtBoxWide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxWide.Location = new System.Drawing.Point(131, 64);
            this.txtBoxWide.Name = "txtBoxWide";
            this.txtBoxWide.Size = new System.Drawing.Size(130, 20);
            this.txtBoxWide.TabIndex = 28;
            // 
            // lblWide
            // 
            this.lblWide.AutoSize = true;
            this.lblWide.Location = new System.Drawing.Point(5, 67);
            this.lblWide.Name = "lblWide";
            this.lblWide.Size = new System.Drawing.Size(65, 13);
            this.lblWide.TabIndex = 27;
            this.lblWide.Text = "Ширина ДС";
            // 
            // RailAntenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 117);
            this.Controls.Add(this.txtBoxWide);
            this.Controls.Add(this.lblWide);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtBoxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtBoxKoef);
            this.Controls.Add(this.lblKoef);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(273, 151);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(273, 151);
            this.Name = "RailAntenForm";
            this.Text = "RailAntenForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtBoxKoef;
        private System.Windows.Forms.Label lblKoef;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtBoxWide;
        private System.Windows.Forms.Label lblWide;
    }
}