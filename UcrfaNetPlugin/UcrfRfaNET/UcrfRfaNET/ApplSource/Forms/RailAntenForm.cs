﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class RailAntenForm : Form, INotifyPropertyChanged
    {
        public RailAntenna RailAnten;
        public RailAntenForm()
        {
            InitializeComponent();
        }

        #region Implementation of INotifyPropertyChanged
        /// <summary>
        /// Имя
        /// </summary>       
        public string NameEquip
        {
            get
            {
                return RailAnten.Name;
            }
            set
            {
                if (value != RailAnten.Name)
                {
                    RailAnten.Name = value;
                    NotifyPropertyChanged("NameEquip");
                }
            }
        }
        
        /// <summary>
        /// Ширина ДС
        /// </summary>   
        public double BeamWidth
        {
            get
            {
                return RailAnten.BeamWidth;
            }
            set
            {
                if (value != RailAnten.BeamWidth)
                {
                    RailAnten.BeamWidth = value;
                    NotifyPropertyChanged("BeamWidth");
                }
            }
        }

        /// <summary>
        /// Коефіцієнт підсилення
        /// </summary>   
        public double Gain
        {
            get
            {
                return RailAnten.Gain;
            }
            set
            {
                if (value != RailAnten.Gain)
                {
                    RailAnten.Gain = value;
                    NotifyPropertyChanged("Gain");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public RailAntenForm(int id)
        {
            RailAnten = new RailAntenna();
            RailAnten.Id = id;
            RailAnten.Load();
            InitializeComponent();
            Text = id != IM.NullI ? "Редагування запису за номером ID: " + id : "Створення нового запису";
            //Привязка 
            txtBoxName.DataBindings.Add("Text", this, "NameEquip");
            txtBoxKoef.DataBindings.Add("Text", this, "Gain");
            txtBoxWide.DataBindings.Add("Text", this, "BeamWidth");
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void Save()
        {
            if (RailAnten != null)
                RailAnten.Save();
        }
    }
}
