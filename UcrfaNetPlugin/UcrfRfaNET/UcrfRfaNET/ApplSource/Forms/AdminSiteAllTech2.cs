﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;
using XICSM.UcrfRfaNET.Map;
using Lis.CommonLib.Binding;
using IdwmNET;
using NearestCountryItem = IdwmNET.NearestCountryItem;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using System.Runtime.InteropServices;
using System.Text;
using System.Reflection;
using System.Windows.Forms.ComponentModel;
using OrmCs;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Форма для редагування записів сайтів всіх радіослужб і прив'язки до адмінсайтів
    /// </summary>
    public partial class AdminSiteAllTech2 : FBaseFormClear, INotifyPropertyChanged
    {
        public const uint WS_DISABLED = 0x08000000;
        public const int WM_ENABLE = 0x000A;
        public const int WM_COMMAND = 0x0111;
        public const int WM_CLOSE = 0x0010;
        public const int ES_READONLY = 0x0800;
        private const int SW_HIDE = 0x00;
        private const int SW_SHOW = 0x05;
        private const int WS_EX_APPWINDOW = 0x40000;
        private const int GWL_EXSTYLE = -0x14;
        private const int WS_EX_TOOLWINDOW = 0x0080;
        public Timer tx_ { get; set; }
        delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        static extern void SetActiveWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool EnableWindow(IntPtr hWnd, bool bEnable);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int PostMessage(IntPtr hWnd, int uMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern long GetWindowLong(IntPtr hWnd, int index);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll")]
        static extern bool InvalidateRect(IntPtr hWnd, IntPtr lpRect, bool bErase);



        


        string GetWindowText(IntPtr hWnd)
        {
            int len = GetWindowTextLength(hWnd) + 1;
            StringBuilder sb = new StringBuilder(len);
            len = GetWindowText(hWnd, sb, len);
            return sb.ToString(0, len);
        }

        private void Timer_Update(object Sender, EventArgs e)
        {
            SetDeactiveWindow();
        }


        private bool SetDeactiveWindow()
        {
            bool isFind = false;
            EnumWindows((hWnd, lParam) =>
            {
                if (IsWindowVisible(hWnd) && GetWindowTextLength(hWnd) != 0)
                {
                    if ((GetWindowText(hWnd).ToString().Contains("Sites for broadcast LF/MF/VHF/UHF")) ||
                        (GetWindowText(hWnd).ToString().Contains("ВФСР СРРЗ сайти")) ||
                        (GetWindowText(hWnd).ToString().Contains("ВРС РМКВ сайти")) ||
                        (GetWindowText(hWnd).ToString().Contains("Сайти нотифікації")) ||
                        (GetWindowText(hWnd).ToString().Contains("ВФСР ССРТ сайти")) ||
                        (GetWindowText(hWnd).ToString().Contains("ВРР сайти")) ||
                        (GetWindowText(hWnd).ToString().Contains("ВФСР ССЗ сайти")) ||
                        (GetWindowText(hWnd).ToString().Contains("Сайти n")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for earth stations")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for earth stations aliases")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for broadcast stations aliases")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for FWA stations")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for FWA stations aliases")) ||
                        (GetWindowText(hWnd).ToString().Contains("Sites for mobile and other terrestrial stations")))
                    {
                        // SetActiveWindow(hWnd);

                        if (IsWindowVisible(hWnd))
                        {
                            IntPtr ButtonHandle_1_E = FindWindowEx(hWnd, IntPtr.Zero, null, "Save and exit");
                            IntPtr ButtonHandle_2_E = FindWindowEx(hWnd, IntPtr.Zero, null, "Save changes");
                            IntPtr ButtonHandle_1_U = FindWindowEx(hWnd, IntPtr.Zero, null, "Зберегти і вийти");
                            IntPtr ButtonHandle_2_U = FindWindowEx(hWnd, IntPtr.Zero, null, "Зберегти зміни");

                            long Style_1_E = GetWindowLong(ButtonHandle_1_E, -16);
                            Style_1_E |= WS_DISABLED;
                            SetWindowLong(ButtonHandle_1_E, -16, Style_1_E);

                            long Style_2_E = GetWindowLong(ButtonHandle_2_E, -16);
                            Style_2_E |= WS_DISABLED;
                            SetWindowLong(ButtonHandle_2_E, -16, Style_2_E);

                            long Style_1_U = GetWindowLong(ButtonHandle_1_U, -16);
                            Style_1_U |= WS_DISABLED;
                            SetWindowLong(ButtonHandle_1_U, -16, Style_1_U);
                            long Style_2_U = GetWindowLong(ButtonHandle_2_U, -16);
                            Style_2_U |= WS_DISABLED;
                            SetWindowLong(ButtonHandle_2_U, -16, Style_2_U);


                            //SetWindowLong(ButtonHandle_1_E, GWL_EXSTYLE, WS_EX_APPWINDOW);
                            //ShowWindow(ButtonHandle_1_E, SW_HIDE);

                            //SetWindowLong(ButtonHandle_2_E, GWL_EXSTYLE, WS_EX_APPWINDOW);
                            //ShowWindow(ButtonHandle_2_E, SW_HIDE);

                            //SetWindowLong(ButtonHandle_1_U, GWL_EXSTYLE, WS_EX_APPWINDOW);
                            //ShowWindow(ButtonHandle_1_U, SW_HIDE);

                            //SetWindowLong(ButtonHandle_2_U, GWL_EXSTYLE, WS_EX_APPWINDOW);
                            //ShowWindow(ButtonHandle_2_U, SW_HIDE);

                            EnableWindow(ButtonHandle_1_E, true);
                            EnableWindow(ButtonHandle_1_E, false);

                            EnableWindow(ButtonHandle_2_E, true);
                            EnableWindow(ButtonHandle_2_E, false);

                            EnableWindow(ButtonHandle_1_U, true);
                            EnableWindow(ButtonHandle_1_U, false);

                            EnableWindow(ButtonHandle_2_U, true);
                            EnableWindow(ButtonHandle_2_U, false);
                          
                            //if (_cnt < 3)
                                    //InvalidateRect(hWnd, IntPtr.Zero, true);
                            

                            //_cnt++;
                            isFind = true;
                        }
                    }

                    
                }
                return true;
            }, IntPtr.Zero);
            return isFind;
        }


        /// <summary>
        /// Отображает форму сайтов для создания новой позиции
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="newPos">Класс новой позиции</param>
        /// <param name="ownerWind">Владелец окна</param>
        public static DialogResult CreateNewPosition(string tableName, ref PositionState2 newPos, IWin32Window ownerWind)
        {
            if (IM.TableRight(tableName, IMTableRight.Insert))
            {
                if (newPos == null)
                    newPos = new PositionState2();
                newPos.TableName = tableName;
                newPos.Id = IM.NullI;
                using (AdminSiteAllTech2 frm = new AdminSiteAllTech2(newPos))
                {
                    
                    frm._createNewPosition = true;
                    //frm._posAdmSite.LoadStatePosition(newPos);
                    DialogResult dr = frm.ShowDialog(ownerWind);
                    if (dr == DialogResult.OK)
                        newPos = frm._posAdmSite;
                    return dr;
                }
            }
            else
                return DialogResult.Cancel;
        }
        /// <summary>
        /// Отображает форму сайтов
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">ID записи</param>
        /// <param name="ownerWind">Владелец окна</param>
        public static void ShowReadOnly(string tableName, int id, IWin32Window ownerWind)
        {
            Show(tableName, id, true, ownerWind);
        }

        /// <summary>
        /// Отображает форму сайтов для заданной таблицы и ИД
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">ID записи</param>
        /// <param name="isReadOnly">Только для чтения</param>
        /// <param name="ownerWind">Владелец окна</param>
        public static DialogResult Show(string tableName, int id, bool isReadOnly, IWin32Window ownerWind)
        {
            if (string.IsNullOrEmpty(tableName) || id == IM.NullI || id == 0)
            {
                const string msgText = "Failed: tableName is empty or Id is null or Id = 0";
                MessageBox.Show(CLocaliz.TxT(msgText), CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return DialogResult.None;
            }
            
            if ((IM.TableRight(tableName) & IMTableRight.Update) != IMTableRight.Update)
                isReadOnly = true;

            using (AdminSiteAllTech2 frm = new AdminSiteAllTech2(tableName, id))
            {
                if (tableName == "SITES" && !isReadOnly && (frm._posAdmSite.Status.ToUpper() == "OK"))
                    isReadOnly = true;
            
                frm.IsEnableChange = !isReadOnly;
                return frm.ShowDialog(ownerWind);
            }
   
        }
        //-----------
        private Idwm _idwmVal;
        private bool _isIdwmInit;
        private bool _createNewPosition = false;
        private PositionAdmSite2 _posAdmSite;
        //private BindingVar bindVar;
        private List<string> _photoList;
        private List<string> _TypeObjectList;
        private SelectAddressControlFast2 _cf;
        private ShortAddressControl _shortAddresCtr;
        private LisMapWf.LisMapWfControl _mapcl;
        private List<string> _delphotoList = new List<string>();
        private string _savedProv = "";
        private string _savedSubProv = "";
        private string _savedCity = "";
        private string _savedTypedCity = "";
        private string _firstLat;
        private string _firstLong;
        private bool _isSites;

        private bool _isCalcAsl = true;
        private bool IsCalcAsl
        {
            get { return _isCalcAsl; }
            set
            {
                if(_isCalcAsl != value)
                {
                    _isCalcAsl = value;
                    txtBoxAsl.ForeColor = _isCalcAsl ? Color.Red : Color.Black;
                    btnMapToAsl.BackgroundImage = _isCalcAsl ? Properties.Resources.questionmark : Properties.Resources.ok;
                }
            }
        }

        private bool _isEnableChange = true;
        public bool ReadOnly { get { return !_isEnableChange; } }

        public const string FieldIsEnableChange = "IsEnableChange";
        /// <summary>
        /// Только для чтения
        /// </summary>
        public bool IsEnableChange
        {
            get { return _isEnableChange; }
            set
            {
                if (_isEnableChange != value)
                {
                    _isEnableChange = value;
                    RaisePropertyChanged(FieldIsEnableChange);
                    _cf.CanEditPosition = IsEnableChange;
                    _shortAddresCtr.CanEdit = IsEnableChange;
                }
            }
        }

        private ComboBoxDictionaryList<string, string> _cmbFoto = new ComboBoxDictionaryList<string, string>();
        
        private ComboBoxDictionaryList<string, string> _cmbTypeObject = new ComboBoxDictionaryList<string, string>();
        
        /// <summary>
        /// Конструктор //+
        /// </summary>
        private AdminSiteAllTech2()
        {
            InitializeComponent();
            //----
            _idwmVal = new Idwm();
            try
            {
                _isIdwmInit = _idwmVal.Init(11); //Init IDWM
            }
            catch
            {
                _isIdwmInit = false;
            }
            _posAdmSite = new PositionAdmSite2();
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="tableName">Имя таблицы позиции</param>
        /// <param name="id">Id записи</param>
        private AdminSiteAllTech2(string tableName, int id)
            : this()
        {
            _posAdmSite.LoadStatePosition(id, tableName);
            StreetChangeForm.ID = id;
            StreetChangeForm.TableName = tableName;
            InitForm();
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="pos">Класс позиции</param>
        private AdminSiteAllTech2(PositionState2 pos)
            : this()
        {
            _posAdmSite = new PositionAdmSite2();
            _posAdmSite.LoadStatePosition(pos);
            StreetChangeForm.ID = _posAdmSite.Id;
            StreetChangeForm.TableName = _posAdmSite.TableName;
            InitForm();
        }

        private void InitForm()
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading position")))
            {
                button_set_layer.Text = CLocaliz.TxT("Save settings");
                pb.SetProgress(0, 4);
                pb.SetBig(CLocaliz.TxT(CLocaliz.TxT("Modules binding")));
                pb.SetSmall("");
                BindCaption();
                if (_posAdmSite.AdminSiteId == IM.NullI)
                    _posAdmSite.CountryId = "UKR";
                pb.SetBig(CLocaliz.TxT(CLocaliz.TxT("Address loading")));
                pb.Increment(false);
                FillAdminSite(_posAdmSite.AdminSiteId);
                FillAddress();
                //---
                _shortAddresCtr = new ShortAddressControl();
                _shortAddresCtr.Init(pnlShortAddress);
                _shortAddresCtr.StreetType = _posAdmSite.StreetType;
                _shortAddresCtr.StreetName = _posAdmSite.StreetName;
                _shortAddresCtr.HouseNumber = _posAdmSite.BuildingNumber;
                _shortAddresCtr.Other = _posAdmSite.Remark;
                
               

                _shortAddresCtr.PropertyChanged += ShortAddresCtr_PropertyChanged;
                TypeObject.SelectedIndexChanged+=new EventHandler(TypeObject_SelectedIndexChanged);
                
                //---
                pb.SetBig(CLocaliz.TxT(CLocaliz.TxT("Map loading")));
                pb.Increment(false);
                FillMap(false);
                FillCoatuu();

                _isSites = (_posAdmSite.TableName == ICSMTbl.SITES);
                btnAccept.Visible = _isSites;

                CheckAdmStatus();
                
                pb.SetBig(CLocaliz.TxT(CLocaliz.TxT("Border calculating")));
                pb.Increment(false);
                CalcBorder();

                pb.SetBig(CLocaliz.TxT(CLocaliz.TxT("Photo loading")));
                pb.Increment(false);
                FillFoto();

                _firstLat = txtBoxLatLow.Text;
                _firstLong = txtBoxLongLow.Text;
                //----
                // ReadOnly
                txtBoxLatLow.DataBindings.Add("ReadOnly", this, "ReadOnly");
                txtBoxLongLow.DataBindings.Add("ReadOnly", this, "ReadOnly");
                btnMapToAsl.DataBindings.Add("Enabled", this, FieldIsEnableChange);
                txtBoxAsl.DataBindings.Add("ReadOnly", this, "ReadOnly");
                btnChangeAdmSite.DataBindings.Add("Enabled", this, FieldIsEnableChange);
                btnOffAdmSite.DataBindings.Add("Enabled", this, FieldIsEnableChange);
                tbOldAddress.DataBindings.Add("ReadOnly", this, "ReadOnly");
                btnAdd.DataBindings.Add("Enabled", this, FieldIsEnableChange);
                btndel.DataBindings.Add("Enabled", this, FieldIsEnableChange);

                //----
                Binding tmpBindingIsChange = new Binding("Enabled", _posAdmSite, "IsChanged");
                tmpBindingIsChange.Format += IsEnableChange_Format;
                btnSave.DataBindings.Add(tmpBindingIsChange);
                tmpBindingIsChange = new Binding("Enabled", _posAdmSite, "IsChanged");
                tmpBindingIsChange.Format += IsEnableChange_Format;
                btnSaveExit.DataBindings.Add(tmpBindingIsChange);
                //---
                tbOldAddress.DataBindings.Add("Text", _posAdmSite, "FullOldAddress");
                
                txtCreatedBy.DataBindings.Add("Text", _posAdmSite, "CreatedBy");
                txtCreatedDate.DataBindings.Add("Text", _posAdmSite, "DateCreated");
                txtCreatedDate.DataBindings[0].Format += FormatDateTime;
                txtModifiedBy.DataBindings.Add("Text", _posAdmSite, "ModifiedBy");
                txtModifiedDate.DataBindings.Add("Text", _posAdmSite, "DateModified");
                txtModifiedDate.DataBindings[0].Format += FormatDateTime;

                //btnError.DataBindings.Add("Enabled", btnAccept, "Enabled");
                //btnError.DataBindings.Add("Visible", btnAccept, "Visible"); 
                
                // Изменения в КОАТУУ
                _cf.CityChangedEvent += CityWasChangedEvent;
                //_cf.TypeCityChangedEvent += CityWasChangedEvent;
                _shortAddresCtr.NasPunct =_cf.CurrentCity.cityName;
                _shortAddresCtr.Koatuu = _cf.CurrentCity.Code;

                // новые контролы
                FillTypeObject();
            }
        }

        private void CheckAdmStatus()
        {
            bool boolCheck;
            bool boolAccept;
            string status = _posAdmSite.CheckStatus(out boolCheck, out boolAccept);
            btnCheck.Enabled = boolCheck && string.IsNullOrEmpty(_posAdmSite.AcceptUser) && (status != CAndE.nic);
            btnAccept.Enabled = boolAccept;

            picAdmStatus.Image = (status == CAndE.Need || status == CAndE.ReqP) ? global::XICSM.UcrfRfaNET.Properties.Resources.questionmark :
                (status == CAndE.Chkd || status == CAndE.OkP) ? global::XICSM.UcrfRfaNET.Properties.Resources.ok :
                (status == CAndE.Error) ? global::XICSM.UcrfRfaNET.Properties.Resources.cancel :
                null;

            /*
            if (_posAdmSite.AdminSiteId == IM.NullI) {
                checkBox_special_check.Visible = false; label7.Visible = false;  }
            else
            {
                label7.Visible = true; 
                checkBox_special_check.Visible = true;
                if (status == CAndE.nic) { checkBox_special_check.Checked = true; } else { checkBox_special_check.Checked = false; }
            }
             */
            int isNicStatus = 0;
            YXfaSiteNic site_ = new YXfaSiteNic();
            if (site_.Fetch(string.Format("([OBJ_ID]={0}) AND ([OBJ_TABLE]={1})", _posAdmSite.Id, _posAdmSite.TableName.ToSql())))
            {
                if (site_.m_nic == 1)
                    isNicStatus = site_.m_nic;
            }
            if ((status == CAndE.nic) || (isNicStatus == 1))
            {
                btnCheck.Enabled = false;
                checkBox_special_check.Checked = true;
                btnAccept.Enabled = false;
            }
            else {
                btnCheck.Enabled = true;
                btnAccept.Enabled = true;
                checkBox_special_check.Checked = false; }
            
            lblAcceptDateVal.DataBindings[0].ReadValue();
            lblAcceptNameVal.DataBindings[0].ReadValue();
            lblCheckDateVal.DataBindings[0].ReadValue();
            lblCheckNamVal.DataBindings[0].ReadValue();
        }

        
        /// <summary>
        /// OBJ->Control
        /// </summary>
        private void IsEnableChange_Format(object sender, ConvertEventArgs e)
        {
            bool val = (bool)e.Value;
            e.Value = val /*& !_createNewPosition*/ & IsEnableChange;
        }
        /// <summary>
        /// Отображаем форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminSiteAllTech_Load(object sender, EventArgs e)
        {
            //btnOk.Enabled &= _createNewPosition;
            //btnOk.Visible = btnOk.Enabled;
            //btnCancel.Enabled &= _createNewPosition;
            //btnCancel.Visible = btnCancel.Enabled;
            //----
            //btnCancelExit.Enabled &= !_createNewPosition;
            //btnCancelExit.Visible = btnCancelExit.Enabled;
            //btnSaveExit.Visible = !_createNewPosition;
            //btnSave.Visible = !_createNewPosition;
            grpBoxPhoto.Enabled = !_createNewPosition;
            //--
            UpdateFullAddress();
            //--
            this.Text = Caption();
            IsCalcAsl = _posAdmSite.Asl == IM.NullD;
           
            FillTypeObject();
        }

        private string Caption()
        {
            return string.Format("{0}: [{1}] {2} ({3})",
                CLocaliz.TxT("Site"), _posAdmSite.TableName, (_posAdmSite.Id == IM.NullI) ? "" : "#" + _posAdmSite.Id.ToString(),
                _createNewPosition ? CLocaliz.TxT("Creating new one") : CLocaliz.TxT("Browse/Edit"));
        }
        /// <summary>
        /// Изменились поля короткого адресса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ShortAddresCtr_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case ShortAddressControl.FieldStreetType:
                    _posAdmSite.StreetType = _shortAddresCtr.StreetType;
                    UpdateFullAddress();
                    break;
                case ShortAddressControl.FieldStreetName:
                    _posAdmSite.StreetName = _shortAddresCtr.StreetName;
                    UpdateFullAddress();
                    break;
                case ShortAddressControl.FieldHouseNumber:
                    _posAdmSite.BuildingNumber = _shortAddresCtr.HouseNumber;
                    UpdateFullAddress();
                    break;
                case ShortAddressControl.FieldOther:
                    _posAdmSite.Remark = _shortAddresCtr.Other;
                    UpdateFullAddress();
                    break;
            }
            CheckAdmSiteDifference();
            if ((_posAdmSite.IsChanged) || (_shortAddresCtr.IsChanged)) { btnSave.Enabled = true; btnSaveExit.Enabled = true; }
        }
        /// <summary>
        /// Изменения в КОАТУУ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CityWasChangedEvent(object sender, SelectAddressControlFast2.CityEventArgs e)
        {
            UpdateFullAddress();
            _posAdmSite.Subprovince = _cf.CurrentCity.subprov;
            _posAdmSite.Province = _cf.CurrentCity.prov;
            _posAdmSite.TypeCity = _cf.CurrentCity.type;
            _posAdmSite.City = _cf.CurrentCity.cityName;
            _posAdmSite.CityId = _cf.CurrentCity.Id;
            FillCoatuu();
            CheckAdmSiteDifference();
            FillTypeObject();
            if ((_posAdmSite.IsChanged) || (_shortAddresCtr.IsChanged)) { btnSave.Enabled = true; btnSaveExit.Enabled = true; }
        }
        /// <summary>
        /// Обновляет поле текущего адреса
        /// </summary>
         void UpdateFullAddress()
        {
            //tbFullAddress.Text = PositionState2.CreateFullAddress(_cf.CurrentCity.Address, _cf.CurrentCity.type == "район" ? _shortAddresCtr.Other : _shortAddresCtr.GetAddress(), _posAdmSite.Tower);
            tbFullAddress.Text = PositionState2.CreateFullAddress(_cf.CurrentCity.Address, _cf.CurrentCity.type == "район" ? _shortAddresCtr.Other : _shortAddresCtr.GetAddress(), TypeObject.Text,true);
        }
        /// <summary>
        /// Калькуляція відстані до кордону //+
        /// </summary>
        private void CalcBorder()
        {
            if(!_isIdwmInit)
            {
                txtBoxLimit.Text = CLocaliz.TxT("IDWM not init");
                return;
            }
            if (_posAdmSite.X == IM.NullD || _posAdmSite.Y == IM.NullD)
            {
                txtBoxLimit.Text = CLocaliz.TxT("Wrong coordinate");
                return;
            }
            float lonDec = (float) _posAdmSite.LonDec;
            float latDec = (float) _posAdmSite.LatDec;
            string[] exclude = { "UKR" };
            NearestCountryItem[] nearestCountry = _idwmVal.GetNearestCountries(Idwm.DecToRadian(lonDec), Idwm.DecToRadian(latDec), 1000, exclude, 100);
            string outStr = "~";
            if (nearestCountry.Length > 0)
                outStr = string.Format("{0} Km ({1})", ((double) (nearestCountry[0].distance)).Round(2), nearestCountry[0].country);
            txtBoxLimit.Text = outStr;
        }

        private void FillAdminSite(int admId)
        {
            _posAdmSite.GetAdminSiteInfo(admId);
            if (admId != IM.NullI)
            {
                string longitAdm;
                string latitAdm;
                if (_posAdmSite.AdminSiteCsys == "4DEC")
                {
                    longitAdm = _posAdmSite.AdminSiteX.LongDECToString();
                    latitAdm = _posAdmSite.AdminSiteY.LatDECToString();
                }
                else if (_posAdmSite.AdminSiteCsys == "4DMS")
                {
                    longitAdm = _posAdmSite.AdminSiteX.LongDMSToString();
                    latitAdm = _posAdmSite.AdminSiteY.LatDMSToString();
                }
                else //?????
                {
                    longitAdm = _posAdmSite.AdminSiteX.DecToDms().LongDMSToString() + " ???";
                    latitAdm = _posAdmSite.AdminSiteY.DecToDms().LatDMSToString() + " ???";
                }

                txtBoxAdmSite.Text = latitAdm + " - " + longitAdm + Environment.NewLine + _posAdmSite.AdminSiteAddress;
            }
            else
                txtBoxAdmSite.Text = "";

            CheckAdmSiteDifference();
        }

        private void SetStatusAdmSite()
        {
            YSites ys = new YSites();
            ys.Format("*");
            if (ys.Fetch(_posAdmSite.AdminSiteId)) {
                ys.m_status = _posAdmSite.Status;
                ys.Save();
            }
        }

        private void CheckAdmSiteDifference()
        {
            lblLat.ForeColor = _posAdmSite.LatDiffersFromAdm ? Color.Red : SystemColors.WindowText;
            lblLat.Font = new Font(lblLat.Font, _posAdmSite.LatDiffersFromAdm ? lblLat.Font.Style | FontStyle.Bold : lblLat.Font.Style & ~(FontStyle.Bold));
            lblLong.ForeColor = _posAdmSite.LonDiffersFromAdm ? Color.Red : SystemColors.WindowText;
            lblLong.Font = new Font(lblLong.Font, _posAdmSite.LonDiffersFromAdm ? lblLong.Font.Style | FontStyle.Bold : lblLong.Font.Style & ~(FontStyle.Bold));
            tbFullAddress.ForeColor = _posAdmSite.AddrDiffersFromAdm ? Color.Red : SystemColors.WindowText;
            tbFullAddress.Font = new Font(tbFullAddress.Font, _posAdmSite.AddrDiffersFromAdm ? tbFullAddress.Font.Style | FontStyle.Bold : tbFullAddress.Font.Style & ~(FontStyle.Bold));

            btAddrLikeAdmSiteHas.Visible = _posAdmSite.AdminSiteId != IM.NullI && _posAdmSite.AddrDiffersFromAdm && IsEnableChange;
            btCoordLikeAdmSiteHas.Visible = _posAdmSite.AdminSiteId != IM.NullI && (_posAdmSite.LatDiffersFromAdm || _posAdmSite.LonDiffersFromAdm) && IsEnableChange;
            /*
            if (_posAdmSite.AdminSiteId == IM.NullI) {
                checkBox_special_check.Visible = false; label7.Visible = false;
            }
            else {
                label7.Visible = true;
                checkBox_special_check.Visible = true;
                bool boolCheck; bool boolAccept;
                _posAdmSite.Status = _posAdmSite.CheckStatus(out boolCheck, out boolAccept);
                if (_posAdmSite.Status == CAndE.nic) { checkBox_special_check.Checked = true; } else { checkBox_special_check.Checked = false; }
            }
             */
            
            int isNicStatus = 0;
            YXfaSiteNic site_ = new YXfaSiteNic();
            if (site_.Fetch(string.Format("([OBJ_ID]={0}) AND ([OBJ_TABLE]={1})", _posAdmSite.Id, _posAdmSite.TableName.ToSql()))) {
                if (site_.m_nic==1)
                    isNicStatus = site_.m_nic;
            }
            if ((_posAdmSite.Status == CAndE.nic) || (isNicStatus==1)) {
                btnCheck.Enabled = false;
                btnAccept.Enabled = false;
                checkBox_special_check.Checked = true;
            } else {
                btnCheck.Enabled = true;
                btnAccept.Enabled = true;
                checkBox_special_check.Checked = false; }
        }
        
        private void UpdateListComboFoto()
        {
            _cmbFoto.Clear();
            for (int i = 0; i < _photoList.Count; i++)
                _cmbFoto.Add(new ComboBoxDictionary<string, string>((_photoList[i].IndexOf("\\\\") == -1) ? HelpFunction.ReadDataFromSysConfig("SHDIR-DOC") + "\\" + _photoList[i] : _photoList[i], System.IO.Path.GetFileName(_photoList[i])));
              
            // pctBox.Refresh();            
        }

        private void FillFoto()
        {
            _cmbFoto.Clear();
            _photoList = _posAdmSite.GetFotoFromCitiesNew();
            UpdateListComboFoto();
            _cmbFoto.InitComboBox(cmbBoxPhoto);
            if (string.IsNullOrEmpty(cmbBoxPhoto.Text))
                if (_cmbFoto.Count > 0)
                    cmbBoxPhoto.Text = _cmbFoto[0].Key;
            pctBox.DataBindings.Add("ImageLocation", _cmbFoto, "Key");
            pctBox.SizeMode = PictureBoxSizeMode.StretchImage;
        }


        /// <summary>
        /// Вертає список типів споруд
        /// </summary>
        /// <returns></returns>
        public List<string> GetTypeSporuda()
        {
            List<string> tmpList = new List<string>();
            IMRecordset COMBO_T = new IMRecordset("COMBO", IMRecordset.Mode.ReadOnly);
            COMBO_T.Select("DOMAIN,ITEM,LANG");
           
            
             try
             {
                 tmpList.Add("");
                 for (COMBO_T.Open(); !COMBO_T.IsEOF(); COMBO_T.MoveNext())
                 {
                     
                     if ((COMBO_T.GetS("DOMAIN")=="STATION_TOWER") && (COMBO_T.GetS("LANG")=="AAB"))
                     {
                     tmpList.Add(COMBO_T.GetS("ITEM"));
                     }
                 }
                
             }
             finally
             {
                 COMBO_T.Final();
            
             }
              
            return tmpList.Distinct().ToList();
        }


        // управление обновлением поля "Тип сооружения"
        private void UpdateListComboTypeObject()
        {
            _cmbTypeObject.Clear();
             for (int i = 0; i < _TypeObjectList.Count; i++)
             _cmbTypeObject.Add(new ComboBoxDictionary<string, string>(i.ToString(), _TypeObjectList[i]));
        }

        // заполнение типами сооружений
        private void FillTypeObject()
        {
            bool STATUS = false;
            _cmbTypeObject.Clear();
            _TypeObjectList = GetTypeSporuda();
            UpdateListComboTypeObject();
            _cmbTypeObject.InitComboBox(TypeObject);
            

            string tmp1 = _posAdmSite.Tower.ToString().Trim().ToLower();
            string tmp2 = "";

            
            if (_createNewPosition)
            {
                
                // поиск соответствия
                for (int i = 0; i < TypeObject.Items.Count; i++)
                {
                    tmp2 = _cmbTypeObject[i].Description.ToString().Trim().ToLower().Trim();
                    if (tmp2 == "будівля")
                    {
                        TypeObject.SelectedIndex = i;
                        STATUS = true;
                        break;
                    }
                }
            }
            else
            {

                // поиск соответствия
                for (int i = 0; i < TypeObject.Items.Count; i++)
                {
                    tmp2 = _cmbTypeObject[i].Description.ToString().Trim().ToLower();
                    if (tmp2 == tmp1)
                    {
                        TypeObject.SelectedIndex = i;
                        STATUS = true;
                        break;
                    }
                }
            }
             
          
          
        }


        private void FillAddress()
        {
            _cf = new SelectAddressControlFast2();
            pnlAddress.Controls.Add(_cf);
            _cf.Dock = DockStyle.Fill;
            _cf.InitializeByCityId(_posAdmSite.CityId);
            _cf.CanEditPosition = true;
            if (_cf.CurrentCity != null)
            {
                _savedCity = _cf.CurrentCity.cityName;
                _savedProv = _cf.CurrentCity.prov;
                _savedTypedCity = _cf.CurrentCity.type;
                _savedSubProv = _cf.CurrentCity.subprov;
            }

              IMRecordset rsStationD = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
              rsStationD.Select("ID,CODE,ADM_CODE");
              rsStationD.SetWhere("ID", IMRecordset.Operation.Eq, _cf.CurrentCity.Id);
              rsStationD.Open();
              if (!rsStationD.IsEOF())
                {
                    //if (StreetChangeForm.Load(_posAdmSite.StreetType, _posAdmSite.StreetName, _cf.CurrentCity.Code))
                    if (StreetChangeForm.Load(_posAdmSite.StreetType, _posAdmSite.StreetName, rsStationD.GetS("CODE")))
                    {
                        pnlShortAddress.BackColor = System.Drawing.Color.FromArgb(236, 183, 0);
                    }
                }
              rsStationD.Close();
              rsStationD.Destroy();
                
            

          
        }
        /// <summary>
        /// Заполняет форму отображения Админ сайта
        /// </summary>
        private void FillCoatuu()
        {
            string Code_Koatu = "";
            string Name = "";
            string Prov = "";
            string SubProv = "";
            IMRecordset rsStationD = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
              rsStationD.Select("ID,CODE,ADM_CODE,NAME,PROVINCE,SUBPROVINCE");
              rsStationD.SetWhere("ID", IMRecordset.Operation.Eq, _cf.CurrentCity.Id);
              rsStationD.Open();
              if (!rsStationD.IsEOF())
              {
                  Code_Koatu = rsStationD.GetS("CODE");
                  Name = rsStationD.GetS("NAME");
                  Prov = rsStationD.GetS("PROVINCE");
                  SubProv = rsStationD.GetS("SUBPROVINCE");
                  if ((rsStationD.GetS("ADM_CODE").Contains("_")) || (rsStationD.GetS("CODE").Contains("_"))) txtBoxKoatuu.BackColor = Color.Aqua;
              }
              rsStationD.Close();
              rsStationD.Destroy();

            //if ((_cf.CurrentCity.AdmCode.Contains("_")) || (_cf.CurrentCity.Code.Contains("_"))) txtBoxKoatuu.BackColor = Color.Aqua;// System.Drawing.Color.FromArgb(236, 183, 0);
            if (_cf.CurrentCity.type == "область")
                txtBoxKoatuu.Text = string.Format("Область = {0} ({1})", Name, Code_Koatu);
            else
            {
                txtBoxKoatuu.Text = string.Format("Область = {1}{0}", Environment.NewLine, Prov);
                if (_cf.CurrentCity.type == "район")
                {
                    txtBoxKoatuu.Text += string.Format("Район = {0} ({1})", Name, Code_Koatu);
                }
                else
                {
                    txtBoxKoatuu.Text += string.Format("Район (м.р.) = {1}{0}", Environment.NewLine, SubProv);
                    txtBoxKoatuu.Text += string.Format("Нас.пункт = {0} ({1})", Name, Code_Koatu);
                }
            }
        }

        /// <summary>
        /// Метод для считывания параметра, хранящего номер текущего слоя выбранного пользователем для отображения на карте
        /// </summary>
        /// <param name="NameXML"></param>
        /// <returns></returns>
        private List<int> GetIndexMapLayer(string NameXML)
        {
            List<int> Mass = new List<int>();
            int CurrIndex = 1;
            int ValIndex = -1;
            try
            {
                XmlTextReader XmlReader = new XmlTextReader(NameXML);
                while (XmlReader.Read())
                {

                    if (XmlReader.NodeType == XmlNodeType.Element)
                    {

                        if (XmlReader.Name.Equals("IndexLayer" + CurrIndex.ToString()))
                        {
                            string element = XmlReader.ReadElementString("IndexLayer" + CurrIndex.ToString());
                            if (!string.IsNullOrEmpty(element)) ValIndex = Convert.ToInt32(element);
                            //MessageBox.Show(ValIndex.ToString());
                            if (!Mass.Contains(ValIndex)) Mass.Add(ValIndex);
                            ++CurrIndex;
                        }
                    }
                }
                XmlReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in [GetIndexMapLayer] method " + ex.Message);
            }
            return Mass;
        }
        /// <summary>
        /// Метод для сохранения параметра, хранящего номер текущего слоя выбранного пользователем для отображения на карте
        /// или создания файла XML если на данный момент его не существует
        /// </summary>
        /// <param name="NameXML"></param>
        /// <returns></returns>
        private bool SetIndexMapLayer(string NameXML, List<int> Numlayer)
        {
            bool res_operation = false;
            try
            {
               if (Numlayer!= null)
                {
                    if (Numlayer.Count > 0)
                    {
                        List<XElement> xel_n = new List<XElement>();
                        XElement xel = new XElement(NameXML, Encoding.UTF8);
                        xel.CreateWriter();
                        for (int i = 0; i < Numlayer.Count; i++)
                        {
                            xel_n.Add(new XElement("IndexLayer" + (i + 1).ToString(), Numlayer[i].ToString()));
                        }
                        var result = new XElement("Settings", xel_n);
                        result.Save(Environment.CurrentDirectory + @"\" + NameXML);
                        res_operation = true;
                        xel_n = null;
                        xel = null;
                        result = null;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error in [SetIndexMapLayer] method " + ex.Message);
            }
            return res_operation;
        }
        /// <summary>
        /// Метод, выполняющий оценку: если файл XML существует, то считываем параметр номер слоя, иначе создаем новый файл с номером слоя по умолчанию
        /// </summary>
        /// <param name="NameXML"></param>
        /// <param name="NumlayerDefault"></param>
        /// <returns></returns>
        private List<int> GetResIndexLayer(string NameXML, List<int> NumlayerDefault, bool status)
        {
            List<int> Res = new List<int>();
            if (!System.IO.File.Exists(Environment.CurrentDirectory + @"\" + NameXML))
            {
                bool res_op = SetIndexMapLayer(NameXML, NumlayerDefault);
                if (res_op) Res = GetIndexMapLayer(Environment.CurrentDirectory + @"\" + NameXML);
            }
            else
            {
                if (status)
                {
                    SetIndexMapLayer(NameXML, NumlayerDefault);
                }
                Res = GetIndexMapLayer(Environment.CurrentDirectory + @"\" + NameXML);
            }
            return Res;

        }


     

        
         /// <summary>
        /// Виведення карти
        /// </summary>
        private void SetIndexLayer(bool status)
        {
            try
            {
                List<int> Mass_Index = new List<int>();
                List<int> Mass_Result = new List<int>();

                if (_mapcl != null)
                {
                   
                    for (int i = 1; i <= _mapcl.Map.Layers.Count; i++)
                    {
                        if ((_mapcl.Map.Layers[i].Selectable)  &&  (_mapcl.Map.Layers[i].Visible))
                        {
                            Mass_Index.Add(i);
                        }
                    }

                    Mass_Result = GetResIndexLayer("SettingMapIndex.xml", Mass_Index, status);
                    if ((Mass_Result != null) && (_mapcl.Map.Layers.Count >= Mass_Result.Count))
                    {
                        for (int i = 0; i < Mass_Result.Count; i++)
                        {
                            _mapcl.Map.Layers[Mass_Result[i]].Selectable = true;
                            _mapcl.Map.Layers[Mass_Result[i]].Visible = true;
                            _mapcl.Update();
                            _mapcl.Refresh();
                        }
                    }
                    Mass_Index=null;
                    Mass_Result= null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, CLocaliz.Error);
            }
        }



        /// <summary>
        /// Виведення карти
        /// </summary>
        private void FillMap(bool status)
        {
            try
            {
                
                if (_mapcl == null)
                {
                    _mapcl = new LisMapWf.LisMapWfControl();
                    _mapcl.Size = new Size(380, 340);
                    grpBoxMap.Controls.Add(_mapcl);
                    _mapcl.Parent = grpBoxMap;
                    _mapcl.Dock = DockStyle.Fill;
                    _mapcl.Init();
                    SetIndexLayer(status);
                    _mapcl.Update();
                    
                }
                _mapcl.Clear(-1, false);
                if ((_posAdmSite != null) && (_posAdmSite.LatDec != IM.NullD) && (_posAdmSite.LonDec != IM.NullD)
                    && (_posAdmSite.LatDec >= -80) && (_posAdmSite.LonDec >= -180)
                    && (_posAdmSite.LatDec <= 80) && (_posAdmSite.LonDec <= 180))
                {
                    _mapcl.ShowStation(_posAdmSite.LonDec, _posAdmSite.LatDec, _posAdmSite.FullAddressAuto, "");
                    _mapcl.FitObjects();
               }
                else
                {
                    _mapcl.SetCenter(31.25, 48.60);
                    _mapcl.SetScale(1545.0);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, CLocaliz.Error);
            }
        }
      
        /// <summary>
        /// Прив'язка даних
        /// </summary>
        private void BindCaption()
        {
            //Latitude          
            Binding tmpBindingLatTxt = new Binding("Text", _posAdmSite, PositionState2.FieldLatDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLatTxt.BindingComplete += BindingCompleteEvent;
            tmpBindingLatTxt.Parse += (s, e) => ControlToCoordinate_Parse(e, txtBoxLatLow);
            tmpBindingLatTxt.Format += CoordinateToControl_Format;
            txtBoxLatLow.DataBindings.Add(tmpBindingLatTxt);

            Binding tmpBindingLatLab = new Binding("Text", _posAdmSite, PositionState2.FieldLatDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLatLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLatLab.Format += (s, e) =>
            {
                bool isCorect = true;
                double tmpVal = (double)ConvertType.ToDouble(e.Value.ToString(), IM.NullD);
                e.Value = "";
                if (tmpVal != IM.NullD)
                {
                    isCorect = HelpFunction.ValidateCoords(tmpVal, EnumCoordLine.Lat);
                    e.Value = HelpFunction.DmsToString(tmpVal, EnumCoordLine.Lat);
                    CalcBorder();
                }
                lblLat.BackColor = isCorect ? SystemColors.Control : Color.LightSalmon;
            };
            lblLat.DataBindings.Add(tmpBindingLatLab);

            //Longitude       
            Binding tmpBindingLongTxt = new Binding("Text", _posAdmSite, PositionState2.FieldLongDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLongTxt.BindingComplete += BindingCompleteEvent;
            tmpBindingLongTxt.Parse += (s, e) => ControlToCoordinate_Parse(e, txtBoxLongLow);
            tmpBindingLongTxt.Format += CoordinateToControl_Format;
            txtBoxLongLow.DataBindings.Add(tmpBindingLongTxt);

            Binding tmpBindingLongLab = new Binding("Text", _posAdmSite, PositionState2.FieldLongDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Format += (s, e) =>
            {
                bool isCorect = true;
                double tmpVal = (double)ConvertType.ToDouble(e.Value.ToString(), IM.NullD);
                e.Value = "";
                if (tmpVal != IM.NullD)
                {
                    isCorect = HelpFunction.ValidateCoords(tmpVal, EnumCoordLine.Lon);
                    e.Value = HelpFunction.DmsToString(tmpVal, EnumCoordLine.Lon);
                    CalcBorder();
                }
                lblLong.BackColor = isCorect ? SystemColors.Control : Color.LightSalmon;
            };
            lblLong.DataBindings.Add(tmpBindingLongLab);

            Binding tmpBindingAsl = new Binding("Text", _posAdmSite, PositionState2.FieldAsl, true, DataSourceUpdateMode.OnValidation);
            tmpBindingAsl.BindingComplete += BindingCompleteEvent;
            tmpBindingAsl.Format += FormatDouble;
            tmpBindingAsl.Parse += (s, e) =>
                                        {
                                            double tmpVal = ConvertType.ToDouble(e.Value.ToString(), IM.NullD);
                                            if (tmpVal == IM.NullD)
                                                tmpVal = IM.NullD;
                                            e.Value = tmpVal;
                                        };
            txtBoxAsl.DataBindings.Add(tmpBindingAsl);

            lblCheckNamVal.DataBindings.Add("Text", _posAdmSite, "SendUser");

            lblCheckDateVal.DataBindings.Add("Text", _posAdmSite, "SendDate");
            lblCheckDateVal.DataBindings[0].Format += FormatDateTime;

            lblAcceptNameVal.DataBindings.Add("Text", _posAdmSite, "AcceptUser");

            lblAcceptDateVal.DataBindings.Add("Text", _posAdmSite, "AcceptDate");
            lblAcceptDateVal.DataBindings[0].Format += FormatDateTime;    
            
            // привязка вспомогательных данных

            TypeObject.DataBindings.Add("Text", _posAdmSite, "TOWER");

            
            if ((_posAdmSite.TableName == ICSMTbl.SITES) || (_posAdmSite.TableName == ICSMTbl.lnkSites) || (_posAdmSite.TableName == ICSMTbl.Site) || (_posAdmSite.TableName == ICSMTbl.itblPositionEs) || (_posAdmSite.TableName == ICSMTbl.itblPositionBro) || (_posAdmSite.TableName == ICSMTbl.itblPositionFmn) || (_posAdmSite.TableName == ICSMTbl.itblPositionHf) || (_posAdmSite.TableName == ICSMTbl.itblPositionMob2) || (_posAdmSite.TableName == ICSMTbl.itblPositionMw) || (_posAdmSite.TableName == ICSMTbl.itblPositionWim))
            {
                   Cust_nbr1.DataBindings.Add("Text", _posAdmSite, "CUST_NBR1");
                   Cust_nbr2.DataBindings.Add("Text", _posAdmSite, "CUST_NBR2");
                   txt_owner.DataBindings.Add("Text", _posAdmSite, "NUMBER_BUILDING");
                   txt_number_building.DataBindings.Add("Text", _posAdmSite, "NAME_OWNER");
            }
        }

        public void FormatDateTime(object sender, ConvertEventArgs e)
        {
            e.Value = ((DateTime)e.Value == IM.NullT) ? "" : ((DateTime)e.Value).ToString();
        }

        public void FormatDouble(object sender, ConvertEventArgs e)
        {
            e.Value = ((double)e.Value == IM.NullD) ? "" : ((double)e.Value).ToString();
        }        

        /// <summary>
        /// OBJ->Control
        /// </summary>
        private void CoordinateToControl_Format(object sender, ConvertEventArgs e)
        {
            double val = (double) e.Value;
            if (val == IM.NullD)
                e.Value = "000000";
            else
                e.Value = CoordToCompon((double)e.Value);
        }
        /// <summary>
        /// OBJ->Control
        /// </summary>
        private void ControlToCoordinate_Parse(ConvertEventArgs e, Control ctr)
        {
            double val = ConvertType.StrToDMS(e.Value.ToString());
            e.Value = val;
            ctr.BackColor = (val != IM.NullD) ? SystemColors.Window : Color.LightSalmon;
            IsCalcAsl = true;
        }
        /// <summary>
        /// Координата -> Компонента
        /// </summary>
        private string CoordToCompon(double val)
        {
            val *= 10000.0;
            string tmpStr = val.Round(1).ToString();
            while (tmpStr.Length < 6)
                tmpStr = "0" + tmpStr;
            return tmpStr;
        }
        /// <summary>
        /// Отображает сообжение об ошибке при преобразовании
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingCompleteEvent(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteState != BindingCompleteState.Success)
                MessageBox.Show(e.ErrorText);
        }

        /// <summary>
        /// Реакція кнопки Зберегти і вийти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveExit_Click(object sender, EventArgs e)
        {
            if (checkBox_special_check.Checked) 
                Save(1);
            else Save(0);
            Close();
        }

        private void btnCancelExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (checkBox_special_check.Checked)
                Save(1);
            else Save(0);
            SetStatusAdmSite();
            CheckAdmSiteDifference();
        }
        /// <summary>
        /// Обработка нажатия клавиши
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Handled == false)
            {
                switch (e.KeyCode)
                {
                    case Keys.F5:
                        if (_createNewPosition)
                        {
                            btnOk.Focus();
                            PressedOkButton();
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            btnSave.Focus();
                            if (checkBox_special_check.Checked)
                                Save(1);
                            else Save(0);
                        }
                        e.Handled = true;
                        break;
                    case Keys.F8:
                        if (_createNewPosition == false)
                        {
                            btnSaveExit.Focus();
                            if (checkBox_special_check.Checked)
                                Save(1);
                            else Save(0);
                            Close();
                            e.Handled = true;
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Зберегти дані по станції
        /// </summary>
        public void Save(int Status_Nic)
        {
            if (_posAdmSite.Status == CAndE.Chkd)
            {
                return;
            }

            if (_isSites)
            {
                //DialogResult res = MessageBox.Show("Ви хочете каскадно обновити всі записи в сайтах",
                //                                   "Каскадне оновлення", MessageBoxButtons.YesNoCancel);
                //RecordPtr rec = new RecordPtr(ICSMTbl.SITES, _posAdmSite.Id);
                //if (_isAccept && _posAdmSite.IsChanged)
                //    if (res == DialogResult.Cancel)
                //        return;

                //if (res == DialogResult.Yes)
                //    PositionState.ChangeSites(rec, rec, true);
            }

            if (string.IsNullOrEmpty(_posAdmSite.AddressAuto) && MessageBox.Show("Адреса не заповнена. Продовжити?", "Підтвердження", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                throw new Exception("Перервано користувачем");

            if (IsCalcAsl)
                CalculateAsl();

            if (_cf != null)
                if (_cf.CurrentCity != null)
                {
                    string message = "";
                    if (!string.IsNullOrEmpty(_savedProv) && _savedProv != _cf.CurrentCity.prov)
                        message += String.Format("Поміняти область з '{0}' на '{1}' ? \r\n", _savedProv, _cf.CurrentCity.prov);
                    if (!string.IsNullOrEmpty(_savedSubProv) && _savedSubProv != _cf.CurrentCity.subprov)
                        message += String.Format("Поміняти район з '{0}' на '{1}' ? \r\n", _savedSubProv, _cf.CurrentCity.subprov);
                    if (!string.IsNullOrEmpty(_savedTypedCity) && _savedTypedCity != _cf.CurrentCity.type)
                        message += String.Format("Поміняти тип нас. пункту з '{0}' на '{1}' ? \r\n", _savedTypedCity, _cf.CurrentCity.type);
                    if (!string.IsNullOrEmpty(_savedCity) && _savedCity != _cf.CurrentCity.cityName)
                        message += String.Format("Поміняти назву населеного пункту з '{0}' на '{1}' ? \r\n", _savedCity, _cf.CurrentCity.cityName);
                    if (!string.IsNullOrEmpty(message))
                        if (MessageBox.Show(message, "Info", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            _posAdmSite.Subprovince = _cf.CurrentCity.subprov;
                            _posAdmSite.Province = _cf.CurrentCity.prov;
                            _posAdmSite.TypeCity = _cf.CurrentCity.type;
                            _posAdmSite.City = _cf.CurrentCity.cityName;
                            _savedProv = _cf.CurrentCity.prov;
                            _savedSubProv = _cf.CurrentCity.subprov;
                            _savedTypedCity = _cf.CurrentCity.type;
                            _savedCity = _cf.CurrentCity.cityName;
                            _posAdmSite.CityId = _cf.CurrentCity.Id;


                        }
                }
            _posAdmSite.SaveFotoCities(_photoList);

            for (int i = 0; i < _delphotoList.Count; i++)
                _posAdmSite.DeleteRecCities(_delphotoList[i]);

            if (_posAdmSite != null)
            {
                float lonDec = (float)_posAdmSite.LonDec;
                float latDec = (float)_posAdmSite.LatDec;
                string[] exclude = { "UKR" };
                NearestCountryItem[] nearestCountry = _idwmVal.GetNearestCountries(Idwm.DecToRadian(lonDec), Idwm.DecToRadian(latDec), 1000, exclude, 100);
                string outStr = "~";
                if (nearestCountry.Length > 0)
                    _posAdmSite.DistBorder = ((double)(nearestCountry[0].distance)).Round(2);

                _posAdmSite.Subprovince = _cf.CurrentCity.subprov;
                _posAdmSite.Province = _cf.CurrentCity.prov;
                _posAdmSite.TypeCity = _cf.CurrentCity.type;
                _posAdmSite.City = _cf.CurrentCity.cityName;
                _posAdmSite.CityId = _cf.CurrentCity.Id;

                _posAdmSite.Save(Status_Nic);
                SetStatusAdmSite();
                _posAdmSite.IsChanged = false;    
            }

            _createNewPosition = false;
            _isSaveWasCalled = true;

            this.Text = Caption();
        }

        private void btnMapToAsl_Click(object sender, EventArgs e)
        {
            CalculateAsl();
        }

        private void CalculateAsl()
        {
            double asl = IMCalculate.CalcALS(_posAdmSite.X, _posAdmSite.Y, _posAdmSite.Csys);
            if(asl != IM.NullD)
                _posAdmSite.Asl = asl;
            IsCalcAsl = false;
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (_photoList.Count > cmbBoxPhoto.SelectedIndex && cmbBoxPhoto.SelectedIndex >= 0)
            {
                _photoList.RemoveAt(cmbBoxPhoto.SelectedIndex);
                _delphotoList.Add(_cmbFoto[cmbBoxPhoto.SelectedIndex].Key);
                _delphotoList.Add(_cmbFoto[cmbBoxPhoto.SelectedIndex].Description);
                _posAdmSite.IsChanged = true;
                btnSave.Enabled = true;
                btnSaveExit.Enabled = true;
            }
            cmbBoxPhoto.Focus();
            UpdateListComboFoto();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            List<string> listFiles = new List<string>();
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.Multiselect = true;
                //openDlg.Filter = "Images|*.jpg;*.bmp;*.gif;*.jpeg";
                if (openDlg.ShowDialog() == DialogResult.OK)
                    listFiles = openDlg.FileNames.ToList();
                if (listFiles.Count > 0)
                {
                    _posAdmSite.IsChanged = true;
                    btnSave.Enabled = true;
                    btnSaveExit.Enabled = true;
                }
            }
            
            //check filename(s) whether it is local file or placed at network resource.
            //if first option, copy local file to network and replace the path
            string networkLocation = "";
            for (int i = 0; i < listFiles.Count; i++)
            {
                if (listFiles[i].IndexOf("\\\\") != 0)
                {
                    if (string.IsNullOrEmpty(networkLocation))
                    {
                        IMRecordset rsPhotoNetPath = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
                        rsPhotoNetPath.Select("ITEM,WHAT");
                        //rsPhotoNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, "PLG_FOLDER_SELECTION_FOTO");
                        rsPhotoNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-DOC");
                        rsPhotoNetPath.Open();
                        if (!rsPhotoNetPath.IsEOF())
                            networkLocation = rsPhotoNetPath.GetS("WHAT");
                        rsPhotoNetPath.Destroy();

                        if (string.IsNullOrEmpty(networkLocation))
                            throw new Exception("Папку для збереження зображень на сервері не задано.\nЗверніться до адміністратора.");

                        if (networkLocation[networkLocation.Length - 1] != '\\')
                            networkLocation += '\\';
                    }
                    string ext = System.IO.Path.GetExtension(listFiles[i]);
                    if (ext.Length > 0 && ext[0] == '.')
                        ext = ext.Substring(1);
                    string fn = System.IO.Path.GetFileNameWithoutExtension(listFiles[i]);
                    string newFileName = networkLocation + fn + "." + ext;
                    //take care of potential existence of target file;
                    int fSuffix = 1;
                    while (System.IO.File.Exists(newFileName))
                        newFileName = networkLocation + fn + (fSuffix++).ToString() + "." + ext;
                    //OK
                    System.IO.File.Copy(listFiles[i], newFileName);
                    listFiles[i] = newFileName;
                }
            }

            _photoList.AddRange(listFiles);
            cmbBoxPhoto.Focus();
            UpdateListComboFoto();
        }

        private void pctBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start(_cmbFoto[cmbBoxPhoto.SelectedIndex].Key);
        }

        private void btnChangeAdmSite_Click(object sender, EventArgs e)
        {
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.SITES, 1, _posAdmSite.X, _posAdmSite.Y);
            //PositionState2 objPosition = FormSelectExistPosition.SelectPositionState(null, ICSMTbl.SITES, 1, _posAdmSite.X, _posAdmSite.Y);
            if (objPosition != null)
            {
                _posAdmSite.AdminSiteId = objPosition.GetI("ID");
                FillAdminSite(_posAdmSite.AdminSiteId);
                CheckAdmSiteDifference();
            }
        }

        private void btnOffAdmSite_Click(object sender, EventArgs e)
        {
            FillAdminSite(IM.NullI);
            CheckAdmSiteDifference();
        }

        /// <summary>
        /// Потрібна перевірка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (IM.TableRight(_posAdmSite.TableName, IMTableRight.Update) == false)
                return;
            
            if (CUsers.CurDepartment != UcrfDepartment.URCP)
            //#7469 добавление подразделения УРЗП
            //if ((CUsers.CurDepartment != UcrfDepartment.URCP) && (CUsers.CurDepartment != UcrfDepartment.URZP))
            {
                MessageBox.Show("Для запиту перевірки потрібно мати поточним підрозділом 'УРЧП'",
                    CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show("Запитати перевірку сайта в філії/УРЧМ?",
                    CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            if (btnSave.Enabled)
                btnSave_Click(this, new EventArgs());

            if (!_isSites && _posAdmSite.AdminSiteId == IM.NullI)
            {
                IMObject obj = IMObject.LoadFromDB(_posAdmSite.TableName, _posAdmSite.Id);
                _posAdmSite.AdminSiteId = PositionState.ClonePositions(obj, ICSMTbl.SITES);
                _posAdmSite.SendUser = IM.ConnectedUser();
                _posAdmSite.SendDate = DateTime.Now;
                _posAdmSite.SaveStatusAudit(); // should be insert operation
                IMRecordset r = new IMRecordset(_posAdmSite.TableName, IMRecordset.Mode.ReadWrite);
                r.Select("ID,ADMS_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _posAdmSite.Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("ADMS_ID",_posAdmSite.AdminSiteId);
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }
                FillAdminSite(_posAdmSite.AdminSiteId);
            }
            else
            {
                if (IM.TableRight("SITES", IMTableRight.Update) && IM.TableRight(PlugTbl.XfaPositionExt, IMTableRight.Update))
                {
                    _posAdmSite.SendUser = IM.ConnectedUser();
                    _posAdmSite.SendDate = DateTime.Now;
                    _posAdmSite.SetStatusSites(CAndE.Need);
                }
                else
                {
                    MessageBox.Show("Для повторного запиту потрібні права на оновлення таблиць SITES та "+PlugTbl.XfaPositionExt, "Не можна", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }

            CheckAdmStatus();
        }

        /// <summary>
        /// Перевірка проведена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (_posAdmSite.TableName == ICSMTbl.SITES)
            {
                string curStatus = _posAdmSite.Status;
                if (curStatus == CAndE.ReqP && (CUsers.CurDepartment & (UcrfDepartment.URZP | UcrfDepartment.Branch)) == UcrfDepartment.UNKNOWN)
                {
                    MessageBox.Show("Для підтвердження перевірки потрібно мати поточним підрозділом 'УРЗП' або 'Філію'",
                        CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (curStatus == CAndE.Need && (CUsers.CurDepartment & (UcrfDepartment.URCM | UcrfDepartment.URZP | UcrfDepartment.Branch)) == UcrfDepartment.UNKNOWN)
                {
                    MessageBox.Show("Для підтвердження перевірки потрібно мати поточним підрозділом 'УРЧМ' або 'Філію'",
                        CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                {
                    string userProvince = CUsers.GetUserProvince();
                    //if (userProvince != _posAdmSite.Province)
                    if ((!CUsers.GetSubProvinceUser().Contains(_posAdmSite.Province)) || (!CUsers.GetSubProvinceUser().Contains(userProvince)))
                    {
                        MessageBox.Show(string.Format("Регіон поточного користувача '{0}' не відповідає регіону сайту '{1}'", userProvince, _posAdmSite.Province),
                            CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                if (MessageBox.Show("Підтвердити коректність даних сайту?",
                        CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;

                if (btnSave.Enabled)
                    btnSave_Click(this, new EventArgs());
                else if (string.IsNullOrEmpty(_posAdmSite.AddressAuto) && MessageBox.Show("Адреса не заповнена. Продовжити?", "Підтвердження", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    throw new Exception("Перервано користувачем");
                
                _posAdmSite.AcceptUser = IM.ConnectedUser();
                _posAdmSite.AcceptDate = DateTime.Now;
                _posAdmSite.SetStatusSites(curStatus == CAndE.ReqP ? CAndE.OkP : CAndE.Chkd);

                IsEnableChange = false;

                CheckAdmStatus();
            }
            else
                MessageBox.Show("Не для этой таблицы");
        }

        private bool _isSaveWasCalled = false;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        /// <summary>
        /// Зробити колір текста компоненти червоним
        /// </summary>
        /// <param name="c"></param>
        private void MakeRedHigh(Control c)
        {
            c.ForeColor = Color.Red;
        }

        /// <summary>
        /// Реакція на зміну координат
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBoxLatLow_TextChanged(object sender, EventArgs e)
        {
            if (txtBoxLatLow.Text != _firstLat && !string.IsNullOrEmpty(_firstLat))
            {
                //IsCalcAsl = true;
            }
            else if (string.IsNullOrEmpty(_firstLat))
            {
                _firstLat = txtBoxLatLow.Text;
            }
        }
        private void txtBoxLongLow_TextChanged(object sender, EventArgs e)
        {
            if (txtBoxLongLow.Text != _firstLong && !string.IsNullOrEmpty(_firstLong))
            {
                //IsCalcAsl = true;
            }
            else if (string.IsNullOrEmpty(_firstLong))
            {
                _firstLong = txtBoxLongLow.Text;
            }
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            //if ((CUsers.CurDepartment & (UcrfDepartment.URCM | UcrfDepartment.Branch)) == UcrfDepartment.UNKNOWN)
            // 7469 добавление подразделения УРЗП
            if ((CUsers.CurDepartment & (UcrfDepartment.URCM | UcrfDepartment.URZP| UcrfDepartment.Branch)) == UcrfDepartment.UNKNOWN)
            {
                MessageBox.Show("Для підтвердження перевірки або відмови в підтвердженні потрібно мати поточним підрозділом 'УРЧМ','УРЗП' або 'Філію'",
                    CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //if (CUsers.CurDepartment != UcrfDepartment.URCM /*UcrfDepartment.Branch*/)
            // 7469 добавление подразделения УРЗП
            if ((CUsers.CurDepartment != UcrfDepartment.URCM) && (CUsers.CurDepartment != UcrfDepartment.URZP))
            {
                string userProvince = CUsers.GetUserProvince();
                //if (userProvince != _posAdmSite.Province)
                //if (!CUsers.GetSubProvinceUser().Contains(_posAdmSite.Province))
                if ((!CUsers.GetSubProvinceUser().Contains(_posAdmSite.Province)) || (!CUsers.GetSubProvinceUser().Contains(userProvince)))
                {
                    MessageBox.Show(string.Format("Регіон поточного користувача '{0}' не відповідає регіону сайту '{1}'", userProvince, _posAdmSite.Province),
                        CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (MessageBox.Show("Помітити сайт як такий, що містить помилки в даних?",
                    CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            _posAdmSite.AcceptUser = IM.ConnectedUser();
            _posAdmSite.AcceptDate = DateTime.Now;
            _posAdmSite.SetStatusSites(CAndE.Error);

            CheckAdmStatus();
        }
        /// <summary>
        /// Вернуть созданый сайт
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            PressedOkButton();
        }
        /// <summary>
        /// Нажата кнопка "ОК"
        /// </summary>
        private void PressedOkButton()
        {
            if (IsCalcAsl)
                CalculateAsl();
        }

        private void btmIcsmSite_Click(object sender, EventArgs e)
        {
            tx_ = new Timer();
            tx_.Interval = 100;
            tx_.Tick += new EventHandler(Timer_Update);
            tx_.Enabled = true;
            //if (!_posAdmSite.IsChanged || (MessageBox.Show(CLocaliz.TxT("Save changes?"), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                //if (_posAdmSite.IsChanged)
                    //Save();
                RecordPtr rpSite = new RecordPtr(_posAdmSite.TableName, _posAdmSite.Id);
                IM.AdminDisconnect();
                this.Close();
                if (rpSite.UserEdit())
                {
                    _posAdmSite.LoadStatePosition(_posAdmSite.Id, _posAdmSite.TableName);
                    AdminSiteAllTech_Load(this, null);
                }
            }
            tx_.Enabled = false;
            
        }

        private void AdminSiteAllTech2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isSaveWasCalled)
                DialogResult = DialogResult.OK;
        }

        private void txtBoxLatLow_Validated(object sender, EventArgs e)
        {
            FillMap(false);
            CheckAdmSiteDifference();
        }

        private void txtBoxLongLow_Validated(object sender, EventArgs e)
        {
            FillMap(false);
            CheckAdmSiteDifference();
        }

        private void btAddrLikeAdmSiteHas_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Встановити населений пункт (КОАТУУ) та адресу відповідно Адм. сайту?", CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (_posAdmSite.AdminSiteId == IM.NullI)
                {
                    MessageBox.Show("Нема адм.сайту", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                
                PositionState2 pos = new PositionState2();
                pos.LoadStatePosition(_posAdmSite.AdminSiteId, ICSMTbl.SITES);
                _posAdmSite.CityId = pos.CityId;
                //_posAdmSite.City = pos.City;
                //_posAdmSite.TypeCity = pos.TypeCity;
                //_posAdmSite.Province = pos.Province;
                //_posAdmSite.Subprovince = pos.Subprovince;

                pnlAddress.Controls.Remove(_cf); //FillAddress() creates new _cf
                FillAddress();
                FillCoatuu();
                
                _posAdmSite.StreetType = pos.StreetType;
                _posAdmSite.StreetName = pos.StreetName;
                _posAdmSite.BuildingNumber = pos.BuildingNumber;
                _posAdmSite.Remark = pos.Remark;
                
                _shortAddresCtr.StreetType = _posAdmSite.StreetType;
                _shortAddresCtr.StreetName = _posAdmSite.StreetName;
                _shortAddresCtr.HouseNumber = _posAdmSite.BuildingNumber;
                _shortAddresCtr.Other = _posAdmSite.Remark;

                FillMap(false);

                CheckAdmSiteDifference();
            }
        }

        private void btCoordLikeAdmSiteHas_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Встановити координати відповідно Адм. сайту?", CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (_posAdmSite.AdminSiteId == IM.NullI)
                {
                    MessageBox.Show("Нема адм.сайту", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                PositionState2 pos = new PositionState2();
                pos.LoadStatePosition(_posAdmSite.AdminSiteId, ICSMTbl.SITES);
                _posAdmSite.Csys = pos.Csys;
                _posAdmSite.X = pos.X;
                _posAdmSite.Y = pos.Y;
                
                CheckAdmSiteDifference();
            }
        }

        private void TypeObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFullAddress();
           //tbFullAddress.Text = PositionState2.CreateFullAddress(_cf.CurrentCity.Address, _cf.CurrentCity.type == "район" ? _shortAddresCtr.Other : _shortAddresCtr.GetAddress(), TypeObject.Text);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(CLocaliz.TxT("Save for visible layers map?"), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SetIndexLayer(true);
                MessageBox.Show(CLocaliz.TxT("Save made"), CLocaliz.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AdminSiteAllTech2_Shown(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnSaveExit.Enabled = false;            
        }

        private void checkBox_special_check_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_special_check.Checked) {
                _posAdmSite.Status = CAndE.nic;
                btnCheck.Enabled = string.IsNullOrEmpty(_posAdmSite.AcceptUser) && (_posAdmSite.Status != CAndE.nic);
                btnAccept.Enabled = false;
                btnSave.Enabled = true;
                btnSaveExit.Enabled = true;     
            }
            else {
                bool boolCheck;  bool boolAccept;
                _posAdmSite.Status = CAndE.Need;
                boolCheck = (_posAdmSite.Status == CAndE.Need || _posAdmSite.Status == CAndE.ReqP);
                boolAccept = !boolCheck && _posAdmSite.Status != CAndE.Del;
                btnCheck.Enabled = boolCheck && string.IsNullOrEmpty(_posAdmSite.AcceptUser) && (_posAdmSite.Status != CAndE.nic);
                btnAccept.Enabled = boolAccept;
                btnSave.Enabled = true;
                btnSaveExit.Enabled = true;     
            }
        }

        private void txtBoxAdmSite_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
