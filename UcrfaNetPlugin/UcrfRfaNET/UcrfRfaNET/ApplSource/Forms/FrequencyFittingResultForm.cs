﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    internal partial class FrequencyFittingResultForm : Form
    {
        public string StationOwner { set { lblOwner.Text = value; } }
        public string PositionA { set { lblPositionA.Text = value; } }
        public string PositionB { set { lblPositionB.Text = value;} }
        public double Range { set { lblRange.Text = value.ToStringNullD()+" км"; }   }
        public double Threshold { set { lblFrequency.Text = value.ToStringNullD() + " дБ"; } }

        public bool MobSta2Stations { get; set; }
        public bool MicrovaweStations { get; set; }
        public bool EarthStations { get; set; }

        public string TableName{ get; set;}
        public int Id { get; set; }
        public int ApplId { get; set; }

        public string FrequencySuffix { get; set; }

        private List<ViborkaEMS.Results> _resultsList = new List<ViborkaEMS.Results>();

        public FrequencyFittingResultForm()
        {
            InitializeComponent();
        }

        private void FrequencyFittingResultForm_Load(object sender, EventArgs e)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            //column.Name = "chId";
            column.HeaderText = "Частота "+FrequencySuffix;
            column.Width = 150;
            resultGrid.Columns.Add(column);
            
            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (CРT) \"нам\" дБВт";
            column.Width = 150;
            column.Visible = MobSta2Stations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (CРT)\"нам\"";
            column.Width = 150;
            column.Visible = MobSta2Stations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (CРT)\"від нас\", дБВт";
            column.Width = 150;
            column.Visible = MobSta2Stations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (CРT)\"від нас\"";
            column.Width = 150;
            column.Visible = MobSta2Stations;
            resultGrid.Columns.Add(column);

            //---
            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (РР) \"нам\", дБВт";
            column.Width = 150;
            column.Visible = MicrovaweStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (РР)\"нам\"";
            column.Width = 150;
            column.Visible = MicrovaweStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (РР)\"від нас\", дБВт";
            column.Width = 150;
            column.Visible = MicrovaweStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (РР)\"від нас\"";
            column.Width = 150;
            column.Visible = MicrovaweStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (ЗС) \"нам\", дБВт";
            column.Width = 150;
            column.Visible = EarthStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (ЗС)\"нам\"";
            column.Width = 150;
            column.Visible = EarthStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.Name = "chNumberDOZV";
            column.HeaderText = "Максимальна завада (ЗС)\"від нас\", дБВт";
            column.Width = 150;
            column.Visible = EarthStations;
            resultGrid.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Кільність завад (ЗС)\"від нас\"";
            column.Width = 150;
            column.Visible = EarthStations;
            resultGrid.Columns.Add(column);

            InternalSetData(_resultsList);

            if (lblPositionA.Text==lblPositionB.Text)
            {
                lblStationBCaption.Visible = false;
                lblPositionB.Visible = false;
                lblStationACaption.Text = "Координати станції";
            } else
            {
                lblStationACaption.Text = "Координати станції A";
            }

            resultGrid.ReadOnly = true;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void lblOwner_Click(object sender, EventArgs e)
        {

        }
        public void SetData(List<ViborkaEMS.Results> resultsList)
        {
            _resultsList = resultsList;
        }

        private void InternalSetData(List<ViborkaEMS.Results> resultsList)
        {
            foreach (ViborkaEMS.Results resEms in resultsList)
            {
                double zero = -9999.0;
                object[] objs = new object[13];

                    objs[0] = resEms.FrequenciersString;

                    objs[1] = resEms.MobstationVictim != zero ? (resEms.MobstationVictim - 30).Round(3).ToStringNullD() : "";
                    objs[2] = resEms.MobstationVictimCount;
                    objs[3] = resEms.MobstationIntf != zero ? (resEms.MobstationIntf - 30).Round(3).ToStringNullD() : "";
                    objs[4] = resEms.MobstationIntfCount;

                    objs[5] = resEms.MicrovaweVictim !=zero ? (resEms.MicrovaweVictim - 30).Round(3).ToStringNullD() : "";
                    objs[6] = resEms.MicrovaweVictimCount;
                    objs[7] = resEms.MicrovaweIntf != zero ? (resEms.MicrovaweIntf - 30).Round(3).ToStringNullD() : "";
                    objs[8] = resEms.MicrovaweIntfCount;                    

                    objs[9] = resEms.EarthVictim != zero ? (resEms.EarthVictim - 30).Round(3).ToStringNullD() : "";
                    objs[10] = resEms.EarthVictimCount;
                    objs[11] = resEms.EarthIntf != zero ? (resEms.EarthVictim - 30).Round(3).ToStringNullD() : "";
                    objs[12] = resEms.EarthIntfCount;

                resultGrid.Rows.Add(objs);
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //csvFileDialog.InitialDirectory = PluginSetting.PluginFolderSetting.FolderFreqFitting;
            //if (csvFileDialog.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            //{
            //public string TableName{ get; set;}
            //public int Id { get; set; }
            try
            {
                var tech = TableName == ICSMTbl.itblMicrowa ? "РРЛ" : "СРТ";
                string strId = Id.ToString();

                string csvFileName = Path.Combine(PluginSetting.PluginFolderSetting.FolderFreqFitting, "Підбір частот_" + tech + "_" + strId + ".csv");

                if (File.Exists(csvFileName))
                    File.Delete(csvFileName);

                StreamWriter csvStream = new StreamWriter(csvFileName, false, Encoding.Default);


                StringBuilder dataHtr = new StringBuilder();
                dataHtr.Append(label1.Text);
                dataHtr.Append(";");
                dataHtr.Append(lblStationACaption.Text);
                dataHtr.Append(";");
                if (lblStationBCaption.Visible)
                {
                    dataHtr.Append(lblStationBCaption.Text);
                    dataHtr.Append(";");
                }
                dataHtr.Append(label4.Text);
                dataHtr.Append(";");
                dataHtr.Append(label5.Text);
                dataHtr.Append(";");
                csvStream.WriteLine(dataHtr.ToString());

                StringBuilder data = new StringBuilder();

                data.Append(lblOwner.Text);
                data.Append(";");
                data.Append(lblPositionA.Text);
                data.Append(";");
                if (lblStationBCaption.Visible)
                {
                    data.Append(lblPositionA.Text);
                    data.Append(";");
                }
                data.Append(lblRange.Text);
                data.Append(";");
                data.Append(lblFrequency.Text);
                data.Append(";");
                csvStream.WriteLine(data.ToString());

                StringBuilder hdr = new StringBuilder();
                for (int i = 0; i < 13; i++)
                {
                    if (resultGrid.Columns[i].Visible)
                    {
                        hdr.Append(resultGrid.Columns[i].HeaderText);
                        hdr.Append(";");
                    }
                }
                csvStream.WriteLine(hdr.ToString());

                foreach (DataGridViewRow row in resultGrid.Rows)
                {
                    StringBuilder bb = new StringBuilder();

                    for (int i = 0; i < 13; i++)
                    {
                        if (resultGrid.Columns[i].Visible)
                        {
                            if (row.Cells[i].Value != null)
                                bb.Append(row.Cells[i].Value.ToString());

                            bb.Append(";");
                        }
                    }
                    csvStream.WriteLine(bb.ToString());
                }

                csvStream.WriteLine(";");
                csvStream.WriteLine(";");
                csvStream.WriteLine("Виконав;" + CUsers.GetUserFio());

                csvStream.Close();

                if (File.Exists(csvFileName))
                {
                    System.Diagnostics.Process.Start(csvFileName);  
                    CEventLog.AddApplEvent(ApplId, EDocEvent.evFreqFitting, DateTime.Now, "", ICSM.IM.NullT, csvFileName);
                }                
            }
            catch (Exception ex)
            {
                CLogs.WriteError(ELogsWhat.Critical, ex);
                MessageBox.Show(ex.Message);                                
            }            
        }
    }
}
