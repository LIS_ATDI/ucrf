﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Classes;
using XICSM.UcrfRfaNET.UtilityClass;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.ApplSource
{
    internal class DiffReport2Object : NotifyPropertyChanged
    {
        private string _fileLink;
        private DateTime _timeFrom1;
        private DateTime _timeFrom2;
        private DateTime _timeTo1;
        private DateTime _timeTo2;
        /// <summary>
        /// Аналізатор спектра
        /// </summary>
        public int MonitorEquipId { get; set; }
        /// <summary>
        /// Дата вимірювання 1
        /// </summary>
        public DateTime Date1 { get; set; }
        /// <summary>
        /// Дата вимірювання 2
        /// </summary>
        public DateTime Date2 { get; set; }
        /// <summary>
        /// Час вимірювання "від" 1
        /// </summary>
        public DateTime TimeFrom1
        { 
            get{return _timeFrom1;}
            set
            {
                if(value != _timeFrom1)
                {
                    _timeFrom1 = value;
                    InvokeNotifyPropertyChanged("TimeFrom1");
                }
            }
        }
        /// <summary>
        /// Час вимірювання "від" 2
        /// </summary>
        public DateTime TimeFrom2
        {
            get { return _timeFrom2; }
            set
            {
                if (value != _timeFrom2)
                {
                    _timeFrom2 = value;
                    InvokeNotifyPropertyChanged("TimeFrom2");
                }
            }
        }
        /// <summary>
        /// Час вимірювання "до" 1
        /// </summary>
        public DateTime TimeTo1
        {
            get { return _timeTo1; }
            set
            {
                if (value != _timeTo1)
                {
                    _timeTo1 = value;
                    InvokeNotifyPropertyChanged("TimeTo1");
                }
            }
        }
        /// <summary>
        /// Час вимірювання "до" 2
        /// </summary>
        public DateTime TimeTo2
        {
            get { return _timeTo2; }
            set
            {
                if (value != _timeTo2)
                {
                    _timeTo2 = value;
                    InvokeNotifyPropertyChanged("TimeTo2");
                }
            }
        }
        /// <summary>
        /// Отмечена дата 1
        /// </summary>
        public bool IsCheckedDate1 { get; set; }
        /// <summary>
        /// Отмечена дата 2
        /// </summary>
        public bool IsCheckedDate2 { get; set; }
        /// <summary>
        /// Доступна дата 2
        /// </summary>
        public bool IsEnableDate2 { get; set; }
        /// <summary>
        /// ID користувача, який візує
        /// </summary>
        public int ViseUserId { get; set; }
        /// <summary>
        /// ID користувача, який проводив вимірювання
        /// </summary>
        public int ExecutorUserId { get; set; }
        /// <summary>
        /// Лінк на файл протоколу
        /// </summary>
        public string FileLink
        {
            get { return _fileLink;}
            set
            {
                if (value != _fileLink)
                {
                    _fileLink = value;
                    InvokeNotifyPropertyChanged("FileLink");
                }
            }
        }
        /// <summary>
        /// ID запису Diff
        /// </summary>
        public int DiffId { get { return DiffObject.Station.Id;} }
        /// <summary>
        /// Таблица запису Diff
        /// </summary>
        public string TableName { get { return DiffObject.DiffTableName; } }
        /// <summary>
        /// Список пользовотелей которые визуют
        /// </summary>
        public ComboBoxDictionaryList<int, string> ViseUserLst = new ComboBoxDictionaryList<int, string>();
        /// <summary>
        /// Список пользователей исполнителей
        /// </summary>
        public ComboBoxDictionaryList<int, string> ExecutorUserLst = new ComboBoxDictionaryList<int, string>();
        /// <summary>
        /// Список устройств
        /// </summary>
        public ComboBoxDictionaryList<int, string> DeviceLst = new ComboBoxDictionaryList<int, string>();
        /// <summary>
        /// Обьект станции
        /// </summary>
        protected BaseDiffClass DiffObject = null;   // Обьект, который управляет заявкой
        //=======================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="diffObject">Объект Diff станции</param>
        public DiffReport2Object(BaseDiffClass diffObject)
        {
            DiffObject = diffObject;
            // По умолчанию
            MonitorEquipId = IM.NullI;
            Date1 = DateTime.Now;
            TimeFrom1 = DateTime.Now;
            TimeTo1 = DateTime.Now;
            Date2 = DateTime.Now;
            TimeFrom2 = DateTime.Now;
            TimeTo2 = DateTime.Now;
            ViseUserId = IM.NullI;
            ExecutorUserId = IM.NullI;
            FileLink = "";
            IsCheckedDate1 = true;
            IsCheckedDate2 = false;
            IsEnableDate2 = (TableName == PlugTbl.itblXnrfaDeiffMwSta);
            // Загрузка устройств
            UpdateMeasureEquip();
            //Загрузка визиторов
            UpdateViseUser();
            //Загрузка
            UpdateExecutorUser();
            Load();
        }
        //=======================================
        /// <summary>
        /// Загружает данные из таблицы
        /// </summary>
        public void Load()
        {
            IMRecordset rsStationDiff = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            rsStationDiff.Select("ID,MEASURE_EQ_ID,MEASURE_DATE1,MEASURE_TIME_FROM1,MEASURE_TIME_TO1");
            rsStationDiff.Select("MEASURE_DATE2,MEASURE_TIME_FROM2,MEASURE_TIME_TO2");
            rsStationDiff.Select("VISA_USER_ID,EXECUTOR_ID,LINK_IE,ENABLE_DATE1,ENABLE_DATE2");
            rsStationDiff.SetWhere("ID", IMRecordset.Operation.Eq, DiffId);
            try
            {
                rsStationDiff.Open();
                if (!rsStationDiff.IsEOF())
                {
                    MonitorEquipId = rsStationDiff.GetI("MEASURE_EQ_ID");
                    Date1 = rsStationDiff.GetT("MEASURE_DATE1");
                    TimeFrom1 = rsStationDiff.GetT("MEASURE_TIME_FROM1");
                    TimeTo1 = rsStationDiff.GetT("MEASURE_TIME_TO1");
                    Date2 = rsStationDiff.GetT("MEASURE_DATE2");
                    TimeFrom2 = rsStationDiff.GetT("MEASURE_TIME_FROM2");
                    TimeTo2 = rsStationDiff.GetT("MEASURE_TIME_TO2");
                    ViseUserId = rsStationDiff.GetI("VISA_USER_ID");
                    ExecutorUserId = rsStationDiff.GetI("EXECUTOR_ID");
                    FileLink = rsStationDiff.GetS("LINK_IE");
                    IsCheckedDate1 = (rsStationDiff.GetI("ENABLE_DATE1") == 0) ? false : true;
                    IsCheckedDate2 = (rsStationDiff.GetI("ENABLE_DATE2") == 0) ? false : true;
                }
            }
            finally
            {
                if (rsStationDiff.IsOpen())
                    rsStationDiff.Close();
                rsStationDiff.Destroy();
            }
            // Установка дат по умолчанию
            if(Date1 == IM.NullT)
            {
                Date1 = DateTime.Now;
                TimeFrom1 = DateTime.Now;
                TimeTo1 = DateTime.Now;
            }
            if (Date2 == IM.NullT)
            {
                Date2 = DateTime.Now;
                TimeFrom2 = DateTime.Now;
                TimeTo2 = DateTime.Now;
            }
        }
        //=======================================
        /// <summary>
        /// Сохраняет данные в таблицу
        /// </summary>
        public void Save()
        {
            IMRecordset rsStationDiff = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            rsStationDiff.Select("ID,MEASURE_EQ_ID,MEASURE_DATE1,MEASURE_TIME_FROM1,MEASURE_TIME_TO1");
            rsStationDiff.Select("MEASURE_DATE2,MEASURE_TIME_FROM2,MEASURE_TIME_TO2");
            rsStationDiff.Select("VISA_USER_ID,EXECUTOR_ID,LINK_IE,ENABLE_DATE1,ENABLE_DATE2");
            rsStationDiff.SetWhere("ID", IMRecordset.Operation.Eq, DiffId);
            try
            {
                rsStationDiff.Open();
                if(!rsStationDiff.IsEOF())
                {
                    rsStationDiff.Edit();
                    rsStationDiff.Put("MEASURE_EQ_ID", MonitorEquipId);
                    rsStationDiff.Put("MEASURE_DATE1", Date1);
                    rsStationDiff.Put("MEASURE_TIME_FROM1", TimeFrom1);
                    rsStationDiff.Put("MEASURE_TIME_TO1", TimeTo1);
                    rsStationDiff.Put("MEASURE_DATE2", Date2);
                    rsStationDiff.Put("MEASURE_TIME_FROM2", TimeFrom2);
                    rsStationDiff.Put("MEASURE_TIME_TO2", TimeTo2);
                    rsStationDiff.Put("VISA_USER_ID", ViseUserId);
                    rsStationDiff.Put("EXECUTOR_ID", ExecutorUserId);
                    rsStationDiff.Put("LINK_IE", FileLink);
                    rsStationDiff.Put("ENABLE_DATE1", (IsCheckedDate1 == true) ? 1 : 0);
                    rsStationDiff.Put("ENABLE_DATE2", (IsCheckedDate2 == true) ? 1 : 0);
                    rsStationDiff.Update();
                }
            }
            finally
            {
                if(rsStationDiff.IsOpen())
                    rsStationDiff.Close();
                rsStationDiff.Destroy();
            }
        }
        /// <summary>
        /// Печатаем документ
        /// </summary>
        public void Print()
        {
            string regionCode = HelpFunction.getAreaCode(DiffObject.Station.Position.Province, DiffObject.Station.Position.City);
            string applIdStr = DiffObject.ApplID.ToString("D4");
            string packetIdStr = DiffObject.PacketID.ToString("D5");
            string fileName = string.Format("{0}\\{1}", PluginSetting.PluginFolderSetting.FolderAktPtk, string.Format("{0}-{1}-{2}-ПІО", regionCode, applIdStr, packetIdStr));
            string rtfFileName = string.Format("{0}.{1}", fileName, "rtf");
            string docFileName = string.Format("{0}.{1}", fileName, "docx");
            FunctionConvertToDOCX doc = new FunctionConvertToDOCX();
            if (System.IO.File.Exists(docFileName))
            {
                string message = string.Format("Файл '{0}' існує. Ви дійсно бажаєте перезаписати його?", docFileName);
                if (MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;
            }
            PrintDocs printDocs = new PrintDocs();
            if (printDocs.CreateOneReport(DocType.AKT_TE_PTK, DiffObject.ApplType, Date1, IM.NullT, PlugTbl.itblXnrfaAppl, DiffObject.ApplID, "", rtfFileName, true, docFileName))
            {
                doc.ConvertDOCToDOCX(docFileName);
                FileLink = docFileName;
                Save();
                OpenLink();
            }
        }
        /// <summary>
        /// Открываем документ
        /// </summary>
        public void OpenLink()
        {
            if (string.IsNullOrEmpty(FileLink) || System.IO.File.Exists(FileLink) == false)
            {
                MessageBox.Show(string.Format("Can't open file '{0}'", FileLink));
                return;
            }
            System.Diagnostics.Process.Start(FileLink);
        }
        /// <summary>
        /// Обновляет пользователей-начальников
        /// </summary>
        private void UpdateViseUser()
        {
            ViseUserLst.Clear();
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                else
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.VRZ.ToString());
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                for(r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    int id = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");
                    string employee = lastName + " " + firstName;
                    ViseUserLst.Add(new ComboBoxDictionary<int, string>(id, employee));
                }
            }
            finally
            {
                if(r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Обновляет пользователей-исполнителей
        /// </summary>
        private void UpdateExecutorUser()
        {
            ExecutorUserLst.Clear();
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,TYPE,TITLE,FIRSTNAME,LASTNAME");
                if (CUsers.CurDepartment == UcrfDepartment.Branch)
                    r.SetWhere("PROVINCE", IMRecordset.Operation.Eq, CUsers.GetUserRegion(IM.ConnectedUser()));
                else
                    r.SetWhere("TYPE", IMRecordset.Operation.Eq, DepartmentType.VRZ.ToString());
                r.OrderBy("LASTNAME", OrderDirection.Ascending);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    int id = r.GetI("ID");
                    string title = r.GetS("TITLE");
                    string firstName = r.GetS("FIRSTNAME");
                    string lastName = r.GetS("LASTNAME");
                    string employee = lastName + " " + firstName;
                    ExecutorUserLst.Add(new ComboBoxDictionary<int, string>(id, employee));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Обновляет список доступного оборудования
        /// </summary>
        private void UpdateMeasureEquip()
        {
            DeviceLst.Clear();
            IMRecordset r = new IMRecordset(PlugTbl.XvMeasureEq, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,NAME");
                r.OrderBy("NAME", OrderDirection.Ascending);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    int id = r.GetI("ID");
                    string name = r.GetS("NAME");
                    DeviceLst.Add(new ComboBoxDictionary<int, string>(id, name));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
    }
}
