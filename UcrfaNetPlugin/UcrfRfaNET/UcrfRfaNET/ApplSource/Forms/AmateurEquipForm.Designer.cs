﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class AmateurEquipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmateurEquipForm));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtBoxDesEmi = new System.Windows.Forms.TextBox();
            this.lblDesEmi = new System.Windows.Forms.Label();
            this.btnDesisEm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnCancel.Location = new System.Drawing.Point(252, 80);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnOk.Location = new System.Drawing.Point(162, 80);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "Прийняти";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // txtBoxName
            // 
            this.txtBoxName.Location = new System.Drawing.Point(137, 12);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(233, 20);
            this.txtBoxName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(92, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 13);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Назва";
            // 
            // txtBoxDesEmi
            // 
            this.txtBoxDesEmi.Location = new System.Drawing.Point(137, 42);
            this.txtBoxDesEmi.Name = "txtBoxDesEmi";
            this.txtBoxDesEmi.Size = new System.Drawing.Size(100, 20);
            this.txtBoxDesEmi.TabIndex = 2;
            // 
            // lblDesEmi
            // 
            this.lblDesEmi.AutoSize = true;
            this.lblDesEmi.Location = new System.Drawing.Point(12, 45);
            this.lblDesEmi.Name = "lblDesEmi";
            this.lblDesEmi.Size = new System.Drawing.Size(119, 13);
            this.lblDesEmi.TabIndex = 17;
            this.lblDesEmi.Text = "Клас випромінювання";
            // 
            // btnDesisEm
            // 
            this.btnDesisEm.Location = new System.Drawing.Point(243, 42);
            this.btnDesisEm.Name = "btnDesisEm";
            this.btnDesisEm.Size = new System.Drawing.Size(25, 20);
            this.btnDesisEm.TabIndex = 20;
            this.btnDesisEm.Text = "...";
            this.btnDesisEm.UseVisualStyleBackColor = true;
            this.btnDesisEm.Click += new System.EventHandler(this.btnDesisEm_Click);
            // 
            // AmateurEquipForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(382, 119);
            this.Controls.Add(this.btnDesisEm);
            this.Controls.Add(this.txtBoxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtBoxDesEmi);
            this.Controls.Add(this.lblDesEmi);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AmateurEquipForm";
            this.Text = "AmateurEquipForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtBoxDesEmi;
        private System.Windows.Forms.Label lblDesEmi;
        private System.Windows.Forms.Button btnDesisEm;
    }
}