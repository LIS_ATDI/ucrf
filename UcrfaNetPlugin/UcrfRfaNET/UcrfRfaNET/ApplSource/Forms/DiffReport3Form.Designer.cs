﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class DiffReport3Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpProtocoleParameters = new System.Windows.Forms.GroupBox();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.clbAdditionals = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbPerformPerson = new System.Windows.Forms.ComboBox();
            this.cbApprovePerson = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChooseAuxiliaryEquipment = new System.Windows.Forms.Button();
            this.lblAuxiliaryEquipment = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnChooseMeasureEquipment = new System.Windows.Forms.Button();
            this.lblMeasurementEquipment = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCertificate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.grbApprovedLink = new System.Windows.Forms.GroupBox();
            this.lblSaved = new System.Windows.Forms.LinkLabel();
            this.grpProtocoleParameters.SuspendLayout();
            this.grbApprovedLink.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpProtocoleParameters
            // 
            this.grpProtocoleParameters.Controls.Add(this.cbRegion);
            this.grpProtocoleParameters.Controls.Add(this.label9);
            this.grpProtocoleParameters.Controls.Add(this.clbAdditionals);
            this.grpProtocoleParameters.Controls.Add(this.label8);
            this.grpProtocoleParameters.Controls.Add(this.cbPerformPerson);
            this.grpProtocoleParameters.Controls.Add(this.cbApprovePerson);
            this.grpProtocoleParameters.Controls.Add(this.label7);
            this.grpProtocoleParameters.Controls.Add(this.label6);
            this.grpProtocoleParameters.Controls.Add(this.btnChooseAuxiliaryEquipment);
            this.grpProtocoleParameters.Controls.Add(this.lblAuxiliaryEquipment);
            this.grpProtocoleParameters.Controls.Add(this.label5);
            this.grpProtocoleParameters.Controls.Add(this.btnChooseMeasureEquipment);
            this.grpProtocoleParameters.Controls.Add(this.lblMeasurementEquipment);
            this.grpProtocoleParameters.Controls.Add(this.label4);
            this.grpProtocoleParameters.Controls.Add(this.label3);
            this.grpProtocoleParameters.Controls.Add(this.dtpEndTime);
            this.grpProtocoleParameters.Controls.Add(this.dtpStartTime);
            this.grpProtocoleParameters.Controls.Add(this.label2);
            this.grpProtocoleParameters.Controls.Add(this.cbCertificate);
            this.grpProtocoleParameters.Controls.Add(this.label1);
            this.grpProtocoleParameters.Location = new System.Drawing.Point(12, 12);
            this.grpProtocoleParameters.Name = "grpProtocoleParameters";
            this.grpProtocoleParameters.Size = new System.Drawing.Size(502, 437);
            this.grpProtocoleParameters.TabIndex = 0;
            this.grpProtocoleParameters.TabStop = false;
            this.grpProtocoleParameters.Text = "Параметри протоколу:";
            // 
            // cbRegion
            // 
            this.cbRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(146, 19);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(349, 21);
            this.cbRegion.TabIndex = 18;
            this.cbRegion.SelectedValueChanged += new System.EventHandler(this.cbRegion_SelectedValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Область:";
            // 
            // clbAdditionals
            // 
            this.clbAdditionals.FormattingEnabled = true;
            this.clbAdditionals.Location = new System.Drawing.Point(9, 336);
            this.clbAdditionals.Name = "clbAdditionals";
            this.clbAdditionals.Size = new System.Drawing.Size(486, 94);
            this.clbAdditionals.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Примітки:";
            // 
            // cbPerformPerson
            // 
            this.cbPerformPerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPerformPerson.FormattingEnabled = true;
            this.cbPerformPerson.Location = new System.Drawing.Point(146, 290);
            this.cbPerformPerson.Name = "cbPerformPerson";
            this.cbPerformPerson.Size = new System.Drawing.Size(349, 21);
            this.cbPerformPerson.TabIndex = 6;
            // 
            // cbApprovePerson
            // 
            this.cbApprovePerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbApprovePerson.FormattingEnabled = true;
            this.cbApprovePerson.Location = new System.Drawing.Point(146, 263);
            this.cbApprovePerson.Name = "cbApprovePerson";
            this.cbApprovePerson.Size = new System.Drawing.Size(349, 21);
            this.cbApprovePerson.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Виконавець:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Затверджує:";
            // 
            // btnChooseAuxiliaryEquipment
            // 
            this.btnChooseAuxiliaryEquipment.Location = new System.Drawing.Point(33, 221);
            this.btnChooseAuxiliaryEquipment.Name = "btnChooseAuxiliaryEquipment";
            this.btnChooseAuxiliaryEquipment.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAuxiliaryEquipment.TabIndex = 4;
            this.btnChooseAuxiliaryEquipment.Text = "Вибрати...";
            this.btnChooseAuxiliaryEquipment.UseVisualStyleBackColor = true;
            this.btnChooseAuxiliaryEquipment.Click += new System.EventHandler(this.btnChooseAuxiliaryEquipment_Click);
            // 
            // lblAuxiliaryEquipment
            // 
            this.lblAuxiliaryEquipment.FormattingEnabled = true;
            this.lblAuxiliaryEquipment.Location = new System.Drawing.Point(146, 188);
            this.lblAuxiliaryEquipment.Name = "lblAuxiliaryEquipment";
            this.lblAuxiliaryEquipment.Size = new System.Drawing.Size(349, 69);
            this.lblAuxiliaryEquipment.TabIndex = 11;
            this.lblAuxiliaryEquipment.TabStop = false;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(6, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Допоміжне обладнання:";
            // 
            // btnChooseMeasureEquipment
            // 
            this.btnChooseMeasureEquipment.Location = new System.Drawing.Point(33, 146);
            this.btnChooseMeasureEquipment.Name = "btnChooseMeasureEquipment";
            this.btnChooseMeasureEquipment.Size = new System.Drawing.Size(75, 23);
            this.btnChooseMeasureEquipment.TabIndex = 3;
            this.btnChooseMeasureEquipment.Text = "Вибрати...";
            this.btnChooseMeasureEquipment.UseVisualStyleBackColor = true;
            this.btnChooseMeasureEquipment.Click += new System.EventHandler(this.btnChooseMeasureEquipment_Click);
            // 
            // lblMeasurementEquipment
            // 
            this.lblMeasurementEquipment.FormattingEnabled = true;
            this.lblMeasurementEquipment.Location = new System.Drawing.Point(146, 113);
            this.lblMeasurementEquipment.Name = "lblMeasurementEquipment";
            this.lblMeasurementEquipment.Size = new System.Drawing.Size(349, 69);
            this.lblMeasurementEquipment.TabIndex = 8;
            this.lblMeasurementEquipment.TabStop = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 30);
            this.label4.TabIndex = 7;
            this.label4.Text = "Основні засоби вимірювальної техніки:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(345, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "по";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(370, 87);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(125, 20);
            this.dtpEndTime.TabIndex = 2;
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Location = new System.Drawing.Point(192, 87);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.Size = new System.Drawing.Size(125, 20);
            this.dtpStartTime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Термін проведення вимірювань з:";
            // 
            // cbCertificate
            // 
            this.cbCertificate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCertificate.FormattingEnabled = true;
            this.cbCertificate.Location = new System.Drawing.Point(146, 46);
            this.cbCertificate.Name = "cbCertificate";
            this.cbCertificate.Size = new System.Drawing.Size(349, 21);
            this.cbCertificate.TabIndex = 0;
            this.cbCertificate.Visible = false;
            this.cbCertificate.SelectedIndexChanged += new System.EventHandler(this.cbCertificate_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Свідоцтво про атестацію:";
            this.label1.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(11, 455);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(92, 455);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Друк";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(436, 455);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Вийти";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grbApprovedLink
            // 
            this.grbApprovedLink.Controls.Add(this.lblSaved);
            this.grbApprovedLink.Location = new System.Drawing.Point(11, 484);
            this.grbApprovedLink.Name = "grbApprovedLink";
            this.grbApprovedLink.Size = new System.Drawing.Size(502, 48);
            this.grbApprovedLink.TabIndex = 4;
            this.grbApprovedLink.TabStop = false;
            this.grbApprovedLink.Text = "Збережений протокол:";
            // 
            // lblSaved
            // 
            this.lblSaved.AutoSize = true;
            this.lblSaved.Location = new System.Drawing.Point(8, 22);
            this.lblSaved.Name = "lblSaved";
            this.lblSaved.Size = new System.Drawing.Size(127, 13);
            this.lblSaved.TabIndex = 0;
            this.lblSaved.TabStop = true;
            this.lblSaved.Text = "Натисніть щоб відкрити";
            this.lblSaved.Visible = false;
            this.lblSaved.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblSaved_LinkClicked);
            // 
            // DiffReport3Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 544);
            this.Controls.Add(this.grbApprovedLink);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grpProtocoleParameters);
            this.Name = "DiffReport3Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Генерація протоколу вимірювань параметрів РЕЗ ";
            this.Load += new System.EventHandler(this.DiffReport3Form_Load);
            this.grpProtocoleParameters.ResumeLayout(false);
            this.grpProtocoleParameters.PerformLayout();
            this.grbApprovedLink.ResumeLayout(false);
            this.grbApprovedLink.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpProtocoleParameters;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCertificate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lblMeasurementEquipment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Button btnChooseAuxiliaryEquipment;
        private System.Windows.Forms.ListBox lblAuxiliaryEquipment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnChooseMeasureEquipment;
        private System.Windows.Forms.ComboBox cbPerformPerson;
        private System.Windows.Forms.ComboBox cbApprovePerson;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox clbAdditionals;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox grbApprovedLink;
        private System.Windows.Forms.LinkLabel lblSaved;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.Label label9;
    }
}