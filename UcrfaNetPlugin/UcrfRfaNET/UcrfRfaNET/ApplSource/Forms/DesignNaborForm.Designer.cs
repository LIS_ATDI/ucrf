﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class DesignNaborForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFileds = new System.Windows.Forms.Label();
            this.chkdLstBoxField = new System.Windows.Forms.CheckedListBox();
            this.chkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.lblNabor = new System.Windows.Forms.Label();
            this.lstBoxNabor = new System.Windows.Forms.ListBox();
            this.btnLoadNab = new System.Windows.Forms.Button();
            this.btnCreateFile = new System.Windows.Forms.Button();
            this.btnExportData = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // lblFileds
            // 
            this.lblFileds.AutoSize = true;
            this.lblFileds.Location = new System.Drawing.Point(12, 8);
            this.lblFileds.Name = "lblFileds";
            this.lblFileds.Size = new System.Drawing.Size(138, 13);
            this.lblFileds.TabIndex = 5;
            this.lblFileds.Text = "Поля для експорту даних:";
            // 
            // chkdLstBoxField
            // 
            this.chkdLstBoxField.FormattingEnabled = true;
            this.chkdLstBoxField.Location = new System.Drawing.Point(12, 26);
            this.chkdLstBoxField.Name = "chkdLstBoxField";
            this.chkdLstBoxField.Size = new System.Drawing.Size(508, 394);
            this.chkdLstBoxField.TabIndex = 4;
            // 
            // chkBoxSelectAll
            // 
            this.chkBoxSelectAll.AutoSize = true;
            this.chkBoxSelectAll.Location = new System.Drawing.Point(15, 426);
            this.chkBoxSelectAll.Name = "chkBoxSelectAll";
            this.chkBoxSelectAll.Size = new System.Drawing.Size(85, 17);
            this.chkBoxSelectAll.TabIndex = 6;
            this.chkBoxSelectAll.Text = "Вибрати всі";
            this.chkBoxSelectAll.UseVisualStyleBackColor = true;
            this.chkBoxSelectAll.CheckedChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
            // 
            // lblNabor
            // 
            this.lblNabor.AutoSize = true;
            this.lblNabor.Location = new System.Drawing.Point(537, 10);
            this.lblNabor.Name = "lblNabor";
            this.lblNabor.Size = new System.Drawing.Size(143, 13);
            this.lblNabor.TabIndex = 13;
            this.lblNabor.Text = "Список створених наборів:";
            // 
            // lstBoxNabor
            // 
            this.lstBoxNabor.FormattingEnabled = true;
            this.lstBoxNabor.Location = new System.Drawing.Point(540, 26);
            this.lstBoxNabor.Name = "lstBoxNabor";
            this.lstBoxNabor.Size = new System.Drawing.Size(177, 446);
            this.lstBoxNabor.TabIndex = 12;
            this.lstBoxNabor.SelectedIndexChanged += new System.EventHandler(this.lstBoxNabor_SelectedIndexChanged);
            // 
            // btnLoadNab
            // 
            this.btnLoadNab.Location = new System.Drawing.Point(398, 449);
            this.btnLoadNab.Name = "btnLoadNab";
            this.btnLoadNab.Size = new System.Drawing.Size(122, 23);
            this.btnLoadNab.TabIndex = 14;
            this.btnLoadNab.Text = "Завантажити набір";
            this.btnLoadNab.UseVisualStyleBackColor = true;
            this.btnLoadNab.Click += new System.EventHandler(this.btnLoadNab_Click);
            // 
            // btnCreateFile
            // 
            this.btnCreateFile.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnCreateFile.Location = new System.Drawing.Point(259, 449);
            this.btnCreateFile.Name = "btnCreateFile";
            this.btnCreateFile.Size = new System.Drawing.Size(122, 23);
            this.btnCreateFile.TabIndex = 15;
            this.btnCreateFile.Text = "Створити файл";
            this.btnCreateFile.UseVisualStyleBackColor = true;
            this.btnCreateFile.Click += new System.EventHandler(this.btnCreateFile_Click);
            // 
            // btnExportData
            // 
            this.btnExportData.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnExportData.Location = new System.Drawing.Point(119, 450);
            this.btnExportData.Name = "btnExportData";
            this.btnExportData.Size = new System.Drawing.Size(122, 23);
            this.btnExportData.TabIndex = 16;
            this.btnExportData.Text = "Експортувати дані";
            this.btnExportData.UseVisualStyleBackColor = true;
            this.btnExportData.Click += new System.EventHandler(this.btnExportData_Click);
            // 
            // DesignNaborForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 485);
            this.Controls.Add(this.btnExportData);
            this.Controls.Add(this.btnCreateFile);
            this.Controls.Add(this.btnLoadNab);
            this.Controls.Add(this.lblNabor);
            this.Controls.Add(this.lstBoxNabor);
            this.Controls.Add(this.chkBoxSelectAll);
            this.Controls.Add(this.lblFileds);
            this.Controls.Add(this.chkdLstBoxField);
            this.Name = "DesignNaborForm";
            this.Text = "Налаштування експорту реєстру РЧП";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFileds;
        private System.Windows.Forms.CheckedListBox chkdLstBoxField;
        private System.Windows.Forms.CheckBox chkBoxSelectAll;
        private System.Windows.Forms.Label lblNabor;
        private System.Windows.Forms.ListBox lstBoxNabor;
        private System.Windows.Forms.Button btnLoadNab;
        public System.Windows.Forms.Button btnCreateFile;
        public System.Windows.Forms.Button btnExportData;
        public System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}