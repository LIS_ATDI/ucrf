﻿namespace XICSM.UcrfRfaNET.ApplSource
{
    partial class DiffReport2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiffReport2Form));
            this.gbParam = new System.Windows.Forms.GroupBox();
            this.cbDate2 = new System.Windows.Forms.CheckBox();
            this.cbDate1 = new System.Windows.Forms.CheckBox();
            this.lblTimeFrom = new System.Windows.Forms.Label();
            this.lblTimeTo = new System.Windows.Forms.Label();
            this.timeTo2 = new System.Windows.Forms.DateTimePicker();
            this.timeFrom2 = new System.Windows.Forms.DateTimePicker();
            this.dateMeasurement2 = new System.Windows.Forms.DateTimePicker();
            this.lblDate2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.dateMeasurement = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbExecutorUser = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbViseUser = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDevice = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.gbLink = new System.Windows.Forms.GroupBox();
            this.lblLink = new System.Windows.Forms.Label();
            this.gbParam.SuspendLayout();
            this.gbLink.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbParam
            // 
            this.gbParam.Controls.Add(this.cbDate2);
            this.gbParam.Controls.Add(this.cbDate1);
            this.gbParam.Controls.Add(this.lblTimeFrom);
            this.gbParam.Controls.Add(this.lblTimeTo);
            this.gbParam.Controls.Add(this.timeTo2);
            this.gbParam.Controls.Add(this.timeFrom2);
            this.gbParam.Controls.Add(this.dateMeasurement2);
            this.gbParam.Controls.Add(this.lblDate2);
            this.gbParam.Controls.Add(this.label6);
            this.gbParam.Controls.Add(this.label5);
            this.gbParam.Controls.Add(this.timeTo);
            this.gbParam.Controls.Add(this.timeFrom);
            this.gbParam.Controls.Add(this.dateMeasurement);
            this.gbParam.Controls.Add(this.label4);
            this.gbParam.Controls.Add(this.cbExecutorUser);
            this.gbParam.Controls.Add(this.label3);
            this.gbParam.Controls.Add(this.cbViseUser);
            this.gbParam.Controls.Add(this.label2);
            this.gbParam.Controls.Add(this.cbDevice);
            this.gbParam.Controls.Add(this.label1);
            this.gbParam.Location = new System.Drawing.Point(12, 12);
            this.gbParam.Name = "gbParam";
            this.gbParam.Size = new System.Drawing.Size(559, 164);
            this.gbParam.TabIndex = 0;
            this.gbParam.TabStop = false;
            this.gbParam.Text = "Параметри протоколу";
            // 
            // cbDate2
            // 
            this.cbDate2.AutoSize = true;
            this.cbDate2.Location = new System.Drawing.Point(197, 75);
            this.cbDate2.Name = "cbDate2";
            this.cbDate2.Size = new System.Drawing.Size(15, 14);
            this.cbDate2.TabIndex = 14;
            this.cbDate2.UseVisualStyleBackColor = true;
            // 
            // cbDate1
            // 
            this.cbDate1.AutoSize = true;
            this.cbDate1.Location = new System.Drawing.Point(197, 50);
            this.cbDate1.Name = "cbDate1";
            this.cbDate1.Size = new System.Drawing.Size(15, 14);
            this.cbDate1.TabIndex = 9;
            this.cbDate1.UseVisualStyleBackColor = true;
            // 
            // lblTimeFrom
            // 
            this.lblTimeFrom.AutoSize = true;
            this.lblTimeFrom.Location = new System.Drawing.Point(342, 76);
            this.lblTimeFrom.Name = "lblTimeFrom";
            this.lblTimeFrom.Size = new System.Drawing.Size(13, 13);
            this.lblTimeFrom.TabIndex = 13;
            this.lblTimeFrom.Text = "з";
            this.lblTimeFrom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTimeTo
            // 
            this.lblTimeTo.AutoSize = true;
            this.lblTimeTo.Location = new System.Drawing.Point(433, 77);
            this.lblTimeTo.Name = "lblTimeTo";
            this.lblTimeTo.Size = new System.Drawing.Size(19, 13);
            this.lblTimeTo.TabIndex = 12;
            this.lblTimeTo.Text = "по";
            // 
            // timeTo2
            // 
            this.timeTo2.CustomFormat = "HH:mm";
            this.timeTo2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo2.Location = new System.Drawing.Point(480, 72);
            this.timeTo2.Name = "timeTo2";
            this.timeTo2.ShowUpDown = true;
            this.timeTo2.Size = new System.Drawing.Size(61, 20);
            this.timeTo2.TabIndex = 6;
            // 
            // timeFrom2
            // 
            this.timeFrom2.CustomFormat = "HH:mm";
            this.timeFrom2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFrom2.Location = new System.Drawing.Point(361, 73);
            this.timeFrom2.Name = "timeFrom2";
            this.timeFrom2.ShowUpDown = true;
            this.timeFrom2.Size = new System.Drawing.Size(60, 20);
            this.timeFrom2.TabIndex = 5;
            // 
            // dateMeasurement2
            // 
            this.dateMeasurement2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateMeasurement2.Location = new System.Drawing.Point(218, 72);
            this.dateMeasurement2.Name = "dateMeasurement2";
            this.dateMeasurement2.Size = new System.Drawing.Size(102, 20);
            this.dateMeasurement2.TabIndex = 4;
            // 
            // lblDate2
            // 
            this.lblDate2.AutoSize = true;
            this.lblDate2.Location = new System.Drawing.Point(16, 77);
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Size = new System.Drawing.Size(111, 13);
            this.lblDate2.TabIndex = 17;
            this.lblDate2.Text = "Дата вимірювання 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(342, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "з";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(433, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "по";
            // 
            // timeTo
            // 
            this.timeTo.CustomFormat = "HH:mm";
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(480, 46);
            this.timeTo.Name = "timeTo";
            this.timeTo.ShowUpDown = true;
            this.timeTo.Size = new System.Drawing.Size(61, 20);
            this.timeTo.TabIndex = 3;
            // 
            // timeFrom
            // 
            this.timeFrom.CustomFormat = "HH:mm";
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFrom.Location = new System.Drawing.Point(361, 47);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.ShowUpDown = true;
            this.timeFrom.Size = new System.Drawing.Size(60, 20);
            this.timeFrom.TabIndex = 2;
            // 
            // dateMeasurement
            // 
            this.dateMeasurement.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateMeasurement.Location = new System.Drawing.Point(218, 46);
            this.dateMeasurement.Name = "dateMeasurement";
            this.dateMeasurement.Size = new System.Drawing.Size(102, 20);
            this.dateMeasurement.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Дата вимірювання";
            // 
            // cbExecutorUser
            // 
            this.cbExecutorUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExecutorUser.FormattingEnabled = true;
            this.cbExecutorUser.Location = new System.Drawing.Point(218, 126);
            this.cbExecutorUser.Name = "cbExecutorUser";
            this.cbExecutorUser.Size = new System.Drawing.Size(323, 21);
            this.cbExecutorUser.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Виконавець";
            // 
            // cbViseUser
            // 
            this.cbViseUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbViseUser.FormattingEnabled = true;
            this.cbViseUser.Location = new System.Drawing.Point(218, 99);
            this.cbViseUser.Name = "cbViseUser";
            this.cbViseUser.Size = new System.Drawing.Size(323, 21);
            this.cbViseUser.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Візує";
            // 
            // cbDevice
            // 
            this.cbDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDevice.FormattingEnabled = true;
            this.cbDevice.Location = new System.Drawing.Point(218, 20);
            this.cbDevice.Name = "cbDevice";
            this.cbDevice.Size = new System.Drawing.Size(323, 21);
            this.cbDevice.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Засіб радіочастотного контролю";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 182);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(496, 182);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Вийти";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(111, 182);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(138, 23);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Друк протоколу";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // gbLink
            // 
            this.gbLink.Controls.Add(this.lblLink);
            this.gbLink.Location = new System.Drawing.Point(12, 211);
            this.gbLink.Name = "gbLink";
            this.gbLink.Size = new System.Drawing.Size(559, 55);
            this.gbLink.TabIndex = 4;
            this.gbLink.TabStop = false;
            this.gbLink.Text = "Збережений протокол";
            // 
            // lblLink
            // 
            this.lblLink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblLink.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblLink.Location = new System.Drawing.Point(6, 16);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(547, 27);
            this.lblLink.TabIndex = 0;
            this.lblLink.Text = "label10";
            this.lblLink.DoubleClick += new System.EventHandler(this.lblLink_DoubleClick);
            // 
            // DiffReport2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 280);
            this.Controls.Add(this.gbLink);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DiffReport2Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Генерація протоколу інструментальної оцінки параметрів";
            this.Load += new System.EventHandler(this.DiffReport2Form_Load);
            this.gbParam.ResumeLayout(false);
            this.gbParam.PerformLayout();
            this.gbLink.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbParam;
        private System.Windows.Forms.ComboBox cbDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker timeTo;
        private System.Windows.Forms.DateTimePicker timeFrom;
        private System.Windows.Forms.DateTimePicker dateMeasurement;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbExecutorUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbViseUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTimeFrom;
        private System.Windows.Forms.Label lblTimeTo;
        private System.Windows.Forms.DateTimePicker timeTo2;
        private System.Windows.Forms.DateTimePicker timeFrom2;
        private System.Windows.Forms.DateTimePicker dateMeasurement2;
        private System.Windows.Forms.Label lblDate2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.GroupBox gbLink;
        private System.Windows.Forms.CheckBox cbDate2;
        private System.Windows.Forms.CheckBox cbDate1;
        private System.Windows.Forms.Label lblLink;
    }
}