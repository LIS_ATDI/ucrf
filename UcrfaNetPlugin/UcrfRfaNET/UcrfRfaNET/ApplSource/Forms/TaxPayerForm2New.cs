﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.Extension;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Reports;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    class TaxPayerForm2New : ITaxPayerForm
    {
        private const string FormType = "Форма 2";

        internal class FreqBand
        {
            public double MaxFreq{get; set;}
            public double MinFreq{get; set;}

            public FreqBand(double minFreq, double maxFreq)
            {
                MaxFreq = maxFreq;
                MinFreq = minFreq;
            }
        }
        /// <summary>
        /// Данные для записи
        /// </summary>
        class DataItem
        {
            public string OwnerName { get; set; }
            public string OwnerRegNum { get; set; }
            public string OwnerAddress { get; set; }
            public string DozvNum { get; set; }
            public DateTime DozvDateFrom { get; set; }
            public DateTime DozvDateTo { get; set; }
            public string DozvAddress { get; set; }
            public DateTime DozvCancelDate { get; set; }
            public string OrderCode { get; set; }
            public string OrderSymbol { get; set; }
            public string Province { get; set; }
            public List<FreqBand> ListFreqBand { get; set; }
            public List<double> ListFreq { get; private set; }
            public string TableName { get; set; }
            public double Bw { get; set; }
            public bool IsValid { get; set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public DataItem()
            {
                OwnerName = "";
                OwnerRegNum = "";
                OwnerAddress = "";
                DozvNum = "";
                DozvDateFrom = IM.NullT;
                DozvDateTo = IM.NullT;
                DozvAddress = "";
                DozvCancelDate = IM.NullT;
                OrderCode = "";
                OrderSymbol = "";
                Province = "";
                ListFreqBand = new List<FreqBand>();
                ListFreq = new List<double>();
                TableName = "";
                Bw = 0.0;
                IsValid = true;
            }

            public bool ContainFreq(params double[] freqs)
            {
                foreach (double freq in freqs)
                {
                    foreach (double list in ListFreq)
                    {
                        if (Math.Abs(freq - list) < 0.0001)
                            return true;
                    }
                }
                return false;
            }

            public void AddFreq(params double[] freqs)
            {
                foreach (double freq in freqs)
                {
                    bool contain = false;
                    foreach (double list in ListFreq)
                    {
                        if (Math.Abs(freq - list) < 0.0001)
                        {
                            contain = true;
                            break;
                        }
                    }
                    if (contain == false)
                        ListFreq.Add(freq);
                }
            }
        }
        /// <summary>
        /// Класс работы с данными
        /// </summary>
        class WriteData
        {
            private readonly Dictionary<string, DataItem> _data;
            /// <summary>
            /// Конструктор
            /// </summary>
            public WriteData()
            {
                _data = new Dictionary<string, DataItem>();
            }
            /// <summary>
            /// Добавить запись
            /// </summary>
            public string[] Add(IDictionary<string, string> radioSystem,
                            int id,
                            int recId,
                            string ownerName,
                            string ownerRegNum,
                            string ownerAddress,
                            string docNum,
                            DateTime docDateFrom,
                            DateTime docDateTo,
                            string docAddress,
                            string orderSymbol,
                            string province,
                            string tableName,
                            string standard,
                            string txFreqTxt,
                            string rxFreqTxt,
                            double bw,
                            DateTime dozvCancelDate)
            {
                List<string> errorList = new List<string>();

                if (string.IsNullOrEmpty(docNum))
                    errorList.Add(string.Format("{1}: Номер дозволу відсутній: ID запису = {0}", id, FormType));
                if (docDateFrom == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата початку дозволу: ID запису = {0}", id, FormType));
                if (docDateTo == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата закінчення дозволу: ID запису = {0}", id, FormType));
                if (string.IsNullOrEmpty(orderSymbol))
                    errorList.Add(string.Format("{1}: Недостатньо інформації для визначення позиції Тарифів: ID запису = {0}", id, FormType));
                else if (radioSystem.ContainsKey(orderSymbol) == false)
                    errorList.Add(string.Format("{1}: Відсутній опис для коду позиції Тарифів '{0}': ID запису = {2}", orderSymbol, FormType, id));
                if (string.IsNullOrEmpty(province))
                    errorList.Add(string.Format("{1}: Відсутній регіон використання: ID запису = {0}", id, FormType));
                if (bw == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня загальна ширина смуги радіочастот: ID запису = {0}", id, FormType));

                string key = ownerRegNum + orderSymbol + province + tableName + recId;
                switch (tableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        switch (standard)
                        {
                            case CRadioTech.SB:
                            case CRadioTech.RPATL:
                                key = ownerRegNum + orderSymbol + province + tableName + recId;
                                break;
                            case CRadioTech.BCUR:
                            case CRadioTech.BAUR:
                                {
                                    double[] freqTx = txFreqTxt.ToDoubleArray('-');
                                    FreqBand[] freq = CreateFreqBand(bw, freqTx);
                                    if (freq.Count() > 0)
                                       key = ownerRegNum + orderSymbol + province + tableName + freq[0].MinFreq + freq[0].MaxFreq;
                                    else
                                        errorList.Add(string.Format("Помилка перетворення частот '{0}', ID запису:{1}", txFreqTxt, recId));
                                }
                                break;
                        }
                        break;
                    case PlugTbl.Ship:
                    case ICSMTbl.itblMobStation:
                         key = ownerRegNum + orderSymbol + province + tableName + recId;
                        break;
                }

                if (errorList.Count > 0)
                    return errorList.ToArray();
                DataItem di = null;
                if (_data.ContainsKey(key))
                {
                    di = _data[key];
                    di.DozvCancelDate = IM.NullT; // в случае множ. разрешений игнор? а остальные даты? записи, вообще, отсеиваются по дате аннулирования? 
                }
                else
                {
                    di = new DataItem();
                    _data.Add(key, di);
                    di.OwnerName = ownerName;
                    di.OwnerRegNum = ownerRegNum;
                    di.OwnerAddress = ownerAddress;
                    di.DozvNum = docNum;
                    di.DozvDateFrom = docDateFrom;
                    di.DozvDateTo = docDateTo;
                    di.DozvAddress = docAddress;
                    di.OrderSymbol = orderSymbol;
                    di.Province = province;
                    di.TableName = tableName;
                    di.DozvCancelDate = dozvCancelDate;
                    if (radioSystem.ContainsKey(orderSymbol))
                        di.OrderCode = radioSystem[di.OrderSymbol];
                }
                if (di.DozvDateFrom > docDateFrom)
                {
                    di.DozvNum = docNum;
                    di.DozvDateFrom = docDateFrom;
                    di.DozvDateTo = docDateTo;
                    di.DozvAddress = docAddress;
                }
                switch (tableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        switch (standard)
                        {
                            case CRadioTech.BCUR:
                            case CRadioTech.BAUR:
                                if (di.Bw == IM.NullD)
                                    di.Bw = bw;
                                if (di.Bw < bw)
                                    di.Bw = bw;
                                break;
                            default:
                                di.Bw = bw;
                                break;
                        }
                        break;
                    case PlugTbl.Ship:
                        di.Bw = bw;
                        break;
                    case ICSMTbl.itblMobStation:
                        di.AddFreq(txFreqTxt.ToDoubleArray(';'));
                        di.AddFreq(rxFreqTxt.ToDoubleArray(';'));
                        di.Bw = bw;
                        break;
                    default:
                        {
                            double[] freqTx = txFreqTxt.ToDoubleArray(';');
                            double[] freqRx = rxFreqTxt.ToDoubleArray(';');
                            List<FreqBand> freq = new List<FreqBand>();
                            freq.AddRange(CreateFreqBand(bw, freqTx));
                            freq.AddRange(CreateFreqBand(bw, freqRx));
                            di.ListFreqBand.AddRange(freq);
                            di.ListFreqBand = MergeFreqBand(di.ListFreqBand.ToArray()).ToList();
                            di.Bw = CalculateBw(di.ListFreqBand.ToArray());
                        }
                        break;
                }
                return errorList.ToArray();
            }
            /// <summary>
            /// Сохранить в файл
            /// </summary>
            /// <param name="path">Путь к папке</param>
            /// <param name="province">область</param>
            /// <param name="repType"></param>
            public void Save(string path, string province, ReportType repType)
            {
                if (_data.Count == 0)
                    return;
                MergeData();
                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Запис в файл")))
                {
                    pb.SetBig("Запис в файл...");
                    using (IReport rep = Report.CreateReport(repType))
                    {
                        rep.Init(Path.Combine(path, string.Format("форма2{0}{1}", string.IsNullOrEmpty(province) ? "" : "-", province)), "Page1","");
                        List<string> line = new List<string>();
                        line.Add("№ з/п{0}");
                        line.Add("Найменування користувача РЧР /ПІБ для користувача РЧР фізичної особи");
                        line.Add("Код ЄДРПОУ/Реєстраційний номер облікової картки платника податків для фізичних осіб суб’єктів господарювання");
                        line.Add("Місцезнаходження користувача – юридичної особи /Місце проживання фізичної особи");
                        line.Add("Перший дозвіл на експлуатацію РЕЗ, ВП на присвоєному номіналі радіочастоти - Номер");
                        line.Add("Перший дозвіл на експлуатацію РЕЗ, ВП на присвоєному номіналі радіочастоти – дата видачі дд.мм.рррр");
                        line.Add("Перший дозвіл на експлуатацію РЕЗ, ВП на присвоєному номіналі радіочастоти – термін дії до мм.рррр");
                        line.Add("Перший дозвіл на експлуатацію РЕЗ, ВП на присвоєному номіналі радіочастоти - Адреса місця встановлення РЕЗ, ВП");
                        line.Add("Вид радіозв’язку");
                        line.Add("Номер позиції статті 320 Податку кодексу України, якою встановлено ставки збору за користування РЧР");
                        line.Add("Назва регіонів користування РЧР");
                        line.Add("Загальна ширина смуги радіочастот, зазначена в дозволі (МГц)");
                        line.Add("Дата анулювання дозволу дд.мм.рррр");
                        line.Add("Примітки");
                        rep.WriteLine(line.ToArray());

                        int counter = 1;
                        int cntRecord = 1;
                        foreach (KeyValuePair<string, DataItem> item in _data)
                        {
                            pb.SetSmall(cntRecord++, _data.Count);
                            line.Clear();
                            DataItem di = item.Value;
                            if (di.IsValid == false)
                                continue;
                            //Подсчитываем кол-во областей
                            int countProvince = 1;
                            if (string.IsNullOrEmpty(di.Province) == false)
                            {
                                if (di.Province.ToLower().Contains("україна"))
                                    countProvince = 27;
                                else
                                    countProvince = di.Province.Split(';').Count();
                            }

                            line.Add(string.Format("{0}", counter++));
                            line.Add(di.OwnerName);
                            line.Add(di.OwnerRegNum);
                            line.Add(di.OwnerAddress);
                            line.Add(di.DozvNum);
                            line.Add(di.DozvDateFrom.ToStringNullT("dd.MM.yyyy"));
                            line.Add(di.DozvDateTo.ToStringNullT("dd.MM.yyyy"));
                            line.Add(string.IsNullOrEmpty(di.DozvAddress) ? "" : di.DozvAddress.Replace(';', '/'));
                            line.Add(di.OrderCode);
                            line.Add(di.OrderSymbol);
                            line.Add(string.IsNullOrEmpty(di.Province) ? "" : di.Province.Replace(';', '/'));
                            if (di.TableName == ICSMTbl.itblMobStation)
                                line.Add((di.Bw * di.ListFreq.Count).Round(4).ToString());
                            else
                                line.Add((di.Bw * countProvince).Round(4).ToString());
                            line.Add(di.DozvCancelDate.ToStringNullT("dd.MM.yyyy"));
                            rep.WriteLine(line.ToArray());

                            if (pb.UserCanceled())
                                break;
                        }
                        if (!pb.UserCanceled())
                            rep.Save();
                    }
                }
            }

            private void MergeData()
            {
                bool needRepeat = false;
                foreach (KeyValuePair<string, DataItem> dataTop in _data)
                {
                    DataItem itemTop = dataTop.Value;
                    switch (itemTop.TableName)
                    {
                        case ICSMTbl.itblMobStation:
                            if (itemTop.ListFreq.Count > 0)
                            {
                                foreach (KeyValuePair<string, DataItem> dataLow in _data)
                                {
                                    DataItem itemLow = dataLow.Value;
                                    if ((itemLow != itemTop) &&
                                        (itemLow.TableName == itemTop.TableName) &&
                                        (itemLow.ListFreq.Count > 0) && (itemLow.OwnerRegNum == itemTop.OwnerRegNum))
                                    {
                                        double[] freqs = itemLow.ListFreq.ToArray();
                                        if(itemTop.ContainFreq(freqs))
                                        {
                                            needRepeat = true;
                                            itemTop.AddFreq(freqs);
                                            if (itemTop.Bw < itemLow.Bw)
                                                itemTop.Bw = itemLow.Bw;
                                            itemLow.ListFreq.Clear();
                                            itemLow.IsValid = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        case PlugTbl.Ship:
                        case PlugTbl.XfaAbonentStation:
                            break;
                        default:
                            if (itemTop.ListFreqBand.Count > 0)
                            {
                                foreach (KeyValuePair<string, DataItem> dataLow in _data)
                                {
                                    DataItem itemLow = dataLow.Value;
                                    if ((itemLow != itemTop) &&
                                        (itemLow.TableName == itemTop.TableName) &&
                                        (itemLow.ListFreqBand.Count > 0) && (itemLow.OwnerRegNum == itemTop.OwnerRegNum))
                                    {
                                        List<FreqBand> freq = new List<FreqBand>();
                                        freq.AddRange(itemLow.ListFreqBand);
                                        freq.AddRange(itemTop.ListFreqBand);
                                        int count = freq.Count;
                                        FreqBand[] freqMerged = MergeFreqBand(freq.ToArray());
                                        if (count != freqMerged.Count())
                                        {
                                            itemTop.ListFreqBand.Clear();
                                            itemTop.ListFreqBand.AddRange(freqMerged);
                                            itemTop.Bw = CalculateBw(itemTop.ListFreqBand.ToArray());
                                            itemLow.ListFreqBand.Clear();
                                            itemLow.IsValid = false;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
                if (needRepeat)
                    MergeData();
            }
        }

        private DateTime _dateBeg;
        private DateTime _dateEnd;
        private string _path;
        private string _tableName;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="date">Дата формирования</param>
        /// <param name="path">Путь к файлам</param>
        /// <param name="tableName">Имя таблицы</param>
        public TaxPayerForm2New(TaxPayerReportParams _params, string tableName)
        {
            _dateBeg = _params.DateBeg;
            _dateEnd = _params.DateEnd;
            _path = _params.FilePath;
            _tableName = tableName;
        }
        /// <summary>
        /// Начать процесс генерации документов
        /// </summary>
        /// <returns>список ошибок</returns>
        public string[] Generate(ReportType repType, bool isOneFile)
        {
            List<string> errorList = new List<string>();
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування переліку користувачів РЧР (Форма 2)")))
            {
                Dictionary<string, string> radioSystem = EriFiles.GetEriCodeAndDescr("Fees");
                pb.SetProgress(0, 50000);
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                string lastProvince = "";
                WriteData wd = new WriteData();
                using (LisRecordSet rs = new LisRecordSet(PlugTbl.XvTaxPayerForm2, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID");
                    rs.Select("OWNER_PROVINCE");
                    rs.Select("OWNER_NAME");
                    rs.Select("OWNER_CODE");
                    rs.Select("OWNER_ADDRESS");
                    rs.Select("DOC_NUM");
                    rs.Select("DOC_DATE_FROM");
                    rs.Select("DOC_DATE_TO");
                    rs.Select("ADDRESS");
                    rs.Select("FEES_ID");
                    rs.Select("Fees.ORDER_SYMBOL");
                    rs.Select("PROVINCE");
                    rs.Select("TX_FREQ_TXT");
                    rs.Select("RX_FREQ_TXT");
                    rs.Select("TABLE_NAME");
                    rs.Select("TABLE_ID");
                    rs.Select("STANDARD");
                    rs.Select("CLASS");
                    rs.Select("EXPL_TYPE");
                    rs.Select("BW");
                    rs.Select("DOZV_DATE_CANCEL");
                    rs.Select("OWNER_ID");
                    rs.SetWhere("DOC_DATE_FROM", IMRecordset.Operation.Le, _dateEnd);
                    rs.SetWhere("DOC_DATE_TO", IMRecordset.Operation.Ge, _dateBeg);
                    if (string.IsNullOrEmpty(_tableName) == false)
                        rs.SetWhere("TABLE_NAME", IMRecordset.Operation.Like, _tableName);
                    rs.OrderBy("OWNER_PROVINCE", OrderDirection.Ascending);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (pb.UserCanceled())
                        {
                            errorList.Add("Перервано користувачем.");
                            break;
                        }
                        pb.Increment(true);
                        int id = rs.GetI("ID");
                        int recId = rs.GetI("TABLE_ID");

                        DateTime dtPermEnd = rs.GetT("DOZV_DATE_CANCEL");
                        if ((dtPermEnd != IM.NullT) && (dtPermEnd < _dateBeg))
                            continue;

                        string provinceOwner = rs.GetS("OWNER_PROVINCE");
                        if (string.IsNullOrEmpty(provinceOwner))
                        {
                            //нет обоасти
                            errorList.Add(string.Format("Форма 2: Відсутня область власника: ID запису = {0}, ID власника = {1}", id, rs.GetI("OWNER_ID")));
                            provinceOwner = "(без регіону)";
                        }

                        if ((provinceOwner != lastProvince) && (isOneFile == false))
                        {
                            if (string.IsNullOrEmpty(lastProvince) == false)
                            {
                                pb.SetBig("Запис в файл...");
                                wd.Save(_path, lastProvince, repType);
                            }
                            lastProvince = provinceOwner;
                            wd = new WriteData();
                            pb.SetBig(lastProvince);
                        }
                        string[] error = wd.Add(radioSystem, id, recId, rs.GetS("OWNER_NAME"),
                                                rs.GetS("OWNER_CODE"), rs.GetS("OWNER_ADDRESS"),
                                                rs.GetS("DOC_NUM"), rs.GetT("DOC_DATE_FROM"),
                                                rs.GetT("DOC_DATE_TO"), rs.GetS("ADDRESS"),
                                                rs.GetS("Fees.ORDER_SYMBOL"), rs.GetS("PROVINCE"),
                                                rs.GetS("TABLE_NAME"), rs.GetS("STANDARD"),
                                                rs.GetS("TX_FREQ_TXT"), rs.GetS("RX_FREQ_TXT"),
                                                rs.GetD("BW"), rs.GetT("DOZV_DATE_CANCEL"));
                        errorList.AddRange(error);
                    }
                    if (!pb.UserCanceled())
                    {
                        if (string.IsNullOrEmpty(lastProvince) == false)
                            wd.Save(_path, lastProvince, repType);
                        else if (isOneFile)
                            wd.Save(_path, "", repType);
                    }
                }
            }
            return errorList.ToArray();
        }
        /// <summary>
        /// Сливает полосы частот
        /// </summary>
        /// <param name="freqBand">Полосы частот</param>
        /// <returns>Слитые полосы частот</returns>
        private static FreqBand[] MergeFreqBand(params FreqBand[] freqBand)
        {
            bool isChanged = false;
            List<FreqBand> retVal = new List<FreqBand>();
            foreach (FreqBand band in freqBand)
            {
                bool isMerged = false;
                foreach (FreqBand freqVal in retVal)
                {
                    if ((freqVal.MinFreq <= band.MaxFreq) && (band.MaxFreq <= freqVal.MaxFreq) && (band.MinFreq <= freqVal.MinFreq))
                    {
                        freqVal.MinFreq = band.MinFreq;
                        isMerged = true;
                    }
                    if ((freqVal.MinFreq <= band.MinFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MaxFreq <= band.MaxFreq))
                    {
                        freqVal.MaxFreq = band.MaxFreq;
                        isMerged = true;
                    }
                    if ((freqVal.MinFreq <= band.MinFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MinFreq <= band.MaxFreq) && (band.MaxFreq <= freqVal.MaxFreq))
                    {
                        isMerged = true;
                    }
                    if ((band.MinFreq <= freqVal.MinFreq) && (freqVal.MinFreq <= band.MaxFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MaxFreq <= band.MaxFreq))
                    {
                        isMerged = true;
                        freqVal.MaxFreq = band.MaxFreq;
                        freqVal.MinFreq = band.MinFreq;
                    }
                    if (isMerged)
                        break;
                }
                if(isMerged == false)
                {
                    //Новая полоса частот
                    retVal.Add(new FreqBand(band.MinFreq, band.MaxFreq));
                }
                else
                {
                    isChanged = true;
                }
            }
            if (isChanged)
                return MergeFreqBand(retVal.ToArray()); //Пытаемся слить новые диапазоны
            return retVal.ToArray();
        }

        /// <summary>
        /// Сотзланеи полос частот
        /// </summary>
        /// <param name="bw">BW</param>
        /// <param name="freqs">список частот</param>
        /// <returns>полосы частот</returns>
        private static FreqBand[] CreateFreqBand(double bw, params double[] freqs)
        {
            List<FreqBand> retVal = new List<FreqBand>();
            bw = bw / 2.0;
            foreach (double freq in freqs)
                retVal.Add(new FreqBand(freq - bw, freq + bw));
            return retVal.ToArray();
        }
        /// <summary>
        /// Подсчитывает BW
        /// </summary>
        /// <param name="freqBand">список частот</param>
        /// <returns>полосы частот</returns>
        private static double CalculateBw(params FreqBand[] freqBand)
        {
            double bw = 0.0;
            foreach (FreqBand freq in freqBand)
                bw += (freq.MaxFreq - freq.MinFreq);
            return bw;
        }
    }
}
