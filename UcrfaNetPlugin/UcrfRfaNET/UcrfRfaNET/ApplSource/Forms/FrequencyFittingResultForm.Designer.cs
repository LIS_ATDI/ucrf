﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class FrequencyFittingResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStationACaption = new System.Windows.Forms.Label();
            this.lblStationBCaption = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblOwner = new System.Windows.Forms.Label();
            this.lblPositionA = new System.Windows.Forms.Label();
            this.lblPositionB = new System.Windows.Forms.Label();
            this.lblRange = new System.Windows.Forms.Label();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.csvFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // resultGrid
            // 
            this.resultGrid.AllowUserToAddRows = false;
            this.resultGrid.AllowUserToDeleteRows = false;
            this.resultGrid.AllowUserToResizeRows = false;
            this.resultGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.resultGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.resultGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultGrid.Location = new System.Drawing.Point(12, 92);
            this.resultGrid.Name = "resultGrid";
            this.resultGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultGrid.Size = new System.Drawing.Size(719, 332);
            this.resultGrid.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Власник РЕЗ:";
            // 
            // lblStationACaption
            // 
            this.lblStationACaption.AutoSize = true;
            this.lblStationACaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStationACaption.Location = new System.Drawing.Point(12, 42);
            this.lblStationACaption.Name = "lblStationACaption";
            this.lblStationACaption.Size = new System.Drawing.Size(138, 13);
            this.lblStationACaption.TabIndex = 2;
            this.lblStationACaption.Text = "Координати станції А:";
            // 
            // lblStationBCaption
            // 
            this.lblStationBCaption.AutoSize = true;
            this.lblStationBCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStationBCaption.Location = new System.Drawing.Point(238, 42);
            this.lblStationBCaption.Name = "lblStationBCaption";
            this.lblStationBCaption.Size = new System.Drawing.Size(138, 13);
            this.lblStationBCaption.TabIndex = 3;
            this.lblStationBCaption.Text = "Координати станції Б:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(457, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Відстань:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(567, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Поріг:";
            // 
            // lblOwner
            // 
            this.lblOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOwner.Location = new System.Drawing.Point(107, 20);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(624, 15);
            this.lblOwner.TabIndex = 6;
            this.lblOwner.Click += new System.EventHandler(this.lblOwner_Click);
            // 
            // lblPositionA
            // 
            this.lblPositionA.Location = new System.Drawing.Point(12, 64);
            this.lblPositionA.Name = "lblPositionA";
            this.lblPositionA.Size = new System.Drawing.Size(202, 15);
            this.lblPositionA.TabIndex = 7;
            // 
            // lblPositionB
            // 
            this.lblPositionB.Location = new System.Drawing.Point(238, 64);
            this.lblPositionB.Name = "lblPositionB";
            this.lblPositionB.Size = new System.Drawing.Size(202, 15);
            this.lblPositionB.TabIndex = 8;
            // 
            // lblRange
            // 
            this.lblRange.Location = new System.Drawing.Point(457, 64);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(63, 15);
            this.lblRange.TabIndex = 9;
            // 
            // lblFrequency
            // 
            this.lblFrequency.Location = new System.Drawing.Point(567, 64);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(60, 15);
            this.lblFrequency.TabIndex = 10;
            this.lblFrequency.Click += new System.EventHandler(this.label9_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Location = new System.Drawing.Point(12, 432);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "Друк";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // csvFileDialog
            // 
            this.csvFileDialog.DefaultExt = "csv";
            this.csvFileDialog.Filter = "CSV files|*.csv";
            this.csvFileDialog.Title = "Друк результатів підбору частот в СSV";
            // 
            // FrequencyFittingResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 467);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lblFrequency);
            this.Controls.Add(this.lblRange);
            this.Controls.Add(this.lblPositionB);
            this.Controls.Add(this.lblPositionA);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblStationBCaption);
            this.Controls.Add(this.lblStationACaption);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resultGrid);
            this.Name = "FrequencyFittingResultForm";
            this.ShowInTaskbar = false;
            this.Text = "Результати підбору частот";
            this.Load += new System.EventHandler(this.FrequencyFittingResultForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView resultGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStationACaption;
        private System.Windows.Forms.Label lblStationBCaption;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.Label lblPositionA;
        private System.Windows.Forms.Label lblPositionB;
        private System.Windows.Forms.Label lblRange;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.SaveFileDialog csvFileDialog;
    }
}