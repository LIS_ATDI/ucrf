﻿using XICSM.UcrfRfaNET.Reports;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Начать процесс генерации документов
    /// </summary>
    /// <returns>список ошибок</returns>
    interface ITaxPayerForm
    {
        string[] Generate(ReportType repType, bool isOneFile);
    }
}
