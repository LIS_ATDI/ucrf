﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.Extension;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Reports;
using System.Windows.Forms;
using OrmCs;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    class TaxPayerForm2 : ITaxPayerForm
    {
        private const string FormType = "Форма 2";
        private static IReport rep = null;

        internal class ListSheet
        {
            public string NameSheet { get; set; }
            public Dictionary<string, string> LstArea { get; set; }

            public ListSheet()
            {
                LstArea = new Dictionary<string, string>();
            }
        }

        internal class FreqBand
        {
            public double MaxFreq{get; set;}
            public double MinFreq{get; set;}

            public FreqBand(double minFreq, double maxFreq)
            {
                MaxFreq = maxFreq;
                MinFreq = minFreq;
            }
        }
        /// <summary>
        /// Данные для записи
        /// </summary>
        class DataItem
        {
            public string OwnerName { get; set; }
            public string OwnerRegNum { get; set; }
            public string OwnerAddress { get; set; }
            public string Owner_Province { get; set; }
            public string DozvNum { get; set; }
            public DateTime DozvDateFrom { get; set; }
            public DateTime DozvDateTo { get; set; }
            public string DozvAddress { get; set; }
            public DateTime DozvCancelDate { get; set; }
            public string OrderCode { get; set; }
            public string OrderSymbol { get; set; }
            public string Province { get; set; }
            public List<FreqBand> ListFreqBand { get; set; }
            public List<double> ListFreq { get; private set; }
            public string TableName { get; set; }
            public double Bw { get; set; }
            public bool IsValid { get; set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public DataItem()
            {
                OwnerName = "";
                OwnerRegNum = "";
                OwnerAddress = "";
                Owner_Province = "";
                DozvNum = "";
                DozvDateFrom = IM.NullT;
                DozvDateTo = IM.NullT;
                DozvAddress = "";
                DozvCancelDate = IM.NullT;
                OrderCode = "";
                OrderSymbol = "";
                Province = "";
                ListFreqBand = new List<FreqBand>();
                ListFreq = new List<double>();
                TableName = "";
                Bw = 0.0;
                IsValid = true;
            }

            public bool ContainFreq(params double[] freqs)
            {
                foreach (double freq in freqs)
                {
                    foreach (double list in ListFreq)
                    {
                        if (Math.Abs(freq - list) < 0.0001)
                            return true;
                    }
                }
                return false;
            }

            public void AddFreq(params double[] freqs)
            {
                foreach (double freq in freqs)
                {
                    bool contain = false;
                    foreach (double list in ListFreq)
                    {
                        if (Math.Abs(freq - list) < 0.0001)
                        {
                            contain = true;
                            break;
                        }
                    }
                    if (contain == false)
                        ListFreq.Add(freq);
                }
            }
        }
        /// <summary>
        /// Класс работы с данными
        /// </summary>
        class WriteData
        {
            private readonly Dictionary<string, DataItem> _data;
            /// <summary>
            /// Конструктор
            /// </summary>
            public WriteData()
            {
                _data = new Dictionary<string, DataItem>();
            }
            /// <summary>
            /// Добавить запись
            /// </summary>
            public string[] Add(IDictionary<string, string> radioSystem,
                            int id,
                            int recId,
                            string ownerName,
                            string ownerRegNum,
                            string ownerAddress,
                            string Owner_Province, 
                            string docNum,
                            DateTime docDateFrom,
                            DateTime docDateTo,
                            string docAddress,
                            string orderSymbol,
                            string province,
                            string tableName,
                            string standard,
                            string txFreqTxt,
                            string rxFreqTxt,
                            double bw,
                            DateTime dozvCancelDate)
            {
                List<string> errorList = new List<string>();

                if (string.IsNullOrEmpty(docNum))
                    errorList.Add(string.Format("{1}: Номер дозволу відсутній: ID запису = {0}", id, FormType));
                if (docDateFrom == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата початку дозволу: ID запису = {0}", id, FormType));
                if (docDateTo == IM.NullT)
                    errorList.Add(string.Format("{1}: Відсутня дата закінчення дозволу: ID запису = {0}", id, FormType));
                if (string.IsNullOrEmpty(orderSymbol))
                    errorList.Add(string.Format("{1}: Недостатньо інформації для визначення позиції Тарифів: ID запису = {0}", id, FormType));
                else if (radioSystem.ContainsKey(orderSymbol) == false)
                    errorList.Add(string.Format("{1}: Відсутній опис для коду позиції Тарифів '{0}': ID запису = {2}", orderSymbol, FormType, id));
                if (string.IsNullOrEmpty(province))
                    errorList.Add(string.Format("{1}: Відсутній регіон використання: ID запису = {0}", id, FormType));
                if (bw == IM.NullD)
                    errorList.Add(string.Format("{1}: Відсутня загальна ширина смуги радіочастот: ID запису = {0}", id, FormType));

                string key = ownerRegNum + orderSymbol + province + tableName + recId;
                switch (tableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        switch (standard)
                        {
                            case CRadioTech.SB:
                            case CRadioTech.RPATL:
                                key = ownerRegNum + orderSymbol + province + tableName + recId;
                                break;
                            case CRadioTech.BCUR:
                            case CRadioTech.BAUR:
                                {
                                    double[] freqTx = txFreqTxt.ToDoubleArray('-');
                                    FreqBand[] freq = CreateFreqBand(bw, freqTx);
                                    if (freq.Count() > 0)
                                       key = ownerRegNum + orderSymbol + province + tableName + freq[0].MinFreq + freq[0].MaxFreq;
                                    else
                                        errorList.Add(string.Format("Помилка перетворення частот '{0}', ID запису:{1}", txFreqTxt, recId));
                                }
                                break;
                        }
                        break;
                    case PlugTbl.Ship:
                    case ICSMTbl.itblMobStation:
                         key = ownerRegNum + orderSymbol + province + tableName + recId;
                        break;
                }

                if (errorList.Count > 0)
                    return errorList.ToArray();
                DataItem di = null;
                if (_data.ContainsKey(key))
                {
                    di = _data[key];
                    di.DozvCancelDate = IM.NullT; // в случае множ. разрешений игнор? а остальные даты? записи, вообще, отсеиваются по дате аннулирования? 
                }
                else
                {
                    di = new DataItem();
                    _data.Add(key, di);
                    di.OwnerName = ownerName;
                    di.OwnerRegNum = ownerRegNum;
                    di.OwnerAddress = ownerAddress;
                    di.Owner_Province = Owner_Province;
                    di.DozvNum = docNum;
                    di.DozvDateFrom = docDateFrom;
                    di.DozvDateTo = docDateTo;
                    di.DozvAddress = docAddress;
                    di.OrderSymbol = orderSymbol;
                    di.Province = province;
                    di.TableName = tableName;
                    di.DozvCancelDate = dozvCancelDate;
                    if (radioSystem.ContainsKey(orderSymbol))
                        di.OrderCode = radioSystem[di.OrderSymbol];
                }
                if (di.DozvDateFrom > docDateFrom)
                {
                    di.DozvNum = docNum;
                    di.DozvDateFrom = docDateFrom;
                    di.DozvDateTo = docDateTo;
                    di.DozvAddress = docAddress;
                }
                switch (tableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        switch (standard)
                        {
                            case CRadioTech.BCUR:
                            case CRadioTech.BAUR:
                                if (di.Bw == IM.NullD)
                                    di.Bw = bw;
                                if (di.Bw < bw)
                                    di.Bw = bw;
                                break;
                            default:
                                di.Bw = bw;
                                break;
                        }
                        break;
                    case PlugTbl.Ship:
                        di.Bw = bw;
                        break;
                    case ICSMTbl.itblMobStation:
                        di.AddFreq(txFreqTxt.ToDoubleArray(';'));
                        di.AddFreq(rxFreqTxt.ToDoubleArray(';'));
                        di.Bw = bw;
                        break;
                    default:
                        {
                            double[] freqTx = txFreqTxt.ToDoubleArray(';');
                            double[] freqRx = rxFreqTxt.ToDoubleArray(';');
                            List<FreqBand> freq = new List<FreqBand>();
                            freq.AddRange(CreateFreqBand(bw, freqTx));
                            freq.AddRange(CreateFreqBand(bw, freqRx));
                            di.ListFreqBand.AddRange(freq);
                            di.ListFreqBand = MergeFreqBand(di.ListFreqBand.ToArray()).ToList();
                            di.Bw = CalculateBw(di.ListFreqBand.ToArray());
                        }
                        break;
                }
                return errorList.ToArray();
            }
            /// <summary>
            /// Сохранить в файл
            /// </summary>
            /// <param name="path">Путь к папке</param>
            /// <param name="province">область</param>
            /// <param name="repType"></param>
            public void Save(string path, string province, ReportType repType, DateTime dateStan, DateTime dateBeg, DateTime dateEnd )
            {


                /////////////////////////////////////////////////////////

                string[] ListSheet = { "м. Київ та Київська область", "Всі інші регіони України" };

                List<ListSheet> Lst_Report = new List<ListSheet>();
                ListSheet tmp_sheet1 = new ListSheet();
                tmp_sheet1.NameSheet = "м. Київ та Київська область";

                ListSheet tmp_sheet2 = new ListSheet();
                tmp_sheet2.NameSheet = "Всі інші регіони України";
                IMRecordset rsArea_Tmp = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                rsArea_Tmp.OrderBy("CODE", OrderDirection.Ascending);
                rsArea_Tmp.Select("NAME,CODE");
                try
                {
                    for (rsArea_Tmp.Open(); !rsArea_Tmp.IsEOF(); rsArea_Tmp.MoveNext())
                    {
                        if ((rsArea_Tmp.GetS("NAME") == "Київ") || (rsArea_Tmp.GetS("NAME") == "Київська"))
                        {
                            tmp_sheet1.LstArea.Add(rsArea_Tmp.GetS("CODE"), rsArea_Tmp.GetS("NAME"));
                        }

                        if ((rsArea_Tmp.GetS("NAME") != "Київ") && (rsArea_Tmp.GetS("NAME") != "Київська"))
                        {
                            tmp_sheet2.LstArea.Add(rsArea_Tmp.GetS("CODE"), rsArea_Tmp.GetS("NAME"));
                        }

                    }

                }
                finally
                {
                    rsArea_Tmp.Close();
                    rsArea_Tmp.Destroy();
                }


                Lst_Report.Add(tmp_sheet1);
                Lst_Report.Add(tmp_sheet2);
                /////////////////////////////////////////////////////////
                int MinNumSheet = 0;
                int MaxNumSheet = Lst_Report.Count-1;

                for (int ii = 0; ii < Lst_Report.Count(); ii++)
                {

                    int CurrSheet = ii;
                    if (_data.Count == 0)
                        return;
                    MergeData();
                    using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Запис в файл")))
                    {
                        pb.SetBig("Запис в файл...");


                        //string DefaultFontName = "Times New Roman";
                        double[] ListHeadColumnWidth = { 5.57, 36.29, 9, 26, 7.14, 9.57, 13, 9.86, 9.71, 6.71, 38.86, 17.43, 6.71, 13.86, 5.14, 8.71, 5.14 };
                        string[] ListHead = {"№ з/п",
                                            "Найменування користувача РЧР /ПІБ для користувача РЧР фізичної особи",
                                            "Код за ЄДРПОУ /реєстраційний номер облікової картки платника податків ",
                                            "Місцезнаходження користувача - юридичної особи/ фізичної особи ",
                                            "Перший дозвіл на експлуатацію РЕЗ та ВП на присвоєному номіналі радіочастоти",
                                            "Номер",
                                            "дата видачі дд.мм.рррр",
                                            "термін дії до    дд.мм.рррр",
                                            "адреса місця встановлення РЕЗ та ВП",
                                            "Вид             радіо-зв`язку",
                                            "Номер позиції статті 320 розділу XV Податкового кодексу України, якою встановлено ставки збору за користування РЧР",
                                            "Назва регіонів користу-вання РЧР",
                                            "Загальна ширина смуги радіочастот, зазначена в дозволі (МГц)",
                                            "Дата припинення користування РЧР 2  дд.мм.рррр",
                                            "Примітки"};



                        if (CurrSheet == MinNumSheet)
                        {
                            rep = Report.CreateReport(repType);
                            rep.Init(Path.Combine(path, string.Format("форма2{0}{1}", string.IsNullOrEmpty(province) ? "" : "-", province)), Lst_Report[ii].NameSheet,"");
                        }
                        else
                        {
                            rep.AddSheet(Lst_Report[ii].NameSheet);
                        }

                        List<string> line = new List<string>();


                        // Производим объединение ячеек M1:Q1
                        rep.UnionCell(9, 0, 13, 0, rep.GetCellStyleDefault(), "Додаток - ф.2");

                        // Производим объединение ячеек A2:Q2
                        rep.UnionCell(0, 1, 13, 1, rep.GetCellStyleBackgroundColor(), "Перелік користувачів радіочастотного ресурсу - платників збору за користування радіочастотним ресурсом України (РЧР), \t\n" +
                                  "які мають дозволи на експлуатацію радіоелектронних засобів (РЕЗ) та випромінювальних пристроїв (ВП), станом на " + dateStan.ToStringNullT("dd.MM.yyyy"));
                        //


                        rep.UnionCell(0, 2, 0, 3, rep.GetCellStyleBorder(), ListHead[0]); rep.SetCellStyle(0, 3, rep.GetCellStyleBorder());
                        rep.UnionCell(1, 2, 1, 3, rep.GetCellStyleBorder(), ListHead[1]);
                        rep.UnionCell(2, 2, 2, 3, rep.GetCellStyleBorder(), ListHead[2]);
                        rep.UnionCell(3, 2, 3, 3, rep.GetCellStyleBorder(), ListHead[3]);
                        rep.UnionCell(4, 2, 7, 2, rep.GetCellStyleBorder(), ListHead[4]); rep.UnionCell(4, 3, 4, 3, rep.GetCellStyleBorder(), ListHead[5]); rep.UnionCell(5, 3, 5, 3, rep.GetCellStyleBorder(), ListHead[6]); rep.UnionCell(6, 3, 6, 3, rep.GetCellStyleBorder(), ListHead[7]); rep.UnionCell(7, 3, 7, 3, rep.GetCellStyleBorder(), ListHead[8]);

                        rep.UnionCell(8, 2, 8, 3, rep.GetCellStyleBorder(), ListHead[9]);
                        rep.UnionCell(9, 2, 9, 3, rep.GetCellStyleBorder(), ListHead[10]);
                        rep.UnionCell(10, 2, 10, 3, rep.GetCellStyleBorder(), ListHead[11]);
                        rep.UnionCell(11, 2, 11, 3, rep.GetCellStyleBorder(), ListHead[12]);
                        rep.UnionCell(12, 2, 12, 3, rep.GetCellStyleBorder(), ListHead[13]);
                        rep.UnionCell(13, 2, 13, 3, rep.GetCellStyleBorder(), ListHead[14]);


                        for (int i = 0; i < ListHeadColumnWidth.Length; i++)
                        {
                            rep.SetColumnWidth(i, (int)ListHeadColumnWidth[i]);

                        }



                        int curr_num = rep.GetIndexRow() + 4;

                        rep.SetIndexRow(curr_num);


                        int counter = 1;

                        foreach (KeyValuePair<string, string> item_prov in Lst_Report[ii].LstArea)
                        {
                            List<DataItem> owner_name = new List<DataItem>();
                            bool status = false;

                            foreach (KeyValuePair<string, DataItem> item in _data)
                            {
                                DataItem di = item.Value;
                                if (di.Owner_Province.Equals(item_prov.Value.Trim())) { status = true; owner_name.Add(di); }
                            }

                            owner_name.Sort(delegate(DataItem us1, DataItem us2)
                            { return us1.OwnerName.CompareTo(us2.OwnerName); });


                            var out_list = from u in owner_name
                                           //orderby u.OwnerName, u.OrderCode, u.Province, u.Bw
                                           orderby u.OwnerName, u.OrderCode, u.Province
                                           select u;


                            if (status)
                            {
                                string temp_item_prov = "";


                                if ((item_prov.Value.ToString() != "Київ") && (item_prov.Value.ToString() != "Севастополь") && (item_prov.Value.ToString() != "АР Крим"))
                                {
                                    //temp_item_prov+=item_prov.Value.ToString() + " обл.";
                                    temp_item_prov += item_prov.Value.ToString() + " область";
                                }
                                else if (item_prov.Value.ToString() == "АР Крим")
                                {
                                    temp_item_prov += item_prov.Value.ToString();
                                }
                                else
                                {
                                    temp_item_prov += "м. " + item_prov.Value.ToString();
                                }


                                curr_num = rep.GetIndexRow();
                                rep.UnionCell(0, curr_num, 13, curr_num, rep.GetCellStyleBackgroundColor(), temp_item_prov);
                                curr_num = rep.GetIndexRow() + 1;
                                rep.SetIndexRow(curr_num);
                                status = false;
                            }



                            int cntRecord = 1;
                            foreach (DataItem di in out_list)
                            {


                                pb.SetSmall(cntRecord++, _data.Count);
                                line.Clear();

                                if (di.IsValid == false)
                                    continue;
                                //Подсчитываем кол-во областей
                                int countProvince = 1;
                                if (string.IsNullOrEmpty(di.Province) == false)
                                {
                                    if (di.Province.ToLower().Contains("україна"))
                                        countProvince = 27;
                                    else
                                        countProvince = di.Province.Split(';').Count();
                                }


                                line.Add(string.Format("{0}", counter++));
                                line.Add(di.OwnerName);
                                line.Add(di.OwnerRegNum);
                                line.Add(di.OwnerAddress);
                                line.Add(di.DozvNum);
                                line.Add(di.DozvDateFrom.ToStringNullT("dd.MM.yyyy"));
                                line.Add(di.DozvDateTo.ToStringNullT("dd.MM.yyyy"));
                                line.Add(string.IsNullOrEmpty(di.DozvAddress) ? "" : di.DozvAddress.Replace(';', '/'));
                                line.Add(di.OrderCode);
                                line.Add(di.OrderSymbol);
                                line.Add(string.IsNullOrEmpty(di.Province) ? "" : di.Province.Replace(';', '/'));
                                if (di.TableName == ICSMTbl.itblMobStation)
                                    line.Add((di.Bw * di.ListFreq.Count).Round(4).ToString());
                                else
                                    line.Add((di.Bw * countProvince).Round(4).ToString());
                                line.Add(di.DozvCancelDate.ToStringNullT("dd.MM.yyyy"));
                                line.Add("");
                                rep.WriteLine(line.ToArray());


                            }



                            if (pb.UserCanceled())
                                break;
                            //}

                            //rep.SetBorderStyle(1, 1, 17, rep.GetIndexRow()-1,1);

                            owner_name.Clear();
                            //out_list.Clear();
                            out_list = null;
                        }
                        if (!pb.UserCanceled())
                        {
                            if (CurrSheet == MaxNumSheet)
                            {
                            //    rep.Save();
                            }
                        }




                    }
                }
            }
            

            private void MergeData()
            {
                bool needRepeat = false;
                foreach (KeyValuePair<string, DataItem> dataTop in _data)
                {
                    DataItem itemTop = dataTop.Value;
                    switch (itemTop.TableName)
                    {
                        case ICSMTbl.itblMobStation:
                            if (itemTop.ListFreq.Count > 0)
                            {
                                foreach (KeyValuePair<string, DataItem> dataLow in _data)
                                {
                                    DataItem itemLow = dataLow.Value;
                                    if ((itemLow != itemTop) &&
                                        (itemLow.TableName == itemTop.TableName) &&
                                        (itemLow.ListFreq.Count > 0) && (itemLow.OwnerRegNum == itemTop.OwnerRegNum))
                                    {
                                        double[] freqs = itemLow.ListFreq.ToArray();
                                        if(itemTop.ContainFreq(freqs))
                                        {
                                            needRepeat = true;
                                            itemTop.AddFreq(freqs);
                                            if (itemTop.Bw < itemLow.Bw)
                                                itemTop.Bw = itemLow.Bw;
                                            itemLow.ListFreq.Clear();
                                            itemLow.IsValid = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        case PlugTbl.Ship:
                        case PlugTbl.XfaAbonentStation:
                            break;
                        default:
                            if (itemTop.ListFreqBand.Count > 0)
                            {
                                foreach (KeyValuePair<string, DataItem> dataLow in _data)
                                {
                                    DataItem itemLow = dataLow.Value;
                                    if ((itemLow != itemTop) &&
                                        (itemLow.TableName == itemTop.TableName) &&
                                        (itemLow.ListFreqBand.Count > 0) && (itemLow.OwnerRegNum == itemTop.OwnerRegNum))
                                    {
                                        List<FreqBand> freq = new List<FreqBand>();
                                        freq.AddRange(itemLow.ListFreqBand);
                                        freq.AddRange(itemTop.ListFreqBand);
                                        int count = freq.Count;
                                        FreqBand[] freqMerged = MergeFreqBand(freq.ToArray());
                                        if (count != freqMerged.Count())
                                        {
                                            itemTop.ListFreqBand.Clear();
                                            itemTop.ListFreqBand.AddRange(freqMerged);
                                            itemTop.Bw = CalculateBw(itemTop.ListFreqBand.ToArray());
                                            itemLow.ListFreqBand.Clear();
                                            itemLow.IsValid = false;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
                if (needRepeat)
                    MergeData();
            }
        }

        private DateTime _dateBeg;
        private DateTime _dateEnd;
        public DateTime _dateStan;
        private string _path;
        private string _tableName;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="date">Дата формирования</param>
        /// <param name="path">Путь к файлам</param>
        /// <param name="tableName">Имя таблицы</param>
        public TaxPayerForm2(TaxPayerReportParams _params, string tableName)
        {
            _dateBeg = _params.DateBeg;
            _dateEnd = _params.DateEnd;
            _dateStan = _params.DateStan;
            _path = _params.FilePath;
            _tableName = tableName;
        }
        /// <summary>
        /// Начать процесс генерации документов
        /// </summary>
        /// <returns>список ошибок</returns>
        public string[] Generate(ReportType repType, bool isOneFile)
        {
            List<string> errorList = new List<string>();
                //string[] additional_standard = { "РРЗ", "СВ", "УКХ", "АБТ", "БАУР", "РУЗО", "РБСС", "ПД", "РМО", "РМ", "ІР", "РП", "МР", "ПВП", "РОПС", "РРК", "РПАТЛ", "ШР", "ТРАНК", "ТЕТРА", "ТР", "ПЕЙДЖ", "РПС", "РЗП", "РСР", "МРл", "СР", "СптРЗ", "ТтТСМ", "СуР", "МсР", "МмР", "БНТ", "САІРС", "ЦР", "NMT", "ПТРМП", "БА", "Р", "РМО", "ІР", "МР", "ПВП", "РВ", "T-DAB", "СРН", "БС" };
                string additional = "[STANDARD] in ('РРЗ','СВ','УКХ','АБТ','БАУР','РУЗО','РБСС','ПД','РМО','РМ','ІР','РП','МР','ПВП','РОПС','РРК','РПАТЛ','ШР','ТРАНК','ТЕТРА','ТР','ПЕЙДЖ','РПС','РЗП','РСР','МРл','СР','СптРЗ','ТтТСМ','СуР','МсР','МмР','БНТ','САІРС','ЦР','NMT','ПТРМП','БА','Р','РМО','ІР','МР','ПВП','РВ','T-DAB','СРН','БС')";
                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Формування переліку користувачів РЧР (Форма 2)")))
                {
                    Dictionary<string, string> radioSystem = EriFiles.GetEriCodeAndDescr("Fees");
                    pb.SetProgress(0, 50000);
                    pb.SetBig(CLocaliz.TxT("Loading data..."));
                    pb.SetSmall(CLocaliz.TxT("Please wait"));
                    string lastProvince = "";
                    WriteData wd = new WriteData();
                    //LisRecordSet rs = null;
                    YXvTaxPayerForm2 rs = null;
                    try
                    {
                        rs = new YXvTaxPayerForm2();
                        rs.Format("*");
                        if (string.IsNullOrEmpty(_tableName) == false)
                            rs.Filter = string.Format("[DOC_DATE_FROM]<='{0}' AND [DOC_DATE_TO]>='{1}' AND [TABLE_NAME] LIKE '%{2}%'  AND {3}", _dateEnd.ToShortDateString(), _dateBeg.ToShortDateString(), _tableName, additional);
                        else rs.Filter = string.Format("[DOC_DATE_FROM]<='{0}' AND [DOC_DATE_TO]>='{1}' AND {2}", _dateEnd.ToShortDateString(), _dateBeg.ToShortDateString(), additional);
                        //rs.Order = "[OWNER_PROVINCE] ASC";
                        /*
                        rs = new LisRecordSet(PlugTbl.XvTaxPayerForm2, IMRecordset.Mode.ReadOnly);

                        rs.Select("ID");
                        rs.Select("OWNER_PROVINCE");
                        rs.Select("OWNER_NAME");
                        rs.Select("OWNER_CODE");
                        rs.Select("OWNER_ADDRESS");
                        rs.Select("DOC_NUM");
                        rs.Select("DOC_DATE_FROM");
                        rs.Select("DOC_DATE_TO");
                        rs.Select("ADDRESS");
                        rs.Select("FEES_ID");
                        rs.Select("Fees.ORDER_SYMBOL");
                        rs.Select("PROVINCE");
                        rs.Select("TX_FREQ_TXT");
                        rs.Select("RX_FREQ_TXT");
                        rs.Select("TABLE_NAME");
                        rs.Select("TABLE_ID");
                        rs.Select("STANDARD");
                        rs.Select("CLASS");
                        rs.Select("EXPL_TYPE");
                        rs.Select("BW");
                        rs.Select("DOZV_DATE_CANCEL");
                        rs.Select("OWNER_ID");
                         
                        rs.SetWhere("DOC_DATE_FROM", IMRecordset.Operation.Le, _dateEnd);
                        rs.SetWhere("DOC_DATE_TO", IMRecordset.Operation.Ge, _dateBeg);
                        if (string.IsNullOrEmpty(_tableName) == false)
                            rs.SetWhere("TABLE_NAME", IMRecordset.Operation.Like, _tableName);

                        rs.SetAdditional(additional);
                        rs.OrderBy("OWNER_PROVINCE", OrderDirection.Ascending);
                        */
                        bool st = false;

                        for (rs.OpenRs(); !rs.IsEOF(); rs.MoveNext())
                        {
                            //if (!additional_standard.Contains(rs.m_standard)) continue;

                            string m_order_symbol = "";
                            YXnrfaFees fees = new YXnrfaFees();
                            if (fees.Fetch(rs.m_fees_id)) {
                                m_order_symbol = fees.m_order_symbol;
                            }

                            if (!st)
                            {
                                st = true;
                            }

                            if (pb.UserCanceled())
                            {
                                errorList.Add("Перервано користувачем.");
                                break;
                            }
                            pb.Increment(true);
                            int id = rs.m_id;
                            int recId = rs.m_table_id;

                            DateTime dtPermEnd = rs.m_dozv_date_cancel;
                            if ((dtPermEnd != IM.NullT) && (dtPermEnd < _dateBeg))
                                continue;

                            string provinceOwner = rs.m_owner_province;
                            if (string.IsNullOrEmpty(provinceOwner))
                            {
                                //нет обоасти
                                errorList.Add(string.Format("Форма 2: Відсутня область власника: ID запису = {0}, ID власника = {1}", id, rs.m_owner_id));
                                provinceOwner = "(без регіону)";
                            }



                            if ((provinceOwner != lastProvince) && (isOneFile == false))
                            {
                                if (string.IsNullOrEmpty(lastProvince) == false)
                                {
                                    pb.SetBig("Запис в файл...");
                                    wd.Save(_path,  lastProvince, repType, _dateStan, _dateBeg, _dateEnd);
                                }
                                lastProvince = provinceOwner;
                                wd = new WriteData();
                                pb.SetBig(lastProvince);
                            }

                            string[] error = wd.Add(radioSystem, id, recId, rs.m_owner_name,
                                                    rs.m_owner_code, rs.m_owner_address, (rs.m_owner_province != null ? rs.m_owner_province : " "),
                                                    rs.m_doc_num, rs.m_doc_date_from,
                                                    rs.m_doc_date_to, rs.m_address,
                                                    m_order_symbol, rs.m_province,
                                                    rs.m_table_name, rs.m_standard,
                                                    rs.m_tx_freq_txt, rs.m_rx_freq_txt,
                                                    rs.m_bw, rs.m_dozv_date_cancel);
                            errorList.AddRange(error);
                        }
                        if (!pb.UserCanceled())
                        {
                            if (string.IsNullOrEmpty(lastProvince) == false)
                            {
                                wd.Save(_path, lastProvince, repType, _dateStan, _dateBeg, _dateEnd);
                                rep.Save();
                            }
                               
                            else if (isOneFile)
                            {
                                wd.Save(_path, "", repType, _dateStan, _dateBeg, _dateEnd);
                                rep.Save();
                            }
                               
                        }
                    }
                    finally
                    {
                        rs.Close();
                        rs.Dispose();
                        GC.SuppressFinalize(rs);
                    }
                }
            return errorList.ToArray();
        }
        /// <summary>
        /// Сливает полосы частот
        /// </summary>
        /// <param name="freqBand">Полосы частот</param>
        /// <returns>Слитые полосы частот</returns>
        private static FreqBand[] MergeFreqBand(params FreqBand[] freqBand)
        {
            bool isChanged = false;
            List<FreqBand> retVal = new List<FreqBand>();
            foreach (FreqBand band in freqBand)
            {
                bool isMerged = false;
                foreach (FreqBand freqVal in retVal)
                {
                    if ((freqVal.MinFreq <= band.MaxFreq) && (band.MaxFreq <= freqVal.MaxFreq) && (band.MinFreq <= freqVal.MinFreq))
                    {
                        freqVal.MinFreq = band.MinFreq;
                        isMerged = true;
                    }
                    if ((freqVal.MinFreq <= band.MinFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MaxFreq <= band.MaxFreq))
                    {
                        freqVal.MaxFreq = band.MaxFreq;
                        isMerged = true;
                    }
                    if ((freqVal.MinFreq <= band.MinFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MinFreq <= band.MaxFreq) && (band.MaxFreq <= freqVal.MaxFreq))
                    {
                        isMerged = true;
                    }
                    if ((band.MinFreq <= freqVal.MinFreq) && (freqVal.MinFreq <= band.MaxFreq) && (band.MinFreq <= freqVal.MaxFreq) && (freqVal.MaxFreq <= band.MaxFreq))
                    {
                        isMerged = true;
                        freqVal.MaxFreq = band.MaxFreq;
                        freqVal.MinFreq = band.MinFreq;
                    }
                    if (isMerged)
                        break;
                }
                if(isMerged == false)
                {
                    //Новая полоса частот
                    retVal.Add(new FreqBand(band.MinFreq, band.MaxFreq));
                }
                else
                {
                    isChanged = true;
                }
            }
            if (isChanged)
                return MergeFreqBand(retVal.ToArray()); //Пытаемся слить новые диапазоны
            return retVal.ToArray();
        }

        /// <summary>
        /// Сотзланеи полос частот
        /// </summary>
        /// <param name="bw">BW</param>
        /// <param name="freqs">список частот</param>
        /// <returns>полосы частот</returns>
        private static FreqBand[] CreateFreqBand(double bw, params double[] freqs)
        {
            List<FreqBand> retVal = new List<FreqBand>();
            bw = bw / 2.0;
            foreach (double freq in freqs)
                retVal.Add(new FreqBand(freq - bw, freq + bw));
            return retVal.ToArray();
        }
        /// <summary>
        /// Подсчитывает BW
        /// </summary>
        /// <param name="freqBand">список частот</param>
        /// <returns>полосы частот</returns>
        private static double CalculateBw(params FreqBand[] freqBand)
        {
            double bw = 0.0;
            foreach (FreqBand freq in freqBand)
                bw += (freq.MaxFreq - freq.MinFreq);
            return bw;
        }
    }
}
