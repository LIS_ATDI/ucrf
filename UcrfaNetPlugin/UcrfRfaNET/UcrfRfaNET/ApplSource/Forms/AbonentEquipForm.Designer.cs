﻿using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class AbonentEquipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbonentEquipForm));
            this.lbl = new System.Windows.Forms.Label();
            this.txtBoxPower = new System.Windows.Forms.TextBox();
            this.grpBoxCertificate = new System.Windows.Forms.GroupBox();
            this.txtBoxDate = new System.Windows.Forms.TextBox();
            this.lblCertDate = new System.Windows.Forms.Label();
            this.txtBoxCertNumb = new System.Windows.Forms.TextBox();
            this.lblCertNumb = new System.Windows.Forms.Label();
            this.txtBoxBw = new System.Windows.Forms.TextBox();
            this.lblBw = new System.Windows.Forms.Label();
            this.txtBoxDesEmi = new System.Windows.Forms.TextBox();
            this.lblDesEmi = new System.Windows.Forms.Label();
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtBoxCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtBoxManuf = new System.Windows.Forms.TextBox();
            this.lblManuf = new System.Windows.Forms.Label();
            this.txtBoxFamily = new System.Windows.Forms.TextBox();
            this.lblFamily = new System.Windows.Forms.Label();
            this.txtBoxTechNo = new System.Windows.Forms.TextBox();
            this.lblTechNo = new System.Windows.Forms.Label();
            this.txtBoxMaxFreqRx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxMaxFreqTx = new System.Windows.Forms.TextBox();
            this.lblMaxFreqTx = new System.Windows.Forms.Label();
            this.txtBoxMinFreqTx = new System.Windows.Forms.TextBox();
            this.lblMinFreqTx = new System.Windows.Forms.Label();
            this.txtBoxMinFreqRx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grpBoxTx = new System.Windows.Forms.GroupBox();
            this.grpBoxRx = new System.Windows.Forms.GroupBox();
            this.grpBoxAll = new System.Windows.Forms.GroupBox();
            this.btnDesEmi = new System.Windows.Forms.Button();
            this.grpBoxCertificate.SuspendLayout();
            this.grpBoxTx.SuspendLayout();
            this.grpBoxRx.SuspendLayout();
            this.grpBoxAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(18, 95);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(85, 13);
            this.lbl.TabIndex = 0;
            this.lbl.Text = "Потужність (Вт)";
            // 
            // txtBoxPower
            // 
            this.txtBoxPower.Location = new System.Drawing.Point(146, 92);
            this.txtBoxPower.Name = "txtBoxPower";
            this.txtBoxPower.Size = new System.Drawing.Size(130, 20);
            this.txtBoxPower.TabIndex = 40;
            // 
            // grpBoxCertificate
            // 
            this.grpBoxCertificate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grpBoxCertificate.Controls.Add(this.txtBoxDate);
            this.grpBoxCertificate.Controls.Add(this.lblCertDate);
            this.grpBoxCertificate.Controls.Add(this.txtBoxCertNumb);
            this.grpBoxCertificate.Controls.Add(this.lblCertNumb);
            this.grpBoxCertificate.Location = new System.Drawing.Point(313, 22);
            this.grpBoxCertificate.Name = "grpBoxCertificate";
            this.grpBoxCertificate.Size = new System.Drawing.Size(236, 71);
            this.grpBoxCertificate.TabIndex = 2;
            this.grpBoxCertificate.TabStop = false;
            this.grpBoxCertificate.Text = "Сертифікат";
            // 
            // txtBoxDate
            // 
            this.txtBoxDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBoxDate.Location = new System.Drawing.Point(115, 46);
            this.txtBoxDate.Name = "txtBoxDate";
            this.txtBoxDate.Size = new System.Drawing.Size(114, 20);
            this.txtBoxDate.TabIndex = 100;
            // 
            // lblCertDate
            // 
            this.lblCertDate.AutoSize = true;
            this.lblCertDate.Location = new System.Drawing.Point(23, 49);
            this.lblCertDate.Name = "lblCertDate";
            this.lblCertDate.Size = new System.Drawing.Size(33, 13);
            this.lblCertDate.TabIndex = 5;
            this.lblCertDate.Text = "Дата";
            // 
            // txtBoxCertNumb
            // 
            this.txtBoxCertNumb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBoxCertNumb.Location = new System.Drawing.Point(115, 18);
            this.txtBoxCertNumb.Name = "txtBoxCertNumb";
            this.txtBoxCertNumb.Size = new System.Drawing.Size(114, 20);
            this.txtBoxCertNumb.TabIndex = 90;
            // 
            // lblCertNumb
            // 
            this.lblCertNumb.AutoSize = true;
            this.lblCertNumb.Location = new System.Drawing.Point(23, 22);
            this.lblCertNumb.Name = "lblCertNumb";
            this.lblCertNumb.Size = new System.Drawing.Size(41, 13);
            this.lblCertNumb.TabIndex = 3;
            this.lblCertNumb.Text = "Номер";
            // 
            // txtBoxBw
            // 
            this.txtBoxBw.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxBw.Location = new System.Drawing.Point(146, 197);
            this.txtBoxBw.Name = "txtBoxBw";
            this.txtBoxBw.Size = new System.Drawing.Size(130, 20);
            this.txtBoxBw.TabIndex = 80;
            // 
            // lblBw
            // 
            this.lblBw.AutoSize = true;
            this.lblBw.Location = new System.Drawing.Point(18, 200);
            this.lblBw.Name = "lblBw";
            this.lblBw.Size = new System.Drawing.Size(103, 13);
            this.lblBw.TabIndex = 9;
            this.lblBw.Text = "Ширина смуги, кГц";
            // 
            // txtBoxDesEmi
            // 
            this.txtBoxDesEmi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxDesEmi.Location = new System.Drawing.Point(146, 170);
            this.txtBoxDesEmi.Name = "txtBoxDesEmi";
            this.txtBoxDesEmi.Size = new System.Drawing.Size(101, 20);
            this.txtBoxDesEmi.TabIndex = 70;
            // 
            // lblDesEmi
            // 
            this.lblDesEmi.AutoSize = true;
            this.lblDesEmi.Location = new System.Drawing.Point(18, 173);
            this.lblDesEmi.Name = "lblDesEmi";
            this.lblDesEmi.Size = new System.Drawing.Size(119, 13);
            this.lblDesEmi.TabIndex = 7;
            this.lblDesEmi.Text = "Клас випромінювання";
            // 
            // txtBoxName
            // 
            this.txtBoxName.Location = new System.Drawing.Point(146, 14);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(130, 20);
            this.txtBoxName.TabIndex = 10;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(18, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 13);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Назва";
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(323, 266);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "Прийняти";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(414, 266);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtBoxCode
            // 
            this.txtBoxCode.Location = new System.Drawing.Point(146, 40);
            this.txtBoxCode.Name = "txtBoxCode";
            this.txtBoxCode.Size = new System.Drawing.Size(130, 20);
            this.txtBoxCode.TabIndex = 20;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(18, 43);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(26, 13);
            this.lblCode.TabIndex = 15;
            this.lblCode.Text = "Код";
            // 
            // txtBoxManuf
            // 
            this.txtBoxManuf.Location = new System.Drawing.Point(146, 118);
            this.txtBoxManuf.Name = "txtBoxManuf";
            this.txtBoxManuf.Size = new System.Drawing.Size(130, 20);
            this.txtBoxManuf.TabIndex = 50;
            // 
            // lblManuf
            // 
            this.lblManuf.AutoSize = true;
            this.lblManuf.Location = new System.Drawing.Point(18, 121);
            this.lblManuf.Name = "lblManuf";
            this.lblManuf.Size = new System.Drawing.Size(56, 13);
            this.lblManuf.TabIndex = 17;
            this.lblManuf.Text = "Виробник";
            // 
            // txtBoxFamily
            // 
            this.txtBoxFamily.Location = new System.Drawing.Point(146, 66);
            this.txtBoxFamily.Name = "txtBoxFamily";
            this.txtBoxFamily.Size = new System.Drawing.Size(130, 20);
            this.txtBoxFamily.TabIndex = 30;
            // 
            // lblFamily
            // 
            this.lblFamily.AutoSize = true;
            this.lblFamily.Location = new System.Drawing.Point(18, 69);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(59, 13);
            this.lblFamily.TabIndex = 19;
            this.lblFamily.Text = "Сімейство";
            // 
            // txtBoxTechNo
            // 
            this.txtBoxTechNo.Location = new System.Drawing.Point(146, 144);
            this.txtBoxTechNo.Name = "txtBoxTechNo";
            this.txtBoxTechNo.Size = new System.Drawing.Size(130, 20);
            this.txtBoxTechNo.TabIndex = 60;
            // 
            // lblTechNo
            // 
            this.lblTechNo.AutoSize = true;
            this.lblTechNo.Location = new System.Drawing.Point(18, 147);
            this.lblTechNo.Name = "lblTechNo";
            this.lblTechNo.Size = new System.Drawing.Size(91, 13);
            this.lblTechNo.TabIndex = 21;
            this.lblTechNo.Text = "Технічний номер";
            // 
            // txtBoxMaxFreqRx
            // 
            this.txtBoxMaxFreqRx.Location = new System.Drawing.Point(115, 37);
            this.txtBoxMaxFreqRx.Name = "txtBoxMaxFreqRx";
            this.txtBoxMaxFreqRx.Size = new System.Drawing.Size(114, 20);
            this.txtBoxMaxFreqRx.TabIndex = 140;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Макс. частота, МГц";
            // 
            // txtBoxMaxFreqTx
            // 
            this.txtBoxMaxFreqTx.Location = new System.Drawing.Point(116, 44);
            this.txtBoxMaxFreqTx.Name = "txtBoxMaxFreqTx";
            this.txtBoxMaxFreqTx.Size = new System.Drawing.Size(114, 20);
            this.txtBoxMaxFreqTx.TabIndex = 120;
            // 
            // lblMaxFreqTx
            // 
            this.lblMaxFreqTx.AutoSize = true;
            this.lblMaxFreqTx.Location = new System.Drawing.Point(7, 47);
            this.lblMaxFreqTx.Name = "lblMaxFreqTx";
            this.lblMaxFreqTx.Size = new System.Drawing.Size(106, 13);
            this.lblMaxFreqTx.TabIndex = 27;
            this.lblMaxFreqTx.Text = "Макс. частота, МГц";
            // 
            // txtBoxMinFreqTx
            // 
            this.txtBoxMinFreqTx.Location = new System.Drawing.Point(116, 13);
            this.txtBoxMinFreqTx.Name = "txtBoxMinFreqTx";
            this.txtBoxMinFreqTx.Size = new System.Drawing.Size(114, 20);
            this.txtBoxMinFreqTx.TabIndex = 110;
            // 
            // lblMinFreqTx
            // 
            this.lblMinFreqTx.AutoSize = true;
            this.lblMinFreqTx.Location = new System.Drawing.Point(6, 16);
            this.lblMinFreqTx.Name = "lblMinFreqTx";
            this.lblMinFreqTx.Size = new System.Drawing.Size(96, 13);
            this.lblMinFreqTx.TabIndex = 25;
            this.lblMinFreqTx.Text = "Мін. частота, МГц";
            // 
            // txtBoxMinFreqRx
            // 
            this.txtBoxMinFreqRx.Location = new System.Drawing.Point(115, 11);
            this.txtBoxMinFreqRx.Name = "txtBoxMinFreqRx";
            this.txtBoxMinFreqRx.Size = new System.Drawing.Size(114, 20);
            this.txtBoxMinFreqRx.TabIndex = 130;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Мін. частота, МГц";
            // 
            // grpBoxTx
            // 
            this.grpBoxTx.Controls.Add(this.txtBoxMaxFreqTx);
            this.grpBoxTx.Controls.Add(this.lblMaxFreqTx);
            this.grpBoxTx.Controls.Add(this.txtBoxMinFreqTx);
            this.grpBoxTx.Controls.Add(this.lblMinFreqTx);
            this.grpBoxTx.Location = new System.Drawing.Point(314, 94);
            this.grpBoxTx.Name = "grpBoxTx";
            this.grpBoxTx.Size = new System.Drawing.Size(236, 81);
            this.grpBoxTx.TabIndex = 31;
            this.grpBoxTx.TabStop = false;
            this.grpBoxTx.Text = "Передавач";
            // 
            // grpBoxRx
            // 
            this.grpBoxRx.Controls.Add(this.txtBoxMaxFreqRx);
            this.grpBoxRx.Controls.Add(this.label1);
            this.grpBoxRx.Controls.Add(this.txtBoxMinFreqRx);
            this.grpBoxRx.Controls.Add(this.label4);
            this.grpBoxRx.Location = new System.Drawing.Point(314, 181);
            this.grpBoxRx.Name = "grpBoxRx";
            this.grpBoxRx.Size = new System.Drawing.Size(236, 70);
            this.grpBoxRx.TabIndex = 32;
            this.grpBoxRx.TabStop = false;
            this.grpBoxRx.Text = "Приймач";
            // 
            // grpBoxAll
            // 
            this.grpBoxAll.Controls.Add(this.btnDesEmi);
            this.grpBoxAll.Controls.Add(this.txtBoxTechNo);
            this.grpBoxAll.Controls.Add(this.lblTechNo);
            this.grpBoxAll.Controls.Add(this.txtBoxFamily);
            this.grpBoxAll.Controls.Add(this.lblFamily);
            this.grpBoxAll.Controls.Add(this.txtBoxManuf);
            this.grpBoxAll.Controls.Add(this.lblManuf);
            this.grpBoxAll.Controls.Add(this.txtBoxCode);
            this.grpBoxAll.Controls.Add(this.lblCode);
            this.grpBoxAll.Controls.Add(this.txtBoxName);
            this.grpBoxAll.Controls.Add(this.lblName);
            this.grpBoxAll.Controls.Add(this.txtBoxBw);
            this.grpBoxAll.Controls.Add(this.lblBw);
            this.grpBoxAll.Controls.Add(this.txtBoxDesEmi);
            this.grpBoxAll.Controls.Add(this.lblDesEmi);
            this.grpBoxAll.Controls.Add(this.txtBoxPower);
            this.grpBoxAll.Controls.Add(this.lbl);
            this.grpBoxAll.Location = new System.Drawing.Point(12, 17);
            this.grpBoxAll.Name = "grpBoxAll";
            this.grpBoxAll.Size = new System.Drawing.Size(296, 234);
            this.grpBoxAll.TabIndex = 33;
            this.grpBoxAll.TabStop = false;
            this.grpBoxAll.Text = "Обладнання";
            // 
            // btnDesEmi
            // 
            this.btnDesEmi.Location = new System.Drawing.Point(253, 170);
            this.btnDesEmi.Name = "btnDesEmi";
            this.btnDesEmi.Size = new System.Drawing.Size(23, 20);
            this.btnDesEmi.TabIndex = 71;
            this.btnDesEmi.Text = "...";
            this.btnDesEmi.UseVisualStyleBackColor = true;
            this.btnDesEmi.Click += new System.EventHandler(this.btnDesEmi_Click);
            // 
            // AbonentEquipForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(553, 301);
            this.Controls.Add(this.grpBoxAll);
            this.Controls.Add(this.grpBoxRx);
            this.Controls.Add(this.grpBoxTx);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpBoxCertificate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(563, 330);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(563, 330);
            this.Name = "AbonentEquipForm";
            this.Text = "AbonentEquipForm";
            this.grpBoxCertificate.ResumeLayout(false);
            this.grpBoxCertificate.PerformLayout();
            this.grpBoxTx.ResumeLayout(false);
            this.grpBoxTx.PerformLayout();
            this.grpBoxRx.ResumeLayout(false);
            this.grpBoxRx.PerformLayout();
            this.grpBoxAll.ResumeLayout(false);
            this.grpBoxAll.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtBoxPower;
        private System.Windows.Forms.GroupBox grpBoxCertificate;
        private System.Windows.Forms.Label lblCertDate;
        private System.Windows.Forms.TextBox txtBoxCertNumb;
        private System.Windows.Forms.Label lblCertNumb;
        private System.Windows.Forms.TextBox txtBoxBw;
        private System.Windows.Forms.Label lblBw;
        private System.Windows.Forms.TextBox txtBoxDesEmi;
        private System.Windows.Forms.Label lblDesEmi;
        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtBoxCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtBoxManuf;
        private System.Windows.Forms.Label lblManuf;
        private System.Windows.Forms.TextBox txtBoxFamily;
        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.TextBox txtBoxTechNo;
        private System.Windows.Forms.Label lblTechNo;
        private System.Windows.Forms.TextBox txtBoxMaxFreqRx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxMaxFreqTx;
        private System.Windows.Forms.Label lblMaxFreqTx;
        private System.Windows.Forms.TextBox txtBoxMinFreqTx;
        private System.Windows.Forms.Label lblMinFreqTx;
        private System.Windows.Forms.TextBox txtBoxMinFreqRx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grpBoxTx;
        private System.Windows.Forms.GroupBox grpBoxRx;
        private System.Windows.Forms.GroupBox grpBoxAll;
        private System.Windows.Forms.TextBox txtBoxDate;
        private System.Windows.Forms.Button btnDesEmi;
    }
}