﻿using System;
namespace XICSM.UcrfRfaNET.ApplSource
{
   partial class MainAppForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.MainMenu = new System.Windows.Forms.MenuStrip();
          this.MenuURCHP = new System.Windows.Forms.ToolStripMenuItem();
          this.URCPDuplicateStation = new System.Windows.Forms.ToolStripMenuItem();
          this.AddSectorMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.RemoveSectorMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.URCPEmsCalc = new System.Windows.Forms.ToolStripMenuItem();
          this.URCPSave = new System.Windows.Forms.ToolStripMenuItem();
          this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.UCRFEMSConclusion = new System.Windows.Forms.ToolStripMenuItem();
          this.eMSPermissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripCoord = new System.Windows.Forms.ToolStripMenuItem();
          this.MenuAdd2Net = new System.Windows.Forms.ToolStripMenuItem();
          this.AddSectorSps = new System.Windows.Forms.ToolStripMenuItem();
          this.AddSectorUps = new System.Windows.Forms.ToolStripMenuItem();
          this.printOnBlank = new System.Windows.Forms.ToolStripMenuItem();
          this.UrcpSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.UrcpFindFrequencies = new System.Windows.Forms.ToolStripMenuItem();
          this.MenuURZP = new System.Windows.Forms.ToolStripMenuItem();
          this.CheckoutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.DiffReportGenerationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.URCP_AttachDoc = new System.Windows.Forms.ToolStripMenuItem();
          this.menuUrzpPtk2 = new System.Windows.Forms.ToolStripMenuItem();
          this.menuUrzpPtk3 = new System.Windows.Forms.ToolStripMenuItem();
          this.MenuURCHM = new System.Windows.Forms.ToolStripMenuItem();
          this.miArticle = new System.Windows.Forms.ToolStripMenuItem();
          this.ChangeFilialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.MenuDVR = new System.Windows.Forms.ToolStripMenuItem();
          this.MenuEXIT = new System.Windows.Forms.ToolStripMenuItem();
          this.GB_OWNER = new System.Windows.Forms.GroupBox();
          this.txtOwnerName = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.txtOwnerRDPO = new System.Windows.Forms.TextBox();
          this.label1 = new System.Windows.Forms.Label();
          this.gbParamGrid = new System.Windows.Forms.GroupBox();
          this.GroupBox2 = new System.Windows.Forms.GroupBox();
          this.dataGridEventLog = new System.Windows.Forms.DataGridView();
          this.GroupBox3 = new System.Windows.Forms.GroupBox();
          this.lblTechUser = new System.Windows.Forms.Label();
          this.dataGridLicence = new System.Windows.Forms.DataGridView();
          this.GroupBox4 = new System.Windows.Forms.GroupBox();
          this.dgLog = new System.Windows.Forms.DataGridView();
          this.tabControl1 = new System.Windows.Forms.TabControl();
          this.tabPage1 = new System.Windows.Forms.TabPage();
          this.textBoxDozvil = new System.Windows.Forms.TextBox();
          this.tabPage2 = new System.Windows.Forms.TabPage();
          this.textBoxVisnovok = new System.Windows.Forms.TextBox();
          this.tabPage3 = new System.Windows.Forms.TabPage();
          this.textBoxNote = new System.Windows.Forms.TextBox();
          this.tabPage4 = new System.Windows.Forms.TabPage();
          this.textBoxComments = new System.Windows.Forms.TextBox();
          this.label3 = new System.Windows.Forms.Label();
          this.tbStatus = new System.Windows.Forms.TextBox();
          this.splitContainerParams = new System.Windows.Forms.SplitContainer();
          this.statusStrip = new System.Windows.Forms.StatusStrip();
          this.tsEmpty = new System.Windows.Forms.ToolStripStatusLabel();
          this.tsArticle = new System.Windows.Forms.ToolStripStatusLabel();
          this.tsWorkCount = new System.Windows.Forms.ToolStripStatusLabel();
          this.tsNet = new System.Windows.Forms.ToolStripStatusLabel();
          this.splitContainerAll = new System.Windows.Forms.SplitContainer();
          this.splitContainerAddParams = new System.Windows.Forms.SplitContainer();
          this.splitContainerAddParams2 = new System.Windows.Forms.SplitContainer();
          this.MainMenu.SuspendLayout();
          this.GB_OWNER.SuspendLayout();
          this.GroupBox2.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridEventLog)).BeginInit();
          this.GroupBox3.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridLicence)).BeginInit();
          this.GroupBox4.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
          this.tabControl1.SuspendLayout();
          this.tabPage1.SuspendLayout();
          this.tabPage2.SuspendLayout();
          this.tabPage3.SuspendLayout();
          this.tabPage4.SuspendLayout();
          this.splitContainerParams.Panel1.SuspendLayout();
          this.splitContainerParams.Panel2.SuspendLayout();
          this.splitContainerParams.SuspendLayout();
          this.statusStrip.SuspendLayout();
          this.splitContainerAll.Panel1.SuspendLayout();
          this.splitContainerAll.Panel2.SuspendLayout();
          this.splitContainerAll.SuspendLayout();
          this.splitContainerAddParams.Panel1.SuspendLayout();
          this.splitContainerAddParams.Panel2.SuspendLayout();
          this.splitContainerAddParams.SuspendLayout();
          this.splitContainerAddParams2.Panel1.SuspendLayout();
          this.splitContainerAddParams2.Panel2.SuspendLayout();
          this.splitContainerAddParams2.SuspendLayout();
          this.SuspendLayout();
          // 
          // MainMenu
          // 
          this.MainMenu.BackColor = System.Drawing.SystemColors.Control;
          this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuURCHP,
            this.MenuURZP,
            this.MenuURCHM,
            this.MenuDVR,
            this.MenuEXIT});
          this.MainMenu.Location = new System.Drawing.Point(0, 0);
          this.MainMenu.Name = "MainMenu";
          this.MainMenu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
          this.MainMenu.Size = new System.Drawing.Size(1028, 24);
          this.MainMenu.TabIndex = 6;
          this.MainMenu.Text = "menuStrip1";
          this.MainMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MainMenu_ItemClicked);
          // 
          // MenuURCHP
          // 
          this.MenuURCHP.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.URCPDuplicateStation,
            this.AddSectorMenuItem,
            this.RemoveSectorMenuItem,
            this.URCPEmsCalc,
            this.URCPSave,
            this.printToolStripMenuItem,
            this.UCRFEMSConclusion,
            this.eMSPermissionToolStripMenuItem,
            this.toolStripCoord,
            this.MenuAdd2Net,
            this.AddSectorSps,
            this.AddSectorUps,
            this.printOnBlank,
            this.UrcpSeparator1,
            this.UrcpFindFrequencies});
          this.MenuURCHP.Name = "MenuURCHP";
          this.MenuURCHP.Size = new System.Drawing.Size(50, 20);
          this.MenuURCHP.Text = "УРЧП";
          // 
          // URCPDuplicateStation
          // 
          this.URCPDuplicateStation.Name = "URCPDuplicateStation";
          this.URCPDuplicateStation.ShortcutKeys = System.Windows.Forms.Keys.F4;
          this.URCPDuplicateStation.Size = new System.Drawing.Size(290, 22);
          this.URCPDuplicateStation.Text = "Дублювати РЧП";
          this.URCPDuplicateStation.Click += new System.EventHandler(this.URCPDuplicateStation_Click);
          // 
          // AddSectorMenuItem
          // 
          this.AddSectorMenuItem.Name = "AddSectorMenuItem";
          this.AddSectorMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
          this.AddSectorMenuItem.Size = new System.Drawing.Size(290, 22);
          this.AddSectorMenuItem.Text = "Додати сектор";
          this.AddSectorMenuItem.Click += new System.EventHandler(this.AddSectorMenuItem_Click);
          // 
          // RemoveSectorMenuItem
          // 
          this.RemoveSectorMenuItem.Name = "RemoveSectorMenuItem";
          this.RemoveSectorMenuItem.Size = new System.Drawing.Size(290, 22);
          this.RemoveSectorMenuItem.Text = "Видалити сектор";
          this.RemoveSectorMenuItem.Click += new System.EventHandler(this.RemoveSectorMenuItem_Click);
          // 
          // URCPEmsCalc
          // 
          this.URCPEmsCalc.Name = "URCPEmsCalc";
          this.URCPEmsCalc.ShortcutKeys = System.Windows.Forms.Keys.F9;
          this.URCPEmsCalc.Size = new System.Drawing.Size(290, 22);
          this.URCPEmsCalc.Text = "Попередній розрахунок ЕМС";
          this.URCPEmsCalc.Click += new System.EventHandler(this.URCPEmsCalc_Click);
          // 
          // URCPSave
          // 
          this.URCPSave.Name = "URCPSave";
          this.URCPSave.ShortcutKeys = System.Windows.Forms.Keys.F5;
          this.URCPSave.Size = new System.Drawing.Size(290, 22);
          this.URCPSave.Text = "Зберегти РЧП";
          this.URCPSave.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
          // 
          // printToolStripMenuItem
          // 
          this.printToolStripMenuItem.Name = "printToolStripMenuItem";
          this.printToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
          this.printToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
          this.printToolStripMenuItem.Text = "Друк";
          this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
          // 
          // UCRFEMSConclusion
          // 
          this.UCRFEMSConclusion.Name = "UCRFEMSConclusion";
          this.UCRFEMSConclusion.Size = new System.Drawing.Size(290, 22);
          this.UCRFEMSConclusion.Text = "Особливі умови Висновку щодо ЕМС";
          this.UCRFEMSConclusion.Click += new System.EventHandler(this.UCRFEMSConclusion_Click);
          // 
          // eMSPermissionToolStripMenuItem
          // 
          this.eMSPermissionToolStripMenuItem.Name = "eMSPermissionToolStripMenuItem";
          this.eMSPermissionToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
          this.eMSPermissionToolStripMenuItem.Text = "Особливі умови Дозволу на експлуатацію";
          this.eMSPermissionToolStripMenuItem.Click += new System.EventHandler(this.eMSPermissionToolStripMenuItem_Click);
          // 
          // toolStripCoord
          // 
          this.toolStripCoord.Name = "toolStripCoord";
          this.toolStripCoord.ShortcutKeys = System.Windows.Forms.Keys.F12;
          this.toolStripCoord.Size = new System.Drawing.Size(290, 22);
          this.toolStripCoord.Text = "Координація";
          this.toolStripCoord.Click += new System.EventHandler(this.toolStripCoord_Click);
          // 
          // MenuAdd2Net
          // 
          this.MenuAdd2Net.Name = "MenuAdd2Net";
          this.MenuAdd2Net.Size = new System.Drawing.Size(290, 22);
          this.MenuAdd2Net.Text = "Приєднати до мережі...";
          this.MenuAdd2Net.Click += new System.EventHandler(this.MenuAdd2Net_Click);
          // 
          // AddSectorSps
          // 
          this.AddSectorSps.Enabled = false;
          this.AddSectorSps.Name = "AddSectorSps";
          this.AddSectorSps.Size = new System.Drawing.Size(290, 22);
          this.AddSectorSps.Text = "Додати СПС";
          this.AddSectorSps.Visible = false;
          this.AddSectorSps.Click += new System.EventHandler(this.AddSectorSps_Click);
          // 
          // AddSectorUps
          // 
          this.AddSectorUps.Enabled = false;
          this.AddSectorUps.Name = "AddSectorUps";
          this.AddSectorUps.Size = new System.Drawing.Size(290, 22);
          this.AddSectorUps.Text = "Додати УПС";
          this.AddSectorUps.Visible = false;
          this.AddSectorUps.Click += new System.EventHandler(this.AddSectorUps_Click);
          // 
          // printOnBlank
          // 
          this.printOnBlank.Enabled = false;
          this.printOnBlank.Name = "printOnBlank";
          this.printOnBlank.Size = new System.Drawing.Size(290, 22);
          this.printOnBlank.Text = "Друк на бланку...";
          this.printOnBlank.Visible = false;
          this.printOnBlank.Click += new System.EventHandler(this.printOnBlankToolStripMenuItem_Click);
          // 
          // UrcpSeparator1
          // 
          this.UrcpSeparator1.Name = "UrcpSeparator1";
          this.UrcpSeparator1.Size = new System.Drawing.Size(287, 6);
          // 
          // UrcpFindFrequencies
          // 
          this.UrcpFindFrequencies.Name = "UrcpFindFrequencies";
          this.UrcpFindFrequencies.Size = new System.Drawing.Size(290, 22);
          this.UrcpFindFrequencies.Text = "Підбір частот...";
          this.UrcpFindFrequencies.Click += new System.EventHandler(this.UrcpFindFrequencies_Click);
          // 
          // MenuURZP
          // 
          this.MenuURZP.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckoutMenuItem,
            this.DiffReportGenerationMenuItem,
            this.URCP_AttachDoc,
            this.menuUrzpPtk2,
            this.menuUrzpPtk3});
          this.MenuURZP.Name = "MenuURZP";
          this.MenuURZP.Size = new System.Drawing.Size(49, 20);
          this.MenuURZP.Text = "УРЗП";
          // 
          // CheckoutMenuItem
          // 
          this.CheckoutMenuItem.Name = "CheckoutMenuItem";
          this.CheckoutMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
          this.CheckoutMenuItem.Size = new System.Drawing.Size(329, 22);
          this.CheckoutMenuItem.Text = "Зберегти";
          this.CheckoutMenuItem.Click += new System.EventHandler(this.CheckoutMenuItem_Click);
          // 
          // DiffReportGenerationMenuItem
          // 
          this.DiffReportGenerationMenuItem.Name = "DiffReportGenerationMenuItem";
          this.DiffReportGenerationMenuItem.Size = new System.Drawing.Size(329, 22);
          this.DiffReportGenerationMenuItem.Text = "Генерація акту ПТК...";
          this.DiffReportGenerationMenuItem.Click += new System.EventHandler(this.DiffReportGenerationMenuItem_Click);
          // 
          // URCP_AttachDoc
          // 
          this.URCP_AttachDoc.Name = "URCP_AttachDoc";
          this.URCP_AttachDoc.Size = new System.Drawing.Size(329, 22);
          this.URCP_AttachDoc.Text = "Приєднати документ";
          this.URCP_AttachDoc.Click += new System.EventHandler(this.URCP_AttachDoc_Click);
          // 
          // menuUrzpPtk2
          // 
          this.menuUrzpPtk2.Name = "menuUrzpPtk2";
          this.menuUrzpPtk2.Size = new System.Drawing.Size(329, 22);
          this.menuUrzpPtk2.Text = "Генерація протоколу інструментальної оцінки";
          this.menuUrzpPtk2.Click += new System.EventHandler(this.menuUrzpPtk2_Click);
          // 
          // menuUrzpPtk3
          // 
          this.menuUrzpPtk3.Name = "menuUrzpPtk3";
          this.menuUrzpPtk3.Size = new System.Drawing.Size(329, 22);
          this.menuUrzpPtk3.Text = "Генерація протоколу вимірювань параметрів РЕЗ";
          this.menuUrzpPtk3.Click += new System.EventHandler(this.menuUrzpPtk3_Click);
          // 
          // MenuURCHM
          // 
          this.MenuURCHM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miArticle,
            this.ChangeFilialToolStripMenuItem});
          this.MenuURCHM.Name = "MenuURCHM";
          this.MenuURCHM.Size = new System.Drawing.Size(51, 20);
          this.MenuURCHM.Text = "УРЧМ";
          // 
          // miArticle
          // 
          this.miArticle.Name = "miArticle";
          this.miArticle.Size = new System.Drawing.Size(157, 22);
          this.miArticle.Text = "Вибрати статтю";
          this.miArticle.Click += new System.EventHandler(this.miArticle_Click);
          // 
          // ChangeFilialToolStripMenuItem
          // 
          this.ChangeFilialToolStripMenuItem.Name = "ChangeFilialToolStripMenuItem";
          this.ChangeFilialToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
          this.ChangeFilialToolStripMenuItem.Text = "Зміна філії";
          this.ChangeFilialToolStripMenuItem.Click += new System.EventHandler(this.ChangeFilialToolStripMenuItem_Click);
          // 
          // MenuDVR
          // 
          this.MenuDVR.Name = "MenuDVR";
          this.MenuDVR.Size = new System.Drawing.Size(42, 20);
          this.MenuDVR.Text = "ДРВ";
          // 
          // MenuEXIT
          // 
          this.MenuEXIT.Name = "MenuEXIT";
          this.MenuEXIT.Size = new System.Drawing.Size(45, 20);
          this.MenuEXIT.Text = "Вихід";
          this.MenuEXIT.Click += new System.EventHandler(this.MenuEXIT_Click);
          // 
          // GB_OWNER
          // 
          this.GB_OWNER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.GB_OWNER.Controls.Add(this.txtOwnerName);
          this.GB_OWNER.Controls.Add(this.label2);
          this.GB_OWNER.Controls.Add(this.txtOwnerRDPO);
          this.GB_OWNER.Controls.Add(this.label1);
          this.GB_OWNER.Location = new System.Drawing.Point(9, 25);
          this.GB_OWNER.Margin = new System.Windows.Forms.Padding(2);
          this.GB_OWNER.Name = "GB_OWNER";
          this.GB_OWNER.Padding = new System.Windows.Forms.Padding(2);
          this.GB_OWNER.Size = new System.Drawing.Size(886, 49);
          this.GB_OWNER.TabIndex = 0;
          this.GB_OWNER.TabStop = false;
          this.GB_OWNER.Text = "Користувач радіочастотним ресурсом України";
          // 
          // txtOwnerName
          // 
          this.txtOwnerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.txtOwnerName.Location = new System.Drawing.Point(179, 21);
          this.txtOwnerName.Margin = new System.Windows.Forms.Padding(2);
          this.txtOwnerName.Name = "txtOwnerName";
          this.txtOwnerName.ReadOnly = true;
          this.txtOwnerName.Size = new System.Drawing.Size(703, 20);
          this.txtOwnerName.TabIndex = 3;
          this.txtOwnerName.DoubleClick += new System.EventHandler(this.txtOwnerName_DoubleClick);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(139, 24);
          this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(39, 13);
          this.label2.TabIndex = 2;
          this.label2.Text = "Назва";
          // 
          // txtOwnerRDPO
          // 
          this.txtOwnerRDPO.Location = new System.Drawing.Point(59, 21);
          this.txtOwnerRDPO.Margin = new System.Windows.Forms.Padding(2);
          this.txtOwnerRDPO.Name = "txtOwnerRDPO";
          this.txtOwnerRDPO.ReadOnly = true;
          this.txtOwnerRDPO.Size = new System.Drawing.Size(76, 20);
          this.txtOwnerRDPO.TabIndex = 1;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(4, 24);
          this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(54, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "ЄДРПОУ";
          // 
          // gbParamGrid
          // 
          this.gbParamGrid.Dock = System.Windows.Forms.DockStyle.Fill;
          this.gbParamGrid.Enabled = false;
          this.gbParamGrid.Location = new System.Drawing.Point(0, 0);
          this.gbParamGrid.Margin = new System.Windows.Forms.Padding(2);
          this.gbParamGrid.Name = "gbParamGrid";
          this.gbParamGrid.Padding = new System.Windows.Forms.Padding(2);
          this.gbParamGrid.Size = new System.Drawing.Size(800, 602);
          this.gbParamGrid.TabIndex = 0;
          this.gbParamGrid.TabStop = false;
          this.gbParamGrid.Text = "Technical characteristics";
          this.gbParamGrid.Resize += new System.EventHandler(this.gbParametersGrid_Resize);
          // 
          // GroupBox2
          // 
          this.GroupBox2.Controls.Add(this.dataGridEventLog);
          this.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.GroupBox2.Location = new System.Drawing.Point(0, 0);
          this.GroupBox2.Margin = new System.Windows.Forms.Padding(2);
          this.GroupBox2.Name = "GroupBox2";
          this.GroupBox2.Padding = new System.Windows.Forms.Padding(2);
          this.GroupBox2.Size = new System.Drawing.Size(203, 602);
          this.GroupBox2.TabIndex = 0;
          this.GroupBox2.TabStop = false;
          this.GroupBox2.Text = "Журнал подій";
          // 
          // dataGridEventLog
          // 
          this.dataGridEventLog.AllowUserToAddRows = false;
          this.dataGridEventLog.AllowUserToDeleteRows = false;
          this.dataGridEventLog.AllowUserToResizeRows = false;
          this.dataGridEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.dataGridEventLog.BackgroundColor = System.Drawing.Color.White;
          this.dataGridEventLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridEventLog.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
          this.dataGridEventLog.Location = new System.Drawing.Point(5, 18);
          this.dataGridEventLog.MultiSelect = false;
          this.dataGridEventLog.Name = "dataGridEventLog";
          this.dataGridEventLog.ReadOnly = true;
          this.dataGridEventLog.RowHeadersWidth = 4;
          this.dataGridEventLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridEventLog.Size = new System.Drawing.Size(193, 579);
          this.dataGridEventLog.TabIndex = 0;
          this.dataGridEventLog.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridEventLog_CellMouseDoubleClick);
          // 
          // GroupBox3
          // 
          this.GroupBox3.Controls.Add(this.lblTechUser);
          this.GroupBox3.Controls.Add(this.dataGridLicence);
          this.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
          this.GroupBox3.Location = new System.Drawing.Point(0, 0);
          this.GroupBox3.Margin = new System.Windows.Forms.Padding(2);
          this.GroupBox3.Name = "GroupBox3";
          this.GroupBox3.Padding = new System.Windows.Forms.Padding(2);
          this.GroupBox3.Size = new System.Drawing.Size(188, 111);
          this.GroupBox3.TabIndex = 3;
          this.GroupBox3.TabStop = false;
          this.GroupBox3.Text = "Дійсні БС";
          // 
          // lblTechUser
          // 
          this.lblTechUser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.lblTechUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.lblTechUser.ForeColor = System.Drawing.Color.Red;
          this.lblTechUser.Location = new System.Drawing.Point(21, 39);
          this.lblTechUser.Name = "lblTechUser";
          this.lblTechUser.Size = new System.Drawing.Size(148, 48);
          this.lblTechUser.TabIndex = 1;
          this.lblTechUser.Text = "Технологічний користувач";
          this.lblTechUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          this.lblTechUser.DoubleClick += new System.EventHandler(this.lblTechUser_DoubleClick);
          // 
          // dataGridLicence
          // 
          this.dataGridLicence.AllowUserToAddRows = false;
          this.dataGridLicence.AllowUserToDeleteRows = false;
          this.dataGridLicence.AllowUserToResizeRows = false;
          this.dataGridLicence.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.dataGridLicence.BackgroundColor = System.Drawing.Color.White;
          this.dataGridLicence.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridLicence.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
          this.dataGridLicence.Location = new System.Drawing.Point(5, 18);
          this.dataGridLicence.MultiSelect = false;
          this.dataGridLicence.Name = "dataGridLicence";
          this.dataGridLicence.ReadOnly = true;
          this.dataGridLicence.RowHeadersWidth = 4;
          this.dataGridLicence.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridLicence.Size = new System.Drawing.Size(178, 88);
          this.dataGridLicence.TabIndex = 0;
          this.dataGridLicence.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridLicence_CellMouseDoubleClick);
          // 
          // GroupBox4
          // 
          this.GroupBox4.Controls.Add(this.dgLog);
          this.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
          this.GroupBox4.Location = new System.Drawing.Point(0, 0);
          this.GroupBox4.Margin = new System.Windows.Forms.Padding(2);
          this.GroupBox4.Name = "GroupBox4";
          this.GroupBox4.Padding = new System.Windows.Forms.Padding(2);
          this.GroupBox4.Size = new System.Drawing.Size(202, 111);
          this.GroupBox4.TabIndex = 5;
          this.GroupBox4.TabStop = false;
          this.GroupBox4.Text = "Журнал змін";
          // 
          // dgLog
          // 
          this.dgLog.AllowUserToAddRows = false;
          this.dgLog.AllowUserToDeleteRows = false;
          this.dgLog.AllowUserToResizeRows = false;
          this.dgLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.dgLog.BackgroundColor = System.Drawing.Color.White;
          this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgLog.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
          this.dgLog.Location = new System.Drawing.Point(5, 18);
          this.dgLog.MultiSelect = false;
          this.dgLog.Name = "dgLog";
          this.dgLog.ReadOnly = true;
          this.dgLog.RowHeadersWidth = 4;
          this.dgLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dgLog.Size = new System.Drawing.Size(192, 88);
          this.dgLog.TabIndex = 0;
          // 
          // tabControl1
          // 
          this.tabControl1.Controls.Add(this.tabPage1);
          this.tabControl1.Controls.Add(this.tabPage2);
          this.tabControl1.Controls.Add(this.tabPage3);
          this.tabControl1.Controls.Add(this.tabPage4);
          this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.tabControl1.Location = new System.Drawing.Point(0, 0);
          this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
          this.tabControl1.Name = "tabControl1";
          this.tabControl1.SelectedIndex = 0;
          this.tabControl1.Size = new System.Drawing.Size(609, 111);
          this.tabControl1.TabIndex = 4;
          // 
          // tabPage1
          // 
          this.tabPage1.Controls.Add(this.textBoxDozvil);
          this.tabPage1.Location = new System.Drawing.Point(4, 22);
          this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
          this.tabPage1.Name = "tabPage1";
          this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
          this.tabPage1.Size = new System.Drawing.Size(601, 85);
          this.tabPage1.TabIndex = 0;
          this.tabPage1.Text = "Дозвіл";
          this.tabPage1.UseVisualStyleBackColor = true;
          // 
          // textBoxDozvil
          // 
          this.textBoxDozvil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.textBoxDozvil.Location = new System.Drawing.Point(4, 4);
          this.textBoxDozvil.Margin = new System.Windows.Forms.Padding(2);
          this.textBoxDozvil.Multiline = true;
          this.textBoxDozvil.Name = "textBoxDozvil";
          this.textBoxDozvil.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.textBoxDozvil.Size = new System.Drawing.Size(593, 77);
          this.textBoxDozvil.TabIndex = 0;
          this.textBoxDozvil.TextChanged += new System.EventHandler(this.textBoxDozvil_TextChanged);
          // 
          // tabPage2
          // 
          this.tabPage2.Controls.Add(this.textBoxVisnovok);
          this.tabPage2.Location = new System.Drawing.Point(4, 22);
          this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
          this.tabPage2.Name = "tabPage2";
          this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
          this.tabPage2.Size = new System.Drawing.Size(601, 85);
          this.tabPage2.TabIndex = 1;
          this.tabPage2.Text = "Висновок";
          this.tabPage2.UseVisualStyleBackColor = true;
          // 
          // textBoxVisnovok
          // 
          this.textBoxVisnovok.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.textBoxVisnovok.Location = new System.Drawing.Point(4, 4);
          this.textBoxVisnovok.Margin = new System.Windows.Forms.Padding(2);
          this.textBoxVisnovok.Multiline = true;
          this.textBoxVisnovok.Name = "textBoxVisnovok";
          this.textBoxVisnovok.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.textBoxVisnovok.Size = new System.Drawing.Size(593, 79);
          this.textBoxVisnovok.TabIndex = 1;
          this.textBoxVisnovok.TextChanged += new System.EventHandler(this.textBoxVisnovok_TextChanged);
          // 
          // tabPage3
          // 
          this.tabPage3.Controls.Add(this.textBoxNote);
          this.tabPage3.Location = new System.Drawing.Point(4, 22);
          this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
          this.tabPage3.Name = "tabPage3";
          this.tabPage3.Size = new System.Drawing.Size(601, 85);
          this.tabPage3.TabIndex = 2;
          this.tabPage3.Text = "Примітки";
          this.tabPage3.UseVisualStyleBackColor = true;
          // 
          // textBoxNote
          // 
          this.textBoxNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.textBoxNote.Location = new System.Drawing.Point(4, 4);
          this.textBoxNote.Margin = new System.Windows.Forms.Padding(2);
          this.textBoxNote.Multiline = true;
          this.textBoxNote.Name = "textBoxNote";
          this.textBoxNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.textBoxNote.Size = new System.Drawing.Size(593, 79);
          this.textBoxNote.TabIndex = 1;
          this.textBoxNote.TextChanged += new System.EventHandler(this.textBoxNote_TextChanged);
          // 
          // tabPage4
          // 
          this.tabPage4.Controls.Add(this.textBoxComments);
          this.tabPage4.Location = new System.Drawing.Point(4, 22);
          this.tabPage4.Name = "tabPage4";
          this.tabPage4.Size = new System.Drawing.Size(601, 85);
          this.tabPage4.TabIndex = 3;
          this.tabPage4.Text = "Коментар по ПТК";
          this.tabPage4.UseVisualStyleBackColor = true;
          // 
          // textBoxComments
          // 
          this.textBoxComments.AcceptsTab = true;
          this.textBoxComments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.textBoxComments.Location = new System.Drawing.Point(4, 4);
          this.textBoxComments.Margin = new System.Windows.Forms.Padding(2);
          this.textBoxComments.Multiline = true;
          this.textBoxComments.Name = "textBoxComments";
          this.textBoxComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.textBoxComments.Size = new System.Drawing.Size(593, 79);
          this.textBoxComments.TabIndex = 1;
          // 
          // label3
          // 
          this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(928, 25);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(37, 13);
          this.label3.TabIndex = 8;
          this.label3.Text = "Status";
          // 
          // tbStatus
          // 
          this.tbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.tbStatus.Location = new System.Drawing.Point(900, 46);
          this.tbStatus.Name = "tbStatus";
          this.tbStatus.ReadOnly = true;
          this.tbStatus.Size = new System.Drawing.Size(116, 20);
          this.tbStatus.TabIndex = 9;
          this.tbStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // splitContainerParams
          // 
          this.splitContainerParams.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainerParams.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
          this.splitContainerParams.Location = new System.Drawing.Point(0, 0);
          this.splitContainerParams.Name = "splitContainerParams";
          // 
          // splitContainerParams.Panel1
          // 
          this.splitContainerParams.Panel1.Controls.Add(this.gbParamGrid);
          this.splitContainerParams.Panel1MinSize = 500;
          // 
          // splitContainerParams.Panel2
          // 
          this.splitContainerParams.Panel2.Controls.Add(this.GroupBox2);
          this.splitContainerParams.Size = new System.Drawing.Size(1007, 602);
          this.splitContainerParams.SplitterDistance = 800;
          this.splitContainerParams.TabIndex = 2;
          // 
          // statusStrip
          // 
          this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsEmpty,
            this.tsArticle,
            this.tsWorkCount,
            this.tsNet});
          this.statusStrip.Location = new System.Drawing.Point(0, 799);
          this.statusStrip.Name = "statusStrip";
          this.statusStrip.RightToLeft = System.Windows.Forms.RightToLeft.No;
          this.statusStrip.Size = new System.Drawing.Size(1028, 22);
          this.statusStrip.TabIndex = 10;
          this.statusStrip.Text = "statusStrip1";
          // 
          // tsEmpty
          // 
          this.tsEmpty.Name = "tsEmpty";
          this.tsEmpty.Size = new System.Drawing.Size(860, 17);
          this.tsEmpty.Spring = true;
          this.tsEmpty.Text = "      ";
          // 
          // tsArticle
          // 
          this.tsArticle.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
          this.tsArticle.Name = "tsArticle";
          this.tsArticle.Size = new System.Drawing.Size(48, 17);
          this.tsArticle.Text = "tsArticle";
          // 
          // tsWorkCount
          // 
          this.tsWorkCount.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
          this.tsWorkCount.Name = "tsWorkCount";
          this.tsWorkCount.Size = new System.Drawing.Size(73, 17);
          this.tsWorkCount.Text = "tsWorkCount";
          // 
          // tsNet
          // 
          this.tsNet.ForeColor = System.Drawing.Color.Red;
          this.tsNet.Name = "tsNet";
          this.tsNet.Size = new System.Drawing.Size(32, 17);
          this.tsNet.Text = "tsNet";
          // 
          // splitContainerAll
          // 
          this.splitContainerAll.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.splitContainerAll.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
          this.splitContainerAll.Location = new System.Drawing.Point(9, 79);
          this.splitContainerAll.Name = "splitContainerAll";
          this.splitContainerAll.Orientation = System.Windows.Forms.Orientation.Horizontal;
          // 
          // splitContainerAll.Panel1
          // 
          this.splitContainerAll.Panel1.Controls.Add(this.splitContainerParams);
          // 
          // splitContainerAll.Panel2
          // 
          this.splitContainerAll.Panel2.Controls.Add(this.splitContainerAddParams);
          this.splitContainerAll.Size = new System.Drawing.Size(1007, 717);
          this.splitContainerAll.SplitterDistance = 602;
          this.splitContainerAll.TabIndex = 11;
          // 
          // splitContainerAddParams
          // 
          this.splitContainerAddParams.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainerAddParams.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
          this.splitContainerAddParams.Location = new System.Drawing.Point(0, 0);
          this.splitContainerAddParams.Name = "splitContainerAddParams";
          // 
          // splitContainerAddParams.Panel1
          // 
          this.splitContainerAddParams.Panel1.Controls.Add(this.GroupBox3);
          // 
          // splitContainerAddParams.Panel2
          // 
          this.splitContainerAddParams.Panel2.Controls.Add(this.splitContainerAddParams2);
          this.splitContainerAddParams.Size = new System.Drawing.Size(1007, 111);
          this.splitContainerAddParams.SplitterDistance = 188;
          this.splitContainerAddParams.TabIndex = 13;
          // 
          // splitContainerAddParams2
          // 
          this.splitContainerAddParams2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainerAddParams2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
          this.splitContainerAddParams2.Location = new System.Drawing.Point(0, 0);
          this.splitContainerAddParams2.Name = "splitContainerAddParams2";
          // 
          // splitContainerAddParams2.Panel1
          // 
          this.splitContainerAddParams2.Panel1.Controls.Add(this.tabControl1);
          // 
          // splitContainerAddParams2.Panel2
          // 
          this.splitContainerAddParams2.Panel2.Controls.Add(this.GroupBox4);
          this.splitContainerAddParams2.Size = new System.Drawing.Size(815, 111);
          this.splitContainerAddParams2.SplitterDistance = 609;
          this.splitContainerAddParams2.TabIndex = 0;
          // 
          // MainAppForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(1028, 821);
          this.Controls.Add(this.splitContainerAll);
          this.Controls.Add(this.statusStrip);
          this.Controls.Add(this.tbStatus);
          this.Controls.Add(this.label3);
          this.Controls.Add(this.GB_OWNER);
          this.Controls.Add(this.MainMenu);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
          this.MainMenuStrip = this.MainMenu;
          this.Margin = new System.Windows.Forms.Padding(2);
          this.MaximizeBox = true;
          this.MinimizeBox = true;
          this.MinimumSize = new System.Drawing.Size(850, 700);
          this.Name = "MainAppForm";
          this.ShowInTaskbar = true;
          this.Text = "MainAppForm";
          this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainAppForm_FormClosing);
          this.Shown += new System.EventHandler(this.MainAppForm_Shown);
          this.MainMenu.ResumeLayout(false);
          this.MainMenu.PerformLayout();
          this.GB_OWNER.ResumeLayout(false);
          this.GB_OWNER.PerformLayout();
          this.GroupBox2.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dataGridEventLog)).EndInit();
          this.GroupBox3.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dataGridLicence)).EndInit();
          this.GroupBox4.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
          this.tabControl1.ResumeLayout(false);
          this.tabPage1.ResumeLayout(false);
          this.tabPage1.PerformLayout();
          this.tabPage2.ResumeLayout(false);
          this.tabPage2.PerformLayout();
          this.tabPage3.ResumeLayout(false);
          this.tabPage3.PerformLayout();
          this.tabPage4.ResumeLayout(false);
          this.tabPage4.PerformLayout();
          this.splitContainerParams.Panel1.ResumeLayout(false);
          this.splitContainerParams.Panel2.ResumeLayout(false);
          this.splitContainerParams.ResumeLayout(false);
          this.statusStrip.ResumeLayout(false);
          this.statusStrip.PerformLayout();
          this.splitContainerAll.Panel1.ResumeLayout(false);
          this.splitContainerAll.Panel2.ResumeLayout(false);
          this.splitContainerAll.ResumeLayout(false);
          this.splitContainerAddParams.Panel1.ResumeLayout(false);
          this.splitContainerAddParams.Panel2.ResumeLayout(false);
          this.splitContainerAddParams.ResumeLayout(false);
          this.splitContainerAddParams2.Panel1.ResumeLayout(false);
          this.splitContainerAddParams2.Panel2.ResumeLayout(false);
          this.splitContainerAddParams2.ResumeLayout(false);
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip MainMenu;
      private System.Windows.Forms.ToolStripMenuItem MenuURCHP;
      private System.Windows.Forms.ToolStripMenuItem MenuURZP;
      private System.Windows.Forms.ToolStripMenuItem MenuURCHM;
      private System.Windows.Forms.ToolStripMenuItem MenuDVR;
      private System.Windows.Forms.ToolStripMenuItem MenuEXIT;
      private System.Windows.Forms.GroupBox GB_OWNER;
      private System.Windows.Forms.TextBox txtOwnerName;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox txtOwnerRDPO;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.GroupBox gbParamGrid;
      private System.Windows.Forms.GroupBox GroupBox2;
      private System.Windows.Forms.GroupBox GroupBox4;
      public System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage3;
      public System.Windows.Forms.TextBox textBoxDozvil;
      public System.Windows.Forms.TextBox textBoxNote;
      private System.Windows.Forms.ToolStripMenuItem AddSectorMenuItem;
      private System.Windows.Forms.ToolStripMenuItem RemoveSectorMenuItem;
      private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
      private System.Windows.Forms.DataGridView dataGridLicence;
      private System.Windows.Forms.DataGridView dgLog;
      private System.Windows.Forms.ToolStripMenuItem URCPEmsCalc;
      private System.Windows.Forms.ToolStripMenuItem URCPSave;
      private System.Windows.Forms.ToolStripMenuItem URCPDuplicateStation;
      private System.Windows.Forms.ToolStripMenuItem UCRFEMSConclusion;
      private System.Windows.Forms.ToolStripMenuItem eMSPermissionToolStripMenuItem;
      private System.Windows.Forms.DataGridView dataGridEventLog;
      private System.Windows.Forms.TabPage tabPage2;
      public System.Windows.Forms.TextBox textBoxVisnovok;
      public System.Windows.Forms.GroupBox GroupBox3;
      private System.Windows.Forms.ToolStripMenuItem CheckoutMenuItem;
      private System.Windows.Forms.ToolStripMenuItem URCP_AttachDoc;
      private System.Windows.Forms.ToolStripMenuItem miArticle;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbStatus;
      private System.Windows.Forms.Label lblTechUser;
      private System.Windows.Forms.SplitContainer splitContainerParams;
      public System.Windows.Forms.TabPage tabPage4;
      public System.Windows.Forms.TextBox textBoxComments;
      private System.Windows.Forms.ToolStripMenuItem DiffReportGenerationMenuItem;
      private System.Windows.Forms.ToolStripMenuItem menuUrzpPtk2;
      private System.Windows.Forms.ToolStripMenuItem ChangeFilialToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem AddSectorSps;
      private System.Windows.Forms.ToolStripMenuItem AddSectorUps;
      private System.Windows.Forms.ToolStripMenuItem menuUrzpPtk3;
      private System.Windows.Forms.ToolStripMenuItem toolStripCoord;
      private System.Windows.Forms.ToolStripSeparator UrcpSeparator1;
      private System.Windows.Forms.ToolStripMenuItem UrcpFindFrequencies;
      private System.Windows.Forms.ToolStripMenuItem printOnBlank;
      private System.Windows.Forms.ToolStripMenuItem MenuAdd2Net;
      private System.Windows.Forms.StatusStrip statusStrip;
      private System.Windows.Forms.ToolStripStatusLabel tsNet;
      private System.Windows.Forms.ToolStripStatusLabel tsArticle;
      private System.Windows.Forms.ToolStripStatusLabel tsWorkCount;
      private System.Windows.Forms.ToolStripStatusLabel tsEmpty;
      private System.Windows.Forms.SplitContainer splitContainerAll;
      private System.Windows.Forms.SplitContainer splitContainerAddParams;
      private System.Windows.Forms.SplitContainer splitContainerAddParams2;
   }
}