﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    public partial class AmateurBandForm : Form
    {
        private BandAmateur _bandAmateur = new BandAmateur();

        public string _power { 
            get { return _bandAmateur.Power[PowerUnits.W].ToStringNullD(); }
            set
            {
                double result = IM.NullD;
                Double.TryParse(value, out result);
                _bandAmateur.Power[PowerUnits.W] = result;                
            }        
        }

        public AmateurBandForm(string tableName, int id)
        {                                   
            InitializeComponent();
            _bandAmateur.Id = id;            
            _bandAmateur.Load();
        }                

        public void SaveAppl()
        {
            _bandAmateur.typeStr.Clear();
            for (int i = 0; i < chckLstBox.Items.Count;i++ )
            {
                if (chckLstBox.GetItemChecked(i))
                    _bandAmateur.typeStr.Add(chckLstBox.Items[i].ToString());
            }
            _bandAmateur.typeTechStr = "";    
            for (int i = 0; i < _bandAmateur.typeStr.Count; i++)
                    _bandAmateur.typeTechStr += _bandAmateur.typeStr[i] + ",";
            if (_bandAmateur.typeTechStr.Length > 0)
                _bandAmateur.typeTechStr = _bandAmateur.typeTechStr.Remove(_bandAmateur.typeTechStr.Length - 1);
            _bandAmateur.Save();
        }

        private void AmateurBandForm_Load(object sender, EventArgs e)
        {
            List<string> _typeStatList = EriFiles.GetEriDescrList("AmateurType");
            txtBandwidth1.DataBindings.Add("Text", _bandAmateur, "BandMin", true);
            txtBandwidth2.DataBindings.Add("Text", _bandAmateur, "BandMax", true);

            cmbBoxCatOper.Items.AddRange(EriFiles.GetEriDescrList("AmateurCateg").ToArray());
            cmbBxTypeStat.Items.AddRange(_typeStatList.ToArray());            
            cmbBoxCatOper.DataBindings.Add("SelectedItem", _bandAmateur, "Category", true);
            cmbBxTypeStat.DataBindings.Add("SelectedItem", _bandAmateur, "TypeStat", true);
                       
            txtPower.DataBindings.Add("Text", this, "_power", true);

            chckLstBox.Items.AddRange(new object[]{"CW","SSB","AM","FM","SSTV","DIGI","MGM","IBP","SAT","EME"});
            for (int i = 0; i < _bandAmateur.typeStr.Count; i++)
                if (chckLstBox.Items.Contains(_bandAmateur.typeStr[i]))
                    chckLstBox.SetItemChecked(chckLstBox.Items.IndexOf(_bandAmateur.typeStr[i]), true);
        }

        private void ShowMessageBand()
        {
            if (txtBandwidth1.Text.ToDouble(IM.NullD)>=txtBandwidth2.Text.ToDouble(IM.NullD))
            {
                MessageBox.Show("Значення частоти в полі 2 повинно бути не менше ніж в полі 1!");
            }
        }

        private void txtBandwidth1_Leave(object sender, EventArgs e)
        {
            ShowMessageBand();
        }

        private void txtBandwidth2_Leave(object sender, EventArgs e)
        {
            ShowMessageBand();
        }
     

        private void cmbBxTypeStat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBxTypeStat.SelectedIndex == 0 || cmbBxTypeStat.SelectedIndex == 3)
            {
                cmbBoxCatOper.Items.Clear();
                cmbBoxCatOper.Items.AddRange(EriFiles.GetEriDescrList("AmateurCateg").ToArray());                
                cmbBoxCatOper.Enabled = true;
                cmbBoxCatOper.SelectedItem = _bandAmateur.Category;                
            }
            else
            {
                _bandAmateur.Category = "";
                cmbBoxCatOper.Items.Clear();                
                cmbBoxCatOper.Enabled = false;                
            }
        }
    }

}
