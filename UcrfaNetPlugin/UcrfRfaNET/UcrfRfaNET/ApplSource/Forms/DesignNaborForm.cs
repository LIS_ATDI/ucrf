﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Налаштування експорту реєстру
    /// </summary>
    public partial class DesignNaborForm : Form
    {
        private XmlAllStation xAll;
        private string _tableName;        
        protected Dictionary<string, string> NaborDict { get; set; }
        public Dictionary<string, string> DictLocal { get; set; }
        public List<string> ListNabor=new List<string>();
        public string CurrEntity = ""; 
        public DesignNaborForm()
        {
            InitializeComponent();         
        }

        //Завантаження даних
        public DesignNaborForm(string tableName)
            : this()
        {            
            _tableName = tableName;            
            ReadData(_tableName);
        }

        private void ReadData(string tableName)
        {
            ReadFields(tableName);            
            ReadNabor();
            SelectedListBox();
        }

        /// <summary>
        /// Формування наборів
        /// </summary>
        private void ReadNabor()
        {
            NaborDict = xAll.LoadNabor();
            for (int i = 0; i < NaborDict.Count; i++)
                lstBoxNabor.Items.Add(NaborDict.Keys.ToList()[i] + "/" + NaborDict.Values.ToList()[i]);
            lstBoxNabor.Items.Add("інший набір");
        }

        /// <summary>
        /// Формування списку полів для експорту даних
        /// </summary>        
        /// <param name="tableName"></param>
        private void ReadFields(string tableName)
        {            
            xAll = new XmlAllStation();
            xAll.GetAllFields(tableName);
            for (int i = 0; i < xAll._fieldsDict.Count; i++)
                chkdLstBoxField.Items.Add(xAll._fieldsDict.Values.ToList()[i]);
        }

        /// <summary>
        /// Вибрати всі прапорці
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {            
            DictLocal=new Dictionary<string, string>();
            for (int i = 0; i < chkdLstBoxField.Items.Count; i++)
            {
                chkdLstBoxField.SetItemChecked(i, chkBoxSelectAll.Checked);
                if (chkBoxSelectAll.Checked)
                {
                    //string key = xAll._fieldsDict.Keys.ToList()[xAll.checkList[i].ToInt32(IM.NullI)];
                    //string value = xAll._fieldsDict.Values.ToList()[xAll.checkList[i].ToInt32(IM.NullI)];
                    string key = xAll._fieldsDict.Keys.ToList()[i];
                    string value = xAll._fieldsDict.Values.ToList()[i];
                    DictLocal.Add(key, value);
                }
            }
        }

        /// <summary>
        /// Завантаження набору
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadNab_Click(object sender, EventArgs e)
        {
            DictLocal=new Dictionary<string, string>();
            if (lstBoxNabor.SelectedIndex < 0)
            {
                MessageBox.Show("Оберіть набір для завантаження");
                return;
            }
            if (xAll._naborList.Count > lstBoxNabor.SelectedIndex)
                xAll.Nabor = xAll._naborList[lstBoxNabor.SelectedIndex];
            xAll.LoadNaborChecked();
            for (int i = 0; i < xAll._fieldsDict.Count; i++)
                chkdLstBoxField.SetItemChecked(i, false);
            for (int i = 0; i < xAll.checkList.Count; i++)
            {
                chkdLstBoxField.SetItemChecked(xAll.checkList[i].ToInt32(IM.NullI), true);
                string key = xAll._fieldsDict.Keys.ToList()[xAll.checkList[i].ToInt32(IM.NullI)];
                string value = xAll._fieldsDict.Values.ToList()[xAll.checkList[i].ToInt32(IM.NullI)];
                DictLocal.Add(key, value); 
            }           
        }

        /// <summary>
        /// Подія на відмітку прапорця поля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBoxNabor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Обрано список інший набір
            SelectedListBox();
        }

        /// <summary>
        /// Відмітка обраного пункту зберігання
        /// </summary>
        private void SelectedListBox()
        {
            if (lstBoxNabor.SelectedIndex==lstBoxNabor.Items.Count-1)
            {
                chkdLstBoxField.Enabled = true;
                btnLoadNab.Enabled = false;
                btnExportData.Enabled = false;                
                chkBoxSelectAll.Enabled = true;               
            }
            else
            {
                chkdLstBoxField.Enabled = false;
                btnLoadNab.Enabled = true;
                btnExportData.Enabled = true;                
                chkBoxSelectAll.Enabled = false;
            }
        }

        /// <summary>
        /// Експортувати дані
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExportData_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < chkdLstBoxField.Items.Count; i++)
                if (chkdLstBoxField.GetItemChecked(i))
                    ListNabor.Add(chkdLstBoxField.Items[i].ToString());
            CurrEntity = xAll.Entity;
        }              

        /// <summary>
        /// Свторити файл
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateFile_Click(object sender, EventArgs e)
        {
            DictLocal=new Dictionary<string, string>();          
            for (int i = 0; i < chkdLstBoxField.Items.Count; i++)
                if (chkdLstBoxField.GetItemChecked(i))
                {
                    string key = xAll._fieldsDict.Keys.ToList()[i];
                    string value = xAll._fieldsDict.Values.ToList()[i];
                    DictLocal.Add(key, value);                 
                    ListNabor.Add(chkdLstBoxField.Items[i].ToString());
                }
            CurrEntity = xAll.Entity;
        }

        /// <summary>
        /// Вертає назву файла в який необхідно зберегти XML
        /// </summary>
        /// <returns></returns>
        public string CreateFile()
        {
            saveFileDialog1.Filter = "Файли XML (*.xml)|*.xml";
            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
                return null;
            return saveFileDialog1.FileName;                      
        }
       
    }
}
