﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;
using XICSM.UcrfRfaNET.Map;
using Lis.CommonLib.Binding;
using IdwmNET;
using NearestCountryItem = IdwmNET.NearestCountryItem;


namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    /// <summary>
    /// Форма для редагування записів сайтів всіх радіослужб і прив'язки до адмінсайтів
    /// </summary>
    public partial class AdminSiteAllTech : Form, INotifyPropertyChanged
    {
        /// <summary>
        /// Отображает форму сайтов
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">ID записи</param>
        public static void Show(string tableName, int id)
        {
            if (ApplSetting.IsDebug)
            {
                using (AdminSiteAllTech frm = new AdminSiteAllTech(tableName, id))
                    frm.ShowDialog();
            }
            else
            {
                RecordPtr rcPtr = new RecordPtr(tableName, id);
                rcPtr.UserEdit();
            }

        }




        //-----------
        private Idwm _idwmVal;
        private bool _isIdwmInit;
        private bool isCalcAsl;
        public string TableName = "";
        public int Id;
        private PositionAdmSite posAdmSite;
        //private BindingVar bindVar;
        private List<string> photoList;
        private SelectAddressControlFast cf;
        private LisMapWf.LisMapWfControl mapcl;
        private string status;
        private List<string> delphotoList = new List<string>();
        private string savedProv = "";
        private string savedSubProv = "";
        private string savedCity = "";
        private string savedTypedCity = "";
        private string firstLat;
        private string firstLong;
        private bool isAccept;
        private bool isSites;

        public string SendUser
        {
            get { return posAdmSite.SendUser; }
            set
            {
                if (posAdmSite.SendUser != value)
                {
                    posAdmSite.SendUser = value;
                    RaisePropertyChanged("SendUser");
                }
            }
        }
        public DateTime SendDate
        {
            get { return posAdmSite.SendDate; }
            set
            {
                if (posAdmSite.SendDate != value)
                {
                    posAdmSite.SendDate = value;
                    RaisePropertyChanged("SendDate");
                }
            }
        }

        public string AcceptUser
        {
            get { return posAdmSite.AcceptUser; }
            set
            {
                if (posAdmSite.AcceptUser != value)
                {
                    posAdmSite.AcceptUser = value;
                    RaisePropertyChanged("AcceptUser");
                }
            }
        }
        public DateTime AcceptDate
        {
            get { return posAdmSite.AcceptDate; }
            set
            {
                if (posAdmSite.AcceptDate != value)
                {
                    posAdmSite.AcceptDate = value;
                    RaisePropertyChanged("AcceptDate");
                }
            }
        }

        private bool _isEnableChange = true;
        public const string FieldIsEnableChange = "IsEnableChange";
        /// <summary>
        /// Только для чтения
        /// </summary>
        public bool IsEnableChange
        {
            get { return _isEnableChange; }
            set
            {
                if (_isEnableChange != value)
                {
                    _isEnableChange = value;
                    RaisePropertyChanged(FieldIsEnableChange);
                }
            }
        }

        private ComboBoxDictionaryList<string, string> cmbFoto = new ComboBoxDictionaryList<string, string>();



        private int _admsId = IM.NullI; //Id админ сайта


        /// <summary>
        /// Конструктор //+
        /// </summary>
        private AdminSiteAllTech()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="tableName">Имя таблицы позиции</param>
        /// <param name="id">Id pfgbcb</param>
        public AdminSiteAllTech(string tableName, int id) : this()
        {
            _idwmVal = new Idwm();
            _isIdwmInit = !_idwmVal.Init(11); //Init IDWM



            bool boolCheck;
            bool boolAccept;

            TableName = tableName;
            Id = id;
            posAdmSite = new PositionAdmSite();
            posAdmSite.LoadStatePosition(id, tableName);
            BindCaption();
            if (posAdmSite.AdminSitesId == IM.NullI)
                posAdmSite.CountryId = "UKR";
            FillAdminSite(posAdmSite.AdminSitesId);
            FillAddress();
            FillMap();
            FillCoatuu();
            _admsId = CheckStatus(out boolCheck, out boolAccept, tableName);
            if (tableName==ICSMTbl.SITES)
                isSites = true;
            if (tableName != ICSMTbl.SITES)
                boolAccept = false;

            CalcBorder();
            btnCheck.Enabled = boolCheck;
            btnAccept.Enabled = boolAccept;           
            FillFoto();
            firstLat = txtBoxLatLow.Text;
            firstLong = txtBoxLongLow.Text;
            //----
            // ReadOnly
            grpBoxKoord.DataBindings.Add("Enabled", this, FieldIsEnableChange);
            grpBoxAddress.DataBindings.Add("Enabled", this, FieldIsEnableChange);
            grpBoxAdmSite.DataBindings.Add("Enabled", this, FieldIsEnableChange);
            grpBoxCheck.DataBindings.Add("Enabled", this, FieldIsEnableChange);
            gbNote.DataBindings.Add("Enabled", this, FieldIsEnableChange);
        }

        /// <summary>
        /// Калькуляція відстані до кордону //+
        /// </summary>
        private void CalcBorder()
        {
            if (!_isIdwmInit || posAdmSite.X == IM.NullD || posAdmSite.Y == IM.NullD)
                return;
            float lonDec = (float) posAdmSite.LonDec;
            float latDec = (float) posAdmSite.LatDec;
            string[] exclude = { "UKR" };
            NearestCountryItem[] nearestCountry = _idwmVal.GetNearestCountries(Idwm.DecToRadian(lonDec), Idwm.DecToRadian(latDec), 1000, exclude, 100);
            string outStr = "~";
            if (nearestCountry.Length > 0)
                outStr = string.Format("{0} Km ({1})", ((double) (nearestCountry[0].distance)).Round(2), nearestCountry[0].country);
            txtBoxLimit.Text = outStr;
        }

        //Перевірка статуса
        private int CheckStatus(out bool check, out bool accept, string tblName)
        {
           return posAdmSite.CheckStatusPosition(out check, out accept, tblName);
        }

        private void FillAdminSite(int admId)
        {
            posAdmSite.GetAdminSiteInfo(admId);
            if (admId != IM.NullI)
            {
                string longitAdm = posAdmSite.AdminSiteX.LongDECToString();
                string latitAdm = posAdmSite.AdminSiteY.LatDECToString();

                if (posAdmSite.AdminSiteCsys != "4DMS")
                {
                    longitAdm = posAdmSite.AdminSiteX.DecToDms().LongDMSToString();
                    latitAdm = posAdmSite.AdminSiteY.DecToDms().LatDMSToString();
                }

                txtBoxAdmSite.Text = latitAdm + " - " + longitAdm + Environment.NewLine + posAdmSite.AdminSiteAddress;
            }
        }

        private void UpdateListComboFoto()
        {
            cmbFoto.Clear();
            for (int i = 0; i < photoList.Count; i++)
               cmbFoto.Add(new ComboBoxDictionary<string, string>((photoList[i].IndexOf("\\\\") == -1) ? HelpFunction.ReadDataFromSysConfig("SHDIR-DOC") + "\\" + photoList[i] : photoList[i], System.IO.Path.GetFileName(photoList[i])));
            // pctBox.Refresh();            
        }

        private void FillFoto()
        {
            cmbFoto.Clear();
            photoList = posAdmSite.GetFotoFromCitiesNew();            
            UpdateListComboFoto();
            cmbFoto.InitComboBox(cmbBoxPhoto);
            if (string.IsNullOrEmpty(cmbBoxPhoto.Text))
                if (cmbFoto.Count > 0)
                    cmbBoxPhoto.Text = cmbFoto[0].Key;
            pctBox.DataBindings.Add("ImageLocation", cmbFoto, "Key");
            pctBox.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void FillAddress()
        {
            cf = new SelectAddressControlFast();
            pnlAddress.Controls.Add(cf);
            cf.InitializeByCityId(posAdmSite.CityId);
            cf.CanEditPosition = true;
            if (cf.CurrentCity != null)
            {
                savedCity = cf.CurrentCity.cityName;
                savedProv = cf.CurrentCity.prov;
                savedTypedCity = cf.CurrentCity.type;
                savedSubProv = cf.CurrentCity.subprov;
            }
        }
        /// <summary>
        /// Заполняет форму отображения Админ сайта
        /// </summary>
        private void FillCoatuu()
        {
            posAdmSite.LoadCitiesId(posAdmSite.CityId);
            txtBoxKoatuu.Text = string.Format("Область = {1}{0}", Environment.NewLine, posAdmSite.CitiesProvince);
            txtBoxKoatuu.Text += string.Format("Район = {1}{0}", Environment.NewLine, posAdmSite.CitiesSubprovince);
            txtBoxKoatuu.Text += string.Format("Нас.пункт = {1}{0} {2}", Environment.NewLine, posAdmSite.CitiesCode, posAdmSite.City);
        }

        /// <summary>
        /// Виведення карти
        /// </summary>
        private void FillMap()
        {
            try
            {
                mapcl = new LisMapWf.LisMapWfControl();
                // mapcl.Show();            
                mapcl.Size = new Size(380, 340);
                mapcl.TabIndex = 0;
                grpBoxMap.Controls.Add(mapcl);
                mapcl.Top += 15;
                mapcl.Left += 5;
                mapcl.Init();
                mapcl.Clear(1, false);
                mapcl.SetCenter(31.25, 48.60);
                mapcl.SetScale(1545.0);
                mapcl.ShowStation(posAdmSite.LonDec, posAdmSite.LatDec, posAdmSite.FullAddress, "");

                mapcl.FitObjects();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //==================================================
        /// <summary>
        /// Отобразить станцию на карте
        /// </summary>
        /// <param name="_table">имя таблицы</param>
        /// <param name="_id">ID записи</param>
        private void ShowOnMap(string _table, int _id)
        {
            TxOnMap tx = new TxOnMap(_table, _id);
            TxsOnMap.InitMap();
            TxsOnMap.AddTx(tx);
        }

        /// <summary>
        /// Прив'язка даних
        /// </summary>
        private void BindCaption()
        {
            //Latitude          
            Binding tmpBindingLatTxt = new Binding("Text", posAdmSite, PositionState.FieldLatDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLatTxt.BindingComplete += BindingCompleteEvent;
            tmpBindingLatTxt.Parse += (s, e) => ControlToCoordinate_Parse(s, e, txtBoxLatLow);
            tmpBindingLatTxt.Format += CoordinateToControl_Format;
            txtBoxLatLow.DataBindings.Add(tmpBindingLatTxt);

            Binding tmpBindingLatLab = new Binding("Text", posAdmSite, PositionState.FieldLatDms, true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLatLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLatLab.Format += (s, e) =>
            {
                bool isCorect = true;
                double tmpVal = (double)ConvertType.ToDouble(e.Value.ToString(), IM.NullD);
                e.Value = "";
                if (tmpVal != IM.NullD)
                {
                    isCorect = HelpFunction.ValidateCoords(tmpVal, EnumCoordLine.Lat);
                    e.Value = HelpFunction.DmsToString(tmpVal, EnumCoordLine.Lat);
                    CalcBorder();
                }
                lblLat.BackColor = isCorect ? SystemColors.Control : Color.LightSalmon;
            };
            lblLat.DataBindings.Add(tmpBindingLatLab);

            //Longitude       
            Binding tmpBindingLongTxt = new Binding("Text", posAdmSite, "LonDms", true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLongTxt.BindingComplete += BindingCompleteEvent;
            tmpBindingLongTxt.Parse += (s, e) => ControlToCoordinate_Parse(s, e, txtBoxLongLow);
            tmpBindingLongTxt.Format += CoordinateToControl_Format;
            txtBoxLongLow.DataBindings.Add(tmpBindingLongTxt);

            Binding tmpBindingLongLab = new Binding("Text", posAdmSite, "LonDms", true, DataSourceUpdateMode.OnPropertyChanged);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Format += (s, e) =>
            {
                bool isCorect = true;
                double tmpVal = (double)ConvertType.ToDouble(e.Value.ToString(), IM.NullD);
                e.Value = "";
                if (tmpVal != IM.NullD)
                {
                    isCorect = HelpFunction.ValidateCoords(tmpVal, EnumCoordLine.Lon);
                    e.Value = HelpFunction.DmsToString(tmpVal, EnumCoordLine.Lon);
                    CalcBorder();
                }
                lblLong.BackColor = isCorect ? SystemColors.Control : Color.LightSalmon;
            };
            lblLong.DataBindings.Add(tmpBindingLongLab);



            txtBoxAddress.DataBindings.Add("Text", posAdmSite, "Address");
            //txtBoxRemark.DataBindings.Add("Text", posAdmSite, "Remark");
            txtBoxRemark.DataBindings.Add("Text", posAdmSite, "FullAddress");
            txtBoxAsl.DataBindings.Add("Text", posAdmSite, "ASl");

            Binding tmpBindingCheckNum = new Binding("Text", posAdmSite, "SendUser", true, DataSourceUpdateMode.OnValidation);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Format += tmpBindingLongLab_Format;
            lblCheckNamVal.DataBindings.Add(tmpBindingCheckNum);

            Binding tmpBindingCheckDat = new Binding("Text", posAdmSite, "SendDate", true, DataSourceUpdateMode.OnValidation);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Format += tmpBindingLongLab_Format;
            lblCheckDateVal.DataBindings.Add(tmpBindingCheckDat);

            Binding tmpBindingAcceptNum = new Binding("Text", this, "AcceptUser", true, DataSourceUpdateMode.OnValidation);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Format += tmpBindingLongLab_Format;
            lblAcceptNameVal.DataBindings.Add(tmpBindingAcceptNum);

            Binding tmpBindingAcceptDate = new Binding("Text", this, "AcceptDate", true, DataSourceUpdateMode.OnValidation);
            tmpBindingLongLab.BindingComplete += BindingCompleteEvent;
            tmpBindingLongLab.Parse += tmpBindingLongLab_Format;

            lblAcceptDateVal.DataBindings.Add(tmpBindingAcceptDate);

            btnSave.DataBindings.Add("Enabled", posAdmSite, "IsChanged");
            btnSaveExit.DataBindings.Add("Enabled", posAdmSite, "IsChanged");
            btnError.DataBindings.Add("Enabled", btnAccept, "Enabled");
        }

        void tmpBindingLongLab_Format(object sender, ConvertEventArgs e)
        {
            if (e.DesiredType != typeof(string))
                return;
            e.Value = e.Value.ToString();
        }

        /// <summary>
        /// OBJ->Control
        /// </summary>
        private void CoordinateToControl_Format(object sender, ConvertEventArgs e)
        {
            double val = (double) e.Value;
            if (val == IM.NullD)
                e.Value = "000000";
            else
                e.Value = CoordToCompon((double)e.Value);
        }
        /// <summary>
        /// OBJ->Control
        /// </summary>
        private void ControlToCoordinate_Parse(object sender, ConvertEventArgs e, Control ctr)
        {
            double val = ConvertType.StrToDMS(e.Value.ToString());
            e.Value = val;
            ctr.BackColor = (val != IM.NullD) ? SystemColors.Window : Color.LightSalmon;
        }
        /// <summary>
        /// Координата -> Компонента
        /// </summary>
        private string CoordToCompon(double val)
        {
            val *= 10000.0;
            string tmpStr = val.Round(1).ToString();
            while (tmpStr.Length < 6)
                tmpStr = "0" + tmpStr;
            return tmpStr;
        }
        /// <summary>
        /// Отображает сообжение об ошибке при преобразовании
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingCompleteEvent(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteState != BindingCompleteState.Success)
                MessageBox.Show(e.ErrorText);
        }

        /// <summary>
        /// Реакція кнопки Зберегти і вийти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveExit_Click(object sender, EventArgs e)
        {
            Save();
            Close();
        }

        private void btnCancelExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void btnICSMSite_Click(object sender, EventArgs e)
        {
            Save();
            RecordPtr rcPtr = new RecordPtr(TableName, Id);
            rcPtr.UserEdit();
        }

        /// <summary>
        /// Зберегти дані по станції
        /// </summary>
        public void Save()
        {
            if (posAdmSite.Status == CAndE.Chkd)
            {
                return;
            }

            if (isSites)
            {
                DialogResult res = MessageBox.Show("Ви хочете каскадно обновити всі записи в сайтах",
                                                   "Каскадне оновлення", MessageBoxButtons.YesNoCancel);
                RecordPtr rec = new RecordPtr(ICSMTbl.SITES, posAdmSite.Id);
                if (isAccept && posAdmSite.IsChanged)
                    if (res == DialogResult.Cancel)
                        return;

                if (res == DialogResult.Yes)
                    PositionState.ChangeSites(rec, rec, true);
            }
            if (isCalcAsl)
                CalculateASL();

            if (cf != null)
                if (cf.CurrentCity != null)
                {
                    string message = "";
                    if (savedProv != cf.CurrentCity.prov)
                        message += String.Format("Поміняти область з {0} на {1} ? \r\n", savedProv, cf.CurrentCity.prov);
                    if (savedSubProv != cf.CurrentCity.subprov)
                        message += String.Format("Поміняти район з {0} на {1} ? \r\n", savedSubProv, cf.CurrentCity.subprov);
                    if (savedTypedCity != cf.CurrentCity.type)
                        message += String.Format("Поміняти тип нас. пункту з {0} на {1} ? \r\n", savedTypedCity, cf.CurrentCity.type);
                    if (savedCity != cf.CurrentCity.cityName)
                        message += String.Format("Поміняти назву населеного пункту з {0} на {1} ? \r\n", savedCity, cf.CurrentCity.cityName);
                    if (!string.IsNullOrEmpty(message))
                        if (MessageBox.Show(message, "Info", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            posAdmSite.Subprovince = cf.CurrentCity.subprov;
                            posAdmSite.Province = cf.CurrentCity.prov;
                            posAdmSite.TypeCity = cf.CurrentCity.type;
                            posAdmSite.City = cf.CurrentCity.cityName;
                            savedProv = cf.CurrentCity.prov;
                            savedSubProv = cf.CurrentCity.subprov;
                            savedTypedCity = cf.CurrentCity.type;
                            savedCity = cf.CurrentCity.cityName;
                            posAdmSite.CityId = cf.CurrentCity.Id;

                        }
                }
            posAdmSite.SaveFotoCities(photoList);

            for (int i = 0; i < delphotoList.Count; i++)
                posAdmSite.DeleteRecCities(delphotoList[i]);

            if (posAdmSite != null)
            {                
                if (txtBoxAsl.Text != posAdmSite.ASl.ToString())
                    posAdmSite.ASl = Convert.ToDouble(txtBoxAsl.Text);
                
                posAdmSite.Save();
                if (!isSites)
                    posAdmSite.SetNewStatus(status);
                posAdmSite.IsChanged = false;    
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMapToAsl_Click(object sender, EventArgs e)
        {
            CalculateASL();
        }

        private void CalculateASL()
        {
            double asl = IMCalculate.CalcALS(posAdmSite.X, posAdmSite.Y, "4DMS");
            txtBoxAsl.Text = asl != IM.NullD ? asl.ToString() : "0";
            if (Convert.ToDouble(txtBoxAsl.Text) <= IM.NullD)
                txtBoxAsl.Text = "0";
            btnMapToAsl.Enabled = false;
            txtBoxAsl.ForeColor = Color.Black;
            isCalcAsl = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btndel_Click(object sender, EventArgs e)
        {
            if (photoList.Count > cmbBoxPhoto.SelectedIndex && cmbBoxPhoto.SelectedIndex >= 0)
            {
                photoList.RemoveAt(cmbBoxPhoto.SelectedIndex);
                delphotoList.Add(cmbFoto[cmbBoxPhoto.SelectedIndex].Key);
                delphotoList.Add(cmbFoto[cmbBoxPhoto.SelectedIndex].Description);
                posAdmSite.IsChanged = true;
            }
            cmbBoxPhoto.Focus();
            UpdateListComboFoto();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            List<string> _listFiles = new List<string>();
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.Multiselect = true;
                //openDlg.Filter = "Images|*.jpg;*.bmp;*.gif;*.jpeg";
                if (openDlg.ShowDialog() == DialogResult.OK)
                    _listFiles = openDlg.FileNames.ToList();
                if(_listFiles.Count > 0)
                    posAdmSite.IsChanged = true;
            }
            photoList.AddRange(_listFiles);
            cmbBoxPhoto.Focus();
            UpdateListComboFoto();
        }

        private void pctBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start(cmbFoto[cmbBoxPhoto.SelectedIndex].Key);
        }

        private void btnChangeAdmSite_Click(object sender, EventArgs e)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(posAdmSite.X, posAdmSite.Y, "4DMS", "4DMS");
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.SITES, 1, tmpXy.Longitude, tmpXy.Latitude);
            if (objPosition != null)
            {
                posAdmSite.AdminSitesId = objPosition.GetI("ID");
                FillAdminSite(objPosition.GetI("ID"));
            }
        }

        private void btnOffAdmSite_Click(object sender, EventArgs e)
        {
            posAdmSite.AdminSitesId = IM.NullI;
            txtBoxAdmSite.Clear();
        }

        /// <summary>
        /// Показати в окремому вікні
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFull_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TableName) && (Id != 0))
            {
                ShowOnMap(TableName, Id);
                TxsOnMap.ShowMap();
            }
        }

        /// <summary>
        /// Потрібна перевірка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheck_Click(object sender, EventArgs e)
        {
            posAdmSite.IsChanged = true;
            btnCheck.Enabled = false;
            if (isSites)
                btnAccept.Enabled = true;
            status = CAndE.PosAdmcodeEnum.NeedCheck.ToString();

            posAdmSite.SetUserParam(true);
            lblCheckDateVal.Text = SendDate.ToString();
            lblCheckNamVal.Text = SendUser;
            if (TableName == ICSMTbl.SITES)
                posAdmSite.SetStatusSites(CAndE.Need);
            if (TableName != ICSMTbl.SITES && _admsId == IM.NullI)
            {
                IMObject obj = IMObject.LoadFromDB(posAdmSite.TableName,posAdmSite.Id);
                posAdmSite.AdminSitesId = PositionState.ClonePositions(obj, ICSMTbl.SITES);
                IMRecordset r= new IMRecordset(posAdmSite.TableName,IMRecordset.Mode.ReadWrite);
                r.Select("ID,ADMS_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, posAdmSite.Id);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        r.Put("ADMS_ID", posAdmSite.AdminSitesId);
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }
            }
        }

        /// <summary>
        /// Перевірка проведена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAccept_Click(object sender, EventArgs e)
        {
            posAdmSite.IsChanged = true;
            isAccept = true;
            btnAccept.Enabled = false;
            btnCheck.Enabled = true;
            status = CAndE.PosAdmcodeEnum.Checked.ToString();

            posAdmSite.SetUserParam(false);
            lblAcceptDateVal.Text = AcceptDate.ToString();
            lblAcceptNameVal.Text = AcceptUser;
            if (TableName == ICSMTbl.SITES)
                posAdmSite.SetStatusSites(CAndE.Chkd);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        /// <summary>
        /// Зробити колір текста компоненти червоним
        /// </summary>
        /// <param name="c"></param>
        private void MakeRedHigh(Control c)
        {
            c.ForeColor = Color.Red;
        }

        /// <summary>
        /// Реакція на зміну координат
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBoxLatLow_TextChanged(object sender, EventArgs e)
        {
            if (txtBoxLatLow.Text != firstLat && !string.IsNullOrEmpty(firstLat))
            {
                MakeRedHigh(txtBoxAsl);
                MakeRedHigh(btnMapToAsl);
                isCalcAsl = true;
                btnMapToAsl.Enabled = true;
            }
            else if (string.IsNullOrEmpty(firstLat))
            {
                firstLat = txtBoxLatLow.Text;
            }
        }
        private void txtBoxLongLow_TextChanged(object sender, EventArgs e)
        {
            if (txtBoxLongLow.Text != firstLong && !string.IsNullOrEmpty(firstLong))
            {
                MakeRedHigh(txtBoxAsl);
                MakeRedHigh(btnMapToAsl);
                isCalcAsl = true;
                btnMapToAsl.Enabled = true;
            }
            else if (string.IsNullOrEmpty(firstLong))
            {
                firstLong = txtBoxLongLow.Text;
            }
        }

        private void grpBoxMap_SizeChanged(object sender, EventArgs e)
        {
            if (mapcl != null)
                mapcl.Size = ((Control)sender).Size;
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Помилка в формуванні адмін. сайту!");
            posAdmSite.SetStatusSites(CAndE.Error);
            btnError.Enabled = false;
            btnAccept.Enabled = false;
            btnCheck.Enabled = true;
        }
    }
}
