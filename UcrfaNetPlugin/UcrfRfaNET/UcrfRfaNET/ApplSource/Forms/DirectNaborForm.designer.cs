﻿namespace XICSM.UcrfRfaNET.ApplSource.Forms
{
    partial class DirectNaborForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkdLstBoxFieldDirect = new System.Windows.Forms.CheckedListBox();
            this.lstBoxNaborDirect = new System.Windows.Forms.ListBox();
            this.chkBoxSelectAllDirect = new System.Windows.Forms.CheckBox();
            this.lblFiledsDirect = new System.Windows.Forms.Label();
            this.lblNameNabDirect = new System.Windows.Forms.Label();
            this.txtBoxNaborDirect = new System.Windows.Forms.TextBox();
            this.lblNameEntDirect = new System.Windows.Forms.Label();
            this.cmbBoxEntityDirect = new System.Windows.Forms.ComboBox();
            this.btnSaveNaborDirect = new System.Windows.Forms.Button();
            this.lblNaborDirect = new System.Windows.Forms.Label();
            this.btnDelNab = new System.Windows.Forms.Button();
            this.btnLoadNab = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkdLstBoxFieldDirect
            // 
            this.chkdLstBoxFieldDirect.FormattingEnabled = true;
            this.chkdLstBoxFieldDirect.Location = new System.Drawing.Point(12, 27);
            this.chkdLstBoxFieldDirect.Name = "chkdLstBoxFieldDirect";
            this.chkdLstBoxFieldDirect.Size = new System.Drawing.Size(508, 394);
            this.chkdLstBoxFieldDirect.TabIndex = 0;
            // 
            // lstBoxNaborDirect
            // 
            this.lstBoxNaborDirect.FormattingEnabled = true;
            this.lstBoxNaborDirect.Location = new System.Drawing.Point(536, 29);
            this.lstBoxNaborDirect.Name = "lstBoxNaborDirect";
            this.lstBoxNaborDirect.Size = new System.Drawing.Size(177, 394);
            this.lstBoxNaborDirect.TabIndex = 1;
            // 
            // chkBoxSelectAllDirect
            // 
            this.chkBoxSelectAllDirect.AutoSize = true;
            this.chkBoxSelectAllDirect.Location = new System.Drawing.Point(22, 427);
            this.chkBoxSelectAllDirect.Name = "chkBoxSelectAllDirect";
            this.chkBoxSelectAllDirect.Size = new System.Drawing.Size(85, 17);
            this.chkBoxSelectAllDirect.TabIndex = 2;
            this.chkBoxSelectAllDirect.Text = "Вибрати всі";
            this.chkBoxSelectAllDirect.UseVisualStyleBackColor = true;
            this.chkBoxSelectAllDirect.CheckedChanged += new System.EventHandler(this.ChkBoxSelectAllDirectCheckedChanged);
            // 
            // lblFiledsDirect
            // 
            this.lblFiledsDirect.AutoSize = true;
            this.lblFiledsDirect.Location = new System.Drawing.Point(12, 9);
            this.lblFiledsDirect.Name = "lblFiledsDirect";
            this.lblFiledsDirect.Size = new System.Drawing.Size(138, 13);
            this.lblFiledsDirect.TabIndex = 3;
            this.lblFiledsDirect.Text = "Поля для експорту даних:";
            // 
            // lblNameNabDirect
            // 
            this.lblNameNabDirect.AutoSize = true;
            this.lblNameNabDirect.Location = new System.Drawing.Point(16, 456);
            this.lblNameNabDirect.Name = "lblNameNabDirect";
            this.lblNameNabDirect.Size = new System.Drawing.Size(77, 13);
            this.lblNameNabDirect.TabIndex = 4;
            this.lblNameNabDirect.Text = "Назва набору";
            // 
            // txtBoxNaborDirect
            // 
            this.txtBoxNaborDirect.Location = new System.Drawing.Point(105, 453);
            this.txtBoxNaborDirect.Name = "txtBoxNaborDirect";
            this.txtBoxNaborDirect.Size = new System.Drawing.Size(157, 20);
            this.txtBoxNaborDirect.TabIndex = 5;
            // 
            // lblNameEntDirect
            // 
            this.lblNameEntDirect.AutoSize = true;
            this.lblNameEntDirect.Location = new System.Drawing.Point(16, 487);
            this.lblNameEntDirect.Name = "lblNameEntDirect";
            this.lblNameEntDirect.Size = new System.Drawing.Size(83, 13);
            this.lblNameEntDirect.TabIndex = 6;
            this.lblNameEntDirect.Text = "Назва сутності";
            // 
            // cmbBoxEntityDirect
            // 
            this.cmbBoxEntityDirect.FormattingEnabled = true;
            this.cmbBoxEntityDirect.Location = new System.Drawing.Point(105, 484);
            this.cmbBoxEntityDirect.Name = "cmbBoxEntityDirect";
            this.cmbBoxEntityDirect.Size = new System.Drawing.Size(157, 21);
            this.cmbBoxEntityDirect.TabIndex = 7;
            // 
            // btnSaveNaborDirect
            // 
            this.btnSaveNaborDirect.Location = new System.Drawing.Point(398, 482);
            this.btnSaveNaborDirect.Name = "btnSaveNaborDirect";
            this.btnSaveNaborDirect.Size = new System.Drawing.Size(122, 23);
            this.btnSaveNaborDirect.TabIndex = 8;
            this.btnSaveNaborDirect.Text = "Зберегти набір";
            this.btnSaveNaborDirect.UseVisualStyleBackColor = true;
            this.btnSaveNaborDirect.Click += new System.EventHandler(this.BtnSaveNaborDirectClick);
            // 
            // lblNaborDirect
            // 
            this.lblNaborDirect.AutoSize = true;
            this.lblNaborDirect.Location = new System.Drawing.Point(533, 13);
            this.lblNaborDirect.Name = "lblNaborDirect";
            this.lblNaborDirect.Size = new System.Drawing.Size(143, 13);
            this.lblNaborDirect.TabIndex = 11;
            this.lblNaborDirect.Text = "Список створених наборів:";
            // 
            // btnDelNab
            // 
            this.btnDelNab.Location = new System.Drawing.Point(568, 482);
            this.btnDelNab.Name = "btnDelNab";
            this.btnDelNab.Size = new System.Drawing.Size(122, 23);
            this.btnDelNab.TabIndex = 12;
            this.btnDelNab.Text = "Видалити набір";
            this.btnDelNab.UseVisualStyleBackColor = true;
            this.btnDelNab.Click += new System.EventHandler(this.btnDelNab_Click);
            // 
            // btnLoadNab
            // 
            this.btnLoadNab.Location = new System.Drawing.Point(568, 446);
            this.btnLoadNab.Name = "btnLoadNab";
            this.btnLoadNab.Size = new System.Drawing.Size(122, 23);
            this.btnLoadNab.TabIndex = 13;
            this.btnLoadNab.Text = "Завантажити набір";
            this.btnLoadNab.UseVisualStyleBackColor = true;
            this.btnLoadNab.Click += new System.EventHandler(this.btnLoadNab_Click);
            // 
            // DirectNaborForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 517);
            this.Controls.Add(this.btnLoadNab);
            this.Controls.Add(this.btnDelNab);
            this.Controls.Add(this.lblNaborDirect);
            this.Controls.Add(this.btnSaveNaborDirect);
            this.Controls.Add(this.cmbBoxEntityDirect);
            this.Controls.Add(this.lblNameEntDirect);
            this.Controls.Add(this.txtBoxNaborDirect);
            this.Controls.Add(this.lblNameNabDirect);
            this.Controls.Add(this.lblFiledsDirect);
            this.Controls.Add(this.chkBoxSelectAllDirect);
            this.Controls.Add(this.lstBoxNaborDirect);
            this.Controls.Add(this.chkdLstBoxFieldDirect);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(733, 551);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(733, 551);
            this.Name = "DirectNaborForm";
            this.Text = "Управління наборами полів для експорту реєстру РЧП";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox chkdLstBoxFieldDirect;
        private System.Windows.Forms.ListBox lstBoxNaborDirect;
        private System.Windows.Forms.CheckBox chkBoxSelectAllDirect;
        private System.Windows.Forms.Label lblFiledsDirect;
        private System.Windows.Forms.Label lblNameNabDirect;
        private System.Windows.Forms.TextBox txtBoxNaborDirect;
        private System.Windows.Forms.Label lblNameEntDirect;
        private System.Windows.Forms.ComboBox cmbBoxEntityDirect;
        private System.Windows.Forms.Button btnSaveNaborDirect;
        private System.Windows.Forms.Label lblNaborDirect;
        private System.Windows.Forms.Button btnDelNab;
        private System.Windows.Forms.Button btnLoadNab;
    }
}

