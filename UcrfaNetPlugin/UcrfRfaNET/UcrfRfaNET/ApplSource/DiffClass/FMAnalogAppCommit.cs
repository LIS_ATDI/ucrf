﻿using System;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class FmAnalogAppCommit : BaseCommitClass
    {
        public int Id { get; set; }

        private bool initialized;
        private PositionState selectedPosition;

        private CoordinateCommitHelper coordHelper;

        private FmAnalogStation fmStation;
        private FmAnalogDiffStation fmDiffStation;        

        # region Cell
        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Система передачі                   
        /// </summary>
        private CellStringComboboxAplyProperty CellGive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача        
        /// </summary>
        private CellDoubleLookupProperty CellPowerTr = new CellDoubleLookupProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        /// <summary>
        /// Сертифікат відповідності номер 
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Сертифікат відповідності Дата 
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        /// <summary>
        /// Довжина фідера
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Направленість антени
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenDirect = new CellStringLookupEncodedProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenPolar = new CellStringLookupEncodedProperty();

        /// <summary>
        /// Висота антени
        /// </summary>
        private CellStringComboboxAplyProperty CellHighAntenn = new CellStringComboboxAplyProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellStringProperty CellMaxKoefG = new CellStringProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellStringProperty CellMaxKoefV = new CellStringProperty();

        /// <summary>
        /// ЕВП Г
        /// </summary>
        private CellStringProperty CellEVPG = new CellStringProperty();
        /// <summary>
        /// ЕВП В
        /// </summary>
        private CellStringProperty CellEVPV = new CellStringProperty();
        /// <summary>
        /// ЕВП Макс
        /// </summary>
        private CellStringProperty CellEVPm = new CellStringProperty();

        /// <summary>
        /// Позивний сигнал
        /// </summary>
        private CellStringComboboxAplyProperty CellPositiv = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringProperty CellInnerState = new CellStringProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringProperty CellOuterState = new CellStringProperty();

        /// <summary>
        /// Центральна частота
        /// </summary>
        private CellStringComboboxAplyProperty CellMaxFreq = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();
        #endregion

        public FmAnalogAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, FmAnalogApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppR2;
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);
            fmStation = new FmAnalogStation();
            fmStation.Id = recordID.Id;
            fmStation.Load();

            fmDiffStation = new FmAnalogDiffStation();
            fmDiffStation.Id = recordID.Id;
            fmDiffStation.Load();
            Comment = fmDiffStation.StatComment;

            //originalPolarization = fmStation.Polarization;
            //originalDirection = fmStation.Direction;
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {         
            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, fmStation.Bandwith.DivNullD(1000.0).ToStringNullD(), fmDiffStation.Bandwith.DivNullD(1000.0).ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, fmStation.ClassEmission, fmDiffStation.ClassEmission);

            //Поменять поведение, если сертификату введен прочерк. 
            if (fmDiffStation.Equipment.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, fmStation.Equipment.Certificate.Symbol, fmDiffStation.Equipment.Certificate.Symbol);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, fmStation.Equipment.Certificate.Date.ToShortDateStringNullT(), Convert.ToDateTime(fmDiffStation.Equipment.Certificate.Date.ToShortDateString()).ToShortDateString());
        }

        /// <summary>
        /// Считывание данных формы при формировании Позиции
        /// </summary>
        private void ReadAllDataPositions()
        {
            CellEquipmentName.Value = fmStation.Equipment.Name;

            CellCertificateNumber.Value = fmStation.Equipment.Certificate.Symbol;
            CellCertificateDate.Value = fmStation.Equipment.Certificate.Date.ToShortDateStringNullT();

            if (!fmStation.InternalStateDict.ContainsKey(fmStation.InnerState))
                fmStation.InnerState = fmStation.InternalStateDict.ToList()[0].Key;
            if (!string.IsNullOrEmpty(fmStation.AGL.ToStringNullD()))
            {
                CellHighAntenn.Value = fmStation.AGL.ToStringNullD();
                CellHighAntenn.Items.Add(fmStation.AGL.ToStringNullD());
            }

            CellInnerState.Value = fmStation.InnerState + " - " + fmStation.InternalStateDict[fmStation.InnerState];

            if (!fmStation.ForeignStateDict.ContainsKey(fmStation.OuterState))
                fmStation.OuterState = fmStation.ForeignStateDict.ToList()[0].Key;
            CellOuterState.Value = fmStation.OuterState + " - " + fmStation.ForeignStateDict[fmStation.OuterState];

            CellMaxFreq.Value = fmStation.CenterFreq.ToStringNullD();
            CellMaxFreq.Items.Add(fmStation.CenterFreq.ToStringNullD());
            CellObjRCHP.IntValue = fmStation.Id;
            CellHighAntenn.Value = fmStation.AGL.ToStringNullD();
            CellMaxHigh.DoubleValue = fmStation.HeightMax;
            CellFiderLength.DoubleValue = fmStation.FeederLength;
            CellFiderlost.DoubleValue = fmStation.FeederLosses;
            CellEquipmentNameNumber.Value = fmStation.NumberRez;
            CellEquipmentNameREZ.Value = fmStation.NameRez;
            CellGive.Value = fmStation.SystSendList[fmStation.TransmissionSyst];
            CellPositiv.Value = fmStation.PositivSignal;
            if (!string.IsNullOrEmpty(fmStation.PositivSignal))
                CellPositiv.Items.Add(fmStation.PositivSignal);          
        }

        public override void UpdateToNewPacket() { }

        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(fmStation.Position.TableName, fmStation.Position.Id);
                rcPtr.UserEdit();
            }
        }

        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "FM_STATION" };
            PositionState diffPos = fmDiffStation.Position.Clone() as PositionState;
            AttachmentControl ac = new AttachmentControl();

            selectedPosition = coordHelper.GetSelectedPosition(fmStation.Position, fmDiffStation.Position);
            if (selectedPosition != fmStation.Position)
                SaveNewPosition(selectedPosition, fmStation.Position, assgnReferenceTables);

            fmStation.TransmissionSyst = fmStation.SystSendList.IndexOf(CellGive.Value);
            fmStation.Position = selectedPosition;

            fmStation.AGL = CellHighAntenn.Value.ToDouble(IM.NullD);

            fmStation.ClassEmission = CellTxDesignEmission.Value;

            fmStation.PositivSignal = CellPositiv.Value;
            fmStation.Bandwith = CellHighAntenn.Value.ToDouble(IM.NullD);

            fmStation.CenterFreq = CellMaxFreq.Value.ToDouble(IM.NullD);
            fmStation.InnerState = CellInnerState.Value.Split(' ')[0].Trim();
            fmStation.OuterState = CellOuterState.Value.Split(' ')[0].Trim();
            fmStation.ApplId = ApplID;
            fmStation.StatComment = fmDiffStation.StatComment;
            fmStation.Save();

            fmDiffStation.Remove();

            ac.RelocateAndCopyDocLinks(
              diffPos.TableName,
              diffPos.Id,
              FmAnalogApp.TableName,
              fmStation.Id,
              fmStation.Position.TableName,
              fmStation.Position.Id,
              ICSMTbl.itblAdmSite,
              fmStation.Position.AdminSitesId);

            //gridParam.Enabled = false;

            return true;
        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;

            ReadAllDataPositions();

            CellPowerTr.Initialize(fmStation.Power[PowerUnits.W].Round(3), fmDiffStation.Equipment.MaxPower[PowerUnits.W].Round(3));

            CellCertificateNumber.Value = fmDiffStation.Equipment.Certificate.Symbol;
            CellCertificateDate.Value = fmDiffStation.Equipment.Certificate.Date.ToShortDateString();
          
            FillDiffComboBox(CellHighAntenn, fmStation.AGL.ToStringNullD(), fmDiffStation.AGL.ToStringNullD());
            
            FillDiffComboBox(CellMaxFreq, fmStation.CenterFreq.ToStringNullD(), fmDiffStation.CenterFreq.ToStringNullD());
            
            FillDiffComboBox(CellPositiv,fmStation.PositivSignal,fmDiffStation.Positiv);            

            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenPolar, fmStation.Polarization, fmDiffStation.Polarization);
            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenDirect, fmStation.Direction, fmDiffStation.Direction);

            coordHelper.Initialize(fmDiffStation.Position, fmStation.Position);         

            CreateEquipmentComboBoxes();
            PaintGrid();
            initialized = true;
            OnRecalculateErp(null);
        }

        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Cell cell) { }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        public void OnBeforeChangePolarization(Cell cell, string newVal)
        {
            CellAntenPolar.Value2 = newVal;
            PaintGrid();
        }

        public void OnAfterChangeFreq(Cell cell)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            r.Select("ID,COD,LOWER_FREQ,UPPER_FREQ");
            int PlanID = objStation.GetI("PLAN_ID");
            double LowerFreq = 0.0;
            double UpperFreq = 0.0;

            r.SetWhere("ID", IMRecordset.Operation.Eq, PlanID);
            r.Open();
            if (!r.IsEOF())
            {
                LowerFreq = r.GetD("LOWER_FREQ");
                UpperFreq = r.GetD("UPPER_FREQ");
            }
            if (r.IsOpen())
                r.Close();
            r.Destroy();

            double EnteredFreq = cell.Value.ToDouble(IM.NullD);            

            if (LowerFreq <= EnteredFreq && UpperFreq >= EnteredFreq)
                objStation.Put("FREQ", EnteredFreq);
            else
            {
                MessageBox.Show(cell.Value + " не вписується в частотний план ( " +
                                (LowerFreq).ToString("F3") + " - " + (UpperFreq).ToString("F3") + " )");
            }
        }

        public void OnBeforeChangeDirection(Cell cell, string newVal)
        {
            CellAntenDirect.Value2 = newVal;
            PaintGrid();
        }

        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                fmStation.Equipment.TableName = newEquip.Table;
                fmStation.Equipment.Id = newEquip.Id;
                fmStation.Equipment.Load();
               // fmStation.OnNewEquipment();

                InitEquipment();

                CellPowerTr.Initialize(fmStation.Power[PowerUnits.W].Round(3), fmDiffStation.Equipment.MaxPower[PowerUnits.W].Round(3));

                PaintGrid();
            }
        }

        public void PaintGrid()
        {
            coordHelper.Paint(fmDiffStation.Position, fmStation.Position);
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = fmStation.Equipment.Name;
            CellTxBandwidth.Value = fmStation.Bandwith.DivNullD(1000.0).ToStringNullD();
            CellTxDesignEmission.Value = fmStation.ClassEmission;

            CellCertificateNumber.Value = fmStation.Equipment.Certificate.Symbol;
            CellCertificateDate.Value = fmStation.Equipment.Certificate.Date.ToShortDateString();
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipBro, param);
            return RecEquip;
        }

        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            PaintGrid();            
        }


        //===========================================================
        /// <summary>
        /// GET XML
        /// </summary>
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.FmAnalogAppCommit;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (initialized)
            {
                fmStation.Power[PowerUnits.W] = CellPowerTr.LookupedDoubleValue;
                fmStation.Polarization = CellAntenPolar.Value;
                fmStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!fmStation.HorizontalDiagramm.IsEmpty && fmStation.Polarization != "V")
                value = fmStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!fmStation.VerticalDiagramm.IsEmpty && fmStation.Polarization != "H")
                value = fmStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            fmStation.CalculateERP();
            if (fmStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = fmStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (fmStation.Erp.Vertical.IsValid)
                CellEVPV.Value = fmStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = fmStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}
