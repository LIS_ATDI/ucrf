﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStationAppCommit : BaseCommitClass
    {

        private int ObjID = IM.NullI;

        private MobStation mobStation;
        private MobDiffStation mobDiffStation;

        private StationSectorCollection<MobStation> _mobStations;
        private StationSectorCollection<MobDiffStation> _mobDiffStations;

        //private int IDDiff = IM.NullI;
        //private int IDPos = IM.NullI;

        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();
        private CellDoubleProperty CellASL = new CellDoubleProperty();
        private CellStringProperty CellAddress = new CellStringProperty();
        private CellStringProperty CellEquipmentName = new CellStringProperty();
        //private CellDoubleProperty CellPowerTr = new CellDoubleProperty();
        private CellStringComboboxAplyProperty CellStandart = new CellStringComboboxAplyProperty();        /// <summary>
        private CellStringProperty CellRez = new CellStringProperty();        /// <summary>
        //private CellDoubleProperty CellBaseType = new CellDoubleProperty();
        private CellStringComboboxAplyProperty CellTransmitterCount = new CellStringComboboxAplyProperty();
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        private CellStringProperty CellFilterType1 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower1 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType1 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight1 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission1 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef1 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth1 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle1 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation1 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber1 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency1 = new CellStringProperty();
        private CellStringProperty CellTxFrequency1 = new CellStringProperty();
        private CellIntegerProperty CellObject1 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff1 = new CellStringComboboxAplyProperty();

        private CellStringProperty CellFilterType2 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower2 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType2 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight2 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission2 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef2 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth2 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle2 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation2 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber2 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency2 = new CellStringProperty();
        private CellStringProperty CellTxFrequency2 = new CellStringProperty();
        private CellIntegerProperty CellObject2 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff2 = new CellStringComboboxAplyProperty();

        private CellStringProperty CellFilterType3 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower3 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType3 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight3 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission3 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef3 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth3 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle3 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation3 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber3 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency3 = new CellStringProperty();
        private CellStringProperty CellTxFrequency3 = new CellStringProperty();
        private CellIntegerProperty CellObject3 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff3 = new CellStringComboboxAplyProperty();

        private CellStringProperty CellFilterType4 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower4 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType4 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight4 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission4 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef4 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth4 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle4 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation4 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber4 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency4 = new CellStringProperty();
        private CellStringProperty CellTxFrequency4 = new CellStringProperty();
        private CellIntegerProperty CellObject4 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff4 = new CellStringComboboxAplyProperty();

        private CellStringProperty CellFilterType5 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower5 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType5 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight5 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission5 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef5 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth5 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle5 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation5 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber5 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency5 = new CellStringProperty();
        private CellStringProperty CellTxFrequency5 = new CellStringProperty();
        private CellIntegerProperty CellObject5 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff5 = new CellStringComboboxAplyProperty();

        private CellStringProperty CellFilterType6 = new CellStringProperty();
        private CellDoubleLookupProperty CellPower6 = new CellDoubleLookupProperty();
        private CellStringProperty CellAntennaType6 = new CellStringProperty();
        private CellDoubleLookupProperty CellAntenaHeight6 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellEmission6 = new CellDoubleLookupProperty();
        private CellStringBrowsableProperty CellAntenaKoef6 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellAntenaWidth6 = new CellStringBrowsableProperty();
        private CellDoubleProperty CellAntenaAngle6 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation6 = new CellDoubleProperty();
        private CellDoubleProperty CellChannelNumber6 = new CellDoubleProperty();
        private CellStringProperty CellRxFrequency6 = new CellStringProperty();
        private CellStringProperty CellTxFrequency6 = new CellStringProperty();
        private CellIntegerProperty CellObject6 = new CellIntegerProperty();
        private CellStringComboboxAplyProperty CellOnOff6 = new CellStringComboboxAplyProperty();

        private CellStringProperty[] CellFilterType;
        private CellDoubleLookupProperty[] CellPower;
        private CellStringProperty[] CellAntennaType;
        private CellDoubleLookupProperty[] CellAntenaHeight;
        private CellDoubleLookupProperty[] CellEmission;
        private CellStringBrowsableProperty[] CellAntenaKoef;
        private CellStringBrowsableProperty[] CellAntenaWidth;
        private CellDoubleProperty[] CellAntenaAngle;
        private CellDoubleProperty[] CellAntenaAttenuation;
        private CellDoubleProperty[] CellChannelNumber;
        private CellStringProperty[] CellRxFrequency;
        private CellStringProperty[] CellTxFrequency;
        private CellIntegerProperty[] CellObject;
        private CellStringProperty[] CellOnOff;

        private CoordinateCommitHelper _coordinateHelper;

        public MobStationAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobStaApp.TableName, ownerId, packetID, radioTech)
        {
            _coordinateHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);

            CellFilterType = new CellStringProperty[6]
                               {
                                  CellFilterType1,
                                  CellFilterType2,
                                  CellFilterType3,
                                  CellFilterType4,
                                  CellFilterType5,
                                  CellFilterType6
                               };
            CellPower = new CellDoubleLookupProperty[6]
                          {
                             CellPower1,
                             CellPower2,
                             CellPower3,
                             CellPower4,
                             CellPower5,
                             CellPower6
                          };


            CellAntennaType = new CellStringProperty[6]
                               {
                                  CellAntennaType1,
                                  CellAntennaType2,
                                  CellAntennaType3,
                                  CellAntennaType4,
                                  CellAntennaType5,
                                  CellAntennaType6
                              };

            CellAntenaHeight = new CellDoubleLookupProperty[6]
                                      {
                                         CellAntenaHeight1,
                                         CellAntenaHeight2,
                                         CellAntenaHeight3,
                                         CellAntenaHeight4,
                                         CellAntenaHeight5,
                                         CellAntenaHeight6
                               };

            CellEmission = new CellDoubleLookupProperty[6]
                          {
                             CellEmission1,
                             CellEmission2,
                             CellEmission3,
                             CellEmission4,
                             CellEmission5,
                             CellEmission6                             
                           };
            CellAntenaKoef = new CellStringBrowsableProperty[6]
                            {
                               CellAntenaKoef1,
                               CellAntenaKoef2,
                               CellAntenaKoef3,
                               CellAntenaKoef4,
                               CellAntenaKoef5,
                               CellAntenaKoef6
                               
        };
            CellAntenaWidth = new CellStringBrowsableProperty[6]
                             {
                                CellAntenaWidth1,
                                CellAntenaWidth2,
                                CellAntenaWidth3,
                                CellAntenaWidth4,
                                CellAntenaWidth5,
                                CellAntenaWidth6
                              };
            CellAntenaAngle = new CellDoubleProperty[6]
                             {
                                CellAntenaAngle1,
                                CellAntenaAngle2,
                                CellAntenaAngle3,
                                CellAntenaAngle4,
                                CellAntenaAngle5,
                                CellAntenaAngle6
                              };
            CellAntenaAttenuation = new CellDoubleProperty[6]
                                   {
                                      CellAntenaAttenuation1,
                                      CellAntenaAttenuation2,
                                      CellAntenaAttenuation3,
                                      CellAntenaAttenuation4,
                                      CellAntenaAttenuation5,
                                      CellAntenaAttenuation6
        };
            CellChannelNumber = new CellDoubleProperty[6]
                               {
         CellChannelNumber1,
         CellChannelNumber2,
         CellChannelNumber3,
         CellChannelNumber4,
         CellChannelNumber5,
         CellChannelNumber6
        };
            CellRxFrequency = new CellStringProperty[6]
                            {
                               CellRxFrequency1,
                               CellRxFrequency2,
                               CellRxFrequency3,
                               CellRxFrequency4,
                               CellRxFrequency5,
                               CellRxFrequency6
                            };
            CellTxFrequency = new CellStringProperty[6]
                          {
                             CellTxFrequency1,
                             CellTxFrequency2,
                             CellTxFrequency3,
                             CellTxFrequency4,
                             CellTxFrequency5,
                             CellTxFrequency6
                          };
            CellObject = new CellIntegerProperty[6]
                           {
                              CellObject1,
                              CellObject2,
                              CellObject3,
                              CellObject4,
                              CellObject5,
                              CellObject6
                           };

            CellOnOff = new CellStringProperty[6]
                           {
                              CellOnOff1,
                              CellOnOff2,
                              CellOnOff3,
                              CellOnOff4,
                              CellOnOff5,
                              CellOnOff6
                           };

            //Стандартные значения
            _mobStations = new StationSectorCollection<MobStation>();
            _mobStations.ApplID = applID;
            _mobStations.LoadSectorsOrSingle(id);
            mobStation = _mobStations.GetSectorByIndex(0);

            _mobDiffStations = new StationSectorCollection<MobDiffStation>();
            _mobDiffStations.ApplID = applID;
            _mobDiffStations.LoadSectorsOrSingle(id);
            mobDiffStation = _mobDiffStations.GetSectorByIndex(0);

            /*for(int i=0; i<CellOnOff.Length; i++)
            {
                CellOnOff[i].Cell
            }*/

        }
    
        public override void UpdateToNewPacket()
        {
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "addr1-New")
            {
                RecordPtr rcPtr = new RecordPtr(mobStation.Position.TableName, mobStation.Position.Id);
                rcPtr.UserEdit();
            }
        }

        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "MOB_STATION" };
            
            PositionState diffPos = mobDiffStation.Position.Clone() as PositionState;            
            AttachmentControl ac = new AttachmentControl();

            StationSectorCollection<MobStation> savedStations = new StationSectorCollection<MobStation>();
            savedStations.ApplID = applID;

            PositionState position = mobStation.Position;
            if (CellAddress.Cell.cellStyle == EditStyle.esPickList && CellAddress.Value == "2")
            {
                position = mobDiffStation.Position;
                SaveNewPosition(position, mobStation.Position, assgnReferenceTables);
            }

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(i);
                MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);

                bool DontRemoveSector = true; //mobDiffStation.OnOff==1 || CellOnOff[i].Pointer == 0
                // Возможно, когда-то придеся удалять сектор.

                if (DontRemoveSector)
                {
                    mobSector.Position = position;

                    mobSector.Equipment.Id = mobStation.Equipment.Id;

                    if (CellTransmitterCount.Cell.cellStyle == EditStyle.esPickList && CellTransmitterCount.Pointer == 1)
                    {
                        mobSector.TransmitterCount = mobDiffStation.TransmitterCount;
                    }

                    mobSector.TxDesignEmission = mobStation.Equipment.DesigEmission;

                    if (CellPower[i].Cell.cellStyle == EditStyle.esPickList)
                    {
                        mobSector.Power[PowerUnits.W] = CellPower[i].LookupedDoubleValue;
                    }

                    if (CellEmission[i].Cell.cellStyle == EditStyle.esPickList)
                    {
                        mobSector.Azimuth = CellEmission[i].LookupedDoubleValue;
                    }

                    if (CellAntenaHeight[i].Cell.cellStyle == EditStyle.esPickList)
                    {
                        mobSector.Agl = CellAntenaHeight[i].LookupedDoubleValue;
                    }

                    mobSector.Standard = mobStation.Standard;
                    mobSector.Rez = mobStation.Rez;
                    mobSector.TxLosses = mobStation.TxLosses;
                    mobSector.Elevation = mobStation.Elevation;
                    mobSector.ApplId = ApplID;
                    mobSector.StatComment = mobDiffStation.StatComment;
                    savedStations.Add(mobSector);
                }
                else
                {
                    mobSector.SetDeletedStatus();
                }
            }

            savedStations.SaveSectorsOrSingle(true);

            sectorCount = _mobDiffStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);
                mobDiffSector.Remove();
            }

            PositionState firstPosition = savedStations.GetSectorByIndex(0).Position;

            ac.RelocateAndCopyDocLinks(
               diffPos.TableName,
               diffPos.Id,
               MobStaApp.TableName,
               savedStations.GetIdList(),
               firstPosition.TableName,
               firstPosition.Id,
               ICSMTbl.itblAdmSite,
               firstPosition.AdminSitesId);

            return true;
        }

        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public override void InitParamGrid(GridCtrl.Cell cell)
        {
        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < 6; i++)
            {
                if (i >= sectorCount)
                {
                    CellFilterType[i].Cell.Visible = false;
                    CellPower[i].Cell.Visible = false;
                    CellAntennaType[i].Cell.Visible = false;
                    CellAntenaHeight[i].Cell.Visible = false;
                    CellEmission[i].Cell.Visible = false;
                    CellAntenaKoef[i].Cell.Visible = false;
                    CellAntenaWidth[i].Cell.Visible = false;
                    CellAntenaAngle[i].Cell.Visible = false;
                    CellAntenaAttenuation[i].Cell.Visible = false;
                    CellChannelNumber[i].Cell.Visible = false;
                    CellRxFrequency[i].Cell.Visible = false;
                    CellTxFrequency[i].Cell.Visible = false;
                    CellObject[i].Cell.Visible = false;
                    CellOnOff[i].Cell.Visible = false;

                    grid.GetCellFromKey("SectorHeader-" + (i + 1).ToString()).Visible = false;
                }
            }

            //CellTransmitterCount.NullString = "Згідно комплектації базової станції";
            InitStationObject();
            InitEquipment();
            CreateComboBoxes();
        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStationAppCommit;
        }

        public void InitStationObject()
        {
            //CellTransmitterCount.DoubleValue = mobStation.TransmitterCount;
            CellTransmitterCount.CanEdit = false;
            //CellTransmitterCount.DoubleValue2 = mobStation.TransmitterCount;

            CellStandart.Value = mobStation.Plan.Standard;
            CellRez.Value = mobStation.Name;

            CellStandart.CanEdit = false;
            CellRez.CanEdit = false;
            CellASL.CanEdit = false;

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(i);

                CellFilterType[i].Value = mobSector.Filter.Name;
                CellFilterType[i].Value2 = mobSector.Filter.Name;
                CellFilterType[i].CanEdit = false;

                CellPower[i].DoubleValue = IM.RoundDeci(mobSector.Power[PowerUnits.W], 3);
                //CellPower[i].DoubleValue2 = IM.RoundDeci(mobSector.Power[PowerUnits.W], 3);
                CellPower[i].CanEdit = false;

                CellAntennaType[i].Value = mobSector.Antenna.Name;
                //CellAntennaType[i].Value2 = mobSector.Antenna.Name;
                CellAntennaType[i].CanEdit = false;

                CellEmission[i].DoubleValue = mobSector.Azimuth;
                //CellEmission[i].DoubleValue2 = mobSector.Azimuth;
                CellEmission[i].CanEdit = false;

                CellAntenaAngle[i].DoubleValue = mobSector.Elevation;
                //CellAntenaAngle[i].DoubleValue2 = mobSector.Elevation;
                CellAntenaAngle[i].CanEdit = false;

                CellAntenaHeight[i].DoubleValue = mobSector.Agl;
                //CellAntenaHeight[i].DoubleValue2 = mobSector.Agl;
                CellAntenaHeight[i].CanEdit = false;

                //CellAntenaKoef[i].DoubleValue = mobSector.Gain;
                //CellAntenaKoef[i].DoubleValue2 = mobSector.Gain;
                InitAntenna(i);
                CellAntenaKoef[i].CanEdit = false;

                //CellAntenaWidth[i].DoubleValue = mobSector.Antenna.AntennaWidth;
                //CellAntenaWidth[i].DoubleValue2 = mobSector.Antenna.AntennaWidth;
                CellAntenaWidth[i].CanEdit = false;

                CellAntenaAttenuation[i].DoubleValue = mobSector.TxLosses;
                CellAntenaAttenuation[i].DoubleValue2 = mobSector.TxLosses;
                CellAntenaAttenuation[i].CanEdit = false;
                //CellAntenaAttenuation[i].CanEdit = false;

                CellChannelNumber[i].Value = mobSector.Plan.ChannelListAsString;
                CellChannelNumber[i].Value2 = mobSector.Plan.ChannelListAsString;
                CellChannelNumber[i].CanEdit = false;

                CellRxFrequency[i].Value = mobSector.Plan.RxFrequencyListAsString;
                CellRxFrequency[i].Value2 = mobSector.Plan.RxFrequencyListAsString;
                CellRxFrequency[i].CanEdit = false;

                CellTxFrequency[i].Value = mobSector.Plan.TxFrequencyListAsString;
                CellTxFrequency[i].Value2 = mobSector.Plan.TxFrequencyListAsString;
                CellTxFrequency[i].CanEdit = false;

                CellObject[i].IntValue = mobSector.Id;
                CellObject[i].CanEdit = false;

                //CellOnOff[i].Value = "Сектор увімкнено";
                CellOnOff[i].CanEdit = false;
            }

            /*
            if (mobDiffStation != null && mobDiffStation.IsInitialized)
            {
               CellLongitude.DoubleValue = mobDiffStation.Position.LonDms;
               CellLatitude.DoubleValue = mobDiffStation.Position.LatDms;
               CellASL.DoubleValue = mobDiffStation.Position.ASl;
               CellAddress.Value = mobDiffStation.Position.FullAddress;

               CellEquipmentName.Value = mobDiffStation.Equipment.Name;
               CellTxDesignEmission.Value = mobDiffStation.DesigEmission;
               CellCertificateNumber.Value = mobDiffStation.Equipment.Certificate.Symbol;
               CellCertificateDate.DateValue = mobDiffStation.Equipment.Certificate.Date;

               CellTransmitterCount.DoubleValue = mobDiffStation.TransmitterCount;

               for (int i = 0; i < sectorCount; i++)
               {
                  MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);

                  CellPower[i].DoubleValue = IM.RoundDeci(mobDiffSector.Power[PowerUnits.W], 3);
                  CellAntennaType[i].Value = mobDiffSector.Antenna.Name;
                  CellEmission[i].DoubleValue = mobDiffSector.Azimuth;
                  //CellAntenaAngle[i].DoubleValue = mobDiffSector.Elevation;
                  CellAntenaHeight[i].DoubleValue = mobDiffSector.Agl;
                  CellAntenaKoef[i].DoubleValue = mobDiffSector.Gain;
                  CellAntenaWidth[i].DoubleValue = mobDiffSector.Antenna.AntennaWidth;
                  //CellAntenaAttenuation[i].DoubleValue = mobDiffSector.TxLosses;                                                         
               }
            }*/
        }

        private void CreateComboBoxes()
        {
            _coordinateHelper.Initialize(mobDiffStation.Position, mobStation.Position);

            if (mobStation.TransmitterCount != mobDiffStation.TransmitterCount)
            {
                CellTransmitterCount.Cell.cellStyle = EditStyle.esPickList;
                CellTransmitterCount.Items.Clear();

                CellTransmitterCount.Items.Add(mobStation.TransmitterCount.ToStringNullD("Згідно комплектації базової станції"));
                CellTransmitterCount.Items.Add(mobDiffStation.TransmitterCount.ToStringNullD("Згідно комплектації базової станції"));
                CellTransmitterCount.Value = CellTransmitterCount.Items[0];
            }
            else
            {
                CellTransmitterCount.Cell.cellStyle = EditStyle.esSimple;
                CellTransmitterCount.Value = mobStation.TransmitterCount.ToStringNullD("Згідно комплектації базової станції");
            }

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(i);
                MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);
                Comment = mobDiffSector.StatComment;
                if (IM.RoundDeci(mobSector.Power[PowerUnits.W], 3) != IM.RoundDeci(mobDiffSector.Power[PowerUnits.W], 3))
                {
                    CellPower[i].Items.Clear();
                    CellPower[i].Items.Add(IM.RoundDeci(mobSector.Power[PowerUnits.W], 3));
                    CellPower[i].Items.Add(IM.RoundDeci(mobDiffSector.Power[PowerUnits.W], 3));
                    CellPower[i].Initialize();
                    CellPower[i].Value = "1";
                }

                if (mobSector.Azimuth != mobDiffSector.Azimuth)
                {
                    CellEmission[i].Items.Clear();
                    CellEmission[i].Items.Add(IM.RoundDeci(mobSector.Azimuth, 2));
                    CellEmission[i].Items.Add(IM.RoundDeci(mobDiffSector.Azimuth, 2));
                    CellEmission[i].Initialize();
                    CellEmission[i].Value = "1";
                }

                if (mobSector.Agl != mobDiffSector.Agl)
                {
                    CellAntenaHeight[i].Items.Clear();
                    CellAntenaHeight[i].Items.Add(IM.RoundDeci(mobSector.Agl, 0));
                    CellAntenaHeight[i].Items.Add(IM.RoundDeci(mobDiffSector.Agl, 0));
                    CellAntenaHeight[i].Initialize();
                    CellAntenaHeight[i].Value = "1";
                }

                string sector_on = CellOnOff[i].Cell.KeyPickList.GetValueByKey("1");

                if (!HelpFunction.AreSameDoubleLists(mobSector.Plan.TxFrequency, mobDiffSector.Plan.TxFrequency))
                {
                    CellTxFrequency[i].Cell.cellStyle = EditStyle.esPickList;
                    KeyedPickList newKeyedPickList = new KeyedPickList();
                    newKeyedPickList.AddToPickList("1", mobStation.Plan.TxFrequencyListAsString);
                    newKeyedPickList.AddToPickList("2", mobDiffStation.Plan.TxFrequencyListAsString);
                    CellTxFrequency[i].Cell.KeyPickList = newKeyedPickList;
                    CellTxFrequency[i].Value = "1";
                    CellTxFrequency[i].Value2 = "1";

                }
                else
                {
                    CellTxFrequency[i].Value = mobStation.Plan.TxFrequencyListAsString;
                    CellTxFrequency[i].Value2 = mobStation.Plan.TxFrequencyListAsString;
                }

                if (!HelpFunction.AreSameDoubleLists(mobSector.Plan.RxFrequency, mobDiffSector.Plan.RxFrequency))
                {
                    CellRxFrequency[i].Cell.cellStyle = EditStyle.esPickList;
                    KeyedPickList newKeyedPickList = new KeyedPickList();
                    newKeyedPickList.AddToPickList("1", mobStation.Plan.RxFrequencyListAsString);
                    newKeyedPickList.AddToPickList("2", mobDiffStation.Plan.RxFrequencyListAsString);
                    CellRxFrequency[i].Cell.KeyPickList = newKeyedPickList;
                    CellRxFrequency[i].Value = "1";
                    CellRxFrequency[i].Value2 = "1";
                }
                else
                {
                    CellRxFrequency[i].Value = mobStation.Plan.RxFrequencyListAsString;
                    CellRxFrequency[i].Value2 = mobStation.Plan.RxFrequencyListAsString;
                }

                CellOnOff[i].Value = sector_on;
                if (mobDiffSector.OnOff == 0)
                    CellOnOff[i].BackColor = CellStringProperty.ColorBgr(0);
            }
        }

        public void OnBeforeChangeTxFrequency(Cell cell, ref string NewValue)
        {
            int sectorIndex = Convert.ToInt32(cell.Key.Replace("TxFrequency", "")) - 1;
            CellTxFrequency[sectorIndex].Value2 = NewValue;
            PaintGrid();
        }

        public void OnBeforeChangeRxFrequency(Cell cell, ref string NewValue)
        {
            int sectorIndex = Convert.ToInt32(cell.Key.Replace("RxFrequency", "")) - 1;
            CellRxFrequency[sectorIndex].Value2 = NewValue;
            PaintGrid();
        }

        public void PaintGrid()
        {
            _coordinateHelper.Paint(mobDiffStation.Position, mobStation.Position);

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(i);
                MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);

                if (!HelpFunction.AreSameDoubleLists(mobStation.Plan.TxFrequency, mobDiffSector.Plan.TxFrequency))
                {
                    //if (CellTxFrequency[i].Value2 == "1")
                    CellTxFrequency[i].BackColor = CellStringProperty.ColorBgr(2);
                    //else
                    //    CellTxFrequency[i].BackColor = CellStringProperty.ColorBgr(0);

                }
                else
                {
                    CellTxFrequency[i].BackColor = CellStringProperty.DefaultBgr();
                }

                if (!HelpFunction.AreSameDoubleLists(mobSector.Plan.RxFrequency, mobDiffSector.Plan.RxFrequency))
                {
                    //if (CellRxFrequency[i].Value2 == "1")
                    CellRxFrequency[i].BackColor = CellStringProperty.ColorBgr(2);
                    //else
                    //    CellRxFrequency[i].BackColor = CellStringProperty.ColorBgr(0);

                }
                else
                {
                    CellRxFrequency[i].BackColor = CellStringProperty.DefaultBgr();
                }
            }
        }

        public void OnBeforeChangeAddr(Cell cell, ref string NewValue)
        {
            CellAddress.Value2 = NewValue;
            PaintGrid();
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = mobStation.Equipment.Name;

            CellTxDesignEmission.Value = mobStation.DesigEmission;

            CellCertificateNumber.Value = mobStation.Equipment.Certificate.Symbol;
            CellCertificateDate.Value = mobStation.Equipment.Certificate.Date.ToShortDateString();
        }

        private void CreateEquipmentComboBoxes()
        {
            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission,
                                                       mobStation.DesigEmission,
                                                       mobDiffStation.DesigEmission);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber,
                                                        mobStation.Equipment.Certificate.Symbol,
                                                        mobDiffStation.Equipment.Certificate.Symbol);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate,
                                                        mobStation.Equipment.Certificate.Date.ToShortDateString(),
                                                        mobDiffStation.Equipment.Certificate.Date.ToShortDateString());
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Підбір обладнання", ICSMTbl.itblEquipPmr, "NAME", initialValue, true);
            recEquip.Table = ICSMTbl.itblEquipPmr;  //Подмена таблицы
            return recEquip;
        }

        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntennaMob, param);
            return RecAnt;
        }

        /// <summary>
        /// On press button equipment
        ///   Set new Equipment if last one is selected
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                mobStation.Equipment.TableName = newEquip.Table;
                mobStation.Equipment.Id = newEquip.Id;
                mobStation.Equipment.Load();

                mobStation.DesigEmission = mobStation.Equipment.DesigEmission;

                InitEquipment();

                PaintGrid();
            }
        }

        public void OnBeforeChangeTransmitterCount(Cell cell, ref string NewValue)
        {
            CellTransmitterCount.Value2 = NewValue;
            PaintGrid();
        }

        public void OnPressButtonAntennaName(Cell cell)
        {
            int index = Convert.ToInt32(cell.Key.Substring(7));

            RecordPtr newAntenna = LookupAntenna(cell.Value, true);

            if ((newAntenna.Id > 0) && (newAntenna.Id < IM.NullI))
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(index - 1);

                mobSector.Antenna.TableName = newAntenna.Table;
                mobSector.Antenna.Id = newAntenna.Id;
                mobSector.Antenna.Load();

                InitAntenna(index - 1);

                PaintGrid();
            }
        }

        public void InitAntenna(int sectorIndex)
        {
            MobStation mobSector = _mobStations.GetSectorByIndex(sectorIndex);
            MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(sectorIndex);

            CellAntennaType[sectorIndex].Value = mobSector.Antenna.Name;

            CellStringBrowsableProperty.InitalizeHelper(CellAntenaKoef[sectorIndex],
               mobSector.Antenna.Gain.ToStringNullD(),
               mobDiffSector.Antenna.Gain.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellAntenaWidth[sectorIndex],
               mobSector.Antenna.AntennaWidth.ToStringNullD(),
               mobDiffSector.Antenna.AntennaWidth.ToStringNullD());
        }
    }
}
