﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using GridCtrl;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class CoordinateDiffHelper
    {
        public static PositionState CheckPosition(CellLongitudeProperty cellLongitude,
           CellLatitudeProperty cellLatitude,           
           PositionState position,
           string nativePosTable)
        {
            PositionState newPosition = position.Clone() as PositionState;

            ///Если позиция неродная и XNRFA_POSITION
            if (newPosition.TableName != nativePosTable && newPosition.TableName != PlugTbl.itblXnrfaPositions)
            {
                newPosition.Id = IM.NullI;
                newPosition.TableName = PlugTbl.itblXnrfaPositions;
            }
                        
            ///Если поменялись координаты 
            if (cellLongitude.DoubleValue != cellLongitude.DoubleValue2 ||
                cellLatitude.DoubleValue != cellLatitude.DoubleValue2)
            {
                newPosition.LonDms = cellLongitude.DoubleValue;
                newPosition.LatDms = cellLatitude.DoubleValue;

                // для XNRFA_POSITION используем существующую, для остальных - новую.
                if (newPosition.TableName != PlugTbl.itblXnrfaPositions)
                {
                    newPosition.Id = IM.NullI;
                    newPosition.TableName = PlugTbl.itblXnrfaPositions;
                }
            }

            if (newPosition.TableName == PlugTbl.itblXnrfaPositions)
                newPosition.SaveToBD();

            return newPosition;
        }
    }
}
