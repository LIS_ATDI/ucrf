﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    public class MicrowaveAppCommit : BaseCommitClass
    {
        private const string positionTable = ICSMTbl.itblPositionMw;
        private PositionState position = new PositionState();

        private int ObjID = IM.NullI;

        private StationSectorCollection<MicrowaveLink> microwaveLinks;
        private StationSectorCollection<MicrowaveLinkDiff> microwaveLinksDiff;

        private MicrowaveLink srcMicrowaveLink;
        private MicrowaveLinkDiff dstMicrowaveLink;


        private CellDoubleProperty CellRange = new CellDoubleProperty();                  // Відстань
        private CellStringProperty CellEquipmentName = new CellStringProperty();          //  Назва/Тип РЕЗ      
        private CellStringBrowsableProperty CellBandwidth = new CellStringBrowsableProperty();              // Ширина смуги , МГц; 
        private CellStringBrowsableProperty CellDesigEmission = new CellStringBrowsableProperty();          // клас випромінювання                
        private CellStringBrowsableProperty CellModulation = new CellStringBrowsableProperty();             // модуляція
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();      // Номер сертифіката відповідності
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();        // Дата сертифіката відповідності
        private CellIntegerProperty CellObjectID = new CellIntegerProperty();             // Об'єкт

        private CellLongitudeProperty CellLongitude1 = new CellLongitudeProperty();       // Широта (гр.хв.сек.)
        private CellLatitudeProperty CellLatitude1 = new CellLatitudeProperty();          // Довгота (гр.хв.сек.)      
        private CellDoubleProperty CellASL1 = new CellDoubleProperty();                   // Висота на рівнем моря
        private CellDoubleLookupProperty CellAzimuth1 = new CellDoubleLookupProperty();   // Кут азимуту випромінювання
        //private CellStringComboboxAplyProperty CellAzimuth1 = new CellStringComboboxAplyProperty();    // Кут азимуту випромінювання

        private CellStringProperty CellAddress1 = new CellStringProperty();               // Адреса місця встановлення РЕЗ      
        private CellDoubleLookupProperty CellMaxPower1 = new CellDoubleLookupProperty();              // Максимальна потужність передавача              
        private CellStringProperty CellAntennaName1 = new CellStringProperty();           // Тип антени
        private CellDoubleLookupProperty CellAGL1 = new CellDoubleLookupProperty();                   // Висота антени над рівнем землі
        private CellStringBrowsableProperty CellDiameter1 = new CellStringBrowsableProperty();              // Діаметр
        private CellStringBrowsableProperty CellGain1 = new CellStringBrowsableProperty();                  // Коефіцієнт підсилення
        private CellStringProperty CellPolarization1 = new CellStringProperty();          // Поляризація
        private CellDoubleLookupProperty CellFrequency1 = new CellDoubleLookupProperty();             // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency1Sector2 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency1Sector3 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency1Sector4 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency1Sector5 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency1Sector6 = new CellDoubleLookupProperty();      // Номінал частот передавання

        private CellDoubleLookupProperty[] CellFrequency1Array;

        private CellLongitudeProperty CellLongitude2 = new CellLongitudeProperty();       // Широта (гр.хв.сек.)
        private CellLatitudeProperty CellLatitude2 = new CellLatitudeProperty();          // Довгота (гр.хв.сек.)      
        private CellDoubleProperty CellASL2 = new CellDoubleProperty();                   // Висота на рівнем моря
        private CellDoubleLookupProperty CellAzimuth2 = new CellDoubleLookupProperty();             // Кут азимуту випромінювання      

        private CellStringProperty CellAddress2 = new CellStringProperty();               // Адреса місця встановлення РЕЗ      
        private CellDoubleLookupProperty CellMaxPower2 = new CellDoubleLookupProperty();              // Максимальна потужність передавача              
        private CellStringProperty CellAntennaName2 = new CellStringProperty();           // Тип антени
        private CellDoubleLookupProperty CellAGL2 = new CellDoubleLookupProperty();                   // Висота антени над рівнем землі
        private CellStringBrowsableProperty CellDiameter2 = new CellStringBrowsableProperty();              // Діаметр
        private CellStringBrowsableProperty CellGain2 = new CellStringBrowsableProperty();                  // Коефіцієнт підсилення
        private CellStringProperty CellPolarization2 = new CellStringProperty();          // Поляризація
        private CellDoubleLookupProperty CellFrequency2 = new CellDoubleLookupProperty();             // Номінал частот передавання

        private CellDoubleLookupProperty CellFrequency2Sector2 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency2Sector3 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency2Sector4 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency2Sector5 = new CellDoubleLookupProperty();      // Номінал частот передавання
        private CellDoubleLookupProperty CellFrequency2Sector6 = new CellDoubleLookupProperty();      // Номінал частот передавання

        private CellDoubleLookupProperty[] CellFrequency2Array;

        private KeyedPickList _modulationList;

        private CoordinateCommitHelper _coordinateHelper1;
        private CoordinateCommitHelper _coordinateHelper2;


        private Grid objGrid;
        //===================================================
        public MicrowaveAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, MicrowaveApp.TableName, ownerId, packetID, radioTech)
        {
            ObjID = id;
            appType = AppType.AppRS;

            _coordinateHelper1 = new CoordinateCommitHelper(CellLongitude1, CellLatitude1, CellAddress1, CellASL1);
            _coordinateHelper2 = new CoordinateCommitHelper(CellLongitude2, CellLatitude2, CellAddress2, CellASL2);


            CellFrequency1Array = new CellDoubleLookupProperty[6]
                              {
                                 CellFrequency1,
                                 CellFrequency1Sector2,
                                 CellFrequency1Sector3,
                                 CellFrequency1Sector4,
                                 CellFrequency1Sector5,
                                 CellFrequency1Sector6
                              };

            CellFrequency2Array = new CellDoubleLookupProperty[6]
                              {
                                 CellFrequency2,
                                 CellFrequency2Sector2,
                                 CellFrequency2Sector3,
                                 CellFrequency2Sector4,
                                 CellFrequency2Sector5,
                                 CellFrequency2Sector6
                              };

            //Стандартные значения
            microwaveLinks = new StationSectorCollection<MicrowaveLink>();
            microwaveLinks.ApplID = applID;
            microwaveLinks.LoadSectorsOrSingle(id);
            srcMicrowaveLink = microwaveLinks.GetSectorByIndex(0);

            microwaveLinksDiff = new StationSectorCollection<MicrowaveLinkDiff>();
            microwaveLinksDiff.ApplID = applID;
            microwaveLinksDiff.LoadSectorsOrSingle(id);
            dstMicrowaveLink = microwaveLinksDiff.GetSectorByIndex(0);
        }
        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Cell cell)
        {
        }

        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            InitStationObject();
            objGrid = grid;
            //BindGrid(grid);         
            CellRange.FormatString = "N3";

            int SectorCount = microwaveLinks.GetSectorCount();
            if (SectorCount > 0)
            {
                CellObjectID.Cell.Visible = false;
                grid.GetCellFromKey("mKObj").Visible = false;
            }

            for (int i = SectorCount; i < 6; i++)
            {
                string cellKey1 = "kFreqNom" + i.ToString();
                grid.GetCellFromKey(cellKey1).row.Visible = false;
            }

            for (int i = 0; i < SectorCount; i++)
            {
                string cellKey1 = "kFreqNom" + i.ToString();
                Cell normCell = grid.GetCellFromKey(cellKey1);
                normCell.Value = normCell.Value + " (ID " + microwaveLinks.GetSectorByIndex(i).Id + ")";
            }

            CreateComboBoxes();
            PaintGrid();
        }

        private void CreateEquipmentComboBoxes()
        {
            //Disable advanced pick list for modulation
            CellModulation.Cell.KeyPickList = null;

            double srcBandwidth = srcMicrowaveLink.Equipment.Bandwidth == IM.NullD
                                      ? IM.NullD
                                      : srcMicrowaveLink.Equipment.Bandwidth.DivNullD(1000.0);

            double dstBandwidth = dstMicrowaveLink.Equipment.Bandwidth == IM.NullD
                                      ? IM.NullD
                                      : dstMicrowaveLink.Equipment.Bandwidth.DivNullD(1000.0);

            CellStringBrowsableProperty.InitalizeHelper(CellBandwidth,
                                                        srcBandwidth.ToStringNullD(),
                                                        dstBandwidth.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellDesigEmission,
                                                        srcMicrowaveLink.Equipment.DesigEmission,
                                                        dstMicrowaveLink.Equipment.DesigEmission);


            CellStringBrowsableProperty.InitalizeHelper(CellModulation,
                                                        _modulationList.GetValueByKey(
                                                            srcMicrowaveLink.Equipment.Modulation),
                                                        _modulationList.GetValueByKey(
                                                            dstMicrowaveLink.Equipment.Modulation));

            if (dstMicrowaveLink.Equipment.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber,
                                                       srcMicrowaveLink.Equipment.Certificate.Symbol,
                                                       dstMicrowaveLink.Equipment.Certificate.Symbol);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate,
                                                        srcMicrowaveLink.Equipment.Certificate.Date.ToShortDateStringNullT(),
                                                        dstMicrowaveLink.Equipment.Certificate.Date.ToShortDateStringNullT());
        }

        public void CreateAntennaComboBoxes(int stationIndex)
        {
            switch (stationIndex)
            {
                case 0:
                    CellStringBrowsableProperty.InitalizeHelper(CellDiameter1,
                                                                srcMicrowaveLink.Stations[0].Antenna.Diameter.ToStringNullD(),
                                                                dstMicrowaveLink.Stations[0].Antenna.Diameter.ToStringNullD());

                    CellStringBrowsableProperty.InitalizeHelper(CellGain1,
                                                                srcMicrowaveLink.Stations[0].Gain.ToStringNullD(),
                                                                dstMicrowaveLink.Stations[0].Gain.ToStringNullD());
                    break;
                case 1:
                    CellStringBrowsableProperty.InitalizeHelper(CellDiameter2,
                                                                srcMicrowaveLink.Stations[1].Antenna.Diameter.ToStringNullD(),
                                                                dstMicrowaveLink.Stations[1].Antenna.Diameter.ToStringNullD());

                    CellStringBrowsableProperty.InitalizeHelper(CellGain2,
                                                                srcMicrowaveLink.Stations[1].Gain.ToStringNullD(),
                                                                dstMicrowaveLink.Stations[1].Gain.ToStringNullD());
                    break;
                default:
                    break;
            }
        }

        public void CreateComboBoxesForPower()
        {
            CellMaxPower1.Initialize(
              srcMicrowaveLink.Stations[0].Power[PowerUnits.dBW],
              dstMicrowaveLink.Stations[0].Power[PowerUnits.dBW]);

            CellMaxPower2.Initialize(
              srcMicrowaveLink.Stations[1].Power[PowerUnits.dBW],
              dstMicrowaveLink.Stations[1].Power[PowerUnits.dBW]);
        }

        public void CreateComboBoxes()
        {
            _coordinateHelper1.Initialize(dstMicrowaveLink.Stations[0].Position, srcMicrowaveLink.Stations[0].Position);
            _coordinateHelper2.Initialize(dstMicrowaveLink.Stations[1].Position, srcMicrowaveLink.Stations[1].Position);

            CellAzimuth1.Initialize(srcMicrowaveLink.Stations[0].Azimuth, dstMicrowaveLink.Stations[0].Azimuth);
            CellAzimuth2.Initialize(srcMicrowaveLink.Stations[1].Azimuth, dstMicrowaveLink.Stations[1].Azimuth);

            CellAGL1.Initialize(srcMicrowaveLink.Stations[0].AGL, dstMicrowaveLink.Stations[0].AGL);
            CellAGL2.Initialize(srcMicrowaveLink.Stations[1].AGL, dstMicrowaveLink.Stations[1].AGL);

            CreateEquipmentComboBoxes();
            CreateAntennaComboBoxes(0);
            CreateAntennaComboBoxes(1);
            CreateComboBoxesForPower();

            Comment = dstMicrowaveLink.StatComment;
            if (srcMicrowaveLink.Stations[0].Polarization != dstMicrowaveLink.Stations[0].Polarization)
            {
                string p1 = srcMicrowaveLink.Stations[0].Polarization;
                string p2 = dstMicrowaveLink.Stations[0].Polarization;

                CellPolarization1.Cell.cellStyle = EditStyle.esPickList;
                KeyedPickList newKeyedPickList = new KeyedPickList();
                newKeyedPickList.AddToPickList(p1, CellPolarization1.Cell.KeyPickList.GetValueByKey(p1));
                newKeyedPickList.AddToPickList(p2, CellPolarization1.Cell.KeyPickList.GetValueByKey(p2));
                CellPolarization1.Cell.KeyPickList = newKeyedPickList;
                CellPolarization1.Cell.Value = p1;
                CellPolarization1.Cell.Value2 = p1;
            }

            if (srcMicrowaveLink.Stations[1].Polarization != dstMicrowaveLink.Stations[1].Polarization)
            {
                string p1 = srcMicrowaveLink.Stations[1].Polarization;
                string p2 = dstMicrowaveLink.Stations[1].Polarization;

                CellPolarization2.Cell.cellStyle = EditStyle.esPickList;
                KeyedPickList newKeyedPickList = new KeyedPickList();
                newKeyedPickList.AddToPickList(p1, CellPolarization2.Cell.KeyPickList.GetValueByKey(p1));
                newKeyedPickList.AddToPickList(p2, CellPolarization2.Cell.KeyPickList.GetValueByKey(p2));
                CellPolarization2.Cell.KeyPickList = newKeyedPickList;
                CellPolarization2.Cell.Value = p1;
                CellPolarization2.Cell.Value2 = p1;
            }

            int srcSectorCount = microwaveLinks.GetSectorCount();
            int dstSectorCount = microwaveLinksDiff.GetSectorCount();

            if (srcSectorCount == dstSectorCount)
            {
                for (int i = 0; i < srcSectorCount; i++)
                {
                    MicrowaveLink srcSector = microwaveLinks.GetSectorByIndex(i);
                    MicrowaveLinkDiff dstSector = microwaveLinksDiff.GetSectorByIndex(i);

                    CellFrequency1Array[i].Initialize(srcSector.Stations[0].Frequency, dstSector.Stations[0].Frequency);
                    CellFrequency2Array[i].Initialize(srcSector.Stations[1].Frequency, dstSector.Stations[1].Frequency);
                }
            }
        }

        public void PaintGrid()
        {
            _coordinateHelper1.Paint(dstMicrowaveLink.Stations[0].Position, srcMicrowaveLink.Stations[0].Position);
            _coordinateHelper2.Paint(dstMicrowaveLink.Stations[1].Position, srcMicrowaveLink.Stations[1].Position);

            if (srcMicrowaveLink.Stations[1].Polarization != dstMicrowaveLink.Stations[1].Polarization)
            {
                if (CellPolarization2.Value2 == dstMicrowaveLink.Stations[1].Polarization)
                    CellPolarization2.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellPolarization2.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellPolarization2.BackColor = CellStringProperty.DefaultBgr();

            if (srcMicrowaveLink.Stations[0].Polarization != dstMicrowaveLink.Stations[0].Polarization)
            {
                if (CellPolarization1.Value2 == dstMicrowaveLink.Stations[0].Polarization)
                    CellPolarization1.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellPolarization1.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellPolarization1.BackColor = CellStringProperty.DefaultBgr();
        }

        public void OnBeforeChangeAddr1(Cell cell, ref string NewValue)
        {
            CellAddress1.Value2 = NewValue;
            RecalculateRange();

            /*switch (NewValue)
            {
               case "1":
                  CellASL1.DoubleValue = srcMicrowaveLink.Stations[0].Position.ASl;
                  break;
               case "2":
                  CellASL1.DoubleValue = dstMicrowaveLink.Stations[0].Position.ASl;
                  break;
               default:
                  break;
            }*/

            InvalidateGrid();
        }

        public void OnBeforeChangeAddr2(Cell cell, ref string NewValue)
        {
            CellAddress2.Value2 = NewValue;
            RecalculateRange();

            InvalidateGrid();
        }

        private void RecalculateRange()
        {
            double lon1 = CellLongitude1.DoubleValue;
            double lat1 = CellLatitude1.DoubleValue;

            if (CellAddress1.Cell.KeyPickList != null && CellAddress1.Value2 == "2")
            {
                lon1 = CellLongitude1.DoubleValue2;
                lat1 = CellLatitude1.DoubleValue2;
            }

            double lon2 = CellLongitude2.DoubleValue;
            double lat2 = CellLatitude2.DoubleValue;

            if (CellAddress2.Cell.KeyPickList != null && CellAddress2.Value2 == "2")
            {
                lon2 = CellLongitude2.DoubleValue2;
                lat2 = CellLatitude2.DoubleValue2;
            }

            IMPosition a = new IMPosition(lon1, lat1, "4DMS");
            IMPosition b = new IMPosition(lon2, lat2, "4DMS");

            CellRange.DoubleValue = IM.RoundDeci(IMPosition.Distance(a, b), 3);
        }

        public void OnBeforeChangeAzimuth1(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeAzimuth2(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeMaxPower1(Cell cell, ref string NewValue)
        {

        }

        public void OnBeforeChangeMaxPower2(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeAGL1(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeAGL2(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangePolarization1(Cell cell, ref string NewValue)
        {
            CellPolarization1.Value2 = NewValue;
            InvalidateGrid();
        }

        public void OnBeforeChangePolarization2(Cell cell, ref string NewValue)
        {
            CellPolarization2.Value2 = NewValue;
            InvalidateGrid();
        }

        public void OnBeforeChangeFrequency1(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeFrequency2(Cell cell, ref string NewValue)
        {
        }

        public void InvalidateGrid()
        {
            PaintGrid();

            if (objGrid != null)
                objGrid.Invalidate();
        }

        //===========================================================
        /// <summary>
        /// Will be changed...
        /// </summary>
        protected override string GetXMLParamGrid()
        {
            return XICSM.UcrfRfaNET.Properties.Resources.MicrowaveAppCommit;
        }

        /// <summary>
        /// Jyst stub
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        /// <summary>
        /// Legacy
        /// </summary>
        /// <param name="docType"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        protected override string getOutDocName(string docType, string number)
        {
            return "";
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "addr1")
            {
                RecordPtr rcPtr = new RecordPtr(srcMicrowaveLink.Stations[0].Position.TableName, srcMicrowaveLink.Stations[0].Position.Id);
                rcPtr.UserEdit();
            }
            if (cell.Key == "addr2")
            {
                RecordPtr rcPtr = new RecordPtr(srcMicrowaveLink.Stations[1].Position.TableName, srcMicrowaveLink.Stations[1].Position.Id);
                rcPtr.UserEdit();
            }
        }

        /// <summary>
        /// Save differences
        /// </summary>
        /// <returns>true if successfull</returns>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "MICROWS" };

            PositionState diffPos1 = dstMicrowaveLink.Stations[0].Position.Clone() as PositionState;
            PositionState diffPos2 = dstMicrowaveLink.Stations[1].Position.Clone() as PositionState;

            AttachmentControl ac1 = new AttachmentControl();
            AttachmentControl ac2 = new AttachmentControl();
            
            StationSectorCollection<MicrowaveLink> savedMicrowaveLinks = new StationSectorCollection<MicrowaveLink>();
            int sectorCount = microwaveLinks.GetSectorCount();
            savedMicrowaveLinks.ApplID = applID;

            PositionState posA = _coordinateHelper1.GetSelectedPosition(
                      srcMicrowaveLink.Stations[0].Position,
                      dstMicrowaveLink.Stations[0].Position);
            PositionState posB = _coordinateHelper2.GetSelectedPosition(
                      srcMicrowaveLink.Stations[1].Position,
                      dstMicrowaveLink.Stations[1].Position);

            if (posA != srcMicrowaveLink.Stations[0].Position)
                SaveNewPosition(posA, srcMicrowaveLink.Stations[0].Position, assgnReferenceTables);
            if (posB != srcMicrowaveLink.Stations[1].Position)
                SaveNewPosition(posA, srcMicrowaveLink.Stations[1].Position, assgnReferenceTables);

            for (int i = 0; i < sectorCount; i++)
            {
                MicrowaveLinkDiff newMicrowaveLink = microwaveLinksDiff.GetSectorByIndex(i);

                MicrowaveLink savedMicrowaveLink = srcMicrowaveLink.Clone() as MicrowaveLink;

                if (savedMicrowaveLink == null)
                {
                    throw new Exception("MicrowaveAppCommit.SaveAppl (THIS EXCEPTION SHOULD NOT HAVE A PLACE)");
                }

                savedMicrowaveLink.Id = microwaveLinks.GetSectorByIndex(i).Id;
                savedMicrowaveLink.Stations[0].Id = microwaveLinks.GetSectorByIndex(i).Stations[0].Id;
                savedMicrowaveLink.Stations[1].Id = microwaveLinks.GetSectorByIndex(i).Stations[1].Id;

                savedMicrowaveLink.Stations[0].Position = posA;

                savedMicrowaveLink.Stations[1].Position = posB;

                savedMicrowaveLink.Stations[0].Position.NeedAddAdminSite = true;

                savedMicrowaveLink.Stations[0].Azimuth = CellAzimuth1.LookupedDoubleValue;
                savedMicrowaveLink.Stations[1].Azimuth = CellAzimuth2.LookupedDoubleValue;

                savedMicrowaveLink.Stations[0].Power[PowerUnits.dBW] = CellMaxPower1.LookupedDoubleValue;
                savedMicrowaveLink.Stations[1].Power[PowerUnits.dBW] = CellMaxPower2.LookupedDoubleValue;

                savedMicrowaveLink.Stations[0].AGL = CellAGL1.LookupedDoubleValue;
                savedMicrowaveLink.Stations[1].AGL = CellAGL2.LookupedDoubleValue;

                if (CellPolarization1.Value == dstMicrowaveLink.Stations[0].Polarization)
                    savedMicrowaveLink.Stations[0].Polarization = dstMicrowaveLink.Stations[0].Polarization;

                if (CellPolarization2.Value == dstMicrowaveLink.Stations[1].Polarization)
                    savedMicrowaveLink.Stations[1].Polarization = dstMicrowaveLink.Stations[1].Polarization;

                savedMicrowaveLink.Stations[0].Frequency = CellFrequency1Array[i].LookupedDoubleValue;
                savedMicrowaveLink.Stations[1].Frequency = CellFrequency2Array[i].LookupedDoubleValue;
                savedMicrowaveLink.ApplId = ApplID;
                savedMicrowaveLink.StatComment = newMicrowaveLink.StatComment;
                savedMicrowaveLinks.Add(savedMicrowaveLink);                
            }

            savedMicrowaveLinks.SaveSectorsOrSingle(true);

            int dstSectors = microwaveLinksDiff.GetSectorCount();
            for (int i = 0; i < dstSectors; i++)
            {
                MicrowaveLinkDiff diff = microwaveLinksDiff.GetSectorByIndex(i);
                diff.Remove();
            }

            MicrowaveLink microwaveLink = savedMicrowaveLinks.GetSectorByIndex(0);

            List<int> stations1 = new List<int>();
            List<int> stations2 = new List<int>();

            for (int i = 0; i < savedMicrowaveLinks.GetSectorCount();i++)
            {
                stations1.Add(savedMicrowaveLinks.GetSectorByIndex(i).Stations[0].Id);
                stations2.Add(savedMicrowaveLinks.GetSectorByIndex(i).Stations[1].Id);
            }

            ac1.RelocateAndCopyDocLinks(
                diffPos1.TableName,
                diffPos1.Id,
                ICSMTbl.itblMicrows,
                stations1,
                microwaveLink.Stations[0].Position.TableName,
                microwaveLink.Stations[0].Position.Id,
                ICSMTbl.itblAdmSite,
                microwaveLink.Stations[0].Position.AdminSitesId);

            ac2.RelocateAndCopyDocLinks(
                diffPos2.TableName,
                diffPos2.Id,
                ICSMTbl.itblMicrows,
                stations2,
                microwaveLink.Stations[1].Position.TableName,
                microwaveLink.Stations[1].Position.Id,
                ICSMTbl.itblAdmSite,
                microwaveLink.Stations[1].Position.AdminSitesId);
            

            return true;
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();
            CreateComboBoxesForPower();

            CellEquipmentName.Value = srcMicrowaveLink.Equipment.Name;
            double srcBandwidth = srcMicrowaveLink.Equipment.Bandwidth.DivNullD(1000.0);
            CellBandwidth.Value = srcBandwidth.ToStringNullD();

            CellDesigEmission.Value = srcMicrowaveLink.Equipment.DesigEmission;

            CellCertificateNumber.Value = srcMicrowaveLink.Equipment.Certificate.Symbol;
            CellCertificateDate.Value = srcMicrowaveLink.Equipment.Certificate.Date.ToShortDateString();

            Power powerMaxObj = srcMicrowaveLink.Equipment.MaxPower;
            Power powerMinObj = srcMicrowaveLink.Equipment.MinPower;

            CellMaxPower1.Red =
               (powerMaxObj.IsValid && powerMaxObj[PowerUnits.dBW] < dstMicrowaveLink.Stations[0].Power[PowerUnits.dBW]) ||
               (powerMinObj.IsValid && powerMinObj[PowerUnits.dBW] > dstMicrowaveLink.Stations[0].Power[PowerUnits.dBW]);

            CellMaxPower2.Red =
              (powerMaxObj.IsValid && powerMaxObj[PowerUnits.dBW] < dstMicrowaveLink.Stations[1].Power[PowerUnits.dBW]) ||
              (powerMinObj.IsValid && powerMinObj[PowerUnits.dBW] > dstMicrowaveLink.Stations[1].Power[PowerUnits.dBW]);

            CellEquipmentName.CanEdit = false;
            CellBandwidth.CanEdit = false;
            CellDesigEmission.CanEdit = false;
            CellModulation.CanEdit = false;

            CellEquipmentName.CanEdit = false;
            CellBandwidth.CanEdit = false;
            CellDesigEmission.CanEdit = false;
            CellModulation.CanEdit = false;
            CellCertificateNumber.CanEdit = false;
            CellCertificateDate.CanEdit = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitStationObject()
        {
            //Value
            //CellASL1.DoubleValue = srcMicrowaveLink.Stations[0].Position.ASl;
            CellAzimuth1.DoubleValue = srcMicrowaveLink.Stations[0].Azimuth;
            //CellAzimuth1.Value = srcMicrowaveLink.Stations[0].Azimuth.ToStringNullD();
            //CellMaxPower1.DoubleValue = srcMicrowaveLink.Stations[0].Power[PowerUnits.dBW];
            CellAGL1.DoubleValue = srcMicrowaveLink.Stations[0].AGL;
            //CellGain1.DoubleValue = srcMicrowaveLink.Stations[0].Gain;         
            //CellDiameter1.DoubleValue = srcMicrowaveLink.Stations[0].Antenna.Diameter;
            CellPolarization1.Value = srcMicrowaveLink.Stations[0].Polarization;
            //CellFrequency1.DoubleValue = srcMicrowaveLink.Stations[0].Frequency;

            //Second station
            //CellASL2.DoubleValue = srcMicrowaveLink.Stations[1].Position.ASl;
            //!
            CellAzimuth2.Value = srcMicrowaveLink.Stations[1].Azimuth.ToStringNullD();
            //CellMaxPower2.DoubleValue = srcMicrowaveLink.Stations[1].Power[PowerUnits.dBW];
            CellAGL2.DoubleValue = srcMicrowaveLink.Stations[1].AGL;
            //CellGain2.DoubleValue = srcMicrowaveLink.Stations[1].Gain;         
            //CellDiameter2.DoubleValue = srcMicrowaveLink.Stations[1].Antenna.Diameter;
            CellPolarization2.Value = srcMicrowaveLink.Stations[1].Polarization;
            //CellFrequency2.DoubleValue = srcMicrowaveLink.Stations[1].Frequency;

            //Value2
            //CellASL1.DoubleValue2 = srcMicrowaveLink.Stations[0].Position.ASl;

            //CellAzimuth1.DoubleValue2 = srcMicrowaveLink.Stations[0].Azimuth;
            //CellAzimuth1.Value2 = srcMicrowaveLink.Stations[0].Azimuth.ToStringNullD();

            //CellMaxPower1.DoubleValue2 = srcMicrowaveLink.Stations[0].Power[PowerUnits.dBW];
            //CellAGL1.DoubleValue2 = dstMicrowaveLink.Stations[0].AGL;
            //CellGain1.DoubleValue2 = dstMicrowaveLink.Stations[0].Gain;

            //CellDiameter1.DoubleValue2 = dstMicrowaveLink.Stations[0].Antenna.Diameter;
            CellPolarization1.Value2 = srcMicrowaveLink.Stations[0].Polarization;
            //CellFrequency1.DoubleValue2 = srcMicrowaveLink.Stations[0].Frequency;

            CellAntennaName1.Value = srcMicrowaveLink.Stations[0].Antenna.Name;
            //CellAntennaName1.Value = "";// dstMicrowaveLink.Stations[0].Antenna.Name;
            CellAntennaName2.Value = srcMicrowaveLink.Stations[1].Antenna.Name;
            //CellAntennaName2.Value2 = "";// dstMicrowaveLink.Stations[1].Antenna.Name;
            //CellEquipmentName.Value = srcMicrowaveLink.Equipment.Name;
            //CellEquipmentName.Value2 = "";// dstMicrowaveLink.Equipment.Name;

            //Second station
            //--CellLongitude2.DoubleValue2 = dstMicrowaveLink.Stations[1].Position.LonDms;
            //CellLatitude2.DoubleValue2 = dstMicrowaveLink.Stations[1].Position.LatDms;

            //CellAddress2.Value2 = srcMicrowaveLink.Stations[1].Position.FullAddress;
            //CellASL2.DoubleValue2 = srcMicrowaveLink.Stations[1].Position.ASl;

            //CellAzimuth2.DoubleValue2 = srcMicrowaveLink.Stations[1].Azimuth;
            //CellAzimuth2.Value2 = srcMicrowaveLink.Stations[1].Azimuth.ToStringNullD();

            //CellMaxPower2.DoubleValue2 = srcMicrowaveLink.Stations[1].Power[PowerUnits.dBW];
            //CellAGL2.DoubleValue2 = srcMicrowaveLink.Stations[1].AGL;
            //CellGain2.DoubleValue2 = dstMicrowaveLink.Stations[1].Gain;

            //CellDiameter2.DoubleValue2 = dstMicrowaveLink.Stations[1].Antenna.Diameter;
            CellPolarization2.Value2 = srcMicrowaveLink.Stations[1].Polarization;
            //CellFrequency2.DoubleValue2 = srcMicrowaveLink.Stations[1].Frequency;


            CellRange.DoubleValue = Convert.ToInt32(srcMicrowaveLink.Range * 1000.0) / 1000.0;
            //CellRange.DoubleValue2 = Convert.ToInt32(srcMicrowaveLink.Range * 1000.0) / 1000.0;

            CellObjectID.Value = ObjID.ToString();

            CellLongitude1.CanEdit = false;
            CellLatitude1.CanEdit = false;
            CellAddress1.CanEdit = false;

            CellAzimuth1.CanEdit = false;
            CellMaxPower1.CanEdit = false;
            CellAGL1.CanEdit = false;
            CellGain1.CanEdit = false;
            CellAntennaName1.CanEdit = false;
            CellDiameter1.CanEdit = false;
            CellPolarization1.CanEdit = false;
            //CellFrequency1.CanEdit = false;

            //Second station
            CellLongitude2.CanEdit = false;
            CellLatitude2.CanEdit = false;
            CellAddress2.CanEdit = false;
            CellAzimuth2.CanEdit = false;
            CellMaxPower2.CanEdit = false;
            CellAGL2.CanEdit = false;
            CellGain2.CanEdit = false;
            CellAntennaName2.CanEdit = false;
            CellDiameter2.CanEdit = false;
            CellPolarization2.CanEdit = false;
            //CellFrequency2.CanEdit = false;

            CellASL1.CanEdit = false;
            CellASL2.CanEdit = false;
            CellRange.CanEdit = false;
            CellObjectID.CanEdit = false;

            _modulationList = CellModulation.Cell.KeyPickList;
            InitEquipment();
            //InitAntenna1();
            //InitAntenna2();

            int srcSectorCount = microwaveLinks.GetSectorCount();

            for (int i = 0; i < srcSectorCount; i++)
            {
                MicrowaveLink srcSector = microwaveLinks.GetSectorByIndex(i);

                CellFrequency1Array[i].DoubleValue = srcSector.Stations[0].Frequency;
                CellFrequency1Array[i].CanEdit = false;

                CellFrequency2Array[i].DoubleValue = srcSector.Stations[1].Frequency;
                CellFrequency2Array[i].CanEdit = false;
            }
        }

        public void OnPressButtonLongitude(Cell cell)
        {
        }

        public void OnPressButtonAddressName(Cell cell)
        {
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipMicrow, param);
            return RecEquip;
        }

        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblMicrowAntenna, param);
            return RecAnt;
        }

        /// <summary>
        /// On press button equipment
        ///   Set new Equipment if last one is selected
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                srcMicrowaveLink.Equipment.TableName = newEquip.Table;
                srcMicrowaveLink.Equipment.Id = newEquip.Id;
                srcMicrowaveLink.Equipment.Load();
                srcMicrowaveLink.OnNewEquipment();

                InitEquipment();

                PaintGrid();
            }
        }

        public void OnPressButtonAntennaName1(Cell cell)
        {
            RecordPtr newAnt = LookupAntenna(cell.Value, true);
            if ((newAnt.Id > 0) && (newAnt.Id < IM.NullI))
            {
                srcMicrowaveLink.Stations[0].Antenna.TableName = newAnt.Table;
                srcMicrowaveLink.Stations[0].Antenna.Id = newAnt.Id;
                srcMicrowaveLink.Stations[0].Antenna.Load();

                //CellDiameter1.DoubleValue = srcMicrowaveLink.Stations[0].Antenna.Diameter;
                //CellDiameter1.DoubleValue2 = srcMicrowaveLink.Stations[0].Antenna.Diameter;
                //CellGain1.DoubleValue = srcMicrowaveLink.Stations[0].Antenna.Gain;
                //CellGain1.DoubleValue2 = srcMicrowaveLink.Stations[0].Antenna.Gain;
                srcMicrowaveLink.Stations[0].Gain = srcMicrowaveLink.Stations[0].Antenna.Gain;
                CellAntennaName1.Value = srcMicrowaveLink.Stations[0].Antenna.Name;
                CreateAntennaComboBoxes(0);
                //CellAntennaName1.Value2 = srcMicrowaveLink.Stations[0].Antenna.Name;
                PaintGrid();
            }
        }

        public void OnPressButtonAntennaName2(Cell cell)
        {
            RecordPtr newAnt = LookupAntenna(cell.Value, true);
            if ((newAnt.Id > 0) && (newAnt.Id < IM.NullI))
            {
                srcMicrowaveLink.Stations[1].Antenna.TableName = newAnt.Table;
                srcMicrowaveLink.Stations[1].Antenna.Id = newAnt.Id;
                srcMicrowaveLink.Stations[1].Antenna.Load();

                //CellDiameter2.DoubleValue = srcMicrowaveLink.Stations[1].Antenna.Diameter;
                //CellDiameter2.DoubleValue2 = srcMicrowaveLink.Stations[1].Antenna.Diameter;
                //CellGain2.DoubleValue = srcMicrowaveLink.Stations[1].Antenna.Gain;
                //CellGain2.DoubleValue2 = srcMicrowaveLink.Stations[1].Antenna.Gain;
                srcMicrowaveLink.Stations[1].Gain = srcMicrowaveLink.Stations[1].Antenna.Gain;
                CellAntennaName2.Value = srcMicrowaveLink.Stations[1].Antenna.Name;
                CreateAntennaComboBoxes(1);
                //CellAntennaName2.Value2 = srcMicrowaveLink.Stations[1].Antenna.Name;
                PaintGrid();
            }
        }

        /// <summary>
        /// Заглушка    
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="grid"></param>
        public override void OnPressButton(Cell cell, Grid grid)
        {

        }
    }
}
