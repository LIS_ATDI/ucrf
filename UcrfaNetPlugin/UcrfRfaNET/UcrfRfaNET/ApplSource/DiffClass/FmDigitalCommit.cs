﻿using System;
using System.Collections.Generic;
using System.Linq;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class FmDigitallCommit : BaseCommitClass
    {
        private bool initialized;
        public int Id { get; set; }

        private PositionState selectedPosition;

        private CoordinateCommitHelper coordHelper;

        private FmDigitallStation fmDgStation;
        private FmDigitallDiffStation fmDgDiffStation;
        #region CELL

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Номер контуру
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentContour = new CellStringComboboxAplyProperty();
        /// <summary>
        /// номер РЕЗ в синхронній мережі
        /// </summary> 
        private CellStringProperty CellEquipmentNameSynchr = new CellStringProperty();

        /// <summary>
        /// Варіант системи
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameVariant = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Кількість несівних
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentCountOstov = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Захисний інтервал
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSafeInterv = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Втрати у фільтрі 
        /// </summary> 
        private CellDoubleProperty CellEquipmentLoseFiltr = new CellDoubleProperty();
        /// <summary>
        /// Тип спектр. маски фільтра
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSpectrMaskFiltr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Тип прийм
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentTypeReceive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Стандарт компресії
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentStandCompr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Зміщення несівної частоти, Гц
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentShiftOstov = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleLookupProperty CellPowerTr = new CellDoubleLookupProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        /// <summary>
        /// Довжина фідера, м
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері, дБ/м
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenDirect = new CellStringLookupEncodedProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenPolar = new CellStringLookupEncodedProperty();

        /// <summary>
        /// Висота антени над землею, м
        /// </summary>
        private CellStringComboboxAplyProperty CellHighAntenn = new CellStringComboboxAplyProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellDoubleProperty CellMaxKoefG = new CellDoubleProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellDoubleProperty CellMaxKoefV = new CellDoubleProperty();

        /// <summary>
        /// ЕВП Г, дБВт 
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В, дБВт
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП Макс., дБВт
        /// </summary>
        private CellDoubleProperty CellEVPm = new CellDoubleProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellInnerState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellOuterState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Блок
        /// </summary>
        private CellStringComboboxAplyProperty CellBlock = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Центральна частота, МГц
        /// </summary>
        private CellDoubleProperty CellMaxFreq = new CellDoubleProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public FmDigitallCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, FmDigitalApp.TableName, ownerId, packetID, radioTech)
        {
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);
            fmDgStation = new FmDigitallStation();
            fmDgStation.Id = recordID.Id;
            fmDgStation.Load();

            fmDgDiffStation = new FmDigitallDiffStation();
            fmDgDiffStation.Id = recordID.Id;
            fmDgDiffStation.Load();

        }

        private void ReadAllDefaultDataPositions()
        {
            CellEquipmentName.Value = fmDgStation.NameRez;
            CellEquipmentNameNumber.Value = fmDgStation.NameNumber;
            CellEquipmentNameREZ.Value = fmDgStation.NameRez;

            CellCertificateNumber.Value = fmDgStation.FmDgEquipment.Certificate.Symbol;
            CellCertificateDate.Value = fmDgStation.FmDgEquipment.Certificate.Date.ToShortDateString();
            CellPowerTr.Value = fmDgStation.FmDgEquipment.MaxPower[PowerUnits.dBm].Round(3).ToStringNullD();
            CellTxBandwidth.Value = fmDgStation.FmDgEquipment.Bandwidth.ToStringNullD();
            CellTxDesignEmission.Value = fmDgStation.FmDgEquipment.DesigEmission;

            if (!fmDgStation.ContourList.Contains(fmDgStation.Countur))
                fmDgStation.Countur = fmDgStation.ContourList[0];
            CellEquipmentContour.Value = fmDgStation.ContourList[fmDgStation.ContourList.IndexOf(fmDgStation.Countur)];

            CellEquipmentName.Value = fmDgStation.NameRez;
            CellEquipmentNameSynchr.Value = fmDgStation.NameSynchr;

            if (!fmDgStation.VariantSystList.Contains(fmDgStation.NameVariant))
                fmDgStation.NameVariant = fmDgStation.VariantSystList[0];
            CellEquipmentNameVariant.Value = fmDgStation.NameVariant;

            if (!fmDgStation.CountNesivList.Contains(fmDgStation.CountOstov))
                fmDgStation.CountOstov = fmDgStation.CountNesivList[0];
            CellEquipmentCountOstov.Value = fmDgStation.CountOstov;

            if (!string.IsNullOrEmpty(fmDgStation.SafeInterv.ToString()))
                CellEquipmentSafeInterv.Value = fmDgStation.SafeInterv.ToString();

            CellEquipmentLoseFiltr.DoubleValue = fmDgStation.LoseFiltr;
            if (!string.IsNullOrEmpty(fmDgStation.SpectrMaskFiltr))
                CellEquipmentSpectrMaskFiltr.Value = fmDgStation.SpectrMaskFiltr;

            if (!fmDgStation.TypeReceiveDict.ContainsValue(fmDgStation.TypeReceive))
                fmDgStation.TypeReceive = fmDgStation.TypeReceiveDict.ToList()[0].Value;
            CellEquipmentTypeReceive.Value = fmDgStation.TypeReceive;

            if (!fmDgStation.StandComprList.Contains(fmDgStation.StandCompr))
                fmDgStation.StandCompr = fmDgStation.StandComprList[0];
            CellEquipmentStandCompr.Value = fmDgStation.StandCompr;

            if (!string.IsNullOrEmpty(fmDgStation.ShiftOstov.ToStringNullD()))
            {
                CellEquipmentShiftOstov.Value = fmDgStation.ShiftOstov.ToStringNullD();
                CellEquipmentShiftOstov.Items.Add(fmDgStation.ShiftOstov.ToStringNullD());
            }

            CellEquipmentName.Value = fmDgStation.FmDgEquipment.Name;

            CellFiderLength.DoubleValue = fmDgStation.FeederLength;
            CellFiderlost.DoubleValue = fmDgStation.FeederLosses;

            if (!string.IsNullOrEmpty(fmDgStation.HeightAnten.ToStringNullD()))
            {
                CellHighAntenn.Value = fmDgStation.HeightAnten.ToStringNullD();
                CellHighAntenn.Items.Add(fmDgStation.HeightAnten.ToStringNullD());
            }

            CellMaxHigh.DoubleValue = fmDgStation.MaxHigh;
            if (!string.IsNullOrEmpty(fmDgStation.InnerState))
                CellInnerState.Value = fmDgStation.InnerState + " - " + fmDgStation.InnerStateDict[fmDgStation.InnerState];

            if (!string.IsNullOrEmpty(fmDgStation.OuterState))
                CellOuterState.Value = fmDgStation.OuterState + " - " + fmDgStation.OuterStateDict[fmDgStation.OuterState];

            if (!fmDgStation.ChannelBlockList.Contains(fmDgStation.Block))
                fmDgStation.Block = fmDgStation.ChannelBlockList[0];
            CellBlock.Value = fmDgStation.Block;

            CellMaxFreq.DoubleValue = fmDgStation.FreqCentr;
            CellObjRCHP.IntValue = fmDgStation.ObjRCHP;

        }

        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            //CellPowerTr.Initialize(fmDgStation.Power[PowerUnits.W], fmDgDiffStation.Power[PowerUnits.W]);

            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, fmDgStation.FmDgEquipment.Bandwidth.ToStringNullD(), fmDgDiffStation.FmDgDiffEquipment.Bandwidth.ToStringNullD());
            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, fmDgStation.FmDgEquipment.DesigEmission, fmDgDiffStation.FmDgDiffEquipment.DesigEmission);
            //Поменять поведение, если сертификату введен прочерк. 
            if (fmDgDiffStation.FmDgDiffEquipment.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, fmDgStation.FmDgEquipment.Certificate.Symbol, fmDgDiffStation.FmDgDiffEquipment.Certificate.Symbol);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, fmDgStation.FmDgEquipment.Certificate.Date.ToShortDateString(), fmDgDiffStation.FmDgDiffEquipment.Certificate.Date.ToShortDateString());
        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;

            ReadAllDefaultDataPositions();
            if (fmDgDiffStation.IsEnableDiff)
            {
                FillDiffComboBox(CellEquipmentShiftOstov, fmDgStation.ShiftOstov.ToStringNullD(), fmDgDiffStation.ShiftOstov.ToStringNullD());
                FillDiffComboBox(CellHighAntenn, fmDgStation.HeightAnten.ToStringNullD(), fmDgDiffStation.HeightAnten.ToStringNullD());

                CellEquipmentName.Value = fmDgDiffStation.FmDgDiffEquipment.Name;

                CellCertificateNumber.Value = fmDgDiffStation.FmDgDiffEquipment.Certificate.Symbol;
                CellCertificateDate.Value = fmDgDiffStation.FmDgDiffEquipment.Certificate.Date.ToShortDateString();
                CellPowerTr.Value = fmDgDiffStation.FmDgDiffEquipment.MaxPower[PowerUnits.dBm].ToStringNullD();
                CellTxBandwidth.Value = fmDgDiffStation.FmDgDiffEquipment.Bandwidth.ToStringNullD();
                CellTxDesignEmission.Value = fmDgDiffStation.FmDgDiffEquipment.DesigEmission;

                Comment = fmDgDiffStation.StatComment;
            }

            coordHelper.Initialize(fmDgDiffStation.Position, fmDgStation.Position);

            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenPolar, fmDgStation.Polarization, fmDgDiffStation.Polar);
            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenDirect, fmDgStation.Direction, fmDgDiffStation.Direct);

            CreateEquipmentComboBoxes();
            initialized = true;
            OnRecalculateErp(null);
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        public void OnPressButtonEquipmentName(Cell cell)
        {
            //string criteria = HelpFunction.ReplaceQuotaSumbols(CellEquipmentName.Value);
            //string param = "{NAME=\"*" + criteria + "*\"}";

            //RecordPtr recEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipBro, param);
            //fmDgStation.EquipId = recEquip.Id;
            //IMObject obj = IMObject.LoadFromDB(recEquip);
            //fmDgStation.NameRez = obj.GetS("NAME");
            //fmDgStation.Bandwith = obj.GetD("BW");
            //fmDgStation.ClassEmission = obj.GetS("DESIG_EMISSION");
            //fmDgStation.CertificateDate = obj.GetT("CUST_DAT1");
            //fmDgStation.CertificateNumber = obj.GetS("CUST_TXT1");
            //CellEquipmentName.Value2 = fmDgStation.NameRez;
            //CellEquipmentName.Value = fmDgStation.NameRez;
            //CreateEquipmentComboBoxes();

            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                fmDgStation.FmDgEquipment.TableName = newEquip.Table;
                fmDgStation.FmDgEquipment.Id = newEquip.Id;
                fmDgStation.FmDgEquipment.Load();
                // fmStation.OnNewEquipment();

                InitEquipment();

                CellPowerTr.Initialize(fmDgStation.Power[PowerUnits.W].Round(3), fmDgDiffStation.FmDgDiffEquipment.MaxPower[PowerUnits.W].Round(3));

                PaintGrid();
            }
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipBro, param);
            return RecEquip;
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = fmDgStation.FmDgEquipment.Name;
            CellTxBandwidth.Value = fmDgStation.FmDgEquipment.Bandwidth.ToStringNullD();
            CellTxDesignEmission.Value = fmDgStation.FmDgEquipment.DesigEmission;

            CellCertificateNumber.Value = fmDgStation.FmDgEquipment.Certificate.Symbol;
            CellCertificateDate.Value = fmDgStation.FmDgEquipment.Certificate.Date.ToShortDateString();
        }

        public void PaintGrid()
        {
            coordHelper.Paint(fmDgDiffStation.Position, fmDgStation.Position);
        }

        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            coordHelper.Paint(fmDgStation.Position, fmDgDiffStation.Position);
        }

        //////////////////////////////////////////
        //Необхідні перевантажені методи
        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(fmDgStation.Position.TableName, fmDgStation.Position.Id);
                rcPtr.UserEdit();
            }
        }
        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "TDAB_STATION" };
            PositionState diffPos = fmDgStation.Position.Clone() as PositionState;
            AttachmentControl ac = new AttachmentControl();

            selectedPosition = coordHelper.GetSelectedPosition(fmDgStation.Position, fmDgDiffStation.Position);
            if (selectedPosition != fmDgStation.Position)
                SaveNewPosition(selectedPosition, fmDgStation.Position, assgnReferenceTables);
            fmDgStation.Position = selectedPosition;

            fmDgDiffStation.Id = recordID.Id;

            fmDgStation.Polarization = CellAntenPolar.Value;
            fmDgStation.Direction = CellAntenDirect.Value;
            Double highAnt;
            if (!Double.TryParse(CellHighAntenn.Value, out highAnt))
                highAnt = 0.0;
            fmDgStation.HeightAnten = Convert.ToDouble(highAnt);
            fmDgStation.Block = CellBlock.Value;
            fmDgStation.FreqCentr = CellMaxFreq.DoubleValue;
            fmDgStation.ApplId = ApplID;
            fmDgStation.StatComment = fmDgDiffStation.StatComment;

            fmDgStation.Save();
            fmDgDiffStation.Remove();

            ac.RelocateAndCopyDocLinks(
              diffPos.TableName,
              diffPos.Id,
              FmAnalogApp.TableName,
              fmDgStation.Id,
              fmDgStation.Position.TableName,
              fmDgStation.Position.Id,
              ICSMTbl.itblAdmSite,
              fmDgStation.Position.AdminSitesId);
            
            return true;
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.FmDigitalCommit;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (initialized)
            {
                fmDgStation.Power[PowerUnits.W] = CellPowerTr.LookupedDoubleValue;
                fmDgStation.Polarization = CellAntenPolar.Value;
                fmDgStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!fmDgStation.HorizontalDiagramm.IsEmpty && fmDgStation.Polarization != "V")
                value = fmDgStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!fmDgStation.VerticalDiagramm.IsEmpty && fmDgStation.Polarization != "H")
                value = fmDgStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            fmDgStation.CalculateERP();
            if (fmDgStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = fmDgStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (fmDgStation.Erp.Vertical.IsValid)
                CellEVPV.Value = fmDgStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = fmDgStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}
