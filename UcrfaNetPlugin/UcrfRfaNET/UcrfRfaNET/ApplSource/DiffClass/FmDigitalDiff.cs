﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using NSPosition;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class FmDigitallDiff : BaseDiffClass
    {
        private bool _initialized;
        public int Id { get; set; }

        private FmDigitallStation fmDgStation;
        private FmDigitallDiffStation fmDgDiffStation;

        #region CELL

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Номер контуру
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentContour = new CellStringComboboxAplyProperty();
        /// <summary>
        /// номер РЕЗ в синхронній мережі
        /// </summary> 
        private CellStringProperty CellEquipmentNameSynchr = new CellStringProperty();

        /// <summary>
        /// Варіант системи
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameVariant = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Кількість несівних
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentCountOstov = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Захисний інтервал
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSafeInterv = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Втрати у фільтрі 
        /// </summary> 
        private CellDoubleProperty CellEquipmentLoseFiltr = new CellDoubleProperty();
        /// <summary>
        /// Тип спектр. маски фільтра
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSpectrMaskFiltr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Тип прийм
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentTypeReceive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Стандарт компресії
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentStandCompr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Зміщення несівної частоти, Гц
        /// </summary> 
        private CellDoubleProperty CellEquipmentShiftOstov = new CellDoubleProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleProperty CellPowerTr = new CellDoubleProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellDoubleProperty CellTxBandwidth = new CellDoubleProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Довжина фідера, м
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері, дБ/м
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringProperty CellAntenDirect = new CellStringProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringProperty CellAntenPolar = new CellStringProperty();

        /// <summary>
        /// Висота антени над землею, м
        /// </summary>
        private CellDoubleProperty CellHighAntenn = new CellDoubleProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellDoubleProperty CellMaxKoefG = new CellDoubleProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellDoubleProperty CellMaxKoefV = new CellDoubleProperty();

        /// <summary>
        /// ЕВП Г, дБВт 
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В, дБВт
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП Макс., дБВт
        /// </summary>
        private CellDoubleProperty CellEVPm = new CellDoubleProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellInnerState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellOuterState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Блок
        /// </summary>
        private CellStringComboboxAplyProperty CellBlock = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Центральна частота, МГц
        /// </summary>
        private CellDoubleProperty CellMaxFreq = new CellDoubleProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public FmDigitallDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, FmDigitalApp.TableName, ownerId, packetID, radioTech)
        {
            //Стандартные значения
            fmDgStation = new FmDigitallStation();
            fmDgStation.Id = recordID.Id;
            fmDgStation.Load();

            //URCP значения
            fmDgDiffStation = new FmDigitallDiffStation();
            fmDgDiffStation.Id = recordID.Id;
            fmDgDiffStation.Load();
            if (fmDgDiffStation.Position == null)
                fmDgDiffStation.Position = fmDgStation.Position;
            SCVisnovok = fmDgStation.Finding;
            _stationObject = fmDgStation;
            CellCertificateDate.NullString = "-";
        }

        private void ReadAllDefaultDataPositions()
        {
            CellLongitude.DoubleValue2 = fmDgStation.Position.LonDms;
            CellLatitude.DoubleValue2 = fmDgStation.Position.LatDms;
            CellAddress.Value2 = fmDgStation.Position.FullAddress;
            CellEquipmentName.Value2 = FillValue2(fmDgStation.NameRez);
            CellEquipmentName.Value = fmDgStation.NameRez;            
            CellEquipmentNameNumber.Value = fmDgStation.NameNumber;
            CellEquipmentNameREZ.Value = fmDgStation.NameRez;
            CellEquipmentContour.Items.AddRange(fmDgStation.ContourList);
            CellLongitude.DoubleValue = fmDgStation.Position.LonDms;
            CellLatitude.DoubleValue = fmDgStation.Position.LatDms;
            CellAddress.Value = fmDgStation.Position.FullAddress;

            if (string.IsNullOrEmpty(fmDgStation.Countur))
                fmDgStation.Countur = fmDgStation.ContourList[0];
            if (!fmDgStation.ContourList.Contains(fmDgStation.Countur))
                CellEquipmentContour.Value = fmDgStation.Countur;
            else
                CellEquipmentContour.Value = fmDgStation.ContourList[fmDgStation.ContourList.IndexOf(fmDgStation.Countur)];

            CellEquipmentNameSynchr.Value = fmDgStation.NameSynchr;

            CellEquipmentNameVariant.Items.AddRange(fmDgStation.VariantSystList);
            if (!fmDgStation.VariantSystList.Contains(fmDgStation.NameVariant))
                fmDgStation.NameVariant = fmDgStation.VariantSystList[0];
            CellEquipmentNameVariant.Value = fmDgStation.NameVariant;

            if (string.IsNullOrEmpty(fmDgStation.CountOstov))
                fmDgStation.CountOstov = fmDgStation.CountNesivList[0];
            CellEquipmentCountOstov.Value = fmDgStation.CountOstov;
            CellEquipmentCountOstov.Items.AddRange(fmDgStation.CountNesivList);

            if (string.IsNullOrEmpty(fmDgStation.SafeInterv.ToString()) || fmDgStation.SafeInterv == IM.NullI)
                fmDgStation.SafeInterv = Convert.ToInt32(fmDgStation.SafeIntervList[0]);
            CellEquipmentSafeInterv.Value = fmDgStation.SafeInterv.ToString();
            CellEquipmentSafeInterv.Items.AddRange(fmDgStation.SafeIntervList);

            CellEquipmentLoseFiltr.DoubleValue = fmDgStation.LoseFiltr;
            CellEquipmentSpectrMaskFiltr.Value = fmDgStation.SpectrMaskFiltr;
            CellEquipmentSpectrMaskFiltr.Items.AddRange(fmDgStation.SpectrMaskList);
            if (string.IsNullOrEmpty(fmDgStation.TypeReceive))
                fmDgStation.TypeReceive = fmDgStation.TypeReceiveDict.ToList()[0].Key;
            CellEquipmentTypeReceive.Value = fmDgStation.TypeReceiveDict[fmDgStation.TypeReceive];
            foreach (KeyValuePair<string, string> s in fmDgStation.TypeReceiveDict)
                CellEquipmentTypeReceive.Items.Add(s.Value);
            if (!fmDgStation.StandComprList.Contains(fmDgStation.StandCompr))
                fmDgStation.StandCompr = fmDgStation.StandComprList[0];
            CellEquipmentStandCompr.Value = fmDgStation.StandCompr;
            CellEquipmentStandCompr.Items.AddRange(fmDgStation.StandComprList);
            CellEquipmentShiftOstov.DoubleValue2 = fmDgStation.ShiftOstov;
            CellEquipmentShiftOstov.DoubleValue = fmDgStation.ShiftOstov;

            if (!string.IsNullOrEmpty(fmDgStation.FmDgEquipment.Certificate.Symbol))
                CellCertificateNumber.Value = fmDgStation.FmDgEquipment.Certificate.Symbol;
            CellCertificateNumber.Value2 = FillValue2(fmDgStation.FmDgEquipment.Certificate.Symbol);
            CellCertificateNumber.SetRedValue();

            CellCertificateDate.DateValue = fmDgStation.FmDgEquipment.Certificate.Date;
            CellCertificateDate.Value2 = FillValue2(fmDgStation.FmDgEquipment.Certificate.Date.ToShortDateStringNullT());
            CellCertificateDate.SetRedValue();

            CellFiderLength.DoubleValue = fmDgStation.FeederLength;
            CellFiderlost.DoubleValue = fmDgStation.FeederLosses;

            CellPowerTr.DoubleValue = fmDgStation.Power[PowerUnits.W];

            CellHighAntenn.Value2 = FillValue2(fmDgStation.HeightAnten.ToStringNullD());
            CellHighAntenn.DoubleValue = fmDgStation.HeightAnten;
            CellMaxHigh.DoubleValue = fmDgStation.MaxHigh;

            CellAntenPolar.Value2 = FillValue2(fmDgStation.Polarization);
            CellAntenPolar.Value = fmDgStation.Polarization;
            CellAntenDirect.Value2 = FillValue2(fmDgStation.Direction);
            CellAntenDirect.Value = fmDgStation.Direction;


            if (!fmDgStation.InnerStateDict.ContainsKey(fmDgStation.InnerState))
                fmDgStation.InnerState = fmDgStation.InnerStateDict.ToList()[0].Key;
            CellInnerState.Value = fmDgStation.InnerState + " - " + fmDgStation.InnerStateDict[fmDgStation.InnerState];
            foreach (KeyValuePair<string, string> s in fmDgStation.InnerStateDict)
                CellInnerState.Items.Add(s.Key + " - " + s.Value);

            if (!fmDgStation.OuterStateDict.ContainsKey(fmDgStation.OuterState))
                fmDgStation.OuterState = fmDgStation.OuterStateDict.ToList()[0].Key;
            CellOuterState.Value = fmDgStation.OuterState + " - " + fmDgStation.OuterStateDict[fmDgStation.OuterState];
            foreach (KeyValuePair<string, string> s in fmDgStation.OuterStateDict)
                CellOuterState.Items.Add(s.Key + " - " + s.Value);

            if (!fmDgStation.ChannelBlockList.Contains(fmDgStation.Block))
                fmDgStation.Block = fmDgStation.ChannelBlockList[0];
            CellBlock.Value = fmDgStation.Block;
            CellBlock.Items.AddRange(fmDgStation.ChannelBlockList);

            CellMaxFreq.DoubleValue = fmDgStation.FreqCentr;
            CellObjRCHP.IntValue = fmDgStation.ObjRCHP;

            CreateEquipmentComboBoxes();
        }

        /// <summary>
        /// Реакция на нажатия кнопки оформления позиции
        /// Формирование адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");
            // Выбираем запись из таблицы                     
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionBro, 1, tmpXy.Longitude, tmpXy.Latitude);
            if (objPosition != null)
            {
                fmDgDiffStation.Position = new PositionState();
                fmDgDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                CellLongitude.DoubleValue = fmDgDiffStation.Position.LongDms;
                CellLatitude.DoubleValue = fmDgDiffStation.Position.LatDms;
                CellASL.DoubleValue = fmDgDiffStation.Position.ASl;
                if (!string.IsNullOrEmpty(fmDgDiffStation.Position.FullAddress))
                    CellAddress.Value = fmDgDiffStation.Position.FullAddress;

                // Форма Адреса місця встановлення РЕЗ
                FormNewPosition frm = new FormNewPosition(fmDgDiffStation.Position.CityId);
                    
                frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
                frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = fmDgDiffStation.Position;
                if (frm.ShowDialog() == DialogResult.OK)
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
            }
        }

        public void OnBeforeChangePowerTr(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeCheckNumber(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeTxBandwidth(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeTxDesignEmission(Cell cell, ref string val) { }

        public void OnBeforeChangeCertificateNumber(Cell cell, ref string val)
        {
            if (string.IsNullOrEmpty(CellCertificateNumber.Value))
                CellCertificateNumber.Value = "";
        }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            DateTime newVal;
            if (!DateTime.TryParse(val, out newVal) || newVal < DateTime.MinValue)
                val = cell.Value;
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        /// <summary>
        /// Реакция на нажатие кнопки создания адреса
        /// Создание нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(fmDgDiffStation.Position.CityId);
            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanAttachPhoto = true;
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                fmDgDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                frm.GetFotos(out listFotosDst, out listFotosSrc);
                if (!string.IsNullOrEmpty(fmDgDiffStation.Position.FullAddress))
                    CellAddress.Value = fmDgDiffStation.Position.FullAddress;
                CellASL.DoubleValue = fmDgDiffStation.Position.ASl;
            }
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellPowerTr.FormatString = "N3";
            CellPowerTr.Value2 = FillValue2(fmDgStation.Power[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerTr.DoubleValue = fmDgDiffStation.Power[PowerUnits.W].Round(3);
            CellPowerTr.SetRedValue();

            CellTxBandwidth.Value2 = FillValue2(fmDgStation.FmDgEquipment.Bandwidth.ToStringNullD());
            CellTxBandwidth.Value = fmDgDiffStation.FmDgDiffEquipment.Bandwidth.ToStringNullD();

            CellTxDesignEmission.Value2 = FillValue2(fmDgStation.FmDgEquipment.DesigEmission);
            CellTxDesignEmission.Value = fmDgDiffStation.FmDgDiffEquipment.DesigEmission;

            CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, fmDgDiffStation.FmDgDiffEquipment.Certificate);
            CellCertificateDate.SetRedValue();
        }

        //////////////////////////////////////////
        //Необхідні перевантажені методи
        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                AdminSiteAllTech.Show(fmDgStation.Position.TableName, fmDgStation.Position.Id);
            }
        }
        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            fmDgDiffStation.Id = recordID.Id;

            fmDgDiffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  fmDgDiffStation.Position,
                  ICSMTbl.itblPositionBro);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            fmDgDiffStation.ShiftOstov = CellEquipmentShiftOstov.DoubleValue;

            fmDgDiffStation.FmDgDiffEquipment.Name = CellEquipmentName.Value;
            fmDgDiffStation.FmDgDiffEquipment.MaxPower[PowerUnits.dBW] = CellPowerTr.DoubleValue;
            fmDgDiffStation.FmDgDiffEquipment.Bandwidth = CellTxBandwidth.DoubleValue;
            fmDgDiffStation.FmDgDiffEquipment.DesigEmission = CellTxDesignEmission.Value;
            fmDgDiffStation.FmDgDiffEquipment.Certificate.Symbol = CellCertificateNumber.Value;
            fmDgDiffStation.FmDgDiffEquipment.Certificate.Date = CellCertificateDate.DateValue;
            fmDgDiffStation.HeightAnten = CellHighAntenn.DoubleValue;
            fmDgDiffStation.Polar = CellAntenPolar.Value.Split(' ')[0].Trim();

            fmDgDiffStation.Direct = CellAntenDirect.Value.Split(' ')[0].Trim();

            fmDgDiffStation.ShiftOstov = CellEquipmentShiftOstov.DoubleValue;

            fmDgDiffStation.Block = CellBlock.Value;
            fmDgDiffStation.Centr = CellMaxFreq.DoubleValue;
            fmDgDiffStation.StatComment = Comment;
            fmDgDiffStation.Save();

            if (listFotosDst != null && listFotosSrc != null)
                fmDgDiffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, TvAnalogDiffStation.TableName, tvStation.Id);

            return true;
        }

        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            ReadAllDefaultDataPositions();

            if (fmDgDiffStation.IsEnableDiff)
            {
                CellAddress.Value = fmDgDiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = fmDgDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = fmDgDiffStation.Position.LatDms;
                CellASL.DoubleValue = fmDgDiffStation.Position.ASl;

                CellEquipmentShiftOstov.DoubleValue = fmDgDiffStation.ShiftOstov;
                CellEquipmentName.Value = fmDgDiffStation.FmDgDiffEquipment.Name;
                CellPowerTr.DoubleValue = fmDgDiffStation.FmDgDiffEquipment.MaxPower[PowerUnits.dBW];
                CellTxBandwidth.DoubleValue = fmDgDiffStation.FmDgDiffEquipment.Bandwidth;
                CellTxDesignEmission.Value = fmDgDiffStation.FmDgDiffEquipment.DesigEmission;
                CellCertificateNumber.Value = fmDgDiffStation.FmDgDiffEquipment.Certificate.Symbol;
                CellCertificateDate.DateValue = fmDgDiffStation.FmDgDiffEquipment.Certificate.Date;
                CellHighAntenn.DoubleValue = fmDgDiffStation.HeightAnten;

                CellAntenDirect.Value = fmDgDiffStation.Direct;
                CellAntenPolar.Value = fmDgDiffStation.Polar;
                CellAntenDirect.Value = fmDgDiffStation.Direct;
                CellAntenPolar.Value = fmDgDiffStation.Polar;

                CellEquipmentShiftOstov.DoubleValue = fmDgDiffStation.ShiftOstov;
                CellBlock.Value = fmDgDiffStation.Block;
                CellMaxFreq.DoubleValue = fmDgDiffStation.Centr;

                Comment = fmDgDiffStation.StatComment;
            } else
            {
                fmDgDiffStation.Position = fmDgStation.Position.Clone() as PositionState;
            }
            _initialized = true;

            OnRecalculateErp(null);
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.FmDigitalDiff;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (_initialized)
            {
                fmDgStation.Power[PowerUnits.W] = CellPowerTr.DoubleValue;
                fmDgStation.Polarization = CellAntenPolar.Value;
                fmDgStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!fmDgStation.HorizontalDiagramm.IsEmpty && fmDgStation.Polarization != "V")
                value = fmDgStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!fmDgStation.VerticalDiagramm.IsEmpty && fmDgStation.Polarization != "H")
                value = fmDgStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            fmDgStation.CalculateERP();
            if (fmDgStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = fmDgStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (fmDgStation.Erp.Vertical.IsValid)
                CellEVPV.Value = fmDgStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = fmDgStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}
