﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStationAppDiff : BaseDiffClass
    {

        private int ObjID = IM.NullI;

        private MobStation mobStation;
        private MobDiffStation mobDiffStation;

        private StationSectorCollection<MobStation> _mobStations;
        private StationSectorCollection<MobDiffStation> _mobDiffStations;

        //private int IDDiff = IM.NullI;
        //private int IDPos = IM.NullI;

        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();
        private CellDoubleProperty CellASL = new CellDoubleProperty();
        private CellStringProperty CellAddress = new CellStringProperty();
        private CellStringProperty CellEquipmentName = new CellStringProperty();
        //private CellDoubleProperty CellPowerTr = new CellDoubleProperty();
        private CellStringComboboxAplyProperty CellStandart = new CellStringComboboxAplyProperty();        /// <summary>
        private CellStringProperty CellRez = new CellStringProperty();        /// <summary>
        //private CellDoubleProperty CellBaseType = new CellDoubleProperty();
        private CellDoubleNullableProperty CellTransmitterCount = new CellDoubleNullableProperty();
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        private CellStringProperty CellFilterType1 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower1 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType1 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight1 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission1 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef1 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth1 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle1 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation1 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber1 = new CellStringProperty();
        private CellStringProperty CellRxFrequency1 = new CellStringProperty();
        private CellStringProperty CellTxFrequency1 = new CellStringProperty();
        private CellIntegerProperty CellObject1 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff1 = new CellIntegerProperty();

        private CellStringProperty CellFilterType2 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower2 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType2 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight2 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission2 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef2 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth2 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle2 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation2 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber2 = new CellStringProperty();
        private CellStringProperty CellRxFrequency2 = new CellStringProperty();
        private CellStringProperty CellTxFrequency2 = new CellStringProperty();
        private CellIntegerProperty CellObject2 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff2 = new CellIntegerProperty();

        private CellStringProperty CellFilterType3 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower3 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType3 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight3 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission3 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef3 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth3 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle3 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation3 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber3 = new CellStringProperty();
        private CellStringProperty CellRxFrequency3 = new CellStringProperty();
        private CellStringProperty CellTxFrequency3 = new CellStringProperty();
        private CellIntegerProperty CellObject3 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff3 = new CellIntegerProperty();

        private CellStringProperty CellFilterType4 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower4 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType4 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight4 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission4 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef4 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth4 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle4 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation4 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber4 = new CellStringProperty();
        private CellStringProperty CellRxFrequency4 = new CellStringProperty();
        private CellStringProperty CellTxFrequency4 = new CellStringProperty();
        private CellIntegerProperty CellObject4 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff4 = new CellIntegerProperty();

        private CellStringProperty CellFilterType5 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower5 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType5 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight5 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission5 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef5 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth5 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle5 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation5 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber5 = new CellStringProperty();
        private CellStringProperty CellRxFrequency5 = new CellStringProperty();
        private CellStringProperty CellTxFrequency5 = new CellStringProperty();
        private CellIntegerProperty CellObject5 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff5 = new CellIntegerProperty();

        private CellStringProperty CellFilterType6 = new CellStringProperty();
        private CellDoubleBoundedProperty CellPower6 = new CellDoubleBoundedProperty();
        private CellStringProperty CellAntennaType6 = new CellStringProperty();
        private CellDoubleBoundedProperty CellAntenaHeight6 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellEmission6 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaKoef6 = new CellDoubleProperty();
        private CellDoubleBoundedProperty CellAntenaWidth6 = new CellDoubleBoundedProperty();
        private CellDoubleProperty CellAntenaAngle6 = new CellDoubleProperty();
        private CellDoubleProperty CellAntenaAttenuation6 = new CellDoubleProperty();
        private CellStringProperty CellChannelNumber6 = new CellStringProperty();
        private CellStringProperty CellRxFrequency6 = new CellStringProperty();
        private CellStringProperty CellTxFrequency6 = new CellStringProperty();
        private CellIntegerProperty CellObject6 = new CellIntegerProperty();
        private CellIntegerProperty CellOnOff6 = new CellIntegerProperty();

        private CellStringProperty[] CellFilterType;
        private CellDoubleBoundedProperty[] CellPower;
        private CellStringProperty[] CellAntennaType;
        private CellDoubleBoundedProperty[] CellAntenaHeight;
        private CellDoubleBoundedProperty[] CellEmission;
        private CellDoubleProperty[] CellAntenaKoef;
        private CellDoubleBoundedProperty[] CellAntenaWidth;
        private CellDoubleProperty[] CellAntenaAngle;
        private CellDoubleProperty[] CellAntenaAttenuation;
        private CellStringProperty[] CellChannelNumber;
        private CellStringProperty[] CellRxFrequency;
        private CellStringProperty[] CellTxFrequency;
        private CellIntegerProperty[] CellObject;
        private CellIntegerProperty[] CellOnOff;

        public MobStationAppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobStaApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppRR;

            CellFilterType = new CellStringProperty[6]
                                 {
                                     CellFilterType1,
                                     CellFilterType2,
                                     CellFilterType3,
                                     CellFilterType4,
                                     CellFilterType5,
                                     CellFilterType6
                                 };
            CellPower = new CellDoubleBoundedProperty[6]
                            {
                                CellPower1,
                                CellPower2,
                                CellPower3,
                                CellPower4,
                                CellPower5,
                                CellPower6
                            };


            CellAntennaType = new CellStringProperty[6]
                                  {
                                      CellAntennaType1,
                                      CellAntennaType2,
                                      CellAntennaType3,
                                      CellAntennaType4,
                                      CellAntennaType5,
                                      CellAntennaType6
                                  };

            CellAntenaHeight = new CellDoubleBoundedProperty[6]
                                   {
                                       CellAntenaHeight1,
                                       CellAntenaHeight2,
                                       CellAntenaHeight3,
                                       CellAntenaHeight4,
                                       CellAntenaHeight5,
                                       CellAntenaHeight6
                                   };

            CellEmission = new CellDoubleBoundedProperty[6]
                               {
                                   CellEmission1,
                                   CellEmission2,
                                   CellEmission3,
                                   CellEmission4,
                                   CellEmission5,
                                   CellEmission6
                               };
            CellAntenaKoef = new CellDoubleProperty[6]
                                 {
                                     CellAntenaKoef1,
                                     CellAntenaKoef2,
                                     CellAntenaKoef3,
                                     CellAntenaKoef4,
                                     CellAntenaKoef5,
                                     CellAntenaKoef6

                                 };
            CellAntenaWidth = new CellDoubleBoundedProperty[6]
                                  {
                                      CellAntenaWidth1,
                                      CellAntenaWidth2,
                                      CellAntenaWidth3,
                                      CellAntenaWidth4,
                                      CellAntenaWidth5,
                                      CellAntenaWidth6
                                  };
            CellAntenaAngle = new CellDoubleProperty[6]
                                  {
                                      CellAntenaAngle1,
                                      CellAntenaAngle2,
                                      CellAntenaAngle3,
                                      CellAntenaAngle4,
                                      CellAntenaAngle5,
                                      CellAntenaAngle6
                                  };
            CellAntenaAttenuation = new CellDoubleProperty[6]
                                        {
                                            CellAntenaAttenuation1,
                                            CellAntenaAttenuation2,
                                            CellAntenaAttenuation3,
                                            CellAntenaAttenuation4,
                                            CellAntenaAttenuation5,
                                            CellAntenaAttenuation6
                                        };
            CellChannelNumber = new CellStringProperty[6]
                                    {
                                        CellChannelNumber1,
                                        CellChannelNumber2,
                                        CellChannelNumber3,
                                        CellChannelNumber4,
                                        CellChannelNumber5,
                                        CellChannelNumber6
                                    };
            CellRxFrequency = new CellStringProperty[6]
                                  {
                                      CellRxFrequency1,
                                      CellRxFrequency2,
                                      CellRxFrequency3,
                                      CellRxFrequency4,
                                      CellRxFrequency5,
                                      CellRxFrequency6
                                  };
            CellTxFrequency = new CellStringProperty[6]
                                  {
                                      CellTxFrequency1,
                                      CellTxFrequency2,
                                      CellTxFrequency3,
                                      CellTxFrequency4,
                                      CellTxFrequency5,
                                      CellTxFrequency6
                                  };
            CellObject = new CellIntegerProperty[6]
                             {
                                 CellObject1,
                                 CellObject2,
                                 CellObject3,
                                 CellObject4,
                                 CellObject5,
                                 CellObject6
                             };

            CellOnOff = new CellIntegerProperty[6]
                            {
                                CellOnOff1,
                                CellOnOff2,
                                CellOnOff3,
                                CellOnOff4,
                                CellOnOff5,
                                CellOnOff6
                            };

            //Стандартные значения
            _mobStations = new StationSectorCollection<MobStation>();
            _mobStations.ApplID = applID;
            _mobStations.LoadSectorsOrSingle(id);
            mobStation = _mobStations.GetSectorByIndex(0);

            _mobDiffStations = new StationSectorCollection<MobDiffStation>();
            _mobDiffStations.ApplID = applID;
            _mobDiffStations.LoadSectorsOrSingle(id);
            mobDiffStation = _mobDiffStations.GetSectorByIndex(0);

            SCVisnovok = mobStation.Finding;

            Comment = mobDiffStation.StatComment;
            CellCertificateDate.NullString = "-";
            _stationObject = mobStation;

            for (int i = 0; i < 6; i++) 
            {
                CellPower[i].LowerBound = 0.0;
                CellEmission[i].LowerBound = 0.0;
                CellEmission[i].UpperBound = 360.0;
                CellAntenaWidth[i].LowerBound = 0.0;
                CellAntenaWidth[i].UpperBound = 360.0;
                CellAntenaHeight[i].LowerBound = -200.0;
                CellAntenaHeight[i].UpperBound = 500.0;
            }
        }

        /// <summary>        
        /// Формування адресу сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            // Обираємо запис з таблиці                     
            NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");
            //IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionMw, 1, tmpXY.Longitude, tmpXY.Latitude);

            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionWim, 1, tmpXY.Longitude, tmpXY.Latitude);
            if (objPosition != null)
            {
                mobDiffStation.Position = new PositionState();
                mobDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();

                CellLongitude.DoubleValue = mobDiffStation.Position.LongDms;
                CellLatitude.DoubleValue = mobDiffStation.Position.LatDms;
                CellASL.DoubleValue = mobDiffStation.Position.ASl;
                CellAddress.Value = mobDiffStation.Position.FullAddress;

                FormNewPosition frm = new FormNewPosition(mobDiffStation.Position.CityId);
                frm.SetLongitudeAsDms(mobDiffStation.Position.LonDms);
                frm.SetLatitudeAsDms(mobDiffStation.Position.LatDms);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = mobDiffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
                }
            }
        }


        public override void UpdateToNewPacket()
        {
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "addr1-New")
            {
                AdminSiteAllTech.Show(mobStation.Position.TableName, mobStation.Position.Id);
            }
        }


        protected override bool SaveAppl()
        {
            if (!gridParam.IsValid)
            {
                MessageBox.Show("Невірно введені дані", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            StationSectorCollection<MobDiffStation> savedMobDiff = new StationSectorCollection<MobDiffStation>();
            {
                int sectorCount = _mobStations.GetSectorCount();

                mobStation.Position =
                CoordinateDiffHelper.CheckPosition(
                      CellLongitude,
                      CellLatitude,
                      mobDiffStation.Position,
                      ICSMTbl.itblPositionWim);
                
                for (int i = 0; i < sectorCount; i++)
                {
                    MobStation mobSector = _mobStations.GetSectorByIndex(i);
                    MobDiffStation mobDiffSector = new MobDiffStation();
                    mobDiffSector.Id = mobSector.Id;

                    mobDiffSector.Position = mobStation.Position;
                    mobDiffSector.Equipment.Id = mobSector.Equipment.Id;
                    mobDiffSector.Equipment.Certificate.Symbol = certificate.Symbol;
                    mobDiffSector.Equipment.Certificate.Date = certificate.Date;
                    mobDiffSector.Equipment.Name = CellEquipmentName.Value;
                    mobDiffSector.Position = mobStation.Position;
                    mobDiffSector.Agl = CellAntenaHeight[i].DoubleValue;
                    mobDiffSector.Gain = CellAntenaKoef[i].DoubleValue;
                    mobDiffSector.TransmitterCount = Convert.ToInt32(CellTransmitterCount.DoubleValue);
                    mobDiffSector.AntenaWidth = CellAntenaWidth[i].DoubleValue;
                    mobDiffSector.Power[PowerUnits.W] = CellPower[i].DoubleValue;
                    mobDiffSector.Antenna.Name = CellAntennaType[i].Value;
                    mobDiffSector.Azimuth = CellEmission[i].DoubleValue;
                    mobDiffSector.DesigEmission = CellTxDesignEmission.Value;

                    if (CellTxFrequency[i].Value != CellTxFrequency[i].Value2)
                        mobDiffSector.Plan.TxFrequency = ConvertType.ToDoubleList(CellTxFrequency[i].Value);
                    else
                        mobDiffSector.Plan.TxFrequency = mobSector.Plan.TxFrequency;

                    if (CellRxFrequency[i].Value != CellRxFrequency[i].Value2)
                        mobDiffSector.Plan.RxFrequency = ConvertType.ToDoubleList(CellRxFrequency[i].Value);
                    else
                        mobDiffSector.Plan.RxFrequency = mobSector.Plan.RxFrequency;

                    mobDiffSector.OnOff = Convert.ToInt32(CellOnOff[i].Value);
                    mobDiffSector.StatComment = Comment;
                    savedMobDiff.ApplID = applID;
                    savedMobDiff.Add(mobDiffSector);
                }
                savedMobDiff.SaveSectorsOrSingle(true);
            }

            if (listFotosDst != null && listFotosSrc != null)
                mobStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, MobStation.TableName, mobStation.Id);
            return true;
        }

        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public override void InitParamGrid(Cell cell)
        {

        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < 6; i++)
            {
                if (i >= sectorCount)
                {
                    CellFilterType[i].Cell.Visible = false;
                    CellPower[i].Cell.Visible = false;
                    CellAntennaType[i].Cell.Visible = false;
                    CellAntenaHeight[i].Cell.Visible = false;
                    CellEmission[i].Cell.Visible = false;
                    CellAntenaKoef[i].Cell.Visible = false;
                    CellAntenaWidth[i].Cell.Visible = false;
                    CellAntenaAngle[i].Cell.Visible = false;
                    CellAntenaAttenuation[i].Cell.Visible = false;
                    CellChannelNumber[i].Cell.Visible = false;
                    CellRxFrequency[i].Cell.Visible = false;
                    CellTxFrequency[i].Cell.Visible = false;
                    CellObject[i].Cell.Visible = false;
                    CellOnOff[i].Cell.Visible = false;

                    grid.GetCellFromKey("SectorHeader-" + (i + 1).ToString()).Visible = false;
                }
            }

            CellTransmitterCount.NullString = "Згідно комплектації базової станції";
            InitStationObject();
        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStationAppDiff;
        }

        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(mobDiffStation.Position.CityId);
            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanEditPosition = true;
            frm.CanAttachPhoto = true;
            frm.CanEditStreet = true;
            frm.OldPosition = mobDiffStation.Position;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                mobDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                CellAddress.Value = mobDiffStation.Position.FullAddress;
                frm.GetFotos(out listFotosDst, out listFotosSrc);
            }
        }

        public void InitStationObject()
        {
            CellLongitude.DoubleValue = mobStation.Position.LonDms;
            CellLatitude.DoubleValue = mobStation.Position.LatDms;
            CellASL.DoubleValue = mobStation.Position.ASl;
            CellAddress.Value = mobStation.Position.FullAddress;

            CellLongitude.DoubleValue2 = mobStation.Position.LonDms;
            CellLatitude.DoubleValue2 = mobStation.Position.LatDms;
            CellASL.DoubleValue2 = mobStation.Position.ASl;
            CellAddress.Value2 = mobStation.Position.FullAddress;

            CellEquipmentName.Value = mobStation.Equipment.Name;
            CellEquipmentName.Value2 = mobStation.Equipment.Name;
            CellEquipmentName.SetRedValue();
            CellTxDesignEmission.Value = mobStation.DesigEmission;
            CellTxDesignEmission.Value2 = mobStation.DesigEmission;
            CellTxDesignEmission.SetRedValue();

            CellCertificateNumber.Value = mobStation.Equipment.Certificate.Symbol;
            CellCertificateNumber.Value2 = mobStation.Equipment.Certificate.Symbol;
            CellCertificateNumber.SetRedValue();

            CellCertificateDate.DateValue = mobStation.Equipment.Certificate.Date;
            CellCertificateDate.DateValue2 = mobStation.Equipment.Certificate.Date;
            CellCertificateDate.SetRedValue();

            CellTransmitterCount.DoubleValue = mobStation.TransmitterCount;
            CellTransmitterCount.DoubleValue2 = mobStation.TransmitterCount;
            CellTransmitterCount.SetRedValue();

            CellStandart.Value = mobStation.Plan.Standard;
            CellRez.Value = mobStation.Name;

            CellStandart.CanEdit = false;
            CellRez.CanEdit = false;
            CellASL.CanEdit = false;

            int sectorCount = _mobStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation mobSector = _mobStations.GetSectorByIndex(i);

                CellFilterType[i].Value = mobSector.Filter.Name;
                CellFilterType[i].Value2 = mobSector.Filter.Name;
                CellFilterType[i].SetRedValue();
                CellFilterType[i].CanEdit = false;

                CellPower[i].DoubleValue = IM.RoundDeci(mobSector.Power[PowerUnits.W], 3);
                CellPower[i].DoubleValue2 = IM.RoundDeci(mobSector.Power[PowerUnits.W], 3);
                CellPower[i].SetRedValue();

                CellAntennaType[i].Value = mobSector.Antenna.Name;
                CellAntennaType[i].Value2 = mobSector.Antenna.Name;
                CellAntennaType[i].SetRedValue();

                CellEmission[i].DoubleValue = mobSector.Azimuth;
                CellEmission[i].DoubleValue2 = mobSector.Azimuth;
                CellEmission[i].SetRedValue();

                CellAntenaAngle[i].DoubleValue = mobSector.Elevation;
                CellAntenaAngle[i].DoubleValue2 = mobSector.Elevation;
                //CellAntenaAngle[i].SetRedValue();

                CellAntenaHeight[i].DoubleValue = mobSector.Agl;
                CellAntenaHeight[i].DoubleValue2 = mobSector.Agl;
                CellAntenaHeight[i].SetRedValue();

                CellAntenaKoef[i].DoubleValue = mobSector.Gain;
                CellAntenaKoef[i].DoubleValue2 = mobSector.Gain;
                CellAntenaKoef[i].SetRedValue();

                CellAntenaWidth[i].DoubleValue = mobSector.Antenna.AntennaWidth;
                CellAntenaWidth[i].DoubleValue2 = mobSector.Antenna.AntennaWidth;
                CellAntenaWidth[i].SetRedValue();

                CellAntenaAttenuation[i].DoubleValue = mobSector.TxLosses;
                CellAntenaAttenuation[i].DoubleValue2 = mobSector.TxLosses;
                //CellAntenaAttenuation[i].SetRedValue();

                CellAntenaAttenuation[i].CanEdit = false;
                CellAntenaAngle[i].CanEdit = false;

                CellChannelNumber[i].Value = mobSector.Plan.ChannelListAsString;
                CellChannelNumber[i].Value2 = mobSector.Plan.ChannelListAsString;
                CellChannelNumber[i].SetRedValue();

                CellRxFrequency[i].Value = mobSector.Plan.RxFrequencyListAsString;
                CellRxFrequency[i].Value2 = mobSector.Plan.RxFrequencyListAsString;
                CellRxFrequency[i].SetRedValue();

                CellTxFrequency[i].Value = mobSector.Plan.TxFrequencyListAsString;
                CellTxFrequency[i].Value2 = mobSector.Plan.TxFrequencyListAsString;
                CellTxFrequency[i].SetRedValue();

                CellObject[i].IntValue = mobSector.Id;
                CellObject[i].CanEdit = false;

                CellOnOff[i].Value = "1";
                CellOnOff[i].CanEdit = false;
            }

            if (mobDiffStation != null && mobDiffStation.IsInitialized)
            {
                CellLongitude.DoubleValue = mobDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = mobDiffStation.Position.LatDms;
                CellASL.DoubleValue = mobDiffStation.Position.ASl;
                CellAddress.Value = mobDiffStation.Position.FullAddress;

                CellEquipmentName.Value = mobDiffStation.Equipment.Name;
                CellTxDesignEmission.Value = mobDiffStation.DesigEmission;

                //CellCertificateNumber.Value = mobDiffStation.Equipment.Certificate.Symbol;
                //CellCertificateDate.DateValue = mobDiffStation.Equipment.Certificate.Date;
                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate,
                                                        mobDiffStation.Equipment.Certificate);

                CellTransmitterCount.DoubleValue = mobDiffStation.TransmitterCount;

                for (int i = 0; i < sectorCount; i++)
                {
                    MobDiffStation mobDiffSector = _mobDiffStations.GetSectorByIndex(i);
                    MobStation mobSector = _mobStations.GetSectorByIndex(i);

                    CellPower[i].DoubleValue = IM.RoundDeci(mobDiffSector.Power[PowerUnits.W], 3);
                    CellAntennaType[i].Value = mobDiffSector.Antenna.Name;
                    CellEmission[i].DoubleValue = mobDiffSector.Azimuth;
                    //CellAntenaAngle[i].DoubleValue = mobDiffSector.Elevation;
                    CellAntenaHeight[i].DoubleValue = mobDiffSector.Agl;
                    CellAntenaKoef[i].DoubleValue = mobDiffSector.Gain;
                    CellAntenaWidth[i].DoubleValue = mobDiffSector.Antenna.AntennaWidth;
                    //CellAntenaAttenuation[i].DoubleValue = mobDiffSector.TxLosses;                                                         
                    CellOnOff[i].Value = mobDiffSector.OnOff.ToString();

                    if (!HelpFunction.AreSameDoubleLists(mobDiffSector.Plan.TxFrequency, mobSector.Plan.TxFrequency))
                        CellTxFrequency[i].Value = mobDiffSector.Plan.TxFrequencyListAsString;
                    else
                        CellTxFrequency[i].Value = mobSector.Plan.TxFrequencyListAsString;

                    ///if (mobDiffSector.Plan.RxFrequencyListAsString!=mobSector.Plan.RxFrequencyListAsString)
                    if (!HelpFunction.AreSameDoubleLists(mobDiffSector.Plan.RxFrequency, mobSector.Plan.RxFrequency))
                        CellRxFrequency[i].Value = mobDiffSector.Plan.RxFrequencyListAsString;
                    else
                        CellRxFrequency[i].Value = mobSector.Plan.RxFrequencyListAsString;
                }
            }
            else
            {
                mobDiffStation.Position = mobStation.Position.Clone() as PositionState;
            }

            for (int i = 0; i < sectorCount; i++)
            {
                CellChannelNumber[i].CanEdit = false;
                CellTxFrequency[i].CanEdit = (radioTech == "CDMA-800");
                CellRxFrequency[i].CanEdit = (radioTech == "CDMA-800");
            }
        }

        public void OnBeforeChangeTxFrequency(Cell cell, ref string NewValue)
        {
        }

        public void OnBeforeChangeRxFrequency(Cell cell, ref string NewValue)
        {
        }
    }
}
