﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
   internal class MicrowaveAppDiff : BaseDiffClass
   {
      protected List<string>[] stListFotosDst = new List<string>[2];
      protected List<string>[] stListFotosSrc = new List<string>[2];

      private const string positionTable = ICSMTbl.itblPositionMw;
      private PositionState position = new PositionState();


      private int ObjID = IM.NullI;

      private MicrowaveLink microwaveLink;
      private MicrowaveLinkDiff microwaveLinkDiff;

      private StationSectorCollection<MicrowaveLink> microwaveLinks;
      private StationSectorCollection<MicrowaveLinkDiff> microwaveLinksDiff;

      private int IDDiff = IM.NullI;
      //private int IDPos = IM.NullI;

      private CellDoubleProperty CellRange = new CellDoubleProperty(); // Відстань
      private CellStringProperty CellEquipmentName = new CellStringProperty(); //  Назва/Тип РЕЗ      
      private CellDoubleProperty CellBandwidth = new CellDoubleProperty(); // Ширина смуги , МГц; 
      private CellStringProperty CellDesigEmission = new CellStringProperty(); // клас випромінювання                
      private CellStringProperty CellModulation = new CellStringProperty(); // модуляція
      private CellStringProperty CellCertificateNumber = new CellStringProperty(); // Номер сертифіката відповідності
      private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty(); // Дата сертифіката відповідності
      private CellIntegerProperty CellObjectID = new CellIntegerProperty(); // Об'єкт

      private CellLongitudeProperty CellLongitude1 = new CellLongitudeProperty(); // Широта (гр.хв.сек.)
      private CellLatitudeProperty CellLatitude1 = new CellLatitudeProperty(); // Довгота (гр.хв.сек.)      
      private CellDoubleProperty CellASL1 = new CellDoubleProperty(); // Висота на рівнем моря
      private CellDoubleBoundedProperty CellAzimuth1 = new CellDoubleBoundedProperty(); // Кут азимуту випромінювання
      private CellStringProperty CellAddress1 = new CellStringProperty(); // Адреса місця встановлення РЕЗ      

      private CellDoubleProperty CellMaxPower1 = new CellDoubleProperty();
                                 // Максимальна потужність передавача              

      private CellStringProperty CellAntennaName1 = new CellStringProperty(); // Тип антени
      private CellDoubleBoundedProperty CellAGL1 = new CellDoubleBoundedProperty(); // Висота антени над рівнем землі
      private CellDoubleBoundedProperty CellDiameter1 = new CellDoubleBoundedProperty(); // Діаметр
      private CellDoubleProperty CellGain1 = new CellDoubleProperty(); // Коефіцієнт підсилення
      private CellStringProperty CellPolarization1 = new CellStringProperty(); // Поляризація
      private CellDoubleProperty CellFrequency1 = new CellDoubleProperty(); // Номінал частот передавання
      private CellDoubleProperty CellFrequency1Sector2 = new CellDoubleProperty(); // Номінал частот передавання
      private CellDoubleProperty CellFrequency1Sector3 = new CellDoubleProperty(); // Номінал частот передавання
      private CellDoubleProperty CellFrequency1Sector4 = new CellDoubleProperty(); // Номінал частот передавання
      private CellDoubleProperty CellFrequency1Sector5 = new CellDoubleProperty(); // Номінал частот передавання
      private CellDoubleProperty CellFrequency1Sector6 = new CellDoubleProperty(); // Номінал частот передавання

      private CellDoubleProperty[] CellFrequency1Array;

      private CellLongitudeProperty CellLongitude2 = new CellLongitudeProperty();       // Широта (гр.хв.сек.)
      private CellLatitudeProperty CellLatitude2 = new CellLatitudeProperty();          // Довгота (гр.хв.сек.)      
      private CellDoubleProperty CellASL2 = new CellDoubleProperty();                   // Висота на рівнем моря
      private CellDoubleBoundedProperty CellAzimuth2 = new CellDoubleBoundedProperty();             // Кут азимуту випромінювання
      private CellStringProperty CellAddress2 = new CellStringProperty();               // Адреса місця встановлення РЕЗ      
      private CellDoubleProperty CellMaxPower2 = new CellDoubleProperty();              // Максимальна потужність передавача              
      private CellStringProperty CellAntennaName2 = new CellStringProperty();           // Тип антени
      private CellDoubleBoundedProperty CellAGL2 = new CellDoubleBoundedProperty();                   // Висота антени над рівнем землі
      private CellDoubleBoundedProperty CellDiameter2 = new CellDoubleBoundedProperty();              // Діаметр
      private CellDoubleProperty CellGain2 = new CellDoubleProperty();                  // Коефіцієнт підсилення
      private CellStringProperty CellPolarization2 = new CellStringProperty();          // Поляризація
      private CellDoubleProperty CellFrequency2 = new CellDoubleProperty();             // Номінал частот передавання
      private CellDoubleProperty CellFrequency2Sector2 = new CellDoubleProperty();      // Номінал частот передавання
      private CellDoubleProperty CellFrequency2Sector3 = new CellDoubleProperty();      // Номінал частот передавання
      private CellDoubleProperty CellFrequency2Sector4 = new CellDoubleProperty();      // Номінал частот передавання
      private CellDoubleProperty CellFrequency2Sector5 = new CellDoubleProperty();      // Номінал частот передавання
      private CellDoubleProperty CellFrequency2Sector6 = new CellDoubleProperty();      // Номінал частот передавання

      private CellDoubleProperty[] CellFrequency2Array;

      //===================================================
      public MicrowaveAppDiff(int id, int ownerId, int packetID, string radioTech)
         : base(id, MicrowaveApp.TableName, ownerId, packetID, radioTech)
      {
                  
         ObjID = id;
         appType = AppType.AppRS;

         CellAzimuth1.UpperBound = 360;
         CellAzimuth1.LowerBound = -180;
         CellAzimuth2.UpperBound = 360;
         CellAzimuth2.LowerBound = -180;

         CellAGL1.LowerBound = -200;
         CellAGL2.LowerBound = -200;
         CellAGL1.UpperBound = 500;
         CellAGL2.UpperBound = 500;
         CellDiameter1.LowerBound = 0;         
         CellDiameter2.LowerBound = 0;

         CellFrequency1Array = new CellDoubleProperty[6]
                              {
                                 CellFrequency1,
                                 CellFrequency1Sector2,
                                 CellFrequency1Sector3,
                                 CellFrequency1Sector4,
                                 CellFrequency1Sector5,
                                 CellFrequency1Sector6
                              };

         CellFrequency2Array = new CellDoubleProperty[6]
                              {
                                 CellFrequency2,
                                 CellFrequency2Sector2,
                                 CellFrequency2Sector3,
                                 CellFrequency2Sector4,
                                 CellFrequency2Sector5,
                                 CellFrequency2Sector6
                              };

         microwaveLinks = new StationSectorCollection<MicrowaveLink>();
         microwaveLinks.ApplID = applID;
         microwaveLinks.LoadSectorsOrSingle(id);
         microwaveLink = microwaveLinks.GetSectorByIndex(0);

         SCVisnovok = microwaveLink.Finding;

         microwaveLinksDiff = new StationSectorCollection<MicrowaveLinkDiff>();         
         microwaveLinksDiff.ApplID = applID;
         microwaveLinksDiff.LoadSectorsOrSingle(id);
         microwaveLinkDiff = microwaveLinksDiff.GetSectorByIndex(0);

         Comment = microwaveLinkDiff.StatComment;
         CellCertificateDate.NullString = "-";

         _stationObject = microwaveLink;
      }          

      //===========================================================
      /// <summary>
      /// Just Legacy
      /// </summary>
      public override void InitParamGrid(Cell cell)
      {                   
      }

      //===========================================================
      /// <summary>
      /// Just Legacy
      /// </summary>
      public override void InitParamGrid(Grid grid)
      {
         gridParam = grid;
         InitStationObject();
         //BindGrid(grid);
         int SectorCount = microwaveLinks.GetSectorCount();
         if (SectorCount>0)
         {
            CellObjectID.Cell.Visible = false;
            grid.GetCellFromKey("mKObj").Visible = false;
         }

         for (int i = SectorCount; i < 6; i++)
         {
            string cellKey1 = "kFreqNom" + i.ToString();
            Cell normCell = grid.GetCellFromKey(cellKey1);
            normCell.row.Visible = false;                       
         }         

          for (int i = 0; i < SectorCount; i++)
          {
              string cellKey1 = "kFreqNom" + i.ToString();
              Cell normCell = grid.GetCellFromKey(cellKey1);
              normCell.Value = normCell.Value + " (ID " + microwaveLinks.GetSectorByIndex(i).Id + ")";           
          }
      }

      //===========================================================
      /// <summary>
      /// Will be changed...
      /// </summary>
      protected override string GetXMLParamGrid()
      {
         return XICSM.UcrfRfaNET.Properties.Resources.MicrowaveAppDiff;
      }

      /// <summary>
      /// Jyst stub
      /// </summary>
      public override void UpdateToNewPacket()
      {
      }

      /// <summary>
      /// Legacy
      /// </summary>
      /// <param name="docType"></param>
      /// <param name="number"></param>
      /// <returns></returns>
      protected override string getOutDocName(string docType, string number)
      {
         return "";
      }

      protected override void ShowObject(Cell cell)
      {
          if (cell.Key == "addr1")
          {
              AdminSiteAllTech.Show(microwaveLink.Stations[0].Position.TableName, microwaveLink.Stations[0].Position.Id);
          }
          if (cell.Key == "addr2")
          {
              AdminSiteAllTech.Show(microwaveLink.Stations[1].Position.TableName, microwaveLink.Stations[1].Position.Id);
          }
      }


      /// <summary>
      /// Save differences
      /// </summary>
      /// <returns>true if successfull</returns>
      protected override bool SaveAppl()
      {
          if (!gridParam.IsValid)
          {
              MessageBox.Show("Невірно введені дані", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return false;
          }
          
          if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, microwaveLinkDiff.Equipment.Certificate))
          {
              MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return false;              
          }

          microwaveLinkDiff.Stations[0].Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude1,
                  CellLatitude1,
                  microwaveLinkDiff.Stations[0].Position,
                  ICSMTbl.itblPositionMw);

          //microwaveLink.Stations[0].Position = microwaveLinkDiff.Stations[0].Position;

          microwaveLinkDiff.Stations[1].Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude2,
                  CellLatitude2,
                  microwaveLinkDiff.Stations[1].Position,
                  ICSMTbl.itblPositionMw);

          //Prevent second!
          //microwaveLink.Stations[1].Position = microwaveLinkDiff.Stations[1].Position;

                                                                      //ICSMTbl.itblMicrows, microwaveLinkDiff.Stations[1].Id);
          // Stations[1].Id);

         microwaveLinkDiff.Stations[0].Azimuth = CellAzimuth1.DoubleValue;
         microwaveLinkDiff.Stations[0].Power[PowerUnits.dBW] = CellMaxPower1.DoubleValue;
         microwaveLinkDiff.Stations[0].AGL = CellAGL1.DoubleValue;
         microwaveLinkDiff.Stations[0].Gain = CellGain1.DoubleValue;
         microwaveLinkDiff.Stations[0].Antenna.Name = CellAntennaName1.Value;
         microwaveLinkDiff.Stations[0].Antenna.Diameter = CellDiameter1.DoubleValue;
         microwaveLinkDiff.Stations[0].Polarization = CellPolarization1.Value;
         microwaveLinkDiff.Stations[0].Frequency = CellFrequency1.DoubleValue;

         microwaveLinkDiff.Stations[1].Azimuth = CellAzimuth2.DoubleValue;
         microwaveLinkDiff.Stations[1].Power[PowerUnits.dBW] = CellMaxPower2.DoubleValue;
         microwaveLinkDiff.Stations[1].AGL = CellAGL2.DoubleValue;
         microwaveLinkDiff.Stations[1].Gain = CellGain2.DoubleValue;
         microwaveLinkDiff.Stations[1].Antenna.Name = CellAntennaName2.Value;
         microwaveLinkDiff.Stations[1].Antenna.Diameter = CellDiameter2.DoubleValue;
         microwaveLinkDiff.Stations[1].Polarization = CellPolarization2.Value;
         microwaveLinkDiff.Stations[1].Frequency = CellFrequency2.DoubleValue;

         microwaveLinkDiff.Equipment.Bandwidth = CellBandwidth.DoubleValue.MulNullD(1000);
         microwaveLinkDiff.Equipment.DesigEmission = CellDesigEmission.Value;
         microwaveLinkDiff.Equipment.Modulation = CellModulation.Value;         
         microwaveLinkDiff.Equipment.Name = CellEquipmentName.Value;

         int sectorCount = microwaveLinks.GetSectorCount();
         microwaveLinksDiff.Clear();
         for (int i = 0; i < sectorCount; i++)
         {
            MicrowaveLinkDiff cloneDiff = microwaveLinkDiff.Clone() as MicrowaveLinkDiff;
            cloneDiff.Id = microwaveLinks.GetSectorByIndex(i).Id;
                                       
            cloneDiff.Stations[0].Frequency = CellFrequency1Array[i].DoubleValue;
            cloneDiff.Stations[1].Frequency = CellFrequency2Array[i].DoubleValue;

            cloneDiff.StatComment = Comment;
            microwaveLinksDiff.Add(cloneDiff);
         }
         
         microwaveLinksDiff.SaveSectorsOrSingle(true);

         microwaveLinkDiff = microwaveLinksDiff.GetSectorByIndex(0);
         if (stListFotosDst[0] != null && stListFotosSrc[0] != null)
             microwaveLinkDiff.Stations[0].Position.SaveFoto(stListFotosDst[0], stListFotosSrc[0]);//
         //ICSMTbl.itblMicrows, microwaveLinkDiff.Stations[0].Id);

         if (stListFotosDst[1] != null && stListFotosSrc[1] != null)
             microwaveLinkDiff.Stations[1].Position.SaveFoto(stListFotosDst[1], stListFotosSrc[1]);

         return true;
      }

      /// <summary>
      /// 
      /// </summary>
      private void InitStationObject()
      {
          //Value
          CellLongitude1.DoubleValue = microwaveLink.Stations[0].Position.LonDms;
          CellLatitude1.DoubleValue = microwaveLink.Stations[0].Position.LatDms;
          CellAddress1.Value = microwaveLink.Stations[0].Position.FullAddress;
          CellASL1.DoubleValue = microwaveLink.Stations[0].Position.ASl;
          CellAzimuth1.DoubleValue = microwaveLink.Stations[0].Azimuth;
          CellMaxPower1.DoubleValue = microwaveLink.Stations[0].Power[PowerUnits.dBW];
          CellAGL1.DoubleValue = microwaveLink.Stations[0].AGL;
          CellGain1.DoubleValue = microwaveLink.Stations[0].Gain;
          CellAntennaName1.Value = microwaveLink.Stations[0].Antenna.Name;
          CellDiameter1.DoubleValue = microwaveLink.Stations[0].Antenna.Diameter;
          CellPolarization1.Value = microwaveLink.Stations[0].Polarization;

          //Second station
          CellLongitude2.DoubleValue = microwaveLink.Stations[1].Position.LonDms;
          CellLatitude2.DoubleValue = microwaveLink.Stations[1].Position.LatDms;
          CellAddress2.Value = microwaveLink.Stations[1].Position.FullAddress;
          CellASL2.DoubleValue = microwaveLink.Stations[1].Position.ASl;
          CellAzimuth2.DoubleValue = microwaveLink.Stations[1].Azimuth;
          CellMaxPower2.DoubleValue = microwaveLink.Stations[1].Power[PowerUnits.dBW];
          CellAGL2.DoubleValue = microwaveLink.Stations[1].AGL;
          CellGain2.DoubleValue = microwaveLink.Stations[1].Gain;
          CellAntennaName2.Value = microwaveLink.Stations[1].Antenna.Name;
          CellDiameter2.DoubleValue = microwaveLink.Stations[1].Antenna.Diameter;
          CellPolarization2.Value = microwaveLink.Stations[1].Polarization;

          //Value2
          CellLongitude1.DoubleValue2 = microwaveLink.Stations[0].Position.LonDms;
          CellLatitude1.DoubleValue2 = microwaveLink.Stations[0].Position.LatDms;
          CellAddress1.Value2 = microwaveLink.Stations[0].Position.FullAddress;
          CellASL1.DoubleValue2 = microwaveLink.Stations[0].Position.ASl;

          CellAzimuth1.DoubleValue2 = microwaveLink.Stations[0].Azimuth;
          CellMaxPower1.DoubleValue2 = microwaveLink.Stations[0].Power[PowerUnits.dBW];
          CellAGL1.DoubleValue2 = microwaveLink.Stations[0].AGL;
          CellGain1.DoubleValue2 = microwaveLink.Stations[0].Gain;
          CellAntennaName1.Value2 = microwaveLink.Stations[0].Antenna.Name;
          CellDiameter1.DoubleValue2 = microwaveLink.Stations[0].Antenna.Diameter;
          CellPolarization1.Value2 = microwaveLink.Stations[0].Polarization;

          CellAzimuth1.SetRedValue();
          CellMaxPower1.SetRedValue();
          CellAGL1.SetRedValue();
          CellGain1.SetRedValue();
          CellAntennaName1.SetRedValue();
          CellDiameter1.SetRedValue();
          CellPolarization1.SetRedValue();

          //Second station
          CellLongitude2.DoubleValue2 = microwaveLink.Stations[1].Position.LonDms;
          CellLatitude2.DoubleValue2 = microwaveLink.Stations[1].Position.LatDms;
          CellAddress2.Value2 = microwaveLink.Stations[1].Position.FullAddress;
          CellASL2.DoubleValue2 = microwaveLink.Stations[1].Position.ASl;

          CellAzimuth2.DoubleValue2 = microwaveLink.Stations[1].Azimuth;
          CellMaxPower2.DoubleValue2 = microwaveLink.Stations[1].Power[PowerUnits.dBW];
          CellAGL2.DoubleValue2 = microwaveLink.Stations[1].AGL;
          CellGain2.DoubleValue2 = microwaveLink.Stations[1].Gain;
          CellAntennaName2.Value2 = microwaveLink.Stations[1].Antenna.Name;
          CellDiameter2.DoubleValue2 = microwaveLink.Stations[1].Antenna.Diameter;
          CellPolarization2.Value2 = microwaveLink.Stations[1].Polarization;

          CellAzimuth2.SetRedValue();
          CellMaxPower2.SetRedValue();
          CellAGL2.SetRedValue();
          CellGain2.SetRedValue();
          CellAntennaName2.SetRedValue();
          CellDiameter2.SetRedValue();
          CellPolarization2.SetRedValue();

          CellEquipmentName.Value = microwaveLink.Equipment.Name;
          CellBandwidth.DoubleValue = microwaveLink.Equipment.Bandwidth.DivNullD(1000.0);
          CellDesigEmission.Value = microwaveLink.Equipment.DesigEmission;
          CellModulation.Value = microwaveLink.Equipment.Modulation;

          CellEquipmentName.Value2 = microwaveLink.Equipment.Name;
          CellBandwidth.DoubleValue2 = microwaveLink.Equipment.Bandwidth.DivNullD(1000.0);
          CellDesigEmission.Value2 = microwaveLink.Equipment.DesigEmission;
          CellModulation.Value2 = microwaveLink.Equipment.Modulation;

          CellEquipmentName.SetRedValue();          
          CellBandwidth.SetRedValue();
          CellDesigEmission.SetRedValue();
          CellModulation.SetRedValue();

          CellCertificateNumber.Value = microwaveLink.Equipment.Certificate.Symbol;
          CellCertificateDate.DateValue = microwaveLink.Equipment.Certificate.Date;
          CellCertificateNumber.Value2 = microwaveLink.Equipment.Certificate.Symbol;
          CellCertificateDate.DateValue2 = microwaveLink.Equipment.Certificate.Date;
          CellCertificateNumber.SetRedValue();
          CellCertificateDate.SetRedValue();
          // Дата сертифіката відповідності
                           
         CellRange.DoubleValue = Convert.ToInt32(microwaveLink.Range*1000.0)/1000.0;
         CellRange.DoubleValue2 = Convert.ToInt32(microwaveLink.Range*1000.0)/1000.0;
         CellRange.SetRedValue();

         CellObjectID.Value = ObjID.ToString();
         CellObjectID.Value2 = ObjID.ToString();
         
         int sectorCount = microwaveLinks.GetSectorCount();
         for (int i = 0; i < sectorCount; i++) 
         {
            MicrowaveLink currentSector = microwaveLinks.GetSectorByIndex(i);

            CellFrequency1Array[i].DoubleValue = currentSector.Stations[0].Frequency;
            CellFrequency2Array[i].DoubleValue = currentSector.Stations[1].Frequency;
            CellFrequency1Array[i].DoubleValue2 = currentSector.Stations[0].Frequency;
            CellFrequency2Array[i].DoubleValue2 = currentSector.Stations[1].Frequency;
            CellFrequency2Array[i].SetRedValue();
            CellFrequency1Array[i].SetRedValue();
         }

         CellASL1.CanEdit = false;
         CellASL2.CanEdit = false;
         CellRange.CanEdit = false;
         CellObjectID.CanEdit = false;
         CellModulation.CanEdit = false;

         if (microwaveLinkDiff.IsInitialized)
         {
            CellMaxPower1.DoubleValue = microwaveLinkDiff.Stations[0].Power[PowerUnits.dBW];
            CellMaxPower2.DoubleValue = microwaveLinkDiff.Stations[1].Power[PowerUnits.dBW];

            CellAGL1.DoubleValue = microwaveLinkDiff.Stations[0].AGL;
            CellAGL2.DoubleValue = microwaveLinkDiff.Stations[1].AGL;

            CellGain1.DoubleValue = microwaveLinkDiff.Stations[0].Gain;
            CellGain2.DoubleValue = microwaveLinkDiff.Stations[1].Gain;

            CellFrequency1.DoubleValue = microwaveLinkDiff.Stations[0].Frequency;
            CellFrequency2.DoubleValue = microwaveLinkDiff.Stations[1].Frequency;

            CellPolarization1.Value = microwaveLinkDiff.Stations[0].Polarization;
            CellPolarization2.Value = microwaveLinkDiff.Stations[1].Polarization;

            CellDesigEmission.Value = microwaveLinkDiff.Equipment.DesigEmission;

            CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, microwaveLinkDiff.Equipment.Certificate);

            //r.Put("CERT_DATE", _diffSta.CertificateDate);
            CellAzimuth1.DoubleValue = microwaveLinkDiff.Stations[0].Azimuth;
            CellAzimuth2.DoubleValue = microwaveLinkDiff.Stations[1].Azimuth;
            CellDiameter1.DoubleValue = microwaveLinkDiff.Stations[0].Antenna.Diameter;
            CellDiameter2.DoubleValue = microwaveLinkDiff.Stations[1].Antenna.Diameter;
            CellBandwidth.DoubleValue = microwaveLinkDiff.Equipment.Bandwidth.DivNullD(1000.0);
            CellModulation.Value = microwaveLinkDiff.Equipment.Modulation;

            CellLongitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LonDms;
            CellLatitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LatDms;
            CellAddress1.Value = microwaveLinkDiff.Stations[0].Position.FullAddress;
            CellASL1.DoubleValue = microwaveLinkDiff.Stations[0].Position.ASl;

            //Second station
            CellLongitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LonDms;
            CellLatitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LatDms;
            CellAddress2.Value = microwaveLinkDiff.Stations[1].Position.FullAddress;
            CellASL2.DoubleValue = microwaveLinkDiff.Stations[1].Position.ASl;

            CellAntennaName1.Value = microwaveLinkDiff.Stations[0].Antenna.Name;
            CellAntennaName2.Value = microwaveLinkDiff.Stations[1].Antenna.Name;
            CellEquipmentName.Value = microwaveLinkDiff.Equipment.Name;
            

            sectorCount = microwaveLinksDiff.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
               MicrowaveLinkDiff currentSector = microwaveLinksDiff.GetSectorByIndex(i);

               CellFrequency1Array[i].DoubleValue = currentSector.Stations[0].Frequency;
               CellFrequency2Array[i].DoubleValue = currentSector.Stations[1].Frequency;    
                          
            }
         } else
         {
             microwaveLinkDiff.Stations[0].Position = microwaveLink.Stations[0].Position.Clone() as PositionState;
             microwaveLinkDiff.Stations[1].Position = microwaveLink.Stations[1].Position.Clone() as PositionState;
         }
      }

      public void OnPressButtonLongitude(Cell cell)
      {
         double longitudeValue;
         double latitudeValue;
         int stationIndex = -1;

         switch (cell.Key)
         {
            case "KLon1":
               longitudeValue = CellLongitude1.DoubleValue;             
               latitudeValue = CellLatitude1.DoubleValue;
               stationIndex = 0;
               break;
            case "KLon2":
               longitudeValue = CellLongitude2.DoubleValue;
               latitudeValue = CellLatitude2.DoubleValue;
               stationIndex = 1;
               break;
            default:
               // Если ключ неправильный, бросаем исключение.
               throw new Exception("MicrowaveAppDiff.Longitude cell key(" + cell.Key + ") is not valid !");
         }
         
          // Выбераем запись из таблицы           
         NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(longitudeValue, latitudeValue, "4DMS", "4DMS");
         IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionMw, 1, tmpXY.Longitude,
                                                                 tmpXY.Latitude);

         if (objPosition != null)
         {
            //RecordPtr Position = new RecordPtr(ICSMTbl.itblPositionMw, objPosition.GetI("ID"));            
            microwaveLinkDiff.Stations[stationIndex].Position.LoadStatePosition(objPosition);
            microwaveLink.Stations[stationIndex].Position = microwaveLinkDiff.Stations[stationIndex].Position;

            switch (stationIndex)
            {
               case 0:                  
                  CellLongitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LonDms;
                  CellLatitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LatDms;
                  CellAddress1.Value = microwaveLinkDiff.Stations[0].Position.FullAddress;
                  CellASL1.DoubleValue = microwaveLinkDiff.Stations[0].Position.ASl;
                  break;
               case 1:
                  CellLongitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LonDms;
                  CellLatitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LatDms;
                  CellAddress2.Value = microwaveLinkDiff.Stations[1].Position.FullAddress;
                  CellASL2.DoubleValue = microwaveLinkDiff.Stations[1].Position.ASl;
                  break;
            }

            FormNewPosition frm = new FormNewPosition(microwaveLinkDiff.Stations[stationIndex].Position.CityId);
            frm.SetLongitudeAsDms(microwaveLinkDiff.Stations[stationIndex].Position.LonDms);
            frm.SetLatitudeAsDms(microwaveLinkDiff.Stations[stationIndex].Position.LatDms);
            frm.CanEditPosition = false;
            frm.CanAttachPhoto = true;
            frm.CanEditStreet = false;
            frm.OldPosition = microwaveLinkDiff.Stations[stationIndex].Position;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                frm.GetFotos(out stListFotosDst[stationIndex], out stListFotosSrc[stationIndex]);
            }

            objPosition.Dispose();
         }
      }

      public void OnPressButtonAddressName(Cell cell)
      {
         double longitudeValue;
         double latitudeValue;
         int stationIndex = -1;

         switch (cell.Key)
         {
            case "addr1":
               longitudeValue = CellLongitude1.DoubleValue;
               latitudeValue = CellLatitude1.DoubleValue;
               stationIndex = 0;
               break;
            case "addr2":
               longitudeValue = CellLongitude2.DoubleValue;
               latitudeValue = CellLatitude2.DoubleValue;
               stationIndex = 1;
               break;
            default:
               // Если ключ неправильный, бросаем исключение.
               throw new Exception("MicrowaveAppDiff.Address cell key(" + cell.Key + ") is not valid !");
         }

         FormNewPosition frm = new FormNewPosition(microwaveLinkDiff.Stations[stationIndex].Position.CityId);
         frm.SetLongitudeAsDms(longitudeValue);
         frm.SetLatitudeAsDms(latitudeValue);
         frm.CanEditPosition = true;
         frm.CanAttachPhoto = true;
         frm.CanEditStreet = true;
         frm.OldPosition = microwaveLinkDiff.Stations[stationIndex].Position;
                   
         if (frm.ShowDialog() == DialogResult.OK)
         {
             frm.GetFotos(out stListFotosDst[stationIndex], out stListFotosSrc[stationIndex]);
            microwaveLinkDiff.Stations[stationIndex].Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, longitudeValue, latitudeValue);
            microwaveLink.Stations[stationIndex].Position = microwaveLinkDiff.Stations[stationIndex].Position;

            switch (stationIndex)
            {
               case 0:
                  CellLongitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LonDms;
                  CellLatitude1.DoubleValue = microwaveLinkDiff.Stations[0].Position.LatDms;
                  CellAddress1.Value = microwaveLinkDiff.Stations[0].Position.FullAddress;
                  CellASL1.DoubleValue = microwaveLinkDiff.Stations[0].Position.ASl;
                  break;
               case 1:
                  CellLongitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LonDms;
                  CellLatitude2.DoubleValue = microwaveLinkDiff.Stations[1].Position.LatDms;
                  CellAddress2.Value = microwaveLinkDiff.Stations[1].Position.FullAddress;
                  CellASL2.DoubleValue = microwaveLinkDiff.Stations[1].Position.ASl;
                  break;               
            }
            Cell cell2 = gridParam.SelectedCell;
            gridParam.SelectedCell = null;
            gridParam.SelectCell(cell2);
         }

         //objPosition.Dispose();
         //}
      }

      /// <summary>
      /// Lookup Equipment
      /// </summary>
      /// <param name="initialValue">search template, usually name of equipment</param>
      /// <param name="bReplaceQuota">will replace quota sumbols</param>
      /// <returns></returns>
      public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
      {
         string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
         string param = "{NAME=\"*" + Criteria + "*\"}";

         RecordPtr RecEquip = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipMicrow, param);
         return RecEquip;
      }

      /// <summary>
      /// On press button equipment
      ///   Set new Equipment if last one is selected
      /// </summary>
      /// <param name="cell"></param>
      public void OnPressButtonEquipmentName(Cell cell)
      {         
         RecordPtr newEquip = LookupEquipment(cell.Value, true);
         if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
         {
            microwaveLinkDiff.Equipment.TableName = newEquip.Table;
            microwaveLinkDiff.Equipment.Id = newEquip.Id;
            microwaveLinkDiff.Equipment.Load();

            CellEquipmentName.Value = microwaveLinkDiff.Equipment.Name;
            CellBandwidth.DoubleValue = microwaveLinkDiff.Equipment.Bandwidth;
            CellDesigEmission.Value = microwaveLinkDiff.Equipment.DesigEmission;
            CellModulation.Value = microwaveLinkDiff.Equipment.Modulation;
            CellCertificateNumber.Value = microwaveLinkDiff.Equipment.Certificate.Symbol;
            CellCertificateDate.DateValue = microwaveLinkDiff.Equipment.Certificate.Date;
         }         
      }

      public void OnBeforeChangeValidateAntHeight1(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal < 0)
            NewValue = cell.Value;*/
      }

      public void OnBeforeChangeValidateAntHeight2(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal<0)
            NewValue = cell.Value;*/
      }

      public void OnBeforeChangeValidateDiameter1(Cell cell, ref string NewValue)
      {
         /*
         double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal < 0)         
            NewValue = cell.Value;
          * /
      }

      public void OnBeforeChangeValidateDiameter2(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal < 0)
            NewValue = cell.Value;*/
      }

      public void OnBeforeChangeValidateFrequency1(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal <= 0)
            NewValue = cell.Value;  */
      }

      public void OnBeforeChangeValidateFrequency2(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal <= 0)
            NewValue = cell.Value;  */
      }

      public void OnBeforeChangeValidateBandwidth(Cell cell, ref string NewValue)
      {
         /*double newVal = IM.NullD;
         if (!Double.TryParse(NewValue, out newVal) || newVal <= 0)
            NewValue = cell.Value; */
      }
            
      /// <summary>
      /// Заглушка    
      /// </summary>
      /// <param name="cell"></param>
      /// <param name="grid"></param>
      public override void OnPressButton(Cell cell, Grid grid)
      {

      }
   }
}
