﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class EarthAppCommit : BaseCommitClass
    {
        private EarthStation station = new EarthStation();
        private EarthDiffStation diffStation = new EarthDiffStation();

        /*private EarthAppDiff diff = null;

        internal class EarthAllValueSet
        {
           public string OrbitalPosition = "";
           //public double Longitude = IM.NullD;
           //public double Latitude = IM.NullD;
           //public double ASL;
           public double Azimuth = IM.NullD;
           //public double Elevation;
           //public string Address;
           //public string EquipmentName;        
           public double MaxPower = IM.NullD;
           public double TxBandwidth = IM.NullD;
           public string TxDesignEmission = "";
           //string TxModulation;
           public double RxBandwidth = IM.NullD;
           public string RxDesignEmission = "";
           //string RxModulation;
           public string CertificateNumber = "";
           public DateTime CertificateDate = IM.NullT;
           //double NoiseTemperature;
           //public string AntennaType;
           public double AGL = IM.NullD;
           public double Diameter = IM.NullD;
           public double TxGain = IM.NullD;
           public double RxGain = IM.NullD;
           public string RxPolarization = "";
           public List<double> RxFrequency = new List<double>();
           public string TxPolarization = "";
           public List<double> TxFrequency = new List<double>();

           public RecordPtr Position = new RecordPtr();
           public RecordPtr Antenna = new RecordPtr();
           public RecordPtr Equipment  = new RecordPtr();
        };

        StationZS st = new StationZS();
        EarthAllValueSet OriginalValues = new EarthAllValueSet();

        public bool initialized = false;*/

        #region Cell Propertues
        private CellStringProperty CellNetworkName = new CellStringProperty();        // Назва супутникової мережі
        private CellDoubleProperty CellOrbitalPosition = new CellDoubleProperty();    // Орбітальна позиція
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();          // Широта (гр.хв.сек.)
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();           // Довгота (гр.хв.сек.)
        private CellDoubleProperty CellASL = new CellDoubleProperty();                // Висота поверхні землі
        private CellDoubleLookupProperty CellAzimuth = new CellDoubleLookupProperty();            // Азимут випромінювання, град
        private CellDoubleProperty CellElevation = new CellDoubleProperty();          // Кут місця випромінювання, град
        private CellStringProperty CellAddress = new CellStringProperty();            // Адреса місця встановлення РЕЗ
        private CellStringProperty CellEquipmentName = new CellStringProperty();      // Назва/Тип РЕЗ
        private CellDoubleLookupProperty CellMaxPower = new CellDoubleLookupProperty();           // Макс. потужність передавача на вх. антени, дБВт\
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();        // Ширина смуги передавача, кГц; 
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();   // клас випромінювання передавача
        private CellStringBrowsableProperty CellTxModulation = new CellStringBrowsableProperty();       // Тип і параметри модуляції випром., що передаються
        private CellStringBrowsableProperty CellRxBandwidth = new CellStringBrowsableProperty();        // Ширина смуги передавача, кГц; 
        private CellStringBrowsableProperty CellRxDesignEmission = new CellStringBrowsableProperty();   // клас випромінювання передавача
        private CellStringBrowsableProperty CellRxModulation = new CellStringBrowsableProperty();       // Тип і параметри модуляції випром., що передаються
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();  // Номер сертифіката відповідності
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();    // Дата сертифіката відповідності
        private CellDoubleProperty CellNoiseTemperature = new CellDoubleProperty();   // Шумова температура приймальної системи, К
        private CellStringProperty CellAntennaType = new CellStringProperty();        // Тип антени
        private CellDoubleLookupProperty CellAGL = new CellDoubleLookupProperty();                // Висота антени над рівнем землі, м
        private CellStringBrowsableProperty CellDiameter = new CellStringBrowsableProperty();           // Діаметр антени, м
        private CellStringBrowsableProperty CellTxGain = new CellStringBrowsableProperty();             // Коефіцієнт підсилення передавання, дБі
        private CellStringBrowsableProperty CellRxGain = new CellStringBrowsableProperty();             // Коефіцієнт підсилення приймання, дБі
        private CellStringLookupEncodedProperty CellRxPolarization = new CellStringLookupEncodedProperty();     // Тип поляризації: у режимі приймання
        private CellStringProperty CellRxFrequency = new CellStringProperty();        // Номінали робочих частот у режимі приймання, ГГц
        private CellStringLookupEncodedProperty CellTxPolarization = new CellStringLookupEncodedProperty();     // Тип поляризації: у режимі передавання
        private CellStringProperty CellTxFrequency = new CellStringProperty();        // Номінали робочих частот у режимі передав., ГГц
        private CellStringProperty CellObjectID = new CellStringProperty();           // Об"єкт(и) РЧП                  
        #endregion

        private int ObjID = IM.NullI;
        private CoordinateCommitHelper _coordinateHelper;

        private KeyedPickList RxModulationList;
        private KeyedPickList TxModulationList;
        //===================================================
        public EarthAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, EarthApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppZS;
            _coordinateHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);

            station = new EarthStation();
            station.Id = id;
            station.Load();

            diffStation = new EarthDiffStation();
            diffStation.Id = id;
            diffStation.Load();
        }
        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Cell cell)
        {

        }

        private void CreateComboBoxes()
        {
            _coordinateHelper.Initialize(diffStation.Position, station.Position);

                
            CellAzimuth.Initialize(station.Azimuth, diffStation.Azimuth);
            
            Comment = diffStation.StatComment;
            
            CellAGL.Initialize(station.AGL, diffStation.AGL);                        
            CellMaxPower.Initialize(station.Power[PowerUnits.dBW], diffStation.Power[PowerUnits.dBW]);

            CellStringLookupEncodedProperty.InitalizeHelper(
                CellTxPolarization,
                station.TxPolarization,
                diffStation.TxPolarization);

            CellStringLookupEncodedProperty.InitalizeHelper(
                CellRxPolarization,
                station.RxPolarization,
                diffStation.RxPolarization);

            if (HelpFunction.ToString(station.TxFrequencies) != HelpFunction.ToString(diffStation.TxFrequencies))
            {
                string p1 = HelpFunction.ToString(station.TxFrequencies);
                string p2 = HelpFunction.ToString(diffStation.TxFrequencies);

                CellTxFrequency.Cell.cellStyle = EditStyle.esPickList;
                KeyedPickList newKeyedPickList = new KeyedPickList();
                newKeyedPickList.AddToPickList("1", p1);
                newKeyedPickList.AddToPickList("2", p2);
                CellTxFrequency.Cell.KeyPickList = newKeyedPickList;
                CellTxFrequency.Cell.Value = "1";
                CellTxFrequency.Cell.Value2 = "1";
            }

            if (HelpFunction.ToString(station.RxFrequencies) != HelpFunction.ToString(diffStation.RxFrequencies))
            {
                string p1 = HelpFunction.ToString(station.RxFrequencies);
                string p2 = HelpFunction.ToString(diffStation.RxFrequencies);

                CellRxFrequency.Cell.cellStyle = EditStyle.esPickList;
                KeyedPickList newKeyedPickList = new KeyedPickList();
                newKeyedPickList.AddToPickList("1", p1);
                newKeyedPickList.AddToPickList("2", p2);
                CellRxFrequency.Cell.KeyPickList = newKeyedPickList;
                CellRxFrequency.Cell.Value = "1";
                CellRxFrequency.Cell.Value2 = "1";
            }

            CreateEquipmentComboBoxes();
            CreateAntennaComboBoxes();
        }

        private void CreateEquipmentComboBoxes()
        {
            CellRxModulation.Cell.KeyPickList = null;
            CellTxModulation.Cell.KeyPickList = null;

            CellStringBrowsableProperty.InitalizeHelper(CellRxBandwidth,
                                                        station.Equipment.RxBandwidth.ToStringNullD(),
                                                        diffStation.Equipment.RxBandwidth.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellRxDesignEmission,
                                                        station.Equipment.RxDesigEmission,
                                                        diffStation.Equipment.RxDesigEmission);

            CellStringBrowsableProperty.InitalizeHelper(CellRxModulation,
                                                        RxModulationList.GetValueByKey(station.Equipment.RxModulation),
                                                        RxModulationList.GetValueByKey(diffStation.Equipment.RxModulation));

            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth,
                                                        station.Equipment.TxBandwidth.ToStringNullD(),
                                                        diffStation.Equipment.TxBandwidth.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission,
                                                        station.Equipment.TxDesigEmission,
                                                        diffStation.Equipment.TxDesigEmission);

            CellStringBrowsableProperty.InitalizeHelper(CellTxModulation,
                                                        TxModulationList.GetValueByKey(station.Equipment.TxModulation),
                                                        TxModulationList.GetValueByKey(diffStation.Equipment.TxModulation));

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber,
                                                        station.Equipment.Certificate.Symbol,
                                                        diffStation.Equipment.Certificate.Symbol);

            if (diffStation.Equipment.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate,
               station.Equipment.Certificate.Date.ToShortDateStringNullT(),
               diffStation.Equipment.Certificate.Date.ToShortDateStringNullT());
        }

        private void CreateAntennaComboBoxes()
        {
            CellStringBrowsableProperty.InitalizeHelper(CellDiameter,
                  station.Antenna.Diameter.ToStringNullD(),
                  diffStation.Antenna.Diameter.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellRxGain,
                  station.Antenna.RxGain.ToStringNullD(),
                  diffStation.Antenna.RxGain.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellTxGain,
                  station.Antenna.TxGain.ToStringNullD(),
                  diffStation.Antenna.TxGain.ToStringNullD());
        }

        public void PaintGrid()
        {
            _coordinateHelper.Paint(diffStation.Position, station.Position);

            if (station.TxPolarization != diffStation.TxPolarization)
            {
                if (CellTxPolarization.Value2 == diffStation.TxPolarization)
                    CellTxPolarization.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellTxPolarization.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellTxPolarization.BackColor = CellStringProperty.DefaultBgr();

            if (station.RxPolarization != diffStation.RxPolarization)
            {
                if (CellRxPolarization.Value2 == diffStation.RxPolarization)
                    CellRxPolarization.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellRxPolarization.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellRxPolarization.BackColor = CellStringProperty.DefaultBgr();


            if (HelpFunction.ToString(station.TxFrequencies) != HelpFunction.ToString(diffStation.TxFrequencies))
            {
                if (CellTxFrequency.Value2 == "2")
                    CellTxFrequency.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellTxFrequency.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellTxFrequency.BackColor = CellStringProperty.DefaultBgr();

            if (HelpFunction.ToString(station.RxFrequencies) != HelpFunction.ToString(diffStation.RxFrequencies))
            {
                if (CellRxFrequency.Value2 == "2")
                    CellRxFrequency.BackColor = CellStringProperty.ColorBgr(2);
                else
                    CellRxFrequency.BackColor = CellStringProperty.ColorBgr(0);
            }
            else
                CellRxFrequency.BackColor = CellStringProperty.DefaultBgr();
        }

        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;

            RxModulationList = CellRxModulation.Cell.KeyPickList;
            TxModulationList = CellTxModulation.Cell.KeyPickList;

            InitStationObject();
            CreateComboBoxes();
        }
        //===========================================================
        /// <summary>
        /// Will be changed...
        /// </summary>
        protected override string GetXMLParamGrid()
        {
            return XICSM.UcrfRfaNET.Properties.Resources.EarthAppCommit;
        }

        /// <summary>
        ///  Read data from XNRFA_DIFF_ET_STAT table
        /// </summary>     
        /*private void ReadDifferences(int objectId)
        {
           IMRecordset r = null;
           try
           {
              r = new IMRecordset(PlugTbl.itblXnrfaDeiffEtSta, IMRecordset.Mode.ReadOnly);
              r.Select("ID,ET_STATION_ID,EQUIP_ID,ANT_ID,PWR_ANT,AGL,AZM_TO,EVEL_MIN,RX_DESIG_EM,TX_DESIG_EM,TX_BW,RX_BW,CERT_NUM,CERT_DATE,DIAMETER,POLAR_TX,POLAR_RX,POS_ID,POS_TABLE");

              r.SetWhere("ET_STATION_ID", IMRecordset.Operation.Eq, objectId);
              r.Open();
              if (!r.IsEOF())
              {
                 int equimpentId = r.GetI("EQUIP_ID");
                 if (equimpentId!=IM.NullI)
                    Equipment = new RecordPtr(ICSMTbl.itblEquipEsta, equimpentId);

                 int antennaId = r.GetI("ANT_ID");
                 if (antennaId!=IM.NullI)
                    Antenna = new RecordPtr(ICSMTbl.itblAntenna, antennaId);

                 double power = r.GetD("PWR_ANT");
                 if (power!=IM.NullD)
                    Power = power;

                 double agl = r.GetD("AGL");
                 if (agl != IM.NullD)
                    AGL = agl;

                 string txPolar = r.GetS("POLAR_TX");
                 if (!string.IsNullOrEmpty(txPolar))
                    TxPolarization = txPolar;

                 string rxPolar = r.GetS("POLAR_RX");
                 if (!string.IsNullOrEmpty(rxPolar))
                    RxPolarization = txPolar;

                 string posTable = r.GetS("POS_TABLE");
                 int posId = r.GetI("POS_ID");
                 if (!string.IsNullOrEmpty(posTable) && posId!=IM.NullI)
                    Position = new RecordPtr(posTable, posId);
               
                 double azimuth = r.GetD("AZM_TO");
                 if (azimuth != IM.NullD)
                    Azimuth = azimuth;

                 string txDesigEm = r.GetS("TX_DESIG_EM");
                 if (!string.IsNullOrEmpty(txDesigEm))
                    TxEmissionClass = txDesigEm;

                 string rxDesigEm = r.GetS("RX_DESIG_EM");
                 if (!string.IsNullOrEmpty(rxDesigEm))
                    RxEmissionClass = rxDesigEm;

                 double txBw = r.GetD("TX_BW");
                 if (txBw != IM.NullD)
                    TxBandwidth = txBw;

                 double rxBw = r.GetD("RX_BW");
                 if (rxBw != IM.NullD)
                    RxBandwidth = rxBw;

                 string CertNo = r.GetS("CERT_NUM");
                 if (!string.IsNullOrEmpty(CertNo))
                    CertificateNo = CertNo;

                 DateTime certDt = r.GetT("CERT_DATE");
                 if (certDt != IM.NullT)
                    CertificateDate = certDt;

                 double dia = r.GetD("DIAMETER");
                 if (dia != IM.NullD)
                    Diameter = dia;               
              }
              ReadDifferencesFreq();
           }
           finally
           {
              if (r != null)
              {
                 r.Close();
                 r.Destroy();
                 r = null;
              }
           }
        }*/

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = station.Equipment.Name;
            CellNoiseTemperature.DoubleValue = station.Equipment.NoiseTemperature;

            CellRxBandwidth.CanEdit = false;
            CellRxDesignEmission.CanEdit = false;
            CellRxModulation.CanEdit = false;
            CellTxBandwidth.CanEdit = false;
            CellTxDesignEmission.CanEdit = false;
            CellTxModulation.CanEdit = false;
            CellCertificateNumber.CanEdit = false;
            CellCertificateDate.CanEdit = false;
        }

        public void InitAntenna()
        {
            CreateAntennaComboBoxes();

            CellAntennaType.Value = station.Antenna.Name;
        }

        /// <summary>
        ///===================================================
        /// Load data from database to Station object
        ///===================================================
        /// </summary>
        /// <param name="objectID"></param>
        /// 
        private void InitStationObject()
        {
            CellLongitude.CanEdit = false;
            CellLatitude.CanEdit = false;
            CellAddress.CanEdit = false;
            CellAzimuth.CanEdit = false;
            CellElevation.CanEdit = false;
            CellNetworkName.CanEdit = false;
            CellOrbitalPosition.CanEdit = false;
            CellDiameter.CanEdit = false;
            CellRxGain.CanEdit = false;
            CellTxGain.CanEdit = false;
            CellAGL.CanEdit = false;
            CellTxPolarization.CanEdit = false;
            CellRxPolarization.CanEdit = false;
            CellTxFrequency.CanEdit = false;
            CellRxFrequency.CanEdit = false;
            CellMaxPower.CanEdit = false;

            CellNetworkName.Value = station.NetworkName;
            CellOrbitalPosition.DoubleValue = station.OrbitalPosition;
            CellAzimuth.DoubleValue = station.Azimuth;
            CellElevation.DoubleValue = station.Elevation;

            CellTxPolarization.Value = station.TxPolarization;
            CellRxPolarization.Value = station.RxPolarization;
            CellTxFrequency.Value = HelpFunction.ToString(station.TxFrequencies);
            CellRxFrequency.Value = HelpFunction.ToString(station.RxFrequencies);
            CellMaxPower.DoubleValue = station.Power[PowerUnits.dBW];            

            InitEquipment();
            InitAntenna();
        }

        public void OnBeforeChangeAddr(Cell cell, ref string NewValue)
        {
            CellAddress.Value2 = NewValue;
            PaintGrid();
        }

        public void OnBeforeChangeRxPolarization(Cell cell, ref string NewValue)
        {
            CellRxPolarization.Value2 = NewValue;
            PaintGrid();
        }

        public void OnBeforeChangeTxPolarization(Cell cell, ref string NewValue)
        {
            CellTxPolarization.Value2 = NewValue;
            PaintGrid();
        }

        public void OnBeforeChangeTxFrequency(Cell cell, ref string NewValue)
        {
            CellTxFrequency.Value2 = NewValue;
            PaintGrid();
        }

        public void OnBeforeChangeRxFrequency(Cell cell, ref string NewValue)
        {
            CellRxFrequency.Value2 = NewValue;
            PaintGrid();
        }

        /// <summary>
        /// On press button equipment
        ///   Set new Equipment if last one is selected
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                station.Equipment.TableName = newEquip.Table;
                station.Equipment.Id = newEquip.Id;
                station.Equipment.Load();

                InitEquipment();

                PaintGrid();
            }
        }

        public void OnPressButtonAntennaName(Cell cell)
        {
            RecordPtr newAntenna = LookupAntenna(cell.Value, true);
            if ((newAntenna.Id > 0) && (newAntenna.Id < IM.NullI))
            {
                station.Antenna.TableName = newAntenna.Table;
                station.Antenna.Id = newAntenna.Id;
                station.Antenna.Load();

                InitAntenna();

                PaintGrid();
            }
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipEsta, param);
            return RecEquip;
        }

        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntenna, param);
            return RecAnt;
        }

        ///===================================================
        /// Bind grid
        ///===================================================
        private void BindGrid(Grid gridParam)
        {
            return;
        }
        /// <summary>
        /// On press button Longitude handler 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            // Выбераем запись из таблицы           
            /*NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionEs, 1, tmpXY.Longitude, tmpXY.Latitude);

            if (objPosition != null)
            {
               Position = new RecordPtr(ICSMTbl.itblPositionEs, objPosition.GetI("ID"));
               objPosition.Dispose();
            }*/
        }

        /// <summary>
        /// Legacy
        /// </summary>
        /// <param name="docType"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        protected override string getOutDocName(string docType, string number)
        {
            return "";
        }

        /// <summary>
        /// Jyst stub
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key=="KAddr-new")
            {
                RecordPtr rcPtr = new RecordPtr(station.Position.TableName, station.Position.Id);
                rcPtr.UserEdit();
            }
        }

        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "EARTH_STATION" };
            PositionState diffPos = diffStation.Position.Clone() as PositionState;                        
            AttachmentControl ac = new AttachmentControl();

            EarthStation savedStation = station;

            PositionState pos = _coordinateHelper.GetSelectedPosition(station.Position, diffStation.Position);
            if (pos != station.Position)
                SaveNewPosition(pos, station.Position, assgnReferenceTables);

            savedStation.Position = pos;
            savedStation.AGL = CellAGL.LookupedDoubleValue;

            if (CellTxFrequency.Cell.KeyPickList != null && CellTxFrequency.Value == "2")
            {
                savedStation.TxFrequencies = diffStation.TxFrequencies;
            }

            if (CellRxFrequency.Cell.KeyPickList != null && CellRxFrequency.Value == "2")
            {
                savedStation.RxFrequencies = diffStation.RxFrequencies;
            }

            savedStation.TxPolarization = CellTxPolarization.Value;
            savedStation.RxPolarization = CellRxPolarization.Value;
                        
            savedStation.Power[PowerUnits.W] = CellMaxPower.LookupedDoubleValue;

            if (CellTxFrequency.Cell.KeyPickList != null && CellTxFrequency.Value == "2")
            {
                savedStation.TxFrequencies = diffStation.TxFrequencies;
            }

            if (CellRxFrequency.Cell.KeyPickList != null && CellRxFrequency.Value == "2")
            {
                savedStation.RxFrequencies = diffStation.RxFrequencies;
            }

            if (savedStation.Position.TableName != ICSMTbl.itblPositionEs)
            {
                savedStation.Position.Id = IM.NullI;
                savedStation.Position.TableName = ICSMTbl.itblPositionEs;
                savedStation.Position.SaveToBD();
            }

            savedStation.ApplId = ApplID;
            savedStation.StatComment = diffStation.StatComment ;
            savedStation.Save();

            diffStation.Remove();

            ac.RelocateAndCopyDocLinks(
               diffPos.TableName,
               diffPos.Id,
               EarthApp.TableName,
               savedStation.Id,
               savedStation.Position.TableName,
               savedStation.Position.Id,
               ICSMTbl.itblAdmSite,
               savedStation.Position.AdminSitesId);

            return true;
        }
    }
}
