﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class TvDigitallDiff : BaseDiffClass
    {
        private bool _initialized;
        public int Id { get; set; }

        private TvDigitallStation tvDgStation;
        private TvDigitallDiffStation tvDgDiffStation;

        #region CELL

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Номер контуру
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentContour = new CellStringComboboxAplyProperty();
        /// <summary>
        /// номер РЕЗ в синхронній мережі
        /// </summary> 
        private CellStringProperty CellEquipmentNameSynchr = new CellStringProperty();

        /// <summary>
        /// Назва виділення
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameSelect = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Назва ОЧМ
        /// </summary> 
        private CellStringProperty CellEquipmentNameOchm = new CellStringProperty();

        /// <summary>
        /// Варіант системи
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameVariant = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Кількість несівних
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentCountOstov = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Захисний інтервал
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSafeInterv = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Втрати у фільтрі 
        /// </summary> 
        private CellDoubleProperty CellEquipmentLoseFiltr = new CellDoubleProperty();
        /// <summary>
        /// Тип спектр. маски фільтра
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSpectrMaskFiltr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Тип прийм
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentTypeReceive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Стандарт компресії
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentStandCompr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Зміщення несівної частоти, Гц
        /// </summary> 
        private CellDoubleProperty CellEquipmentShiftOstov = new CellDoubleProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleProperty CellPowerTr = new CellDoubleProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellDoubleBoundedProperty CellTxBandwidth = new CellDoubleBoundedProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Довжина фідера, м
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері, дБ/м
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringComboboxAplyProperty CellAntenDirect = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringComboboxAplyProperty CellAntenPolar = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Висота антени над землею, м
        /// </summary>
        private CellDoubleBoundedProperty CellHighAntenn = new CellDoubleBoundedProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellDoubleProperty CellMaxKoefG = new CellDoubleProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellDoubleProperty CellMaxKoefV = new CellDoubleProperty();

        /// <summary>
        /// ЕВП Г, дБВт 
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В, дБВт
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП Макс., дБВт
        /// </summary>
        private CellDoubleProperty CellEVPm = new CellDoubleProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellInnerState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellOuterState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// ТВ канал
        /// </summary>
        private CellStringComboboxAplyProperty CellTvChannel = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Центральна частота, МГц
        /// </summary>
        private CellDoubleBoundedProperty CellMaxFreq = new CellDoubleBoundedProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public TvDigitallDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, CTvDigital.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTV2d;
            //Стандартные значения
            tvDgStation = new TvDigitallStation();
            tvDgStation.Id = recordID.Id;
            tvDgStation.Load();

            //URCP значения
            tvDgDiffStation = new TvDigitallDiffStation();
            tvDgDiffStation.Id = recordID.Id;
            tvDgDiffStation.Load();
            if (tvDgDiffStation.Position == null)
                tvDgDiffStation.Position = tvDgStation.Position.Clone() as PositionState;

            CellCertificateDate.NullString = "-";
            ReadAllDefaultDataPositions();
            SCVisnovok = tvDgStation.Finding;
            _stationObject = tvDgStation;
        }

        private void ReadAllDefaultDataPositions()
        {
            CellLongitude.DoubleValue2 = tvDgStation.Position.LonDms;
            CellLatitude.DoubleValue2 = tvDgStation.Position.LatDms;
            CellASL.Value2 = FillValue2(tvDgStation.Position.ASl.ToStringNullD());
            CellAddress.Value2 = FillValue2(tvDgStation.Position.FullAddress);

            CellLongitude.DoubleValue = tvDgStation.Position.LonDms;
            CellLatitude.DoubleValue = tvDgStation.Position.LatDms;
            CellASL.Value = tvDgStation.Position.ASl.ToStringNullD();
            CellAddress.Value = tvDgStation.Position.FullAddress;

            string tmpDate;
            if (tvDgStation.TvDgEquipment.Certificate.Date == IM.NullT)
                tmpDate = "";
            else
                tmpDate = tvDgStation.TvDgEquipment.Certificate.Date.ToShortDateString();
            CellCertificateDate.Value2 = FillValue2(tmpDate);
            CellCertificateNumber.Value2 = FillValue2(tvDgStation.TvDgEquipment.Certificate.Symbol);
            CellCertificateDate.Value = tmpDate;
            CellCertificateNumber.Value = tvDgStation.TvDgEquipment.Certificate.Symbol;

            CellEquipmentName.Value2 = FillValue2(tvDgStation.TvDgEquipment.Name);
            CellEquipmentName.Value = tvDgStation.TvDgEquipment.Name;

            CellEquipmentNameNumber.Value2 = tvDgStation.NameNumber;
            CellEquipmentNameNumber.Value = tvDgStation.NameNumber;
            CellEquipmentNameREZ.Value = tvDgStation.NameRez;
            CellEquipmentContour.Items.AddRange(tvDgStation.ContourList);
            if (string.IsNullOrEmpty(tvDgStation.Countur) && tvDgStation.ContourList.Count > 0)
                tvDgStation.Countur = tvDgStation.ContourList[0];
            else
                tvDgStation.Countur = "";
            if(tvDgStation.ContourList.Contains(tvDgStation.Countur))
                CellEquipmentContour.Value = tvDgStation.ContourList[tvDgStation.ContourList.IndexOf(tvDgStation.Countur)];
            CellEquipmentNameSelect.Items.AddRange(tvDgStation.NameSelectList);
            CellEquipmentNameSelect.Value = tvDgStation.NameSelect;
            CellEquipmentNameSynchr.Value = tvDgStation.NameSynchr;
            CellEquipmentNameOchm.Value = tvDgStation.NameOchm;

            foreach (KeyValuePair<string, string> s in tvDgStation.VariantSystDict)
                CellEquipmentNameVariant.Items.Add(s.Value);
            if (!tvDgStation.VariantSystDict.ContainsKey(tvDgStation.NameVariant))
                tvDgStation.NameVariant = tvDgStation.VariantSystDict.Count > 0 ? tvDgStation.VariantSystDict.ToList()[0].Key : "";
            CellEquipmentNameVariant.Value = tvDgStation.VariantSystDict[tvDgStation.NameVariant];
            if (string.IsNullOrEmpty(tvDgStation.CountOstov) && tvDgStation.CountNesivList.Count > 0)
                tvDgStation.CountOstov = tvDgStation.CountNesivList[0];
            else
                tvDgStation.CountOstov = "";
            CellEquipmentCountOstov.Value = tvDgStation.CountOstov;
            CellEquipmentCountOstov.Items.AddRange(tvDgStation.CountNesivList);

            if (string.IsNullOrEmpty(tvDgStation.SafeInterv.ToString()) || tvDgStation.SafeInterv == IM.NullI)
                tvDgStation.SafeInterv = tvDgStation.SafeIntervDict.Count > 0 ? Convert.ToInt32(tvDgStation.SafeIntervDict.ToList()[0].Key) : 0;
            CellEquipmentSafeInterv.Value = tvDgStation.SafeIntervDict[tvDgStation.SafeInterv.ToString()];
            foreach (KeyValuePair<string, string> s in tvDgStation.SafeIntervDict)
                CellEquipmentSafeInterv.Items.Add(s.Value);
            CellEquipmentLoseFiltr.DoubleValue = tvDgStation.LoseFiltr;
            CellEquipmentSpectrMaskFiltr.Value = tvDgStation.SpectrMaskFiltr;
            CellEquipmentSpectrMaskFiltr.Items.AddRange(tvDgStation.SpectrMaskList);
            if (string.IsNullOrEmpty(tvDgStation.TypeReceive))
                tvDgStation.TypeReceive = tvDgStation.TypeReceiveDict.ToList()[0].Key;
            CellEquipmentTypeReceive.Value = tvDgStation.TypeReceiveDict[tvDgStation.TypeReceive];
            foreach (KeyValuePair<string, string> s in tvDgStation.TypeReceiveDict)
                CellEquipmentTypeReceive.Items.Add(s.Value);

            if (!tvDgStation.StandComprList.Contains(tvDgStation.StandCompr))
                tvDgStation.StandCompr = tvDgStation.StandComprList[0];
            CellEquipmentStandCompr.Value = tvDgStation.StandCompr;
            CellEquipmentStandCompr.Items.AddRange(tvDgStation.StandComprList);

            CellEquipmentShiftOstov.Value2 = FillValue2(tvDgStation.ShiftOstov.ToStringNullD());
            CellEquipmentShiftOstov.DoubleValue = tvDgStation.ShiftOstov;

            CellPowerTr.Value2 = FillValue2(IM.RoundDeci(tvDgStation.Power[PowerUnits.W], 3).ToStringNullD());
            CellPowerTr.SetRedValue();
            CellPowerTr.Value = IM.RoundDeci(tvDgStation.Power[PowerUnits.W], 3).ToStringNullD();

            CellTxBandwidth.Value2 = FillValue2(tvDgStation.Bandwith.ToStringNullD());
            CellTxBandwidth.DoubleValue = tvDgStation.Bandwith;

            CellTxDesignEmission.Value2 = FillValue2(tvDgStation.ClassEmission);
            CellTxDesignEmission.Value = tvDgStation.ClassEmission;

            if (!string.IsNullOrEmpty(tvDgStation.FeederLength.ToStringNullD()))
                CellFiderLength.DoubleValue = tvDgStation.FeederLength;
            if (!string.IsNullOrEmpty(tvDgStation.FeederLosses.ToStringNullD()))
                CellFiderlost.DoubleValue = tvDgStation.FeederLosses;

            if (!string.IsNullOrEmpty(tvDgStation.TvDgEquipment.Certificate.Symbol))
            {
                if (tvDgStation.TvDgEquipment.Certificate.Symbol == "-")
                    tvDgStation.TvDgEquipment.Certificate.Symbol = "";
                CellCertificateNumber.Value = tvDgStation.TvDgEquipment.Certificate.Symbol;
            }
            CellCertificateNumber.Value2 = FillValue2(tvDgStation.TvDgEquipment.Certificate.Symbol);
            CellCertificateNumber.SetRedValue();

            CellCertificateDate.Value = tmpDate;
            CellCertificateDate.Value2 = FillValue2(tmpDate);
            CellCertificateDate.SetRedValue();

            CellAntenDirect.Value2 = FillValue2(tvDgStation.Direction);
            CellAntenDirect.Value = tvDgStation.Direction;

            CellAntenPolar.Value2 = FillValue2(tvDgStation.Polarization);
            CellAntenPolar.Value = tvDgStation.Polarization;

            CellHighAntenn.Value2 = FillValue2(tvDgStation.HeightAnten.ToStringNullD());
            CellHighAntenn.DoubleValue = tvDgStation.HeightAnten;

            CellMaxHigh.DoubleValue = tvDgStation.MaxHigh;
            if (!tvDgStation.InnerStateDict.ContainsKey(tvDgStation.InnerState))
                tvDgStation.InnerState = tvDgStation.InnerStateDict.ToList()[0].Key;
            CellInnerState.Value = tvDgStation.InnerState + " - " + tvDgStation.InnerStateDict[tvDgStation.InnerState];
            foreach (KeyValuePair<string, string> s in tvDgStation.InnerStateDict)
                CellInnerState.Items.Add(s.Key + " - " + s.Value);

            if (!tvDgStation.OuterStateDict.ContainsKey(tvDgStation.OuterState))
                tvDgStation.OuterState = tvDgStation.OuterStateDict.ToList()[0].Key;
            CellOuterState.Value = tvDgStation.OuterState + " - " + tvDgStation.OuterStateDict[tvDgStation.OuterState];
            foreach (KeyValuePair<string, string> s in tvDgStation.OuterStateDict)
                CellOuterState.Items.Add(s.Key + " - " + s.Value);

            if (string.IsNullOrEmpty(tvDgStation.TvChannel))
                tvDgStation.TvChannel = tvDgStation.ChannelList[0];
            CellTvChannel.Value2 = FillValue2(tvDgStation.TvChannel);
            CellTvChannel.Value = tvDgStation.TvChannel;
            CellTvChannel.Items.AddRange(tvDgStation.ChannelList);

            CellMaxFreq.DoubleValue = tvDgStation.FreqCentr;
            CellObjRCHP.IntValue = tvDgStation.ObjRCHP;
        }


        /// <summary>
        /// Реакция на нажатия кнопки оформления позиции
        /// Формирование адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");

            // Выбираем запись из таблицы                     
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionBro, 1, tmpXy.Longitude, tmpXy.Latitude);
            if (objPosition != null)
            {
                tvDgDiffStation.Position = new PositionState();
                tvDgDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                CellLongitude.DoubleValue = tvDgDiffStation.Position.LongDms;
                CellLatitude.DoubleValue = tvDgDiffStation.Position.LatDms;
                CellASL.DoubleValue = tvDgDiffStation.Position.ASl;
                if (!string.IsNullOrEmpty(tvDgDiffStation.Position.FullAddress))
                    CellAddress.Value = tvDgDiffStation.Position.FullAddress;


                // Форма Адреса місця встановлення РЕЗ
                FormNewPosition frm = new FormNewPosition(tvDgDiffStation.Position.CityId);

                frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
                frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = tvDgDiffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
            }
        }

        public void OnBeforeChangePowerTr(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeTxBandwidth(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeTxDesignEmission(Cell cell, ref string val) { }

        public void OnBeforeChangeCertificateNumber(Cell cell, ref string val) { }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            DateTime newVal;
            if (!DateTime.TryParse(val, out newVal) || newVal < DateTime.MinValue)
                val = cell.Value;
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal) { }


        public void OnPressButtonEvpv(Cell cell)
        {
            double power = objStation.GetD("PWR_ANT");
            double feederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
            CellEVPV.Value = CalcErpHorz(power - feederLoss);
        }

        public void OnPressButtonEvpg(Cell cell)
        {
            double power = objStation.GetD("PWR_ANT");
            double feederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
            CellEVPG.Value = CalcErpVert(power - feederLoss);
        }

        protected string CalcErpHorz(double basePower)
        {
            string polarization = objStation.GetS("POLARIZATION");

            if (polarization != "V")
            {
                FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (Г)",
                                     "Азимут, град", "ЕВП., дБВт");

                string strDiagVertical = objStation.GetS("DIAGV");
                string strDiagHorizontal = objStation.GetS("DIAGH");
                double gain = objStation.GetD("GAIN");

                HelpFunction.StringToGainedDiagEx(strDiagVertical, gain);
                List<double> diagDblH = HelpFunction.StringToGainedDiagEx(strDiagHorizontal, gain);

                double erphMax = -1E+10;
                List<double> diagDbl = new List<double>();

                foreach (double gainAlpha in diagDblH)
                {
                    double erp = basePower/*Power - FeederLoss*/ + gainAlpha;
                    if (erphMax < erp)
                        erphMax = erp;
                    diagDbl.Add(Math.Round(erp * 100.0) / 100.0);
                }

                f.Data = diagDbl;
                f.ShowDialog();

                if (erphMax != -1E+10)
                {
                    objStation.Put("ERP_H", erphMax);
                    return erphMax.ToStringNullD("F3");
                }
                objStation.Put("ERP_H", IM.NullD);
                return "";
            }
            objStation.Put("ERP_H", IM.NullD);
            return "не застосовується";
        }
        //-------------------------------------------------------------------------------------
        protected string CalcErpVert(double basePower)
        {
            string polarization = objStation.GetS("POLARIZATION");

            if (polarization != "H")
            {
                FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (В)",
                                     "Азимут, град", "ЕВП., дБВт");

                string strDiagVertical = objStation.GetS("DIAGV");
                string strDiagHorizontal = objStation.GetS("DIAGH");
                double gain = objStation.GetD("GAIN");

                List<double> diagDblV = HelpFunction.StringToGainedDiagEx(strDiagVertical, gain);
                HelpFunction.StringToGainedDiagEx(strDiagHorizontal, gain);

                double erpvMax = -1E+10;
                List<double> diagDbl = new List<double>();

                //double Power = objStation.GetD("PWR_ANT");
                //double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");

                foreach (double gainAlpha in diagDblV)
                {
                    double erp = basePower/*Power - FeederLoss*/ + gainAlpha;
                    if (erpvMax < erp)
                        erpvMax = erp;

                    diagDbl.Add(Math.Round(erp * 100.0) / 100.0);
                }

                f.Data = diagDbl;
                f.ShowDialog();

                if (erpvMax != -1E+10)
                {
                    objStation.Put("ERP_V", erpvMax);
                    return erpvMax.ToStringNullD("F3");
                }
                objStation.Put("ERP_V", IM.NullD);
                return "";
            }
            objStation.Put("ERP_V", IM.NullD);
            return "не застосовується";
        }
        //-------------------------------------------------------------------------------------
        protected string CalcErpMixed(double basePower)
        {
            FEnterEVP f = new FEnterEVP("Розраховане значення ЕВП по азимутам (М)",
                                 "Азимут, град", "ЕВП., дБВт");

            string strDiagVertical = objStation.GetS("DIAGV");
            string strDiagHorizontal = objStation.GetS("DIAGH");
            double gain = objStation.GetD("GAIN");
            List<double> diagDblV = HelpFunction.StringToGainedDiagEx(strDiagVertical, gain);
            List<double> diagDblH = HelpFunction.StringToGainedDiagEx(strDiagHorizontal, gain);

            //double Power = objStation.GetD("PWR_ANT");
            //double FeederLoss = objStation.GetD("CUST_NBR1") * objStation.GetD("CUST_NBR2");
            List<double> diagDbl = new List<double>();

            int count = diagDblV.Count;
            double erpvHmax = -1E+10;
            for (int i = 0; i < count; i++)
            {
                double erpv = basePower/*Power - FeederLoss*/ + diagDblV[i];
                double erph = basePower/*Power - FeederLoss*/ + diagDblH[i];

                double erpvh = 10.0 * Math.Log10(Math.Pow(10.0, erpv / 10.0) + Math.Pow(10.0, erph / 10.0));

                if (erpvHmax < erpvh)
                    erpvHmax = erpvh;

                diagDbl.Add(Math.Round(erpvh * 100.0) / 100.0);
            }

            f.Data = diagDbl;
            f.ShowDialog();

            return erpvHmax.ToStringNullD("F3");
        }

        public void OnPressButtonCellMaxKoefV(Cell cell)
        {
            string diagV = objStation.GetS("DIAGV");
            double gain = objStation.GetD("GAIN");
            string polarization = objStation.GetS("POLARIZATION");

            if (!string.IsNullOrEmpty(diagV) && polarization != "H")
            {
                if (diagV.Equals("OMNI"))
                {
                    CellMaxKoefV.Value = gain.ToStringNullD("F3");
                }
                else
                {
                    List<double> diagDblV = HelpFunction.StringToGainedDiag(diagV, gain);
                    CellMaxKoefV.Value = (diagDblV.Max()).ToStringNullD("F3");
                }
            }
            else
            {
                CellMaxKoefV.Value = "не застосовується";
            }
        }

        /// <summary>
        /// Реакция на нажатие кнопки создания адреса
        /// Создание нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(tvDgDiffStation.Position.CityId);
            frm.OldPosition = tvDgDiffStation.Position;            
            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanAttachPhoto = true;
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                tvDgDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                frm.GetFotos(out listFotosDst, out listFotosSrc);
                if (!string.IsNullOrEmpty(tvDgDiffStation.Position.FullAddress))
                    CellAddress.Value = tvDgDiffStation.Position.FullAddress;
                CellLatitude.DoubleValue = tvDgDiffStation.Position.LatDms;
                CellLongitude.DoubleValue = tvDgDiffStation.Position.LonDms;
                CellASL.DoubleValue = tvDgDiffStation.Position.ASl;
            }
        }



        /// <summary>
        /// Створення Комбо боксів для обладнання
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellPowerTr.Value2 = FillValue2(tvDgStation.TvDgEquipment.MaxPower[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerTr.DoubleValue = tvDgDiffStation.TvDgDiffEquipment.MaxPower[PowerUnits.W].Round(3);

            CellTxBandwidth.Value2 = FillValue2(tvDgStation.TvDgEquipment.Bandwidth.ToStringNullD());
            CellTxBandwidth.Value = tvDgDiffStation.TvDgDiffEquipment.Bandwidth.ToStringNullD();

            CellTxDesignEmission.Value2 = FillValue2(tvDgStation.TvDgEquipment.DesigEmission);
            CellTxDesignEmission.Value = tvDgDiffStation.TvDgDiffEquipment.DesigEmission;

            CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, tvDgDiffStation.TvDgDiffEquipment.Certificate);
        }


        //////////////////////////////////////////
        //Необхідні перевантажені методи

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                AdminSiteAllTech.Show(tvDgStation.Position.TableName, tvDgStation.Position.Id);
            }
        }

        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            tvDgDiffStation.Id = recordID.Id;

            /*if (CellLongitude.DoubleValue != CellLongitude.DoubleValue2 ||
            CellLatitude.DoubleValue != CellLatitude.DoubleValue2 ||
            tvDgDiffStation.Position.TableName != ICSMTbl.itblPositionBro)
            {
                tvDgDiffStation.Position.LonDms = CellLongitude.DoubleValue;
                tvDgDiffStation.Position.LatDms = CellLatitude.DoubleValue;
                // Сброс id в IM.NullI заставит создать новую запись                
                tvDgDiffStation.Position.Id = IM.NullI;
                tvDgDiffStation.Position.TableName = PlugTbl.itblXnrfaPositions;
            }

            if (tvDgDiffStation.Position.TableName == PlugTbl.itblXnrfaPositions)
                tvDgDiffStation.Position.SaveToBD();*/

            tvDgDiffStation.Position = 
                CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  tvDgDiffStation.Position,
                  ICSMTbl.itblPositionBro);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            tvDgDiffStation.ShiftOstov = CellEquipmentShiftOstov.DoubleValue;

            tvDgDiffStation.TvDgDiffEquipment.Name = CellEquipmentName.Value;
            tvDgDiffStation.TvDgDiffEquipment.MaxPower[PowerUnits.W] = CellPowerTr.DoubleValue.Round(3);
            tvDgDiffStation.TvDgDiffEquipment.Bandwidth = CellTxBandwidth.DoubleValue;
            tvDgDiffStation.TvDgDiffEquipment.DesigEmission = CellTxDesignEmission.Value;
            tvDgDiffStation.TvDgDiffEquipment.Certificate.Symbol = CellCertificateNumber.Value;
            tvDgDiffStation.TvDgDiffEquipment.Certificate.Date = CellCertificateDate.DateValue;
            tvDgDiffStation.HeightAnten = CellHighAntenn.DoubleValue;
            tvDgDiffStation.Polar = CellAntenPolar.Value.Split(' ')[0].Trim();
            tvDgDiffStation.TvChannel = CellTvChannel.Value;
            tvDgDiffStation.Direct = CellAntenDirect.Value.Split(' ')[0].Trim();
            tvDgDiffStation.FreqCentr = CellMaxFreq.DoubleValue;
            tvDgDiffStation.StatComment = Comment;
            tvDgDiffStation.Save();

            if (listFotosDst != null && listFotosSrc != null)
                tvDgDiffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, TvAnalogDiffStation.TableName, tvStation.Id);

            return true;
        }

        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {

            if (tvDgDiffStation.IsEnableDiff)
            {
                CellAddress.Value = tvDgDiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = tvDgDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = tvDgDiffStation.Position.LatDms;
                CellASL.DoubleValue = tvDgDiffStation.Position.ASl;

                CellEquipmentShiftOstov.DoubleValue = tvDgDiffStation.ShiftOstov;
                CellEquipmentName.Value = tvDgDiffStation.TvDgDiffEquipment.Name;
                CellPowerTr.DoubleValue = tvDgDiffStation.TvDgDiffEquipment.MaxPower[PowerUnits.W];
                CellTxBandwidth.LowerBound = 0.0;
                CellTxBandwidth.DoubleValue = tvDgDiffStation.TvDgDiffEquipment.Bandwidth;
                CellTxDesignEmission.Value = tvDgDiffStation.TvDgDiffEquipment.DesigEmission;

                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, tvDgDiffStation.TvDgDiffEquipment.Certificate);

                CellAntenDirect.Value = tvDgDiffStation.Direct;
                CellAntenPolar.Value = tvDgDiffStation.Polar;
                CellHighAntenn.LowerBound = 0.0;
                CellHighAntenn.DoubleValue = tvDgDiffStation.HeightAnten;
                CellTvChannel.Value = tvDgDiffStation.TvChannel;
                CellMaxFreq.LowerBound = 0.0;
                CellMaxFreq.DoubleValue = tvDgDiffStation.FreqCentr;
                Comment = tvDgDiffStation.StatComment;
                CreateEquipmentComboBoxes();
            }
            else
            {
                tvDgDiffStation.Position = tvDgStation.Position;                
            }
            _initialized = true;
            OnRecalculateErp(null);

        }
        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.TvDigitallDiff;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (_initialized)
            {
                tvDgStation.Power[PowerUnits.W] = CellPowerTr.DoubleValue;
                tvDgStation.Polarization = CellAntenPolar.Value;
                tvDgStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!tvDgStation.HorizontalDiagramm.IsEmpty && tvDgStation.Polarization != "V")
                value = tvDgStation.HorizontalDiagramm.Max.ToStringNullD();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!tvDgStation.VerticalDiagramm.IsEmpty && tvDgStation.Polarization != "H")
                value = tvDgStation.VerticalDiagramm.Max.ToStringNullD();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            tvDgStation.CalculateERP();
            if (tvDgStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = tvDgStation.Erp.Horizontal[PowerUnits.dBW].ToStringNullD("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (tvDgStation.Erp.Vertical.IsValid)
                CellEVPV.Value = tvDgStation.Erp.Vertical[PowerUnits.dBW].ToStringNullD("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = tvDgStation.Erp.Overall[PowerUnits.dBW].ToStringNullD("F3");
        }
    }
}
