﻿using System;
using System.Linq;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStation2AppCommit : BaseCommitClass
    {
        private PositionState selectedPosition;

        private CoordinateCommitHelper coordHelper;


        private MobStation2AppStation mob2Station;
        private MobStation2AppDiffStation mob2DiffStation;

        //6 комірок
        private StationSectorCollection<MobStation2AppStation> mob2StationList;
        private StationSectorCollection<MobStation2AppDiffStation> mob2StationListDiff;
        //Масиви для секторів
        private CellStringProperty[] arrayGeneral;
        private CellDoubleLookupProperty[] arrayAzimuth;
        private CellDoubleLookupProperty[] arrayPlaceSite;
        private CellStringProperty[] arrayTypeAnten;
        private CellDoubleLookupProperty[] arrayHighAnten;
        private CellStringBrowsableProperty[] arrayKoefGain;
        private CellStringBrowsableProperty[] arrayWdthAntena;
        private CellStringComboboxAplyProperty[] arrayPolar;
        private CellStringComboboxAplyProperty[] arrayNomAccept;
        private CellStringComboboxAplyProperty[] arrayNomSend;
        private CellStringComboboxAplyProperty[] arrayOnOff;
        private CellIntegerProperty[] arrayObjRchp;

        #region Cell
        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Максимальна потужність передавача, дБВт        
        /// </summary>
        private CellStringComboboxAplyProperty CellPowerTr = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();

        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        /// <summary>
        /// Типи і параметри модуляцій
        /// </summary>
        private CellStringBrowsableProperty CellModulations = new CellStringBrowsableProperty();

        /// <summary>
        /// Швидкість цифрового потоку, Мбіт/c
        /// </summary>
        private CellDoubleProperty CellSpeedThread = new CellDoubleProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        /// <summary>
        /// Назви заголовків секторів
        /// </summary>
        private CellStringProperty CellGeneral1 = new CellStringProperty();
        private CellStringProperty CellGeneral2 = new CellStringProperty();
        private CellStringProperty CellGeneral3 = new CellStringProperty();
        private CellStringProperty CellGeneral4 = new CellStringProperty();
        private CellStringProperty CellGeneral5 = new CellStringProperty();
        private CellStringProperty CellGeneral6 = new CellStringProperty();

        /// <summary>
        /// Азимут максимального випромінювання, град 
        /// </summary>        
        private CellDoubleLookupProperty CellAglMaxEmission1 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAglMaxEmission2 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAglMaxEmission3 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAglMaxEmission4 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAglMaxEmission5 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAglMaxEmission6 = new CellDoubleLookupProperty();

        /// <summary>
        /// Кут місця максимального випромінювання, град 
        /// </summary>        
        private CellDoubleLookupProperty CellAngleSiteMaxEmission1 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAngleSiteMaxEmission2 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAngleSiteMaxEmission3 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAngleSiteMaxEmission4 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAngleSiteMaxEmission5 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellAngleSiteMaxEmission6 = new CellDoubleLookupProperty();

        /// <summary>
        /// Антена:тип
        /// </summary>        
        private CellStringProperty CellAntennaType1 = new CellStringProperty();
        private CellStringProperty CellAntennaType2 = new CellStringProperty();
        private CellStringProperty CellAntennaType3 = new CellStringProperty();
        private CellStringProperty CellAntennaType4 = new CellStringProperty();
        private CellStringProperty CellAntennaType5 = new CellStringProperty();
        private CellStringProperty CellAntennaType6 = new CellStringProperty();

        /// <summary>
        /// Висота антени над рівнем землі, м
        /// </summary>        
        private CellDoubleLookupProperty CellHighAntenna1 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellHighAntenna2 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellHighAntenna3 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellHighAntenna4 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellHighAntenna5 = new CellDoubleLookupProperty();
        private CellDoubleLookupProperty CellHighAntenna6 = new CellDoubleLookupProperty();

        /// <summary>
        /// Коефіцієнт підсилення, дБі
        /// </summary>
        private CellStringBrowsableProperty CellMaxKoef1 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellMaxKoef2 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellMaxKoef3 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellMaxKoef4 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellMaxKoef5 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellMaxKoef6 = new CellStringBrowsableProperty();


        /// <summary>
        /// Ширина ДС антени(горизонт.) , дБі
        /// </summary>
        private CellStringBrowsableProperty CellWidthAntenna1 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellWidthAntenna2 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellWidthAntenna3 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellWidthAntenna4 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellWidthAntenna5 = new CellStringBrowsableProperty();
        private CellStringBrowsableProperty CellWidthAntenna6 = new CellStringBrowsableProperty();


        /// <summary>
        /// Тип поляризації 
        /// </summary>
        private CellStringComboboxAplyProperty CellAntenPolar1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Номінал частот приймання, МГц
        /// </summary>
        private CellStringComboboxAplyProperty CellNominAccept1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominAccept2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominAccept3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominAccept4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominAccept5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominAccept6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Номінал частот передавання, МГц
        /// </summary>
        private CellStringComboboxAplyProperty CellNominSend1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominSend2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominSend3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominSend4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominSend5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellNominSend6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Сектор ввімкнено/вимкнено
        /// </summary>
        private CellStringComboboxAplyProperty CellOnOff1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP1 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP2 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP3 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP4 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP5 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP6 = new CellIntegerProperty();
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public MobStation2AppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobSta2App.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppBS;
            FillArraySectors();
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);

            //Стандартні значення

            mob2StationList = new StationSectorCollection<MobStation2AppStation>();
            mob2StationList.ApplID = applID;
            mob2StationList.LoadSectorsOrSingle(id);
            mob2Station = mob2StationList.GetSectorByIndex(0);

            mob2Station.Id = recordID.Id;
            // mob2Station.Load();

            //URZP значення
            mob2StationListDiff = new StationSectorCollection<MobStation2AppDiffStation>();
            mob2StationListDiff.ApplID = applID;
            mob2StationListDiff.LoadSectorsOrSingle(id);

            mob2DiffStation = mob2StationListDiff.GetSectorByIndex(0);
            mob2DiffStation.Id = recordID.Id;
            // mob2DiffStation.Load();
        }

        /// <summary>
        /// Зчитування стандартних даних для форми
        /// </summary>
        private void ReadAllDataDefaultPositions()
        {
            for (int i = 0; i < 6; i++)
                arrayGeneral[i].Value = "Сектор" + (i + 1);

            CellASL.DoubleValue = mob2Station.Position.ASl;
            CellSpeedThread.DoubleValue = mob2Station.SpeedThread;
            CellPowerTr.Value = mob2Station.Power.ToStringNullD();
            CellPowerTr.Items.Add(mob2Station.Power.ToStringNullD());
            CellTxBandwidth.Value = mob2Station. mobStationEquipment.Bandwidth.ToStringNullD();
            CellTxDesignEmission.Value = mob2Station.ClassEmission;

            if (mob2Station.ModulationList.Count <= mob2Station.Modulation || mob2Station.Modulation < 0)
                mob2Station.Modulation = 0;

            CellModulations.Value = mob2Station.ModulationList[mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)];
            CellCertificateNumber.Value = mob2Station.mobStationEquipment.Certificate.Symbol;
            CellCertificateDate.Value = mob2Station.mobStationEquipment.Certificate.Date.ToShortDateString();
            CellEquipmentName.Value = mob2Station.NameRez;

            //заповнення 6 комірок
            int sectorCount = mob2StationList.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation2AppStation currentSector = mob2StationList.GetSectorByIndex(i);
                currentSector.Load();

                arrayTypeAnten[i].Value = currentSector.TypeAnten;

                arrayNomAccept[i].Value = currentSector.NominAccept;
                arrayNomSend[i].Value = currentSector.NominSend;

                //arrayTypeAnten[i].Items.Add(currentSector.TypeAnten);

                arrayAzimuth[i].DoubleValue = currentSector.Azimuth;
                arrayAzimuth[i].Items.Clear();
                arrayAzimuth[i].Items.Add(currentSector.Azimuth);

                arrayHighAnten[i].DoubleValue = currentSector.AGL;
                arrayHighAnten[i].Items.Clear();
                arrayHighAnten[i].Items.Add(currentSector.AGL);

                arrayPlaceSite[i].DoubleValue = currentSector.PlaceSite;
                arrayPlaceSite[i].Items.Clear();
                arrayPlaceSite[i].Items.Add(currentSector.PlaceSite);

                arrayKoefGain[i].Value = currentSector.Koef.ToStringNullD();
                arrayWdthAntena[i].Value = currentSector.WideAnten.ToStringNullD();

                arrayNomAccept[i].Value = currentSector.NominAccept;
                arrayNomAccept[i].Items.Add(currentSector.NominAccept);

                arrayNomSend[i].Value = currentSector.NominSend;
                arrayNomSend[i].Items.Add(currentSector.NominSend);

                arrayObjRchp[i].IntValue = currentSector.Id;
                if (!string.IsNullOrEmpty(currentSector.PolarAnt))
                {
                    arrayPolar[i].Value = currentSector.PolarAnt + " - " + currentSector.PolarAntDict[currentSector.PolarAnt];
                    arrayPolar[i].Items.Add(currentSector.PolarAnt + " - " + currentSector.PolarAntDict[currentSector.PolarAnt]);
                }
            }
        }

        /// <summary>
        /// Обробник події зміни адреси в комбобоксі
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="newValue"></param>
        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            coordHelper.Paint(mob2DiffStation.Position, mob2Station.Position);
        }

        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                mob2Station.mobStationEquipment.TableName = newEquip.Table;
                mob2Station.mobStationEquipment.Id = newEquip.Id;
                mob2Station.mobStationEquipment.Load();

                mob2Station.Bandwith = mob2Station.mobStationEquipment.Bandwidth;
                mob2Station.ClassEmission = mob2Station.mobStationEquipment.DesigEmission;
                mob2Station.Modulation = Convert.ToInt32(mob2Station.mobStationEquipment.Modulation);

                InitEquipment();

                mob2Station.Power = mob2Station.mobStationEquipment.MaxPower[PowerUnits.dBm];
                CellPowerTr.Items.Clear();
                //Введені дані більші 
                if (mob2Station.Power >= mob2DiffStation.MaxPower)
                {
                    CellPowerTr.Value = mob2DiffStation.MaxPower.ToStringNullD();
                    CellPowerTr.BackColor = CellStringProperty.ColorBgr(2);
                }
                else
                {
                    CellPowerTr.Value = mob2Station.Power.ToStringNullD();
                    CellPowerTr.Items.Add(mob2Station.Power.ToStringNullD());
                    CellPowerTr.Items.Add(mob2DiffStation.MaxPower.ToStringNullD());
                    CellPowerTr.BackColor = CellStringProperty.ColorBgr(0);
                }

                if (CellPowerTr.Items.Count > 1)
                    if (CellPowerTr.Items[0] != mob2Station.Power.ToStringNullD())
                        CellPowerTr.Items[1] = mob2Station.Power.ToStringNullD();

                PaintGrid();
            }
        }

        public void InitAntenna(int sectorIndex)
        {

            MobStation2AppStation mobSector = mob2StationList.GetSectorByIndex(sectorIndex);
            MobStation2AppDiffStation mobDiffSector = mob2StationListDiff.GetSectorByIndex(sectorIndex);

            arrayTypeAnten[sectorIndex].Value = mobSector.mobStationAntena.Name;

            CellStringBrowsableProperty.InitalizeHelper(arrayKoefGain[sectorIndex],
               mobSector.mobStationAntena.Gain.ToStringNullD(),
               mobDiffSector.KoefAnten.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(arrayWdthAntena[sectorIndex],
               mobSector.mobStationAntena.AntennaWidth.ToStringNullD(),
               mobDiffSector.WideAnten.ToStringNullD());
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = mob2Station.mobStationEquipment.Name;
            CellTxBandwidth.Value = mob2Station.mobStationEquipment.Bandwidth.ToStringNullD();
            CellTxDesignEmission.Value = mob2Station.ClassEmission;
            CellModulations.Value = mob2Station.ModulationList[mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)];
            CellCertificateNumber.Value = mob2Station.mobStationEquipment.Certificate.Symbol;
            CellCertificateDate.Value = mob2Station.mobStationEquipment.Certificate.Date.ToShortDateString();
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + criteria + "*\"}";

            RecordPtr recEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipMob2, param);
            return recEquip;
        }

        /// <summary>
        /// Створення Комбо боксів для антени(які не можуть бути вибраними) при запуску
        /// </summary>
        private void CreateAntenaComboBoxes(int sectorIndex)
        {
            MobStation2AppStation mobSector = mob2StationList.GetSectorByIndex(sectorIndex);
            MobStation2AppDiffStation mobDiffSector = mob2StationListDiff.GetSectorByIndex(sectorIndex);

            CellStringBrowsableProperty.InitalizeHelper(arrayKoefGain[sectorIndex],
                mobSector.Koef.ToStringNullD(),
                mobDiffSector.KoefAnten.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(arrayWdthAntena[sectorIndex],
               mobSector.WideAnten.ToStringNullD(),
               mobDiffSector.WideAnten.ToStringNullD());
        }


        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, mob2Station.mobStationEquipment.Bandwidth.ToStringNullD(), mob2DiffStation.Bandwith.ToStringNullD());
            CellStringBrowsableProperty.InitalizeHelper(CellModulations, mob2Station.ModulationList[mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)], mob2Station.ModulationList[Convert.ToInt32(mob2DiffStation.Modulation)]);
            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, mob2Station.ClassEmission, mob2DiffStation.ClassEmission);

            //Поменять поведение, если сертификату введен прочерк. 
            if (mob2DiffStation.mobDiffStationEquip.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, mob2Station.mobStationEquipment.Certificate.Symbol, mob2DiffStation.mobDiffStationEquip.Certificate.Symbol);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, mob2Station.mobStationEquipment.Certificate.Date.ToShortDateString(), mob2DiffStation.mobDiffStationEquip.Certificate.Date.ToShortDateString());
        }

        //Формування масивів секторів...
        private void FillArraySectors()
        {
            arrayGeneral = new[]
                               {
                                   CellGeneral1,
                                   CellGeneral2,
                                   CellGeneral3,
                                   CellGeneral4,
                                   CellGeneral5,
                                   CellGeneral6
                               };

            arrayAzimuth = new[]
                               {
                                   CellAglMaxEmission1,
                                   CellAglMaxEmission2,
                                   CellAglMaxEmission3,
                                   CellAglMaxEmission4,
                                   CellAglMaxEmission5,
                                   CellAglMaxEmission6
                               };
            arrayPlaceSite = new[]
                                 {
                                     CellAngleSiteMaxEmission1,
                                     CellAngleSiteMaxEmission2,
                                     CellAngleSiteMaxEmission3,
                                     CellAngleSiteMaxEmission4,
                                     CellAngleSiteMaxEmission5,
                                     CellAngleSiteMaxEmission6
                                 };
            arrayTypeAnten = new[]
                                 {
                                     CellAntennaType1,
                                     CellAntennaType2,
                                     CellAntennaType3,
                                     CellAntennaType4,
                                     CellAntennaType5,
                                     CellAntennaType6
                                 };
            arrayHighAnten = new[]
                                 {
                                     CellHighAntenna1,
                                     CellHighAntenna2,
                                     CellHighAntenna3,
                                     CellHighAntenna4,
                                     CellHighAntenna5,
                                     CellHighAntenna6
                                 };
            arrayKoefGain = new[]
                                {
                                    CellMaxKoef1,
                                    CellMaxKoef2,
                                    CellMaxKoef3,
                                    CellMaxKoef4,
                                    CellMaxKoef5,
                                    CellMaxKoef6
                                };
            arrayWdthAntena = new[]
                                  {
                                      CellWidthAntenna1,
                                      CellWidthAntenna2,
                                      CellWidthAntenna3,
                                      CellWidthAntenna4,
                                      CellWidthAntenna5,
                                      CellWidthAntenna6
                                  };
            arrayPolar = new[]
                             {
                                 CellAntenPolar1,
                                 CellAntenPolar2,
                                 CellAntenPolar3,
                                 CellAntenPolar4,
                                 CellAntenPolar5,
                                 CellAntenPolar6  
                             };
            arrayNomAccept = new[]
                                 {
                                     CellNominAccept1,
                                     CellNominAccept2,
                                     CellNominAccept3,
                                     CellNominAccept4,
                                     CellNominAccept5,
                                     CellNominAccept6
                                 };
            arrayNomSend = new[]
                               {
                                   CellNominSend1,
                                   CellNominSend2,
                                   CellNominSend3,
                                   CellNominSend4,
                                   CellNominSend5,
                                   CellNominSend6
                               };

            arrayOnOff = new[]
                             {
                                 CellOnOff1,
                                 CellOnOff2,
                                 CellOnOff3,
                                 CellOnOff4,
                                 CellOnOff5,
                                 CellOnOff6
                             };

            arrayObjRchp = new[]
                               {
                                   CellObjRCHP1,
                                   CellObjRCHP2,
                                   CellObjRCHP3,
                                   CellObjRCHP4,
                                   CellObjRCHP5,
                                   CellObjRCHP6
                               };
        }

        //Необхідні перегружені методи

        /// <summary>
        /// Ініціалізація параметрів гріда
        /// </summary>
        /// <param name="grid">Грід</param>
        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            ReadAllDataDefaultPositions();
            #region IsDiff
            if (mob2DiffStation.IsEnableDiff)
            {
                if (!string.IsNullOrEmpty(mob2DiffStation.Position.ASl.ToStringNullD()))
                    CellASL.DoubleValue = mob2DiffStation.Position.ASl;

                CellCertificateNumber.Value = mob2DiffStation.mobDiffStationEquip.Certificate.Symbol;
                CellCertificateDate.Value = mob2DiffStation.mobDiffStationEquip.Certificate.Date.ToShortDateString();

                if (!string.IsNullOrEmpty(mob2DiffStation.Modulation))
                    if (mob2Station.ModulationList.Count >= mob2DiffStation.Modulation.ToInt32(IM.NullI))
                        if (mob2DiffStation.Modulation.Trim() != mob2Station.mobStationEquipment.Modulation.Trim())
                            CellModulations.Items.Add(mob2Station.ModulationList[mob2DiffStation.Modulation.ToInt32(IM.NullI)]);
                CellTxBandwidth.Value = mob2DiffStation.Bandwith.ToStringNullD();
                CellModulations.Value = mob2Station.ModulationList[mob2DiffStation.Modulation.ToInt32(IM.NullI)];
                if (mob2DiffStation.MaxPower.ToStringNullD() != CellPowerTr.Items[0])
                {
                    CellPowerTr.Items.Add(mob2DiffStation.MaxPower.ToStringNullD());
                    CellPowerTr.BackColor = CellStringProperty.ColorBgr(0);
                }
                else
                    CellPowerTr.BackColor = CellStringProperty.ColorBgr(2);
                CellTxDesignEmission.Value = mob2DiffStation.ClassEmission;

                //Заповнення строчок з 6 елементами                
                int sectorCount = mob2StationList.GetSectorCount();
                for (int i = 0; i < sectorCount; i++)
                {
                    MobStation2AppDiffStation currentSectorDiff = mob2StationListDiff.GetSectorByIndex(i);
                    MobStation2AppStation currentSector = mob2StationList.GetSectorByIndex(i);
                    currentSectorDiff.Load();

                    arrayAzimuth[i].Items.Add(currentSectorDiff.AglMaxEmission);
                    arrayAzimuth[i].Initialize();
                    FillColorComboBox(arrayAzimuth[i]);

                    arrayHighAnten[i].Items.Add(currentSectorDiff.HighAnten);
                    arrayHighAnten[i].Initialize();
                    FillColorComboBox(arrayHighAnten[i]);

                    arrayPlaceSite[i].Items.Add(currentSectorDiff.AglSiteMax);
                    arrayPlaceSite[i].Initialize();
                    FillColorComboBox(arrayPlaceSite[i]);

                    arrayKoefGain[i].Value = currentSectorDiff.KoefAnten.ToStringNullD();

                    arrayWdthAntena[i].Value = currentSectorDiff.WideAnten.ToStringNullD();

                    FillDiffComboBox(arrayNomAccept[i], currentSector.NominAccept, currentSectorDiff.NomReceive);
                    FillDiffComboBox(arrayNomSend[i], currentSector.NominSend, currentSectorDiff.NomSend);
                    string polar = "";
                    string polarDiff="";
                    if (mob2Station.PolarAntDict.ContainsKey(currentSector.PolarAnt))
                        polar = currentSectorDiff.PolarAnten + " - " +
                                           mob2Station.PolarAntDict[currentSector.PolarAnt];
                    if (mob2Station.PolarAntDict.ContainsKey(currentSectorDiff.PolarAnten))
                        polarDiff = currentSectorDiff.PolarAnten + " - " +
                                           mob2Station.PolarAntDict[currentSectorDiff.PolarAnten];
                    //if (!string.IsNullOrEmpty(currentSectorDiff.PolarAnten))
                    //    if (arrayPolar[i].Items.Count > 0)
                    //        if (currentSectorDiff.PolarAnten != arrayPolar[i].Items[0].Split(' ')[0].Trim())
                    //            arrayPolar[i].Items.Add(currentSectorDiff.PolarAnten + " - " + mob2Station.PolarAntDict[currentSectorDiff.PolarAnten]);
                    FillDiffComboBox(arrayPolar[i], polar, polarDiff);

                    //if (arrayNomAccept[i].Items[0] != currentSectorDiff.NomReceive)
                    //    arrayNomAccept[i].Items.Add(currentSectorDiff.NomReceive);
                    //FillColorComboBox(arrayNomAccept[i]);

                    //if (arrayNomSend[i].Items[0] != currentSectorDiff.NomSend)
                    //    arrayNomSend[i].Items.Add(currentSectorDiff.NomSend);
                    //FillColorComboBox(arrayNomSend[i]);
                    arrayOnOff[i].Value = mob2Station.OnOffList[currentSectorDiff.OnOff];
                    Comment = currentSectorDiff.StatComment;
                }
            }
            #endregion

            int sectCount = mob2StationList.GetSectorCount();
            for (int i = 0; i < 6; i++)
            {
                if (i >= sectCount)
                {
                    FillCellUnvisible(arrayAzimuth[i].Cell);
                    FillCellUnvisible(arrayPlaceSite[i].Cell);
                    FillCellUnvisible(arrayTypeAnten[i].Cell);
                    FillCellUnvisible(arrayKoefGain[i].Cell);
                    FillCellUnvisible(arrayWdthAntena[i].Cell);
                    FillCellUnvisible(arrayPolar[i].Cell);
                    FillCellUnvisible(arrayNomAccept[i].Cell);
                    FillCellUnvisible(arrayNomSend[i].Cell);
                    FillCellUnvisible(arrayObjRchp[i].Cell);
                    FillCellUnvisible(arrayHighAnten[i].Cell);
                    FillCellUnvisible(arrayGeneral[i].Cell);
                    FillCellUnvisible(arrayOnOff[i].Cell);
                    // grid.GetCellFromKey("SectorHeader-" + (i + 1).ToString()).Visible = false;                    
                }
            }

            coordHelper.Initialize(mob2DiffStation.Position, mob2Station.Position);
            CreateEquipmentComboBoxes();
            for (int i = 0; i < sectCount; i++)
            {
                CreateAntenaComboBoxes(i);

                //CellOnOff[i].Value = sector_on;
                if (mob2StationListDiff.GetSectorByIndex(i).OnOff == 0)
                    arrayOnOff[i].BackColor = CellStringProperty.ColorBgr(0);
            }
        }

        private void FillColorComboBox(CellDoubleLookupProperty doubleLookUp)
        {
            if (doubleLookUp.Items.Count == 2)
                if (doubleLookUp.Items[0] != doubleLookUp.Items[1])
                    doubleLookUp.BackColor = CellStringProperty.ColorBgr(0);
        }

        public void OnPressButtonAntennaName(Cell cell)
        {
            int index = Convert.ToInt32(cell.Key.Substring(cell.Key.Count() - 1));

            RecordPtr newAntenna = LookupAntenna(cell.Value, true);

            if ((newAntenna.Id > 0) && (newAntenna.Id < IM.NullI))
            {
                MobStation2AppStation mobSector = mob2StationList.GetSectorByIndex(index - 1);

                mobSector.mobStationAntena.TableName = newAntenna.Table;
                mobSector.mobStationAntena.Id = newAntenna.Id;
                mobSector.mobStationAntena.Load();

                InitAntenna(index - 1);

                PaintGrid();

            }
        }

        public void PaintGrid()
        {
            coordHelper.Paint(mob2DiffStation.Position, mob2Station.Position);
        }


        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
            string criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + criteria + "*\"}";

            RecordPtr recAnt = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntennaMob2, param);
            return recAnt;
        }

        /// <summary>
        /// Перетворення переданої комірки Гріда в невидиму
        /// </summary>
        /// <param name="cellObj">Комірка</param>
        private static void FillCellUnvisible(Cell cellObj)
        {
            cellObj.Visible = false;
            cellObj.TabOrder = 0;
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStation2AppCommit;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(mob2Station.Position.TableName, mob2Station.Position.Id);
                rcPtr.UserEdit();
            }
        }

        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "MOB_STATION2" };

            PositionState diffPos = mob2DiffStation.Position.Clone() as PositionState;
            AttachmentControl ac = new AttachmentControl();

            selectedPosition = coordHelper.GetSelectedPosition(mob2Station.Position, mob2DiffStation.Position);
            if (selectedPosition != mob2Station.Position)
            {
                mob2Station.PosId = SaveNewPosition(selectedPosition, mob2Station.Position, assgnReferenceTables);
                mob2Station.Position = selectedPosition;
            }

            mob2Station.Position.ASl = CellASL.DoubleValue;
            mob2Station.Position.LatDms = CellLatitude.DoubleValue;
            mob2Station.Position.LonDms = CellLongitude.DoubleValue;
            if (CellAddress.Value != "1")
                mob2Station.Position.FullAddress = mob2DiffStation.Position.FullAddress;

            mob2Station.ClassEmission = CellTxDesignEmission.Value;
            mob2Station.Modulation = mob2Station.ModulationList.IndexOf(CellModulations.Value);
            mob2Station.NameRez = CellEquipmentName.Value;

            mob2Station.Power = CellPowerTr.Value.ToDouble(IM.NullD);
            mob2Station.Bandwith = CellPowerTr.Value.ToDouble(IM.NullD);
            // mob2Station.Save();

            //Зберігання строчок з 6 комірками
            int sectorCount = mob2StationList.GetSectorCount();
            StationSectorCollection<MobStation2AppStation> savedMob2Sectors = new StationSectorCollection<MobStation2AppStation>();

            for (int i = 0; i < sectorCount; i++)
            {
                MobStation2AppStation cloneSector = mob2StationList.GetSectorByIndex(i).Clone() as MobStation2AppStation;

                if (cloneSector != null)
                {
                    cloneSector.Id = mob2StationList.GetSectorByIndex(i).Id;

                    cloneSector.Position = selectedPosition;
                    cloneSector.TypeAnten = arrayTypeAnten[i].Value;
                    cloneSector.Koef = Convert.ToDouble(arrayKoefGain[i].Value);//**
                    cloneSector.PlaceSite = arrayPlaceSite[i].LookupedDoubleValue; // Elevation
                    cloneSector.AGL = arrayHighAnten[i].LookupedDoubleValue;
                    cloneSector.WideAnten = Convert.ToDouble(arrayWdthAntena[i].Value); // **
                    cloneSector.NominAccept = arrayNomAccept[i].Value; // RxFrequency
                    cloneSector.NominSend = arrayNomSend[i].Value;     // TxFrequency
                    cloneSector.Azimuth = arrayAzimuth[i].LookupedDoubleValue;
                    cloneSector.PolarAnt = arrayPolar[i].Value.Split(' ')[0].Trim(); //Polarization
                    cloneSector.OnOff = mob2Station.OnOffList.IndexOf(arrayOnOff[i].Value);
                    cloneSector.ApplId = ApplID;
                    cloneSector.StatComment = mob2DiffStation.StatComment;
                    savedMob2Sectors.Add(cloneSector);
                }
            }
            savedMob2Sectors.ApplID = applID;
            savedMob2Sectors.SaveSectorsOrSingle(true);

            //sectorCount = _mobDiffStations.GetSectorCount();
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation2AppDiffStation mob2DiffSector = mob2StationListDiff.GetSectorByIndex(i);
                mob2DiffSector.Remove();
            }

            ac.RelocateAndCopyDocLinks(
               diffPos.TableName,
               diffPos.Id,
               MobSta2App.TableName,
               savedMob2Sectors.GetIdList(),
               mob2Station.Position.TableName,
               mob2Station.Position.Id,
               ICSMTbl.itblAdmSite,
               mob2Station.Position.AdminSitesId);

            return true;
        }
    }
}
