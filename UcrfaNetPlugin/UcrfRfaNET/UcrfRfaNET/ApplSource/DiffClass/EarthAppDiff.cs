﻿using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.Documents;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class EarthAppDiff : BaseDiffClass
    {
        private EarthStation station = new EarthStation();
        private EarthDiffStation diffStation = new EarthDiffStation();
        public bool initialized = false;

        #region Cell Propertues
        private CellStringProperty CellNetworkName = new CellStringProperty();        // Назва супутникової мережі
        private CellDoubleProperty CellOrbitalPosition = new CellDoubleProperty();    // Орбітальна позиція
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();          // Широта (гр.хв.сек.)
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();           // Довгота (гр.хв.сек.)
        private CellDoubleProperty CellASL = new CellDoubleProperty();                // Висота поверхні землі
        private CellDoubleBoundedProperty CellAzimuth = new CellDoubleBoundedProperty();            // Азимут випромінювання, град
        private CellDoubleBoundedProperty CellElevation = new CellDoubleBoundedProperty();          // Кут місця випромінювання, град
        private CellStringProperty CellAddress = new CellStringProperty();            // Адреса місця встановлення РЕЗ
        private CellStringProperty CellEquipmentName = new CellStringProperty();      // Назва/Тип РЕЗ
        private CellDoubleProperty CellMaxPower = new CellDoubleProperty();           // Макс. потужність передавача на вх. антени, дБВт\
        private CellDoubleProperty CellTxBandwidth = new CellDoubleProperty();        // Ширина смуги передавача, кГц; 
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();   // клас випромінювання передавача
        private CellStringProperty CellTxModulation = new CellStringProperty();       // Тип і параметри модуляції випром., що передаються
        private CellDoubleProperty CellRxBandwidth = new CellDoubleProperty();        // Ширина смуги передавача, кГц; 
        private CellStringProperty CellRxDesignEmission = new CellStringProperty();   // клас випромінювання передавача
        private CellStringProperty CellRxModulation = new CellStringProperty();       // Тип і параметри модуляції випром., що передаються
        private CellStringProperty CellCertificateNumber = new CellStringProperty();  // Номер сертифіката відповідності
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();    // Дата сертифіката відповідності
        private CellDoubleProperty CellNoiseTemperature = new CellDoubleProperty();   // Шумова температура приймальної системи, К
        private CellStringProperty CellAntennaType = new CellStringProperty();        // Тип антени
        private CellDoubleBoundedProperty CellAGL = new CellDoubleBoundedProperty();                // Висота антени над рівнем землі, м
        private CellDoubleBoundedProperty CellDiameter = new CellDoubleBoundedProperty();           // Діаметр антени, м
        private CellDoubleProperty CellTxGain = new CellDoubleProperty();             // Коефіцієнт підсилення передавання, дБі
        private CellDoubleProperty CellRxGain = new CellDoubleProperty();             // Коефіцієнт підсилення приймання, дБі
        private CellStringProperty CellRxPolarization = new CellStringProperty();     // Тип поляризації: у режимі приймання
        private CellStringProperty CellRxFrequency = new CellStringProperty();        // Номінали робочих частот у режимі приймання, ГГц
        private CellStringProperty CellTxPolarization = new CellStringProperty();     // Тип поляризації: у режимі передавання
        private CellStringProperty CellTxFrequency = new CellStringProperty();        // Номінали робочих частот у режимі передав., ГГц
        private CellStringProperty CellObjectID = new CellStringProperty();           // Об"єкт(и) РЧП                  
        #endregion

        //===================================================
        public EarthAppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, EarthApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppZS;

            station = new EarthStation();            
            station.Id = id;
            station.Load();

            diffStation = new EarthDiffStation();
            diffStation.Id = id;
            diffStation.Load();
            SCVisnovok = station.Finding;

            Comment = diffStation.StatComment;
            CellAzimuth.LowerBound = -180;
            CellAzimuth.UpperBound = 360;
            CellElevation.LowerBound = -90;
            CellElevation.UpperBound = 90;
            CellAGL.LowerBound = -200;
            CellAGL.UpperBound = 500;
            CellDiameter.LowerBound = 0;            

            CellCertificateDate.NullString = "-";
            _stationObject = station;
        }

        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Cell cell)
        {

        }

        //===========================================================
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            InitStationObject(recID);
            //BindGrid(grid);         
        }

        //===========================================================
        /// <summary>
        /// Will be changed...
        /// </summary>
        protected override string GetXMLParamGrid()
        {
            return XICSM.UcrfRfaNET.Properties.Resources.EarthAppDiff;
        }

        /// <summary>
        ///===================================================
        /// Load data from database to Station object
        ///===================================================
        /// </summary>
        /// <param name="objectID"></param>
        /// 
        public void InitStationObject(int objectID)
        {
            CellMaxPower.FormatString = "N1";

            CellLongitude.DoubleValue = station.Position.LonDms;
            CellLatitude.DoubleValue = station.Position.LatDms;
            CellASL.DoubleValue = station.Position.ASl;
            CellAddress.Value = station.Position.FullAddress;

            CellAGL.DoubleValue = station.AGL;
            CellNetworkName.Value = station.NetworkName;
            CellOrbitalPosition.DoubleValue = station.OrbitalPosition;
            CellAzimuth.DoubleValue = station.Azimuth;
            CellElevation.DoubleValue = station.Elevation;
            CellObjectID.Value = objectID.ToString();
            CellRxFrequency.Value = HelpFunction.ToString(station.RxFrequencies);
            CellTxFrequency.Value = HelpFunction.ToString(station.TxFrequencies);
            CellEquipmentName.Value = station.Equipment.Name;
            CellRxBandwidth.DoubleValue = station.Equipment.RxBandwidth;
            CellTxBandwidth.DoubleValue = station.Equipment.TxBandwidth;
            CellRxDesignEmission.Value = station.Equipment.RxDesigEmission;
            CellTxDesignEmission.Value = station.Equipment.TxDesigEmission;
            CellTxModulation.Value = station.Equipment.TxModulation;
            CellRxModulation.Value = station.Equipment.RxModulation;
            CellMaxPower.DoubleValue = station.Power[PowerUnits.dBW];/* Math.Max(
            station.Equipment.MaxPower[PowerUnits.dBW],
            station.Equipment.MinPower[PowerUnits.dBW]); */
            CellNoiseTemperature.DoubleValue = station.Equipment.NoiseTemperature;

            CellTxPolarization.Value = station.TxPolarization;
            CellRxPolarization.Value = station.RxPolarization;
            CellCertificateDate.DateValue = station.Equipment.Certificate.Date;
            CellCertificateNumber.Value = station.Equipment.Certificate.Symbol;

            CellAntennaType.Value = station.Antenna.Name;
            CellDiameter.DoubleValue = station.Antenna.Diameter;
            CellTxGain.DoubleValue = station.Antenna.TxGain;
            CellRxGain.DoubleValue = station.Antenna.RxGain;
            CellAGL.DoubleValue = station.AGL;

            //---
            CellLongitude.DoubleValue2 = station.Position.LonDms;
            CellLatitude.DoubleValue2 = station.Position.LatDms;
            CellAddress.Value2 = station.Position.FullAddress;

            CellAGL.DoubleValue2 = station.AGL;
            CellAzimuth.DoubleValue2 = station.Azimuth;
            CellRxFrequency.Value2 = CellRxFrequency.Value;
            CellTxFrequency.Value2 = CellTxFrequency.Value;
            CellEquipmentName.Value2 = station.Equipment.Name;
            CellRxBandwidth.DoubleValue2 = station.Equipment.RxBandwidth;
            CellTxBandwidth.DoubleValue2 = station.Equipment.TxBandwidth;
            CellRxDesignEmission.Value2 = station.Equipment.RxDesigEmission;
            CellTxDesignEmission.Value2 = station.Equipment.TxDesigEmission;
            CellTxModulation.Value2 = station.Equipment.TxModulation;
            CellRxModulation.Value2 = station.Equipment.RxModulation;
            CellMaxPower.DoubleValue2 = CellMaxPower.DoubleValue;
            CellTxPolarization.Value2 = station.TxPolarization;
            CellRxPolarization.Value2 = station.RxPolarization;
            CellCertificateDate.DateValue2 = station.Equipment.Certificate.Date;
            CellCertificateNumber.Value2 = station.Equipment.Certificate.Symbol;

            CellAntennaType.Value2 = station.Antenna.Name;
            CellDiameter.DoubleValue2 = station.Antenna.Diameter;
            CellTxGain.DoubleValue2 = station.Antenna.TxGain;
            CellRxGain.DoubleValue2 = station.Antenna.RxGain;            
            CellAGL.DoubleValue2 = station.AGL;

            CellDiameter.SetRedValue();
            CellAGL.SetRedValue();
            CellCertificateDate.SetRedValue();
            CellCertificateDate.SetRedValue();
            CellMaxPower.SetRedValue();
            CellRxBandwidth.SetRedValue();
            CellTxBandwidth.SetRedValue();
            CellAzimuth.SetRedValue();
            //CellElevation.SetRedValue();
            CellRxDesignEmission.SetRedValue();
            CellTxDesignEmission.SetRedValue();
            CellRxGain.SetRedValue();
            CellTxGain.SetRedValue();
            CellTxFrequency.SetRedValue();
            CellRxFrequency.SetRedValue();

            if (diffStation.IsInitialized)
            {
                //***         
                CellEquipmentName.Value = diffStation.Equipment.Name;
                CellMaxPower.DoubleValue = diffStation.Power[PowerUnits.dBW];
                CellRxBandwidth.DoubleValue = diffStation.Equipment.RxBandwidth;
                CellTxBandwidth.DoubleValue = diffStation.Equipment.TxBandwidth;
                CellRxDesignEmission.Value = diffStation.Equipment.RxDesigEmission;
                CellTxDesignEmission.Value = diffStation.Equipment.TxDesigEmission;
                CellRxModulation.Value = diffStation.Equipment.RxModulation;
                CellTxModulation.Value = diffStation.Equipment.TxModulation;
                CellCertificateNumber.Value = diffStation.Equipment.Certificate.Symbol;
                CellCertificateDate.DateValue = diffStation.Equipment.Certificate.Date;

                CellAzimuth.DoubleValue = diffStation.Azimuth;
                //diffStation.Eq Power[PowerUnits.dBW] = CellMaxPower1.DoubleValue;
                CellAGL.DoubleValue = diffStation.AGL;

                //diffStation.Antenna.Id = station.Antenna.Id;
                CellAntennaType.Value = diffStation.Antenna.Name;
                CellDiameter.DoubleValue = diffStation.Antenna.Diameter;
                CellRxGain.DoubleValue = diffStation.Antenna.RxGain;
                CellTxGain.DoubleValue = diffStation.Antenna.TxGain;
                CellRxPolarization.Value = diffStation.RxPolarization;
                CellTxPolarization.Value = diffStation.TxPolarization;
                CellRxFrequency.Value = HelpFunction.ToString(diffStation.RxFrequencies);
                CellTxFrequency.Value = HelpFunction.ToString(diffStation.TxFrequencies);

                CellLongitude.DoubleValue = diffStation.Position.LonDms;
                CellLatitude.DoubleValue = diffStation.Position.LatDms;
                CellASL.DoubleValue = diffStation.Position.ASl;
                CellAddress.Value = diffStation.Position.FullAddress;

            } else
            {
                diffStation.Position = station.Position.Clone() as PositionState;                
            }
            
            /*         
            OriginalValues.AGL = CellAGL.DoubleValue;
            OriginalValues.Antenna = Antenna;
            OriginalValues.Equipment = Equipment;
            OriginalValues.Position = Position;
            OriginalValues.TxBandwidth = CellTxBandwidth.DoubleValue;
            OriginalValues.RxBandwidth = CellRxBandwidth.DoubleValue;
            OriginalValues.Azimuth = CellAzimuth.DoubleValue;
            OriginalValues.Diameter = CellDiameter.DoubleValue;
            OriginalValues.TxDesignEmission = CellTxDesignEmission.Value;
            OriginalValues.RxDesignEmission = CellRxDesignEmission.Value;
            OriginalValues.TxPolarization = TxPolarization;
            OriginalValues.RxPolarization = TxPolarization;
            OriginalValues.TxFrequency = new List<double>(st.nomFreqTx);
            OriginalValues.RxFrequency = new List<double>(st.nomFreqRx);
            OriginalValues.MaxPower = CellMaxPower.DoubleValue;
            OriginalValues.CertificateNumber = CertificateNo;
            OriginalValues.CertificateDate = CertificateDate;         
               
            CellNetworkName.Value2 = CellNetworkName.Value;
            CellOrbitalPosition.DoubleValue2 = CellOrbitalPosition.DoubleValue;
            CellLongitude.DoubleValue2 = CellLongitude.DoubleValue;
            CellLatitude.DoubleValue2 = CellLatitude.DoubleValue;
            CellASL.DoubleValue2 = CellASL.DoubleValue;
            CellAzimuth.DoubleValue2 = CellAzimuth.DoubleValue;
            CellElevation.DoubleValue2 = CellElevation.DoubleValue;
            CellAddress.Value2 = CellAddress.Value;         
            CellEquipmentName.Value2 = CellEquipmentName.Value;
            CellMaxPower.DoubleValue2 = CellMaxPower.DoubleValue;
            CellTxBandwidth.DoubleValue2 = CellTxBandwidth.DoubleValue;               
            CellTxDesignEmission.Value2 = CellTxDesignEmission.Value;                           
            CellTxModulation.Value2 = CellTxModulation.Value;
            CellRxBandwidth.DoubleValue2 = CellRxBandwidth.DoubleValue;
            CellRxDesignEmission.Value2 = CellRxDesignEmission.Value;
            CellRxModulation.Value2 = CellRxModulation.Value;
            CellCertificateNumber.Value2 = CellCertificateNumber.Value;
            CellCertificateDate.Value2 = CellCertificateDate.Value;
            CellNoiseTemperature.DoubleValue2 = CellNoiseTemperature.DoubleValue;
            CellAntennaType.Value2 = CellAntennaType.Value;
            CellAGL.DoubleValue2 = CellAGL.DoubleValue;
            CellDiameter.DoubleValue2 = CellDiameter.DoubleValue;      
            CellTxGain.DoubleValue2 = CellTxGain.DoubleValue;      
            CellRxGain.DoubleValue2 = CellRxGain.DoubleValue;      
            CellRxPolarization.Value2 = CellRxPolarization.Value;      
            CellRxFrequency.Value2 = CellRxFrequency.Value;      
            CellTxPolarization.Value2 = CellTxPolarization.Value;
            CellTxFrequency.Value2 = CellTxFrequency.Value;      
                          
            CellLongitude.CanEdit = true;
            CellLatitude.CanEdit = true;
            CellAddress.CanEdit = true;
            CellAzimuth.CanEdit = true;
         
            CellEquipmentName.CanEdit = true;
            CellRxFrequency.CanEdit = true;
            CellTxFrequency.CanEdit = true;
            CellTxBandwidth.CanEdit = true;
            CellRxBandwidth.CanEdit = true;
            CellTxDesignEmission.CanEdit = true;
            CellRxDesignEmission.CanEdit = true;
            CellCertificateNumber.CanEdit = true;
            CellCertificateDate.CanEdit = true;

            CellAntennaType.CanEdit = true;         

            CellAGL.CanEdit = true;
            CellDiameter.CanEdit = true;

            //Current URZP values should be leaded just sfter after storing original values. 
            ReadDifferences(objectID);

            //!!! ACHTUNG!
            CellRxFrequency.Value = HelpFunction.ToString(st.nomFreqRx);
            CellTxFrequency.Value = HelpFunction.ToString(st.nomFreqTx);
            */
            CellNetworkName.CanEdit = false;
            CellOrbitalPosition.CanEdit = false;
            CellASL.CanEdit = false;
            CellElevation.CanEdit = false;
            CellNoiseTemperature.CanEdit = false;
            CellTxPolarization.CanEdit = false;
            CellRxPolarization.CanEdit = false;
            CellTxModulation.CanEdit = false;
            CellRxModulation.CanEdit = false;            
        }
        /*
        ///===================================================
        /// Bind grid
        ///===================================================
        private void BindGrid(Grid gridParam)
        {                  
           return;
        }
        */
        /// <summary>
        /// On press button Longitude handler 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            double longitudeValue = CellLongitude.DoubleValue;
            double latitudeValue = CellLatitude.DoubleValue;

            // Выбераем запись из таблицы           
            NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(longitudeValue, latitudeValue, "4DMS", "4DMS");
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionMw, 1, tmpXY.Longitude,
                                                                    tmpXY.Latitude);

            if (objPosition != null)
            {
                diffStation.Position.LoadStatePosition(objPosition);

                CellLongitude.DoubleValue = diffStation.Position.LonDms;
                CellLatitude.DoubleValue = diffStation.Position.LatDms;
                CellAddress.Value = diffStation.Position.FullAddress;
                CellASL.DoubleValue = diffStation.Position.ASl;

                FormNewPosition frm = new FormNewPosition(diffStation.Position.CityId);
                frm.SetLongitudeAsDms(diffStation.Position.LonDms);
                frm.SetLatitudeAsDms(diffStation.Position.LatDms);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = diffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
                }

                objPosition.Dispose();
            }
        }

        public void OnPressButtonAddressName(Cell cell)
        {
            double longitudeValue = CellLongitude.DoubleValue;
            double latitudeValue = CellLatitude.DoubleValue;

            FormNewPosition frm = new FormNewPosition(diffStation.Position.CityId);// IM.NullI);
            frm.SetLatitudeAsDms(latitudeValue);
            frm.SetLongitudeAsDms(longitudeValue);
            frm.CanEditPosition = true;
            frm.CanAttachPhoto = true;
            frm.CanEditStreet = true;
            frm.OldPosition = diffStation.Position;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                frm.GetFotos(out listFotosDst, out listFotosSrc);
                diffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, longitudeValue, latitudeValue);                
            }

            CellLongitude.DoubleValue = diffStation.Position.LonDms;
            CellLatitude.DoubleValue = diffStation.Position.LatDms;
            CellAddress.Value = diffStation.Position.FullAddress;
            CellASL.DoubleValue = diffStation.Position.ASl;
        }

        /*
        /// <summary>
        ///  Get City ID. Used for forming position.
        /// </summary>
        /// <returns>City</returns>
        private int GetCityID()
        {
           int cityID = 0;
           if (Position.Id != IM.NullI)
           {
              IMObject objPosition = IMObject.LoadFromDB(Position);
              cityID = objPosition.GetI("CITY_ID");
              objPosition.Dispose();
           }
           return cityID;
        }
        /// <summary>
        /// On Address pres button handler
        ///   Creates new site
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
           int cityId = GetCityID();

           FormNewPosition frm = new FormNewPosition(cityId);
           if (frm.ShowDialog() == DialogResult.OK)
           {
              NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");
              IMObject objPosition = frm.getNewPosition(ICSMTbl.itblPositionEs, tmpXY.Longitude, tmpXY.Latitude);
                                    
              if (objPosition != null)
              {
                 // I'm not sure in "ICSMTbl.itblPositionEs"
                 Position = new RecordPtr(ICSMTbl.itblPositionEs, objPosition.GetI("ID"));
                 objPosition.Dispose();
              }
           }
        }
        /// <summary>
        ///  Selects settelite network.
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonSatNet(Cell cell)
        {
           // Выбераем запись из таблицы
           FSelectSatellite frm = new FSelectSatellite(HelpFunction.ReplaceQuotaSumbols(cell.Value));
           if (frm.ShowDialog() == DialogResult.OK)
           {
              //RecordPtr satellite = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the satellite"), ICSMTbl.itblSatellites, "");
              RecordPtr satellite = new RecordPtr(ICSMTbl.itblSatellites, frm.GetID());
              // Заносим новые данные в ячейки
              string satelName = "";
              IMRecordset r = new IMRecordset(ICSMTbl.itblSatellites, IMRecordset.Mode.ReadOnly);
              r.Select("ID,SAT_NAME,LONG_NOM");
              r.SetWhere("ID", IMRecordset.Operation.Eq, satellite.Id);
              r.Open();
              if (!r.IsEOF())
              {
                 satelName = r.GetS("SAT_NAME");
              }
              r.Close();
              r.Destroy();

              CellNetworkName.Value = satelName;

              // Вытаскиваем поле LONG_NOM
              double long_nom = IM.NullD;
              IMRecordset r2 = new IMRecordset(ICSMTbl.itblSatellites, IMRecordset.Mode.ReadOnly);
              r2.Select("ID,SAT_NAME,LONG_NOM");
              r2.SetWhere("SAT_NAME", IMRecordset.Operation.Like, satelName);
              r2.Open();
              if (!r2.IsEOF())
              {
                 long_nom = r2.GetD("LONG_NOM");
              }
              r2.Close();
              r2.Destroy();
              //Cell cellLongNom = grid.GetCellFromKey("KOrbit");
              if (long_nom != IM.NullD)
                 CellOrbitalPosition.Value = long_nom.ToString();
              else
                 CellOrbitalPosition.Value = "";

              double orbita = long_nom;
              /*OnCellValidate(cell, grid);
              double lon = objStation.GetD("LONGITUDE");
              double lat = objStation.GetD("LATITUDE");
              Cell fillCell = grid.GetCellFromKey("KOrbit");
              double orbita = ConvertType.ToDouble(fillCell, IM.NullD);

              if (lon != IM.NullD && lat != IM.NullD && lon != 0 && lat != 0)
              {
                 if (orbita != IM.NullD)
                 {
                    double azim = IMCalculate.CalcAzimSat(lon, lat, orbita);
                    double curAzim = objStation.GetD("AZM_TO");
                    if (azim != IM.NullD && curAzim != azim)
                    {
                       fillCell = grid.GetCellFromKey("KAzim");
                       fillCell.Value = IM.RoundDeci(azim, 1).ToString();
                       OnCellValidate(fillCell, grid);
                    }
                 }
                 fillCell = grid.GetCellFromKey("KAgl");
                 double agl = ConvertType.ToDouble(fillCell, IM.NullD);
                 fillCell = grid.GetCellFromKey("KAsl");
                 double asl = ConvertType.ToDouble(fillCell, IM.NullD);
                 if (agl != IM.NullD && asl != IM.NullD && orbita != IM.NullD)
                 {
                    double elev = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1);
                    double curElev = objStation.GetD("ELEV_MIN");
                    if (curElev != elev)
                    {
                       fillCell = grid.GetCellFromKey("KElev");
                       fillCell.Value = IM.RoundDeci(IMCalculate.CalcElevSatGeo(lon, lat, orbita, asl + agl), 1).ToString();
                       OnCellValidate(fillCell, grid);
                    }
                 }
              }
           }
           frm.Dispose();
        }
        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
           string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
           string param = "{NAME=\"*" + Criteria + "*\"}";
         
           RecordPtr RecEquip = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipEsta, param);
           return RecEquip;
        }
        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
           string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
           string param = "{NAME=\"*" + Criteria + "*\"}";

           RecordPtr RecAnt = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntenna, param);
           return RecAnt;
        }      
        /// <summary>
        /// On press button equipment
        ///   Set new Equipment if last one is selected
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonEquipmentName(Cell cell)
        {
           RecordPtr newEquip = LookupEquipment(cell.Value, true);
           if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
              Equipment = newEquip;
        }
        /// <summary>
        /// On press button antenna
        ///   Set new Antenna if last one is selected
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAntennaName(Cell cell)
        {
           RecordPtr newAnt = LookupAntenna(cell.Value, true);
           if ((newAnt.Id > 0) && (newAnt.Id < IM.NullI))
              Antenna = newAnt;
        }

        /// <summary>
        /// It was license selection. 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonNomTx(Cell cell)
        {         
        }
        /// <summary>
        /// Longitude before change handler.
        ///        this needs (oldStrValue != NewValue) check
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeLongitude(Cell cell, ref string NewValue)
        {
           string oldStrValue = HelpFunction.DmsToString(CellLongitude.DoubleValue, EnumCoordLine.Lon);
           if (oldStrValue != NewValue)
           {
              CellLongitude.DoubleValue = ConvertType.StrToDMS(NewValue);
              NewValue = HelpFunction.DmsToString(CellLongitude.DoubleValue, EnumCoordLine.Lon);
           }
        }
        /// <summary>
        /// Latitude before change handler.
        ///       this needs (oldStrValue != NewValue) check
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeLatitude(Cell cell, ref string NewValue)
        {         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeTxFrequencies(Cell cell, ref string NewValue)
        {
           st.nomFreqTx = ConvertType.ToDoubleList(NewValue);
           NewValue = HelpFunction.ToString(st.nomFreqTx);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeRxFrequencies(Cell cell, ref string NewValue)
        {
           st.nomFreqRx = ConvertType.ToDoubleList(NewValue);
           NewValue = HelpFunction.ToString(st.nomFreqRx);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeEquipmentName(Cell cell, ref string NewValue)
        {         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeAntennaType(Cell cell, ref string NewValue)
        {
           //NewValue = cell.Value;        
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeAzimuth(Cell cell, ref string NewValue)
        {
           double doubleRes = IM.NullD;
           if (Double.TryParse(NewValue, out doubleRes))
           {
              //if (Azimuth != doubleRes)
              if (doubleRes >= 0 && doubleRes < 360)
                 CellAzimuth.DoubleValue = doubleRes;
              else
              {
                 MessageBox.Show("Азимут повинен бути в межах від 0 до 360","Помилка");
                 NewValue = cell.Value;
              }
           } else {
              CellAzimuth.DoubleValue = IM.NullD;
              NewValue = "";
           }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangePower(Cell cell, ref string NewValue)
        {
           double doubleRes = IM.NullD;
           if (Double.TryParse(NewValue, out doubleRes))
           {            
              if (doubleRes >= 0)
                 CellMaxPower.DoubleValue = doubleRes;
              else
              {
                 MessageBox.Show("Потужність повинен бути від 0", "Помилка");
                 //Revert to old value
                 NewValue = cell.Value;
              }
           }
           else
           {
              //Set to empty
              CellMaxPower.DoubleValue = IM.NullD;
              NewValue = "";
           }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeTxBand(Cell cell, ref string NewValue)
        {         
           double doubleRes = IM.NullD;
           if (Double.TryParse(NewValue, out doubleRes))
           {
              if (doubleRes >= 0)
                 TxBandwidth = doubleRes;
              else
              {
                 MessageBox.Show("Смуга пропускання повинен бути від 0", "Помилка");
                 //Revert to old value
                 NewValue = cell.Value;
              }
           }
           else
           {
              //Set to empty
              TxBandwidth = IM.NullD;
              NewValue = "";
           }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeTxEmi(Cell cell, ref string NewValue)
        {         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeRxBand(Cell cell, ref string NewValue)
        {         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeRxEmi(Cell cell, ref string NewValue)
        {       
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeCertNo(Cell cell, ref string NewValue)
        {
           CertificateNo = NewValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="NewValue"></param>
        public void OnBeforeChangeCertDa(Cell cell, ref string NewValue)
        {
           DateTime CertDateTime = IM.NullT;
           if (!DateTime.TryParse(NewValue, out CertDateTime))
           {
              NewValue = "";
              CertificateDate = IM.NullT;            
           }
           else
           {
              CertificateDate = CertDateTime;
           }
        }

        public void OnBeforeChangeAgl(Cell cell, ref string NewValue)
        {
           double doubleRes = IM.NullD;
           if (Double.TryParse(NewValue, out doubleRes))
           {
              if (doubleRes < 0)               
              {
                 MessageBox.Show("Висота над землею повинен бути від 0", "Помилка");
                 //Revert to old value
                 NewValue = cell.Value;
              }
           }
           else
           {
              //Set to empty            
              NewValue = "";
           }
        }
      
        public void OnBeforeChangeDiam(Cell cell, ref string NewValue)
        {
           double doubleRes = IM.NullD;
           if (Double.TryParse(NewValue, out doubleRes))
           {
              if (doubleRes < 0)               
              {
                 MessageBox.Show("Діаметер повинен бути від 0", "Помилка");
                 //Revert to old value
                 NewValue = cell.Value;
              }
           }
           else
           {
              //Set to empty            
              NewValue = "";
           }
        }
        public void OnBeforeChangePolarRx(Cell cell, ref string NewValue)
        {
           RxPolarization = NewValue;
        }            

        public void OnBeforeChangePolarTx(Cell cell, ref string NewValue)
        {
           TxPolarization = NewValue;
        }

        */
        public void OnBeforeChangeNomTx(Cell cell, ref string NewValue)
        {
            station.TxFrequencies = ConvertType.ToDoubleList(NewValue);
            station.TxFrequencies.Sort();
            NewValue = HelpFunction.ToString(station.TxFrequencies);
        }

        public void OnBeforeChangeNomRx(Cell cell, ref string NewValue)
        {
            station.RxFrequencies = ConvertType.ToDoubleList(NewValue);
            station.RxFrequencies.Sort();
            NewValue = HelpFunction.ToString(station.RxFrequencies);
        }
        /*
        /// <summary>
        /// Just stub
        /// </summary>
        /// <param name="cell"></param>
        public void OnAfterChangeLongitude(Cell cell)
        {

        }
        /// <summary>
        /// Just stub
        /// </summary>
        /// <param name="cell"></param>
        public void OnAfterChangeLatitude(Cell cell)
        {

        }   
        */
        /// <summary>
        /// Jyst stub
        /// </summary>
        public override void UpdateToNewPacket()
        {
        }

        /// <summary>
        /// Проверить, не получилось ли так, что пользователь "немножечко"
        /// подправил координаты. Актуальна только для POSITION_ES, потому использование
        /// IMObject оправдано
        /// </summary>
        /// <returns>Очевидно...</returns>
        private bool AreChangedCoordinates(RecordPtr position)
        {
            if (position.Table == ICSMTbl.itblPositionEs)
            {
                IMObject objPosition = IMObject.LoadFromDB(position);

                double longitude =
                   (NSPosition.Position.convertPosition(objPosition.GetD("X"), 0, objPosition.GetS("CSYS"), "DMS").Longitude);
                double latitude =
                   (NSPosition.Position.convertPosition(0, objPosition.GetD("Y"), objPosition.GetS("CSYS"), "DMS").Latitude);
                objPosition.Dispose();

                if (longitude != CellLongitude.DoubleValue || latitude != CellLatitude.DoubleValue)
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Сохраняет позицию.
        /// 
        /// Для POSITION_ES - копирует в XNRFA_POSITION, _только_ ecли оператор "поправил" координаты.
        /// для XNRFA_POSITION - только пытается обновить долготу/широту/адресс
        /// для других позиций - копирует в XNRFA_POSITION, но также "переписывает" координаты
        ///      если пользователь их подправил.
        /// </summary>
        /*private void SavePosition()
        {
           int newPosId = IM.NullI;
           switch (Position.Table)
           {
              case ICSMTbl.itblPositionEs:
                 if (AreChangedCoordinates(Position))
                    newPosId = CopyPositionToXNRFA(Position.Table, Position.Id, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                 break;
              case PlugTbl.itblXnrfaPositions:
                 {
                    IMRecordset xnrfaPosition = new IMRecordset(PlugTbl.itblXnrfaPositions, IMRecordset.Mode.ReadWrite);
                    xnrfaPosition.Select("ID,LONGITUDE,LATITUDE,REMARK");
                    xnrfaPosition.SetWhere("ID", IMRecordset.Operation.Eq, Position.Id);
                    xnrfaPosition.Open();
                    if (!xnrfaPosition.IsEOF())
                    {
                       xnrfaPosition.Edit();
                       xnrfaPosition.Put("LONGITUDE", CellLongitude.DoubleValue);
                       xnrfaPosition.Put("LATITUDE", CellLatitude.DoubleValue);
                       xnrfaPosition.Put("REMARK", CellAddress.Value);
                       xnrfaPosition.Close();
                       xnrfaPosition.Destroy();
                    }
                 }
                 break;
              default:
                 if (AreChangedCoordinates(Position))
                    newPosId = CopyPositionToXNRFA(Position.Table, Position.Id, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                 else
                    newPosId = CopyPositionToXNRFA(Position.Table, Position.Id, IM.NullD, IM.NullD);
                 Position = new RecordPtr(PlugTbl.itblXnrfaPositions, newPosId);
                 break;
           }
        }*/


        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "NewKey-KAddr")
            {
                AdminSiteAllTech.Show(station.Position.TableName, station.Position.Id);
            }
        }

        /// <summary>
        /// Save differences
        /// </summary>
        /// <returns>true if successfull</returns>
        protected override bool SaveAppl()
        {
            if (!gridParam.IsValid)
            {
                MessageBox.Show("Невірно введені дані", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, diffStation.Equipment.Certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            diffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  diffStation.Position,
                  ICSMTbl.itblPositionEs);
            
            if (listFotosDst != null && listFotosSrc != null)
                diffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, EarthDiffStation.TableName, diffStation.Id);

            diffStation.Equipment.Id = station.Equipment.Id;
            diffStation.Equipment.Name = CellEquipmentName.Value;
            diffStation.Power[PowerUnits.dBW] = CellMaxPower.DoubleValue;
            diffStation.Equipment.RxBandwidth = CellRxBandwidth.DoubleValue;
            diffStation.Equipment.TxBandwidth = CellTxBandwidth.DoubleValue;
            diffStation.Equipment.RxDesigEmission = CellRxDesignEmission.Value;
            diffStation.Equipment.TxDesigEmission = CellTxDesignEmission.Value;
            diffStation.Equipment.RxModulation = CellRxModulation.Value;
            diffStation.Equipment.TxModulation = CellTxModulation.Value;
            //diffStation.Equipment.Certificate.Symbol = CellCertificateNumber.Value;
            //diffStation.Equipment.Certificate.Date = CellCertificateDate.DateValue;

            diffStation.Azimuth = CellAzimuth.DoubleValue;
            //diffStation.Eq Power[PowerUnits.dBW] = CellMaxPower1.DoubleValue;
            diffStation.AGL = CellAGL.DoubleValue;

            diffStation.Antenna.Id = station.Antenna.Id;
            diffStation.Antenna.Name = CellAntennaType.Value;
            diffStation.Antenna.Diameter = CellDiameter.DoubleValue;
            diffStation.Antenna.RxGain = CellRxGain.DoubleValue;
            diffStation.Antenna.TxGain = CellTxGain.DoubleValue;
            diffStation.RxPolarization = CellRxPolarization.Value;
            diffStation.TxPolarization = CellTxPolarization.Value;
            diffStation.RxFrequencies = station.RxFrequencies;
            diffStation.TxFrequencies = station.TxFrequencies;
            diffStation.StatComment = Comment;
            appType = AppType.AppZS;
            diffStation.Save();

            return true;
        }

        /// <summary>
        /// Legacy
        /// </summary>
        /// <param name="docType"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        protected override string getOutDocName(string docType, string number)
        {
            return "";
        }

        /// <summary>
        /// Заглушка    
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="grid"></param>
        public override void OnPressButton(Cell cell, Grid grid)
        {

        }
    }
}
