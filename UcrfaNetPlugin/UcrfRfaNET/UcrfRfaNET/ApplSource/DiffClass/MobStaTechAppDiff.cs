﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStaTechAppDiff : BaseDiffClass
    {
        public int Id { get; set; }

        private MobStaTechAppStation techStation;
        private MobStaTechAppDiffStation techDiffStation;


        #region CELL

        /// <summary>
        /// Радіотехнологія(Стандарт)
        /// </summary>
        private CellStringProperty CellRadioTech = new CellStringProperty();

        /// <summary>
        /// Означення станції
        /// </summary>
        private CellStringProperty CellDefStat = new CellStringProperty();
        /// <summary>
        /// Клас станції
        /// </summary>
        private CellStringComboboxAplyProperty CellClassStat = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleProperty CellPowerTr = new CellDoubleProperty();

        /// <summary>
        /// Клас випромінювання               
        /// </summary>        
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Тип антени
        /// </summary>        
        private CellStringProperty CellAntennaType = new CellStringProperty();

        /// <summary>
        /// Коефіцієнт підсилення антени, дБі
        /// </summary>        
        private CellDoubleBoundedProperty CellMaxKoef = new CellDoubleBoundedProperty();
        /// <summary>
        /// Ширина ДС 
        /// </summary>        
        private CellDoubleBoundedProperty CellWidthAntenna = new CellDoubleBoundedProperty();

        /// <summary>
        /// Висота антени над рівнем землі, м
        /// </summary>                
        private CellDoubleProperty CellHighAntenna = new CellDoubleProperty();

        /// <summary>
        /// Азимут випромінювання антени, град 
        /// </summary>
        private CellDoubleBoundedNullableProperty CellAglMaxEmission = new CellDoubleBoundedNullableProperty();

        /// <summary>
        /// Затухання у фідерному тракті, дБ
        /// </summary>
        private CellDoubleProperty CellFider = new CellDoubleProperty();

        /// <summary>
        /// Позивний
        /// </summary>
        private CellStringProperty CellCall = new CellStringProperty();

        
        /// <summary>
        /// Дуплексний рознос, МГц
        /// </summary>
        private CellDoubleProperty CellDuplex = new CellDoubleProperty();

        /// <summary>
        /// Частоти передавання, МГц
        /// </summary>
        private CellStringProperty CellNominSend = new CellStringProperty();

        /// <summary>
        /// Частоти приймання, МГц
        /// </summary>
        private CellStringProperty CellNominAccept = new CellStringProperty();
     

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public MobStaTechAppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobStaTechnologicalApp.TableName, ownerId, packetID, radioTech)
        {
            //Стандартные значения
            techStation = new MobStaTechAppStation();
            techStation.Id = recordID.Id;
            techStation.Load();

            CellAglMaxEmission.NullString = "ND";
            CellAglMaxEmission.Substitution.Add(0, "ND");
            CellAglMaxEmission.EnableSubstitution = true;

            ReadAllDefaultDataPositions();
            //URCP значения
            techDiffStation = new MobStaTechAppDiffStation();
            techDiffStation.Id = recordID.Id;
            techDiffStation.Load();

            if (techDiffStation.Position == null)
                techDiffStation.Position = techStation.Position;
            SCVisnovok = techStation.Finding;                        

            appType = AppType.AppTR;
            CellCertificateDate.NullString = "-";
            
        }

        /// <summary>
        /// Зчитування всіх стандартних параметрів
        /// </summary>
        private void ReadAllDefaultDataPositions()
        {
            CellMaxKoef.LowerBound = 0.0;
            CellWidthAntenna.LowerBound = 0.0;
            CellWidthAntenna.UpperBound = 360.0;
            CellAglMaxEmission.LowerBound = 0.0;
            CellAglMaxEmission.UpperBound = 360.0;
            CellLongitude.DoubleValue2 = techStation.Position.LonDms;
            CellLatitude.DoubleValue2 = techStation.Position.LatDms;
            CellASL.Value2 = FillValue2(techStation.Position.ASl.ToStringNullD());
            CellAddress.Value2 = FillValue2(techStation.Position.FullAddress);
                      
            CellCertificateNumber.Value = techStation.techStationEquip.Certificate.Symbol;
            CellCertificateNumber.Value2 = FillValue2(techStation.techStationEquip.Certificate.Symbol);            
            
            CellCertificateDate.DateValue = techStation.techStationEquip.Certificate.Date;            
            CellCertificateDate.DateValue2 = techStation.techStationEquip.Certificate.Date;
            CellCertificateDate.SetRedValue();            

            CellLongitude.DoubleValue = techStation.Position.LonDms;
            CellLatitude.DoubleValue = techStation.Position.LatDms;
            CellASL.DoubleValue = techStation.Position.ASl;
            CellAddress.Value = techStation.Position.FullAddress;
            CellRadioTech.Value2 = FillValue2(techStation.Standart);
            CellDefStat.Value2 = FillValue2(techStation.Defin);
            if (!techStation.ClassStationDict.ContainsKey(techStation.ClassStat))
                techStation.ClassStat = techStation.ClassStationDict.ToList()[0].Key;
            CellClassStat.Value2 = FillValue2(techStation.ClassStat + " - " + techStation.ClassStationDict[techStation.ClassStat]);

            if (!string.IsNullOrEmpty(techStation.ClassStat))
                CellClassStat.Value = techStation.ClassStat + " - " + techStation.ClassStationDict[techStation.ClassStat];
            foreach (KeyValuePair<string, string> dict in techStation.ClassStationDict)
                CellClassStat.Items.Add(dict.Key + " - " + dict.Value);

            if (string.IsNullOrEmpty(techStation.NameRez))
                techStation.NameRez = "*";
            CellEquipmentName.Value2 = FillValue2(techStation.techStationEquip.Name);
            CellPowerTr.Value2 = FillValue2(techStation.techStationEquip.MaxPower[PowerUnits.W].Round(3).ToStringNullD());
            CellTxDesignEmission.Value2 = FillValue2(techStation.DesigEmiss);            

            CellAntennaType.Value2 = FillValue2(techStation.techStationAntena.Name);
            CellMaxKoef.Value2 = FillValue2(techStation.techStationAntena.Gain.ToStringNullD());
            CellWidthAntenna.Value2 = FillValue2(techStation.techStationAntena.AntennaWidth.ToStringNullD());
            CellHighAntenna.Value2 = FillValue2(techStation.Agl.ToStringNullD());
            CellAglMaxEmission.DoubleValue2 = techStation.Azimuth;
            CellFider.Value2 = FillValue2(techStation.Fider.ToStringNullD());
            CellCall.Value2 = FillValue2(techStation.Call);
            CellDuplex.Value2 = FillValue2(techStation.Duplex.ToStringNullD());
            CellNominSend.Value2 = FillValue2(techStation.NSend);
            CellNominAccept.Value2 = FillValue2(techStation.NAccept);
            CellObjRCHP.Value2 = FillValue2(techStation.Id.ToString());
           
     

            CellRadioTech.Value = techStation.Standart;
            CellDefStat.Value = techStation.Defin;
            if (!string.IsNullOrEmpty(techStation.techStationEquip.Name))
                CellEquipmentName.Value = techStation.techStationEquip.Name;
            CellPowerTr.Value = techStation.techStationEquip.MaxPower[PowerUnits.W].Round(3).ToStringNullD();
            CellTxDesignEmission.Value = techStation.DesigEmiss;
            
            if (!string.IsNullOrEmpty(techStation.techStationAntena.Name))
                CellAntennaType.Value = techStation.techStationAntena.Name;
            CellMaxKoef.DoubleValue = techStation.techStationAntena.Gain;
            CellWidthAntenna.DoubleValue = techStation.techStationAntena.AntennaWidth;
            CellHighAntenna.DoubleValue = techStation.Agl;
            CellAglMaxEmission.DoubleValue = techStation.Azimuth;
            CellFider.DoubleValue = techStation.Fider;
            CellCall.Value = techStation.Call;
            CellDuplex.DoubleValue = techStation.Duplex;
            CellNominSend.Value = techStation.NSend;
            CellNominAccept.Value = techStation.NAccept;
            CellObjRCHP.IntValue = techStation.Id;
        }

        /// <summary>        
        /// Формування адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXY = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");
            // Выбираем запись из таблицы                                 
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionWim, 1, tmpXY.Longitude, tmpXY.Latitude);
            if (objPosition != null)
            {
                techDiffStation.Position = new PositionState();
                techDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                CellLongitude.DoubleValue = techDiffStation.Position.LongDms;
                CellLatitude.DoubleValue = techDiffStation.Position.LatDms;
                CellASL.DoubleValue = techDiffStation.Position.ASl;
                if (!string.IsNullOrEmpty(techDiffStation.Position.FullAddress))
                    CellAddress.Value = techDiffStation.Position.FullAddress;

                // Форма Адреса місця встановлення РЕЗ
                FormNewPosition frm;
                if (techDiffStation.Position != null)
                {
                    frm = !string.IsNullOrEmpty(techDiffStation.Position.CityId.ToString())
                              ? new FormNewPosition(techDiffStation.Position.CityId)
                              : new FormNewPosition(techStation.Position.CityId);
                }
                else
                    frm = new FormNewPosition(techStation.Position.CityId);

                frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
                frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = techDiffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
            }
        }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            //DateTime newVal;
            //if (!DateTime.TryParse(val, out newVal) || newVal < DateTime.MinValue)
            //    val = cell.Value;
        }


        public void OnBeforeChangeFreq(Cell cell, ref string val)
        {
            ChangeColor(cell, Colors.okvalue);
            List<double> freqList = ConvertType.ToDoubleList(val);
            freqList.Sort();

            if (freqList.Count > 0)
            {
                if (CheckValidityFrequency(freqList))
                    val = HelpFunction.ToString(freqList);
                else
                {
                    ChangeColor(cell, Colors.badValue);
                    val = HelpFunction.ToString(freqList);
                }
            }
            else
                val = cell.Value;
        }

        private bool CheckValidityFrequency(List<double> freqList)
        {
            foreach (double freq in freqList)
                if (freq <= 0.0)
                    return false;
            return true;
        }

        /// <summary>        
        /// Створення нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(techDiffStation.Position.CityId);
            //FormNewPosition frm = new FormNewPosition(IM.NullI);

            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanAttachPhoto = true;
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;
            frm.OldPosition = techDiffStation.Position;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                techDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                frm.GetFotos(out listFotosDst, out listFotosSrc);
                if (!string.IsNullOrEmpty(techDiffStation.Position.FullAddress))
                    CellAddress.Value = techDiffStation.Position.FullAddress;
                CellASL.DoubleValue = techDiffStation.Position.ASl;
            }
        }


        /// <summary>
        /// Ініціалізація всього Гріду
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            InitStationObject();
        }

        private void InitStationObject()
        {            
            if (techDiffStation.IsEnableDiff)
            {
                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, techDiffStation.techDiffStationEquip.Certificate);
                CellRadioTech.Value = techDiffStation.Standart;
                CellDefStat.Value = techDiffStation.Defin;
                if (techStation.ClassStationDict.ContainsKey(techDiffStation.ClassStat))
                    CellClassStat.Value = techDiffStation.ClassStat + " - " + techStation.ClassStationDict[techDiffStation.ClassStat];
                CellASL.DoubleValue = techDiffStation.Asl;
                CellAddress.Value = techDiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = techDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = techDiffStation.Position.LatDms;
                CellASL.DoubleValue = techDiffStation.Position.ASl;
                CellPowerTr.Value = techDiffStation.Power[PowerUnits.W].Round(3).ToStringNullD();
                CellTxDesignEmission.Value = techDiffStation.DesigEmiss;

                CellHighAntenna.DoubleValue = techDiffStation.HeightAnt;
                CellAntennaType.Value = techDiffStation.TypeAnt;
                CellEquipmentName.Value = techDiffStation.NameRez;
                CellWidthAntenna.DoubleValue = techDiffStation.WidthAnt;
                CellMaxKoef.DoubleValue = techDiffStation.Koef;

                CellAglMaxEmission.DoubleValue = techDiffStation.Azimuth;
                //CellFider.DoubleValue = techDiffStation.Fider;
                CellCall.Value = techDiffStation.Call;

                CellNominAccept.Value = techDiffStation.NomReceive;
                CellNominSend.Value = techDiffStation.NomSend;
                Comment = techDiffStation.StatComment;
                
            }
            else
            {
                techDiffStation.Position = techStation.Position.Clone() as PositionState;
            }
            _stationObject = techStation;            
        }


        //////////////////////////////////////////
        //Необхідні перевантажені методи

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                AdminSiteAllTech.Show(techStation.Position.TableName, techStation.Position.Id);
            }
        }

        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            techDiffStation.Id = recordID.Id;

            /*if (CellLongitude.DoubleValue != CellLongitude.DoubleValue2 ||
       CellLatitude.DoubleValue != CellLatitude.DoubleValue2 ||
       techDiffStation.Position.TableName != ICSMTbl.itblPositionWim)
            {
                techDiffStation.Position.LonDms = CellLongitude.DoubleValue;
                techDiffStation.Position.LatDms = CellLatitude.DoubleValue;
                // Сброс id в IM.NullI заставит создать новую запись                
                techDiffStation.Position.Id = IM.NullI;
                techDiffStation.Position.TableName = PlugTbl.itblXnrfaPositions;
            }
            if (techDiffStation.Position.TableName == PlugTbl.itblXnrfaPositions)
                techDiffStation.Position.SaveToBD();*/

            techDiffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  techDiffStation.Position,
                  ICSMTbl.itblPositionWim);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            techDiffStation.Standart = CellRadioTech.Value;
            techDiffStation.Defin = CellDefStat.Value;
            techDiffStation.ClassStat = CellClassStat.Value.Split(' ')[0].Trim();
            //techDiffStation.EquipmentName = CellEquipmentName.Value;         
            techDiffStation.Power[PowerUnits.W] = CellPowerTr.DoubleValue;
            techDiffStation.DesigEmiss = CellTxDesignEmission.Value;
            techDiffStation.Asl = CellASL.DoubleValue;
            //Сертифікат (и)
            techDiffStation.techDiffStationEquip.Certificate.Symbol = certificate.Symbol;
            techDiffStation.techDiffStationEquip.Certificate.Date = certificate.Date;

            techDiffStation.HeightAnt = CellHighAntenna.DoubleValue;
            techDiffStation.TypeAnt = CellAntennaType.Value;
            techDiffStation.NameRez = CellEquipmentName.Value;
            techDiffStation.WidthAnt = CellWidthAntenna.DoubleValue;
            techDiffStation.Koef = CellMaxKoef.DoubleValue;

            techDiffStation.Azimuth = CellAglMaxEmission.DoubleValue;
            //techDiffStation.Fider = CellFider.DoubleValue;
            techDiffStation.Call = CellCall.Value;
            techDiffStation.NomReceive = CellNominAccept.Value;
            techDiffStation.NomSend = CellNominSend.Value;
            techDiffStation.StatComment = Comment;
            techDiffStation.Save();
            if (listFotosDst != null && listFotosSrc != null)
                techDiffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, MobStaTechAppDiffStation.TableName, techStation.Id);
            return true;
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStaTechAppDiff;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }
    }
}
