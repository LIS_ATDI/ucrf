﻿using System;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStaTechAppCommit : BaseCommitClass
    {
        public int Id { get; set; }

        private PositionState selectedPosition;
        private CoordinateCommitHelper coordHelper;

        private MobStaTechAppStation techStation;
        private MobStaTechAppDiffStation techDiffStation;

        #region CELL

        /// <summary>
        /// Радіотехнологія(Стандарт)
        /// </summary>
        private CellStringComboboxAplyProperty CellRadioTech = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Означення станції
        /// </summary>
        private CellStringComboboxAplyProperty CellDefStat = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Клас станції
        /// </summary>
        private CellStringComboboxAplyProperty CellClassStat = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleLookupProperty CellPowerTr = new CellDoubleLookupProperty();

        /// <summary>
        /// Клас випромінювання               
        /// </summary>        
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        /// <summary>
        /// Тип антени
        /// </summary>        
        private CellStringProperty CellAntennaType = new CellStringProperty();

        /// <summary>
        /// Коефіцієнт підсилення антени, дБі
        /// </summary>        
        private CellStringBrowsableProperty CellMaxKoef = new CellStringBrowsableProperty();
        /// <summary>
        /// Ширина ДС 
        /// </summary>        
        private CellStringBrowsableProperty CellWidthAntenna = new CellStringBrowsableProperty();

        /// <summary>
        /// Висота антени над рівнем землі, м
        /// </summary>                
        private CellStringComboboxAplyProperty CellHighAntenna = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Азимут випромінювання антени, град 
        /// </summary>
        private CellStringComboboxAplyProperty CellAglMaxEmission = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Затухання у фідерному тракті, дБ
        /// </summary>
        private CellStringProperty CellFider = new CellStringProperty();

        /// <summary>
        /// Позивний
        /// </summary>
        private CellStringComboboxAplyProperty CellCall = new CellStringComboboxAplyProperty();


        /// <summary>
        /// Ідентифікатор МПС
        /// </summary>
        private CellStringProperty CellMMSI = new CellStringProperty();


        /// <summary>
        /// Дуплексний рознос, МГц
        /// </summary>
        private CellDoubleProperty CellDuplex = new CellDoubleProperty();

        /// <summary>
        /// Частоти передавання, МГц
        /// </summary>
        private CellStringComboboxAplyProperty CellNominSend = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Частоти приймання, МГц
        /// </summary>
        private CellStringComboboxAplyProperty CellNominAccept = new CellStringComboboxAplyProperty();


        /// <summary>
        /// Означення РЕЗ
        /// </summary>
        private CellStringProperty CellOznach_REZ = new CellStringProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public MobStaTechAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobStaTechnologicalApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTR;
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);

            //Стандартные значения
            techStation = new MobStaTechAppStation();
            techStation.Id = recordID.Id;
            techStation.Load();

            //URCP значения
            techDiffStation = new MobStaTechAppDiffStation();
            techDiffStation.Id = recordID.Id;
            techDiffStation.Load();
        }

        /// <summary>
        /// Зчитування всіх стандартних параметрів
        /// </summary>
        private void ReadAllDefaultDataPositions()
        {
            CellRadioTech.Value = techStation.Standart;
            CellDefStat.Value = techStation.Defin;
            if (techStation.ClassStationDict.ContainsKey(techStation.ClassStat))
            {
                CellClassStat.Value = techStation.ClassStat + " - " + techStation.ClassStationDict[techStation.ClassStat];
                CellClassStat.Items.Add(techStation.ClassStat + " - " + techStation.ClassStationDict[techStation.ClassStat]);
            }
            CellEquipmentName.Value = techStation.techStationEquip.Name;
            CellPowerTr.Value = techStation.techStationEquip.MaxPower[PowerUnits.W].Round(3).ToString();
            CellTxDesignEmission.Value = techStation.DesigEmiss;

            CellCertificateNumber.Value = techStation.CertNumb;
            CellCertificateDate.Value = techStation.CertDate.ToShortDateStringNullT();

            CellCertificateNumber.Value = techStation.techStationEquip.Certificate.Symbol;
            CellCertificateDate.Value = techStation.techStationEquip.Certificate.Date.ToShortDateString();

            CellAntennaType.Value = techStation.techStationAntena.Name;
            CellMaxKoef.Value = techStation.techStationAntena.Gain.ToStringNullD();
            CellWidthAntenna.Value = techStation.techStationAntena.AntennaWidth.ToStringNullD();

            CellHighAntenna.Value = techStation.Agl.ToStringNullD();
            CellHighAntenna.Items.Add(techStation.Agl.ToStringNullD());

            CellAglMaxEmission.Value = techStation.Azimuth.ToStringNullD();
            CellAglMaxEmission.Items.Add(techStation.Azimuth.ToStringNullD());

            CellFider.Value = techStation.Fider.ToString();

            CellObjRCHP.IntValue = techStation.AntId;

            CellCall.Value = techStation.Call;
            CellCall.Items.Add(techStation.Call);
            CellDuplex.DoubleValue = techStation.Duplex;

            CellNominSend.Value = techStation.NSend;
            CellNominSend.Items.Add(techStation.NSend);
            CellNominAccept.Value = techStation.NAccept;
            CellNominAccept.Items.Add(techStation.NAccept);


            CellMMSI.Value = techStation.MMSI.ToString();
            CellOznach_REZ.Value = techStation.Oznach_REZ.ToString();
        }

        /// <summary>
        /// Обробник події зміни адреси в комбобоксі
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="newValue"></param>
        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            coordHelper.Paint(techDiffStation.Position, techStation.Position);
        }

        public void OnPressButtonAntennaName(Cell cell)
        {
            RecordPtr newAntenna = LookupAntenna(cell.Value, true);

            if ((newAntenna.Id > 0) && (newAntenna.Id < IM.NullI))
            {
                techStation.techStationAntena.TableName = newAntenna.Table;
                techStation.techStationAntena.Id = newAntenna.Id;
                techStation.techStationAntena.Load();

                InitAntenna();
                PaintGrid();
            }
        }

        public void InitAntenna()
        {
            CreateAntenComboBoxes();
            CellAntennaType.Value = techStation.techStationAntena.Name;

            CellStringBrowsableProperty.InitalizeHelper(CellMaxKoef,
               techStation.techStationAntena.Gain.ToStringNullD(),
               techDiffStation.Koef.ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellWidthAntenna,
               techStation.techStationAntena.AntennaWidth.ToStringNullD(),
               techDiffStation.WidthAnt.ToStringNullD());
        }

        /// <summary>
        /// Lookup Antenna
        /// </summary>
        /// <param name="initialValue">search template, usually name of antenna</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupAntenna(string initialValue, bool bReplaceQuota)
        {
            string criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + criteria + "*\"}";

            RecordPtr recAnt = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the antenna"), ICSMTbl.itblAntennaMob, param);
            return recAnt;
        }

        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                techStation.techStationEquip.TableName = newEquip.Table;
                techStation.techStationEquip.Id = newEquip.Id;
                techStation.techStationEquip.Load();

                techStation.DesigEmiss = techStation.techStationEquip.DesigEmission;

                InitEquipment();               

                PaintGrid();
            }
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            RecordPtr recEquip = SelectEquip("Підбір обладнання", ICSMTbl.itblEquipPmr, "NAME", initialValue, true);
            recEquip.Table = ICSMTbl.itblEquipPmr;  //Подмена таблицы
            return recEquip;
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = techStation.techStationEquip.Name;
            CellTxDesignEmission.Value = techStation.DesigEmiss;

            CellCertificateNumber.Value = techStation.techStationEquip.Certificate.Symbol;
            CellCertificateDate.Value = techStation.techStationEquip.Certificate.Date.ToShortDateString();
        }

        public void OnPressButtonObj(Cell cell)
        {
            int ID = objStation.GetI("ID");
            IMDBList.Show("Координуємі записи (якщо нема, то нема)", ICSMTbl.WienCoordMob, "[ID] in (" +
                " select wc.ID from %" + PlugTbl.XfaWienCrd + " wc, %" + ICSMTbl.itblMobStaFreqs + " mf " +
                " where wc.MOBSTA_FREQ_ID = mf.ID and wc.MOBSTA_FREQ_TN = '" + ICSMTbl.itblMobStation + "' and mf.STA_ID = " + ID.ToString() +
                " )", "TX_FREQ", OrderDirection.Ascending, 0, 0);                
        }

        public void PaintGrid()
        {
            coordHelper.Paint(techDiffStation.Position, techStation.Position);
        }


        /// <summary>
        /// Створення Комбо боксів для антени(які не можуть бути вибраними)
        /// </summary>
        private void CreateAntenComboBoxes()
        {            
            CellStringBrowsableProperty.InitalizeHelper(CellMaxKoef, techStation.techStationAntena.Gain.ToStringNullD(), techDiffStation.Koef.ToStringNullD());
            CellStringBrowsableProperty.InitalizeHelper(CellWidthAntenna, techStation.techStationAntena.AntennaWidth.ToStringNullD(), techDiffStation.WidthAnt.ToStringNullD());            
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellPowerTr.Initialize(techStation.techStationEquip.MaxPower[PowerUnits.W].Round(3), techDiffStation.Power[PowerUnits.W].Round(3));

            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, techStation.DesigEmiss, techDiffStation.DesigEmiss);

            //Поменять поведение, если сертификату введен прочерк. 
            if (techDiffStation.techDiffStationEquip.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, techStation.techStationEquip.Certificate.Symbol, techDiffStation.techDiffStationEquip.Certificate.Symbol);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, techStation.techStationEquip.Certificate.Date.ToShortDateStringNullT(), Convert.ToDateTime(techDiffStation.techDiffStationEquip.Certificate.Date.ToShortDateString()).ToShortDateString());
        }

        //Необхідні перевантажені методи


        /// <summary>
        /// Ініціалізація параметрів гріда
        /// </summary>
        /// <param name="cell"></param>
        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            ReadAllDefaultDataPositions();

            CellCertificateNumber.Value = techDiffStation.techDiffStationEquip.Certificate.Symbol;
            CellCertificateDate.Value = techDiffStation.techDiffStationEquip.Certificate.Date.ToShortDateString();

            CellPowerTr.Value = techDiffStation.Power[PowerUnits.W].Round(3).ToStringNullD();
            CellTxDesignEmission.Value = techDiffStation.DesigEmiss;

            CellWidthAntenna.Value = techDiffStation.WidthAnt.ToStringNullD();
            CellMaxKoef.Value = techDiffStation.Koef.ToStringNullD();

            FillDiffComboBox(CellHighAntenna, techStation.Agl.ToStringNullD(), techDiffStation.HeightAnt.ToStringNullD());

            CellRadioTech.Value = techDiffStation.Standart;
            CellDefStat.Value = techDiffStation.Defin;

            if (techStation.ClassStationDict.ContainsKey(techStation.ClassStat) && techStation.ClassStationDict.ContainsKey(techDiffStation.ClassStat))
                FillDiffComboBox(CellClassStat, techStation.ClassStat + " - " + techStation.ClassStationDict[techStation.ClassStat],
                    techDiffStation.ClassStat + " - " + techStation.ClassStationDict[techDiffStation.ClassStat]);

            CellASL.DoubleValue = techDiffStation.Asl;                       

            FillDiffComboBox(CellAglMaxEmission, techStation.Azimuth.ToStringNullD(), techDiffStation.Azimuth.ToStringNullD());
            
            FillDiffComboBox(CellCall, techStation.Call, techDiffStation.Call);
            
            FillDiffComboBox(CellNominSend, techStation.NSend, techDiffStation.NomSend);
            
            FillDiffComboBox(CellNominAccept, techStation.NAccept, techDiffStation.NomReceive);
            Comment = techDiffStation.StatComment;

            coordHelper.Initialize(techDiffStation.Position, techStation.Position);
            CreateEquipmentComboBoxes();
            CreateAntenComboBoxes();

            CellObjRCHP.Cell.ButtonText = "Координуємі...";
         
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(techStation.Position.TableName, techStation.Position.Id);
                rcPtr.UserEdit();
            }
        }

        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "MOB_STATION" };
            
            try
            {
                PositionState diffPos = techDiffStation.Position.Clone() as PositionState;
                AttachmentControl ac = new AttachmentControl();
                selectedPosition = coordHelper.GetSelectedPosition(techStation.Position, techDiffStation.Position);
                if (selectedPosition != techStation.Position)
                {
                    techStation.PosId = SaveNewPosition(selectedPosition, techStation.Position, assgnReferenceTables);
                    techStation.Position = selectedPosition;
                }

                techStation.Id = recordID.Id;

                techStation.Standart = CellRadioTech.Value;
                techStation.Defin = CellDefStat.Value;
                techStation.ClassStat = CellClassStat.Value.Split(' ')[0].Trim();
                                
                //Сертифікат (и)                

                if (!string.IsNullOrEmpty(CellHighAntenna.Value))
                    techStation.Agl = Convert.ToDouble(CellHighAntenna.Value);

                if (!string.IsNullOrEmpty(CellAglMaxEmission.Value))
                    techStation.Azimuth = Convert.ToDouble(CellAglMaxEmission.Value);
                techStation.Call = CellCall.Value;
                if (!string.IsNullOrEmpty(CellFider.Value))
                    techStation.Fider = Convert.ToDouble(CellFider.Value);
                techStation.NSend = CellNominSend.Value;
                techStation.NAccept = CellNominAccept.Value;
                techStation.ApplId = ApplID;
                techStation.StatComment = techDiffStation.StatComment;
                techStation.MMSI = CellMMSI.Value;
                techStation.Oznach_REZ = CellOznach_REZ.Value;

                techStation.Save();

                techDiffStation.Remove();

                ac.RelocateAndCopyDocLinks(
                    diffPos.TableName,
                    diffPos.Id,
                    MobStaApp.TableName,
                    techStation.Id,
                    techStation.Position.TableName,
                    techStation.Position.Id,
                    ICSMTbl.itblAdmSite,
                    techStation.Position.AdminSitesId);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return true;
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStaTechAppCommit;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }
    }
}
