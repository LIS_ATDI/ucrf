﻿using System;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class CTvAnalogAppCommit : BaseCommitClass
    {
        private bool _initialized;

        private const string posTable = ICSMTbl.itblPositionBro;
        private PositionState selectedPosition;
        private CoordinateCommitHelper coordHelper;
        private TvAnalogStation tvStation;
        private TvAnalogDiffStation tvDiffStation;
        /// <summary>
        /// Широта
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();
        /// <summary>
        /// Довгота
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();
        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();
        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Стандарт
        /// </summary>
        private CellStringComboboxAplyProperty CellStandart = new CellStringComboboxAplyProperty();
        /// <summary>
        /// система
        /// </summary>
        private CellStringComboboxAplyProperty CellSystem = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Зміщення несівної частоти, кГц
        /// </summary>
        private CellStringComboboxAplyProperty CellEquipmentShiftOstov = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();
        /// <summary>
        /// Потужність передавача,відео
        /// </summary>
        private CellDoubleLookupProperty CellPowerVideo = new CellDoubleLookupProperty(); // 
        /// <summary>
        /// Потужність передавача,аудио
        /// </summary>
        private CellDoubleLookupProperty CellPowerAudio = new CellDoubleLookupProperty();
        /// <summary>
        /// Ширина смуги випромінювання,МГц
        /// </summary>
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();
        /// <summary>
        /// Сертифікат відповідності номер 
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Сертифікат відповідності Дата 
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();
        /// <summary>
        /// Клас випромінювання,відео
        /// </summary>
        private CellStringBrowsableProperty CellEmiVideo = new CellStringBrowsableProperty();
        /// <summary>
        /// Клас випромінювання,звук
        /// </summary>
        private CellStringBrowsableProperty CellEmiAudio = new CellStringBrowsableProperty();
        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringLookupEncodedProperty CellAntennaDir = new CellStringLookupEncodedProperty();
        /// <summary>
        ///  Поляризація антени
        /// </summary>
        private CellStringLookupEncodedProperty CellAntennaPolar = new CellStringLookupEncodedProperty();
        /// <summary>
        /// Висота антени над Землею
        /// </summary>
        private CellStringComboboxAplyProperty CellAntennaHeight = new CellStringComboboxAplyProperty();
        /// <summary>
        /// максимальна висота
        /// </summary>
        private CellDoubleProperty CellAntennaMaxHeight = new CellDoubleProperty();
        /// <summary>
        /// Довжина фідера
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();
        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellStringProperty CellMaxKoefG = new CellStringProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>
        private CellStringProperty CellMaxKoefV = new CellStringProperty();
        /// <summary>
        /// ЕВП
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП макс
        /// </summary>
        private CellStringProperty CellEVPm = new CellStringProperty();

        /// <summary>
        /// ТВ канал
        /// </summary>
        private CellStringComboboxAplyProperty Celltvchan = new CellStringComboboxAplyProperty();
        /// <summary>
        /// частота:відео
        /// </summary>
        private CellStringComboboxAplyProperty CellfreqVid = new CellStringComboboxAplyProperty();
        /// <summary>
        /// частота:звук
        /// </summary>
        private CellStringComboboxAplyProperty CellfreqSnd = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Позивний сигнал(програма мовлення)
        /// </summary>
        private CellStringProperty CellPositiv = new CellStringProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellhomeState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellinternationalState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Об'єкти РЧП 
        /// </summary>
        private CellStringProperty CellObjRchp = new CellStringProperty();

        public CTvAnalogAppCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, ICSMTbl.itblTV, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTV2;
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);

            tvStation = new TvAnalogStation();
            tvStation.Id = recordID.Id;
            tvStation.Load();

            tvDiffStation = new TvAnalogDiffStation();
            tvDiffStation.Id = recordID.Id;
            tvDiffStation.Load();
            Comment = tvDiffStation.StatComment;
        }

        #region Overrides of BaseAppClass


        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {

        }

        /// <summary>
        /// Загружаем данные для базовой станции
        /// </summary>
        private void ReadLoadDefaultPosition()
        {
            CellEmiVideo.Value = tvStation.EmiVideo;
            CellEmiAudio.Value = tvStation.EmiAudio;

            if (tvStation.SystemDict.Count > 0)
                if (tvStation.SystemDict.ContainsKey(tvStation.Systems))
                    CellSystem.Value = tvStation.SystemDict[tvStation.Systems];

            CellStandart.Value = tvStation.Standart;

            CellEquipmentNameNumber.Value = tvStation.NumberREZ;
            CellEquipmentNameREZ.Value = tvStation.Position.Name;

            CellEquipmentShiftOstov.Value = tvStation.NesivFreq.ToStringNullD();

            CellEquipmentName.Value = tvStation.TvStationEquipment.Name;

            CellCertificateNumber.Value = tvStation.TvStationEquipment.Certificate.Symbol;
            CellCertificateDate.Value = tvStation.TvStationEquipment.Certificate.Date.ToShortDateStringNullT();
            CellFiderlost.DoubleValue = tvStation.FeederLosses;
            CellFiderLength.DoubleValue = tvStation.FeederLength;

            CellPositiv.Value = tvStation.Call;

            CellAntennaHeight.Value = tvStation.Height.ToStringNullD();

            CellAntennaMaxHeight.DoubleValue = tvStation.MaxHeight;
           

            //if (!tvStation.ChannelList.Contains(tvStation.tvChannel))
            //    if (tvStation.ChannelList.Count > 0)
            //        tvStation.tvChannel = tvStation.ChannelList[0];
            Celltvchan.Value = tvStation.tvChannel;

            if (tvStation.HomeDict.Count > 0)
                if (tvStation.HomeDict.ContainsKey(tvStation.homeState))
                    CellhomeState.Value = tvStation.homeState + " - " + tvStation.HomeDict[tvStation.homeState];

            if (tvStation.InternationalDict.Count > 0)
                if (tvStation.InternationalDict.ContainsKey(tvStation.intState))
                    CellinternationalState.Value = tvStation.intState + " - " + tvStation.InternationalDict[tvStation.intState];

            //if (!string.IsNullOrEmpty(tvStation.NesivFreq.ToStringNullD()))
            CellEquipmentShiftOstov.Items.Add(tvStation.NesivFreq.ToStringNullD());

            // if (!string.IsNullOrEmpty(tvStation.Height.ToStringNullD()))
            CellAntennaHeight.Items.Add(tvStation.Height.ToStringNullD());

            // if (!string.IsNullOrEmpty(tvStation.tvChannel))
            Celltvchan.Items.Add(tvStation.tvChannel);

            //if (tvStation.freqAud.Round(3)!=IM.NullD)
            CellfreqSnd.Items.Add(tvStation.freqAud.Round(3).ToStringNullD());

            // if (tvStation.freqVid.Round(3)!=IM.NullD)
            CellfreqVid.Items.Add(tvStation.freqVid.Round(3).ToStringNullD());

            CellObjRchp.Value = tvStation.Id.ToString();
        }
        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "NewKey-KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(tvStation.Position.TableName, tvStation.Position.Id);
                rcPtr.UserEdit();
            }
        }
        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "TV_STATION" };
            PositionState diffPos = tvDiffStation.Position.Clone() as PositionState;
            AttachmentControl ac = new AttachmentControl();

            selectedPosition = coordHelper.GetSelectedPosition(tvStation.Position, tvDiffStation.Position);
            if (selectedPosition != tvStation.Position)
                SaveNewPosition(selectedPosition, tvStation.Position, assgnReferenceTables);
            tvStation.Position = selectedPosition;

            //tvStation.EmiVideo = CellEmiVideo.Value;
            //tvStation.EmiAudio = CellEmiAudio.Value;
            tvStation.Height = CellAntennaHeight.Value.ToDouble(IM.NullD);
            tvStation.tvChannel = Celltvchan.Value;
            tvStation.Direction = CellAntennaDir.Value;
            tvStation.Polarization = CellAntennaPolar.Value;
            tvStation.NesivFreq = CellEquipmentShiftOstov.Value.ToDouble(IM.NullD);
            tvStation.freqAud = CellfreqSnd.Value.ToDouble(IM.NullD);
            tvStation.freqVid = CellfreqVid.Value.ToDouble(IM.NullD);
            tvStation.PowVideo[PowerUnits.W] = CellPowerVideo.LookupedDoubleValue;
            tvStation.PowAudio[PowerUnits.W] = CellPowerAudio.LookupedDoubleValue;
            tvStation.ApplId = ApplID;
            tvStation.StatComment = tvDiffStation.StatComment;
            tvStation.Save();

            tvDiffStation.Remove();

            ac.RelocateAndCopyDocLinks(
               diffPos.TableName,
               diffPos.Id,
               CTvAnalog.TableName,
               tvStation.Id,
               tvStation.Position.TableName,
               tvStation.Position.Id,
               ICSMTbl.itblAdmSite,
               tvStation.Position.AdminSitesId);

            return true;
        }

        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            return "";
        }

        /// <summary>
        /// Печать документа
        /// </summary>
        /// <param name="cell">Ячейка грида</param>
        public override void InitParamGrid(Cell cell)
        {

        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;
            ReadLoadDefaultPosition();

            //CellEquipmentName.Value = tvDiffStation.EquipmentName;

            CellPowerAudio.Initialize(tvStation.PowAudio[PowerUnits.W].Round(3), tvDiffStation.PowAudio.Round(3));
            CellPowerVideo.Initialize(tvStation.PowVideo[PowerUnits.W].Round(3), tvDiffStation.PowVideo.Round(3));

            CellEmiVideo.Value = tvDiffStation.EmiVideo;
            CellEmiAudio.Value = tvDiffStation.EmiAudio;

            CellCertificateNumber.Value = tvDiffStation.TvDiffStationEquipment.Certificate.Symbol;
            CellCertificateDate.Value = tvDiffStation.TvDiffStationEquipment.Certificate.Date.ToShortDateString();

            FillDiffComboBox(CellEquipmentShiftOstov, tvStation.NesivFreq.Round(3).ToStringNullD(), tvDiffStation.NesivFreq.Round(3).ToStringNullD());

            FillDiffComboBox(CellAntennaHeight, tvStation.Height.Round(3).ToStringNullD(), tvDiffStation.Height.Round(3).ToStringNullD());

            if (!string.IsNullOrEmpty(tvStation.EmiVideo))
                CellEmiVideo.Items.Add(tvStation.EmiVideo);
            if (!string.IsNullOrEmpty(tvDiffStation.EmiVideo))
                CellEmiVideo.Items.Add(tvDiffStation.EmiVideo);

            if (!string.IsNullOrEmpty(tvStation.EmiAudio))
                CellEmiAudio.Items.Add(tvStation.EmiAudio);
            if (!string.IsNullOrEmpty(tvDiffStation.EmiAudio))
                CellEmiAudio.Items.Add(tvDiffStation.EmiAudio);

            CellAddress.Value = tvDiffStation.Position.FullAddress;

            if (!string.IsNullOrEmpty(tvStation.TvStationEquipment.Certificate.Symbol))
                CellCertificateNumber.Items.Add(tvStation.TvStationEquipment.Certificate.Symbol);
            if (!string.IsNullOrEmpty(tvDiffStation.CertificateNumber))
                CellCertificateNumber.Items.Add(tvDiffStation.CertificateNumber);

            if (!string.IsNullOrEmpty(tvStation.TvStationEquipment.Certificate.Date.ToShortDateStringNullT()))
                CellCertificateDate.Items.Add(tvStation.TvStationEquipment.Certificate.Date.ToShortDateStringNullT());
            if (!string.IsNullOrEmpty(tvDiffStation.CertificateDate.ToString()))
                CellCertificateDate.Items.Add(tvDiffStation.CertificateDate.ToString());

            FillDiffComboBox(Celltvchan, tvStation.tvChannel, tvDiffStation.Channel);

            FillDiffComboBox(CellfreqVid, tvStation.freqVid.Round(3).ToStringNullD(), tvDiffStation.FreqVideo.Round(3).ToStringNullD());

            FillDiffComboBox(CellfreqSnd, tvStation.freqAud.Round(3).ToStringNullD(), tvDiffStation.FreqAudio.Round(3).ToStringNullD());

            CellStringLookupEncodedProperty.InitalizeHelper(CellAntennaPolar, tvStation.Polarization, tvDiffStation.Polar);
            CellStringLookupEncodedProperty.InitalizeHelper(CellAntennaDir, tvStation.Direction, tvDiffStation.Direction);

            coordHelper.Initialize(tvDiffStation.Position, tvStation.Position);
            CreateEquipmentComboBoxes();

            _initialized = true;
            OnRecalculateErp(null);
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, tvStation.TvStationEquipment.BandWidth.Round(3).ToStringNullD(), tvDiffStation.Bandwith.Round(3).ToStringNullD());

            CellStringBrowsableProperty.InitalizeHelper(CellEmiAudio, tvStation.TvStationEquipment.EAudio, tvDiffStation.EmiAudio);

            CellStringBrowsableProperty.InitalizeHelper(CellEmiVideo, tvStation.TvStationEquipment.EVideo, tvDiffStation.EmiVideo);

            //Поменять поведение, если сертификату введен прочерк. 
            if (tvDiffStation.TvDiffStationEquipment.Certificate.Symbol == "-")
            {
                CellCertificateNumber.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
                CellCertificateDate.Behavior = CellStringBrowsableProperty.BehaviorType.Certificate;
            }

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, tvStation.TvStationEquipment.Certificate.Symbol, tvDiffStation.TvDiffStationEquipment.Certificate.Symbol);

            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, tvStation.TvStationEquipment.Certificate.Date.ToShortDateStringNullT(), Convert.ToDateTime(tvDiffStation.TvDiffStationEquipment.Certificate.Date.ToShortDateString()).ToShortDateString());


        }
        #endregion


        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.CTvAnalogAppCommit;
        }

        public void OnPressButtonLongitude(Cell cell)
        {
            //IMObject objPosition = frmSelectPosition.SelectPosition(posTable, 1, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
            //if (objPosition != null)
            //{
            //    tvDiffStation.Position = new PositionState();
            //    tvDiffStation.Position.LoadStatePosition(objPosition);
            //    objPosition.Dispose();
            //    CellAddress.Value = tvDiffStation.Position.FullAddress;
            //    CellLongitude.DoubleValue = tvDiffStation.Position.X;
            //    CellLatitude.DoubleValue = tvDiffStation.Position.Y;
            //}
        }


        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                tvStation.TvStationEquipment.TableName = newEquip.Table;
                tvStation.TvStationEquipment.Id = newEquip.Id;
                tvStation.TvStationEquipment.Load();
                tvStation.OnNewEquipment();

                InitEquipment();

                CellPowerAudio.Initialize(tvStation.PowAudio[PowerUnits.W], tvDiffStation.PowAudio);
                CellPowerVideo.Initialize(tvStation.PowVideo[PowerUnits.W], tvDiffStation.PowVideo);

                PaintGrid();
            }
        }

        public void PaintGrid()
        {
            coordHelper.Paint(tvDiffStation.Position, tvStation.Position);
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipBro, param);
            return RecEquip;
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = tvStation.TvStationEquipment.Name;
            CellTxBandwidth.Value = tvStation.Bandwith.Round(3).ToStringNullD();
            
            CellEmiVideo.Value = tvStation.TvStationEquipment.EVideo;
            CellEmiAudio.Value = tvStation.TvStationEquipment.EAudio;

            CellCertificateNumber.Value = tvStation.TvStationEquipment.Certificate.Symbol;
            CellCertificateDate.Value = tvStation.TvStationEquipment.Certificate.Date.ToShortDateString();
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            coordHelper.Paint(tvDiffStation.Position, tvStation.Position);
        }

        public void OnBeforeChangeShiftOstov(Cell cell, ref string newValue) { }

        public void OnPressButtonAddressName(Cell cell) { }

        public void OnRecalculateErp(Cell cell)
        {
            if (_initialized)
            {
                tvStation.Power[PowerUnits.W] = CellPowerVideo.LookupedDoubleValue;
                tvStation.Polarization = CellAntennaPolar.Value;
                tvStation.Direction = CellAntennaDir.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!tvStation.HorizontalDiagramm.IsEmpty && tvStation.Polarization != "V")
                value = tvStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!tvStation.VerticalDiagramm.IsEmpty && tvStation.Polarization != "H")
                value = tvStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            tvStation.CalculateERP();
            if (tvStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = tvStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (tvStation.Erp.Vertical.IsValid)
                CellEVPV.Value = tvStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = tvStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}

