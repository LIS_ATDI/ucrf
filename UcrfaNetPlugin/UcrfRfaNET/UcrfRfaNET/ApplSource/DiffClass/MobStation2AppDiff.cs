﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class MobStation2AppDiff : BaseDiffClass
    {
        private MobStation2AppStation mob2Station;
        private MobStation2AppDiffStation mob2DiffStation;

        public string TableName = ICSMTbl.MobStation2;

        //6 комірок
        private StationSectorCollection<MobStation2AppStation> mob2StationList;
        private StationSectorCollection<MobStation2AppDiffStation> mob2StationListDiff;
        //Масиви для секторів
        private CellStringProperty[] arrayGeneral;
        private CellDoubleBoundedProperty[] arrayAzimuth;
        private CellDoubleBoundedProperty[] arrayPlaceSite;
        private CellStringProperty[] arrayTypeAnten;
        private CellDoubleProperty[] arrayHighAnten;
        private CellDoubleProperty[] arrayKoefGain;
        private CellDoubleBoundedProperty[] arrayWdthAntena;
        private CellStringComboboxAplyProperty[] arrayPolar;
        private CellStringProperty[] arrayNomAccept;
        private CellStringProperty[] arrayNomSend;
        private CellStringComboboxAplyProperty[] arrayOnOff;
        private CellIntegerProperty[] arrayObjRchp;

        //Список фотографій
        public List<string> ListFotosSrc;
        public List<string> ListFotosDst;

        //private int IDPos = IM.NullI;
        #region CELL
        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Максимальна потужність передавача, дБВт        
        /// </summary>
        private CellDoubleProperty CellPowerTr = new CellDoubleProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellDoubleProperty CellTxBandwidth = new CellDoubleProperty();

        /// <summary>
        /// Клас випромінювання               
        /// </summary>        
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        /// <summary>
        /// Типи і параметри модуляцій
        /// </summary>
        private CellStringComboboxAplyProperty CellModulations = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Швидкість цифрового потоку, Мбіт/c
        /// </summary>
        private CellDoubleProperty CellSpeedThread = new CellDoubleProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Назви заголовків
        /// </summary>
        private CellStringProperty CellGeneral1 = new CellStringProperty();
        private CellStringProperty CellGeneral2 = new CellStringProperty();
        private CellStringProperty CellGeneral3 = new CellStringProperty();
        private CellStringProperty CellGeneral4 = new CellStringProperty();
        private CellStringProperty CellGeneral5 = new CellStringProperty();
        private CellStringProperty CellGeneral6 = new CellStringProperty();

        /// <summary>
        /// Азимут максимального випромінювання, град 
        /// </summary>
        private CellDoubleBoundedProperty CellAglMaxEmission1 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAglMaxEmission2 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAglMaxEmission3 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAglMaxEmission4 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAglMaxEmission5 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAglMaxEmission6 = new CellDoubleBoundedProperty();

        /// <summary>
        /// Кут місця максимального випромінювання, град 
        /// </summary>
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission1 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission2 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission3 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission4 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission5 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellAngleSiteMaxEmission6 = new CellDoubleBoundedProperty();

        /// <summary>
        /// Антена:тип
        /// </summary>        
        private CellStringProperty CellAntennaType1 = new CellStringProperty();
        private CellStringProperty CellAntennaType2 = new CellStringProperty();
        private CellStringProperty CellAntennaType3 = new CellStringProperty();
        private CellStringProperty CellAntennaType4 = new CellStringProperty();
        private CellStringProperty CellAntennaType5 = new CellStringProperty();
        private CellStringProperty CellAntennaType6 = new CellStringProperty();

        /// <summary>
        /// Висота антени над рівнем землі, м
        /// </summary>                
        private CellDoubleProperty CellHighAntenna1 = new CellDoubleProperty();
        private CellDoubleProperty CellHighAntenna2 = new CellDoubleProperty();
        private CellDoubleProperty CellHighAntenna3 = new CellDoubleProperty();
        private CellDoubleProperty CellHighAntenna4 = new CellDoubleProperty();
        private CellDoubleProperty CellHighAntenna5 = new CellDoubleProperty();
        private CellDoubleProperty CellHighAntenna6 = new CellDoubleProperty();

        /// <summary>
        /// Коефіцієнт підсилення, дБі
        /// </summary>        
        private CellDoubleProperty CellMaxKoef1 = new CellDoubleProperty();
        private CellDoubleProperty CellMaxKoef2 = new CellDoubleProperty();
        private CellDoubleProperty CellMaxKoef3 = new CellDoubleProperty();
        private CellDoubleProperty CellMaxKoef4 = new CellDoubleProperty();
        private CellDoubleProperty CellMaxKoef5 = new CellDoubleProperty();
        private CellDoubleProperty CellMaxKoef6 = new CellDoubleProperty();

        /// <summary>
        /// Ширина ДС антени(горизонт.) , дБі
        /// </summary>        
        private CellDoubleBoundedProperty CellWidthAntenna1 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellWidthAntenna2 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellWidthAntenna3 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellWidthAntenna4 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellWidthAntenna5 = new CellDoubleBoundedProperty();
        private CellDoubleBoundedProperty CellWidthAntenna6 = new CellDoubleBoundedProperty();

        /// <summary>
        /// Тип поляризації 
        /// </summary>
        private CellStringComboboxAplyProperty CellAntenPolar1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellAntenPolar6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Номінал частот приймання, МГц
        /// </summary>
        private CellStringProperty CellNominAccept1 = new CellStringProperty();
        private CellStringProperty CellNominAccept2 = new CellStringProperty();
        private CellStringProperty CellNominAccept3 = new CellStringProperty();
        private CellStringProperty CellNominAccept4 = new CellStringProperty();
        private CellStringProperty CellNominAccept5 = new CellStringProperty();
        private CellStringProperty CellNominAccept6 = new CellStringProperty();

        /// <summary>
        /// Номінал частот передавання, МГц
        /// </summary>
        private CellStringProperty CellNominSend1 = new CellStringProperty();
        private CellStringProperty CellNominSend2 = new CellStringProperty();
        private CellStringProperty CellNominSend3 = new CellStringProperty();
        private CellStringProperty CellNominSend4 = new CellStringProperty();
        private CellStringProperty CellNominSend5 = new CellStringProperty();
        private CellStringProperty CellNominSend6 = new CellStringProperty();

        /// <summary>
        /// Сектор ввімкнено/вимкнено
        /// </summary>
        private CellStringComboboxAplyProperty CellOnOff1 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff2 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff3 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff4 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff5 = new CellStringComboboxAplyProperty();
        private CellStringComboboxAplyProperty CellOnOff6 = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP1 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP2 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP3 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP4 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP5 = new CellIntegerProperty();
        private CellIntegerProperty CellObjRCHP6 = new CellIntegerProperty();
        #endregion
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public MobStation2AppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, MobSta2App.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppBS;

            // Створення масивів секторних данних для забезпечення спрощення роботи з ними
            FillArraySectors();
            //Стандартні значення
            mob2StationList = new StationSectorCollection<MobStation2AppStation>();
            mob2StationList.ApplID = applID;
            mob2StationList.LoadSectorsOrSingle(id);
            mob2Station = mob2StationList.GetSectorByIndex(0);
            SCVisnovok = mob2Station.Finding;

            //URZP значення
            mob2StationListDiff = new StationSectorCollection<MobStation2AppDiffStation>();
            mob2StationListDiff.ApplID = applID;
            mob2StationListDiff.LoadSectorsOrSingle(id);

            ReadAllDataDefaultPositions();
            mob2DiffStation = mob2StationListDiff.GetSectorByIndex(0);
            if (mob2DiffStation.Position == null)
                mob2DiffStation.Position = mob2Station.Position;
            CellCertificateDate.NullString = "-";
            if (mob2DiffStation.IsEnableDiff)
            {
                CellAddress.Value = mob2DiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = mob2DiffStation.Position.LonDms;
                CellLatitude.DoubleValue = mob2DiffStation.Position.LatDms;
                CellASL.DoubleValue = mob2DiffStation.Position.ASl;

                CellPowerTr.DoubleValue = mob2DiffStation.MaxPower;
                CellEquipmentName.Value = mob2DiffStation.EquipmentName;
                CellTxBandwidth.DoubleValue = mob2DiffStation.Bandwith;
                CellTxDesignEmission.Value = mob2DiffStation.ClassEmission;
                if (mob2Station.ModulationList.Count < mob2DiffStation.Modulation.ToInt32(IM.NullI))
                    mob2DiffStation.Modulation = "0";
                CellModulations.Value = mob2Station.ModulationList[mob2DiffStation.Modulation.ToInt32(IM.NullI)];

                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, mob2DiffStation.mobDiffStationEquip.Certificate);

                //Заповнення полів з 6 елементами...                
                int sectorCount = mob2StationList.GetSectorCount();
                for (int i = 0; i < sectorCount; i++)
                {
                    MobStation2AppDiffStation currentSectorDiff = mob2StationListDiff.GetSectorByIndex(i);
                    currentSectorDiff.Load();
                    if (!string.IsNullOrEmpty(currentSectorDiff.NomReceive))
                        arrayNomAccept[i].Value = currentSectorDiff.NomReceive;
                    if (!string.IsNullOrEmpty(currentSectorDiff.NomSend))
                        arrayNomSend[i].Value = currentSectorDiff.NomSend;
                    arrayHighAnten[i].DoubleValue = currentSectorDiff.HighAnten;

                    arrayPlaceSite[i].DoubleValue = currentSectorDiff.AglSiteMax;
                    arrayWdthAntena[i].DoubleValue = currentSectorDiff.WideAnten;

                    arrayAzimuth[i].DoubleValue = currentSectorDiff.AglMaxEmission;

                    arrayTypeAnten[i].Value = currentSectorDiff.AntenType;
                    arrayKoefGain[i].DoubleValue = currentSectorDiff.KoefAnten;
                    if (!string.IsNullOrEmpty(currentSectorDiff.OnOff.ToString()))
                        if (mob2Station.OnOffList.Count >= currentSectorDiff.OnOff)
                            arrayOnOff[i].Value = mob2Station.OnOffList[currentSectorDiff.OnOff];
                    if (!string.IsNullOrEmpty(currentSectorDiff.PolarAnten))
                        arrayPolar[i].Value = currentSectorDiff.PolarAnten + " - " + mob2Station.PolarAntDict[currentSectorDiff.PolarAnten];
                    Comment = currentSectorDiff.StatComment;
                }
            } else
            {
                mob2DiffStation.Position = mob2Station.Position.Clone() as PositionState;
            }
            _stationObject = mob2Station;
        }

        private void ReadAllDataDefaultPositions()
        {
            for (int i = 0; i < 6; i++)
                arrayGeneral[i].Value = "Сектор" + (i + 1);
            CellLongitude.DoubleValue2 = mob2Station.Position.LonDms;
            CellLatitude.DoubleValue2 = mob2Station.Position.LatDms;
            CellASL.Value2 = FillValue2(mob2Station.Position.ASl.ToStringNullD());
            CellAddress.Value2 = FillValue2(mob2Station.Position.FullAddress);
            CellTxBandwidth.Value2 = FillValue2(mob2Station.mobStationEquipment.Bandwidth.ToStringNullD());
            CellTxDesignEmission.Value2 = FillValue2(mob2Station.mobStationEquipment.DesigEmission);
            if (string.IsNullOrEmpty(mob2Station.NameRez))
                mob2Station.NameRez = "*";
            CellEquipmentName.Value2 = FillValue2(mob2Station.mobStationEquipment.Name);
            CellPowerTr.Value2 = FillValue2(mob2Station.Power.ToStringNullD());
            CellSpeedThread.Value2 = FillValue2(mob2Station.SpeedThread.ToStringNullD());

            if (!string.IsNullOrEmpty(mob2Station.CertificateNumber))
                mob2Station.mobStationEquipment.Certificate.Symbol = mob2Station.CertificateNumber;
            CellCertificateNumber.Value = mob2Station.mobStationEquipment.Certificate.Symbol;
            CellCertificateNumber.Value2 = mob2Station.mobStationEquipment.Certificate.Symbol;
            CellCertificateNumber.SetRedValue();

            mob2Station.mobStationEquipment.Certificate.Date = mob2Station.CertificateDate;
            CellCertificateDate.DateValue = mob2Station.mobStationEquipment.Certificate.Date;
            CellCertificateDate.DateValue2 = mob2Station.mobStationEquipment.Certificate.Date;
            CellCertificateDate.SetRedValue();

            CellLongitude.DoubleValue = mob2Station.Position.LonDms;
            CellLatitude.DoubleValue = mob2Station.Position.LatDms;
            CellASL.DoubleValue = mob2Station.Position.ASl;
            CellAddress.Value = mob2Station.Position.FullAddress;
            CellPowerTr.Value = mob2Station.Power.ToStringNullD();
            CellTxBandwidth.Value = mob2Station.mobStationEquipment.Bandwidth.ToStringNullD();
            if (!string.IsNullOrEmpty(mob2Station.mobStationEquipment.DesigEmission))
                CellTxDesignEmission.Value = mob2Station.mobStationEquipment.DesigEmission;
            CellSpeedThread.DoubleValue = mob2Station.SpeedThread;
            if (!string.IsNullOrEmpty(mob2Station.mobStationEquipment.Name))
                CellEquipmentName.Value = mob2Station.mobStationEquipment.Name;
            CellPowerTr.Value = mob2Station.Power.ToStringNullD();
            CellModulations.Items.AddRange(mob2Station.ModulationList);
            if (!(mob2Station.ModulationList.Count > 0 && mob2Station.ModulationList.Count >= mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)))
                mob2Station.mobStationEquipment.Modulation = "0";
            CellModulations.Value2 = FillValue2(mob2Station.ModulationList[mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)]);
            CellModulations.Value = mob2Station.ModulationList[mob2Station.mobStationEquipment.Modulation.ToInt32(IM.NullI)];
            int sectorCount = mob2StationList.GetSectorCount();
            //Сектори
            for (int i = 0; i < sectorCount; i++)
            {
                MobStation2AppStation currentSector = mob2StationList.GetSectorByIndex(i);
                currentSector.Load();
                arrayPlaceSite[i].LowerBound = 0.0;
                arrayPlaceSite[i].UpperBound = 360.0;
                arrayWdthAntena[i].LowerBound = 0.0;
                arrayWdthAntena[i].UpperBound = 360.0;
                arrayAzimuth[i].LowerBound = 0.0;
                arrayAzimuth[i].UpperBound = 360.0;

                arrayAzimuth[i].DoubleValue = currentSector.Azimuth;
                arrayAzimuth[i].Value2 = FillValue2(currentSector.Azimuth.ToStringNullD());
                arrayPlaceSite[i].DoubleValue = currentSector.PlaceSite;
                arrayPlaceSite[i].Value2 = FillValue2(currentSector.PlaceSite.ToStringNullD());
                arrayHighAnten[i].DoubleValue = currentSector.AGL;
                arrayHighAnten[i].Value2 = FillValue2(currentSector.AGL.ToStringNullD());
                arrayPolar[i].Items.AddRange(currentSector.FillPolar());
                if (string.IsNullOrEmpty(currentSector.PolarAnt))
                    currentSector.PolarAnt = currentSector.PolarAntDict.ToList()[0].Key;
                arrayPolar[i].Value2 = FillValue2(currentSector.PolarAnt + " - " + currentSector.PolarAntDict[currentSector.PolarAnt]);
                arrayPolar[i].Value = currentSector.PolarAnt + " - " + currentSector.PolarAntDict[currentSector.PolarAnt];

                arrayNomAccept[i].Value = currentSector.NominAccept;
                arrayNomAccept[i].Value2 = FillValue2(currentSector.NominAccept);
                arrayNomSend[i].Value2 = FillValue2(currentSector.NominSend);
                arrayNomSend[i].Value = currentSector.NominSend;
                arrayObjRchp[i].IntValue = currentSector.ObjRchp;
                arrayObjRchp[i].Value2 = FillValue2(currentSector.ObjRchp.ToString());
                if (!string.IsNullOrEmpty(currentSector.TypeAnten))
                    arrayTypeAnten[i].Value = currentSector.mobStationAntena.Name;
                arrayTypeAnten[i].Value2 = FillValue2(currentSector.mobStationAntena.Name);
                arrayKoefGain[i].DoubleValue = currentSector.mobStationAntena.Gain;
                arrayKoefGain[i].Value2 = FillValue2(currentSector.mobStationAntena.Gain.ToStringNullD());
                arrayWdthAntena[i].DoubleValue = currentSector.mobStationAntena.AntennaWidth;
                arrayWdthAntena[i].Value2 = FillValue2(currentSector.mobStationAntena.AntennaWidth.ToStringNullD());

                arrayOnOff[i].Value = mob2Station.OnOffList[1];
                arrayOnOff[i].Items.AddRange(mob2Station.OnOffList);
            }
        }

        /// <summary>        
        /// Формування адресу сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");

            // Обираємо запис з таблиці                     
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionMob2, 1, tmpXy.Longitude, tmpXy.Latitude);

            if (objPosition != null)
            {
                mob2DiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                CellLongitude.DoubleValue = mob2DiffStation.Position.LongDms;
                CellLatitude.DoubleValue = mob2DiffStation.Position.LatDms;
                CellASL.DoubleValue = mob2DiffStation.Position.ASl;
                if (!string.IsNullOrEmpty(mob2DiffStation.Position.FullAddress))
                    CellAddress.Value = mob2DiffStation.Position.FullAddress;

                // Створення форми адреси місця встановлення РЕЗ
                FormNewPosition frm = new FormNewPosition(mob2DiffStation.Position.CityId);

                frm.SetLongitudeAsDms(mob2DiffStation.Position.LonDms);
                frm.SetLatitudeAsDms(mob2DiffStation.Position.LatDms);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = mob2DiffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                    frm.GetFotos(out ListFotosDst, out ListFotosSrc);
            }
        }

        //валідація        
        public void OnBeforeChangeAntType(Cell cell, ref string val)
        {
            if (string.IsNullOrEmpty(val))
                val = cell.Value;
        }

        public void OnBeforeChangeNumb(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) || newVal < 0)
                val = cell.Value;
        }
        public void OnBeforeChangeTxEmi(Cell cell, ref string val)
        {

        }

        public void OnBeforeChangeCertNo(Cell cell, ref string val)
        {

        }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            DateTime newVal;
            if (!DateTime.TryParse(val, out newVal))
                val = cell.Value;
        }

        public void OnBeforeChangeGrad(Cell cell, ref string val)
        {
            if (val.ToInt32(IM.NullI) == 360)
                val = "0";
        }

        public void OnBeforeChangeFreq(Cell cell, ref string val)
        {
            ChangeColor(cell, Colors.okvalue);
            List<double> freqList = ConvertType.ToDoubleList(val);

            if (freqList.Count > 0)
            {
                if (CheckValidityFrequency(freqList))
                    val = HelpFunction.ToString(freqList);
                else
                {
                    ChangeColor(cell, Colors.badValue);
                    val = HelpFunction.ToString(freqList);
                }
            }
            else
                val = cell.Value;
        }

        private bool CheckValidityFrequency(List<double> freqList)
        {
            foreach (double freq in freqList)
                if (freq <= 0.0)
                    return false;
            return true;
        }

        //Перевірка на наявність змін в Diff
        public static bool IsPresent(int objId, int packetID)
        {
            bool result1 = false;
            bool result2 = true;

            IMRecordset r = null;
            try
            {
                r = new IMRecordset(PlugTbl.itblXnrfaDiffMobSta2, IMRecordset.Mode.ReadWrite);
                r.Select("ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, objId);

                r.Open();
                if (!r.IsEOF())
                    result1 = true;
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            if (result1)
            {
                try
                {
                    r = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,STATUS");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, packetID);

                    r.Open();
                    if (!r.IsEOF())
                    {
                        string status = r.GetS("STATUS");
                        if (status == "SentToUrcp")
                            result2 = true;
                    }
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Dispose();
                }
            }
            return result1 && result2;
        }

        /// <summary>        
        /// Створення нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(mob2DiffStation.Position.CityId);
            frm.OldPosition = mob2DiffStation.Position;
            
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;
            frm.CanAttachPhoto = true;
            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);

            //frm.CanEditPosition = false;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                mob2DiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                frm.GetFotos(out ListFotosDst, out ListFotosSrc);
                if (!string.IsNullOrEmpty(mob2DiffStation.Position.FullAddress))
                    CellAddress.Value = mob2DiffStation.Position.FullAddress;
                CellASL.DoubleValue = mob2DiffStation.Position.ASl;
            }
        }

        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            int sectorCount = mob2StationList.GetSectorCount();
            for (int i = 0; i < 6; i++)
            {
                if (i >= sectorCount)
                {
                    FillCellUnvisible(arrayAzimuth[i].Cell);
                    FillCellUnvisible(arrayPlaceSite[i].Cell);
                    FillCellUnvisible(arrayTypeAnten[i].Cell);
                    FillCellUnvisible(arrayKoefGain[i].Cell);
                    FillCellUnvisible(arrayWdthAntena[i].Cell);
                    FillCellUnvisible(arrayPolar[i].Cell);
                    FillCellUnvisible(arrayNomAccept[i].Cell);
                    FillCellUnvisible(arrayNomSend[i].Cell);
                    FillCellUnvisible(arrayObjRchp[i].Cell);
                    FillCellUnvisible(arrayHighAnten[i].Cell);
                    FillCellUnvisible(arrayGeneral[i].Cell);
                    FillCellUnvisible(arrayOnOff[i].Cell);
                }
            }

        }
        /// <summary>
        /// Перетворення переданої комірки Гріда в невидиму
        /// </summary>
        /// <param name="cellObj">Комірка</param>
        private static void FillCellUnvisible(Cell cellObj)
        {
            cellObj.Visible = false;
            cellObj.TabOrder = 0;
        }

        //Формування масивів секторів...
        private void FillArraySectors()
        {
            arrayGeneral = new[]
                               {
                                   CellGeneral1,
                                   CellGeneral2,
                                   CellGeneral3,
                                   CellGeneral4,
                                   CellGeneral5,
                                   CellGeneral6     
                               };

            arrayAzimuth = new[]
                               {
                                   CellAglMaxEmission1,
                                   CellAglMaxEmission2,
                                   CellAglMaxEmission3,
                                   CellAglMaxEmission4,
                                   CellAglMaxEmission5,
                                   CellAglMaxEmission6
                               };
            arrayPlaceSite = new[]
                                 {
                                     CellAngleSiteMaxEmission1,
                                     CellAngleSiteMaxEmission2,
                                     CellAngleSiteMaxEmission3,
                                     CellAngleSiteMaxEmission4,
                                     CellAngleSiteMaxEmission5,
                                     CellAngleSiteMaxEmission6
                                 };

            arrayTypeAnten = new[]
                                 {
                                     CellAntennaType1,
                                     CellAntennaType2,
                                     CellAntennaType3,
                                     CellAntennaType4,
                                     CellAntennaType5,
                                     CellAntennaType6
                                 };
            arrayHighAnten = new[]
                                 {
                                     CellHighAntenna1,
                                     CellHighAntenna2,
                                     CellHighAntenna3,
                                     CellHighAntenna4,
                                     CellHighAntenna5,
                                     CellHighAntenna6
                                 };
            arrayKoefGain = new[]
                                {
                                    CellMaxKoef1,
                                    CellMaxKoef2,
                                    CellMaxKoef3,
                                    CellMaxKoef4,
                                    CellMaxKoef5,
                                    CellMaxKoef6
                                };
            arrayWdthAntena = new[]
                                  {
                                      CellWidthAntenna1,
                                      CellWidthAntenna2,
                                      CellWidthAntenna3,
                                      CellWidthAntenna4,
                                      CellWidthAntenna5,
                                      CellWidthAntenna6
                                  };
            arrayPolar = new[]
                             {
                                 CellAntenPolar1,
                                 CellAntenPolar2,
                                 CellAntenPolar3,
                                 CellAntenPolar4,
                                 CellAntenPolar5,
                                 CellAntenPolar6  
                             };
            arrayNomAccept = new[]
                                 {
                                     CellNominAccept1,
                                     CellNominAccept2,
                                     CellNominAccept3,
                                     CellNominAccept4,
                                     CellNominAccept5,
                                     CellNominAccept6
                                 };
            arrayNomSend = new[]
                               {
                                   CellNominSend1,
                                   CellNominSend2,
                                   CellNominSend3,
                                   CellNominSend4,
                                   CellNominSend5,
                                   CellNominSend6
                               };

            arrayOnOff = new[]
                             {
                                 CellOnOff1,
                                 CellOnOff2,
                                 CellOnOff3,
                                 CellOnOff4,
                                 CellOnOff5,
                                 CellOnOff6
                             };

            arrayObjRchp = new[]
                               {
                                   CellObjRCHP1,
                                   CellObjRCHP2,
                                   CellObjRCHP3,
                                   CellObjRCHP4,
                                   CellObjRCHP5,
                                   CellObjRCHP6
                               };
        }

        //Необхідні перегружені методи
        /// <summary>
        /// Ініціалізація параметрів гріда
        /// </summary>
        /// <param name="cell"></param>
        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.MobStation2AppDiff;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                AdminSiteAllTech.Show(mob2Station.Position.TableName, mob2Station.Position.Id);
            }
        }


        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {

            //Збереження функції
            mob2DiffStation.Id = recordID.Id;

            /*if (CellLongitude.DoubleValue != CellLongitude.DoubleValue2 ||
             CellLatitude.DoubleValue != CellLatitude.DoubleValue2 ||
             mob2DiffStation.Position.TableName != ICSMTbl.itblPositionMob2)
            {
                mob2DiffStation.Position.LonDms = CellLongitude.DoubleValue;
                mob2DiffStation.Position.LatDms = CellLatitude.DoubleValue;
                // Сброс id в IM.NullI заставит создать новую запись                
                mob2DiffStation.Position.Id = IM.NullI;
                mob2DiffStation.Position.TableName = PlugTbl.itblXnrfaPositions;
            }
            if (mob2DiffStation.Position.TableName == PlugTbl.itblXnrfaPositions)
                mob2DiffStation.Position.SaveToBD();*/

            mob2DiffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  mob2DiffStation.Position,
                  ICSMTbl.itblPositionMob2);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            mob2DiffStation.MaxPower = CellPowerTr.DoubleValue;
            mob2DiffStation.EquipmentName = CellEquipmentName.Value;
            mob2DiffStation.Modulation = mob2Station.ModulationList.IndexOf(CellModulations.Value).ToString();
            mob2DiffStation.Bandwith = CellTxBandwidth.DoubleValue;
            mob2DiffStation.ClassEmission = CellTxDesignEmission.Value;
            //Сертифікат (и)
            mob2DiffStation.mobDiffStationEquip.Certificate.Symbol = certificate.Symbol;
            mob2DiffStation.mobDiffStationEquip.Certificate.Date = certificate.Date;
            int sectorCount = mob2StationList.GetSectorCount();
            StationSectorCollection<MobStation2AppDiffStation> mob2StationDiffList = new StationSectorCollection<MobStation2AppDiffStation>();

            for (int i = 0; i < sectorCount; i++)
            {
                MobStation2AppDiffStation cloneDiff = mob2DiffStation.Clone() as MobStation2AppDiffStation;

                if (cloneDiff != null)
                {
                    cloneDiff.Id = mob2StationList.GetSectorByIndex(i).Id;

                    cloneDiff.AglSiteMax = arrayPlaceSite[i].DoubleValue;
                    cloneDiff.WideAnten = arrayWdthAntena[i].DoubleValue;
                    cloneDiff.NomReceive = arrayNomAccept[i].Value;
                    cloneDiff.NomSend = arrayNomSend[i].Value;
                    cloneDiff.HighAnten = arrayHighAnten[i].DoubleValue;
                    cloneDiff.AglMaxEmission = arrayAzimuth[i].DoubleValue;
                    cloneDiff.AntenType = arrayTypeAnten[i].Value;
                    cloneDiff.KoefAnten = arrayKoefGain[i].DoubleValue;
                    cloneDiff.PolarAnten = arrayPolar[i].Value.Split(' ')[0].Trim();
                    cloneDiff.OnOff = mob2Station.OnOffList.IndexOf(arrayOnOff[i].Value);
                    cloneDiff.StatComment = Comment;
                    mob2StationDiffList.Add(cloneDiff);
                }
            }
            mob2StationDiffList.ApplID = applID;
            mob2StationDiffList.SaveSectorsOrSingle(true);
            if (ListFotosDst != null && ListFotosSrc != null)
                mob2DiffStation.Position.SaveFoto(ListFotosDst, ListFotosSrc);//, MobStation2AppDiffStation.TableName, mob2Station.Id);
            return true;
        }
    }
}
