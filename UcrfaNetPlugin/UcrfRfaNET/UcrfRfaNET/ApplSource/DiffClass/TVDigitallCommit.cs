﻿using System;
using System.Collections.Generic;
using System.Linq;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class TvDigitallCommit : BaseCommitClass
    {
        private bool _initialized;
        public int Id { get; set; }

        private PositionState selectedPosition;

        private CoordinateCommitHelper coordHelper;

        private TvDigitallStation tvDgStation;
        private TvDigitallDiffStation tvDgDiffStation;
        #region CELL

        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі, м
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Номер контуру
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentContour = new CellStringComboboxAplyProperty();
        /// <summary>
        /// номер РЕЗ в синхронній мережі
        /// </summary> 
        private CellStringProperty CellEquipmentNameSynchr = new CellStringProperty();

        /// <summary>
        /// Назва виділення
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameSelect = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Назва ОЧМ
        /// </summary> 
        private CellStringProperty CellEquipmentNameOchm = new CellStringProperty();

        /// <summary>
        /// Варіант системи
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentNameVariant = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Кількість несівних
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentCountOstov = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Захисний інтервал
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSafeInterv = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Втрати у фільтрі 
        /// </summary> 
        private CellDoubleProperty CellEquipmentLoseFiltr = new CellDoubleProperty();
        /// <summary>
        /// Тип спектр. маски фільтра
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentSpectrMaskFiltr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Тип прийм
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentTypeReceive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Стандарт компресії
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentStandCompr = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Зміщення несівної частоти, Гц
        /// </summary> 
        private CellStringComboboxAplyProperty CellEquipmentShiftOstov = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача, Вт        
        /// </summary>
        private CellDoubleLookupProperty CellPowerTr = new CellDoubleLookupProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellStringBrowsableProperty CellTxBandwidth = new CellStringBrowsableProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringBrowsableProperty CellTxDesignEmission = new CellStringBrowsableProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateNumber = new CellStringBrowsableProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellStringBrowsableProperty CellCertificateDate = new CellStringBrowsableProperty();

        /// <summary>
        /// Довжина фідера, м
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері, дБ/м
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenDirect = new CellStringLookupEncodedProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringLookupEncodedProperty CellAntenPolar = new CellStringLookupEncodedProperty();

        /// <summary>
        /// Висота антени над землею, м
        /// </summary>
        private CellStringComboboxAplyProperty CellHighAntenn = new CellStringComboboxAplyProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellDoubleProperty CellMaxKoefG = new CellDoubleProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellDoubleProperty CellMaxKoefV = new CellDoubleProperty();

        /// <summary>
        /// ЕВП Г, дБВт 
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В, дБВт
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП Макс., дБВт
        /// </summary>
        private CellDoubleProperty CellEVPm = new CellDoubleProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellInnerState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellOuterState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// ТВ канал
        /// </summary>
        private CellStringComboboxAplyProperty CellTvChannel = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Центральна частота, МГц
        /// </summary>
        private CellStringComboboxAplyProperty CellMaxFreq = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public TvDigitallCommit(int id, int ownerId, int packetID, string radioTech)
            : base(id, CTvDigital.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTV2d;
            coordHelper = new CoordinateCommitHelper(CellLongitude, CellLatitude, CellAddress, CellASL);
            tvDgStation = new TvDigitallStation();
            tvDgStation.Id = recordID.Id;
            tvDgStation.Load();

            tvDgDiffStation = new TvDigitallDiffStation();
            tvDgDiffStation.Id = recordID.Id;
            tvDgDiffStation.Load();
        }

        private void ReadAllDefaultDataPositions()
        {
            CellEquipmentNameNumber.Value = tvDgStation.NameNumber;
            CellEquipmentNameREZ.Value = tvDgStation.NameRez;

            CellEquipmentContour.Items.AddRange(tvDgStation.ContourList);
            if (!tvDgStation.ContourList.Contains(tvDgStation.Countur))
                tvDgStation.Countur = tvDgStation.ContourList[0];
            CellEquipmentContour.Value = tvDgStation.ContourList[tvDgStation.ContourList.IndexOf(tvDgStation.Countur)];

            CellEquipmentNameSynchr.Value = tvDgStation.NameSynchr;

            CellEquipmentNameSelect.Items.AddRange(tvDgStation.NameSelectList);
            CellEquipmentNameSelect.Value = tvDgStation.NameSelect;
            CellEquipmentNameOchm.Value = tvDgStation.NameOchm;

            CellEquipmentTypeReceive.Value = tvDgStation.TypeReceive;

            foreach (KeyValuePair<string, string> s in tvDgStation.VariantSystDict)
                CellEquipmentNameVariant.Items.Add(s.Value);
            if (!tvDgStation.VariantSystDict.ContainsKey(tvDgStation.NameVariant))
                tvDgStation.NameVariant = tvDgStation.VariantSystDict.ToList()[0].Key;
            CellEquipmentNameVariant.Value = tvDgStation.VariantSystDict[tvDgStation.NameVariant];
            if (!tvDgStation.CountNesivList.Contains(tvDgStation.CountOstov))
                tvDgStation.CountOstov = tvDgStation.CountNesivList[0];
            CellEquipmentCountOstov.Value = tvDgStation.CountOstov;
            CellEquipmentCountOstov.Items.AddRange(tvDgStation.CountNesivList);
            if (!tvDgStation.SafeIntervDict.ContainsKey(tvDgStation.SafeInterv.ToString()))
                tvDgStation.SafeInterv = Convert.ToInt32(tvDgStation.SafeIntervDict.ToList()[0].Key);
            CellEquipmentSafeInterv.Value = tvDgStation.SafeIntervDict[tvDgStation.SafeInterv.ToString()];
            foreach (KeyValuePair<string, string> s in tvDgStation.SafeIntervDict)
                CellEquipmentSafeInterv.Items.Add(s.Value);

            CellEquipmentLoseFiltr.DoubleValue = tvDgStation.LoseFiltr;
            if (!string.IsNullOrEmpty(tvDgStation.SpectrMaskFiltr))
                CellEquipmentSpectrMaskFiltr.Value = tvDgStation.SpectrMaskFiltr;
            CellEquipmentSpectrMaskFiltr.Items.AddRange(tvDgStation.SpectrMaskList);

            CellEquipmentShiftOstov.Value = tvDgStation.ShiftOstov.ToStringNullD();
            CellEquipmentShiftOstov.Items.Add(tvDgStation.ShiftOstov.ToStringNullD());

            CellEquipmentName.Value = tvDgStation.TvDgEquipment.Name;

            CellEquipmentStandCompr.Value = tvDgStation.StandCompr;

            CellFiderLength.DoubleValue = tvDgStation.FeederLength;
            CellFiderlost.DoubleValue = tvDgStation.FeederLosses;

            if (tvDgStation.HeightAnten != IM.NullD)
            {
                CellHighAntenn.Value = tvDgStation.HeightAnten.ToStringNullD();
                CellHighAntenn.Items.Add(tvDgStation.HeightAnten.ToStringNullD());
            }

            CellMaxHigh.DoubleValue = tvDgStation.MaxHigh;
            if (!string.IsNullOrEmpty(tvDgStation.InnerState))
                CellInnerState.Value = tvDgStation.InnerState + " - " + tvDgStation.InnerStateDict[tvDgStation.InnerState];
            foreach (KeyValuePair<string, string> s in tvDgStation.InnerStateDict)
                CellInnerState.Items.Add(s.Key + " - " + s.Value);
            if (!string.IsNullOrEmpty(tvDgStation.OuterState))
                CellOuterState.Value = tvDgStation.OuterState + " - " + tvDgStation.OuterStateDict[tvDgStation.OuterState];
            foreach (KeyValuePair<string, string> s in tvDgStation.OuterStateDict)
                CellOuterState.Items.Add(s.Key + " - " + s.Value);
            if (!string.IsNullOrEmpty(tvDgStation.TvChannel))
            {
                CellTvChannel.Value = tvDgStation.TvChannel;
                CellTvChannel.Items.Add(tvDgStation.TvChannel);
            }
            if (tvDgStation.FreqCentr != IM.NullD)
            {
                CellMaxFreq.Value = tvDgStation.FreqCentr.ToStringNullD();
                CellMaxFreq.Items.Add(tvDgStation.FreqCentr.ToStringNullD());
            }
            CellObjRCHP.IntValue = tvDgStation.ObjRCHP;
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання(які не можуть бути вибраними)
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, (tvDgStation.TvDgEquipment.Bandwidth).ToStringNullD(), (tvDgDiffStation.TvDgDiffEquipment.Bandwidth).ToStringNullD());
            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, tvDgStation.TvDgEquipment.DesigEmission, tvDgDiffStation.TvDgDiffEquipment.DesigEmission);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, tvDgStation.TvDgEquipment.Certificate.Symbol, tvDgDiffStation.TvDgDiffEquipment.Certificate.Symbol);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, tvDgStation.TvDgEquipment.Certificate.Date.ToShortDateString(), tvDgDiffStation.TvDgDiffEquipment.Certificate.Date.ToShortDateString());
        }

        public override void InitParamGrid(Grid grid)
        {
            gridParam = grid;

            ReadAllDefaultDataPositions();
            if (tvDgDiffStation.IsEnableDiff)
            {
                FillDiffComboBox(CellEquipmentShiftOstov, tvDgStation.ShiftOstov.ToStringNullD(), tvDgDiffStation.ShiftOstov.ToStringNullD());                

                FillDiffComboBox(CellHighAntenn, tvDgStation.HeightAnten.Round(3).ToStringNullD(), tvDgDiffStation.HeightAnten.Round(3).ToStringNullD());

                FillDiffComboBox(CellTvChannel, tvDgStation.TvChannel, tvDgDiffStation.TvChannel);

                FillDiffComboBox(CellMaxFreq, tvDgStation.FreqCentr.Round(3).ToStringNullD(), tvDgDiffStation.FreqCentr.Round(3).ToStringNullD());
            }

            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenPolar, tvDgStation.Polarization, tvDgDiffStation.Polar);
            CellStringLookupEncodedProperty.InitalizeHelper(CellAntenDirect, tvDgStation.Direction, tvDgDiffStation.Direct);
            CellPowerTr.Initialize(tvDgStation.Power[PowerUnits.W].Round(3), tvDgDiffStation.TvDgDiffEquipment.MaxPower[PowerUnits.W].Round(3));

            Comment = tvDgDiffStation.StatComment;
            coordHelper.Initialize(tvDgDiffStation.Position, tvDgStation.Position);

            CellStringBrowsableProperty.InitalizeHelper(CellTxBandwidth, (tvDgStation.TvDgEquipment.Bandwidth).ToStringNullD(), (tvDgDiffStation.TvDgDiffEquipment.Bandwidth).ToStringNullD());
            CellStringBrowsableProperty.InitalizeHelper(CellTxDesignEmission, tvDgStation.TvDgEquipment.DesigEmission, tvDgDiffStation.TvDgDiffEquipment.DesigEmission);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateNumber, tvDgStation.TvDgEquipment.Certificate.Symbol, tvDgDiffStation.TvDgDiffEquipment.Certificate.Symbol);
            CellStringBrowsableProperty.InitalizeHelper(CellCertificateDate, tvDgStation.TvDgEquipment.Certificate.Date.ToShortDateString(), tvDgDiffStation.TvDgDiffEquipment.Certificate.Date.ToShortDateString());
            
            _initialized = true;
            OnRecalculateErp(null);
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        public void OnPressButtonEquipmentName(Cell cell)
        {
            RecordPtr newEquip = LookupEquipment(cell.Value, true);
            if ((newEquip.Id > 0) && (newEquip.Id < IM.NullI))
            {
                tvDgStation.TvDgEquipment.TableName = newEquip.Table;
                tvDgStation.TvDgEquipment.Id = newEquip.Id;
                tvDgStation.TvDgEquipment.Load();

                InitEquipment();

                CellPowerTr.Initialize(tvDgStation.Power[PowerUnits.W].Round(3), tvDgDiffStation.TvDgDiffEquipment.MaxPower[PowerUnits.W].Round(3));

                PaintGrid();
            }
        }

        public void PaintGrid()
        {
            coordHelper.Paint(tvDgDiffStation.Position, tvDgStation.Position);
        }

        /// <summary>
        /// Lookup Equipment
        /// </summary>
        /// <param name="initialValue">search template, usually name of equipment</param>
        /// <param name="bReplaceQuota">will replace quota sumbols</param>
        /// <returns></returns>
        public RecordPtr LookupEquipment(string initialValue, bool bReplaceQuota)
        {
            string Criteria = bReplaceQuota ? HelpFunction.ReplaceQuotaSumbols(initialValue) : initialValue;
            string param = "{NAME=\"*" + Criteria + "*\"}";

            RecordPtr RecEquip = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the equipment"), ICSMTbl.itblEquipBro, param);
            return RecEquip;
        }

        private void InitEquipment()
        {
            CreateEquipmentComboBoxes();

            CellEquipmentName.Value = tvDgStation.TvDgEquipment.Name;
            CellTxBandwidth.Value = tvDgStation.TvDgEquipment.Bandwidth.ToStringNullD();
            CellTxDesignEmission.Value = tvDgStation.TvDgEquipment.DesigEmission;

            CellCertificateNumber.Value = tvDgStation.TvDgEquipment.Certificate.Symbol;
            CellCertificateDate.Value = tvDgStation.TvDgEquipment.Certificate.Date.ToShortDateString();
        }

        public void OnBeforeChangeAddr(Cell cell, ref string newValue)
        {
            CellAddress.Value2 = newValue;
            coordHelper.Paint(tvDgStation.Position, tvDgDiffStation.Position);
        }

        //////////////////////////////////////////
        //Необхідні перевантажені методи
        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                RecordPtr rcPtr = new RecordPtr(tvDgStation.Position.TableName, tvDgStation.Position.Id);
                rcPtr.UserEdit();
            }
        }
        /// <summary>
        /// Реакція на зберігання всіх даних        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            string[] assgnReferenceTables = { "DVBT_STATION" };
            PositionState diffPos = tvDgDiffStation.Position.Clone() as PositionState;
            AttachmentControl ac = new AttachmentControl();

            selectedPosition = coordHelper.GetSelectedPosition(tvDgStation.Position, tvDgDiffStation.Position);
            if (selectedPosition != tvDgStation.Position)
                SaveNewPosition(selectedPosition, tvDgStation.Position, assgnReferenceTables);
            tvDgStation.Position = selectedPosition;
            tvDgDiffStation.Id = recordID.Id;
            tvDgStation.ShiftOstov = CellEquipmentShiftOstov.Value.ToDouble(IM.NullD);
            tvDgStation.TvDgEquipment.Name = CellEquipmentName.Value;
            tvDgStation.Polarization = CellAntenPolar.Value;
            tvDgStation.Direction = CellAntenDirect.Value;
            tvDgStation.HeightAnten = CellHighAntenn.Value.ToDouble(IM.NullD);
            tvDgStation.TvChannel = CellTvChannel.Value;
            tvDgStation.FreqCentr = CellMaxFreq.Value.ToDouble(IM.NullD);
            tvDgStation.ApplId = ApplID;
            tvDgStation.StatComment = tvDgDiffStation.StatComment;
            tvDgStation.Save();

            tvDgDiffStation.Remove();

            ac.RelocateAndCopyDocLinks(
               diffPos.TableName,
               diffPos.Id,
               CTvDigital.TableName,
               tvDgStation.Id,
               tvDgStation.Position.TableName,
               tvDgStation.Position.Id,
               ICSMTbl.itblAdmSite,
               tvDgStation.Position.AdminSitesId);

            return true;
        }

        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.TvDigitallCommit;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (_initialized)
            {
                tvDgStation.Power[PowerUnits.W] = CellPowerTr.LookupedDoubleValue;
                tvDgStation.Polarization = CellAntenPolar.Value;
                tvDgStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!tvDgStation.HorizontalDiagramm.IsEmpty && tvDgStation.Polarization != "V")
                value = tvDgStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!tvDgStation.VerticalDiagramm.IsEmpty && tvDgStation.Polarization != "H")
                value = tvDgStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            tvDgStation.CalculateERP();
            if (tvDgStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = tvDgStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (tvDgStation.Erp.Vertical.IsValid)
                CellEVPV.Value = tvDgStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = tvDgStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}
