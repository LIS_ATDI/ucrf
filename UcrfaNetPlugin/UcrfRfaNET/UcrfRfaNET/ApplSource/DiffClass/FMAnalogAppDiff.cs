﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class FmAnalogAppDiff : BaseDiffClass
    {
        private bool initialized;

        private FmAnalogStation fmStation;
        private FmAnalogDiffStation fmDiffStation;

        //private int IDPos = IM.NullI;
        # region Cell
        /// <summary>
        /// Широта (гр.хв.сек.)
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// // Довгота (гр.хв.сек.)
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary> 
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Система передачі                   
        /// </summary>
        private CellStringComboboxAplyProperty CellGive = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача        
        /// </summary>
        private CellDoubleProperty CellPowerTr = new CellDoubleProperty();

        /// <summary>
        /// Ширина смуги , МГц; 
        /// </summary> 
        private CellDoubleProperty CellTxBandwidth = new CellDoubleProperty();
        /// <summary>
        /// клас випромінювання               
        /// </summary>        
        private CellStringProperty CellTxDesignEmission = new CellStringProperty();

        /// <summary>
        /// Номер сертифіката відповідності
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Дата сертифіката відповідності
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Довжина фідера
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Направленість антени
        /// </summary>
        private CellStringProperty CellAntenDirect = new CellStringProperty();
        /// <summary>
        /// Поляризація антени          
        /// </summary>
        private CellStringProperty CellAntenPolar = new CellStringProperty();

        /// <summary>
        /// Висота антени
        /// </summary>
        private CellDoubleProperty CellHighAntenn = new CellDoubleProperty();
        /// <summary>
        /// макс. ефективна висота         
        /// </summary>
        private CellDoubleProperty CellMaxHigh = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellStringProperty CellMaxKoefG = new CellStringProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>        
        private CellStringProperty CellMaxKoefV = new CellStringProperty();

        /// <summary>
        /// ЕВП Г
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();
        /// <summary>
        /// ЕВП Макс
        /// </summary>
        private CellDoubleProperty CellEVPm = new CellDoubleProperty();

        /// <summary>
        /// Позивний сигнал
        /// </summary>
        private CellStringProperty CellPositiv = new CellStringProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellInnerState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellOuterState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Центральна частота
        /// </summary>
        private CellDoubleProperty CellMaxFreq = new CellDoubleProperty();

        /// <summary>
        /// Об'єкт(и) РЧП
        /// </summary>
        private CellIntegerProperty CellObjRCHP = new CellIntegerProperty();
        #endregion

        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ownerId"></param>
        /// <param name="packetID"></param>
        /// <param name="radioTech"></param>
        public FmAnalogAppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, FmAnalogApp.TableName, ownerId, packetID, radioTech)
        {
            appType = AppType.AppR2;
            //Стандартные значения
            fmStation = new FmAnalogStation();
            fmStation.Id = recordID.Id;
            fmStation.Load();

            SCVisnovok = fmStation.Finding;
            //URCP значения
            fmDiffStation = new FmAnalogDiffStation();
            fmDiffStation.Id = recordID.Id;
            fmDiffStation.Load();

            CellCertificateDate.NullString = "-";
            _stationObject = fmStation;
        }


        /// <summary>
        /// Створення Комбо боксів для обладнання
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellPowerTr.FormatString = "N3";
            CellPowerTr.DoubleValue2 = fmStation.Power[PowerUnits.W].Round(3);//.Round(3)4;
            CellPowerTr.DoubleValue = fmDiffStation.Equipment.MaxPower[PowerUnits.W].Round(3);//.Round(3);
            CellPowerTr.SetRedValue();

            CellTxBandwidth.DoubleValue2 = fmStation.Bandwith.DivNullD(1000.0);
            CellTxBandwidth.DoubleValue = fmDiffStation.Bandwith.DivNullD(1000.0);
            CellTxBandwidth.SetRedValue();

            CellTxDesignEmission.Value2 = FillValue2(fmStation.ClassEmission);
            CellTxDesignEmission.Value = fmDiffStation.ClassEmission;

            CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, fmDiffStation.Equipment.Certificate);

            CellCertificateDate.SetRedValue();
        }

        /// <summary>
        /// Считывание данных формы при формировании Позиции
        /// </summary>
        private void ReadAllDataPositions()
        {
            CellLongitude.DoubleValue2 = fmStation.Position.LonDms;
            CellLatitude.DoubleValue2 = fmStation.Position.LatDms;
            CellAddress.Value2 = FillValue2(fmStation.Position.FullAddress);
            CellPowerTr.Value2 = FillValue2(fmStation.Power[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerTr.SetRedValue();

            CellTxBandwidth.DoubleValue = fmStation.Bandwith.DivNullD(1000.0);
            CellTxBandwidth.DoubleValue2 = fmStation.Bandwith.DivNullD(1000.0);
            CellTxBandwidth.SetRedValue();

            CellTxDesignEmission.Value2 = FillValue2(fmStation.ClassEmission);

            CellEquipmentName.Value2 = FillValue2(fmStation.Equipment.Name);

            if (!string.IsNullOrEmpty(fmStation.Equipment.Certificate.Symbol))
                CellCertificateNumber.Value = fmStation.Equipment.Certificate.Symbol;
            CellCertificateNumber.Value2 = FillValue2(fmStation.Equipment.Certificate.Symbol);
            CellCertificateNumber.SetRedValue();

            CellCertificateDate.DateValue = fmStation.Equipment.Certificate.Date;
            CellCertificateDate.DateValue2 = fmStation.Equipment.Certificate.Date;
            CellCertificateDate.SetRedValue();

            CellLongitude.DoubleValue = fmStation.Position.LonDms;
            CellLatitude.DoubleValue = fmStation.Position.LatDms;
            CellAddress.Value = fmStation.Position.FullAddress;
            CellPowerTr.Value = fmStation.Power[PowerUnits.W].Round(3).ToStringNullD();
            CellTxBandwidth.DoubleValue = fmStation.Bandwith.DivNullD(1000.0);
            CellTxDesignEmission.Value = fmStation.Equipment.DesigEmission;
            CellEquipmentName.Value = fmStation.Equipment.Name;
            CellPositiv.Value2 = FillValue2(fmStation.PositivSignal);
            CellPositiv.Value = fmStation.PositivSignal;

            foreach (KeyValuePair<string, string> s in fmStation.InternalStateDict)
                CellInnerState.Items.Add(s.Key + " - " + s.Value);

            foreach (KeyValuePair<string, string> s in fmStation.ForeignStateDict)
                CellOuterState.Items.Add(s.Key + " - " + s.Value);

            if (fmStation.InternalStateDict.ContainsKey(fmStation.InnerState))
                CellInnerState.Value = fmStation.InnerState + " - " + fmStation.InternalStateDict[fmStation.InnerState];
            else
                CellInnerState.Value = CellInnerState.Items[0];

            if (fmStation.ForeignStateDict.ContainsKey(fmStation.OuterState))
                CellOuterState.Value = fmStation.OuterState + " - " + fmStation.ForeignStateDict[fmStation.OuterState];
            else
                CellOuterState.Value = CellOuterState.Items[0];

            CellMaxFreq.DoubleValue = fmStation.CenterFreq;
            CellObjRCHP.IntValue = fmStation.Id;

            CellHighAntenn.Value2 = FillValue2(fmStation.AGL.ToStringNullD());
            CellHighAntenn.DoubleValue = CellHighAntenn.DoubleValue2;
            CellMaxHigh.DoubleValue = fmStation.HeightMax;
            CellFiderLength.DoubleValue = fmStation.FeederLength;
            CellFiderlost.DoubleValue = fmStation.FeederLosses;
            CellASL.DoubleValue = fmStation.Position.ASl;
            CellEquipmentNameNumber.Value = fmStation.NumberRez;
            CellEquipmentNameREZ.Value = fmStation.NameRez;
            CellGive.Value = fmStation.SystSendList[0];

            CellAntenDirect.Value2 = FillValue2(fmStation.Direction);
            CellAntenPolar.Value2 = FillValue2(fmStation.Polarization);
            CellAntenDirect.Value = CellAntenDirect.Value2;
            CellAntenPolar.Value = CellAntenPolar.Value2;

            CellMaxFreq.Value2 = FillValue2(fmStation.CenterFreq.ToStringNullD());

            CellMaxKoefG.Value2 = FillValue2(fmStation.HorizontalDiagramm.Max.ToStringNullD());
            CellMaxKoefV.Value2 = FillValue2(fmStation.VerticalDiagramm.Max.ToStringNullD());
            CellMaxKoefG.Value = fmStation.HorizontalDiagramm.Max.ToStringNullD();
            CellMaxKoefV.Value = fmStation.VerticalDiagramm.Max.ToStringNullD();
        }

        public void OnBeforeChangeSelect(Cell cell, string newVal)
        {
            if (cell.Key == "NewKey-AntPol")
                fmStation.Polarization = newVal.Split(' ')[0].Trim();

            CellMaxKoefG.Value2 = FillValue2(fmStation.HorizontalDiagramm.Max.ToStringNullD());
            CellMaxKoefV.Value2 = FillValue2(fmStation.VerticalDiagramm.Max.ToStringNullD());
            CellMaxKoefG.Value = fmStation.HorizontalDiagramm.Max.ToStringNullD();
            CellMaxKoefV.Value = fmStation.VerticalDiagramm.Max.ToStringNullD();
            OnPressButtonCellMaxKoefG(null);
            OnPressButtonCellMaxKoefV(null);
            OnPressButtonEvpg(null);
            OnPressButtonEvpv(null);
            CellEVPm.Value = fmStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }

        /// <summary>
        /// Реакция на нажатия кнопки оформления позиции
        /// Формирование адреса сайта
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");

            // Выбираем запись из таблицы                     
            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionBro, 1, tmpXy.Longitude, tmpXy.Latitude);
            if (objPosition != null)
            {
                fmDiffStation.Position = new PositionState();
                fmDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                CellLongitude.DoubleValue = fmDiffStation.Position.LongDms;
                CellLatitude.DoubleValue = fmDiffStation.Position.LatDms;
                CellASL.DoubleValue = fmDiffStation.Position.ASl;
                if (!string.IsNullOrEmpty(fmDiffStation.Position.FullAddress))
                    CellAddress.Value = fmDiffStation.Position.FullAddress;


                // Форма Адреса місця встановлення РЕЗ
                FormNewPosition frm = new FormNewPosition(fmDiffStation.Position.CityId);

                frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
                frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;
                frm.OldPosition = fmDiffStation.Position;

                if (frm.ShowDialog() == DialogResult.OK)
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
            }
        }

        public void OnBeforeChangeMaxKoefV(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellMaxKoefV.Value))
                CellMaxKoefV.Value = "не застосовується";
        }

        public void OnBeforeChangeMaxKoefG(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellMaxKoefG.Value))
                CellMaxKoefG.Value = "не застосовується";
        }

        public void OnBeforeChangePowerTr(Cell cell, ref string val) { }

        public void OnBeforeChangeDesignEmission(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellTxDesignEmission.Value))
                CellTxDesignEmission.Value = "";
        }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            DateTime newVal;
            if (!DateTime.TryParse(val, out newVal) || newVal < DateTime.MinValue)
                val = cell.Value;
        }

        public void OnBeforeChangeCertificateNumber(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellCertificateNumber.Value))
                CellCertificateNumber.Value = "";
        }

        public void OnBeforeChangeEquipment(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellEquipmentName.Value))
                CellEquipmentName.Value = "*";
        }

        /// <summary>
        /// Реакция на нажатие кнопки создания адреса
        /// Создание нового сайта 
        /// </summary>
        /// <param name="cell"></param>
        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(fmDiffStation.Position.CityId);
            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanAttachPhoto = true;
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;
            frm.OldPosition = fmDiffStation.Position;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                fmDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                frm.GetFotos(out listFotosDst, out listFotosSrc);
                if (!string.IsNullOrEmpty(fmDiffStation.Position.FullAddress))
                    CellAddress.Value = fmDiffStation.Position.FullAddress;
                CellASL.DoubleValue = fmDiffStation.Position.ASl;
            }
        }
        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "KAddr")
            {
                AdminSiteAllTech.Show(fmStation.Position.TableName, fmStation.Position.Id);
            }
        }

        /// <summary>
        /// Реакция на сохранение всех данных        
        /// </summary>
        /// <returns></returns>
        protected override bool SaveAppl()
        {
            fmDiffStation.Id = recordID.Id;

            fmDiffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  fmDiffStation.Position,
                  ICSMTbl.itblPositionBro);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            fmDiffStation.Equipment.MaxPower[PowerUnits.W] = CellPowerTr.DoubleValue.ToStringNullD().Trim().ToDouble(IM.NullD);
            fmDiffStation.Bandwith = CellTxBandwidth.DoubleValue.MulNullD(1000.0);
            fmDiffStation.ClassEmission = CellTxDesignEmission.Value.Trim();
            //Сертифікат (и)
            fmDiffStation.Equipment.Name = CellEquipmentName.Value;
            fmDiffStation.Equipment.Certificate.Symbol = certificate.Symbol.Trim();
            fmDiffStation.Equipment.Certificate.Date = certificate.Date;
            //Антена
            fmDiffStation.Direction = CellAntenDirect.Value;//.Split(' ')[0].Trim();
            fmDiffStation.Polarization = CellAntenPolar.Value;//.Split(' ')[0].Trim();
            fmDiffStation.CenterFreq = CellMaxFreq.DoubleValue;
            fmDiffStation.Positiv = CellPositiv.Value;

            fmDiffStation.AGL = CellHighAntenn.DoubleValue;
            fmDiffStation.StatComment = Comment;
            fmDiffStation.Save();
            if (listFotosDst != null && listFotosSrc != null)
                fmDiffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, FmAnalogDiffStation.TableName, fmStation.Id);

            return true;
        }
        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid)
        {
            InitStationObject();
            fmDiffStation.Position = fmStation.Position.Clone() as PositionState;
            OnRecalculateErp(null);
        }



        private void InitStationObject()
        {
            ReadAllDataPositions();
            //???
            if (fmDiffStation.Position == null)
                fmDiffStation.Position = fmStation.Position;

            if (fmDiffStation.IsEnableDiff)
            {
                CellAddress.Value = fmDiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = fmDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = fmDiffStation.Position.LatDms;

                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, fmDiffStation.Equipment.Certificate);

                CellEquipmentName.Value = fmDiffStation.Equipment.Name;
                CellPowerTr.Value = fmDiffStation.Equipment.MaxPower[PowerUnits.W].Round(3).ToStringNullD();
                CellTxBandwidth.DoubleValue = fmDiffStation.Bandwith.DivNullD(1000.0);
                CellTxDesignEmission.Value = fmDiffStation.ClassEmission;

                CellPositiv.Value = fmDiffStation.Positiv;

                CellAntenDirect.Value = fmDiffStation.Direction;
                CellAntenPolar.Value = fmDiffStation.Polarization;

                CellMaxFreq.DoubleValue = fmDiffStation.CenterFreq;
                CellHighAntenn.DoubleValue = fmDiffStation.AGL;
                Comment = fmDiffStation.StatComment;
                CreateEquipmentComboBoxes();
            } else
            {
                fmDiffStation.Position = fmStation.Position.Clone() as PositionState;
            }

            initialized = true;
        }

        /// <summary>
        /// Инициализация параметров грида
        /// </summary>
        /// <param name="cell"></param>
        public override void InitParamGrid(Cell cell) { }
        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.FmAnalogAppDiff;
        }

        public override void UpdateToNewPacket() { }
        protected override string getOutDocName(string docType, string number) { return ""; }
        public void OnRecalculateErp(Cell cell)
        {
            if (initialized)
            {
                fmStation.Power[PowerUnits.W] = CellPowerTr.DoubleValue;
                fmStation.Polarization = CellAntenPolar.Value;
                fmStation.Direction = CellAntenDirect.Value;

                FillGainAndErpCells();
            }
        }
        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!fmStation.HorizontalDiagramm.IsEmpty && fmStation.Polarization != "V")
                value = fmStation.HorizontalDiagramm.Max.ToStringNullD();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!fmStation.VerticalDiagramm.IsEmpty && fmStation.Polarization != "H")
                value = fmStation.VerticalDiagramm.Max.ToStringNullD();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            fmStation.CalculateERP();
            if (fmStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = fmStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (fmStation.Erp.Vertical.IsValid)
                CellEVPV.Value = fmStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = fmStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
        public void OnPressButtonCellMaxKoefG(Cell cell) { }
        public void OnPressButtonCellMaxKoefV(Cell cell) { }
        public void OnPressButtonEvpv(Cell cell) { }
        public void OnPressButtonEvpg(Cell cell) { }
        public void OnAfterChangeFreq(Cell cell)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
            r.Select("ID,COD,LOWER_FREQ,UPPER_FREQ");
            int planID = objStation.GetI("PLAN_ID");
            double lowerFreq = 0.0;
            double upperFreq = 0.0;

            r.SetWhere("ID", IMRecordset.Operation.Eq, planID);
            r.Open();
            if (!r.IsEOF())
            {
                lowerFreq = r.GetD("LOWER_FREQ");
                upperFreq = r.GetD("UPPER_FREQ");
            }
            if (r.IsOpen())
                r.Close();
            r.Destroy();

            double enteredFreq = cell.Value.ToDouble(IM.NullD);

            if (lowerFreq <= enteredFreq && upperFreq >= enteredFreq)
                objStation.Put("FREQ", enteredFreq);
            else
            {
                MessageBox.Show(cell.Value + " не вписується в частотний план ( " +
                                (lowerFreq).ToString("F3") + " - " + (upperFreq).ToString("F3") + " )");
                //double currentFreq = objStation.GetD("FREQ");
                //cell.Value = currentFreq.ToString("F3");
            }
        }
    }

}
