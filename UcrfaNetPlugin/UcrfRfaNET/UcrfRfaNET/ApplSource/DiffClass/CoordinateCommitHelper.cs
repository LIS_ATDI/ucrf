﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    internal class CoordinateCommitHelper
    {
        private CellLongitudeProperty CellLongitude;
        private CellLatitudeProperty CellLatitude;
        private CellStringProperty CellAddress;
        private CellDoubleProperty CellASL;


        public CoordinateCommitHelper(CellLongitudeProperty cellLongitude,
           CellLatitudeProperty cellLatitude,
           CellStringProperty cellAddress)
        {
            CellLongitude = cellLongitude;
            CellLatitude = cellLatitude;
            CellAddress = cellAddress;
            CellASL = null;
        }

        public CoordinateCommitHelper(CellLongitudeProperty cellLongitude,
           CellLatitudeProperty cellLatitude,
           CellStringProperty cellAddress,
           CellDoubleProperty cellASL)
        {
            CellLongitude = cellLongitude;
            CellLatitude = cellLatitude;
            CellAddress = cellAddress;
            CellASL = cellASL;
        }     

        public void Initialize(PositionState position1, PositionState position2)
        {
            CellLongitude.DoubleValue = position1.LonDms;
            CellLatitude.DoubleValue = position1.LatDms;
            CellAddress.Value = position1.FullAddress;

            CellLongitude.DoubleValue2 = position2.LonDms;
            CellLatitude.DoubleValue2 = position2.LatDms;

            if (position1.Id != position2.Id ||
                position1.TableName != position2.TableName)
            {
                CellAddress.Cell.cellStyle = EditStyle.esPickList;
                CellAddress.Cell.KeyPickList = new KeyedPickList();
                if (position1.FullAddress != position2.FullAddress)
                {
                    CellAddress.Cell.KeyPickList.AddToPickList("1", position2.FullAddress);
                    CellAddress.Cell.KeyPickList.AddToPickList("2", position1.FullAddress);
                }
                else
                {
                    CellAddress.Cell.KeyPickList.AddToPickList("1", position2.FullAddress);
                    CellAddress.Cell.KeyPickList.AddToPickList("2", position1.FullAddress + " ");
                }
                CellAddress.Value = "1";
                CellAddress.Value2 = "1";
            }

            Paint(position1, position2);
        }


        public void Paint(PositionState position1, PositionState position2)
        {
            if (position1.Id != position2.Id || position1.TableName != position2.TableName)
            {
                //encoded value
                if (CellAddress.Value2 == "2")
                {
                    //УРЗП
                    CellLongitude.TextColor2 = CellStringProperty.ColorText(2);
                    CellLatitude.TextColor2 = CellStringProperty.ColorText(2);
                    CellAddress.BackColor = CellStringProperty.ColorBgr(2);
                    if (CellASL != null)
                        CellASL.DoubleValue = position2.ASl;
                }
                else
                {
                    //По висновку
                    CellLongitude.TextColor2 = CellStringProperty.ColorText(0);
                    CellLatitude.TextColor2 = CellStringProperty.ColorText(0);
                    CellAddress.BackColor = CellStringProperty.ColorBgr(0);

                    if (CellASL != null)
                        CellASL.DoubleValue = position1.ASl;
                }
            }          
        }
       
        /// <summary>
        /// Вызвращает, какая позиция была выбрана
        /// </summary>
        /// <param name="position1">"УРЧП-шная" позиция</param>
        /// <param name="position2">"УРЗП-шная" позиция/с акта ПТК</param>
        /// <returns></returns>
        public PositionState GetSelectedPosition(PositionState position1, PositionState position2)
        {
            if (position1.Id != position2.Id ||
                position1.TableName != position2.TableName)
            {
                if (CellAddress.Value == "1")
                    return position1;
                else
                    return position2;
            }
            else
            {
                return position1;
            }
        }
    }
}
