﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GridCtrl;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.ApplSource.DiffClasses
{
    class CTvAnalogAppDiff : BaseDiffClass
    {
        private bool _initialized;
        private TvAnalogStation tvStation;
        private TvAnalogDiffStation tvDiffStation;

        #region Cell
        /// <summary>
        /// Широта
        /// </summary>
        private CellLongitudeProperty CellLongitude = new CellLongitudeProperty();

        /// <summary>
        /// Довгота
        /// </summary>
        private CellLatitudeProperty CellLatitude = new CellLatitudeProperty();

        /// <summary>
        /// Висота поверхні землі
        /// </summary>
        private CellDoubleProperty CellASL = new CellDoubleProperty();

        /// <summary>
        /// Адреса місця встановлення РЕЗ
        /// </summary>
        private CellStringProperty CellAddress = new CellStringProperty();

        /// <summary>
        /// Номер РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentNameNumber = new CellStringProperty();
        /// <summary>
        /// Назва опори
        /// </summary>
        private CellStringProperty CellEquipmentNameREZ = new CellStringProperty();

        /// <summary>
        /// Стандарт
        /// </summary>
        private CellStringComboboxAplyProperty CellStandart = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Система
        /// </summary>
        private CellStringComboboxAplyProperty CellSystem = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Зміщення несівной частоти, кГц
        /// </summary>
        private CellStringComboboxAplyProperty CellEquipmentShiftOstov = new CellStringComboboxAplyProperty();

        /// <summary>
        /// Назва/Тип РЕЗ
        /// </summary>
        private CellStringProperty CellEquipmentName = new CellStringProperty();

        /// <summary>
        /// Потужність передавача,відео
        /// </summary>        
        private CellDoubleProperty CellPowerVideo = new CellDoubleProperty();
        /// <summary>
        /// Потужність передавача,аудіо
        /// </summary>
        private CellDoubleProperty CellPowerAudio = new CellDoubleProperty();

        /// <summary>
        /// Ширина смуги випромінювання,МГц
        /// </summary>
        private CellDoubleBoundedProperty CellTxBandwidth = new CellDoubleBoundedProperty();

        /// <summary>
        /// Клас випромінювання,відео
        /// </summary>
        private CellStringProperty CellEmiVideo = new CellStringProperty();
        /// <summary>
        /// Клас випромінювання,звук
        /// </summary>
        private CellStringProperty CellEmiAudio = new CellStringProperty();

        /// <summary>
        /// Сертифікат відповідності номер 
        /// </summary>
        private CellStringProperty CellCertificateNumber = new CellStringProperty();
        /// <summary>
        /// Сертифікат відповідності Дата 
        /// </summary>
        private CellDateNullableProperty CellCertificateDate = new CellDateNullableProperty();

        /// <summary>
        /// Довжина фідера,м
        /// </summary>
        private CellDoubleProperty CellFiderLength = new CellDoubleProperty();
        /// <summary>
        /// Втрати у фідері, дБ/м
        /// </summary>
        private CellDoubleProperty CellFiderlost = new CellDoubleProperty();

        /// <summary>
        /// Спрямованість антени
        /// </summary>
        private CellStringProperty CellAntennaDir = new CellStringProperty();
        /// <summary>
        ///  Поляризація антени
        /// </summary>
        private CellStringProperty CellAntennaPolar = new CellStringProperty();

        /// <summary>
        /// Висота антени над землею
        /// </summary>
        private CellDoubleProperty CellAntennaHeight = new CellDoubleProperty();
        /// <summary>
        /// максимальна ефективна висота
        /// </summary>
        private CellDoubleProperty CellAntennaMaxHeight = new CellDoubleProperty();

        /// <summary>
        /// Макс. коеф. підсилення Г
        /// </summary>
        private CellStringProperty CellMaxKoefG = new CellStringProperty();
        /// <summary>
        /// Макс. коеф. підсилення В
        /// </summary>
        private CellStringProperty CellMaxKoefV = new CellStringProperty();

        /// <summary>
        /// ЕВП Г
        /// </summary>
        private CellDoubleProperty CellEVPG = new CellDoubleProperty();
        /// <summary>
        /// ЕВП В
        /// </summary>
        private CellDoubleProperty CellEVPV = new CellDoubleProperty();

        /// <summary>
        /// ЕВП макс
        /// </summary>
        private CellStringProperty CellEVPm = new CellStringProperty();

        /// <summary>
        /// Позивний сигнал(програма мовлення)
        /// </summary>
        private CellStringProperty CellPositiv = new CellStringProperty();

        /// <summary>
        /// Внутрішній стан
        /// </summary>
        private CellStringComboboxAplyProperty CellhomeState = new CellStringComboboxAplyProperty();
        /// <summary>
        /// Міжнародний стан
        /// </summary>
        private CellStringComboboxAplyProperty CellinternationalState = new CellStringComboboxAplyProperty();

        /// <summary>
        /// ТВ канал
        /// </summary>
        private CellStringComboboxAplyProperty Celltvchan = new CellStringComboboxAplyProperty();
        /// <summary>
        /// частота:відео
        /// </summary>
        private CellDoubleBoundedProperty CellfreqVid = new CellDoubleBoundedProperty();
        /// <summary>
        /// частота:звук
        /// </summary>
        private CellDoubleBoundedProperty CellfreqSnd = new CellDoubleBoundedProperty();

        /// <summary>
        /// Об'єкти РЧП 
        /// </summary>
        private CellStringProperty CellObjRchp = new CellStringProperty();

        #endregion

        public CTvAnalogAppDiff(int id, int ownerId, int packetID, string radioTech)
            : base(id, ICSMTbl.itblTV, ownerId, packetID, radioTech)
        {
            appType = AppType.AppTV2;
            tvStation = new TvAnalogStation();
            tvStation.Id = recordID.Id;
            tvStation.Load();

            SCVisnovok = tvStation.Finding;

            tvDiffStation = new TvAnalogDiffStation();
            tvDiffStation.Id = recordID.Id;
            tvDiffStation.Load();

            ReadAllDataPositions();

            if (tvDiffStation.Position == null)
                tvDiffStation.Position = tvStation.Position;
            CellCertificateDate.NullString = "-";
            if (tvDiffStation.IsEnableDiff)
            {
                CellAddress.Value = tvDiffStation.Position.FullAddress;
                CellLongitude.DoubleValue = tvDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = tvDiffStation.Position.LatDms;
                CellASL.DoubleValue = tvDiffStation.Position.ASl;

                CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, tvDiffStation.TvDiffStationEquipment.Certificate);

                CellEquipmentShiftOstov.Value = tvDiffStation.NesivFreq.ToStringNullD();
                if (!string.IsNullOrEmpty(tvDiffStation.EquipmentName))
                    CellEquipmentName.Value = tvDiffStation.EquipmentName;
                CellPowerVideo.DoubleValue = tvDiffStation.PowVideo.Round(3);
                CellPowerAudio.DoubleValue = tvDiffStation.PowAudio.Round(3);
                CellTxBandwidth.LowerBound = 0.0;
                CellTxBandwidth.DoubleValue = tvDiffStation.Bandwith;
                CellEmiVideo.Value = tvDiffStation.EmiVideo;
                CellEmiAudio.Value = tvDiffStation.EmiAudio;

                Celltvchan.Value = tvDiffStation.Channel;

                CellAntennaDir.Value = tvDiffStation.Direction;
                CellAntennaPolar.Value = tvDiffStation.Polar;
                CellAntennaHeight.Value = tvDiffStation.Height.Round(3).ToStringNullD();
                CellfreqVid.LowerBound = 0.0;
                CellfreqVid.DoubleValue = tvDiffStation.FreqVideo.Round(3);
                CellfreqSnd.LowerBound = 0.0;
                CellfreqSnd.DoubleValue = tvDiffStation.FreqAudio.Round(3);
                Comment = tvDiffStation.StatComment;
                CreateEquipmentComboBoxes();
            } else
            {
                tvDiffStation.Position = tvStation.Position.Clone() as PositionState;
            }

            _initialized = true;

            OnRecalculateErp(null);
            _stationObject = tvStation;            
        }

        /// <summary>
        /// Створення Комбо боксів для обладнання
        /// </summary>
        private void CreateEquipmentComboBoxes()
        {
            CellPowerVideo.Value2 = FillValue2(tvStation.PowVideo[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerVideo.DoubleValue = tvDiffStation.PowVideo.Round(3);

            CellPowerAudio.Value2 = FillValue2(tvStation.PowAudio[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerAudio.DoubleValue = tvDiffStation.PowAudio.Round(3);

            CellTxBandwidth.Value2 = FillValue2(tvStation.TvStationEquipment.BandWidth.ToStringNullD());
            CellTxBandwidth.Value = tvDiffStation.Bandwith.ToStringNullD();

            CellEmiVideo.Value2 = FillValue2(tvStation.TvStationEquipment.EVideo);
            CellEmiVideo.Value = tvDiffStation.EmiVideo;

            CellEmiAudio.Value2 = FillValue2(tvStation.TvStationEquipment.EAudio);
            CellEmiAudio.Value = tvDiffStation.EmiAudio;

            CertificateHelper.InitializeCertificate(CellCertificateNumber, CellCertificateDate, tvDiffStation.TvDiffStationEquipment.Certificate);
            CellCertificateDate.SetRedValue();
        }

        #region Overrides of BaseAppClass

        /// <summary>
        /// Обновляет записи о пакете в станции
        /// функция необходима только для использования из пакета
        /// </summary>
        public override void UpdateToNewPacket()
        {

        }
        /// <summary>
        /// Загружаем данные для базовой станции
        /// </summary>
        private void ReadAllDataPositions()
        {
            CellLongitude.DoubleValue2 = tvStation.Position.LonDms;
            CellLatitude.DoubleValue2 = tvStation.Position.LatDms;
            CellASL.Value2 = FillValue2(tvStation.Position.ASl.ToStringNullD());
            CellAddress.Value2 = FillValue2(tvStation.Position.FullAddress);

            CellLongitude.DoubleValue = tvStation.Position.LonDms;
            CellLatitude.DoubleValue = tvStation.Position.LatDms;
            CellASL.DoubleValue = tvStation.Position.ASl;
            CellAddress.Value = tvStation.Position.FullAddress;

            foreach (KeyValuePair<string, string> system in tvStation.SystemDict)
                CellSystem.Items.Add(system.Value);
            if (!tvStation.SystemDict.ContainsKey(tvStation.Systems))
                tvStation.Systems = tvStation.SystemDict.ToList()[0].Key;
            CellSystem.Value = tvStation.SystemDict[tvStation.Systems];

            CellStandart.Items.AddRange(tvStation.StandartList);
            if (!tvStation.StandartList.Contains(tvStation.Standart))
                tvStation.Standart = tvStation.StandartList[0];
            CellStandart.Value = tvStation.Standart;
            if (string.IsNullOrEmpty(tvStation.NesivFreq.ToStringNullD()))
                if (tvStation.ShiftList.Count > 0)
                    tvStation.NesivFreq = tvStation.ShiftList[0].ToDouble(IM.NullD);

            CellEquipmentShiftOstov.Value2 = FillValue2(tvStation.NesivFreq.Round(3).ToStringNullD());
            CellEquipmentShiftOstov.Value = tvStation.NesivFreq.Round(3).ToStringNullD();

            CellEquipmentShiftOstov.Items.AddRange(tvStation.ShiftList);

            if (!string.IsNullOrEmpty(tvStation.NumberREZ))
                CellEquipmentNameNumber.Value = tvStation.NumberREZ;
            CellEquipmentNameNumber.Value2 = FillValue2(tvStation.NumberREZ);
            if (!string.IsNullOrEmpty(tvStation.Position.Name))
                CellEquipmentNameREZ.Value = tvStation.Position.Name;
            CellEquipmentNameREZ.Value2 = FillValue2(tvStation.Position.Name);

            CellEquipmentName.Value2 = FillValue2(tvStation.TvStationEquipment.Name);
            CellPowerVideo.Value2 = FillValue2(tvStation.PowVideo[PowerUnits.W].Round(3).ToStringNullD());
            CellPowerAudio.Value2 = FillValue2(tvStation.PowAudio[PowerUnits.W].Round(3).ToStringNullD());// tvStation.PowAudio;               

            if (!string.IsNullOrEmpty(tvStation.TvStationEquipment.Certificate.Symbol))
                CellCertificateNumber.Value = tvStation.TvStationEquipment.Certificate.Symbol;
            CellCertificateNumber.Value2 = FillValue2(tvStation.TvStationEquipment.Certificate.Symbol);
            CellCertificateNumber.SetRedValue();

            string tmpDate;
            if (tvStation.TvStationEquipment.Certificate.Date == IM.NullT)
                tmpDate = "";
            else
                tmpDate = tvStation.TvStationEquipment.Certificate.Date.ToShortDateString();

            CellCertificateDate.Value = tmpDate;
            CellCertificateDate.Value2 = FillValue2(tmpDate);
            CellCertificateDate.SetRedValue();

            CellEmiVideo.Value2 = FillValue2(tvStation.TvStationEquipment.EVideo);
            CellEmiAudio.Value2 = FillValue2(tvStation.TvStationEquipment.EAudio);
            CellTxBandwidth.Value2 = FillValue2(tvStation.TvStationEquipment.BandWidth.Round(3).ToStringNullD());

            if (!string.IsNullOrEmpty(tvStation.TvStationEquipment.Name))
                CellEquipmentName.Value = tvStation.TvStationEquipment.Name;

            CellPowerVideo.DoubleValue = tvStation.PowVideo[PowerUnits.W].Round(3);
            CellPowerAudio.DoubleValue = tvStation.PowAudio[PowerUnits.W].Round(3);

            CellEmiVideo.Value = tvStation.TvStationEquipment.EVideo;
            CellEmiAudio.Value = tvStation.TvStationEquipment.EAudio;
            CellTxBandwidth.Value2 = FillValue2(tvStation.TvStationEquipment.BandWidth.Round(3).ToStringNullD());

            CellFiderlost.Value2 = FillValue2(tvStation.FeederLosses.Round(3).ToStringNullD());
            CellFiderLength.Value2 = FillValue2(tvStation.FeederLength.Round(3).ToStringNullD());
            CellFiderlost.DoubleValue = tvStation.FeederLosses.Round(3);
            CellFiderLength.DoubleValue = tvStation.FeederLength.Round(3);

            /*foreach (KeyValuePair<string, string> diction in tvStation.OrientDict)
                CellAntennaDir.Items.Add(diction.Key + " - " + diction.Value);*/
            CellAntennaDir.Value2 = FillValue2(tvStation.Direction);
            CellAntennaDir.Value = tvStation.Direction;

            CellAntennaPolar.Value2 = FillValue2(tvStation.Polarization);
            CellAntennaPolar.Value = tvStation.Polarization;

            CellAntennaHeight.Value2 = FillValue2(tvStation.Height.Round(3).ToStringNullD());
            CellAntennaMaxHeight.Value2 = FillValue2(tvStation.MaxHeight.Round(3).ToStringNullD());
            CellAntennaHeight.DoubleValue = tvStation.Height.Round(3);
            CellAntennaMaxHeight.DoubleValue = tvStation.MaxHeight.Round(3);


            CellPositiv.Value2 = FillValue2(tvStation.Call);
            CellPositiv.Value = tvStation.Call;

            if (!tvStation.HomeDict.ContainsKey(tvStation.homeState))
                if (tvStation.HomeDict.Count > 0)
                    tvStation.homeState = tvStation.HomeDict.ToList()[0].Key;

            CellhomeState.Value = tvStation.homeState + " - " + tvStation.HomeDict[tvStation.homeState];
            foreach (KeyValuePair<string, string> dict in tvStation.HomeDict)
                CellhomeState.Items.Add(dict.Key + " - " + dict.Value);

            if (!tvStation.InternationalDict.ContainsKey(tvStation.intState))
                if (tvStation.InternationalDict.Count > 0)
                    tvStation.intState = tvStation.InternationalDict.ToList()[0].Key;

            CellinternationalState.Value = tvStation.intState + " - " + tvStation.InternationalDict[tvStation.intState];
            foreach (KeyValuePair<string, string> dict in tvStation.InternationalDict)
                CellinternationalState.Items.Add(dict.Key + " - " + dict.Value);

            //Celltvchan.Items.AddRange(tvStation.ChannelList);

            CellfreqVid.Value2 = FillValue2(tvStation.freqVid.Round(3).ToStringNullD());
            CellfreqSnd.Value2 = FillValue2(tvStation.freqAud.Round(3).ToStringNullD());
            if (!string.IsNullOrEmpty(tvStation.tvChannel))
                tvStation.tvChannel = string.Empty;


            CellfreqVid.DoubleValue = tvStation.freqVid.Round(3);
            CellfreqSnd.DoubleValue = tvStation.freqAud.Round(3);
           // if (string.IsNullOrEmpty(tvStation.tvChannel))
           //     if (tvStation.ChannelList.Count > 0)
           //         tvStation.tvChannel = tvStation.ChannelList[0];
           // Celltvchan.Value2 = FillValue2(tvStation.tvChannel);
            Celltvchan.Value = tvStation.tvChannel;
            Celltvchan.Items.AddRange(tvStation.ChannelList);

            // доработка
            string current_channel = "";
            if (tvStation.Id != IM.NullI)
            {
                List<string> rt = new List<string>();
                rt.Clear();
                IMRecordset r1 = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadOnly);
                r1.Select("ID,CHANNEL");
                r1.SetWhere("ID", IMRecordset.Operation.Eq, tvStation.Id);
                r1.Open();
                if (!r1.IsEOF())
                {
                    current_channel = r1.GetS("CHANNEL");
                }
                if (r1.IsOpen())
                    r1.Close();
                r1.Destroy();
            }


            Celltvchan.Value2 = current_channel;


            //


            CellObjRchp.Value2 = FillValue2(tvStation.ObjRchp.ToString());
            CellObjRchp.Value = tvStation.ObjRchp.ToString();
        }

        protected override void ShowObject(Cell cell)
        {
            if (cell.Key == "NewKey-KAddr")
            {
                AdminSiteAllTech.Show(tvStation.Position.TableName, tvStation.Position.Id);
            }          
        }

        /// <summary>
        /// Внутреняя функция для сохранения заявки в БД
        /// </summary>
        protected override bool SaveAppl()
        {
            tvDiffStation.Id = recordID.Id;

            /*if (CellLongitude.DoubleValue != CellLongitude.DoubleValue2 ||
              CellLatitude.DoubleValue != CellLatitude.DoubleValue2 ||
              tvDiffStation.Position.TableName != ICSMTbl.itblPositionBro)
            {
                tvDiffStation.Position.LonDms = CellLongitude.DoubleValue;
                tvDiffStation.Position.LatDms = CellLatitude.DoubleValue;
                // Сброс id в IM.NullI заставит создать новую запись                
                tvDiffStation.Position.Id = IM.NullI;
                tvDiffStation.Position.TableName = PlugTbl.itblXnrfaPositions;
            }
            if (tvDiffStation.Position.TableName == PlugTbl.itblXnrfaPositions)
                tvDiffStation.Position.SaveToBD();*/

            tvDiffStation.Position =
              CoordinateDiffHelper.CheckPosition(
                  CellLongitude,
                  CellLatitude,
                  tvDiffStation.Position,
                  ICSMTbl.itblPositionBro);

            Certificate certificate = new Certificate();
            if (!CertificateHelper.CheckCertificate(CellCertificateNumber, CellCertificateDate, certificate))
            {
                MessageBox.Show("Невірно введено дані про сертифікат\r\nПотрібно або ввести дані про сертифікат, або поставити прочерки в обох клітинках.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            tvDiffStation.TvDiffStationEquipment.Certificate.Symbol = certificate.Symbol;
            tvDiffStation.TvDiffStationEquipment.Certificate.Date = certificate.Date;

            tvDiffStation.NesivFreq = CellEquipmentShiftOstov.Value.ToDouble(IM.NullD);
            tvDiffStation.EquipmentName = CellEquipmentName.Value;
            tvDiffStation.PowVideo = CellPowerVideo.DoubleValue;
            tvDiffStation.PowAudio = CellPowerAudio.DoubleValue;
            tvDiffStation.Bandwith = CellTxBandwidth.DoubleValue;
            tvDiffStation.EmiVideo = CellEmiVideo.Value;
            tvDiffStation.EmiAudio = CellEmiAudio.Value;

            tvDiffStation.Height = CellAntennaHeight.Value.ToDouble(IM.NullD);

            tvDiffStation.FreqVideo = CellfreqVid.DoubleValue;
            tvDiffStation.FreqAudio = CellfreqSnd.DoubleValue;
            tvDiffStation.Channel = Celltvchan.Value;

            tvDiffStation.Direction = CellAntennaDir.Value;
            tvDiffStation.Polar = CellAntennaPolar.Value;
            tvDiffStation.StatComment = Comment;
            tvDiffStation.Save();

            if (listFotosDst != null && listFotosSrc != null)
                tvDiffStation.Position.SaveFoto(listFotosDst, listFotosSrc);//, TvAnalogDiffStation.TableName, tvStation.Id);
            return true;
        }

        /// <summary>
        /// Возвращает полное имя выходного документа
        /// </summary>
        protected override string getOutDocName(string docType, string number)
        {
            return string.Empty;
        }


        public override void InitParamGrid(Cell cell)
        {

        }

        /// <summary>
        /// Just Legacy
        /// </summary>
        public override void InitParamGrid(Grid grid) { }

        #endregion

        public void OnPressButtonLongitude(Cell cell)
        {
            NSPosition.RecordXY tmpXy = NSPosition.Position.convertPosition(CellLongitude.DoubleValue, CellLatitude.DoubleValue, "4DMS", "4DMS");

            IMObject objPosition = frmSelectPosition.SelectPosition(ICSMTbl.itblPositionBro, 1, tmpXy.Longitude, tmpXy.Latitude);
            if (objPosition != null)
            {
                tvDiffStation.Position = new PositionState();
                tvDiffStation.Position.LoadStatePosition(objPosition);
                objPosition.Dispose();
                if (!string.IsNullOrEmpty(tvDiffStation.Position.FullAddress))
                    CellAddress.Value = tvDiffStation.Position.FullAddress;
                CellASL.DoubleValue = tvDiffStation.Position.ASl;
                CellLongitude.DoubleValue = tvDiffStation.Position.LonDms;
                CellLatitude.DoubleValue = tvDiffStation.Position.LatDms;

                // Форма Адреса місця встановлення РЕЗ
                FormNewPosition frm = new FormNewPosition(tvDiffStation.Position.CityId);                

                frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
                frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
                frm.OldPosition = tvDiffStation.Position;
                frm.CanEditPosition = false;
                frm.CanAttachPhoto = true;
                frm.CanEditStreet = false;

                if (frm.ShowDialog() == DialogResult.OK)
                {                    
                    frm.GetFotos(out listFotosDst, out listFotosSrc);
                }
            }
        }

        public void OnBeforeChangeEquipmentName(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(newVal))
                CellEquipmentName.Value = "*";
        }

        public void OnBeforeChangePowerTr(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal))
                val = cell.Value;
        }


        public void OnBeforeChangeTv(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal) && newVal < 0)
                val = cell.Value;
        }

        public void OnBeforeChangeDesignEmission(Cell cell, string newVal)
        {

        }

        public void OnBeforeChangeShiftOstov(Cell cell, ref string val)
        {
            double newVal;
            if (!double.TryParse(val, out newVal))
                val = cell.Value;
        }

        public void OnBeforeChangeCertificateDate(Cell cell, ref string val)
        {
            DateTime newVal;
            if (!DateTime.TryParse(val, out newVal) || newVal < DateTime.MinValue)
                val = cell.Value;
        }


        public void OnBeforeChangeCertificateNumber(Cell cell, string newVal)
        {
            if (string.IsNullOrEmpty(CellCertificateNumber.Value))
                CellCertificateNumber.Value = "";
        }

        public void OnPressButtonAddressName(Cell cell)
        {
            FormNewPosition frm = new FormNewPosition(tvDiffStation.Position.CityId);
            frm.OldPosition = tvDiffStation.Position;                

            frm.SetLongitudeAsDms(CellLongitude.DoubleValue);
            frm.SetLatitudeAsDms(CellLatitude.DoubleValue);
            frm.CanAttachPhoto = true;
            frm.CanEditPosition = true;
            frm.CanEditStreet = true;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                tvDiffStation.Position = frm.LoadNewPosition(PlugTbl.itblXnrfaPositions, CellLongitude.DoubleValue, CellLatitude.DoubleValue);
                if (!string.IsNullOrEmpty(tvDiffStation.Position.FullAddress))
                    CellAddress.Value = tvDiffStation.Position.FullAddress;
            }
        }

        protected override string GetXMLParamGrid()
        {
            return Properties.Resources.CTvAnalogAppDiff;
        }

        public void OnRecalculateErp(Cell cell)
        {
            if (_initialized)
            {
                tvStation.Power[PowerUnits.W] = CellPowerVideo.DoubleValue;
                tvStation.Polarization = CellAntennaPolar.Value;
                tvStation.Direction = CellAntennaDir.Value;

                FillGainAndErpCells();
            }
        }

        private void FillGainAndErpCells()
        {
            string value;

            ///--------------
            if (!tvStation.HorizontalDiagramm.IsEmpty && tvStation.Polarization != "V")
                value = tvStation.HorizontalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefG.Value = value;
            CellMaxKoefG.Value2 = FillValue2(CellMaxKoefG.Value);

            ///--------------
            if (!tvStation.VerticalDiagramm.IsEmpty && tvStation.Polarization != "H")
                value = tvStation.VerticalDiagramm.Max.ToString();
            else
                value = "не застосовується";

            CellMaxKoefV.Value = value;
            CellMaxKoefV.Value2 = FillValue2(CellMaxKoefV.Value);

            tvStation.CalculateERP();
            if (tvStation.Erp.Horizontal.IsValid)
                CellEVPG.Value = tvStation.Erp.Horizontal[PowerUnits.dBW].ToString("F3");
            else
                CellEVPG.Value = "не застосовується";

            if (tvStation.Erp.Vertical.IsValid)
                CellEVPV.Value = tvStation.Erp.Vertical[PowerUnits.dBW].ToString("F3");
            else
                CellEVPV.Value = "не застосовується";

            CellEVPm.Value = tvStation.Erp.Overall[PowerUnits.dBW].ToString("F3");
        }
    }
}
