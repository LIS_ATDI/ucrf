﻿namespace XICSM.UcrfRfaNET.Calculation.ResultForm
{
   partial class TreeColumnWinForms
   {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.hostWpfElement = new System.Windows.Forms.Integration.ElementHost();
          this.SuspendLayout();
          // 
          // hostWpfElement
          // 
          this.hostWpfElement.BackColor = System.Drawing.SystemColors.Control;
          this.hostWpfElement.Dock = System.Windows.Forms.DockStyle.Fill;
          this.hostWpfElement.Location = new System.Drawing.Point(0, 0);
          this.hostWpfElement.Name = "hostWpfElement";
          this.hostWpfElement.Size = new System.Drawing.Size(508, 265);
          this.hostWpfElement.TabIndex = 0;
          this.hostWpfElement.Text = "elementHost1";
          this.hostWpfElement.Child = null;
          // 
          // TreeColumnWinForms
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.Controls.Add(this.hostWpfElement);
          this.Name = "TreeColumnWinForms";
          this.Size = new System.Drawing.Size(508, 265);
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Integration.ElementHost hostWpfElement;
   }
}
