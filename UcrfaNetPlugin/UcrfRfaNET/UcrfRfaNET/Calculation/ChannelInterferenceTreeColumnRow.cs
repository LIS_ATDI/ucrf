﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.Calculation
{
   /// <summary>
   /// Класс описывает строку для отображения помех
   /// в основном и соседнем канале в TreeColumnView
   /// </summary>
   internal class ChannelInterferenceTreeColumnRow : TreeColumnRowBase
   {
      /// <summary>
      /// номер сектора
      /// </summary>
      [TreeColumn("Sector")]
      public StringElemCell Sector { get; set; }
      /// <summary>
      /// Частота
      /// </summary>
      [TreeColumn("Frequency (MGz)")]
      public DoubleElemCell Frequency { get; set; }
      /// <summary>
      /// Помеха
      /// </summary>
      [TreeColumn("Interference")]
      public DoubleElemCell Interference { get; set; }
      /// <summary>
      /// IRF
      /// </summary>
      [TreeColumn("IRF (dB)")]
      public DoubleElemCell Irf { get; set; }
      //===============================================
      /// <summary>
      /// Конструктор
      /// </summary>
      public ChannelInterferenceTreeColumnRow() : base()
      {
         Sector.Value = "";
         Frequency.Value = 0.0;
         Interference.Value = 0.0;
         Irf.Value = 0.0;
      }
   }
}
