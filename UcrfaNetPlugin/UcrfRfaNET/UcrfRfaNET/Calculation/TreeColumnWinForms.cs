﻿using System.Windows.Forms;
using ComponentsLib;

namespace XICSM.UcrfRfaNET.Calculation.ResultForm
{
   /// <summary>
   /// Компонента пользователя, для отображения
   /// </summary>
   public partial class TreeColumnWinForms : UserControl
   {
      /// <summary>
      /// WPF компонент TreeColumnCtrl
      /// </summary>
      private TreeColumnCtrl _treeColumn;
      /// <summary>
      /// WPF компонент TreeColumnCtrl для внешного использования
      /// </summary>
      public TreeColumnCtrl TreeColumn {get { return _treeColumn; }}
      /// <summary>
      /// Конструктор
      /// </summary>
      public TreeColumnWinForms()
      {
         InitializeComponent();
         //====
         _treeColumn = new TreeColumnCtrl();
         hostWpfElement.Child = _treeColumn;
      }
   }
}
