﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.Calculation
{
   /// <summary>
   /// Класс описывает строку для отображения помех
   /// при расчете ЭМС для зарубежных линков в TreeColumnView
   /// </summary>
   internal class InterferenceEmcForeignMicrowaRow : TreeColumnRowBase
   {
      /// <summary>
      ///+ TD
      /// </summary>
      [TreeColumn("TD")]
      public DoubleElemCell Td { get; set; }
      /// <summary>
      ///+ IRF
      /// </summary>
      [TreeColumn("IRF (dB)")]
      public DoubleElemCell Irf { get; set; }
      /// <summary>
      ///+ Растояние
      /// </summary>
      [TreeColumn("Distance")]
      public DoubleElemCell Distance { get; set; }
      /// <summary>
      ///+ Потери
      /// </summary>
      [TreeColumn("Loss")]
      public DoubleElemCell Loss { get; set; } 
      /// <summary>
      ///+ Частота помехи
      /// </summary>
      [TreeColumn("Frequency noise")]
      public DoubleElemCell FreqNoise { get; set; }
      /// <summary>
      /// Частота жертвы
      /// </summary>
      [TreeColumn("Frequency sacrifice")]
      public DoubleElemCell FreqSacrifice { get; set; }
      /// <summary>
      ///+ Pi
      /// </summary>
      [TreeColumn("Pi (Pпом)")]
      public DoubleElemCell Pi { get; set; }
      /// <summary>
      ///+ Азимут помехи
      /// </summary>
      [TreeColumn("Azimuth noise")]
      public DoubleElemCell AzimuthNoise { get; set; }
      /// <summary>
      ///+ Азимут жертвы
      /// </summary>
      [TreeColumn("Azimuth sacrifice")]
      public DoubleElemCell AzimuthSacrifice { get; set; }
      /// <summary>
      ///+ Коэф. усил. помехи
      /// </summary>
      [TreeColumn("Gain noise")]
      public DoubleElemCell GainNoise { get; set; }
      /// <summary>
      ///+ Коэф. усил. жертвы
      /// </summary>
      [TreeColumn("Gain sacrifice")]
      public DoubleElemCell GainSacrifice { get; set; }
      /// <summary>
      /// Дельта F помехи
      /// </summary>
      [TreeColumn("dFпрд")]
      public DoubleElemCell DeltaFNoise { get; set; }
      /// <summary>
      /// Дельта F жертвы
      /// </summary>
      [TreeColumn("dFпрд")]
      public DoubleElemCell DeltaFSacrifice { get; set; }
      /// <summary>
      /// KTBF
      /// </summary>
      [TreeColumn("KTBF")]
      public DoubleElemCell KtbfSacrifice { get; set; }
      /// <summary>
      /// Макс. усиление помехи
      /// </summary>
      [TreeColumn("Gmax")]
      public DoubleElemCell GmaxNoise { get; set; }
      /// <summary>
      /// Макс. усиление жертвы
      /// </summary>
      [TreeColumn("Gmax sacrifice")]
      public DoubleElemCell GmaxSacrifice { get; set; }
      /// <summary>
      /// Мощность помехи
      /// </summary>
      [TreeColumn("Pпрд")]
      public DoubleElemCell PowerNoise { get; set; }
      /// <summary>
      /// Макс. Азимут помехи
      /// </summary>
      [TreeColumn("Amax Tx")]
      public DoubleElemCell AmaxNoise { get; set; }
      /// <summary>
      /// Макс. Азимут жертвы
      /// </summary>
      [TreeColumn("Amax Rx")]
      public DoubleElemCell AmaxSacrifice { get; set; }
      /// <summary>
      /// ID станции
      /// </summary>
      [TreeColumn("ID station")]
      public IntElemCell StationId { get; set; }
      /// <summary>
      /// Название таблицы
      /// </summary>
      [TreeColumn("Table name")]
      public StringElemCell TableName { get; set; }

      /// <summary>
      /// Позиция станции 1 (всегда должна быть)
      /// </summary>
      public ICSM.IMPosition PositionStationNoise { get; set; }
      /// <summary>
      /// Позиция станции 1 (Может отсутствовать)
      /// </summary>
      public ICSM.IMPosition PositionLinkNoise { get; set; }
      /// <summary>
      /// Позиция станции 1 (всегда должна быть)
      /// </summary>
      public ICSM.IMPosition PositionStationSacrifice { get; set; }
      /// <summary>
      /// Позиция станции 1 (Может отсутствовать)
      /// </summary>
      public ICSM.IMPosition PositionLinkSacrifice { get; set; }

      //===============================================
      /// <summary>
      /// Конструктор
      /// </summary>
      public InterferenceEmcForeignMicrowaRow()
         : base()
      {
         StationId = new IntElemCell();
         TableName = new StringElemCell();
         Td = new DoubleElemCell();
         Irf = new DoubleElemCell();
         Pi = new DoubleElemCell();
         Distance = new DoubleElemCell(2);
         Loss = new DoubleElemCell(4);

         GainNoise = new DoubleElemCell(4);
         GmaxNoise = new DoubleElemCell(4);
         AzimuthNoise = new DoubleElemCell(4);
         FreqNoise = new DoubleElemCell(4);
         PowerNoise = new DoubleElemCell(4);
         DeltaFNoise = new DoubleElemCell(4);
         AmaxNoise = new DoubleElemCell(4);
         PositionStationNoise = new ICSM.IMPosition();
         PositionLinkNoise = new ICSM.IMPosition();

         GainSacrifice = new DoubleElemCell(4);
         AzimuthSacrifice = new DoubleElemCell(4);
         FreqSacrifice = new DoubleElemCell(4);
         KtbfSacrifice = new DoubleElemCell(4);
         DeltaFSacrifice = new DoubleElemCell(4);
         AmaxSacrifice = new DoubleElemCell(4);
         GmaxSacrifice = new DoubleElemCell(4);
         PositionStationSacrifice = new ICSM.IMPosition();
         PositionLinkSacrifice = new ICSM.IMPosition();
      }
   }
}
