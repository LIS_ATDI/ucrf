﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Calculation.ResultForm;

namespace XICSM.UcrfRfaNET.Calculation
{
   public partial class TabsTreeColumn : UserControl
   {
      public List<IEnumerable> ListOfItemSources;
      /// <summary>
      /// Конструктор
      /// </summary>
      public TabsTreeColumn()
      {
         InitializeComponent();
         //----------
         ListOfItemSources = new List<IEnumerable>();
      }
       /// <summary>
      /// Добавляет новую вкладку на форму
      /// </summary>
      /// <param name="tabCaption">Заголовок вкладки</param>
      /// <param name="itemDate">Данные для отображения</param>
      /// <param name="keyOfTab">Ключевая строка закладки (с таким названием формируется файл для сохранения данных)</param>
      /// <exception cref="System.ArgumentNullException">Thrown when IEnumerable is null</exception>
      public TreeColumnWinForms AddNewTab(string tabCaption, IEnumerable itemDate, string keyOfTab)
      {
           return AddNewTab(tabCaption, itemDate, keyOfTab, "pluginlocal");
      }
      /// <summary>
      /// Добавляет новую вкладку на форму
      /// </summary>
      /// <param name="tabCaption">Заголовок вкладки</param>
      /// <param name="itemDate">Данные для отображения</param>
      /// <param name="keyOfTab">Ключевая строка закладки (с таким названием формируется файл для сохранения данных)</param>
      /// <param name="settingFile">название файла настроек</param>
      /// <exception cref="System.ArgumentNullException">Thrown when IEnumerable is null</exception>
      public TreeColumnWinForms AddNewTab(string tabCaption, IEnumerable itemDate, string keyOfTab, string settingFile)
      {
         if (itemDate == null)
            throw new System.ArgumentNullException();

         if (string.IsNullOrEmpty(tabCaption))
            tabCaption = string.Format("{0}", tabControl.TabPages.Count);
         string settingFileName = "";
         if (string.IsNullOrEmpty(keyOfTab) == false)
         {
            string workSpaceFoulder = IM.GetWorkspaceFolder() + "\\PluginSettings";
            if (System.IO.Directory.Exists(workSpaceFoulder) == false)
               System.IO.Directory.CreateDirectory(workSpaceFoulder);
            if (System.IO.Directory.Exists(workSpaceFoulder) == true)
                settingFileName = string.Format("{0}\\{1}.cfg", workSpaceFoulder, settingFile);
         }
         TreeColumnWinForms newTab = new TreeColumnWinForms();
         newTab.TreeColumn.ItemsSource = itemDate;
         newTab.TreeColumn.RootSetting = keyOfTab;
         newTab.TreeColumn.SettingFile = settingFileName;
         newTab.TreeColumn.LoadColumnFromFile();

         tabControl.TabPages.Add(tabCaption);
         tabControl.TabPages[tabControl.TabPages.Count - 1].Name = string.Format("tabControl_TabPages_{0}",
                                                                                 tabControl.TabPages.Count - 1);
         tabControl.TabPages[tabControl.TabPages.Count - 1].Controls.Add(newTab);
         newTab.Dock = DockStyle.Fill;
         ListOfItemSources.Add(itemDate);
         return newTab;
      }
      /// <summary>
      /// Обработка измениния текущей вкладки
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void tabControl_SelectedIndexChanged(object sender, System.EventArgs e)
      {
          int indexSelectedTab = tabControl.SelectedIndex;
          if(indexSelectedTab > -1)
          {
              TreeColumnWinForms tab = tabControl.TabPages[indexSelectedTab].Controls[0] as TreeColumnWinForms;
              if(tab != null)
                  tab.TreeColumn.Focus();
          }
      }

      /// <summary>
      /// Очистаить таблицы
      /// </summary>
      public void Clear()
      {
          tabControl.TabPages.Clear();                    
          ListOfItemSources.Clear();
      }
   }
}
