﻿namespace XICSM.UcrfRfaNET.Users
{
    partial class SelectUserDepartmentType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbManagementType = new System.Windows.Forms.GroupBox();
            this.rbBranch = new System.Windows.Forms.RadioButton();
            this.rbUamv = new System.Windows.Forms.RadioButton();
            this.rbUrcm = new System.Windows.Forms.RadioButton();
            this.rbUrzp = new System.Windows.Forms.RadioButton();
            this.rbUrcp = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbManagementType.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbManagementType
            // 
            this.gbManagementType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbManagementType.Controls.Add(this.rbBranch);
            this.gbManagementType.Controls.Add(this.rbUamv);
            this.gbManagementType.Controls.Add(this.rbUrcm);
            this.gbManagementType.Controls.Add(this.rbUrzp);
            this.gbManagementType.Controls.Add(this.rbUrcp);
            this.gbManagementType.Location = new System.Drawing.Point(12, 12);
            this.gbManagementType.Name = "gbManagementType";
            this.gbManagementType.Size = new System.Drawing.Size(190, 154);
            this.gbManagementType.TabIndex = 0;
            this.gbManagementType.TabStop = false;
            this.gbManagementType.Text = "Тип підрозділу";
            // 
            // rbBranch
            // 
            this.rbBranch.AutoSize = true;
            this.rbBranch.Location = new System.Drawing.Point(7, 112);
            this.rbBranch.Name = "rbBranch";
            this.rbBranch.Size = new System.Drawing.Size(52, 17);
            this.rbBranch.TabIndex = 4;
            this.rbBranch.TabStop = true;
            this.rbBranch.Text = "Філія";
            this.rbBranch.UseVisualStyleBackColor = true;
            // 
            // rbUamv
            // 
            this.rbUamv.AutoSize = true;
            this.rbUamv.Location = new System.Drawing.Point(6, 89);
            this.rbUamv.Name = "rbUamv";
            this.rbUamv.Size = new System.Drawing.Size(56, 17);
            this.rbUamv.TabIndex = 3;
            this.rbUamv.TabStop = true;
            this.rbUamv.Text = "УАМВ";
            this.rbUamv.UseVisualStyleBackColor = true;
            // 
            // rbUrcm
            // 
            this.rbUrcm.AutoSize = true;
            this.rbUrcm.Location = new System.Drawing.Point(6, 66);
            this.rbUrcm.Name = "rbUrcm";
            this.rbUrcm.Size = new System.Drawing.Size(57, 17);
            this.rbUrcm.TabIndex = 2;
            this.rbUrcm.TabStop = true;
            this.rbUrcm.Text = "УРЧМ";
            this.rbUrcm.UseVisualStyleBackColor = true;
            // 
            // rbUrzp
            // 
            this.rbUrzp.AutoSize = true;
            this.rbUrzp.Location = new System.Drawing.Point(6, 43);
            this.rbUrzp.Name = "rbUrzp";
            this.rbUrzp.Size = new System.Drawing.Size(55, 17);
            this.rbUrzp.TabIndex = 1;
            this.rbUrzp.TabStop = true;
            this.rbUrzp.Text = "УРЗП";
            this.rbUrzp.UseVisualStyleBackColor = true;
            // 
            // rbUrcp
            // 
            this.rbUrcp.AutoSize = true;
            this.rbUrcp.Location = new System.Drawing.Point(7, 20);
            this.rbUrcp.Name = "rbUrcp";
            this.rbUrcp.Size = new System.Drawing.Size(56, 17);
            this.rbUrcp.TabIndex = 0;
            this.rbUrcp.TabStop = true;
            this.rbUrcp.Text = "УРЧП";
            this.rbUrcp.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(70, 183);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "ОК";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // SelectUserDepartmentType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 218);
            this.ControlBox = false;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbManagementType);
            this.Name = "SelectUserDepartmentType";
            this.ShowIcon = false;
            this.Text = "Вибір типу підрозділу";
            this.Load += new System.EventHandler(this.SelectUserManagementType_Load);
            this.gbManagementType.ResumeLayout(false);
            this.gbManagementType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbManagementType;
        private System.Windows.Forms.RadioButton rbUamv;
        private System.Windows.Forms.RadioButton rbUrcm;
        private System.Windows.Forms.RadioButton rbUrzp;
        private System.Windows.Forms.RadioButton rbUrcp;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton rbBranch;
    }
}