﻿using System;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.Users
{
    public partial class SelectUserDepartmentType : FBaseForm
    {
        /// <summary>
        /// Выбрать управление
        /// </summary>
        /// <returns>Выбраное управление</returns>
        public static UcrfDepartment InitUserDepartment(UcrfDepartment curManagement)
        {
            using (SelectUserDepartmentType frm = new SelectUserDepartmentType(curManagement))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                    curManagement = frm.SelectedManagement();
            }
            return curManagement;
        }

        /// <summary>
        /// Текущее управление
        /// </summary>
        private UcrfDepartment _curManagement;
        /// <summary>
        /// Флаг возможности выхода
        /// </summary>
        private bool _canExit;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="management">Текущее управление</param>
        public SelectUserDepartmentType(UcrfDepartment management)
        {
            InitializeComponent();
            //
            _curManagement = management;
            _canExit = false;
        }

        /// <summary>
        /// Подтвердить закрытие формы
        /// </summary>
        /// <returns>TRUE - Можна закрыть форму</returns>
        protected override bool ConfirmClose()
        {
            return _canExit; //Запрещаем закрывать форму
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectUserManagementType_Load(object sender, EventArgs e)
        {
            UcrfDepartment allManagement = CUsers.GetAllUserDepartments();
            int manageInt = (int) allManagement;
            const int urcm = (int)UcrfDepartment.URCM;
            const int urcp = (int)UcrfDepartment.URCP;
            const int urzp = (int)UcrfDepartment.URZP;
            const int uamv = (int)UcrfDepartment.UAMV;
            const int branch = (int)UcrfDepartment.Branch;
            rbUrcp.Enabled = ((manageInt & urcp) == urcp);
            rbUrcm.Enabled = ((manageInt & urcm) == urcm);
            rbUrzp.Enabled = ((manageInt & urzp) == urzp);
            rbUamv.Enabled = ((manageInt & uamv) == uamv);
            rbBranch.Enabled = ((manageInt & branch) == branch);
            //
            manageInt = (int) _curManagement;
            rbUrcp.Checked = ((manageInt & urcp) == urcp);
            rbUrcm.Checked = ((manageInt & urcm) == urcm);
            rbUrzp.Checked = ((manageInt & urzp) == urzp);
            rbUamv.Checked = ((manageInt & uamv) == uamv);
            rbBranch.Checked = ((manageInt & branch) == branch);
        }
        /// <summary>
        /// Возвращает выбраное управление
        /// </summary>
        /// <returns>Возвращает выбраное управление</returns>
        public UcrfDepartment SelectedManagement()
        {
            if (rbUrcp.Checked && rbUrcp.Enabled)
                return UcrfDepartment.URCP;
            else if (rbUrcm.Checked && rbUrcm.Enabled)
                return UcrfDepartment.URCM;
            else if (rbUrzp.Checked && rbUrzp.Enabled)
                return UcrfDepartment.URZP;
            else if (rbUamv.Checked && rbUamv.Enabled)
                return UcrfDepartment.UAMV;
            else if (rbBranch.Checked && rbBranch.Enabled)
                return UcrfDepartment.Branch;
            return UcrfDepartment.UNKNOWN;
        }
        /// <summary>
        /// Нажали кнопку ОК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            _canExit = true;
        }
    }
}
