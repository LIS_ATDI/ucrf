﻿namespace XICSM.UcrfRfaNET
{
    partial class FormEMS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TbxTD = new System.Windows.Forms.TextBox();
            this.TbxFreqDiapazon = new System.Windows.Forms.TextBox();
            this.TbxRadius = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnRozrahunokEMS = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chbGSM1800 = new System.Windows.Forms.CheckBox();
            this.chbMOB = new System.Windows.Forms.CheckBox();
            this.MOB_group = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chbBlock = new System.Windows.Forms.CheckBox();
            this.chbIntermod = new System.Windows.Forms.CheckBox();
            this.chbCoCH = new System.Windows.Forms.CheckBox();
            this.Mob_porog = new System.Windows.Forms.TextBox();
            this.R_mob_inter = new System.Windows.Forms.TextBox();
            this.R_mob = new System.Windows.Forms.TextBox();
            this.ChbOnlyAll = new System.Windows.Forms.CheckBox();
            this.ChbCoordinate = new System.Windows.Forms.CheckBox();
            this.ChbEarth = new System.Windows.Forms.CheckBox();
            this.ChbSRT = new System.Windows.Forms.CheckBox();
            this.ChbRadio = new System.Windows.Forms.CheckBox();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.chb_1_1_1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.MOB_group.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TbxTD);
            this.groupBox1.Controls.Add(this.TbxFreqDiapazon);
            this.groupBox1.Controls.Add(this.TbxRadius);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 196);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 161);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Завдання параметрів розрахунків";
            // 
            // TbxTD
            // 
            this.TbxTD.Location = new System.Drawing.Point(417, 109);
            this.TbxTD.Name = "TbxTD";
            this.TbxTD.Size = new System.Drawing.Size(100, 20);
            this.TbxTD.TabIndex = 5;
            this.TbxTD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
            // 
            // TbxFreqDiapazon
            // 
            this.TbxFreqDiapazon.Location = new System.Drawing.Point(417, 78);
            this.TbxFreqDiapazon.Name = "TbxFreqDiapazon";
            this.TbxFreqDiapazon.Size = new System.Drawing.Size(100, 20);
            this.TbxFreqDiapazon.TabIndex = 4;
            this.TbxFreqDiapazon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
            // 
            // TbxRadius
            // 
            this.TbxRadius.Location = new System.Drawing.Point(417, 45);
            this.TbxRadius.Name = "TbxRadius";
            this.TbxRadius.Size = new System.Drawing.Size(100, 20);
            this.TbxRadius.TabIndex = 3;
            this.TbxRadius.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(395, 41);
            this.label3.TabIndex = 2;
            this.label3.Text = "Максимальне припустиме перевищення порогу чутливості внаслідок завад, TD [дб]";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Частотний інтервал +/- [МГц]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Максимальна відстань пошуку завад [км]";
            // 
            // BtnRozrahunokEMS
            // 
            this.BtnRozrahunokEMS.Location = new System.Drawing.Point(118, 370);
            this.BtnRozrahunokEMS.Name = "BtnRozrahunokEMS";
            this.BtnRozrahunokEMS.Size = new System.Drawing.Size(108, 23);
            this.BtnRozrahunokEMS.TabIndex = 0;
            this.BtnRozrahunokEMS.Text = "Розрахунок";
            this.BtnRozrahunokEMS.UseVisualStyleBackColor = true;
            this.BtnRozrahunokEMS.Click += new System.EventHandler(this.BtnRozrahunokEMS_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chbGSM1800);
            this.groupBox2.Controls.Add(this.chbMOB);
            this.groupBox2.Controls.Add(this.MOB_group);
            this.groupBox2.Controls.Add(this.ChbOnlyAll);
            this.groupBox2.Controls.Add(this.ChbCoordinate);
            this.groupBox2.Controls.Add(this.ChbEarth);
            this.groupBox2.Controls.Add(this.ChbSRT);
            this.groupBox2.Controls.Add(this.ChbRadio);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(534, 178);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Вибір категорій станцій, які залучаються до розрахунків";
            // 
            // chbGSM1800
            // 
            this.chbGSM1800.AutoSize = true;
            this.chbGSM1800.Location = new System.Drawing.Point(382, 19);
            this.chbGSM1800.Name = "chbGSM1800";
            this.chbGSM1800.Size = new System.Drawing.Size(150, 17);
            this.chbGSM1800.TabIndex = 7;
            this.chbGSM1800.Text = "Базові станції GSM 1800";
            this.chbGSM1800.UseVisualStyleBackColor = true;
            this.chbGSM1800.Visible = false;
            this.chbGSM1800.CheckedChanged += new System.EventHandler(this.chbGSM1800_CheckedChanged);
            // 
            // chbMOB
            // 
            this.chbMOB.AutoSize = true;
            this.chbMOB.Location = new System.Drawing.Point(248, 19);
            this.chbMOB.Name = "chbMOB";
            this.chbMOB.Size = new System.Drawing.Size(130, 17);
            this.chbMOB.TabIndex = 6;
            this.chbMOB.Text = "Базові станції UMTS";
            this.chbMOB.UseVisualStyleBackColor = true;
            this.chbMOB.Visible = false;
            this.chbMOB.CheckedChanged += new System.EventHandler(this.chbMOB_CheckedChanged);
            // 
            // MOB_group
            // 
            this.MOB_group.Controls.Add(this.chb_1_1_1);
            this.MOB_group.Controls.Add(this.label7);
            this.MOB_group.Controls.Add(this.label6);
            this.MOB_group.Controls.Add(this.label5);
            this.MOB_group.Controls.Add(this.label4);
            this.MOB_group.Controls.Add(this.chbBlock);
            this.MOB_group.Controls.Add(this.chbIntermod);
            this.MOB_group.Controls.Add(this.chbCoCH);
            this.MOB_group.Controls.Add(this.Mob_porog);
            this.MOB_group.Controls.Add(this.R_mob_inter);
            this.MOB_group.Controls.Add(this.R_mob);
            this.MOB_group.Location = new System.Drawing.Point(241, 38);
            this.MOB_group.Name = "MOB_group";
            this.MOB_group.Size = new System.Drawing.Size(282, 138);
            this.MOB_group.TabIndex = 5;
            this.MOB_group.TabStop = false;
            this.MOB_group.Text = "Параметри для розрахунку завад UMTS 1920-1980";
            this.MOB_group.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(183, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Для 1980-2000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Граничне значення перевищення \"порогу\"";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 26);
            this.label5.TabIndex = 7;
            this.label5.Text = "Максимальна відстань для \r\nінтермодуляційної завади та блокування\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Максимальна відстань для завади в \r\nосновному та сусідньму каналі";
            // 
            // chbBlock
            // 
            this.chbBlock.AutoSize = true;
            this.chbBlock.Checked = true;
            this.chbBlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbBlock.Location = new System.Drawing.Point(5, 116);
            this.chbBlock.Name = "chbBlock";
            this.chbBlock.Size = new System.Drawing.Size(86, 17);
            this.chbBlock.TabIndex = 5;
            this.chbBlock.Text = "Блокування";
            this.chbBlock.UseVisualStyleBackColor = true;
            // 
            // chbIntermod
            // 
            this.chbIntermod.AutoSize = true;
            this.chbIntermod.Checked = true;
            this.chbIntermod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbIntermod.Location = new System.Drawing.Point(5, 101);
            this.chbIntermod.Name = "chbIntermod";
            this.chbIntermod.Size = new System.Drawing.Size(154, 17);
            this.chbIntermod.TabIndex = 4;
            this.chbIntermod.Text = "Інтермодуляційна завада";
            this.chbIntermod.UseVisualStyleBackColor = true;
            // 
            // chbCoCH
            // 
            this.chbCoCH.AutoSize = true;
            this.chbCoCH.Checked = true;
            this.chbCoCH.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbCoCH.Location = new System.Drawing.Point(5, 87);
            this.chbCoCH.Name = "chbCoCH";
            this.chbCoCH.Size = new System.Drawing.Size(229, 17);
            this.chbCoCH.TabIndex = 3;
            this.chbCoCH.Text = "Завада в основному і сусідньому каналі";
            this.chbCoCH.UseVisualStyleBackColor = true;
            // 
            // Mob_porog
            // 
            this.Mob_porog.Location = new System.Drawing.Point(6, 64);
            this.Mob_porog.Name = "Mob_porog";
            this.Mob_porog.Size = new System.Drawing.Size(41, 20);
            this.Mob_porog.TabIndex = 2;
            this.Mob_porog.Text = "0";
            // 
            // R_mob_inter
            // 
            this.R_mob_inter.Location = new System.Drawing.Point(6, 42);
            this.R_mob_inter.Name = "R_mob_inter";
            this.R_mob_inter.Size = new System.Drawing.Size(41, 20);
            this.R_mob_inter.TabIndex = 1;
            this.R_mob_inter.Text = "1";
            // 
            // R_mob
            // 
            this.R_mob.Location = new System.Drawing.Point(6, 19);
            this.R_mob.Name = "R_mob";
            this.R_mob.Size = new System.Drawing.Size(41, 20);
            this.R_mob.TabIndex = 0;
            this.R_mob.Text = "3";
            // 
            // ChbOnlyAll
            // 
            this.ChbOnlyAll.AutoSize = true;
            this.ChbOnlyAll.Checked = true;
            this.ChbOnlyAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChbOnlyAll.Location = new System.Drawing.Point(6, 142);
            this.ChbOnlyAll.Name = "ChbOnlyAll";
            this.ChbOnlyAll.Size = new System.Drawing.Size(236, 17);
            this.ChbOnlyAll.TabIndex = 4;
            this.ChbOnlyAll.Text = "Не враховувати станції даного оператору";
            this.ChbOnlyAll.UseVisualStyleBackColor = true;
            // 
            // ChbCoordinate
            // 
            this.ChbCoordinate.AutoSize = true;
            this.ChbCoordinate.Location = new System.Drawing.Point(6, 115);
            this.ChbCoordinate.Name = "ChbCoordinate";
            this.ChbCoordinate.Size = new System.Drawing.Size(163, 17);
            this.ChbCoordinate.TabIndex = 3;
            this.ChbCoordinate.Text = "Станції, що координуються";
            this.ChbCoordinate.UseVisualStyleBackColor = true;
            // 
            // ChbEarth
            // 
            this.ChbEarth.AutoSize = true;
            this.ChbEarth.Checked = true;
            this.ChbEarth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChbEarth.Location = new System.Drawing.Point(6, 88);
            this.ChbEarth.Name = "ChbEarth";
            this.ChbEarth.Size = new System.Drawing.Size(77, 17);
            this.ChbEarth.TabIndex = 2;
            this.ChbEarth.Text = "ЗС станції";
            this.ChbEarth.UseVisualStyleBackColor = true;
            // 
            // ChbSRT
            // 
            this.ChbSRT.AutoSize = true;
            this.ChbSRT.Checked = true;
            this.ChbSRT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChbSRT.Location = new System.Drawing.Point(6, 61);
            this.ChbSRT.Name = "ChbSRT";
            this.ChbSRT.Size = new System.Drawing.Size(84, 17);
            this.ChbSRT.TabIndex = 1;
            this.ChbSRT.Text = "СРТ станції";
            this.ChbSRT.UseVisualStyleBackColor = true;
            // 
            // ChbRadio
            // 
            this.ChbRadio.AutoSize = true;
            this.ChbRadio.Checked = true;
            this.ChbRadio.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChbRadio.Location = new System.Drawing.Point(6, 34);
            this.ChbRadio.Name = "ChbRadio";
            this.ChbRadio.Size = new System.Drawing.Size(77, 17);
            this.ChbRadio.TabIndex = 0;
            this.ChbRadio.Text = "РРЛ лінки";
            this.ChbRadio.UseVisualStyleBackColor = true;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(287, 370);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "Вийти";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // chb_1_1_1
            // 
            this.chb_1_1_1.AutoSize = true;
            this.chb_1_1_1.Location = new System.Drawing.Point(157, 102);
            this.chb_1_1_1.Name = "chb_1_1_1";
            this.chb_1_1_1.Size = new System.Drawing.Size(53, 17);
            this.chb_1_1_1.TabIndex = 10;
            this.chb_1_1_1.Text = "1+1-1";
            this.chb_1_1_1.UseVisualStyleBackColor = true;
            this.chb_1_1_1.Visible = false;
            // 
            // FormEMS
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(552, 404);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnRozrahunokEMS);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormEMS";
            this.Text = "FormEMS";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.MOB_group.ResumeLayout(false);
            this.MOB_group.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbxTD;
        private System.Windows.Forms.TextBox TbxFreqDiapazon;
        private System.Windows.Forms.TextBox TbxRadius;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnRozrahunokEMS;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ChbEarth;
        private System.Windows.Forms.CheckBox ChbSRT;
        private System.Windows.Forms.CheckBox ChbRadio;
        private System.Windows.Forms.CheckBox ChbOnlyAll;
        private System.Windows.Forms.CheckBox ChbCoordinate;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.GroupBox MOB_group;
        private System.Windows.Forms.CheckBox chbMOB;
        private System.Windows.Forms.CheckBox chbBlock;
        private System.Windows.Forms.CheckBox chbIntermod;
        private System.Windows.Forms.CheckBox chbCoCH;
        private System.Windows.Forms.TextBox Mob_porog;
        private System.Windows.Forms.TextBox R_mob_inter;
        private System.Windows.Forms.TextBox R_mob;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chbGSM1800;
        private System.Windows.Forms.CheckBox chb_1_1_1;
    }
}