﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Calculation;
using XICSM.UcrfRfaNET.Calculation.ResultForm;

namespace XICSM.UcrfRfaNET.CalculationEMC
{
    //internal partial class FrmCalculationEmsResult : Calculation.ResultForm.FrmCalculationResultBase
    internal partial class FrmCalculationEmsResult : Form
    {
        private Dictionary<object, object> _dictTabItems = new Dictionary<object, object>();
        private TabsTreeColumn _tabsTree;
        public TabsTreeColumn TabsTree { get { return _tabsTree; } }
        private System.Drawing.Point _pointStationNoise;
        private System.Drawing.Point _pointLinkNoise;
        private System.Drawing.Point _pointStationSacrifice;
        private System.Drawing.Point _pointLinkSacrifice;
        /// <summary>
        /// Владелец текущей станции
        /// </summary>
        public string OwnerCurStation { get; set; }
        /// <summary>
        /// Адрес текущей станции
        /// </summary>
        public string AddressCurStation { get; set; }
        /// <summary>
        /// ID текущей станции
        /// </summary>
        public int IdCurStation { get; set; }
        /// <summary>
        /// Название таблицы
        /// </summary>
        public string TableNameCurStation { get; set; }
        /// <summary>
        /// Максимальный размер по X полотна для прорисовки
        /// </summary>
        private int _maxX;
        /// <summary>
        /// Максимальный размер по Y полотна для прорисовки
        /// </summary>
        private int _maxY;
        /// <summary>
        /// Конструктор
        /// </summary>
        public FrmCalculationEmsResult()
            : base()
        {
            InitializeComponent();
            //-----
            _tabsTree = new TabsTreeColumn();
            pnlTabsTreeColumn.Controls.Add(TabsTree);
            TabsTree.Dock = DockStyle.Fill;
            TabsTree.tabControl.SelectedIndexChanged += new EventHandler(TabsTree_TabIndexChanged);
            //-----
            _maxX = pctrBoxStations.Width;
            _maxY = pctrBoxStations.Height;
            //-----
            // Binding
            lblOwnerCurStation.DataBindings.Add("Text", this, "OwnerCurStation");
            lblAddressCurStation.DataBindings.Add("Text", this, "AddressCurStation");
            lblIdCurStation.DataBindings.Add("Text", this, "IdCurStation");
            //--------
            lblRed.Left = txtBoxStation1.Left - 2;
            lblRed.Top = txtBoxStation1.Top - 2;
            lblRed.Width = txtBoxStation1.Width + 4;
            lblRed.Height = txtBoxStation1.Height + 4;
            //--------
            lblGreen.Left = txtBoxStation2.Left - 2;
            lblGreen.Top = txtBoxStation2.Top - 2;
            lblGreen.Width = txtBoxStation2.Width + 4;
            lblGreen.Height = txtBoxStation2.Height + 4;
        }
        /// <summary>
        /// Изменилась вкладка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TabsTree_TabIndexChanged(object sender, EventArgs e)
        {
            if (_dictTabItems.ContainsKey(TabsTree.tabControl.SelectedTab))
                UpdateDetailAboutStation(_dictTabItems[TabsTree.tabControl.SelectedTab]);
            else
                UpdateDetailAboutStation(null);
        }
        /// <summary>
        /// Скрыть/отобразить доп. информацию
        /// </summary>
        private void chckBoxDetails_CheckedChanged(object sender, EventArgs e)
        {
            pnlDetails.Visible = chckBoxDetails.Checked;
            pnlResults.Height += (pnlDetails.Visible) ? -pnlDetails.Height : pnlDetails.Height;
        }
        /// <summary>
        /// Вывести на карту
        /// </summary>
        private void btnOnMap_Click(object sender, EventArgs e)
        {
            Map.TxOnMap curStn = new Map.TxOnMap(TableNameCurStation, IdCurStation);
            Map.TxsOnMap.InitMap();
            Map.TxsOnMap.AddSpecStation(curStn);
            bool isAddedStation = false;
            int indexTab = TabsTree.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TabsTree.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                foreach (object source in curItemSource)
                {
                    InterferenceEmcForeignMicrowaRow tmpSource = source as InterferenceEmcForeignMicrowaRow;
                    if ((tmpSource != null) && (tmpSource.IsChecked == true))
                    {
                        Map.TxOnMap tx = new Map.TxOnMap(tmpSource.TableName.Value, tmpSource.StationId.Value);
                        Map.TxsOnMap.InitMap();
                        Map.TxsOnMap.AddTx(tx);
                        isAddedStation = true;
                    }
                }
            }
            if (isAddedStation == true)
                Map.TxsOnMap.ShowMap();
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");
        }
        /// <summary>
        /// Изменилася выбраная станция
        /// </summary>
        public void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            UpdateDetailAboutStation(e.NewValue);
            _dictTabItems[sender] = e.NewValue;
            if(TabsTree.tabControl.SelectedTab != null)
                _dictTabItems[TabsTree.tabControl.SelectedTab] = e.NewValue;
        }
        /// <summary>
        /// Обновляет данные о станции
        /// </summary>
        /// <param name="obj">текущая выделен. строка</param>
        private void UpdateDetailAboutStation(object obj)
        {
            if (obj == null)
            {
                ClearDataAboutStation();
            }
            else
            {
                InterferenceEmcForeignMicrowaRow newRow = obj as InterferenceEmcForeignMicrowaRow;
                if (newRow == null)
                {
                    ClearDataAboutStation();
                    return;
                }
                // формируем данные
                UpdateAboutStation(newRow);
            }
        }
        /// <summary>
        /// Очищаем данные о станции
        /// </summary>
        private void ClearDataAboutStation()
        {
            txtBoxWay.Text = "";
            txtBoxStation2.Text = "";
            txtBoxStation1.Text = "";
            //-----
            _pointStationNoise = new System.Drawing.Point();
            _pointStationSacrifice = new System.Drawing.Point();
            _pointLinkNoise = new System.Drawing.Point();
            _pointLinkSacrifice = new System.Drawing.Point();
            pctrBoxStations.Invalidate();
        }
        /// <summary>
        /// Обновляет данные о выбранной станции
        /// </summary>
        /// <param name="newStationData">Данные новой станции</param>
        private void UpdateAboutStation(InterferenceEmcForeignMicrowaRow newStationData)
        {
            //------
            string newDataString = string.Format("TD: {1} дБ{0}", Environment.NewLine, newStationData.Td.Value.Round(1));
            newDataString += string.Format("Pпом: {1} дБВт{0}", Environment.NewLine, newStationData.Pi.Value.Round(1));
            newDataString += string.Format("IRF: {1} дБ{0}", Environment.NewLine, newStationData.Irf.Value.Round(1));
            newDataString += string.Format("dF: {1} МГц{0}", Environment.NewLine, Math.Abs(newStationData.FreqNoise.Value - newStationData.FreqSacrifice.Value).Round(1));
            newDataString += string.Format("S: {1} км{0}", Environment.NewLine, newStationData.Distance.Value.Round(2));
            newDataString += string.Format("L: {1} дБ{0}", Environment.NewLine, newStationData.Loss.Value.Round(1));
            txtBoxWay.Text = newDataString;
            //------
            newDataString = string.Format("KTBF: {1} дБВт{0}", Environment.NewLine, newStationData.KtbfSacrifice.Value.Round(1));
            newDataString += string.Format("Fпрм: {1} МГц{0}", Environment.NewLine, newStationData.FreqSacrifice.Value.Round(1));
            newDataString += string.Format("dFпрм: {1} МГц{0}", Environment.NewLine, newStationData.DeltaFSacrifice.Value.Round(1));
            newDataString += string.Format("Amax: {1} {0}", Environment.NewLine, newStationData.AmaxSacrifice.Value.Round(0));
            newDataString += string.Format("Gmax: {1} дБ{0}", Environment.NewLine, newStationData.GmaxSacrifice.Value.Round(1));
            newDataString += string.Format("A: {1} {0}", Environment.NewLine, newStationData.AzimuthSacrifice.Value.Round(0));
            newDataString += string.Format("G: {1} дБи{0}", Environment.NewLine, newStationData.GainSacrifice.Value.Round(1));
            txtBoxStation2.Text = newDataString;
            //------
            newDataString = string.Format("Pпрд: {1} дБВт{0}", Environment.NewLine, newStationData.PowerNoise.Value.Round(1));
            newDataString += string.Format("Fпрд: {1} МГц{0}", Environment.NewLine, newStationData.FreqNoise.Value.Round(1));
            newDataString += string.Format("dFпрд: {1} МГц{0}", Environment.NewLine, newStationData.DeltaFNoise.Value.Round(1));
            newDataString += string.Format("Amax: {1} {0}", Environment.NewLine, newStationData.AmaxNoise.Value.Round(0));
            newDataString += string.Format("Gmax: {1} дБ{0}", Environment.NewLine, newStationData.GmaxNoise.Value.Round(1));
            newDataString += string.Format("A: {1} {0}", Environment.NewLine, newStationData.AzimuthNoise.Value.Round(0));
            newDataString += string.Format("G: {1} дБи{0}", Environment.NewLine, newStationData.GainNoise.Value.Round(1));
            txtBoxStation1.Text = newDataString;
            //-----
            UpdateStations(newStationData.PositionStationNoise, newStationData.PositionLinkNoise,
                           newStationData.PositionStationSacrifice, newStationData.PositionLinkSacrifice);
        }
        /// <summary>
        /// Перепрорисовка станций
        /// </summary>
        private void UpdateStations(IMPosition posStationNoise, IMPosition posLinkNoise, IMPosition posStationSacrifice, IMPosition posLinkSacrifice)
        {
            //------
            // Ищем максим. и миним. значения
            double kLatitude = Math.Cos(posStationNoise.Lat * Math.PI / 180.0);
            double minPosY = posStationNoise.Lat;
            double minPosX = posStationNoise.Lon * kLatitude;
            double maxPosY = posStationNoise.Lat;
            double maxPosX = posStationNoise.Lon * kLatitude;
            minPosY = Math.Min(posStationSacrifice.Lat, minPosY);
            maxPosY = Math.Max(posStationSacrifice.Lat, maxPosY);
            minPosX = Math.Min(posStationSacrifice.Lon * kLatitude, minPosX);
            maxPosX = Math.Max(posStationSacrifice.Lon * kLatitude, maxPosX);
            if ((posLinkNoise.Lat != IM.NullD) && (posLinkNoise.Lon != IM.NullD))
            {
                minPosY = Math.Min(posLinkNoise.Lat, minPosY);
                maxPosY = Math.Max(posLinkNoise.Lat, maxPosY);
                minPosX = Math.Min(posLinkNoise.Lon * kLatitude, minPosX);
                maxPosX = Math.Max(posLinkNoise.Lon * kLatitude, maxPosX);
            }
            if ((posLinkSacrifice.Lat != IM.NullD) && (posLinkSacrifice.Lon != IM.NullD))
            {
                minPosY = Math.Min(posLinkSacrifice.Lat, minPosY);
                maxPosY = Math.Max(posLinkSacrifice.Lat, maxPosY);
                minPosX = Math.Min(posLinkSacrifice.Lon * kLatitude, minPosX);
                maxPosX = Math.Max(posLinkSacrifice.Lon * kLatitude, maxPosX);
            }
            double deltaY = maxPosY - minPosY;
            double deltaX = maxPosX - minPosX;
            double maxDelta = Math.Max(deltaX, deltaY);
            // Вычисляем центр координат
            double yCentre = (maxPosY - minPosY) / 2 + minPosY;
            double xCentre = (maxPosX - minPosX) / 2 + minPosX;
            // расчитываем координаты
            _pointStationNoise = new System.Drawing.Point();
            _pointStationNoise.Y = (int)TransformPositionY(posStationNoise.Lat, yCentre, maxDelta, _maxY);
            _pointStationNoise.X = (int)TransformPositionX(posStationNoise.Lon * kLatitude, xCentre, maxDelta, _maxX);
            //-----
            _pointStationSacrifice = new System.Drawing.Point();
            _pointStationSacrifice.Y = (int)TransformPositionY(posStationSacrifice.Lat, yCentre, maxDelta, _maxY);
            _pointStationSacrifice.X = (int)TransformPositionX(posStationSacrifice.Lon * kLatitude, xCentre, maxDelta, _maxX);
            //-----
            _pointLinkNoise = new System.Drawing.Point();
            if ((posLinkNoise.Lat != IM.NullD) && (posLinkNoise.Lon != IM.NullD))
            {
                _pointLinkNoise.Y = (int)TransformPositionY(posLinkNoise.Lat, yCentre, maxDelta, _maxY);
                _pointLinkNoise.X = (int)TransformPositionX(posLinkNoise.Lon * kLatitude, xCentre, maxDelta, _maxX);
            }
            //----
            _pointLinkSacrifice = new System.Drawing.Point();
            if ((posLinkSacrifice.Lat != IM.NullD) && (posLinkSacrifice.Lon != IM.NullD))
            {
                _pointLinkSacrifice.Y = (int)TransformPositionY(posLinkSacrifice.Lat, yCentre, maxDelta, _maxY);
                _pointLinkSacrifice.X = (int)TransformPositionX(posLinkSacrifice.Lon * kLatitude, xCentre, maxDelta, _maxX);
            }
            pctrBoxStations.Invalidate();
        }
        /// <summary>
        /// Трансформует одну коодинату в другую
        /// </summary>
        /// <param name="coord">координата в старой системе счисления начиная с 0</param>
        /// <param name="coordCentre">Центр координат</param>
        /// <param name="maxDelta">Максимальный значение в старой системе счисления</param>
        /// <param name="maxNewCoord">Максимальный значение в новой системе счисления</param>
        /// <returns></returns>
        private double TransformPositionX(double coord, double coordCentre, double maxDelta, double maxNewCoord)
        {
            double retVal = (coord - coordCentre) * (maxNewCoord - 30) / maxDelta;
            retVal += maxNewCoord / 2;
            return retVal;
        }
        private double TransformPositionY(double coord, double coordCentre, double maxDelta, double maxNewCoord)
        {
            double retVal = (coord - coordCentre) * (maxNewCoord - 30) / maxDelta;
            retVal = maxNewCoord / 2 - retVal;
            return retVal;
        }
        /// <summary>
        /// Перепрорисовка компоненты
        /// </summary>
        private void pctrBoxStations_Paint(object sender, PaintEventArgs e)
        {
            RepaintStations(e.Graphics);
        }
        /// <summary>
        /// Перерысовывает станции
        /// </summary>
        private void RepaintStations(Graphics curGraphics)
        {
            const int radius = 5;
            curGraphics.FillRectangle(new SolidBrush(Color.White), 0, 0, _maxX, _maxY);
            if ((_pointStationSacrifice.IsEmpty == true) || (_pointStationNoise.IsEmpty == true))
                return;
            curGraphics.DrawLine(new Pen(Color.Black, 1), _pointStationSacrifice, _pointStationNoise);
            if (_pointLinkNoise.IsEmpty == false)
            {
                curGraphics.DrawLine(new Pen(Color.Red, 1), _pointLinkNoise, _pointStationNoise);
                curGraphics.FillEllipse(new SolidBrush(Color.Red), _pointLinkNoise.X - radius, _pointLinkNoise.Y - radius, radius * 2, radius * 2);
            }
            curGraphics.FillEllipse(new SolidBrush(Color.Red), _pointStationNoise.X - radius, _pointStationNoise.Y - radius, radius * 2, radius * 2);
            //----
            if (_pointLinkSacrifice.IsEmpty == false)
            {
                curGraphics.DrawLine(new Pen(Color.Green, 1), _pointLinkSacrifice, _pointStationSacrifice);
                curGraphics.FillEllipse(new SolidBrush(Color.Green), _pointLinkSacrifice.X - radius, _pointLinkSacrifice.Y - radius, radius * 2, radius * 2);
            }
            curGraphics.FillEllipse(new SolidBrush(Color.Green), _pointStationSacrifice.X - radius, _pointStationSacrifice.Y - radius, radius * 2, radius * 2);
        }
        /// <summary>
        /// Отправить выделенные записи в телеком
        /// </summary>
        private void btnToIcst_Click(object sender, EventArgs e)
        {
            List<InterferenceEmcForeignMicrowaRow> lstRow = new List<InterferenceEmcForeignMicrowaRow>();
            int indexTab = TabsTree.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TabsTree.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                foreach (object source in curItemSource)
                {
                    InterferenceEmcForeignMicrowaRow tmpSource = source as InterferenceEmcForeignMicrowaRow;
                    if ((tmpSource != null) && (tmpSource.IsChecked == true))
                    {
                        lstRow.Add(tmpSource);
                    }
                }
            }
            if (lstRow.Count > 0)
            {
                List<RecordPtr> lstBag = new List<RecordPtr>();
                List<string> lstCaption = new List<string>();
                RecordPtr bag = IMBag.CreateTemporary(lstRow[0].TableName.Value);
                lstBag.Add(bag);
                lstCaption.Add(CLocaliz.TxT(lstRow[0].TableName.Value));
                foreach (InterferenceEmcForeignMicrowaRow row in lstRow)
                {
                    IMRecordset r = new IMRecordset("BG_" + lstRow[0].TableName.Value, IMRecordset.Mode.ReadWrite);
                    r.Select("BAG_ID,OBJ_ID");
                    r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                    r.Open();
                    r.AddNew();
                    r.Put("BAG_ID", bag.Id);
                    r.Put("OBJ_ID", row.StationId.Value);
                    r.Update();
                    if (r.IsOpen() == true)
                        r.Close();
                    r.Destroy();
                }

                //------------------
                //Добавление текущей станции в Bag
                bag = IMBag.CreateTemporary(TableNameCurStation);
                lstBag.Add(bag);
                lstCaption.Add(CLocaliz.TxT("Current station"));
                {
                    IMRecordset r = new IMRecordset("BG_" + TableNameCurStation, IMRecordset.Mode.ReadWrite);
                    r.Select("BAG_ID,OBJ_ID");
                    r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                    r.Open();
                    r.AddNew();
                    r.Put("BAG_ID", bag.Id);
                    r.Put("OBJ_ID", IdCurStation);
                    r.Update();
                    if (r.IsOpen() == true)
                        r.Close();
                    r.Destroy();
                }


                IMBag.Display2(lstBag.ToArray(), lstCaption.ToArray(), null, null);
                foreach (RecordPtr bagItem in lstBag)
                {
                    IMBag.DeleteBag(bagItem);
                }
            }
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");

        }
    }
}
