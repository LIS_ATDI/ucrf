﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using System.Globalization;

namespace XICSM.UcrfRfaNET
{
   public partial class FormEMS : FBaseForm
   {
      private int[] _tableListIs;
      private string _tableName = string.Empty;
      public static NumberFormatInfo provider = new NumberFormatInfo();

      public FormEMS(string tableName, params int[] tableId)
      {
         InitializeComponent();
         _tableName = tableName;
         _tableListIs = tableId;
         FillsData();
         if ((_tableListIs == null) || (_tableListIs.Length == 0))
             throw new Exception("Error input data");
      }

      #region METHODS

      private void FillsData()
      {
         Station tempStation = new Station(_tableName, _tableListIs[0]);
         if (_tableName.Equals("MOB_STATION2"))
         {
            TbxTD.Text = "15";
            TbxFreqDiapazon.Text = (tempStation._bwTX * 2.0).ToString();
            TbxRadius.Text = "50";
            chbMOB.Visible = true;
            chbGSM1800.Visible = true;
            return;
         }

         if (_tableName.Equals("EARTH_STATION"))
         {
            FiilsRadius(tempStation);
            double td = 0.3;
            TbxTD.Text = td.ToString();
            TbxFreqDiapazon.Text = (tempStation._bwTX * 2.0).ToString();
            return;
         }
         if (_tableName.Equals("MICROWA") ||
             _tableName.Equals(ICSMTbl.FxLkClink))
         {
            FiilsRadius(tempStation.stationA);
            TbxTD.Text = "3";
            TbxFreqDiapazon.Text = (tempStation.stationA._bwTX * 2.0).ToString();
            return;
         }
      }

      private void FiilsRadius(Station tempStation)
      {
         double value = 0.0;
         try { value = tempStation.STFrequncyTX[0] / Math.Pow(10.0, 3.0); }
         catch { }

         if (value >= 0.0 && value <= 7.0)
         {
            TbxRadius.Text = "250"; return;
         }
         if (value >= 7.0 && value <= 12.0)
         {
            TbxRadius.Text = "200"; return;
         }
         if (value >= 12.0 && value <= 17.0)
         {
            TbxRadius.Text = "150"; return;
         }
         if (value >= 17.0 && value <= 25.0)
         {
            TbxRadius.Text = "100"; return;
         }
         if (value >= 25.0 && value <= 40.0)
         {
            TbxRadius.Text = "50"; return;
         }
      }

      #endregion

      #region EVENTS

      private void BtnRozrahunokEMS_Click(object sender, EventArgs e)
      {
         Plugin.curStation = null;
         Plugin.listData = null;
         if (TbxRadius.Text.Length > 0 && TbxFreqDiapazon.Text.Length > 0 &&
            TbxTD.Text.Length > 0)
         {
            double radius, freqDiapazon, compareTD;

            radius = ConvertType.ToDouble(TbxRadius.Text, 0);
            freqDiapazon = ConvertType.ToDouble(TbxFreqDiapazon.Text, 0);
            compareTD = ConvertType.ToDouble(TbxTD.Text, 0);

            List<Station> stationList = new List<Station>();
            foreach (int id in _tableListIs)
            {
                Station station = new Station(_tableName, id);
                if ((station._typeOfStation == Station.TypeOfStation.Radio) ||
                    (station._typeOfStation == Station.TypeOfStation.ForeignLink))
                {
                    if (!station.ChecsOnNull(station.stationA, station._typeOfStation) ||
                        !station.ChecsOnNull(station.stationB, station._typeOfStation))
                    {
                        //TODO Добавить отображение реальной ошибки
                        MessageBox.Show("Sorry, but selected station has invalid data",
                                        "Sorry, but selected station has invalid data", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    if (!station.ChecsOnNull(station, station._typeOfStation))
                    {
                        //TODO Добавить отображение реальной ошибки
                        MessageBox.Show("Sorry, but selected station has invalid data",
                                        "Sorry, but selected station has invalid data", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                }
                station._show = true;
                stationList.Add(station);
            }
            ViborkaEMS viborkaEms = new ViborkaEMS(stationList.ToArray(), radius, freqDiapazon, compareTD,
               ChbSRT.Checked, ChbEarth.Checked, ChbRadio.Checked, ChbOnlyAll.Checked, ChbCoordinate.Checked);
            viborkaEms.Calculate();
            viborkaEms.ShowResult();
             // вотсдесь по идее должне быть и новый расчет Для средств мобильной связи.

            if ((chbMOB.Checked)||(chbGSM1800.Checked))  // итак очень важный момент его включаем только при необходимости расчитать совместно MOB и СРТ
            {
                double radiusMob = 0, radiusInter = 0, radiusBlock = 0, PIMob = 0, PIBlock = 0, PIInter = 0;
                if (chbCoCH.Checked)
                {
                    if (R_mob.TextLength > 0 && Mob_porog.TextLength > 0)
                    {
                        radiusMob = ConvertType.ToDouble(R_mob.Text, 0);
                        PIMob = ConvertType.ToDouble(Mob_porog.Text, 0);
                    }
                    else
                    {
                        MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                if (chbIntermod.Checked)
                {
                    if (R_mob_inter.TextLength > 0 && Mob_porog.TextLength > 0)
                    {
                        radiusInter = ConvertType.ToDouble(R_mob_inter.Text, 0);
                        PIMob = ConvertType.ToDouble(Mob_porog.Text, 0);
                    }
                    else
                    {
                        MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                if (chbBlock.Checked)
                {
                    if (R_mob_inter.TextLength > 0 && Mob_porog.TextLength > 0)
                    {
                        radiusBlock = ConvertType.ToDouble(R_mob_inter.Text, 0);
                        PIMob = ConvertType.ToDouble(Mob_porog.Text, 0);
                    }
                    else
                    {
                        MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                if (!chbCoCH.Checked && !chbIntermod.Checked && !chbBlock.Checked)
                {
                    return;
                }
                // до данного момента была проверенна коректность параметров.
                // начало расчетов 3G Напимним что в 3G источник помехи станция СРТ а жертва станция UMTS
                if (chbMOB.Checked)
                {
                    CalculationVRR.ViborkaVRR viborksEMS_MOB = new CalculationVRR.ViborkaVRR(_tableListIs, radiusMob, radiusBlock, radiusInter, PIMob, chbCoCH.Checked, chbIntermod.Checked, chbBlock.Checked,true, 2000, 1920);
                    viborksEMS_MOB.Calculate_mob_srt(1980, 2000);
                    viborksEMS_MOB.ShowResultForm();
                }
                // конец расчетов и демонстраци 3G

                // начало расчетов c GSM пока не отлаженно.
                if (chbGSM1800.Checked)
                {
                    CalculationVRR.ViborkaVRR viborksEMS_MOB = new CalculationVRR.ViborkaVRR(_tableListIs, radiusMob, radiusBlock, radiusInter, PIMob, chbCoCH.Checked, chbIntermod.Checked, chbBlock.Checked, chb_1_1_1.Checked, 1880, 1710); // пока не отлаженно. А вобще сбда закидываются станции для которых идет расчет. 
                    viborksEMS_MOB.Calculate_mob_srt(1785, 1805);
                    viborksEMS_MOB.ShowResultForm();
                }
                // конец расчетов c GSM
            }
             // вот тут и закончился расчет 3G

         }
      }

      #endregion

      private void Tbx_KeyPressOnDigit(object sender, KeyPressEventArgs e)
      {
         // Если это не цифра.
         if ((!Char.IsDigit(e.KeyChar)) && (e.KeyChar != 45) && (e.KeyChar != '\b'))
         {
            // Запрет на ввод более одной десятичной точки.
            TextBox tb = (TextBox)sender;
            char separator = provider.CurrencyGroupSeparator[0];
            if (e.KeyChar != separator || tb.Text.IndexOf(separator) != -1)
            {
               e.Handled = true;
            }
         }
      }

      private void BtnCancel_Click(object sender, EventArgs e)
      {
         Plugin.curStation = null;
         this.Close();
      }

      private void chbMOB_CheckedChanged(object sender, EventArgs e)
      {
          if (chbMOB.Checked)
          {
              MOB_group.Visible = true;
              chbGSM1800.Checked = false; 
              MOB_group.Text = "Параметри для розрахунку завад UMTS 1920-1980";
              label7.Text = "Для 1980-2000";
              chb_1_1_1.Visible = false;
          }
          else 
          {
              if (!chbGSM1800.Checked) MOB_group.Visible = false;
          }
      }

      private void chbGSM1800_CheckedChanged(object sender, EventArgs e)
      {
          if (chbGSM1800.Checked)
          {
              MOB_group.Visible = true;
              chbMOB.Checked = false;
              MOB_group.Text = "Параметри для розрахунку завад GSM 1710-1880";
              label7.Text = "Для 1785-1805";
              chb_1_1_1.Visible = true;
          }
          else
          {
               if (!chbMOB.Checked) MOB_group.Visible = false;
          }
      }
   }
}
