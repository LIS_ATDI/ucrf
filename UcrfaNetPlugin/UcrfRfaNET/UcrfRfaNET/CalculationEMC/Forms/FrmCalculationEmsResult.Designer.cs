﻿namespace XICSM.UcrfRfaNET.CalculationEMC
{
   partial class FrmCalculationEmsResult
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.grpBoxCurStationData = new System.Windows.Forms.GroupBox();
          this.lblIdCurStation = new System.Windows.Forms.Label();
          this.lblAddressCurStation = new System.Windows.Forms.Label();
          this.lblOwnerCurStation = new System.Windows.Forms.Label();
          this.label3 = new System.Windows.Forms.Label();
          this.label2 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.chckBoxDetails = new System.Windows.Forms.CheckBox();
          this.btnOnMap = new System.Windows.Forms.Button();
          this.btnToIcst = new System.Windows.Forms.Button();
          this.pnlDetails = new System.Windows.Forms.Panel();
          this.pctrBoxStations = new System.Windows.Forms.PictureBox();
          this.txtBoxWay = new System.Windows.Forms.TextBox();
          this.txtBoxStation2 = new System.Windows.Forms.TextBox();
          this.txtBoxStation1 = new System.Windows.Forms.TextBox();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.lblRed = new System.Windows.Forms.Label();
          this.lblGreen = new System.Windows.Forms.Label();
          this.pnlTabsTreeColumn = new System.Windows.Forms.Panel();
          this.pnlResults = new System.Windows.Forms.Panel();
          this.grpBoxCurStationData.SuspendLayout();
          this.pnlDetails.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pctrBoxStations)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.pnlResults.SuspendLayout();
          this.SuspendLayout();
          // 
          // grpBoxCurStationData
          // 
          this.grpBoxCurStationData.Controls.Add(this.lblIdCurStation);
          this.grpBoxCurStationData.Controls.Add(this.lblAddressCurStation);
          this.grpBoxCurStationData.Controls.Add(this.lblOwnerCurStation);
          this.grpBoxCurStationData.Controls.Add(this.label3);
          this.grpBoxCurStationData.Controls.Add(this.label2);
          this.grpBoxCurStationData.Controls.Add(this.label1);
          this.grpBoxCurStationData.Dock = System.Windows.Forms.DockStyle.Top;
          this.grpBoxCurStationData.Location = new System.Drawing.Point(0, 0);
          this.grpBoxCurStationData.Name = "grpBoxCurStationData";
          this.grpBoxCurStationData.Size = new System.Drawing.Size(762, 70);
          this.grpBoxCurStationData.TabIndex = 0;
          this.grpBoxCurStationData.TabStop = false;
          // 
          // lblIdCurStation
          // 
          this.lblIdCurStation.AutoSize = true;
          this.lblIdCurStation.Location = new System.Drawing.Point(94, 48);
          this.lblIdCurStation.Name = "lblIdCurStation";
          this.lblIdCurStation.Size = new System.Drawing.Size(35, 13);
          this.lblIdCurStation.TabIndex = 5;
          this.lblIdCurStation.Text = "label6";
          // 
          // lblAddressCurStation
          // 
          this.lblAddressCurStation.AutoSize = true;
          this.lblAddressCurStation.Location = new System.Drawing.Point(94, 31);
          this.lblAddressCurStation.Name = "lblAddressCurStation";
          this.lblAddressCurStation.Size = new System.Drawing.Size(35, 13);
          this.lblAddressCurStation.TabIndex = 4;
          this.lblAddressCurStation.Text = "label5";
          // 
          // lblOwnerCurStation
          // 
          this.lblOwnerCurStation.AutoSize = true;
          this.lblOwnerCurStation.Location = new System.Drawing.Point(94, 14);
          this.lblOwnerCurStation.Name = "lblOwnerCurStation";
          this.lblOwnerCurStation.Size = new System.Drawing.Size(35, 13);
          this.lblOwnerCurStation.TabIndex = 3;
          this.lblOwnerCurStation.Text = "label4";
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(7, 48);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(21, 13);
          this.label3.TabIndex = 2;
          this.label3.Text = "ID:";
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(7, 31);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(48, 13);
          this.label2.TabIndex = 1;
          this.label2.Text = "Address:";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(7, 14);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(41, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Owner:";
          // 
          // chckBoxDetails
          // 
          this.chckBoxDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.chckBoxDetails.AutoSize = true;
          this.chckBoxDetails.Checked = true;
          this.chckBoxDetails.CheckState = System.Windows.Forms.CheckState.Checked;
          this.chckBoxDetails.Location = new System.Drawing.Point(3, 297);
          this.chckBoxDetails.Name = "chckBoxDetails";
          this.chckBoxDetails.Size = new System.Drawing.Size(58, 17);
          this.chckBoxDetails.TabIndex = 1;
          this.chckBoxDetails.Text = "Details";
          this.chckBoxDetails.UseVisualStyleBackColor = true;
          this.chckBoxDetails.CheckedChanged += new System.EventHandler(this.chckBoxDetails_CheckedChanged);
          // 
          // btnOnMap
          // 
          this.btnOnMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnOnMap.Location = new System.Drawing.Point(684, 291);
          this.btnOnMap.Name = "btnOnMap";
          this.btnOnMap.Size = new System.Drawing.Size(75, 23);
          this.btnOnMap.TabIndex = 3;
          this.btnOnMap.Text = "On map";
          this.btnOnMap.UseVisualStyleBackColor = true;
          this.btnOnMap.Click += new System.EventHandler(this.btnOnMap_Click);
          // 
          // btnToIcst
          // 
          this.btnToIcst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnToIcst.Location = new System.Drawing.Point(603, 291);
          this.btnToIcst.Name = "btnToIcst";
          this.btnToIcst.Size = new System.Drawing.Size(75, 23);
          this.btnToIcst.TabIndex = 2;
          this.btnToIcst.Text = "To ICST";
          this.btnToIcst.UseVisualStyleBackColor = true;
          this.btnToIcst.Click += new System.EventHandler(this.btnToIcst_Click);
          // 
          // pnlDetails
          // 
          this.pnlDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.pnlDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
          this.pnlDetails.Controls.Add(this.pctrBoxStations);
          this.pnlDetails.Controls.Add(this.txtBoxWay);
          this.pnlDetails.Controls.Add(this.txtBoxStation2);
          this.pnlDetails.Controls.Add(this.txtBoxStation1);
          this.pnlDetails.Controls.Add(this.pictureBox1);
          this.pnlDetails.Controls.Add(this.lblRed);
          this.pnlDetails.Controls.Add(this.lblGreen);
          this.pnlDetails.Location = new System.Drawing.Point(0, 399);
          this.pnlDetails.Name = "pnlDetails";
          this.pnlDetails.Size = new System.Drawing.Size(762, 206);
          this.pnlDetails.TabIndex = 2;
          // 
          // pctrBoxStations
          // 
          this.pctrBoxStations.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
          this.pctrBoxStations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pctrBoxStations.Location = new System.Drawing.Point(3, 3);
          this.pctrBoxStations.Name = "pctrBoxStations";
          this.pctrBoxStations.Size = new System.Drawing.Size(200, 200);
          this.pctrBoxStations.TabIndex = 4;
          this.pctrBoxStations.TabStop = false;
          this.pctrBoxStations.WaitOnLoad = true;
          this.pctrBoxStations.Paint += new System.Windows.Forms.PaintEventHandler(this.pctrBoxStations_Paint);
          // 
          // txtBoxWay
          // 
          this.txtBoxWay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.txtBoxWay.BackColor = System.Drawing.Color.White;
          this.txtBoxWay.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.txtBoxWay.Location = new System.Drawing.Point(433, 43);
          this.txtBoxWay.Multiline = true;
          this.txtBoxWay.Name = "txtBoxWay";
          this.txtBoxWay.ReadOnly = true;
          this.txtBoxWay.Size = new System.Drawing.Size(114, 108);
          this.txtBoxWay.TabIndex = 2;
          this.txtBoxWay.TabStop = false;
          this.txtBoxWay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // txtBoxStation2
          // 
          this.txtBoxStation2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.txtBoxStation2.BackColor = System.Drawing.Color.White;
          this.txtBoxStation2.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.txtBoxStation2.Location = new System.Drawing.Point(620, 34);
          this.txtBoxStation2.Multiline = true;
          this.txtBoxStation2.Name = "txtBoxStation2";
          this.txtBoxStation2.ReadOnly = true;
          this.txtBoxStation2.Size = new System.Drawing.Size(130, 135);
          this.txtBoxStation2.TabIndex = 4;
          this.txtBoxStation2.TabStop = false;
          this.txtBoxStation2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // txtBoxStation1
          // 
          this.txtBoxStation1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.txtBoxStation1.BackColor = System.Drawing.Color.White;
          this.txtBoxStation1.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.txtBoxStation1.ForeColor = System.Drawing.SystemColors.WindowText;
          this.txtBoxStation1.Location = new System.Drawing.Point(231, 34);
          this.txtBoxStation1.Multiline = true;
          this.txtBoxStation1.Name = "txtBoxStation1";
          this.txtBoxStation1.ReadOnly = true;
          this.txtBoxStation1.Size = new System.Drawing.Size(130, 135);
          this.txtBoxStation1.TabIndex = 1;
          this.txtBoxStation1.TabStop = false;
          this.txtBoxStation1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // pictureBox1
          // 
          this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.pictureBox1.Image = global::XICSM.UcrfRfaNET.Properties.Resources.cloud;
          this.pictureBox1.Location = new System.Drawing.Point(367, 13);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(247, 167);
          this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
          this.pictureBox1.TabIndex = 5;
          this.pictureBox1.TabStop = false;
          // 
          // lblRed
          // 
          this.lblRed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.lblRed.BackColor = System.Drawing.Color.Brown;
          this.lblRed.Location = new System.Drawing.Point(219, 13);
          this.lblRed.Name = "lblRed";
          this.lblRed.Size = new System.Drawing.Size(123, 74);
          this.lblRed.TabIndex = 0;
          // 
          // lblGreen
          // 
          this.lblGreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.lblGreen.BackColor = System.Drawing.Color.ForestGreen;
          this.lblGreen.Location = new System.Drawing.Point(617, 13);
          this.lblGreen.Name = "lblGreen";
          this.lblGreen.Size = new System.Drawing.Size(123, 75);
          this.lblGreen.TabIndex = 3;
          // 
          // pnlTabsTreeColumn
          // 
          this.pnlTabsTreeColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.pnlTabsTreeColumn.Location = new System.Drawing.Point(3, 3);
          this.pnlTabsTreeColumn.Name = "pnlTabsTreeColumn";
          this.pnlTabsTreeColumn.Size = new System.Drawing.Size(756, 282);
          this.pnlTabsTreeColumn.TabIndex = 0;
          // 
          // pnlResults
          // 
          this.pnlResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.pnlResults.BackColor = System.Drawing.SystemColors.Control;
          this.pnlResults.Controls.Add(this.pnlTabsTreeColumn);
          this.pnlResults.Controls.Add(this.btnToIcst);
          this.pnlResults.Controls.Add(this.chckBoxDetails);
          this.pnlResults.Controls.Add(this.btnOnMap);
          this.pnlResults.Location = new System.Drawing.Point(0, 76);
          this.pnlResults.Name = "pnlResults";
          this.pnlResults.Size = new System.Drawing.Size(762, 317);
          this.pnlResults.TabIndex = 1;
          // 
          // FrmCalculationEmsResult
          // 
          this.ClientSize = new System.Drawing.Size(762, 613);
          this.Controls.Add(this.pnlResults);
          this.Controls.Add(this.pnlDetails);
          this.Controls.Add(this.grpBoxCurStationData);
          this.MinimumSize = new System.Drawing.Size(770, 640);
          this.Name = "FrmCalculationEmsResult";
          this.Text = "Result of EMS calculation";
          this.grpBoxCurStationData.ResumeLayout(false);
          this.grpBoxCurStationData.PerformLayout();
          this.pnlDetails.ResumeLayout(false);
          this.pnlDetails.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pctrBoxStations)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.pnlResults.ResumeLayout(false);
          this.pnlResults.PerformLayout();
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox grpBoxCurStationData;
      private System.Windows.Forms.CheckBox chckBoxDetails;
      private System.Windows.Forms.Button btnOnMap;
      private System.Windows.Forms.Button btnToIcst;
      private System.Windows.Forms.Panel pnlDetails;
      private System.Windows.Forms.Label lblIdCurStation;
      private System.Windows.Forms.Label lblAddressCurStation;
      private System.Windows.Forms.Label lblOwnerCurStation;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox txtBoxStation1;
      private System.Windows.Forms.TextBox txtBoxStation2;
      private System.Windows.Forms.TextBox txtBoxWay;
      private System.Windows.Forms.PictureBox pctrBoxStations;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Panel pnlTabsTreeColumn;
      private System.Windows.Forms.Panel pnlResults;
      private System.Windows.Forms.Label lblRed;
      private System.Windows.Forms.Label lblGreen;
   }
}