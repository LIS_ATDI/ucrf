﻿using System;
using System.Windows;
using System.Collections.Generic;
using ICSM;
using LisUtility;
using NSPosition;
using System.Text;

namespace XICSM.UcrfRfaNET
{
   public class Station
   {
      public enum TypeOfStation
      {
         SRT,
         Earth,
         Radio,
         ForeignLink,
         //ForeignStation
      }

      private int tblID;

      public int _tableID
      {
         get { return tblID; }
         set
         {
            if (value == 0)
               value = 0;
            tblID = value;
         }
      }
      // id станции
      public double _latitude; // широта
      public double _longitude; // довгота
      public List<double> STFrequncyRX = new List<double>(); // частота прийомників (поле AssignedFrequencies.RX_FREQ)
      public List<double> STFrequncyTX = new List<double>(); // частота передатчиків (поле AssignedFrequencies.TX_FREQ)
      public List<double> STIRFList = new List<double>(); // значення IRF
      public List<double> STL525List = new List<double>(); // значення L525
      public List<double> STTD = new List<double>(); // знеачення TD
      public List<double> CURTX = new List<double>(); // частота на якій максимальне ТД 
      public List<double> CURRX = new List<double>(); // частота на якій максимальне ТД       
      public int _countCalcTD; // count calculated of TD;

      public double _aboveSeaLevel; // висота над рівнем моря в м
      public double AboveEarthLevel { get; set; } // висота над рівнем землі в м
      public double _bwTX; // ширина смуги частот передавача
      public double _bwRX; // ширина смуги частот приймача
      public double _power; // потужність
      public double _lFT; // ослаблення фідера передатчика
      public double _lFR; // ослаблення фідера приймача
      public double _kdbf; // рівень власних шумів прийомника
      public double _TH; // чувствительность, дБм 
      public double _s_n; //  отношение сигнал шум необходимое для демодуляции сигнала расчитаное на основании модуляции.
      public double _carrier; // уровень полезного сигнала на входе приемника без учета рельефа.

      //public string _identificator = string.Empty; // ідентифікатор станції
      public string _owner = string.Empty; // власник станції
      public string _remark = string.Empty; // адресса станції      
      public string Name { get; set; }  //Название станции
      public string Num_Dozv { get; set; }  //Вспомогательное поле (номер разрешения)
      public DateTime Dozv_Start { get; set; }  //Дата початку дії
      public string Num_Visn { get; set; }  //Вспомогательное поле (номер заключения)
      public string Nom_Freq { get; set; }  //Вспомогательное поле (номинал чатосты)
      public string NameOwner { get; set; }  //Вспомогательное поле (Наименование владельца)
      public string AddressOwner { get; set; }  //Вспомогательное поле (Адрес владельца)
      public string NameOwner2 { get; set; }  //Вспомогательное поле (Наименование владельца) (для РРС)
      public string AddressOwner2 { get; set; }  //Вспомогательное поле (Адрес владельца) (для РРС)
      public string NameFilial { get; set; }  //Вспомогательное поле (Имя филиала)
      public string Table_ { get; set; }  //Вспомогательное поле (имя таблицы)
      public int ID_ { get; set; }  //Вспомогательное поле

      public string _aDiagH = string.Empty; // діаграма по горизонталі
      public string _bDiagV = string.Empty; // діаграма по вертикалі
      public string _antenDIAGA = string.Empty; // діаграма станції
      public double _azimuth; // азимут карточки(з станції)
      public double _bElevation; // угол в з карточки
      public double _noise_t; // шумовая температура
      public double _diametrE; // диаметр антенны передатчика
      public double _diametrR; // диаметр антенны приемника
      public double _gMax; // максімальний коефіцієнт підсилення антени      
      public TypeOfStation _typeOfStation;
      public Station stationA;
      public Station stationB;
      public bool pomehaContain = false; // значить помеха присутня
      public bool zhertvaContain = false; // значить жертва присутня
      
      public List<double> PI = new List<double>();
      public List<double> F_ = new List<double>();
      public List<double> F_int = new List<double>();
      public List<double> GT = new List<double>();
      public List<double> GR = new List<double>();
      public List<double> A1 = new List<double>();
      public List<double> A2 = new List<double>();
      public List<double> B1 = new List<double>();
      public List<double> B2 = new List<double>();
      public List<double> IRF = new List<double>();
      public List<double> DISTANCE = new List<double>(); // відстань d
      public int _ownerID; // ID овнера
      public bool _show = false; // the station is displayed or don't displayed
      public string _table = string.Empty;  // Имя таблицы

      public Station()
      {
         Name = "";
      }

      public Station(string tableName, int TableID) : this()
      {
         stationA = new Station();
         stationB = new Station();
         _tableID = TableID;
         _table = tableName;
         CreateStation(tableName);
      }

      #region METHODS

      public  static string GetAdressInformation(String POSTCODE, String PROVINCE, String SUBPROVINCE, String CITY, String ADDRESS, String TYPE_CITY)
      {
          String Temp = "";

          if (POSTCODE.Trim() != "")
          {
              Temp = POSTCODE + ", ";
          }

          if ((PROVINCE.Trim() != "") && (PROVINCE.Trim() != "АР Крим"))
          {
              Temp = Temp + PROVINCE + " обл.";
          }
          else
          {
              Temp = PROVINCE;
          }
          if (SUBPROVINCE.Trim() != "")
          {
              if (PROVINCE.Trim() != "")
              {
                  Temp = Temp + ", " + SUBPROVINCE + " р-н.";
              }
              if (PROVINCE.Trim() == "")
              {
                  Temp = Temp + SUBPROVINCE + " р-н.";
              }
          }
          if (CITY.Trim() != "")
          {
              if (!TYPE_CITY.Trim().Equals("обл") && !TYPE_CITY.Trim().Equals("обл.") && !TYPE_CITY.Trim().Equals("р-н") && !TYPE_CITY.Trim().Equals("р-н."))      
              {
                  if (!TYPE_CITY.EndsWith("."))
                  {
                      if (Temp.Trim() != "")
                      {
                          Temp = Temp + ", " + TYPE_CITY.TrimEnd() + ". " + CITY + ".";
                      }
                      else
                      {
                          Temp = Temp + TYPE_CITY.TrimEnd() + ". " + CITY + ".";
                      }
                  }
                  else
                  {
                      if (Temp.Trim() != "")
                      {
                          Temp = Temp + ", " + TYPE_CITY.TrimEnd() + CITY + ".";
                      }
                      else
                      {
                          Temp = Temp +  TYPE_CITY.TrimEnd() + CITY + ".";
                      }
                  }
              }
            
          }
          if (ADDRESS.Trim() != "")
          {
              if (Temp.Trim() != "")
              {
                  Temp = Temp + "," + ADDRESS;
              }
              if (Temp.Trim() == "")
              {
                  Temp = ADDRESS;
              }
          }
          return Temp;

      }

      // initialization selected station
      public static void GetOwnerData(Station curStat, ref string OwnerRemark)
      {
         if (curStat._typeOfStation == TypeOfStation.SRT)
         {
            IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadOnly);
            r.Select("ID,Position.REMARK,Owner.NAME,Position.REMARK,Position.ADDRESS,Position.POSTCODE,Position.PROVINCE,Position.SUBPROVINCE,Position.CITY,Position.City.CUST_TXT4");//NAME,Owner.NAME,Owner.ADDRESS,
            r.SetWhere("ID", IMRecordset.Operation.Eq, curStat._tableID);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               OwnerRemark = r.GetS("Owner.NAME") + "\r\n";
               OwnerRemark += GetAdressInformation(r.GetS("Position.POSTCODE"), r.GetS("Position.PROVINCE"), r.GetS("Position.SUBPROVINCE"), r.GetS("Position.CITY"), r.GetS("Position.ADDRESS"), r.GetS("Position.City.CUST_TXT4"));
            }
            r.Close();
            r.Destroy();
         }
         if (curStat._typeOfStation == TypeOfStation.Earth)
         {
            IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
            r.Select("Group.Antenna.Station.ID,Group.Antenna.Station.Owner.NAME,Group.Antenna.Station.Position.REMARK,Group.Antenna.Station.Position.ADDRESS,Group.Antenna.Station.Position.POSTCODE,Group.Antenna.Station.Position.PROVINCE,Group.Antenna.Station.Position.SUBPROVINCE,Group.Antenna.Station.Position.CITY,Group.Antenna.Station.Position.City.CUST_TXT4");
            r.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, curStat._tableID);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               OwnerRemark = r.GetS("Group.Antenna.Station.Owner.NAME") + "\r\n";
               OwnerRemark += GetAdressInformation(r.GetS("Group.Antenna.Station.Position.POSTCODE"), r.GetS("Group.Antenna.Station.Position.PROVINCE"), r.GetS("Group.Antenna.Station.Position.SUBPROVINCE"), r.GetS("Group.Antenna.Station.Position.CITY"), r.GetS("Group.Antenna.Station.Position.ADDRESS"), r.GetS("Group.Antenna.Station.Position.City.CUST_TXT4"));
                
            }
            r.Close();
            r.Destroy();
         }
         if (curStat._typeOfStation == TypeOfStation.Radio)
         {
            IMRecordset r = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
            r.Select("ID,Owner.NAME,StationA.Position.REMARK,StationB.Position.REMARK,StationA.Position.ADDRESS,StationB.Position.ADDRESS,StationA.Position.POSTCODE,StationB.Position.POSTCODE,StationA.Position.PROVINCE,StationB.Position.PROVINCE,StationA.Position.SUBPROVINCE,StationB.Position.SUBPROVINCE,StationA.Position.CITY,StationB.Position.CITY,StationA.Position.City.CUST_TXT4,StationB.Position.City.CUST_TXT4"); 
            r.SetWhere("ID", IMRecordset.Operation.Eq, curStat._tableID);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               OwnerRemark = r.GetS("Owner.NAME") + "\r\n";
               OwnerRemark += GetAdressInformation(r.GetS("StationA.Position.POSTCODE"), r.GetS("StationA.Position.PROVINCE"), r.GetS("StationA.Position.SUBPROVINCE"), r.GetS("StationA.Position.CITY"), r.GetS("StationA.Position.ADDRESS"), r.GetS("StationA.Position.City.CUST_TXT4")) + "\r\n";
               OwnerRemark += GetAdressInformation(r.GetS("StationB.Position.POSTCODE"), r.GetS("StationB.Position.PROVINCE"), r.GetS("StationB.Position.SUBPROVINCE"), r.GetS("StationB.Position.CITY"), r.GetS("StationB.Position.ADDRESS"), r.GetS("StationB.Position.City.CUST_TXT4"));

            }
            r.Close();
            r.Destroy();
         }         
      }

      private void CreateStation(string tableName)
      {
         AboveEarthLevel = 3.0;
         if (tableName.Equals(ICSMTbl.itblMobStation2))
         {
            bool baseFilled = false;
            IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadOnly);
            r.Select("ID,Position.LONGITUDE,Position.LATITUDE,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,Equipment.BW,RxEquipment.BW,PWR_ANT,Equipment.ANALOG_DIGITAL,Position.ASL,GAIN,TX_LOSSES,RX_LOSSES,Equipment.KTBF,AGL,Antenna.DIAGA,Antenna.DIAGH,Antenna.DIAGV,AZIMUTH,ELEVATION,OWNER_ID,DESIG_EMISSION");//NAME,Owner.NAME,Owner.ADDRESS,
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);            
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               STFrequncyRX.Add(r.GetD("AssignedFrequencies.RX_FREQ"));
               STFrequncyTX.Add(r.GetD("AssignedFrequencies.TX_FREQ"));

               if (!baseFilled)
               {
                  _longitude = r.GetD("Position.LONGITUDE");
                  _latitude = r.GetD("Position.LATITUDE");
                  string aaa = r.GetS("DESIG_EMISSION");
                  _bwTX = ParserForSRTBW(aaa);
                  _bwRX = _bwTX;
                  if (_bwTX == -1)
                  {
                     _bwTX = r.GetD("Equipment.BW");
                     _bwRX = r.GetD("RxEquipment.BW");

                     if (_bwTX == IM.NullD)
                     {
                        if (_bwRX != IM.NullD)
                           _bwTX = _bwRX;
                        else
                           _bwTX = 10000;
                     }

                     if (_bwRX == IM.NullD)
                        _bwRX = _bwTX;
                     _bwTX /= 1000.0;
                     _bwRX /= 1000.0;
                  }                  

                  _power = r.GetD("PWR_ANT") + 30;
                  //_identificator = r.GetS("NAME");
                  //_owner = r.GetS("Owner.NAME");
                  //_adress = r.GetS("Owner.ADDRESS");
                  double agl = r.GetD("AGL"); // висота над землёй
                  AboveEarthLevel = agl;
                  _aboveSeaLevel = r.GetD("Position.ASL") + agl;
                  _antenDIAGA = r.GetS("Antenna.DIAGA"); // діаграма станції
                  _aDiagH = r.GetS("Antenna.DIAGH"); // діаграма по горизонталі               
                  _bDiagV = r.GetS("Antenna.DIAGV"); // діаграма по вертикалі
                  _azimuth = r.GetD("AZIMUTH"); // азимут карточки(з станції)
                  _bElevation = r.GetD("ELEVATION"); // угол в з карточки  
                  _gMax = r.GetD("GAIN"); // максімальний коефіцієнт підсилення антени
                  _lFT = r.GetD("TX_LOSSES");  // ослаблення фідера передатчика  
                  _lFR = r.GetD("RX_LOSSES"); // ослаблення фідера приймача
                  _kdbf = r.GetD("Equipment.KTBF"); // рівень власних шумів прийомника
                  _ownerID = r.GetI("OWNER_ID"); // ID овнера
                  _typeOfStation = TypeOfStation.SRT;                  

                  baseFilled = true;
               }
            }
            r.Close();
            r.Destroy();
            return;
         }

         if (tableName.Equals(ICSMTbl.itblEarthStation))
         {
            bool baseFilled = false;
            IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
            r.Select("FREQ,Group.BDWDTH,Group.NOISE_T,Group.Antenna.DIAMETER,Group.EMI_RCP,Group.Antenna.Station.LONGITUDE,Group.Antenna.Station.LATITUDE,Group.Antenna.Station.AZM_TO,Group.Antenna.Station.ALTITUDE,Group.Antenna.Station.ELEV_MIN,Group.PWR_MAX,Group.Antenna.Station.Owner.NAME,Group.Antenna.Station.Owner.ADDRESS,Group.Antenna.Station.Owner.REPRESENT,Group.Antenna.Station.OWNER_ID,Group.Antenna.EMI_RCP");
            r.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, _tableID);

            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               //string emisOrRec = r.GetS("Group.EMI_RCP"); // узнаем що це - передатчик чи прийомник
               string emisOrRec = r.GetS("Group.Antenna.EMI_RCP"); // узнаем що це - передатчик чи прийомник
               double freq = r.GetD("FREQ"); // частота чи передатчика чи прийомника
               double _bwX = r.GetD("Group.BDWDTH") / 1000; // ширина спектра передатчика и прийомника      

               if (emisOrRec.Equals("E"))
               {
                  STFrequncyTX.Add(freq); // частота передатчиків (поле AssignedFrequencies.TX_FREQ)
                  _bwTX = _bwX; //r.GetD("BW"); // ширина смуги частот передавача
                  if (_bwTX == IM.NullD || _bwTX <= 0)
                     _bwTX = 2.0;

                  _diametrE = r.GetD("Group.Antenna.DIAMETER"); // диаметр антенны
                  if (_diametrE == IM.NullD)
                     _diametrE = 0.6;
               }
               if (emisOrRec.Equals("R"))
               {
                  STFrequncyRX.Add(freq); // частота прийомників 

                  _bwRX = _bwX; //r.GetD("RxEquipment.BW"); // ширина смуги частот приймача                  
                  if (_bwRX == IM.NullD || _bwTX <= 0)
                     _bwRX = 2.0;

                  _noise_t = r.GetD("Group.NOISE_T"); // шумовая температура     

                  _diametrR = r.GetD("Group.Antenna.DIAMETER"); // диаметр антенны
                  if (_diametrR == IM.NullD)
                     _diametrR = 0.6;

                  _kdbf = KTBF(_bwRX, _noise_t);
               }

               if (!baseFilled)
               {
                  _longitude = r.GetD("Group.Antenna.Station.LONGITUDE");
                  _latitude = r.GetD("Group.Antenna.Station.LATITUDE");
                  _power = r.GetD("Group.PWR_MAX") + 30; // потужність
                  AboveEarthLevel = 3.0; 
                  _aboveSeaLevel = r.GetD("Group.Antenna.Station.ALTITUDE");// +agl; // висота над рівнем моря в м                           
                  _azimuth = r.GetD("Group.Antenna.Station.AZM_TO"); // азимут карточки(з станції)
                  _bElevation = r.GetD("Group.Antenna.Station.ELEV_MIN"); // угол в з карточки                                              
                  _typeOfStation = TypeOfStation.Earth;
                  //_identificator = r.GetS("Group.Antenna.Station.Owner.NAME");
                  //_adress = r.GetS("Group.Antenna.Station.Owner.ADDRESS");
                  //_owner = r.GetS("Group.Antenna.Station.Owner.REPRESENT");
                  _ownerID = r.GetI("Group.Antenna.Station.OWNER_ID");
                  if (STFrequncyTX.Count > 0)
                     _gMax = GMAX(_diametrE, STFrequncyTX[0]);                  

                  baseFilled = true;
               }
            }
            r.Close();
            r.Destroy();
            return;
         }

         if (tableName.Equals("MICROWA"))
         {
            //bool baseFilled = false;
            //IMRecordset r = new IMRecordset("MICROWS", IMRecordset.Mode.ReadOnly);
            //r.Select("Position.LONGITUDE,Position.LATITUDE,ROLE,TX_FREQ,POWER,TX_LOSSES,RX_LOSSES,Microwave.ID,Microwave.Equipment.KTBF,Microwave.Equipment.BW,Ant1.GAIN,AZIMUTH,ANGLE_ELEV,Position.NAME,Position.CODE,Position.ADDRESS,Microwave.Owner.NAME,Microwave.Owner.ADDRESS,Microwave.Owner.REPRESENT,Microwave.OWNER_ID");            
            IMRecordset r = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
            r.Select("ID,StationA.Position.LONGITUDE,StationA.Position.LATITUDE,StationB.Position.LONGITUDE,StationB.Position.LATITUDE,StationA.TX_FREQ,StationB.TX_FREQ,StationA.POWER,StationB.POWER,StationA.TX_LOSSES,StationB.TX_LOSSES,StationA.RX_LOSSES,StationB.RX_LOSSES,Equipment.KTBF,Equipment.RXTH_3,Equipment.RXTH_6,Equipment.MODULATION,Equipment.C_I,Equipment.BW,StationA.Ant1.GAIN,StationB.Ant1.GAIN,StationA.GAIN,StationB.GAIN,StationA.AZIMUTH,StationB.AZIMUTH,StationA.ANGLE_ELEV,StationB.ANGLE_ELEV,OWNER_ID");//StationA.Position.NAME,StationB.Position.NAME,StationA.Position.CODE,StationB.Position.CODE,StationA.Position.ADDRESS,StationB.Position.ADDRESS,Owner.NAME,Owner.ADDRESS,Owner.REPRESENT
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                        
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               //string role = r.GetS("ROLE");
               //double freqTX = r.GetD("TX_FREQ");

               //if (role.Equals("A"))
               //{
               stationA.STFrequncyTX.Add(r.GetD("StationA.TX_FREQ"));
               stationA.STFrequncyRX.Add(r.GetD("StationB.TX_FREQ"));
               stationA._longitude = r.GetD("StationA.Position.LONGITUDE");
               stationA._latitude = r.GetD("StationA.Position.LATITUDE");
               stationA._power = r.GetD("StationA.POWER");
               stationA._lFT = r.GetD("StationA.TX_LOSSES");
               stationA._lFR = r.GetD("StationA.RX_LOSSES");
               stationA._bwTX = r.GetD("Equipment.BW");// / 1000.0;

               if (stationA._bwTX != IM.NullD && stationA._bwTX > 0)
                  stationA._bwTX /= 1000.0;
               else
                  stationA._bwTX = 28;               

               stationB._bwRX = stationA._bwTX;

               stationA._gMax = r.GetD("StationA.GAIN");
               if (stationA._gMax == IM.NullD)
               stationA._gMax = r.GetD("StationA.Ant1.GAIN");
               if (stationA._gMax == IM.NullD)
                  stationA._gMax = 0.0;

               // простановка енергетических параметров приемника
               stationA._kdbf = r.GetD("Equipment.KTBF");
               if (stationA._kdbf == IM.NullD)
               {
                   stationA._kdbf = -112.58 + 10 * Math.Log10(stationB._bwRX);
               }
               double _th_3; 
               double _th_6;
               _th_3 = r.GetD("Equipment.RXTH_3");
               _th_6 = r.GetD("Equipment.RXTH_6");
               if (_th_3 != IM.NullD || _th_6 != IM.NullD)
               {
                   if (_th_3 != IM.NullD && _th_6 != IM.NullD)
                   {
                       stationA._TH = Math.Min(_th_3, _th_6);
                   }
                   else
                   {
                       if (_th_3 != IM.NullD)
                       {
                           stationA._TH = _th_6;
                       }
                       else
                       {
                           stationA._TH = _th_3;
                       }

                   }
               }
               else
               {
                   stationA._TH = 16 + stationA._kdbf;
               }
               stationA._s_n = r.GetD("Equipment.C_I");
               if (stationA._s_n == IM.NullD)
               {
                   stationA._s_n = stationA._TH - stationA._kdbf;
               }
               string _mod;
               _mod = r.GetS("Equipment.MODULATION");
               if ((_mod == "10" && stationA._s_n == IM.NullD)||(_mod == "10" && stationA._s_n < 22))
               {
                   stationA._s_n = 22;
               }
               if ((_mod == "11" && stationA._s_n == IM.NullD) || (_mod == "11" && stationA._s_n < 24.5))
               {
                   stationA._s_n = 24.5;
               }
               if ((_mod == "48" && stationA._s_n == IM.NullD) || (_mod == "48" && stationA._s_n < 27))
               {
                   stationA._s_n = 27;
               }
               if ((_mod == "49" && stationA._s_n == IM.NullD) || (_mod == "49" && stationA._s_n < 32.5))
               {
                   stationA._s_n = 32.5;
               }
               if ((_mod == "50" && stationA._s_n == IM.NullD) || (_mod == "50" && stationA._s_n < 34))
               {
                   stationA._s_n = 34;
               }

               stationA._typeOfStation = TypeOfStation.Radio;
               stationA._azimuth = r.GetD("StationA.AZIMUTH");
               stationA._bElevation = r.GetD("StationA.ANGLE_ELEV");
               //stationA._adress = r.GetS("StationA.Position.ADDRESS");
               //stationA._identificator = r.GetS("StationA.Position.CODE");
               stationA._tableID = _tableID;
               stationA._ownerID = r.GetI("OWNER_ID");               

               stationB.STFrequncyTX.Add(r.GetD("StationB.TX_FREQ"));
               stationB.STFrequncyRX.Add(r.GetD("StationA.TX_FREQ"));
               stationB._longitude = r.GetD("StationB.Position.LONGITUDE");
               stationB._latitude = r.GetD("StationB.Position.LATITUDE");
               stationB._power = r.GetD("StationB.POWER");
               stationB._lFT = r.GetD("StationB.TX_LOSSES");
               stationB._lFR = r.GetD("StationB.RX_LOSSES");
               stationB._bwTX = r.GetD("Equipment.BW");

               if (stationB._bwTX != IM.NullD && stationB._bwTX > 0)
                  stationB._bwTX /= 1000.0;
               else
                  stationB._bwTX = 28;

               stationA._bwRX = stationB._bwTX;
               stationB._gMax = r.GetD("StationB.GAIN");
               if (stationB._gMax == IM.NullD)
               stationB._gMax = r.GetD("StationB.Ant1.GAIN");
               if (stationB._gMax == IM.NullD)
                  stationB._gMax = 0.0;

               stationB._kdbf = stationA._kdbf;
               stationB._s_n = stationA._s_n;
               stationB._TH = stationA._TH;
               stationB._typeOfStation = TypeOfStation.Radio;
               stationB._azimuth = r.GetD("StationB.AZIMUTH");
               stationB._bElevation = r.GetD("StationB.ANGLE_ELEV");
               //stationB._adress = r.GetS("Position.NAME");
               //stationB._identificator = r.GetS("Position.CODE");
               stationB._tableID = _tableID;
               stationB._ownerID = r.GetI("OWNER_ID");
               double d = 0;
               d = StationUtility.AntennaLength(stationA._longitude, stationA._latitude, stationB._longitude, stationB._latitude);
               double loss;
               if (d != 0)
               {
                   loss =  32.45 + 20*Math.Log10(d*stationA.STFrequncyTX[0]);
               }
               else
               {
                   loss = 60;
               }
               stationA._carrier = stationB._power + stationB._gMax + stationA._gMax -loss;
               stationB._carrier = stationA._power + stationB._gMax + stationA._gMax - loss;
               //}
               //if (!baseFilled)
               //{
               _typeOfStation = TypeOfStation.Radio;
               //_adress = r.GetS("Microwave.Owner.ADDRESS");
               //_owner = r.GetS("Owner.NAME") + " " + r.GetS("Owner.REPRESENT");
               _ownerID = r.GetI("OWNER_ID");
               _tableID = _tableID;
               //baseFilled = true;
               //}
            }

            r.Close();
            r.Destroy();
         }

         if (tableName.Equals(ICSMTbl.FxLkClink))
         {
            _typeOfStation = TypeOfStation.ForeignLink;
            IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.Select("StationA.LONGITUDE,StationA.LATITUDE");
            r.Select("StationB.LONGITUDE,StationB.LATITUDE");
            r.Select("StationA.TX_FREQ,StationB.TX_FREQ");
            r.Select("BW,KTBF");
            r.Select("StationA.PWR_ANT,StationB.PWR_ANT");
            r.Select("StationA.TX_LOSSES,StationA.RX_LOSSES");
            r.Select("StationB.TX_LOSSES,StationB.RX_LOSSES");
            r.Select("StationA.AZIMUTH");
            r.Select("StationB.AZIMUTH");
            r.Select("StationA.ELEVATION");
            r.Select("StationB.ELEVATION");
            r.Select("StationA.AGL,StationA.ASL");
            r.Select("StationB.AGL,StationB.ASL");
            r.Select("StationA.GAIN");
            r.Select("StationB.GAIN");
            r.Select("StationA.RX_GAIN");
            r.Select("StationB.RX_GAIN");
            r.Select("StationB.RX_GAIN");
            r.Select("StationA.DIAMETER");
            r.Select("StationB.DIAMETER");
            r.Select("StationA.CP_PATTERN");
            r.Select("StationB.CP_PATTERN");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);

            r.Open();
            if(!r.IsEOF())
            {
               //Станция А
               stationA._typeOfStation = TypeOfStation.ForeignLink;
               stationA._tableID = _tableID;
               stationA._ownerID = IM.NullI;
               stationA._longitude = r.GetD("StationA.LONGITUDE");
               stationA._latitude = r.GetD("StationA.LATITUDE");
               stationA.STFrequncyTX.Add(r.GetD("StationA.TX_FREQ"));
               stationA.STFrequncyRX.Add(r.GetD("StationB.TX_FREQ"));
               stationA._bwTX = r.GetD("BW") / 1000.0;
               stationA._bwRX = stationA._bwTX;
               stationA._power = r.GetD("StationA.PWR_ANT") + 30.0;
               if ((stationA._power > 999.0) || (stationA._power < -999.0))
                  stationA._power = -999.0;
               stationA._lFT = r.GetD("StationA.TX_LOSSES");
               stationA._lFR = r.GetD("StationA.RX_LOSSES");
               stationA._kdbf = r.GetD("KTBF");
               stationA._azimuth = r.GetD("StationA.AZIMUTH");
               stationA._bElevation = r.GetD("StationA.ELEVATION");
               double seaLevel = (r.GetD("StationA.AGL") == IM.NullD) ? 0.0 : r.GetD("StationA.AGL");
               seaLevel += (r.GetD("StationA.ASL") == IM.NullD) ? 0.0 : r.GetD("StationA.ASL");
               stationA._aboveSeaLevel = seaLevel;

               double gMaxTmp = r.GetD("StationA.GAIN");
               if(gMaxTmp == IM.NullD)
                  gMaxTmp = r.GetD("StationA.RX_GAIN");
               if (gMaxTmp == IM.NullD)
               {
                  gMaxTmp = 0.0;
                  double curFreqTx = (stationA.STFrequncyTX.Count > 0) ? stationA.STFrequncyTX[0] : IM.NullD;
                  if(curFreqTx == IM.NullD)
                     curFreqTx = (stationA.STFrequncyRX.Count > 0) ? stationA.STFrequncyRX[0] : IM.NullD;
                  double diamAntenna = r.GetD("StationA.DIAMETER");
                  double lamda = 300000000.0 / curFreqTx;
                  if ((diamAntenna != IM.NullD) && (curFreqTx != IM.NullD))
                     gMaxTmp = 20.0*Math.Log10(diamAntenna/lamda) + 7.7;
      }
               stationA._gMax = gMaxTmp;
               stationA._aDiagH = r.GetS("StationA.CP_PATTERN");
               if (string.IsNullOrEmpty(stationA._aDiagH) == false)
                  stationA._aDiagH = string.Format("FOREIGN {0}", stationA._aDiagH);
               stationA._bDiagV = stationA._aDiagH;
               stationA._antenDIAGA = "HV";

               // Станция Б
               stationB._typeOfStation = TypeOfStation.ForeignLink;
               stationB._tableID = _tableID;
               stationB._ownerID = IM.NullI;
               stationB._longitude = r.GetD("StationB.LONGITUDE");
               stationB._latitude = r.GetD("StationB.LATITUDE");
               stationB.STFrequncyTX.Add(r.GetD("StationB.TX_FREQ"));
               stationB.STFrequncyRX.Add(r.GetD("StationA.TX_FREQ"));
               stationB._bwTX = r.GetD("BW") / 1000.0;
               stationB._bwRX = stationB._bwTX;
               stationB._power = r.GetD("StationB.PWR_ANT") + 30.0;
               if ((stationB._power > 999.0) || (stationB._power < -999.0))
                  stationB._power = -999.0;
               stationB._lFT = r.GetD("StationB.TX_LOSSES");
               stationB._lFR = r.GetD("StationB.RX_LOSSES");
               stationB._kdbf = r.GetD("KTBF");
               stationB._azimuth = r.GetD("StationB.AZIMUTH");
               stationB._bElevation = r.GetD("StationB.ELEVATION");
               seaLevel = (r.GetD("StationB.AGL") == IM.NullD) ? 0.0 : r.GetD("StationB.AGL");
               seaLevel += (r.GetD("StationB.ASL") == IM.NullD) ? 0.0 : r.GetD("StationB.ASL");
               stationB._aboveSeaLevel = seaLevel;

               gMaxTmp = r.GetD("StationB.GAIN");
               if (gMaxTmp == IM.NullD)
                  gMaxTmp = r.GetD("StationB.RX_GAIN");
               if (gMaxTmp == IM.NullD)
               {
                  gMaxTmp = 0.0;
                  double curFreqTx = (stationB.STFrequncyTX.Count > 0) ? stationB.STFrequncyTX[0] : IM.NullD;
                  if (curFreqTx == IM.NullD)
                     curFreqTx = (stationB.STFrequncyRX.Count > 0) ? stationB.STFrequncyRX[0] : IM.NullD;
                  double diamAntenna = r.GetD("StationB.DIAMETER");
                  double lamda = 300000000.0 / curFreqTx;
                  if ((diamAntenna != IM.NullD) && (curFreqTx != IM.NullD))
                     gMaxTmp = 20.0 * Math.Log10(diamAntenna / lamda) + 7.7;
               }
               stationB._gMax = gMaxTmp;
               stationB._aDiagH = r.GetS("StationB.CP_PATTERN");
               if (string.IsNullOrEmpty(stationB._aDiagH) == false)
                  stationB._aDiagH = string.Format("FOREIGN {0}", stationB._aDiagH);
               stationB._bDiagV = stationB._aDiagH;
               stationB._antenDIAGA = "HV";


               _ownerID = IM.NullI;
               _aboveSeaLevel = (stationB._aboveSeaLevel > stationA._aboveSeaLevel)
                                   ? stationB._aboveSeaLevel
                                   : stationA._aboveSeaLevel;
            }
            if(r.IsOpen())
               r.Close();
            r.Destroy();
         }

         if (tableName.Equals(ICSMTbl.WienCoordMob))
         {
            bool baseFilled = false;
            IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.Select("X");
            r.Select("Y");
            r.Select("TX_FREQ");
            r.Select("RX_FREQ");
            r.Select("ASL");
            r.Select("AGL");
            r.Select("BW");
            r.Select("PWR_ANT");
            r.Select("TX_LOSSES");
            //?? r.Select("RX_LOSSES");
            r.Select("ELEVATION");
            r.Select("AZIMUTH");
            r.Select("GAIN");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               STFrequncyRX.Add(r.GetD("RX_FREQ"));
               STFrequncyTX.Add(r.GetD("TX_FREQ"));

               if (!baseFilled)
               {
                  baseFilled = true;
                  _typeOfStation = TypeOfStation.ForeignLink;
                  _longitude = r.GetD("X");
                  _latitude = r.GetD("Y");
                  _aboveSeaLevel = (r.GetD("AGL") == IM.NullD) ? 0.0 : r.GetD("AGL");
                  _aboveSeaLevel += (r.GetD("ASL") == IM.NullD) ? 0.0 : r.GetD("ASL");
                  _bwTX = r.GetD("BW");
                  _bwRX = r.GetD("BW");
                  _power = r.GetD("PWR_ANT");
                  _lFT = r.GetD("TX_LOSSES");
                  //?? _lFR = r.GetD("RX_LOSSES");
                  _azimuth = r.GetD("AZIMUTH");
                  _bElevation = r.GetD("ELEVATION");
                  _gMax = r.GetD("GAIN");

                  //_antenDIAGA = r.GetS("Antenna.DIAGA"); // діаграма станції
                  //_aDiagH = r.GetS("Antenna.DIAGH"); // діаграма по горизонталі               
                  //_bDiagV = r.GetS("Antenna.DIAGV"); // діаграма по вертикалі
                  //_kdbf = r.GetD("Equipment.KTBF"); // рівень власних шумів прийомника
                  //_ownerID = r.GetI("OWNER_ID"); // ID овнера
               }
            }
            if(r.IsOpen())
               r.Close();
            r.Destroy();
            return;
         }

      }

      /// <summary>
      /// Загружает дополнительние данные о станции, которые не касаються расчетов
      /// </summary>
      public void LoadAdditionalFields()
      {
         if (_table.Equals(ICSMTbl.itblMobStation2))
         {
            IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadOnly);
            r.Select("ID,Owner.NAME,NAME,Position.REMARK,Position.ADDRESS,Position.POSTCODE,Position.PROVINCE,Position.SUBPROVINCE,Position.CITY,Position.City.CUST_TXT4");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
            r.Open();
            if(!r.IsEOF())
            {
               _owner = r.GetS("Owner.NAME");
               _remark = GetAdressInformation(r.GetS("Position.POSTCODE"), r.GetS("Position.PROVINCE"), r.GetS("Position.SUBPROVINCE"), r.GetS("Position.CITY"), r.GetS("Position.ADDRESS"), r.GetS("Position.City.CUST_TXT4"));
               Name = r.GetS("NAME");
            }
            if(r.IsOpen())
               r.Close();
            r.Destroy();
            return;
         }
         else if (_table.Equals(ICSMTbl.itblEarthStation))
         {

            IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
            r.Select("Group.Antenna.Station.ID,Group.Antenna.Station.Owner.NAME");
            r.Select("Group.Antenna.Station.Position.REMARK,Group.Antenna.Station.NAME,Group.Antenna.Station.Position.ADDRESS,Group.Antenna.Station.Position.POSTCODE,Group.Antenna.Station.Position.PROVINCE,Group.Antenna.Station.Position.SUBPROVINCE,Group.Antenna.Station.Position.CITY,Group.Antenna.Station.Position.City.CUST_TXT4");
            r.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, _tableID);
            r.Open();
            if(!r.IsEOF())
            {
               _owner = r.GetS("Group.Antenna.Station.Owner.NAME");
               _remark = GetAdressInformation(r.GetS("Group.Antenna.Station.Position.POSTCODE"), r.GetS("Group.Antenna.Station.Position.PROVINCE"), r.GetS("Group.Antenna.Station.Position.SUBPROVINCE"), r.GetS("Group.Antenna.Station.Position.CITY"), r.GetS("Group.Antenna.Station.Position.ADDRESS"), r.GetS("Group.Antenna.Station.Position.City.CUST_TXT4"));
               Name = r.GetS("Group.Antenna.Station.NAME");
            }
            if(r.IsOpen())
               r.Close();
            r.Destroy();
            return;
         }
         else if (_table.Equals("MICROWA"))
         {
            IMRecordset r = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
            r.Select("ID,Owner.NAME,NAME,StationA.Position.NAME,StationB.Position.NAME");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
            r.Open();
            if(!r.IsEOF())
            {
               stationA._owner = r.GetS("Owner.NAME");
               stationB._owner = stationA._owner;
               stationA._remark = r.GetS("StationA.Position.NAME");
               stationB._remark = r.GetS("StationB.Position.NAME");
               stationA.Name = r.GetS("NAME");
               stationB.Name = stationA.Name;
            }
            if(r.IsOpen())
               r.Close();
            r.Destroy();
         }
         else if (_table.Equals(ICSMTbl.FxLkClink))
         {
            IMRecordset r = new IMRecordset(_table, IMRecordset.Mode.ReadOnly);
            r.Select("ID,StationA.NAME,StationB.NAME,StationA.MicrowaveStation.Position.REMARK,StationB.MicrowaveStation.Position.REMARK,ADM,StationA.MicrowaveStation.Position.ADDRESS,StationA.MicrowaveStation.Position.POSTCODE,StationA.MicrowaveStation.Position.PROVINCE,StationA.MicrowaveStation.Position.SUBPROVINCE,StationA.MicrowaveStation.Position.CITY,StationB.MicrowaveStation.Position.ADDRESS,StationB.MicrowaveStation.Position.POSTCODE,StationB.MicrowaveStation.Position.PROVINCE,StationB.MicrowaveStation.Position.SUBPROVINCE,StationB.MicrowaveStation.Position.CITY,StationB.MicrowaveStation.Position.City.CUST_TXT4,StationA.MicrowaveStation.Position.City.CUST_TXT4");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
            r.Open();
            if (!r.IsEOF())
            {
               stationA._owner = r.GetS("ADM");
               stationB._owner = stationA._owner;
               stationA._remark = GetAdressInformation(r.GetS("StationA.MicrowaveStation.Position.POSTCODE"), r.GetS("StationA.MicrowaveStation.Position.PROVINCE"), r.GetS("StationA.MicrowaveStation.Position.SUBPROVINCE"), r.GetS("StationA.MicrowaveStation.Position.CITY"), r.GetS("StationA.MicrowaveStation.Position.ADDRESS"), r.GetS("StationA.MicrowaveStation.Position.City.CUST_TXT4"));
               stationB._remark = GetAdressInformation(r.GetS("StationB.MicrowaveStation.Position.POSTCODE"), r.GetS("StationB.MicrowaveStation.Position.PROVINCE"), r.GetS("StationB.MicrowaveStation.Position.SUBPROVINCE"), r.GetS("StationB.MicrowaveStation.Position.CITY"), r.GetS("StationB.MicrowaveStation.Position.ADDRESS"), r.GetS("StationB.MicrowaveStation.Position.City.CUST_TXT4"));
               stationA.Name = r.GetS("StationA.NAME");
               stationB.Name = r.GetS("StationB.NAME");
            }
            if (r.IsOpen())
               r.Close();
            r.Destroy();
         }
         else if (_table.Equals(ICSMTbl.WienCoordMob))
         {
            IMRecordset r = new IMRecordset(_table, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
            r.Open();
            if(!r.IsEOF())
            {// TODO незнаю откуда брать
               //_owner = r.GetS("");
               //_remark = r.GetS("");
               //Name = r.GetS("");
            }
            if (r.IsOpen())
               r.Close();
            r.Destroy();
         }
      }

      /// <summary>
      /// Розрахунок KTBF його я використовую для земних станцій
      /// </summary>
      /// <param name="bwRX">смугова частота приймача [МГГц]</param>
      /// <param name="noise_t">шумова температура</param>
      /// <returns></returns>
      public double KTBF(double bwRX, double noise_t)
      {
         double k = 1.38 * Math.Pow(10, -17);
         double kdbf = 10.0 * Math.Log10(k * bwRX * noise_t) + 30;         
         return kdbf;
      }

      public double GMAX(double diametr, double freqTX)
      {
         double alfa = 300.0 / freqTX;
         double gMax = 20.0 * Math.Log10(diametr / alfa);
         return gMax;
      }

      public bool ChecsOnNull(Station st, Station.TypeOfStation typeStation)
      {
         if (st._latitude == IM.NullD || st._longitude == IM.NullD)
            return false;
         ///////////////////////////////////////////////
         if (st._power == IM.NullD)
            st._power = 0.0;
         if (st._azimuth == IM.NullD)
            st._azimuth = 0.0;
         if (st._bElevation == IM.NullD)
            st._bElevation = 0.0;

         if (typeStation == TypeOfStation.Radio)
         {
            if (st._gMax == IM.NullD)
               st._gMax = 36.0;
         }
         else if (st._gMax == IM.NullD)
            st._gMax = 0.0;

         if (st._lFT == IM.NullD)
            st._lFT = 0.0;
         if (st._lFR == IM.NullD)
            st._lFR = 0.0;
         if (st._aboveSeaLevel == IM.NullD)
            st._aboveSeaLevel = 0.0;
         ///////////////////////////////////////////////
         List<double> tmp = new List<double>();
         foreach (double tx in st.STFrequncyTX)
         {
            if (tx != IM.NullD && tx != 0 && st._bwTX > 0)
               tmp.Add(tx);
         }
         st.STFrequncyTX.Clear();
         foreach (double tx in tmp)
         {
            st.STFrequncyTX.Add(tx);
         }
         tmp.Clear();
         ///////////////////////////////////////////////
         foreach (double rx in st.STFrequncyRX)
         {
            if (rx != IM.NullD && rx != 0 && st._bwRX > 0)
               tmp.Add(rx);
         }
         st.STFrequncyRX.Clear();
         foreach (double rx in tmp)
         {
            st.STFrequncyRX.Add(rx);
         }
         tmp.Clear();
         ///////////////////////////////////////////////
         if (st._kdbf == IM.NullD)
         {
            if (typeStation == TypeOfStation.SRT)
               st._kdbf = KTBF(st._bwRX, 300.0);
            else
            {
               if (typeStation == TypeOfStation.Earth)
                  st._kdbf = KTBF(st._bwRX, 120.0);
               else
                  st._kdbf = KTBF(st._bwRX, 300.0);
            }
         }

         if (typeStation == TypeOfStation.SRT)
         {
            if (string.IsNullOrEmpty(_aDiagH) && string.IsNullOrEmpty(_bDiagV))
            {
               return false;
               //int index = _aDiagH.IndexOf(' ');
               //if (index > 0)
               //   string _aDiagH = _aDiagH.Remove(0, index + 1);

               //index = _bDiagV.IndexOf(' ');
               //if (index > 0)
               //   string str1 _bDiagV = _bDiagV.Remove(0, index + 1);

               //string[] data = _aDiagH.Split(' ');
               //foreach (string str in data)
               //{
               //   double tmpVal;
               //   if (!double.TryParse(str, out tmpVal))
               //      return false;
               //}
            }
         }
         return true;
      }

      public static double ParserForSRTBW(string str)
      {
         try
         {
            StringBuilder strBuilder = new StringBuilder();
            string simvol2 = string.Empty;
            bool prohod = false;

            if (!string.IsNullOrEmpty(str))
            {
               for (int i = 0; i < str.Length; i++)
               {
                  char simvol = str[i];

                  if (char.IsDigit(simvol))
                     strBuilder.Append(simvol);
                  else
                  {
                     if (!prohod)
                     {
                        strBuilder.Append(".");
                        simvol2 = str.Substring(i, 1);
                        str = str.Substring(i + 1);
                        i = -1;
                        prohod = true;
                     }
                     else
                        break;
                  }
               }

               if (simvol2.Equals("M"))
               {
                  double bw = Convert.ToDouble(strBuilder.ToString(), FormEMS.provider);
                  return bw;
               }
               if (simvol2.Equals("K"))
               {
                  double bw = Convert.ToDouble(strBuilder.ToString(), FormEMS.provider) / 1000;
                  return bw;
               }
               if (simvol2.Equals("H"))
               {
                  double bw = Convert.ToDouble(strBuilder.ToString(), FormEMS.provider) / 1000000;
                  return bw;
               }
               if (simvol2.Equals("G"))
               {
                  double bw = Convert.ToDouble(strBuilder.ToString(), FormEMS.provider) * 1000;
                  return bw;
               }

               return -1;         
            }
            else
               return -1;
         }
         catch
         {
            return -1;
         }
      }

      #endregion
   }
}
