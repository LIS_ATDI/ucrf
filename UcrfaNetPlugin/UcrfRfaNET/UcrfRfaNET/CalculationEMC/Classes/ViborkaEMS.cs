﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSPosition;
using ICSM;
using NSIRF;
using Distribution;
using LisUtility;
using System.Collections;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using XICSM.UcrfRfaNET.Calculation.ResultForm;
using XICSM.UcrfRfaNET.Map;

namespace XICSM.UcrfRfaNET
{
    internal class ViborkaEMS
   {       
      #region MEMBERS      

       public class Results
       {
           public string FrequenciersString;

           public int MicrovaweIntfCount;
           public double MicrovaweIntf;
           public int MobstationIntfCount;
           public double MobstationIntf;
           public int EarthIntfCount;
           public double EarthIntf;

           public int MicrovaweVictimCount;
           public double MicrovaweVictim;
           public int MobstationVictimCount;
           public double MobstationVictim;
           public int EarthVictimCount;
           public double EarthVictim;
      };

      public enum TypeOfFrequency // тип розрахунку по частоті
      {
         Zhertva, // жертва
         Pomeha // помеха
      }

      private bool isCancelled = false;

      public bool IsCancelled{ get { return isCancelled; }}

      private List<int> _ListZhertvaMicroWave = new List<int>(); // колекція жертв MicroWave
      private List<int> _ListPomehaMicroWave = new List<int>(); // колекція помєх MicroWave
      private List<int> _ListZhertvaForeingMicroWave = new List<int>(); // колекція жертв MicroWave
      private List<int> _ListPomehaForeingMicroWave = new List<int>(); // колекція помєх MicroWave
      //private List<int> _ListZhertvaForeingStation = new List<int>(); // колекція жертв MicroWave
      //private List<int> _ListPomehaForeingStation = new List<int>(); // колекція помєх MicroWave
      private List<int> _ListZhertvaSRT = new List<int>(); // колекція жертв SRT
      private List<int> _ListPomehaSRT = new List<int>(); // колекція помєх SRT
      private List<int> _ListZhertvaEarth = new List<int>(); // колекція жертв Earth
      private List<int> _ListPomehaEarth = new List<int>(); // колекція помєх Earth

      private List<Station> stListZhertvaMicroWave = new List<Station>(); // колекція жертв
      private List<Station> stListPomehaMicroWave = new List<Station>(); // колекція помєх
      private List<Station> stListZhertvaForeingMicroWave = new List<Station>(); // колекція жертв
      private List<Station> stListPomehaForeingMicroWave = new List<Station>(); // колекція помєх
      //private List<Station> stListZhertvaForeingStation = new List<Station>(); // колекція жертв
      //private List<Station> stListPomehaForeingStation = new List<Station>(); // колекція помєх
      private List<Station> stListZhertvaSRT = new List<Station>(); // колекція жертв SRT
      private List<Station> stListPomehaSRT = new List<Station>(); // колекція помєх SRT
      private List<Station> stListZhertvaEarth = new List<Station>(); // колекція жертв Earth
      private List<Station> stListPomehaEarth = new List<Station>(); // колекція помєх Earth

        
      private Station _curCalcStation;
      private Station[] curStatList;
      private double _radius;
      private double _freqDiapazon;
      private double _compareTD;
      private bool _needSRT;
      private bool _needEarth;
      private bool _needRadio;
      private bool _needForeing;
      private bool _needOnlyAll;
      private int countRecord = 0;

      #endregion

      #region CONSTRUKTOR

      public ViborkaEMS(Station currentStation, double radius, double freqDiapazon, double compareTD,
                        bool needSRT, bool needEarth, bool needRadio, bool needOnlyAll, bool needForeing) :
             this(new Station[]{currentStation}, radius, freqDiapazon, compareTD,
                  needSRT, needEarth, needRadio, needOnlyAll, needForeing) {}

      public ViborkaEMS(Station[] currentStation, double radius, double freqDiapazon, double compareTD,
         bool needSRT, bool needEarth, bool needRadio, bool needOnlyAll, bool needForeing)
      {
          curStatList = currentStation;
         _radius = radius;
         _freqDiapazon = freqDiapazon;
         _compareTD = compareTD;
         _needSRT = needSRT;
         _needEarth = needEarth;
         _needRadio = needRadio;
         _needForeing = needForeing;
         _needOnlyAll = needOnlyAll;
         if ((curStatList == null) || (curStatList.Length == 0))
             throw new Exception("Error input data");
      }

      #endregion

      #region CALCULATE

      /// <summary>
      /// Main calculation method
      /// </summary>
      /// 
      public void Calculate()
      {
          Calculate(CLocaliz.TxT("Calculation EMS"));
      }

      /// <summary>
      /// Main calculation method
      /// </summary>
      public void Calculate(string strTitle)
      {
         isCancelled = false;
         int numberSector = 1;
         List<Station> tmpListPomehaEarth = new List<Station>();
         List<Station> tmpListZhertvaEarth = new List<Station>();
         List<Station> tmpListPomehaSrt = new List<Station>();
         List<Station> tmpListZhertvaSrt = new List<Station>();
         List<Station> tmpListZhertvaMicroWave = new List<Station>();
         List<Station> tmpListPomehaMicroWave = new List<Station>();
         List<Station> tmpListZhertvaForeingMicroWave = new List<Station>();
         List<Station> tmpListPomehaForeingMicroWave = new List<Station>();
         foreach (Station station in curStatList)
         {
             using (LisProgressBar pBar = new LisProgressBar(strTitle + string.Format(" ({0})", numberSector++)))
             {
                 _curCalcStation = station;
                 //---
                 _ListPomehaEarth = new List<int>();
                 _ListZhertvaEarth = new List<int>();
                 _ListZhertvaSRT = new List<int>();
                 _ListPomehaSRT = new List<int>();
                 _ListPomehaMicroWave = new List<int>();
                 _ListZhertvaMicroWave = new List<int>();
                 _ListPomehaForeingMicroWave = new List<int>();
                 _ListZhertvaForeingMicroWave = new List<int>();
                 //---
                 stListPomehaEarth = new List<Station>();
                 stListZhertvaEarth = new List<Station>();
                 stListPomehaSRT = new List<Station>();
                 stListZhertvaSRT = new List<Station>();
                 stListZhertvaMicroWave = new List<Station>();
                 stListPomehaMicroWave = new List<Station>();
                 stListZhertvaForeingMicroWave = new List<Station>();
                 stListPomehaForeingMicroWave = new List<Station>();
                 //---
                 isCancelled = isCancelled | pBar.UserCanceled(); // for displayed button cancel 
                 //IMProgress.Create("Calculation EMS");
                 //IMProgress.ShowBig("Choose coordinates");

                 pBar.SetBig(CLocaliz.TxT("Choose coordinates..."));
                 this.CalculateOnRadius();

                 //IMProgress.ShowBig("Creation Stations");
                 pBar.SetBig(CLocaliz.TxT("Creation Stations..."));
                 this.FillingStations();
                 this.CheckOnBaseStation();

                 if (_needOnlyAll)
                 {
                     pBar.SetBig(CLocaliz.TxT("Checking Stations on operator..."));
                     this.CheckOnOperator();
                 }

                 //pBar.ShowBig(CLocaliz.TxT("Calculation TD..."));
                 //IMProgress.ShowBig("Calculation TD");
                 countRecord = 0;

                 if (_needSRT)
                 {
                     pBar.SetBig(CLocaliz.TxT("Calculation TD for SRT stations..."));
                     this.CalculateTD(stListZhertvaSRT, stListPomehaSRT, Station.TypeOfStation.SRT);
                 }
                 if (_needEarth)
                 {
                     pBar.SetBig(CLocaliz.TxT("Calculation TD for Earth stations..."));
                     this.CalculateTD(stListZhertvaEarth, stListPomehaEarth, Station.TypeOfStation.Earth);
                 }
                 if (_needRadio)
                 {
                     pBar.SetBig(CLocaliz.TxT("Calculation TD for Radio stations..."));
                     this.CalculateTD(stListZhertvaMicroWave, stListPomehaMicroWave, Station.TypeOfStation.Radio);
                 }

                 if (_needForeing)
                 {
                     pBar.SetBig(CLocaliz.TxT("Calculation TD for Foreing stations..."));
                     this.CalculateTD(stListZhertvaForeingMicroWave, stListPomehaForeingMicroWave,
                                      Station.TypeOfStation.ForeignLink);
                     //this.CalculateTD(stListZhertvaForeingStation, stListPomehaForeingStation, Station.TypeOfStation.ForeignStation);
                 }

                 countRecord = 0;

                 if (_needSRT)
                 {
                     pBar.SetBig(CLocaliz.TxT("Verification data of TD for SRT stations..."));
                     this.CheckOnAllowableTD(ref stListZhertvaSRT);
                     this.CheckOnAllowableTD(ref stListPomehaSRT);
                 }
                 if (_needEarth)
                 {
                     pBar.SetBig(CLocaliz.TxT("Verification data of TD for Earth stations..."));
                     this.CheckOnAllowableTD(ref stListZhertvaEarth);
                     this.CheckOnAllowableTD(ref stListPomehaEarth);
                 }
                 if (_needRadio)
                 {
                     pBar.SetBig(CLocaliz.TxT("Verification data of TD for Radio stations..."));
                     this.CheckOnAllowableTD(ref stListZhertvaMicroWave);
                     this.CheckOnAllowableTD(ref stListPomehaMicroWave);
                 }
                 //Отбираем станции
                 pBar.SetBig(CLocaliz.TxT("Station comparing..."));
                 MergeStations(stListZhertvaSRT, tmpListZhertvaSrt);
                 MergeStations(stListPomehaSRT, tmpListPomehaSrt);
                 MergeStations(stListZhertvaEarth, tmpListZhertvaEarth);
                 MergeStations(stListPomehaEarth, tmpListPomehaEarth);
                 MergeStations(stListZhertvaMicroWave, tmpListZhertvaMicroWave);
                 MergeStations(stListPomehaMicroWave, tmpListPomehaMicroWave);
             }
         }
         //Возвращаем данные
         stListZhertvaSRT = tmpListZhertvaSrt;
         stListPomehaSRT = tmpListPomehaSrt;
         stListZhertvaEarth = tmpListZhertvaEarth;
         stListPomehaEarth = tmpListPomehaEarth;
         stListZhertvaMicroWave = tmpListZhertvaMicroWave;
         stListPomehaMicroWave = tmpListPomehaMicroWave;
      }

      private void MergeStations(List<Station> fromList, List<Station> toList)
      {
          foreach (Station station in fromList)
          {
              int index = IM.NullI;  //Индекс найденой станции
              for (int i = 0; i < toList.Count; i++)
              {
                  if (station._tableID == toList[i]._tableID)
                  {
                      index = i;
                      break;
                  }
              }
              if (index == IM.NullI)
                  toList.Add(station);
              else
              {
                  if ((station._typeOfStation == Station.TypeOfStation.Radio) &&
                      (toList[index]._typeOfStation == Station.TypeOfStation.Radio))
                  {
                      Station curStation = toList[index];
                      double maxP1Station = -9999.0;
                      double maxP1CurStation = -9999.0;
                      if ((station.stationA != null) &&
                          (station.stationA.PI != null) &&
                          (station.stationA.PI.Count > 0))
                          maxP1Station = station.stationA.PI[0];
                      if ((station.stationB != null) &&
                          (station.stationB.PI != null) &&
                          (station.stationB.PI.Count > 0) &&
                          (station.stationB.PI[0] > maxP1Station))
                          maxP1Station = station.stationB.PI[0];
                      if ((curStation.stationA != null) &&
                          (curStation.stationA.PI != null) &&
                          (curStation.stationA.PI.Count > 0))
                          maxP1CurStation = curStation.stationA.PI[0];
                      if ((curStation.stationB != null) &&
                          (curStation.stationB.PI != null) &&
                          (curStation.stationB.PI.Count > 0) &&
                          (curStation.stationB.PI[0] > maxP1CurStation))
                          maxP1CurStation = curStation.stationB.PI[0];
                      if(maxP1Station > maxP1CurStation)
                          toList[index] = station;
                  }
                  else
                  {
                      if ((station.PI.Count > 0) &&
                          (toList[index].PI.Count > 0) &&
                          (station.PI[0] > toList[index].PI[0]))
                          toList[index] = station;
                  }
              }
          }
      }

      /// <summary>
      /// checks the stations on value of radius and frequency
      /// </summary>
      private void CalculateOnRadius()
      {
         RecordPos positionRadius;
         RecordPos positionRadius1;
         //"Position.X > 68,2497711999907 AND Position.X < 68,2516288000093 AND Position.Y < 39,9759288000093 AND Position.Y > 39,9740711999907"
         if ((_curCalcStation._typeOfStation == Station.TypeOfStation.Radio) ||
            (_curCalcStation._typeOfStation == Station.TypeOfStation.ForeignLink))
         {
            positionRadius = Position.GetRangeCoordinates(_curCalcStation.stationA._longitude, _curCalcStation.stationA._latitude,
             _radius, "Position.X", "Position.Y");
            positionRadius1 = Position.GetRangeCoordinates(_curCalcStation.stationB._longitude, _curCalcStation.stationB._latitude,
             _radius, "Position.X", "Position.Y");

            if (_needSRT)
            {
               Search(Station.TypeOfStation.SRT, positionRadius, _curCalcStation.stationA);
               Search(Station.TypeOfStation.SRT, positionRadius1, _curCalcStation.stationB);
            }
            if (_needEarth)
            {
               Search(Station.TypeOfStation.Earth, positionRadius, _curCalcStation.stationA);
               Search(Station.TypeOfStation.Earth, positionRadius1, _curCalcStation.stationB);
            }
            if (_needRadio)
            {
               Search(Station.TypeOfStation.Radio, positionRadius, _curCalcStation.stationA);
               Search(Station.TypeOfStation.Radio, positionRadius1, _curCalcStation.stationB);
            }
            if (_needForeing)
            {
               Search(Station.TypeOfStation.ForeignLink, positionRadius, _curCalcStation.stationA);
               Search(Station.TypeOfStation.ForeignLink, positionRadius1, _curCalcStation.stationB);
         }
         }
         else
         {
            positionRadius = Position.GetRangeCoordinates(_curCalcStation._longitude, _curCalcStation._latitude,
                _radius, "Position.X", "Position.Y");
            if (_needSRT)
               Search(Station.TypeOfStation.SRT, positionRadius, _curCalcStation);
            if (_needEarth)
               Search(Station.TypeOfStation.Earth, positionRadius, _curCalcStation);
            if (_needRadio)
               Search(Station.TypeOfStation.Radio, positionRadius, _curCalcStation);
            if (_needForeing)
            {
               Search(Station.TypeOfStation.ForeignLink, positionRadius, _curCalcStation);
         }
      }
      }

      #region SEARCH

      private void Search(Station.TypeOfStation typeOfStation, RecordPos positionRadius, Station curStat)
      {
         countRecord = 0;
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Searching")))
         {
            // земные станции
            if (typeOfStation == Station.TypeOfStation.Earth)
            {
               pBar.ShowBig(CLocaliz.TxT("Search for Earth stations"));

               IMRecordset r = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
               r.Select("Group.Antenna.Station.ID,FREQ,Group.BDWDTH,Group.EMI_RCP,Group.Antenna.Station.LONGITUDE,Group.Antenna.Station.LATITUDE");
               r.Select("Group.Antenna.Station.STATUS");
               r.Select("Group.Antenna.EMI_RCP");
               r.SetWhere("Group.Antenna.Station.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
               r.SetWhere("Group.Antenna.Station.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
               r.SetWhere("Group.Antenna.Station.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
               r.SetWhere("Group.Antenna.Station.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
               r.OrderBy("Group.Antenna.Station.ID", OrderDirection.Ascending);

               int lastTablID = 0;
               int lastTablID1 = 0;

               for (r.Open(); !r.IsEOF(); r.MoveNext())
               {
                  if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("Group.Antenna.Station.STATUS")) == false)
                     continue; //Отбрасываем ненужные станции

                  //string emisOrRec = r.GetS("Group.EMI_RCP"); // узнаем що це - передатчик чи прийомник
                  string emisOrRec = r.GetS("Group.Antenna.EMI_RCP"); // узнаем що це - передатчик чи прийомник
                  double freq = r.GetD("FREQ"); // частота чи передатчика чи прийомника
                  double _bwX = r.GetD("Group.BDWDTH");// / 1000.0; // ширина спектра передатчика и прийомника                                                           

                  if (_bwX != IM.NullD && _bwX > 0)
                     _bwX /= 1000.0;
                  else
                     _bwX = 2.0;

                  if (emisOrRec.Equals("E"))
                  {
                     if (freq != IM.NullD)
                        if (CalculateOnFrequency(freq, TypeOfFrequency.Pomeha, curStat._bwRX, _bwX, curStat))//(CalculateOnFrequency(freq, TypeOfFrequency.Pomeha, _curStat._bwRX, _bwX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                        {
                           int tableID = r.GetI("Group.Antenna.Station.ID");//r.GetI("Group.Antenna.Station.SITE_ID");

                           //if (curStat._tableID != tableID) // якщо айдіха не дорівнює текущій(вибраній) станції
                           //{
                           if (lastTablID != tableID) // на станції передатчиків чи приймачів може бути багато
                           {
                              lastTablID = tableID;
                              _ListPomehaEarth.Add(tableID);
                           }
                           //}
                        }
                  }
                  if (emisOrRec.Equals("R"))
                  {
                     // выборка помех     
                     if (freq != IM.NullD)
                        if (CalculateOnFrequency(freq, TypeOfFrequency.Zhertva, curStat._bwTX, _bwX, curStat)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                        {
                           int tableID = r.GetI("Group.Antenna.Station.ID");//r.GetI("Group.Antenna.Station.SITE_ID");
                           //if (curStat._tableID != tableID) // якщо айдіха не дорівнює текущій(вибраній) станції
                           //{
                           if (lastTablID1 != tableID) // на станції передатчиків чи приймачів може бути багато
                           {
                              lastTablID1 = tableID;
                              _ListZhertvaEarth.Add(tableID);
                           }
                           //}
                        }
                  }
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
               r.Close();
               r.Destroy();
            }

            // СРТ станції
            if (typeOfStation == Station.TypeOfStation.SRT)
            {
               pBar.ShowBig(CLocaliz.TxT("Search for SRT stations"));

               IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation2, IMRecordset.Mode.ReadOnly);
               r.Select("ID,Position.LONGITUDE,Position.LATITUDE,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,Equipment.BW,RxEquipment.BW,DESIG_EMISSION");
               r.Select("STATUS");
               r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
               r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
               r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
               r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
               r.OrderBy("ID", OrderDirection.Ascending);

               int lastTablID = 0;
               int lastTablID1 = 0;
               for (r.Open(); !r.IsEOF(); r.MoveNext())
               {
                  if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                     continue; //Отбрасываем ненужные станции

                  double fPD = r.GetD("AssignedFrequencies.TX_FREQ"); // частота передатчика
                  double fPR = r.GetD("AssignedFrequencies.RX_FREQ"); // частота прийомника              
                  
                  double _bwTX = Station.ParserForSRTBW(r.GetS("DESIG_EMISSION"));
                  double _bwRX = _bwTX;
                  if (_bwTX == -1)
                  {
                     _bwTX = r.GetD("Equipment.BW");
                     _bwRX = r.GetD("RxEquipment.BW");

                     if (_bwTX == IM.NullD || _bwTX <= 0)
                     {
                        if (_bwRX != IM.NullD && _bwRX > 0)
                           _bwTX = _bwRX;
                        else
                           _bwTX = 10000;
                     }

                     if (_bwRX == IM.NullD || _bwRX <= 0)
                        _bwRX = _bwTX;

                     _bwTX /= 1000.0;
                     _bwRX /= 1000.0;
                  }

                  if (fPR != IM.NullD)
                     if (CalculateOnFrequency(fPR, TypeOfFrequency.Zhertva, curStat._bwRX, _bwTX, curStat)) //CalculateOnFrequency(fPR, TypeOfFrequency.Zhertva, _curStat._bwRX, _bwTX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                     {
                        int tableID = r.GetI("ID");

                        //if (curStat._tableID != tableID) // якщо айдіха не дорівнює текущій(вибраній) станції
                        //{
                        if (lastTablID != tableID) // на станції передатчиків чи приймачів може бути багато
                        {
                           lastTablID = tableID;
                           _ListZhertvaSRT.Add(tableID);
                        }
                        //}
                     }

                  // выборка помех
                  // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                  if (fPD != IM.NullD)
                     if (CalculateOnFrequency(fPD, TypeOfFrequency.Pomeha, curStat._bwTX, _bwRX, curStat))//CalculateOnFrequency(fPD, TypeOfFrequency.Pomeha, _curStat._bwTX, _bwRX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                     {
                        int tableID = r.GetI("ID");

                        //if (curStat._tableID != tableID) // якщо айдіха не дорівнює текущій(вибраній) станції
                        //{
                        if (lastTablID1 != tableID) // на станції передатчиків чи приймачів може бути багато
                        {
                           lastTablID1 = tableID;

                           _ListPomehaSRT.Add(tableID);
                        }
                        //}
                     }
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();  // for displayed button cancel 
               }
               r.Close();
               r.Destroy();
            }

            // радіо Лінкі
            if (typeOfStation == Station.TypeOfStation.Radio)
            {
               pBar.ShowBig(CLocaliz.TxT("Search for Radio stations"));

               IMRecordset r = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
               r.Select("ID,StationA.Position.LONGITUDE,StationA.Position.LATITUDE,StationB.Position.LONGITUDE,StationB.Position.LATITUDE,StationA.TX_FREQ,StationB.TX_FREQ,StationA.POWER,StationB.POWER,StationA.TX_LOSSES,StationB.TX_LOSSES,StationA.RX_LOSSES,StationB.RX_LOSSES,Equipment.KTBF,Equipment.BW,StationA.Ant1.GAIN,StationB.Ant1.GAIN,StationA.AZIMUTH,StationB.AZIMUTH,StationA.ANGLE_ELEV,StationB.ANGLE_ELEV,StationA.Position.NAME,StationB.Position.NAME,StationA.Position.CODE,StationB.Position.CODE,StationA.Position.ADDRESS,StationB.Position.ADDRESS,Owner.NAME,Owner.ADDRESS,Owner.REPRESENT,OWNER_ID");//           ,,,,,,,,,,,,,,,Microwave.Owner.REPRESENT,Microwave.OWNER_ID");            
               r.Select("STATUS");
               string sqlFilterStationA =
                   string.Format("([StationA.Position.LONGITUDE] >= {0}) AND ([StationA.Position.LONGITUDE] <= {1}) AND ([StationA.Position.LATITUDE] <= {2}) AND ([StationA.Position.LATITUDE] >= {3})",
                   positionRadius.X1, positionRadius.X2, positionRadius.Y1, positionRadius.Y2);
               string sqlFilterStationB =
                      string.Format("([StationB.Position.LONGITUDE] >= {0}) AND ([StationB.Position.LONGITUDE] <= {1}) AND ([StationB.Position.LATITUDE] <= {2}) AND ([StationB.Position.LATITUDE] >= {3})",
                     positionRadius.X1, positionRadius.X2, positionRadius.Y1, positionRadius.Y2);
               string sqlFilter = string.Format("(({0}) OR ({1}))", sqlFilterStationA, sqlFilterStationB).Replace(',', '.');
               r.SetAdditional(sqlFilter);
               r.OrderBy("ID", OrderDirection.Ascending);

               int lastTablID = 0;
               for (r.Open(); !r.IsEOF(); r.MoveNext())
               {
                  if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                     continue; //Отбрасываем ненужные станции

                  int tableID = r.GetI("ID");
                  //if (curStat._tableID != tableID) // якщо айдіха не дорівнює текущій(вибраній) станції
                  //{
                  if (lastTablID != tableID) // на станції передатчиків чи приймачів може бути багато
                  {
                     double freqTXa = r.GetD("StationA.TX_FREQ");
                     double freqTXb = r.GetD("StationB.TX_FREQ");
                     double freqRXa = freqTXb;
                     double freqRXb = freqTXa;

                     bool aPomeha = false, aZhertva = false, bPomeha = false, bZhertva = false;

                     double bwX = r.GetD("Equipment.BW");
                     if (bwX == IM.NullD || bwX < 0)
                        bwX = 28000;
                     bwX /= 1000.0;

                     //if (CalculateOnFrequency(freqTX, TypeOfFrequency.Pomeha, curStat._bwTX, bwX, curStat)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                     //{ createStation = true; }
                     //if (CalculateOnFrequency(freqTX, TypeOfFrequency.Zhertva, curStat._bwRX, bwX, curStat)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                     //{ createStation = true; }

                     //if (createStation)
                     {
                        //Station mainStation = new Station("MICROWA", tableID);
                        //if (!mainStation.ChecsOnNull(mainStation.stationA, Station.TypeOfStation.Radio) ||
                        //   !mainStation.ChecsOnNull(mainStation.stationB, Station.TypeOfStation.Radio))
                        //   continue;

                        //&& mainStation.stationA.STFrequncyRX.Count > 0
                        //if (mainStation.stationA.STFrequncyTX.Count > 0)
                        if (freqTXa != IM.NullD)
                        {
                           //double freqTXa = mainStation.stationA.STFrequncyTX[0];
                           if (CalculateOnFrequency(freqTXa, TypeOfFrequency.Pomeha, curStat._bwTX, bwX, curStat))//CalculateOnFrequency(freqTXa, TypeOfFrequency.Pomeha, _curStat._bwTX, mainStation.stationA._bwRX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                           {
                              aPomeha = true;
                              //_ListPomehaMicroWave.Add(mainStation);
                              //mainStation.stationA.pomehaContain = true;
                           }
                        }
                        //if (mainStation.stationA.STFrequncyRX.Count > 0)
                        if (freqRXa != IM.NullD)
                        {
                           //double freqRXa = mainStation.stationA.STFrequncyRX[0];
                           if (CalculateOnFrequency(freqRXa, TypeOfFrequency.Zhertva, curStat._bwRX, bwX, curStat))//CalculateOnFrequency(freqRXa, TypeOfFrequency.Zhertva, _curStat._bwRX, mainStation.stationA._bwTX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                           {
                              aZhertva = true;
                              //_ListZhertvaMicroWave.Add(mainStation);
                              //mainStation.stationA.zhertvaContain = true;
                           }
                        }

                        //if (mainStation.stationB.STFrequncyTX.Count > 0)
                        if (freqTXb != IM.NullD)
                        {
                           //double freqTXb = mainStation.stationB.STFrequncyTX[0];
                           // проверка попадания на частоту жертви(по передатчику), шукаємо жертви                        
                           if (CalculateOnFrequency(freqTXb, TypeOfFrequency.Pomeha, curStat._bwTX, bwX, curStat))//CalculateOnFrequency(freqTXb, TypeOfFrequency.Pomeha, _curStat._bwTX, mainStation.stationB._bwRX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                           {
                              bPomeha = true;
                              //_ListPomehaMicroWave.Add(mainStation);
                              //mainStation.stationB.pomehaContain = true;
                           }
                        }
                        //if (mainStation.stationB.STFrequncyRX.Count > 0)
                        if (freqRXb != IM.NullD)
                        {
                           //double freqRXb = mainStation.stationB.STFrequncyRX[0];
                           // проверка попадания на частоту жертви(по передатчику), шукаємо жертви                        
                           if (CalculateOnFrequency(freqRXb, TypeOfFrequency.Zhertva, curStat._bwRX, bwX, curStat))//CalculateOnFrequency(freqRXb, TypeOfFrequency.Zhertva, _curStat._bwRX, mainStation.stationB._bwTX)) // проверка попадания на частоту жертви(по передатчику), шукаємо жертви
                           {
                              bZhertva = true;
                              //_ListZhertvaMicroWave.Add(mainStation);
                              //mainStation.stationB.zhertvaContain = true;
                           }
                        }

                        if (aPomeha || bPomeha)
                           _ListPomehaMicroWave.Add(tableID);
                        if (aZhertva || bZhertva)
                           _ListZhertvaMicroWave.Add(tableID);
                        //if (mainStation.stationB.zhertvaContain || mainStation.stationA.zhertvaContain)
                        //   _ListZhertvaMicroWave.Add(mainStation);
                        //if (mainStation.stationB.pomehaContain || mainStation.stationA.pomehaContain)
                        //   _ListPomehaMicroWave.Add(mainStation);
                     }
                  }
                  lastTablID = tableID;
                  //createStation = false;
                  //}
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();  // for displayed button cancel 
               }
               r.Close();
               r.Destroy();
            }

            // Радио линк международный 
            if (typeOfStation == Station.TypeOfStation.ForeignLink)
            {
               using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Searching foreing links...")))
               {
                  pb.SetBig(CLocaliz.TxT("Searching foreing links..."));

                  IMRecordset r = new IMRecordset(ICSMTbl.FxLkClink, IMRecordset.Mode.ReadOnly);
                  r.Select("ID");
                  r.Select("StationA.LONGITUDE,StationA.LATITUDE");
                  r.Select("StationB.LONGITUDE,StationB.LATITUDE");
                  r.Select("StationA.TX_FREQ");
                  r.Select("StationB.TX_FREQ");
                  r.Select("BW");

                  r.SetWhere("StationA.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                  r.SetWhere("StationB.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                  r.SetWhere("StationA.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                  r.SetWhere("StationB.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);

                  r.SetWhere("StationA.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                  r.SetWhere("StationB.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                  r.SetWhere("StationA.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                  r.SetWhere("StationB.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                  r.OrderBy("ID", OrderDirection.Ascending);

                  int lastTablId = 0;

                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                     int tableId = r.GetI("ID");
                     if (lastTablId != tableId) // на станції передатчиків чи приймачів може бути багато
                     {
                        double freqTXa = r.GetD("StationA.TX_FREQ");
                        double freqTXb = r.GetD("StationB.TX_FREQ");
                        double freqRXa = freqTXb;
                        double freqRXb = freqTXa;

                        bool aPomeha = false;
                        bool aZhertva = false;
                        bool bPomeha = false;
                        bool bZhertva = false;

                        double bwX = r.GetD("BW");
                        if (bwX == IM.NullD || bwX < 0)
                           bwX = 28000;
                        bwX /= 1000.0;

                        if ((freqTXa != IM.NullD) &&
                            (CalculateOnFrequency(freqTXa, TypeOfFrequency.Pomeha, curStat._bwTX, bwX, curStat)))
                        {
                           aPomeha = true;
         }
                        if ((freqRXa != IM.NullD) &&
                            (CalculateOnFrequency(freqRXa, TypeOfFrequency.Zhertva, curStat._bwRX, bwX, curStat)))
                        {
                           aZhertva = true;
      }
                        // проверка попадания на частоту жертви(по передатчику), шукаємо жертви                        
                        if ((freqTXb != IM.NullD) &&
                            (CalculateOnFrequency(freqTXb, TypeOfFrequency.Pomeha, curStat._bwTX, bwX, curStat)))
                        {
                           bPomeha = true;
                        }
                        // проверка попадания на частоту жертви(по передатчику), шукаємо жертви                        
                        if ((freqRXb != IM.NullD) &&
                            (CalculateOnFrequency(freqRXb, TypeOfFrequency.Zhertva, curStat._bwRX, bwX, curStat)))
                        {
                           bZhertva = true;
                        }

                        if (aPomeha || bPomeha)
                           _ListPomehaForeingMicroWave.Add(tableId);
                        if (aZhertva || bZhertva)
                           _ListZhertvaForeingMicroWave.Add(tableId);
                     }
                     lastTablId = tableId;
                     pb.SetSmall(countRecord++);
                     isCancelled = isCancelled | pb.UserCanceled();
                  }
                  if (r.IsOpen())
                     r.Close();
                  r.Destroy();
               }
            }
         }
      }

      #endregion

      // проверка попадания на частоту
      private bool CalculateOnFrequency(double freq, TypeOfFrequency typ, double bwTx, double bwRx, Station curStat) // _bwANALIZ  ширина смуги пропускання передавача чи приймача
      {
         double predel1 = freq - _freqDiapazon - (bwTx + bwRx) / 2;
         double predel2 = freq + _freqDiapazon + (bwTx + bwRx) / 2;

         if (typ == TypeOfFrequency.Zhertva)
         {
            foreach (double stantionFreq in curStat.STFrequncyTX)
            {
               if (stantionFreq > predel1 && stantionFreq < predel2)
                  return true;
            }
            return false;
         }
         else
         {
            foreach (double stantionFreq in curStat.STFrequncyRX)
            {
               if (stantionFreq > predel1 && stantionFreq < predel2)
               {
                  return true;
               }
            }
            return false;
         }
      }

      private void FillingStations()
      {         
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("FillingStations")))
         {
            // for Earth Stations
            IEnumerable<int> distID;
            if (_needEarth)
            {
               countRecord = 0;
               pBar.ShowBig(CLocaliz.TxT("Filling stations of Earth"));
               
               distID = _ListPomehaEarth.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("EARTH_STATION", id);
                  if (st.ChecsOnNull(st, Station.TypeOfStation.Earth))
                     stListPomehaEarth.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled |  pBar.UserCanceled();
               }

               distID = _ListZhertvaEarth.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("EARTH_STATION", id);
                  if (st.ChecsOnNull(st, Station.TypeOfStation.Earth))
                     stListZhertvaEarth.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
            }
                        
            // for SRT Stations                  
            if (_needSRT)
            {
               countRecord = 0;
               pBar.ShowBig(CLocaliz.TxT("Filling stations of SRT"));
               
               distID = _ListPomehaSRT.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("MOB_STATION2", id);
                  if (st.ChecsOnNull(st, Station.TypeOfStation.SRT))
                     stListPomehaSRT.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }

               distID = _ListZhertvaSRT.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("MOB_STATION2", id);
                  if (st.ChecsOnNull(st, Station.TypeOfStation.SRT))
                     stListZhertvaSRT.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
            }
                        
            if (_needRadio)
            {
               countRecord = 0;
               pBar.ShowBig(CLocaliz.TxT("Filling stations of Radio"));

               distID = _ListZhertvaMicroWave.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("MICROWA", id);
                  if (st.ChecsOnNull(st.stationA, Station.TypeOfStation.Radio) &&
                     st.ChecsOnNull(st.stationB, Station.TypeOfStation.Radio))
                     stListZhertvaMicroWave.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
               ////////////////////////////////////////////////////
               distID = _ListPomehaMicroWave.Distinct();
               foreach (int id in distID)
               {
                  Station st = new Station("MICROWA", id);
                  if (st.ChecsOnNull(st.stationA, Station.TypeOfStation.Radio) &&
                     st.ChecsOnNull(st.stationB, Station.TypeOfStation.Radio))
                     stListPomehaMicroWave.Add(st);
                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
            }

            if (_needForeing)
            {
               using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Filling foreing stations...")))
               {
                  countRecord = 0;
                  pb.SetBig(CLocaliz.TxT("Filling foreing stations..."));

                  distID = _ListZhertvaForeingMicroWave.Distinct();
                  foreach (int id in distID)
                  {
                     Station st = new Station(ICSMTbl.FxLkClink, id);
                     if (st.ChecsOnNull(st.stationA, Station.TypeOfStation.ForeignLink) &&
                         st.ChecsOnNull(st.stationB, Station.TypeOfStation.ForeignLink))
                        stListZhertvaForeingMicroWave.Add(st);
                     pb.SetSmall(countRecord++);
                     isCancelled = isCancelled | pb.UserCanceled();
         }
                  ////////////////////////////////////////////////////
                  distID = _ListPomehaForeingMicroWave.Distinct();
                  foreach (int id in distID)
                  {
                     Station st = new Station(ICSMTbl.FxLkClink, id);
                     if (st.ChecsOnNull(st.stationA, Station.TypeOfStation.ForeignLink) &&
                         st.ChecsOnNull(st.stationB, Station.TypeOfStation.ForeignLink))
                        stListPomehaForeingMicroWave.Add(st);
                     pb.SetSmall(countRecord++);
                     isCancelled = isCancelled | pb.UserCanceled();
      }
               }
            }
         }
      }

      #region CALCULATE_TD

      private void CalculateTD(List<Station> stListZhertva, List<Station> stListPomeha, Station.TypeOfStation tyPEOFSTAT)
      {
         ///////////////////// станції жертви         
         if ((tyPEOFSTAT != Station.TypeOfStation.Radio) &&
             (tyPEOFSTAT != Station.TypeOfStation.ForeignLink))
         {
            if ((_curCalcStation._typeOfStation == Station.TypeOfStation.Radio) || (_curCalcStation._typeOfStation == Station.TypeOfStation.ForeignLink))
            {
               foreach (Station st in stListZhertva)
               {                  
                  CalculateTDForZhertva(st, _curCalcStation.stationA);
                  CalculateTDForZhertva(st, _curCalcStation.stationB);
               }
               foreach (Station st in stListPomeha)
               {
                  CalculateTDForPomeha(st, _curCalcStation.stationA);
                  CalculateTDForPomeha(st, _curCalcStation.stationB);
               }
            }
            else
            {
               foreach (Station st in stListZhertva)
               {
                  CalculateTDForZhertva(st, _curCalcStation);
               }
               foreach (Station st in stListPomeha)
               {
                  CalculateTDForPomeha(st, _curCalcStation);
               }
            }
         }
         else
         {
            if ((_curCalcStation._typeOfStation == Station.TypeOfStation.Radio) ||
                (_curCalcStation._typeOfStation == Station.TypeOfStation.ForeignLink))
            {
               foreach (Station st in stListZhertva)
               {
                  //if (st.stationA.zhertvaContain)
                  {
                     CalculateTDForZhertva(st.stationA, _curCalcStation.stationA);
                     CalculateTDForZhertva(st.stationA, _curCalcStation.stationB);
                  }
                  //if (st.stationB.zhertvaContain)
                  {
                     CalculateTDForZhertva(st.stationB, _curCalcStation.stationA);
                     CalculateTDForZhertva(st.stationB, _curCalcStation.stationB);
                  }
               }
               foreach (Station st in stListPomeha)
               {
                  //if (st.stationA.pomehaContain)
                  {
                     CalculateTDForPomeha(st.stationA, _curCalcStation.stationA);
                     CalculateTDForPomeha(st.stationA, _curCalcStation.stationB);
                  }
                  //if (st.stationB.pomehaContain)
                  {
                     CalculateTDForPomeha(st.stationB, _curCalcStation.stationA);
                     CalculateTDForPomeha(st.stationB, _curCalcStation.stationB);
                  }
               }
            }
            else
            {
               foreach (Station st in stListZhertva)
               {
                  //if (st.stationA.zhertvaContain)
                  {
                     CalculateTDForZhertva(st.stationA, _curCalcStation);
                  }
                  //if (st.stationB.zhertvaContain)
                  {
                     CalculateTDForZhertva(st.stationB, _curCalcStation);
                  }
               }
               foreach (Station st in stListPomeha)
               {
                  //if (st.stationA.pomehaContain)
                  {
                     CalculateTDForPomeha(st.stationA, _curCalcStation);
                  }
                  //if (st.stationB.pomehaContain)
                  {
                     CalculateTDForPomeha(st.stationB, _curCalcStation);
                  }
               }
            }
         }
      }

      private void CalculateTDForPomeha(Station st, Station curStat)
      {
         //curStation - жертва, station - помеха
          using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("CalculateTD")))
         {
            foreach (double TX in st.STFrequncyTX)
            {
               foreach (double curRX in curStat.STFrequncyRX)
               {
                   double irf = 0;
                   irf = IRF.GetIRF(TX, curRX, st._bwTX, curStat._bwRX, IRF.System.Digital);
                   st.STIRFList.Add(irf);

                   double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                                                           st._longitude, st._latitude);

                   double l525 = 60.0;
                   if (ApplSetting.IsDebugCalculation)
                   {
                       if (d > 0.0)
                       {
                           if ((TX > 3000.0) ||
                               (st._typeOfStation != Station.TypeOfStation.SRT) ||
                               (curStat._typeOfStation != Station.TypeOfStation.SRT))
                           {
                               double TXggc = TX/1000; // переводим в ГГц
                               l525 = L525.GetL525(TXggc, d);
                           }
                           else
                           {
                               double e;
                               if(curStat.AboveEarthLevel > st.AboveEarthLevel)
                                   e = _1546_4.Get_E(curStat.AboveEarthLevel, curStat.AboveEarthLevel, d, TX, 1, curStat._aboveSeaLevel, st.AboveEarthLevel);
                               else
                                   e = _1546_4.Get_E(st.AboveEarthLevel, st.AboveEarthLevel, d, TX, 1, st._aboveSeaLevel, curStat.AboveEarthLevel);
                               l525 = 139.3 - e + 20.0*Math.Log10(TX);
                           }
                       }
                   }
                   else
                   {
                       if (d != 0)
                       {
                           double TXggc = TX/1000; // переводим в ГГц
                           l525 = L525.GetL525(TXggc, d);
                       }
                   }

                   st.STL525List.Add(l525);

                  // получаю азимуты
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(st._longitude, st._latitude,
                      curStat._longitude, curStat._latitude, ref a1, ref a2);

                  // получаю углы места
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double GT = 0, GR = 0, TD = 0, PI = 0, F_ = 0, F_int = 0;

                  // calculating GR for current station
                  double deltaA2 = Math.Abs(a2 - curStat._azimuth);
                  double deltaB2 = Math.Abs(b2 - curStat._bElevation);
                  switch (curStat._typeOfStation)
                  {
                     case Station.TypeOfStation.SRT:
                        GR = StationUtility.AntennGainCalculation(a2, curStat._azimuth, b2, curStat._bElevation, curStat._gMax, curStat._aDiagH, curStat._bDiagV, curStat._antenDIAGA);
                        break;

                     case Station.TypeOfStation.Earth:
                        GR = StationUtility.AntennaGainWithAntDiam(curStat._diametrE, deltaA2, deltaB2, TX);
                        break;

                     case Station.TypeOfStation.Radio:
                        GR = StationUtility.AntennaGainWithMaxGain(curStat._gMax, deltaA2, deltaB2, TX);
                        break;
                     case Station.TypeOfStation.ForeignLink:
                        if ((curStat._gMax < 25.0) &&
                            !string.IsNullOrEmpty(curStat._bDiagV) && !string.IsNullOrEmpty(curStat._aDiagH))
                        {
                           GR = StationUtility.AntennGainCalculation(a2, curStat._azimuth, b2, curStat._bElevation, curStat._gMax, curStat._aDiagH, curStat._bDiagV, curStat._antenDIAGA);
                  }
                        else
                        {
                           GR = StationUtility.AntennaGainWithMaxGain(curStat._gMax, deltaA2, deltaB2, TX);
                        }
                        break;

                  }

                  // calculating GT for station with viborka
                  double deltaA1 = Math.Abs(a1 - st._azimuth);
                  double deltaB1 = Math.Abs(b1 - st._bElevation);
                  switch (curStat._typeOfStation)
                  {
                     case Station.TypeOfStation.SRT:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = st._power + GT - st._lFT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GT = StationUtility.AntennaGainWithAntDiam(st._diametrE, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              }
                              PI = st._power + GT - st._lFT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                        break;
                        }
                        break;

                     case Station.TypeOfStation.Earth:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = st._power + GT - st._lFT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GT = StationUtility.AntennaGainWithAntDiam(st._diametrE, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              }
                              PI = st._power + GT - st._lFT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                        break;
                        }
                        break;

                     case Station.TypeOfStation.Radio:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = st._power + GT - st._lFT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = curStat._carrier - curStat._kdbf - curStat._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.Earth:
                              GT = StationUtility.AntennaGainWithAntDiam(st._diametrE, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              F_ = curStat._carrier - curStat._kdbf - curStat._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.Radio:
                              GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              F_ = curStat._carrier - curStat._kdbf - curStat._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              }
                              PI = st._power + GT - st._lFT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = curStat._carrier - curStat._kdbf - curStat._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                        break;
                  }
                        break;
                     case Station.TypeOfStation.ForeignLink:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GT = StationUtility.AntennGainCalculation(a1, st._azimuth, b1, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = st._power + GT - st._lFT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GT = StationUtility.AntennaGainWithAntDiam(st._diametrE, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GT = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA1, deltaB1, TX);
                              PI = st._power + GT - l525 + GR - curStat._lFR - irf;
                              TD = StationUtility.TD(PI, curStat._kdbf);
                              break;
                        }
                        break;

                  }
                  ////////////////////////////////////////////////////////               
                  st.STTD.Add(TD);
                  st.PI.Add(PI);
                  st.F_.Add(F_);
                  st.F_int.Add(F_int);
                  st.GT.Add(GT);
                  st.GR.Add(GR);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.IRF.Add(irf);
                  st.DISTANCE.Add(d);
                  st.CURTX.Add(TX);
                  st.CURRX.Add(curRX);
                  st._countCalcTD++;

                  pBar.ShowSmall(countRecord++);                  
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
            }
         }
      }

      private void CalculateTDForZhertva(Station st, Station curStat)
      {
         // Здесь curStat это помеха st - жертва.
          using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("CalculateTD")))
         {      
            foreach (double RX in st.STFrequncyRX)
            {
               foreach (double curTX in curStat.STFrequncyTX)
               {
                  double irf = 0;
                  irf = IRF.GetIRF(curTX, RX, curStat._bwTX, st._bwRX, IRF.System.Digital);
                  st.STIRFList.Add(irf);

                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                      st._longitude, st._latitude);

                  double l525 = 60.0;
                  if (ApplSetting.IsDebugCalculation)
                  {
                      if (d > 0.0)
                      {
                          if ((curTX > 3000.0) ||
                              (st._typeOfStation != Station.TypeOfStation.SRT) ||
                              (curStat._typeOfStation != Station.TypeOfStation.SRT))
                          {
                              double TXggc = curTX / 1000; // переводим в ГГц
                              l525 = L525.GetL525(TXggc, d);
                          }
                          else
                          {
                              double e;
                              if (curStat.AboveEarthLevel > st.AboveEarthLevel)
                                  e = _1546_4.Get_E(curStat.AboveEarthLevel, curStat.AboveEarthLevel, d, curTX, 1, curStat._aboveSeaLevel, st.AboveEarthLevel);
                              else
                                  e = _1546_4.Get_E(st.AboveEarthLevel, st.AboveEarthLevel, d, curTX, 1, st._aboveSeaLevel, curStat.AboveEarthLevel);
                              l525 = 139.3 - e + 20.0 * Math.Log10(curTX);
                          }
                      }
                  }
                  else
                  {
                      if (d != 0)
                      {
                          double TXggc = curTX / 1000.0; // переводим в ГГц
                          l525 = L525.GetL525(TXggc, d);
                      }
                  }

                  st.STL525List.Add(l525);

                  // получаю азимуты
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
                      st._longitude, st._latitude, ref a1, ref a2);

                  // получаю углы места
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double GT = 0, GR = 0, TD = 0, PI = 0, F_ = 0, F_int = 0;

                  // calculating GT for current station
                  double deltaA1 = Math.Abs(a1 - curStat._azimuth);
                  double deltaB1 = Math.Abs(b1 - curStat._bElevation);
                  switch (curStat._typeOfStation)
                  {
                     case Station.TypeOfStation.SRT:
                        GT = StationUtility.AntennGainCalculation(a1, curStat._azimuth, b1, curStat._bElevation, curStat._gMax, curStat._aDiagH, curStat._bDiagV, curStat._antenDIAGA);
                        break;

                     case Station.TypeOfStation.Earth:
                        GT = StationUtility.AntennaGainWithAntDiam(curStat._diametrE, deltaA1, deltaB1, RX);
                        break;

                     case Station.TypeOfStation.Radio:
                        GT = StationUtility.AntennaGainWithMaxGain(curStat._gMax, deltaA1, deltaB1, RX);
                        break;
                     case Station.TypeOfStation.ForeignLink:
                        if ((curStat._gMax < 25.0) &&
                            !string.IsNullOrEmpty(curStat._bDiagV) && !string.IsNullOrEmpty(curStat._aDiagH))
                        {
                           GT = StationUtility.AntennGainCalculation(a1, curStat._azimuth, b1, curStat._bElevation, curStat._gMax, curStat._aDiagH, curStat._bDiagV, curStat._antenDIAGA);
                  }
                        else
                        {
                           GT = StationUtility.AntennaGainWithMaxGain(curStat._gMax, deltaA1, deltaB1, RX);
                        }
                        break;
                  }

                  // calculating GR for station with viborka
                  double deltaA2 = Math.Abs(a2 - st._azimuth);
                  double deltaB2 = Math.Abs(b2 - st._bElevation);
                  switch (curStat._typeOfStation)
                  {
                     case Station.TypeOfStation.SRT:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = curStat._power + GT - curStat._lFT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GR = StationUtility.AntennaGainWithAntDiam(st._diametrR, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - curStat._lFT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - curStat._lFT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = st._carrier - st._kdbf - st._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              }
                              PI = curStat._power + GT - curStat._lFT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                        break;
                        }
                        break;

                     case Station.TypeOfStation.Earth:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = curStat._power + GT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GR = StationUtility.AntennaGainWithAntDiam(st._diametrE, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = st._carrier - st._kdbf - st._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              }
                              PI = curStat._power + GT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                        break;
                        }
                        break;

                     case Station.TypeOfStation.Radio:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = curStat._power + GT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GR = StationUtility.AntennaGainWithAntDiam(st._diametrR, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = st._carrier - st._kdbf - st._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                           case Station.TypeOfStation.ForeignLink:
                              if ((st._gMax < 25.0) &&
                                  !string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(st._aDiagH))
                              {
                                 GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                        }
                              else
                              {
                                 GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              }
                              PI = curStat._power + GT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                        break;
                  }
                        break;
                     case Station.TypeOfStation.ForeignLink:
                        switch (st._typeOfStation)
                        {
                           case Station.TypeOfStation.SRT:
                              GR = StationUtility.AntennGainCalculation(a2, st._azimuth, b2, st._bElevation, st._gMax, st._aDiagH, st._bDiagV, st._antenDIAGA);
                              PI = curStat._power + GT - l525 + GR - st._lFR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Earth:
                              GR = StationUtility.AntennaGainWithAntDiam(st._diametrR, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              break;
                           case Station.TypeOfStation.Radio:
                              GR = StationUtility.AntennaGainWithMaxGain(st._gMax, deltaA2, deltaB2, RX);
                              PI = curStat._power + GT - l525 + GR - irf;
                              TD = StationUtility.TD(PI, st._kdbf);
                              F_ = st._carrier - st._kdbf - st._s_n; // расчет запаса без помех
                              F_int = F_  - TD; // расчет запаса на замирание с момехой
                              break;
                        }
                        break;
                  }
                  /////////////////////////////////////////////////////////                  
                  st.STTD.Add(TD);
                  st.PI.Add(PI);
                  st.F_.Add(F_);
                  st.F_int.Add(F_int);
                  st.GT.Add(GT);
                  st.GR.Add(GR);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.IRF.Add(irf);
                  st.DISTANCE.Add(d);
                  st.CURTX.Add(curTX);
                  st.CURRX.Add(RX);
                  st._countCalcTD++;

                  pBar.ShowSmall(countRecord++);
                  isCancelled = isCancelled | pBar.UserCanceled();
               }
            }
         }
      }

      #region CHECKS_TD
      /// <summary>
      /// checks the station on value of TD
      /// </summary>
      /// <param name="stList">any station</param>
      private void CheckOnAllowableTD(ref List<Station> stList)
      {
         List<Station> dublicate = new List<Station>();
         if (stList.Count > 0)
         {
            if (stList[0]._typeOfStation != Station.TypeOfStation.Radio)
            {
               foreach (Station st in stList)
               {
                  if (CheckOnMaxData(st))
                     dublicate.Add(st);
               }

               stList.Clear();
               stList = dublicate;
            }
            else
            {
               foreach (Station st in stList)
               {
                  bool fl = false;

                  if (CheckOnMaxData(st.stationA))
                     fl = true;

                  if (CheckOnMaxData(st.stationB))
                     fl = true;
                  
                  if(fl)
                    dublicate.Add(st);
               }

               stList.Clear();
               stList = dublicate;
            }
         }
      }

      /// <summary>
      /// checks the station on maximum value of TD and compares with an allowable value and then clears other values of TD, IRF, PI.
      /// </summary>
      /// <param name="st">any Station</param>
      /// <returns>true if the value of TD more then allowable value</returns>
      private bool CheckOnMaxData(Station st)
      {
         if (st.STTD.Count > 0)
         {
            st._show = true;
            double maxTD = st.STTD[0];
            int index = 0;
            for (int i = 0; i < st.STTD.Count; i++)
            {
               if (maxTD < st.STTD[i])
               {
                  maxTD = st.STTD[i];
                  index = i;
               }
            }
            if (_compareTD > maxTD)
               return false;

            double irf = st.IRF[index];
            double pi = st.PI[index];
            double F_ = st.F_[index];
            double F_int = st.F_int[index];
            double a1 = st.A1[index];
            double a2 = st.A2[index];
            double b1 = st.B1[index];
            double b2 = st.B2[index];
            double gt = st.GT[index];
            double gr = st.GR[index];
            double d = st.DISTANCE[index];
            double maxTX = st.CURTX[index];
            double maxRX = st.CURRX[index];
            double l525 = st.STL525List[index];
            

            st.STTD.Clear();
            st.PI.Clear();
            st.F_.Clear();
            st.F_int.Clear();
            st.IRF.Clear();
            st.A1.Clear();
            st.A2.Clear();
            st.B1.Clear();
            st.B2.Clear();
            st.GT.Clear();
            st.GR.Clear();
            st.CURTX.Clear();
            st.CURRX.Clear();
            st.DISTANCE.Clear();
            st.STL525List.Clear();

            st.STTD.Add(maxTD);
            st.PI.Add(pi);
            st.F_.Add(F_);
            st.F_int.Add(F_int);
            st.IRF.Add(irf);
            st.A1.Add(a1);
            st.A2.Add(a2);
            st.B1.Add(b1);
            st.B2.Add(b2);
            st.GT.Add(gt);
            st.GR.Add(gr);
            st.DISTANCE.Add(d);
            st.CURTX.Add(maxTX);
            st.CURRX.Add(maxRX);
            st.STL525List.Add(l525);

            return true;
         }
         else
            return false;
      }

      #endregion

      #endregion

      #endregion

      #region SHOW_RESULT

      public void ShowResult()
      {
          List<RecordPtr> listData = new List<RecordPtr>();
          List<string> listCaption = new List<string>();
          string ownerRemark = string.Empty;
          using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculation")))
          {
              pBar.ShowBig(CLocaliz.TxT("Prepare result of data..."));
              isCancelled = isCancelled | pBar.UserCanceled();

              Map.TxsOnMap.ClearAllStationList();
              foreach (Station station in curStatList)
              {
                  Map.TxOnMap tx = new TxOnMap(station._table, station._tableID);
                  tx.IsCalculation = true;
                  Map.TxsOnMap.AddSpecStation(tx);
              }

              if (_needRadio)
              {
                  // Очистка таблицы XNRFA_BG_MICROWA
                  /*
                  IMRecordset  r_xnrfa_ = new IMRecordset("XNRFA_BG_MICROWA", IMRecordset.Mode.ReadWrite);
                  r_xnrfa_.Select("ID,OBJ_ID,BAG_ID,F_,F_int,TYPE");
                  r_xnrfa_.SetWhere("ID", IMRecordset.Operation.Neq, 0);
                  for (r_xnrfa_.Open(); !r_xnrfa_.IsEOF(); r_xnrfa_.MoveNext())
                  {
                      r_xnrfa_.Delete();
                  }
                  r_xnrfa_.Close();
                   */ 
                  listData.Add(FillBagStations(stListZhertvaMicroWave, Station.TypeOfStation.Radio,
                                               TypeOfFrequency.Zhertva));
                  listData.Add(FillBagStations(stListPomehaMicroWave, Station.TypeOfStation.Radio,
                                               TypeOfFrequency.Pomeha));
                  listCaption.Add("Жертви РРЛ (" + stListZhertvaMicroWave.Count + ")");
                  listCaption.Add("Завади РРЛ (" + stListPomehaMicroWave.Count + ")");
              }
              if (_needEarth)
              {
                  listData.Add(FillBagStations(stListZhertvaEarth, Station.TypeOfStation.Earth,
                                               TypeOfFrequency.Zhertva));
                  listData.Add(FillBagStations(stListPomehaEarth, Station.TypeOfStation.Earth,
                                               TypeOfFrequency.Pomeha));
                  listCaption.Add("Жертви ЗС (" + stListZhertvaEarth.Count + ")");
                  listCaption.Add("Завади ЗС (" + stListPomehaEarth.Count + ")");
              }
              if (_needSRT)
              {
                  listData.Add(FillBagStations(stListZhertvaSRT, Station.TypeOfStation.SRT,
                                               TypeOfFrequency.Zhertva));
                  listData.Add(FillBagStations(stListPomehaSRT, Station.TypeOfStation.SRT,
                                               TypeOfFrequency.Pomeha));
                  listCaption.Add("Жертви СРТ (" + stListZhertvaSRT.Count + ")");
                  listCaption.Add("Завади СРТ (" + stListPomehaSRT.Count + ")");
              }
              //------------------
              //Добавляем ислед. станцию
              foreach (Station station in curStatList)
              {
                  station._show = true;
                  if (station.stationA != null)
                      station.stationA._show = true;
                  if (station.stationB != null)
                      station.stationB._show = true;
              }
              Plugin.curStation = curStatList[0];
              Plugin.listData = listData;
              listData.Add(FillBagStations(curStatList.ToList(), curStatList[0]._typeOfStation, TypeOfFrequency.Pomeha));
              listCaption.Add("Текущая станция");
              Station.GetOwnerData(curStatList[0], ref ownerRemark);
          }
          IMBag.Display2(listData.ToArray(), listCaption.ToArray(), ownerRemark, null);
          foreach (RecordPtr h in listData)
          {
              IMBag.DeleteBag(h);
          }
          Map.TxsOnMap.ClearAllStationList();

          //=====================================================
          // Загрузка данных по международным станциям
          Station curStationForMs = curStatList[0];
          using (CalculationEMC.FrmCalculationEmsResult resultCalculationForm = new CalculationEMC.FrmCalculationEmsResult())
          {
              bool isNeedToShow = false;
              //------
              curStationForMs.LoadAdditionalFields();
              resultCalculationForm.OwnerCurStation = curStationForMs._owner;
              if ((curStationForMs.stationA != null) && (!string.IsNullOrEmpty(curStationForMs.stationA._owner)))
                  resultCalculationForm.OwnerCurStation = curStationForMs.stationA._owner;
              resultCalculationForm.AddressCurStation = curStationForMs._remark;
              if ((curStationForMs.stationA != null) && (!string.IsNullOrEmpty(curStationForMs.stationA._remark)))
                  resultCalculationForm.AddressCurStation = curStationForMs.stationA._remark;
              resultCalculationForm.IdCurStation = curStationForMs._tableID;
              resultCalculationForm.TableNameCurStation = curStationForMs._table;
              //------
              List<Calculation.InterferenceEmcForeignMicrowaRow> lstInterfForeignWicrova =
                  new List<Calculation.InterferenceEmcForeignMicrowaRow>();
              while (stListPomehaForeingMicroWave.Count > 0)
              {
                  Station curStationA = null;
                  Station curStationB = null;
                  Station stBase = stListPomehaForeingMicroWave[0];
                  stBase.LoadAdditionalFields();
                  Station st = null;
                  Station stb = null;
                  int index = 0;

                  double td00 = (stBase.stationA.STTD.Count > 0) ? stBase.stationA.STTD[0] : -999;
                  double td01 = (stBase.stationA.STTD.Count > 1) ? stBase.stationA.STTD[1] : -999;
                  double td10 = (stBase.stationB.STTD.Count > 0) ? stBase.stationB.STTD[0] : -999;
                  double td11 = (stBase.stationB.STTD.Count > 1) ? stBase.stationB.STTD[1] : -999;

                  if ((td00 > td01) && (td00 > td10) && (td00 > td11))
                  {
                      st = stBase.stationA;
                      stb = stBase.stationB;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationA != null) && (curStationForMs.stationA._tableID != 0) &&
                          (curStationForMs.stationA._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationA;
                          curStationB = curStationForMs.stationB;
                      }
                      index = 0;
                  }
                  else if ((td01 > td00) && (td01 > td10) && (td01 > td11))
                  {
                      st = stBase.stationA;
                      stb = stBase.stationB;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationB != null) && (curStationForMs.stationB._tableID != 0) &&
                          (curStationForMs.stationB._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationB;
                          curStationB = curStationForMs.stationA;
                      }
                      index = 1;
                  }
                  else if ((td10 > td00) && (td10 > td01) && (td10 > td11))
                  {
                      st = stBase.stationB;
                      stb = stBase.stationA;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationA != null) && (curStationForMs.stationA._tableID != 0) &&
                          (curStationForMs.stationA._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationA;
                          curStationB = curStationForMs.stationB;
                      }
                      index = 0;
                  }
                  else
                  {
                      st = stBase.stationB;
                      stb = stBase.stationA;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationB != null) && (curStationForMs.stationB._tableID != 0) &&
                          (curStationForMs.stationB._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationB;
                          curStationB = curStationForMs.stationA;
                      }
                      index = 1;
                  }
                  if ((curStationB != null) && ((curStationB._tableID == 0) || (curStationB._tableID == IM.NullI)))
                      curStationB = null;
                  if (curStationB == null)
                      curStationB = curStationA;
                  if (st._tableID != curStationA._tableID)
                  {
                      //=====
                      Calculation.InterferenceEmcForeignMicrowaRow tmpRow =
                          new Calculation.InterferenceEmcForeignMicrowaRow();
                      if (st.STTD.Count()>0)
                      {
                          tmpRow.IsCheckBoxVisible = true;
                          tmpRow.Description.Value = st.Name;
                          tmpRow.TableName.Value = stBase._table;
                          tmpRow.StationId.Value = st._tableID;
                          tmpRow.Td.Value = st.STTD[index];
                          tmpRow.Irf.Value = st.IRF[index];
                          tmpRow.Pi.Value = st.PI[index];


                          tmpRow.Distance.Value = st.DISTANCE[index];
                          tmpRow.Loss.Value = st.STL525List[index];
                          //----
                          tmpRow.GainNoise.Value = st.GT[index];
                          tmpRow.AzimuthNoise.Value = st.A1[index];
                          tmpRow.FreqNoise.Value = st.CURTX[index];
                          tmpRow.DeltaFNoise.Value = st._bwTX;
                          tmpRow.AmaxNoise.Value = st._azimuth;
                          tmpRow.GmaxNoise.Value = st._gMax;
                          tmpRow.PowerNoise.Value = st._power;
                          tmpRow.PositionStationNoise = new IMPosition(st._longitude, st._latitude, "4DEC");
                          tmpRow.PositionLinkNoise = new IMPosition(stb._longitude, stb._latitude, "4DEC");
                          //----
                          tmpRow.GainSacrifice.Value = st.GR[index];
                          tmpRow.AzimuthSacrifice.Value = st.A2[index];
                          tmpRow.FreqSacrifice.Value = st.CURRX[index];
                          tmpRow.DeltaFSacrifice.Value = curStationA._bwRX;
                          tmpRow.KtbfSacrifice.Value = curStationA._kdbf - 30;
                          tmpRow.AmaxSacrifice.Value = curStationA._azimuth;
                          tmpRow.GmaxSacrifice.Value = curStationA._gMax;
                          tmpRow.PositionStationSacrifice = new IMPosition(curStationA._longitude, curStationA._latitude,
                                                                           "4DEC");
                          tmpRow.PositionLinkSacrifice = new IMPosition();
                          if (curStationB != null)
                              tmpRow.PositionLinkSacrifice = new IMPosition(curStationB._longitude,
                                                                            curStationB._latitude,
                                                                            "4DEC");
                          lstInterfForeignWicrova.Add(tmpRow);
                      }
                  }
                  //-----
                  // освобождаем память
                  stListPomehaForeingMicroWave.RemoveAt(0);
              }
              if (lstInterfForeignWicrova.Count > 0)
              {
                  TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab("Помеха",
                                                                                       lstInterfForeignWicrova,
                                                                                       "PomehaForeingMicrowave");
                  newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                  isNeedToShow = true;
              }
              //====================================================
              lstInterfForeignWicrova = new List<Calculation.InterferenceEmcForeignMicrowaRow>();
              while (stListZhertvaForeingMicroWave.Count > 0)
              {
                  Station curStationA = null;
                  Station curStationB = null;
                  Station stBase = stListZhertvaForeingMicroWave[0];
                  stBase.LoadAdditionalFields();
                  Station st = null;
                  Station stb = null;
                  int index = 0;

                  double td00 = (stBase.stationA.STTD.Count > 0) ? stBase.stationA.STTD[0] : -999;
                  double td01 = (stBase.stationA.STTD.Count > 1) ? stBase.stationA.STTD[1] : -999;
                  double td10 = (stBase.stationB.STTD.Count > 0) ? stBase.stationB.STTD[0] : -999;
                  double td11 = (stBase.stationB.STTD.Count > 1) ? stBase.stationB.STTD[1] : -999;

                  if ((td00 > td01) && (td00 > td10) && (td00 > td11))
                  {
                      st = stBase.stationA;
                      stb = stBase.stationB;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationA != null) && (curStationForMs.stationA._tableID != 0) &&
                          (curStationForMs.stationA._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationA;
                          curStationB = curStationForMs.stationB;
                      }
                      index = 0;
                  }
                  else if ((td01 > td00) && (td01 > td10) && (td01 > td11))
                  {
                      st = stBase.stationA;
                      stb = stBase.stationB;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationB != null) && (curStationForMs.stationB._tableID != 0) &&
                          (curStationForMs.stationB._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationB;
                          curStationB = curStationForMs.stationA;
                      }
                      index = 1;
                  }
                  else if ((td10 > td00) && (td10 > td01) && (td10 > td11))
                  {
                      st = stBase.stationB;
                      stb = stBase.stationA;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationA != null) && (curStationForMs.stationA._tableID != 0) &&
                          (curStationForMs.stationA._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationA;
                          curStationB = curStationForMs.stationB;
                      }
                      index = 0;
                  }
                  else
                  {
                      st = stBase.stationB;
                      stb = stBase.stationA;
                      curStationA = curStationForMs;
                      curStationB = null;
                      if ((curStationForMs.stationB != null) && (curStationForMs.stationB._tableID != 0) &&
                          (curStationForMs.stationB._tableID != IM.NullI))
                      {
                          curStationA = curStationForMs.stationB;
                          curStationB = curStationForMs.stationA;
                      }
                      index = 1;
                  }
                  if (st._tableID != curStationA._tableID)
                  {
                      //=====
                      Calculation.InterferenceEmcForeignMicrowaRow tmpRow =
                          new Calculation.InterferenceEmcForeignMicrowaRow();

                      if (st.STTD.Count() > 0)
                      {
                          tmpRow.IsCheckBoxVisible = true;
                          tmpRow.Description.Value = st.Name;
                          tmpRow.TableName.Value = stBase._table;
                          tmpRow.StationId.Value = st._tableID;
                          tmpRow.Td.Value = st.STTD[index];
                          tmpRow.Irf.Value = st.IRF[index];
                          tmpRow.Pi.Value = st.PI[index];
                          tmpRow.Distance.Value = st.DISTANCE[index];
                          tmpRow.Loss.Value = st.STL525List[index];
                          //----
                          tmpRow.GainNoise.Value = st.GT[index];
                          tmpRow.AzimuthNoise.Value = st.A1[index];
                          tmpRow.FreqNoise.Value = st.CURTX[index];
                          tmpRow.DeltaFNoise.Value = curStationA._bwTX;
                          tmpRow.AmaxNoise.Value = curStationA._azimuth;
                          tmpRow.GmaxNoise.Value = curStationA._gMax;
                          tmpRow.PowerNoise.Value = curStationA._power;
                          tmpRow.PositionStationNoise = new IMPosition(curStationA._longitude, curStationA._latitude,
                                                                       "4DEC");
                          tmpRow.PositionLinkNoise = new IMPosition();
                          if (curStationB != null)
                              tmpRow.PositionLinkNoise = new IMPosition(curStationB._longitude, curStationB._latitude,
                                                                        "4DEC");
                          //----
                          tmpRow.GainSacrifice.Value = st.GR[index];
                          tmpRow.AzimuthSacrifice.Value = st.A2[index];
                          tmpRow.FreqSacrifice.Value = st.CURRX[index];
                          tmpRow.DeltaFSacrifice.Value = st._bwRX;
                          tmpRow.KtbfSacrifice.Value = st._kdbf - 30;
                          tmpRow.AmaxSacrifice.Value = st._azimuth;
                          tmpRow.GmaxSacrifice.Value = st._gMax;
                          tmpRow.PositionStationSacrifice = new IMPosition(st._longitude, st._latitude, "4DEC");
                          tmpRow.PositionLinkSacrifice = new IMPosition(stb._longitude, stb._latitude, "4DEC");
                          lstInterfForeignWicrova.Add(tmpRow);
                      }
                  }
                  //-----
                  // освобождаем память
                  stListZhertvaForeingMicroWave.RemoveAt(0);
              }
              if (lstInterfForeignWicrova.Count > 0)
              {
                  TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab("Жертва",
                                                                                       lstInterfForeignWicrova,
                                                                                       "ZhertvaForeingMicrowave");
                  newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                  isNeedToShow = true;
              }
              //====================================================
              if (isNeedToShow == true)
                  resultCalculationForm.ShowDialog();
          }
      }

       private RecordPtr FillBagStations(List<Station> stList, Station.TypeOfStation typeOfStation,
         TypeOfFrequency typeFreq)
      {
          IMRecordset r = null; //IMRecordset r_xnrfa_ = null;
         RecordPtr b1 = new RecordPtr();

         if (typeOfStation == Station.TypeOfStation.ForeignLink)
         {
             b1 = IMBag.CreateTemporary(ICSMTbl.FxLkClink);
             r = new IMRecordset("BG_FXLK_CLINK", IMRecordset.Mode.ReadWrite);
             r.Select("BAG_ID,OBJ_ID");
             r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
             r.Open();
             foreach (Station st in stList)
             {
                 r.AddNew();
                 r.Put("BAG_ID", b1.Id);
                 r.Put("OBJ_ID", st._tableID);
                 r.Update();
             }
         }
         else if (typeOfStation == Station.TypeOfStation.Radio)
         {

             //r = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
             //r.Select("BAG_ID,OBJ_ID,TD,IRF,Pi,DISTANCE,GAIN_SACRIFICE,GAIN_NOISE,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,MICROWA_ID,FREQ_SACRIFICE,FREQ_NOISE");
             //r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
             //r.Open();

             b1 = IMBag.CreateTemporary("MICROWA");
             r = new IMRecordset("BG_MICROWA", IMRecordset.Mode.ReadWrite);
             r.Select("BAG_ID,OBJ_ID,TD,IRF,Pi,DISTANCE,GAIN_SACRIFICE,GAIN_NOISE,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,MICROWA_ID,FREQ_SACRIFICE,FREQ_NOISE,CUST_TXT2,CUST_TXT3");
             r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
             r.Open();


             /*
             r_xnrfa_ = new IMRecordset("XNRFA_BG_MICROWA", IMRecordset.Mode.ReadWrite);
             r_xnrfa_.Select("ID,OBJ_ID,BAG_ID,F_,F_int,TYPE");
             r_xnrfa_.SetWhere("ID", IMRecordset.Operation.Eq, 0);
             r_xnrfa_.Open();
              */ 
             


             //bool fl = false;
             Station st;

             foreach (Station ST in stList)
             {
                 double staTD = 0, stbTD = 0;
                 if (typeFreq == TypeOfFrequency.Zhertva)
                 {
                     //if (ST.stationA.zhertvaContain)
                     {
                         if (ST.stationA.STTD.Count > 0)
                             staTD = ST.stationA.STTD[0];
                     }
                     //if (ST.stationB.zhertvaContain)
                     {
                         if (ST.stationB.STTD.Count > 0)
                             stbTD = ST.stationB.STTD[0];
                     }
                 }
                 else
                 {
                     //if (ST.stationA.pomehaContain)
                     {
                         if (ST.stationA.STTD.Count > 0)
                             staTD = ST.stationA.STTD[0];
                     }
                     //if (ST.stationB.pomehaContain)
                     {
                         if (ST.stationB.STTD.Count > 0)
                             stbTD = ST.stationB.STTD[0];
                     }
                 }

                 if (staTD > stbTD)
                     st = ST.stationA;
                 else
                     st = ST.stationB;

                 if (st._tableID == 0)
                     continue;

                 if (!st._show)
                     continue;

                 r.AddNew();
                 r.Put("BAG_ID", b1.Id);
                 r.Put("OBJ_ID", st._tableID);

                 /*
                 r_xnrfa_.AddNew();
                 int id = IM.AllocID("XNRFA_BG_MICROWA", 1, -1);
                 r_xnrfa_.Put("ID", id);
                 r_xnrfa_.Put("BAG_ID", b1.Id);
                 r_xnrfa_.Put("OBJ_ID", st._tableID);
                  */ 


                 if (st.STTD.Count > 0)
                 {
                     /*
                     r_xnrfa_.Put("F_int", st.F_int[0]);  
                     r_xnrfa_.Put("F_", st.F_[0]);
                     r_xnrfa_.Put("TYPE", typeFreq.ToString());  
                      */ 
                     
                                          
                     r.Put("TD", st.STTD[0]);
                     r.Put("IRF", st.IRF[0]);
                     r.Put("Pi", st.PI[0]);
                     r.Put("DISTANCE", st.DISTANCE[0]);
                     r.Put("GAIN_SACRIFICE", st.GT[0]);
                     r.Put("GAIN_NOISE", st.GR[0]);
                     r.Put("AZIMUTH_SACRIFICE", st.A1[0]);
                     r.Put("AZIMUTH_NOISE", st.A2[0]);
                     r.Put("LOSS", st.STL525List[0]);
                     r.Put("FREQ_SACRIFICE", st.CURTX[0]);
                     r.Put("FREQ_NOISE", st.CURRX[0]);
                     r.Put("MICROWA_ID", ST._tableID);
                     //r.Put("CUST_TXT1", typeFreq.ToString());
                     r.Put("CUST_TXT2", Math.Round(st.F_[0],2).ToString().Replace(",","."));
                     r.Put("CUST_TXT3", Math.Round(st.F_int[0],2).ToString().Replace(",", "."));
                 }
                 r.Update();
                 //r_xnrfa_.Update();
             }

            
             
         }
         else if (typeOfStation == Station.TypeOfStation.Earth || typeOfStation == Station.TypeOfStation.SRT)
         {
             if (typeOfStation == Station.TypeOfStation.Earth)
             {
                 b1 = IMBag.CreateTemporary("EARTH_STATION");
                 r = new IMRecordset("BG_EARTH_STATION", IMRecordset.Mode.ReadWrite);
             }
             else
             {
                 b1 = IMBag.CreateTemporary("MOB_STATION2");
                 r = new IMRecordset("BG_MOB_STATION2", IMRecordset.Mode.ReadWrite);
             }
             if (typeOfStation == Station.TypeOfStation.Earth)
                 r.Select("BAG_ID,OBJ_ID,TD,IRF,Pi,DISTANCE,GAIN_SACRIFICE,GAIN_NOISE,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,EARTH_STAT_ID,FREQ_SACRIFICE,FREQ_NOISE");
             else
                 r.Select("BAG_ID,OBJ_ID,TD,IRF,Pi,DISTANCE,GAIN_SACRIFICE,GAIN_NOISE,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,MOB_STAT2_ID,FREQ_SACRIFICE,FREQ_NOISE");

             r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
             r.Open();

             foreach (Station st in stList)
             {
                 if (st._tableID == 0)
                     continue;

                 if (!st._show)
                     continue;

                 r.AddNew();
                 r.Put("BAG_ID", b1.Id);
                 r.Put("OBJ_ID", st._tableID);
                 if (st.STTD.Count > 0)
                 {
                     r.Put("TD", st.STTD[0]);
                     r.Put("IRF", st.IRF[0]);
                     r.Put("Pi", st.PI[0]);
                     r.Put("DISTANCE", st.DISTANCE[0]);
                     r.Put("GAIN_SACRIFICE", st.GT[0]);
                     r.Put("GAIN_NOISE", st.GR[0]);
                     r.Put("AZIMUTH_SACRIFICE", st.A1[0]);
                     r.Put("AZIMUTH_NOISE", st.A2[0]);
                     r.Put("LOSS", st.STL525List[0]);
                     r.Put("FREQ_SACRIFICE", st.CURTX[0]);
                     r.Put("FREQ_NOISE", st.CURRX[0]);
                     if (typeOfStation == Station.TypeOfStation.Earth)
                         r.Put("EARTH_STAT_ID", st._tableID);
                     else r.Put("MOB_STAT2_ID", st._tableID);
                 }
                 try
                 {
                     r.Update();
                 }
                 catch
                 {

                 }
             }
         }

         r.Close();
         r.Destroy();
         //r_xnrfa_.Close();
         //r_xnrfa_.Destroy();
         return b1;
      }

      private void CheckOnBaseStation()
      {
         ////////////////////////////////////////////////////////////////////
         //MICROWAWE STATION
         ////////////////////////////////////////////////////////////////////
         if (_curCalcStation._typeOfStation == Station.TypeOfStation.Radio)
         {
            List<Station> removeSt = new List<Station>();
            foreach (Station st in stListZhertvaMicroWave)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListZhertvaMicroWave.Remove(rem);

            ////////////////////////////////////////////////////////////////////
            removeSt.Clear();
            foreach (Station st in stListPomehaMicroWave)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListPomehaMicroWave.Remove(rem);

            return;
         }

         ////////////////////////////////////////////////////////////////////
         //EARTH STATION
         ////////////////////////////////////////////////////////////////////
         if (_curCalcStation._typeOfStation == Station.TypeOfStation.Earth)
         {
            List<Station> removeSt = new List<Station>();
            foreach (Station st in stListZhertvaEarth)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListZhertvaEarth.Remove(rem);

            ////////////////////////////////////////////////////////////////////
            removeSt.Clear();
            foreach (Station st in stListPomehaEarth)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListPomehaEarth.Remove(rem);

            return;
         }

         ////////////////////////////////////////////////////////////////////
         //SRT STATION
         ////////////////////////////////////////////////////////////////////
         if (_curCalcStation._typeOfStation == Station.TypeOfStation.SRT)
         {
            List<Station> removeSt = new List<Station>();
            foreach (Station st in stListZhertvaSRT)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListZhertvaSRT.Remove(rem);

            ////////////////////////////////////////////////////////////////////
            removeSt.Clear();
            foreach (Station st in stListPomehaSRT)
            {
               if (st._tableID == _curCalcStation._tableID)
                  removeSt.Add(st);
            }
            foreach (Station rem in removeSt)
               stListPomehaSRT.Remove(rem);

            return;
         }
      }

      private void CheckOnOperator()
      {
         ////////////////////////////////////////////////////////////////////
         //SRT STATION
         ////////////////////////////////////////////////////////////////////
         List<Station> removeSt = new List<Station>();
         foreach (Station st in stListZhertvaSRT)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListZhertvaSRT.Remove(rem);

         ////////////////////////////////////////////////////////////////////
         removeSt.Clear();
         foreach (Station st in stListPomehaSRT)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListPomehaSRT.Remove(rem);

         ////////////////////////////////////////////////////////////////////
         //EARTH STATION
         ////////////////////////////////////////////////////////////////////
         removeSt.Clear();
         foreach (Station st in stListZhertvaEarth)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListZhertvaEarth.Remove(rem);

         ////////////////////////////////////////////////////////////////////
         removeSt.Clear();
         foreach (Station st in stListPomehaEarth)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListZhertvaEarth.Remove(rem);

         ////////////////////////////////////////////////////////////////////
         //MICROWAVE STATION
         ////////////////////////////////////////////////////////////////////
         removeSt.Clear();
         foreach (Station st in stListZhertvaMicroWave)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListZhertvaMicroWave.Remove(rem);

         ////////////////////////////////////////////////////////////////////
         removeSt.Clear();
         foreach (Station st in stListPomehaMicroWave)
         {
            if (st._ownerID == _curCalcStation._ownerID)
               removeSt.Add(st);
         }
         foreach (Station rem in removeSt)
            stListPomehaMicroWave.Remove(rem);
      }

      private double GetMaxPi(List<Station> stationList)
      {
          if (stationList.Count>0)
          {
              double maxPi = -9999;
              foreach(Station st in stationList)
                  if (st.PI.Count>0)
                    maxPi = Math.Max(maxPi, st.PI.Max());
              return maxPi;
          }
          else
          {
              return -9999;
          }
      }

      private double GetMaxPi2(List<Station> stationList)
      {
          if (stationList.Count > 0)
          {
              double maxPi = -9999;
              foreach (Station st in stationList)
              {
                  if (st.stationA.PI.Count>0)
                      maxPi = Math.Max(maxPi, st.stationA.PI.Max());

                  if (st.stationB.PI.Count > 0)
                      maxPi = Math.Max(maxPi, st.stationB.PI.Max());
              }
              return maxPi;
          }
          else
          {
              return -9999;
          }
      }

      public ViborkaEMS.Results GetResults()
      {
          ViborkaEMS.Results result = new ViborkaEMS.Results();

          result.FrequenciersString = curStatList[0].STFrequncyTX[0].ToStringNullD() + " МГц /" + curStatList[0].STFrequncyRX[0].ToStringNullD() + " МГц ";
          
          result.MicrovaweIntf = GetMaxPi2(stListPomehaMicroWave);
          result.MobstationIntf = GetMaxPi(stListPomehaSRT);
          result.EarthIntf = GetMaxPi(stListPomehaEarth);

          result.MicrovaweIntfCount = stListPomehaMicroWave.Count;
          result.MobstationIntfCount = stListPomehaSRT.Count;
          result.EarthIntfCount = stListPomehaEarth.Count;

          result.MicrovaweVictim = GetMaxPi2(stListZhertvaMicroWave);
          result.MobstationVictim = GetMaxPi(stListZhertvaSRT);
          result.EarthVictim = GetMaxPi(stListZhertvaEarth);

          result.MicrovaweVictimCount = stListZhertvaMicroWave.Count;
          result.MobstationVictimCount = stListZhertvaSRT.Count;
          result.EarthVictimCount = stListZhertvaEarth.Count;

          return result;
      }

      #endregion
   }
}