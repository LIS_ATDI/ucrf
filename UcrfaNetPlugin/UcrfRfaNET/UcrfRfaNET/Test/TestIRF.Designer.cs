﻿namespace XICSM.UcrfRfaNET
{
   partial class TestIRF
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.Tbxf = new System.Windows.Forms.TextBox();
         this.Tbxd = new System.Windows.Forms.TextBox();
         this.Tbxh1 = new System.Windows.Forms.TextBox();
         this.Tbxh2 = new System.Windows.Forms.TextBox();
         this.TbxResult = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.BtnResult = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // Tbxf
         // 
         this.Tbxf.Location = new System.Drawing.Point(161, 20);
         this.Tbxf.Name = "Tbxf";
         this.Tbxf.Size = new System.Drawing.Size(100, 22);
         this.Tbxf.TabIndex = 0;
         // 
         // Tbxd
         // 
         this.Tbxd.Location = new System.Drawing.Point(161, 57);
         this.Tbxd.Name = "Tbxd";
         this.Tbxd.Size = new System.Drawing.Size(100, 22);
         this.Tbxd.TabIndex = 1;
         // 
         // Tbxh1
         // 
         this.Tbxh1.Location = new System.Drawing.Point(161, 100);
         this.Tbxh1.Name = "Tbxh1";
         this.Tbxh1.Size = new System.Drawing.Size(100, 22);
         this.Tbxh1.TabIndex = 2;
         // 
         // Tbxh2
         // 
         this.Tbxh2.Location = new System.Drawing.Point(161, 139);
         this.Tbxh2.Name = "Tbxh2";
         this.Tbxh2.Size = new System.Drawing.Size(100, 22);
         this.Tbxh2.TabIndex = 3;
         // 
         // TbxResult
         // 
         this.TbxResult.Location = new System.Drawing.Point(161, 179);
         this.TbxResult.Name = "TbxResult";
         this.TbxResult.Size = new System.Drawing.Size(100, 22);
         this.TbxResult.TabIndex = 4;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(52, 23);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(12, 17);
         this.label1.TabIndex = 5;
         this.label1.Text = "f";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(52, 60);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(16, 17);
         this.label2.TabIndex = 6;
         this.label2.Text = "d";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(52, 103);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(24, 17);
         this.label3.TabIndex = 7;
         this.label3.Text = "h1";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(52, 142);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(24, 17);
         this.label4.TabIndex = 8;
         this.label4.Text = "h2";
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(52, 182);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(76, 17);
         this.label5.TabIndex = 9;
         this.label5.Text = "Результат";
         // 
         // BtnResult
         // 
         this.BtnResult.Location = new System.Drawing.Point(105, 225);
         this.BtnResult.Name = "BtnResult";
         this.BtnResult.Size = new System.Drawing.Size(114, 33);
         this.BtnResult.TabIndex = 10;
         this.BtnResult.Text = "Розрахунок";
         this.BtnResult.UseVisualStyleBackColor = true;
         this.BtnResult.Click += new System.EventHandler(this.BtnResult_Click);
         // 
         // TestIRF
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(339, 270);
         this.Controls.Add(this.BtnResult);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.TbxResult);
         this.Controls.Add(this.Tbxh2);
         this.Controls.Add(this.Tbxh1);
         this.Controls.Add(this.Tbxd);
         this.Controls.Add(this.Tbxf);
         this.Name = "TestIRF";
         this.Text = "TestIRF";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox Tbxf;
      private System.Windows.Forms.TextBox Tbxd;
      private System.Windows.Forms.TextBox Tbxh1;
      private System.Windows.Forms.TextBox Tbxh2;
      private System.Windows.Forms.TextBox TbxResult;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Button BtnResult;
   }
}