﻿using System;
using ICSM;

namespace XICSM.UcrfRfaNET.Extension
{
    internal static class DateTimeExtension
    {
        //===================================================
        /// <summary>
        /// Преобразовывает DateTime в строку с учетом IM.NULLT
        /// </summary>
        /// <param name="value">DateTime</param>
        /// <returns>строковое значение DateTime с учетом IM.NULLT</returns>
        public static string ToStringNullT(this DateTime value)
        {
            string retVal = "";
            if (value != IM.NullT)
                retVal = value.ToString();
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Преобразовывает DateTime в строку с учетом IM.NULLT
        /// </summary>
        /// <param name="value">DateTime</param>
        /// <param name="format">Формат преобразования</param>
        /// <returns>строковое значение DateTime с учетом IM.NULLT</returns>
        public static string ToStringNullT(this DateTime value, string format)
        {
            string retVal = "";
            if (value != IM.NullT)
                retVal = value.ToString(format);
            return retVal;
        }
    }
}
