﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET
{
   internal static class RecordPtrExtension
   {
      //===================================================
      /// <summary>
      /// Возвращаетс специальные условия для Р135
      /// </summary>
      /// <param name="value">RecordPtr записи</param>
      /// <param name="docType">Тип документа</param>
      /// <returns>Возвращаетс специальные условия для Р135</returns>
      public static string SpecialConditionR135(this RecordPtr value, string docType)
      {
         string retVal = "";
         //-----
         IMRecordset rs = new IMRecordset(PlugTbl.SpecCondition, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,DOC_TYPE,SQL,TEXT_CONDITION");
         rs.SetWhere("DOC_TYPE", IMRecordset.Operation.Like, docType);
         for(rs.Open();!rs.IsEOF();rs.MoveNext())
         {
            string sql = rs.GetS("SQL");
            if (string.IsNullOrEmpty(sql) == false)
            {
               string localSql = string.Format(sql, value.Id);
               int retSql = -1;
               IM.ExecuteScalar(ref retSql, localSql);
               if(retSql == 1)
               {
                  retVal = rs.GetS("TEXT_CONDITION");
                  break;
               }
            }
         }
         if (rs.IsOpen())
            rs.Close();
         rs.Destroy();
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Возвращает список заявок к которым приязана станция
      /// </summary>
      /// <param name="value">RecordPtr записи</param>
      /// <returns>Возвращает список заявок к которым приязана станция</returns>
      public static List<int> Applications(this RecordPtr value)
      {
         List<int> retVal = new List<int>();
         IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
         rs.Select("ID");
         rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, value.Table);
         rs.SetAdditional("([OBJ_ID1]=" + value.Id + " OR [OBJ_ID2]=" + value.Id +
                          " OR [OBJ_ID3]=" + value.Id + " OR [OBJ_ID4]=" + value.Id +
                          " OR [OBJ_ID5]=" + value.Id + " OR [OBJ_ID6]=" + value.Id + ")");
         rs.OrderBy("ID", OrderDirection.Descending);
         for(rs.Open();!rs.IsEOF(); rs.MoveNext())
         {
            retVal.Add(rs.GetI("ID"));
         }
         if(rs.IsOpen())
            rs.Close();
         rs.Destroy();
         return retVal;
      }

      //==================================================
      /// <summary>
      /// Создает новый сайт, копирую данные из исходного
      /// </summary>     
      /// <returns>новый объект сайта</returns>
      public static IMRecordset CloneRecord2(this IMRecordset r, RecordPtr recPtr, string notAvFileds, bool isPrimary)
      {
          List<string> notAvailableList= new List<string>(notAvFileds.Split(','));
          OrmTable ormTab = OrmSchema.Table(recPtr.Table);
          OrmField[] fldMas;
          try
          {
              fldMas = ormTab.Fields;
          }
          catch
          {
              ormTab = OrmSchema.Table(recPtr.Table);
              fldMas = ormTab.Fields;
          }
                    
          //IMObject srcPosition = IMObject.LoadFromDB(recPtr.Table, recPtr.Id);
          //IMObject retPosition = IMObject.New(toTable);

          r = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadWrite);
          foreach (OrmField fld in fldMas)
          {
              if (fld == null)
                  continue;
              if (notAvailableList.Contains(fld.Name))
                  continue;
              if ((fld.DDesc != null) && (fld.IsCompiledInEdition) && (fld.Nature == OrmFieldNature.Column) &&
                    ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)))
                  r.Select(fld.Name);             
          }
          r.SetWhere("ID",IMRecordset.Operation.Eq, recPtr.Id);
          try
          {
              r.Open();
              r.AddNew();
              if (isPrimary)
              {
                  int _id = IM.AllocID(recPtr.Table, 1, -1);
                  r.Put("ID", _id);
              }
              foreach (OrmField fld in fldMas)
              {
                  if (fld == null || fld.Name == "ID")
                      continue;
                  if (notAvailableList.Contains(fld.Name))
                      continue;

                  if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME)
                      r.Put(fld.Name, r.GetT(fld.Name));
                  if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                      if (fld.DDesc.ParamType == "int")
                          r.Put(fld.Name, r.GetI(fld.Name));
                      else
                          r.Put(fld.Name, r.GetD(fld.Name));
                  if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)
                      r.Put(fld.Name, r.GetS(fld.Name));
              }
              //r.Update();
          }
          catch (Exception)
          { }
          //finally
          //{           
          //    r.Final();
          //}
          return r;
      }

      /// <summary>
      /// Створює новий запис і копією його з запису recPtr
      /// </summary>
      /// <param name="r"> вихідний рекордсет</param>
      /// <param name="recPtr">запис таблиці</param>
      /// <param name="notAvFileds">поля, вказані через кому, які не повинні копіюватися</param>
      /// <param name="isPrimary">чи присутнє поле ID</param>
      public static void CloneRecord(out IMRecordset r, RecordPtr recPtr, string notAvFileds, bool isPrimary)
      {
          List<string> notAvailableList = new List<string>(notAvFileds.Split(','));
          OrmTable ormTab = OrmSchema.Table(recPtr.Table);
          OrmField[] fldMas;
          try
          {
              fldMas = ormTab.Fields;
          }
          catch
          {
              ormTab = OrmSchema.Table(recPtr.Table);
              fldMas = ormTab.Fields;
          }                           
          List<string> selectString= new List<string>();
          IMRecordset  r1 = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
          r = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadWrite);
          foreach (OrmField fld in fldMas)
          {
              if (fld == null)
                  continue;
              if (notAvailableList.Contains(fld.Name))
                  continue;
              if ((fld.DDesc != null) && (fld.IsCompiledInEdition) && (fld.Nature == OrmFieldNature.Column) &&
                    ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)))
                  selectString.Add(fld.Name);         
          }
          foreach (string select in selectString)
              r1.Select(select);
          if (isPrimary)
              r1.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
          try
          {
              r1.Open();
              if (!r1.IsEOF())
              {             
                  foreach (string select in selectString)
                      r.Select(select);
                  r.Open();
                  r.AddNew();
                  if (isPrimary)
                  {
                      int _id = IM.AllocID(recPtr.Table, 1, -1);
                      r.Put("ID", _id);
                  }
                  foreach (OrmField fld in fldMas)
                  {
                      if (fld == null || fld.Name == "ID")
                          continue;
                      if (notAvailableList.Contains(fld.Name))
                          continue;

                      if ((fld.DDesc != null) && (fld.IsCompiledInEdition) && (fld.Nature == OrmFieldNature.Column))
                      {
                          if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME)
                          {
                              DateTime date = r1.GetT(fld.Name);
                              r.Put(fld.Name, date);
                          }
                          if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                              if (fld.DDesc.ParamType == "int")
                              {
                                  int iNumb = r1.GetI(fld.Name);
                                  r.Put(fld.Name, iNumb);
                              }
                              else
                              {
                                  double dbl = r1.GetD(fld.Name);
                                  r.Put(fld.Name, dbl);
                              }
                          if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)
                          {
                              string s = r1.GetS(fld.Name);
                              r.Put(fld.Name, s);
                          }
                      }
                  }
              }
          }
          catch (Exception ex)
          {
              CLogs.WriteError(ELogsWhat.Critical, ex);
              throw ex;
          }   
          finally
          {
              r1.Final();
          }
      }
   }
}
