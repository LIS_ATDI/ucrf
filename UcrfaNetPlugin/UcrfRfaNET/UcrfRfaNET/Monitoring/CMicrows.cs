﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CMicrows : BaseStationXML
   {
      //===================================================
      protected int sectorID = IM.NullI;
      protected IMObject Microwava = null;
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CMicrows(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
         try
         {
            Microwava = IMObject.LoadFromDB(ICSMTbl.itblMicrowa, stationObj.GetI("MW_ID"));
         }
         catch
         {
            //TODO Написать сообщение об ошибке
         }
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if ((stationObj == null) || (Microwava == null))
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         //Заполняем сектор
         sectorID = CSectors.WriteSector(_root, globalStationID, Microwava.GetS("DESIG_EM"));
         //-------------
         //Заполняем частоты
         AddFreq(stationID);
         //-----------
         //Заполняем антену
         int antennaID = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.AntennaMW, stationObj.GetI("ANT_ID")));
         //-----------
         //Заполняем оборудование
         int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.EQUIP_MW, Microwava.GetI("EQPMW_ID")));
         //-----------
         {//Заполняем передатчик
            double pow = stationObj.GetD("POWER");
            double azimuth = stationObj.GetD("AZIMUTH");
            double agl = stationObj.GetD("AGL1");
            string polar = stationObj.GetS("POLAR");
            double gain = stationObj.GetD("GAIN");
            CTransmitter.WriteNewTransmitter(_root, sectorID, antennaID, txEquipID, pow, azimuth, agl, polar, gain, IM.NullI, IM.NullD, stationObj.GetI("ANT_ID"), ICSMTbl.AntennaMW);
         }
      }
      //===================================================
      public int AddFreq(int idMicrowa)
      {
         int freqID = IM.NullI;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
         rs.Select("TX_FREQ,LinkedTo.TX_FREQ");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, idMicrowa);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               freqID = CStationFreqsXML.WriteStationFreq(_root, sectorID, rs.GetD("LinkedTo.TX_FREQ"), rs.GetD("TX_FREQ"), ICSMTbl.itblMicrows);
         }
         finally
         {
            rs.Destroy();
         }
         return freqID;
      }
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = Microwava.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return Microwava.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return "";
      }
      //---------
      protected override string getStatus()
      {
         return Microwava.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(Microwava.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionMw, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionMw, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         double retVal = IM.NullD;
         {
            IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionMw, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,ASL");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
            try
            {
               rs.Open();
               if(!rs.IsEOF())
                  retVal = rs.GetD("ASL");
            }
            finally
            {
               rs.Destroy();
            }
         }
         return retVal;
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, Microwava.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(ICSMTbl.itblPositionMw, stationObj.GetI("SITE_ID"));
      }
      //===================================================
      /// <summary>
      /// Возвращает FREQ_BAND_WIDTH
      /// </summary>
      protected override string getFREQ_BAND_WIDTH()
      {
         string retVal = "";
         if (CMonitoringAppl.GetListOfLicence(applID).Count() > 0)
            retVal = "Згідно ліцензій на користування РЧР";
         return retVal;
      }
      #endregion
   }
}
