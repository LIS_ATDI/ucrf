﻿using System;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using System.Runtime.InteropServices;
using System.IO;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CMonitoringAppl
   {
      internal enum StateXmlR136
      {
         Wait,
         Sent,
         Ok,
         Resent,
         ErAnswer,
      }
      //===================================================
      /// <summary>
      /// Типы документов для Р135
      /// </summary>
      public enum DocType
      {
         /// <summary>
         /// Неизвестный
         /// </summary>
         NONE,
         /// <summary>
         /// Разрешение
         /// </summary>
         PERM,
         /// <summary>
         /// Висновок
         /// </summary>
         CONC,
      }
      //===================================================
      /// <summary>
      /// Даные о заявке
      /// </summary>
      private class CApplObject
      {
         public int ApplID = IM.NullI;
         public List<int> objIDs = new List<int>();
         public string ObjTable = "";
      }
      //===================================================
      private CApplObject ApplObj = null;
      private string fileName = "";
      private BaseAppClass applClass = null;
      private int ApplID = IM.NullI;
      //-----
      private static DocType _docType = DocType.NONE;
      //===================================================
      /// <summary>
      /// Выбераем заявки, которые необходимо отправить в Р135 и отправляем
      /// </summary>
      public static int SendToR135(bool isSilent)
      {
         int retCount = 0;
         DateTime curDate = DateTime.Now;
         Dictionary<int,int> idDict = new Dictionary<int, int>();
         using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Loading records...")))
         {
            const string tableName = PlugTbl.STATE_XML135;
            pBar.SetBig(string.Format(CLocaliz.TxT("Loading records from a table {0}"), tableName));
            using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,APPL_ID,DATE_CREATED");
                r.SetWhere("STATUS", IMRecordset.Operation.Like, StateXmlR136.Wait.ToString());
                r.OrderBy("DATE_CREATED", OrderDirection.Ascending);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    DateTime tmpDate = r.GetT("DATE_CREATED");
                    if (tmpDate >= curDate)
                        continue; //Пока пропустим стануию
                    int curStateId = r.GetI("ID");
                    int curApplId = r.GetI("APPL_ID");
                    if (idDict.ContainsKey(curStateId) == false)
                    {
                        idDict.Add(curStateId, curApplId);
                        pBar.SetSmall(curStateId);
                    }
                }
            }
         }
         //-----
         //Создаем XML
         string localFolder = IM.GetWorkspaceFolder();
         using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Creating XML...")))
         {
            string message = string.Empty;
            foreach (var item in idDict)
            {
               if (pBar.UserCanceled() == false)
               {
                  string fileName = string.Format("{0}\\li_{2}_{1}.xml", localFolder,
                                                  Guid.NewGuid().ToString().ToUpper(),
                                                  DateTime.Now.ToString("yyyy_MM_dd_HHmmss"));
                  //------
                  // Тип документа
                  _docType = DocType.NONE;
                  {
                     IMRecordset r = new IMRecordset(PlugTbl.STATE_XML135, IMRecordset.Mode.ReadOnly);
                     r.Select("ID,DOCUMENT");
                     r.SetWhere("ID", IMRecordset.Operation.Eq, item.Key);
                     r.Open();
                     if (!r.IsEOF())
                     {
                         string tmpDocType = r.GetS("DOCUMENT");
                         try
                         {
                             _docType = (DocType) Enum.Parse(typeof (DocType), tmpDocType, true);
                         }
                         catch
                         {
                             _docType = DocType.NONE;
                         }

                     }
                     if (r.IsOpen())
                        r.Close();
                     r.Destroy();
                  }
                  if (_docType != DocType.NONE)
                  {
                      CMonitoringAppl mAppl = new CMonitoringAppl();
                      if (mAppl.CreateXMLAppl(fileName, item.Value))
                      {
                          if (FtpWriterReader.WriteFileR135(fileName))
                          {
                              ChangeStatusXmlR135ToSent(item.Key, fileName);
                              retCount++;
                          }
                      }
                      else
                          message = "Error. Look at the log";
                  }
               }
            }
            if (!isSilent)
            {
                message += string.Format("\nWere created {0} XML files", retCount);
               MessageBox.Show(message);
            }
         }
         return retCount;
      }

      /// <summary>
      /// Decompresses the source buffer into the destination buffer.
      /// </summary>
      /// <param name="dest"></param>
      /// <param name="destLen"> Upon entry, is the total size of the destination buffer, which must be large enough to hold the entire uncompressed data (The size of the uncompressed data must have been saved previously by the compressor and transmitted to the decompressor by some mechanism outside the scope of this compression library.). 
      /// Upon exit, destLen is the actual size of the compressed buffer</param>
      /// <param name="source"></param>
      /// <param name="sourceLen">the byte length of the source buffer</param>
      /// <returns>Z_OK if success, 
      /// Z_MEM_ERROR if there was not enough memory, 
      /// Z_BUF_ERROR if there was not enough room in the output buffer, or 
      /// Z_DATA_ERROR if the input data was corrupted or incomplete. 
      /// In the case where there is not enough room, uncompress() will fill the output buffer with the uncompressed data up to that point</returns>
      /// 
      [DllImport("zlib.dll", CallingConvention = CallingConvention.Cdecl)]
      static extern Int32 uncompress(IntPtr dest, ref uint destLen, IntPtr source, uint sourceLen);

      // zlib.ddl functions return codes
      const Int32 Z_OK            = 0;
      const Int32 Z_STREAM_END    = 1;
      const Int32 Z_NEED_DICT     = 2;
      const Int32 Z_ERRNO         = (-1);
      const Int32 Z_STREAM_ERROR  = (-2);
      const Int32 Z_DATA_ERROR    = (-3);
      const Int32 Z_MEM_ERROR     = (-4);
      const Int32 Z_BUF_ERROR     = (-5);
      const Int32 Z_VERSION_ERROR = (-6);

      //===================================================
      /// <summary>
      /// Читаем CML и XML
      /// </summary>
      public static int ReadFromR135(bool isSilent)
      {
         using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Reading tickets from P135...")))
         {
            int count = 0;
            pBar.SetBig("Loading a list of files...");
            pBar.SetProgress(0, 100);
            try
            {
                string[] fileNames = FtpWriterReader.GetFileList(CSetting.FtpMonitoringSetting.hostNameIcsm,
                                                                 CSetting.FtpMonitoringSetting.userNameIcsm,
                                                                 CSetting.FtpMonitoringSetting.passwordIcsm);
                foreach (string fileName in fileNames)
                {
                    pBar.SetBig(string.Format("Processing the file {0}...", fileName));
                    //string fileName = System.IO.Path.ChangeExtension(_fileName, System.IO.Path.GetExtension(_fileName).ToLower());
                    bool isXml = fileName.EndsWith(".xml");
                    bool isCml = fileName.EndsWith(".cml");
                    if (isXml || isCml)
                    {
                        String tempPath = System.IO.Path.GetTempPath();
                        FtpWriterReader.Download(tempPath,
                                                 fileName,
                                                 CSetting.FtpMonitoringSetting.hostNameIcsm,
                                                 CSetting.FtpMonitoringSetting.userNameIcsm,
                                                 CSetting.FtpMonitoringSetting.passwordIcsm);
                        string fullFileName = string.Format("{0}{2}{1}", tempPath, fileName, (tempPath.Length > 0 && tempPath[tempPath.Length-1] == '\\') ? "" : "\\");
                        if (!System.IO.File.Exists(fullFileName))
                        {
                            CLogs.WriteError(ELogsWhat.Monitoring, string.Format("Can't download file {0}", fileName), isSilent);
                            continue;
                        }

                        string xmlFileName = fullFileName;

                        try
                        {
                            if (isCml)
                            {
                                xmlFileName = System.IO.Path.ChangeExtension(fullFileName, "xml");
                                //load, uncompress, save the result
                                using (FileStream fsSource = new FileStream(fullFileName, FileMode.Open))
                                {

                                    byte[] inBuff = new byte[fsSource.Length];
                                    int numBytesToRead = (int)fsSource.Length;
                                    if (numBytesToRead < 5)
                                        throw new Exception("File '" + fileName + "' contains just " + numBytesToRead.ToString() + " bytes");
                                    int numBytesRead = 0;
                                    while (numBytesToRead > 0)
                                    {
                                        // Read may return anything from 0 to numBytesToRead. 
                                        int n = fsSource.Read(inBuff, numBytesRead, numBytesToRead);
                                        // Break when the end of the file is reached. 
                                        if (n == 0)
                                            break;
                                        numBytesRead += n;
                                        numBytesToRead -= n;
                                    }

                                    uint numBytesToUncompress = (uint)inBuff.Length - 4;

                                    uint uncompressedLengthToDo = 0;
                                    uncompressedLengthToDo |= inBuff[inBuff.Length - 4];
                                    uncompressedLengthToDo |= (((uint)inBuff[inBuff.Length - 3]) << 8);
                                    uncompressedLengthToDo |= (((uint)inBuff[inBuff.Length - 2]) << 16);
                                    uncompressedLengthToDo |= (((uint)inBuff[inBuff.Length - 1]) << 24);

                                    byte[] outBuff = new byte[uncompressedLengthToDo];

                                    GCHandle hDest = GCHandle.Alloc(outBuff, GCHandleType.Pinned);
                                    GCHandle hSource = GCHandle.Alloc(inBuff, GCHandleType.Pinned);

                                    uint uncompressedLengthAsIs = uncompressedLengthToDo;

                                    try
                                    {
                                        Int32 res = uncompress(hDest.AddrOfPinnedObject(), ref uncompressedLengthAsIs, hSource.AddrOfPinnedObject(), numBytesToUncompress);
                                        if (res < 0)
                                            throw new Exception(string.Format("Failed to decompress file '{0}': {1}; declared uncompressed length = {2}",
                                                fileName, res == Z_MEM_ERROR ? "Z_MEM_ERROR - not enough memory" :
                                                            res == Z_BUF_ERROR ? "Z_BUF_ERROR - not enough room in the output buffer" :
                                                            res == Z_DATA_ERROR ? "Z_DATA_ERROR - input data was corrupted or incomplete" :
                                                            ("Unknown error (" + res.ToString() + ")"),
                                                uncompressedLengthToDo
                                                ));
                                        if (uncompressedLengthAsIs != uncompressedLengthToDo)
                                            throw new Exception(string.Format("Uncompressed length ({0}) is not as expected ({1})", uncompressedLengthAsIs, uncompressedLengthToDo));
                                    }
                                    finally
                                    {
                                        hDest.Free();
                                        hSource.Free();
                                    }
                                    // Write the byte array to the other FileStream. 
                                    using (FileStream fsNew = new FileStream(xmlFileName, FileMode.Create))
                                    {
                                        fsNew.Write(outBuff, 0, (int)uncompressedLengthToDo);
                                    }
                                }
                            }

                            using (System.IO.StreamReader myStream = new System.IO.StreamReader(xmlFileName))
                            {
                                if (CLoadAnswer.HandleAnswer(myStream, fileName))
                                {
                                    FtpWriterReader.DeleteFile(fileName,
                                                               CSetting.FtpMonitoringSetting.hostNameIcsm,
                                                               CSetting.FtpMonitoringSetting.userNameIcsm,
                                                               CSetting.FtpMonitoringSetting.passwordIcsm);
                                    pBar.Increment(true);
                                    count++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CLogs.WriteError(ELogsWhat.Monitoring, ex, isSilent);
                        }
                        // all streams released. can delete files, or the same file
                        File.Delete(fullFileName);
                        File.Delete(xmlFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                CLogs.WriteError(ELogsWhat.Monitoring, ex, isSilent);
            }
            return count;
         }
      }
      //===================================================
      /// <summary>
      /// Создет XML файл для заявки
      /// </summary>
      /// <param name="_fileName">полное имя файла, куда положить XML данные</param>
      /// <param name="applIdList"></param>
      /// <returns>TRUE - все ОК, иначе FALSE</returns>
      public bool CreateXMLAppl(string _fileName, params int[] applIdList)
      {
         using (LisProgressBar pb = new LisProgressBar("Creating XML"))
         {
            fileName = _fileName;
            if (fileName == "")
               return false;  //Имя файла ????
            //-----
            BaseXMLexport.Clear(); //Подготовка для нового XML файла
            bool needToSave = false;
            XElement ex = BaseXMLexport.GetDocument();
            //-----
            foreach (int _applID in applIdList)
            {
               ApplID = _applID;
               {
                  IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                  rs.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
                  rs.SetWhere("ID", IMRecordset.Operation.Eq, _applID);
                  try
                  {
                     rs.Open();
                     if (!rs.IsEOF())
                     {
                        ApplObj = new CApplObject();
                        ApplObj.ApplID = _applID;
                        for (int i = 1; i < 7; i++)
                        {
                           int tmpID = rs.GetI(string.Format("OBJ_ID{0}", i));
                           if ((tmpID != IM.NullI) && (tmpID != 0))
                              ApplObj.objIDs.Add(tmpID);
                        }
                        ApplObj.ObjTable = rs.GetS("OBJ_TABLE");
                     }
                     else
                     {
                        ApplObj = null;
                        CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't load the application object ID={0}"), _applID));
                     }
                  }
                  finally
                  {
                     rs.Destroy();
                  }
               }

               if (ApplObj == null)
                  continue;

               //Обязательная проверка
               if (ApplObj.objIDs.Count == 0)
               {
                  CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("The application ID={0} doesn't contain any station"), ApplObj.ApplID));
                  continue;
               }

               pb.SetBig(string.Format("{0} ({1})", ApplObj.ObjTable, ApplObj.objIDs[0]));
               //------
               try
               {
                   applClass = BaseAppClass.GetBaseAppl(ApplObj.ApplID);
               }
               catch
               {
                  applClass = null;
                  CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't load the application classID={0}"), ApplObj.ApplID));
               }

               if (applClass == null)
                  continue;
               //----------
               string TableName = ApplObj.ObjTable;
               switch (TableName)
               {
                  case ICSMTbl.itblTDAB:
                     ExportTdabStation(ex);
                     break;
                  case ICSMTbl.itblDVBT:
                     ExportDvbtStation(ex);
                     break;
                  case ICSMTbl.itblEarthStation:
                     ExportEarthStation(ex);
                     break;
                  case ICSMTbl.itblMobStation:
                     ExportMobStation(ex);
                     break;
                  case ICSMTbl.itblMobStation2:
                     ExportMobStation2(ex);
                     break;
                  case ICSMTbl.itblMicrowa:
                     ExportMicrowa(ex);
                     break;
                  case ICSMTbl.itblFM:
                     ExportFM(ex);
                     break;
                  case ICSMTbl.itblTV:
                     ExportTV(ex);
                     break;
                   case ICSMTbl.itblShip:
                   case PlugTbl.XfaAbonentStation:
                   case PlugTbl.XfaAmateur:
                     int objId = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0];
                     BaseStationXML bxml = (TableName == ICSMTbl.itblShip) ? new CShip(ex, new RecordPtr(TableName, objId), ApplObj.ApplID) as BaseStationXML :
                         (TableName == PlugTbl.XfaAbonentStation) ? new CAbonentStation(ex, new RecordPtr(TableName, objId), ApplObj.ApplID) as BaseStationXML :
                         (TableName == PlugTbl.XfaAmateur) ? new CAmateur(ex, new RecordPtr(TableName, objId), ApplObj.ApplID) as BaseStationXML :
                         null;
                     if (bxml != null)
                     {
                         bxml.Export();
                         WriteDocuments(ex, bxml.GlobalStationID, applClass);
                     }
                     break;
                  default:
                     CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("A function for exporting the table \"{0}\" is not implemented"), TableName));
                     break;
               }
               needToSave = true;
            }
            bool retVal = false;
            if(needToSave == true)
               retVal = BaseXMLexport.SaveXMLDocument(ex, fileName);
            if (applClass != null)
            {
                applClass.Dispose();
                applClass = null;
            }
            return retVal;
         }
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для TdabStation
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportTdabStation(XElement ex)
      {
         int tdabID = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0];
         BaseStationXML bxml = new CTdabStation(ex, new RecordPtr(ICSMTbl.itblTDAB, tdabID), ApplObj.ApplID);
         bxml.Export();
         SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
         WriteDocuments(ex, bxml.GlobalStationID, applClass);
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для DvbtStation
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportDvbtStation(XElement ex)
      {
         int dvbtID = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0];
         BaseStationXML bxml = new CDvbtStation(ex, new RecordPtr(ICSMTbl.itblDVBT, dvbtID), ApplObj.ApplID);
         bxml.Export();
         SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
         WriteDocuments(ex, bxml.GlobalStationID, applClass);
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для EarthStation
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportEarthStation(XElement ex)
      {
         int earthID = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0];
         BaseStationXML bxml = new CEarthStation(ex, new RecordPtr(ICSMTbl.itblEarthStation, earthID), ApplObj.ApplID);
         bxml.Export();
         SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
         WriteDocuments(ex, bxml.GlobalStationID, applClass);
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для MobStation2
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportMobStation2(XElement ex)
      {
         if (ApplObj.objIDs.Count > 0)
         {
            BaseStationXML bxml = new CMobStation2(ex, new RecordPtr(ICSMTbl.itblMobStation2, ApplObj.objIDs[0]), ApplObj.ApplID, ref ApplObj.objIDs);
            bxml.Export();
            SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
            WriteDocuments(ex, bxml.GlobalStationID, applClass);
         }
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для MobStation
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportMobStation(XElement ex)
      {
         if (ApplObj.objIDs.Count > 0)
         {
            BaseStationXML bxml = new CMobStation(ex, new RecordPtr(ICSMTbl.itblMobStation, ApplObj.objIDs[0]), ApplObj.ApplID, ref ApplObj.objIDs);
            bxml.Export();
            SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
            WriteDocuments(ex, bxml.GlobalStationID, applClass);
         }
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для MICROWA
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportMicrowa(XElement ex)
      {
         if ((ApplObj.objIDs.Count > 0) && (ApplObj.objIDs[0] != IM.NullI))
         {
            BaseStationXML bxml = new CMicrowa(ApplID, applClass, ex, new RecordPtr(ICSMTbl.itblMicrowa, ApplObj.objIDs[0]), ApplObj.objIDs);
            bxml.Export();
         }
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для FM
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportFM(XElement ex)
      {
         int fmID = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0]; ;
         BaseStationXML bxml = new CFMStation(ex, new RecordPtr(ICSMTbl.itblFM, fmID), ApplObj.ApplID);
         bxml.Export();
         SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
         WriteDocuments(ex, bxml.GlobalStationID, applClass);
         return true;
      }
      //===================================================
      /// <summary>
      /// Создает XML файл для TV
      /// </summary>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      private bool ExportTV(XElement ex)
      {
         int tvID = (ApplObj.objIDs.Count == 0) ? IM.NullI : ApplObj.objIDs[0]; ;
         BaseStationXML bxml = new CTVStation(ex, new RecordPtr(ICSMTbl.itblTV, tvID), ApplObj.ApplID);
         bxml.Export();
         SaveLicense(ApplObj.ApplID, bxml.GlobalStationID, ex, applClass.OwnerID);
         WriteDocuments(ex, bxml.GlobalStationID, applClass);
         return true;
      }
      //===================================================
      /// <summary>
      /// Сохраняем лицензии в XML файл
      /// </summary>
      /// <param name="applID">ID заявки</param>
      /// <param name="globalStationID">Глобальный идентификатор станции</param>
      /// <param name="ex">XElement</param>
      internal static void SaveLicense(int applID, int globalStationID, XElement ex, int ownerStationId)
      {
         List<int> licID = GetListOfLicence(applID);
         foreach(int item in licID)
            CLicence.WriteNewStnLicence(ex, item, globalStationID, ownerStationId);
      }
      //===================================================
      /// <summary>
      /// Возвращает список ID лицензий, которые связаны с заявкой
      /// </summary>
      /// <param name="applID">ID заявки</param>
      /// <returns>Возвращает список ID лицензий, которые связаны с заявкой</returns>
      internal static List<int> GetListOfLicence(int applID)
      {
         List<int> retVal = new List<int>();
         IMRecordset rs = new IMRecordset(PlugTbl.APPL_LIC, IMRecordset.Mode.ReadOnly);
         rs.Select("LIC_ID");
         rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applID);
         try
         {
            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
               retVal.Add(rs.GetI("LIC_ID"));
         }
         finally
         {
            rs.Destroy();
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Созраняем разрешения в XML файл
      /// </summary>
      /// <param name="ex">XElement</param>
      /// <param name="globalStationID">Глобальный идентификатор станции</param>
      /// <param name="appl">Класс заявки</param>
      internal static void WriteDocuments(XElement ex, int globalStationID, BaseAppClass appl)
      {
         switch(_docType)
         {
            case DocType.PERM:
               SavePermissions(ex, globalStationID, appl);
               break;
            case DocType.CONC:
               SaveСonclusions(ex, globalStationID, appl);
               break;
      }
      }
      //===================================================
      /// <summary>
      /// Созраняем разрешения в XML файл
      /// </summary>
      /// <param name="ex">XElement</param>
      /// <param name="globalStationId">Глобальный идентификатор станции</param>
      /// <param name="appl">Класс заявки</param>
      internal static void SavePermissions(XElement ex, int globalStationId, BaseAppClass appl)
      {
          RecordPtr recPtr = new RecordPtr(appl.objTableName, appl.ApplID);
          string specCondR135 = recPtr.SpecialConditionR135("PERM");
          string permNum;
          DateTime startDate;
          DateTime stopDate;
          DateTime cancelDateDozv;
          DateTime printDateDozv;
          Documents.ApplDocuments.GetPermData(appl.ApplID, out permNum, out startDate, out stopDate, out printDateDozv, out cancelDateDozv);
          string specialCondition = specCondR135 + appl.SCDozvil;
          string oldDozvNumber = Documents.ApplDocuments.GetOldPermNum(appl.ApplID);
          if (string.IsNullOrEmpty(oldDozvNumber))
          {
              oldDozvNumber = "";
              string numVisn = Documents.ApplDocuments.GetConcNum(appl.ApplID);
              if (string.IsNullOrEmpty(numVisn) == false)
                  oldDozvNumber = string.Format("висновок {0}", numVisn);
          }
          else
          {
              string tmpOldDozvNumber = oldDozvNumber;
              // не отменять самого себя - #7253
              oldDozvNumber = (oldDozvNumber != permNum) ? string.Format("дозвіл {0} не дійсний", tmpOldDozvNumber) : "";
          }
          CPermission.WriteNewStnPermission(ex, globalStationId, permNum, startDate, stopDate,
                                            CPermission.EPermissionType.Use, specialCondition, oldDozvNumber,
                                            cancelDateDozv);

          {
              string numVisn;
              DateTime dateFromVisn, dateToVisn, datePrintVisn;
              Documents.ApplDocuments.GetConcData(appl.ApplID, out numVisn, out dateFromVisn, out dateToVisn, out datePrintVisn);
              if (string.IsNullOrEmpty(oldDozvNumber))
              {
                  if(string.IsNullOrEmpty(numVisn) == false)
                  {
                      CPermission.WriteNewStnPermission(ex, globalStationId, numVisn, dateFromVisn, dateToVisn,
                                                        CPermission.EPermissionType.Build, "", "", DateTime.Now);
                  }
              }
              else
              {
                  string numDozvOld;
                  DateTime dateFromDozvOld, dateToDozvOld, datePrintDozvOld, dateCancelDozvOld;
                  Documents.ApplDocuments.GetOldPermData(appl.ApplID, out numDozvOld, out dateFromDozvOld,
                                                         out dateToDozvOld, out datePrintDozvOld, out dateCancelDozvOld);
                  if ((dateFromVisn > dateFromDozvOld) == false)
                  {
                      if (numDozvOld != permNum) // не писать два раза ссылку на один и тот же РД - #7253
                      CPermission.WriteNewStnPermission(ex, globalStationId, numDozvOld, dateFromDozvOld, dateToDozvOld,
                                                        CPermission.EPermissionType.Use, "", "", DateTime.Now);
                  }
                  else
                  {
                      if((dateToDozvOld > DateTime.Now) == false)
                      {
                          CPermission.WriteNewStnPermission(ex, globalStationId, numVisn, dateFromVisn, dateToVisn,
                                                        CPermission.EPermissionType.Build, "", "", DateTime.Now);
                      }
                      else
                      {
                          if (numDozvOld != permNum) // не писать два раза ссылку на один и тот же РД - #7253
                          CPermission.WriteNewStnPermission(ex, globalStationId, numDozvOld, dateFromDozvOld, dateToDozvOld,
                                                        CPermission.EPermissionType.Use, "", "", DateTime.Now);
                          CPermission.WriteNewStnPermission(ex, globalStationId, numVisn, dateFromVisn, dateToVisn,
                                                        CPermission.EPermissionType.Build, "", "", DateTime.Now);
                      }

                  }
              }
          }
      }
      //===================================================
      /// <summary>
      /// Созраняем заключения ЭМС в XML файл
      /// </summary>
      /// <param name="ex">XElement</param>
      /// <param name="globalStationID">Глобальный идентификатор станции</param>
      /// <param name="appl">Класс заявки</param>
      internal static void SaveСonclusions(XElement ex, int globalStationID, BaseAppClass appl)
      {
         RecordPtr recPtr = new RecordPtr(appl.objTableName, appl.ApplID);
         string specCondR135 = recPtr.SpecialConditionR135("CONC");
         string permNum = Documents.ApplDocuments.GetConcNum(appl.ApplID);
         DateTime startDate = Documents.ApplDocuments.GetConcDateFrom(appl.ApplID);
         DateTime stopDate = Documents.ApplDocuments.GetConcDateTo(appl.ApplID);
         string specialCondition = specCondR135 + appl.SCVisnovok;
         CPermission.WriteNewStnPermission(ex, globalStationID, permNum, startDate, stopDate, CPermission.EPermissionType.Build, specialCondition, "", IM.NullT);
      }
      //===================================================
      /// <summary>
      /// Изменить статус отправки XML файла
      /// </summary>
      private static void ChangeStatusXmlR135ToSent(int id, string fileName)
      {
         IMRecordset r = new IMRecordset(PlugTbl.STATE_XML135, IMRecordset.Mode.ReadWrite);
         r.Select("ID,FILE_NAME,STATUS,DATE_SENT");
         r.SetWhere("ID", IMRecordset.Operation.Eq, id);
         try
         {
            r.Open();
            if(!r.IsEOF())
            {
               r.Edit();
               r.Put("FILE_NAME", System.IO.Path.GetFileName(fileName));
               r.Put("STATUS", StateXmlR136.Sent.ToString());
               r.Put("DATE_SENT", DateTime.Now);
               r.Update();
            }
         }
         finally
         {
            r.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Изменить статус отправки XML файла
      /// </summary>
      internal static bool ChangeStatusXmlR135ByFileName(string fileName, StateXmlR136 state, string message)
      {
         bool retVal = false;
         IMRecordset r = new IMRecordset(PlugTbl.STATE_XML135, IMRecordset.Mode.ReadWrite);
         r.Select("ID,FILE_NAME,STATUS,DATE_UPDATED,MESSAGE");
         r.SetWhere("FILE_NAME", IMRecordset.Operation.Like, fileName);
         try
         {
            r.Open();
            if (!r.IsEOF())
            {
               r.Edit();
               r.Put("STATUS", state.ToString());
               r.Put("DATE_UPDATED", DateTime.Now);
               r.Put("MESSAGE", message.Cut(4000));
               r.Update();
               retVal = true;
            }
         }
         finally
         {
            r.Destroy();
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Пометить заявку для отправки в Р135
      /// </summary>
      public static void MarkApplToSendToR135(int applId, DocType docType)
      {
         const string tableName = PlugTbl.STATE_XML135;
         IMRecordset rs = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,APPL_ID,STATUS,DATE_CREATED,CREATED_BY,DOCUMENT");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, -1);
         try
         {
            rs.Open();
            rs.AddNew();
            rs.Put("ID", IM.AllocID(tableName, 1, -1));
            rs.Put("APPL_ID", applId);
            rs.Put("STATUS", StateXmlR136.Wait.ToString());
            rs.Put("DATE_CREATED", DateTime.Now);
            rs.Put("DOCUMENT", docType.ToString());
            rs.Put("CREATED_BY", IM.ConnectedUser());
            rs.Update();
         }
         finally
         {
            rs.Destroy();
         }
      }
   }
}
