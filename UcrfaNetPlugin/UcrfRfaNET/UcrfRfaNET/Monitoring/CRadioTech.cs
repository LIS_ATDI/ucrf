﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CRadioTech
   {
      public const string riverPerm = "РЕКА";
      public const string seaLicence = "МОРЕ";
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictRadioTech.Clear();
         radioTechID = 0;
      }
      //======================================================
      private static Dictionary<string, int> dictRadioTech = new Dictionary<string, int>();
      private static int radioTechID = 0;
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новой лицензии. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="radioTech">имя радиотехнологии</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewRadioTech(XElement root, string radioTech)
      {
         using (LisProgressBar pb = new LisProgressBar("Radio technology"))
         {
            //-----
            string ptrStr = string.Format("{0}_{1}", "RADIOTECH", radioTech);
            pb.SetSmall(ptrStr);
            if (dictRadioTech.ContainsKey(ptrStr) == true)
               return dictRadioTech[ptrStr];
            //----
            string descr = "unknown";
            if (radioTech == riverPerm)
                descr = "Дозвіл на суднову річкову радіостанцію";
            else if (radioTech == seaLicence)
                descr = "Ліцензія суднової станції";
            else
            {
               IMRecordset rs = new IMRecordset(ICSMTbl.itblRadioSystem, IMRecordset.Mode.ReadOnly);
               rs.Select("ID,DESCRIPTION");
               rs.SetWhere("NAME", IMRecordset.Operation.Like, radioTech);
               try
               {
                  rs.Open();
                  if(!rs.IsEOF())
                     descr = rs.GetS("DESCRIPTION");
               }
               finally
               {
                  rs.Destroy();
               }
            }
            //----
            XElement elem = new XElement("RADIO_TECH");
            XElement temp = new XElement("ID");
            temp.SetValue(++radioTechID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NAME");
            temp.SetValue(descr.Cut(128).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("SHORT_NAME");
            temp.SetValue(descr.Cut(128).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            int tmpRadioTechGlobalID = CExternalID.GetRadioTechExtID(radioTech);
            CExternalID.GetItemKey(elem, tmpRadioTechGlobalID);
            root.Element("RadioTechnologies").Add(elem);
            //-----
            dictRadioTech.Add(ptrStr, radioTechID);
            return radioTechID;
         }
      }
   }
}
