﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CMobStation2 : CMobStationT
   {
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CMobStation2(XElement root, RecordPtr rec, int _applID, ref List<int> sectList)
         : base(root, rec, _applID, ref sectList)
      {
      }
      //===================================================
      /// <summary>
      /// Возвращает название таблицы частот
      /// </summary>
      /// <returns>Возвращает название таблицы частот</returns>
      protected override string getFreqTable()
      {
         return ICSMTbl.itblMobStaFreqs2;
      }
      //===================================================
      /// <summary>
      /// Возвращает название таблицы позиции
      /// </summary>
      /// <returns>Возвращает название таблицы позиции</returns>
      protected override string getPosTable()
      {
         return ICSMTbl.itblPositionMob2;
      }
      //===================================================
      /// <summary>
      /// Возвращает название таблицы антены
      /// </summary>
      /// <returns>Возвращает название таблицы антены</returns>
      protected override string getAntTable()
      {
         return ICSMTbl.itblAntennaMob2;
      }
      //===================================================
      /// <summary>
      /// Возвращает название таблицы Оборудования
      /// </summary>
      /// <returns>Возвращает название таблицы Оборудования</returns>
      protected override string getEquipTable()
      {
         return ICSMTbl.itblEquipMob2;
      }
   }
}
