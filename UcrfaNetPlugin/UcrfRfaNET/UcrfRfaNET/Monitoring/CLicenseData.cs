﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CLicenseData
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
      }
      //======================================================
      /// <summary>
      /// Создает элемент в XML LicenseData
      /// </summary>
      /// <param name="root">XElement</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewLicData(XElement root, int licID, string radioTech, string standard)
      {
         using (LisProgressBar pb = new LisProgressBar("LicenseData"))
         {
            if (string.IsNullOrEmpty(radioTech) && string.IsNullOrEmpty(standard))
               return IM.NullI;
            //----
            XElement elem = new XElement("LICENSEDATA");
            XElement temp = new XElement("LICENSEDATAID");
            temp.SetValue(licID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("LICENSEID");
            temp.SetValue(licID);
            elem.Add(temp);
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(radioTech))
            {
               int rtID = CRadioTech.WriteNewRadioTech(root, radioTech);
               temp = new XElement("RADIO_TECH_ID");
               temp.SetValue(rtID);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(standard))
            {
               int stID = CSystemsXML.GetSystemID(standard, root);
               temp = new XElement("SYSTEMID");
               temp.SetValue(stID);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, licID);
            root.Element("LicenseDatas").Add(elem);
            return licID;
         }
      }
   }
}
