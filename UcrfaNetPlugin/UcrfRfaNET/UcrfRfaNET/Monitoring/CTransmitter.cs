﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CTransmitter
   {
      public class TTransmitterData
      {
         public double AntheightEffect { get; set; } //Максимальная эффективная высота антенны (m)
         public double Riseangle { get; set; } //сумарний кут нахилу
         public double AzimuthEnd { get; set; } //конечный азимут
         public string FactoryNo { get; set; } //заводской номер
         
         //================================
         public TTransmitterData()
         {
            AntheightEffect = IM.NullD;
            Riseangle = IM.NullD;
            AzimuthEnd = IM.NullD;
            FactoryNo = null;
         }
      }
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictTransm.Clear();
      }
      //======================================================
      private static Dictionary<string, int> dictTransm = new Dictionary<string, int>();
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новым передатчиком. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="sectorID">ID сектора</param>
      /// <param name="antID">ID антены</param>
      /// <param name="txEquipID">ID оборудования</param>
      /// <param name="pow">мощность (dBm)</param>
      /// <param name="azimuth">азимут (градусы)</param>
      /// <param name="antHeight">Высота антенны над землей (м)</param>
      /// <param name="polar">Поляризация</param>
      /// <param name="antGain">коеф. усиления антенны (dB)</param>
      /// <param name="fiderLen">Длина фидера</param>
      /// <param name="fiderLoss">Потери фидера</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewTransmitter(XElement root,
         int sectorID, int antID, int txEquipID,
         double pow, double azimuth, double antHeight,
         string polar, double antGain,
         int fiderLen,
         double fiderLoss,
         int baseAntennaID,
         string antennaTable)
      {
          return WriteNewTransmitter(root, new RecordPtr(), sectorID, antID, txEquipID, pow, azimuth, antHeight,
                                    polar, antGain, fiderLen, fiderLoss, baseAntennaID, antennaTable,
                                    IM.NullD, null, null); 
      }
      //===================================================
      public static int WriteNewTransmitter(XElement root,
         RecordPtr recPtr, int sectorID, int antID, int txEquipID,
         double pow, double azimuth, double antHeight,
         string polar, double antGain,
         int fiderLen,
         double fiderLoss,
         int baseAntennaID,
         string antennaTable,
         double powEffect,
         bool? isDirected,
         TTransmitterData exData
         )
      {
         if(exData == null)
            exData = new TTransmitterData();
         using (LisProgressBar pb = new LisProgressBar("Transmitter"))
         {
            double HBeamWidth = IM.NullD;
            double VBeamWidth = IM.NullD;
            if ((baseAntennaID != IM.NullI) && (antennaTable != ""))
            {
               IMRecordset rs = new IMRecordset(antennaTable, IMRecordset.Mode.ReadOnly);
               rs.Select("ID,H_BEAMWIDTH,V_BEAMWIDTH,ANT_DIR");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, baseAntennaID);
               try
               {
                  rs.Open();
                  if (!rs.IsEOF())
                  {
                     HBeamWidth = rs.GetD("H_BEAMWIDTH");
                     VBeamWidth = rs.GetD("V_BEAMWIDTH");
                     HBeamWidth = (HBeamWidth == IM.NullD) ? HBeamWidth : HBeamWidth * 10.0;
                     VBeamWidth = (VBeamWidth == IM.NullD) ? VBeamWidth : VBeamWidth * 10.0;
                     if (!isDirected.HasValue)
                        isDirected = !rs.GetS("ANT_DIR").Contains("ND");
                  }
               }
               finally
               {
                  rs.Destroy();
               }
            }

            //-----
            int txID = sectorID;
            // almost everywhere transmitter just duplicates sector, 
            // but sometimes (at least ships) we have more than one TX within one sector
            // both sector and TX extIDs are generated within one sequence so it's safe to mix methods
            if (!string.IsNullOrEmpty(recPtr.Table) && recPtr.Id != IM.NullI && recPtr.Id != 0)
                txID = CExternalID.GetRecordPtrExtID(recPtr);
            
            string ptrStr = string.Format("{0}_{1}", "TRANSMITTER", txID);
            pb.SetSmall(ptrStr);
            if (dictTransm.ContainsKey(ptrStr) == true)
               return dictTransm[ptrStr];
            //----
            XElement elem = new XElement("TRANSMITTER");
            XElement temp = new XElement("TRANSMITTERID");
            temp.SetValue(txID);
            elem.Add(temp);
            ///////////////////////////////////////////
            if (pow != IM.NullD)
            {
               //double powTmp = System.Math.Pow(10.0, (pow - 30.0)/10).Round(4); //#3923
               double powTmp = pow.Round(9); //#4005
               temp = new XElement("POWER");
               temp.SetAttributeValue("dim", "2");
               temp.SetValue(powTmp);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (powEffect != IM.NullD)
            {
               temp = new XElement("POWER_EFFECT");
               temp.SetValue(powEffect);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            temp = new XElement("SECTORID_REF");
            temp.SetValue(sectorID);
            elem.Add(temp);
            ///////////////////////////////////////////
            if(exData.Riseangle != IM.NullD)
            {
               double tmp = 10*exData.Riseangle;
               if (tmp < 0.0)
                  tmp = -tmp;
               temp = new XElement("RISEANGLE");
               temp.SetValue(tmp);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (azimuth != IM.NullD)
            {
               temp = new XElement("AZIMUTH");//TODO проверить систему счисления
               //temp.SetValue((long)(azimuth * 100.0)); //Пока просто умножаю на 100
               //temp.SetValue((int)azimuth); // 1 Измениль в соотвествии с задачей #3630
               temp.SetValue((int)(azimuth * 10.0)); // 2 Измениль в соотвествии с задачей #3662
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (exData.AzimuthEnd != IM.NullD)
            {
               temp = new XElement("AZIMUTH_END");
               temp.SetValue((int)(exData.AzimuthEnd * 10.0));
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (antHeight != IM.NullD && antHeight != 0.0)
            {
               temp = new XElement("ANTHEIGHT");
               //temp.SetValue((int)antHeight);
               temp.SetValue((int)(antHeight * 100.0)); // #3663
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (exData.AntheightEffect != IM.NullD)
            {
               temp = new XElement("ANTHEIGHT_EFFECT");
               temp.SetValue((int)(exData.AntheightEffect * 100.0));
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (polar != "")
            {
               temp = new XElement("POLARIZATION");
               temp.SetValue(polar.ToStrXMLMonitoring());
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (antGain != IM.NullD)
            {
               temp = new XElement("ANT_GAIN");
               temp.SetValue(antGain);//Пока заношу в dB
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (fiderLoss != IM.NullD && fiderLen != IM.NullI)
            {
               temp = new XElement("FEEDERLOSS_PER_LEN");
               temp.SetValue(fiderLoss);
               elem.Add(temp);

               temp = new XElement("FEEDERLEN");
               temp.SetValue(fiderLen);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(exData.FactoryNo))
            {
                temp = new XElement("FACTORYNO");
                temp.SetValue(exData.FactoryNo);
                elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (antID != IM.NullI)
            {
               temp = new XElement("ANTENNAID");
               temp.SetValue(antID);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (isDirected.HasValue)
            {
               temp = new XElement("IS_DIRECTED");
               temp.SetValue(isDirected.Value);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (HBeamWidth != IM.NullD)
            {
               temp = new XElement("DDA_WIDTH_H");
               temp.SetValue(HBeamWidth);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (VBeamWidth != IM.NullD)
            {
               temp = new XElement("DDA_WIDTH_V");
               temp.SetValue(VBeamWidth);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (txEquipID != IM.NullI)
            {
               temp = new XElement("TREQUIPMENTID");
               temp.SetValue(txEquipID);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, txID);
            root.Element("Transmitters").Add(elem);
            //-----
            dictTransm.Add(ptrStr, txID);
            return txID;
         }
      }
   }
}
