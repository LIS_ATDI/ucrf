﻿namespace XICSM.UcrfRfaNET.Monitoring
{
   partial class FSettingFtp
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.label1 = new System.Windows.Forms.Label();
         this.tbHost = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.tbUserName = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.tbPassword = new System.Windows.Forms.TextBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.buttonTestConnection = new System.Windows.Forms.Button();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.tbPasswordIcsm = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.tbUserNameIcsm = new System.Windows.Forms.TextBox();
         this.tbHostIcsm = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.buttonOk = new System.Windows.Forms.Button();
         this.openFileDialogTestConnection = new System.Windows.Forms.OpenFileDialog();
         this.btnTestIcsm = new System.Windows.Forms.Button();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 26);
         this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(29, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Host";
         // 
         // tbHost
         // 
         this.tbHost.Location = new System.Drawing.Point(111, 23);
         this.tbHost.Margin = new System.Windows.Forms.Padding(2);
         this.tbHost.Name = "tbHost";
         this.tbHost.Size = new System.Drawing.Size(255, 20);
         this.tbHost.TabIndex = 1;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 50);
         this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(60, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "User Name";
         // 
         // tbUserName
         // 
         this.tbUserName.Location = new System.Drawing.Point(111, 47);
         this.tbUserName.Margin = new System.Windows.Forms.Padding(2);
         this.tbUserName.Name = "tbUserName";
         this.tbUserName.Size = new System.Drawing.Size(255, 20);
         this.tbUserName.TabIndex = 5;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(12, 74);
         this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(53, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "Password";
         // 
         // tbPassword
         // 
         this.tbPassword.Location = new System.Drawing.Point(110, 71);
         this.tbPassword.Margin = new System.Windows.Forms.Padding(2);
         this.tbPassword.Name = "tbPassword";
         this.tbPassword.PasswordChar = '*';
         this.tbPassword.Size = new System.Drawing.Size(256, 20);
         this.tbPassword.TabIndex = 7;
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.buttonTestConnection);
         this.groupBox1.Controls.Add(this.tbPassword);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.tbHost);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.tbUserName);
         this.groupBox1.Location = new System.Drawing.Point(12, 12);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(380, 142);
         this.groupBox1.TabIndex = 11;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Settings for FTP server of R135";
         // 
         // buttonTestConnection
         // 
         this.buttonTestConnection.Location = new System.Drawing.Point(291, 105);
         this.buttonTestConnection.Name = "buttonTestConnection";
         this.buttonTestConnection.Size = new System.Drawing.Size(75, 23);
         this.buttonTestConnection.TabIndex = 15;
         this.buttonTestConnection.Text = "Test";
         this.buttonTestConnection.UseVisualStyleBackColor = true;
         this.buttonTestConnection.Click += new System.EventHandler(this.buttonTestConnection_Click);
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.btnTestIcsm);
         this.groupBox2.Controls.Add(this.tbPasswordIcsm);
         this.groupBox2.Controls.Add(this.label5);
         this.groupBox2.Controls.Add(this.label6);
         this.groupBox2.Controls.Add(this.tbUserNameIcsm);
         this.groupBox2.Controls.Add(this.tbHostIcsm);
         this.groupBox2.Controls.Add(this.label4);
         this.groupBox2.Location = new System.Drawing.Point(12, 167);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(380, 134);
         this.groupBox2.TabIndex = 12;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Settings for FTP server of ICS Manager";
         // 
         // tbPasswordIcsm
         // 
         this.tbPasswordIcsm.Location = new System.Drawing.Point(109, 66);
         this.tbPasswordIcsm.Margin = new System.Windows.Forms.Padding(2);
         this.tbPasswordIcsm.Name = "tbPasswordIcsm";
         this.tbPasswordIcsm.PasswordChar = '*';
         this.tbPasswordIcsm.Size = new System.Drawing.Size(256, 20);
         this.tbPasswordIcsm.TabIndex = 13;
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(11, 45);
         this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(60, 13);
         this.label5.TabIndex = 10;
         this.label5.Text = "User Name";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(11, 69);
         this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(53, 13);
         this.label6.TabIndex = 11;
         this.label6.Text = "Password";
         // 
         // tbUserNameIcsm
         // 
         this.tbUserNameIcsm.Location = new System.Drawing.Point(110, 42);
         this.tbUserNameIcsm.Margin = new System.Windows.Forms.Padding(2);
         this.tbUserNameIcsm.Name = "tbUserNameIcsm";
         this.tbUserNameIcsm.Size = new System.Drawing.Size(255, 20);
         this.tbUserNameIcsm.TabIndex = 12;
         // 
         // tbHostIcsm
         // 
         this.tbHostIcsm.Location = new System.Drawing.Point(110, 18);
         this.tbHostIcsm.Margin = new System.Windows.Forms.Padding(2);
         this.tbHostIcsm.Name = "tbHostIcsm";
         this.tbHostIcsm.Size = new System.Drawing.Size(256, 20);
         this.tbHostIcsm.TabIndex = 9;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(12, 21);
         this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(29, 13);
         this.label4.TabIndex = 8;
         this.label4.Text = "Host";
         // 
         // buttonCancel
         // 
         this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.buttonCancel.Location = new System.Drawing.Point(305, 338);
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.Size = new System.Drawing.Size(75, 23);
         this.buttonCancel.TabIndex = 13;
         this.buttonCancel.Text = "Cancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
         // 
         // buttonOk
         // 
         this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
         this.buttonOk.Location = new System.Drawing.Point(203, 338);
         this.buttonOk.Name = "buttonOk";
         this.buttonOk.Size = new System.Drawing.Size(75, 23);
         this.buttonOk.TabIndex = 14;
         this.buttonOk.Text = "OK";
         this.buttonOk.UseVisualStyleBackColor = true;
         this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
         // 
         // btnTestIcsm
         // 
         this.btnTestIcsm.Location = new System.Drawing.Point(290, 97);
         this.btnTestIcsm.Name = "btnTestIcsm";
         this.btnTestIcsm.Size = new System.Drawing.Size(75, 23);
         this.btnTestIcsm.TabIndex = 14;
         this.btnTestIcsm.Text = "Test";
         this.btnTestIcsm.UseVisualStyleBackColor = true;
         this.btnTestIcsm.Click += new System.EventHandler(this.btnTestIcsm_Click);
         // 
         // FSettingFtp
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.CancelButton = this.buttonCancel;
         this.ClientSize = new System.Drawing.Size(403, 385);
         this.Controls.Add(this.buttonOk);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.Margin = new System.Windows.Forms.Padding(2);
         this.Name = "FSettingFtp";
         this.Text = "FTP settings";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbHost;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox tbUserName;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbPassword;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.TextBox tbHostIcsm;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button buttonTestConnection;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.Button buttonOk;
      private System.Windows.Forms.OpenFileDialog openFileDialogTestConnection;
      private System.Windows.Forms.TextBox tbPasswordIcsm;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox tbUserNameIcsm;
      private System.Windows.Forms.Button btnTestIcsm;
   }
}