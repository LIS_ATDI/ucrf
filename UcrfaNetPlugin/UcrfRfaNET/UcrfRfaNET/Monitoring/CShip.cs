﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Monitoring
{
    public class CShip : BaseStationXML
    {
        int ID { get; set; }
        int OWNER_ID { get; set; }
        string NAME { get; set; }
        string NAME_ENG { get; set; }
        string CALL_SIGN { get; set; }
        string PORT_REGISTER { get; set; }
        string PORT_REGISTER_ENG { get; set; }
        bool? USE_A1 { get; set; }
        bool? USE_A2 { get; set; }
        bool? USE_A3 { get; set; }
        string INMARSAT_NUM1 { get; set; }
        string INMARSAT_NUM2 { get; set; }
        string COSPAS_SARSAT_ID1 { get; set; }
        string COSPAS_SARSAT_ID2 { get; set; }
        string COSPAS_SARSAT_ID3 { get; set; }
        bool? UA_INTERNAL_WATER { get; set; }
        string TERRITORIAL { get; set; }
        string OWNER { get; set; }
        string OWNER_ENG { get; set; }
        string SHIP_OWNER_ENG { get; set; }
        string DEPT_REGISTER { get; set; }
        string DEPT_REGISTER_ENG { get; set; }
        string REMARKS { get; set; }
        string CustTxt2;
        string NatSrv;
        string SelCall;
        string MmsiNum;
        string ShipType;

        private struct ShipEquipment
        {
            public int id;
            public int eqpId;
            public int cnt;
            public string de;
            public string typ;
            //public string cls;
            public Power pwr;
        }
        List<ShipEquipment> shipEqp = new List<ShipEquipment>();

      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CShip(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
          ID = IM.NullI;
          OWNER_ID = IM.NullI;
          IMRecordset rsObj = new IMRecordset(PlugTbl.itblXnrfaShipExt, IMRecordset.Mode.ReadOnly);
          try
          {
              rsObj.Select("Ship.NAME,Ship.NAME_LATIN,Ship.CALL_SIGN,Ship.CUST_TXT2,Port.NAME_UKR,Port.NAME_ENG,"+
                  "Ship.CUST_CHB1,Ship.CUST_CHB2,Ship.CUST_CHB3,Ship.CUST_CHB4," +//A1,A2,A3,Internal
                  "Ship.CUST_TXT2,Ship.CUST_TXT6,Ship.CUST_TXT7,"+
                  "Ship.CUST_TXT8,Ship.CUST_TXT9," +//Inmarsat
                  "Ship.SHIP_TYPE,"+
                  "Ship.SEL_CALL_1,Ship.MMSI_NUM,Ship.NAT_SRV,"+
                  "Ship.OWNER_ID,Owner.NAME,Owner.NAME_LATIN,ShipOwner.NAME,ShipOwner.NAME_LATIN,Charter.NAME,Charter.NAME_LATIN,"+
                  "Registrator.NAME,Registrator.NAME_LATIN,Ship.REMARK," +
                  "COSPAS_SARSAT_IDENT_1,COSPAS_SARSAT_IDENT_2,COSPAS_SARSAT_IDENT_3,BANDWIDTH");
              rsObj.SetWhere("Ship.ID", IMRecordset.Operation.Eq, rec.Id);
              rsObj.Open();
              if (!rsObj.IsEOF())
              {
                  ID = rec.Id;
                  NAME = rsObj.GetS("Ship.NAME");
                  NAME_ENG = rsObj.GetS("Ship.NAME_LATIN");
                  OWNER_ID = rsObj.GetI("Ship.OWNER_ID");
                  CustTxt2 = rsObj.GetS("Ship.CUST_TXT2");
                  CALL_SIGN = rsObj.GetS("Ship.CALL_SIGN");
                  PORT_REGISTER = rsObj.GetS("Port.NAME_UKR");
                  PORT_REGISTER_ENG = rsObj.GetS("Port.NAME_ENG");
                  USE_A1 = rsObj.GetB("Ship.CUST_CHB1");
                  USE_A2 = rsObj.GetB("Ship.CUST_CHB2");
                  USE_A3 = rsObj.GetB("Ship.CUST_CHB3");
                  INMARSAT_NUM1 = rsObj.GetS("Ship.CUST_TXT8");
                  INMARSAT_NUM2 = rsObj.GetS("Ship.CUST_TXT9");
                  COSPAS_SARSAT_ID1 = rsObj.GetS("COSPAS_SARSAT_IDENT_1");
                  COSPAS_SARSAT_ID2 = rsObj.GetS("COSPAS_SARSAT_IDENT_2");
                  COSPAS_SARSAT_ID3 = rsObj.GetS("COSPAS_SARSAT_IDENT_3");
                  UA_INTERNAL_WATER = rsObj.GetB("Ship.CUST_CHB4");
                  TERRITORIAL = rsObj.GetS("Ship.CUST_TXT6");
                  OWNER = rsObj.GetS("Owner.NAME");
                  OWNER_ENG = rsObj.GetS("Owner.NAME_LATIN");
                  SHIP_OWNER_ENG = rsObj.GetS("ShipOwner.NAME_LATIN");
                  DEPT_REGISTER = rsObj.GetS("Registrator.NAME");
                  DEPT_REGISTER_ENG = rsObj.GetS("Registrator.NAME_LATIN");
                  REMARKS = rsObj.GetS("Ship.REMARK");
                  BW = rsObj.GetD("BANDWIDTH");
                  NatSrv = rsObj.GetS("Ship.NAT_SRV");
                  SelCall = rsObj.GetS("Ship.SEL_CALL_1");
                  MmsiNum = rsObj.GetS("Ship.MMSI_NUM");
                  ShipType = rsObj.GetS("Ship.SHIP_TYPE");

                  // ship equipment
                  shipEqp = new List<ShipEquipment>();
                  IMRecordset rsEqp = new IMRecordset(ICSMTbl.itblShipEqp, IMRecordset.Mode.ReadOnly);
                  try
                  {
                      rsEqp.Select("ID,EQUIP_ID,POWER,EMICLS,REMARK,Equipment.DESIG_EMISSION,Equipment.CUST_TXT6,Equipment.CUST_TXT5,Equipment.CUST_TXT2");
                      rsEqp.SetWhere("STA_ID", IMRecordset.Operation.Eq, rec.Id);
                      for (rsEqp.Open(); !rsEqp.IsEOF(); rsEqp.MoveNext())
                      {
                          ShipEquipment eqp = new ShipEquipment();
                          eqp.id = rsEqp.GetI("ID");
                          eqp.eqpId = rsEqp.GetI("EQUIP_ID");
                          eqp.pwr = new Power();
                          eqp.pwr[PowerUnits.dBW] = rsEqp.GetD("POWER");
                          eqp.de = rsEqp.GetS("Equipment.CUST_TXT2");
                          eqp.typ = rsEqp.GetS("Equipment.CUST_TXT5");
                          //eqp.cls = rsEqp.GetS("Equipment.CUST_TXT6");
                          eqp.cnt = 0;
                          try { eqp.cnt = Convert.ToInt32(rsEqp.GetS("REMARK")); }
                          catch { }
                          shipEqp.Add(eqp);
                      }
                  }
                  finally
                  {
                      rsEqp.Destroy();
                  }
              }
              else
                  throw new Exception("No Ship with ID="+rec.Id.ToString()+" in database");
          }
          finally
          {
              rsObj.Destroy();
          }
      }

      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
          if (ID == IM.NullI)
              return; //Прекращаем обработку, та как не загрузили станцию
          //-------------
          // Секция STATION
          base.Export();
          //Секция Ship
          XElement elem = new XElement("SHIP");
          AddElement(elem, "ID", ID);
          AddElement(elem, "NAME", NAME, 64);
          AddElement(elem, "NAME_ENG", NAME_ENG, 64);
          AddElement(elem, "PORT_REGISTER", PORT_REGISTER, 128);
          AddElement(elem, "PORT_REGISTER_ENG", PORT_REGISTER_ENG, 128);
          AddElement(elem, "USE_A1", USE_A1);
          AddElement(elem, "USE_A2", USE_A2);
          AddElement(elem, "USE_A3", USE_A3);
          AddElement(elem, "INMARSAT_NUM1", INMARSAT_NUM1, 16);
          AddElement(elem, "INMARSAT_NUM2", INMARSAT_NUM2, 16);
          AddElement(elem, "COSPAS_SARSAT_ID1", COSPAS_SARSAT_ID1, 16);
          AddElement(elem, "COSPAS_SARSAT_ID2", COSPAS_SARSAT_ID2, 16);
          AddElement(elem, "COSPAS_SARSAT_ID3", COSPAS_SARSAT_ID3, 16);
          AddElement(elem, "UA_INTERNAL_WATER", UA_INTERNAL_WATER);
          AddElement(elem, "TERRITORIAL", TERRITORIAL, 128);
          AddElement(elem, "OWNER", OWNER, 256);
          AddElement(elem, "OWNER_ENG", OWNER_ENG, 256);
          AddElement(elem, "SHIP_OWNER_ENG", SHIP_OWNER_ENG, 256);
          AddElement(elem, "DEPT_REGISTER", DEPT_REGISTER, 256);
          AddElement(elem, "DEPT_REGISTER_ENG", DEPT_REGISTER_ENG, 256);
          AddElement(elem, "REMARKS", REMARKS, 128);                  

          CExternalID.GetItemKey(elem, ID);
          _root.Element("Ships").Add(elem);

          // sectors, transmitters, equipment
          int cnt = 0;
          foreach (ShipEquipment shEqp in shipEqp)
          {
              XElement sector = new XElement("SECTOR");
              int gSectorID = CExternalID.GetStringExtID(string.Format("SECTOR_{0}_{1}", globalStationID, cnt++));
              sector.Add(new XElement("SECTORID", gSectorID));
              sector.Add(new XElement("STATIONID_REF", globalStationID));
              
              AddElement(sector, "EMISSIONCLASS", shEqp.de);
            
              /* #7253, #7252
              int lcsect = 0;
              if (shEqp.typ.IndexOf("УКХ радіостанції") > 0) lcsect = 21;
              else if (shEqp.typ.IndexOf("ПХ/КХ") > 0) lcsect = 22;
              else if (shEqp.typ.IndexOf("земні станції") > 0) lcsect = 23;
              else if (shEqp.typ.IndexOf("Коспас") > 0) lcsect = 24;
              else if (shEqp.typ.IndexOf("Інмарсат") > 0) lcsect = 25;
              else if (shEqp.typ.IndexOf("УКХ АРБ") > 0) lcsect = 26;
              else if (shEqp.typ.IndexOf("радіолокаційні відповідачі") > 0) lcsect = 27;
              else if (shEqp.typ.IndexOf("аппарат. двоб. р/тел. зв.") > 0) lcsect = 28;
              else if (shEqp.typ.IndexOf("РЛС") > 0) lcsect = 29;
              else if (shEqp.typ.IndexOf("Інше") > 0) lcsect = 30;
              else if (shEqp.typ.IndexOf("АІС") > 0) lcsect = 31;
              else if (shEqp.typ.IndexOf("НАВТЕКС") > 0) lcsect = 32;
              if (lcsect > 0)
                  AddElement(sector, "LIC_SECTION", lcsect);
               */
              AddElement(sector, "LIC_SECTION", shEqp.typ);

              AddElement(sector, "TRANSMITTER_COUNT", shEqp.cnt);
              
              CExternalID.GetItemKey(sector, gSectorID);
              _root.Element("Sectors").Add(sector);
              
              int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.itblEquipPmr, shEqp.eqpId));
              CTransmitter.WriteNewTransmitter(_root, new RecordPtr(ICSMTbl.itblShipEqp, shEqp.id), gSectorID, IM.NullI, txEquipID, shEqp.pwr[PowerUnits.dBm], 
                    IM.NullD, IM.NullD, "", IM.NullD, IM.NullI, IM.NullD, IM.NullI, "", IM.NullD, null, null);
          }
      }

      protected override int getOwnerID()
      {
          return CEnterprise.WriteEnterprise(_root, OWNER_ID);
      }
      protected override string getName()
      {
          return NAME;
      }
      protected override string getCallSign()
      {
          return CustTxt2;
      }
      protected override string getStatus()
      {
          return "";
      }
      protected override RecordPtr getPosition()
      {
          return new RecordPtr();
      }
      protected override double getAltitude()
      {
          return IM.NullD;
      }
      protected override double getLatitudeDEC()
      {
          return IM.NullD;
      }
      protected override double getLongitudeDEC()
      {
          return IM.NullD;
      }
      protected override int getTechID()
      {
          return CRadioTech.WriteNewRadioTech(_root, ShipType == "RS" ? CRadioTech.riverPerm : CRadioTech.seaLicence);
      }
      protected override int getSystemID()
      {
          return CSystemsXML.GetSystemID("РБСС", _root);
      }
      protected override int GetShipId() { return ID; }
      protected override string GetPermStnClassName()
      {
          return "Суднова";
      }
      protected override string GetNickTelegraph()
      {
          return CALL_SIGN;
      }
      protected override string GetCarModel()
      {
          return NAME;
      }
      protected override string GetCarRegNum() 
      { 
          return NAME_ENG; 
      }
      protected override string GetCorresponderCategory()
      {
          return NatSrv;
      }
      protected override string GetSeaMobileSrvc()
      {
          return MmsiNum;
      }
      protected override string GetCallSelected()
      {
          return SelCall;
      }
    }
}
