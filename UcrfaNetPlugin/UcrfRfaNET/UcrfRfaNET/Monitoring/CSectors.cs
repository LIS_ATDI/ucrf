﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CSectors
   {
      public static int WriteSector(XElement root, int stationID, string emiClass)
      {
         return WriteSector(root, stationID, emiClass, IM.NullI, false);
      }
      //===================================================
      public static int WriteSector(XElement root, int stationID, string emiClass, bool inversGlobID)
      {
         return WriteSector(root, stationID, emiClass, IM.NullI, inversGlobID);
      }
      //===================================================
      public static int WriteSector(XElement root, int stationID, string emiClass, int sectorNum, bool inversGlobID)
      {
         emiClass = emiClass.Replace("-", ""); //#4092

         string sectorName = "";
         if (sectorNum >= 0 && sectorNum <= 25)
             sectorName = new string((char)((int)'A' + sectorNum), 1);
         else
             sectorName = "A"+sectorNum.ToString();
          /*
         switch (sectorNum)
         {
            case 0: sectorName = "A";break;
            case 1: sectorName = "B"; break;
            case 2: sectorName = "C"; break;
            case 3: sectorName = "D"; break;
            case 4: sectorName = "E"; break;
            case 5: sectorName = "F"; break;
         }
           * */
         XElement elem = new XElement("SECTOR");
         int gSectorID = CExternalID.GetStringExtID(string.Format("SECTOR_{0}_{1}", stationID, sectorName));
         if (inversGlobID)
            gSectorID = -gSectorID;
         XElement temp = new XElement("SECTORID");
         temp.SetValue(gSectorID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("STATIONID_REF");
         temp.SetValue(stationID);
         elem.Add(temp);
         ///////////////////////////////////////////
         if (!string.IsNullOrEmpty(sectorName))
         {
            temp = new XElement("SECTORNAME");
            temp.SetValue(sectorName);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         if (!string.IsNullOrEmpty(emiClass))
         {
            temp = new XElement("EMISSIONCLASS");
            temp.SetValue(emiClass.ToStrXMLMonitoring());
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         CExternalID.GetItemKey(elem, gSectorID);
         root.Element("Sectors").Add(elem);
         return gSectorID;
      }
   }
}
