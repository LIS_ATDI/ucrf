﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CTdabStation : BaseStationXML
   {
            //===================================================
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CTdabStation(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if (stationObj == null)
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         //Заполняем сектор
         int sectorID = CSectors.WriteSector(_root, globalStationID, stationObj.GetS("DESIG_EM"));
         //-------------
         //Заполняем частоты
         int freqID = CStationFreqsXML.WriteStationFreq(_root, sectorID, IM.NullD, stationObj.GetD("FREQ"), ICSMTbl.itblTDAB);
         //-----------
         //Заполняем антену
         //int antennaID = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.ANTENNA_BRO, stationObj.GetI("ANT_ID")));
         int antennaID = IM.NullI; //Нет антенны
         //-----------
         //Заполняем оборудование
         int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.EQUIP_BRO, stationObj.GetI("EQUIP_ID")));
         //-----------
         {//Заполняем передатчик
            double pow = stationObj.GetD("PWR_ANT");
            if (pow != IM.NullD)
               pow += 30; //dBW -> dBm
            double azimuth = stationObj.GetD("AZIMUTH");
            double agl = stationObj.GetD("AGL");
            string polar = stationObj.GetS("POLARIZATION");
            double gain = stationObj.GetD("GAIN");
            CTransmitter.WriteNewTransmitter(_root, sectorID, antennaID, txEquipID, pow, azimuth, agl, polar, gain, IM.NullI, IM.NullD, IM.NullI, "");
         }
      }
      //===================================================
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = stationObj.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return stationObj.GetS("CALL_SIGN");
      }
      //---------
      protected override string getStatus()
      {
         return stationObj.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         double asl = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,ASL");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               asl = rs.GetD("ASL");
         }
         finally
         {
            rs.Destroy();
         }
         return asl;
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(ICSMTbl.itblPositionBro, stationObj.GetI("SITE_ID"));
      }
      //===================================================
      /// <summary>
      /// Возвращает группу пользователей
      /// </summary>
      protected override EEntrpGroupType GetEntrpGroupType()
      {
         if (CMonitoringAppl.GetListOfLicence(applID).Count > 0)
            return EEntrpGroupType.DTRP;
         return EEntrpGroupType.TECH;
      }
      #endregion

   }
}
