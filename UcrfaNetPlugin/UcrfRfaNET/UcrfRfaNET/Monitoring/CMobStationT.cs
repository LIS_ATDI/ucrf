﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;
using Lis.CommonLib.Extensions;


namespace XICSM.UcrfRfaNET.Monitoring
{
   public abstract class CMobStationT : BaseStationXML
   {
      //===================================================
      private List<int> _sectorsIdList;
      string FreqTable { get { return getFreqTable(); } }
      string PosTable { get { return getPosTable(); } }
      string AntTable { get { return getAntTable(); } }
      string EquipTable { get { return getEquipTable(); } }
      int NetId { get; set; }
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      protected abstract string getFreqTable();   //Возвращает название таблицы частот
      protected abstract string getPosTable();    //Возвращает название таблицы позиции
      protected abstract string getAntTable();    //Возвращает название таблицы антены
      protected abstract string getEquipTable();  //Возвращает название таблицы оборудования
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CMobStationT(XElement root, RecordPtr rec, int _applID, ref List<int> sectList)
         : base(root, rec, _applID)
      {
         _sectorsIdList = sectList;
         NetId = IM.NullI;
          //search for network(s) 
          if (_sectorsIdList != null && _sectorsIdList.Count > 0)
          {
            IMRecordset rsNetRef = new IMRecordset(PlugTbl.RefBaseStation, IMRecordset.Mode.ReadOnly);
            try
            {
             rsNetRef.Select("NET_ID");
             string sectorCriteriaList = "STATION_ID="+_sectorsIdList[0].ToString();
             for (int i = 1; i < _sectorsIdList.Count; i++)
                sectorCriteriaList += (" or STATION_ID="+_sectorsIdList[i].ToString());
             rsNetRef.SetAdditional("STATION_TABLE = '" + rec.Table + "' and (" + sectorCriteriaList + ")");
             //for (rsNetRef.Open(); !rsNetRef.IsEOF(); rsNetRef.MoveNext())
             rsNetRef.Open(); if (!rsNetRef.IsEOF())
             {
                 NetId = rsNetRef.GetI("NET_ID");
             }
            }
            finally
            {
             rsNetRef.Destroy();
            }
          }
      }
      protected override int GetNetId()
      {
          return NetId != IM.NullI ? CNet.AddItemToXml(_root, NetId) : IM.NullI;
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if (stationObj == null)
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         for(int i = 0; i< _sectorsIdList.Count;i++)
         {
            int itemSector = _sectorsIdList[i];
            IMObject sectorObj = null;
            try
            {
               sectorObj = IMObject.LoadFromDB(currentPtr.Table, itemSector);
            }
            catch(Exception ex)
            {
               CLogs.WriteError(ELogsWhat.Monitoring, string.Format("Can't load sector ID={0}. {1}", itemSector, ex.Message));
               sectorObj = null;
            }
            if(sectorObj == null)
               continue;
            //Заполняем сектор
            int sectorID = CSectors.WriteSector(_root, globalStationID, sectorObj.GetS("DESIG_EMISSION"), i, false);
            //-------------
            string standard = stationObj.GetS("STANDARD");
            //Заполняем частоты
            //int freqID = IM.NullI;
            {
               List<CStationFreqsXML.DataFreq> listFreq = new List<CStationFreqsXML.DataFreq>();
               IMRecordset rs = new IMRecordset(FreqTable, IMRecordset.Mode.ReadOnly);
               rs.Select("TX_FREQ,RX_FREQ,ChannelTx.CHANNEL,ChannelRx.CHANNEL");
               rs.SetWhere("STA_ID", IMRecordset.Operation.Eq, itemSector);
               rs.OrderBy("ChannelTx.CHANNEL", OrderDirection.Ascending);
               try
               {
                  int lastChannel = IM.NullI - 2;
                  for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                  {
                     if ((standard == UcrfRfaNET.CRadioTech.GSM_900) || (standard == UcrfRfaNET.CRadioTech.GSM_1800))
                     {
                        int curChannel = rs.GetS("ChannelTx.CHANNEL").ToInt32(IM.NullI);
                        if ((lastChannel + 1) == curChannel)
                        {
                           listFreq[listFreq.Count - 1].FreqTxToMhz = rs.GetD("TX_FREQ");
                           listFreq[listFreq.Count - 1].FreqRxToMhz = rs.GetD("RX_FREQ");
                           listFreq[listFreq.Count - 1].ChannelTo = curChannel;
                        }
                        else
                        {
                           listFreq.Add(new CStationFreqsXML.DataFreq());
                           listFreq[listFreq.Count - 1].FreqTxFromMhz = rs.GetD("TX_FREQ");
                           listFreq[listFreq.Count - 1].FreqRxFromMhz = rs.GetD("RX_FREQ");
                           listFreq[listFreq.Count - 1].ChannelFrom = curChannel;
                           listFreq[listFreq.Count - 1].FreqTxToMhz = rs.GetD("TX_FREQ");
                           listFreq[listFreq.Count - 1].FreqRxToMhz = rs.GetD("RX_FREQ");
                           listFreq[listFreq.Count - 1].ChannelTo = curChannel;
                        }
                        lastChannel = curChannel;
                     }
                     else
                     {
                        listFreq.Add(new CStationFreqsXML.DataFreq());
                        if (!UcrfRfaNET.CRadioTech.IsTechnol(standard))
                        {//Не технологічний звязок
                           listFreq[listFreq.Count - 1].FreqTxFromMhz = rs.GetD("TX_FREQ");
                           listFreq[listFreq.Count - 1].FreqRxFromMhz = rs.GetD("RX_FREQ");
                           listFreq[listFreq.Count - 1].ChannelFrom = rs.GetS("ChannelTx.CHANNEL").ToInt32(IM.NullI);
                           if (!UcrfRfaNET.CRadioTech.getListRadioTech(AppType.AppRR).Contains(standard) ||
                               standard == UcrfRfaNET.CRadioTech.UMTS)
                              listFreq[listFreq.Count - 1].ChannelInFrom = rs.GetS("ChannelRx.CHANNEL").ToInt32(IM.NullI);
                        }
                        else
                        {//Технологічний звязок
                           listFreq[listFreq.Count - 1].FreqTxFromMhz = rs.GetD("TX_FREQ");
                           listFreq[listFreq.Count - 1].FreqRxFromMhz = rs.GetD("RX_FREQ");
                        }
                     }
                  }
               }
               finally
               {
                  rs.Destroy();
               }
               //-----
               foreach (var dataFreq in listFreq)
               {
                  CStationFreqsXML.WriteStationFreq(_root, sectorID, FreqTable, dataFreq);
               }
            }
            //-----------
            //Заполняем антену
            int antennaID = CAntenna.WriteNewAntenna(_root, new RecordPtr(AntTable, sectorObj.GetI("ANT_ID")));
            //-----------
            //Заполняем оборудование
            int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(EquipTable, sectorObj.GetI("EQUIP_ID")));
            //-----------
            {
               //Заполняем передатчик
               double pow = sectorObj.GetD("PWR_ANT") + 30.0;
               double azimuth = sectorObj.GetD("AZIMUTH");
               double agl = sectorObj.GetD("AGL");
               string polar = sectorObj.GetS("POLAR");
               double gain = sectorObj.GetD("GAIN");
               CTransmitter.TTransmitterData data = new CTransmitter.TTransmitterData();
               data.Riseangle = sectorObj.GetD("ELEVATION");
               if (UcrfRfaNET.CRadioTech.IsTechnol(standard))
               {
                  data.AzimuthEnd = azimuth.Round(2);
                  if (data.AzimuthEnd == 0.0)//#4185
                     data.AzimuthEnd = 360;
               }

               //#4306
               if (UcrfRfaNET.CRadioTech.getListRadioTech(AppType.AppRR).Contains(standard))
               {//#4256
                  if ((standard != UcrfRfaNET.CRadioTech.CDMA_450) && (standard != UcrfRfaNET.CRadioTech.CDMA_800)) //#4306
                     data.Riseangle = 1.0;
                  if ((standard == UcrfRfaNET.CRadioTech.GSM_900) || (standard == UcrfRfaNET.CRadioTech.GSM_1800))
                  {
                     azimuth = 0.0;
                     data.AzimuthEnd = 360.0;
                  }
               }

               CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), sectorID, antennaID, txEquipID, pow, azimuth, agl, polar, gain,
                                                IM.NullI, IM.NullD, sectorObj.GetI("ANT_ID"), AntTable, IM.NullD, null, data);
            }
         }
      }
      //===================================================
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = stationObj.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return stationObj.GetS("CUST_TXT9");
      }
      //---------
      protected override string getStatus()
      {
         return stationObj.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(PosTable, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("POS_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(PosTable, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("POS_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         double retVal = IM.NullD;
         {
            IMRecordset rs = new IMRecordset(PosTable, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,ASL");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("POS_ID"));
            try
            {
               rs.Open();
               if(!rs.IsEOF())
                  retVal = rs.GetD("ASL");
            }
            finally
            {
               rs.Destroy();
            }
         }
         return retVal;
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(PosTable, stationObj.GetI("POS_ID"));
      }
      //===================================================
      /// <summary>
      /// Возвращает FREQ_BAND_WIDTH
      /// </summary>
      protected override string getFREQ_BAND_WIDTH()
      {
         string retVal = "";
         string standard = stationObj.GetS("STANDARD");
         if (UcrfRfaNET.CRadioTech.IsTechnol(standard))
         {//#3922
            int countFreq = 0;
            {//Кол-во уникальних частот
               IMRecordset rs = new IMRecordset(FreqTable, IMRecordset.Mode.ReadOnly);
               rs.Select("TX_FREQ,RX_FREQ");
               rs.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationID);
               try
               {
                  for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                  {
                     double freqTx = rs.GetD("TX_FREQ").Round(6);
                     double freqRx = rs.GetD("RX_FREQ").Round(6);
                     countFreq++;
                     if (Math.Abs(freqTx - freqRx) > 0.000001)
                        countFreq++;
                  }
               }
               finally
               {
                  rs.Destroy();
               }
            }
            //---------
            double bandWidth = IM.NullD;
            {//ширина полосы
               IMRecordset rs = new IMRecordset(currentPtr.Table, IMRecordset.Mode.ReadOnly);
               rs.Select("Plan.BANDWIDTH");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, stationID);
               try
               {
                  rs.Open();
                  if (!rs.IsEOF())
                     bandWidth = rs.GetD("Plan.BANDWIDTH");
               }
               finally
               {
                  rs.Destroy();
               }
            }
            //---------
            if ((countFreq > 0) && (bandWidth != IM.NullD))
               retVal = ((int)((double)countFreq * bandWidth * 1000.0)).ToString();
         }
         else if (CMonitoringAppl.GetListOfLicence(applID).Count > 0)
            retVal = "Згідно ліцензії на користування РЧР";
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Возвращает TRANSMITTER_COUNT
      /// </summary>
      protected override int GetTransmitterCount()
      {
         int retVal = stationObj.GetI("EQUIP_NBR");
         if (retVal == IM.NullI)
            retVal = -2;  //#3890
         return retVal;
      }
      /// <summary>
      /// Возвращает COM_SYS_ID (ссылку коммуникацтонный стандарт)
      /// </summary>
      protected override int GetComSysId()
      {
         int comSystem = IM.NullI;
         if (stationObj.GetS("NETWORK_IDENT").ToUpper().Contains("АЛТАЙ"))
            comSystem = CComSystem.GetComSystemID("\"Алтай\"", _root);
         return comSystem;
      }
      /// <summary>
      /// Возвращает PERM_STN_CLASS_ID
      /// </summary>
      protected override string GetPermStnClassName()
      {//#3973
         string standard = stationObj.GetS("STANDARD");
         if (UcrfRfaNET.CRadioTech.IsTechnol(standard))
         {
            CPermStnClass.PermStnClassData exData = new CPermStnClass.PermStnClassData();
            string className = stationObj.GetS("CLASS").ToUpper();
            switch (className.ToUpper())
            {// Использовать eri файл не получилось, так как у нас там
             // другие значения. Потому пришлось жестко прописать в коде.
               case "FB":
                  return "Базова";
               case "FL":
                  return "Стаціонарна абонентська";
               case "FC":
                  return "Берегова";
            }
         }
         return "";
      }

      #endregion
   }
}
