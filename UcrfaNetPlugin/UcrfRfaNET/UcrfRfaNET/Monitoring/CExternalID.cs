﻿using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CExternalID
   {
      //===================================================
      /// <summary>
      /// Создает обязательный элемента в каждой записи C DBGUID ICSM-а
      /// </summary>
      /// <param name="x">Куда поместить</param>
      /// <param name="externalID">Глобальный ID</param>
      /// <returns>x</returns>
      public static XElement GetItemKey(XElement x, int externalID)
      {
         return GetItemKey(x, externalID, CDBGuids.ICSM_DBGuid);
      }
      //===================================================
      /// <summary>
      /// Создает обязательный элемента в каждой записи
      /// </summary>
      /// <param name="x">Куда поместить</param>
      /// <param name="externalID">Глобальный ID</param>
      /// <param name="dbGuidID">ID DBGuid</param>
      /// <returns>x</returns>
      public static XElement GetItemKey(XElement x, int externalID, int dbGuidID)
      {
         x.Add(new XElement("EXTERNALID", externalID));
         x.Add(new XElement("DBGUIDID", dbGuidID));
         return x;
      }
      //===================================================
      /// <summary>
      /// Возвращает уникальный идентификатор для строки
      /// </summary>
      /// <param name="element">строка идентификации</param>
      /// <returns>уникальный ID</returns>
      private static int GetGlobalID(string element)
      {
         int retVal = IM.NullI;
         IMRecordset rs = new IMRecordset(PlugTbl.MONITORING, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,ELEM");
         rs.SetWhere("ELEM", IMRecordset.Operation.Like, element);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               retVal = rs.GetI("ID");
            else
            {
               retVal = IM.AllocID(PlugTbl.MONITORING, 1, -1);
               rs.AddNew();
               rs.Put("ID", retVal);
               rs.Put("ELEM", element);
               rs.Update();
            }
         }
         finally
         {
            rs.Destroy();
         }
         return retVal;
      }
      #region Функции для генерации внешних ID
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для страны
      /// </summary>
      /// <param name="country">название страны</param>
      /// <returns>Возвращает EXTERNALID для страны</returns>
      public static int GetCountryExtID(string country)
      {
         return GetGlobalID(string.Format("COUNTRY_{0}", country));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для области
      /// </summary>
      /// <param name="province">название области</param>
      /// <returns>Возвращает EXTERNALID для области</returns>
      public static int GetProvinceExtID(string province)
      {
         return GetGlobalID(string.Format("PROVINCE_{0}", province));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для района
      /// </summary>
      /// <param name="subprov">название района</param>
      /// <param name="countryID">Глобальный ID страны</param>
      /// <param name="provinceID">Глобальный ID области</param>
      /// <returns>Возвращает EXTERNALID для района</returns>
      public static int GetSubprovinceExtID(string subprov, int provinceID)
      {
         return GetGlobalID(string.Format("SUBPROVINCE_{0}_PROVINCEID_{1}", subprov, provinceID));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для типа населенного пункта
      /// </summary>
      /// <param name="subprov">название района</param>
      /// <returns>Возвращает EXTERNALID для типа населенного пункта</returns>
      public static int GetCityTypeExtID(string cityType)
      {
         return GetGlobalID(string.Format("CITYTYPE_{0}", cityType));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для населенного пункта
      /// </summary>
      /// <param name="city">название населенного пункта</param>
      /// <param name="cityTypeID">ID типа населенного пункта</param>
      /// <param name="subProvID">ID района</param>
      /// <param name="provID">ID области</param>
      /// <param name="countryID">ID страны</param>
      /// <returns>Возвращает EXTERNALID для населенного пункта</returns>
      public static int GetCityExtID(string city, int cityTypeID, int subProvID, int provID, int countryID)
      {
         return GetGlobalID(string.Format("CYTI_{0}_CITYTYPEID_{1}_SUBPROVID_{2}_PROVID_{3}_COUNTRYID_{4}", city, cityTypeID, subProvID, provID, countryID));
      }
      public static int GetStreetTypeId(string streetType)
      {
          return GetGlobalID(string.Format("STREETTYPE_{0}", streetType));
      }
      public static int GetStreetId(int cityId, string streetType, string street)
      {
          return GetGlobalID(string.Format("CITY_{0}_STREET_{1}_{2}", cityId, streetType, street));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для записи в таблице
      /// </summary>
      /// <param name="rec">RecordPtr</param>
      /// <returns>Возвращает EXTERNALID для записи в таблице</returns>
      public static int GetRecordPtrExtID(RecordPtr rec)
      {
         return GetGlobalID(string.Format("RECORD_{0}_{1}", rec.Table, rec.Id));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для строки
      /// </summary>
      /// <param name="val">строка</param>
      /// <returns>Возвращает EXTERNALID для строки</returns>
      public static int GetStringExtID(string val)
      {
         return GetGlobalID(val);
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для разрешений и высновков
      /// </summary>
      /// <param name="number">Нозвание документа</param>
      /// <returns>Возвращает EXTERNALID для разрешений и высновков</returns>
      public static int GetDocumentsExtID(string number)
      {
         return GetGlobalID(string.Format("PERMISSION_{0}", number));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для системы
      /// </summary>
      /// <param name="system">название системы</param>
      /// <returns>Возвращает EXTERNALID для системы</returns>
      public static int GetSystemExtID(string system)
      {
         return GetGlobalID(string.Format("SYSTEM_{0}", system));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для коммуникационной системы
      /// </summary>
      /// <param name="system">название системы</param>
      /// <returns>Возвращает EXTERNALID для системы</returns>
      public static int GetComSystemExtID(string system)
      {
         return GetGlobalID(string.Format("COM_SYSTEM_{0}", system));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для радиотехнологии
      /// </summary>
      /// <param name="radioTech">короткое название радиотехнологии</param>
      /// <returns>Возвращает EXTERNALID для радиотехнологии</returns>
      public static int GetRadioTechExtID(string radioTech)
      {
         return GetGlobalID(string.Format("RADIOTECH_{0}", radioTech));
      }
      //===================================================
      /// <summary>
      /// Возвращает EXTERNALID для класса станции
      /// </summary>
      /// <param name="stnClass">название класса станции</param>
      /// <returns>Возвращает EXTERNALID для радиотехнологии</returns>
      public static int GetPrmStnClassID(string stnClass)
      {
         return GetGlobalID(string.Format("PRM_STN_CLASS_{0}", stnClass));
      }
      //===================================================
      //TODO переделать
      /// <summary>
      /// Частоты
      /// </summary>
      /// <param name="sectorId"></param>
      /// <param name="stationTableName"></param>
      /// <param name="data"></param>
      /// <returns></returns>
      internal static int GetFreqExtId(int sectorId, string stationTableName, CStationFreqsXML.DataFreq data)
      {
         string ptrStr = string.Format("{0}_{1}_{2}_{3}_{4}_{5}",
                                       "FREQ",
                                       stationTableName,
                                       string.Format("{0}-{1}", data.FreqTxFromMhz, data.FreqTxToMhz),
                                       string.Format("{0}-{1}", data.FreqRxFromMhz, data.FreqRxToMhz),
                                       string.Format("{0}-{1}", data.ChannelFrom, data.ChannelTo),
                                       sectorId);
         return GetGlobalID(ptrStr);
      }

      #endregion
   }
}
