﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;
using System;

namespace XICSM.UcrfRfaNET.Monitoring
{
    
   public class CLicence
   {
       // Нужно сделать доступным для подсчета налогов
       public class LicenseData
       {
           public int LicOwnerId { get; set; } //Кто владелец лилицензии (если лицензия используеться другими)
           public int LicID { get; set; }
           public string LicNumber { get; set; }
           public DateTime StartDate { get; set; }
           public DateTime StopDate { get; set; }
           public int LicType { get; set; }
           public int OwnerID { get; set; }
           public string Standard { get; set; }
           //------
           public LicenseData()
           {
               LicOwnerId = IM.NullI;
               LicID = IM.NullI;
               LicNumber = "";
               StartDate = IM.NullT;
               StopDate = IM.NullT;
               LicType = IM.NullI;
               OwnerID = IM.NullI;
               Standard = "";
           }
       }

       /// <summary>
       /// Извлекает данные о лицензии с базы
       /// </summary>
       /// <param name="licId"></param>
       /// <returns></returns>
      public static LicenseData ExtractLicenseDataFromDb(int licId)
      {
            LicenseData licenseData = new LicenseData();

            IMRecordset rsAppLic = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
            rsAppLic.Select("ID,NAME,START_DATE,STOP_DATE,OWNER_ID,STANDARD");
          
            rsAppLic.SetWhere("ID", IMRecordset.Operation.Eq, licId);
            try
            {
                rsAppLic.Open();
                if (!rsAppLic.IsEOF())
                {
                    licenseData.LicID = licId;
                    licenseData.LicNumber = rsAppLic.GetS("NAME");
                    licenseData.StartDate = rsAppLic.GetT("START_DATE");
                    licenseData.StopDate = rsAppLic.GetT("STOP_DATE");
                    licenseData.OwnerID = rsAppLic.GetI("OWNER_ID");
                    licenseData.Standard = rsAppLic.GetS("STANDARD");
                }
            }
            finally
            {
                if (rsAppLic.IsOpen())
                    rsAppLic.Close();

                rsAppLic.Destroy();
            }
           return licenseData;                     
      }

      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictLic.Clear();
         countStnLic = 0;
      }
      //======================================================
      private static Dictionary<string, int> dictLic = new Dictionary<string, int>();
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новой лицензии. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="licID">ID лицензии</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewLicence(XElement root, int licID, int OwnerStationID)
      {
         using (LisProgressBar pb = new LisProgressBar("License"))
         {
            //----
            LicenseData ex = new LicenseData();
            ex.LicNumber = "unknown";
            ex.LicID = licID;
            try
            {
               IMObject licObj = IMObject.LoadFromDB(new RecordPtr(ICSMTbl.LICENCE, licID));
               ex.LicNumber = licObj.GetS("NAME");
               ex.StartDate = licObj.GetT("START_DATE");
               ex.StopDate = licObj.GetT("STOP_DATE");
               ex.OwnerID = licObj.GetI("OWNER_ID");
               ex.Standard = licObj.GetS("STANDARD");
               switch (licObj.GetS("TYPE2"))
               {
                  case "НР":
                     ex.LicType = 3;
                     break;
                  case "НКРЗ":
                     ex.LicType = 2;
                     break;
                  default:
                     ex.LicType = IM.NullI;
                     break;
               }
            }
            catch
            {
               CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't load a licence where ID={0}"), licID));
            }
            //--------------------
            //
            if ((OwnerStationID != IM.NullI) &&
                (ex.OwnerID != IM.NullI) &&
                (ex.OwnerID != OwnerStationID))
            {//Создаем уще одну лицензию того, кто пользуеться
               int licOld = ex.LicID;
               ex.LicID += 10000000;
               ex.LicOwnerId = CreateLicense(root, ex);
               ex.LicID = licOld;  //Возвращаем ID дицензии
               ex.OwnerID = OwnerStationID;
            }
            //----
            int retVal = CreateLicense(root, ex);


            return retVal;
         }
      }
      //===================================================
      private static int CreateLicense(XElement root, LicenseData ex)
      {
         //-----
         string ptrStr = string.Format("{0}_{1}", "LICENCE", ex.LicID);
         if (dictLic.ContainsKey(ptrStr) == true)
            return dictLic[ptrStr];

         XElement elem = new XElement("LICENSE");
         XElement temp = new XElement("LICENSEID");
         temp.SetValue(ex.LicID);
         elem.Add(temp);
         ///////////////////////////////////////////
         if (ex.LicOwnerId != IM.NullI)
         {
            temp = new XElement("LIC_OWN_ID");
            temp.SetValue(ex.LicOwnerId);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         temp = new XElement("LICENSENO");
         temp.SetValue(ex.LicNumber.ToStrXMLMonitoring());
         elem.Add(temp);
         ///////////////////////////////////////////
         if (ex.StartDate != IM.NullT)
         {
            temp = new XElement("LICENSESTARTDATE");
            temp.SetValue(ex.StartDate.ToMDateTime());
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         if (ex.StopDate != IM.NullT)
         {
            temp = new XElement("LICENSEENDDATE");
            temp.SetValue(ex.StopDate.ToMDateTime());
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         if (ex.OwnerID != IM.NullI)
         {
            int entrID = CEnterprise.WriteEnterprise(root, ex.OwnerID);
            if (entrID != IM.NullI)
            {
               temp = new XElement("ENTERPRISEID");
               temp.SetValue(entrID);
               elem.Add(temp);
            }
         }
         ///////////////////////////////////////////
         if (ex.LicType != IM.NullI)
         {
            temp = new XElement("LIC_TYPE");
            temp.SetValue(ex.LicType);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         CExternalID.GetItemKey(elem, ex.LicID);
         root.Element("Licenses").Add(elem);
         //-----
         dictLic.Add(ptrStr, ex.LicID);
         //-----
         CLicenseData.WriteNewLicData(root, ex.LicID, "", ex.Standard);
         return ex.LicID;
      }
      //======================================================
      private static int countStnLic = 0;
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новой лицензии в станции. (StnLicenses)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="licID">ID лицензии</param>
      /// <param name="stationID">ID станции</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewStnLicence(XElement root, int licID, int stationID, int ownerStationId)
      {
         int newLicID = WriteNewLicence(root, licID, ownerStationId);
         if (stationID != IM.NullI)
         {
            countStnLic++;
            //----
            XElement elem = new XElement("STN_LIC");
            XElement temp = new XElement("ID");
            temp.SetValue(countStnLic);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("STN_ID");
            temp.SetValue(stationID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("LIC_ID");
            temp.SetValue(newLicID);
            elem.Add(temp);
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, countStnLic);
            root.Element("StnLicenses").Add(elem);
            return countStnLic;
         }
         return IM.NullI;
      }
   }
}
