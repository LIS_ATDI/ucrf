﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.Monitoring
{
   /// <summary>
   /// Группа пользователей
   /// </summary>
   public enum EEntrpGroupType
   {
      /// <summary>
      /// Лицензиат
      /// </summary>
      LIC,
      /// <summary>
      /// Технологический
      /// </summary>
      TECH,
      /// <summary>
      /// ТРК
      /// </summary>
      TRK,
      /// <summary>
      /// Распространение телерадиопрограмм
      /// </summary>
      DTRP
   }
}
