﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.Net;

namespace XICSM.UcrfRfaNET.Monitoring
{
    public class CNet
    {
        public static void Clear()
        {
            cache.Clear();
            curId = 0;
        }
        //======================================================
        private static Dictionary<string, int> cache = new Dictionary<string, int>();
        private static int curId = 0;

        public static int AddItemToXml(XElement root, int id)
        {
            string ptrStr = string.Format("{0}_{1}", "NET", id);
            if (cache.ContainsKey(ptrStr) == true)
                return cache[ptrStr];

            Net.NetObject net = new Net.NetObject();
            net.Load(id);

            XElement elem = new XElement("NET");
            XElement temp = new XElement("ID");
            temp.SetValue(++curId);
            elem.Add(temp);
            ///////////////////////////////////////////
            if (net.OwnerId != IM.NullI)
            {
                int docOwnerId = CEnterprise.WriteEnterprise(root, net.OwnerId);
                if (docOwnerId != IM.NullI)
                {
                    temp = new XElement("OWNER_ID");
                    temp.SetValue(docOwnerId.ToString());
                    elem.Add(temp);
                }
            }
            //else
            //    return IM.NullI; // владелец обязателен
            ///////////////////////////////////////////
            temp = new XElement("NAME");
            temp.SetValue(net.Name.Cut(256).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("TYPE");
            NetStationItem[] stations = net.BaseStations;
            bool hasBases = false;
            bool atLeastOneBaseHasNoLicence = false;
            foreach (NetStationItem station in stations)
            {
                if (station.StationType == NetStationItem.EnumStationType.Base)
                {
                    hasBases = true;
                    if (station.IsLicence == false)
                        atLeastOneBaseHasNoLicence = true;
                }
            }
            temp.SetValue(hasBases && !atLeastOneBaseHasNoLicence ? "LIC" : "TECH");
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("SYSTEM_ID");
            temp.SetValue("1");
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NICK_PHONE");
            temp.SetValue(net.CallSign.Cut(32).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            /*
            "BASE_CNT"
            "FIXED_CNT"
            "MOBILE_CNT"
            "MOVABLE_CNT"
            */
            /*TODO: frequencies?*/

            CExternalID.GetItemKey(elem, id);
            root.Element("Nets").Add(elem);
            //-----
            cache.Add(ptrStr, curId);
            
            return curId;
        }
    }
}
