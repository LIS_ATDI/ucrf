﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.Monitoring
{
   static class CMonitoringExtension
   {
      //------
      private static Dictionary<string, EStatusStation> _statusDictionary = null;
      public static Dictionary<string, EStatusStation> StatusDictionary
      {
         get
         {
            if(_statusDictionary == null)
               _statusDictionary = new Dictionary<string, EStatusStation>();
            return _statusDictionary;
         }
      }
      //===================================================
      /// <summary>
      /// Статический конструктор
      /// </summary>
      static CMonitoringExtension()
      {
         //#3748
         StatusDictionary.Add("A", EStatusStation.ssRequest);//заявка
         StatusDictionary.Add("AA", EStatusStation.ssRequest);//заявка
         StatusDictionary.Add("L", EStatusStation.ssTest);//Тестовая
         StatusDictionary.Add("M", EStatusStation.ssOperative);//заключение ЭМС
         StatusDictionary.Add("P", EStatusStation.ssJob);//рабочая
         StatusDictionary.Add("PP", EStatusStation.ssJob);//рабочая
         StatusDictionary.Add("N", EStatusStation.ssArchive);//
         StatusDictionary.Add("R", EStatusStation.ssArchive);//
         StatusDictionary.Add("RR", EStatusStation.ssArchive);//
         StatusDictionary.Add("X", EStatusStation.ssArchive);//
         StatusDictionary.Add("ZIP", EStatusStation.ssArchive);//
      }
      //===================================================
      /// <summary>
      /// Converts the station's status to the monitoring status
      /// </summary>
      /// <param name="value">a station's status</param>
      /// <returns>a monitoring's status</returns>
      public static int ToMStatus(this string value)
      {
         EStatusStation retVal = EStatusStation.ssUnknown;
         if (StatusDictionary.ContainsKey(value))
            retVal = StatusDictionary[value];
         return (int)retVal;
      }
      //===================================================
      /// <summary>
      /// Converts the date to the monitoring date
      /// </summary>
      /// <param name="value">a station status</param>
      /// <returns>a monitoring status</returns>
      public static string ToMDateTime(this DateTime value)
      {
         return value.ToString("yyyy-MM-dd"); //Unknown
      }
      //===================================================
      /// <summary>
      /// Cuts a string
      /// </summary>
      /// <param name="value">a string</param>
      /// <param name="maxLenght">a maximum lenght of the string</param>
      /// <returns>A cuted string</returns>
      public static string Cut(this string value, int maxLenght)
      {
         if (value.Length <= maxLenght)
            return value;
         return value.Remove(maxLenght);
      }
   }
}
