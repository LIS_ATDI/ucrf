﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace XICSM.UcrfRfaNET.Monitoring
{
   internal static class CComSystem
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         LoadedComSystems.Clear();
         count = 0;
      }
      //======================================================
      private static readonly Dictionary<string, int> LoadedComSystems = new Dictionary<string, int>();
      private static int count = 0;
      //===================================================
      /// <summary>
      /// Возвращает ID системы (стандарт)
      /// </summary>
      /// <param name="system">Название системы</param>
      /// <returns>ID системы</returns>
      public static int GetComSystemID(string comSystem, XElement root)
      {
         if (LoadedComSystems.ContainsKey(comSystem))
            return LoadedComSystems[comSystem];
         //--------------
         // Созадем новую систему
         XElement systemEl = new XElement("COM_SYSTEM");
         //----
         XElement temp = new XElement("ID");
         temp.SetValue(++count);
         systemEl.Add(temp);
         //----
         temp = new XElement("NAME");
         temp.SetValue(comSystem.ToStrXMLMonitoring());
         systemEl.Add(temp);
         //----
         int glblSystemID = CExternalID.GetComSystemExtID(comSystem);
         CExternalID.GetItemKey(systemEl, glblSystemID);
         XElement Systems = root.Element("CommunicationSystems");
         Systems.Add(systemEl);
         //----
         LoadedComSystems[comSystem] = count;
         return count;
      }

   }
}
