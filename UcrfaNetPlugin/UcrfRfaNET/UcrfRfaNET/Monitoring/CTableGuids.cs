﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CTableGuids
   {
      //===================================================
      /// <summary>
      /// Создает TableGuids.TABLE_GUID в XML
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="name">поле TableGuids.TABLE_GUID.TABLE</param>
      /// <param name="descr">поле TableGuids.TABLE_GUID.GUID</param>
      public static void WriteTableGuid(XElement root, string table, string guid)
      {
         //----
         //Создаем новую запись
         XElement elem = new XElement("TABLE_GUID");
         ///////////////////////////////////////////
         XElement temp = new XElement("TABLE");
         temp.SetValue(table);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("GUID");
         temp.SetValue(guid);
         elem.Add(temp);
         root.Element("TableGUIDs").Add(elem);
      }
   }
}
