﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CPermStnClass
   {
      public class PermStnClassData
      {
         public string Name { get; set; }
         //================================
         public PermStnClassData()
         {
            Name = "";
         }
      }
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictPrmStnClass.Clear();
      }
      //======================================================
      private static Dictionary<int, int> dictPrmStnClass = new Dictionary<int, int>();
      //private static int permStnClassID = 0;
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новой лицензии. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="radioTech">имя радиотехнологии</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewPrmStnClass(XElement root, PermStnClassData exData)
      {
         using (LisProgressBar pb = new LisProgressBar("Permition station class"))
         {
            int retVal = IM.NullI;
            if (string.IsNullOrEmpty(exData.Name))
               return retVal;
            //-----
            int idPermStnClass = CExternalID.GetPrmStnClassID(exData.Name);
            pb.SetSmall(exData.Name);
            if (dictPrmStnClass.ContainsKey(idPermStnClass) == true)
               return dictPrmStnClass[idPermStnClass];
            //----
            XElement elem = new XElement("PERM_STN_CLASS");
            XElement temp = new XElement("ID");
            temp.SetValue(idPermStnClass);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NAME");
            temp.SetValue(exData.Name.Cut(128).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, idPermStnClass);
            root.Element("PermStnClasses").Add(elem);
            //-----
            dictPrmStnClass.Add(idPermStnClass, idPermStnClass);
            return idPermStnClass;
         }
      }
   }
}
