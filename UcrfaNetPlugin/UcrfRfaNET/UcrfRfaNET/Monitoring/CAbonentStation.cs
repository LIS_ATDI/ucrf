﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.ApplSource.Classes;

using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Monitoring
{
    public class CAbonentStation : BaseStationXML
    {
        int Id = IM.NullI;
        int OwnerID = IM.NullI;
        string Name;
        string CallSign;
        int NetId = IM.NullI;
        int EqpId = IM.NullI;
        string Status;
        string Standard;
        double LongitudeDEC = IM.NullD;
        double LatitudeDEC = IM.NullD;
        double Altitude = IM.NullD; // AGL
        RecordPtr Position;
        string Location;
        string RegionOfUse;
        Power PwrAnt; //dBW
        AbonentStation.TypeExplAbonent ExplType;
        string DesigEmission;
        string FactoryNum;
        string PermNum;
        DateTime EndOfUse = IM.NullT;
        double Bw = IM.NullD;
        string BlankNum;
        List<CStationFreqsXML.DataFreq> freqList = new List<CStationFreqsXML.DataFreq>();
        EEntrpGroupType OwnerType = EEntrpGroupType.TECH;
        string UserName;
        DateTime? OpenDate;
        string BaseStationTable;
        string Table;
        List<int> BaseStationIds = new List<int>();

      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CAbonentStation(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
          IMRecordset objData = new IMRecordset(rec.Table, IMRecordset.Mode.ReadOnly);
          try
          {
              objData.Select("ID,USER_ID,POS_ID,CALL_SIGN,STATUS,STANDARD,AGL,NET_ID,BIUSE_DATE,"+
                  "EXPL_TYPE,LOCATION,USE_REGION,EQP_ID,PWR_ANT,Equipment.DESIG_EMISSION,FACTORY_NUM,PERM_NUM,EOUSE_DATE,BW,BLANK_NUM," +
                  "Position.LONGITUDE,Position.LATITUDE,Abonent.NAME,Position.PROVINCE,Position.CITY_ID,Position.ADDRESS");
              if (rec.Table==PlugTbl.XfaAbonentStation) objData.Select("CALL");
              objData.SetWhere("ID", IMRecordset.Operation.Eq, rec.Id);
              objData.Open();
              if (objData.IsEOF())
              {
                  //TODO: log error
              }
              else
              {
                  Id = rec.Id;
                  Table = rec.Table;
                  OwnerID = objData.GetI("USER_ID");
                  NetId = objData.GetI("NET_ID");
                  EqpId = objData.GetI("EQP_ID");
                  Name = objData.GetS("CALL_SIGN");
                  CallSign = objData.GetS("CALL_SIGN");
                  Status = objData.GetS("STATUS").Trim();
                  Standard = objData.GetS("STANDARD").Trim();
                  LongitudeDEC = objData.GetD("Position.LONGITUDE");
                  LatitudeDEC = objData.GetD("Position.LATITUDE");
                  Altitude = objData.GetD("AGL"); // AGL
                  //string StationClass = objData.Get("");
                  //string FREQ_BAND_WIDTH = objData.Get("");
                  Position = new RecordPtr(PlugTbl.XfaPosition, objData.GetI("POS_ID"));
                  PwrAnt = new Power();
                  PwrAnt[PowerUnits.dBW] = objData.GetD("PWR_ANT");
                  try
                  {
                      ExplType = (AbonentStation.TypeExplAbonent)Enum.Parse(typeof(AbonentStation.TypeExplAbonent), objData.GetS("EXPL_TYPE"));
                  } catch {}

                  if ((objData.GetS("EXPL_TYPE").Trim()=="0") && (rec.Table==PlugTbl.XfaAbonentStation))
                  { CallSign = objData.GetS("CALL"); Name = objData.GetS("CALL"); }

                  Location = objData.GetS("LOCATION");
                  RegionOfUse = objData.GetS("USE_REGION");
                  DesigEmission = objData.GetS("Equipment.DESIG_EMISSION");
                  FactoryNum = objData.GetS("FACTORY_NUM");
                  PermNum = objData.GetS("PERM_NUM");
                  EndOfUse = objData.GetT("EOUSE_DATE");
                  Bw = objData.GetD("BW");
                  BlankNum = objData.GetS("BLANK_NUM");

                  UserName = objData.GetS("Abonent.NAME");
                  OpenDate = objData.GetT("BIUSE_DATE");
                  
                  // frequencies
                  IMRecordset rsFreqs = new IMRecordset(PlugTbl.XfaFreq, IMRecordset.Mode.ReadOnly);
                  try
                  {
                      int Count_Rec = 0; int Temp_Count_Rec = 0;
                      double temp_txFreqFrom = 0;
                      double temp_rxFreqFrom = 0;
                      IM.ExecuteScalar(ref Count_Rec, "SELECT COUNT(*) from %XFA_FREQ where STA_ID=" + rec.Id);

                      rsFreqs.Select("TX_FREQ,RX_FREQ,TX_CHANNEL,RX_CHANNEL");
                      rsFreqs.SetWhere("STA_ID", IMRecordset.Operation.Eq, rec.Id);
                      rsFreqs.OrderBy("TX_FREQ", OrderDirection.Ascending);


                      for (rsFreqs.Open(); !rsFreqs.IsEOF(); rsFreqs.MoveNext())
                      {
                          Temp_Count_Rec++;
                          double txFreq = rsFreqs.GetD("TX_FREQ");
                          double rxFreq = rsFreqs.GetD("RX_FREQ");


                          if ((Count_Rec == 2) && (Standard == XICSM.UcrfRfaNET.CRadioTech.BAUR))
                          {

                              if (Temp_Count_Rec == 1)
                              {
                                  temp_rxFreqFrom = rxFreq;
                                  temp_txFreqFrom = txFreq;
                              }

                              if (Temp_Count_Rec == 2)
                              {
                                  freqList.Add(new CStationFreqsXML.DataFreq());
                                  freqList[freqList.Count - 1].FreqRxFromMhz = temp_rxFreqFrom;
                                  freqList[freqList.Count - 1].FreqRxToMhz = rxFreq;
                                  freqList.Add(new CStationFreqsXML.DataFreq());
                                  freqList[freqList.Count - 1].FreqTxFromMhz = temp_txFreqFrom;
                                  freqList[freqList.Count - 1].FreqTxToMhz = txFreq;
                              }

                          }
                          else
                          {
                              freqList.Add(new CStationFreqsXML.DataFreq());
                              {
                                  freqList[freqList.Count - 1].FreqTxFromMhz = txFreq;
                                  freqList[freqList.Count - 1].FreqRxFromMhz = rxFreq;
                              }
                          }

                      }
                  }
                  finally
                  {
                      rsFreqs.Destroy();
                  }

                  Dictionary<string, HashSet<int>> ids = new Dictionary<string, HashSet<int>>();
                  IMRecordset rsBaseSta = new IMRecordset(PlugTbl.RefBaseStation, IMRecordset.Mode.ReadOnly);
                  try
                  {
                      rsBaseSta.Select("STATION_ID,STATION_TABLE");
                      rsBaseSta.SetWhere("NET_ID", IMRecordset.Operation.Eq, NetId);
                      for (rsBaseSta.Open(); !rsBaseSta.IsEOF(); rsBaseSta.MoveNext())
                      {
                          string tn = rsBaseSta.GetS("STATION_TABLE");
                          int id = rsBaseSta.GetI("STATION_ID");
                          if (id != IM.NullI)
                          {
                              if (!ids.ContainsKey(tn))
                                  ids.Add(tn, new HashSet<int>());
                              ids[tn].Add(id);
                          }
                      }
                  }
                  catch
                  {
                      rsBaseSta.Destroy();
                  }

                  //base stations, if present
                  string whereClause = "";
                  foreach (KeyValuePair<string, HashSet<int>> p in ids)
                  {
                      string idlist = "";
                      foreach (int id in p.Value)
                          idlist += (idlist.Length > 0 ? "," : "") + id.ToString();
                      if (idlist.Length > 0 && p.Key.Length > 0)
                          whereClause += 
                              (whereClause.Length > 0 ? " or " : "") +
                              "(" +
                              "(OBJ_ID1 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              "or (OBJ_ID2 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              "or (OBJ_ID3 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              "or (OBJ_ID4 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              "or (OBJ_ID5 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              "or (OBJ_ID6 in (" + idlist + ") and [OBJ_TABLE] = '" + p.Key + "')" +
                              ")";
                  }

                  if (whereClause.Length > 0)
                  {
                      IMRecordset rsAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                      try
                      {
                          rsAppl.Select("ID,OBJ_TABLE,OBJ_ID1");
                          string bsIdListQry = "select STATION_ID from %" + PlugTbl.RefBaseStation + " where NET_ID = " + NetId.ToString() + " and STATION_TABLE = [OBJ_TABLE]";
                          rsAppl.SetAdditional(
                              whereClause
                              /*
                              "OBJ_ID1 in (" + bsIdListQry + ")" +
                              " or OBJ_ID2 in (" + bsIdListQry + ")" +
                              " or OBJ_ID3 in (" + bsIdListQry + ")" +
                              " or OBJ_ID4 in (" + bsIdListQry + ")" +
                              " or OBJ_ID5 in (" + bsIdListQry + ")" +
                              " or OBJ_ID6 in (" + bsIdListQry + ")"
                               * */
                              );
                          for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                          {
                              int licCnt = 0;
                              IM.ExecuteScalar(ref licCnt, "select count(*) from %" + PlugTbl.itblXnrfaApplLic + " al where al.APPL_ID=" + rsAppl.GetI("ID"));
                              if (licCnt > 0)
                                  OwnerType = EEntrpGroupType.LIC;
                              BaseStationTable = rsAppl.GetS("OBJ_TABLE");
                              BaseStationIds.Add(rsAppl.GetI("OBJ_ID1"));
                          }
                      }
                      finally
                      {
                          rsAppl.Destroy();
                      }

                      /*
                      IMRecordset rsLicences = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadOnly);
                      try
                      {
                          rsLicences.Select("Licence.NAME,Licence.START_DATE,Licence.STOP_DATE,Licence.END_DATE");
                          rsLicences.SetAdditional("APPL_ID in (select a.ID from %" + PlugTbl.APPL + " a, %" + PlugTbl.RefBaseStation + " r" +
                              " where r.NET_ID = " + NetId.ToString() + " and r.STATION_TABLE = a.OBJ_TABLE" +
                              " and (r.STATION_ID=a.OBJ_ID1 or r.STATION_ID=a.OBJ_ID2 or r.STATION_ID=a.OBJ_ID3 or r.STATION_ID=a.OBJ_ID4 or r.STATION_ID=a.OBJ_ID5 or r.STATION_ID=a.OBJ_ID6))");
                          //for (rsLicences.Open(); !rsLicences.IsEOF(); rsLicences.MoveNext())
                          rsLicences.Open(); if (!rsLicences.IsEOF())
                          {
                              OwnerType = EEntrpGroupType.LIC;
                          }
                      }
                      finally
                      {
                          rsLicences.Destroy();
                      }
                           * */
                  }
              }
          }
          finally
          {
              objData.Destroy();
          }
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      protected override int getOwnerID()
      {
          return CEnterprise.WriteEnterprise(_root, OwnerID);
      }
      protected override string getName()
      {
          return Name;
      }
      protected override string getCallSign()
      {
          return CallSign;
      }
      protected override string getStatus()
      {
          return Status;
      }
      protected override int getSystemID()
      {
          return CSystemsXML.GetSystemID(Standard, _root);
      }
      protected override int getTechID()
      {
          return CRadioTech.WriteNewRadioTech(_root, Standard);
      }
      protected override double getLongitudeDEC()
      {
          return LongitudeDEC;
      }
      protected override double getLatitudeDEC()
      {
          return LatitudeDEC;
      }
      protected override double getAltitude()
      {
          return Altitude;
      }
      protected override RecordPtr getPosition()
      {
          return Position;
      }
      protected override string getStationType()
      {
          switch (ExplType)
          {
              case AbonentStation.TypeExplAbonent.Stationary: return "8";
              case AbonentStation.TypeExplAbonent.Portable: return "2";
              case AbonentStation.TypeExplAbonent.Movable: return "1";
              default: return "";
          }
      }
      protected override string getFREQ_BAND_WIDTH() { return ""; }
      protected override string GetPermStnClassName()
      {
          switch (ExplType)
          {
              case AbonentStation.TypeExplAbonent.Stationary: return "Абонентська";
              case AbonentStation.TypeExplAbonent.Portable: return "Ношена";
              case AbonentStation.TypeExplAbonent.Movable: return "Мобільна";
              default: return "";
          }
      }

      protected override int GetNetId()
      {
          return NetId != IM.NullI ? CNet.AddItemToXml(_root, NetId) : IM.NullI;
      }
      protected override string GetCarRegNum() { return ExplType == AbonentStation.TypeExplAbonent.Movable ? Location : ""; }
      protected override EEntrpGroupType GetEntrpGroupType()
      {
          return OwnerType;
      }

      protected override string GetUserName()
      {
          return UserName;
      }

      protected override DateTime? GetOpenDate()
      {
          return OpenDate;
      }

      protected override string GetRegionOfUse()
      {
          return RegionOfUse;
      }

      public override void Export()
      {
          base.Export();

          //linkage to base stations, if any
          int id = 0; string BranchCode = ""; string NameFilial = "";
          CAbonentOwner own = new CAbonentOwner();
          RailwayApplStation rw = new RailwayApplStation();
          try
          {
              if (Table == PlugTbl.ABONENT_OWNER) { own.Read(Id); BranchCode = own.zsBranchCode; }
              if (Table == PlugTbl.XfaAbonentStation) { rw.Load(Id); BranchCode = rw.BrunchCode; }
          }
          catch (Exception) { }

          foreach (int bsId in BaseStationIds)
          {
              XElement bsLink = new XElement("STN_BASE");
              AddElement(bsLink, "ID", ++id);
              AddElement(bsLink, "ABONENT_ID", GlobalStationID);
              AddElement(bsLink, "BASE_STN_ID", CExternalID.GetRecordPtrExtID(new RecordPtr(BaseStationTable, bsId)));
              AddElement(bsLink, "BASE_STN_SYS_ID", CDBGuids.ICSM_DBGuid);
              _root.Element("StnBases").Add(bsLink);
          }



          int ID_OWNER = IM.NullI;
          string address="";
          IMRecordset r = new IMRecordset(ICSMTbl.itblUsers, IMRecordset.Mode.ReadOnly);
          r.Select("ID,NAME,REGIST_NUM, REPR_ROLE,REPR_TITLE, POSTCODE, ADDRESS, VISITORS_ADDRESS, City.COUNTRY,City.PROVINCE,City.SUBPROVINCE,City.NAME");
          r.SetWhere("REGIST_NUM", IMRecordset.Operation.Like, "*/" + BranchCode+"*");
          try
          {
              r.Open();
              if (!r.IsEOF())
              {
                  NameFilial = r.GetS("NAME");
                  ID_OWNER = r.GetI("ID");
                  address = string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}", r.GetS("REPR_ROLE")!="" ? r.GetS("REPR_ROLE")+", ":"", r.GetS("REPR_TITLE")!="" ? r.GetS("REPR_TITLE")+", ": "", r.GetS("POSTCODE")!="" ? r.GetS("POSTCODE")+", ":"", r.GetS("ADDRESS")!=""?r.GetS("ADDRESS")+", ":"", r.GetS("VISITORS_ADDRESS")!=""?r.GetS("VISITORS_ADDRESS")+", ":"", r.GetS("City.COUNTRY")!=""?r.GetS("City.COUNTRY")+", ":"", r.GetS("City.PROVINCE")!="" ? r.GetS("City.PROVINCE")+"обл., ":"", r.GetS("City.SUBPROVINCE")!="" ? r.GetS("City.SUBPROVINCE")+"р-н, ": "", r.GetS("City.NAME")!="" ? r.GetS("City.NAME"):"");
              }
          }
          catch (Exception ex)
          {

          }
          finally
          {
              r.Close();
              r.Destroy();
          }
        

          IMObject rsOwner = null;
          try
          {

              if (!string.IsNullOrEmpty(BranchCode))
              {
              rsOwner = IMObject.LoadFromDB(ICSMTbl.USERS, ID_OWNER);
              //if (((rsOwner.GetS("NAME").Contains("Українська залізниця")) || (rsOwner.GetS("NAME").Contains("Укрзалізниця"))) && ((Standard == "УКХ") || (Standard == "КХ")))
              if ((Standard == "УКХ") || (Standard == "КХ"))
              {
                  int aDBGUID1 = CDBGuids.WriteDBGuid(_root, "{1D2A3F30-A982-4253-B2D6-7D5E31417C6E}", "", "");
                  int aDBGUID2 = CDBGuids.WriteDBGuid(_root, "{123A8742-2638-401A-B198-5E66A8635D75}", "", "");
                  int aDBGUID3 = CDBGuids.WriteDBGuid(_root, "{70C57668-A833-4A7E-A5B1-D7CF19677724}", "", "");
                  int aDBGUID4 = CDBGuids.WriteDBGuid(_root, "{F53D6425-A438-47F1-B171-09709EE0D613}", "", "");

                  int propertyID3 = CProperties.AddProperty(_root, aDBGUID2 != IM.NullI ? aDBGUID2 : IM.NullI, CProperties.PropertyType.ptInExtUpd);
                  int propertyID4 = CProperties.AddProperty(_root, aDBGUID3 != IM.NullI ? aDBGUID3 : IM.NullI, CProperties.PropertyType.ptInExtUpd);
                  int propertyID5 = CProperties.AddProperty(_root, aDBGUID4 != IM.NullI ? aDBGUID4 : IM.NullI, CProperties.PropertyType.ptInExtUpd);
                  //for (int k = 0; k < 2; k++) {  CPropertyValues.AddPropertyValue2(_root, propertyID3, propertyID4, globalStationID, aDBGUID1, getOwnerID(), rsOwner.GetS("NAME"), IM.NullI, k);  }
                  for (int k = 0; k < 3; k++) { CPropertyValues.AddPropertyValue2(_root, propertyID3, propertyID4, propertyID5, globalStationID, aDBGUID1, BranchCode, NameFilial, address, IM.NullI, k); }
              }
                  
              }
          }
          catch
          {
              rsOwner = null;
              //CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't load USERS ID={0}"), ID_OWNER));
          }



          int sectorID = CSectors.WriteSector(_root, globalStationID, DesigEmission, 0, false);
          int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(PlugTbl.XfaEquip, EqpId));
          CTransmitter.TTransmitterData tData = new CTransmitter.TTransmitterData();
          tData.FactoryNo = FactoryNum;

          CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), sectorID, IM.NullI, txEquipID, PwrAnt[PowerUnits.dBm], IM.NullD, Altitude, "", IM.NullD,
                                               IM.NullI, IM.NullD, IM.NullI, "", IM.NullD, null, tData);
          
          foreach (var dataFreq in freqList)
          {
              CStationFreqsXML.WriteStationFreq(_root, sectorID, PlugTbl.XfaFreq, dataFreq);
          }
      }
   }
}
