﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Monitoring
{
    public class CAmateur : BaseStationXML
    {
        int Id = IM.NullI;
        int OwnerID = IM.NullI;
        string Name = "";
        string CallSign = "";
        string Status = "";
        double LongitudeDEC = IM.NullD;
        double LatitudeDEC = IM.NullD;
        double Altitude = IM.NullD; // AGL
        RecordPtr Position;
        string Category;
        //private string Area;
        //private int CityId = IM.NullI;
        //private string Street;
        //private string Bldng;
        string IsShared;

      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CAmateur(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
          Id = IM.NullI;
          IMRecordset objData = new IMRecordset(rec.Table, IMRecordset.Mode.ReadOnly);
          try
          {
              objData.Select("ID,USER_ID,POS_ID,OWNER_ID,CALL_SIGN,STATUS,AGL,CATEG_OPER,APC," +
                  "Position.LONGITUDE,Position.LATITUDE,Position.PROVINCE,Position.CITY_ID,Position.CUST_TXT7,Position.CUST_TXT8,Position.ADDRESS");
              objData.SetWhere("ID", IMRecordset.Operation.Eq, rec.Id);
              objData.Open();
              if (objData.IsEOF())
              {
                  //TODO: log error
              }
              else
              {
                  Id = rec.Id;
                  OwnerID = objData.GetI("USER_ID");
                  Name = objData.GetS("CALL_SIGN");
                  CallSign = objData.GetS("CALL_SIGN");
                  Status = objData.GetS("STATUS").Trim();
                  LongitudeDEC = objData.GetD("Position.LONGITUDE");
                  LatitudeDEC = objData.GetD("Position.LATITUDE");
                  Altitude = objData.GetD("AGL"); // AGL
                  Position = new RecordPtr(PlugTbl.XfaPosition, objData.GetI("POS_ID"));
                  Category = objData.GetS("CATEG_OPER");
                  IsShared = objData.GetS("APC");
              }
          }
          finally
          {
              objData.Destroy();
          }
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
          if (Id == IM.NullI)
              return; //Прекращаем обработку, та как не загрузили станцию
          //-------------
          // секция <STATION>
          base.Export();

          //equipment
          string equListIdList = "";
          Dictionary<int, int> sectorIDs = new Dictionary<int, int>();

          IMRecordset rsEquList = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly);
          try
          {
              rsEquList.Select("ID,STA_ID,EQUIP_ID,NUMB,PLANT_NUMB,POWER,DEVI,Equipment.DESIG_EMISSION");
              rsEquList.SetWhere("STA_ID", IMRecordset.Operation.Eq, Id);
              int i = 0;
              for (rsEquList.Open(); !rsEquList.IsEOF() && i < 6; rsEquList.MoveNext(), i++)
              {
                  // new sector, corresponding transmitter and its equipment
                  int equListId = rsEquList.GetI("ID");
                  int sectorID = CSectors.WriteSector(_root, globalStationID, rsEquList.GetS("Equipment.DESIG_EMISSION"), i, false);
                  
                  equListIdList += (equListIdList.Length > 0 ? "," : "") + equListId.ToString();
                  sectorIDs.Add(equListId, sectorID);

                  int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(PlugTbl.XfaEquipAmateur, rsEquList.GetI("EQUIP_ID")));
                  
                  CTransmitter.TTransmitterData txData = new CTransmitter.TTransmitterData();
                  txData.FactoryNo = rsEquList.GetS("PLANT_NUMB");
                  Power pwr = new Power();
                  pwr[PowerUnits.W] = rsEquList.GetD("POWER");

                  
                  int txId = CTransmitter.WriteNewTransmitter(
                      _root, new RecordPtr(PlugTbl.XfaEsAmateur, equListId), sectorID, IM.NullI, 
                      txEquipID, pwr[PowerUnits.dBm], IM.NullD, Altitude, "", IM.NullD, IM.NullI, IM.NullD, IM.NullI, "", IM.NullD, null, txData);

              }
          }
          finally
          {
              rsEquList.Destroy();
          }

          if (equListIdList.Length > 0)
          {
              // frequency blocks

              IMRecordset rsFreqBlocks = new IMRecordset(PlugTbl.XfaFeAmateur, IMRecordset.Mode.ReadOnly);
              try
              {
                  rsFreqBlocks.Select("SEL_EQUIP_ID,Bandwidth.BANDMIN,Bandwidth.BANDMAX");
                  rsFreqBlocks.SetAdditional("SEL_EQUIP_ID in (" + equListIdList + ")");
                  rsFreqBlocks.OrderBy("Bandwidth.BANDMIN", OrderDirection.Ascending);
                  rsFreqBlocks.OrderBy("SEL_EQUIP_ID", OrderDirection.Ascending);
                  for (rsFreqBlocks.Open(); !rsFreqBlocks.IsEOF(); rsFreqBlocks.MoveNext())
                  {
                      double freqMin = rsFreqBlocks.GetD("Bandwidth.BANDMIN");
                      double freqMax = rsFreqBlocks.GetD("Bandwidth.BANDMAX");
                      CStationFreqsXML.DataFreq dataFreq = new CStationFreqsXML.DataFreq();
                      {
                          dataFreq.FreqTxFromMhz = freqMin;
                          dataFreq.FreqRxFromMhz = freqMin;
                          dataFreq.FreqTxToMhz = freqMax;
                          dataFreq.FreqRxToMhz = freqMax;
                      }
                      CStationFreqsXML.WriteStationFreq(_root, sectorIDs[rsFreqBlocks.GetI("SEL_EQUIP_ID")], PlugTbl.XfaFeAmateur, dataFreq);
                  }
              }
              finally
              {
                  rsFreqBlocks.Destroy();
              }

              // frequencies
              IMRecordset rsFreqs = new IMRecordset(PlugTbl.XfaFreqAmateur, IMRecordset.Mode.ReadOnly);
              try
              {
                  rsFreqs.Select("FREQ_ID,TX_FREQ,RX_FREQ,CHANNEL");
                  rsFreqs.SetAdditional("FREQ_ID in (" + equListIdList + ")");
                  rsFreqs.OrderBy("TX_FREQ", OrderDirection.Ascending);
                  rsFreqs.OrderBy("FREQ_ID", OrderDirection.Ascending);
                  for (rsFreqs.Open(); !rsFreqs.IsEOF(); rsFreqs.MoveNext())
                  {
                      double txFreq = rsFreqs.GetD("TX_FREQ");
                      double rxFreq = rsFreqs.GetD("RX_FREQ");
                      CStationFreqsXML.DataFreq dataFreq = new CStationFreqsXML.DataFreq();
                      {//каналы не используем
                          dataFreq.FreqTxFromMhz = txFreq;
                          dataFreq.FreqRxFromMhz = rxFreq;
                      }
                      CStationFreqsXML.WriteStationFreq(_root, sectorIDs[rsFreqs.GetI("FREQ_ID")], PlugTbl.XfaFreqAmateur, dataFreq);
                  }
              }
              finally
              {
                  rsFreqs.Destroy();
              }
          }

      }
      protected override int getOwnerID()
      {
          return CEnterprise.WriteEnterprise(_root, OwnerID);
      }
      protected override string getName()
      {
          return Name;
      }
      protected override string getCallSign()
      {
          return CallSign;
      }
      protected override string getStatus()
      {
          return Status;
      }
      protected override RecordPtr getPosition()
      {
          return Position;
      }
      protected override double getLongitudeDEC()
      {
          return LongitudeDEC;
      }
      protected override double getLatitudeDEC()
      {
          return LatitudeDEC;
      }
      protected override double getAltitude()
      {
          return Altitude;
      }
      protected override int getTechID()
      {
          return CRadioTech.WriteNewRadioTech(_root, "АР");
      }
      protected override int getSystemID()
      {
          return CSystemsXML.GetSystemID("АР", _root);
      }
      protected override bool? GetIsShared()
      {
          switch (IsShared) { case "I": return false; case "C": return true; default: return null; }
      }
      protected override int? GetCategory()
      {
          switch (Category) { case "1": return 1; case "2": return 2; case "3": return 3; case "4": return 4; default: return null; }
      }
      
    }
}
