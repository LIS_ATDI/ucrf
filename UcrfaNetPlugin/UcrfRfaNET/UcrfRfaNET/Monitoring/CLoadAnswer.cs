﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;
using Lis.CommonLib.Extensions; 

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CLoadAnswer
   {
      //===================================================
      /// <summary>
      /// Обрабатывает XML с ответом от Р-135
      /// </summary>
      /// <returns>TRUE-ответ обработан, иначе FALSE-ошибка, смотрите лог</returns>
      public static bool HandleAnswer(TextReader stream, string fileName)
      {
         bool retVal = false;
         //
         XDocument xmlDoc = XDocument.Load(stream);
         string error = "";
         if (CXMLTest.TestAllXmlToSchema(xmlDoc.ToString(), "Ticket.xsd", "http://www.radiosoft.com.ua/Ticket", ref error) == false)
         {
            throw new ApplicationException(string.Format(CLocaliz.TxT("Schema error: Can't load the XML document from the file \"{0}\" {1}"), fileName, error));
         }
         //------
         // Вытаскиваем ID ICSM
         int dbId = IM.NullI;
         {
            IEnumerable<XElement> dbGuids = from item in xmlDoc.Elements().Elements().Elements()
                                            where (item.Name.LocalName == "DBGUID") &&
                                                  (item.Value.Contains(CDBGuids.DbGuidIcsm))
                                            select item;
            foreach (XElement elem in dbGuids.Elements())
               if (elem.Name.LocalName == "ID")
               {
                  dbId = elem.Value.ToInt32(IM.NullI);
                  break;
               }
         }
         //------
         // Загружаем FileInfo
         IEnumerable<XElement> fileInfos = from item in xmlDoc.Elements().Elements().Elements()
                                           where item.Name.LocalName == "FILE_INFO"
                                           select item;
         //------
         // Загружаем Report
         IEnumerable<XElement> reports = from item in xmlDoc.Elements().Elements().Elements()
                                         where item.Name.LocalName == "REPORT"
                                         select item;

         foreach(XElement itemFileInfo in fileInfos)
         {
            string fileNameLocal = "";
            int docId = IM.NullI;
            CMonitoringAppl.StateXmlR136 state = CMonitoringAppl.StateXmlR136.Ok;
            foreach(XElement itenFileInfoElem in itemFileInfo.Elements())
            {
               switch (itenFileInfoElem.Name.LocalName)
               {
                  case "FILE_NAME":
                     fileNameLocal = itenFileInfoElem.Value;
                     break;
                  case "ID":
                     docId = itenFileInfoElem.Value.ToInt32(IM.NullI);
                     break;
                  case "IS_ERROR":
                     if (itenFileInfoElem.Value.ToUpper() == "TRUE")
                        state = CMonitoringAppl.StateXmlR136.ErAnswer;
                     break;
               }
            }
            //==========
            foreach(XElement report in reports)
            {
               int docIdReport = IM.NullI;
               string message = "";
               foreach (XElement itemReport in report.Elements())
               {
                  switch (itemReport.Name.LocalName)
                  {
                     case "DOC_ID":
                        docIdReport = itemReport.Value.ToInt32(IM.NullI);
                        break;
                     case "MESSAGE":
                        message = itemReport.Value;
                        break;
                  }
               }
               //====
               if(docId == docIdReport)
               {
                  if (CMonitoringAppl.ChangeStatusXmlR135ByFileName(fileNameLocal, state, message))
                     retVal = true;
                  break;
               }
            }
         }
         return retVal;
      }
   }
}
