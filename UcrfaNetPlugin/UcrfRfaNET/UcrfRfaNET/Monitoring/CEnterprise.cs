﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CEnterprise
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         lstOwner.Clear();
      }
      //======================================================
      private static List<int> lstOwner = new List<int>();
      //======================================================
      /// <summary>
      /// Сохраняем в XML файл данные о владельце
      /// </summary>
      /// <param name="ex">XElement</param>
      /// <param name="ownerID">ID владельца (USERS)</param>
      /// <returns>Возвращает глобальный ID владельца, иначе IM.NULLI</returns>
      public static int WriteEnterprise(XElement ex, int ownerID)
      {
         using (LisProgressBar pb = new LisProgressBar("Owner"))
         {
            pb.SetSmall(string.Format("{0}_{1}", ICSMTbl.USERS, ownerID));
            //-----
            if (lstOwner.Contains(ownerID) == true)
               return ownerID;
            //----
            IMObject rsOwner = null;
            try
            {
               rsOwner = IMObject.LoadFromDB(ICSMTbl.USERS, ownerID);
            }
            catch
            {
               rsOwner = null;
               CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't load USERS ID={0}"), ownerID));
            }
            if (rsOwner == null)
               return IM.NullI; //Дальше нечего делать
            //-----
            string tmpStr = "";
            XElement elEnterprise = new XElement("ENTERPRISE");
            /////////////////////////////////
            XElement temp = new XElement("ENTRPRID");
            temp.SetValue(ownerID);
            elEnterprise.Add(temp);
            /////////////////////////////////
            temp = new XElement("ENTERPRISE");
            temp.SetValue(rsOwner.GetS("NAME").Cut(256).ToStrXMLMonitoring());
            elEnterprise.Add(temp);
            /////////////////////////////////
            tmpStr = rsOwner.GetS("REGIST_NUM");
            if (!string.IsNullOrEmpty(tmpStr))
            {
               temp = new XElement("OKPO");
               temp.SetValue(tmpStr.Cut(16).ToStrXMLMonitoring());
               elEnterprise.Add(temp);
            }
            /////////////////////////////////
            temp = new XElement("SHORTNAME");
            temp.SetValue(rsOwner.GetS("NAME").Cut(256).ToStrXMLMonitoring());
            elEnterprise.Add(temp);
            /////////////////////////////////
            tmpStr = rsOwner.GetS("CODE");
            if (!string.IsNullOrEmpty(tmpStr))
            {
               temp = new XElement("CODE");
               temp.SetValue(tmpStr.Cut(16).ToStrXMLMonitoring());
               elEnterprise.Add(temp);
            }
            /////////////////////////////////
            int cityID;
            int cityTypeID;
            int subprovinceID;
            int provinceID;
            int countryID;
            CPositionXML.WritePositionUser(ex, new RecordPtr(ICSMTbl.USERS, ownerID), out cityID, out cityTypeID, out subprovinceID, out provinceID, out countryID);
            /////////////////////////////////
            if (provinceID != IM.NullI)
            {
               temp = new XElement("AREAID_JUR");
               temp.SetValue(provinceID);
               elEnterprise.Add(temp);
            }
            /////////////////////////////////
            tmpStr = rsOwner.GetS("SUBPROVINCE");
            if (!string.IsNullOrEmpty(tmpStr))
            {
               temp = new XElement("DISTRICTNAME_JUR");
               temp.SetValue(tmpStr);
               elEnterprise.Add(temp);
            }
            /////////////////////////////////
            if (cityID != IM.NullI)
            {
               temp = new XElement("CITYID_JUR");
               temp.SetValue(cityID);
               elEnterprise.Add(temp);
            }
            /////////////////////////////////
            tmpStr = rsOwner.GetS("POSTCODE");
            if (!string.IsNullOrEmpty(tmpStr))
            {
               temp = new XElement("ZIP_JUR");
               temp.SetValue(tmpStr);
               elEnterprise.Add(temp);
            }
            elEnterprise.Add(new XElement("ADDRESS_JUR", rsOwner.GetS("ADDRESS")));
            /////////////////////////////////
            CExternalID.GetItemKey(elEnterprise, ownerID);
            ex.Element("Enterprises").Add(elEnterprise);
            //---
            lstOwner.Add(ownerID);
            return ownerID;
         }
      }
   }
}
