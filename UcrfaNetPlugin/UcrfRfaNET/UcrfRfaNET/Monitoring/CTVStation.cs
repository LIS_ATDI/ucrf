﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CTVStation : BaseStationXML
   {
      //===================================================
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CTVStation(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if (stationObj == null)
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         //Заполняем сектор
         int sectorVideoID = CSectors.WriteSector(_root, globalStationID, stationObj.GetS("DESIG_EM_V"), false);
         int sectorAudioID = CSectors.WriteSector(_root, globalStationID, stationObj.GetS("DESIG_EM_S1"), true);
         //-------------
         //Заполняем частоты
         int channel = stationObj.GetS("CHANNEL").ToInt32(IM.NullI);
         double freq = stationObj.GetD("FREQ_V_CARR");
         int freqVideoID = IM.NullI;
         {
            freqVideoID = CStationFreqsXML.WriteStationFreq(_root, sectorVideoID, IM.NullD, freq, ICSMTbl.itblTV, channel, IM.NullI);
         }
         int freqAudioID = IM.NullI;
         {
            double freqtmp = IM.NullD;
            double delta = stationObj.GetD("DELTA_F_SND1");
            if ((delta != IM.NullD) && (freq != IM.NullD))
               freqtmp = freq + delta;
            freqAudioID = CStationFreqsXML.WriteStationFreq(_root, sectorAudioID, IM.NullD, freqtmp, ICSMTbl.itblTV, channel, IM.NullI);
         }
         //-----------
         //Заполняем антену
         int antennaID = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.ANTENNA_BRO, stationObj.GetI("ANT_ID")), stationObj.GetD("GAIN"), stationObj.GetS("POLARIZATION"));
         //-----------
         //Заполняем оборудование
         int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.EQUIP_BRO, stationObj.GetI("EQUIP_ID")));
         //-----------
         double powEffect = IM.NullD;
         {
            switch(stationObj.GetS("POLARIZATION"))
            {
               case "V":
                  //Диаграммы напавленности
                  CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGV"), stationObj.GetD("GAIN"));
                  powEffect = stationObj.GetD("ERP_V");
                  break;
               case "H":
                  //Диаграммы напавленности
                  CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGH"), stationObj.GetD("GAIN"));
                  powEffect = stationObj.GetD("ERP_H");
                  break;
               case "M":
                  {
                     //Диаграммы напавленности
                     CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGH"), stationObj.GetD("GAIN"));
                     double erpV = stationObj.GetD("ERP_V");
                     double erpH = stationObj.GetD("ERP_H");
                     if (erpH != IM.NullD && erpV != IM.NullD)
                        powEffect = 10.0 * Math.Log10(Math.Pow(10.0, erpV / 10.0) + Math.Pow(10.0, erpH / 10.0));
                  }
                  break;
            }
         }
         {//Заполняем передатчик Видео
            double pow = stationObj.GetD("PWR_ANT");
            if (pow != IM.NullD)
               pow += 30; //dBW -> dBm
            double azimuth = stationObj.GetD("AZIMUTH");
            double agl = stationObj.GetD("AGL");
            string polar = stationObj.GetS("POLARIZATION");
            double gain = stationObj.GetD("GAIN");
            double fiderLoss = stationObj.GetD("CUST_NBR2");
            int fiderLen = IM.NullI;
            double tmpFiderLen = stationObj.GetD("CUST_NBR1");
            if (tmpFiderLen != IM.NullD)
               fiderLen = (int)(tmpFiderLen * 100.0);
            CTransmitter.TTransmitterData exData = new CTransmitter.TTransmitterData();
            exData.AntheightEffect = stationObj.GetD("EFHGT_MAX");
            CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), sectorVideoID, antennaID, txEquipID, pow, azimuth, agl, polar, gain, fiderLen, fiderLoss, stationObj.GetI("ANT_ID"), ICSMTbl.ANTENNA_BRO, powEffect, stationObj.IsBroadcastDirection(), exData);
         }
         //-----------
         {//Заполняем передатчик Аудио
            double powSound = IM.NullD;
            double pow = stationObj.GetD("PWR_ANT"); //TODO необходима мощность аудио
            if (pow != IM.NullD)
            {
               pow = Math.Round(Math.Pow(10.0, pow/10), 3);
               double ratio = stationObj.GetD("PWR_RATIO_1");
               if (ratio != IM.NullD && ratio != 0.0)
               {
                  powSound = pow/ratio;
                  powSound = 10.0*Math.Log10(powSound) + 30;
               }
            }
            double azimuth = stationObj.GetD("AZIMUTH");
            double agl = stationObj.GetD("AGL");
            string polar = stationObj.GetS("POLARIZATION");
            double gain = stationObj.GetD("GAIN");
            double fiderLoss = stationObj.GetD("CUST_NBR2");
            int fiderLen = IM.NullI;
            double tmpFiderLen = stationObj.GetD("CUST_NBR1");
            if (tmpFiderLen != IM.NullD)
               fiderLen = (int)(tmpFiderLen * 100.0);
            CTransmitter.TTransmitterData exData = new CTransmitter.TTransmitterData();
            exData.AntheightEffect = stationObj.GetD("EFHGT_MAX");
            CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), sectorAudioID, antennaID, txEquipID, powSound, azimuth, agl, polar, gain, fiderLen, fiderLoss, stationObj.GetI("ANT_ID"), ICSMTbl.ANTENNA_BRO, powEffect, stationObj.IsBroadcastDirection(), exData);
         }
      }
      //===================================================
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = stationObj.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getStatus()
      {
         return stationObj.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         double asl = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,ASL");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               asl = rs.GetD("ASL");
         }
         finally
         {
            rs.Destroy();
         }
         return asl;
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(ICSMTbl.itblPositionBro, stationObj.GetI("SITE_ID"));
      }
      //--------
      /// <summary>
      /// Возвращает VideoColor для аналогового телевидиния
      /// </summary>
      protected override string getVideoColor()
      {
         string videoColor = stationObj.GetS("COLOUR_SYSTEM");
         //Вытаскиваем полное название
         string fileName = "EQUIP_COLORSYS";
         IMEriLOV eriData = new IMEriLOV(fileName);
         string freDescr = null, freBmp = null;
         eriData.GetEntry(videoColor, ref freDescr, ref freBmp);
         if (string.IsNullOrEmpty(freDescr))
               freDescr = "";
         return freDescr;
      }
      //--------
      /// <summary>
      /// Возвращает VideoSys для аналогового телевидиния
      /// </summary>
      protected override string getVideoSys()
      {
         return stationObj.GetS("TVSYS_CODE");
      }
      //--------
      /// <summary>
      /// Возвращает смещение несущей для аналогового телевидиния (KHz)
      /// </summary>
      protected override double getOffsetCarrier()
      {
         return stationObj.GetD("OFFSET_V_KHZ");
      }
      //-------
      /// <summary>
      /// Возвращает класс станции
      /// </summary>
      protected override string getStationClass()
      {
         return "ВТ";
      }
      //===================================================
      /// <summary>
      /// Возвращает FREQ_BAND_WIDTH
      /// </summary>
      protected override string getFREQ_BAND_WIDTH()
      {
         //double bw = stationObj.GetD("BW");
         //if (bw != IM.NullD)
         //   bw *= 100.0;
         //return (bw != IM.NullD) ? bw.Round(0).ToString() : "";
         return "8000000"; //#3793
      }
      //===================================================
      /// <summary>
      /// Возвращает группу пользователей
      /// </summary>
      protected override EEntrpGroupType GetEntrpGroupType()
      {
         if (CMonitoringAppl.GetListOfLicence(applID).Count > 0)
            return EEntrpGroupType.DTRP;
         return EEntrpGroupType.TECH;
      }
      #endregion
   }
}
