﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;
using System;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CPermission
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         _dictPerm.Clear();
         countStnPerm = 0;
      }
      //======================================================
      public enum EPermissionType : byte
      {
         /// <summary>
         /// Заключение ЕМС
         /// </summary>
         Build = 0, 
         Operation = 1,
         Import = 2,
         Project = 3,
         /// <summary>
         /// Разрешение
         /// </summary>
         Use = 4,
         Acquisition = 5,
         Unknown = 6,
          TempUse = 7,
          UseExstract = 8,
          Licence = 9,
      }
      //======================================================
      private static Dictionary<string, int> _dictPerm = new Dictionary<string, int>();
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новом разрешении. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="stationId">ID станции</param>
      /// <param name="permNumber">Номер разрешения</param>
      /// <param name="startDate">Дата начала</param>
      /// <param name="stopDate">Дата окончания</param>
      /// <param name="cancelDate">Лата аналирования</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewPermission(XElement root, int stationId, string permNumber, DateTime startDate, DateTime stopDate, EPermissionType prtmType, string SpecialCondition, string closeDozvNum, DateTime cancelDate)
      {
         using (LisProgressBar pb = new LisProgressBar("Permissions"))
         {
            if (string.IsNullOrEmpty(permNumber) ||
                startDate == IM.NullT)
               return IM.NullI;
            //-----
            string ptrStr = string.Format("{0}_{1}", "PERMISSION", permNumber);
            pb.SetSmall(ptrStr);
            if (_dictPerm.ContainsKey(ptrStr) == true)
               return _dictPerm[ptrStr];
            //----
            int permId = CExternalID.GetDocumentsExtID(permNumber);
            //----
            XElement elem = new XElement("PERM");
            XElement temp = new XElement("PERMID");
            temp.SetValue(permId);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("STATIONID");
            temp.SetValue(stationId);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("PERMNO");
            temp.SetValue(permNumber.ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            if (startDate != IM.NullT)
            {
               temp = new XElement("PERMSTARTDATE");
               temp.SetValue(startDate.ToMDateTime());
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (stopDate != IM.NullT)
            {
               temp = new XElement("PERMENDDATE");
               temp.SetValue(stopDate.ToMDateTime());
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (cancelDate != IM.NullT)
            {
                temp = new XElement("PERM_CLOSE_DATE");
                temp.SetValue(cancelDate.ToMDateTime());
                elem.Add(temp);
            }
            ///////////////////////////////////////////
            temp = new XElement("KIND");
            temp.SetValue((byte)prtmType);
            elem.Add(temp);
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(closeDozvNum))
            {
               temp = new XElement("PERM_NUM_CLOSED");
               temp.SetValue(closeDozvNum);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(SpecialCondition))
            {
               temp = new XElement("SPECIAL_CONDITONS");
               temp.SetValue(SpecialCondition);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, permId);
            root.Element("Permissions").Add(elem);
            //-----
            _dictPerm.Add(ptrStr, permId);
            return permId;
         }
      }
      //======================================================
      private static int countStnPerm = 0;
      //======================================================
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новом разрешении. (StnPermissions)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="stationID">ID станции</param>
      /// <param name="permNumber">Номер разрешения</param>
      /// <param name="startDate">Дата начала</param>
      /// <param name="stopDate">Дата окончания</param>
      /// <param name="cancelDate">Дата анулирования</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewStnPermission(XElement root, int stationID, string permNumber, DateTime startDate, DateTime stopDate, EPermissionType prtmType, string SpecialCondition, string closeDozvNum, DateTime cancelDate)
      {
         int newPermID = WriteNewPermission(root, stationID, permNumber, startDate, stopDate, prtmType, SpecialCondition, closeDozvNum, cancelDate);
         if ((stationID != IM.NullI) && (newPermID != IM.NullI))
         {
            countStnPerm++;
            //----
            XElement elem = new XElement("STN_PERM");
            XElement temp = new XElement("ID");
            temp.SetValue(countStnPerm);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("STN_ID");
            temp.SetValue(stationID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("PERM_ID");
            temp.SetValue(newPermID);
            elem.Add(temp);
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, countStnPerm);
            root.Element("StnPermissions").Add(elem);
            return countStnPerm;
         }
         return IM.NullI;
      }
   }
}
