﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public static class CExtensionMonitoring
   {
       public static bool? GetB(this IMRecordset obj, string fieldname)
       {
           bool? res = null;
           int resi = obj.GetI(fieldname);
           if (resi != IM.NullI)
               res = resi != 0;
           return res;
       }
       //===================================================
      /// <summary>
      /// Преобразовывает запрещенные символы в XML
      /// </summary>
      /// <param name="value">строка</param>
      /// <returns>Возвращает строку без запрещенных символов для XML</returns>
      public static string ToStrXMLMonitoring(this string value)
      {
         return value;
      }
      //===================================================
      /// <summary>
      /// Возвращает направленность антены вещатилей
      /// </summary>
      /// <param name="rs">IMRecordset</param>
      /// <returns>TRUE - направленная, FALSE - ненаправленная</returns>
      public static  bool IsBroadcastDirection(this IMObject rs)
      {
         bool retVal = false;
         switch (rs.GetS("POLARIZATION"))
         {
            case "V":
               retVal = GetBroadcastDirection(rs, "DIAGV");
               break;
            case "H":
               retVal = GetBroadcastDirection(rs, "DIAGH");
               break;
            default:
               retVal = GetBroadcastDirection(rs, "DIAGH") || GetBroadcastDirection(rs, "DIAGV");
               break;
         }
         return retVal;
      }
      //===================================================
      public static bool GetBroadcastDirection(IMObject rs, string fieldName)
      {
         bool isDirected = true;
         string diag = rs.GetS(fieldName);
         if ((diag == "") || diag.Contains("OMNI"))
            isDirected = false;
         return isDirected;
      }
   }
}
