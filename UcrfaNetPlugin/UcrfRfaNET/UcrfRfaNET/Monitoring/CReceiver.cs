﻿using System.Collections.Generic;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CReceiver
   {
      public class ReceiverData
      {
         public string Polarization { get; set; } //Поляризация
         public double AntGain { get; set; }   // dB
         public int AntId { get; set; }        //ID антенны
         //================================
         public ReceiverData()
         {
            Polarization = "";
            AntGain = IM.NullD;
            AntId = IM.NullI;
         }
      }
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictReceiver.Clear();
      }
      //======================================================
      private static Dictionary<string, int> dictReceiver = new Dictionary<string, int>();
      //======================================================
      //===================================================
      public static int WriteNewReceiver(XElement root, int sectorId, ReceiverData exData)
      {
         if (exData == null)
            exData = new ReceiverData();
         using (LisProgressBar pb = new LisProgressBar("Receiver"))
         {
            //-----
            string ptrStr = string.Format("{0}_{1}", "RECEIVER", sectorId);
            pb.SetSmall(ptrStr);
            if (dictReceiver.ContainsKey(ptrStr) == true)
               return dictReceiver[ptrStr];
            //----
            XElement elem = new XElement("RECEIVER");
            XElement temp = new XElement("RECEIVERID");
            temp.SetValue(sectorId);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("SECTORID_REF");
            temp.SetValue(sectorId);
            elem.Add(temp);
            ///////////////////////////////////////////
            if (exData.Polarization != "")
            {
               temp = new XElement("POLARIZATION");
               temp.SetValue(exData.Polarization.ToStrXMLMonitoring());
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (exData.AntGain != IM.NullD)
            {
               temp = new XElement("ANT_GAIN");
               temp.SetValue(exData.AntGain);//Пока заношу в dB
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (exData.AntId != IM.NullI)
            {
               temp = new XElement("ANTENNAID");
               temp.SetValue(exData.AntId);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, sectorId);
            root.Element("Receivers").Add(elem);
            //-----
            dictReceiver.Add(ptrStr, sectorId);
            return sectorId;
         }
      }
   }
}
