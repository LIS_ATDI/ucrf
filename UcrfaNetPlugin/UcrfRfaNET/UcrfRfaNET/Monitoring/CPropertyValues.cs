﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CPropertyValues
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         propValID = 0;
      }
      //===================================================
      private static int propValID = 0;
      //===================================================
      /// <summary>
      /// Создает элемент PropertyValues (PRP_VAL) в XML
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="PROP_ID">Сcылка на Properties</param>
      /// <param name="REL_ID">ID записи</param>
      /// <param name="REL_TYPE">ID GUID таблицы</param>
      /// <returns></returns>
      public static int AddPropertyValue(XElement root, int PROP_ID, int REL_ID, int REL_TYPE, int VAL_INT_FROM, int VAL_INT_TO)
      {
         //----
         XElement elem = BasePropertyValue(PROP_ID, REL_ID, REL_TYPE);
         ///////////////////////////////////////////
         XElement temp = null;
         if (VAL_INT_FROM != IM.NullI)
         {
            temp = new XElement("VAL_INT_FROM");
            temp.SetValue(VAL_INT_FROM);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         if (VAL_INT_TO != IM.NullI)
         {
            temp = new XElement("VAL_INT_TO");
            temp.SetValue(VAL_INT_TO);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         root.Element("PropertyValues").Add(elem);
         return propValID;
      }
      /// <summary>
      /// Создает элемент PropertyValues (PRP_VAL) в XML (и создает дополнительный тэг VAL_STR с наименованием пользователя)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="PROP_ID3">Сcылка на Properties 3</param>
      /// <param name="PROP_ID4">Сcылка на Properties 4</param>
      /// <param name="REL_ID">ID записи</param>
      /// <param name="REL_TYPE">ID GUID таблицы</param>
      /// <param name="Cnt">Доп признак (VAL_STR  -  1, VAL_INT_FROM - 0 )</param>
      /// <returns></returns>
      public static int AddPropertyValue2(XElement root, int PROP_ID3, int PROP_ID4, int PROP_ID5, int REL_ID, int REL_TYPE, string VAL_INT_FROM, string Name_Filial,string Address, int VAL_INT_TO, int Cnt)
      {
         XElement elem = null;
         if (Cnt == 0)
         {
             elem = BasePropertyValue(PROP_ID3, REL_ID, REL_TYPE);
         }
         else if (Cnt >= 1)
         {
             if (Cnt == 1)
             {
                 elem = BasePropertyValue(PROP_ID4, REL_ID, REL_TYPE);
             }
             if (Cnt == 2)
             {
                 elem = BasePropertyValue(PROP_ID5, REL_ID, REL_TYPE);
             }
         }

         ///////////////////////////////////////////
         XElement temp = null;
         if (Cnt == 0)
         {
                 temp = new XElement("VAL_INT_FROM");
                 temp.SetValue(VAL_INT_FROM);
                 elem.Add(temp);
         }
         //////////////////////////////////////////
         if (Cnt >= 1)
         {
             if (Cnt == 1)
             {
                 temp = new XElement("VAL_STR");
                 temp.SetValue(Name_Filial);
                 elem.Add(temp);
             }
             if (Cnt == 2)
             {
                 temp = new XElement("VAL_STR");
                 temp.SetValue(Address);
                 elem.Add(temp);
             }
         }
         ///////////////////////////////////////////
         if (VAL_INT_TO != IM.NullI)
         {
            temp = new XElement("VAL_INT_TO");
            temp.SetValue(VAL_INT_TO);
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         root.Element("PropertyValues").Add(elem);
         return propValID;
      }
      //===================================================
      /// <summary>
      /// Создает основные элементы PropertyValues
      /// </summary>
      /// <param name="PROP_ID">Сcылка на Properties</param>
      /// <param name="REL_ID">ID записи</param>
      /// <param name="REL_TYPE">ID GUID таблицы</param>
      /// <returns></returns>
      private static XElement BasePropertyValue(int PROP_ID, int REL_ID, int REL_TYPE)
      {
         //Создаем новую запись
         XElement elem = new XElement("PRP_VAL");
         ///////////////////////////////////////////
         XElement temp = new XElement("ID");
         temp.SetValue(++propValID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("SYS_ID");
         temp.SetValue(CDBGuids.ICSM_DBGuid);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("PROP_ID");
         temp.SetValue(PROP_ID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("REL_ID");
         temp.SetValue(REL_ID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("REL_TYPE");
         temp.SetValue(REL_TYPE);
         elem.Add(temp);
         return elem;
      }
   }
}
