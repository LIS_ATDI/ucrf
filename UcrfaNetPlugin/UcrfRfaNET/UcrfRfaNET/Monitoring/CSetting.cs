﻿namespace XICSM.UcrfRfaNET.Monitoring
{
   internal class CSetting
   {
      //====================================================
      // Описание сонстрант для данных
      private const string CstNostNameR135 = "FTP_R135_HOST_NAME";
      private const string CstUserName = "FTP_R135_USER_NAME";
      private const string CstPassword = "FTP_R135_P";
      private const string CstHostNameIcsm = "FTP_ICSM_HOST_NAME";
      private const string CstUserNameIcsm = "FTP_ICSM_USER_NAME";
      private const string CstPasswordIcsm = "FTP_ICSM_P";
      //====================================================
      public struct TftpMonitoringSetting
      {
         public string hostNameR135;
         public string userName;
         public string password;
         public string hostNameIcsm;
         public string userNameIcsm;
         public string passwordIcsm;
      }
      //===================================================
      /// <summary>
      /// IP адресс Р135-й
      /// </summary>
      public static string IpFtpP135 { get{return FtpMonitoringSetting.hostNameR135;} }
      /// <summary>
      /// IP адресс ICSM
      /// </summary>
      public static string IpFtpIcsm { get { return FtpMonitoringSetting.hostNameIcsm;} }
      //-------
      public static TftpMonitoringSetting FtpMonitoringSetting;
      //===================================================
      static CSetting()
      {
         //FTP settings
         FtpMonitoringSetting = new TftpMonitoringSetting();
         LoadFtpSettings();
      }
      //===================================================
      /// <summary>
      /// Загружаем настройки FTP
      /// </summary>
      public static void LoadFtpSettings()
      {
         FtpMonitoringSetting.hostNameIcsm = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstHostNameIcsm);
         FtpMonitoringSetting.hostNameR135 = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstNostNameR135);
         FtpMonitoringSetting.userName = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstUserName);
         FtpMonitoringSetting.password = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstPassword);
         FtpMonitoringSetting.userNameIcsm = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstUserNameIcsm);
         FtpMonitoringSetting.passwordIcsm = HelpClasses.HelpFunction.ReadDataFromSysConfig(CstPasswordIcsm);
      }
      //===================================================
      /// <summary>
      /// Сохраняем настройки FTP
      /// </summary>
      public static void SaveFtpSettings()
      {
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstHostNameIcsm, FtpMonitoringSetting.hostNameIcsm);
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstNostNameR135, FtpMonitoringSetting.hostNameR135);
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstUserName, FtpMonitoringSetting.userName);
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstPassword, FtpMonitoringSetting.password);
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstUserNameIcsm, FtpMonitoringSetting.userNameIcsm);
         HelpClasses.HelpFunction.WriteDataToSysConfig(CstPasswordIcsm, FtpMonitoringSetting.passwordIcsm);
      }
   }
}
