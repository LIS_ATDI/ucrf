﻿using System;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.Monitoring
{
   internal partial class FSettingFtp : FBaseForm
   {
      //===================================================
      // Properties
      public string hostNameR135 { get { return CSetting.FtpMonitoringSetting.hostNameR135; } set { CSetting.FtpMonitoringSetting.hostNameR135 = value; } }
      public string userName { get { return CSetting.FtpMonitoringSetting.userName; } set { CSetting.FtpMonitoringSetting.userName = value; } }
      public string password { get { return CSetting.FtpMonitoringSetting.password; } set { CSetting.FtpMonitoringSetting.password = value; } }
      public string hostNameIcsm { get { return CSetting.FtpMonitoringSetting.hostNameIcsm; } set { CSetting.FtpMonitoringSetting.hostNameIcsm = value; } }
      public string userNameIcsm { get { return CSetting.FtpMonitoringSetting.userNameIcsm; } set { CSetting.FtpMonitoringSetting.userNameIcsm = value; } }
      public string passwordIcsm { get { return CSetting.FtpMonitoringSetting.passwordIcsm; } set { CSetting.FtpMonitoringSetting.passwordIcsm = value; } }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      public FSettingFtp()
      {
         InitializeComponent();
         //================
         tbHost.DataBindings.Add("Text", this, "hostNameR135", true);
         tbUserName.DataBindings.Add("Text", this, "userName", true);
         tbPassword.DataBindings.Add("Text", this, "password", true);
         tbHostIcsm.DataBindings.Add("Text", this, "hostNameIcsm", true);
         tbUserNameIcsm.DataBindings.Add("Text", this, "userNameIcsm", true);
         tbPasswordIcsm.DataBindings.Add("Text", this, "passwordIcsm", true);
      }
      //===================================================
      /// <summary>
      /// Сохраняем измененные данные
      /// </summary>
      private void buttonOk_Click(object sender, EventArgs e)
      {
         CSetting.SaveFtpSettings();
      }
      //===================================================
      /// <summary>
      /// Отменяем изменения
      /// </summary>
      private void buttonCancel_Click(object sender, EventArgs e)
      {
         CSetting.LoadFtpSettings();         
      }
      //===================================================
      /// <summary>
      /// Тестовое подключение
      /// </summary>
      private void buttonTestConnection_Click(object sender, EventArgs e)
      {
         if (openFileDialogTestConnection.ShowDialog() == DialogResult.OK)
         {
            if (FtpWriterReader.TestConnectionR135(openFileDialogTestConnection.FileName) == false)
               MessageBox.Show(CLocaliz.TxT("The ftp test is completed with error"), CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
               MessageBox.Show(CLocaliz.TxT("The ftp test is completed successfully"), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
         }
      }

      private void btnTestIcsm_Click(object sender, EventArgs e)
      {
         if (openFileDialogTestConnection.ShowDialog() == DialogResult.OK)
         {
            if (FtpWriterReader.TestConnectionICSM(openFileDialogTestConnection.FileName) == false)
               MessageBox.Show(CLocaliz.TxT("The ftp test is completed with error"), CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
               MessageBox.Show(CLocaliz.TxT("The ftp test is completed successfully"), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
         }
      }
   }
}
