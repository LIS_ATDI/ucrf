﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class FtpWriterReader
   {
      //===================================================
      /// <summary>
      /// Отправляет файл в Р135
      /// </summary>
      public static bool WriteFileR135(string fileName)
      {
         bool retVal = false;
         try
         {
            Upload(fileName, CSetting.FtpMonitoringSetting.hostNameR135, CSetting.FtpMonitoringSetting.userName, CSetting.FtpMonitoringSetting.password);
            retVal = true;
         }
         catch (Exception ex)
         {
            CLogs.WriteError(ELogsWhat.Monitoring, ex);
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Проверка соединения (загружет -> переименовывает -> удаляет файл)
      /// </summary>
      public static bool TestConnectionR135(string fileName)
      {
         bool retVal = false;
         try
         {
            Upload(fileName, CSetting.FtpMonitoringSetting.hostNameR135, CSetting.FtpMonitoringSetting.userName, CSetting.FtpMonitoringSetting.password);
            DeleteFile(fileName, CSetting.FtpMonitoringSetting.hostNameR135, CSetting.FtpMonitoringSetting.userName, CSetting.FtpMonitoringSetting.password);
            retVal = true;
         }
         catch (Exception ex)
         {
            CLogs.WriteError(ELogsWhat.Monitoring, ex);
            //MessageBox.Show(ex.Message, "Ftp error");
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Проверка соединения (загружет -> переименовывает -> удаляет файл)
      /// </summary>
      public static bool TestConnectionICSM(string fileName)
      {
         bool retVal = false;
         try
         {
            string uploadedFile = Path.GetFileName(fileName);
            Upload(fileName, CSetting.FtpMonitoringSetting.hostNameIcsm, CSetting.FtpMonitoringSetting.userNameIcsm, CSetting.FtpMonitoringSetting.passwordIcsm);
            string[] files = GetFileList(CSetting.FtpMonitoringSetting.hostNameIcsm, CSetting.FtpMonitoringSetting.userNameIcsm, CSetting.FtpMonitoringSetting.passwordIcsm);
            string tmpFolder = ICSM.IM.GetWorkspaceFolder();
            Download(tmpFolder, uploadedFile, CSetting.FtpMonitoringSetting.hostNameIcsm, CSetting.FtpMonitoringSetting.userNameIcsm, CSetting.FtpMonitoringSetting.passwordIcsm);
            if (!System.IO.File.Exists(string.Format("{0}\\{1}", tmpFolder, uploadedFile)))
               throw new Exception("Can't download file");
            DeleteFile(fileName, CSetting.FtpMonitoringSetting.hostNameIcsm, CSetting.FtpMonitoringSetting.userNameIcsm, CSetting.FtpMonitoringSetting.passwordIcsm);
            retVal = true;
         }
         catch (Exception ex)
         {
            CLogs.WriteError(ELogsWhat.Monitoring, ex);
            //MessageBox.Show(ex.Message, "Ftp error");
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Uploads file to FTP
      /// </summary>
      /// <param name="filename">A file name</param>
      private static void Upload(string filename, string ftpUrl, string userName, string passw)
      {
         using (LisProgressBar pb = new LisProgressBar("Ftp connection..."))
         {
            FileInfo fileInf = new FileInfo(filename);
            string asName = fileInf.Name + "x";
            pb.SetBig(string.Format("Connecting to {0}", ftpUrl));
            string uri = string.Format("{0}/{1}", ftpUrl, asName);
            FtpWebRequest reqFtp = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
            reqFtp.Credentials = new NetworkCredential(userName, passw);
            reqFtp.KeepAlive = false;
            reqFtp.Method = WebRequestMethods.Ftp.UploadFile;
            reqFtp.UseBinary = true;
            reqFtp.ContentLength = fileInf.Length;
            const int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            FileStream fs = fileInf.OpenRead();
            pb.SetBig(string.Format("Uploading file: {0}", fileInf.Name));
            long curLeng = 0;
            int lastPercent = -1;
            int curPercent = 0;
            try
            {
               Stream strm = reqFtp.GetRequestStream();
               int contentLen = fs.Read(buff, 0, buffLength);
               while (contentLen != 0)
               {
                  curLeng += contentLen;
                  curPercent = (int)(curLeng * 100 / reqFtp.ContentLength);
                  if (curPercent != lastPercent)
                  {
                     lastPercent = curPercent;
                     if (lastPercent > 100)
                        lastPercent = 100;
                     pb.SetSmall(lastPercent, 100);
                     pb.SetProgress(lastPercent, 100);
                  }
                  strm.Write(buff, 0, contentLen);
                  contentLen = fs.Read(buff, 0, buffLength);
               }
               strm.Close();
               Rename(asName, fileInf.Name, ftpUrl, userName, passw);
            }
            finally
            {
               fs.Close();
            }
         }
      }
      //===================================================
      /// <summary>
      /// Renames a file on the FTP
      /// </summary>
      /// <param name="currentFilename">a current file name</param>
      /// <param name="newFilename">a new file name</param>
      private static void Rename(string currentFileName, string newFileName, string ftpUrl, string userName, string passw)
      {
         using (LisProgressBar pb = new LisProgressBar("Ftp connection..."))
         {
            string curFile = Path.GetFileName(currentFileName);
            string newFile = Path.GetFileName(newFileName);
            pb.SetBig(string.Format("Renaming file: {0} -> {1}", curFile, newFile));
            pb.SetSmall("");
            pb.SetProgress(0, 0);
            FtpWebResponse response = null;
            Stream ftpStream = null;
            try
            {
               string uri = string.Format("{0}/{1}", ftpUrl, curFile);
               FtpWebRequest reqFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
               reqFtp.Method = WebRequestMethods.Ftp.Rename;
               reqFtp.RenameTo = newFile;
               reqFtp.UseBinary = true;
               reqFtp.Credentials = new NetworkCredential(userName, passw);
               response = (FtpWebResponse)reqFtp.GetResponse();
               ftpStream = response.GetResponseStream();
            }
            finally
            {
               if (ftpStream != null)
                  ftpStream.Close();
               if (response != null)
                  response.Close();
            }
         }
      }
      //===================================================
      /// <summary>
      /// Deletes file from FTP
      /// </summary>
      /// <param name="fileName"></param>
      public static void DeleteFile(string fileName, string ftpUrl, string userName, string passw)
      {
         using (LisProgressBar pb = new LisProgressBar("Ftp connection..."))
         {
            string curFileName = Path.GetFileName(fileName);
            pb.SetBig(string.Format("Deleting file: {0}", curFileName));
            pb.SetSmall("");
            pb.SetProgress(0, 0);
            FtpWebResponse response = null;
            Stream datastream = null;
            StreamReader sr = null;
            try
            {
               string uri = string.Format("{0}/{1}", ftpUrl, curFileName);
               FtpWebRequest reqFtp = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
               reqFtp.Credentials = new NetworkCredential(userName,
                                                          passw);
               reqFtp.KeepAlive = false;
               reqFtp.Method = WebRequestMethods.Ftp.DeleteFile;
               response = (FtpWebResponse)reqFtp.GetResponse();
               datastream = response.GetResponseStream();
               sr = new StreamReader(datastream);
               sr.ReadToEnd();
            }
            finally
            {
               if (sr != null)
                  sr.Close();
               if (datastream != null)
                  datastream.Close();
               if (response != null)
                  response.Close();
            }
         }
      }
      //===================================================
      /// <summary>
      /// Returns a list of files and directories
      /// </summary>
      public static string[] GetFileList(string ftpUrl, string userName, string passw)
      {
         StringBuilder result = new StringBuilder();
         FtpWebRequest reqFTP;
         WebResponse response = null;
         StreamReader reader = null;
         try
         {
            string uri = string.Format("{0}", ftpUrl);
            reqFTP = (FtpWebRequest) FtpWebRequest.Create(new Uri(uri));
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(userName, passw);
            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
            response = reqFTP.GetResponse();
            reader = new StreamReader(response.GetResponseStream());
            string line = reader.ReadLine();
            while (line != null)
            {
               result.Append(line);
               result.Append("\n");
               line = reader.ReadLine();
            }
            result.Remove(result.ToString().LastIndexOf('\n'), 1);
         }
         finally
         {
            if(reader != null)
               reader.Close();
            if(response != null)
               response.Close();
         }
         return result.ToString().Split('\n');
      }
      //===================================================
      /// <summary>
      /// Downloads file
      /// </summary>
      public static void Download(string filePath, string fileName, string ftpUrl, string userName, string passw)
      {
         FtpWebRequest reqFTP;
         Stream ftpStream = null;
         FtpWebResponse response = null;
         FileStream outputStream = null;
         try
         {
            string uri = string.Format("{0}/{1}", ftpUrl, fileName);
            reqFTP = (FtpWebRequest) FtpWebRequest.Create(new Uri(uri));
            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
            reqFTP.UseBinary = true;
            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
            reqFTP.Credentials = new NetworkCredential(userName, passw);
            reqFTP.KeepAlive = false;
            response = (FtpWebResponse) reqFTP.GetResponse();
            ftpStream = response.GetResponseStream();
            const int bufferSize = 2048;
            byte[] buffer = new byte[bufferSize];

            int readCount = ftpStream.Read(buffer, 0, bufferSize);
            while (readCount > 0)
            {
               if(outputStream == null)
                  outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);
               outputStream.Write(buffer, 0, readCount);
               readCount = ftpStream.Read(buffer, 0, bufferSize);
            }
         }
         finally
         {
            if(ftpStream != null)
               ftpStream.Close();
            if(outputStream != null)
               outputStream.Close();
            if(response != null)
               response.Close();
         }
      }

   }
}
