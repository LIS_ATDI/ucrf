﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET;
using System.IO;


namespace XICSM.UcrfRfaNET.Monitoring
{
   public class BaseXMLexport
   {
      //===================================================
      /// <summary>
      /// Создает XML документ
      /// </summary>
      /// <returns>XML документ</returns>
      public static XElement GetDocument()
      {
         XDocument xdoc = new XDocument(new XDeclaration("1.0", "UTF-8", null));
         XProcessingInstruction xinstruction = new XProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"exchange2.xsl\"");
         xdoc.Add(xinstruction);
         XElement root = new XElement("Exchange");
         root.Add(
         new XAttribute("xmlnschange", "changethisvalue_to_xmlns1=http://www.radiosoft.com.ua/Exchange"),
         //new XAttribute("xmlns", "http://www.radiosoft.com.ua/Exchange"),
         new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
         new XAttribute(XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance") + "schemaLocation", "http://www.radiosoft.com.ua/Exchange exchange2.xsd"),
         new XAttribute("DocGUID", "{D63E14EC-C14E-411F-A3BF-B6F7F755CE8A}"),
         new XAttribute("DocDescription", "Ліцензіати"),
         new XAttribute("SrcGUID", CDBGuids.DbGuidIcsm),
         new XAttribute("SrcDescription", "ICSM"),
         new XAttribute("SrcAddress", CSetting.IpFtpIcsm),
         new XAttribute("DstGUID", "{90FE8386-E532-4199-BC70-E317D2F7765A}"),
         new XAttribute("DstDescription", "RS-135U УКРАЇНА"),
         new XAttribute("DstAddress", CSetting.IpFtpP135),
         new XAttribute("Remarks", "Обмін ліцензіатами з РС135У"),
         new XAttribute("TicketType", "FULL"),
         new XAttribute("ExchangeDOC_GUID", "{" + Guid.NewGuid().ToString().ToUpperInvariant() + "}"),
         //new XAttribute("DateModified", "2010-10-08T13:40:56"));
         new XAttribute("DateModified", DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss")));
         xdoc.Add(root);

         XElement tableGuids = new XElement("TableGUIDs");
         tableGuids.Add(new XAttribute("xmlnschange", "changethisvalue_to_xmlns1=http://www.radiosoft.com.ua/simple"));

         root.Add(
            tableGuids,
            new XElement("DBGuids"),
            new XElement("Enterprises"),
            new XElement("Departments"),
            new XElement("Stations"),
            new XElement("StnLicenses"),
            new XElement("StnBases"),
            new XElement("Nets"),
            new XElement("NetFreqs"),
            new XElement("Countrys"),
            new XElement("Areas"),
            new XElement("Districts"),
            new XElement("Regions"),
            new XElement("Citys"),
            new XElement("Streets"),
            new XElement("TypeCitys"),
            new XElement("TypeStreets"),
            new XElement("Positions"),
            new XElement("Banks"),
            new XElement("Sectors"),
            new XElement("Transmitters"),
            new XElement("Receivers"),
            new XElement("StationFreqs"),
            new XElement("StnPermissions"),
            new XElement("Permissions"),
            new XElement("Licenses"),
            new XElement("RegisterCards"),
            new XElement("RegionUses"),
            new XElement("LicenseDatas"),
            new XElement("LicenseFreqs"),
            new XElement("RESTypes"),
            new XElement("Services"),
            new XElement("Systems"),
            new XElement("Selectivities"),
            new XElement("SystemDiapazones"),
            new XElement("TrEquip"),
            new XElement("RcEquip"),
            new XElement("TrEquipStd"),
            new XElement("RcEquipStd"),
            new XElement("Antennas"),
            new XElement("DDAHMatrix"),
            new XElement("DDAVMatrix"),
            new XElement("TFilters"),
            new XElement("Matrices"),
            new XElement("PlanRFRs"),
            new XElement("PlanFreqRFRs"),
            new XElement("RadioTechnologies"),
            new XElement("ShipResTypes"),
            new XElement("PermStnClasses"),
            new XElement("CommunicationSystems"),
            new XElement("RCWorks"),
            new XElement("RadioControlTechnologies"),
            new XElement("RadioTechEquipments"),
            new XElement("RadioTechEnterprises"),
            new XElement("Ships"),
            new XElement("PropertyValues"),
            new XElement("Properties"),
            new XElement("PropertyDims")
         );

         //------
         //GUID ICSM
         CDBGuids.AddICSM(root);
         CTableGuids.WriteTableGuid(root, "Stations", "{4419164C-6E75-461F-AD25-F58E9E4707EE}");

         return root;
      }
      //===================================================
      /// <summary>
      /// Сохраняет XML документ в файл
      /// </summary>
      /// <param name="ex">XElement XML документ</param>
      /// <param name="fileName">Имя файла</param>
      /// <returns>TRUE если все ОК, иначе FALSE</returns>
      public static bool SaveXMLDocument(XElement ex, string fileName)
      {
         bool retVal = false;
         try
         {
            string strToSave = ex.Document.ToString().Replace("xmlnschange=\"changethisvalue_to_xmlns1=http://www.radiosoft.com.ua/Exchange\"", "xmlns=\"http://www.radiosoft.com.ua/Exchange\"").Replace("xmlnschange=\"changethisvalue_to_xmlns1=http://www.radiosoft.com.ua/simple\"", "xmlns=\"http://www.radiosoft.com.ua/simple\"");
            string error = "";
            Encoding encdng = System.Text.Encoding.UTF8;
            using (StreamWriter outputStream = new StreamWriter(fileName, false))
            {
               outputStream.WriteLine(ex.Document.Declaration);
               outputStream.WriteLine(strToSave);
            }
            if (CXMLTest.TestAllXmlToSchema(strToSave, "exchange2.xsd", "http://www.radiosoft.com.ua/Exchange", ref error) == false)
               CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Schema error: Can't save the XML document in the file \"{0}\" {1}"), fileName, error));
            else
               retVal = true;
         }
         catch(Exception e)
         {
            CLogs.WriteError(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't save the XML document in the file \"{0}\": {1}"), fileName, e.Message));
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Очищает все массивы и подготавливает данные для создания нового XML файла
      /// </summary>
      public static void Clear()
      {
         CPermission.Clear();
         CAntenna.Clear();
         CLicence.Clear();
         CPositionXML.Clear();
         CStationFreqsXML.Clear();
         CSystemsXML.Clear();
         CTransmitter.Clear();
         CTxEquip.Clear();
         CEnterprise.Clear();
         CProperties.Clear();
         CPropertyValues.Clear();
         CDBGuids.Clear();
         CRadioTech.Clear();
         CComSystem.Clear();
         CPermStnClass.Clear();
         CNet.Clear();
      }
   }
}
