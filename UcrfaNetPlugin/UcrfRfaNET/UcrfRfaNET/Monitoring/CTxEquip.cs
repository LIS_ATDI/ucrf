﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;
using System;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CTxEquip
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictEquip.Clear();
      }
      //======================================================
      private static Dictionary<string, int> dictEquip = new Dictionary<string, int>();
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новом оборудовании. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="ptr">Record pointer</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewTxEquip(XElement root, RecordPtr ptr)
      {
         using (LisProgressBar pb = new LisProgressBar("Equipment"))
         {
            //-----
            string ptrStr = string.Format("{0}_{1}", ptr.Table, ptr.Id);
            pb.SetSmall(ptrStr);
            if (dictEquip.ContainsKey(ptrStr) == true)
               return dictEquip[ptrStr];
            //-----
            string name = "Невідомий передавач";
            string manufacturer = "";
            string certificate = "";
            string desigEmission = "";
            DateTime certDate = IM.NullT;  
            {                
               try
               {
                   string manufFld = "MANUFACTURER";
                   string certFld = "CUST_TXT1";
                   string certDateFld = "CUST_DAT1";
                   if (ptr.Table == PlugTbl.XfaEquip)
                   {
                       certFld = "CERTIFICATE_NUM";
                       certDateFld = "CERTIFICATE_DATE";
                   }
                   else if (ptr.Table == PlugTbl.XfaEquipAmateur)
                   {
                       manufFld = "CREATED_BY"; // fake
                       certFld = "NAME"; // fake
                       certDateFld = "DATE_CREATED"; // fake
                   }

                   string fldList = "ID,NAME,DESIG_EMISSION," + manufFld + "," + certFld + "," + certDateFld;
                   IMRecordset rs = IMRecordset.ForRead(ptr, fldList);
                  name = rs.GetS("NAME");
                  manufacturer = rs.GetS("MANUFACTURER");
                  desigEmission = rs.GetS("DESIG_EMISSION");
                  certificate = rs.GetS(certFld);
                  certDate = rs.GetT(certDateFld);
                  rs.Destroy();
               }
               catch
               {
               }
            }
            //-----
            //Создаем новый элемент
            int globId = CExternalID.GetRecordPtrExtID(ptr);
            //----
            XElement elem = new XElement("TR_EQUIP");
            XElement temp = new XElement("TREQUIPMENTID");
            temp.SetValue(globId);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NAME");
            temp.SetValue(name.Cut(128).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NTYPE");
            temp.SetValue(name.Cut(128).ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            // using new methods to construct XML
            if (ptr.Table != PlugTbl.XfaEquipAmateur)
                BaseStationXML.AddElement(elem, "MANUFACTURER", manufacturer, 64);

            BaseStationXML.AddElement(elem, "RADIATIONCLASS", desigEmission.Replace("-",""), 0);

            if (ptr.Table != PlugTbl.XfaEquipAmateur)
            {
                BaseStationXML.AddElement(elem, "CERT_NUM", certificate, 64);
                BaseStationXML.AddElement(elem, "CERT_DATE", certDate, "yyyy-MM-dd");
            }

            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, globId);
            root.Element("TrEquip").Add(elem);
            //-----
            dictEquip.Add(ptrStr, globId);
            return globId;
         }
      }
   }
}
