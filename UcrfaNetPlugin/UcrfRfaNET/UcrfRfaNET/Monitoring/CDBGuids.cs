﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CDBGuids
   {
      public const string DbGuidIcsm = "{B2E2EAA6-FF64-4142-B279-990D8CAD2B52}";
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dbguidID = 0;
         dictGuids.Clear();
         ICSM_DBGuid = 0;
      }
      //===================================================
      //Properties
      private static int dbguidID = 0;  //ID записи
      //Список уже занесенных записей
      private static Dictionary<string, int> dictGuids = new Dictionary<string, int>();
      //GUID ICSM
      public static int ICSM_DBGuid { get; set; }
      //===================================================
      /// <summary>
      /// Создает DBGuids.DBGUID в XML (исключает дубликаты)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="name">поле DBGuids.DBGUID.NAME</param>
      /// <param name="descr">поле DBGuids.DBGUID.DESCRIPTION</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int WriteDBGuid(XElement root, string name, string descr, string address)
      {
         if (dictGuids.ContainsKey(name) == true)
            return dictGuids[name];
         //----
         //Создаем новую запись
         XElement elem = new XElement("DBGUID");
         ///////////////////////////////////////////
         XElement temp = new XElement("DBGUIDID");
         temp.SetValue(++dbguidID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("NAME");
         temp.SetValue(name.ToStrXMLMonitoring());
         elem.Add(temp);
         ///////////////////////////////////////////
         if (!string.IsNullOrEmpty(descr))
         {
            temp = new XElement("DESCRIPTION");
            temp.SetValue(descr.Cut(64).ToStrXMLMonitoring());
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         if (!string.IsNullOrEmpty(address))
         {
            temp = new XElement("ADDRESS");
            temp.SetValue(address.Cut(256).ToStrXMLMonitoring());
            elem.Add(temp);
         }
         ///////////////////////////////////////////
         root.Element("DBGuids").Add(elem);
         dictGuids.Add(name, dbguidID);
         return dbguidID;
      }
      //===================================================
      /// <summary>
      /// Создает DBGuids.DBGUID в XML (исключает дубликаты)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="name">поле DBGuids.DBGUID.NAME</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int WriteDBGuid(XElement root, string name)
      {
         return WriteDBGuid(root, name, "", "");
      }
      //===================================================
      /// <summary>
      /// Добавляет DBGUID ICSM-а
      /// </summary>
      /// <param name="root">XElement</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int AddICSM(XElement root)
      {
         ICSM_DBGuid = WriteDBGuid(root, DbGuidIcsm, "ICSM", CSetting.IpFtpIcsm);
         return ICSM_DBGuid;
      }
      //===================================================
      /// <summary>
      /// Добавляет DBGUID таблицы STATIONS
      /// </summary>
      /// <param name="root">XElement</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int AddSTATIONS(XElement root)
      {
         return WriteDBGuid(root, "{1D2A3F30-A982-4253-B2D6-7D5E31417C6E}");
      }
      //===================================================
      /// <summary>
      /// Добавляет DBGUID радиорелеек
      /// </summary>
      /// <param name="root">XElement</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int AddWicrowa(XElement root)
      {
         return WriteDBGuid(root, "{47A84145-63A4-4DEB-BA55-1C6EB557E747}");
      }
   }
}
