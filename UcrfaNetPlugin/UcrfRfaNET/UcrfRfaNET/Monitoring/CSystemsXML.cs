﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;


namespace XICSM.UcrfRfaNET.Monitoring
{
   /// <summary>
   /// Формирует элемент Systems
   /// </summary>
   internal static class CSystemsXML
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         loadedSystems.Clear();
         count = 0;
      }
      //======================================================
      private static Dictionary<string, int> loadedSystems = new Dictionary<string,int>();
      private static int count=0;
      //===================================================
      /// <summary>
      /// Возвращает ID системы (стандарт)
      /// </summary>
      /// <param name="system">Название системы</param>
      /// <returns>ID системы</returns>
      public static int GetSystemID(string system, XElement root)
      {
         if (loadedSystems.ContainsKey(system))
            return loadedSystems[system];
         //--------------
         // Созадем новую систему
         XElement systemEl = new XElement("SYSTEM");
         //----
         XElement temp = new XElement("SYSTEMID");
         temp.SetValue(++count);
         systemEl.Add(temp);
         //----
         temp = new XElement("SYSTEMNAME");
         temp.SetValue(system.ToStrXMLMonitoring());
         systemEl.Add(temp);
         //----
         int glblSystemID = CExternalID.GetSystemExtID(system);
         CExternalID.GetItemKey(systemEl, glblSystemID);
         XElement Systems = root.Element("Systems");
         Systems.Add(systemEl);
         //----
         loadedSystems[system] = count;
         return count;
      }
   }
}
