﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;
using Lis.CommonLib.Extensions; 

namespace XICSM.UcrfRfaNET.Monitoring
{
   class CDvbtStation : BaseStationXML
   {
      //===================================================
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CDvbtStation(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if (stationObj == null)
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         //Заполняем сектор
         int sectorID = CSectors.WriteSector(_root, globalStationID, stationObj.GetS("DESIG_EM"));
         ////-------------
         //Заполняем частоты
         int channel = IM.NullI;
         if (stationObj.GetS("CHANNEL").ToDouble(IM.NullD) != IM.NullD)
            channel = (int)stationObj.GetS("CHANNEL").ToDouble(0);
         int freqID = CStationFreqsXML.WriteStationFreq(_root, sectorID, IM.NullD, stationObj.GetD("FREQ"), ICSMTbl.itblDVBT, channel, IM.NullI);
         //-----------
         //Заполняем антену
         int antennaID = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.ANTENNA_BRO, stationObj.GetI("ANT_ID")), stationObj.GetD("GAIN"), stationObj.GetS("POLARIZATION"));
         //-----------
         //Заполняем оборудование
         int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.EQUIP_BRO, stationObj.GetI("EQUIP_ID")));
         //-----------
         {//Заполняем передатчик
            double pow = stationObj.GetD("PWR_ANT");
            if (pow != IM.NullD)
               pow += 30; //dBW -> dBm
            double azimuth = stationObj.GetD("AZIMUTH");
            double agl = stationObj.GetD("AGL");
            string polar = stationObj.GetS("POLARIZATION");
            double gain = stationObj.GetD("GAIN");
            int fiderLen = stationObj.GetI("CUST_NBR1");
            double fiderLoss = stationObj.GetD("CUST_NBR2");
            double powEffect = IM.NullI;
            switch (polar)
            {
               case "V":
                  //Диаграммы напавленности
                  CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGV"), stationObj.GetD("GAIN"));
                  powEffect = stationObj.GetD("ERP_V");
                  break;
               case "H":
                  //Диаграммы напавленности
                  CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGH"), stationObj.GetD("GAIN"));
                  powEffect = stationObj.GetD("ERP_H");
                  break;
               case "M":
                  {
                     //Диаграммы напавленности
                     CDDAMatrix.WriteDDAMatrix(_root, GlobalStationID, antennaID, CDDAMatrix.polar.H, stationObj.GetS("DIAGH"), stationObj.GetD("GAIN"));
                     double erpV = stationObj.GetD("ERP_V");
                     double erpH = stationObj.GetD("ERP_H");
                     if (erpH != IM.NullD && erpV != IM.NullD)
                        powEffect = 10.0 * Math.Log10(Math.Pow(10.0, erpV / 10.0) + Math.Pow(10.0, erpH / 10.0));
                  }
                  break;

            }
            CTransmitter.TTransmitterData exData = new CTransmitter.TTransmitterData();
            exData.AntheightEffect = stationObj.GetD("EFHGT_MAX");
            CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), sectorID, antennaID, txEquipID, pow, azimuth, agl, polar, gain, fiderLen, fiderLoss, stationObj.GetI("ANT_ID"), ICSMTbl.ANTENNA_BRO, powEffect, stationObj.IsBroadcastDirection(), exData);
         }
      }
      //===================================================
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = stationObj.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return stationObj.GetS("CALL_SIGN");
      }
      //---------
      protected override string getStatus()
      {
         return stationObj.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionBro, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         return stationObj.GetD("AGL");
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(ICSMTbl.itblPositionBro, stationObj.GetI("SITE_ID"));
      }
      //===================================================
      /// <summary>
      /// Возвращает FREQ_BAND_WIDTH
      /// </summary>
      protected override string getFREQ_BAND_WIDTH()
      {
         double bw = stationObj.GetD("BW");
         if (bw != IM.NullD)
            bw *= 1000.0; //#3951
         return (bw != IM.NullD) ? bw.Round(0).ToString() : "";
      }
      //===================================================
      /// <summary>
      /// Возвращает группу пользователей
      /// </summary>
      protected override EEntrpGroupType GetEntrpGroupType()
      {
         if (CMonitoringAppl.GetListOfLicence(applID).Count > 0)
            return EEntrpGroupType.DTRP;
         return EEntrpGroupType.TECH;
      }
      //===================================================
      /// <summary>
      /// Возвращает класс станции
      /// </summary>
      protected override string getStationClass()
      {
         return "BT";
      }
      #endregion
   }
}
