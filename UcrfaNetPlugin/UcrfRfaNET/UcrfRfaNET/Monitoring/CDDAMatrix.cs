﻿using System.Collections.Generic;
using System.Xml.Linq;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.Monitoring
{
   internal class CDDAMatrix
   {
      public enum polar{V, H};
      struct strctDiag
      {
         public int angle;
         public double val;
      }
      //===================================================
      public static void WriteDDAMatrix(XElement root, int stationID, int antennaID, polar pol, string diag, double gain)
      {
         List<strctDiag> data = new List<strctDiag>();
         string[] splitDiag = diag.Split(' ');
         if (splitDiag.Length == 0)
            return;

         switch (splitDiag[0])
         {
            case "VECTOR":
               if (splitDiag.Length > 2)
               {
                  int step = splitDiag[1].ToInt32(IM.NullI);
                  if (step == IM.NullI)
                     break;

                  int angle = 0;
                  for (int i = 2; i < splitDiag.Length; i++)
                  {
                     double val = splitDiag[i].ToDouble(IM.NullD);
                     if(val != IM.NullD)
                        data.Add(new strctDiag(){angle=angle, val=(gain-val)});
                     angle += step;
                  }
               }
               break;
            case "OMNI":
               for (int i = 0; i < 36; i++)
                  data.Add(new strctDiag() { angle = i*10, val = gain });
               break;
         }
         WriteDDAMatrix(root, stationID, antennaID, data, pol);
      }
      //===================================================
      private static void WriteDDAMatrix(XElement root, int stationID, int antennaID, List<strctDiag> data, polar pol)
      {
         if (data.Count == 0)
            return;

         foreach(strctDiag item in data)
         {
            string ptrFormat = string.Format("DDAMATRIX_{0}_ANT{1}_STAT{2}_ANGL{3}", pol, antennaID, stationID, item.angle);
            int glID = CExternalID.GetStringExtID(ptrFormat);
            ////
            XElement elem = new XElement("DDA_MATRIX");
            XElement temp = new XElement("ID");
            temp.SetValue(glID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("REF_ID");
            temp.SetValue(antennaID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("ANGLE");
            temp.SetValue((int)(item.angle*10.0));
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NVALUE");
            temp.SetValue((int)(item.val * 10.0));
            elem.Add(temp);
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, glID);
            if (pol == polar.H)
               root.Element("DDAHMatrix").Add(elem);
            else if (pol == polar.V)
               root.Element("DDAVMatrix").Add(elem);
         }
      }
   }
}
