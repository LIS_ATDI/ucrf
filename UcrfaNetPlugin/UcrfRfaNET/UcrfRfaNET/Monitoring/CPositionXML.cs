﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CPositionXML
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         countryesList.Clear();
         provincesList.Clear();
         subprovincesList.Clear();
         cityTypesList.Clear();
         cityesList.Clear();
         streetList.Clear();
         streetTypeList.Clear();
      }
      //======================================================
      private static List<int> countryesList = new List<int>();
      private static List<int> provincesList = new List<int>();
      private static List<int> subprovincesList = new List<int>();
      private static List<int> cityTypesList = new List<int>();
      private static List<int> cityesList = new List<int>();
      private static List<int> streetList = new List<int>();
      private static List<int> streetTypeList = new List<int>();
      //===================================================
      /// <summary>
      /// Записываем позицию из таблицы POSITION_XXX
      /// </summary>
      /// <param name="root"></param>
      /// <param name="posPtr"></param>
      /// <param name="cityID"></param>
      /// <param name="countryID"></param>
      /// <param name="address"></param>
      public static void WritePosition(XElement root, RecordPtr posPtr, 
         string regionOgUse,
         out int cityID,
         out int cityTypeID,
         out int subprovinceID,
         out int provinceID,
         out int countryID, 
         out string address,
         out int StreetId, 
         out int StreetTypeId,
         out string Bldng)
      {
         cityID = IM.NullI;
         cityTypeID = IM.NullI;
         subprovinceID = IM.NullI;
         provinceID = IM.NullI;
         countryID = IM.NullI;
         address = "";
         StreetId = IM.NullI;
         StreetTypeId = IM.NullI;
         Bldng = "";

         using (LisProgressBar pb = new LisProgressBar("Position"))
         {
            pb.SetSmall(string.Format("{0}_{1}", posPtr.Table, posPtr.Id));
            IMRecordset rsPosition = null;
            if (!string.IsNullOrEmpty(posPtr.Table) && posPtr.Id != IM.NullI)
                rsPosition = new IMRecordset(posPtr.Table, IMRecordset.Mode.ReadOnly);
            try
            {
                string country = "";
                string province = "";
                string subprovince = "";
                string city = "";
                string cityType = "";
                string code = "";
                string streetType = "";
                string street = "";
                    
                int cityIDPos = IM.NullI;
                if (rsPosition != null)
                {
                    rsPosition.Select("CITY_ID,COUNTRY_ID,PROVINCE,SUBPROVINCE,CITY,ADDRESS,CUST_TXT3,CUST_TXT4,CUST_TXT8");
                    rsPosition.SetWhere("ID", IMRecordset.Operation.Eq, posPtr.Id);
                    try
                    {
                        rsPosition.Open();
                        if (rsPosition.IsEOF())
                            throw new Exception("No such position in database");
                        cityIDPos = rsPosition.GetI("CITY_ID");
                    }
                    catch
                    {
                        CLogs.WriteError(ELogsWhat.Monitoring, string.Format("Can't load the position {0} ID={1}", posPtr.Table, posPtr.Id));
                        return;
                    }                    
                    //------
                    if (cityIDPos != IM.NullI)
                    {
                        IMObject rsCity = null;
                        try
                        {
                            rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, cityIDPos);
                        }
                        catch
                        {
                            //TODO Написать сообщение об ошибке
                            cityIDPos = IM.NullI;
                            rsCity = null;
                        }
                        if (rsCity != null)
                        {
                            country = rsCity.GetS("COUNTRY");
                            province = rsCity.GetS("PROVINCE");
                            subprovince = rsCity.GetS("SUBPROVINCE");
                            city = rsCity.GetS("NAME");
                            cityType = rsCity.GetS("CUST_TXT1");
                            code = rsCity.GetS("CODE");
                        }
                    }
                    else // (cityIDPos == IM.NullI)
                    {
                        country = rsPosition.GetS("COUNTRY_ID");
                        province = rsPosition.GetS("PROVINCE");
                        subprovince = rsPosition.GetS("SUBPROVINCE");
                        city = rsPosition.GetS("CITY");
                        cityType = "";
                        code = "";
                    }
                    address = rsPosition.GetS("ADDRESS");
                    streetType = rsPosition.GetS("CUST_TXT4");
                    street = rsPosition.GetS("CUST_TXT3");
                    Bldng = rsPosition.GetS("CUST_TXT8");
                }
                if (string.IsNullOrEmpty(province))
                    province = regionOgUse;
                //--------
                WritePositionXML(root, country, province, subprovince, city, cityType, code, street, streetType, out countryID, out provinceID, out subprovinceID, out cityID, out cityTypeID, out StreetId, out StreetTypeId);
            }
            finally
            {
                if (rsPosition != null)
                    rsPosition.Destroy();
            }
         }
      }
      //===================================================
      /// <summary>
      /// Записывает позицию из таблицы USER
      /// </summary>
      /// <param name="root"></param>
      /// <param name="posPtr"></param>
      /// <param name="cityID"></param>
      /// <param name="countryID"></param>
      public static void WritePositionUser(XElement root, RecordPtr posPtr,
         out int cityID,
         out int cityTypeID,
         out int subprovinceID,
         out int provinceID,
         out int countryID)
      {
         using (LisProgressBar pb = new LisProgressBar("User position"))
         {
            pb.SetSmall(string.Format("{0}_{1}", posPtr.Table, posPtr.Id));
            cityID = IM.NullI;
            cityTypeID = IM.NullI;
            subprovinceID = IM.NullI;
            provinceID = IM.NullI;
            countryID = IM.NullI;
            IMObject rsUser = null;
            try
            {
               rsUser = IMObject.LoadFromDB(posPtr);
            }
            catch
            {
               CLogs.WriteError(ELogsWhat.Monitoring, string.Format("Can't load the record {0} ID={1}", posPtr.Table, posPtr.Id));
            }
            if (rsUser == null)
               return; //Дальше нам нечего делать
            //------
            string country = "";
            string province = "";
            string subprovince = "";
            string city = "";
            string cityType = "";
            string code = "";
            int cityIDPos = rsUser.GetI("CITY_ID");
            if (cityIDPos != IM.NullI)
            {
               IMObject rsCity = null;
               try
               {
                  rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, cityIDPos);
               }
               catch
               {
                  //TODO Написать сообщение об ошибке
                  cityIDPos = IM.NullI;
                  rsCity = null;
               }
               if (rsCity != null)
               {
                  country = rsCity.GetS("COUNTRY");
                  province = rsCity.GetS("PROVINCE");
                  subprovince = rsCity.GetS("SUBPROVINCE");
                  city = rsCity.GetS("NAME");
                  cityType = rsCity.GetS("CUST_TXT1");
                  code = rsCity.GetS("CODE");
               }
            }
            if (cityIDPos == IM.NullI)
            {
               country = rsUser.GetS("COUNTRY_ID");
               province = rsUser.GetS("PROVINCE");
               subprovince = rsUser.GetS("SUBPROVINCE");
               city = rsUser.GetS("CITY");
               cityType = "";
               code = "";
            }
            //--------
            int streetId = IM.NullI;
            int streetTypeId = IM.NullI;
            WritePositionXML(root, country, province, subprovince, city, cityType, code, "", "", out countryID, out provinceID, out subprovinceID, out cityID, out cityTypeID, out streetId, out streetTypeId);
         }
      }
      //===================================================
      /// <summary>
      /// Записывает данные об позиции
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="cityID"></param>
      /// <param name="countryID"></param>
      /// <param name="country"></param>
      /// <param name="province"></param>
      /// <param name="subprovince"></param>
      /// <param name="city"></param>
      /// <param name="cityType"></param>
      private static void WritePositionXML(XElement root, 
         string country, 
         string province, 
         string subprovince, 
         string city, 
         string cityType,
         string code,
         string street,
         string streetType,
         out int countryID,
         out int provinceID,
         out int subprovinceID,
         out int cityID, 
         out int cityTypeID,
         out int streetId,
         out int streetTypeId)
      {
         {//Вытаскиваем полное имя страны
            string fileName = "Country";
            IMEriLOV eriData = new IMEriLOV(fileName);
            string freDescr = null, freBmp = null;
            eriData.GetEntry(country, ref freDescr, ref freBmp);
            if (!string.IsNullOrEmpty(freDescr))
               country = freDescr;
         }
         //--------
         countryID = IM.NullI;
         if (!string.IsNullOrEmpty(country))
         {
             countryID = CExternalID.GetCountryExtID(country);
             if (countryesList.Contains(countryID) == false)
             {
                 XElement elem = new XElement("COUNTRY");
                 XElement temp = new XElement("COUNTRYID");
                 temp.SetValue(countryID);
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 temp = new XElement("NAME");
                 temp.SetValue(country.Cut(64).ToStrXMLMonitoring());
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 CExternalID.GetItemKey(elem, countryID);
                 root.Element("Countrys").Add(elem);
                 countryesList.Add(countryID);
             }
         }
         //-------
         if (!string.IsNullOrEmpty(province))
         {
            int sepIdx = province.IndexOf(';');
             if (sepIdx >= 0)
                 province = province.Cut(sepIdx);
         }
         provinceID = IM.NullI;
         if (!string.IsNullOrEmpty(province))
         {
             provinceID = CExternalID.GetProvinceExtID(province);
             if (provincesList.Contains(provinceID) == false)
             {
                 XElement elem = new XElement("AREA");
                 XElement temp = new XElement("AREAID");
                 temp.SetValue(provinceID);
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 if (countryID != IM.NullI)
                     elem.Add(new XElement("COUNTRYID", countryID));
                 ///////////////////////////////////////////
                 temp = new XElement("NAME");
                 temp.SetValue(province.ToStrXMLMonitoring());
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 CExternalID.GetItemKey(elem, provinceID);
                 root.Element("Areas").Add(elem);
                 provincesList.Add(provinceID);
             }
         }
         //------
         subprovinceID = IM.NullI;
         if (!string.IsNullOrEmpty(subprovince))
         {
             subprovinceID = CExternalID.GetSubprovinceExtID(subprovince, provinceID);
             if (subprovincesList.Contains(subprovinceID) == false)
             {
                 XElement elem = new XElement("DISTRICT");
                 XElement temp = new XElement("DISTRICTID");
                 temp.SetValue(subprovinceID);
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 if (provinceID != IM.NullI)
                 {
                     temp = new XElement("AREAID");
                     temp.SetValue(provinceID);
                     elem.Add(temp);
                 }
                 ///////////////////////////////////////////
                 temp = new XElement("NAME");
                 temp.SetValue(subprovince.ToStrXMLMonitoring());
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 CExternalID.GetItemKey(elem, subprovinceID);
                 root.Element("Districts").Add(elem);
                 subprovincesList.Add(subprovinceID);
             }
         }
         //-------
         cityTypeID = IM.NullI;
         if (!string.IsNullOrEmpty(cityType))
         {
             cityTypeID = CExternalID.GetCityTypeExtID(cityType);
             if (cityTypesList.Contains(cityTypeID) == false)
             {
                 XElement elem = new XElement("TYPECITY");
                 XElement temp = new XElement("TYPECITYID");
                 temp.SetValue(cityTypeID);
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 temp = new XElement("NAME");
                 temp.SetValue(cityType.ToStrXMLMonitoring());
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 string shortCityType = GetShortCityType(cityType);
                 if (!string.IsNullOrEmpty(shortCityType))
                 {
                     temp = new XElement("SHORTNAME");
                     temp.SetValue(shortCityType.Cut(16).ToStrXMLMonitoring());
                     elem.Add(temp);
                 }
                 ///////////////////////////////////////////
                 CExternalID.GetItemKey(elem, cityTypeID);
                 root.Element("TypeCitys").Add(elem);
                 cityTypesList.Add(cityTypeID);
             }
         }
         //-------
         cityID = IM.NullI;
         if (!string.IsNullOrEmpty(city))
         {
             cityID = CExternalID.GetCityExtID(city, cityTypeID, subprovinceID, provinceID, countryID);
             if (cityesList.Contains(cityID) == false)
             {
                 XElement elem = new XElement("CITY");
                 XElement temp = new XElement("CITYID");
                 temp.SetValue(cityID);
                 elem.Add(temp);
                 ///////////////////////////////////////////
                 if (cityTypeID != IM.NullI)
                     elem.Add(new XElement("TYPECITYID", cityTypeID));
                 if (provinceID != IM.NullI)
                     elem.Add(new XElement("AREAID", provinceID));
                 if (subprovinceID != IM.NullI)
                     elem.Add(new XElement("DISTRICTID", subprovinceID));
                 ///////////////////////////////////////////
                 elem.Add(new XElement("NAME", city.ToStrXMLMonitoring()));
                 ///////////////////////////////////////////
                 if (string.IsNullOrEmpty(code) == false)
                 {
                     temp = new XElement("CODE");
                     temp.SetValue(code.ToStrXMLMonitoring());
                     elem.Add(temp);
                 }
                 ///////////////////////////////////////////
                 CExternalID.GetItemKey(elem, cityID);
                 root.Element("Citys").Add(elem);
                 cityesList.Add(cityID);
             }
         }
         streetTypeId = IM.NullI;
         if (!string.IsNullOrEmpty(streetType))
         {
             streetTypeId = CExternalID.GetStreetTypeId(streetType);
             if (streetTypeList.Contains(streetTypeId) == false)
             {
                 XElement elm = new XElement("TYPESTREET");
                 elm.Add(new XElement("TYPESTREETID", streetTypeId));
                 elm.Add(new XElement("NAME", streetType));
                 CExternalID.GetItemKey(elm, streetTypeId);
                 root.Element("TypeStreets").Add(elm);
                 streetTypeList.Add(streetTypeId);
             }
         }
         streetId = IM.NullI;
         if (!string.IsNullOrEmpty(street))
         {
             streetId = CExternalID.GetStreetId(cityID, streetType, street);
             if (streetList.Contains(streetId) == false)
             {
                 XElement elm = new XElement("STREET");
                 elm.Add(new XElement("STREETID", streetId));
                 if (streetTypeId != IM.NullI)
                    elm.Add(new XElement("TYPESTREETID", streetTypeId));
                 if (cityID != IM.NullI)
                     elm.Add(new XElement("CITYID", cityID));
                 elm.Add(new XElement("NAME", street));
                 CExternalID.GetItemKey(elm, streetId);
                 root.Element("Streets").Add(elm);
                 streetList.Add(streetId);
             }
         }
      }
      //===================================================
      /// <summary>
      /// Возвращает котроткое название сипа населен. пункта
      /// </summary>
      /// <param name="cityType">длиное название типа нас.пункта</param>
      /// <returns>короткое название типа насел. пункта или пустую строку</returns>
      private static string GetShortCityType(string cityType)
      {
         string retVal = "";
         switch (cityType)
         {
            case "місто":
               retVal = "м.";
               break;
            case "село":
               retVal = "с.";
               break;
            case "селище міського типу":
               retVal = "смт";
               break;
            case "селище":
               retVal = "с-ще";
               break;
         }
         return retVal;
      }
   }
}
