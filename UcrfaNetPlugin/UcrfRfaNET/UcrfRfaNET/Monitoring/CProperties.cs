﻿using System.Xml.Linq;
using System.Collections.Generic;


namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CProperties
   {
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         lstProp.Clear();
      }
      //===================================================
      //
      public enum PropertyType : byte
      {
         ptStatic = 0,
         ptIn = 1,
         ptOut = 2,
         ptInAuto = 3,
         ptInExtUpd = 4
      }
      //===================================================
      //Список уже занесенных записей
      private static List<int> lstProp = new List<int>();
      //===================================================
      /// <summary>
      /// Создает элемент Properties (PROP) в XML (исключает дубликаты)
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="ID">поле Properties.PROP.ID</param>
      /// <param name="type">поле Properties.PROP.TYPE</param>
      /// <returns>возращает DBGuids.DBGUID.DBGUID</returns>
      public static int AddProperty(XElement root, int ID, PropertyType type)
      {
         if (lstProp.Contains(ID) == true)
            return lstProp.IndexOf(ID);
         //----
         //Создаем новую запись
         XElement elem = new XElement("PROP");
         ///////////////////////////////////////////
         XElement temp = new XElement("ID");
         temp.SetValue(ID);
         elem.Add(temp);
         ///////////////////////////////////////////
         temp = new XElement("TYPE");
         temp.SetValue((byte)type);
         elem.Add(temp);
         ///////////////////////////////////////////
         root.Element("Properties").Add(elem);
         lstProp.Add(ID);
         return ID;
      }
   }
}
