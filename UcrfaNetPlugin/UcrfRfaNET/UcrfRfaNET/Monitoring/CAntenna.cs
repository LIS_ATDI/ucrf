﻿using System.Xml.Linq;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CAntenna
   {
      public class AntennaData
      {
         public string Ddah { get; set; }
         public string Ddav { get; set; }
         public string Polar { get; set; }
         //================================
         public AntennaData()
         {
            Ddah = "";
            Ddav = "";
            Polar = "";
         }
      }
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictAntenna.Clear();
      }
      //======================================================
      private static Dictionary<string, int> dictAntenna = new Dictionary<string, int>();
      //======================================================
      /// <summary>
      /// Создает элемент в XML о новой антенне. Исключает дубликаты
      /// </summary>
      /// <param name="root">XElement</param>
      /// <param name="ptr">Record pointer</param>
      /// <returns>ID of a new element, else NULLI</returns>
      public static int WriteNewAntenna(XElement root, RecordPtr ptr)
      {
         string ptrStr = string.Format("{0}_{1}", ptr.Table, ptr.Id);
         AntennaData exData = new AntennaData();
         //-----
         string name = "Невідома антена";
         double gain = IM.NullD;
         {
            try
            {
               using (IMObject ant = IMObject.LoadFromDB(ptr))
               {
                  name = ant.GetS("NAME");
                  gain = ant.GetD("GAIN");
                  string tmp = ant.GetS("DIAGH");
                  if(tmp.Contains("WIEN "))
                     exData.Ddah = tmp.Substring(5);
                  tmp = ant.GetS("DIAGV");
                  if (tmp.Contains("WIEN "))
                     exData.Ddav = tmp.Substring(5);
                  exData.Polar = ant.GetS("POLARIZATION");
               }
            }
            catch
            {
            }
         }
         return WriteNewAntenna(root, ptrStr, name, gain, "", exData);
      }
      //===================================================
      public static int WriteNewAntenna(XElement root, RecordPtr ptr, double gain, string polar)
      {
         string ptrStr = string.Format("{0}_{1}_{2}", ptr.Table, ptr.Id, gain);
         //-----
         string name = "Невідома антена";
         {
            try
            {
               using (IMObject ant = IMObject.LoadFromDB(ptr))
               {
                  name = ant.GetS("NAME");
               }
            }
            catch
            {
            }
         }
         return WriteNewAntenna(root, ptrStr, name, gain, polar, new AntennaData());
      }
      //===================================================
      private static int WriteNewAntenna(XElement root, string hashName, string name, double gain, string polar, AntennaData exData)
      {
         using (LisProgressBar pb = new LisProgressBar("Antenna"))
         {
            //-----
            string ptrStr = hashName;
            pb.SetSmall(ptrStr);
            if (dictAntenna.ContainsKey(ptrStr))
               return dictAntenna[ptrStr];
            //-----
            string tmpStr;
            //Создаем новый элемент
            int globID = CExternalID.GetStringExtID(hashName);
            //----
            XElement elem = new XElement("ANTENNA");
            XElement temp = new XElement("ANTENNAID");
            temp.SetValue(globID);
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NAME");
            temp.SetValue(name.ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            temp = new XElement("NTYPE");
            temp.SetValue(name.ToStrXMLMonitoring());
            elem.Add(temp);
            ///////////////////////////////////////////
            if (gain != IM.NullD)
            {
               temp = new XElement("GAIN");
               temp.SetValue(gain);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(exData.Ddah))
            {
               temp = new XElement("DDAH");
               temp.SetValue(exData.Ddah.Cut(8));
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            if (!string.IsNullOrEmpty(exData.Ddav))
            {
               temp = new XElement("DDAV");
               temp.SetValue(exData.Ddav.Cut(8));
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            tmpStr = "";
            if (!string.IsNullOrEmpty(exData.Polar))
               tmpStr = exData.Polar;
            else if (!string.IsNullOrEmpty(polar))
               tmpStr = polar;
            if (!string.IsNullOrEmpty(tmpStr))
            {
               temp = new XElement("POLARIZATION");
               temp.SetValue(tmpStr);
               elem.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elem, globID);
            root.Element("Antennas").Add(elem);
            //-----
            dictAntenna.Add(ptrStr, globID);
            return globID;
         }
      }
   }
}
