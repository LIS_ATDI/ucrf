﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using XICSM.UcrfRfaNET.ApplSource;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CMicrowa : BaseStationXML
   {
      private struct roleSration
      {
         public string role;
         public int id;
      }

      private BaseAppClass ApplClass { get; set; }
      private List<int> listStations = new List<int>();
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CMicrowa(int _applID, BaseAppClass applClass, XElement root, RecordPtr rec, List<int> lstStation)
         : base(root, rec, _applID)
      {
         ApplClass = applClass;
         for (int i = 1; i < lstStation.Count; i++)
         {
            int item = lstStation[i];
            if (item != IM.NullI)
               listStations.Add(item);
         }
      }
      //===================================================
      /// <summary>
      /// Запуска
      /// </summary>
      public override void Export()
      {
         List<roleSration> lstMicrowsID = new List<roleSration>();
         {
            int microwaID = currentPtr.Id;
            IMRecordset rs = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,ROLE");
            rs.SetWhere("MW_ID", IMRecordset.Operation.Eq, microwaID);
            rs.OrderBy("ROLE", OrderDirection.Ascending);
            try
            {
               for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
               {
                  roleSration item;
                  item.id = rs.GetI("ID");
                  item.role = rs.GetS("ROLE");
                  lstMicrowsID.Add(item);
               }
            }
            finally
            {
               rs.Destroy();
            }
            //------
            if (lstMicrowsID.Count == 0)
            {
               CLogs.WriteWarning(ELogsWhat.Monitoring, string.Format(CLocaliz.TxT("Can't find the records in the MICROWS where \"MW_ID\"={0}"), microwaID));
               return;
            }
         }
         //------
         int stationDBGUID = CDBGuids.AddSTATIONS(_root);
         int microwaDBGUID = CDBGuids.AddWicrowa(_root);
         int propertyID = CProperties.AddProperty(_root, microwaDBGUID, CProperties.PropertyType.ptStatic);
         int globIDMicrowa = CExternalID.GetRecordPtrExtID(currentPtr);
         //------
         foreach (roleSration microwsID in lstMicrowsID)
         {
            CMicrows bxml = new CMicrows(_root, new RecordPtr(ICSMTbl.itblMicrows, microwsID.id), applID);
            bxml.Export();
            foreach(int item in listStations)
            {
               int stId = IM.NullI;
               IMRecordset rs = new IMRecordset(ICSMTbl.itblMicrows, IMRecordset.Mode.ReadOnly);
               rs.Select("ID");
               rs.SetWhere("MW_ID", IMRecordset.Operation.Eq, item);
               rs.SetWhere("ROLE", IMRecordset.Operation.Eq, microwsID.role);
               try
               {
                  for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                     stId = rs.GetI("ID");
               }
               finally
               {
                  rs.Destroy();
               }
               if(stId != IM.NullI)
                  bxml.AddFreq(stId);
            }
            CMonitoringAppl.SaveLicense(applID, bxml.GlobalStationID, _root, ApplClass.OwnerID);
            CMonitoringAppl.WriteDocuments(_root, bxml.GlobalStationID, ApplClass);
            CPropertyValues.AddPropertyValue(_root, propertyID, bxml.GlobalStationID, stationDBGUID, globIDMicrowa, IM.NullI);
         }
      }
      //===================================================
      #region Virtual methods (Не используется)
      //---------
      protected override int getOwnerID()
      {
         return IM.NullI;
      }
      //---------
      protected override string getName()
      {
         return "";
      }
      //---------
      protected override string getCallSign()
      {
         return "";
      }
      //---------
      protected override string getStatus()
      {
         return "";
      }
      //---------
      protected override int getSystemID()
      {
         return IM.NullI;
      }
      //---------
      protected override double getLongitudeDEC()
      {
         return IM.NullD;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         return IM.NullD;
      }
      //--------
      protected override double getAltitude()
      {
         return IM.NullD;
      }
      //--------
      protected override int getTechID()
      {
         return IM.NullI;
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr();
      }
      #endregion
   }
}
