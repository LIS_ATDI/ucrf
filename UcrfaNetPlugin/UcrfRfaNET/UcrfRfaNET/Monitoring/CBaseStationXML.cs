﻿using System.Xml.Linq;
using Lis.CommonLib.Extensions;
using ICSM;
using System;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public enum EStatusStation
   {
      ssJob = 0,      //рабочая
      ssArchive = 1,  //нерабочая
      ssRequest = 2,  //заявка
      ssOperative = 3,//заключение ЭМС
      ssTest = 4,     //Тестовая
      ssUnknown = 5,  //Неизвестная
      ssStorage = 6   //складская
   }

   public interface IXMLExport
   {
      void Export();
      void Export(RecordPtr recPointer);
   }

   public abstract class BaseStationXML : IXMLExport
   {
      protected int globalStationID = IM.NullI;
      protected int applID { get; set; }
      public int GlobalStationID { get { return globalStationID; } }
      protected XElement _root;
      protected RecordPtr currentPtr;
      protected IMObject stationObj = null;
      protected double BW = IM.NullD; // MHz
      public BaseStationXML(XElement root, RecordPtr rec, int _applID)
      {
         applID = _applID;
         _root = root;
         currentPtr = rec;
         try
         {
             if ((rec.Table != PlugTbl.XfaAbonentStation) && (rec.Table != PlugTbl.XfaAmateur))
                stationObj = IMObject.LoadFromDB(currentPtr);
         }
         catch (System.Exception e)
         {
            stationObj = null;
            CLogs.WriteError(ELogsWhat.Monitoring, string.Format("Can't load the station TABLE_NAME={0} ID={1}: {2}", currentPtr.Table, currentPtr.Id, e.Message));
         }
      }

      #region Virtual functions
      protected virtual void Reinit(RecordPtr recPtr) { }
      /// <summary>
      /// Возвращает ID владельца
      /// </summary>
      protected virtual int getOwnerID()
      {
          int ownerID = stationObj.GetI("OWNER_ID");
          return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      /// <summary>
      /// Возвращает название станции
      /// </summary>
      protected virtual string getName()
      {
          return stationObj.GetS("NAME");
      }
      /// <summary>
      /// Возвращает позывной
      /// </summary>
      protected virtual string getCallSign()
      {
          return stationObj.GetS("CALL_SIGN");
      }
      /// <summary>
      /// Возвращает статус станции
      /// </summary>
      protected virtual string getStatus()
      {
          return stationObj.GetS("STATUS");
      }
      /// <summary>
      /// Возвращает ID Системы (стандарта) (1)
      /// </summary>
      protected virtual int getSystemID()
      {
          return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      /// <summary>
      /// Вернуть ID технологии
      /// </summary>
      /// <returns></returns>
      protected virtual int getTechID()
      {
          return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      /// <summary>
      /// Долгота
      /// </summary>
      protected abstract double getLongitudeDEC();
      /// <summary>
      /// Широта
      /// </summary>
      /// <returns></returns>
      protected abstract double getLatitudeDEC();
      /// <summary>
      /// Возвращает высоту установки антены над уровнем земли
      /// </summary>
      protected abstract double getAltitude();
      /// <summary>
      /// Возвращает RecordPtr для позиции
      /// </summary>
      protected abstract RecordPtr getPosition();
      protected virtual int GetNetId() { return IM.NullI; }
      /// <summary>
      /// Возвращает VideoColor для аналогового телевидиния
      /// </summary>
      protected virtual string getVideoColor() { return ""; }
      /// <summary>
      /// Возвращает VideoSys для аналогового телевидиния
      /// </summary>
      protected virtual string getVideoSys() { return ""; }
      /// <summary>
      /// Возвращает смещение несущей для аналогового телевидиния (KHz)
      /// </summary>
      protected virtual double getOffsetCarrier() { return IM.NullD; }
      /// <summary>
      /// Возвращает класс станции
      /// </summary>
      protected virtual string getStationClass() { return ""; }
      /// <summary>
      /// Возвращает тип станции
      /// </summary>
      protected virtual string getStationType() { return ""; }
      /// <summary>
      /// Возвращает FREQ_BAND_WIDTH
      /// </summary>
      protected virtual string getFREQ_BAND_WIDTH() 
      {
          return (BW != IM.NullD) ? IM.RoundDeci(BW * 1000, 1).ToString() : "";
      }
      /// <summary>
      /// Возвращает SATELLITE
      /// </summary>
      protected virtual string GetSatellite() { return ""; }
      /// <summary>
      /// Возвращает TRANSMITTER_COUNT
      /// </summary>
      protected virtual int GetTransmitterCount() { return IM.NullI; }
      /// <summary>
      /// Возвращает COM_SYS_ID (ссылку коммуникацтонный стандарт)
      /// </summary>
      protected virtual int GetComSysId() { return IM.NullI; }
      /// <summary>
      /// Возвращает PERM_STN_CLASS_ID
      /// </summary>
      protected virtual int GetPrmStnClassId() 
      {
          int retVal = IM.NullI;
          string stnClassName = GetPermStnClassName();
          if (!string.IsNullOrEmpty(stnClassName))
          {
              CPermStnClass.PermStnClassData exData = new CPermStnClass.PermStnClassData();
              exData.Name = stnClassName;
              retVal = CPermStnClass.WriteNewPrmStnClass(_root, exData);
          }
          return retVal;
      }

      protected virtual string GetPermStnClassName()
      {
          return "";
      }
      /// <summary>
      /// Судно!
      /// </summary>
      protected virtual int GetShipId() { return IM.NullI; }
      /// <summary>
      /// Номер автомобиля для возимых
      /// </summary>
      /// <returns></returns>
      protected virtual string GetCarRegNum() { return null; }
      /// <summary>
      /// Возвращает группу пользователей
      /// </summary>
      protected virtual EEntrpGroupType GetEntrpGroupType()
      {
         if (CMonitoringAppl.GetListOfLicence(applID).Count > 0)
            return EEntrpGroupType.LIC;
         return EEntrpGroupType.TECH;
      }
      #endregion

      protected XElement elStation = null;
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public virtual void Export()
      {
         double tmpDouble;
         int tmpInt;
         string tmpString = "";
         globalStationID = CExternalID.GetRecordPtrExtID(currentPtr);
         elStation = new XElement("STATION");
         {
            /////////////////////////////////
            XElement temp = new XElement("STATIONID");
            temp.SetValue(globalStationID);
            elStation.Add(temp);

            /////////////////////////////////
            tmpInt = getOwnerID();
            if (tmpInt.IsNotNullI())
            {
               temp = new XElement("ENTRPRID");
               temp.SetValue(tmpInt);
               elStation.Add(temp);
            }
            /////////////////////////////////
            AddElement(elStation, "NET_ID", GetNetId());
            /////////////////////////////////
            tmpString = getName();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("STATIONNAME");
               temp.SetValue(tmpString.Cut(32).ToStrXMLMonitoring());
               elStation.Add(temp);
            }
            /////////////////////////////////
            tmpString = getStationClass();
            if (!string.IsNullOrEmpty(tmpString))
            {
                temp = new XElement("STATIONCLASS");
                temp.SetValue(tmpString.Cut(16).ToStrXMLMonitoring());
                elStation.Add(temp);
            }
            /////////////////////////////////
            tmpString = getCallSign();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("NICK_NAME");
               temp.SetValue(tmpString.Cut(256).ToStrXMLMonitoring());
               elStation.Add(temp);
            }

            AddElement(elStation, "NICK_TELEGRAPH", GetNickTelegraph()); // Морська (телеграф)
            //AddElement(elStation, "PERM_LIST_NUM", GetPermListNum()); // Номер бланку дозвільного документу
            
            /////////////////////////////////
            // Удалил по просьбе Радиософт
            //temp = new XElement("STATUSID");
            //temp.SetValue(getStatus().ToMStatus());
            //elStation.Add(temp);

            ///////////////////////////////////
            temp = new XElement("SYSTEMID");
            temp.SetValue(getSystemID());
            elStation.Add(temp);

            /////////////////////////////////
            tmpDouble = getLatitudeDEC();
            if (tmpDouble != IM.NullD && tmpDouble != 0.0)
            {
               temp = new XElement("LATITUDE");
               temp.SetValue(tmpDouble);
               elStation.Add(temp);
            }
            /////////////////////////////////
            tmpDouble = getLongitudeDEC();
            if (tmpDouble != IM.NullD && tmpDouble != 0.0)
            {
               temp = new XElement("LONGITUDE");
               temp.SetValue(tmpDouble);
               elStation.Add(temp);
            }
            /////////////////////////////////
            tmpDouble = getAltitude();
            if (tmpDouble.IsNotNullD() && tmpDouble != 0.0)
            {
               temp = new XElement("ALTITUDE");
               temp.SetValue((long)(tmpDouble * 100.0));
               elStation.Add(temp);
            }
            {
               //----------
               //Заполняем позицию
               int cityID = IM.NullI;
               int countryID = IM.NullI;
               int cityTypeID = IM.NullI;
               int subprovinceID = IM.NullI;
               int provinceID = IM.NullI;
               string address = "";
               int StreetId = IM.NullI;
               int StreetTypeId = IM.NullI;
               string Bldng = "";

               CPositionXML.WritePosition(_root, getPosition(), GetRegionOfUse(), out cityID, out cityTypeID, out subprovinceID, out provinceID, out countryID, out address,
                   out StreetId, out StreetTypeId, out Bldng);
               
               AddElement(elStation, "AREAID", provinceID);
               AddElement(elStation, "CITYID", cityID);
               //AddElement(elStation, "STREETID", StreetId);
               //AddElement(elStation, "STREETTYPEID", StreetTypeId);
               AddElement(elStation, "COUNTRY_ID", countryID);
               //AddElement(elStation, "BUIDINGNO", Bldng);
               AddElement(elStation, "SEATING", address, 256);
                  
            }

            AddElement(elStation, "USER", GetUserName(), 128);

            //////////////////////////////////
            tmpInt = GetTransmitterCount();
            if (tmpInt != IM.NullI)
            {
               temp = new XElement("TRANSMITTER_COUNT");
               temp.SetValue(tmpInt);
               elStation.Add(temp);
            }
            /////////////////////////////////
            tmpString = getStationType();
            if (!string.IsNullOrEmpty(tmpString))
            {
                temp = new XElement("STN_TYPE");
                temp.SetValue(tmpString.ToStrXMLMonitoring());
                elStation.Add(temp);
            }
            //////////////////////////////////
            tmpString = getVideoColor();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("VIDEO_COLOR");
               temp.SetValue(tmpString.ToStrXMLMonitoring());
               elStation.Add(temp);
            }
            //////////////////////////////////
            tmpString = getVideoSys();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("VIDEO_SYS");
               temp.SetValue(tmpString.ToStrXMLMonitoring());
               elStation.Add(temp);
            }
            //////////////////////////////////
            tmpDouble = getOffsetCarrier();
            if (tmpDouble != IM.NullD)
            {
               temp = new XElement("OFFSET_CARRIER");
               temp.SetValue((int)(tmpDouble * 1000.0));
               elStation.Add(temp);
            }
            //////////////////////////////////
            temp = new XElement("RC_TECH_ID");
            temp.SetValue(getTechID());
            elStation.Add(temp);
            //////////////////////////////////
            tmpInt = GetComSysId();
            if (tmpInt != IM.NullI)
            {
               temp = new XElement("COM_SYS_ID");
               temp.SetValue(tmpInt);
               elStation.Add(temp);
            }
            //////////////////////////////////
            tmpInt = GetPrmStnClassId();
            if (tmpInt != IM.NullI)
            {
               temp = new XElement("PERM_STN_CLASS_ID");
               temp.SetValue(tmpInt);
               elStation.Add(temp);
            }
            //////////////////////////////////
            tmpString = getFREQ_BAND_WIDTH();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("FREQ_BAND_WIDTH");
               temp.SetValue(tmpString.ToStrXMLMonitoring());
               elStation.Add(temp);
            }

            AddElement(elStation, "OPEN_DATE", GetOpenDate(), "yyyy-MM-dd");
            AddElement(elStation, "CALL_SELECTED", GetCallSelected(), 32);
            AddElement(elStation, "SEA_MOBILE_SRVC", GetSeaMobileSrvc(), 32);
            AddElement(elStation, "CORRESPONDER_CATEGORY", GetCorresponderCategory());

            //////////////////////////////////
            {
               EEntrpGroupType group = GetEntrpGroupType();
               temp = new XElement("ENTRPR_GROUP");
               temp.SetValue(group.ToString());
               elStation.Add(temp);
            }
            //////////////////////////////////
            tmpString = GetSatellite();
            if (!string.IsNullOrEmpty(tmpString))
            {
               temp = new XElement("SATELLITE");
               temp.SetValue(tmpString.Cut(255).ToStrXMLMonitoring());
               elStation.Add(temp);
            }
            /////////////////////////////////
            AddElement(elStation, "SHIP_ID", GetShipId());
            AddElement(elStation, "CAR_MODEL", GetCarModel(), 64);
            AddElement(elStation, "CAR_GOV_NUM", GetCarRegNum(), 16);

            AddElement(elStation, "CATEGORY", GetCategory());
            AddElement(elStation, "IS_SHARED", GetIsShared());

            CExternalID.GetItemKey(elStation, globalStationID);
            _root.Element("Stations").Add(elStation);
            //end stations
         }
      }

      protected virtual bool? GetIsShared()
      {
          return null;
      }

      protected virtual int? GetCategory()
      {
          return null;
      }

      protected virtual string GetSeaMobileSrvc()
      {
          return null;
      }

      protected virtual string GetCallSelected()
      {
          return null;
      }

      protected virtual string GetCorresponderCategory()
      {
          return null;
      }

      protected virtual string GetCarModel()
      {
          return null;
      }

      protected virtual string GetRegionOfUse()
      {
          return null;
      }

      protected virtual DateTime? GetOpenDate()
      {
          return null;
      }

      protected virtual string GetUserName()
      {
          return null;
      }

      protected virtual string GetIsReg()
      {
          return "";
      }

      protected virtual string GetAdSynchroNetResNumber()
      {
          return "";
      }

      protected virtual string GetAdCircuitNumber()
      {
          return "";
      }

      protected virtual string GetAdReceiveType()
      {
          return "";
      }

      protected virtual string GetPermListNum()
      {
          return "";
      }

      protected virtual string GetRcTechId()
      {
          return "";
      }

      protected virtual string GetNickTelegraph()
      {
          return "";
      }

      protected virtual string GetNickPhone()
      {
          return "";
      }

      static public void AddElement(XElement parent, string name, DateTime? value, string format)
      {
          if (value != null && value != IM.NullT)
          {
              XElement elm = new XElement(name);
              if (string.IsNullOrEmpty(format))
                  elm.SetValue(value);
              else
                  elm.SetValue(((DateTime)value).ToString(format));
              parent.Add(elm);
          }
      }

      static public void AddElement(XElement parent, string name, string value, int lengthLimit)
      {
          if (!string.IsNullOrEmpty(value))
          {
              XElement elm = new XElement(name);
              if (lengthLimit > 0)
                  value = value.Cut(lengthLimit);
              elm.SetValue(value.ToStrXMLMonitoring());
              parent.Add(elm);
          }
      }

      static public void AddElement(XElement parent, string name, object value)
      {
          //is value is not null and not one from predefined nulls
          if (value != null && !(value is int && (int)value == IM.NullI) && !(value is double && (double)value == IM.NullD) && !(value is DateTime && (DateTime)value == IM.NullT))
              parent.Add(new XElement(name, value));
      }


      #region IXMLExport Members

      public virtual void Export(RecordPtr recPointer)
      {
         Reinit(recPointer);
         Export();
      }

      #endregion
   }
}
