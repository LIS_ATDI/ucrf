﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ICSM;

namespace XICSM.UcrfRfaNET.Monitoring
{
   internal class CEarthStation : BaseStationXML
   {
      protected int stationID
      {
         get
         {
            if (stationObj != null)
               return stationObj.GetI("ID");
            return IM.NullI;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="root">Root XML element</param>
      /// <param name="rec">RecordPtr</param>
      public CEarthStation(XElement root, RecordPtr rec, int _applID)
         : base(root, rec, _applID)
      {
      }
      //===================================================
      /// <summary>
      /// Exports all data into XML
      /// </summary>
      public override void Export()
      {
         if (stationObj == null)
            return; //Прекращаем обработку, та как не загрузили станцию
         //-------------
         // Создаеться основные элементы станции
         base.Export();
         //-------------
         //Заполняем сектор
         int sectorID = IM.NullI;
         int localEquipID = IM.NullI;
         double localPwr = 0;
         {//Заполняем сектор
            string dEmi = "";
            IMRecordset estaEmi = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
            estaEmi.Select("DESIG_EMISSION,EQPCONF_ID,PWR_TOT_PK");
            estaEmi.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, stationID);
            estaEmi.SetWhere("Group.Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");
            estaEmi.SetWhere("EMI_TYPE", IMRecordset.Operation.Like, "N");
            try
            {
               estaEmi.Open();
               if (!estaEmi.IsEOF())
               {
                  dEmi = estaEmi.GetS("DESIG_EMISSION");
                  localEquipID = estaEmi.GetI("EQPCONF_ID");
                  localPwr = estaEmi.GetD("PWR_TOT_PK");
               }
            }
            finally
            {
               estaEmi.Destroy();
            }
            sectorID = CSectors.WriteSector(_root, globalStationID, dEmi);
         }
         //-------------
         //Заполняем частоты
         {
            double txFreq = IM.NullD;
            double rxFreq = IM.NullD;
            IMRecordset estaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
            estaAssgn.Select("FREQ,Group.Antenna.EMI_RCP");
            estaAssgn.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, stationID);
            //estaAssgn.SetWhere("Group.Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");
            try
            {
               for(estaAssgn.Open();!estaAssgn.IsEOF(); estaAssgn.MoveNext())
                  if(estaAssgn.GetS("Group.Antenna.EMI_RCP").ToUpper() == "E")
                     txFreq = estaAssgn.GetD("FREQ");
                  else
                     rxFreq = estaAssgn.GetD("FREQ");
            }
            finally
            {
               estaAssgn.Destroy();
            }
            if ((txFreq != IM.NullD) || (rxFreq != IM.NullD))
               CStationFreqsXML.WriteStationFreq(_root, sectorID, rxFreq, txFreq, ICSMTbl.itblEstaAssgn);
         }
         //-----------
         //Заполняем антену
         int antennaIDTx = IM.NullI;
         int antIDTx = IM.NullI;
         double localGAINTx = 0.0;
         {
            IMRecordset estaAnt = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
            estaAnt.Select("ANT_ID,GAIN");
            estaAnt.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, stationID);
            estaAnt.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "E");
            try
            {
               estaAnt.Open();
               if (!estaAnt.IsEOF())
               {
                  antIDTx = estaAnt.GetI("ANT_ID");
                  localGAINTx = estaAnt.GetD("GAIN");
               }
            }
            finally
            {
               estaAnt.Destroy();
            }
            antennaIDTx = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.itblAntenna, antIDTx), localGAINTx, "");
         }
         int antennaIDRx = IM.NullI;
         int antIDRx = IM.NullI;
         double localGAINRx = 0.0;
         {
            IMRecordset estaAnt = new IMRecordset(ICSMTbl.itblEstaAntenna, IMRecordset.Mode.ReadOnly);
            estaAnt.Select("ANT_ID,GAIN");
            estaAnt.SetWhere("ESTA_ID", IMRecordset.Operation.Eq, stationID);
            estaAnt.SetWhere("EMI_RCP", IMRecordset.Operation.Like, "R");
            try
            {
               estaAnt.Open();
               if (!estaAnt.IsEOF())
               {
                  antIDRx = estaAnt.GetI("ANT_ID");
                  localGAINRx = estaAnt.GetD("GAIN");
               }
            }
            finally
            {
               estaAnt.Destroy();
            }
            antennaIDRx = CAntenna.WriteNewAntenna(_root, new RecordPtr(ICSMTbl.itblAntenna, antIDRx), localGAINRx, "");
         }
         //-----------
         //Заполняем оборудование
         int txEquipID = CTxEquip.WriteNewTxEquip(_root, new RecordPtr(ICSMTbl.itblEquipEsta, localEquipID));
         //-----------
         {//Заполняем передатчик
            double pow = 0.0;
            string polar = "";
            double azimuth = stationObj.GetD("AZM_TO");
            double agl = stationObj.GetD("AGL");
            double gain = localGAINTx;

            IMRecordset estaGroup = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
            estaGroup.Select("POLAR_TYPE,PWR_MAX");
            estaGroup.SetWhere("Antenna.Station.ID", IMRecordset.Operation.Eq, stationID);
            estaGroup.SetWhere("Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");
            try
            {
               estaGroup.Open();
               if (!estaGroup.IsEOF())
               {
                  polar = estaGroup.GetS("POLAR_TYPE");
                  pow = estaGroup.GetD("PWR_MAX") + 30.0;
               }
            }
            finally
            {
               estaGroup.Destroy();
            }
            CTransmitter.TTransmitterData exData = new CTransmitter.TTransmitterData();
            exData.Riseangle = stationObj.GetD("ELEV_MIN");
            CTransmitter.WriteNewTransmitter(_root, new RecordPtr(), 
                                             sectorID,
                                             antennaIDTx,
                                             txEquipID,
                                             pow,
                                             azimuth,
                                             agl,
                                             polar,
                                             gain,
                                             IM.NullI,
                                             IM.NullD,
                                             antIDTx,
                                             ICSMTbl.itblAntenna,
                                             IM.NullD,
                                             null,
                                             exData);
         }
         //-----------
         {//Заполняем приемник
            string polar = "";
            double gain = localGAINRx;

            IMRecordset estaGroup = new IMRecordset(ICSMTbl.itblEstaGroup, IMRecordset.Mode.ReadOnly);
            estaGroup.Select("POLAR_TYPE,PWR_MAX");
            estaGroup.SetWhere("Antenna.Station.ID", IMRecordset.Operation.Eq, stationID);
            estaGroup.SetWhere("Antenna.EMI_RCP", IMRecordset.Operation.Like, "R");
            try
            {
               estaGroup.Open();
               if (!estaGroup.IsEOF())
               {
                  polar = estaGroup.GetS("POLAR_TYPE");
               }
            }
            finally
            {
               estaGroup.Destroy();
            }
            CReceiver.ReceiverData data = new CReceiver.ReceiverData();
            data.AntGain = gain;
            data.Polarization = polar;
            data.AntId = antennaIDRx;            
            CReceiver.WriteNewReceiver(_root, sectorID, data);
         }
      }
      //===================================================
      #region Virtual methods
      //---------
      protected override int getOwnerID()
      {
         int ownerID = stationObj.GetI("OWNER_ID");
         return CEnterprise.WriteEnterprise(_root, ownerID);
      }
      //---------
      protected override string getName()
      {
         return stationObj.GetS("NAME");
      }
      //---------
      protected override string getCallSign()
      {
         return ""; //TODO В земных станциях позывного нет?
      }
      //---------
      protected override string getStatus()
      {
         return stationObj.GetS("STATUS");
      }
      //---------
      protected override int getSystemID()
      {
         return CSystemsXML.GetSystemID(stationObj.GetS("STANDARD"), _root);
      }
      //---------
      protected override double getLongitudeDEC()
      {
         double Long = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionEs, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LONGITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Long = rs.GetD("LONGITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Long;
      }
      //---------
      protected override double getLatitudeDEC()
      {
         double Lat = IM.NullD;
         IMRecordset rs = new IMRecordset(ICSMTbl.itblPositionEs, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,LATITUDE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, stationObj.GetI("SITE_ID"));
         try
         {
            rs.Open();
            if (!rs.IsEOF())
               Lat = rs.GetD("LATITUDE");
         }
         finally
         {
            rs.Destroy();
         }
         return Lat;
      }
      //--------
      protected override double getAltitude()
      {
         return stationObj.GetD("AGL");
      }
      //--------
      protected override int getTechID()
      {
         return CRadioTech.WriteNewRadioTech(_root, stationObj.GetS("STANDARD"));
      }
      //--------
      protected override RecordPtr getPosition()
      {
         return new RecordPtr(ICSMTbl.itblPositionEs, stationObj.GetI("SITE_ID"));
      }
      //--------
      /// <summary>
      /// Возвращает SATELLITE
      /// </summary>
      protected override string GetSatellite()
      {
         double deg = stationObj.GetD("LONG_NOM").Round(6);
         string retVal = string.Format("{0} {1} {2}",
                                       stationObj.GetS("SAT_NAME"),
                                       deg,
                                       (deg < 0) ? "W" : "E");
         
         return retVal;
      }

      #endregion
   }
}
