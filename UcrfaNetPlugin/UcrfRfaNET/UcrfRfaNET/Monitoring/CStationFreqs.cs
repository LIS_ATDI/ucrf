﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;

namespace XICSM.UcrfRfaNET.Monitoring
{
   public class CStationFreqsXML
   {
      //======================================================
      /// <summary>
      /// Класс для передачи данных
      /// </summary>
      public sealed class DataFreq
      {
         public double FreqTxFromMhz { get; set; }
         public double FreqTxToMhz { get; set; }
         public double FreqRxFromMhz { get; set; }
         public double FreqRxToMhz { get; set; }
         public int ChannelFrom { get; set; }
         public int ChannelTo { get; set; }
         public int ChannelInFrom { get; set; }
         public int ChannelInTo { get; set; }
         //--------
         public DataFreq()
         {
            FreqTxFromMhz = IM.NullD;
            FreqTxToMhz = IM.NullD;
            FreqRxFromMhz = IM.NullD;
            FreqRxToMhz = IM.NullD;
            ChannelFrom = IM.NullI;
            ChannelTo = IM.NullI;
            ChannelInFrom = IM.NullI;
            ChannelInTo = IM.NullI;
         }
      }
      //======================================================
      /// <summary>
      /// Подготовка данных для создания нового XML
      /// </summary>
      public static void Clear()
      {
         dictFreq.Clear();
      }
      //======================================================
      private static Dictionary<int, int> dictFreq = new Dictionary<int, int>();
      //===================================================
      public static int WriteStationFreq(XElement root, int sectorId, double rxFreqMHz, double txFreqMHz, string stationTableName)
      {
         return WriteStationFreq(root, sectorId, rxFreqMHz, txFreqMHz, stationTableName, IM.NullI, IM.NullI);
      }
      //===================================================
      public static int WriteStationFreq(XElement root, int sectorId, double rxFreqMHz, double txFreqMHz, string stationTableName,
         int channelFrom, int channelTo)
      {
         DataFreq data = new DataFreq();
         data.FreqRxFromMhz = rxFreqMHz;
         data.FreqRxToMhz = IM.NullD;
         data.FreqTxFromMhz = txFreqMHz;
         data.FreqTxToMhz = IM.NullD;
         data.ChannelFrom = channelFrom;
         data.ChannelTo = channelTo;
         return WriteStationFreq(root, sectorId, stationTableName, data);
      }
      //===================================================
      public static int WriteStationFreq(XElement root, int sectorId, string stationTableName, DataFreq data)
      {
         // check data consistency
         if ((data.FreqRxFromMhz == IM.NullD || data.FreqRxFromMhz == 0.0)
             && (data.FreqRxToMhz == IM.NullD || data.FreqRxToMhz == 0.0)
             && (data.FreqTxFromMhz == IM.NullD || data.FreqTxFromMhz == 0.0)
             && (data.FreqTxToMhz == IM.NullD || data.FreqTxToMhz == 0.0)
             && (data.ChannelFrom == IM.NullI || data.ChannelFrom == 0)
             && (data.ChannelTo == IM.NullI || data.ChannelTo == 0)
             )
             return 0;

         using (LisProgressBar pb = new LisProgressBar("Frequency"))
         {
            pb.SetSmall("Frequency");
            
            int freqId = CExternalID.GetFreqExtId(sectorId, stationTableName, data);
            //----
            if (dictFreq.ContainsKey(freqId))
               return dictFreq[freqId];
            //----
            XElement elFreq = new XElement("FREQ");
            XElement temp = new XElement("FREQID");
            temp.SetValue(freqId);
            elFreq.Add(temp);
            ///////////////////////////////////////////
            if (sectorId != IM.NullI)
            {
               temp = new XElement("REF_ID");
               temp.SetValue(sectorId);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.FreqRxFromMhz != IM.NullD)
            {
               temp = new XElement("FREQ_IN_FROM");
               temp.SetValue((data.FreqRxFromMhz * 100000.0).Round(0));
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.FreqRxToMhz != IM.NullD)
            {
               temp = new XElement("FREQ_IN_TO");
               temp.SetValue((data.FreqRxToMhz * 100000.0).Round(0));
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.FreqTxFromMhz != IM.NullD)
            {
               temp = new XElement("FREQ_OUT_FROM");
               temp.SetValue(data.FreqTxFromMhz * 100000.0);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.FreqTxToMhz != IM.NullD)
            {
               temp = new XElement("FREQ_OUT_TO");
               temp.SetValue(data.FreqTxToMhz * 100000.0);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.ChannelFrom != IM.NullI)
            {
               temp = new XElement("CHANNEL_FROM");
               temp.SetValue(data.ChannelFrom);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.ChannelTo != IM.NullI)
            {
               temp = new XElement("CHANNEL_TO");
               temp.SetValue(data.ChannelTo);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.ChannelInFrom != IM.NullI)
            {
               temp = new XElement("CHANNEL_IN_FROM");
               temp.SetValue(data.ChannelInFrom);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            if (data.ChannelInTo != IM.NullI)
            {
               temp = new XElement("CHANNEL_IN_TO");
               temp.SetValue(data.ChannelInTo);
               elFreq.Add(temp);
            }
            ///////////////////////////////////////////
            CExternalID.GetItemKey(elFreq, freqId);
            root.Element("StationFreqs").Add(elFreq);
            //------
            dictFreq.Add(freqId, freqId);
            return freqId;
         }
      }
   }
}
