﻿using System;
using Lis.OfficeBridge;

namespace XICSM.UcrfRfaNET.Office
{
    class WordManager
    {
        /// <summary>
        /// Конвертирует RTF файл в DOC
        /// </summary>
        /// <param name="rtfFile">Полное имя RTF файла</param>
        /// <param name="docFile">Полное имя DOC файла</param>
        public static void RtfToDoc(string rtfFile, string docFile)
        {
            WordBridge word = new WordBridge();
            try
            {
                string FileName = docFile.Replace(".docx", ".doc");
                docFile = FileName;

                word.Init();
                word.OpenFile(rtfFile);
                word.SaveAs(docFile, WordBridge.WdSaveFormatOffice.WdFormatDocument);
                word.CloseDoc();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                word.Dispose();
            }
        }
    }
}
