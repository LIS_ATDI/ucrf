﻿namespace XICSM.UcrfRfaNET.AutoTest
{
   partial class FormAutoTest
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.gbAutoTestLog = new System.Windows.Forms.GroupBox();
         this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
         this.gbAutoTestLog.SuspendLayout();
         this.SuspendLayout();
         // 
         // gbAutoTestLog
         // 
         this.gbAutoTestLog.Controls.Add(this.richTextBoxLog);
         this.gbAutoTestLog.Location = new System.Drawing.Point(12, 12);
         this.gbAutoTestLog.Name = "gbAutoTestLog";
         this.gbAutoTestLog.Size = new System.Drawing.Size(632, 335);
         this.gbAutoTestLog.TabIndex = 0;
         this.gbAutoTestLog.TabStop = false;
         this.gbAutoTestLog.Text = "Auto Test Log";
         // 
         // richTextBoxLog
         // 
         this.richTextBoxLog.BackColor = System.Drawing.SystemColors.Window;
         this.richTextBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
         this.richTextBoxLog.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
         this.richTextBoxLog.Location = new System.Drawing.Point(3, 16);
         this.richTextBoxLog.Name = "richTextBoxLog";
         this.richTextBoxLog.ReadOnly = true;
         this.richTextBoxLog.Size = new System.Drawing.Size(626, 316);
         this.richTextBoxLog.TabIndex = 0;
         this.richTextBoxLog.Text = "text";
         // 
         // FormAutoTest
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(656, 359);
         this.Controls.Add(this.gbAutoTestLog);
         this.MinimizeBox = false;
         this.Name = "FormAutoTest";
         this.Text = "Auto test";
         this.gbAutoTestLog.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox gbAutoTestLog;
      private System.Windows.Forms.RichTextBox richTextBoxLog;
   }
}