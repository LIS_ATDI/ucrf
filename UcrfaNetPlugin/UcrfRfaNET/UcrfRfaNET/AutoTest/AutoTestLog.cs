﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.AutoTest
{
   /// <summary>
   /// Формирует лог автоматического тестирования
   /// </summary>
   class AutoTestLog : IDisposable
   {
      private string fileName;
      private FormAutoTest fAutoTest = null;
      private List<string> errorList = null;
      private string descrTest;
      private DateTime startDateTime;
      //===================================================
      /// <summary>
      /// Создает новый лог файл
      /// </summary>
      /// <param name="pathName">Папка для лог файла</param>
      /// <returns>TRUE - ОК, иначе FALSE</returns>
      public bool Create(string pathName)
      {
         string filename = string.Format("{0}.rtf", DateTime.Now.ToString("yyyyMMdd_hhmmss"));
         return Create(pathName, filename);
      }
      //===================================================
      /// <summary>
      /// Создает новый лог файл
      /// </summary>
      /// <param name="pathName">Папка для лог файла</param>
      /// <param name="fileName">Имя файла</param>
      /// <returns>TRUE - ОК, иначе FALSE</returns>
      public bool Create(string pathName, string fName)
      {
         fileName = string.Format("{0}\\{1}", pathName, fName);
         if(errorList == null)
            errorList = new List<string>();
         fAutoTest = new FormAutoTest();
         fAutoTest.Show();
         fAutoTest.AddLine(string.Format("Start test {0}", CurDateTime()), fAutoTest.Cstandard);
         return true;
      }
      //===================================================
      /// <summary>
      /// IDisposable Members
      /// </summary>
      public void Dispose()
      {
         if (fAutoTest != null)
         {
            fAutoTest.SaveFile(fileName);
            fAutoTest.Close();
            fAutoTest.Dispose();
         }
      }
      //===================================================
      /// <summary>
      /// Запуск нового теста (обнуляет счетчик ошибок)
      /// </summary>
      public void RunTest(string description)
      {
         if (fAutoTest == null)
            return;
         errorList.Clear();  //Обнуляем список ошибок
         descrTest = description;
         startDateTime = DateTime.Now;
      }
      //===================================================
      /// <summary>
      /// Тест закончен (выводи сообщение о результатах теста)
      /// </summary>
      public void FinishTest()
      {
         if (fAutoTest == null)
            return;
         TimeSpan dtAll = DateTime.Now.Subtract(startDateTime);
         string outMessage = string.Format("{1} / {2} {3}: {0}",
                                           descrTest,
                                           CurTime(startDateTime),
                                           string.Format("{0}:{1}:{2}.{3}", dtAll.Hours, dtAll.Minutes, dtAll.Seconds, dtAll.Milliseconds),
                                           (errorList.Count > 0) ? string.Format("Error ({0})", errorList.Count) : "OK");
         if(errorList.Count > 0)
            fAutoTest.AddLine(outMessage, fAutoTest.CerrorTest);
         else
            fAutoTest.AddLine(outMessage, fAutoTest.CokTest);
         foreach(string itemMess in errorList)
            fAutoTest.AddLine(string.Format("\t{0}", itemMess), fAutoTest.CerrorMessage);
      }
      //===================================================
      /// <summary>
      /// Выводит сообщение об ошибке
      /// </summary>
      public void ErrorMessage(string message)
      {
         errorList.Add(message);
      }
      //===================================================
      /// <summary>
      /// Возвращает строку с датой и временем
      /// </summary>
      private string CurDateTime()
      {
         return string.Format("{0} {1}", CurDate(DateTime.Now), CurTime(DateTime.Now));
      }
      //===================================================
      /// <summary>
      /// Возвращает строку с датой
      /// </summary>
      private string CurDate(DateTime dt)
      {
         return dt.ToString("yyyy.MM.dd");
      }
      //===================================================
      /// <summary>
      /// Возвращает строку с временем
      /// </summary>
      private string CurTime(DateTime dt)
      {
         return dt.ToString("hh:mm:ss");
      }
   }
}
