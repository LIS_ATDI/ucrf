﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.AutoTest
{
   public partial class FormAutoTest : Form
   {
      //===================================================
      /// <summary>
      /// Стандарты цветов сообщений
      /// </summary>
      public readonly Color Cstandard = Color.Black;
      public readonly Color CokTest = Color.Green;
      public readonly Color CerrorTest = Color.Red;
      public readonly Color CerrorMessage = Color.Red;
      //===================================================
      public FormAutoTest()
      {
         InitializeComponent();
      }
      //===================================================
      /// <summary>
      /// Сохраняем файл лога
      /// </summary>
      /// <param name="fileName">полное имя файла</param>
      /// <returns>TRUE - все ОК, иначе FALSE</returns>
      public bool SaveFile(string fileName)
      {
         bool retVal = false;
         try
         {
            richTextBoxLog.SaveFile(fileName, RichTextBoxStreamType.RichText);
            retVal = true;
         }
         catch (System.ArgumentException){}
         catch (System.IO.IOException){}
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Добавляет строку в лог с закраской
      /// </summary>
      /// <param name="line">строка</param>
      /// <param name="color">цвет строки</param>
      public void AddLine(string line, Color color)
      {
         richTextBoxLog.SelectionColor = color;
         richTextBoxLog.AppendText(line + Environment.NewLine);
         Refresh();
      }
   }
}
