﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.Documents;

namespace XICSM.UcrfRfaNET.StationsStatus
{
   /// <summary>
   /// Класс описывает структуру одного перехода
   /// </summary>
   public class StateWorkFlow
   {
      /// <summary>
      /// Состояние из которого необходимо перейти
      /// </summary>
      [Browsable(false)]
      public string StateFrom{get;set;}
      /// <summary>
      /// Тип события, по которому выполняется переход
      /// </summary>
      [Browsable(false)]
      public string TypeEction{get;set;}
      /// <summary>
      /// Событие, по которому выполняется переход
      /// </summary>
      [Browsable(false)]
      public string Action { get; set; }
      /// <summary>
      /// Состояние в которое необходимо перейти
      /// </summary>
      [Browsable(false)]
      public string StateTo { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="stateFrom">Состояние из которого выполняется переход</param>
      /// <param name="stateTo">Состояние в которое выполняется переход</param>
      /// <param name="typeEction">Тип события, по которому выполняется переход</param>
      /// <param name="action">Событие, по которому выполняется переход</param>
      public StateWorkFlow(string stateFrom, string stateTo, string typeEction, string action)
      {
         StateFrom = stateFrom;
         StateTo = stateTo;
         TypeEction = typeEction;
         Action = action;
      }
      //=================
      //Для Binding-а
      public string BindStateFrom{get { return StateFrom; }}
      public string BindStateTo { get { return StateTo; } }
      public string BindTypeEction
      {
         get
         {
            StationStatus.ETypeEvent typeEvent = StationStatus.ToTypeEvent(TypeEction);
            Dictionary<StationStatus.ETypeEvent, string> dictEvent = StationStatus.GetHumanDictionaryTypeEvent();
            if (dictEvent.ContainsKey(typeEvent) == true)
               return dictEvent[typeEvent];
            return "";
         }
      }
      public string BindEction
      {
         get
         {
            string retVal = "";
            switch (StationStatus.ToTypeEvent(TypeEction))
            {
               case StationStatus.ETypeEvent.ApplPrintDoc:
               case StationStatus.ETypeEvent.PacketPrintDoc:
               case StationStatus.ETypeEvent.AttachDoc:
                  {
                     Documents.Documents docum = new Documents.Documents();
                     string docTypeLocal = Action;
                     retVal = docum.GetHumanNameOfDoc(docTypeLocal);
                  }
                  break;
               case StationStatus.ETypeEvent.AttachAppl:
                  {
                     EPacketType packetTypeLocal = Action.ToPacketType();
                     retVal = packetTypeLocal.GetHumanNameOfPacketType();
                  }
                  break;
               case StationStatus.ETypeEvent.EventLog:
                  {
                     EDocEvent eventTypeLocal = EventConvert.ToEvent(Action);
                     retVal = EventConvert.GetHumanNameOfEvent(eventTypeLocal);
                  }
                  break;
               case StationStatus.ETypeEvent.TypeAppl:
               case StationStatus.ETypeEvent.DeleteAppl:
                  {
                     AppType applTypeLocal = appTypeConvert.StringToAppType(Action);
                     retVal = appTypeConvert.GetHumanNameOfAppType(applTypeLocal);
                  }
                  break;
            }
            return retVal;
         }
      }
   }
   //===========================================
   public class StationWorkFlow
   {
      /// <summary>
      /// Список, который описывает WorkFlow станции
      /// </summary>
      private AdvancedList<StateWorkFlow> stateWf;
      public AdvancedList<StateWorkFlow> StateWf { get { return stateWf; } }
      /// <summary>
      /// Статический конструктор
      /// </summary>
      public StationWorkFlow()
      {
         stateWf = new AdvancedList<StateWorkFlow>();
      }
      /// <summary>
      /// Добавляет новый WorkFlow к списку WorkFlow
      /// </summary>
      /// <param name="stateFrom">Состояние из которого выполняется переход</param>
      /// <param name="stateTo">Состояние в которое выполняется переход</param>
      /// <param name="typeEction">Тип события, по которому выполняется переход</param>
      /// <param name="action">Событие, по которому выполняется переход</param>
      public void AddWorkFlow(string stateFrom, string stateTo, string typeEction, string action)
      {
         // Проверка на возможность добавить такой переход.
         // Не будет ли он приводить к конфликтной ситуации, а именно:
         // с одного и того же состояния можно перейти в два разных состояния
         // по одинаковых событиях
         List<StateWorkFlow> listOfCoincidence = (from item in StateWf
                                                  where ((item.StateFrom == stateFrom) &&
                                                         (item.TypeEction == typeEction) &&
                                                         (item.Action == action))
                                                  select item).ToList();
         if (listOfCoincidence.Count > 0)
         {// есть конфликт, вываливаем ошибку
            string message = string.Format("You can't add workflow where:\nState from = {0}\nState to = {1}\nType action = {2}\nEction = {3}\nbecause this workflow conflicts with next workflow:",
                                           stateFrom, stateTo, typeEction, action);
            foreach (StateWorkFlow item in listOfCoincidence)
            {
               message += string.Format("\nState from = {0}\nState to = {1}\nType action = {2}\nEction = {3}",
                                        item.StateFrom, item.StateTo, item.TypeEction, item.Action);
            }
            throw new ArgumentException(message);
         }
         //Все нормально, добавляем workflow
         stateWf.Add(new StateWorkFlow(stateFrom, stateTo, typeEction, action));
      }
      /// <summary>
      /// Удаляет workflow по индексу
      /// </summary>
      /// <param name="index">Индекс записи</param>
      public void DeleteWorkFlow(int index)
      {
         stateWf.RemoveAt(index);
      }
      /// <summary>
      /// Возвращает состояни в которое можна перейти из определенного состояния по определенному событию
      /// </summary>
      /// <param name="stateFrom">Состояние из поторого необходимо перейти</param>
      /// <param name="typeEction">Тип события</param>
      /// <param name="action">событие</param>
      /// <returns>Новое состояние если есть переходи, иначе текущее состояние</returns>
      public string GetState(string stateFrom, string typeEction, string action)
      {
         string retVal = stateFrom;
         List<StateWorkFlow> listOfState = (from item in StateWf
                                            where ((item.StateFrom == stateFrom) &&
                                                   (item.TypeEction == typeEction) &&
                                                   (item.Action == action))
                                            select item).ToList();
         if (listOfState.Count > 0)
            retVal = listOfState[0].StateTo;
         return retVal;
      }
   }
   /// <summary>
   /// Класс описывает структуру одного автоматического перехода
   /// </summary>
   public class StateAutoWorkFlow
   {
      public string Description { get; set; }
      public string SqlQuery { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="descr">Описание запроса</param>
      /// <param name="sqlQuery">Sql запрос</param>
      public StateAutoWorkFlow(string descr, string sqlQuery)
      {
         Description = descr;
         SqlQuery = sqlQuery;
      }
      /// <summary>
      /// Выполнить запрос
      /// </summary>
      /// <param name="isSilent"></param>
      public void Execute(bool isSilent)
      {
         try
         {
            if (ICSM.IM.Execute(SqlQuery) != 0)
            {
               if (isSilent == false)
                  System.Windows.Forms.MessageBox.Show("Sql query was executed successful", "Message",
                                                       System.Windows.Forms.MessageBoxButtons.OK,
                                                       System.Windows.Forms.MessageBoxIcon.Information);
            }
            else
            {
               if (isSilent == false)
                  System.Windows.Forms.MessageBox.Show("Sql query failed", "Error",
                                                       System.Windows.Forms.MessageBoxButtons.OK,
                                                       System.Windows.Forms.MessageBoxIcon.Error);
            }
         }
         catch (Exception ex)
         {
            if (isSilent == false)
               System.Windows.Forms.MessageBox.Show(ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK,
                                                    System.Windows.Forms.MessageBoxIcon.Stop);
         }
      }
   }
   //===========================================
   public class StationAutoWorkFlow
   {
      /// <summary>
      /// Список, который описывает WorkFlow станции
      /// </summary>
      private AdvancedList<StateAutoWorkFlow> stateAwf;
      public AdvancedList<StateAutoWorkFlow> StateAwf { get { return stateAwf; } }
      /// <summary>
      /// Статический конструктор
      /// </summary>
      public StationAutoWorkFlow()
      {
         //stateAwf = new BindingList<StateAutoWorkFlow>();
         stateAwf = new AdvancedList<StateAutoWorkFlow>();
      }
      /// <summary>
      /// Добавляет новый WorkFlow к списку WorkFlow
      /// </summary>
      /// <param name="description">Описание</param>
      /// <param name="sqlQuery">Sql запрос</param>
      public void AddWorkFlow(string description, string sqlQuery)
      {
         stateAwf.Add(new StateAutoWorkFlow(description, sqlQuery));
      }
      /// <summary>
      /// Удаляет workflow по индексу
      /// </summary>
      /// <param name="index">Индекс записи</param>
      public void DeleteWorkFlow(int index)
      {
         stateAwf.RemoveAt(index);
      }
   }
}