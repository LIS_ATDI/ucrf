﻿namespace XICSM.UcrfRfaNET.StationsStatus
{
   partial class ModifyStatus
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifyStatus));
          this.toolStrip = new System.Windows.Forms.ToolStrip();
          this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
          this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
          this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.toolStripButtonAddAutoStatus = new System.Windows.Forms.ToolStripButton();
          this.toolStripButtonDeleteAutoStatus = new System.Windows.Forms.ToolStripButton();
          this.dataGridWorkFlow = new System.Windows.Forms.DataGridView();
          this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.btnOk = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.tabControl1 = new System.Windows.Forms.TabControl();
          this.tabPageEction = new System.Windows.Forms.TabPage();
          this.tabPageAuto = new System.Windows.Forms.TabPage();
          this.dataGridViewSqlAuto = new System.Windows.Forms.DataGridView();
          this.ColumnDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.ColumnSqlQuery = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.toolStrip.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridWorkFlow)).BeginInit();
          this.tabControl1.SuspendLayout();
          this.tabPageEction.SuspendLayout();
          this.tabPageAuto.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSqlAuto)).BeginInit();
          this.SuspendLayout();
          // 
          // toolStrip
          // 
          this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAdd,
            this.toolStripButtonDelete,
            this.toolStripSeparator1,
            this.toolStripButtonAddAutoStatus,
            this.toolStripButtonDeleteAutoStatus});
          this.toolStrip.Location = new System.Drawing.Point(0, 0);
          this.toolStrip.Name = "toolStrip";
          this.toolStrip.Size = new System.Drawing.Size(580, 25);
          this.toolStrip.TabIndex = 0;
          this.toolStrip.Text = "toolStrip1";
          // 
          // toolStripButtonAdd
          // 
          this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripButtonAdd.Image = global::XICSM.UcrfRfaNET.Properties.Resources.add;
          this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonAdd.Name = "toolStripButtonAdd";
          this.toolStripButtonAdd.Size = new System.Drawing.Size(23, 22);
          this.toolStripButtonAdd.Text = "Add new status workflow";
          this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
          // 
          // toolStripButtonDelete
          // 
          this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripButtonDelete.Image = global::XICSM.UcrfRfaNET.Properties.Resources.remove;
          this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonDelete.Name = "toolStripButtonDelete";
          this.toolStripButtonDelete.Size = new System.Drawing.Size(23, 22);
          this.toolStripButtonDelete.Text = "toolStripButton1";
          this.toolStripButtonDelete.ToolTipText = "Delete selected status workflow";
          this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
          // 
          // toolStripSeparator1
          // 
          this.toolStripSeparator1.Name = "toolStripSeparator1";
          this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
          // 
          // toolStripButtonAddAutoStatus
          // 
          this.toolStripButtonAddAutoStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripButtonAddAutoStatus.Image = global::XICSM.UcrfRfaNET.Properties.Resources.add;
          this.toolStripButtonAddAutoStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonAddAutoStatus.Name = "toolStripButtonAddAutoStatus";
          this.toolStripButtonAddAutoStatus.Size = new System.Drawing.Size(23, 22);
          this.toolStripButtonAddAutoStatus.Text = "Add new auto workflow status";
          this.toolStripButtonAddAutoStatus.Click += new System.EventHandler(this.toolStripButtonAddAutoStatus_Click);
          // 
          // toolStripButtonDeleteAutoStatus
          // 
          this.toolStripButtonDeleteAutoStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripButtonDeleteAutoStatus.Image = global::XICSM.UcrfRfaNET.Properties.Resources.remove;
          this.toolStripButtonDeleteAutoStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonDeleteAutoStatus.Name = "toolStripButtonDeleteAutoStatus";
          this.toolStripButtonDeleteAutoStatus.Size = new System.Drawing.Size(23, 22);
          this.toolStripButtonDeleteAutoStatus.Text = "Delete auto workflow status";
          this.toolStripButtonDeleteAutoStatus.Click += new System.EventHandler(this.toolStripButtonDeleteAutoStatus_Click);
          // 
          // dataGridWorkFlow
          // 
          this.dataGridWorkFlow.AllowUserToAddRows = false;
          this.dataGridWorkFlow.AllowUserToDeleteRows = false;
          this.dataGridWorkFlow.AllowUserToResizeRows = false;
          this.dataGridWorkFlow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridWorkFlow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
          this.dataGridWorkFlow.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dataGridWorkFlow.Location = new System.Drawing.Point(3, 3);
          this.dataGridWorkFlow.MultiSelect = false;
          this.dataGridWorkFlow.Name = "dataGridWorkFlow";
          this.dataGridWorkFlow.ReadOnly = true;
          this.dataGridWorkFlow.RowHeadersVisible = false;
          this.dataGridWorkFlow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridWorkFlow.Size = new System.Drawing.Size(542, 340);
          this.dataGridWorkFlow.TabIndex = 0;
          this.dataGridWorkFlow.DoubleClick += new System.EventHandler(this.dataGridWorkFlow_DoubleClick);
          // 
          // Column1
          // 
          this.Column1.DataPropertyName = "BindStateFrom";
          this.Column1.HeaderText = "Status from";
          this.Column1.Name = "Column1";
          this.Column1.ReadOnly = true;
          this.Column1.Width = 90;
          // 
          // Column2
          // 
          this.Column2.DataPropertyName = "BindTypeEction";
          this.Column2.HeaderText = "Type action";
          this.Column2.Name = "Column2";
          this.Column2.ReadOnly = true;
          this.Column2.Width = 150;
          // 
          // Column3
          // 
          this.Column3.DataPropertyName = "BindEction";
          this.Column3.HeaderText = "Action";
          this.Column3.Name = "Column3";
          this.Column3.ReadOnly = true;
          this.Column3.Width = 150;
          // 
          // Column4
          // 
          this.Column4.DataPropertyName = "BindStateTo";
          this.Column4.HeaderText = "Status to";
          this.Column4.Name = "Column4";
          this.Column4.ReadOnly = true;
          this.Column4.Width = 80;
          // 
          // btnOk
          // 
          this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.btnOk.Location = new System.Drawing.Point(386, 417);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(75, 23);
          this.btnOk.TabIndex = 2;
          this.btnOk.Text = "Ok";
          this.btnOk.UseVisualStyleBackColor = true;
          this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnCancel.Location = new System.Drawing.Point(493, 417);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 3;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          // 
          // tabControl1
          // 
          this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tabControl1.Controls.Add(this.tabPageEction);
          this.tabControl1.Controls.Add(this.tabPageAuto);
          this.tabControl1.Location = new System.Drawing.Point(12, 28);
          this.tabControl1.Name = "tabControl1";
          this.tabControl1.SelectedIndex = 0;
          this.tabControl1.Size = new System.Drawing.Size(556, 372);
          this.tabControl1.TabIndex = 4;
          this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
          // 
          // tabPageEction
          // 
          this.tabPageEction.Controls.Add(this.dataGridWorkFlow);
          this.tabPageEction.Location = new System.Drawing.Point(4, 22);
          this.tabPageEction.Name = "tabPageEction";
          this.tabPageEction.Padding = new System.Windows.Forms.Padding(3);
          this.tabPageEction.Size = new System.Drawing.Size(548, 346);
          this.tabPageEction.TabIndex = 0;
          this.tabPageEction.Text = "Status workflow by action";
          this.tabPageEction.UseVisualStyleBackColor = true;
          // 
          // tabPageAuto
          // 
          this.tabPageAuto.Controls.Add(this.dataGridViewSqlAuto);
          this.tabPageAuto.Location = new System.Drawing.Point(4, 22);
          this.tabPageAuto.Name = "tabPageAuto";
          this.tabPageAuto.Padding = new System.Windows.Forms.Padding(3);
          this.tabPageAuto.Size = new System.Drawing.Size(548, 346);
          this.tabPageAuto.TabIndex = 1;
          this.tabPageAuto.Text = "Status workflow auto";
          this.tabPageAuto.UseVisualStyleBackColor = true;
          // 
          // dataGridViewSqlAuto
          // 
          this.dataGridViewSqlAuto.AllowUserToAddRows = false;
          this.dataGridViewSqlAuto.AllowUserToDeleteRows = false;
          this.dataGridViewSqlAuto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridViewSqlAuto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDescription,
            this.ColumnSqlQuery});
          this.dataGridViewSqlAuto.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dataGridViewSqlAuto.Location = new System.Drawing.Point(3, 3);
          this.dataGridViewSqlAuto.MultiSelect = false;
          this.dataGridViewSqlAuto.Name = "dataGridViewSqlAuto";
          this.dataGridViewSqlAuto.ReadOnly = true;
          this.dataGridViewSqlAuto.RowHeadersVisible = false;
          this.dataGridViewSqlAuto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridViewSqlAuto.Size = new System.Drawing.Size(542, 340);
          this.dataGridViewSqlAuto.TabIndex = 0;
          this.dataGridViewSqlAuto.DoubleClick += new System.EventHandler(this.dataGridViewSqlAuto_DoubleClick);
          // 
          // ColumnDescription
          // 
          this.ColumnDescription.DataPropertyName = "Description";
          this.ColumnDescription.HeaderText = "Description";
          this.ColumnDescription.Name = "ColumnDescription";
          this.ColumnDescription.ReadOnly = true;
          // 
          // ColumnSqlQuery
          // 
          this.ColumnSqlQuery.DataPropertyName = "SqlQuery";
          this.ColumnSqlQuery.HeaderText = "Sql query";
          this.ColumnSqlQuery.Name = "ColumnSqlQuery";
          this.ColumnSqlQuery.ReadOnly = true;
          this.ColumnSqlQuery.Width = 400;
          // 
          // ModifyStatus
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.CancelButton = this.btnCancel;
          this.ClientSize = new System.Drawing.Size(580, 452);
          this.Controls.Add(this.tabControl1);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOk);
          this.Controls.Add(this.toolStrip);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MinimizeBox = false;
          this.Name = "ModifyStatus";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Edit the status workflow";
          this.Shown += new System.EventHandler(this.ModifyStatus_Shown);
          this.toolStrip.ResumeLayout(false);
          this.toolStrip.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridWorkFlow)).EndInit();
          this.tabControl1.ResumeLayout(false);
          this.tabPageEction.ResumeLayout(false);
          this.tabPageAuto.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSqlAuto)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.ToolStrip toolStrip;
      private System.Windows.Forms.Button btnOk;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.DataGridView dataGridWorkFlow;
      private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
      private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
      private System.Windows.Forms.ToolStripButton toolStripButtonAddAutoStatus;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPageEction;
      private System.Windows.Forms.TabPage tabPageAuto;
      private System.Windows.Forms.DataGridView dataGridViewSqlAuto;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
      private System.Windows.Forms.ToolStripButton toolStripButtonDeleteAutoStatus;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescription;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSqlQuery;
   }
}