﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.StationsStatus
{
   public partial class ModifyStatus : Form
   {
      private StationStatus _wf;
      private StationAutoStatus _aWf;
      //------
      public ModifyStatus()
      {
         InitializeComponent();
         //----
         dataGridWorkFlow.DoubleBuffered(true);
         dataGridViewSqlAuto.DoubleBuffered(true);
         //----
         _wf = new StationStatus();
         _aWf = new StationAutoStatus();
         dataGridWorkFlow.DataSource = _wf.wf.StateWf;
         dataGridViewSqlAuto.DataSource = _aWf.aWf.StateAwf;
         //----
         UpdateButtons();
      }
      /// <summary>
      /// Сохраняем изменения
      /// </summary>
      private void btnOk_Click(object sender, EventArgs e)
      {
         _wf.Save();
         _aWf.Save();
      }
      /// <summary>
      /// Добавить новый workflow
      /// </summary>
      private void toolStripButtonAdd_Click(object sender, EventArgs e)
      {
         try
         {
            using (AddNewStatus frm = new AddNewStatus(CLocaliz.TxT("Add new status workflow")))
            {
               if(frm.ShowDialog() == DialogResult.OK)
                  _wf.wf.AddWorkFlow(frm.StatusFrom, frm.StatusTo, frm.TypeOfEvent.ToString(), frm.Event);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
         }
      }
      /// <summary>
      /// Удалить выбраную строку
      /// </summary>
      private void toolStripButtonDelete_Click(object sender, EventArgs e)
      {
         if ((dataGridWorkFlow.SelectedRows.Count > 0) &&
             (MessageBox.Show(CLocaliz.TxT("Do you want to delete selcted row?"),
                              "",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question) == DialogResult.Yes))
         {
            StateWorkFlow curWf = dataGridWorkFlow.SelectedRows[0].DataBoundItem as StateWorkFlow;
            if (curWf != null)
               _wf.wf.StateWf.Remove(curWf);
         }
      }
      /// <summary>
      /// Добавляем sql запрос на автоматическое проставление сттусов
      /// </summary>
      private void toolStripButtonAddAutoStatus_Click(object sender, EventArgs e)
      {
         try
         {
            using (AddNewAutoStatus frm = new AddNewAutoStatus(CLocaliz.TxT("Add new auto status workflow")))
            {
               if (frm.ShowDialog() == DialogResult.OK)
                  _aWf.aWf.AddWorkFlow(frm.Description, frm.SqlQuery);
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
         }
      }
      /// <summary>
      /// Редактирование записи
      /// </summary>
      private void dataGridWorkFlow_DoubleClick(object sender, EventArgs e)
      {
         DataGridWorkFlowEdit();
      }
      /// <summary>
      /// Редактирование записи
      /// </summary>
      private void DataGridWorkFlowEdit()
      {
         if (dataGridWorkFlow.SelectedRows.Count > 0)
         {
            StateWorkFlow curWf = dataGridWorkFlow.SelectedRows[0].DataBoundItem as StateWorkFlow;
            if (curWf != null)
            {
               try
               {
                  StationStatus.ETypeEvent eventType = StationStatus.ToTypeEvent(curWf.TypeEction);
                  using (AddNewStatus frm = new AddNewStatus(CLocaliz.TxT("Edit status workflow"), curWf.StateFrom, curWf.StateTo, eventType, curWf.Action))
                  {
                     if (frm.ShowDialog() == DialogResult.OK)
                     {
                        curWf.StateFrom = frm.StatusFrom;
                        curWf.StateTo = frm.StatusTo;
                        curWf.TypeEction = frm.TypeOfEvent.ToString();
                        curWf.Action = frm.Event;
                     }
                  }
               }
               catch (Exception ex)
               {
                  MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
               }
            }
         }
      }
      /// <summary>
      /// Удалить sql автоматического проставления статусов
      /// </summary>
      private void toolStripButtonDeleteAutoStatus_Click(object sender, EventArgs e)
      {
         if ((dataGridViewSqlAuto.SelectedRows.Count > 0) &&
             (MessageBox.Show(CLocaliz.TxT("Do you want to delete selcted row?"),
                              "",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question) == DialogResult.Yes))
         {
            StateAutoWorkFlow curWf = dataGridViewSqlAuto.SelectedRows[0].DataBoundItem as StateAutoWorkFlow;
            if (curWf != null)
               _aWf.aWf.StateAwf.Remove(curWf);
         }
      }
      /// <summary>
      /// Редактирование записи
      /// </summary>
      private void DataGridAutoWorkFlowEdit()
      {
         if (dataGridViewSqlAuto.SelectedRows.Count > 0)
         {
            StateAutoWorkFlow curWf = dataGridViewSqlAuto.SelectedRows[0].DataBoundItem as StateAutoWorkFlow;
            if (curWf != null)
            {
               try
               {
                  using (AddNewAutoStatus frm = new AddNewAutoStatus(CLocaliz.TxT("Edit auto status workflow"), curWf.Description, curWf.SqlQuery))
                  {
                     if (frm.ShowDialog() == DialogResult.OK)
                     {
                        curWf.Description = frm.Description;
                        curWf.SqlQuery = frm.SqlQuery;
                     }
                  }
               }
               catch (Exception ex)
               {
                  MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
               }
            }
         }
      }
      /// <summary>
      /// Редактируем запись
      /// </summary>
      private void dataGridViewSqlAuto_DoubleClick(object sender, EventArgs e)
      {
         DataGridAutoWorkFlowEdit();
      }
      /// <summary>
      /// Блокировка кнопок
      /// </summary>
      private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
      {
         UpdateButtons();
      }
      /// <summary>
      /// Обновляет доступ к кнопкам
      /// </summary>
      private void UpdateButtons()
      {
         toolStripButtonAdd.Enabled = false;
         toolStripButtonDelete.Enabled = false;
         toolStripButtonAddAutoStatus.Enabled = false;
         toolStripButtonDeleteAutoStatus.Enabled = false;
         if(tabControl1.SelectedTab == tabPageEction)
         {
            toolStripButtonAdd.Enabled = true;
            toolStripButtonDelete.Enabled = true;
         }
         else if(tabControl1.SelectedTab == tabPageAuto)
         {
            toolStripButtonAddAutoStatus.Enabled = true;
            toolStripButtonDeleteAutoStatus.Enabled = true;
         }
      }

      private void ModifyStatus_Shown(object sender, EventArgs e)
      {
          tabPageEction.Text = CLocaliz.TxT("Status workflow by action");
          tabPageAuto.Text = CLocaliz.TxT("Status workflow auto");
          Text = CLocaliz.TxT("Edit the status workflow");
          dataGridWorkFlow.Columns[0].HeaderText=CLocaliz.TxT("Status from");
          dataGridWorkFlow.Columns[1].HeaderText = CLocaliz.TxT("Type action");
          dataGridWorkFlow.Columns[2].HeaderText = CLocaliz.TxT("Action");
          dataGridWorkFlow.Columns[3].HeaderText = CLocaliz.TxT("Status to");

          dataGridViewSqlAuto.Columns[0].HeaderText = CLocaliz.TxT("Description");
          dataGridViewSqlAuto.Columns[1].HeaderText = CLocaliz.TxT("Sql query");


          toolStripButtonAdd.Text = CLocaliz.TxT("Add new status workflow");
          toolStripButtonDelete.ToolTipText = CLocaliz.TxT("Delete selected status workflow");
          toolStripButtonAddAutoStatus.Text = CLocaliz.TxT("Add new auto workflow status");
          toolStripButtonDeleteAutoStatus.Text = CLocaliz.TxT("Delete auto workflow status");
      }
   }

   /// <summary>
   /// Разрирение класса DataGridView
   /// </summary>
   public static class ExtensionDataGridView
   {
      /// <summary>
      /// Включаем двойную буфферизацию DataGridView
      /// </summary>
      /// <param name="dgv">Сам грид</param>
      /// <param name="setting">Включать / не включать</param>
      public static void DoubleBuffered(this DataGridView dgv, bool setting)
      {
         Type dgvType = dgv.GetType();
         PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
             BindingFlags.Instance | BindingFlags.NonPublic);
         pi.SetValue(dgv, setting, null);
      }
   }
}
