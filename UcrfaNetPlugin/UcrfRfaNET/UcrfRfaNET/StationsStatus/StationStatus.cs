﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.UtilityClass;


namespace XICSM.UcrfRfaNET.StationsStatus
{
   /// <summary>
   /// Тип определяющий  статус станции
   /// </summary>
    public class StatusType : NotifyPropertyChanged
   {
      /// <summary>
      /// Статус
      /// </summary>
      private string _status;
      public string StatusAsString
      {
         get { return _status; }
         set
         {
            if (_status != value)
            {
               _status = value;
               InvokeNotifyPropertyChanged("StatusAsString");
               InvokeNotifyPropertyChanged("StatusFromEri");
            }
         }
      }
      private string _forStatus;
      public string ForStatusAsString
      {
          get { return _forStatus; }
          set
          {
              if (_forStatus != value)
              {
                  _forStatus = value;
                  InvokeNotifyPropertyChanged("ForStatusAsString");
                  InvokeNotifyPropertyChanged("StatusFromEri");
              }
          }
      }

      /// <summary>
      /// Определение статуса из eri файла
      /// </summary>
      public string StatusFromEri
      {
         get
         {
             if (!string.IsNullOrEmpty(_forStatus))
                 return StatusAsString.ToEriStationStatus() + " / " + _forStatus;
             return StatusAsString.ToEriStationStatus();
         }
      }
      /// <summary>
      /// Сонструктор
      /// </summary>
      public StatusType()
      {
         _status = "";
      }
      /// <summary>
      /// Проверка статуса
      /// </summary>
      /// <returns>TRUE - если статус не пустой, иначе FALSE</returns>
      public bool IsEmpty()
      {
         return string.IsNullOrEmpty(_status);
      }

      #region Object
      /// <summary>
      /// Перезагрузка проверки равенства
      /// </summary>
      /// <param name="obj">Параметр сравнения</param>
      /// <returns></returns>
      public override bool Equals(object obj)
      {
         if (obj == null)
            return false;

         StatusType p = obj as StatusType;
         if (p == null)
            return false;
         return (_status == p._status);
      }
      /// <summary>
      /// Проверка равенства
      /// </summary>
      /// <param name="other">Скем сравниваем</param>
      /// <returns>TRUE - если равны, иначе FALSE</returns>
      public bool Equals(StatusType other)
      {
         if (ReferenceEquals(null, other)) return false;
         if (ReferenceEquals(this, other)) return true;
         return Equals(other._status, _status);
      }
      /// <summary>
      /// Возвращает HashCode
      /// </summary>
      /// <returns>Возвращает HashCode</returns>
      public override int GetHashCode()
      {
         return (_status != null ? _status.GetHashCode() : 0);
      }
      /// <summary>
      /// В строку
      /// </summary>
      /// <returns>Строку статуса</returns>
      public override string ToString()
      {
          return StatusFromEri;
      }
      /// <summary>
      /// operator ==
      /// </summary>
      public static bool operator ==(StatusType v1, StatusType v2)
      {
         if (ReferenceEquals(null, v1)) return false;
         if (ReferenceEquals(null, v2)) return false;
         return v1.Equals(v2);
      }
      /// <summary>
      /// operator !=
      /// </summary>
      public static bool operator !=(StatusType v1, StatusType v2)
      {
         return !(v1 == v2);
      }

      #endregion
   }
   //======================================================
   /// <summary>
   /// Класс конвертирует тип StatusType
   /// </summary>
   public static class StatusTypeConvert
   {
      //===================================================
      /// <summary>
      /// Конвертирует тип StatusType в строку
      /// </summary>
      /// <param name="val">что конвертировать</param>
      /// <returns>сроковое представление типа</returns>
      public static string ToStr(this StatusType val)
      {
         return val.StatusAsString;
      }
      //===================================================
      public static StatusType ToStatusType(this string val)
      {
         StatusType retType = new StatusType();
         if (string.IsNullOrEmpty(val) == false)
            retType.StatusAsString = val;
         return retType;
      }
   }
   //======================================================
   public class StationStatus
   {
       public enum ETypeEvent
       {
           /// <summary>
           /// Событие в логе
           /// </summary>
           EventLog,
           /// <summary>
           /// Создание новой заявки
           /// </summary>
           TypeAppl,
           /// <summary>
           /// Пакетная печать документа
           /// </summary>
           PacketPrintDoc,
           /// <summary>
           /// Печать документа из ВРВ
           /// </summary>
           ApplPrintDoc,
           /// <summary>
           /// Прикрипление заявки к новому пакету
           /// </summary>
           AttachAppl,
           /// <summary>
           /// Прикрипление документа к заявке из пакета
           /// </summary>
           AttachDoc,
           /// <summary>
           /// Удаление завки из пакета
           /// </summary>
           DeleteAppl,
       }
      //-------
      public static ETypeEvent ToTypeEvent(string eventType)
      {
         ETypeEvent retType = ETypeEvent.TypeAppl;
         try
         {
            retType = (ETypeEvent)Enum.Parse(typeof(ETypeEvent), eventType);
         }
         catch
         { }
         return retType;
      }
      //-------
      public static Dictionary<ETypeEvent, string> GetHumanDictionaryTypeEvent()
      {
         Dictionary<ETypeEvent, string> retVal = new Dictionary<ETypeEvent, string>();
         retVal.Add(ETypeEvent.EventLog, CLocaliz.TxT("EventLog"));
         retVal.Add(ETypeEvent.TypeAppl, CLocaliz.TxT("TypeAppl"));
         retVal.Add(ETypeEvent.DeleteAppl, CLocaliz.TxT("DeleteAppl"));
         retVal.Add(ETypeEvent.PacketPrintDoc, CLocaliz.TxT("PacketPrintDoc"));
         retVal.Add(ETypeEvent.ApplPrintDoc, CLocaliz.TxT("ApplPrintDoc"));
         retVal.Add(ETypeEvent.AttachAppl, CLocaliz.TxT("AttachAppl"));
         retVal.Add(ETypeEvent.AttachDoc, CLocaliz.TxT("AttachDoc"));
         return retVal;
      }
      private StationWorkFlow _wf;
      public StationWorkFlow wf { get { return _wf; } }
      /// <summary>
      /// Конструктор
      /// </summary>
      public StationStatus()
      {
         Load();
      }
      /// <summary>
      /// Возвращает новое состояние по событию в логе
      /// </summary>
      /// <param name="from">Переходим из статуса</param>
      /// <param name="events">Тип события</param>
      public StatusType GetNewStatus(StatusType from, EDocEvent events)
      {
         string strStatusFrom = from.ToStr();
         string strEvent = events.ToString();
         string strTypeEvent = ETypeEvent.EventLog.ToString();
         string strState = _wf.GetState(strStatusFrom, strTypeEvent, strEvent);
         return strState.ToStatusType();
      }
      /// <summary>
      /// Возвращает новое состояние по типу заявки
      /// </summary>
      /// <param name="from">Переходим из статуса</param>
      /// <param name="events">Тип заявки</param>
      /// <param name="typeEvent">Тип события</param>
      public StatusType GetNewStatus(StatusType from, AppType events, ETypeEvent typeEvent)
      {
          string strStatusFrom = from.ToStr();
          string strEvent = events.ToString();
          string strTypeEvent = typeEvent.ToString();
          string strState = _wf.GetState(strStatusFrom, strTypeEvent, strEvent);
          return strState.ToStatusType();
      }
      /// <summary>
      /// Возвращает новое состояние при печати документа
      /// </summary>
      /// <param name="from">Переходим из статуса</param>
      /// <param name="events">Тип документа</param>
      /// <param name="typeEvent">Тип события</param>
      public StatusType GetNewStatus(StatusType from, string events, ETypeEvent typeEvent)
      {
         string strStatusFrom = from.ToStr();
         string strEvent = events.ToString();
         string strTypeEvent = typeEvent.ToString();
         string strState = _wf.GetState(strStatusFrom, strTypeEvent, strEvent);
         return strState.ToStatusType();
      }
      /// <summary>
      /// Возвращает новое состояние при прикрепление заявки к новому пакету
      /// </summary>
      /// <param name="from">Переходим из статуса</param>
      /// <param name="events">Тип пакета</param>
      public StatusType GetNewStatus(StatusType from, EPacketType events)
      {
         string strStatusFrom = from.ToStr();
         string strEvent = events.ToString();
         string strTypeEvent = ETypeEvent.AttachAppl.ToString();
         string strState = _wf.GetState(strStatusFrom, strTypeEvent, strEvent);
         return strState.ToStatusType();
      }
      /// <summary>
      /// Возвращает список типов документов из которых есть переходи
      /// </summary>
      /// <param name="typeEvent">Тип собития</param>
      /// <returns>Список документов</returns>
      private List<string> GetAllDocs(ETypeEvent typeEvent)
      {
         string strTypeEvent = typeEvent.ToString();
         List<string> retVal = (from item in _wf.StateWf
                                 where item.TypeEction == strTypeEvent
                                 select item.Action).ToList();
         return retVal;
      }
      /// <summary>
      /// Возвращает список типов документов из которых есть переходи с определенного статуса станции
      /// </summary>
      /// <param name="typeEvent">Тип собития</param>
      /// <param name="from">Статус</param>
      /// <returns>Список документов</returns>
      private List<string> GetDocsFromStatus(ETypeEvent typeEvent, StatusType from)
      {
         string strStatusFrom = from.ToStr();
         string strTypeEvent = typeEvent.ToString();
         List<string> retVal = (from item in _wf.StateWf
                                 where ((item.TypeEction == strTypeEvent) && (item.StateFrom == strStatusFrom))
                                 select item.Action).ToList();
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Возвращает список документов, которые можна прекриплять к
      /// текущим станциям
      /// </summary>
      /// <param name="typeEvent">Тип события</param>
      /// <param name="idList">ID заявок</param>
      /// <returns></returns>
      public List<string> AccessedDocuments(ETypeEvent typeEvent, params int[] idList)
      {
         //if (idList.Count() == 0)
           // return new List<string>();
         List<string> retVal = GetAllDocs(typeEvent);
         List<StatusType> processedStatus = new List<StatusType>();
         foreach (int id in idList)
         {
            StatusType curStatus = ApplSource.BaseAppClass.ReadStatus(id);
            if (processedStatus.Contains(curStatus) == false)
            {
               processedStatus.Add(curStatus);  //Больше обрабатывать не будем данный статус
               List<string> tmpListDocs = retVal;
               List<string> tmpListDocsForThisTatus = GetDocsFromStatus(typeEvent, curStatus);
               retVal = new List<string>();
               foreach (string itemDocType in tmpListDocsForThisTatus)
                  if (tmpListDocs.Contains(itemDocType))
                     retVal.Add(itemDocType);
            }
         }
         return retVal;
      }
      /// <summary>
      /// Сохранить WorkFlow
      /// </summary>
      public void Save()
      {
         IMRecordset rsStatus = new IMRecordset(PlugTbl.STATUS_WF, IMRecordset.Mode.ReadWrite);
         rsStatus.Select("ID,CODE_WF");
         rsStatus.SetWhere("IS_AUTO", IMRecordset.Operation.Eq, 0);
         rsStatus.Open();
         bool isAdd = false;
         foreach (StateWorkFlow item in _wf.StateWf)
         {
            if (rsStatus.IsEOF() || (isAdd == true))
            {
               isAdd = true;
               rsStatus.AddNew();
               rsStatus.Put("ID", IM.AllocID(PlugTbl.STATUS_WF, 1, -1));
            }
            else
            {
               rsStatus.Edit();
            }
            string formatString = string.Format("{0}#{1}#{2}#{3}", item.StateFrom, item.StateTo, item.TypeEction, item.Action);
            rsStatus.Put("CODE_WF", formatString);
            rsStatus.Update();
            rsStatus.MoveNext();
         }

         if (isAdd == false)
            while (!rsStatus.IsEOF())
            {
               rsStatus.Delete();
               rsStatus.MoveNext();
            }
         rsStatus.Close();
         rsStatus.Destroy();
      }
      /// <summary>
      /// Загрузка WorkFlow
      /// </summary>
      public void Load()
      {
         _wf = new StationWorkFlow();
         IMRecordset rsStatus = new IMRecordset(PlugTbl.STATUS_WF, IMRecordset.Mode.ReadOnly);
         rsStatus.Select("ID,CODE_WF");
         rsStatus.SetWhere("IS_AUTO", IMRecordset.Operation.Eq, 0);
         for (rsStatus.Open(); !rsStatus.IsEOF(); rsStatus.MoveNext())
         {
            string[] elem = rsStatus.GetS("CODE_WF").Split('#');
            if (elem.Length == 4)
               _wf.AddWorkFlow(elem[0], elem[1], elem[2], elem[3]);
         }
         rsStatus.Close();
         rsStatus.Destroy();
      }
   }
   //======================================================
   /// <summary>
   /// Автоматическое проставление статей
   /// </summary>
   public class StationAutoStatus
   {
      private StationAutoWorkFlow _aWf;
      public StationAutoWorkFlow aWf { get { return _aWf; } }

      public StationAutoStatus()
      {
         Load();
      }
      /// <summary>
      /// Сохранить WorkFlow
      /// </summary>
      public void Save()
      {
         IMRecordset rsStatus = new IMRecordset(PlugTbl.STATUS_WF, IMRecordset.Mode.ReadWrite);
         rsStatus.Select("ID,CODE_WF,IS_AUTO");
         rsStatus.SetWhere("IS_AUTO", IMRecordset.Operation.Eq, 1);
         rsStatus.Open();
         bool isAdd = false;
         foreach (StateAutoWorkFlow item in _aWf.StateAwf)
         {
            if (rsStatus.IsEOF() || (isAdd == true))
            {
               isAdd = true;
               rsStatus.AddNew();
               rsStatus.Put("ID", IM.AllocID(PlugTbl.STATUS_WF, 1, -1));
               rsStatus.Put("IS_AUTO", 1);
            }
            else
            {
               rsStatus.Edit();
            }
            string formatString = string.Format("{0}#{1}", item.Description, item.SqlQuery);
            rsStatus.Put("CODE_WF", formatString);
            rsStatus.Update();
            rsStatus.MoveNext();
         }

         if (isAdd == false)
            while (!rsStatus.IsEOF())
            {
               rsStatus.Delete();
               rsStatus.MoveNext();
            }
         rsStatus.Close();
         rsStatus.Destroy();
      }
      /// <summary>
      /// Загрузка WorkFlow
      /// </summary>
      public void Load()
      {
         _aWf = new StationAutoWorkFlow();
         IMRecordset rsStatus = new IMRecordset(PlugTbl.STATUS_WF, IMRecordset.Mode.ReadOnly);
         rsStatus.Select("ID,CODE_WF,IS_AUTO");
         rsStatus.SetWhere("IS_AUTO", IMRecordset.Operation.Eq, 1);
         for (rsStatus.Open(); !rsStatus.IsEOF(); rsStatus.MoveNext())
         {
            string[] elem = rsStatus.GetS("CODE_WF").Split('#');
            if (elem.Length == 2)
               _aWf.AddWorkFlow(elem[0], elem[1]);
         }
         rsStatus.Close();
         rsStatus.Destroy();
      }
   }
   //======================================================
   /// <summary>
   /// Класс позволяет сортировать списки в гриде (Честно украс из форума)
   /// </summary>
   /// <typeparam name="T">Тип данных, который отображается в списке</typeparam>
   public class AdvancedList<T> : BindingList<T>, IBindingListView where T : class
   {
      //===================================================
      protected override bool IsSortedCore
      {
         get { return _sorts != null; }
      }
      //===================================================
      protected override void RemoveSortCore()
      {
         _sorts = null;
      }
      //===================================================
      protected override bool SupportsSortingCore
      {
         get { return true; }
      }
      //===================================================
      protected override ListSortDirection SortDirectionCore
      {
         get
         {
            return _sorts == null ? ListSortDirection.Ascending :
                                   _sorts.PrimaryDirection;
         }
      }
      //===================================================
      protected override PropertyDescriptor SortPropertyCore
      {
         get { return _sorts == null ? null : _sorts.PrimaryProperty; }
      }
      //===================================================
      protected override void ApplySortCore(PropertyDescriptor prop,
                                            ListSortDirection direction)
      {
         ListSortDescription[] arr = {new ListSortDescription(prop, direction)};
         ApplySort(new ListSortDescriptionCollection(arr));
      }
      //===================================================
      PropertyComparerCollection<T> _sorts;
      public void ApplySort(ListSortDescriptionCollection sortCollection)
      {
         bool oldRaise = RaiseListChangedEvents;
         RaiseListChangedEvents = false;
         try
         {
            PropertyComparerCollection<T> tmp
            = new PropertyComparerCollection<T>(sortCollection);
            List<T> items = new List<T>(this);
            items.Sort(tmp);
            int index = 0;
            foreach (T item in items)
            {
               SetItem(index++, item);
            }
            _sorts = tmp;
         }
         finally
         {
            RaiseListChangedEvents = oldRaise;
            ResetBindings();
         }
      }
      //===================================================
      string IBindingListView.Filter
      {
         get { throw new NotImplementedException(); }
         set { throw new NotImplementedException(); }
      }
      //===================================================
      void IBindingListView.RemoveFilter()
      {
         throw new NotImplementedException();
      }
      //===================================================
      ListSortDescriptionCollection IBindingListView.SortDescriptions
      {
         get { return _sorts.Sorts; }
      }
      //===================================================
      bool IBindingListView.SupportsAdvancedSorting
      {
         get { return true; }
      }
      //===================================================
      bool IBindingListView.SupportsFiltering
      {
         get { return false; }
      }
   }
   //===================================================
   //===================================================
   //===================================================
   public class PropertyComparerCollection<T> : IComparer<T> where T : class
   {
      private readonly ListSortDescriptionCollection _sorts;
      private readonly PropertyComparer<T>[] _comparers;
      //===================================================
      public ListSortDescriptionCollection Sorts
      {
         get { return _sorts; }
      }
      //===================================================
      public PropertyComparerCollection(ListSortDescriptionCollection sorts)
      {
         if (sorts == null) throw new ArgumentNullException("sorts");
         this._sorts = sorts;
         List<PropertyComparer<T>> list = new
         List<PropertyComparer<T>>();
         foreach (ListSortDescription item in sorts)
         {
            list.Add(new PropertyComparer<T>(item.PropertyDescriptor,
            item.SortDirection == ListSortDirection.Descending));
         }
         _comparers = list.ToArray();
      }
      //===================================================
      public PropertyDescriptor PrimaryProperty
      {
         get
         {
            return _comparers.Length == 0 ? null :
            _comparers[0].Property;
         }
      }
      //===================================================
      public ListSortDirection PrimaryDirection
      {
         get
         {
            return _comparers.Length == 0 ? ListSortDirection.Ascending
            : _comparers[0].Descending ?
            ListSortDirection.Descending
            : ListSortDirection.Ascending;
         }
      }
      //===================================================
      int IComparer<T>.Compare(T x, T y)
      {
         int result = 0;
         for (int i = 0; i < _comparers.Length; i++)
         {
            result = _comparers[i].Compare(x, y);
            if (result != 0) break;
         }
         return result;
      }
   }
   //===================================================
   //===================================================
   //===================================================
   //===================================================
   //===================================================
   public class PropertyComparer<T> : IComparer<T> where T : class
   {
      private readonly bool _descending;
      public bool Descending { get { return _descending; } }
      private readonly PropertyDescriptor _property;
      public PropertyDescriptor Property { get { return _property; } }
      //===================================================
      public PropertyComparer(PropertyDescriptor property, bool descending)
      {
         if (property == null) throw new
         ArgumentNullException("property");
         this._descending = descending;
         this._property = property;
      }
      //===================================================
      public int Compare(T x, T y)
      {
         if ((x == null) || (y == null))
            return 0;
         int value = Comparer.Default.Compare(_property.GetValue(x), _property.GetValue(y));
         return _descending ? -value : value;
      }
   }
}