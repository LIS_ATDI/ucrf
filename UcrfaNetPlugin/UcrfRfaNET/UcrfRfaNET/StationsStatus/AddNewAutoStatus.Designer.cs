﻿namespace XICSM.UcrfRfaNET.StationsStatus
{
   partial class AddNewAutoStatus
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.label1 = new System.Windows.Forms.Label();
          this.tbDescription = new System.Windows.Forms.TextBox();
          this.gbSql = new System.Windows.Forms.GroupBox();
          this.tbSqlQuery = new System.Windows.Forms.TextBox();
          this.btnTestSql = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.btnOk = new System.Windows.Forms.Button();
          this.gbSql.SuspendLayout();
          this.SuspendLayout();
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(10, 9);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(60, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Description";
          // 
          // tbDescription
          // 
          this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbDescription.Location = new System.Drawing.Point(13, 30);
          this.tbDescription.Name = "tbDescription";
          this.tbDescription.Size = new System.Drawing.Size(553, 20);
          this.tbDescription.TabIndex = 1;
          // 
          // gbSql
          // 
          this.gbSql.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.gbSql.Controls.Add(this.tbSqlQuery);
          this.gbSql.Location = new System.Drawing.Point(13, 57);
          this.gbSql.Name = "gbSql";
          this.gbSql.Size = new System.Drawing.Size(553, 199);
          this.gbSql.TabIndex = 2;
          this.gbSql.TabStop = false;
          this.gbSql.Text = "Sql query";
          // 
          // tbSqlQuery
          // 
          this.tbSqlQuery.Dock = System.Windows.Forms.DockStyle.Fill;
          this.tbSqlQuery.Location = new System.Drawing.Point(3, 16);
          this.tbSqlQuery.Multiline = true;
          this.tbSqlQuery.Name = "tbSqlQuery";
          this.tbSqlQuery.Size = new System.Drawing.Size(547, 180);
          this.tbSqlQuery.TabIndex = 0;
          // 
          // btnTestSql
          // 
          this.btnTestSql.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.btnTestSql.Location = new System.Drawing.Point(13, 272);
          this.btnTestSql.Name = "btnTestSql";
          this.btnTestSql.Size = new System.Drawing.Size(155, 23);
          this.btnTestSql.TabIndex = 3;
          this.btnTestSql.Text = "Test sql";
          this.btnTestSql.UseVisualStyleBackColor = true;
          this.btnTestSql.Click += new System.EventHandler(this.btnTestSql_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnCancel.Location = new System.Drawing.Point(491, 272);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 4;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          // 
          // btnOk
          // 
          this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.btnOk.Location = new System.Drawing.Point(396, 272);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(75, 23);
          this.btnOk.TabIndex = 5;
          this.btnOk.Text = "Ok";
          this.btnOk.UseVisualStyleBackColor = true;
          // 
          // AddNewAutoStatus
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(578, 307);
          this.Controls.Add(this.btnOk);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnTestSql);
          this.Controls.Add(this.gbSql);
          this.Controls.Add(this.tbDescription);
          this.Controls.Add(this.label1);
          this.MinimizeBox = false;
          this.Name = "AddNewAutoStatus";
          this.ShowInTaskbar = false;
          this.Text = "Auto status";
          this.Shown += new System.EventHandler(this.AddNewAutoStatus_Shown);
          this.gbSql.ResumeLayout(false);
          this.gbSql.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbDescription;
      private System.Windows.Forms.GroupBox gbSql;
      private System.Windows.Forms.TextBox tbSqlQuery;
      private System.Windows.Forms.Button btnTestSql;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.Button btnOk;
   }
}