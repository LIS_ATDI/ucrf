﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.StationsStatus
{
   public partial class AddNewAutoStatus : Form
   {
      private StateAutoWorkFlow _wf;
      public string Description
      {
         get { return _wf.Description; }
         set
         {
            if (_wf.Description != value)
               _wf.Description = value;
         }
      }
      public string SqlQuery
      {
         get { return _wf.SqlQuery; }
         set
         {
            if (_wf.SqlQuery != value)
               _wf.SqlQuery = value;
         }
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="caption">Шапочка формочки</param>
      /// <param name="descr">Описание</param>
      /// <param name="sqlQuery">Sql запрос</param>
      public AddNewAutoStatus(string caption, string descr, string sqlQuery)
      {
         InitializeComponent();
         //-----
         Text = caption;
         //-----
         _wf = new StateAutoWorkFlow(descr, sqlQuery);
         //-----
         tbDescription.DataBindings.Add("Text", this, "Description");
         tbSqlQuery.DataBindings.Add("Text", this, "SqlQuery");
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="caption">Шапочка формочки</param>
      public AddNewAutoStatus(string caption) : this(caption, "", "") { }
      /// <summary>
      /// Проверить sql запрос
      /// </summary>
      private void btnTestSql_Click(object sender, EventArgs e)
      {
         _wf.Execute(false);
      }

      private void AddNewAutoStatus_Shown(object sender, EventArgs e)
      {
          label1.Text = CLocaliz.TxT("Description");
          gbSql.Text = CLocaliz.TxT("Sql query");
          btnTestSql.Text = CLocaliz.TxT("Test sql");

      }
   }
}
