﻿namespace XICSM.UcrfRfaNET.StationsStatus
{
   partial class AddNewStatus
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.label1 = new System.Windows.Forms.Label();
          this.tbStatusFrom = new System.Windows.Forms.TextBox();
          this.tbStatusTo = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.label3 = new System.Windows.Forms.Label();
          this.label4 = new System.Windows.Forms.Label();
          this.cbEventType = new System.Windows.Forms.ComboBox();
          this.cbEvent = new System.Windows.Forms.ComboBox();
          this.btnOk = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.SuspendLayout();
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(12, 19);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(60, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Status from";
          // 
          // tbStatusFrom
          // 
          this.tbStatusFrom.Location = new System.Drawing.Point(114, 16);
          this.tbStatusFrom.Name = "tbStatusFrom";
          this.tbStatusFrom.Size = new System.Drawing.Size(175, 20);
          this.tbStatusFrom.TabIndex = 1;
          // 
          // tbStatusTo
          // 
          this.tbStatusTo.Location = new System.Drawing.Point(114, 96);
          this.tbStatusTo.Name = "tbStatusTo";
          this.tbStatusTo.Size = new System.Drawing.Size(175, 20);
          this.tbStatusTo.TabIndex = 3;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(12, 99);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(49, 13);
          this.label2.TabIndex = 2;
          this.label2.Text = "Status to";
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(12, 45);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(58, 13);
          this.label3.TabIndex = 4;
          this.label3.Text = "Event type";
          // 
          // label4
          // 
          this.label4.AutoSize = true;
          this.label4.Location = new System.Drawing.Point(12, 72);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(35, 13);
          this.label4.TabIndex = 5;
          this.label4.Text = "Event";
          // 
          // cbEventType
          // 
          this.cbEventType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbEventType.FormattingEnabled = true;
          this.cbEventType.Location = new System.Drawing.Point(114, 42);
          this.cbEventType.Name = "cbEventType";
          this.cbEventType.Size = new System.Drawing.Size(320, 21);
          this.cbEventType.TabIndex = 6;
          // 
          // cbEvent
          // 
          this.cbEvent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbEvent.FormattingEnabled = true;
          this.cbEvent.Location = new System.Drawing.Point(114, 69);
          this.cbEvent.Name = "cbEvent";
          this.cbEvent.Size = new System.Drawing.Size(320, 21);
          this.cbEvent.TabIndex = 7;
          // 
          // btnOk
          // 
          this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.btnOk.Location = new System.Drawing.Point(133, 140);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(75, 23);
          this.btnOk.TabIndex = 8;
          this.btnOk.Text = "Ok";
          this.btnOk.UseVisualStyleBackColor = true;
          // 
          // btnCancel
          // 
          this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnCancel.Location = new System.Drawing.Point(234, 140);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 9;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          // 
          // AddNewStatus
          // 
          this.AcceptButton = this.btnOk;
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.CancelButton = this.btnCancel;
          this.ClientSize = new System.Drawing.Size(459, 175);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOk);
          this.Controls.Add(this.cbEvent);
          this.Controls.Add(this.cbEventType);
          this.Controls.Add(this.label4);
          this.Controls.Add(this.label3);
          this.Controls.Add(this.tbStatusTo);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.tbStatusFrom);
          this.Controls.Add(this.label1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "AddNewStatus";
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Add a new status";
          this.Shown += new System.EventHandler(this.AddNewStatus_Shown);
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbStatusFrom;
      private System.Windows.Forms.TextBox tbStatusTo;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.ComboBox cbEventType;
      private System.Windows.Forms.ComboBox cbEvent;
      private System.Windows.Forms.Button btnOk;
      private System.Windows.Forms.Button btnCancel;
   }
}