﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.Documents;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.StationsStatus
{
   public partial class AddNewStatus : Form, INotifyPropertyChanged
   {
       Documents.Documents docTypes = new Documents.Documents();
                  
      private string _statusFrom;
      public string StatusFrom
      {
         get { return _statusFrom; }
         set
         {
            if (_statusFrom != value)
            {
               _statusFrom = value;
               NotifyPropertyChanged("StatusFrom");
            }
         }
      }

      private string _statusTo;
      public string StatusTo
      {
         get { return _statusTo; }
         set
         {
            if (_statusTo != value)
            {
               _statusTo = value;
               NotifyPropertyChanged("StatusTo");
            }
         }
      }

      private StationStatus.ETypeEvent _typeOfEvent;
      public StationStatus.ETypeEvent TypeOfEvent
      {
         get{ return _typeOfEvent;}
         set
         {
            if(_typeOfEvent != value)
            {
               _typeOfEvent = value;
               ReloadEventComboBox();
               NotifyPropertyChanged("TypeOfEvent");
            }
         }
      }

      private string _event;
      public string Event
      {
         get { return _event; }
         set
         {
            if (_event != value)
            {
               _event = value;
               NotifyPropertyChanged("Event");
            }
         }
      }
      //-----------
      private ComboBoxDictionaryList<StationStatus.ETypeEvent, string> _listEventType;
      private ComboBoxDictionaryList<string, string> _listEvent;
      //-----------
      /// <summary>
      /// Конструктор
      /// </summary>
      public AddNewStatus(string caption, string statusFrom, string statusTo, StationStatus.ETypeEvent eventType, string events)
      {
         InitializeComponent();
         //-------
         Text = caption;
         //-------
         _typeOfEvent = eventType;
         _event = events;
         _statusFrom = statusFrom;
         _statusTo = statusTo;
         //-------
         tbStatusFrom.DataBindings.Add("Text", this, "StatusFrom", true);
         tbStatusTo.DataBindings.Add("Text", this, "StatusTo", true);
         //==========
         _listEventType = new ComboBoxDictionaryList<StationStatus.ETypeEvent, string>();
         _listEventType.InitComboBox(cbEventType);
         Dictionary<StationStatus.ETypeEvent, string> dictOfEventType = StationStatus.GetHumanDictionaryTypeEvent();
         foreach (KeyValuePair<StationStatus.ETypeEvent, string> item in dictOfEventType)
            _listEventType.Add(new ComboBoxDictionary<StationStatus.ETypeEvent, string>(item.Key, item.Value));
         //==========
         _listEvent = new ComboBoxDictionaryList<string, string>();
         _listEvent.InitComboBox(cbEvent);
         //==========
         cbEventType.DataBindings.Add("SelectedValue", this, "TypeOfEvent", true);
         cbEvent.DataBindings.Add("SelectedValue", this, "Event", true);
         ReloadEventComboBox();
      }
      //=====================================
      public AddNewStatus(string caption)
         : this(caption, "", "", StationStatus.ETypeEvent.ApplPrintDoc, "")
      {
      }
      /// <summary>
      /// Перезагрузка списка событий
      /// </summary>
      private void ReloadEventComboBox()
      {
         _listEvent.Clear();
         switch (TypeOfEvent)
         {
            case StationStatus.ETypeEvent.ApplPrintDoc:
            case StationStatus.ETypeEvent.PacketPrintDoc:
            case StationStatus.ETypeEvent.AttachDoc:
               {
                  foreach (string docType in docTypes._docTypes.Keys)
                  {
                      _listEvent.Add(new ComboBoxDictionary<string, string>(docType, docTypes._docTypes[docType].desc));
                  }
               }
               break;
            case StationStatus.ETypeEvent.AttachAppl:
               {
                  foreach (string packetNames in Enum.GetNames(typeof(EPacketType)))
                  {
                     EPacketType packetTypeLocal = packetNames.ToPacketType();
                     string humanDocName = packetTypeLocal.GetHumanNameOfPacketType();
                     _listEvent.Add(new ComboBoxDictionary<string, string>(packetTypeLocal.ToStr(), humanDocName));
                  }
               }
               break;
            case StationStatus.ETypeEvent.EventLog:
               {
                  foreach (string events in Enum.GetNames(typeof (EDocEvent)))
                  {
                     EDocEvent eventTypeLocal = EventConvert.ToEvent(events);
                     string humanDocName = EventConvert.GetHumanNameOfEvent(eventTypeLocal);
                     _listEvent.Add(new ComboBoxDictionary<string, string>(EventConvert.ToStr(eventTypeLocal), humanDocName));
                  }
               }
               break;
            case StationStatus.ETypeEvent.TypeAppl:
            case StationStatus.ETypeEvent.DeleteAppl:
               {
                  foreach (string appl in Enum.GetNames(typeof (AppType)))
                  {
                     AppType applTypeLocal = appTypeConvert.StringToAppType(appl);
                     string humanDocName = appTypeConvert.GetHumanNameOfAppType(applTypeLocal);
                     _listEvent.Add(new ComboBoxDictionary<string, string>(appTypeConvert.AppTypeToString(applTypeLocal), humanDocName));
                  }
               }
               break;
         }
         cbEvent.DropDownHeight = cbEvent.ItemHeight * (_listEvent.Count + 1);
      }

      #region INotifyPropertyChanged
      public event PropertyChangedEventHandler PropertyChanged;

      // NotifyPropertyChanged will raise the PropertyChanged event passing the
      // source property that is being updated.
      public void NotifyPropertyChanged(string propertyName)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
         }
      }
      #endregion

      private void AddNewStatus_Shown(object sender, EventArgs e)
      {
          Text = CLocaliz.TxT("Add a new status");
          label1.Text = CLocaliz.TxT("Status from");
          label3.Text = CLocaliz.TxT("Event type");
          label4.Text = CLocaliz.TxT("Event");
          label2.Text = CLocaliz.TxT("Status to");
      }

   }
}
