﻿using System.ComponentModel;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.UtilityClass
{
    public partial class UserControlNotifyPropertyChanged : UserControl, INotifyPropertyChanged
    {
        public UserControlNotifyPropertyChanged()
        {
            InitializeComponent();
        }

        public const string FieldIsChanged = "IsChanged";
        protected object InLock = new object();
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get { return _isChanged; }
            set
            {
                if (_isChanged != value)
                {
                    _isChanged = value;
                    InvokeNotifyPropertyChanged(FieldIsChanged);
                }
            }
        }

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != FieldIsChanged)
                    IsChanged = true;
            }
        }
        #endregion
    }
}
