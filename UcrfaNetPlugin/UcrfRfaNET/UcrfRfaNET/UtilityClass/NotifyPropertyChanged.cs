﻿using System;
using System.ComponentModel;

namespace XICSM.UcrfRfaNET.UtilityClass
{
    public class NotifyPropertyChanged : INotifyPropertyChanged
    {
        public const string FieldIsChanged = "IsChanged";
        protected object InLock = new object();
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get { return _isChanged;}
            set
            {
                if(_isChanged != value)
                {
                    _isChanged = value;
                    InvokeNotifyPropertyChanged(FieldIsChanged);
                }
            }
        }

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != FieldIsChanged)
                    IsChanged = true;
            }
        }
        #endregion

        #region Thread safe function
        protected bool GetThreadSafeValue(ref bool oldVal)
        {
            bool retVal = false;
            lock (InLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref bool oldVal, bool newValue, string propertyName)
        {
            bool localChange = false;
            lock (InLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected string GetThreadSafeValue(ref string oldVal)
        {
            string retVal = "";
            lock (InLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref string oldVal, string newValue, string propertyName)
        {
            bool localChange = false;
            lock (InLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected DateTime GetThreadSafeValue(ref DateTime oldVal)
        {
            DateTime retVal;
            lock (InLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref DateTime oldVal, DateTime newValue, string propertyName)
        {
            bool localChange = false;
            lock (InLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected int GetThreadSafeValue(ref int oldVal)
        {
            int retVal;
            lock (InLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref int oldVal, int newValue, string propertyName)
        {
            bool localChange = false;
            lock (InLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }
        #endregion
    }
}
