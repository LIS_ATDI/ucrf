﻿using System.Windows;
using System;
using System.IO;
using System.Xml;
using ICSM;

namespace XICSM.UcrfRfaNET.Documents
{
    /// <summary>
    /// Типы операций
    /// </summary>
    internal enum OperationType
    {
        Unknown,
        Insert,
        Update,
        Delete,
    }
    /// <summary>
    /// Статус контракта
    /// </summary>
    internal enum StatusContructType
    {
        Unknown,
        Asserted,
        NotAsserted,
        Closed,
    }

    /// <summary>
    /// Класс разширения для OperationType
    /// </summary>
    internal static class OperationTypeExtension
    {
        //===================================================
        /// <summary>
        /// Преобразовывает строки в OperationType
        /// </summary>
        /// <param name="value">строка</param>
        /// <returns>OperationType</returns>
        public static OperationType ToOperationType(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return OperationType.Unknown;
            switch (value)
            {
                case "I":
                    return OperationType.Insert;
                case "U":
                    return OperationType.Update;
                case "D":
                    return OperationType.Delete;
            }
            return OperationType.Unknown;
        }
    }

    /// <summary>
    /// Класс разширения для StatusContructType
    /// </summary>
    internal static class StatusContructTypeExtension
    {
        //===================================================
        /// <summary>
        /// Преобразовывает строки в StatusContructType
        /// </summary>
        /// <param name="value">строка</param>
        /// <returns>StatusContructType</returns>
        public static StatusContructType ToStatusContructType(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return StatusContructType.Unknown;
            switch (value)
            {
                case "0":
                    return StatusContructType.Asserted;
                case "1":
                    return StatusContructType.NotAsserted;
                case "2":
                    return StatusContructType.Closed;
            }
            return StatusContructType.Unknown;
        }
    }


    internal class MonitoringContract
    {
        //===================================================
        /// <summary>
        /// Разбирает XML файл ответа из Паруса
        /// </summary>
        /// <param name="xml">текст XML</param>
        /// <param name="err">Код возможной ошибки</param>
        /// <param name="service"> Код сервиса</param>
        /// <returns>успешно или нет прошло чтение файла</returns>
        public static bool XmlMonitoringContractRead(string xml, out int err, string service)
        {
            string xmlschemaerror = "";
            if (!CXMLTest.TestXmlToSchema(xml, ref xmlschemaerror))
            {
                err = (int)DRVError.de_20;
                return false;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(new StringReader(xml));

            DateTime dateOper = IM.NullT;
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            OperationType typeOper = OperationType.Update;
            StatusContructType statusContract = StatusContructType.Unknown;
            int contractId = IM.NullI;
            int contractId_monitor_contracts = IM.NullI;
            Int32 parusContractId = 0;
            int userId = IM.NullI;
            int userId_monitor_contracts = IM.NullI;
            string numContract = "";
            string codeBranchOffice = "";



            string RN_AGENT = "";
            int RN_CONTRACTS = 0;
            int STATUS_CONTRACTS = 0;
            int TYPE_CONTRACTS = 0;

            int EmployeeId = IM.NullI;

            //DATE_OPER
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("DATE_OPER");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString == "")
                {
                    err = (int)DRVError.de_5;
                    return false;
                }
                try
                {
                    dateOper = Convert.ToDateTime(tmpString);
                }
                catch (Exception)
                {
                    err = (int)DRVError.de_7;
                    return false;
                }
            }

            //TYPE_OPER
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("TYPE_OPER");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                typeOper = tmpString.ToOperationType();
                if (typeOper == OperationType.Unknown)
                {
                    err = (int)DRVError.de_29;
                    return false;
                }
            }

            //RN_AGENT
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("RN_AGENT");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString == "")
                {
                    err = (int)DRVError.de_6;
                    return false;
                }




                double localGuid = ConvertType.ToDouble(tmpString, IM.NullD);
                if (localGuid == IM.NullD)
                {
                    err = (int)DRVError.de_8;
                    return false;
                }
                else
                {
                    RN_AGENT = tmpString.Trim();
                }

                RecordPtr recPtr = GlobalDB.CGlobalXML.GetRecordByGUID(localGuid, PlugTbl.MonitoringContract);
                userId = recPtr.Id;
                if (userId == 0)
                {
                    recPtr = GlobalDB.CGlobalXML.GetRecordByGUID(localGuid, ICSMTbl.USERS);
                    userId = recPtr.Id;
                }

                //recPtr = GlobalDB.CGlobalXML.GetRecordByGUID(localGuid, PlugTbl.xnrfa_monitor_contracts);
                //userId_monitor_contracts = recPtr.Id;
            }

            //RN_CONTRACTS
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("RN_CONTRACTS");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString == "")
                {
                    err = (int)DRVError.de_6;
                    return false;
                }

                double localGuid = ConvertType.ToDouble(tmpString, IM.NullD);
                if (localGuid == IM.NullD)
                {
                    err = (int)DRVError.de_8;
                    return false;
                }
                else
                {
                    RN_CONTRACTS = Convert.ToInt32(tmpString);
                }

                parusContractId = (Int32)localGuid;
                RecordPtr recPtr = GlobalDB.CGlobalXML.GetRecordByGUID(localGuid,PlugTbl.MonitoringContract);
                contractId = recPtr.Id;
                RecordPtr recPtrX = GlobalDB.CGlobalXML.GetRecordByGUID(localGuid, PlugTbl.xnrfa_monitor_contracts);
                contractId_monitor_contracts = recPtrX.Id;
            }

            //NUMB_CONTRACTS
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("NUMB_CONTRACTS");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                numContract = tmpString;
            }

            //STATUS_CONTRACTS
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("STATUS_CONTRACTS");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                statusContract = tmpString.ToStatusContructType();
                if (statusContract == StatusContructType.Unknown)
                {
                    err = (int)DRVError.de_30;
                    return false;
                }

                if (tmpString == "")
                {
                    err = (int)DRVError.de_6;
                    return false;
                }

                double localGuid = ConvertType.ToDouble(tmpString, IM.NullD);
                if (localGuid == IM.NullD)
                {
                    err = (int)DRVError.de_8;
                    return false;
                }
                else
                {
                    STATUS_CONTRACTS = Convert.ToByte(tmpString);
                }
            }

            //DATE_FROM
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("DATE_FROM");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString == "")
                {
                    err = (int)DRVError.de_5;
                    return false;
                }
                try
                {
                    dateFrom = Convert.ToDateTime(tmpString);
                }
                catch (Exception)
                {
                    err = (int)DRVError.de_7;
                    return false;
                }
            }

            //DATE_TO
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("DATE_TO");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString != "")
                {
                    try
                    {
                        dateTo = Convert.ToDateTime(tmpString);
                    }
                    catch (Exception)
                    {
                        err = (int)DRVError.de_7;
                        return false;
                    }
                }
            }
            //CODE_JURPERS
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("CODE_JURPERS");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                codeBranchOffice = tmpString;
                if (string.IsNullOrEmpty(codeBranchOffice))
                {
                    err = (int)DRVError.de_31;
                    return false;
                }
            }


            //EMPLOYEE_ID
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("EMPLOYEE_ID");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                if (tmpString == "")
                {
                    err = (int)DRVError.de_33;
                    return false;
                }

                EmployeeId = Convert.ToInt32(tmpString.ToString().Trim());

            }

            //TYPE_CONTRACTS
            {
                XmlNodeList itemNode = xmlDoc.GetElementsByTagName("TYPE_CONTRACTS");
                string tmpString = "";
                if (itemNode.Count > 0)
                    tmpString = itemNode[0].InnerText;
                
                if (tmpString == "")
                {
                    err = (int)DRVError.de_6;
                    return false;
                }

                double localGuid = ConvertType.ToDouble(tmpString, IM.NullD);
                if (localGuid == IM.NullD)
                {
                    err = (int)DRVError.de_8;
                    return false;
                }
                else
                {
                    TYPE_CONTRACTS = Convert.ToInt32(tmpString);
                }
            }


            switch (typeOper)
            {
                case OperationType.Insert:
                case OperationType.Update:
                    if (service == EService.ICSM)
                    {
                        try
                        {
                            using (Icsm.LisTransaction tr2 = new Icsm.LisTransaction())
                            {
                                using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.MonitoringContract, IMRecordset.Mode.ReadWrite))
                                {
                                    rs.Select("ID");
                                    rs.Select("USERS_ID");
                                    rs.Select("NUM_CONTRACT");
                                    rs.Select("STATUS_CONTRACT");
                                    rs.Select("DATE_FROM");
                                    rs.Select("DATE_TO");
                                    rs.Select("CODE_BRANCH_OFFICE");
                                    rs.Select("OPERATION_DATE");
                                    rs.Select("CREATED_DATE");
                                    rs.Select("MODIFIED_DATE");
                                    rs.SetWhere("ID", IMRecordset.Operation.Eq, contractId);
                                    rs.Open();
                                    //MessageBox.Show(contractId.ToString());
                                    
                                    if (rs.IsEOF())
                                    {
                                        rs.AddNew();
                                        int id = IM.AllocID(PlugTbl.MonitoringContract, 1, -1);
                                        GlobalDB.CGlobalXML.AddGUIDRecord(new RecordPtr(PlugTbl.MonitoringContract, id), parusContractId);
                                        rs.Put("ID", id);
                                        rs.Put("CREATED_DATE", DateTime.Now);
                                    }
                                    else
                                    {
                                        rs.Edit();
                                        rs.Put("MODIFIED_DATE", DateTime.Now);
                                    }
                                    rs.Put("USERS_ID", userId);
                                    rs.Put("NUM_CONTRACT", numContract);
                                    rs.Put("STATUS_CONTRACT", statusContract.ToString());
                                    rs.Put("DATE_FROM", dateFrom);
                                    rs.Put("DATE_TO", dateTo);
                                    rs.Put("OPERATION_DATE", dateOper);
                                    rs.Put("CODE_BRANCH_OFFICE", codeBranchOffice);
                                    rs.Update();

                                }
                                tr2.Commit();


                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }


                        try
                        {
                            using (Icsm.LisTransaction tr1 = new Icsm.LisTransaction())
                            {
                                using (Icsm.LisRecordSet rs1 = new Icsm.LisRecordSet(PlugTbl.xnrfa_monitor_contracts, IMRecordset.Mode.ReadWrite))
                                {
                                    rs1.Select("ID");
                                    rs1.Select("RN_AGENT");
                                    rs1.Select("RN_CONTRACTS");
                                    rs1.Select("NUMB_CONTRACTS");
                                    rs1.Select("STATUS_CONTRACTS");
                                    rs1.Select("TYPE_CONTRACTS");
                                    rs1.Select("DATE_FROM");
                                    rs1.Select("DATE_TO");
                                    rs1.Select("CODE_JURPERS");
                                    rs1.Select("CREATED_BY");
                                    rs1.Select("CREATED_DATE");
                                    rs1.Select("MODIFIED_BY");
                                    rs1.Select("MODIFIED_DATE");
                                    rs1.Select("CLOSED_BY");
                                    rs1.Select("CLOSED_DATE");
                                    rs1.SetWhere("ID", IMRecordset.Operation.Eq, contractId_monitor_contracts);
                                    rs1.Open();

                                    

                                    if (rs1.IsEOF())
                                    {

                                        rs1.AddNew();
                                        int id = IM.AllocID(PlugTbl.xnrfa_monitor_contracts, 1, -1);
                                        GlobalDB.CGlobalXML.AddGUIDRecord(new RecordPtr(PlugTbl.xnrfa_monitor_contracts, id), parusContractId);
                                        rs1.Put("ID", id);


                                        if (STATUS_CONTRACTS == 2)
                                        {
                                            rs1.Put("CREATED_BY", EmployeeId.ToString().TrimEnd());
                                            rs1.Put("CREATED_DATE", DateTime.Now);
                                            rs1.Put("CLOSED_BY", EmployeeId.ToString().TrimEnd());
                                            rs1.Put("CLOSED_DATE", DateTime.Now);
                                            rs1.Put("RN_AGENT", RN_AGENT);
                                            rs1.Put("RN_CONTRACTS", RN_CONTRACTS);
                                            rs1.Put("NUMB_CONTRACTS", numContract.TrimEnd());
                                            rs1.Put("STATUS_CONTRACTS", STATUS_CONTRACTS.ToString().TrimEnd());
                                            rs1.Put("TYPE_CONTRACTS", TYPE_CONTRACTS.ToString());
                                            rs1.Put("DATE_FROM", dateFrom);
                                            rs1.Put("DATE_TO", dateTo);
                                            rs1.Put("CODE_JURPERS", codeBranchOffice.TrimEnd());
                                        }
                                        else
                                        {

                                            rs1.Put("CREATED_BY", EmployeeId.ToString().TrimEnd());
                                            rs1.Put("CREATED_DATE", DateTime.Now);
                                            rs1.Put("RN_AGENT", RN_AGENT);
                                            rs1.Put("RN_CONTRACTS", RN_CONTRACTS);
                                            rs1.Put("NUMB_CONTRACTS", numContract.TrimEnd());
                                            rs1.Put("STATUS_CONTRACTS", STATUS_CONTRACTS.ToString().TrimEnd());
                                            rs1.Put("TYPE_CONTRACTS", TYPE_CONTRACTS.ToString());
                                            rs1.Put("DATE_FROM", dateFrom);
                                            rs1.Put("DATE_TO", dateTo);
                                            rs1.Put("CODE_JURPERS", codeBranchOffice.TrimEnd());
                                        }

                           
                                  

                                    }
                                    else
                                    {



                                        if (STATUS_CONTRACTS == 2)
                                        {
                                            rs1.Edit();
                                            rs1.Put("CLOSED_BY", EmployeeId.ToString().TrimEnd());
                                            rs1.Put("CLOSED_DATE", DateTime.Now);
                                            rs1.Put("RN_AGENT", RN_AGENT);
                                            rs1.Put("RN_CONTRACTS", RN_CONTRACTS);
                                            rs1.Put("NUMB_CONTRACTS", numContract.TrimEnd());
                                            rs1.Put("STATUS_CONTRACTS", STATUS_CONTRACTS.ToString().TrimEnd());
                                            rs1.Put("TYPE_CONTRACTS", TYPE_CONTRACTS.ToString());
                                            rs1.Put("DATE_FROM", dateFrom);
                                            rs1.Put("DATE_TO", dateTo);
                                            rs1.Put("CODE_JURPERS", codeBranchOffice.TrimEnd());
                                        }
                                        
                                            //if ((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim()) <= (Byte)STATUS_CONTRACTS) && ((Byte)STATUS_CONTRACTS <= 1))
                                        else if ((((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim()) <= (Byte)STATUS_CONTRACTS) && ((Byte)STATUS_CONTRACTS <= 1))) || (((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim()) > (Byte)STATUS_CONTRACTS) && ((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim()) <= 2))) && (Byte)STATUS_CONTRACTS >= 1) || ((((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim()) == 1) && ((Byte)STATUS_CONTRACTS) == 0))))
                                            {
                                                rs1.Edit();
                                                rs1.Put("MODIFIED_BY", EmployeeId.ToString().TrimEnd());
                                                rs1.Put("MODIFIED_DATE", DateTime.Now);
                                                rs1.Put("RN_AGENT", RN_AGENT);
                                                rs1.Put("RN_CONTRACTS", RN_CONTRACTS);
                                                rs1.Put("NUMB_CONTRACTS", numContract.TrimEnd());
                                                rs1.Put("STATUS_CONTRACTS", STATUS_CONTRACTS.ToString().TrimEnd());
                                                rs1.Put("TYPE_CONTRACTS", TYPE_CONTRACTS.ToString());
                                                rs1.Put("DATE_FROM", dateFrom);
                                                rs1.Put("DATE_TO", dateTo);
                                                rs1.Put("CODE_JURPERS", codeBranchOffice.TrimEnd());


                                            }

                                        
                                            if (((Convert.ToByte(rs1.GetS("STATUS_CONTRACTS").ToString().Trim())==2) && ((Byte)STATUS_CONTRACTS)==0))
                                        {
                                            rs1.AddNew();
                                            int id = IM.AllocID(PlugTbl.xnrfa_monitor_contracts, 1, -1);
                                            GlobalDB.CGlobalXML.AddGUIDRecord(new RecordPtr(PlugTbl.xnrfa_monitor_contracts, id), parusContractId);
                                            rs1.Put("ID", id);

                                            rs1.Put("CREATED_BY", EmployeeId.ToString().TrimEnd());
                                            rs1.Put("CREATED_DATE", DateTime.Now);
                                            rs1.Put("RN_AGENT", RN_AGENT);
                                            rs1.Put("RN_CONTRACTS", RN_CONTRACTS);
                                            rs1.Put("NUMB_CONTRACTS", numContract.TrimEnd());
                                            rs1.Put("STATUS_CONTRACTS", STATUS_CONTRACTS.ToString().TrimEnd());
                                            rs1.Put("TYPE_CONTRACTS", TYPE_CONTRACTS.ToString());
                                            rs1.Put("DATE_FROM", dateFrom);
                                            rs1.Put("DATE_TO", dateTo);
                                            rs1.Put("CODE_JURPERS", codeBranchOffice.TrimEnd());


                                        }
                                        


                                           

                                        
                                    }

                                  
                                    rs1.Update();
                                }
                                tr1.Commit();
                                
                            }
                        }

                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                    break;
                case OperationType.Delete:
                    if (service == EService.ICSM)
                    {
                        using (Icsm.LisTransaction tr2 = new Icsm.LisTransaction())
                        {
                            using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.MonitoringContract, IMRecordset.Mode.ReadWrite))
                            {
                                rs.Select("ID");
                                rs.SetWhere("ID", IMRecordset.Operation.Eq, contractId);
                                rs.Open();
                                if (!rs.IsEOF())
                                    rs.Delete();
                            }
                            tr2.Commit();
                        }
                    }
                    break;
            }
            err = 0;
            return true;
        }
    }
}
