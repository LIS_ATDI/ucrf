﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.OfficeBridge;
using System.Windows;

namespace XICSM.UcrfRfaNET.Documents
{
    /// <summary>
    /// Управление документами заявки
    /// </summary>
    internal class ApplDocuments
    {
        #region Permissions
        /// <summary>
        /// Добавить новое разрешение к заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">номер разрешения</param>
        /// <param name="dateFrom">дата от</param>
        /// <param name="dateTo">дата до</param>
        /// <param name="file">полный путь к файлу</param>
        public static void CreateNewPermission(int applId, string numPerm, DateTime dateFrom, DateTime dateTo, string file)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_NEW");
            rsAppl.Select("DOZV_DATE_FROM_NEW");
            rsAppl.Select("DOZV_DATE_TO_NEW");
            rsAppl.Select("DOZV_CREATED_BY_NEW");
            rsAppl.Select("DOZV_FILE_NEW");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if(!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("DOZV_NUM_NEW", numPerm);
                    rsAppl.Put("DOZV_DATE_FROM_NEW", dateFrom);
                    rsAppl.Put("DOZV_DATE_TO_NEW", dateTo);
                    rsAppl.Put("DOZV_CREATED_BY_NEW", IM.ConnectedUser());
                    rsAppl.Put("DOZV_FILE_NEW", file);
                    rsAppl.Update();
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }


        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNumber">Номер бланка</param>
        public static void PrintPermisionBase(int applId, string numPerm, DateTime printDate, string blankNumber)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_NEW");
            rsAppl.Select("DOZV_DATE_FROM_NEW");
            rsAppl.Select("DOZV_DATE_TO_NEW");
            rsAppl.Select("DOZV_CREATED_BY_NEW");
            rsAppl.Select("DOZV_FILE_NEW");

            rsAppl.Select("DOZV_NUM");
            rsAppl.Select("DOZV_DATE_FROM");
            rsAppl.Select("DOZV_DATE_TO");
            rsAppl.Select("DOZV_DATE_PRINT");
            rsAppl.Select("DOZV_DATE_CANCEL");
            rsAppl.Select("DOZV_CREATED_BY");
            rsAppl.Select("DOZV_FILE");

            rsAppl.Select("DOZV_NUM_OLD");
            rsAppl.Select("DOZV_DATE_FROM_OLD");
            rsAppl.Select("DOZV_DATE_TO_OLD");
            rsAppl.Select("DOZV_DATE_PRINT_OLD");
            rsAppl.Select("DOZV_DATE_CANCEL_OLD");
            rsAppl.Select("DOZV_CREATED_BY_OLD");
            rsAppl.Select("DOZV_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newPermNum = rsAppl.GetS("DOZV_NUM_NEW");
                    if (numPerm != newPermNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating permission print date. Can't find permision number {0}. Print date: {1}. Current permission number is {2}",
                                             numPerm, printDate, newPermNum));
                    }
                    else
                    {
                        // DOZV_NUM_NEW -> DOZV_NUM -> DOZV_NUM_OLD
                        // DOZV_DATE_FROM_NEW -> DOZV_DATE_PRINT -> DOZV_DATE_PRINT_OLD
                        // DOZV_DATE_TO_NEW -> DOZV_DATE_TO -> DOZV_DATE_TO_OLD
                        // DOZV_CREATED_BY_NEW -> DOZV_CREATED_BY -> DOZV_CREATED_BY_OLD
                        // DOZV_FILE_NEW -> DOZV_FILE -> DOZV_FILE_OLD
                        // Print DRV -> DOZV_DATE_FROM -> DOZV_DATE_FROM_OLD
                        // DOZV_DATE_CANCEL -> DOZV_DATE_CANCEL_OLD

                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("DOZV_NUM_OLD", rsAppl.GetS("DOZV_NUM"));
                        rsAppl.Put("DOZV_DATE_FROM_OLD", rsAppl.GetT("DOZV_DATE_FROM"));
                        rsAppl.Put("DOZV_DATE_TO_OLD", rsAppl.GetT("DOZV_DATE_TO"));
                        rsAppl.Put("DOZV_DATE_PRINT_OLD", rsAppl.GetT("DOZV_DATE_PRINT"));
                        rsAppl.Put("DOZV_DATE_CANCEL_OLD", rsAppl.GetT("DOZV_DATE_CANCEL"));
                        rsAppl.Put("DOZV_CREATED_BY_OLD", rsAppl.GetS("DOZV_CREATED_BY"));
                        rsAppl.Put("DOZV_FILE_OLD", rsAppl.GetS("DOZV_FILE"));
                        // Новое разрешение в текущее
                        DateTime dateTo = rsAppl.GetT("DOZV_DATE_TO_NEW");
                        rsAppl.Put("DOZV_NUM", rsAppl.GetS("DOZV_NUM_NEW"));
                        rsAppl.Put("DOZV_DATE_PRINT", rsAppl.GetT("DOZV_DATE_FROM_NEW"));
                        rsAppl.Put("DOZV_DATE_TO", dateTo);
                        rsAppl.Put("DOZV_DATE_FROM", printDate);
                        rsAppl.Put("DOZV_DATE_CANCEL", IM.NullT);
                        rsAppl.Put("DOZV_CREATED_BY", rsAppl.GetS("DOZV_CREATED_BY_NEW"));
                        rsAppl.Put("DOZV_FILE", rsAppl.GetS("DOZV_FILE_NEW"));
                        rsAppl.Update();
                        AttachmentControl.UpdateAttachmentData(numPerm, printDate, blankNumber);
                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Обновление данных для печати
        /// </summary>
        /// <param name="applId">ID заявки</param>

        public static void PrintPermision(int applId, string numPerm, DateTime printDate, string blankNumber)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_DATE_FROM_NEW");
            rsAppl.Select("DOZV_DATE_PRINT");
            rsAppl.Select("DOZV_DATE_PRINT_OLD");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {

                    rsAppl.Edit();
                    rsAppl.Put("DOZV_DATE_PRINT_OLD", rsAppl.GetT("DOZV_DATE_PRINT"));
                    rsAppl.Put("DOZV_DATE_PRINT", rsAppl.GetT("DOZV_DATE_FROM_NEW"));
                    rsAppl.Update();
                    AttachmentControl.UpdateAttachmentData(numPerm, printDate, blankNumber);
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Обновление данных для события "Видано дозвіл"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNumber">Номер бланка</param>
        public static void IssuedDocPermision_XFA_ABONENT_STATION(int applId, string numPerm, DateTime printDate, string blankNumber)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_NEW");
            rsAppl.Select("DOZV_DATE_TO_NEW");
            rsAppl.Select("DOZV_CREATED_BY_NEW");
            rsAppl.Select("DOZV_FILE_NEW");

            rsAppl.Select("DOZV_NUM");
            rsAppl.Select("DOZV_DATE_FROM");
            rsAppl.Select("DOZV_DATE_TO");
            rsAppl.Select("DOZV_DATE_PRINT");
            rsAppl.Select("DOZV_DATE_CANCEL");
            rsAppl.Select("DOZV_CREATED_BY");
            rsAppl.Select("DOZV_FILE");

            rsAppl.Select("DOZV_NUM_OLD");
            rsAppl.Select("DOZV_DATE_FROM_OLD");
            rsAppl.Select("DOZV_DATE_TO_OLD");
            rsAppl.Select("DOZV_DATE_PRINT_OLD");
            rsAppl.Select("DOZV_DATE_CANCEL_OLD");
            rsAppl.Select("DOZV_CREATED_BY_OLD");
            rsAppl.Select("DOZV_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newPermNum = rsAppl.GetS("DOZV_NUM_NEW");

                    if (numPerm != newPermNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating permission print date. Can't find permision number {0}. Print date: {1}. Current permission number is {2}",
                                             numPerm, printDate, newPermNum));
                    }
                    else
                    {


                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("DOZV_NUM_OLD", rsAppl.GetS("DOZV_NUM"));
                        rsAppl.Put("DOZV_DATE_FROM_OLD", rsAppl.GetT("DOZV_DATE_FROM"));
                        rsAppl.Put("DOZV_DATE_TO_OLD", rsAppl.GetT("DOZV_DATE_TO"));
                        rsAppl.Put("DOZV_DATE_PRINT_OLD", rsAppl.GetT("DOZV_DATE_PRINT"));
                        rsAppl.Put("DOZV_DATE_CANCEL_OLD", rsAppl.GetT("DOZV_DATE_CANCEL"));
                        rsAppl.Put("DOZV_CREATED_BY_OLD", rsAppl.GetS("DOZV_CREATED_BY"));
                        rsAppl.Put("DOZV_FILE_OLD", rsAppl.GetS("DOZV_FILE"));
                        // Новое разрешение в текущее
                        DateTime dateTo = rsAppl.GetT("DOZV_DATE_TO_NEW");
                        rsAppl.Put("DOZV_NUM", rsAppl.GetS("DOZV_NUM_NEW"));
                        rsAppl.Put("DOZV_DATE_FROM", printDate);
                        rsAppl.Put("DOZV_DATE_TO", dateTo);
                        rsAppl.Put("DOZV_DATE_PRINT", DateTime.Now);
                        rsAppl.Put("DOZV_DATE_CANCEL", IM.NullT);
                        rsAppl.Put("DOZV_CREATED_BY", rsAppl.GetS("DOZV_CREATED_BY_NEW"));
                        rsAppl.Put("DOZV_FILE", rsAppl.GetS("DOZV_FILE_NEW"));
                        rsAppl.Update();
                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }


        /// <summary>
        /// Обновление данных для события "Видано дозвіл"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNumber">Номер бланка</param>
        public static void IssuedDocPermision(int applId, string numPerm, DateTime printDate, string blankNumber)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_NEW");
            rsAppl.Select("DOZV_DATE_TO_NEW");
            rsAppl.Select("DOZV_CREATED_BY_NEW");
            rsAppl.Select("DOZV_FILE_NEW");

            rsAppl.Select("DOZV_NUM");
            rsAppl.Select("DOZV_DATE_FROM");
            rsAppl.Select("DOZV_DATE_TO");
            rsAppl.Select("DOZV_DATE_CANCEL");
            rsAppl.Select("DOZV_CREATED_BY");
            rsAppl.Select("DOZV_FILE");

            rsAppl.Select("DOZV_NUM_OLD");
            rsAppl.Select("DOZV_DATE_FROM_OLD");
            rsAppl.Select("DOZV_DATE_TO_OLD");
            rsAppl.Select("DOZV_DATE_CANCEL_OLD");
            rsAppl.Select("DOZV_CREATED_BY_OLD");
            rsAppl.Select("DOZV_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newPermNum = rsAppl.GetS("DOZV_NUM_NEW");

                    if (numPerm != newPermNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating permission print date. Can't find permision number {0}. Print date: {1}. Current permission number is {2}",
                                             numPerm, printDate, newPermNum));
                    }
                    else
                    {


                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("DOZV_NUM_OLD", rsAppl.GetS("DOZV_NUM"));
                        rsAppl.Put("DOZV_DATE_FROM_OLD", rsAppl.GetT("DOZV_DATE_FROM"));
                        rsAppl.Put("DOZV_DATE_TO_OLD", rsAppl.GetT("DOZV_DATE_TO"));
                        rsAppl.Put("DOZV_DATE_CANCEL_OLD", rsAppl.GetT("DOZV_DATE_CANCEL"));
                        rsAppl.Put("DOZV_CREATED_BY_OLD", rsAppl.GetS("DOZV_CREATED_BY"));
                        rsAppl.Put("DOZV_FILE_OLD", rsAppl.GetS("DOZV_FILE"));
                        // Новое разрешение в текущее
                        DateTime dateTo = rsAppl.GetT("DOZV_DATE_TO_NEW");
                        rsAppl.Put("DOZV_NUM", rsAppl.GetS("DOZV_NUM_NEW"));
                        rsAppl.Put("DOZV_DATE_TO", dateTo);
                        rsAppl.Put("DOZV_DATE_FROM", printDate);
                        rsAppl.Put("DOZV_DATE_CANCEL", IM.NullT);
                        rsAppl.Put("DOZV_CREATED_BY", rsAppl.GetS("DOZV_CREATED_BY_NEW"));
                        rsAppl.Put("DOZV_FILE", rsAppl.GetS("DOZV_FILE_NEW"));
                        rsAppl.Update();
                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
       
        /// <summary>
        /// Обновление данных о печати документа
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="cancelDate">Дата анулирования</param>
        public static void CancelPermision(int applId, DateTime cancelDate)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_DATE_CANCEL");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("DOZV_DATE_CANCEL", cancelDate);
                    rsAppl.Update();
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
        /// <summary>
        /// Загружает данные дозвола
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="dateFrom">Дата ОТ</param>
        /// <param name="dateTo">Дата ДО</param>
        /// <param name="datePrint">Дата печати</param>
        /// <param name="dateCancel">Дата анулирования</param>
        public static void GetPermData(int applId, out string numPerm, out DateTime dateFrom, out DateTime dateTo, out DateTime datePrint, out DateTime dateCancel)
        {
            numPerm = "";
            dateFrom = IM.NullT;
            dateTo = IM.NullT;
            datePrint = IM.NullT;
            dateCancel = IM.NullT;
            //---
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM");
            rsAppl.Select("DOZV_DATE_FROM");
            rsAppl.Select("DOZV_DATE_TO");
            rsAppl.Select("DOZV_DATE_PRINT");
            rsAppl.Select("DOZV_DATE_CANCEL");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    numPerm = rsAppl.GetS("DOZV_NUM");
                    dateFrom = rsAppl.GetT("DOZV_DATE_FROM");
                    dateTo = rsAppl.GetT("DOZV_DATE_TO");
                    datePrint = rsAppl.GetT("DOZV_DATE_PRINT");
                    dateCancel = rsAppl.GetT("DOZV_DATE_CANCEL");
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Загружает номера разрешения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Номер разрешения</returns>
        public static string GetPermNum(int applId)
        {
            string numPerm = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            DateTime dateCancel = IM.NullT;
            GetPermData(applId, out numPerm, out dateFrom, out dateTo, out datePrint, out dateCancel);
            return numPerm;
        }
        //==============================================================
        /// <summary>
        /// Возвращает корректную дату окончания разрешения
        /// </summary>
        public static DateTime GetValidPermissionDateTo(int applId)
        {
            string numPerm = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            DateTime dateCancel = IM.NullT;
            GetPermData(applId, out numPerm, out dateFrom, out dateTo, out datePrint, out dateCancel);
            if ((datePrint == IM.NullT || datePrint > DateTime.Now) ||
                (dateCancel != IM.NullT && (string.Compare(dateCancel.ToString("yyyy.MM.dd"), DateTime.Now.ToString("yyyy.MM.dd")) < 0)))
                dateTo = IM.NullT;
            return dateTo;
        }
        //==============================================================
        /// <summary>
        /// Возвращает корректную дату начала дозвола разрешения
        /// </summary>
        public static DateTime GetValidPermissionDateFrom(int applId)
        {
            string numPerm = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            DateTime dateCancel = IM.NullT;
            GetPermData(applId, out numPerm, out dateFrom, out dateTo, out datePrint, out dateCancel);
            if (/*(datePrint == IM.NullT || datePrint > DateTime.Now) ||*/
                (dateCancel != IM.NullT && (string.Compare(dateCancel.ToString("yyyy.MM"), DateTime.Now.ToString("yyyy.MM")) < 0)))
                dateFrom = IM.NullT;
            return dateFrom;
        }
        /// <summary>
        /// Загружает данные старого разрешения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="dateFrom">Дата ОТ</param>
        /// <param name="dateTo">Дата ДО</param>
        /// <param name="datePrint">Дата печати</param>
        /// <param name="dateCancel">Дата анулирования</param>
        public static void GetOldPermData(int applId, out string numPerm, out DateTime dateFrom, out DateTime dateTo, out DateTime datePrint, out DateTime dateCancel)
        {
            numPerm = "";
            dateFrom = IM.NullT;
            dateTo = IM.NullT;
            datePrint = IM.NullT;
            dateCancel = IM.NullT;
            //---
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_OLD");
            rsAppl.Select("DOZV_DATE_FROM_OLD");
            rsAppl.Select("DOZV_DATE_TO_OLD");
            rsAppl.Select("DOZV_DATE_PRINT_OLD");
            rsAppl.Select("DOZV_DATE_CANCEL_OLD");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    numPerm = rsAppl.GetS("DOZV_NUM_OLD");
                    dateFrom = rsAppl.GetT("DOZV_DATE_FROM_OLD");
                    dateTo = rsAppl.GetT("DOZV_DATE_TO_OLD");
                    datePrint = rsAppl.GetT("DOZV_DATE_PRINT_OLD");
                    dateCancel = rsAppl.GetT("DOZV_DATE_CANCEL_OLD");
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
        /// <summary>
        /// Загружает номера старого разрешения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Номер старого разрешения</returns>
        public static string GetOldPermNum(int applId)
        {
            string numPerm = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            DateTime dateCancel = IM.NullT;
            GetOldPermData(applId, out numPerm, out dateFrom, out dateTo, out datePrint, out dateCancel);
            return numPerm;
        }
        /// <summary>
        /// Загружает данные нового разрешения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numPerm">Номер разрешения</param>
        /// <param name="dateFrom">Дата ОТ</param>
        /// <param name="dateTo">Дата ДО</param>
        /// <param name="filePath">Путь к файлу</param>
        public static void GetNewPermData(int applId, out string numPerm, out DateTime dateFrom, out DateTime dateTo, out string filePath)
        {
            numPerm = "";
            dateFrom = IM.NullT;
            dateTo = IM.NullT;
            filePath = "";
            //---
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID");
            rsAppl.Select("DOZV_NUM_NEW");
            rsAppl.Select("DOZV_DATE_FROM_NEW");
            rsAppl.Select("DOZV_DATE_TO_NEW");
            rsAppl.Select("DOZV_FILE_NEW");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    numPerm = rsAppl.GetS("DOZV_NUM_NEW");
                    dateFrom = rsAppl.GetT("DOZV_DATE_FROM_NEW");
                    dateTo = rsAppl.GetT("DOZV_DATE_TO_NEW");
                    filePath = rsAppl.GetS("DOZV_FILE_NEW");
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
        /// <summary>
        /// Загружает номера нового разрешения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Номер нового разрешения</returns>
        public static string GetNewPermNum(int applId)
        {
            string numPerm = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            string filePath = "";
            GetNewPermData(applId, out numPerm, out dateFrom, out dateTo, out filePath);
            return numPerm;
        }
        #endregion
        #region Conclusion
        /// <summary>
        /// Добавить новое заключение к заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numConc">номер заключения</param>
        /// <param name="dateFrom">дата от</param>
        /// <param name="dateTo">дата до</param>
        /// <param name="file">полный путь к файлу</param>
        public static void CreateNewConclusion(int applId, string numConc, DateTime dateFrom, DateTime dateTo, string file)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM_NEW");
            rsAppl.Select("CONC_DATE_FROM_NEW");
            rsAppl.Select("CONC_DATE_TO_NEW");
            rsAppl.Select("CONC_FILE_NEW");
            rsAppl.Select("CONC_CREATED_BY_NEW");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("CONC_NUM_NEW", numConc);
                    rsAppl.Put("CONC_DATE_FROM_NEW", dateFrom);
                    rsAppl.Put("CONC_DATE_TO_NEW", dateTo);
                    rsAppl.Put("CONC_CREATED_BY_NEW", IM.ConnectedUser());
                    rsAppl.Put("CONC_FILE_NEW", file);
                    rsAppl.Update();
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Напечатано заключения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numConc">Номер заключения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNum">Номер бланка</param>
        public static void PrintConclusionBase(int applId, string numConc, DateTime printDate, string blankNum)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);

            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM_NEW");
            rsAppl.Select("CONC_DATE_FROM_NEW");
            rsAppl.Select("CONC_DATE_TO_NEW");
            rsAppl.Select("CONC_FILE_NEW");
            rsAppl.Select("CONC_CREATED_BY_NEW");

            rsAppl.Select("CONC_NUM");
            rsAppl.Select("CONC_DATE_FROM");
            rsAppl.Select("CONC_DATE_TO");
            rsAppl.Select("CONC_DATE_PRINT");
            rsAppl.Select("CONC_CREATED_BY");
            rsAppl.Select("CONC_FILE");

            rsAppl.Select("CONC_NUM_OLD");
            rsAppl.Select("CONC_DATE_FROM_OLD");
            rsAppl.Select("CONC_DATE_TO_OLD");
            rsAppl.Select("CONC_DATE_PRINT_OLD");
            rsAppl.Select("CONC_CREATED_BY_OLD");
            rsAppl.Select("CONC_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newConcNum = rsAppl.GetS("CONC_NUM_NEW");
                    if (numConc != newConcNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating conclusion print date. Can't find conclusion number {0}. Print date: {1}. Current conclusion number is {2}",
                                             numConc, printDate, newConcNum));
                    }
                    else
                    {
                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("CONC_NUM_OLD", rsAppl.GetS("CONC_NUM"));
                        rsAppl.Put("CONC_DATE_FROM_OLD", rsAppl.GetT("CONC_DATE_FROM"));
                        rsAppl.Put("CONC_DATE_TO_OLD", rsAppl.GetT("CONC_DATE_TO"));
                        rsAppl.Put("CONC_DATE_PRINT_OLD", rsAppl.GetT("CONC_DATE_PRINT"));
                        rsAppl.Put("CONC_CREATED_BY_OLD", rsAppl.GetS("CONC_CREATED_BY"));
                        rsAppl.Put("CONC_FILE_OLD", rsAppl.GetS("CONC_FILE"));
                        // Новое разрешение в текущее
                        DateTime dateTo = CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId));
                        rsAppl.Put("CONC_NUM", rsAppl.GetS("CONC_NUM_NEW"));
                        rsAppl.Put("CONC_DATE_FROM", printDate);
                        rsAppl.Put("CONC_DATE_TO", CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId)));
                        rsAppl.Put("CONC_DATE_PRINT", rsAppl.GetT("CONC_DATE_FROM_NEW"));
                        rsAppl.Put("CONC_CREATED_BY", rsAppl.GetS("CONC_CREATED_BY_NEW"));
                        rsAppl.Put("CONC_FILE", rsAppl.GetS("CONC_FILE_NEW"));
                        rsAppl.Update();
                        AttachmentControl.UpdateAttachmentData(numConc, printDate, blankNum);
                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Для conclusion
        /// </summary>
        /// <param name="applId">ID заявки</param>
        public static void PrintConclusion(int applId, string numConc, DateTime printDate, string blankNum)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);

            rsAppl.Select("ID");
            rsAppl.Select("CONC_DATE_FROM_NEW");
            rsAppl.Select("CONC_DATE_PRINT");
            rsAppl.Select("CONC_DATE_PRINT_OLD");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {

                    rsAppl.Edit();
                    // Копируем текущее разрешение в старое
                    rsAppl.Put("CONC_DATE_PRINT_OLD", rsAppl.GetT("CONC_DATE_PRINT"));
                    rsAppl.Put("CONC_DATE_PRINT", rsAppl.GetT("CONC_DATE_FROM_NEW"));
                    rsAppl.Update();
                    AttachmentControl.UpdateAttachmentData(numConc, printDate, blankNum);
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <param name="numConc"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="file"></param>
        public static void CreateNewConclusion_XFA_ABONENT_STATION(int applId, string numConc, DateTime dateFrom, DateTime dateTo, string file)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM_NEW");
            rsAppl.Select("CONC_DATE_FROM_NEW");
            rsAppl.Select("CONC_DATE_TO_NEW");
            rsAppl.Select("CONC_FILE_NEW");
            rsAppl.Select("CONC_CREATED_BY_NEW");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("CONC_NUM_NEW", numConc);
                    rsAppl.Put("CONC_DATE_FROM_NEW", dateFrom);
                    rsAppl.Put("CONC_DATE_TO_NEW", dateTo);
                    rsAppl.Put("CONC_FILE_NEW", file);
                    rsAppl.Put("CONC_CREATED_BY_NEW", IM.ConnectedUser());
                    rsAppl.Update();
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Для события "Видано висновок"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numConc">Номер заключения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNum">Номер бланка</param>
        public static void IssuedDocConclusion_XFA_ABONENT_STATION(int applId, string numConc, DateTime printDate, string blankNum)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);

            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM_NEW");
            rsAppl.Select("CONC_DATE_TO_NEW");
            rsAppl.Select("CONC_FILE_NEW");
            rsAppl.Select("CONC_CREATED_BY_NEW");

            rsAppl.Select("CONC_NUM");
            rsAppl.Select("CONC_DATE_FROM");
            rsAppl.Select("CONC_DATE_TO");
            rsAppl.Select("CONC_DATE_PRINT");
            rsAppl.Select("CONC_CREATED_BY");
            rsAppl.Select("CONC_FILE");

            rsAppl.Select("CONC_NUM_OLD");
            rsAppl.Select("CONC_DATE_FROM_OLD");
            rsAppl.Select("CONC_DATE_TO_OLD");
            rsAppl.Select("CONC_DATE_PRINT_OLD");
            rsAppl.Select("CONC_CREATED_BY_OLD");
            rsAppl.Select("CONC_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newConcNum = rsAppl.GetS("CONC_NUM_NEW");
                    if (numConc != newConcNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating conclusion print date. Can't find conclusion number {0}. Print date: {1}. Current conclusion number is {2}",
                                             numConc, printDate, newConcNum));
                    }
                    else
                    {
                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("CONC_NUM_OLD", rsAppl.GetS("CONC_NUM"));
                        rsAppl.Put("CONC_DATE_FROM_OLD", rsAppl.GetT("CONC_DATE_FROM"));
                        rsAppl.Put("CONC_DATE_TO_OLD", rsAppl.GetT("CONC_DATE_TO"));
                        rsAppl.Put("CONC_DATE_PRINT_OLD", rsAppl.GetT("CONC_DATE_PRINT"));
                        rsAppl.Put("CONC_FILE_OLD", rsAppl.GetS("CONC_FILE"));
                        rsAppl.Put("CONC_CREATED_BY_OLD", rsAppl.GetS("CONC_CREATED_BY"));
                        // Новое разрешение в текущее
                        DateTime dateTo = CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId));
                        rsAppl.Put("CONC_NUM", rsAppl.GetS("CONC_NUM_NEW"));
                        rsAppl.Put("CONC_DATE_FROM", printDate);
                        rsAppl.Put("CONC_DATE_TO", CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId)));
                        rsAppl.Put("CONC_DATE_PRINT", DateTime.Now);
                        rsAppl.Put("CONC_FILE", rsAppl.GetS("CONC_FILE_NEW"));
                        rsAppl.Put("CONC_CREATED_BY", rsAppl.GetS("CONC_CREATED_BY_NEW"));
                        rsAppl.Update();

                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        /// <summary>
        /// Для события "Видано висновок"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numConc">Номер заключения</param>
        /// <param name="printDate">дата печати</param>
        /// <param name="blankNum">Номер бланка</param>
        public static void IssuedDocConclusion(int applId, string numConc, DateTime printDate, string blankNum)
        {
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);

            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM_NEW");
            rsAppl.Select("CONC_DATE_TO_NEW");
            rsAppl.Select("CONC_FILE_NEW");
            rsAppl.Select("CONC_CREATED_BY_NEW");

            rsAppl.Select("CONC_NUM");
            rsAppl.Select("CONC_DATE_FROM");
            rsAppl.Select("CONC_DATE_TO");
            rsAppl.Select("CONC_CREATED_BY");
            rsAppl.Select("CONC_FILE");

            rsAppl.Select("CONC_NUM_OLD");
            rsAppl.Select("CONC_DATE_FROM_OLD");
            rsAppl.Select("CONC_DATE_TO_OLD");
            rsAppl.Select("CONC_CREATED_BY_OLD");
            rsAppl.Select("CONC_FILE_OLD");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    string newConcNum = rsAppl.GetS("CONC_NUM_NEW");
                    if (numConc != newConcNum)
                    {
                        CLogs.WriteError(ELogsWhat.Appl,
                                         string.Format(
                                             "Updating conclusion print date. Can't find conclusion number {0}. Print date: {1}. Current conclusion number is {2}",
                                             numConc, printDate, newConcNum));
                    }
                    else
                    {
                        rsAppl.Edit();
                        // Копируем текущее разрешение в старое
                        rsAppl.Put("CONC_NUM_OLD", rsAppl.GetS("CONC_NUM"));
                        rsAppl.Put("CONC_DATE_FROM_OLD", rsAppl.GetT("CONC_DATE_FROM"));
                        rsAppl.Put("CONC_DATE_TO_OLD", rsAppl.GetT("CONC_DATE_TO"));
                        rsAppl.Put("CONC_CREATED_BY_OLD", rsAppl.GetS("CONC_CREATED_BY"));
                        rsAppl.Put("CONC_FILE_OLD", rsAppl.GetS("CONC_FILE"));
                        // Новое разрешение в текущее
                        DateTime dateTo = CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId));
                        rsAppl.Put("CONC_NUM", rsAppl.GetS("CONC_NUM_NEW"));
                        rsAppl.Put("CONC_DATE_FROM", printDate);
                        rsAppl.Put("CONC_DATE_TO", CalculateMaxConcDateTo(printDate, BaseAppClass.GetStandardFromAppl(applId)));
                        rsAppl.Put("CONC_CREATED_BY", rsAppl.GetS("CONC_CREATED_BY_NEW"));
                        rsAppl.Put("CONC_FILE", rsAppl.GetS("CONC_FILE_NEW"));
                        rsAppl.Update();

                    }
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
        
        /// <summary>
        /// Загружает данные заключения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="numConc">Номер заключения</param>
        /// <param name="dateFrom">Дата ОТ</param>
        /// <param name="dateTo">Дата ДО</param>
        /// <param name="datePrint">Дата печати</param>
        public static void GetConcData(int applId, out string numConc, out DateTime dateFrom, out DateTime dateTo, out string path)
        {
            numConc = "";
            dateFrom = IM.NullT;
            dateTo = IM.NullT;
            path = "";
            //---
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM");
            rsAppl.Select("CONC_DATE_FROM");
            rsAppl.Select("CONC_DATE_TO");
            rsAppl.Select("CONC_DATE_PRINT");
            rsAppl.Select("CONC_FILE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    numConc = rsAppl.GetS("CONC_NUM");
                    dateFrom = rsAppl.GetT("CONC_DATE_FROM");
                    dateTo = rsAppl.GetT("CONC_DATE_TO");
                    path = rsAppl.GetS("CONC_FILE");
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }

        public static void GetConcData(int applId, out string numConc, out DateTime dateFrom, out DateTime dateTo, out DateTime datePrint)
        {
            numConc = "";
            dateFrom = IM.NullT;
            dateTo = IM.NullT;
            datePrint = IM.NullT;
            //---
            IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID");
            rsAppl.Select("CONC_NUM");
            rsAppl.Select("CONC_DATE_FROM");
            rsAppl.Select("CONC_DATE_TO");
            rsAppl.Select("CONC_DATE_PRINT");
            rsAppl.Select("CONC_DATE_PRINT");

            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    numConc = rsAppl.GetS("CONC_NUM");
                    dateFrom = rsAppl.GetT("CONC_DATE_FROM");
                    dateTo = rsAppl.GetT("CONC_DATE_TO");
                    datePrint = rsAppl.GetT("CONC_DATE_PRINT");
                }
            }
            finally
            {
                rsAppl.Final();
            }
        }
        /// <summary>
        /// Загружает номер заключения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Номер заключения</returns>
        public static string GetConcNum(int applId)
        {
            string numConc = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            GetConcData(applId, out numConc, out dateFrom, out dateTo, out datePrint);
            return numConc;
        }
        /// <summary>
        /// Загружает дату ОТ заключения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Дата ОТ заключения</returns>
        public static DateTime GetConcDateFrom(int applId)
        {
            string numConc = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            GetConcData(applId, out numConc, out dateFrom, out dateTo, out datePrint);
            return dateFrom;
        }
        /// <summary>
        /// Загружает дату ДО заключения
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Дата ДО заключения</returns>
        public static DateTime GetConcDateTo(int applId)
        {
            string numConc = "";
            DateTime dateFrom = IM.NullT;
            DateTime dateTo = IM.NullT;
            DateTime datePrint = IM.NullT;
            GetConcData(applId, out numConc, out dateFrom, out dateTo, out datePrint);
            return dateTo;
        }

        /// <summary>
        /// Возвращает максимально допустимую дату заключения (дата окончания заключения)
        /// </summary>
        /// <param name="startDate">Дата, ото которой начинаеться отсчет</param>
        /// <param name="radiotech">Радиотехнология</param>
        /// <returns>Максимально допустимую дату заключения (дата окончания заключения)</returns>
        public static DateTime CalculateMaxConcDateTo(DateTime startDate, string radiotech)
        {
            switch (radiotech)
            {
                case CRadioTech.DVBT:
                case CRadioTech.DVBT2:
                case CRadioTech.ATM:
                case CRadioTech.AAB:
                    return startDate.AddMonths(12);
                default:
                    return startDate.AddMonths(6);
            }
        }
        #endregion

        internal static void SendToPrinter(string filePath, bool visible)
        {
#if DEBUG
            MessageBox.Show("SendToPrinter(\"" + filePath + "\", " + visible + ")...", "Debug message");
#endif
            WordBridge word = new WordBridge();
            try
            {
                word.Init();
                word.ShowWord(visible);
                word.OpenFile(filePath);
                word.Print();
                word.CloseDoc();
            }
            finally
            {
                word.Dispose();
            }
        }
    }
}
