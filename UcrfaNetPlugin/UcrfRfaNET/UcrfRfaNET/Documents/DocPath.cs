﻿using System;
using System.ComponentModel;
using XICSM.UcrfRfaNET.UtilityClass;
using ICSM;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Documents
{
    internal class DocPath
    {
        public class DocPathType : NotifyPropertyChanged
        {
            public const string FieldPath = "Path";
            public const string FieldDepTypeString = "DepTypeString";
            public const string FieldDocTypeString = "DocTypeString";

            private string _docTypeValue = DocType.DocUnknown;
            private string _depTypeValue = "";
            private string _path = "";

            private static Documents docTypes = new Documents();
            private static Dictionary<string, string> deptTypes = EnumLoader.LoadEnum("EMPLOYEE_TYPE"); 
            /// <summary>
            /// Тип документа
            /// </summary>
            public string DocTypeValue
            {
                get { return _docTypeValue; }
                set
                {
                    if (_docTypeValue != value)
                    {
                        _docTypeValue = value;
                        UpdateFilter();
                    }
                }
            }
            /// <summary>
            /// Ногрмальное название типа документа
            /// </summary>
            public string DocTypeString { get { return docTypes._docTypes[DocTypeValue].desc; } }
            /// <summary>
            /// Тип департамета
            /// </summary>
            public string DepTypeValue
            {
                get { return _depTypeValue; }
                set
                {
                    if (_depTypeValue != value)
                    {
                        _depTypeValue = value;
                        UpdateFilter();
                    }
                }
            }
            /// <summary>
            /// Ногрмальное название департамета
            /// </summary>
            public string DepTypeString { get { return (deptTypes.GetItemDescription(DepTypeValue)); } }
            /// <summary>
            /// Путь к папке
            /// </summary>
            public string Path
            {
                get { return GetThreadSafeValue(ref _path); }
                set { SetThreadSafeValue(ref _path, value, FieldPath); }
            }

            public string Filter { get; protected set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public DocPathType()
            {
                UpdateFilter();
            }
            /// <summary>
            /// Обновляет фильтр
            /// </summary>
            private void UpdateFilter()
            {
                Filter = "XRFA_" + DepTypeValue + "_" + DocTypeValue;
            }
        }

        public BindingList<DocPathType> ListDocPath { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public DocPath()
        {
            ListDocPath = new BindingList<DocPathType>();
        }
        /// <summary>
        /// Загрузить данные
        /// </summary>
        public void Load()
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.Loading))
            {
                pb.SetProgress(0, 600);
                pb.SetBig("Загрузка папок");

                //загрузить
                Dictionary<string, string> DepartmentList = EnumLoader.LoadEnum("EMPLOYEE_TYPE");
                // legacy departments
                /*
                if (DepartmentList.ContainsKey("VRR"))
                    DepartmentList.Add("VRR", "ВРР");
                if (DepartmentList.ContainsKey("VFSR"))
                    DepartmentList.Add("VFSR", "ВФСР");
                if (DepartmentList.ContainsKey("VRS"))
                    DepartmentList.Add("VRS", "ВРС");
                if (DepartmentList.ContainsKey("VMA"))
                    DepartmentList.Add("VMA", "ВМА");
                if (DepartmentList.ContainsKey("FILIA"))
                    DepartmentList.Add("FILIA", "Филия");
                if (DepartmentList.ContainsKey("URCM"))
                    DepartmentList.Add("URCM", "УРЧМ");
                if (DepartmentList.ContainsKey("URZP"))
                    DepartmentList.Add("URZP", "УРЗП");
                if (DepartmentList.ContainsKey("VRZ"))
                    DepartmentList.Add("VRZ", "ВРЗ");
                 * */

                Documents docTypes = new Documents();
                foreach (string docTypeName in docTypes._docTypes.Keys)
                {
                    foreach (string depTypeName in DepartmentList.Keys)
                    {
                        if (depTypeName == "" || PluginSetting.PluginFolderSetting.UserTypeCodesToIgnore.Contains(depTypeName))
                            continue;

                        DocPathType tmp = new DocPathType();
                        tmp.DocTypeValue = docTypeName;
                        tmp.DepTypeValue = depTypeName;
                        switch (tmp.DocTypeValue)
                        {
                            case DocType.DocUnknown: //
                            case DocType.AKT_PTK: // Есть отдельная папка для всех актов ПТК
                                continue;
                        }
                        
                        using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly))
                        {
                            rs.Select("ITEM,WHAT");
                            rs.SetWhere("ITEM", IMRecordset.Operation.Eq, tmp.Filter);
                            rs.Open();
                            if (!rs.IsEOF())
                                tmp.Path = rs.GetS("WHAT");
                        }
                        tmp.IsChanged = false;
                        ListDocPath.Add(tmp);
                        pb.Increment(true);
                    }
                }
            }
        }
        /// <summary>
        /// Сохранить данные
        /// </summary>
        public void Save()
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.Saving))
            {
                pb.SetProgress(0, ListDocPath.Count);
                pb.SetBig("Збереження папок");
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    foreach (DocPathType path in ListDocPath)
                    {
                        using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadWrite))
                        {
                            rs.Select("ITEM,WHAT");
                            rs.SetWhere("ITEM", IMRecordset.Operation.Like, path.Filter);
                            rs.Open();
                            if (rs.IsEOF())
                            {
                                rs.AddNew();
                                rs.Put("ITEM", path.Filter);
                            }
                            else
                            {
                                rs.Edit();
                            }
                            rs.Put("WHAT", path.Path);
                            rs.Update();
                        }
                        pb.Increment(true);
                    }
                    tr.Commit();
                }
            }
        }
        /// <summary>
        /// Найти путь к файлу
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="depType">Тип департамета</param>
        /// <returns>Путь к файлу</returns>
        /*
        public string Find(string docType, DepartmentType depType)
        {
            foreach (DocPathType path in ListDocPath)
            {
                if ((path.DocTypeValue == docType) && (path.DepTypeValue == depType))
                    return path.Path;
            }
            return "";
        }
         * */
    }
}
