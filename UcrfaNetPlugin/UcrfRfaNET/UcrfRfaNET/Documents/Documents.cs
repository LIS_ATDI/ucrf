﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.Documents
{
   //======================================================
   /// <summary>
   /// Типы документов
   /// </summary>
    class DocType
    {
        public const string LYST_TR_CALLSIGN = "LYST_TR_CALLSIGN";      //Лист-повідомлення про призначення позивних сигналів
        public const string LYST_ZRS_CALLSIGN = "LYST_ZRS_CALLSIGN";    //Лист-повідомлення про призначення позивних сигналів
        public const string DOZV_AR3_SPEC = "DOZV_AR3_SPEC";            //Тимчасовий спеціальний дозвіл 
        public const string DOZV_TR_SPEC = "DOZV_TR_SPEC";             //Тимчасовий спеціальний дозвіл 
        public const string DOZV_TRAIN         = "DOZV_TRAIN";          //Дозвіл новый
        public const string DOZV_FOREIGHT      = "DOZV_FOREIGHT";       //Дозвіл
        public const string DocUnknown         = "";                    //Неизвестный
        public const string VISN               = "VISN";                //Висновок
        public const string DOZV               = "DOZV";                //Дозвіл
        public const string PCR                = "PCR";                 //Повідомлення про скасування рішення про видачу висновків
        public const string sentPCR            = "sentPCR";             //Відправленно документ "Повідомлення про скасування рішення про видачу висновків"
        public const string VVP                = "VVP";                 //Відмова у наданні дозволу
        public const string sentVVP            = "sentVVP";             //Відправленно документ "Відмова у наданні дозволу"
        public const string DRV                = "DRV";                 //Службова записка до ДРВ на рахунок
        public const string VVE                = "VVE";                 //Відмова у наданні висновку щодо ЕМС
        public const string sentVVE            = "sentVVE";             //Відправленно повідомлення про "Відмова у наданні висновку щодо ЕМС"
        public const string LYST               = "LYST";                //Повідомлення про необхідність узгодження МК
        public const string _0159              = "_0159";               //На узгодження з ГШ ЗС України
        public const string FYLIA              = "FYLIA";               //Запит до регіональної філії УДЦР або УРЧМ (коорд.+адреса)
        public const string PCR2               = "PCR2";                //Залишення без розгляду (форма 2)
        public const string LYST_N             = "LYST_N";              //Повідомлення про необхідність узгодження МК (негативна)
        public const string _0159_N            = "_0159_N";             //На узгодження з ГШ ЗС України (негативна)
        public const string VISN_NR            = "VISN_NR";             //Висновок в НР
        public const string DOZV_OPER          = "DOZV_OPER";           //Дозвіл оператора
        public const string TEST_TV            = "TEST_TV";             //Погодження тестового включення (ТВ)
        public const string TEST_NV            = "TEST_NV";             //Погодження тестового включення (НВ)
        public const string TEST_PTK           = "TEST_PTK";            //Погодження тестового включення (ПТК)
        public const string TEST_PTK_NV        = "TEST_PTK_NV";         //Погодження тестового включення (ПТК + НВ)
        public const string TEST_PCR2          = "TEST_PCR2";           //Погодження тестового включення + залишення без розгляду
        public const string LIST_FYLIA         = "LIST_FYLIA";          //Лист до філії (перенаправлення заявочних документів)
        public const string NOTICE_PTK         = "NOTICE_PTK";          //Попередження щодо визначення заявником строків ПТК
        public const string URCM_PTK           = "URCM_PTK";            //Службова записка до УРЧМ щодо їх участі в ПТК
        public const string URCM_DOC           = "URCM_DOC";            //Службова записка до УРЧП про передачу заявочних документів
        public const string AKT_PTK            = "AKT_PTK";             //АКТ ПТК
        public const string URCM_NOTICE        = "URCM_NOTICE";         //Службова записка до УРЧМ
        public const string DOZV_CANCEL        = "DOZV_CANCEL";         //Анулювання дозволу
        public const string REESTR_VID         = "REESTR_VID";          //Реєстраційну відомість
        public const string DOZV_CANCEL_ANNUL  = "DOZV_CANCEL_ANNUL";   //Скасування анулювання дозволу
        public const string GIVE_LICENCE       = "GIVE_LICENCE";        //Разрешение на выдачу лицензии
        public const string MESS_DOZV_CANCEL   = "MESS_DOZV_CANCEL";    //Повідомлення про скасування рішення про видачу дозволу
        public const string COPY_APPL_LAW      = "COPY_APPL_LAW";       //Приєднання документа "Копія позовної заяви до суду"
        public const string SELECTION_FREQ     = "SELECTION_FREQ";      //Подбор частот
        public const string CALCUL_EMC         = "CALCUL_EMC";          //Подсчет ЕМС
        public const string ATTACH_DOZV_BY_NKRZ= "ATTACH_DOZV_BY_NKRZ"; //Анулювання дозволу рішенням НКРЗ
        public const string NV                 = "NV";                  // Натурні випробування
        public const string TV                 = "TV";                  // Тестові випробування
        public const string MP                 = "MP";                  // Вимірювання параметрів РЕЗ
        public const string ITMP               = "ITMP";                // Інструм. оцінка вимірювання параметрів РЕЗ
        public const string GENERAL            = "GENERAL";             // Загальні висновки
        public const string AKT_TE_PTK         = "AKT_TE_PTK";          // Акт інструментальної оцінки
        public const string DOZV_SPS           = "DOZV_SPS";            // Дозвіл на використання СПС
        public const string GARM_CERT          = "GARM_CERT";           // Гарм. екз. серифікат (тип)
        public const string NOVIC_CERT         = "NOVIC_CERT";          // Гарм. екз. серифікат (тип)
        public const string DOZV_MOVE          = "DOZV_MOVE";           // Дозвіл на рухому станцію
        public const string DOZV_RETR          = "DOZV_RETR";           // Дозвіл на ретранслятор
        public const string DOZV_APC           = "DOZV_APC";            // Дозвіл на АРС 
        public const string DOZV_APC50         = "DOZV_APC50";          // Дозвіл на АРС 50 Мгц
        public const string PARAM_REZ          = "PARAM_REZ";           // Протокол вимірювання параметрів РЕЗ      
        public const string REPORT             = "REPORT";              // Звіт (задача 6587, службова записка №16/06 від 05.І.2012, Кіріченка В.І.)
        public const string ARMY = "ARMY";                             // Звіт (задача 6587, службова записка №16/06 від 05.І.2012, Кіріченка В.І.)
        public const string ADOC = "ADOC";                             // Інший док

        private string _code;
        public string code { get { return _code; } }
        public string desc { get; set; }
        private bool _builtIn;
        public bool builtIn { get { return _builtIn; } }
        public bool urcpAccessible { get; set; }
        public bool urzpAccessible { get; set; }
        public bool branchAccessible { get; set; }
        public bool packetAccessible { get; set; }

        public DocType(string __code, string _desc, bool __builtIn, bool _urcpAccessible, bool _urzpAccessible, bool _branchAccessible, bool _packetAccessible)
        {
            _code = __code;
            desc = _desc;
            _builtIn = __builtIn;
            urcpAccessible = _urcpAccessible;
            urzpAccessible = _urzpAccessible;
            branchAccessible = _branchAccessible;
            packetAccessible = _packetAccessible;
        }
   }
   /// <summary>
   /// Класс работы с документами
   /// </summary>
   class Documents
   {                
        /// <summary>
        /// Словарь названий документов
        /// </summary>
        public Dictionary<string, DocType> _docTypes = null;
        /// <summary>
        /// Конструктор
        /// </summary>
        public Documents()
        {
                
            _docTypes = new Dictionary<string, DocType>();
            _docTypes.Add(DocType.DocUnknown, new DocType(DocType.DocUnknown, "Неизвестный", true, false, false, false, false));
            _docTypes.Add(DocType.VISN, new DocType(DocType.VISN, "Висновок", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV, new DocType(DocType.DOZV, "Дозвіл", true, false, false, false, false));
            _docTypes.Add(DocType.PCR, new DocType(DocType.PCR, "Повідомлення про скасування рішення про видачу висновків", true, false, false, false, false));
            _docTypes.Add(DocType.sentPCR, new DocType(DocType.sentPCR, "Відправленно документ \"Повідомлення про скасування рішення про видачу висновків\"", true, false, false, false, false));
            _docTypes.Add(DocType.VVP, new DocType(DocType.VVP, "Відмова у наданні дозволу", true, false, false, false, false));
            _docTypes.Add(DocType.sentVVP, new DocType(DocType.sentVVP, "Відправленно документ \"Відмова у наданні дозволу\"", true, false, false, false, false));
            _docTypes.Add(DocType.DRV, new DocType(DocType.DRV, "Службова записка до ДРВ на рахунок", true, false, false, false, false));
            _docTypes.Add(DocType.VVE, new DocType(DocType.VVE, "Відмова у наданні висновку щодо ЕМС", true, false, false, false, false));
            _docTypes.Add(DocType.sentVVE, new DocType(DocType.sentVVE, "Відправленно повідомлення про \"Відмова у наданні висновку щодо ЕМС\"", true, false, false, false, false));
            _docTypes.Add(DocType.LYST, new DocType(DocType.LYST, "Повідомлення про необхідність узгодження МК", true, false, false, false, false));
            _docTypes.Add(DocType._0159, new DocType(DocType._0159, "На узгодження з ГШ ЗС України", true, false, false, false, false));
            _docTypes.Add(DocType.FYLIA, new DocType(DocType.FYLIA, "Запит до регіональної філії УДЦР або УРЧМ (коорд.+адреса)", true, false, false, false, false));
            _docTypes.Add(DocType.PCR2, new DocType(DocType.PCR2, "Залишення без розгляду (форма 2)", true, false, false, false, false));
            _docTypes.Add(DocType.LYST_N, new DocType(DocType.LYST_N, "Повідомлення про необхідність узгодження МК (негативна)", true, false, false, false, false));
            _docTypes.Add(DocType._0159_N, new DocType(DocType._0159_N, "На узгодження з ГШ ЗС України (негативна)", true, false, false, false, false));
            _docTypes.Add(DocType.VISN_NR, new DocType(DocType.VISN_NR, "Висновок в НР", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_OPER, new DocType(DocType.DOZV_OPER, "Дозвіл оператора", true, false, false, false, false));
            _docTypes.Add(DocType.TEST_TV, new DocType(DocType.TEST_TV, "Погодження тестового включення (ТВ)", true, false, false, false, false));
            _docTypes.Add(DocType.TEST_NV, new DocType(DocType.TEST_NV, "Погодження тестового включення (НВ)", true, false, false, false, false));
            _docTypes.Add(DocType.TEST_PTK, new DocType(DocType.TEST_PTK, "Погодження тестового включення (ПТК)", true, false, false, false, false));
            _docTypes.Add(DocType.TEST_PTK_NV, new DocType(DocType.TEST_PTK_NV, "Погодження тестового включення (ПТК + НВ)", true, false, false, false, false));
            _docTypes.Add(DocType.TEST_PCR2, new DocType(DocType.TEST_PCR2, "Погодження тестового включення + залишення без розгляду", true, false, false, false, false));
            _docTypes.Add(DocType.LIST_FYLIA, new DocType(DocType.LIST_FYLIA, "Лист до філії (перенаправлення заявочних документів)", true, false, false, false, false));
            _docTypes.Add(DocType.NOTICE_PTK, new DocType(DocType.NOTICE_PTK, "Попередження щодо визначення заявником строків ПТК", true, false, false, false, false));
            _docTypes.Add(DocType.URCM_PTK, new DocType(DocType.URCM_PTK, "Службова записка до УРЧМ щодо їх участі в ПТК", true, false, false, false, false));
            _docTypes.Add(DocType.URCM_DOC, new DocType(DocType.URCM_DOC, "Службова записка до УРЧП про передачу заявочних документів", true, false, false, false, false));
            _docTypes.Add(DocType.AKT_PTK, new DocType(DocType.AKT_PTK, "АКТ ПТК", true, false, false, false, false));
            _docTypes.Add(DocType.URCM_NOTICE, new DocType(DocType.URCM_NOTICE, "Службова записка до УРЧМ", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_CANCEL, new DocType(DocType.DOZV_CANCEL, "Анулювання дозволу", true, false, false, false, false));
            _docTypes.Add(DocType.REESTR_VID, new DocType(DocType.REESTR_VID, "Реєстраційну відомість", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_CANCEL_ANNUL, new DocType(DocType.DOZV_CANCEL_ANNUL, "Скасування анулювання дозволу", true, false, false, false, false));
            _docTypes.Add(DocType.GIVE_LICENCE, new DocType(DocType.GIVE_LICENCE, "Разрешение на выдачу лицензии", true, false, false, false, false));
            _docTypes.Add(DocType.MESS_DOZV_CANCEL, new DocType(DocType.MESS_DOZV_CANCEL, "Повідомлення про скасування рішення про видачу дозволу", true, false, false, false, false));
            _docTypes.Add(DocType.COPY_APPL_LAW, new DocType(DocType.COPY_APPL_LAW, "Копія позовної заяви до суду", true, false, false, false, false));
            _docTypes.Add(DocType.SELECTION_FREQ, new DocType(DocType.SELECTION_FREQ, "Подбор частот", true, false, false, false, false));
            _docTypes.Add(DocType.CALCUL_EMC, new DocType(DocType.CALCUL_EMC, "Подсчет ЕМС", true, false, false, false, false));
            _docTypes.Add(DocType.ATTACH_DOZV_BY_NKRZ, new DocType(DocType.ATTACH_DOZV_BY_NKRZ, "Анулювання дозволу рішенням НКРЗ", true, false, false, false, false));
            _docTypes.Add(DocType.NV, new DocType(DocType.NV, "Натурні випробування", true, false, false, false, false));
            _docTypes.Add(DocType.TV, new DocType(DocType.TV, "Тестові випробування", true, false, false, false, false));
            _docTypes.Add(DocType.MP, new DocType(DocType.MP, "Вимірювання параметрів РЕЗ", true, false, false, false, false));
            _docTypes.Add(DocType.ITMP, new DocType(DocType.ITMP, "Інструм. оцінка вимірювання параметрів РЕЗ", true, false, false, false, false));
            _docTypes.Add(DocType.GENERAL, new DocType(DocType.GENERAL, "Загальні висновки", true, false, false, false, false));
            _docTypes.Add(DocType.AKT_TE_PTK, new DocType(DocType.AKT_TE_PTK, "Акт інструментальної оцінки", true, false, false, false, false));
            _docTypes.Add(DocType.GARM_CERT, new DocType(DocType.GARM_CERT, "Гармонізований екзаменаційний сертифікат", true, false, false, false, false));
            _docTypes.Add(DocType.NOVIC_CERT, new DocType(DocType.NOVIC_CERT, "Гармонізований екзаменаційний сертифікат", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_SPS, new DocType(DocType.DOZV_SPS, "Дозвіл на використання СПС", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_RETR, new DocType(DocType.DOZV_RETR, "Дозвіл на ретранслятор", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_APC, new DocType(DocType.DOZV_APC, "Дозвіл на АРС", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_APC50, new DocType(DocType.DOZV_APC50, "Дозвіл на АРС50", true, false, false, false, false));
            _docTypes.Add(DocType.PARAM_REZ, new DocType(DocType.PARAM_REZ, "Протокол вимірювання параметрів РЕЗ", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_MOVE, new DocType(DocType.DOZV_MOVE, "Дозвіл на рухому станцію", true, false, false, false, false));
            _docTypes.Add(DocType.REPORT,              new DocType(DocType.REPORT,              "Звіт", true, false, false, false,false));
            _docTypes.Add(DocType.ARMY, new DocType(DocType.ARMY, "Лист в ГШ тест", true, false, false, false, false));
            _docTypes.Add(DocType.ADOC, new DocType(DocType.ADOC, "Інший документ", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_FOREIGHT, new DocType(DocType.DOZV_FOREIGHT, "Дозвіл іноземцям", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_TRAIN, new DocType(DocType.DOZV_TRAIN, "Дозвіл для Укрзалізниці", true, false, false, false, false));
            _docTypes.Add(DocType.LYST_TR_CALLSIGN, new DocType(DocType.LYST_TR_CALLSIGN, "Лист-повідомлення про призначення позивних сигналів", true, false, false, false, false));
            _docTypes.Add(DocType.LYST_ZRS_CALLSIGN, new DocType(DocType.LYST_ZRS_CALLSIGN, "Лист-повідомлення про призначення позивних сигналів", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_AR3_SPEC, new DocType(DocType.DOZV_AR3_SPEC, "Тимчасовий спеціальний дозвіл ", true, false, false, false, false));
            _docTypes.Add(DocType.DOZV_TR_SPEC, new DocType(DocType.DOZV_TR_SPEC, "Тимчасовий спеціальний дозвіл ", true, false, false, false, false));

            IMRecordset rs = new IMRecordset("COMBO", IMRecordset.Mode.ReadOnly);
            rs.Select("DOMAIN,ITEM,LANG,LEGEN");
            rs.SetWhere("DOMAIN", IMRecordset.Operation.Eq, "DocType");
            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
            {
                string Code = rs.GetS("ITEM");
                if (Code == "VE" || Code == "DE")
                    continue;
                string Description = rs.GetS("LEGEN");
                DocType docType = _docTypes.ContainsKey(Code) ? _docTypes[Code] : null;
                if (docType == null)
                {
                    docType = new DocType(Code, Description, false, false, false, false,false);
                    _docTypes.Add(docType.code, docType);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Description))
                        docType.desc = Description;
                }
            }
            rs.Destroy();
        }

        private void ClearAccessibility()
        {
            foreach (DocType dt in _docTypes.Values)
            {
                dt.urcpAccessible = false;
                dt.urzpAccessible = false;
                dt.branchAccessible = false;
                dt.packetAccessible = false;
            }
        }

        private const string confPrefix = "XUCRF_DOCTYPE_ACC_";

        public void LoadWfConfiguration()
        {
            ClearAccessibility();
            IMRecordset rsConf = new IMRecordset("SYS_CONFIG", ICSM.IMRecordset.Mode.ReadOnly);
            rsConf.Select("ITEM,WHAT");
            rsConf.SetWhere("ITEM", IMRecordset.Operation.Like, confPrefix + "*");
            for (rsConf.Open(); !rsConf.IsEOF(); rsConf.MoveNext())
            {
                string docType = rsConf.GetS("ITEM").Substring(confPrefix.Length).TrimEnd().ToUpper();
                if (_docTypes.ContainsKey(docType))
                {
                    DocType docTypeEntry = _docTypes[docType];
                    string conf = rsConf.GetS("WHAT");
                    docTypeEntry.urcpAccessible = conf.Length > 0 && conf[0] == '+';
                    docTypeEntry.urzpAccessible = conf.Length > 1 && conf[1] == '+';
                    docTypeEntry.branchAccessible = conf.Length > 2 && conf[2] == '+';
                    docTypeEntry.packetAccessible  = conf.Length > 3 && conf[3] == '+';
                }
            }       
            rsConf.Destroy();
        }

        public void SaveWfConfiguration()
        {
            IM.AdminDisconnect();
            IM.Execute("delete from %SYS_CONFIG where ITEM like '" + confPrefix + "%'");
            IMRecordset rsConf = new IMRecordset("SYS_CONFIG", ICSM.IMRecordset.Mode.ReadWrite);
            rsConf.Select("ITEM,WHAT");
            rsConf.SetWhere("ITEM", IMRecordset.Operation.Like, confPrefix + "*");
            rsConf.Open();
            foreach (DocType dt in _docTypes.Values)
            {
                if (dt.urcpAccessible || dt.urzpAccessible || dt.branchAccessible || dt.packetAccessible)
                {
                    rsConf.AddNew();
                    rsConf.Put("ITEM", confPrefix + dt.code);
                    rsConf.Put("WHAT", "" + (dt.urcpAccessible ? '+' : '-') + (dt.urzpAccessible ? '+' : '-') + (dt.branchAccessible ? '+' : '-') + (dt.packetAccessible ? '+' : '-'));
                    rsConf.Update();
                }
            }
            rsConf.Destroy();
        }

        public DocType GetDocType(string code)
        {
            if (_docTypes.ContainsKey(code) == true)
                return _docTypes[code];
            else 
                throw new Exception ("No such document type ('"+code+"')");
        }

      /// <summary>
      /// Возвращает человеческое название документа
      /// </summary>
      /// <param name="docType">Тп документа</param>
      /// <returns>Название документа</returns>
      public string GetHumanNameOfDoc(string docType)
      {
         if (_docTypes.ContainsKey(docType) == true)
            return CLocaliz.TxT(_docTypes[docType].desc);
         return "";
      }
   }
   //======================================================
   /// <summary>
   /// Класс конвертирует тип StatusType
   /// </summary>
   public static class DocTypeConvert
   {
      //===================================================
      /// <summary>
      /// Конвертирует тип документа в событие
      /// </summary>
      /// <param name="val">что конвертировать</param>
      /// <returns>сроковое представление типа</returns>
      public static EDocEvent ToEventType(this string val)
      {
          EDocEvent retEvent = EDocEvent.evAttachDoc;
          switch (val)
          {
              case DocType.ATTACH_DOZV_BY_NKRZ:
                  retEvent = EDocEvent.evDocAnnulByNkrz;
                  break;
              case DocType.AKT_TE_PTK:
                  retEvent = EDocEvent.evAKT_TE_PTK;
                  break;
              case DocType.PARAM_REZ:
                  retEvent = EDocEvent.evPARAM_REZ;
                  break;
              case DocType.CALCUL_EMC:
                  retEvent = EDocEvent.evCalculationEms;
                  break;
              case DocType.DOZV_CANCEL:
                  retEvent = EDocEvent.evDocCancellation;
                  break;
              case DocType.DOZV_SPS:              
                  retEvent = EDocEvent.evSPS;
                  break;
              case DocType.DOZV_MOVE:
                  retEvent = EDocEvent.evMOVE;
                  break;
              case DocType.NOVIC_CERT:
              case DocType.GARM_CERT:
                  retEvent = EDocEvent.evSERT;
                  break;
              case DocType.DOZV_RETR:
              case DocType.DOZV_APC:
              case DocType.DOZV_APC50:
              case DocType.DOZV_FOREIGHT:
              case DocType.DOZV_TRAIN:
                 retEvent = EDocEvent.evDOZV;
                  break;
              case DocType.LYST_TR_CALLSIGN:
              case DocType.LYST_ZRS_CALLSIGN:
                  retEvent = EDocEvent.evApplCallSign;
                  break;
              case DocType.DOZV_AR3_SPEC:
              case DocType.DOZV_TR_SPEC:
                  retEvent = EDocEvent.evSpecialDozv;
                  break;
          }
          return retEvent;
      }
   }
}
