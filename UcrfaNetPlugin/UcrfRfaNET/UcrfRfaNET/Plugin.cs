﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.ApplSource.Forms;
using XICSM.UcrfRfaNET.CalculationVRR;
using XICSM.UcrfRfaNET.CalculationVRS;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.Logs;
using XICSM.UcrfRfaNET.Map;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using XICSM.UcrfRfaNET.GlobalDB;
using XICSM.UcrfRfaNET.Monitoring;
using XICSM.UcrfRfaNET.Reminder;
using OrmCs;
using IdwmNET;
using NearestCountryItem = IdwmNET.NearestCountryItem;


namespace XICSM.UcrfRfaNET
{
    public class Plugin : IPlugin
    {
        private Idwm _idwmVal;
        internal Control IcsmMainWindows { get; private set; }
        //Версия плагина
        public string Ident { get { return "UcrfRfaNET"; } }
        public string Description { get { return null; } }
        public double SchemaVersion { get { return UpdateSchema.schemaVersion; } }
        public static Station curStation { get; set; }
        public static List<RecordPtr> listData {get; set;}
        public static bool isResize = false;
        public List<Station> List_Stations = new List<Station>();
        public List<Station> List_Stations_Another = new List<Station>();
        public List<Station> List_Stations_Microwa = new List<Station>();
        private List<string> _curr_prov;
        private bool isStopped = false;
        public Plugin()
        {
            curStation = null;
            listData = new List<RecordPtr>();
            Lis.CommonLib.Extensions.CDoubleExtension.NullDouble = IM.NullD;
            Lis.CommonLib.Extensions.DateTimeExtension.NullDate = IM.NullT;
            Lis.CommonLib.Extensions.CIntegerExtension.NullInteger = IM.NullI;
            IcsmMainWindows = null;
            if (OrmCs.OrmSchema.Ev == null)
                OrmCs.OrmSchema.Ev = new IMExecEnv();
        }

        //===================================================
        public bool OtherMessage(string message, Object inParam, ref Object outParam)
        {
            System.Diagnostics.Debug.Indent();
            System.Diagnostics.Debug.WriteLine("OtherMessage(" + message + ")");
            System.Diagnostics.Debug.Unindent();
            switch (message)
            {
                case "ON_OPEN_WORKSPACE":
                    {
                        try
                        {
                            const string msg = "Initialization setting database";
                            using (LisProgressBar pb = new LisProgressBar(msg))
                            {
                                pb.SetBig(msg);
                                pb.SetSmall(CLocaliz.PleaseWait);
                                LocalSetting.DbSqLite.InitDatabase(IM.GetWorkspaceFolder());
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Помилка ініціалізації налаштувань плагіна: "+'\n'+ ex.Message, CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        try
                        {
                            // Инициализация управления
                            CUsers.InitUserDepartment();
                            UcrfDepartment curDepartment = CUsers.GetCurDepartment();
                            if (curDepartment == UcrfDepartment.UNKNOWN)
                            {
                                if (CUsers.CountDepartments() == 1)
                                {
                                    CUsers.CurDepartment = CUsers.GetAllUserDepartments();
                                    MessageBox.Show(string.Format("Підрозділ автоматично установлений в '{0}'", CUsers.CurDepartment.ToText()), "Інфо", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                    MessageBox.Show("Не визначений підрозділ", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            else if (CUsers.CountDepartments() > 1)
                                MessageBox.Show("Підрозділ: " + curDepartment.ToText(), "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            TableRights.Init(); //инициализация прав доступа к таблицам. Вызывает AdminDisconnect(), поэтому в самом конце
                            // remark: it seems, ON_OPEN_WORKSPACE is called with Admin already disconnected
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show("Помилка ініціалізації плагіна: " + ex.Message, CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        SelectAddressControlFast2.DropCityList();

                        if (ApplSetting.IsDebug)
                            MessageBox.Show("Plugin is used in debug mode!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (ApplSetting.IsDebugCalculation)
                            MessageBox.Show("Отладочный режим расчетов запущен!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case "UCRF_LOAD_FROM_UTDB":
                    {
                        CLogs.WriteInfo(ELogsWhat.AutoAction, "Загрузка данных из UTDB начата...");
                        CLogs.WriteLogTxt("Загрузка данных из UTDB начата...");
                        int count = CGlobalDB.ReadUTDB(true, EService.ICSM);
                        CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Загрузка данных из UTDB закончена. Загружено {0} XML файлов.", count));
                        CLogs.WriteLogTxt("Общее число успешно обработанных файлов: " + count.ToString());
                        CLogs.WriteLogTxt("Загрузка данных из UTDB закончена.");
                    }
                    break;
                case "UCRF_TEST":
                    MessageBox.Show(@"Это тестовое сообщение автоматической обработки данных!");
                    break;
                case "DBLCLK_RECORD":
                    if (inParam is RecordPtr)
                    {
                        RecordPtr recordPtrParam = (RecordPtr)inParam;
                        OnDblClickRecord(recordPtrParam);
                    }
                    break;
                case "UCRF_SEND_TO_R135":
                    {
                        CLogs.WriteInfo(ELogsWhat.AutoAction, "Отправка в Р135 начата...");
                        int count = SendToR135(true);
                        CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Отправка в Р135 закончена. Отправлено {0} записей.", count));
                    }
                    break;
                case "UCRF_READ_FROM_R135":
                    {
                        CLogs.WriteInfo(ELogsWhat.AutoAction, "Чтение из Р135 начата...");
                        int count = ReadFromR135(true);
                        CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Чтение из Р135 закончена. Обработано {0} записей.", count));
                    }
                    break;
                case "UCRF_AUTO_SET_STATUS":
                    OnAutoSetStatus();
                    break;
                case "UCRF_UPDATE_WORK_COUNT":
                    {
                        CLogs.WriteInfo(ELogsWhat.AutoAction, "Установка кол-ва работ начата...");
                        int count = CArticleUpdate.UpdateWorksCount();
                        CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Установка кол-ва работ закончена. Обновлено {0} записей.", count));
                    }
                    break;
                case "URFA_RUN_AUTO_TEST":
                    if (ApplSetting.IsRunTest)
                    {
                        RunAutoTest(true);   //Запускаем автоматическое тестирование
                    }
                    break;
            }
            if(message.StartsWith("UCRF_SET_ARTICLE"))
            {
                string tableName = "";
                if(message.Contains("="))
                {
                    int posSymbol = message.IndexOf("=");
                    if((posSymbol > 0) && (posSymbol < message.Length - 1))
                        tableName = message.Substring(posSymbol + 1);
                }
                CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Установка статей начата... Таблица: '{0}'", tableName));
                int count = CArticleUpdate.UpdateArticles(true, tableName);
                CLogs.WriteInfo(ELogsWhat.AutoAction, string.Format("Установка статей закончена. Обновлено {0} записей.", count));
            }
            return false;
        }

        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            return UpdateSchema.UpgradeDatabase(s, dbCurVersion);
        }

        public void RegisterSchema(IMSchema s)
        {
            UpdateSchema.RegisterSchema(s);
            string appFolder = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string err;
            if (!OrmCs.OrmSchema.ParseSchema(appFolder, "UcrfRfaNET", "UcrfRfaNET.Schema", out err)) MessageBox.Show("Could not load 'UcrfRfaNET.Schema' :" + err);
        }

        public void RegisterBoard(IMBoard b)
        {
            // -- таблицы ICSM --
            b.RegisterQueryMenuBuilder(ICSMTbl.itblMobStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblMobStation2, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblEarthStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblMicrowa, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblTV, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblFM, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblTDAB, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblDVBT, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblGSM, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblShip, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.FxLkClink, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.USERS, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.SITES, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.Site, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.WienAnsMob, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.LICENCE, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.ChAllotments, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.FixMobNotice, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.FxLkClinkPrv, OnGetQueryMenu);

            

            // -- таблицы плагина --
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaDrvApplAppsURCM, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.xnrfa_monitor_contracts, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.LinkSites, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl._itblXnrfaExternFilter, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaPacket, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaAppl, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaSCPattern, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaPrice, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.ABONENT_OWNER, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaDrvApplURCM, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.LOGS, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.SpecCondition, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaAbonentStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaEmitterStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaEquip, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaEquipAmateur, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaNet, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaRefBaseStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.MeasureEquipment, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.AuxiliaryEquipment, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaMeasureEquipmentCertificates, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.itblXnrfaFees, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaBandAmateur, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.AllStations, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaAmateur, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XfaAnten, OnGetQueryMenu);

            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionBro, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionEs, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionFmn, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionHf, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionMob2, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionMw, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(ICSMTbl.itblPositionWim, OnGetQueryMenu);            
            b.RegisterQueryMenuBuilder(PlugTbl.XfaPosition, OnGetQueryMenu);            

            b.RegisterQueryMenuBuilder(PlugTbl.XvAbonentStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvAmateur, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvAppl, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvEmiStation, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvMsreqCert, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvPacket, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvPermAll, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvShip, OnGetQueryMenu);
            
            b.RegisterQueryMenuBuilder(PlugTbl.XvAnten, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvEquip, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvEquipAmateur, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvAbonentOwner, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvApplPay, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvApplPayUrchm, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvWorksUrchm, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvAuxEq, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvMeasureEq, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvSites, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvFilialSite, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XvAllPosition, OnGetQueryMenu);

            //b.RegisterQueryMenuBuilder(PlugTbl.XvFilEquip, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.AllDecentral, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XnChangeMsg, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.XnChangeLog, OnGetQueryMenu);
            b.RegisterQueryMenuBuilder(PlugTbl.RegistryPort, OnGetQueryMenu);
            // -- виды плагина --
            b.RegisterQueryMenuBuilder(PlugTbl.XvMonitorUsers, OnGetQueryMenu);

            b.RegisterQueryMenuBuilder("XFA_BAND_EMI", OnGetQueryMenu);
            b.RegisterQueryMenuBuilder("XFA_EMI_TO_BAND", OnGetQueryMenu);
            b.RegisterQueryMenuBuilder("WIEN_HEADREQ_MOB", OnGetQueryMenu);
            b.RegisterQueryMenuBuilder("CITIES", OnGetQueryMenu);
        }
        //=================================================
        // Запрос отображения Popup Menu
        //=================================================
        public List<IMQueryMenuNode> OnGetQueryMenu(String tableName, int nbSelMin)
        {
            List<IMQueryMenuNode> menuList = new List<IMQueryMenuNode>();
            UcrfDepartment curManagement = CUsers.GetCurDepartment();
            //=================================================
            //Таблицы манагера
            //=================================================
            switch (tableName)
            {
                case PlugTbl.itblXnrfaDrvApplAppsURCM:
                    if ((curManagement == UcrfDepartment.URCM)) {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Recalc count work..."), null,
                               OnRecalcWork, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete record..."), null,
                               DeleteRecCHAPPURCM, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
                    }
                    break;
                case "WIEN_HEADREQ_MOB":
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Generate electronic file of request (*.LIS)"), null,
                            OnEditLisFile, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Update);
                    break;
                case "CITIES":
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode("Завантажити файл...", null,
                            OnLoadFileStreet, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Update);
                    break;
                case PlugTbl.LinkSites:
                    EditPositions2(tableName, tableName, nbSelMin, ref menuList);
                    break;

                case ICSMTbl.ChAllotments:
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Update allotted channels"), null,
                            OnUpdateAllottedChannels, IMQueryMenuNode.ExecMode.EachRecord), IMTableRight.Update);
                    break;
                case ICSMTbl.USERS:
                    //if (curManagement == UcrfDepartment.URCM)
                    if ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch))
                    {
                       
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("URCM Memo to DRV by pay owner"), null,
                            x => OnSelectionOwnerForDrvFromUser(x.TableId, x.TableName,0), IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    if (nbSelMin == 1 && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                        menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Імпорт замовлення на моніторинг..."), null, OnImportMonitoring, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                    break;

                case PlugTbl.xnrfa_monitor_contracts:
                    //if (curManagement == UcrfDepartment.URCM)
                    if ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch))
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("URCM Memo to DRV by pay owner"), null,
                            x => OnSelectionOwnerForDrvFromUser(x.TableId,x.TableName), IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    break;
                case PlugTbl.XvMonitorUsers:
                    if ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch))
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode("Службова за РЧМ по фактичним вимірам...", null,
                          OnSelectionXvMonitorUsers, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    }
                    break;
                case ICSMTbl.WienAnsMob:
                    if (nbSelMin == 1)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show coordination form..."), null,
                            x =>
                            {
                                int coordinateId = IM.NullI;
                                IMRecordset rs = new IMRecordset(x.TableName, IMRecordset.Mode.ReadOnly);
                                try
                                {
                                    rs.Select("ID,Coordination_Link.ID");
                                    rs.SetWhere("ID", IMRecordset.Operation.Eq, x.TableId);
                                    rs.Open();
                                    if (!rs.IsEOF())
                                    {
                                        coordinateId = rs.GetI("Coordination_Link.ID");
                                    }
                                }
                                finally
                                {
                                    rs.Final();
                                }
                                if (coordinateId != IM.NullI)
                                {
                                    RecordPtr recCoordinate = new RecordPtr(ICSMTbl.WienCoordMob, coordinateId);
                                    recCoordinate.UserEdit();
                                }
                                return false;
                            }, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    break;
                case ICSMTbl.SITES:
                    EditPositions2(tableName, tableName, nbSelMin, ref menuList);
                    if (nbSelMin > 0)
                        AddContextMenu(ref menuList, ICSMTbl.SITES, new IMQueryMenuNode(CLocaliz.TxT("Union..."),
                                       null, OnUnionPos, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
                    break;
                case ICSMTbl.itblPositionBro:
                case ICSMTbl.itblPositionFmn:
                case ICSMTbl.itblPositionHf:
                case ICSMTbl.itblPositionMob2:
                case ICSMTbl.itblPositionMw:
                case ICSMTbl.itblPositionWim:
                    EditPositions2(tableName, tableName, nbSelMin, ref menuList);
                    if ((IM.TableRight(tableName) & IMTableRight.Update) == IMTableRight.Update)
                        menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Latinize NAMEs..."), "URCP",
                                       (context) => 
                                       {
                                           if (MessageBox.Show("This action will convert NAMEs of all selected positions to 'CITY'+'ID'.\r\nContinue?", CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                                               return false;
                                           
                                           IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadWrite);
                                           rs.Select("ID,CITY,NAME");
                                           rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines); 
                                           try
                                           {
                                               using (LisProgressBar pb = new LisProgressBar("Updating position names..."))
                                               {
                                                   int recTotal = 0;
                                                   for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                                                   {
                                                       string oldName = rs.GetS("CITY");
                                                       string newName = (oldName.Length > 0 ? (LatinPlugin.Transliteration.Translite(oldName, false) + " ") : "") + rs.GetI("ID").ToString();
                                                       pb.SetBig(rs.GetI("ID") + ": " + oldName + " => " + newName);
                                                       pb.SetSmall(++recTotal);

                                                       if (pb.UserCanceled())
                                                           return true;

                                                       rs.Edit();
                                                       rs.Put("NAME", newName);
                                                       rs.Update();
                                                   }
                                               }
                                           }
                                           finally
                                           {
                                               rs.Destroy();
                                           }
                                           return true;
                                       } 
                                       , IMQueryMenuNode.ExecMode.SelectionOfRecords));
                    break;
                case ICSMTbl.itblPositionEs:
                case ICSMTbl.Site:
                    EditPositions2(tableName, tableName, nbSelMin, ref menuList);
                    break;
                case ICSMTbl.FixMobNotice:
                    if (nbSelMin > 0)
                    {
                       menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Export to Coordinated Links"), null, OnNotificationToCoordination, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                       menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Export to table WIEN_COORD_MOB..."), null,
                                       (context) => 
                                       {
                                          
                                           IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                                           rs.Select("ID,ADM,T_OP_HH_FR,T_OP_HH_TO,T_FREQ_ASSGN,DESIG_EMISSION,NAME,T_STN_CLS,NAT_SRV,LONGITUDE,LATITUDE,ASL,T_ADM_REF_ID, T_D_INUSE,T_D_ADM_NTC,COUNTRY_ID,RADIUS");
                                           rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines); 
                                           try
                                           {
                                               using (LisProgressBar pb = new LisProgressBar("Process exporting to table WIEN_COORD_MOB..."))
                                               {
                                                   int recTotal = 0;
                                                   for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                                                   {
                                                       int ID = rs.GetI("ID");
                                                       double T_FREQ_ASSGN = rs.GetD("T_FREQ_ASSGN");
                                                       string DESIG_EMISSION = rs.GetS("DESIG_EMISSION");
                                                       string NAME = rs.GetS("NAME");
                                                       string T_STN_CLS = rs.GetS("T_STN_CLS");
                                                       string NAT_SRV = rs.GetS("NAT_SRV");
                                                       double LONGITUDE = rs.GetD("LONGITUDE");
                                                       double LATITUDE = rs.GetD("LATITUDE");
                                                       double ASL = rs.GetD("ASL");
                                                       string T_ADM_REF_ID = rs.GetS("T_ADM_REF_ID");
                                                       DateTime T_D_INUSE = rs.GetT("T_D_INUSE");
                                                       DateTime T_D_ADM_NTC = rs.GetT("T_D_ADM_NTC");
                                                       string COUNTRY_ID = rs.GetS("COUNTRY_ID");
                                                       double RADIUS = rs.GetD("RADIUS");
                                                       string ADM = rs.GetS("ADM");
                                                       int T_OP_HH_FR = rs.GetI("T_OP_HH_FR");
                                                       int T_OP_HH_TO = rs.GetI("T_OP_HH_TO");
                                                       int CHAN_OCC = 0;
                                                       if (T_OP_HH_TO == 24)
                                                       {
                                                        CHAN_OCC = 1; 
                                                       }
                                                       else
                                                       {
                                                        CHAN_OCC = 0;
                                                       }


                                                     
                                                       pb.SetSmall(++recTotal);

                                                       if (pb.UserCanceled())
                                                           return true;


                                                       IMRecordset rsWienCoordMob = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
                                                       rsWienCoordMob.Select("ID,POLAR,FREQ_TYPE,C3,CHAN_OCC,OP_HH_FR,OP_HH_TO,TX_FREQ,DESIG_EMISSION,NAME,CLASS,SERVICE,LONGITUDE,LATITUDE,ASL,P6,BIUSE_DATE,CREQUEST_DATE,COUNTRY_ID,RADIUS,POWER,ANT_TYPE,AGL,ELEVATION,GAIN,AZIMUTH,DIAGA,DIAGH,DIAGV,RADIUS");
                                                       rsWienCoordMob.Open();
                                                       rsWienCoordMob.AddNew();
                                                       rsWienCoordMob.Put("ID", IM.AllocID(ICSMTbl.WienCoordMob, 1, -1));
                                                       rsWienCoordMob.Put("TX_FREQ", T_FREQ_ASSGN);
                                                       rsWienCoordMob.Put("DESIG_EMISSION", DESIG_EMISSION);
                                                       rsWienCoordMob.Put("NAME", NAME);
                                                       rsWienCoordMob.Put("CLASS", T_STN_CLS);
                                                       rsWienCoordMob.Put("SERVICE", NAT_SRV);
                                                       rsWienCoordMob.Put("LONGITUDE", LONGITUDE);
                                                       rsWienCoordMob.Put("LATITUDE", LATITUDE);
                                                       rsWienCoordMob.Put("ASL", ASL);
                                                       rsWienCoordMob.Put("P6", T_ADM_REF_ID.Cut(6));
                                                       rsWienCoordMob.Put("BIUSE_DATE", T_D_INUSE);
                                                       rsWienCoordMob.Put("CREQUEST_DATE", T_D_ADM_NTC);
                                                       rsWienCoordMob.Put("COUNTRY_ID", COUNTRY_ID);
                                                       rsWienCoordMob.Put("RADIUS", RADIUS);
                                                       rsWienCoordMob.Put("C3", ADM);
                                                       rsWienCoordMob.Put("CHAN_OCC", CHAN_OCC.ToString());
                                                       rsWienCoordMob.Put("OP_HH_FR", T_OP_HH_FR);
                                                       rsWienCoordMob.Put("OP_HH_TO", T_OP_HH_TO);
                                                       rsWienCoordMob.Put("FREQ_TYPE", "2");
                                                       rsWienCoordMob.Put("POLAR", "V");




                                                       IMRecordset rs_antenna = new IMRecordset(ICSMTbl.T1nAntennas, IMRecordset.Mode.ReadOnly);
                                                       rs_antenna.Select("ID,NOT_ID, T_PWR_DBW, T_PWR_EIV, T_HGT_AGL,T_ELEV,T_GAIN_MAX, T_AZM_MAX_E, T_ANT_DIR, T_BMWDTH");
                                                       rs_antenna.SetWhere("NOT_ID", IMRecordset.Operation.Eq, ID);
                                                       for (rs_antenna.Open(); !rs_antenna.IsEOF(); rs_antenna.MoveNext())
                                                       {
                                                           int ID_ANT = rs_antenna.GetI("ID");

                                                           double T_PWR_DBW = rs_antenna.GetD("T_PWR_DBW");
                                                           string T_PWR_EIV = rs_antenna.GetS("T_PWR_EIV");
                                                           double T_HGT_AGL = rs_antenna.GetD("T_HGT_AGL");
                                                           double T_ELEV = rs_antenna.GetD("T_ELEV");
                                                           double T_GAIN_MAX = rs_antenna.GetD("T_GAIN_MAX");
                                                           double T_AZM_MAX_E = rs_antenna.GetD("T_AZM_MAX_E");
                                                           string T_ANT_DIR = rs_antenna.GetS("T_ANT_DIR");
                                                           double T_BMWDTH = rs_antenna.GetD("T_BMWDTH");
                                                           

                                                           rsWienCoordMob.Put("POWER", T_PWR_DBW);
                                                           rsWienCoordMob.Put("ANT_TYPE", T_PWR_EIV);
                                                           rsWienCoordMob.Put("AGL", T_HGT_AGL);
                                                           rsWienCoordMob.Put("ELEVATION", T_ELEV);
                                                           rsWienCoordMob.Put("GAIN", T_GAIN_MAX);
                                                           rsWienCoordMob.Put("AZIMUTH", T_AZM_MAX_E);
                                                           rsWienCoordMob.Put("DIAGH", T_ANT_DIR);
                                                           if (T_ANT_DIR == "ND")
                                                           {
                                                               rsWienCoordMob.Put("DIAGA", "HV");
                                                               rsWienCoordMob.Put("DIAGH", "WIEN 000ND00");
                                                               rsWienCoordMob.Put("DIAGV", "WIEN 000ND00");
                                                           }
                                                           else if (T_ANT_DIR == "D")
                                                           {
                                                               rsWienCoordMob.Put("DIAGA", "HV");
                                                               T_BMWDTH = (int) Math.Ceiling(Math.Round(T_BMWDTH / 2, 0));
                                                               rsWienCoordMob.Put("DIAGH", string.Format("WIEN {0}ЕА10",((int)(T_BMWDTH)).ToString().Length==0 ? "000" : ((int)(T_BMWDTH)).ToString().Length==1? "00"+((int)(T_BMWDTH)).ToString() :((int)(T_BMWDTH)).ToString().Length==2 ? "0"+((int)(T_BMWDTH)).ToString() : ((int)(T_BMWDTH)).ToString().Length==3 ? ((int)(T_BMWDTH)).ToString() : ((int)(T_BMWDTH)).ToString()));
                                                               rsWienCoordMob.Put("DIAGV", string.Format("WIEN {0}ЕА10", ((int)(T_BMWDTH)).ToString().Length == 0 ? "000" : ((int)(T_BMWDTH)).ToString().Length == 1 ? "00" + ((int)(T_BMWDTH)).ToString() : ((int)(T_BMWDTH)).ToString().Length == 2 ? "0" + ((int)(T_BMWDTH)).ToString() : ((int)(T_BMWDTH)).ToString().Length == 3 ? ((int)(T_BMWDTH)).ToString() : ((int)(T_BMWDTH)).ToString()));
                                                           }




                                                           IMRecordset rs_antenna_radius = new IMRecordset(ICSMTbl.T1nRxTxStations, IMRecordset.Mode.ReadOnly);
                                                          rs_antenna_radius.Select("ANT_ID,ID, RADIUS");
                                                          rs_antenna_radius.SetWhere("ANT_ID", IMRecordset.Operation.Eq, ID_ANT);
                                                          for (rs_antenna_radius.Open(); !rs_antenna_radius.IsEOF(); rs_antenna_radius.MoveNext())
                                                          {
                                                              int RAD = rs_antenna_radius.GetI("RADIUS");
                                                              rsWienCoordMob.Put("RADIUS", RAD);
                                                              break;
                                                          }
                                                          


                                                           break;
                                                       }

                                                       rsWienCoordMob.Update();

                                                   }
                                               }
                                           }
                                           finally
                                           {
                                               rs.Destroy();
                                           }
                                           return true;
                                       } 
                                       , IMQueryMenuNode.ExecMode.SelectionOfRecords));
                     }
                    break;
            }
            //=================================================
            //Таблицы плагина
            //=================================================

            if (tableName == "XFA_BAND_EMI")
            {
                if (nbSelMin > 0)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Delete records"), null, OnDeleteBandFreq, IMQueryMenuNode.ExecMode.FirstRecord));
                if (nbSelMin > 0)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null, OnBandFreq, IMQueryMenuNode.ExecMode.FirstRecord));

                menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnBandFreqNew, IMQueryMenuNode.ExecMode.Table));
                
            }

            if (tableName == PlugTbl.APPL)
            // additional use, PlugTbl.APPL is used in swich below being combined with other case constant
            {
                if (nbSelMin > 0 && curManagement == UcrfDepartment.URCP)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Оновити список ліцензій РЧП..."), null, OnUpdateLicenceList, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                //AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Update licence list..."), null, OnUpdateLicenceList, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Update);
            }

            switch (tableName)
            {
                case PlugTbl.XnChangeLog:
                    if (nbSelMin == 1)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show log..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show appl..."), null,
                            x =>
                            {
                                int applId = IM.NullI;
                                IMRecordset rs = new IMRecordset(x.TableName, IMRecordset.Mode.ReadOnly);
                                try
                                {
                                    rs.Select("ID,APPL_ID");
                                    rs.SetWhere("ID", IMRecordset.Operation.Eq, x.TableId);
                                    rs.Open();
                                    if (!rs.IsEOF())
                                    {
                                        applId = rs.GetI("APPL_ID");
                                    }
                                }
                                finally
                                {
                                    rs.Final();
                                }
                                if (applId != IM.NullI)
                                {
                                    ShowApplForm(applId, true);
                                }
                                return false;
                            }, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    break;
                case PlugTbl.XnChangeMsg:
                    if (nbSelMin == 1)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show log..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show appl..."), null,
                            x =>
                                {
                                    int applId = IM.NullI;
                                    IMRecordset rs = new IMRecordset(x.TableName, IMRecordset.Mode.ReadOnly);
                                    try
                                    {
                                        rs.Select("ID,OBJ_ID");
                                        rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.APPL);
                                        rs.SetWhere("ID", IMRecordset.Operation.Eq, x.TableId);
                                        rs.Open();
                                        if(!rs.IsEOF())
                                        {
                                            applId = rs.GetI("OBJ_ID");
                                        }
                                    }
                                    finally
                                    {
                                        rs.Final();
                                    }
                                    if(applId != IM.NullI)
                                    {
                                        ShowApplForm(applId, true);
                                    }
                                    return false;
                                },
                            IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    break;
                case PlugTbl.RegistryPort:
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("New port of registry..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                    if (nbSelMin == 1)
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit port of registry..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    if (nbSelMin > 0)
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete port of registry"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
                    break;
                case PlugTbl.XfaNet:
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("New record..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                    if (nbSelMin == 1)
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit record..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    if (nbSelMin > 0)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete record(s)..."), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
                        if (curManagement == UcrfDepartment.URCM)
                            AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Article confirmation..."), null, OnConfirmNetArticleContextMenu, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    }
                    break;
                  case PlugTbl.xnrfa_monitor_contracts:
                    if (nbSelMin == 1 && ((curManagement == UcrfDepartment.URCM)  || (curManagement == UcrfDepartment.Branch)))
                        menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Імпорт замовлення на моніторинг..."), null, OnImportMonitoring, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                    break;
                case PlugTbl.XvMonitorUsers:
                    if (nbSelMin == 1 && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                        menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Імпорт замовлення на моніторинг..."), null, OnImportMonitoring, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                    //if (curManagement == UcrfDepartment.URCM)
                    if ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch))
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("URCM Memo to DRV by pay owner"), null,
                            x =>{
                                    int ownerId = IM.NullI;
                                    using(LisRecordSet rs = new LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                                    {
                                        rs.Select("ID,OWNER_ID");
                                        rs.SetWhere("ID", IMRecordset.Operation.Eq, x.TableId);
                                        rs.Open();
                                        if(!rs.IsEOF())
                                        {
                                            ownerId = rs.GetI("OWNER_ID");
                                        }
                                    }
                                    return OnSelectionOwnerForDrvFromUser(ownerId, PlugTbl.XvMonitorUsers, x.TableId);
                                }, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Find stations without articles"), null,
                            x =>{
                                    int ownerId = IM.NullI;
                                    using(LisRecordSet rs = new LisRecordSet(tableName, IMRecordset.Mode.ReadOnly))
                                    {
                                        rs.Select("ID,OWNER_ID");
                                        rs.SetWhere("ID", IMRecordset.Operation.Eq, x.TableId);
                                        rs.Open();
                                        if(!rs.IsEOF())
                                        {
                                            ownerId = rs.GetI("OWNER_ID");
                                        }
                                    }
                                    ShowStationWithoutArticle(ownerId, x.TableId);
                                    return false;
                                }, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    break;
                case PlugTbl.APPL:
                case PlugTbl.XvAppl:
                        if (nbSelMin == 1)
                            AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Open appl..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);

                        if (nbSelMin > 0)
                        {
                            if (curManagement == UcrfDepartment.URCP)
                            {
                                List<IMQueryMenuNode> sublst = new List<IMQueryMenuNode>();
                                sublst.Add(new IMQueryMenuNode("Висновок", null, OnMonitoringConcXml, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                                sublst.Add(new IMQueryMenuNode("Дозвіл", null, OnMonitoringDozvXml, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                                menuList.Add(new IMQueryMenuNode("Відправити в РС-135У", sublst));
                            }
                            AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Update frequencies..."), null, OnUpdateFreq, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Update);
                            AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Change pay owner..."), null, OnChangePayOwner, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Update);
                            if (Article.ArticleChangeForm.CanUpdateArticle())
                                AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Article confirmation..."), null, OnConfirmArticleFromAllStationContextMenu, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                        }
                        if ((nbSelMin > 0) && (ApplSetting.IsDebug))
                        {
                            menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Test memory"), null, OnTestMemory, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                            menuList.Add(new IMQueryMenuNode
                                (CLocaliz.TxT("Set article"),
                                 null,
                                 (x) =>
                                 {
                                     ArticleLib.ArticleUpdate.UpdateArticle(x.TableId);
                                     return true;
                                 },
                                 IMQueryMenuNode.ExecMode.FirstRecord));
                            menuList.Add(new IMQueryMenuNode
                                (CLocaliz.TxT("Set count work"),
                                 null,
                                 (x) =>
                                 {
                                     ArticleLib.CountWork.SetWorkCount(x.TableId);
                                     return true;
                                 },
                                 IMQueryMenuNode.ExecMode.FirstRecord));
                        }
                    break;
                case PlugTbl.itblXnrfaDrvApplURCM:
                case PlugTbl.XvApplPayUrchm:
                    if (((CUsers.GetCurDepartment() == UcrfDepartment.URCM) || (CUsers.GetCurDepartment() == UcrfDepartment.Branch)) && (nbSelMin == 1))
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Open DRV memo..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    break;
                case PlugTbl.itblXnrfaDrvAppl:
                    if ((CUsers.GetCurDepartment() == UcrfDepartment.URCP) && (nbSelMin == 1))
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Open DRV memo..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    break;
            }

            if (tableName.Equals(PlugTbl.AllStations))
            {
               //string status = conte.Object["STATUS"];

                if (nbSelMin == 1 && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Імпорт замовлення на моніторинг..."), null, OnImportMonitoring, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if (nbSelMin > 0 && curManagement == UcrfDepartment.URCP)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Оновити список ліцензій РЧП..."), null, OnUpdateLicenceList, IMQueryMenuNode.ExecMode.SelectionOfRecords));
  
                if ((nbSelMin > 0) && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("ТРК РЕЗ  РРЗ (Запланувати)..."), null, OnTRKREZ_Plan, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if ((nbSelMin > 0) && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("ТРК РЕЗ  РРЗ (Внести результати)..."), null, OnTRKREZ_Rez, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if ((nbSelMin > 0) && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("ТРК РЕЗ  РРЗ (Завантажити файл)..."), null, OnLoadFile, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if ((nbSelMin > 0) && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("ТРК РЕЗ  РРЗ (Врахувати)..."), null, OnTRKREZ_Plan_Contain, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Find stations..."), null, OnFindStations, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Show appl..."), null, OnEditStationApplAllStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    if (curManagement == UcrfDepartment.URCM)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("URCM Memo to DRV by pay owner"), null, OnSelectionOwnerPayForDrv, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Find stations without articles"), null, OnFindStationsWithoutArticle, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Набори для експорту реєстра РЧП"), null, OnExpDrv, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);

                    if (ApplSetting.IsDebug)
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Send to UTDB"), null, OnCreateXmlUtdb, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    if (Article.ArticleChangeForm.CanUpdateArticle())
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Article confirmation..."), null, OnConfirmArticleFromAllStationContextMenu, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Налаштування експорту реєстру РЧП"), null, OnRepairDrv, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);

                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x=>
                                       {
                                           OnMapShowAllStation(x, false);
                                           return false;
                                       },
                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                       {
                                           OnMapShowAllStation(x, true);
                                           return false;
                                       },
                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);

                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Export to file"), null, OnExportToFileAllStation,
                                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Change pay owner..."), null, OnChangePayOwner,
                                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Update);
                }
            }
            else if (tableName.Equals(PlugTbl.AllDecentral))
            {
                if (nbSelMin == 1 && ((curManagement == UcrfDepartment.URCM) || (curManagement == UcrfDepartment.Branch)))
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Імпорт замовлення на моніторинг..."), null, OnImportMonitoring, IMQueryMenuNode.ExecMode.SelectionOfRecords));

                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Open appl..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    if (curManagement == UcrfDepartment.URCM)
                    {
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("URCM Memo to DRV by pay owner"), null, OnSelectionOwnerPayForDrv, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Find stations without articles"), null, OnFindStationsWithoutArticle, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    }
                    //AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Набори для експорту реєстра РЧП"), null, OnExpDrv, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);

                    //if (ApplSetting.IsDebug)
                    //    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Send to UTDB"), null, OnCreateXmlUtdb, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    if (Article.ArticleChangeForm.CanUpdateArticle())
                        AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Article confirmation..."), null, OnConfirmArticleFromAllStationContextMenu, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Change pay owner..."), null, OnChangePayOwner,
                                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Update);
                }
                //if (nbSelMin > 0)
                //{
                //    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Налаштування експорту реєстру РЧП"), null, OnRepairDrv, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);

                //    AddContextMenu(ref menuList, tableName,
                //                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null, OnMapShowAllStation,
                //                                       IMQueryMenuNode.ExecMode.EachRecord), IMTableRight.Select);

                //    AddContextMenu(ref menuList, tableName,
                //                   new IMQueryMenuNode(CLocaliz.TxT("Export to file"), null, OnExportToFileAllStation,
                //                                       IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                //}
            }

            if (tableName.Equals(ICSMTbl.itblShip))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblShip, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    //AddContextMenu(ref menuList, ICSMTbl.itblShip, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
            }
            if (tableName.Equals(PlugTbl.XvShip))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, PlugTbl.Ship, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStationView, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    //AddContextMenu(ref menuList, ICSMTbl.itblShip, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
            }

            if (tableName.Equals(ICSMTbl.itblMobStation2))//tableName == "MOB_STATION2")
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation2, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation2, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation2, new IMQueryMenuNode(CLocaliz.TxT("Додати до ОУ..."), null, ProcessBagStationAnother, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation2, new IMQueryMenuNode(CLocaliz.TxT("Сформувати ОУ..."), null, OnGenerateBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation2, new IMQueryMenuNode(CLocaliz.TxT("Очистити буфер для ОУ..."), null, OnClearBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblEarthStation))// == "EARTH_STATION")
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblEarthStation, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblEarthStation, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblEarthStation, new IMQueryMenuNode(CLocaliz.TxT("Додати до ОУ..."), null, ProcessBagStationAnother, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblEarthStation, new IMQueryMenuNode(CLocaliz.TxT("Сформувати ОУ..."), null, OnGenerateBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblEarthStation, new IMQueryMenuNode(CLocaliz.TxT("Очистити буфер для ОУ..."), null, OnClearBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblMicrowa))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Додати до ОУ (Жертви)..."), null, ProcessBagStationMicrowaGertva, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Додати до ОУ (Завади)..."), null, ProcessBagStationMicrowaZavada, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Сформувати ОУ..."), null, OnGenerateBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMicrowa, new IMQueryMenuNode(CLocaliz.TxT("Очистити буфер для ОУ..."), null, OnClearBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.FxLkClink))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, OnEMSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
            }

            if (tableName.Equals(ICSMTbl.FxLkClinkPrv))
            {
                if (nbSelMin == 1)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS..."), null, 
                        context => 
                        {
                            IMRecordset rs = IMRecordset.ForRead(new RecordPtr(context.TableName, context.TableId), "STA_ID");
                            context.TableId = rs.GetI("STA_ID");
                            context.TableName = ICSMTbl.FxLkClink;
                            rs.Destroy();
                            return context.TableId == IM.NullI ? false : OnEMSProcess(context);
                        }
                    , IMQueryMenuNode.ExecMode.FirstRecord));
            }

            if (tableName.Equals(ICSMTbl.itblMobStation))// "MOB_STATION"))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS"), null, OnVRRProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    //-----
                    {
                        List<IMQueryMenuNode> sublst = new List<IMQueryMenuNode>();
                        AddContextMenu(ref sublst, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Generate station in coordination table"), null, OnHcmMobStationProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                        menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("HCM"), sublst));
                    }
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Додати до ОУ..."), null, ProcessBagStationAnother, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Сформувати ОУ..."), null, OnGenerateBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblMobStation, new IMQueryMenuNode(CLocaliz.TxT("Очистити буфер для ОУ..."), null, OnClearBagStation, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblTV))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblTV, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblTV, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS"), null, OnVRSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblFM))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblFM, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblFM, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS"), null, OnVRSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblDVBT))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblDVBT, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblDVBT, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS"), null, OnVRSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblTDAB))
            {
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, ICSMTbl.itblTDAB, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    AddContextMenu(ref menuList, ICSMTbl.itblTDAB, new IMQueryMenuNode(CLocaliz.TxT("Calculate EMS"), null, OnVRSProcess, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                {
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map"), null,
                                       x =>
                                           {
                                               OnMapShow(x, false);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                    AddContextMenu(ref menuList, tableName,
                                   new IMQueryMenuNode(CLocaliz.TxT("Show on the map with contour"), null,
                                       x =>
                                           {
                                               OnMapShow(x, true);
                                               return false;
                                           },
                                           IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Select);
                }
            }
            if (tableName.Equals(ICSMTbl.itblGSM))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, ICSMTbl.itblGSM, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            else if (tableName.Equals(PlugTbl.XfaAbonentStation))
            {              
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaAbonentStation, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            else if (tableName.Equals(PlugTbl.XvAbonentStation))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaAbonentStation, new IMQueryMenuNode(CLocaliz.TxT("Edit station..."), null, OnOpenApplFromStationView, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }

            if (tableName.Equals(PlugTbl._itblXnrfaExternFilter))// "XNRFA_EXTERN_FILTERS"))
            {
                AddContextMenu(ref menuList, PlugTbl._itblXnrfaExternFilter, new IMQueryMenuNode(CLocaliz.TxT("New external filter..."), null, OnAdditionFiltersNew, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl._itblXnrfaExternFilter, new IMQueryMenuNode(CLocaliz.TxT("Edit external filter..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl._itblXnrfaExternFilter, new IMQueryMenuNode(CLocaliz.TxT("Delete external filter"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XfaEmitterStation))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaEmitterStation, new IMQueryMenuNode(CLocaliz.TxT("Edit ..."), null, OnOpenApplFromStation, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            if (tableName.Equals(PlugTbl.XvEmiStation))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaEmitterStation, new IMQueryMenuNode(CLocaliz.TxT("Edit ..."), null, OnOpenApplFromStationView, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            if (tableName.Equals(PlugTbl.itblXnrfaPacket))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaPacket, new IMQueryMenuNode(CLocaliz.TxT("New packet..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaPacket, new IMQueryMenuNode(CLocaliz.TxT("Edit packet..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaPacket, new IMQueryMenuNode(CLocaliz.TxT("Delete packet"), null, OnDeletePacket, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XvPacket))
            {
                AddContextMenu(ref menuList, PlugTbl.XvPacket, new IMQueryMenuNode(CLocaliz.TxT("New packet..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XvPacket, new IMQueryMenuNode(CLocaliz.TxT("Edit packet..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.XvPacket, new IMQueryMenuNode(CLocaliz.TxT("Delete packet"), null, OnDeletePacket, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.itblXnrfaSCPattern))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaSCPattern, new IMQueryMenuNode(CLocaliz.TxT("Add pattern"), null, OnAddEditPattern, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaSCPattern, new IMQueryMenuNode(CLocaliz.TxT("Edit pattern"), null, OnAddEditPattern, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaSCPattern, new IMQueryMenuNode(CLocaliz.TxT("Delete pattern"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }

            if (tableName.Equals(ICSMTbl.LICENCE))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, ICSMTbl.LICENCE, new IMQueryMenuNode(CLocaliz.TxT("Full duplicate record"), null, OnDublicateRecordLicense, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Insert);
            }

            if (tableName.Equals(PlugTbl.itblXnrfaPrice))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaPrice, new IMQueryMenuNode(CLocaliz.TxT("Add price..."), null, OnAddEditPrice, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                {
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaPrice,
                                   new IMQueryMenuNode(CLocaliz.TxT("Edit price..."), null, OnAddEditPrice,
                                                       IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                    if (ApplSetting.IsDebug)
                        AddContextMenu(ref menuList, tableName,
                                       new IMQueryMenuNode(CLocaliz.TxT("Send to UTDB"), null, OnCreateXmlUtdb,
                                                           IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaPrice, new IMQueryMenuNode(CLocaliz.TxT("Delete price"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.ABONENT_OWNER))
            {
                AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Add owner..."), null, OnAddEditOwner, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Edit owner..."), null, OnAddEditOwner, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Delete owner"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XvAbonentOwner))
            {
                AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Add owner..."), null, OnAddEditOwner, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Edit owner..."), null, OnAddEditOwner, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.ABONENT_OWNER, new IMQueryMenuNode(CLocaliz.TxT("Delete owner"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.SpecCondition))
            {
                AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Add new sql condition..."), null, OnAddEditSpecConditionR135, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit sql condition..."), null, OnAddEditSpecConditionR135, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete sql condition"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.LOGS))
            {
                if (nbSelMin == 1)
                    menuList.Add(new IMQueryMenuNode(CLocaliz.TxT("Show..."), null, OnShowLog, IMQueryMenuNode.ExecMode.FirstRecord));
            }
            if (tableName.Equals(PlugTbl.XfaEquip))
            {
                AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnNewEditXfaEquip, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if(nbSelMin == 1)
                {
                   AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null,
                      OnNewEditXfaEquip, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                   AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("Copy record"), null,
                      OnCopyEditXfaEquip, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                }
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("Delete records"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XvEquip))
            {
                AddContextMenu(ref menuList, PlugTbl.XvEquip, new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnNewEditXfaEquip, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XvEquip, new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null, OnNewEditXfaEquip, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            if (tableName.Equals(PlugTbl.XfaEquipAmateur))
            {
                AddContextMenu(ref menuList, PlugTbl.XfaEquipAmateur, new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaEquipAmateur, new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("Delete records"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XvEquipAmateur))
            {
                AddContextMenu(ref menuList, PlugTbl.XvEquipAmateur, new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XvEquipAmateur, new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            if (tableName.Equals(PlugTbl.XfaAnten))
            {
                AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("New record"), null, OnNewEditXfaAnten, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit record"), null, OnNewEditXfaAnten, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.XfaEquip, new IMQueryMenuNode(CLocaliz.TxT("Delete records"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XfaRefBaseStation))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete base station from net..."), null, OnDeleteBaseStationFromNet, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Delete);
            }

            if (tableName.Equals(PlugTbl.MeasureEquipment) || tableName.Equals(PlugTbl.XvMeasureEq))
            {
                AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Add measure equipment"), null, OnEditMeasureEquipment, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit measure equipment"), null, OnEditMeasureEquipment, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete measure equipment"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }

            EditPositions2(PlugTbl.XfaPosition, tableName, nbSelMin, ref menuList);


            if (tableName.Equals(PlugTbl.AuxiliaryEquipment) || tableName.Equals(PlugTbl.XvAuxEq))
            {
                AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Add auxiliary equipment"), null, OnEditAuxiliaryEquipment, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Edit auxiliary equipment"), null, OnEditAuxiliaryEquipment, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, tableName, new IMQueryMenuNode(CLocaliz.TxT("Delete auxiliary equipment"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }

            if (tableName.Equals(PlugTbl.itblXnrfaMeasureEquipmentCertificates))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Add measure equipment certificate"), null, OnEditMeasureEquipmentCertificate, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Edit measure equipment certificate"), null, OnEditMeasureEquipmentCertificate, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Delete measure equipment certificate"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
            if (tableName.Equals(PlugTbl.XvMsreqCert))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Add measure equipment certificate"), null, OnEditMeasureEquipmentCertificate, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Edit measure equipment certificate"), null, OnEditMeasureEquipmentCertificate, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaMeasureEquipmentCertificates, new IMQueryMenuNode(CLocaliz.TxT("Delete measure equipment certificate"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }


            if (tableName.Equals(PlugTbl.XfaBandAmateur))
            {
                AddContextMenu(ref menuList, PlugTbl.XfaBandAmateur, new IMQueryMenuNode(CLocaliz.TxT("Add amateur band"), null, OnEditAmateurBand, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaBandAmateur, new IMQueryMenuNode(CLocaliz.TxT("Edit amateur band"), null, OnEditAmateurBand, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.XfaBandAmateur, new IMQueryMenuNode(CLocaliz.TxT("Delete amateur band"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }

            if (tableName.Equals(PlugTbl.XfaAmateur))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaAmateur, new IMQueryMenuNode(CLocaliz.TxT("Edit amateurs"), null, OnEditAmateur, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }
            if (tableName.Equals(PlugTbl.XvAmateur))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XfaAmateur, new IMQueryMenuNode(CLocaliz.TxT("Edit amateurs"), null, OnEditAmateurView, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }

            if (tableName.Equals(PlugTbl.XvFilialSite))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XvFilialSite, new IMQueryMenuNode(CLocaliz.TxT("Open..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }

            if (tableName.Equals(PlugTbl.XvFilEquip))
            {
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.XvFilEquip, new IMQueryMenuNode(CLocaliz.TxT("Open..."), null, OnNewEditContextMenu, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
            }

            if (tableName.Equals(PlugTbl.itblXnrfaFees))
            {
                AddContextMenu(ref menuList, PlugTbl.itblXnrfaFees, new IMQueryMenuNode(CLocaliz.TxT("Add fees"), null, OnEditFees, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaFees, new IMQueryMenuNode(CLocaliz.TxT("Edit fees"), null, OnEditFees, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if (nbSelMin > 0)
                    AddContextMenu(ref menuList, PlugTbl.itblXnrfaFees, new IMQueryMenuNode(CLocaliz.TxT("Delete fees"), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }

           

            return menuList;
        }
        /// <summary>
        /// Подтверждение статьи сети
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnConfirmNetArticleContextMenu(IMQueryMenuNode.Context context)
        {
            const string mess = "Ви дійсно бажаєте підтвердити статтю для вибраних мереж?";
            if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return false;

            using (LisProgressBar pBar = new LisProgressBar("Підтвердження статей..."))
            {
                pBar.SetProgress(0, 100);
                using (LisRecordSet rsAppl = new LisRecordSet(context.TableName, IMRecordset.Mode.ReadOnly))
                {
                    rsAppl.Select("ID");
                    rsAppl.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                    {
                        int curId = rsAppl.GetI("ID");
                        Net.NetObject net = new Net.NetObject();
                        net.Load(curId, false);
                        if (ArticleLib.ArticleFunc.GetStatusForCodePriceID(net.ArticleId) == false)
                        {
                            string messError = string.Format(CLocaliz.TxT("The indicated fare is not available, select another fare!"), Environment.NewLine);
                            if (MessageBox.Show(messError, "", MessageBoxButtons.OK, MessageBoxIcon.Warning) != DialogResult.OK)
                                return false;
                        }
                        if (net.CanConfirmArticle() == false)
                        {
                            string messError = string.Format("Стаття не може бути підтверджена для мережі ID:{0}{1}Продовжити?", curId, Environment.NewLine);
                            if (MessageBox.Show(messError, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                                return false;
                        }
                        else
                        {
                            net.SetConfirmArticle();
                            net.Save();
                        }
                        pBar.Increment(true);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Подтверждение статьи для выбранных заявок
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnConfirmArticleFromAllStationContextMenu(IMQueryMenuNode.Context context)
        {
            string fieldName;
            switch (context.TableName)
            {
                case PlugTbl.AllStations:
                    fieldName = "APPL_ID";
                    break;
                case PlugTbl.AllDecentral:
                case PlugTbl.APPL:
                    fieldName = "ID";
                    break;
                default:
                    fieldName = "";
                    break;
            }
            if (string.IsNullOrEmpty(fieldName))
                return false;

            const string mess = "Ви дійсно бажаєте підтвердити статтю для вибраних заявок?";
            if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return false;

            using (LisProgressBar pBar = new LisProgressBar("Підтвердження статей..."))
            {
                pBar.SetProgress(0, 100);
                using(LisRecordSet rsAppl = new LisRecordSet(context.TableName, IMRecordset.Mode.ReadOnly))
                {
                    rsAppl.Select(fieldName);
                    rsAppl.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                    {
                        int curId = rsAppl.GetI(fieldName);
                        Article.ApplArticles article = new Article.ApplArticles();
                        article.Load(curId);


                        if (ArticleLib.ArticleFunc.GetStatusForCodePriceID(article.ArticleId1) == false)
                        {
                            string messError = string.Format(CLocaliz.TxT("The indicated fare is not available, select another fare!"), curId, Environment.NewLine);
                            MessageBox.Show(messError, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }

                        if (article.CanConfirmArticle() == false)
                        {
                            string messError = string.Format("Стаття не може бути підтверджена для заявки ID:{0}{1}Продовжити?", curId, Environment.NewLine);
                            if (MessageBox.Show(messError, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                                return false;
                        }
                        else
                        {
                            article.SetConfirmArticle();
                            //article.Save(curId);
                            article.Save_SQL(curId);
                        }
                        pBar.Increment(true);
                    }
                }
            }
            return true;
        }

        private bool OnDublicateRecordLicense(IMQueryMenuNode.Context context)
        {
            if (MessageBox.Show("Ви впевнені в дублюванні запису?", "Попередження", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //Дублювати запис в ліцензії
                RecordPtr rPtr = new RecordPtr(context.TableName, context.TableId);                                
                IMRecordset r;
                RecordPtrExtension.CloneRecord(out r, rPtr, "", true);
                //Визначити новий ID для CH_ALLOTMENTS                
                int idCh = r.GetI("ID");
                r.Update();
                r.Final();
                List<int> idChList= new List<int>();
                //Визначити всі ID для CH_ALLOTMENTS
                IMRecordset rCh= new IMRecordset(ICSMTbl.ChAllotments,IMRecordset.Mode.ReadOnly);
                rCh.Select("ID,LIC_ID");
                rCh.SetWhere("LIC_ID", IMRecordset.Operation.Eq, rPtr.Id);
                try
                {
                    for (rCh.Open(); !rCh.IsEOF(); rCh.MoveNext())
                        idChList.Add(rCh.GetI("ID"));
                }
                finally 
                {
                    rCh.Final();
                }   
                //Продублювати всі CH_ALLOT
                for (int i = 0; i < idChList.Count; i++)
                {
                    IMRecordset rAllot;
                    RecordPtrExtension.CloneRecord(out rAllot, new RecordPtr(ICSMTbl.ChAllotments, idChList[i]), "", true);
                    rAllot.Put("LIC_ID", idCh);
                    rAllot.Update();
                    rAllot.Final();  
                }
            }                     
            return true;
        }

        private void EditPositions(string checkTable, string tableName, int nbSelMin,ref List<IMQueryMenuNode> menuList)
        {
            if (tableName.Equals(checkTable))
            {
                AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Add position..."), null, OnEditPosAdmSite, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Edit position..."), null, OnEditPosAdmSite, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if ((nbSelMin > 0) && (tableName == PlugTbl.XfaPosition))
                    AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Delete position..."), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
        }

        private void EditPositions2(string checkTable, string tableName, int nbSelMin, ref List<IMQueryMenuNode> menuList)
        {
            if (tableName.Equals(checkTable))
            {
                AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Add position..."), null, OnEditPosAdmSite2, IMQueryMenuNode.ExecMode.Table), IMTableRight.Insert);
                if (nbSelMin == 1)
                    AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(
                        (IM.TableRight(checkTable) & IMTableRight.Update) == IMTableRight.Update ? CLocaliz.TxT("Edit position...") : CLocaliz.TxT("Browse position..."), 
                        null, OnEditPosAdmSite2, IMQueryMenuNode.ExecMode.FirstRecord), IMTableRight.Select);
                if ((nbSelMin > 0) && (tableName == PlugTbl.XfaPosition))
                    AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Delete position..."), null, OnDeleteAllRecord, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
                if ((nbSelMin > 0) && ((tableName == ICSMTbl.Site) || (tableName == ICSMTbl.SITES) || (tableName == ICSMTbl.itblPositionEs) || (tableName == ICSMTbl.itblPositionBro) || (tableName == ICSMTbl.itblPositionFmn) || (tableName == ICSMTbl.itblPositionHf) || (tableName == ICSMTbl.itblPositionMob2) || (tableName == ICSMTbl.itblPositionMw) || (tableName == ICSMTbl.itblPositionWim) || (tableName == PlugTbl.XfaPosition)))
                    AddContextMenu(ref menuList, checkTable, new IMQueryMenuNode(CLocaliz.TxT("Update distance border..."), null, OnUpdateDistance, IMQueryMenuNode.ExecMode.SelectionOfRecords), IMTableRight.Delete);
            }
        }

        /// <summary>
        /// Об'єднання записів адмін сайтів
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnUnionPos(IMQueryMenuNode.Context context)
        {
            List<int> idList= new List<int>();
            IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
            try
            {
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    idList.Add(r.GetI("ID"));
            }
            finally
            {
                r.Final();
            }

            bool isRepeat;
            RecordPtr RecAnt;
            do
            {
                isRepeat = false;   
                RecAnt = RecordPtr.UserSearch(CLocaliz.TxT("Seach of the Site"), context.TableName, "");
                if (!((RecAnt.Id > 0) && (RecAnt.Id < IM.NullI)))
                    return false;
                if (idList.Contains(RecAnt.Id))
                {
                    isRepeat = true;
                    MessageBox.Show(CLocaliz.TxT("Chosen ID contains in selected list!"));
                }
            } while (isRepeat);

            DialogResult changeFields = MessageBox.Show("Ви хочете змінити значення прив'язаних полів сайту при об'єднанні?",
                                                 "Заміна полів сайту",
                                                 MessageBoxButtons.YesNoCancel);
            if (changeFields == DialogResult.Cancel)
                return false;
            foreach (int id in idList)
            {                                
                RecordPtr recContext = new RecordPtr(context.TableName, id);
                UnionSites(RecAnt, recContext, changeFields==DialogResult.Yes);
            }
                   
            return true;
        }

        /// <summary>
        /// Об'єднання комірок
        /// </summary>
        /// <param name="context">запис, з яким зливається інший(запис остається)</param>
        /// <param name="recAnt">запис, який зливається з context(запис видаляється)</param>        
        /// <param name="changeFields">Чи потрібно копіювати дані при об'єднанні</param>
        private void UnionSites(RecordPtr context, RecordPtr recAnt, bool changeFields)
        {
            using (LisTransaction tr = new LisTransaction())
            {
                PositionState.ChangeSites(context, recAnt, changeFields);
                //3.Видалити Site2
                using (LisRecordSet rSite2 = new LisRecordSet(recAnt.Table, IMRecordset.Mode.ReadWrite))
                {
                    rSite2.Select("ID");
                    rSite2.SetWhere("ID", IMRecordset.Operation.Eq, recAnt.Id);
                    rSite2.Open();
                    if (!rSite2.IsEOF())
                        rSite2.Delete();
                }
                tr.Commit();
            }
        }

        private bool OnEditPosAdmSite(IMQueryMenuNode.Context context)
        {
            AdminSiteAllTech.Show(context.TableName, context.TableId);
            return true;
        }

        private bool OnEditPosAdmSite2(IMQueryMenuNode.Context context)
        {
            /*
            string rightInfo = string.Format("{5} on {0}:\r" + Environment.NewLine + "select \t{1}\rinsert \t{2}\rupdate \t{3}\rdelete \t{4}",
                context.TableName,
                IM.TableRight(context.TableName, IMTableRight.Select), IM.TableRight(context.TableName, IMTableRight.Insert), IM.TableRight(context.TableName, IMTableRight.Update), IM.TableRight(context.TableName, IMTableRight.Delete),
                IM.ConnectedUser());
            MessageBox.Show(rightInfo, "Right Info");
            */
            if (context.TableId == IM.NullI || context.TableId == 0)
            {
                PositionState2 pos = null;
                DialogResult dr = AdminSiteAllTech2.CreateNewPosition(context.TableName, ref pos, IcsmMainWindows);
                if ((pos != null) && (dr == DialogResult.OK))
                {
                    pos.Save(0);
                    IM.RefreshQueries(context.TableName);
                }
            }
            else
                AdminSiteAllTech2.Show(context.TableName, context.TableId, (IM.TableRight(context.TableName) & IMTableRight.Update) != IMTableRight.Update, IcsmMainWindows);
            return true;
        }

        public void GetMainMenu(IMMainMenu mainMenu)
        {
            mainMenu.SetInsertLocation("Help", IMMainMenu.InsertLocation.Before);
            mainMenu.InsertItem("UCRF Plugin\\Change department...", OnChangeDepartment, ICSMTbl.USERS);
            mainMenu.InsertItem("UCRF Plugin\\Recognize document...", OnStartRecognizeDocument, ICSMTbl.DocFiles);
            mainMenu.InsertItem("UCRF Plugin\\Get RFA tax payers...", OnGetTaxPayerList, PlugTbl.XvTaxPayerForm1);
            mainMenu.InsertItem("UCRF Plugin\\Ftp settings...", OnSetFtpServer, "SYS_CONFIG");
            mainMenu.InsertItem("UCRF Plugin\\Plugin settings...", OnPluginSettings, "SYS_CONFIG");
            mainMenu.InsertItem("UCRF Plugin\\Edit Status...", OnEditStatus, "MOB_STATION2");
            mainMenu.InsertItem("UCRF Plugin\\Reminder", OnReminder, "REMINDER");
            mainMenu.InsertItem("UCRF Plugin\\UTDB test", OnUtdbTest, "SYS_CONFIG");
            mainMenu.InsertItem("UCRF Plugin\\About plugin...", OnAboutPlugin, "SYS_CONFIG");
            mainMenu.InsertItem("UCRF Plugin\\SOURCER", OnSourcer, "SYS_CONFIG");
            mainMenu.InsertItem("Tools\\Reports\\Special Report(#608)", OnSpecialReport, PlugTbl.itblXnrfaReport);
#if DEBUG
            if (ApplSetting.IsDebug)
            {
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Send to R135", OnSendToR135, "SYS_CONFIG");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Load from R135", OnLoadFromR135, "SYS_CONFIG");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Load the answer from R-135 (manually)", OnAnswerP135, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\IRF Test form...", OnTestIRFForm, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Test FEnterForm...", OnTestFEnterForm, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\OnTestXML...", OnTestXML, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Test connection to UTDB...", OnTestGlobalDb, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Answer from PARUS to UTDB...", OnAnswerParus, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Read XML from UTDB...", OnReadDRVAnswer, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Load Parus employeer...", OnAnswerParusEmployeer, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\test", ReadXMLContragents_DEBUG, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Start updating articles", OnStartArticleUpdate, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Start updating works count", OnStartWorksCountUpdate, "MOB_STATION2");
                mainMenu.InsertItem("UCRF Plugin\\Only for debug\\Set status automatically", OnAutoSetStatus, "MOB_STATION2");
            }
#endif
            if (ApplSetting.IsRunTest)
            {
                mainMenu.InsertItem("UCRF Plugin\\Auto test\\Run all tests", OnRunAutoTest, "SYS_CONFIG");
            }
        }

        #region Local functions
        //===================================================
        /// <summary>
        /// Добавляем контекстное меню, если есть права
        /// </summary>
        /// <param name="menuList">список менб</param>
        /// <param name="tableName">имя таблицы</param>
        /// <param name="node">элемент меню</param>
        /// <param name="right">права</param>
        private void AddContextMenu(ref List<IMQueryMenuNode> menuList, string tableName, IMQueryMenuNode node, IMTableRight right)
        {
            if ((IM.TableRight(tableName) & right) == right)
                menuList.Add(node);
        }
        //===================================================
        /// <summary>
        /// Отправить станции в Р135
        /// </summary>
        private int SendToR135(bool isSilent)
        {
            return CMonitoringAppl.SendToR135(isSilent);
        }
        //===================================================
        /// <summary>
        /// Обработать ответы из Р135
        /// </summary>
        private int ReadFromR135(bool isSilent)
        {
            return CMonitoringAppl.ReadFromR135(isSilent);
        }

        #endregion

        #region EVENTS

        /// <summary>
        /// Декларация классов объектов плагина
        /// </summary>
        public void OnSourcer()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.DefaultExt = "cs";
            if (sfd.ShowDialog() == DialogResult.OK)
                IM.SourcerPlugin(Ident,sfd.FileName);
        }

        /// <summary>
        /// Изменить управление
        /// </summary>
        public void OnChangeDepartment()
        {
            CUsers.ChangeUserDepartment();
        }
        /// <summary>
        /// Запустить функцию 
        /// </summary>
        public void OnStartRecognizeDocument()
        {
            using(DocRecognizer.FormDocRecognize d = new DocRecognizer.FormDocRecognize())
                d.Show();
        }
        /// <summary>
        /// this function only for debuging please remove it when it will be not nessecary at all
        /// </summary>
        public void ReadXMLContragents_DEBUG()
        {
            GlobalDB.CGlobalDB.ReadUTDB(false,EService.ICSM);
        }
        //TODO Only for debug
        public void OnStartArticleUpdate()
        {
            CArticleUpdate.UpdateArticles(false, "");
        }
        //TODO Only for debug
        public void OnStartWorksCountUpdate()
        {
            CArticleUpdate.UpdateWorksCount();
        }
        /// <summary>
        /// Создает запись для международной координации
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnHcmMobStationProcess(IMQueryMenuNode.Context context)
        {
            HCM.HcmMobStation.AddMobStationToWien(context.TableId);
            return true;
        }
        /// <summary>
        /// Запустить автоматическую простановку статей
        /// </summary>
        public void OnAutoSetStatus()
        {
            StationsStatus.StationAutoStatus autoStatus = new StationsStatus.StationAutoStatus();
            foreach (StationsStatus.StateAutoWorkFlow item in autoStatus.aWf.StateAwf)
            {
                item.Execute(true);
            }
        }
        /// <summary>
        /// Редактирвание warkflow статусов
        /// </summary>
        public void OnEditStatus()
        {
            using (StationsStatus.ModifyStatus frm = new StationsStatus.ModifyStatus())
                frm.ShowDialog();
        }

        //===================================================
        /// <summary>
        /// Устанавливаем параметры для FTP соедидения
        /// </summary>
        public void OnSetFtpServer()
        {
            using (FSettingFtp frm = new FSettingFtp())
            {
                frm.ShowDialog();
            }
        }
        //===================================================
        /// <summary>
        /// Устанавливаем параметры плагина
        /// </summary>
        public void OnPluginSettings()
        {
            using (FrmPluginSetting frm = new FrmPluginSetting())
            {
                frm.ShowDialog();
            }
        }
        //===================================================
        /// <summary>
        /// Отправляет станции в Р135
        /// </summary>
        public void OnSendToR135()
        {
            SendToR135(false);
        }
        //===================================================
        /// <summary>
        /// Отправляет станции в Р135
        /// </summary>
        public void OnLoadFromR135()
        {
            CMonitoringAppl.ReadFromR135(false);
        }

        public void OnAnswerP135()
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        System.IO.StreamReader myStream = new System.IO.StreamReader(fileDialog.FileName);
                        if (myStream != null)
                        {
                            using (myStream)
                            {
                                CLoadAnswer.HandleAnswer(myStream, fileDialog.FileName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    }
                }
            }
        }
        //===================================================
        /// <summary>
        /// Отображает версию плагина
        /// </summary>
        public void OnAboutPlugin()
        {
//            string isRelease = "Release";
//#if DEBUG
//            isRelease = "Debug";
//#endif
//            string about = string.Format("Version   : {0} ({1})\n" +
//                                         "Revision  : {3}\n" +
//                                         "Date build: {2}",
//                                      VersionPlugin,
//                                      string.Format("{0}{1}", isRelease, (ApplSetting.IsDebug) ? " [Test]" : ""),
//                                      CVersion.DateBuild,
//                                      CVersion.CurRevision);
//            MessageBox.Show(about, CLocaliz.TxT("About plugin..."), MessageBoxButtons.OK, MessageBoxIcon.Information);

            using(Auxiliary.Form.AboutPluginBox aboutForm = new Auxiliary.Form.AboutPluginBox())
            {
                aboutForm.ShowDialog();
            }
        }

        public void OnGetTaxPayerList()
        {
            TaxPayerForm taxPayerForm = new TaxPayerForm();
            taxPayerForm.ShowDialog();
        }

        //??
        public void OnTestIRFForm()
        {
            TestIRF testForm = new TestIRF();
            testForm.ShowDialog();
        }

        /// <summary>
        /// ЦОБ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnExpDrv(IMQueryMenuNode.Context context)
        {
            using (DirectNaborForm dnf = new DirectNaborForm(context.TableName))
            {
                dnf.ShowDialog();
            }
            return false;
        }

        /// <summary>
        /// Робота з ЦОБ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnRepairDrv(IMQueryMenuNode.Context context)
        {
            CreatePartXml cpx = new CreatePartXml();
            try
            {
                IMRecordset rs;
                using (DesignNaborForm dnf = new DesignNaborForm(context.TableName))
                {
                    DialogResult dr = dnf.ShowDialog();
                    //створити файл
                    if (dr == DialogResult.Yes)
                    {
                        try
                        {
                            rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                            XDocument xDoc = cpx.ExportTableRecursion(rs, dnf.DictLocal, context, 0, 0, null);
                            string fileName = dnf.CreateFile();
                            if (fileName != null)
                            {
                                xDoc.Save(fileName);
                                MessageBox.Show("Дані успішно збережено!");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Дані не збережено!" + ex.Message);
                        }
                    }
                    //Експорт в БД
                    else if (dr == DialogResult.OK)
                    {
                        try
                        {
                            rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                            XDocument xDoc = cpx.ExportTableRecursion(rs, dnf.DictLocal, context, 0, 0, null);
                            CGlobalDB cDb = new CGlobalDB();
                            cDb.OpenConnection();
                            cDb.WriteXMLToDB(xDoc.ToString(), dnf.CurrEntity);
                            cDb.CloseConnection();
                            MessageBox.Show("Xml файл записано в БД успішно!");
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Xml файл не записано в БД! " + e.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        //??
        public void OnTestXML()
        {
            List<RecordPtr> listEquip = new List<RecordPtr>();
            listEquip.Add(new RecordPtr("EQUIP_MOB2", 296));
            listEquip.Add(new RecordPtr("EQUIP_MOB2", 295));
            string fileName = "C:\\test.xml";
            MessageBox.Show(GlobalDB.CGlobalXML.CreateEquipXML(listEquip, ref fileName).ToString());


            List<RecordPtr> listSite = new List<RecordPtr>();
            listSite.Add(new RecordPtr("POSITION_BRO", 390));
            listSite.Add(new RecordPtr("POSITION_BRO", 389));
            fileName = "C:\\test_site.xml";
            MessageBox.Show(GlobalDB.CGlobalXML.CreateSiteXML(listSite, ref fileName).ToString());
        }
        //??
        public void OnTestGlobalDb()
        {
            TestGlobalDb.RunTestGlobalDb(new GlobalDbLocal());
        }

        public void OnAnswerParus()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string xmlStr = "";
                using (System.IO.TextReader sr = new System.IO.StreamReader(dlg.FileName, Encoding.GetEncoding(1251)))
                {
                    xmlStr = sr.ReadToEnd();
                }
                CGlobalDB gdb = new CGlobalDB();
                if (gdb.OpenConnection())
                {
                    gdb.WriteXMLToDB(xmlStr, EEssence.STATE_APPL_PAY_PARUS, EService.PARUS);
                    gdb.CloseConnection();
                    MessageBox.Show("OK");
                }
            }
            dlg.Dispose();
        }

        public void OnAnswerParusEmployeer()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string xmlStr = "";
                using (System.IO.StreamReader sr = new System.IO.StreamReader(dlg.FileName, Encoding.GetEncoding(1251)))
                {
                    String line;
                    // Read and display lines from the file until the end of
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null)
                        xmlStr += line;
                }
                CGlobalDB gdb = new CGlobalDB();
                if (gdb.OpenConnection())
                {
                    gdb.WriteXMLToDB(xmlStr, EEssence.EMPLOYEE_PARUS, EService.PARUS);
                    gdb.CloseConnection();
                    MessageBox.Show("OK");
                }
            }
            dlg.Dispose();
        }

        public void OnReadDRVAnswer()
        {
            CGlobalDB gdb = new CGlobalDB();
            if (gdb.OpenConnection())
            {
                if (gdb.GetCountRecords() > 0)
                {
                    string xml = gdb.ReadXmlFromDb();
                    int err = gdb.ExecuteData(xml,EService.ICSM);
                    if (err == 0)
                        gdb.ChangeStatus(EStateXML.NotActive, 0);
                    else
                        gdb.ChangeStatus(EStateXML.Error, err);
                    MessageBox.Show("Текущий ответ обработан. Осталось необработаных ответов: " + gdb.GetCountRecords().ToString(), "Завершение обработки ответа из ДРВ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Нет заявок для обработки!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                gdb.CloseConnection();
            }
            else
                MessageBox.Show("Ошибка подключения к UTDB.");
        }


        private bool OnNewEditXfaEquip(IMQueryMenuNode.Context context)
        {
           return ShowAbonentEquipForm(context.TableId, false);
        }

        private bool OnCopyEditXfaEquip(IMQueryMenuNode.Context context) {
           return ShowAbonentEquipForm(context.TableId, true);
        }

        private bool ShowAbonentEquipForm(int id, bool copyRecord) {
           using(AbonentEquipForm frm = new AbonentEquipForm(id, copyRecord))
              if(frm.ShowDialog() == DialogResult.OK)
                 frm.Save();
           return true;
        }

        private bool OnDeleteBaseStationFromNet(IMQueryMenuNode.Context context)
        {
            string nameNet = "";
            string stationName = "";
            {
                //Вытаскиваем текущие данные
                IMRecordset rsNet = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                rsNet.Select("ID,Net.NAME,BaseMobStation.NAME,BaseMobStation2.NAME");
                rsNet.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
                try
                {
                    rsNet.Open();
                    if (!rsNet.IsEOF())
                    {
                        nameNet = rsNet.GetS("Net.NAME");
                        stationName = rsNet.GetS("BaseMobStation.NAME");
                        if (string.IsNullOrEmpty(stationName))
                            stationName = rsNet.GetS("BaseMobStation2.NAME");
                    }
                }
                finally
                {
                    if (rsNet.IsOpen())
                        rsNet.Close();
                    rsNet.Destroy();
                }
            }
            string message = string.Format(CLocaliz.TxT("You are trying to delete the base station '{0}' from the net '{1}'. Do you want to continue?"), stationName, nameNet);
            DialogResult res = MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                               MessageBoxDefaultButton.Button2);
            if (res != DialogResult.Yes)
                return false;
            message = string.Format(CLocaliz.TxT("Do you want to delete base station from the net '{0}'?"), nameNet);
            res = MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                               MessageBoxDefaultButton.Button2);
            if (res != DialogResult.Yes)
                return false;
            //Удаляем запись
            {
                IMRecordset rsNetBaseStation = new IMRecordset(context.TableName, IMRecordset.Mode.ReadWrite);
                rsNetBaseStation.Select("ID");
                rsNetBaseStation.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
                try
                {
                    rsNetBaseStation.Open();
                    if (!rsNetBaseStation.IsEOF())
                        rsNetBaseStation.Delete();
                }
                finally
                {
                    if (rsNetBaseStation.IsOpen())
                        rsNetBaseStation.Close();
                    rsNetBaseStation.Destroy();
                }
            }
            return true;
        }
        ///// <summary>
        ///// Редактируем данніе сети
        ///// </summary>
        //private bool OnEditAbonentNet(IMQueryMenuNode.Context context)
        //{
        //    string nameNet = "";
        //    string netCallSign = "";
        //    {
        //        //Вытаскиваем текущие данные
        //        IMRecordset rsNet = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
        //        rsNet.Select("ID,NAME,CALL_SIGN");
        //        rsNet.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
        //        try
        //        {
        //            rsNet.Open();
        //            if (!rsNet.IsEOF())
        //            {
        //                nameNet = rsNet.GetS("NAME");
        //                netCallSign = rsNet.GetS("CALL_SIGN");
        //            }
        //        }
        //        finally
        //        {
        //            if (rsNet.IsOpen())
        //                rsNet.Close();
        //            rsNet.Destroy();
        //        }
        //    }
        //    {
        //        //Отображаем окно для редактирования данных
        //        IMDialog dialogNewNet = new IMDialog();
        //        dialogNewNet.Add("txtNetName", "", nameNet, CLocaliz.TxT("Net name"));
        //        dialogNewNet.Add("txtCallName", "", netCallSign, CLocaliz.TxT("Call name"));
        //        if (dialogNewNet.Enter() == false)
        //            return false;
        //        nameNet = dialogNewNet.Get("txtNetName") as string;
        //        netCallSign = dialogNewNet.Get("txtCallName") as string;
        //    }
        //    {
        //        //Сохраняем данные
        //        IMRecordset rsNet = new IMRecordset(context.TableName, IMRecordset.Mode.ReadWrite);
        //        rsNet.Select("ID,NAME,CALL_SIGN,DATE_MODIFIED,MODIFIED_BY");
        //        rsNet.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
        //        try
        //        {
        //            rsNet.Open();
        //            if (!rsNet.IsEOF())
        //            {
        //                rsNet.Edit();
        //                rsNet.Put("NAME", nameNet);
        //                rsNet.Put("CALL_SIGN", netCallSign);
        //                rsNet.Put("DATE_MODIFIED", DateTime.Now);
        //                rsNet.Put("MODIFIED_BY", IM.ConnectedUser());
        //                rsNet.Update();
        //            }
        //        }
        //        finally
        //        {
        //            if (rsNet.IsOpen())
        //                rsNet.Close();
        //            rsNet.Destroy();
        //        }
        //    }
        //    return true;
        //}


        public void OnTestFEnterForm()
        {
            List<double> collect = new List<double>();
            for (int i = 0; i < 30; i++)
            {
                collect.Add(i);
            }

            FEnterEVP fEnter = new FEnterEVP(collect, "Test Window", "First Row", "Second Row");
            fEnter.ShowDialog();
            fEnter.Dispose();
        }

        private bool OnAdditionFiltersNew(IMQueryMenuNode.Context context)
        {
            AdditionFillters.AdditionFilters addFilters = new AdditionFillters.AdditionFilters(context.TableName);
            addFilters.ShowDialog();
            addFilters.Dispose();
            return true;
        }

        [Obsolete("Только для отладки")]
        private bool OnCreateXmlUtdb(IMQueryMenuNode.Context context)
        {
            string essence;
            switch (context.TableName)
            {
                case PlugTbl.AllStations:
                    essence = EEssence.StationIcsm;
                    break;
                case PlugTbl.itblXnrfaPrice:
                    essence = EEssence.PriceIcsm;
                    break;
                default:
                    throw new IMException(string.Format("Not implemented for table {0}", context.TableName));
            }
            ExportToXmlUtdb test = new ExportToXmlUtdb();
            XDocument doc = test.ExportTable(context.TableName, context.TableId, 0);
            CGlobalDB gdb = new CGlobalDB();
            if (gdb.OpenConnection() == true)
            {
                StringBuilder xml = new StringBuilder();
                xml.AppendLine(doc.Declaration.ToString());
                xml.AppendLine(doc.ToString());
                gdb.WriteXMLToDB(xml.ToString(), essence, EService.ICSM);
                gdb.CloseConnection();
                MessageBox.Show("OK");
            }
            else
                MessageBox.Show("Error");
            return false;
        }

        //===================================================
        /// <summary>
        /// Помечает станцию для отправки в Р135 по разрешению
        /// </summary>
        private bool OnMonitoringDozvXml(IMQueryMenuNode.Context context)
        {
            using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Loading records...")))
            {
                pBar.SetBig(string.Format(CLocaliz.TxT("Loading records from a table {0}"), context.TableName));
                IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                r.Select("ID");
                r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        int curId = r.GetI("ID");
                        CMonitoringAppl.MarkApplToSendToR135(curId, CMonitoringAppl.DocType.PERM);
                        pBar.SetSmall(curId);
                    }
                }
                finally
                {
                    r.Destroy();
                }
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Помечает станцию для отправки в Р135 по заключению
        /// </summary>
        private bool OnMonitoringConcXml(IMQueryMenuNode.Context context)
        {
            using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Loading records...")))
            {
                pBar.SetBig(string.Format(CLocaliz.TxT("Loading records from a table {0}"), context.TableName));
                IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                r.Select("ID");
                r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        int curId = r.GetI("ID");
                        CMonitoringAppl.MarkApplToSendToR135(curId, CMonitoringAppl.DocType.CONC);
                        pBar.SetSmall(curId);
                    }
                }
                finally
                {
                    r.Destroy();
                }
            }
            return false;
        }
        //TODO Удалить
        //===================================================
        /// <summary>
        /// Создаем файл для мониторинга
        /// </summary>
        private bool OnTestMemory(IMQueryMenuNode.Context context)
        {
            using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Test memmory...")))
            {
                int count = 0;
                IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                r.Select("ID");
                r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        using (LisProgressBar pb = new LisProgressBar("Test memory"))
                        {
                            while (pb.UserCanceled() == false)
                            {
                                pb.SetSmall(count++);
                                using (BaseAppClass tmp = BaseAppClass.GetBaseAppl(r.GetI("ID")))
                                {
                                }
                            }
                        }
                    }
                }
                finally
                {
                    r.Destroy();
                }
            }
            GC.Collect();
            return false;
        }

        //===================================================
        /// <summary>
        /// 
        /// </summary>
        private bool OnUpdateDistance(IMQueryMenuNode.Context context)
        {
            try
            {
                DialogResult result = MessageBox.Show(CLocaliz.TxT("Do you want update distance border?"), "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    _idwmVal = new Idwm();
                    try {
                        _idwmVal.Init(11); //Init IDWM
                    }
                    catch {
                    }
                    int countUpdateRec = 0;
                    using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Update distance border..."))) {
                        IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadWrite);
                        r.Select("ID,LONGITUDE,LATITUDE,DIST_BORDER");
                        r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                        try {
                            for (r.Open(); !r.IsEOF(); r.MoveNext()) {
                                pBar.SetBig(string.Format("Розрахунок відстані для кордону для ID = {0}", r.GetI("ID")));
                                int curId = r.GetI("ID");
                                float lonDec = (float)r.GetD("LONGITUDE");
                                float latDec = (float)r.GetD("LATITUDE");
                                string[] exclude = { "UKR" };
                                NearestCountryItem[] nearestCountry = _idwmVal.GetNearestCountries(Idwm.DecToRadian(lonDec), Idwm.DecToRadian(latDec), 1000, exclude, 100);
                                if (nearestCountry.Length > 0){
                                    double DistBorder = ((double)(nearestCountry[0].distance)).Round(2);
                                    r.Edit();
                                    r.Put("DIST_BORDER", DistBorder);
                                    countUpdateRec++;
                                    r.Update();
                                }
                                CLogs.WriteInfo(ELogsWhat.Unknown, string.Format("{0} ({1})", context.TableName, curId));
                                pBar.SetSmall(curId);
                            }
                        }
                        finally {
                            r.Close();
                            r.Destroy();
                        }
                        MessageBox.Show(string.Format("Оновлено записів: {0}", countUpdateRec));
                    }
                    _idwmVal.Dispose();
                }
            }
            catch
            {
            }
            return true;
        }
        //===================================================
        /// <summary>
        /// Удаляет запись из БД
        /// </summary>
        private bool OnDeleteAllRecord(IMQueryMenuNode.Context context)
        {
            DialogResult result = MessageBox.Show(CLocaliz.TxT("Do you want to delete selected record(s)?"), CLocaliz.TxT("Delete record"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Deleting records...")))
                {
                    pBar.SetBig(string.Format(CLocaliz.TxT("Deleting records from a table {0}"), context.TableName));
                    IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadWrite);
                    r.Select("ID");
                    r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    try
                    {
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            int curId = r.GetI("ID");
                            CLogs.WriteInfo(ELogsWhat.Delete, string.Format("{0} ({1})", context.TableName, curId));
                            pBar.SetSmall(curId);
                            r.Delete();
                        }
                    }
                    finally
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
            }
            return true;
        }
        //===================================================
        /// <summary>
        /// Удаляет пакета из БД
        /// </summary>
        private bool OnDeletePacket(IMQueryMenuNode.Context context)
        {
            using (LisProgressBar pBar = new LisProgressBar(CLocaliz.TxT("Checking the records...")))
            {
                UcrfDepartment curManagment = CUsers.GetCurDepartment();
                List<int> lstPacketID = new List<int>();
                List<int> lstPacketManagment = new List<int>();
                {//Выборка ID пакетов
                    IMRecordset r = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,CREATED_MANAGEMENT");
                    r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    try
                    {
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            int curId = r.GetI("ID");
                            UcrfDepartment packetManagement = r.GetS("CREATED_MANAGEMENT").ToUcrfDepartment();
                            if (CUsers.IsUseManagement(packetManagement, curManagment) == false)
                                lstPacketManagment.Add(curId); //Проверка совместимости пользователей
                            lstPacketID.Add(curId);
                            pBar.SetSmall(curId);
                        }
                    }
                    finally
                    {
                        r.Destroy();
                    }
                }
                //----
                {//Проверка на непустые пакеты
                    string idString = "";
                    foreach (int id in lstPacketID)
                        idString += (id + ",");
                    if (idString != "")
                        idString = "(" + idString.Remove(idString.Length - 1) + ")";
                    else
                        idString = "(0)";

                    lstPacketID.Clear();

                    IMRecordset r = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                    r.Select("Packet.ID");
                    r.SetAdditional("([Packet.ID] in " + idString + ")");
                    try
                    {
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                            lstPacketID.Add(r.GetI("Packet.ID"));
                    }
                    finally
                    {
                        r.Destroy();
                    }
                }
                if (lstPacketID.Count != 0)
                {
                    string lstId = HelpFunction.ToString<int>(lstPacketID);
                    MessageBox.Show(string.Format("Деякі пакети не пусті. Видаляти не пусті пакети заборонено. Операція відманена. ({0})", lstId), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else if (lstPacketManagment.Count != 0)
                {
                    string lstManagment = HelpFunction.ToString<int>(lstPacketManagment);
                    MessageBox.Show(string.Format("Деякі пакети були створені не Вашим управлінням. Операція відманена. ({0})", lstManagment), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                    return OnDeleteAllRecord(context);
            }
            return true;
        }
        //===================================================
        /// <summary>
        /// Поиск станции из вида ALL_STATIONS
        /// </summary>
        private bool OnFindStations(IMQueryMenuNode.Context context)
        {
            using (FFindAllStation frm = new FFindAllStation(context.TableId))
            {
                frm.ShowDialog();
            }
            return false;
        }

        private bool OnGenerateBagStation(IMQueryMenuNode.Context context)
        {
            try
            {
                if ((curStation != null) && (List_Stations.Count > 0))
                {
                    isStopped = false;
                    DialogResult DA = MessageBox.Show(string.Format("Виконати формування ОУ для станції {0}({1})?", curStation.Table_, curStation._tableID), "", MessageBoxButtons.YesNo);
                    if (DA == DialogResult.Yes)
                    {
                        List_Stations_Microwa = List_Stations.FindAll(r => r.Table_ == "MICROWA");
                        List_Stations_Another = List_Stations.FindAll(r => r.Table_ != "MICROWA");
                        if (List_Stations_Microwa.Count > 0)
                            OnGenerateBagStationMicrowa(context);
                        if (List_Stations_Another.Count > 0)
                            OnGenerateBagStationAnother(context);
                        List_Stations.Clear();
                    }
                }
                else MessageBox.Show("Буфер з переліком станцій пустий!");
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnGenerateBagStationMicrowa(IMQueryMenuNode.Context context)
        {
            try
            {
                AppType appt = AppType.AppUnknown;
                bool isSuccessSave = false;
                if (curStation == null) List_Stations_Microwa.Clear();
                if ((curStation != null) && (List_Stations_Microwa.Count > 0))
                {
                    //DialogResult DA = MessageBox.Show(string.Format("Виконати формування ОУ для станції {0}({1})?", curStation.Table_, curStation._tableID), "", MessageBoxButtons.YesNo);
                    //if (DA == DialogResult.Yes)
                    //{
                    string TABLE_NAME = "";
                    string var1 = ""; string var_RRZ1 = ""; string var_RRZ2 = ""; string var2 = ""; string var_extend = ""; string var3 = ""; string var_filial = ""; string Frg4_loop = "";

                    List<string> L_NameOwner = new List<string>();
                    foreach (Station item in List_Stations_Microwa)
                    {
                        if (item.Table_ != "MICROWA")
                        {
                            if (!L_NameOwner.Contains(item.NameOwner))
                                L_NameOwner.Add(item.NameOwner);
                        }
                    }

                    List<KeyValuePair<string, string>> List_Owner = new List<KeyValuePair<string, string>>();
                    string Out_Val_REZ = "";
                    string var_extend_ = "";
                    foreach (string ir in L_NameOwner)
                    {
                        string Address = "";
                        var_extend_ = "";
                        foreach (Station item in List_Stations_Microwa)
                        {
                            if (item.Table_ != "MICROWA")
                            {
                                if (item.NameOwner == ir)
                                {
                                    if (!string.IsNullOrEmpty(item.Num_Dozv))
                                        var_extend_ = "дозвіл на експлуатацію № " + item.Num_Dozv;
                                    else if (!string.IsNullOrEmpty(item.Num_Visn))
                                        var_extend_ = "висновок щодо ЕМС № " + item.Num_Visn;
                                    else if (!string.IsNullOrEmpty(item.Nom_Freq))
                                        var_extend_ = "номінал радіочастоти " + item.Nom_Freq + " МГц";
                                    Address += item.AddressOwner + string.Format(" ({0}); ", var_extend_);
                                }
                            }
                        }
                        if (Address.Length > 0)
                        {
                            Address = Address.Remove(Address.Length - 1, 1);
                            Out_Val_REZ += " РЕЗ " + ir + ", який запланований за адресою: " + Address;
                        }
                    }




                    foreach (Station item in List_Stations_Microwa)
                    {
                        if (item.Table_ != "MICROWA")
                        {
                            //var1 = "з РЕЗ " + item.NameOwner + ", який запланований за адресою:  " + item.AddressOwner;
                        }
                        else
                        {
                            var_RRZ1 = "з боку РРС " + item.NameOwner + ", що розташована за адресою:  " + item.AddressOwner;
                            if ((item.NameOwner2 != "") && (item.AddressOwner2 != ""))
                            {
                                var_RRZ2 = " РРС " + item.NameOwner2 + ", яка розташована за адресою:  " + item.AddressOwner2;
                            }
                            var1 = string.Format(" {0},{1} ", var_RRZ1, var_RRZ2);
                        }
                        if (!string.IsNullOrEmpty(item.Num_Dozv))
                            var_extend = "дозвіл на експлуатацію № " + item.Num_Dozv;
                        else if (!string.IsNullOrEmpty(item.Num_Visn))
                            var_extend = "висновок щодо ЕМС № " + item.Num_Visn;
                        else if (!string.IsNullOrEmpty(item.Nom_Freq))
                            var_extend = "номінал радіочастоти " + item.Nom_Freq + " МГц";

                        if (item.Table_ != "MICROWA")
                        {
                            var2 += item.NameOwner + ", ";
                        }
                        else var2 += item.NameOwner2 + ", ";

                        if (item.Table_ == "MICROWA")
                            Frg4_loop += string.Format("{0} ({1})", var1, var_extend) + "; ";

                        var_filial = item.NameFilial != "ДРЗП УДЦР УДЦР" ? item.NameFilial : "УДЦР";
                        TABLE_NAME = item.Table_;
                    }

                    if (TABLE_NAME == "MICROWA")
                    {
                        if (Frg4_loop.Length > 0) Frg4_loop = Frg4_loop.Remove(Frg4_loop.Length - 2, 2);
                    }
                    else
                    {
                        if (Out_Val_REZ.Length > 0) Frg4_loop = "з" + Out_Val_REZ.Remove(Out_Val_REZ.Length - 1, 1);
                    }


                    if (var2.Length > 0) var2 = var2.Remove(var2.Length - 2, 2);

                    string Frg1 = "";
                    if (TABLE_NAME != "MICROWA") Frg1 = "1. Аналіз розрахунків електромагнітної сумісності (ЕМС) розташованих частотних присвоєнь вищезгаданого РЕЗ з частотними присвоєннями іншим РЕЗ свідчить про можливість створення взаємних радіозавад " + Frg4_loop + "." + Environment.NewLine;
                    else Frg1 = "1. Аналіз розрахунків електромагнітної сумісності (ЕМС) запланованих частотних присвоєнь вищезгаданого РЕЗ з частотними присвоєннями іншим РЕЗ свідчить про можливість створення взаємних радіозавад " + Frg4_loop + "." + Environment.NewLine;

                    string Frg5 = "2. Термін дії висновку становить шість місяців." + Environment.NewLine;
                    string Frg6 = "                                                                                      ОСОБЛИВІ УМОВИ" + Environment.NewLine;
                    string Frg7 = "1. Можливість здійснення запланованих частотних присвоєнь може бути визначена за результатами натурних випробувань (НВ)." + Environment.NewLine;
                    string Frg8 = "2. НВ проводяться Заявником із залученням фахівців ";
                    string Frg9 = string.Format("{0}", var2);
                    string Frg10 = string.Format(" та представників {0}.", var_filial) + Environment.NewLine;
                    string Frg11 = "3. Організація та проведення НВ здійснюється відповідно до Порядку проведення приймальних випробувань РЕЗ та випромінювальних пристроїв на місці експлуатації (затверджений рішенням НКРЗ від 26.07.07 №854)." + Environment.NewLine;
                    string Frg12 = "4. За результатами НВ Заявником складається відповідний протокол." + Environment.NewLine;
                    string Frg13 = "5. Рішення про видачу дозволу на експлуатацію РЕЗ приймається УДЦР на підставі позитивних результатів НВ." + Environment.NewLine;
                    string Out_Str = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", Frg1, Frg5, Frg6, Frg7, Frg8, Frg9, Frg10, Frg11, Frg12, Frg13);

                    YXnrfaAppl rs = new YXnrfaAppl();
                    {
                        rs.Format("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE,DOZV_NUM,DOZV_NUM_NEW");
                        if (rs.Fetch(string.Format("([OBJ_ID1]={0} OR [OBJ_ID2]={0} OR [OBJ_ID3]={0}  OR [OBJ_ID4]={0}   OR [OBJ_ID5]={0}  OR [OBJ_ID6]={0}) AND ([OBJ_TABLE]={1})", curStation._tableID, curStation._table.ToSql())))
                        {
                            using (BaseAppClass obj = BaseAppClass.GetAppl(rs.m_id, true))
                            {
                                CustomDialogBoxOU.CurrentPress = DialogResultMessageBoxButtons.Override;
                                DialogResultMessageBoxButtons tpxc = DialogResultMessageBoxButtons.Override;
                                if (obj != null)
                                {
                                    appt = obj.appType;
                                    try
                                    {
                                        if (obj.SCVisnovok != null)
                                        {
                                            if (!string.IsNullOrEmpty(obj.SCVisnovok))
                                            {
                                                CustomDialogBoxOU OU_dialog = new CustomDialogBoxOU(obj.SCVisnovok, Out_Str, "Формування особливих умов висновку щодо ЕМС");
                                                tpxc = CustomDialogBoxOU.CurrentPress;
                                            }
                                            if (tpxc == DialogResultMessageBoxButtons.Append)
                                            {
                                                if (MainAppForm.isShowDialog)
                                                {
                                                    CallBackFunction.callbackEventHandler(obj.SCVisnovok = obj.SCVisnovok + (obj.SCVisnovok.Length > 0 ? Environment.NewLine : "") + Out_Str);
                                                    isSuccessSave = true;
                                                }
                                                else
                                                {
                                                    obj.SCVisnovok = obj.SCVisnovok + (obj.SCVisnovok.Length > 0 ? Environment.NewLine : "") + Out_Str;
                                                    using (MainAppForm formAppl = new MainAppForm(obj))
                                                    {
                                                        formAppl.SaveToBase();
                                                        isSuccessSave = true;
                                                    }
                                                }
                                            }
                                            else if (tpxc == DialogResultMessageBoxButtons.Override)
                                            {
                                                if (MainAppForm.isShowDialog)
                                                {
                                                    CallBackFunction.callbackEventHandler(obj.SCVisnovok = Out_Str); isSuccessSave = true;
                                                }
                                                else
                                                {
                                                    obj.SCVisnovok = Out_Str;
                                                    using (MainAppForm formAppl = new MainAppForm(obj)) { formAppl.SaveToBase(); isSuccessSave = true; }
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                    catch (Exception exx)
                                    {

                                    }
                                }
                            }
                        }
                        rs.Close();
                    }
                    //}
                    if (isSuccessSave)
                    {
                        CJournal.CreateReport(curStation._tableID, appt);
                        curStation = null; List_Stations_Microwa.Clear();
                        MessageBox.Show("ОУ сформовані успішно!");
                    }
                }
                else { if (List_Stations_Microwa.Count == 0) MessageBox.Show("Буфер з переліком станцій пустий!"); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnGenerateBagStationAnother(IMQueryMenuNode.Context context)
        {
            try
            {
                List<KeyValuePair<string, string>> value_except = new List<KeyValuePair<string, string>>();
                value_except.Add(new KeyValuePair<string, string>("Товариство з обмеженою відповідальністю", "ТОВ"));
                value_except.Add(new KeyValuePair<string, string>("Приватне підприємство", "ПП"));
                value_except.Add(new KeyValuePair<string, string>("Фізична особа-підприємець", "ФОП"));
                value_except.Add(new KeyValuePair<string, string>("ПРИВАТНЕ АКЦІОНЕРНЕ ТОВАРИСТВО", "ПрАТ"));
                value_except.Add(new KeyValuePair<string, string>("ПУБЛІЧНЕ АКЦІОНЕРНЕ ТОВАРИСТВО", "ПАТ"));
                value_except.Add(new KeyValuePair<string, string>("Державне підприємство", "ДП"));
                value_except.Add(new KeyValuePair<string, string>("Телерадіокомпанія", "ТРК"));
                value_except.Add(new KeyValuePair<string, string>("Спільне підприємство", "СП"));
                value_except.Add(new KeyValuePair<string, string>("Відкрите акціонерне товариство", "ВАТ"));
                value_except.Add(new KeyValuePair<string, string>("Комунальне підприємство", "КП"));
                value_except.Add(new KeyValuePair<string, string>("Закрите акціонерне товариство", "ЗАТ"));
                value_except.Add(new KeyValuePair<string, string>("Громадська організація", "ГО"));
                value_except.Add(new KeyValuePair<string, string>("КОНЦЕРН РАДІОМОВЛЕННЯ, РАДІОЗВ`ЯЗКУ ТА ТЕЛЕБАЧЕННЯ", "КРРТ"));
                value_except.Add(new KeyValuePair<string, string>("Державне підприємство науково-телекомунікаційний центр ''Українська академічна і дослідницька мережа'' інституту фізики конденсованих систем НАН України", "УАРНЕТ"));
                AppType appt = AppType.AppUnknown;
                bool isSuccessSave = false;
                if (curStation == null) { List_Stations_Another.Clear(); }
                if ((curStation != null) && (List_Stations_Another.Count > 0)) {
                    DialogResult DA = DialogResult.Yes;
                    if (DA == DialogResult.Yes) {
                        string TABLE_NAME = "";
                        string var1 = ""; string var_RRZ1 = ""; string var_RRZ2 = ""; string var2 = ""; string var_extend = ""; string var3 = ""; string var_filial = ""; string Frg4_loop = "";
                        List<string> L_NameOwner = new List<string>();
                        foreach (Station item in List_Stations_Another) {
                            if (item.Table_ != "MICROWA"){
                                if (!L_NameOwner.Contains(item.NameOwner))
                                    L_NameOwner.Add(item.NameOwner);
                            }
                        }

                        List<KeyValuePair<string, string>> List_Owner = new List<KeyValuePair<string, string>>();
                        string Out_Val_REZ = "";
                        string var_extend_ = "";
                        List<Station> Distinct_List_Stations = new List<Station>();
                        foreach (Station item in List_Stations_Another) {
                            if (item.Table_ != "MICROWA") {
                                foreach (string ir in L_NameOwner) {
                                    if (item.NameOwner == ir)  {
                                        if (!string.IsNullOrEmpty(item.Num_Dozv)) {
                                            if ((Distinct_List_Stations.Find(r => r.Num_Dozv == item.Num_Dozv)) == null) {
                                                if ((Distinct_List_Stations.Find(r => r.ID_ == item.ID_ && r.Table_ == item.Table_)) == null) {
                                                    Distinct_List_Stations.Add(item);
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(item.Num_Visn)){
                                            if ((Distinct_List_Stations.Find(r => r.Num_Visn == item.Num_Visn)) == null) {
                                                if ((Distinct_List_Stations.Find(r => r.ID_ == item.ID_ && r.Table_ == item.Table_)) == null) {
                                                    Distinct_List_Stations.Add(item);
                                                }
                                            }
                                        }
                                        else {
                                            if ((Distinct_List_Stations.Find(r => r.ID_ == item.ID_ && r.Table_ == item.Table_)) == null) {
                                                Distinct_List_Stations.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                Distinct_List_Stations.Add(item);
                            }
                        }

                        foreach (string ir in L_NameOwner) {
                            string Address = "";
                            var_extend_ = "";
                            List<KeyValuePair<string, string>> L_F_Dozv = new List<KeyValuePair<string,string>>(); 
                            List<KeyValuePair<string, string>> L_F_Visn = new List<KeyValuePair<string, string>>();
                            List<KeyValuePair<string, string>> L_F_Freq = new List<KeyValuePair<string, string>>();
                            List<Station> RF = Distinct_List_Stations.FindAll(z => z.Table_ != "MICROWA" && z.NameOwner == ir);
                            List<string> L_Address = new List<string>();
                            foreach (Station item in RF) {
                                if (!string.IsNullOrEmpty(item.Num_Dozv)) {
                                    if (!L_F_Dozv.Contains(new KeyValuePair<string, string>(item.Num_Dozv, item.AddressOwner)))
                                        L_F_Dozv.Add(new KeyValuePair<string, string>(item.Num_Dozv, item.AddressOwner));
                                }
                                else if (!string.IsNullOrEmpty(item.Num_Visn)) {
                                    if (!L_F_Visn.Contains(new KeyValuePair<string, string>(item.Num_Visn, item.AddressOwner)))
                                        L_F_Visn.Add(new KeyValuePair<string, string>(item.Num_Visn, item.AddressOwner));
                                }
                                else if (!string.IsNullOrEmpty(item.Nom_Freq))  {
                                    if (!L_F_Freq.Contains(new KeyValuePair<string, string>(item.Nom_Freq, item.AddressOwner)))
                                        L_F_Freq.Add(new KeyValuePair<string, string>(item.Nom_Freq, item.AddressOwner));
                                }
                                if (!L_Address.Contains(item.AddressOwner))
                                    L_Address.Add(item.AddressOwner);
                            }


                            if (isResize == false)
                            {
                                foreach (string addr_item in L_Address)
                                {

                                    bool isFindDozv = false;
                                    bool isFindVisn = false;
                                    bool isFindFreq = false;

                                    List<KeyValuePair<string, string>> L_F_Dozv_temp = new List<KeyValuePair<string, string>>();
                                    List<KeyValuePair<string, string>> L_F_Visn_temp = new List<KeyValuePair<string, string>>();
                                    List<KeyValuePair<string, string>> L_F_Freq_temp = new List<KeyValuePair<string, string>>();

                                    L_F_Dozv_temp = L_F_Dozv.FindAll(r => r.Value == addr_item);
                                    L_F_Visn_temp = L_F_Visn.FindAll(r => r.Value == addr_item);
                                    L_F_Freq_temp = L_F_Freq.FindAll(r => r.Value == addr_item);
                                    string addr = addr_item;
                                    string temp_dozv = "";
                                    foreach (KeyValuePair<string, string> s in L_F_Dozv_temp)
                                    {
                                        if (!string.IsNullOrEmpty(s.Key))
                                        {
                                            if (!isFindDozv) { temp_dozv += "дозвіл на експлуатацію № "; isFindDozv = true; }
                                            temp_dozv += s.Key + (isFindDozv ? ", " : "");
                                        }
                                    }
                                    if (temp_dozv.Length > 0) { temp_dozv = temp_dozv.Remove(temp_dozv.Length - 2, 2); }
                                    if ((isResize == false) && (temp_dozv.Length > 0))
                                    {
                                        Address += addr + string.Format(" ({0}); ", temp_dozv);
                                    }
                                    else if (temp_dozv.Length > 0) { Address += string.Format(" ({0}); ", temp_dozv); }

                                    {
                                        addr = addr_item;
                                        string temp_visn = "";
                                        foreach (KeyValuePair<string, string> s in L_F_Visn_temp)
                                        {
                                            if (!string.IsNullOrEmpty(s.Key))
                                            {
                                                if (!isFindVisn) { temp_visn += "висновок щодо ЕМС № "; isFindVisn = true; }
                                                temp_visn += s.Key + (isFindVisn ? ", " : "");
                                            }
                                        }
                                        if (temp_visn.Length > 0) { temp_visn = temp_visn.Remove(temp_visn.Length - 2, 2); }
                                        if ((isResize == false) && (temp_visn.Length > 0))
                                        {
                                            Address += addr + string.Format(" ({0}); ", temp_visn);
                                        }
                                        else if (temp_visn.Length > 0) { Address += string.Format(" ({0}); ", temp_visn); }
                                    }

                                    {
                                        addr = addr_item;
                                        string temp_freq = "";
                                        foreach (KeyValuePair<string, string> s in L_F_Freq_temp)
                                        {
                                            if (!string.IsNullOrEmpty(s.Key))
                                            {
                                                if (!isFindFreq) { temp_freq += "номінал радіочастоти "; isFindFreq = true; }
                                                temp_freq += s.Key + " МГц" + (isFindFreq ? ", " : "");
                                            }
                                        }
                                        if (temp_freq.Length > 0) { temp_freq = temp_freq.Remove(temp_freq.Length - 2, 2); }
                                        if ((isResize == false) && (temp_freq.Length > 0))
                                        {
                                            Address += addr + string.Format(" ({0}); ", temp_freq);
                                        }
                                        else if (temp_freq.Length > 0) { Address += string.Format(" ({0}); ", temp_freq); }
                                    }
                                }
                            }
                            else if (isResize == true) {
                                    
                                    bool isFindDozv = false;
                                    bool isFindVisn = false;
                                    bool isFindFreq = false;

                                   
                                    string temp_dozv = "";
                                    foreach (KeyValuePair<string, string> s in L_F_Dozv)
                                    {
                                        if (!string.IsNullOrEmpty(s.Key)) {
                                            if (!isFindDozv) { temp_dozv += "дозвіл на експлуатацію № "; isFindDozv = true; }
                                            temp_dozv += s.Key + (isFindDozv ? ", " : "");
                                        }
                                    }
                                    if (temp_dozv.Length > 0) { temp_dozv = temp_dozv.Remove(temp_dozv.Length - 2, 2); }
                                    if (temp_dozv.Length > 0) { Address += string.Format(" ({0}); ", temp_dozv); }


                                    
                                    string temp_visn = "";
                                    foreach (KeyValuePair<string, string> s in L_F_Visn)
                                    {
                                          if (!string.IsNullOrEmpty(s.Key))
                                          {
                                              if (!isFindVisn) { temp_visn += "висновок щодо ЕМС № "; isFindVisn = true; }
                                              temp_visn += s.Key + (isFindVisn ? ", " : "");
                                          }
                                    }
                                    if (temp_visn.Length > 0) { temp_visn = temp_visn.Remove(temp_visn.Length - 2, 2); }
                                    if (temp_visn.Length > 0) { Address += string.Format(" ({0}); ", temp_visn); }
                                    
                                   var_extend_ = "";
                                        foreach (Station item in Distinct_List_Stations) {
                                            if (item.NameOwner == ir) {
                                                KeyValuePair<string, string> s_d = L_F_Dozv.Find(r => r.Key == item.Num_Dozv);
                                                KeyValuePair<string, string> s_v = L_F_Visn.Find(r => r.Key == item.Num_Visn);
                                                if (((s_d.Key == "") || (s_d.Key == null)) && ((s_v.Key == "") || (s_v.Key == null))) {
                                                    if (!string.IsNullOrEmpty(item.Nom_Freq)) {
                                                        var_extend_ = " номінал радіочастоти " + item.Nom_Freq + " МГц";
                                                        Address += item.AddressOwner + string.Format(" ({0}); ", var_extend_);
                                                    }
                                                }
                                            }
                                        }
                                    
                                   
                                
                            }

                                if (Address.Length > 0) {
                                    Address = Address.Remove(Address.Length - 1, 1);
                                    if (isResize == false)
                                        Out_Val_REZ += " РЕЗ " + ir + ", який розташований за адресою: " + Address;
                                    else Out_Val_REZ += " РЕЗ " + ir + " " + Address;
                                }
                            
                        }

                        foreach (Station item in Distinct_List_Stations)
                        {
                            if (item.Table_ != "MICROWA")
                            {
                                //var1 = "з РЕЗ " + item.NameOwner + ", який запланований за адресою:  " + item.AddressOwner;
                            }
                            else
                            {
                                if (isResize == false)
                                    var_RRZ1 = "з боку РРС " + item.NameOwner + ", що розташована за адресою:  " + item.AddressOwner;
                                else var_RRZ1 = "з боку РРС " + item.NameOwner;

                                if (isResize == false)
                                {
                                    if ((item.NameOwner2 != "") && (item.AddressOwner2 != ""))
                                    {
                                        var_RRZ2 = " РРС " + item.NameOwner2 + ", яка розташована за адресою:  " + item.AddressOwner2;
                                    }
                                }
                                else
                                {
                                    if (item.NameOwner2 != "")
                                    {
                                        var_RRZ2 = " РРС " + item.NameOwner2;
                                    }
                                }
                                var1 = string.Format(" {0},{1} ", var_RRZ1, var_RRZ2);
                            }
                            if (!string.IsNullOrEmpty(item.Num_Dozv))
                                var_extend = "дозвіл на експлуатацію № " + item.Num_Dozv;
                            else if (!string.IsNullOrEmpty(item.Num_Visn))
                                var_extend = "висновок щодо ЕМС № " + item.Num_Visn;
                            else if (!string.IsNullOrEmpty(item.Nom_Freq))
                                var_extend = "номінал радіочастоти " + item.Nom_Freq + " МГц";

                            if (item.Table_ != "MICROWA")
                            {
                                var2 += item.NameOwner + ", ";
                            }
                            else var2 += item.NameOwner2 + ", ";

                            if (item.Table_ == "MICROWA")
                                Frg4_loop += string.Format("{0} ({1})", var1, var_extend) + "; ";

                            var_filial = item.NameFilial != "ДРЗП УДЦР УДЦР" ? item.NameFilial : "УДЦР";
                            TABLE_NAME = item.Table_;
                        }

                        if (TABLE_NAME == "MICROWA")
                        {
                            if (Frg4_loop.Length > 0) Frg4_loop = Frg4_loop.Remove(Frg4_loop.Length - 2, 2);
                        }
                        else
                        {
                            if (Out_Val_REZ.Length > 0) Frg4_loop = "з" + Out_Val_REZ.Remove(Out_Val_REZ.Length - 1, 1);
                        }


                        if (var2.Length > 0) var2 = var2.Remove(var2.Length - 2, 2);

                        string Frg1 = "";
                        if (TABLE_NAME != "MICROWA") Frg1 = "1. Аналіз розрахунків електромагнітної сумісності (ЕМС) запланованих частотних присвоєнь вищезгаданого РЕЗ з частотними присвоєннями іншим РЕЗ свідчить про можливість створення взаємних радіозавад " + Frg4_loop + "." + Environment.NewLine;
                        else Frg1 = "1. Аналіз розрахунків електромагнітної сумісності (ЕМС) запланованих частотних присвоєнь вищезгаданого РЕЗ з частотними присвоєннями іншим РЕЗ свідчить про можливість створення взаємних радіозавад " + Frg4_loop + "." + Environment.NewLine;

                        string Frg5 = "2. Термін дії висновку становить шість місяців." + Environment.NewLine;
                        string Frg6 = "                                                                                      ОСОБЛИВІ УМОВИ" + Environment.NewLine;
                        string Frg7 = "1. Можливість здійснення запланованих частотних присвоєнь може бути визначена за результатами натурних випробувань (НВ)." + Environment.NewLine;
                        string Frg8 = "2. НВ проводяться Заявником із залученням фахівців вищезазначених підприємств";
                        //string Frg9 = string.Format("{0}", var2);
                        string Frg10 = string.Format(" та представників {0}.", var_filial) + Environment.NewLine;
                        string Frg11 = "3. Організація та проведення НВ здійснюється відповідно до Порядку проведення приймальних випробувань РЕЗ та випромінювальних пристроїв на місці експлуатації (затверджений рішенням НКРЗ від 26.07.07 №854)." + Environment.NewLine;
                        string Frg12 = "4. За результатами НВ Заявником складається відповідний протокол." + Environment.NewLine;
                        string Frg13 = "5. Рішення про видачу дозволу на експлуатацію РЕЗ приймається УДЦР на підставі позитивних результатів НВ." + Environment.NewLine;
                        string Out_Str = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", Frg1, Frg5, Frg6, Frg7, Frg8, Frg10, Frg11, Frg12, Frg13);
                        ///////////
                        foreach (KeyValuePair<string, string> val in value_except) {
                            while (true) {
                                if (Out_Str.Replace(" ", "").ToLower().Contains(val.Key.Replace(" ", "").ToLower())) {
                                    int ind = Out_Str.ToLower().IndexOf(val.Key.ToLower());
                                    if (ind > 0) {
                                        Out_Str = Out_Str.Remove(ind, val.Key.Length);
                                        Out_Str = Out_Str.Insert(ind, val.Value);
                                    }
                                    else break;
                                }
                                else break;
                            }
                        }
                        /////////

                        if (isResize == false)
                        {
                            if (Out_Str.Length >= 2000) { isResize = true; if (!isStopped) { OnGenerateBagStationAnother(context); } }
                        }
                        else
                        {
                            if (Out_Str.Length >= 2000) { MessageBox.Show("Текст занадто великий і не може бути збережений!"); isResize = false; curStation = null; return false; }
                        }



                        if ((curStation != null) && (isStopped==false))
                        {
                            YXnrfaAppl rs = new YXnrfaAppl();
                            {
                                rs.Format("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE,DOZV_NUM,DOZV_NUM_NEW");
                                if (rs.Fetch(string.Format("([OBJ_ID1]={0} OR [OBJ_ID2]={0} OR [OBJ_ID3]={0}  OR [OBJ_ID4]={0}   OR [OBJ_ID5]={0}  OR [OBJ_ID6]={0}) AND ([OBJ_TABLE]={1})", curStation._tableID, curStation._table.ToSql())))
                                {
                                    using (BaseAppClass obj = BaseAppClass.GetAppl(rs.m_id, true))
                                    {
                                        CustomDialogBoxOU.CurrentPress = DialogResultMessageBoxButtons.Override;
                                        DialogResultMessageBoxButtons tpxc = DialogResultMessageBoxButtons.Override;
                                        if (obj != null)
                                        {
                                            appt = obj.appType;
                                            try
                                            {
                                                if (obj.SCVisnovok != null)
                                                {
                                                    if (!string.IsNullOrEmpty(obj.SCVisnovok))
                                                    {
                                                        CustomDialogBoxOU OU_dialog = new CustomDialogBoxOU(obj.SCVisnovok, Out_Str, "Формування особливих умов висновку щодо ЕМС");
                                                        tpxc = CustomDialogBoxOU.CurrentPress;
                                                    }
                                                    if (tpxc == DialogResultMessageBoxButtons.Append)
                                                    {
                                                        if (MainAppForm.isShowDialog)
                                                        {
                                                            if (obj.SCVisnovok.Length + Out_Str.Length >= 2000) { MessageBox.Show("Текст занадто великий і не може бути збережений!"); isResize = false; curStation = null; return false; }
                                                            CallBackFunction.callbackEventHandler(obj.SCVisnovok = obj.SCVisnovok + (obj.SCVisnovok.Length > 0 ? Environment.NewLine : "") + Out_Str);
                                                            isResize = false;
                                                            isSuccessSave = true;
                                                        }
                                                        else
                                                        {
                                                            obj.SCVisnovok = obj.SCVisnovok + (obj.SCVisnovok.Length > 0 ? Environment.NewLine : "") + Out_Str;
                                                            using (MainAppForm formAppl = new MainAppForm(obj))
                                                            {
                                                                formAppl.SaveToBase();
                                                                isResize = false;
                                                                isSuccessSave = true;
                                                            }
                                                        }
                                                    }
                                                    else if (tpxc == DialogResultMessageBoxButtons.Override)
                                                    {
                                                        if (MainAppForm.isShowDialog)
                                                        {
                                                            CallBackFunction.callbackEventHandler(obj.SCVisnovok = Out_Str); isResize = false; isSuccessSave = true;
                                                        }
                                                        else
                                                        {
                                                            obj.SCVisnovok = Out_Str;
                                                            using (MainAppForm formAppl = new MainAppForm(obj)) { formAppl.SaveToBase(); isResize = false; isSuccessSave = true; }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isResize = false;
                                                        isStopped = true;
                                                    }
                                                }
                                                else { }
                                            }
                                            catch (Exception exx)
                                            {

                                            }
                                        }
                                    }
                                }
                                rs.Close();
                            }
                        }
                    }
                    if (isSuccessSave)
                    {
                        if (curStation != null)
                            CJournal.CreateReport(curStation._tableID, appt);
                        curStation = null; List_Stations_Another.Clear();
                        isResize = false;
                        MessageBox.Show("ОУ сформовані успішно!");
                    }
                }
                else { if (List_Stations_Another.Count == 0) MessageBox.Show("Буфер з переліком станцій пустий!"); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
            return false;
        }

        private bool OnClearBagStation(IMQueryMenuNode.Context context)
        {
            DialogResult DA = MessageBox.Show("Виконати очищення проміжного буферу?", "", MessageBoxButtons.YesNo);
            if (DA == DialogResult.Yes)
            {
                List_Stations.Clear();
                List_Stations_Another.Clear();
                List_Stations_Microwa.Clear();
                MessageBox.Show("Проміжний буфер з даними ОУ успішно очищено!");
            }
            return false;
        }


        private bool ProcessBagStationAnother(IMQueryMenuNode.Context context)
        {
            ProcessBagStation(context, 0);
            return false;
        }

        private bool ProcessBagStationMicrowaGertva(IMQueryMenuNode.Context context)
        {
            ProcessBagStation(context, 0);
            return false;
        }

        private bool ProcessBagStationMicrowaZavada(IMQueryMenuNode.Context context)
        {
            ProcessBagStation(context, 1);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="Num"></param>
        private void ProcessBagStation(IMQueryMenuNode.Context context, int Num)
        {
            try
            {
                List<int> IDs_Station = new List<int>();
                if (curStation == null) { List_Stations.Clear(); List_Stations_Another.Clear(); List_Stations_Microwa.Clear(); }
                if (curStation != null)
                {
                    DialogResult DA = DialogResult.Yes;
                    if (List_Stations.Count > 0) DA = MessageBox.Show("В проміжному буфері вже є дані про станції. Додати обрані нові?", "", MessageBoxButtons.YesNo);
                    if (DA == DialogResult.Yes)
                    {
                        List<ExtendProps> L_prpos = new List<ExtendProps>();
                        string AllQuery = "";
                        IMRecordset rs_m_station = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                        rs_m_station.Select("ID");
                        rs_m_station.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                        for (rs_m_station.Open(); !rs_m_station.IsEOF(); rs_m_station.MoveNext())
                        {
                            string Query = string.Format(" ([OBJ_ID1]={0} OR [OBJ_ID2]={0} OR [OBJ_ID3]={0}  OR [OBJ_ID4]={0}  OR [OBJ_ID5]={0}  OR [OBJ_ID6]={0} AND [OBJ_TABLE]='{1}') ", rs_m_station.GetI("ID"), context.TableName);
                            AllQuery += Query + " OR ";
                        }
                        rs_m_station.Close();
                        rs_m_station.Destroy();
                        if (AllQuery.Length > 0)
                        {
                            AllQuery = AllQuery.Remove(AllQuery.Length - 4, 4);
                            using (YXnrfaAppl appl = new YXnrfaAppl())
                            {
                                appl.Format("*,MobileSector1(*,Position(*)),BaseSector1(*,Position(*)),Microwave(*,StationA(*,Position(*))),EarthStation(*,Position(*))");
                                appl.Filter = AllQuery;
                                for (appl.OpenRs(); !appl.IsEOF(); appl.MoveNext())
                                {
                                    ExtendProps prpos = new ExtendProps();
                                    prpos.obj_id1 = appl.m_obj_id1;
                                    prpos.obj_id2 = appl.m_obj_id2;
                                    prpos.obj_id3 = appl.m_obj_id3;
                                    prpos.obj_id4 = appl.m_obj_id4;
                                    prpos.obj_id5 = appl.m_obj_id5;
                                    prpos.obj_id6 = appl.m_obj_id6;
                                    prpos.obj_table = appl.m_obj_table;

                                    if (!string.IsNullOrEmpty(appl.m_dozv_num))
                                    {
                                        prpos.Dozv_Start = appl.m_dozv_date_from;
                                        prpos.NumDzv = appl.m_dozv_num;
                                    }
                                    else if (!string.IsNullOrEmpty(appl.m_conc_num))
                                    {
                                        prpos.Dozv_Start = appl.m_conc_date_from;
                                        prpos.NumVisn = appl.m_conc_num;
                                    }

                                    switch (context.TableName)
                                    {
                                        case "MOB_STATION":
                                            prpos.CurrFieldProvince = appl.m_MobileSector1.m_Position.m_province;
                                            break;
                                        case "MOB_STATION2":
                                            prpos.CurrFieldProvince = appl.m_BaseSector1.m_Position.m_province;
                                            break;
                                        case "MICROWA":
                                            prpos.CurrFieldProvince = appl.m_Microwave.m_StationA.m_Position.m_province;
                                            break;
                                        case "EARTH_STATION":
                                            prpos.CurrFieldProvince = appl.m_EarthStation.m_Position.m_province;
                                            break;
                                    }
                                    switch (prpos.CurrFieldProvince)
                                    {

                                        case "АР Крим":
                                            prpos.OutStr = "Кримська філія";
                                            break;
                                        case "Вінницька":
                                        case "Житомирська":
                                        case "Хмельницька":
                                            prpos.OutStr = "Подільська філія";
                                            break;
                                        case "Закарпатська":
                                        case "Івано-Франківська":
                                        case "Чернівецька":
                                            prpos.OutStr = "Карпатська філія";
                                            break;
                                        case "Київ":
                                        case "Київська":
                                            prpos.OutStr = "ДРЗП УДЦР";
                                            break;
                                        case "Черкаська":
                                        case "Кіровоградська":
                                        case "Дніпропетровська":
                                        case "Запорізька":
                                        case "Донецька":
                                            prpos.OutStr = "Центральна філія";
                                            break;
                                        case "Миколаївська":
                                        case "Одеська":
                                        case "Херсонська":
                                            prpos.OutStr = "Південна філія";
                                            break;
                                        case "Рівненська":
                                        case "Тернопільська":
                                        case "Львівська":
                                        case "Волинська":
                                            prpos.OutStr = "Західна філія";
                                            break;
                                        case "Сумська":
                                        case "Чернігівська":
                                        case "Полтавська":
                                        case "Харківська":
                                        case "Луганська":
                                            prpos.OutStr = "Північно-Східна філія";
                                            break;
                                    }
                                    L_prpos.Add(prpos);
                                }
                                appl.Close();
                            }
                        }





                        int ID_BAG = IM.NullI;
                        if (context.TableName == "MICROWA")
                        {
                            List<RecordPtr> rc_list = listData.FindAll(t => t.Table == "BAG_MICROWA"); List<int> L_bag_microwa = new List<int>();
                            foreach (RecordPtr st in rc_list)
                            {
                                L_bag_microwa.Add(st.Id);
                            }
                            L_bag_microwa.Sort(); if (L_bag_microwa.Count > 0) ID_BAG = L_bag_microwa[Num];
                        }

                        //List<int> Distinct_Station = new List<int>();
                        IMRecordset rs_m = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            if ((context.TableName == "MOB_STATION2") || (context.TableName == "MOB_STATION") || (context.TableName == "EARTH_STATION") || (context.TableName == "MICROWA"))
                                rs_m.Select("ID,STANDARD,Owner.NAME");
                            if ((context.TableName == "MOB_STATION2") || (context.TableName == "MOB_STATION") || (context.TableName == "EARTH_STATION"))
                                rs_m.Select("Position.REMARK,Position.PROVINCE");
                            if ((context.TableName == "MOB_STATION2") || (context.TableName == "MOB_STATION"))
                                rs_m.Select("AssignedFrequencies.TX_FREQ");
                            if (context.TableName == "MICROWA")
                                rs_m.Select("ID,StationA.TX_FREQ,StationB.TX_FREQ,StationA.TX_FREQ,StationA.ID,StationB.ID,Owner.NAME");


                            rs_m.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                            //rs_m.OrderBy("BagData.Bag.ID", OrderDirection.Descending);
                            for (rs_m.Open(); !rs_m.IsEOF(); rs_m.MoveNext())
                            {
                                //if (!Distinct_Station.Contains(rs_m.GetI("ID")))
                                //Distinct_Station.Add(rs_m.GetI("ID"));
                                //else continue;

                                bool isAdded = false;
                                int ID_Station = rs_m.GetI("ID");
                                //int Bag_ID = rs_m.GetI("BagData.Bag.ID");
                                double FREQ_SACRIFICE = IM.NullD;
                                double FREQ_NOISE = IM.NullD;

                                string NumDzv = ""; string NumVisn = ""; DateTime Dozv_Start = IM.NullT;
                                string OutStr = ""; string CurrFieldProvince = "";

                                ExtendProps ex_find = L_prpos.Find(r => (r.obj_id1 == rs_m.GetI("ID") || r.obj_id2 == rs_m.GetI("ID") || r.obj_id3 == rs_m.GetI("ID") || r.obj_id4 == rs_m.GetI("ID") || r.obj_id5 == rs_m.GetI("ID") || r.obj_id6 == rs_m.GetI("ID")) && r.obj_table == context.TableName);
                                if (ex_find == null)
                                    continue;
                                else
                                {
                                    NumDzv = ex_find.NumDzv;
                                    NumVisn = ex_find.NumVisn;
                                    Dozv_Start = ex_find.Dozv_Start;
                                    OutStr = ex_find.OutStr;
                                    CurrFieldProvince = ex_find.CurrFieldProvince;
                                }

                                using (IMRecordset rs_bg_mob = new IMRecordset("BG_" + context.TableName, IMRecordset.Mode.ReadOnly))
                                {
                                    rs_bg_mob.Select("BAG_ID,OBJ_ID,FREQ_SACRIFICE,FREQ_NOISE");
                                    rs_bg_mob.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID_Station);
                                    if (context.TableName == "MICROWA") rs_bg_mob.SetWhere("BAG_ID", IMRecordset.Operation.Eq, ID_BAG);
                                    //rs_bg_mob.SetWhere("BAG_ID", IMRecordset.Operation.Eq, Bag_ID);
                                    for (rs_bg_mob.Open(); !rs_bg_mob.IsEOF(); rs_bg_mob.MoveNext())
                                    {
                                        Station rxc = new Station();
                                        rxc.ID_ = rs_m.GetI("ID");
                                        rxc.Table_ = context.TableName;
                                        rxc.Num_Dozv = NumDzv;
                                        rxc.Dozv_Start = Dozv_Start;
                                        if ((context.TableName == "MOB_STATION2") || (context.TableName == "MOB_STATION") || (context.TableName == "EARTH_STATION"))
                                        {
                                            rxc.NameOwner = rs_m.GetS("Owner.NAME");
                                            string Address = ""; IM.ExecuteScalar(ref Address, string.Format("(SELECT ICSM.ADDRESS_FULL('{0}',id) from %{1} s where s.id={2})", context.TableName, context.TableName, rs_m.GetI("ID")));
                                            rxc.AddressOwner = Address;
                                            //rxc.AddressOwner = rs_m.GetS("Position.REMARK");
                                        }
                                        if ((context.TableName == "MOB_STATION2") || (context.TableName == "MOB_STATION"))
                                            rxc.Nom_Freq = rs_m.GetD("AssignedFrequencies.TX_FREQ") != IM.NullD ? rs_m.GetD("AssignedFrequencies.TX_FREQ").ToString() : "";
                                        if (context.TableName == "MICROWA")
                                        {
                                            // Для жертв
                                            if (Num == 0) {
                                                FREQ_SACRIFICE = rs_bg_mob.GetD("FREQ_SACRIFICE");
                                                FREQ_NOISE = rs_bg_mob.GetD("FREQ_NOISE");
                                                rxc.NameOwner2 = rs_m.GetS("Owner.NAME");
                                                int StationNum = -1;
                                                // частота жертвы
                                                if (FREQ_NOISE == rs_m.GetD("StationA.TX_FREQ")) { rxc.Nom_Freq = rs_m.GetD("StationB.TX_FREQ") != IM.NullD ? rs_m.GetD("StationB.TX_FREQ").ToString() : ""; StationNum = rs_m.GetI("StationB.ID"); }
                                                // частота жертвы
                                                if (FREQ_NOISE == rs_m.GetD("StationB.TX_FREQ")) { rxc.Nom_Freq = rs_m.GetD("StationA.TX_FREQ") != IM.NullD ? rs_m.GetD("StationA.TX_FREQ").ToString() : ""; StationNum = rs_m.GetI("StationA.ID"); }
                                                ///
                                                string Address2 = ""; IM.ExecuteScalar(ref Address2, string.Format("(SELECT ICSM.ADDRESS_FULL('{0}',id) from %{1} s where s.id={2})", "MICROWS", "MICROWS", StationNum));
                                                rxc.AddressOwner2 = Address2;

                                                ////



                                                IMRecordset rs_microwa = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
                                                rs_microwa.Select("ID,Owner.NAME,StationA.ID,StationB.ID,StationA.TX_FREQ,StationB.TX_FREQ");
                                                rs_microwa.SetWhere("ID", IMRecordset.Operation.Eq, curStation._tableID);
                                                for (rs_microwa.Open(); !rs_microwa.IsEOF(); rs_microwa.MoveNext())
                                                {

                                                    int NUM_ID_STATION = IM.NullI;
                                                    // частота завады
                                                    if (FREQ_SACRIFICE == rs_microwa.GetD("StationA.TX_FREQ")) { NUM_ID_STATION = 1; }
                                                    // частота завады
                                                    if (FREQ_SACRIFICE == rs_microwa.GetD("StationB.TX_FREQ")) { NUM_ID_STATION = 2; }


                                                    rxc.NameOwner = rs_microwa.GetS("Owner.NAME");
                                                    string Address = ""; IM.ExecuteScalar(ref Address, string.Format("(SELECT ICSM.ADDRESS_FULL('{0}',id) from %{1} s where s.id={2})", "MICROWS", "MICROWS", NUM_ID_STATION == 1 ? rs_microwa.GetI("StationA.ID") : rs_microwa.GetI("StationB.ID")));
                                                    rxc.AddressOwner = Address;
                                                    break;
                                                }
                                                rs_microwa.Close();
                                                rs_microwa.Destroy();
                                            }
                                            //Для завад
                                            if (Num == 1)
                                            {
                                                FREQ_SACRIFICE = rs_bg_mob.GetD("FREQ_SACRIFICE");
                                                FREQ_NOISE = rs_bg_mob.GetD("FREQ_NOISE");
                                                rxc.NameOwner2 = rs_m.GetS("Owner.NAME");
                                                int StationNum = -1;
                                                int NUM_ID_STATION = IM.NullI;
                                                // частота жертвы
                                                if (FREQ_SACRIFICE == rs_m.GetD("StationA.TX_FREQ")) { NUM_ID_STATION = 1; }
                                                // частота жертвы
                                                if (FREQ_SACRIFICE == rs_m.GetD("StationB.TX_FREQ")) { NUM_ID_STATION = 2; }
                                                ///
                                                string Address2 = ""; IM.ExecuteScalar(ref Address2, string.Format("(SELECT ICSM.ADDRESS_FULL('{0}',id) from %{1} s where s.id={2})", "MICROWS", "MICROWS", NUM_ID_STATION ==1 ? rs_m.GetI("StationA.ID") : rs_m.GetI("StationB.ID")));
                                                rxc.AddressOwner2 = Address2;

                                                ////



                                                IMRecordset rs_microwa = new IMRecordset("MICROWA", IMRecordset.Mode.ReadOnly);
                                                rs_microwa.Select("ID,Owner.NAME,StationA.ID,StationB.ID,StationA.TX_FREQ,StationB.TX_FREQ");
                                                rs_microwa.SetWhere("ID", IMRecordset.Operation.Eq, curStation._tableID);
                                                for (rs_microwa.Open(); !rs_microwa.IsEOF(); rs_microwa.MoveNext())
                                                {

                                                    
                                                    // частота завады
                                                    if (FREQ_NOISE == rs_microwa.GetD("StationA.TX_FREQ")) { rxc.Nom_Freq = rs_m.GetD("StationB.TX_FREQ") != IM.NullD ? rs_m.GetD("StationB.TX_FREQ").ToString() : ""; StationNum = rs_microwa.GetI("StationB.ID"); } 
                                                    // частота завады
                                                    if (FREQ_NOISE == rs_microwa.GetD("StationB.TX_FREQ")) { rxc.Nom_Freq = rs_m.GetD("StationA.TX_FREQ") != IM.NullD ? rs_m.GetD("StationA.TX_FREQ").ToString() : ""; StationNum = rs_microwa.GetI("StationA.ID"); }

                                                    rxc.NameOwner = rs_microwa.GetS("Owner.NAME");
                                                    string Address = ""; IM.ExecuteScalar(ref Address, string.Format("(SELECT ICSM.ADDRESS_FULL('{0}',id) from %{1} s where s.id={2})", "MICROWS", "MICROWS", StationNum));
                                                    rxc.AddressOwner = Address;
                                                    break;
                                                }
                                                rs_microwa.Close();
                                                rs_microwa.Destroy();
                                            }
                                        }

                                        if (context.TableName == "EARTH_STATION")
                                        {
                                            StationEarthStation EA = new StationEarthStation();
                                            EA.Load(rs_m.GetI("ID"));
                                            if (EA.nomFreqTx.Count > 0)
                                            {
                                                rxc.Nom_Freq = EA.nomFreqTx[0] != IM.NullD ? EA.nomFreqTx[0].ToString() : "";
                                            }
                                        }

                                        rxc.Num_Visn = NumVisn;
                                        rxc.NameFilial = OutStr + " УДЦР";
                                        Station st_find = List_Stations.Find(r => r.ID_ == rs_m.GetI("ID"));
                                        if (context.TableName == "MICROWA")
                                        {
                                            if ((st_find == null) && (rxc.NameOwner2 != null) && (rxc.AddressOwner2 != null) && (rxc.NameOwner != null) && (rxc.AddressOwner != null))
                                            {
                                                isAdded = true;
                                                List_Stations.Add(rxc);
                                                break;
                                            }
                                            else
                                            {
                                                if (!IDs_Station.Contains(ID_Station))
                                                {
                                                    //if (st_find != null)
                                                    //{
                                                    //MessageBox.Show(string.Format("Станція ID - {0} вже додана в буфер!", ID_Station));
                                                    //}
                                                    if ((rxc.NameOwner2 == null) || (rxc.AddressOwner2 == null) || (rxc.NameOwner == null) || (rxc.AddressOwner == null))
                                                    {
                                                        string Formta = string.Format("Станція ID - {0} (BAG_ID={1}) не додана, оскільки одне з полів пусте ", ID_Station, ID_BAG) + Environment.NewLine;
                                                        if (rxc.NameOwner != null)
                                                            Formta += string.Format("Станція ID - {0}, NameOwner = {1}", ID_Station, rxc.NameOwner) + Environment.NewLine;
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, NameOwner = ''", ID_Station) + Environment.NewLine;

                                                        if (rxc.AddressOwner != null)
                                                            Formta += string.Format("Станція ID - {0}, AddressOwner = {1}", ID_Station, rxc.AddressOwner) + Environment.NewLine;
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, AddressOwner = ''", ID_Station) + Environment.NewLine;

                                                        if (rxc.NameOwner2 != null)
                                                            Formta += string.Format("Станція ID - {0}, NameOwner2 = {1}", ID_Station, rxc.NameOwner2) + Environment.NewLine;
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, NameOwner2 = ''", ID_Station) + Environment.NewLine;

                                                        if (rxc.AddressOwner2 != null)
                                                            Formta += string.Format("Станція ID - {0}, AddressOwner2 = {1}", ID_Station, rxc.AddressOwner2) + Environment.NewLine;
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, AddressOwner2 = ''", ID_Station) + Environment.NewLine;


                                                        if (FREQ_SACRIFICE != IM.NullD)
                                                            Formta += string.Format("Станція ID - {0}, FREQ_SACRIFICE = {1}", ID_Station, FREQ_SACRIFICE) + Environment.NewLine;
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, FREQ_SACRIFICE = ''}", ID_Station) + Environment.NewLine;



                                                        if (FREQ_NOISE != IM.NullD)
                                                            Formta += string.Format("Станція ID - {0}, FREQ_NOISE = {1}", ID_Station, FREQ_NOISE);
                                                        else
                                                            Formta += string.Format("Станція ID - {0}, FREQ_NOISE = ''", ID_Station);
                                                        MessageBox.Show(Formta);

                                                        IDs_Station.Add(ID_Station);
                                                        break;
                                                    }
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (st_find == null)
                                            {
                                                isAdded = true;
                                                List_Stations.Add(rxc);
                                            }
                                        }

                                        //}
                                    }
                                    rs_bg_mob.Close();
                                }
                                //break;
                            }
                        }
                        finally
                        {
                            rs_m.Destroy();
                        }
                    }
                }
                else MessageBox.Show("Відсутня дослідна станція!");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }


        private bool OnEMSProcess(IMQueryMenuNode.Context context)
        {
            FormEMS formEms = new FormEMS(context.TableName, context.TableId);
            formEms.ShowDialog();
            formEms.Dispose();
            return false;
        }
        //===================================================
        /// <summary>
        /// Создаем и редактируем шаблоны особливих умов
        /// </summary>
        private bool OnAddEditPattern(IMQueryMenuNode.Context context)
        {
            FSCPattern formPattern = new FSCPattern(context.TableId);
            formPattern.ShowDialog();
            formPattern.Dispose();
            return false;
        }

        private bool OnVRRProcess(IMQueryMenuNode.Context context)
        {
            FormVRR formVRR = new FormVRR(context.TableName, context.TableId);
            formVRR.ShowDialog();
            formVRR.Dispose();
            return false;
        }
        //==================================================
        /// <summary>
        /// Отобразить станцию на карте
        /// </summary>
        /// <param name="_table">имя таблицы</param>
        /// <param name="_id">ID записи</param>
        private void ShowOnMap(string _table, int _id, bool withCountor)
        {
            var tx = new TxOnMap(_table, _id);
            tx.IsCalculation = withCountor;
            TxsOnMap.AddTx(tx);
        }
        //==================================================
        /// <summary>
        /// Обработчик события "Отобразить на карте"
        /// </summary>
        private bool OnMapShow(IMQueryMenuNode.Context context, bool withContour)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Adding stations on the map...")))
            {
                IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                rs.Select("ID");
                rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                try
                {
                    int index = 0;
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int tableId = rs.GetI("ID");
                        string tableName = context.TableName;
                        pb.SetBig(string.Format("{0}:{1}", tableName, tableId));
                        pb.SetSmall(index++);
                        ShowOnMap(tableName, tableId, withContour);
                    }
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }
                pb.SetBig("Загрузка карти...");
                TxsOnMap.ShowMap();
            }
            return false;
        }
        //==================================================
        /// <summary>
        /// Обработчик события "Отобразить на карте"
        /// </summary>
        private bool OnMapShowAllStation(IMQueryMenuNode.Context context, bool withCountor)
        {
            using (LisProgressBar pb = new LisProgressBar("Загрузка станцій..."))
            {
                int index = 0;
                IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                rs.Select("ID,TABLE_NAME,TABLE_ID");
                rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                try
                {
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int tableId = rs.GetI("TABLE_ID");
                        string tableName = rs.GetS("TABLE_NAME");
                        pb.SetBig(string.Format("{0}:{1}", tableName, tableId));
                        pb.SetSmall(index++);
                        ShowOnMap(tableName, tableId, withCountor);
                    }
                }
                finally
                {
                    rs.Final();
                }
                pb.SetBig("Загрузка карти...");
                TxsOnMap.ShowMap();
                return false;
            }
        }

        private bool OnExportToFileAllStation(IMQueryMenuNode.Context context)
        {

            AllStationsCsvExport _export = new AllStationsCsvExport();

            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Exporting stations to file...")))
            {
                pBar.ShowBig(CLocaliz.TxT("Exporting..."));
                pBar.UserCanceled();


                _export.Export(context, pBar);
                /*
                try
                {
                    List<int> ids = new List<int>();
                    //int index = 0;                                        
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int id = rs.GetI("ID");
                        ids.Add(id);
                        //_export.Export(id);
                        //ShowOnMap(context.TableName, );

                        if (ids.Count >= AllStationsCsvExport.Max)
                        {
                            _export.Export(ids);
                            ids.Clear();
                        }

                        pBar.ShowSmall(id);
                        pBar.UserCanceled();
                    }     
                    if (ids.Count>0)             
                        _export.Export(ids);
                }
                finally
                {
                    rs.Close();
                    rs.Destroy();
                }*/
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Общая функция для открытия окна записи плагиновских таблиц
        /// </summary>
        private bool OnNewEditContextMenu(IMQueryMenuNode.Context context)
        {
            RecordPtr recPtr;
            recPtr.Table = context.TableName;
            recPtr.Id = context.TableId;
            OnDblClickRecord(recPtr);
            return false;
        }
        //===================================================
        /// <summary>
        /// Shows VRS Calculation
        /// </summary>
        private bool OnVRSProcess(IMQueryMenuNode.Context context)
        {
            FormVRS formVrs = new FormVRS(context.TableId, context.TableName);
            formVrs.ShowDialog();
            formVrs.Dispose();
            return false;
        }
        //===================================================
        /// <summary>
        /// Adds or Edits price
        /// </summary>
        private bool OnAddEditPrice(IMQueryMenuNode.Context context)
        {
            FPrice form = new FPrice(context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
                IM.RefreshQueries(context.TableName);
            form.Dispose();
            return false;
        }
        //===================================================
        /// <summary>
        /// Displays the form with the log's data
        /// </summary>
        private bool OnShowLog(IMQueryMenuNode.Context context)
        {
            CLogs.ShowLog(context.TableId);
            return false;
        }
        //===================================================
        /// <summary>
        /// Adds or Edits owner of abonent station
        /// </summary>
        private bool OnAddEditOwner(IMQueryMenuNode.Context context)
        {
            FAbonentOwner form = new FAbonentOwner(context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
                IM.RefreshQueries(context.TableName);
            form.Dispose();
            return false;
        }
        //===================================================
        /// <summary>
        /// Adds or Edits owner of abonent station
        /// </summary>
        private bool OnAddEditSpecConditionR135(IMQueryMenuNode.Context context)
        {
            using (FrmSpecConditionR135 form = new FrmSpecConditionR135(context.TableId))
            {
                if (form.ShowDialog() == DialogResult.OK)
                    IM.RefreshQueries(context.TableName);
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Открывает окно заявки для вида
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnOpenApplFromStationView(IMQueryMenuNode.Context context)
        {
            GetTableFromView(ref context.TableName);
            ShowApplFormView(context.TableName, context.TableId);
            return false;
        }

        /// <summary>
        /// Получить имя таблицы из ее вида
        /// </summary>
        /// <param name="name"></param>
        private void GetTableFromView(ref string name)
        {
            switch (name)
            {
                case PlugTbl.XvAbonentStation:
                    name = PlugTbl.XfaAbonentStation;
                    break;
                case PlugTbl.XvAmateur:
                    name = PlugTbl.XfaAmateur;
                    break;
                case PlugTbl.XvAppl:
                    name = PlugTbl.APPL;
                    break;
                case PlugTbl.XvEmiStation:
                    name = PlugTbl.XfaEmitterStation;
                    break;                
                case PlugTbl.XvMsreqCert:
                    name = PlugTbl.itblXnrfaMeasureEquipmentCertificates;
                    break;
                case PlugTbl.XvPacket:
                    name = PlugTbl.itblXnrfaPacket;
                    break;
                case PlugTbl.XvShip:
                    name = PlugTbl.Ship;
                    break;
                default:                  
                    break;
            }
        }

        //===================================================
        /// <summary>
        /// Открывает окно заявки
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnOpenApplFromStation(IMQueryMenuNode.Context context)
        {
            GetTableFromView(ref context.TableName);
            MainAppForm.ShowApplForm(context.TableName, context.TableId, false);
            return false;
        }

        //===================================================
        /// <summary>
        /// Открывает окно измерительного оборудования
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnEditMeasureEquipment(IMQueryMenuNode.Context context)
        {
            MeasureEquipmentForm form = new MeasureEquipmentForm(context.TableName, context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                form.SaveAppl();
            }

            return false;
        }

        //===================================================
        /// <summary>
        /// Открывает окно сертификатов оборудования
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        ///
        private bool OnEditMeasureEquipmentCertificate(IMQueryMenuNode.Context context)
        {
            GetTableFromView(ref context.TableName);
            MeasureEquipmentCertificateForm form = new MeasureEquipmentCertificateForm(context.TableName, context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                form.SaveAppl();
            }

            return false;
        }


        //===================================================
        /// <summary>
        /// Открывает окно дополнительного оборудования
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnEditAuxiliaryEquipment(IMQueryMenuNode.Context context)
        {
            AuxiliaryEquipmentForm form = new AuxiliaryEquipmentForm(context.TableName, context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                form.SaveAppl();
            }

            return false;
        }

        /// <summary>
        /// Xfa-anten
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnNewEditXfaAnten(IMQueryMenuNode.Context context)
        {
            using (RailAntenForm frm = new RailAntenForm(context.TableId))
                if (frm.ShowDialog() == DialogResult.OK)
                    frm.Save();
            return true;
        }

        //Форма редагування аматорів
        private bool OnEditAmateur(IMQueryMenuNode.Context context)
        {
            if (!string.IsNullOrEmpty(context.TableName) && (context.TableId != 0))
                MainAppForm.ShowApplForm(context.TableName, context.TableId, false);

            return false;
        }        
        private bool OnEditAmateurView(IMQueryMenuNode.Context context)
        {
            context.TableName = PlugTbl.XfaAmateur;
            if (!string.IsNullOrEmpty(context.TableName) && (context.TableId != 0))
                ShowApplFormView(context.TableName, context.TableId);

            return false;
        }

        private bool OnEditAmateurBand(IMQueryMenuNode.Context context)
        {
            AmateurBandForm form = new AmateurBandForm(context.TableName, context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                form.SaveAppl();
            }
            return false;
        }

        /// <summary>
        /// Форма для редактирования взносов
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnEditFees(IMQueryMenuNode.Context context)
        {
            FeesForm form = new FeesForm(context.TableName, context.TableId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                form.SaveAppl();
            }
            return false;
        }

        //===================================================
        /// <summary>
        /// Открывает окно заявки из ALL_STATIONS
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnEditStationApplAllStation(IMQueryMenuNode.Context context)
        {
            string tableName = "";
            int tableID = 0;
            IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
            rs.Select("TABLE_NAME,TABLE_ID");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    tableID = rs.GetI("TABLE_ID");
                    tableName = rs.GetS("TABLE_NAME");
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }

            if (!string.IsNullOrEmpty(tableName) && (tableID != 0))
                MainAppForm.ShowApplForm(tableName, tableID, true);
            return false;
        }

        //===================================================
        /// <summary>
        /// Обработка двойного клика на записи
        /// </summary>
        /// <param name="recPtr">recordPtr на запись</param>
        private void OnDblClickRecord(RecordPtr recPtr)
        {
            IM.AdminDisconnect(); // elsewhere we get ICSM__ as user performing queries
            switch (recPtr.Table)
            {
                case "XNRFA_STREETS":
                    StreetChangeForm r_f = new StreetChangeForm(recPtr);
                    r_f.ShowDialog();
                    break;

                case PlugTbl.XfaNet:
                case PlugTbl.XvNet:
                    Net.NetForm.ShowForm(recPtr.Id);
                    break;
                case PlugTbl.RegistryPort:
                    RegistryPortForm.ShowForm(recPtr.Id);
                    break;
                case PlugTbl.XvEquipAmateur:
                case PlugTbl.XfaEquipAmateur:
                    {
                        using (AmateurEquipForm frm = new AmateurEquipForm(recPtr.Id))
                            if (frm.ShowDialog() == DialogResult.Yes)
                            {
                                frm.Save();
                                IM.RefreshQueries(PlugTbl.XvEquipAmateur); IM.RefreshQueries(PlugTbl.XfaEquipAmateur);
                            }
                    }
                    break;
                case PlugTbl.XnChangeLog:
                    {
                        int msgId = IM.NullI;
                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rs.Select("ID,MSG_ID");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                msgId = rs.GetI("MSG_ID");
                            }
                        }
                        finally
                        {
                            rs.Final();
                        }
                        if (msgId != IM.NullI)
                        {
                            RecordPtr rcdPtr = new RecordPtr(PlugTbl.XnChangeMsg, msgId);
                            OnDblClickRecord(rcdPtr);
                        }
                    }
                    break;
                case PlugTbl.XnChangeMsg:
                    ChangeMessageForm.Show(recPtr.Id);
                    break;
                case PlugTbl.MeasureEquipment:
                case PlugTbl.XvMeasureEq:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = recPtr.Table;
                        context.TableId = recPtr.Id;
                        OnEditMeasureEquipment(context);
                    }
                    break;
                case PlugTbl.AuxiliaryEquipment:
                case PlugTbl.XvAuxEq:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = recPtr.Table;
                        context.TableId = recPtr.Id;
                        OnEditAuxiliaryEquipment(context);
                    }
                    break;
                case PlugTbl.XvMsreqCert:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.itblXnrfaMeasureEquipmentCertificates;
                        context.TableId = recPtr.Id;
                        OnEditMeasureEquipmentCertificate(context);
                    }
                    break;
                case PlugTbl.itblXnrfaMeasureEquipmentCertificates:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.itblXnrfaMeasureEquipmentCertificates;
                        context.TableId = recPtr.Id;
                        OnEditMeasureEquipmentCertificate(context);
                    }
                    break;
                case PlugTbl.itblXnrfaFees:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = recPtr.Table;
                        context.TableId = recPtr.Id;
                        OnEditFees(context);
                    }
                    break;
                case PlugTbl.XvAmateur:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.XfaAmateur;
                        context.TableId = recPtr.Id;
                        OnEditAmateurView(context);
                    }
                    break;
                case PlugTbl.XfaAmateur:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.XfaAmateur;
                        context.TableId = recPtr.Id;
                        OnEditAmateur(context);
                    }
                    break;

                case PlugTbl.XfaAbonentStation:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.XfaAbonentStation;
                        context.TableId = recPtr.Id;
                        OnOpenApplFromStation(context);
                    }
                    break;
                case PlugTbl.XvAbonentStation:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.XfaAbonentStation;
                        context.TableId = recPtr.Id;
                        OnOpenApplFromStationView(context);
                    }
                    break;
                case PlugTbl.ABONENT_OWNER:
                case PlugTbl.XvAbonentOwner:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = PlugTbl.ABONENT_OWNER;
                        context.TableId = recPtr.Id;
                        OnAddEditOwner(context);
                    }
                    break;

                case PlugTbl.XfaBandAmateur:
                    {
                        IMQueryMenuNode.Context context = new IMQueryMenuNode.Context();
                        context.TableName = recPtr.Table;
                        context.TableId = recPtr.Id;
                        OnEditAmateurBand(context);
                    }
                    break;
                case PlugTbl.XvPacket:
                case PlugTbl.itblXnrfaPacket:
                    using (FPacket formPacket = new FPacket(recPtr.Id, recPtr.Table))
                    {
                        formPacket.ShowDialog(IcsmMainWindows);
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                    break;
                case PlugTbl.XvEmiStation:
                    ShowApplFormView(PlugTbl.XfaEmitterStation, recPtr.Id);
                    break;
                case PlugTbl.XvShip:
                    ShowApplFormView(PlugTbl.Ship, recPtr.Id);
                    break;
                case PlugTbl.XfaEmitterStation:
                    MainAppForm.ShowApplForm(PlugTbl.XfaEmitterStation, recPtr.Id, false);
                    break;
                case PlugTbl.XvFilEquip:
                    {
                        int objId = IM.NullI;
                        string objTable = "";
                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rs.Select("ID,EQUIP_ID,EQUIP_TABLE");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                objId = rs.GetI("EQUIP_ID");
                                objTable = rs.GetS("EQUIP_TABLE");
                            }
                        }
                        finally
                        {
                            rs.Final();
                        }
                        if ((objId != IM.NullI) && (string.IsNullOrEmpty(objTable) == false))
                        {
                            if (objTable == ICSMTbl.itblEquipPmr)
                            {
                                RecordPtr rcPtr = new RecordPtr(ICSMTbl.itblEquipPmr, objId);
                                rcPtr.UserEdit();
                            }
                            else if (objTable == PlugTbl.XvEquip || objTable == PlugTbl.XfaEquip)
                            {
                                using (AbonentEquipForm frm = new AbonentEquipForm(objId))
                                    if (frm.ShowDialog() == DialogResult.OK)
                                        frm.Save();
                            }
                            else if (objTable == PlugTbl.XfaEquipAmateur || objTable == PlugTbl.XvEquipAmateur)
                            {
                                using (AmateurEquipForm frm = new AmateurEquipForm(objId))
                                    if (frm.ShowDialog() == DialogResult.Yes)
                                        frm.Save();
                            }

                        }
                    }
                    break;
                case PlugTbl.XvFilialSite:
                    {
                        string objTable = PlugTbl.XfaPosition;
                        int objId = recPtr.Id;

                        //AdminSiteAllTech2.Show(objTable, objId, (IM.TableRight(objTable) & IMTableRight.Update) != IMTableRight.Update, IcsmMainWindows);

                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rs.Select("ID,TABLE_NAME");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                objId = rs.GetI("ID");
                                objTable = rs.GetS("TABLE_NAME");
                            }
                        }
                        finally
                        {
                            rs.Final();
                        }
                        if ((objId != IM.NullI) && (string.IsNullOrEmpty(objTable) == false))
                        {
                            AdminSiteAllTech2.Show(objTable, objId, (IM.TableRight(objTable) & IMTableRight.Update) != IMTableRight.Update, IcsmMainWindows);
                        }
                    }
                    break;
                case PlugTbl.XvSites:
                    {
                        AdminSiteAllTech2.Show(ICSMTbl.SITES, recPtr.Id, (IM.TableRight(ICSMTbl.SITES) & IMTableRight.Update) != IMTableRight.Update, IcsmMainWindows);
                    }
                    break;
                case PlugTbl.XvAllPosition:
                    {
                        int objId = IM.NullI;
                        string objTable = "";
                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rs.Select("ID,REF_ID,TABLE_NAME");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                objId = rs.GetI("REF_ID");
                                objTable = rs.GetS("TABLE_NAME");
                            }
                        }
                        finally
                        {
                            rs.Final();
                        }
                        if ((objId != IM.NullI) && (string.IsNullOrEmpty(objTable) == false))
                        {
                            AdminSiteAllTech2.ShowReadOnly(objTable, objId, IcsmMainWindows);
                        }
                    }
                    break;
                case PlugTbl.XvAppl:
                case PlugTbl.AllDecentral:
                case PlugTbl.APPL:
                case PlugTbl.APPL_LIC:
                    {
                        int applId = IM.NullI;
                        string table = recPtr.Table;
                        if (recPtr.Table == PlugTbl.APPL_LIC)
                        {
                            IMRecordset rs = IMRecordset.ForRead(recPtr, "APPL_ID");
                            try
                            {
                                if (!rs.IsEOF()) applId = rs.GetI("APPL_ID");
                            }
                            finally
                            {
                                rs.Destroy();
                            }
                            table = PlugTbl.APPL;
                        }
                        else
                            applId = recPtr.Id;
                        BaseAppClass obj = BaseAppClass.GetAppl(applId, false);
                        obj.SetRight(table);
                        using (MainAppForm formAppl = new MainAppForm(obj))
                        {
                            formAppl.ShowDialog();
                        }
                    }
                    break;
                case PlugTbl.AllStations:
                    {
                        string tableName = "";
                        int tableID = 0;
                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        rs.Select("ID,TABLE_NAME,TABLE_ID");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                        try
                        {
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                tableID = rs.GetI("TABLE_ID");
                                tableName = rs.GetS("TABLE_NAME");
                            }
                        }
                        finally
                        {
                            rs.Close();
                            rs.Destroy();
                        }

                        if (!string.IsNullOrEmpty(tableName) && (tableID != 0 || tableID != IM.NullI))
                            MainAppForm.ShowApplForm(tableName, tableID, true);
                    }
                    break;
                case PlugTbl.EventLog:
                    {
                        string tableName = "";
                        int tableId = 0;
                        IMRecordset rs = new IMRecordset(recPtr.Table, IMRecordset.Mode.ReadOnly);
                        rs.Select("ID,OBJ_ID,OBJ_TABLE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                        try
                        {
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                tableId = rs.GetI("OBJ_ID");
                                tableName = rs.GetS("OBJ_TABLE");
                            }
                        }
                        finally
                        {
                            if (rs.IsOpen() == true)
                                rs.Close();
                            rs.Destroy();
                        }
                        OnDblClickRecord(new RecordPtr(tableName, tableId));
                    }
                    break;
                case PlugTbl.XvMonitorUsers:
                    if ((CUsers.GetCurDepartment() == UcrfDepartment.URCM) || (CUsers.GetCurDepartment() == UcrfDepartment.Branch))
                    {
                        int idApplPay = IM.NullI;
                        using (LisRecordSet rs = new LisRecordSet(recPtr.Table, IMRecordset.Mode.ReadOnly))
                        {
                            rs.Select("ID,APP_URCM_ID");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                idApplPay = rs.GetI("APP_URCM_ID");
                            }
                        }
                       

                        if (idApplPay != IM.NullI)
                        {
                            CDRVMemoURCM drvMemo = new CDRVMemoURCM(idApplPay, IM.NullI, PlugTbl.XvMonitorUsers, recPtr.Id,"");
                            drvMemo.ShowForm();
                            /*
                            DialogResult DA = DialogResult.Yes;
                            string Message_ = "";
                                using (LisRecordSet rs = new LisRecordSet(recPtr.Table, IMRecordset.Mode.ReadOnly)) {
                                    rs.Select("ID");
                                    rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                                    rs.Open();
                                    if (!rs.IsEOF()) {
                                        using (LisRecordSet rs_xnrfa_applpayurcm = new LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly)){
                                            rs_xnrfa_applpayurcm.Select("ID,CREATED_DATE,MEMO_NUMBER,STATUS,CONTRACTS_ID,EMPLOYEE_ID");
                                            rs_xnrfa_applpayurcm.SetWhere("CONTRACTS_ID", IMRecordset.Operation.Eq, rs.GetI("ID"));
                                            for (rs_xnrfa_applpayurcm.Open(); !rs_xnrfa_applpayurcm.IsEOF(); rs_xnrfa_applpayurcm.MoveNext()){
                                                if (rs_xnrfa_applpayurcm.GetT("CREATED_DATE")>new DateTime(DateTime.Now.Year,DateTime.Now.Month,1)){
                                                    using (LisRecordSet rs_employee = new LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
                                                    {
                                                        rs_employee.Select("ID,LASTNAME");
                                                        rs_employee.SetWhere("ID", IMRecordset.Operation.Eq, rs_xnrfa_applpayurcm.GetI("EMPLOYEE_ID"));
                                                        for (rs_employee.Open(); !rs_employee.IsEOF(); rs_employee.MoveNext()){
                                                            Message_ += string.Format("Номер-{0}, Дата-{1}, Статус-{2},Співробітник-(3) ", rs_xnrfa_applpayurcm.GetS("MEMO_NUMBER"), rs_xnrfa_applpayurcm.GetT("CREATED_DATE"), rs_xnrfa_applpayurcm.GetS("STATUS"), rs_employee.GetS("LASTNAME"))+Environment.NewLine;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            if (!string.IsNullOrEmpty(Message_)){
                                Message_ = "Для даного договору сформовано службову: " + Environment.NewLine + Message_;
                                Message_ += "Продовжити процедуру формування службової (Так/Hі)?";
                                DA = MessageBox.Show(Message_, "", MessageBoxButtons.YesNo);
                            }

                            if ((string.IsNullOrEmpty(Message_)) || (DA == DialogResult.Yes)) {
                                CDRVMemoURCM drvMemo = new CDRVMemoURCM(idApplPay, IM.NullI, PlugTbl.XvMonitorUsers, recPtr.Id);
                                drvMemo.ShowForm();
                            }
                             */ 
                        }
                    }
                    break;
                case PlugTbl.itblXnrfaDrvApplURCM:
                case PlugTbl.XvApplPayUrchm:
                    if (((CUsers.GetCurDepartment() == UcrfDepartment.URCM) || (CUsers.GetCurDepartment() == UcrfDepartment.Branch)) && (recPtr.Id != IM.NullI))
                    {
                        CDRVMemoURCM drvMemo = new CDRVMemoURCM(recPtr.Id, IM.NullI, PlugTbl.XvApplPayUrchm,0,"");
                        drvMemo.ShowForm();
                    }
                    break;
                case PlugTbl.itblXnrfaDrvAppl:
                    if ((CUsers.GetCurDepartment() == UcrfDepartment.URCP) && (recPtr.Id != IM.NullI))
                    {
                        //TODO Дописать форму для отображения заявок
                    }
                    break;
                case PlugTbl.XvTaxPayerForm1:
                    if (IM.TableRight(ICSMTbl.LICENCE, IMTableRight.Select))
                    {
                        int idLicence = IM.NullI;
                        using (LisRecordSet rs = new LisRecordSet(recPtr.Table, IMRecordset.Mode.ReadOnly))
                        {
                            rs.Select("ID,LICENCE_ID");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                                idLicence = rs.GetI("LICENCE_ID");
                        }
                        if (idLicence != IM.NullI)
                        {
                            RecordPtr rp = new RecordPtr(ICSMTbl.LICENCE, idLicence);
                            IM.AdminDisconnect();
                            rp.UserEdit();
                        }
                    }
                    else
                    {
                        MessageBox.Show(CLocaliz.TxT("Not enough rigths"), CLocaliz.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case PlugTbl.XvTaxPayerForm2:
                case PlugTbl.XvTaxPaeyrForm3:
                    if (IM.TableRight(PlugTbl.APPL, IMTableRight.Select))
                    {
                        int idAppl = IM.NullI;
                        bool readOnly = true;
                        using (LisRecordSet rs = new LisRecordSet(recPtr.Table, IMRecordset.Mode.ReadOnly))
                        {
                            rs.Select("ID,APPL_ID,Application.OBJ_TABLE");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, recPtr.Id);
                            rs.Open();
                            if (!rs.IsEOF())
                            {
                                idAppl = rs.GetI("APPL_ID");
                                readOnly = !IM.TableRight(rs.GetS("Application.OBJ_TABLE"), IMTableRight.Update);
                            }
                        }
                        if (idAppl != IM.NullI)
                            MainAppForm.ShowApplFormBase(idAppl, readOnly);
                    }
                    else
                    {
                        MessageBox.Show(CLocaliz.TxT("Not enough rigths"), CLocaliz.Information, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case PlugTbl.itblXnrfaReport:
                    {
                        SpecialReportForm reportForm = new SpecialReportForm(recPtr.Id);
                        reportForm.ShowDialog();
                    }
                    break;
                case PlugTbl._itblXnrfaExternFilter:
                    using (AdditionFillters.AdditionFilters addFilters = new AdditionFillters.AdditionFilters(recPtr.Table, recPtr.Id))
                    {
                        addFilters.ShowDialog();
                    }
                    break;
                case PlugTbl.LOGS:
                    CLogs.ShowLog(recPtr.Id);
                    break;
                default:
                    MessageBox.Show(
                       string.Format(CLocaliz.TxT("The default edit menu is not implemented for the table '{0}'"),
                                     recPtr.Table));
                    break;
            }
        }

        //===================================================
        /// <summary>
        /// Открываем вид форму ВРВ
        /// </summary>
        /// <param name="_table">имя таблицы</param>
        /// <param name="_id">ID записи</param>
        private void ShowApplFormView(string _table, int _id)
        {

            string tableView = GetViewFromTable(_table);
            BaseAppClass obj = BaseAppClass.GetStation(_id, _table, 0, false);
            if (obj != null)
                obj.SetRight(tableView);            
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }

        private string GetViewFromTable(string table)
        {
            switch (table)
            {
                case PlugTbl.XfaAbonentStation:
                    return PlugTbl.XvAbonentStation;
                case PlugTbl.XfaAmateur:
                    return PlugTbl.XvAmateur;
                case PlugTbl.APPL:
                    return PlugTbl.XvAppl;
                case PlugTbl.XfaEmitterStation:
                    return PlugTbl.XvEmiStation;
                case PlugTbl.itblXnrfaMeasureEquipmentCertificates:
                    return PlugTbl.XvMsreqCert;
                case PlugTbl.itblXnrfaPacket:
                    return PlugTbl.XvPacket;
                case PlugTbl.Ship:
                    return PlugTbl.XvShip;
                default:
                    return table;
            }
        }
        //===================================================
        /// <summary>
        /// Открываем форму ВРВ по ID заявки
        /// </summary>
        /// <param name="applId">ID записи</param>
        /// <param name="readOnly">Режим радактирования формы (true - только на чтение)</param>
        private void ShowApplForm(int applId, bool readOnly)
        {
            BaseAppClass obj = BaseAppClass.GetBaseAppl(applId);
            if (readOnly)
                obj.ReadOnly = readOnly; //Не смотрим на права доступа
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }
        //===================================================
        /// <summary>
        /// Поиск всех станций с пустыми статьями
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnFindStationsWithoutArticle(IMQueryMenuNode.Context context)
        {
            int ownerId = 0;
            string title = CLocaliz.TxT("Get owner ID");
            using (LisProgressBar pb = new LisProgressBar(title))
            {
                pb.SetBig(CLocaliz.TxT("Please wait"));
                IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                rs.Select("OWNER_ID");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
                try
                {
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        ownerId = rs.GetI("OWNER_ID");
                    }
                }
                finally
                {
                    rs.Final();
                }
            }
            if (ownerId != 0 && ownerId != IM.NullI)
                ShowStationWithoutArticle(ownerId);
            return false;
        }

        /// <summary>
        /// Ищет и отображает станции без статей (перегруженная)
        /// </summary>
        /// <param name="ownerId"></param>
        private bool ShowStationWithoutArticle(int ownerId, int params1)
        {
            /*
            List<string> provs = new List<string>();
            _curr_prov = new List<string>();
            _curr_prov.Clear();

              const string constUkraine = "Україна";

              IMRecordset rs3 = new IMRecordset(PlugTbl.XvMonitorUsers, IMRecordset.Mode.ReadOnly);
              rs3.Select("ID,CONTRACT_REGION_CODE");
              rs3.SetWhere("ID", IMRecordset.Operation.Eq, params1);

              try
              {
                  rs3.Open();
                  if (!rs3.IsEOF())
                  {

                      if (!string.IsNullOrEmpty(rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim()))
                      {

                          
                           IMRecordset rs4 = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                  rs4.Select("ID,CODE,NAME");
                  rs4.SetWhere("CODE", IMRecordset.Operation.Eq, rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim());

                  try
                  {
                      rs4.Open();
                      if (!rs4.IsEOF())
                      {

                          
                              if (!string.IsNullOrEmpty(rs4.GetS("NAME").ToString().Trim()))
                              {
                                  _curr_prov.Add(rs4.GetS("CODE").ToString().Trim());
                                  if (CDRVMemoURCM.SearchOnElementList(_curr_prov,"32"))
                                   {
                                      _curr_prov.Add("80");
                                   }
                              }
                          
                      }
                  }
                  finally
                  {
                      rs4.Final();
                  }

                      }
                  }
              }
              finally
              {
                  rs3.Final();
              }

                       if (string.IsNullOrEmpty(CUsers.GetUserProvince()) == false)
                       {
                           provs.Add(CUsers.GetUserProvince());
                       }
                       else
                       {


                           FProvincesList fprov = new FProvincesList();
                           fprov.Text = CLocaliz.TxT("The choice of areas to search for networks and stations without articles");
                           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                           rsArea.Select("NAME");
                           try
                           {
                               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                                   fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                               fprov.chlbProvs.Sorted = true;

                               for (int j = 0; j < fprov.chlbProvs.Items.Count; j++)
                               {
                                   for (int jj = 0; jj < _curr_prov.Count; jj++)
                                   {
                                       if (fprov.chlbProvs.Items[j].ToString().Trim() == CDRVMemoURCM.GetNameAreaOnCode(_curr_prov[jj].ToString().Trim()))
                                           fprov.chlbProvs.SetItemChecked(j, true);
                                       //break;
                                   }
                               }

                              
                           }
                           finally
                           {
                               rsArea.Close();
                               rsArea.Destroy();
                           }

                           if (fprov.ShowDialog() == DialogResult.OK)
                           {
                               if (fprov.chlbProvs.CheckedItems.Count!= fprov.chlbProvs.Items.Count)
                                   foreach (object obj in fprov.chlbProvs.CheckedItems)
                                   {
                                       provs.Add(obj.ToString());
                                   }
                           }
                           else
                           {
                               return false;
                           }

                       }


                       int[] applIds;
                       int[] netIds;

                       if ((provs != null) && (provs.Count > 0))
                       {

                           Article.StationWithoutArticle.FindStationWithoutArticle(ownerId, out applIds, out netIds, provs);
                       }
                       else
                       {
                           Article.StationWithoutArticle.FindStationWithoutArticle(ownerId, out applIds, out netIds);
                       }
             */

              

            List<string> provs = new List<string>();
            _curr_prov = new List<string>();
            _curr_prov.Clear();

            const string constUkraine = "Україна";


            if (string.IsNullOrEmpty(CUsers.GetUserProvince()) == false)
            {
                //   provs.Add(CUsers.GetUserProvince());
                //}
                List<string> lst_filia = CUsers.GetSubProvinceUser();
                if (lst_filia != null)
                {

                    FProvincesList fprov = new FProvincesList();
                    fprov.Text = CLocaliz.TxT("The choice of areas to search for networks and stations without articles");
                    IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                    rsArea.Select("NAME");
                    try
                    {
                        for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                            fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                        fprov.chlbProvs.Sorted = true;

                        for (int j = 0; j < fprov.chlbProvs.Items.Count; j++)
                        {
                            for (int jj = 0; jj < lst_filia.Count; jj++)
                            {
                                if (fprov.chlbProvs.Items[j].ToString().Trim() == lst_filia[jj].ToString().Trim())
                                    fprov.chlbProvs.SetItemChecked(j, true);
                                //break;
                            }
                        }

                    }
                    finally
                    {
                        rsArea.Close();
                        rsArea.Destroy();
                    }

                    if (fprov.ShowDialog() == DialogResult.OK)
                    {
                        if (fprov.chlbProvs.CheckedItems.Count != fprov.chlbProvs.Items.Count)
                            foreach (object obj in fprov.chlbProvs.CheckedItems)
                            {
                                provs.Add(obj.ToString());
                            }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                string CONTRACT_REGION_CODE = "";
                IMRecordset rs3 = new IMRecordset(PlugTbl.XvMonitorUsers, IMRecordset.Mode.ReadOnly);
                rs3.Select("ID,CONTRACT_REGION_CODE");
                rs3.SetWhere("ID", IMRecordset.Operation.Eq, params1);

                try
                {
                    rs3.Open();
                    if (!rs3.IsEOF())
                    {
                        if (!string.IsNullOrEmpty(rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim()))
                        {
                            CONTRACT_REGION_CODE = rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim().Length == 1 ? "0" + rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim() : rs3.GetS("CONTRACT_REGION_CODE").ToString().Trim();
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                //_curr_prov.Add(CONTRACT_REGION_CODE);
                if (string.IsNullOrEmpty(CONTRACT_REGION_CODE))
                {
                    _curr_prov.Add("80");
                    _curr_prov.Add("32");
                }
                else
                {
                    _curr_prov.Add(CONTRACT_REGION_CODE);
                }


                FProvincesList fprov = new FProvincesList();
                fprov.Text = CLocaliz.TxT("The choice of areas to search for networks and stations without articles");
                IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                rsArea.Select("NAME");
                try
                {
                    for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                        fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                    fprov.chlbProvs.Sorted = true;

                    for (int j = 0; j < fprov.chlbProvs.Items.Count; j++)
                    {
                        for (int jj = 0; jj < _curr_prov.Count; jj++)
                        {
                            if (fprov.chlbProvs.Items[j].ToString().Trim() == CDRVMemoURCM.GetNameAreaOnCode(_curr_prov[jj].ToString().Trim()))
                                fprov.chlbProvs.SetItemChecked(j, true);
                            //break;
                        }
                    }

                }
                finally
                {
                    rsArea.Close();
                    rsArea.Destroy();
                }

                if (fprov.ShowDialog() == DialogResult.OK)
                {
                    if (fprov.chlbProvs.CheckedItems.Count != fprov.chlbProvs.Items.Count)
                        foreach (object obj in fprov.chlbProvs.CheckedItems)
                        {
                            provs.Add(obj.ToString());
                        }
                }
                else
                {
                    return false;
                }

            }

            int[] applIds;
            int[] netIds;

            if ((provs != null) && (provs.Count > 0))
            {
                Article.StationWithoutArticle.FindStationWithoutArticle(ownerId, out applIds, out netIds, provs);
            }
            else
            {
                Article.StationWithoutArticle.FindStationWithoutArticle(ownerId, out applIds, out netIds);
            }



            if ((netIds.Length > 0) || ((applIds.Length > 0)))
            {
                string mess = string.Format("Знайдено {0} станцій", applIds.Length);
                MessageBox.Show(mess);

                 
                     if (netIds.Length > 0)
                     {
                         //Сети
                         StringBuilder sbTmp = new StringBuilder(1000);
                         int i = 0;
                         string sql = "";
                         while (i < netIds.Length)
                         {
                             for (int j = 0; j < 200 && i < netIds.Length; ++j, ++i)
                             {
                                 int id = netIds[i];
                                 sbTmp.AppendFormat("{0},", id);
                             }
                             if (sbTmp.Length == 0)
                                 sbTmp.Append("0,");
                             if (sql == "")
                                 sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                             else
                                 sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                         }
                         if (sql != "")
                         {
                             IMDBList.Show(string.Format(CLocaliz.TxT("Net without articles ({0})"), netIds.Length), PlugTbl.XfaNet, sql, "ID", OrderDirection.Ascending, 0, 0);
                         }
                         
                     }
                     if (applIds.Length > 0)
                     {
                         StringBuilder sbTmp = new StringBuilder(1000);
                         int i = 0;
                         string sql = "";
                         while (i < applIds.Length)
                         {
                             for (int j = 0; j < 200 && i < applIds.Length; ++j, ++i)
                             {
                                 int id = applIds[i];
                                 sbTmp.AppendFormat("{0},", id);
                             }
                             if (sbTmp.Length == 0)
                                 sbTmp.Append("0,");
                             if (sql == "")
                                 sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                             else
                                 sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                         }
                         if (sql != "")
                         {
                             IMDBList.Show(string.Format(CLocaliz.TxT("Stations without articles ({0})"), applIds.Length), PlugTbl.APPL, sql, "ID", OrderDirection.Ascending, 0, 0);
                         }
                         
                     }
               
            
            }
            else
            {
                MessageBox.Show("Станції без статей відсутні.");
            }
            return true;
        }

        /// <summary>
        /// Ищет и отображает станции без статей
        /// </summary>
        /// <param name="ownerId"></param>
        private void ShowStationWithoutArticle(int ownerId)
        {
            int[] applIds;
            int[] netIds;
            Article.StationWithoutArticle.FindStationWithoutArticle(ownerId, out applIds, out netIds);

            if ((netIds.Length > 0) || ((applIds.Length > 0)))
            {
                MessageBox.Show(string.Format("Знайдено {0} станцій", applIds.Length));
                if (netIds.Length > 0)
                {
                    //Сети
                    StringBuilder sbTmp = new StringBuilder(1000);
                    int i = 0;
                    string sql = "";
                    while (i < netIds.Length)
                    {
                        for (int j = 0; j < 200 && i < netIds.Length; ++j, ++i)
                        {
                            int id = netIds[i];
                            sbTmp.AppendFormat("{0},", id);
                        }
                        if (sbTmp.Length == 0)
                            sbTmp.Append("0,");
                        if (sql == "")
                            sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                        else
                            sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                    }
                    if (sql != "")
                        IMDBList.Show(string.Format(CLocaliz.TxT("Net without articles ({0})"), netIds.Length), PlugTbl.XfaNet, sql, "ID", OrderDirection.Ascending, 0, 0);
                }
                if (applIds.Length > 0)
                {
                    StringBuilder sbTmp = new StringBuilder(1000);
                    int i = 0;
                    string sql = "";
                    while (i < applIds.Length)
                    {
                        for (int j = 0; j < 200 && i < applIds.Length; ++j, ++i)
                        {
                            int id = applIds[i];
                            sbTmp.AppendFormat("{0},", id);
                        }
                        if (sbTmp.Length == 0)
                            sbTmp.Append("0,");
                        if (sql == "")
                            sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                        else
                            sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                    }
                    if (sql != "")
                        IMDBList.Show(string.Format(CLocaliz.TxT("Stations without articles ({0})"), applIds.Length), PlugTbl.APPL, sql, "ID", OrderDirection.Ascending, 0, 0);
                }
            }
            else
            {
                MessageBox.Show("Станції без статей відсутні.");
            }
        }
        //===================================================
        /// <summary>
        /// Сформировать заявку на выставление счета по мониторингу
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnSelectionOwnerPayForDrv(IMQueryMenuNode.Context context)
        {
            int ownerId = 0;
            IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
            rs.Select("Application.PAY_OWNER_ID");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    ownerId = rs.GetI("Application.PAY_OWNER_ID");
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }

            if (ownerId != 0)
            {
                CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ownerId, context.TableName,0,"");
                drvMemo.ShowForm();
            }
            else
            {
                MessageBox.Show("This station doesn't contain an pay owner.", "", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            return false;
        }

        private bool OnSelectionXvMonitorUsers(IMQueryMenuNode.Context context)
        {
            int ownerId = 0;
            int app_urcm_id = 0;
            int id_xnrfa_mon_contr = IM.NullI;
            IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,OWNER_ID,APP_URCM_ID");
            rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
            try {
                rs.Open();
                if (!rs.IsEOF()) {
                    ownerId = rs.GetI("OWNER_ID");
                    id_xnrfa_mon_contr = rs.GetI("ID");
                }
            }
            finally {
                rs.Close();
                rs.Destroy();
            }


            if (ownerId != 0) {
                CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ownerId, PlugTbl.XvMonitorUsers, id_xnrfa_mon_contr, false, true, false, IM.NullI);
                drvMemo.ShowForm();
            }
            else
            {
                MessageBox.Show("This station doesn't contain an pay owner.", "", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            return false;
        }

        
        //===================================================
        /// <summary>
        /// Сформировать заявку на выставление счета по мониторингу (v.2)
        /// </summary>
        /// <param name="ownerId">ID плательщика</param>
        /// <param name="ownerId">Table_Name Имя таблицы</param>
        /// <returns>true</returns>
        private bool OnSelectionOwnerForDrvFromUser(int ownerId, string Table_Name)
        {
            if ((ownerId != 0) && (ownerId != IM.NullI))
            {
                CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ownerId, Table_Name);
                drvMemo.ShowForm();
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Сформировать заявку на выставление счета по мониторингу (v.1)
        /// </summary>
        /// <param name="ownerId">ID плательщика</param>
        /// <returns>true</returns>
        private bool OnSelectionOwnerForDrvFromUser(int ownerId, string Table_Name,int Params1)
        {
            if ((ownerId != 0) && (ownerId != IM.NullI)){
                DialogResult DA = DialogResult.Yes;
                string Message_ = "";
                    using (LisRecordSet rs = new LisRecordSet(Table_Name, IMRecordset.Mode.ReadOnly)) {
                        rs.Select("ID");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, Params1);
                        rs.Open();
                        if (!rs.IsEOF()) {
                            using (LisRecordSet rs_xnrfa_applpayurcm = new LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly)) {
                                rs_xnrfa_applpayurcm.Select("ID,CREATED_DATE,MEMO_NUMBER,DOC_PATH,STATUS,CONTRACTS_ID,EMPLOYEE_ID");
                                rs_xnrfa_applpayurcm.SetWhere("CONTRACTS_ID", IMRecordset.Operation.Eq, rs.GetI("ID"));
                                for (rs_xnrfa_applpayurcm.Open(); !rs_xnrfa_applpayurcm.IsEOF(); rs_xnrfa_applpayurcm.MoveNext()){
                                    if (rs_xnrfa_applpayurcm.GetT("CREATED_DATE") > new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)) {
                                        using (LisRecordSet rs_employee = new LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly)){
                                            rs_employee.Select("ID,LASTNAME");
                                            rs_employee.SetWhere("ID", IMRecordset.Operation.Eq, rs_xnrfa_applpayurcm.GetI("EMPLOYEE_ID"));
                                            for (rs_employee.Open(); !rs_employee.IsEOF(); rs_employee.MoveNext()) {
                                                Message_ += string.Format("Номер-{0}, Дата-{1}, Статус-{2},Співробітник-{3} ", rs_xnrfa_applpayurcm.GetS("DOC_PATH"), rs_xnrfa_applpayurcm.GetT("CREATED_DATE"), rs_xnrfa_applpayurcm.GetS("STATUS"), rs_employee.GetS("LASTNAME")) + Environment.NewLine;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                

                if (!string.IsNullOrEmpty(Message_)) {
                    Message_ = "Для даного договору сформовано службову: " + Environment.NewLine + Message_;
                    Message_+="Продовжити процедуру формування службової (Так/Hі)?";
                    DA = MessageBox.Show(Message_, "", MessageBoxButtons.YesNo);
                }

                if ((string.IsNullOrEmpty(Message_)) || (DA== DialogResult.Yes)) {
                    CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ownerId, Table_Name, Params1,"");
                    drvMemo.ShowForm();
                }
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Открывает окно заявки служебной записки в ДРВ от УРЧМ
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnURCMMemoOpen(IMQueryMenuNode.Context context)
        {
            if (context.TableId != IM.NullI && context.TableId > 0)
            {
                CDRVMemoURCM drvMemo = new CDRVMemoURCM(context.TableId, IM.NullI, context.TableName,0,"");
                drvMemo.ShowForm();
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Обновить частоты
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnUpdateFreq(IMQueryMenuNode.Context context)
        {
            if (MessageBox.Show(CLocaliz.TxT("Обновити частоти?"),
                                CLocaliz.Warning,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return false;
            bool retVal = false;
            try
            {
                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Update frequencies...")))
                {
                    pb.SetProgress(0, 50);
                    pb.SetBig(CLocaliz.TxT("Updating..."));
                    using (LisRecordSet rs = new LisRecordSet(context.TableName, IMRecordset.Mode.ReadOnly))
                    {
                        int countStation = 0;
                        int curCounter = 0;
                        bool exit = false;
                        rs.Select("ID,OBJ_TABLE");
                        rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            if (exit)
                                break;
                            string tableName = rs.GetS("OBJ_TABLE");
                            switch (tableName)
                            {
                                case ICSMTbl.itblMobStation:
                                case ICSMTbl.itblMobStation2:
                                    {
                                        try
                                        {
                                            using (LisTransaction tr = new LisTransaction())
                                            {
                                                int applId = rs.GetI("ID");
                                                BaseAppClass obj = BaseAppClass.GetAppl(applId, true);
                                                using (MainAppForm formAppl = new MainAppForm(obj))
                                                {
                                                    formAppl.Text = "XXX"; //Так нада
                                                    retVal = true;
                                                    countStation++;
                                                    obj.OnSaveAppl();

                                                }
                                                tr.Commit();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            retVal = false;
                                            string msg = string.Format("Помилка.{0}{1}{0}{0}Продовжити?",
                                                                       Environment.NewLine, ex.Message);
                                            if(MessageBox.Show(msg, CLocaliz.Error, MessageBoxButtons.YesNo,
                                                               MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
                                                               != DialogResult.Yes)
                                                exit = true;
                                        }
                                    }
                                    break;
                            }
                            curCounter++;
                            pb.SetSmall(countStation, curCounter);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                retVal = false;
                string msg = string.Format("Помилка.{0}{1}{0}Операція перервана.{0}{2}",
                                           Environment.NewLine, ex.Message, ex.StackTrace);
                MessageBox.Show(msg, CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retVal;
        }

        //===================================================
        /// <summary>
        /// Изменить плательщика
        /// </summary>
        /// <param name="context">context</param>
        /// <returns>true</returns>
        private bool OnChangePayOwner(IMQueryMenuNode.Context context)
        {
            bool retVal = false;
            UcrfDepartment curManagement = CUsers.GetCurDepartment();
            RecordPtr rp = RecordPtr.UserSearch(CLocaliz.TxT("Select pay owner"), ICSMTbl.itblUsers, "");
            if (rp.Id != IM.NullI)
            {
                string selectField;
                string getField;
                string province=""; 
                switch (context.TableName)
                {
                    case PlugTbl.APPL:
                        selectField = "ID,STANDARD";
                        getField = "ID";
                        break;
                    case PlugTbl.AllDecentral:
                        selectField = "ID,PROVINCE";
                        getField = "ID";
                        province = "PROVINCE";
                        break;
                    case PlugTbl.AllStations:
                        selectField = "ID,APPL_ID,PROVINCE";
                        getField = "APPL_ID";
                        province = "PROVINCE";
                        break;
                    default:
                        throw new Exception("Unsupported table:" + context.TableName);
                }
                string msg = CLocaliz.TxT("Змінити платника?");
                if (
                    MessageBox.Show(msg, CLocaliz.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                    MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return false;
                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Changing pay owner")))
                {
                    pb.SetProgress(0, 50);
                    pb.SetBig(CLocaliz.TxT("Loading..."));
                    using (LisTransaction tr = new LisTransaction())
                    {
                        List<int> applIds = new List<int>();
                        using (LisRecordSet rs = new LisRecordSet(context.TableName, IMRecordset.Mode.ReadOnly))
                        {
                            
                            const int maxCounter = 20;
                            rs.Select(selectField);
                            rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                            {
                                int applId = rs.GetI(getField);
                                string applStr="";

                                if (context.TableName == PlugTbl.APPL)
                                {
                                    // Выборка по выбранным областям

                                    IMRecordset recset_ = new IMRecordset(PlugTbl.APPL , IMRecordset.Mode.ReadOnly);
                                    recset_.Select("ID,STANDARD");
                                    recset_.SetWhere("ID", IMRecordset.Operation.Eq, rs.GetI(getField));
                                    recset_.Select("BaseSector1.Position.PROVINCE,Microwave.StationA.Position.PROVINCE,EarthStation.Position.PROVINCE,MobileSector1.Position.PROVINCE,Abonent.Position.PROVINCE, Abonent.USE_REGION,TvStation.Position.PROVINCE,DvbtStation.Position.PROVINCE,FmStation.Position.PROVINCE,TdabStation.Position.PROVINCE,AmateurStation.Position.PROVINCE");

                                
                                        try
                                        {
                                            recset_.Open();
                                            if (!recset_.IsEOF())
                                            {
                                                applStr = recset_.GetS("BaseSector1.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("Microwave.StationA.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("EarthStation.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("MobileSector1.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("Abonent.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("Abonent.USE_REGION");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("TvStation.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("DvbtStation.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("FmStation.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("TdabStation.Position.PROVINCE");
                                                if (string.IsNullOrEmpty(applStr)) applStr = recset_.GetS("AmateurStation.Position.PROVINCE");
                                                
                                            }
                                        }
                                        finally
                                        {
                                            recset_.Close();
                                            recset_.Destroy();
                                        }

                                            
                                    
                                            
                                }
                             
                                    if (context.TableName!= PlugTbl.APPL)
                                    {
                                        applStr = rs.GetS(province);
                                    }

                                    

                                    if (curManagement == UcrfDepartment.Branch)
                                    {
                                        if (!string.IsNullOrEmpty(applStr))
                                        {
                                            List<string> Lst = new List<string>();
                                            Lst.Clear();
                                            Lst = CUsers.GetSubProvinceOnUser(CUsers.GetUserProvince());
                                            if (Lst.Contains(applStr))
                                            {
                                                if (applIds.Contains(applId) == false)
                                                {
                                                    applIds.Add(applId);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (applIds.Contains(applId) == false)
                                            applIds.Add(applId);
                                    }

                                    if (applIds.Count > maxCounter)
                                    {

                                        ChangePayOwner(rp.Id, applIds.ToArray());
                                        applIds.Clear();
                                    }
                                
                                retVal = true;
                                pb.Increment(true);
                            }
                        }
                        if (applIds.Count > 0)
                        {
                            ChangePayOwner(rp.Id, applIds.ToArray());
                        }
                        tr.Commit();
                    }
                }
            }
            return retVal;
        }
        #endregion

        /// <summary>
        /// Изменить плательщика
        /// </summary>
        /// <param name="payOwnerId">ID плательщика</param>
        /// <param name="applIds">Список id заявок </param>
        private void ChangePayOwner(int payOwnerId, params int[] applIds)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Changing pay owner")))
            {
                int index = 0;
                pb.SetBig(CLocaliz.TxT("Updating..."));
                StringBuilder sqlBuilder = new StringBuilder(100);
                foreach (int applId in applIds)
                {
                    if (sqlBuilder.Length > 0)
                        sqlBuilder.Append(",");
                    sqlBuilder.Append(applId);
                }

                using (LisRecordSet rs = new LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadWrite))
                {
                    rs.Select("ID,PAY_OWNER_ID");
                    rs.SetAdditional(string.Format("[ID] in ({0})", sqlBuilder));
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        rs.Edit();
                        rs.Put("PAY_OWNER_ID", payOwnerId);
                        rs.Update();
                        pb.SetSmall(++index);
                    }
                }
            }
        }

        #region AUTO_TEST

        private void OnRunAutoTest()
        {
            RunAutoTest(false);
        }

        private void RunAutoTest(bool isSilent)
        {
            if (!ApplSetting.IsRunTest)
            {
                if (!isSilent)
                    MessageBox.Show("Auto test is blocked");
                return;
            }
            using (AutoTest.AutoTestLog log = new XICSM.UcrfRfaNET.AutoTest.AutoTestLog())
            {
                log.Create(ApplSetting.RunTestLogPath);
                //Запускаем тесты
                //Пример:
                //log.RunTest("description");             //Обязательная команда перед началом каждого теста
                //log.ErrorMessage("Error message");      //Сообщение об ошибку 
                //log.FinishTest();                       //Обязательная команда после каждого теста
            }
        }
        #endregion

        private void OnReminder()
        {
            ReminderForm reminderForm = new ReminderForm();
            reminderForm.ShowDialog();
        }

        private void OnUtdbTest()
        {
            using(var globalDbTest = new GlobalDbTest())
            {
                globalDbTest.ShowDialog();
            }
        }

        private void OnSpecialReport()
        {
            SpecialReportForm reportForm = new SpecialReportForm();
            reportForm.ShowDialog();
        }

        private bool OnNotificationToCoordination(IMQueryMenuNode.Context context)
        {
            IM.AdminDisconnect();
            if ((IM.TableRight(ICSMTbl.FxLkClink) & IMTableRight.Insert) != IMTableRight.Insert)
            {
                MessageBox.Show("Відсутнє право додавання записів до таблиці " + ICSMTbl.FxLkClink, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            else
            {
                if (MessageBox.Show("Імпортувати вибрані записи до таблиці Координованих Лінків?", "Підтвердження", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {    
                    T1nToClink exporter = new T1nToClink();
                    exporter.Import(context);
                    IM.RefreshQueries(ICSMTbl.FxLkClink);
                    IM.RefreshQueries(ICSMTbl.FxLkClinkSta);
                    MessageBox.Show("Успішно", "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return true;
            }
        }

        private bool OnNotificateWienNob(IMQueryMenuNode.Context context)
        {
                return false;
        }

        private bool OnImportMonitoring(IMQueryMenuNode.Context context)
        {
            FormImportMonitoring FImport = new FormImportMonitoring(context);
            FImport.ShowDialog();
            return false;
        }
        

        private bool OnUpdateLicenceList(IMQueryMenuNode.Context context)
        {
            if (FLicParams.Config() == DialogResult.OK)
            {
                IM.AdminDisconnect();
                List<int> idList = new List<int>();
                IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    // suppose tablename == XNRFA_APPL
                    if ((context.TableName == "XNRFA_APPL") || (context.TableName == PlugTbl.AllDecentral))
                    {
                        rs.Select("ID");
                    }
                    else if (context.TableName == PlugTbl.AllStations)
                    {
                        rs.Select("APPL_ID");
                    }

                    rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines | IMRecordset.WhereCopyOptions.KeepOrder);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if ((context.TableName == "XNRFA_APPL") || (context.TableName == PlugTbl.AllDecentral))
                        {
                            idList.Add(rs.GetI("ID"));
                        }
                        else if (context.TableName == PlugTbl.AllStations)
                        {
                            idList.Add(rs.GetI("APPL_ID"));
                        }
                    }
                }
                finally
                {
                    rs.Destroy();
                }
                UpdateLicenceList(idList.ToArray());
                IM.RefreshQueries(PlugTbl.APPL_LIC);
                IM.RefreshQueries(PlugTbl.APPL);
            }
            return false;
        }

        private bool OnCheckStandard(IMDBList context, string TableName)
        {
            bool isRRZ=true;
                IMRecordset rs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                try
                {
                    // suppose tablename == XNRFA_APPL
                    rs.Select("ID,STANDARD");
                    rs.AddSelectionFrom(context, IMRecordset.WhereCopyOptions.SelectedLines | IMRecordset.WhereCopyOptions.KeepOrder);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (rs.GetS("STANDARD")!="РРЗ")
                        {
                            isRRZ = false;
                            MessageBox.Show("Даний функціонал тільки для технології РРЗ!");
                            break;
                        }
                    }
                }
                finally
                {
                    rs.Destroy();
                }

                return isRRZ;
        }

        private bool OnTRKREZ_Plan(IMQueryMenuNode.Context context)
        {
            bool isView = false;
            bool IsUpdate = false;
            ComboBox cmb = new ComboBox();
            if (!OnCheckStandard(context.DataList, context.TableName)) return false;
            Dictionary<string, string> dictExplType = EriFiles.GetEriCodeAndDescr("Months");
            foreach (KeyValuePair<string, string> pair in dictExplType) {
                HelpClasses.Forms.cbItem cb = new HelpClasses.Forms.cbItem();
                int MT=IM.NullI; 
                if (int.TryParse(pair.Key, out MT)) {
                    cb.Month = MT;
                }
                cb.Value = pair.Value;
                cmb.Items.Add(cb);
            }
            FormRRZ_Plan.FillStart(context.TableName, context.DataList, ref cmb, DateTime.Now.Year);
            FormRRZ_Plan Plan = new FormRRZ_Plan(context, cmb);
            Plan.ShowDialog();
          
            
            //if (FormRRZ_Plan.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

        private bool OnTRKREZ_Plan_Contain(IMQueryMenuNode.Context context)
        {
            bool isView = false;
            bool IsUpdate = false;
            ComboBox cmb = new ComboBox();
            if (!OnCheckStandard(context.DataList, context.TableName)) return false;
            Dictionary<string, string> dictExplType = EriFiles.GetEriCodeAndDescr("Months");
            foreach (KeyValuePair<string, string> pair in dictExplType)
            {
                HelpClasses.Forms.cbItemContain cb = new HelpClasses.Forms.cbItemContain();
                int MT = IM.NullI;
                if (int.TryParse(pair.Key, out MT))
                {
                    if (MT > DateTime.Now.Month)
                    {
                        cb.Month = MT;
                        cb.Year = DateTime.Now.Year;
                        cb.Value = pair.Value;
                        cmb.Items.Add(cb);
                    }
                }
            }
            FormRRZ_Contain Plan = new FormRRZ_Contain(context, cmb);
            Plan.ShowDialog();


            //if (FormRRZ_Plan.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

        private bool OnDeleteBandFreq(IMQueryMenuNode.Context context)
        {
              DialogResult DA = MessageBox.Show("Видалити вибраний запис?", "Увага!", MessageBoxButtons.YesNo);
              if (DA == DialogResult.Yes)
              {
                  IMRecordset rsCh = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                  rsCh.Select("ID");
                  rsCh.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
                  for (rsCh.Open(); !rsCh.IsEOF(); rsCh.MoveNext())
                  {
                      rsCh.Delete();
                  }
                  if (rsCh.IsOpen())
                      rsCh.Close();
                  rsCh.Destroy();
              }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnLoadFileStreet(IMQueryMenuNode.Context context)
        {
            FormLoadFileStreet str_ = new FormLoadFileStreet(context);
            str_.ShowDialog();
            return false;
        }

        private bool DeleteRecCHAPPURCM(IMQueryMenuNode.Context context)
        {
            if (MessageBox.Show(CLocaliz.TxT("Delete record?"), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (Icsm.LisRecordSet rs_chapp = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplAppsURCM, IMRecordset.Mode.ReadWrite)) {
                    rs_chapp.Select("ID,APPL_ID,APP_URCM_ID");
                    rs_chapp.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    rs_chapp.Open();
                    if (!rs_chapp.IsEOF()) {
                        int ID = rs_chapp.GetI("ID");
                        IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_TABLE,TABLE_NAME");
                        r.SetAdditional("([ID]=" + rs_chapp.GetI("APPL_ID") + ")");
                        r.Open();
                        if (!r.IsEOF()) {
                            IMRecordset r_microwa = new IMRecordset(ICSMTbl.itblMicrowa, IMRecordset.Mode.ReadOnly);
                            r_microwa.Select("ID,StationA.ID,StationB.ID");
                            r_microwa.SetAdditional(" (([ID]=" + r.GetI("OBJ_ID1") + ") OR ( [ID]=" + r.GetI("OBJ_ID2") + " )) ");
                            for (r_microwa.Open(); !r_microwa.IsEOF(); r_microwa.MoveNext()) {
                                int ID_A = r_microwa.GetI("StationA.ID");
                                int ID_B = r_microwa.GetI("StationB.ID");
                                using (IMRecordset rsAllStation_microwa = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite)) {
                                    rsAllStation_microwa.Select("ID,OBJ_ID,OBJ_TABLE,APP_URCM_ID");
                                    rsAllStation_microwa.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "MICROWS");
                                    rsAllStation_microwa.SetWhere("APP_URCM_ID", IMRecordset.Operation.Eq, rs_chapp.GetI("APP_URCM_ID"));
                                    rsAllStation_microwa.SetAdditional(" (([OBJ_ID]=" + ID_A + ") OR ([OBJ_ID]=" + ID_B + ")) ");
                                    for (rsAllStation_microwa.Open(); !rsAllStation_microwa.IsEOF(); rsAllStation_microwa.MoveNext()) {
                                        rsAllStation_microwa.Edit();
                                        rsAllStation_microwa.Put("APP_URCM_ID", IM.NullI);
                                        rsAllStation_microwa.Update();
                                    }
                                }
                            }
                            r_microwa.Close();
                        }
                        r.Close();
                        rs_chapp.Delete();
                        CLogs.WriteInfo(ELogsWhat.Delete, string.Format("Запис ID={0} таблиці {1} APP_URCM_ID={2} APPL_ID={3} видалено '{4}' успішно. ", ID, PlugTbl.itblXnrfaDrvApplAppsURCM, rs_chapp.GetI("APP_URCM_ID"), rs_chapp.GetI("APPL_ID"), IM.ConnectedUser()));
                    }
                }
            }
            return true;
        }

        private bool OnRecalcWork(IMQueryMenuNode.Context context)
        {
            if (MessageBox.Show(CLocaliz.TxT("Recalculate?"), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string Log = "";
                using (Icsm.LisRecordSet rs_chapp = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplAppsURCM, IMRecordset.Mode.ReadOnly)) {
                    rs_chapp.Select("ID,APP_URCM_ID");
                    rs_chapp.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    rs_chapp.Open();
                    if (!rs_chapp.IsEOF()) {
                        using (Icsm.LisRecordSet rs_applurcm = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly)) {
                            rs_applurcm.Select("ID,STATUS,OWNER_ID");
                            rs_applurcm.SetWhere("ID", IMRecordset.Operation.Eq, rs_chapp.GetI("APP_URCM_ID"));
                            rs_applurcm.Open();
                            if (!rs_applurcm.IsEOF()) {
                                if (rs_applurcm.GetS("STATUS").ToLower() == "saved") {
                                    IM.Execute(string.Format("DELETE FROM %{0} WHERE APPLPAY_ID={1}", PlugTbl.itblXnrfaDrvApplWorksURCM, rs_chapp.GetI("APP_URCM_ID")));
                                    //CLogs.WriteInfo(ELogsWhat.RecalcWorks, string.Format("Записи таблиці {0}, що відповідають APP_URCM_ID={1}  видалено '{2}' успішно. ", PlugTbl.itblXnrfaDrvApplWorksURCM, rs_chapp.GetI("APP_URCM_ID"), IM.ConnectedUser()));
                                    Log = string.Format("Записи таблиці {0}, що відповідають APP_URCM_ID={1}  видалено '{2}' успішно. ", PlugTbl.itblXnrfaDrvApplWorksURCM, rs_chapp.GetI("APP_URCM_ID"), IM.ConnectedUser());
                                    MemoLines ll = new MemoLines();
                                    using (Icsm.LisRecordSet rs_applurcm_ = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplAppsURCM, IMRecordset.Mode.ReadOnly)) {
                                        rs_applurcm_.Select("ID,APP_URCM_ID,PRICE_ID,COUNT,ADRES,PROVINCE,STANDARD,APPL_ID");
                                        rs_applurcm_.SetWhere("APP_URCM_ID", IMRecordset.Operation.Eq, rs_chapp.GetI("APP_URCM_ID"));
                                        for (rs_applurcm_.Open(); !rs_applurcm_.IsEOF(); rs_applurcm_.MoveNext()) {
                                            ll.AddItem(rs_applurcm_.GetI("PRICE_ID"), "", rs_applurcm_.GetS("STANDARD"), rs_applurcm_.GetI("APPL_ID"), rs_applurcm_.GetI("COUNT"), rs_applurcm_.GetS("ADRES"), "", rs_applurcm_.GetS("PROVINCE"));
                                        }
                                    }
                                    List<int> PriceID = new List<int>();
                                    for (int i=0;i<ll.lines.Count;i++) {
                                            PriceID.Add(ll.lines[i].priceId);
                                    }

                                    foreach (int item in PriceID) {
                                        for (int i=0;i<ll.lines.Count;i++) {
                                            if (ll.lines[i].priceId==item) {
                                                int Cnt_work = 0;
                                                for (int j=0; j<ll.lines[i].appIds.Count;j++) {  
                                                    Cnt_work+=ll.lines[i].appIds[j].count;
                                                }

                                                 using (Icsm.LisRecordSet rsWork = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplWorksURCM, IMRecordset.Mode.ReadWrite)) {
                                                 rsWork.Select("ID,APPLPAY_ID,PRICE_ID,COUNT");
                                                 rsWork.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                                                 rsWork.Open();
                                                 if (rsWork.IsEOF()) {
                                                      int newId = IM.AllocID(PlugTbl.itblXnrfaDrvApplWorksURCM, 1, -1);
                                                        rsWork.AddNew();
                                                        rsWork.Put("ID", newId);
                                                        rsWork.Put("APPLPAY_ID", rs_chapp.GetI("APP_URCM_ID"));
                                                        rsWork.Put("PRICE_ID",item);
                                                        rsWork.Put("COUNT",Cnt_work);
                                                        rsWork.Update();
                                                    }
                                                 }
                                            }
                                        }
                                    }
                                    Log += Environment.NewLine + string.Format("Записи таблиці {0}, що відповідають APP_URCM_ID={1} створено '{2}' успішно. ", PlugTbl.itblXnrfaDrvApplWorksURCM, rs_chapp.GetI("APP_URCM_ID"), IM.ConnectedUser());
                                    //CLogs.WriteInfo(ELogsWhat.RecalcWorks, string.Format("Записи таблиці {0}, що відповідають APP_URCM_ID={1} створено '{2}' успішно. ", PlugTbl.itblXnrfaDrvApplWorksURCM, rs_chapp.GetI("APP_URCM_ID"), IM.ConnectedUser()));
                                    CDRVMemoURCM drvMemo = new CDRVMemoURCM(rs_chapp.GetI("APP_URCM_ID"), IM.NullI, PlugTbl.XvApplPayUrchm, 0, Log);
                                    drvMemo.ShowForm();
                                    //CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, rs_applurcm.GetI("OWNER_ID"), "USERS", 0, false, false, true, rs_chapp.GetI("APP_URCM_ID"));
                                    //drvMemo.ShowForm();
                                    
                                }
                                else {
                                    MessageBox.Show(string.Format("Службова записка знаходиться у статусі {0}, в якому дана операція заборонена",rs_applurcm.GetS("STATUS").ToLower()));
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        private bool OnEditLisFile(IMQueryMenuNode.Context context)
        {

            string networkLocation = "";
                IMRecordset rsNetPath = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
                rsNetPath.Select("ITEM,WHAT");
                rsNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-NOT-COM");
                rsNetPath.Open();
                if (!rsNetPath.IsEOF())
                      networkLocation = rsNetPath.GetS("WHAT");
                 rsNetPath.Destroy();

            RecordPtr rec = new RecordPtr("WIEN_HEADREQ_MOB",context.TableId);
            if (!rec.UserEdit())
            {
                IMRecordset r = new IMRecordset("WIEN_HEADREQ_MOB", IMRecordset.Mode.ReadOnly);
                r.Select("ID,PATH");
                r.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
                r.Open();
                if (!r.IsEOF())
                {
                    IMRecordset r_wien_ans_mob = new IMRecordset("WIEN_ANS_MOB", IMRecordset.Mode.ReadOnly);
                    r_wien_ans_mob.Select("HEADREQ_ID,CO_LINK_ID");
                    r_wien_ans_mob.SetWhere("HEADREQ_ID", IMRecordset.Operation.Eq, r.GetI("ID"));
                    r_wien_ans_mob.Open();
                    if (!r_wien_ans_mob.IsEOF())
                    {
                        IMRecordset r_wien_coord_mob = new IMRecordset("WIEN_COORD_MOB", IMRecordset.Mode.ReadOnly);
                        r_wien_coord_mob.Select("ID,P6,F2,R1,O1,CLASS,RADIUS,ASL");
                        r_wien_coord_mob.SetWhere("ID", IMRecordset.Operation.Eq, r_wien_ans_mob.GetI("CO_LINK_ID"));
                        for (r_wien_coord_mob.Open(); !r_wien_coord_mob.IsEOF(); r_wien_coord_mob.MoveNext())
                        {
                            //210-219 (10)
                            //string Key = r_wien_coord_mob.GetS("P6").PadLeft(6, '0') + (r_wien_coord_mob.GetI("F2")!=IM.NullI ? r_wien_coord_mob.GetI("F2").ToString().PadLeft(2, ' ') : "00") + (r_wien_coord_mob.GetI("R1")!=IM.NullI ? r_wien_coord_mob.GetI("R1").ToString().PadLeft(1, ' ') : "0")  + (r_wien_coord_mob.GetI("O1")!=IM.NullI ? r_wien_coord_mob.GetI("O1").ToString().PadLeft(1, ' '):"0");
                            ////14-15 (2)
                            //string class_ = r_wien_coord_mob.GetS("CLASS").PadLeft(2, ' ');
                            //67-71 (5)
                            //double radius = r_wien_coord_mob.GetD("RADIUS"); string rds_ = ((int)Math.Round(radius)).ToString().PadLeft(5,' ');
                            //72-75 (4)
                            //double ASL = r_wien_coord_mob.GetD("ASL"); string asl_ = ((int)Math.Round(ASL)).ToString().PadLeft(4, ' ');
                            if (!string.IsNullOrEmpty(networkLocation))
                            {
                                string Net_ = networkLocation;
                                networkLocation += @"\" + r.GetS("PATH");
                                HelpFunction.CorrectFile_LIS(networkLocation,  Net_, r.GetS("PATH"), r.GetI("ID"));
                            }
                            break;
                        }
                         if (r_wien_coord_mob.IsOpen())
                             r_wien_coord_mob.Close();
                         r_wien_coord_mob.Destroy();

                    }
                    if (r_wien_ans_mob.IsOpen())
                        r_wien_ans_mob.Close();
                    r_wien_ans_mob.Destroy();


                }
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            return false;
        }
        
        private bool OnBandFreq(IMQueryMenuNode.Context context)
        {
            bool IsUpdate = false;
            FormBandFreq Band = new FormBandFreq(context.TableId);
            Band.ShowDialog();
            if (FormBandFreq.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

        private bool OnBandFreqNew(IMQueryMenuNode.Context context)
        {
            bool IsUpdate = false;
            context = null;
            FormBandFreq Band = new FormBandFreq(-1);
            Band.ShowDialog();
            if (FormBandFreq.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

           

        private bool OnLoadFile(IMQueryMenuNode.Context context)
        {
            bool IsUpdate = false;
            FormLoadFile frm = new FormLoadFile(context);
            frm.ShowDialog();
            if (FormLoadFile.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

        private bool OnTRKREZ_Rez(IMQueryMenuNode.Context context)
        {
            bool IsUpdate = false;
            DateTime DT = IM.NullT;
            string Txt = "";
            if (!OnCheckStandard(context.DataList, context.TableName)) return false;
            if (FormRRZ_Result.FillStart(context.TableName, context.DataList, ref DT, ref Txt))
            {
                FormRRZ_Result Res = new FormRRZ_Result(context, DT, Txt);
                Res.ShowDialog();
            }
            //if (FormRRZ_Result.DA == DialogResult.OK) IsUpdate = true;
            return IsUpdate;
        }

        static public void UpdateLicenceList(int[] idList)
        {
            int recTotal = 0; bool isInterrupt = false;
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Processing assignments...")))
            {
                List<int> newLicIdList = new List<int>();
                bool isInteractive = false;
                LicParams lp = LicParams.GetLicParams();
                
                foreach (int applId in idList)
                {
                         BaseAppClass appl = BaseAppClass.GetAppl(applId, false);
                    

                        if (pb.UserCanceled())
                        {
                            if (MessageBox.Show(CLocaliz.TxT("Interrupt?"), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                break;
                        }

                        string objTable = appl.objTableName;
                        bool isRigthToUpdate = (IM.TableRight(objTable) & IMTableRight.Update) == IMTableRight.Update;
                        //MessageBox.Show(string.Format("{0}: ID={1}, OBJ_TABLE={2}, Right to update={3}", ++recTotal
                        //, applId, objTable, isRigthToUpdate));
                        

                        if (!isRigthToUpdate)
                        {
                            if (MessageBox.Show(string.Format(CLocaliz.TxT("You have no right to perform this operation (no right to update table {0}). Interrupt?"), objTable), CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                break;
                            else
                                continue;
                        }


                        if (isInterrupt == false)
                        {

                            if (appl.GetActualLicenceList(isInteractive, ref newLicIdList, ref isInterrupt))
                            {

                                appl.licenceListId = newLicIdList;
                                appl.licenceListId.Sort();
                                appl.CheckProlongatedLicences(ref appl.licenceListId);




                                appl.ShowDetailDozv();
                                appl.SaveLicence();

                                MainAppForm app = new MainAppForm(appl);
                                app.SaveToBase();

                                if ((!isInteractive) && (lp.AutoLinkLicences)) isInteractive = true;

                                if (isInterrupt == false)
                                {
                                    pb.SetBig("APPL.ID = " + applId.ToString());
                                    pb.SetSmall(++recTotal);
                                    pb.SetProgress(recTotal, recTotal);
                                }
                                app.Dispose();
                            }

                        }
                        else
                        {
                            break;
                        }

                    appl.Dispose();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            MessageBox.Show(string.Format("{0} records processed", recTotal));
        }

        private bool OnUpdateAllottedChannels(IMQueryMenuNode.Context context)
        {
            string channels = "";
            int planId = IM.NullI;
            IMRecordset rsChal = IMRecordset.ForRead(new RecordPtr(context.TableName, context.TableId), "TYPE,PLAN_ID,CHANNELS");
            try
            {
                channels = rsChal.GetS("CHANNELS");
                planId = rsChal.GetI("PLAN_ID");
            }
            finally
            {
                rsChal.Destroy();
            }
            // delete alloted channels for 
            IM.Execute("delete from %" + ICSMTbl.itblChAllotCh + " where ALLOT_ID = " + context.TableId.ToString());
            if (planId == IM.NullI || channels == "")
                return false;
            // decompose CHANNELS field
            List<int> chList = ConvertType.ToChanNumbList(channels);
            // select all channels from associated Plan
            IMRecordset rsChannels = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
            try
            {
                rsChannels.Select("FREQ,CHANNEL,PARITY,BANDWIDTH,OFFSET");
                rsChannels.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, planId);
                rsChannels.Open();
                if (rsChannels.IsEOF())
                    return false;
                IMRecordset rsChAllCh = new IMRecordset(ICSMTbl.itblChAllotCh, IMRecordset.Mode.ReadWrite);
                try
                {
                    rsChAllCh.Select("ID,ALLOT_ID,FREQ,PLAN_ID,CHANNEL,PARITY,BANDWIDTH,OFFSET");
                    // iterate through plan channels list, inserting channelsintto allotted table btw
                    for (rsChAllCh.Open(); !rsChannels.IsEOF(); rsChannels.MoveNext())
                    {
                        int iCh = -1;
                        string ch = rsChannels.GetS("CHANNEL");
                        try { iCh = Convert.ToInt32(ch); }
                        catch { }
                        if (chList.Contains(iCh))
                        {
                            rsChAllCh.AddNew();
                            rsChAllCh.Put("ID", IM.AllocID(ICSMTbl.itblChAllotCh, 1, -1));
                            rsChAllCh.Put("ALLOT_ID", context.TableId);
                            rsChAllCh.Put("FREQ", rsChannels.GetD("FREQ"));
                            rsChAllCh.Put("PLAN_ID", planId);
                            rsChAllCh.Put("CHANNEL", ch);
                            rsChAllCh.Put("PARITY", rsChannels.GetS("PARITY"));
                            rsChAllCh.Put("BANDWIDTH", rsChannels.GetD("BANDWIDTH"));
                            rsChAllCh.Put("OFFSET", rsChannels.GetD("OFFSET"));
                            rsChAllCh.Update();
                        }
                    }
                }
                finally
                {
                    rsChAllCh.Destroy();
                }
            }
            finally
            {
                rsChannels.Destroy();
            }
            return false;
        }
    }
    public class ExtendProps
    {
        public int obj_id1=IM.NullI;
        public int obj_id2 = IM.NullI;
        public int obj_id3 = IM.NullI;
        public int obj_id4 = IM.NullI;
        public int obj_id5 = IM.NullI;
        public int obj_id6 = IM.NullI;
        public string obj_table = "";


        public string NumDzv = "";
        public string NumVisn = "";
        public DateTime Dozv_Start = IM.NullT;
        public string OutStr = "";
        public string CurrFieldProvince = "";
    }
   
}


