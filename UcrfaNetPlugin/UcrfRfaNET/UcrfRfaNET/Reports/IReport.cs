﻿using System;
using System.Drawing;
using System.Linq;
using Lis.OfficeBridge;
using NPOI.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;


namespace XICSM.UcrfRfaNET.Reports
{
    /// <summary>
    /// Интерфейс для генерации отчета
    /// </summary>
    interface IReport : IDisposable
    {
        /// <summary>
        /// Инициализация генератора
        /// </summary>
        /// <param name="fileName">Полный путь к файлу. Если разширение файла отсутствует, то оно добавиться автоматически</param>
        void Init(string fileName, string NameSheet, string separator);
        /// <summary>
        /// Добавить палитру
        /// </summary>
        /// <param name="NameSheet"></param>
        void AddSheet(string NameSheet);
        /// <summary>
        /// Записть строку в файл
        /// </summary>
        /// <param name="columns">список значенией колонок</param>
        void WriteLine(params string[] columns);
        /// <summary>
        /// Сохранить файл
        /// </summary>
        void Save();
        /// <summary>
        /// Освободить все ресурсы
        /// </summary>
        void Close();
        /// <summary>
        /// Объеденить ячейки
        /// </summary>
        void UnionCell(int col1, int row1, int col2, int row2);
        void UnionCell(int col1, int row1, int col2, int row2, ICellStyle st, string val);

        /// <summary>
        /// Установить формат для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="cellFormat">формат ячейки</param>
        void SetCellFormat(int col, int row, CellFormat cellFormat);
        

        /// <summary>
        ///  Установить содержимое ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="val">Содержимое ячейки</param>
        void SetCellValue(int col, int row, string val);
        

        /// <summary>
        /// Установить цвет фона для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет фона</param>
        void SetCellBackgroundColor(int col, int row, Color color);
        
      

        /// <summary>
        /// Установить размер шрифта
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        void SetCellFontSize(int col, int row,string Name, int Size);


        /// <summary>
        /// Установить текущий номер строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        void SetIndexRow(int Value);
        

        /// <summary>
        /// Получить  номер текущей строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        int GetIndexRow();


        /// <summary>
        /// Установка ширины столбцов
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Width"></param>

        void SetColumnWidth(int col, int row, double Width);

        void SetColumnWidth(int col, int length_row);

        /// <summary>
        /// Установка направления текста
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Orientation"></param>
        void SetTextOrientation(int col, int row, int Orientation);
        

        /// <summary>
        /// Выравнивание текста в ячейке по вертикали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        void SetVerticalAlignment(int col, int row, int Alignment);
     

        /// <summary>
        /// Выравнивание текста в ячейке по горизонтали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        void SetHorisontalAlignment(int col, int row, int Alignment);
        

        /// <summary>
        /// Установка высоты строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Height"></param>
        void SetRowHeight(int col, int row, double Height);

        /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        void SetBorderStyle(int col, int row, int Style);
        //void SetBorderStyle();

          /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col1"></param>
        /// <param name="row1"></param>
        /// <param name="col2"></param>
        /// <param name="row2"></param>
        /// <param name="Size"></param>
        void SetBorderStyle(int col1, int row1, int col2, int row2, int Style);

        void SetBackgroundColor();


        void SetBorderStyle();


        void SetBorderStyleDefault();

        void SetCellStyle(int col, int row, ICellStyle st);



        ICellStyle GetCellStyleBorder();
        ICellStyle GetCellStyleHead();
        ICellStyle GetCellStyleBackgroundColor();
        ICellStyle GetCellStyleDefault();
        ICellStyle GetCellStyleBackgroundDefaultColor();
                
    }
}
