﻿using System;
using System.IO;
using System.Linq;
using Lis.OfficeBridge;
using System.Drawing;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace XICSM.UcrfRfaNET.Reports
{
    class ReportExcel : IReport
    {
        private ExcelBridge _ew = null;
        private int _indexRow;
        private string _fileName;
        #region Implementation of IDisposable

        public void Dispose()
        {
            Close();
        }

        #endregion

        #region Implementation of IReport


         /// <summary>
        /// Установить текущий номер строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetIndexRow(int Value)
        {
            _indexRow = Value;
        }

        /// <summary>
        /// Получить  номер текущей строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public int GetIndexRow()
        {
           return _indexRow;
        }

        /// <summary>
        /// Установить размер шрифта
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetCellFontSize(int col, int row, string Name, int Size)
        {
            _ew.SetCellFontSize(col, row, Name, Size);
        }

        /// <summary>
        /// Инициализация генератора
        /// </summary>
        /// <param name="fileName">Полный путь к файлу. Если разширение файла отсутствует, то оно добавиться автоматически</param>
        public void Init(string fileName,string NameSheet, string separator)
        {
            if (string.IsNullOrEmpty(Path.GetExtension(fileName)))
                fileName += ".xls";
            _fileName = fileName;
            _indexRow = 1;
            _ew = new ExcelBridge();
            if (_ew.Init() == false)
                throw new Exception("Error Excel initialization");
            _ew.DisplayAlerts(false);
            _ew.AddWorkbook(NameSheet);
        }

        /// <summary>
        /// Добавить новую палитру
        /// </summary>
        /// <param name="NameSheet"></param>
        //public void AddSheet(IWorkbook wk_, string NameSheet)
        public void AddSheet(string NameSheet)
        {

        }

        /// <summary>
        /// Записть строку в файл
        /// </summary>
        /// <param name="columns">список значенией колонок</param>
        public void WriteLine(params string[] columns)
        {
            for (int i = 0; i < columns.Count(); i++ )
            {
                int col = i + 1;
                _ew.SetCellFormat(col, _indexRow, CellFormat.ExcelTextFormat);
                _ew.SetCellValue(col, _indexRow, columns[i]);
                _ew.SetCellFontSize(col, _indexRow, "Times New Roman", 8);
            }
            _indexRow++;
        }

        /// <summary>
        /// Установить формат для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="cellFormat">формат ячейки</param>
        public void SetCellFormat(int col, int row, CellFormat cellFormat)
        {
            _ew.SetCellFormat(col, row, cellFormat);
        }

        /// <summary>
        ///  Установить содержимое ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="val">Содержимое ячейки</param>
        public void SetCellValue(int col, int row, string val)
        {
            _ew.SetCellValue(col,row, val);
        }

         /// <summary>
        /// Установить цвет фона для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет фона</param>
        public void SetCellBackgroundColor(int col, int row, Color color)
        {
            _ew.SetCellBackgroundColor(col,row,color);
        }

        /// <summary>
        /// Установить цвет фона шрифта ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет шрифта</param>
        public void SetCellForegroundColor(int col, int row, Color color)
        {
            _ew.SetCellForegroundColor(col, row, color);
        }

        /// <summary>
        /// Сделать объединение ячеек
        /// </summary>
        /// <param name="columns">список значенией колонок</param>
        public void UnionCell(int col1, int row1, int col2, int row2)
        {
            _ew.UnionCell(col1, row1, col2, row2);
        }

        public void UnionCell(int col1, int row1, int col2, int row2, ICellStyle st, string val)
        {

        }

         /// <summary>
        /// Установка ширины столбцов
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Width"></param>

        public void SetColumnWidth(int col, int row, double Width)
        {
            _ew.SetColumnWidth(col, row, Width);
        }

        public void SetColumnWidth(int col, int length_row)
        {

        }


         /// <summary>
        /// Установка направления текста
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Orientation"></param>
        public void SetTextOrientation(int col, int row, int Orientation)
        {
            _ew.SetTextOrientation(col,row,Orientation);
        }

         /// <summary>
        /// Выравнивание текста в ячейке по вертикали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetVerticalAlignment(int col, int row, int Alignment)
        {
            _ew.SetVerticalAlignment(col,row,Alignment);
        }

        /// <summary>
        /// Выравнивание текста в ячейке по горизонтали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetHorisontalAlignment(int col, int row, int Alignment)
        {
            _ew.SetHorisontalAlignment(col,row,Alignment);
        }

         /// <summary>
        /// Установка высоты строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Height"></param>
        public void SetRowHeight(int col, int row, double Height)
        {
            _ew.SetRowHeight(col,row,Height);
        }


         /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetBorderStyle(int col, int row, int Style)
        {
            _ew.SetBorderStyle(col, row, Style);
        }

        /// <summary>
        /// Установить вид границ
        /// </summary>
        /// <param name="col1"></param>
        /// <param name="row1"></param>
        /// <param name="col2"></param>
        /// <param name="row2"></param>
        /// <param name="Size"></param>
        public void SetBorderStyle(int col1, int row1, int col2, int row2, int Style)
        {
            _ew.SetBorderStyle(col1, row1,col2,row2, Style);
        }
                
        /// <summary>
        /// Сохранить файл
        /// </summary>
        public void Save()
        {
            _ew.Save(_fileName);
        }

        /// <summary>
        /// Освободить все ресурсы
        /// </summary>
        public void Close()
        {
            if (_ew != null)
            {
                _ew.Close();
                _ew = null;
            }
        }

        public ICellStyle GetCellStyleBorder()
        {
            return _ew.GetCellStyleBorder();
        }

        public ICellStyle GetCellStyleHead()
        {
            return _ew.GetCellStyleHead();
        }

        public ICellStyle GetCellStyleBackgroundColor()
        {
           
            return _ew.GetCellStyleBackgroundColor();
        }


        public ICellStyle GetCellStyleBackgroundDefaultColor()
        {
            return _ew.GetCellStyleBackgroundDefaultColor(); 
        }


        public ICellStyle GetCellStyleDefault()
        {
          
            return _ew.GetCellStyleDefault();
        }

        public void SetCellStyle(int col, int row, ICellStyle st)
        {
            //
        }

        public void SetBackgroundColor()
        {
            //
        }

        public void SetBorderStyle()
        {
            //
        }

        public void SetBorderStyleDefault()
        {
            //
        }

        #endregion
    }
}
