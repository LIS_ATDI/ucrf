﻿using System;

namespace XICSM.UcrfRfaNET.Reports
{
    /// <summary>
    /// Типы отчетов
    /// </summary>
    enum ReportType
    {
        Csv,    //CSV файл 
        Excel,  //Excel файл
        ExcelFast,  //Excel с прямым методом записи в файл

    }
    /// <summary>
    /// Класс-фабрика отчетов
    /// </summary>
    class Report
    {
        public static IReport CreateReport(ReportType repType)
        {
            switch(repType)
            {
                case ReportType.Csv:
                    return new ReportCsv();
                case ReportType.Excel:
                    return new ReportExcel();
                case ReportType.ExcelFast:
                    return new ReportExcelFast();
                default:
                    throw new Exception("Error type:" + repType);
            }
        }
    }
}
