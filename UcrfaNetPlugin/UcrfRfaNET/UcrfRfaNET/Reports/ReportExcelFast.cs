﻿using System;
using System.IO;
using System.Linq;
using Lis.OfficeBridge;
using System.Drawing;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;


namespace XICSM.UcrfRfaNET.Reports
{
    class ReportExcelFast : IReport
    {
        private ExcelFastNPOI _ew = null;
        private IWorkbook _wk = null;
        private int _indexRow;
        private string _fileName;
        #region Implementation of IDisposable

        public void Dispose()
        {
            Close();
        }

        #endregion

        #region Implementation of IReport

        

        /// <summary>
        /// Установить текущий номер строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public void SetIndexRow(int Value)
        {
            _indexRow = Value;
        }

        /// <summary>
        /// Получить  номер текущей строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Size"></param>
        public int GetIndexRow()
        {
           return _indexRow;
        }
    
        /// <summary>
        /// Инициализация генератора
        /// </summary>
        /// <param name="fileName">Полный путь к файлу. Если разширение файла отсутствует, то оно добавиться автоматически</param>
        public void Init(string fileName,string NameSheet, string separator)
        {
            if (string.IsNullOrEmpty(Path.GetExtension(fileName)))
                fileName += ".xlsx";
            _fileName = fileName;
            _indexRow = 0;
            _ew = new ExcelFastNPOI();
            _ew.AddWorkbook(NameSheet);
            _wk = _ew.GetWorkbookEx();
        }

        /// <summary>
        /// Добавить новую палитру
        /// </summary>
        /// <param name="NameSheet"></param>
        public void AddSheet(string NameSheet)
        {
            _indexRow = 0;
            _ew.AddWorkbookEx(_ew.oWorkbook_, NameSheet);
        }

        public void SetCellStyle(int col, int row, ICellStyle st)
        {
            _ew.SetCellStyle(col, row, st);
        }

        /// <summary>
        /// Записть строку в файл
        /// </summary>
        /// <param name="columns">список значенией колонок</param>
        public void WriteLine(params string[] columns)
        {
            _ew.CreateNewRow(GetIndexRow());
             for (int j = 0; j < columns.Count(); j++)
                 {
                     //if ((j == 2) || (j == 9)) { _ew.SetCellValue(j, GetIndexRow(), columns[j], _ew.GetCellStyleCustom()); }
                     if ((j == 2) ) { _ew.SetCellValue(j, GetIndexRow(), columns[j], _ew.GetCellStyleCustom()); }
                     else { _ew.SetCellValue(j, GetIndexRow(), columns[j], _ew.GetCellStyleBorder()); }
                 }
             _ew.ReleaseRow();
            _indexRow++;
        }


        public void SetBorderStyle(int col1, int row1, int col2, int row2, int Style)
        {
            //
        }
     

        public void SetCellFormat(int col, int row, CellFormat cellFormat)
        {
            //
        }

        public void SetCellFontSize(int col, int row, string Name, int Size)
        {
            //
        }

        public void SetBorderStyle(int col, int row, int Style)
        {
            //
        }

        public void SetBackgroundColor()
        {
            //
        }

        public void SetBorderStyle()
        {
            //
        }

        public void SetBorderStyleDefault()
        {
            //
        }


        /// <summary>
        ///  Установить содержимое ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="val">Содержимое ячейки</param>
        public void SetCellValue(int col, int row, string val,ICellStyle st)
        {
            _ew.SetCellValue(col, row, val, st);
        }


        public void SetCellValue(int col, int row, string val)
        {

        }
      

        /// <summary>
        /// Сделать объединение ячеек
        /// </summary>
        /// <param name="columns">список значенией колонок</param>
        public void UnionCell(int col1, int row1, int col2, int row2, ICellStyle st, string val)
        {
            _ew.UnionCell(col1, row1, col2, row2,st, val);
        }

        public void UnionCell(int col1, int row1, int col2, int row2)
        {

        }

         /// <summary>
        /// Установка ширины столбцов
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Width"></param>

        public void SetColumnWidth(int col, int length_row)
        {
            _ew.SetColumnWidth(col, length_row);
        }


        public void SetColumnWidth(int col, int row, double Width)
        {
            //
        }

         /// <summary>
        /// Установка направления текста
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Orientation"></param>
        public void SetTextOrientation(int col, int row, int Orientation)
        {
            
        }

         /// <summary>
        /// Выравнивание текста в ячейке по вертикали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetVerticalAlignment(int col, int row, int Alignment)
        {
          
        }

        /// <summary>
        /// Выравнивание текста в ячейке по горизонтали
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Alignment"></param>
        public void SetHorisontalAlignment(int col, int row, int Alignment)
        {
            
        }

         /// <summary>
        /// Установка высоты строки
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="Height"></param>
        public void SetRowHeight(int col, int row, double Height)
        {
            
        }


           /// <summary>
        /// Установить цвет фона для ячейки
        /// </summary>
        /// <param name="col">Номер колонки, начинается с 1</param>
        /// <param name="row">Номер строки, начинается с 1</param>
        /// <param name="color">цвет фона</param>
        public void SetCellBackgroundColor(int col, int row, Color color)
        {
            _ew.SetCellBackgroundColor(col, row, color);
        }
               
        /// <summary>
        /// Сохранить файл
        /// </summary>
        public void Save()
        {
            _ew.Save(_fileName);
        }


       
        /// <summary>
        /// Освободить все ресурсы
        /// </summary>
        public void Close()
        {
            if (_ew != null)
            {
                _ew.Close();
                _ew = null;
            }
        }

        public ICellStyle GetCellStyleBorder()
        {
            return _ew.GetCellStyleBorder();
        }

        public ICellStyle GetCellStyleHead()
        {
            return _ew.GetCellStyleHead();
        }

        public ICellStyle GetCellStyleBackgroundColor()
        {
            return _ew.GetCellStyleBackgroundColor();
        }

        public ICellStyle GetCellStyleBackgroundDefaultColor()
        {
            return _ew.GetCellStyleBackgroundDefaultColor();
        }

        public ICellStyle GetCellStyleDefault()
        {
            return _ew.GetCellStyleDefault();
        }

        #endregion
    }
}
