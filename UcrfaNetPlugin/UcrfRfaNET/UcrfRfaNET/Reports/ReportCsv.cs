﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.OfficeBridge;
using System.Drawing;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;


namespace XICSM.UcrfRfaNET.Reports
{
    class ReportCsv : IReport
    {
        private StreamWriter _wr = null;
        private string _separator = ";";
        #region Implementation of IReport


        public void Init(string fileName, string NameSheet, string separator)
        {
            if (separator == "")
                _separator = PluginSetting.PluginFolderSetting.Separator;
            else  _separator = separator;

            if (string.IsNullOrEmpty(Path.GetExtension(fileName)))
                fileName += ".csv";
            _wr = new StreamWriter(fileName, false, Encoding.Default);
        }

        /// <summary>
        /// Добавить новую палитру
        /// </summary>
        /// <param name="NameSheet"></param>
        public void AddSheet(string NameSheet)
        {

        }

        
        public void SetIndexRow(int Value)
        {
            /// заглушка
        }

      
        public int GetIndexRow()
        {
            /// заглушка
            return 0;
        }



        public void SetCellFontSize(int col, int row,string Name, int Size)
        {
            /// заглушка
        }

        public void UnionCell(int col1, int row1, int col2, int row2)
        {
            /// заглушка
        }

        public void UnionCell(int col1, int row1, int col2, int row2, ICellStyle st, string val)
        {

        }
        
        


       
        public void SetCellFormat(int col, int row, CellFormat cellFormat)
        {
            /// заглушка
        }

     
        public void SetCellValue(int col, int row, string val)
        {
            /// заглушка
        }

       
        public void SetCellBackgroundColor(int col, int row, Color color)
        {
            /// заглушка
        }

       
        public void SetCellForegroundColor(int col, int row, Color color)
        {
            /// заглушка
        }

        

        public void SetColumnWidth(int col, int row, double Width)
        {
            /// заглушка
        }

        public void SetColumnWidth(int col, int length_row)
        {
            /// заглушка
        }


      
        public void SetTextOrientation(int col, int row, int Orientation)
        {
            /// заглушка
        }


        public void SetVerticalAlignment(int col, int row, int Alignment)
        {
            /// заглушка
        }

        
        public void SetHorisontalAlignment(int col, int row, int Alignment)
        {
            /// заглушка
        }

       
        public void SetRowHeight(int col, int row, double Height)
        {
            /// заглушка
        }


        public void SetBorderStyle(int col, int row, int Style)
        {
            /// заглушка
        }

        public void SetBorderStyle(int col1, int row1, int col2, int row2, int Style)
        {
            /// заглушка
        }
        public void WriteLine(params string[] columns)
        {
            if (_wr == null)
                throw new Exception("Class is not inited. Call function Init()");
            foreach (string column in columns)
            {
                _wr.Write(column);
                _wr.Write(_separator);
            }
            _wr.WriteLine();
        }

        public void Save()
        {
            if (_wr == null)
                throw new Exception("Class is not inited. Call function Init()");
            _wr.Flush();
        }

        public void Close()
        {
            if (_wr != null)
            {
                _wr.Close();
                _wr.Dispose();
                _wr = null;
            }
        }

        public ICellStyle GetCellStyleBorder()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleHead()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleBackgroundColor()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleDefault()
        {
            ICellStyle cr = null;
            return cr;
        }

        public ICellStyle GetCellStyleBackgroundDefaultColor()
        {
            ICellStyle cr = null;
            return cr;
        }

        public void SetCellStyle(int col, int row, ICellStyle st)
        {
            //
        }

        public void SetBackgroundColor()
        {
            //
        }

        public void SetBorderStyle()
        {
            //
        }

        public void SetBorderStyleDefault()
        {
            //
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            Close();
        }

        #endregion
    }
}
