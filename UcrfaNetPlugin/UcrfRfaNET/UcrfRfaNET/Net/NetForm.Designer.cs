﻿namespace XICSM.UcrfRfaNET.Net
{
    partial class NetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNetName = new System.Windows.Forms.TextBox();
            this.tbCallSign = new System.Windows.Forms.TextBox();
            this.tbOwner = new System.Windows.Forms.TextBox();
            this.btnSelectOwner = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gbStations = new System.Windows.Forms.GroupBox();
            this.dgStation = new System.Windows.Forms.DataGridView();
            this.btCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnAddBs = new System.Windows.Forms.Button();
            this.btnAddAbonent = new System.Windows.Forms.Button();
            this.btnDeleteStation = new System.Windows.Forms.Button();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbCountBase = new System.Windows.Forms.TextBox();
            this.tbCountStationary = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCountAbonent = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbCountAll = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbCountAllActived = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAutoSetArticle = new System.Windows.Forms.CheckBox();
            this.numWorkCount = new System.Windows.Forms.NumericUpDown();
            this.btnWorkCountUpdate = new System.Windows.Forms.Button();
            this.tbWorkCountSuggest = new System.Windows.Forms.TextBox();
            this.btnArticleUpdate = new System.Windows.Forms.Button();
            this.tbArticleSuggest = new System.Windows.Forms.TextBox();
            this.cbArticle = new System.Windows.Forms.ComboBox();
            this.btnConfirmArticle = new System.Windows.Forms.Button();
            this.gbConfirmArticle = new System.Windows.Forms.GroupBox();
            this.lblDateConfirmedArticle = new System.Windows.Forms.Label();
            this.lblUserConfirmedArticle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbStations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStation)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount)).BeginInit();
            this.gbConfirmArticle.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Назва мережі";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Позивний мережі";
            // 
            // tbNetName
            // 
            this.tbNetName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNetName.Location = new System.Drawing.Point(118, 12);
            this.tbNetName.Name = "tbNetName";
            this.tbNetName.Size = new System.Drawing.Size(496, 20);
            this.tbNetName.TabIndex = 1;
            // 
            // tbCallSign
            // 
            this.tbCallSign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCallSign.Location = new System.Drawing.Point(118, 38);
            this.tbCallSign.Name = "tbCallSign";
            this.tbCallSign.Size = new System.Drawing.Size(496, 20);
            this.tbCallSign.TabIndex = 3;
            // 
            // tbOwner
            // 
            this.tbOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOwner.Location = new System.Drawing.Point(118, 64);
            this.tbOwner.Name = "tbOwner";
            this.tbOwner.ReadOnly = true;
            this.tbOwner.Size = new System.Drawing.Size(460, 20);
            this.tbOwner.TabIndex = 5;
            // 
            // btnSelectOwner
            // 
            this.btnSelectOwner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectOwner.Location = new System.Drawing.Point(584, 62);
            this.btnSelectOwner.Name = "btnSelectOwner";
            this.btnSelectOwner.Size = new System.Drawing.Size(30, 23);
            this.btnSelectOwner.TabIndex = 6;
            this.btnSelectOwner.Text = "...";
            this.btnSelectOwner.UseVisualStyleBackColor = true;
            this.btnSelectOwner.Click += new System.EventHandler(this.btnSelectOwner_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Власник мережі";
            // 
            // gbStations
            // 
            this.gbStations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStations.Controls.Add(this.dgStation);
            this.gbStations.Location = new System.Drawing.Point(12, 119);
            this.gbStations.Name = "gbStations";
            this.gbStations.Size = new System.Drawing.Size(797, 411);
            this.gbStations.TabIndex = 9;
            this.gbStations.TabStop = false;
            this.gbStations.Text = "Станції мережі";
            // 
            // dgStation
            // 
            this.dgStation.AllowUserToAddRows = false;
            this.dgStation.AllowUserToDeleteRows = false;
            this.dgStation.BackgroundColor = System.Drawing.Color.White;
            this.dgStation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgStation.Location = new System.Drawing.Point(3, 16);
            this.dgStation.Name = "dgStation";
            this.dgStation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStation.Size = new System.Drawing.Size(791, 392);
            this.dgStation.TabIndex = 0;
            this.dgStation.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgStation_CellMouseDoubleClick);
            this.dgStation.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgStation_RowPrePaint);
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(826, 569);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 25;
            this.btCancel.Text = "Відмінити";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(720, 569);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 24;
            this.btnOk.Text = "Зберегти";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnAddBs
            // 
            this.btnAddBs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBs.Location = new System.Drawing.Point(824, 254);
            this.btnAddBs.Name = "btnAddBs";
            this.btnAddBs.Size = new System.Drawing.Size(75, 23);
            this.btnAddBs.TabIndex = 21;
            this.btnAddBs.Text = "БС/АС";
            this.btnAddBs.UseVisualStyleBackColor = true;
            this.btnAddBs.Click += new System.EventHandler(this.btnAddBs_Click);
            // 
            // btnAddAbonent
            // 
            this.btnAddAbonent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAbonent.Location = new System.Drawing.Point(824, 283);
            this.btnAddAbonent.Name = "btnAddAbonent";
            this.btnAddAbonent.Size = new System.Drawing.Size(75, 23);
            this.btnAddAbonent.TabIndex = 22;
            this.btnAddAbonent.Text = "Абонент";
            this.btnAddAbonent.UseVisualStyleBackColor = true;
            this.btnAddAbonent.Click += new System.EventHandler(this.btnAddAbonent_Click);
            // 
            // btnDeleteStation
            // 
            this.btnDeleteStation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteStation.Location = new System.Drawing.Point(824, 342);
            this.btnDeleteStation.Name = "btnDeleteStation";
            this.btnDeleteStation.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteStation.TabIndex = 23;
            this.btnDeleteStation.Text = "Видалити";
            this.btnDeleteStation.UseVisualStyleBackColor = true;
            this.btnDeleteStation.Click += new System.EventHandler(this.btnDeleteStation_Click);
            // 
            // tbNote
            // 
            this.tbNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNote.Location = new System.Drawing.Point(118, 93);
            this.tbNote.Name = "tbNote";
            this.tbNote.Size = new System.Drawing.Size(496, 20);
            this.tbNote.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Нотатки";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 548);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Кіл-ть БС";
            // 
            // tbCountBase
            // 
            this.tbCountBase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCountBase.BackColor = System.Drawing.SystemColors.Control;
            this.tbCountBase.Location = new System.Drawing.Point(71, 545);
            this.tbCountBase.Name = "tbCountBase";
            this.tbCountBase.ReadOnly = true;
            this.tbCountBase.Size = new System.Drawing.Size(50, 20);
            this.tbCountBase.TabIndex = 12;
            this.tbCountBase.TabStop = false;
            this.tbCountBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCountStationary
            // 
            this.tbCountStationary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCountStationary.BackColor = System.Drawing.SystemColors.Control;
            this.tbCountStationary.Location = new System.Drawing.Point(71, 572);
            this.tbCountStationary.Name = "tbCountStationary";
            this.tbCountStationary.ReadOnly = true;
            this.tbCountStationary.Size = new System.Drawing.Size(50, 20);
            this.tbCountStationary.TabIndex = 14;
            this.tbCountStationary.TabStop = false;
            this.tbCountStationary.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 575);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Кіл-ть СА";
            // 
            // tbCountAbonent
            // 
            this.tbCountAbonent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCountAbonent.BackColor = System.Drawing.SystemColors.Control;
            this.tbCountAbonent.Location = new System.Drawing.Point(209, 545);
            this.tbCountAbonent.Name = "tbCountAbonent";
            this.tbCountAbonent.ReadOnly = true;
            this.tbCountAbonent.Size = new System.Drawing.Size(50, 20);
            this.tbCountAbonent.TabIndex = 16;
            this.tbCountAbonent.TabStop = false;
            this.tbCountAbonent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(136, 548);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Кіл-ть Абон.";
            // 
            // tbCountAll
            // 
            this.tbCountAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCountAll.BackColor = System.Drawing.SystemColors.Control;
            this.tbCountAll.Location = new System.Drawing.Point(209, 572);
            this.tbCountAll.Name = "tbCountAll";
            this.tbCountAll.ReadOnly = true;
            this.tbCountAll.Size = new System.Drawing.Size(50, 20);
            this.tbCountAll.TabIndex = 18;
            this.tbCountAll.TabStop = false;
            this.tbCountAll.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(136, 575);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Всього";
            // 
            // tbCountAllActived
            // 
            this.tbCountAllActived.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCountAllActived.BackColor = System.Drawing.SystemColors.Control;
            this.tbCountAllActived.Location = new System.Drawing.Point(276, 572);
            this.tbCountAllActived.Name = "tbCountAllActived";
            this.tbCountAllActived.ReadOnly = true;
            this.tbCountAllActived.Size = new System.Drawing.Size(89, 20);
            this.tbCountAllActived.TabIndex = 20;
            this.tbCountAllActived.TabStop = false;
            this.tbCountAllActived.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(273, 543);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Всього активних";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbAutoSetArticle);
            this.groupBox1.Controls.Add(this.numWorkCount);
            this.groupBox1.Controls.Add(this.btnWorkCountUpdate);
            this.groupBox1.Controls.Add(this.tbWorkCountSuggest);
            this.groupBox1.Controls.Add(this.btnArticleUpdate);
            this.groupBox1.Controls.Add(this.tbArticleSuggest);
            this.groupBox1.Controls.Add(this.cbArticle);
            this.groupBox1.Location = new System.Drawing.Point(620, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 101);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Стаття та кіл-сть робіт №1";
            // 
            // cbAutoSetArticle
            // 
            this.cbAutoSetArticle.AutoSize = true;
            this.cbAutoSetArticle.Location = new System.Drawing.Point(6, 78);
            this.cbAutoSetArticle.Name = "cbAutoSetArticle";
            this.cbAutoSetArticle.Size = new System.Drawing.Size(273, 17);
            this.cbAutoSetArticle.TabIndex = 6;
            this.cbAutoSetArticle.Text = "Автоматично встановлювати статтю та кількість";
            this.cbAutoSetArticle.UseVisualStyleBackColor = true;
            // 
            // numWorkCount
            // 
            this.numWorkCount.Location = new System.Drawing.Point(6, 49);
            this.numWorkCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numWorkCount.Name = "numWorkCount";
            this.numWorkCount.Size = new System.Drawing.Size(121, 20);
            this.numWorkCount.TabIndex = 3;
            // 
            // btnWorkCountUpdate
            // 
            this.btnWorkCountUpdate.Location = new System.Drawing.Point(142, 46);
            this.btnWorkCountUpdate.Name = "btnWorkCountUpdate";
            this.btnWorkCountUpdate.Size = new System.Drawing.Size(38, 23);
            this.btnWorkCountUpdate.TabIndex = 4;
            this.btnWorkCountUpdate.Text = "<-";
            this.btnWorkCountUpdate.UseVisualStyleBackColor = true;
            this.btnWorkCountUpdate.Click += new System.EventHandler(this.btnWorkCountUpdate_Click);
            // 
            // tbWorkCountSuggest
            // 
            this.tbWorkCountSuggest.Location = new System.Drawing.Point(195, 46);
            this.tbWorkCountSuggest.Name = "tbWorkCountSuggest";
            this.tbWorkCountSuggest.ReadOnly = true;
            this.tbWorkCountSuggest.Size = new System.Drawing.Size(80, 20);
            this.tbWorkCountSuggest.TabIndex = 5;
            // 
            // btnArticleUpdate
            // 
            this.btnArticleUpdate.Location = new System.Drawing.Point(142, 19);
            this.btnArticleUpdate.Name = "btnArticleUpdate";
            this.btnArticleUpdate.Size = new System.Drawing.Size(38, 23);
            this.btnArticleUpdate.TabIndex = 1;
            this.btnArticleUpdate.Text = "<-";
            this.btnArticleUpdate.UseVisualStyleBackColor = true;
            this.btnArticleUpdate.Click += new System.EventHandler(this.btnArticleUpdate_Click);
            // 
            // tbArticleSuggest
            // 
            this.tbArticleSuggest.Location = new System.Drawing.Point(195, 19);
            this.tbArticleSuggest.Name = "tbArticleSuggest";
            this.tbArticleSuggest.ReadOnly = true;
            this.tbArticleSuggest.Size = new System.Drawing.Size(80, 20);
            this.tbArticleSuggest.TabIndex = 2;
            // 
            // cbArticle
            // 
            this.cbArticle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArticle.FormattingEnabled = true;
            this.cbArticle.Location = new System.Drawing.Point(6, 19);
            this.cbArticle.Name = "cbArticle";
            this.cbArticle.Size = new System.Drawing.Size(121, 21);
            this.cbArticle.TabIndex = 0;
            // 
            // btnConfirmArticle
            // 
            this.btnConfirmArticle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirmArticle.ForeColor = System.Drawing.Color.Red;
            this.btnConfirmArticle.Location = new System.Drawing.Point(817, 119);
            this.btnConfirmArticle.Name = "btnConfirmArticle";
            this.btnConfirmArticle.Size = new System.Drawing.Size(84, 62);
            this.btnConfirmArticle.TabIndex = 26;
            this.btnConfirmArticle.Text = "Підтвердити статтю";
            this.btnConfirmArticle.UseVisualStyleBackColor = true;
            this.btnConfirmArticle.Click += new System.EventHandler(this.btnConfirmArticle_Click);
            // 
            // gbConfirmArticle
            // 
            this.gbConfirmArticle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbConfirmArticle.Controls.Add(this.lblDateConfirmedArticle);
            this.gbConfirmArticle.Controls.Add(this.lblUserConfirmedArticle);
            this.gbConfirmArticle.Location = new System.Drawing.Point(405, 536);
            this.gbConfirmArticle.Name = "gbConfirmArticle";
            this.gbConfirmArticle.Size = new System.Drawing.Size(260, 56);
            this.gbConfirmArticle.TabIndex = 27;
            this.gbConfirmArticle.TabStop = false;
            this.gbConfirmArticle.Text = "Підтвердив статтю";
            // 
            // lblDateConfirmedArticle
            // 
            this.lblDateConfirmedArticle.AutoSize = true;
            this.lblDateConfirmedArticle.Location = new System.Drawing.Point(6, 36);
            this.lblDateConfirmedArticle.Name = "lblDateConfirmedArticle";
            this.lblDateConfirmedArticle.Size = new System.Drawing.Size(116, 13);
            this.lblDateConfirmedArticle.TabIndex = 1;
            this.lblDateConfirmedArticle.Text = "lblDateConfirmedArticle";
            // 
            // lblUserConfirmedArticle
            // 
            this.lblUserConfirmedArticle.AutoSize = true;
            this.lblUserConfirmedArticle.Location = new System.Drawing.Point(6, 16);
            this.lblUserConfirmedArticle.Name = "lblUserConfirmedArticle";
            this.lblUserConfirmedArticle.Size = new System.Drawing.Size(115, 13);
            this.lblUserConfirmedArticle.TabIndex = 0;
            this.lblUserConfirmedArticle.Text = "lblUserConfirmedArticle";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 557);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "(+ не УКХ)";
            // 
            // NetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 604);
            this.Controls.Add(this.gbConfirmArticle);
            this.Controls.Add(this.btnConfirmArticle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbCountAllActived);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbCountAll);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbCountAbonent);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbCountStationary);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbCountBase);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbNote);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnDeleteStation);
            this.Controls.Add(this.btnAddAbonent);
            this.Controls.Add(this.btnAddBs);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.gbStations);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSelectOwner);
            this.Controls.Add(this.tbOwner);
            this.Controls.Add(this.tbCallSign);
            this.Controls.Add(this.tbNetName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.MinimumSize = new System.Drawing.Size(880, 450);
            this.Name = "NetForm";
            this.Text = "Мережа";
            this.gbStations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgStation)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount)).EndInit();
            this.gbConfirmArticle.ResumeLayout(false);
            this.gbConfirmArticle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNetName;
        private System.Windows.Forms.TextBox tbCallSign;
        private System.Windows.Forms.TextBox tbOwner;
        private System.Windows.Forms.Button btnSelectOwner;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbStations;
        private System.Windows.Forms.DataGridView dgStation;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnAddBs;
        private System.Windows.Forms.Button btnAddAbonent;
        private System.Windows.Forms.Button btnDeleteStation;
        private System.Windows.Forms.TextBox tbNote;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbCountBase;
        private System.Windows.Forms.TextBox tbCountStationary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCountAbonent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbCountAll;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbCountAllActived;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numWorkCount;
        private System.Windows.Forms.Button btnWorkCountUpdate;
        private System.Windows.Forms.TextBox tbWorkCountSuggest;
        private System.Windows.Forms.Button btnArticleUpdate;
        private System.Windows.Forms.TextBox tbArticleSuggest;
        private System.Windows.Forms.ComboBox cbArticle;
        private System.Windows.Forms.CheckBox cbAutoSetArticle;
        private System.Windows.Forms.Button btnConfirmArticle;
        private System.Windows.Forms.GroupBox gbConfirmArticle;
        private System.Windows.Forms.Label lblDateConfirmedArticle;
        private System.Windows.Forms.Label lblUserConfirmedArticle;
        private System.Windows.Forms.Label label4;
    }
}