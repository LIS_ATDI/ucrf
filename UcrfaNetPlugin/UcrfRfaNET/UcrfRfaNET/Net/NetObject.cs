﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ICSM;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.UtilityClass;
using System.Windows;

namespace XICSM.UcrfRfaNET.Net
{
    class NetObject : NotifyPropertyChanged
    {
        /// <summary>
        /// Запускает алгоритм выбора базовых станции
        /// </summary>
        /// <param name="ownerId">Id владельца</param>
        /// <param name="excludeId">Исключить ID</param>
        /// <returns>RecordPtr</returns>
        public static RecordPtr SelectBase(int ownerId, params int[] excludeId)
        {
            RecordPtr retVal = new RecordPtr("", IM.NullI);
            {
                string param0 = "0";
                foreach (int exclId in excludeId)
                {
                    if (string.IsNullOrEmpty(param0) == false)
                        param0 += ",";
                    param0 += exclId;
                }
                if (string.IsNullOrEmpty(param0) == false)
                    param0 = string.Format("(([{1}] NOT IN ({0})) OR ([{2}] NOT IN ({0})))", param0, "MobileSector1.ID", "BaseSector1.ID");

                string param1 = string.Format("([{0}] LIKE '{1}')", "OBJ_TABLE", ICSMTbl.itblMobStation);
                string param2 = string.Format("([{0}] = {1})", "MobileSector1.OWNER_ID", ownerId);
                string param3 = string.Format("({0} AND {1})", param1, param2);
                string param4 = string.Format("([{0}] LIKE '{1}')", "OBJ_TABLE", ICSMTbl.itblMobStation2);
                string param5 = string.Format("([{0}] = {1})", "BaseSector1.OWNER_ID", ownerId);
                string param6 = string.Format("({0} AND {1})", param4, param5);
                string param7 = string.Format("({0} OR {1}) AND {2}", param3, param6, param0);
                RecordPtr retRec = ApplSource.BaseAppClass.SelectRecord("Базові станції",
                                                                        PlugTbl.APPL,
                                                                        param7,
                                                                        "",
                                                                        OrderDirection.Ascending);
                if ((retRec.Id > 0) && (retRec.Id != IM.NullI))
                {
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,OBJ_TABLE,OBJ_ID1");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, retRec.Id);
                        rs.Open();
                        if (!rs.IsEOF())
                        {
                            retVal.Id = rs.GetI("OBJ_ID1");
                            retVal.Table = rs.GetS("OBJ_TABLE");
                        }
                    }
                }
            }
            return retVal;
        }
        /// <summary>
        /// Запускает алгоритм выбора абонентской станции
        /// </summary>
        /// <param name="ownerId">Id владельца</param>
        /// <param name="netId">Id текущей сети</param>
        /// <returns>RecordPtr</returns>
        public static RecordPtr SelectAbonent(int ownerId, int netId)
        {
            RecordPtr retVal = new RecordPtr(PlugTbl.XfaAbonentStation, IM.NullI);
            {
                string param = "{NAME=\"*" + HelpClasses.HelpFunction.ReplaceQuotaSumbols("") + "*\"}";
                RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Select an owner"), ICSMTbl.USERS, param);
                if ((user.Id > 0) && (user.Id != IM.NullI))
                    ownerId = user.Id;
            }
            {
                string param1 = string.Format("([{0}] {2} {1})", "Abonent.USER_ID", ownerId, (ownerId == IM.NullI) ? "<" : "=");
                string param2 = string.Format("(([{0}] <> {1}) or ([{0}] is NULL))", "Abonent.NET_ID", netId);
                string param4 = string.Format("([{0}] <> {1})", "Abonent.ID", IM.NullI);
                string param3 = string.Format("({0} AND {1} AND {2})", param1, param2, param4);
                RecordPtr retRec = ApplSource.BaseAppClass.SelectRecord("Абоненти",
                                                                        PlugTbl.APPL,
                                                                        param3,
                                                                        "Abonent.USE_REGION",
                                                                        OrderDirection.Ascending);
                if ((retRec.Id > 0) && (retRec.Id != IM.NullI))
                {
                    string netName = "";
                    int connectedNet = IM.NullI;
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,Abonent.ID,Abonent.NET_ID,Abonent.Net.NAME");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, retRec.Id);
                        rs.Open();
                        if(!rs.IsEOF())
                        {
                            connectedNet = rs.GetI("Abonent.NET_ID");
                            netName = rs.GetS("Abonent.Net.NAME");
                            retVal.Id = rs.GetI("Abonent.ID"); ;
                        }
                    }
                    string mess = string.Format("Вибрана станція приєднана до мережі '{0}'.{1}", netName,
                                                Environment.NewLine);
                    mess += "Станція буде від’єднання від мережі. Продовжити?";
                    if ((connectedNet != IM.NullI) &&
                        (System.Windows.MessageBox.Show(mess, "", System.Windows.MessageBoxButton.YesNo,
                                                        System.Windows.MessageBoxImage.Question) != System.Windows.MessageBoxResult.Yes))
                        retVal.Id = IM.NullI;
                }
            }
            return retVal;
        }
        //=======================================
        /// <summary>
        /// Возвращает название сети
        /// </summary>
        /// <param name="netId">Id сети</param>
        /// <returns>Название сети</returns>
        public static string GetNetName(int netId)
        {
            string retVal = "";
            using (LisRecordSet rs = new LisRecordSet(TableName, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID,NAME,CALL_SIGN");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, netId);
                rs.Open();
                if (!rs.IsEOF())
                    retVal = rs.GetS("NAME");
            }
            return retVal;
        }
        //=======================================
        /// <summary>
        /// Возвращает ID сетей, к которым привязана станция
        /// </summary>
        /// <param name="stationId">Id станции</param>
        /// <param name="tableName">Название таблицы</param>
        /// <returns>список сетей</returns>
        public static int[] GetConnectedNets(int stationId, string tableName)
        {
            List<int> retVal = new List<int>();
            switch (tableName)
            {
                case PlugTbl.XfaAbonentStation:
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,NET_ID");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            int netId = rs.GetI("NET_ID");
                            retVal.Add(netId);
                        }
                    }
                    break;
                case ICSMTbl.itblMobStation:
                case ICSMTbl.itblMobStation2:
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite))
                    {
                        rs.Select("ID,NET_ID");
                        rs.SetWhere("STATION_ID", IMRecordset.Operation.Eq, stationId);
                        rs.SetWhere("STATION_TABLE", IMRecordset.Operation.Like, tableName);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            int netId = rs.GetI("NET_ID");
                            retVal.Add(netId);
                        }
                    }
                    break;
            }
            return retVal.ToArray();
        }
        //=======================================
        /// <summary>
        /// Возвращает названия сетей, к которым привязана станция
        /// </summary>
        /// <param name="stationId">Id станции</param>
        /// <param name="tableName">Название таблицы</param>
        /// <returns>список сетей</returns>
        public static string[] GetConnectedNetsName(int stationId, string tableName)
        {
            List<string> retVal = new List<string>();
            switch (tableName)
            {
                case PlugTbl.XfaAbonentStation:
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadOnly))
                    {
                        rs.Select("ID,Net.NAME");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            string netName = rs.GetS("Net.NAME");
                            retVal.Add(netName);
                        }
                    }
                    break;
                case ICSMTbl.itblMobStation:
                case ICSMTbl.itblMobStation2:
                    using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite))
                    {
                        rs.Select("ID,Net.NAME");
                        rs.SetWhere("STATION_ID", IMRecordset.Operation.Eq, stationId);
                        rs.SetWhere("STATION_TABLE", IMRecordset.Operation.Like, tableName);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            string netName = rs.GetS("Net.NAME");
                            retVal.Add(netName);
                        }
                    }
                    break;
            }
            return retVal.ToArray();
        }

        private UcrfDepartment curDept;
        private const string TableName = PlugTbl.XfaNet;
        //--
        public const string FieldId = "Id";
        private int _id = IM.NullI;
        public int Id
        {
            get { return GetThreadSafeValue(ref _id); }
            set { SetThreadSafeValue(ref _id, value, FieldId); }
        }
        //--
        public const string FieldName = "Name";
        private string _name = "";
        public string Name
        {
            get { return GetThreadSafeValue(ref _name); }
            set { SetThreadSafeValue(ref _name, value, FieldName); }
        }
        //--
        public const string FieldCallSign = "CallSign";
        private string _callSign = "";
        public string CallSign
        {
            get { return GetThreadSafeValue(ref _callSign); }
            set { SetThreadSafeValue(ref _callSign, value, FieldCallSign); }
        }
        //--
        public const string FieldOwnerId = "OwnerId";
        private int _ownerId = IM.NullI;
        public int OwnerId
        {
            get { return GetThreadSafeValue(ref _ownerId); }
            set { SetThreadSafeValue(ref _ownerId, value, FieldOwnerId); }
        }
        //--
        public const string FieldNeedSetArticleAuto = "NeedSetArticleAuto";
        private bool _needSetArticleAuto = true;
        public bool NeedSetArticleAuto
        {
            get { return GetThreadSafeValue(ref _needSetArticleAuto); }
            set { SetThreadSafeValue(ref _needSetArticleAuto, value, FieldNeedSetArticleAuto); }
        }
        //--
        public const string FieldArticleId = "ArticleId";
        private int _articleId = IM.NullI;
        public int ArticleId
        {
            get { return GetThreadSafeValue(ref _articleId); }
            set
            {
                SetThreadSafeValue(ref _articleId, value, FieldArticleId);
                ClearConfirmArticle();
            }
        }
        //--
        public const string FieldWorkCountNotNull = "WorkCountNotNull";
        public int WorkCountNotNull
        {
            get
            {
                int work = WorkCount;
                if (work == IM.NullI)
                    work = 0;
                return work;
            }
            set
            {
                int work = value;
                if (work == 0)
                    work = IM.NullI;
                WorkCount = work;
                InvokeNotifyPropertyChanged(FieldWorkCountNotNull);
            }
        }
        //--
        public const string FieldWorkCount = "WorkCount";
        private int _workCount = IM.NullI;
        public int WorkCount
        {
            get { return GetThreadSafeValue(ref _workCount); }
            set
            {
                SetThreadSafeValue(ref _workCount, value, FieldWorkCount);
                ClearConfirmArticle();
            }
        }
        //--
        public const string FieldNote = "Note";
        private string _note = "";
        public string Note
        {
            get { return GetThreadSafeValue(ref _note); }
            set { SetThreadSafeValue(ref _note, value, FieldNote); }
        }
        //--
        public const string FieldIsConfirmed = "IsConfirmed";
        private bool _isConfirmed = false;
        public bool IsConfirmed
        {
            get { return GetThreadSafeValue(ref _isConfirmed); }
            set { SetThreadSafeValue(ref _isConfirmed, value, FieldIsConfirmed); }
        }
        //--
        public const string FieldConfirmedBy = "ConfirmedBy";
        private string _confirmedBy = "";
        public string ConfirmedBy
        {
            get { return GetThreadSafeValue(ref _confirmedBy); }
            set { SetThreadSafeValue(ref _confirmedBy, value, FieldConfirmedBy); }
        }
        //--
        public const string FieldConfirmedDate = "ConfirmedDate";
        private DateTime _confirmedDate = IM.NullT;
        public DateTime ConfirmedDate
        {
            get { return GetThreadSafeValue(ref _confirmedDate); }
            set { SetThreadSafeValue(ref _confirmedDate, value, FieldConfirmedDate); }
        }
        //--
        public const string FieldArticleIdSuggest = "ArticleIdSuggest";
        public int ArticleIdSuggest
        {
            get { return GetArticleId(); }
        }
        //--
        public const string FieldWorkCountSuggest = "WorkCountSuggest";
        public int WorkCountSuggest
        {
            get { return GetWorkCount(); }
        }
        //--
        private NetStations _stationList = new NetStations();
        public NetStations StationList
        {
            get { return _stationList; }
        }
        /// <summary>
        /// Список активных станций
        /// </summary>
        public NetStationItem[] ActiveStations
        {
            get
            {
                List<NetStationItem> active = new List<NetStationItem>();
                //Формируем список удаляемых станций
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.IsActive)
                        active.Add(netStation);
                }
                return active.ToArray();
            }
        }
        //Plan #6887 WAS_USED=1
        public int CountActiveWU 
        {
            get
            {
                int count = 0;
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.IsActiveWU)
                        count++;
                }
                return count;
            }
        }
        /// <summary>
        /// Список выбранных станций
        /// </summary>
        public NetStationItem[] SelectedStations
        {
            get
            {
                List<NetStationItem> selected = new List<NetStationItem>();
                //Формируем список удаляемых станций
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.IsChecked)
                        selected.Add(netStation);
                }
                return selected.ToArray();
            }
        }
        /// <summary>
        /// Список БС станций
        /// </summary>
        public NetStationItem[] BaseStations
        {
            get
            {
                List<NetStationItem> retVal = new List<NetStationItem>();
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.StationType == NetStationItem.EnumStationType.Base)
                        retVal.Add(netStation);
                }
                return retVal.ToArray();
            }
        }
        /// <summary>
        /// Список БС станций
        /// </summary>
        public NetStationItem[] StationaryStations
        {
            get
            {
                List<NetStationItem> retVal = new List<NetStationItem>();
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.StationType == NetStationItem.EnumStationType.Stationary)
                        retVal.Add(netStation);
                }
                return retVal.ToArray();
            }
        }
        /// <summary>
        /// Список БС станций
        /// </summary>
        public NetStationItem[] AbonentStations
        {
            get
            {
                List<NetStationItem> retVal = new List<NetStationItem>();
                foreach (NetStationItem netStation in StationList)
                {
                    if (netStation.StationType == NetStationItem.EnumStationType.Abonent)
                        retVal.Add(netStation);
                }
                return retVal.ToArray();
            }
        }

        private bool _isFullLoaded = true;
        /// <summary>
        /// Конструктор
        /// </summary>
        public NetObject()
        {
            //Мониторим изменения
            StationList.ListChanged += (s, ex) => { IsChanged = true; };
        }
        /// <summary>
        /// Загрузить сеть
        /// </summary>
        /// <param name="netId">Id сети</param>
        public void Load(int netId)
        {
            Load(netId, true);
        }
        /// <summary>
        /// Загрузить сеть
        /// </summary>
        /// <param name="netId">Id сети</param>
        /// <param name="isFull">Полная загрузка данных сети</param>
        public void Load(int netId, bool isFull)
        {
            using (LisProgressBar pb = new LisProgressBar("Загрузка мережі"))
            {
                using (LisRecordSet rs = new LisRecordSet(TableName, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID,NAME,CALL_SIGN,OWNER_ID,PRICE_ID,WORKS_COUNT,NOTE,AUTO_SET_ARTICLE");
                    rs.Select("IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, netId);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        Id = rs.GetI("ID");
                        Name = rs.GetS("NAME");
                        CallSign = rs.GetS("CALL_SIGN");
                        OwnerId = rs.GetI("OWNER_ID");
                        ArticleId = rs.GetI("PRICE_ID");
                        Note = rs.GetS("NOTE");
                        int work = rs.GetI("WORKS_COUNT");
                        WorkCount = (work == IM.NullI) ? 0 : work;
                        NeedSetArticleAuto = (rs.GetI("AUTO_SET_ARTICLE") == 1);
                        IsConfirmed = (rs.GetI("IS_CONFIRMED") == 1) ? true : false;
                        ConfirmedBy = rs.GetS("CONFIRMED_BY");
                        ConfirmedDate = rs.GetT("CONFIRMED_DATE");
                    }
                }
                if(isFull)
                    StationList.LoadStations(Id);
                _isFullLoaded = isFull;
                IsChanged = false;
            }
        }
        /// <summary>
        /// Сохранить сеть
        /// </summary>
        public void Save()
        {
            using (LisProgressBar pb = new LisProgressBar("Зберегання мережі"))
            {
                using (LisTransaction tran = new LisTransaction())
                {
                    using (LisRecordSet rs = new LisRecordSet(TableName, IMRecordset.Mode.ReadWrite))
                    {
                        rs.Select("ID,TABLE_NAME,NAME,CALL_SIGN,OWNER_ID,PRICE_ID,WORKS_COUNT,NOTE");
                        rs.Select("AUTO_SET_ARTICLE");
                        rs.Select("DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                        rs.Select("IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                        rs.Open();
                        if (rs.IsEOF())
                        {
                            rs.AddNew();
                            Id = IM.AllocID(TableName, 1, -1);
                            rs.Put("ID", Id);
                            rs.Put("TABLE_NAME", TableName);
                            rs.Put("DATE_CREATED", DateTime.Now);
                            rs.Put("CREATED_BY", IM.ConnectedUser());
                        }
                        else
                        {
                            rs.Edit();
                            rs.Put("DATE_MODIFIED", DateTime.Now);
                            rs.Put("MODIFIED_BY", IM.ConnectedUser());
                        }
                        rs.Put("NAME", Name);
                        rs.Put("CALL_SIGN", CallSign);
                        rs.Put("NOTE", Note);
                        rs.Put("OWNER_ID", OwnerId);
                        rs.Put("PRICE_ID", ArticleId);
                        rs.Put("WORKS_COUNT", (WorkCount == 0) ? IM.NullI : WorkCount);
                        rs.Put("AUTO_SET_ARTICLE", NeedSetArticleAuto ? 1 : IM.NullI);
                        rs.Put("IS_CONFIRMED", IsConfirmed ? 1 : 0);
                        rs.Put("CONFIRMED_BY", ConfirmedBy);
                        rs.Put("CONFIRMED_DATE", ConfirmedDate);
                        rs.Update();
                    }
                    if(_isFullLoaded)
                        StationList.SaveStations(Id);
                    tran.Commit();
                    IsChanged = false;
                }
            }
        }
        /// <summary>
        /// Удалить выбранные записи
        /// </summary>
        public void DeleteSelected()
        {
            //Удаляем станции
            foreach (NetStationItem netStation in SelectedStations)
                StationList.Remove(netStation);
        }
        /// <summary>
        /// Возвращает подходящую статью
        /// </summary>
        /// <returns>ID статьи</returns>
        private int GetArticleId()
        {
            const string standard = "УКХ";
            string stand = standard;

            /*
            bool isBaseStationWithLicence = true;
            bool isBaseStation = false;
            foreach (NetStationItem activeStation in ActiveStations)
            {
                if((activeStation.StationType == NetStationItem.EnumStationType.Base) && activeStation.IsActive)
                {
                    isBaseStation = true;
                    if(activeStation.IsLicence == false)
                        isBaseStationWithLicence = false;
                    stand = activeStation.Standard;
                    if ((activeStation.Standard != standard) && (activeStation.Standard != "ЦУКХ"))
                        isBaseStationWithLicence = false;
                }
            }
             */
            bool isBaseStationWithLicence = true;
            bool isBaseStation = true;
            string setArticle = "";
            if (isBaseStationWithLicence && isBaseStation)
            {
                int countStation = ActiveStations.Length;
                if (countStation <= 5)
                    setArticle = ArticleLib.ArticleFunc.article_22_1;
                    //setArticle = ArticleLib.ArticleFunc.article_22_1_1;
                else if (countStation <= 15)
                    setArticle = ArticleLib.ArticleFunc.article_22_1;
                    //setArticle = ArticleLib.ArticleFunc.article_22_1_2;
                else if (countStation <= 30)
                    setArticle = ArticleLib.ArticleFunc.article_22_1;
                    //setArticle = ArticleLib.ArticleFunc.article_22_1_3;
                else if (countStation <= 50)
                    setArticle = ArticleLib.ArticleFunc.article_22_1;
                    //setArticle = ArticleLib.ArticleFunc.article_22_1_4;
                else
                    setArticle = ArticleLib.ArticleFunc.article_22_1;
                    //setArticle = ArticleLib.ArticleFunc.article_22_1_5;
                
            }
            
            if (string.IsNullOrEmpty(stand)) stand=standard;
            return ArticleLib.ArticleFunc.GetArticleId(setArticle, stand);
        }
        /// <summary>
        /// Возвращает количество работ
        /// </summary>
        /// <returns>кол-во работ</returns>
        private int GetWorkCount()
        {
            HashSet<double> countUnicFreqs = new HashSet<double>();
           /* foreach (NetStationItem abonentStation in AbonentStations)
            {
                foreach (double freq in abonentStation.FreqsTx)
                {
                    if (countUnicFreqs.Contains(freq) == false)
                        countUnicFreqs.Add(freq);
                }
                foreach (double freq in abonentStation.FreqsRx)
                {
                    if (countUnicFreqs.Contains(freq) == false)
                        countUnicFreqs.Add(freq);
                }
            }
            */ 
            foreach (NetStationItem baseStation in BaseStations)
            {
                foreach (double freq in baseStation.FreqsTx)
                {
                    if (countUnicFreqs.Contains(freq) == false)
                        countUnicFreqs.Add(freq);
                }
                foreach (double freq in baseStation.FreqsRx)
                {
                    if (countUnicFreqs.Contains(freq) == false)
                        countUnicFreqs.Add(freq);
                }
            }

            return countUnicFreqs.Count;
        }
        /// <summary>
        /// Очищаем поля подтверждения статьи
        /// </summary>
        public void ClearConfirmArticle()
        {
            IsConfirmed = false;
            ConfirmedBy = "";
            ConfirmedDate = IM.NullT;
        }
        /// <summary>
        /// устаналиваем поля подтверждения статьи
        /// </summary>
        public void SetConfirmArticle()
        {
            if (!CanConfirmArticle())
                throw new Exceptions.IllegalArticleException(CLocaliz.TxT("The article can't be confirmed"));
            IsConfirmed = true;
            ConfirmedBy = CUsers.ConnectedUser();
            ConfirmedDate = DateTime.Now;
        }
       
        /// <summary>
        /// отменяем подтверждение статьи
        /// </summary>
        public void DisableConfirmArticle()
        {
            if (CanConfirmArticleCustom())
            {
                IsConfirmed = false;
               // WorkCount = 0;
                ConfirmedBy = "";
                ConfirmedDate = IM.NullT;
               // ArticleId = IM.NullI;
            }
        }
      
        /// <summary>
        /// Определяет возможность подтвердить статью
        /// </summary>
        public bool CanConfirmArticle()
        {
            bool canConfirm;
            //кроме стандартной проверки на наличие значений для статей (ID и колтчество работ)  выполняется вызов метода CanConfirmArticleCustom,
            //возвращающего признак принадлежности статьи к технологии УКХ или ЦУКХ, а также признак "Технлогичности" БС
            curDept = CUsers.GetCurDepartment();
            if (curDept == UcrfDepartment.Branch)
            {
                if ((ArticleId == IM.NullI && ((WorkCount == IM.NullI) || (WorkCount == 0))) && CanConfirmArticleCustom())
                {
                    canConfirm = true;
                }

                else if ((ArticleId != IM.NullI && (WorkCount != IM.NullI) && (WorkCount != 0)) && CanConfirmArticleCustom())
                {
                    canConfirm = true;
                }
                else
                {
                    canConfirm = false;
                }
            }
            else
            {
                if (ArticleId == IM.NullI && ((WorkCount == IM.NullI) || (WorkCount == 0)))
                 {
                   canConfirm = true;
                 }
                 else if (ArticleId != IM.NullI && (WorkCount != IM.NullI) && (WorkCount != 0))
                 {
                   canConfirm = true;
                 }
                 else
                 {
                   canConfirm = false;
                 }
            }
            return canConfirm; 
        }

        /// <summary>
        /// Определяет возможность подтвердить статью
        /// </summary>
        public bool CanConfirmArticleCustom()
        {
            bool canConfirm;
            // вызов методов CompareRadioTech (проверка принадлежности статьи к технологиям УКХ и ЦУКХ) и проверка статической переменной NetStationItem.isTechUser на признак "Технологичности"  БС
            if ((CompareRadioTech(ArticleId, CRadioTech.YKX) && (!NetStationItem.isTechUser)) || (CompareRadioTech(ArticleId, CRadioTech.ЦУКХ) && (!NetStationItem.isTechUser)))
            {
                canConfirm = true;
            }
            else
            {
                canConfirm = false;
            }
            return canConfirm; 
        }
        /// <summary>
        /// Определяет возможность подтвердить статью
        /// </summary>
        public bool CanConfirmArticleCustomize(int ID)
        {
            // вызов статического метода, возвращающего признак (можно или нет подтверждать статью)
            return NetObject.CanConfirmArticleCustom(ID);
        }

        /// <summary>
        /// Определяет возможность подтвердить статью (статический метод)
        /// </summary>
        public static bool CanConfirmArticleCustom(int ID)
        {
            bool canConfirm=false;
            if (ID != IM.NullI)
            {
                // вызов методов CompareRadioTech (проверка принадлежности статьи к технологиям УКХ и ЦУКХ) и проверка статической переменной NetStationItem.isTechUser на признак "Технологичности"  БС
                if ((NetObject.CompareRadioTech(ID, CRadioTech.YKX) && (!NetStationItem.isTechUser))  || (NetObject.CompareRadioTech(ID, CRadioTech.ЦУКХ) && (!NetStationItem.isTechUser)))
                {
                    canConfirm = true;
                }
                else
                {
                    canConfirm = false;
                }
            }
            return canConfirm; 
        }

        /// <summary>
        /// Определяет возможность подтвердить статью
        /// </summary>
        public bool CanConfirmArticleCustomizeNew(int ID)
        {
            // вызов статического метода, возвращающего признак (можно или нет подтверждать статью)
            return NetObject.CanConfirmArticleCustomNew(ID);
        }
        /// <summary>
        /// Определяет возможность подтвердить статью (статический метод)
        /// </summary>
        public static bool CanConfirmArticleCustomNew(int ID)
        {
            bool canConfirm = false;
            if (ID != IM.NullI)
            {
                // вызов методов CompareRadioTech (проверка принадлежности статьи к технологиям УКХ и ЦУКХ) и проверка статической переменной NetStationItem.isTechUser на признак "Технологичности"  БС
                if ((NetObject.CompareRadioTech(ID, CRadioTech.YKX)) || (NetObject.CompareRadioTech(ID, CRadioTech.ЦУКХ)))
                {
                    canConfirm = true;
                }
                else
                {
                    canConfirm = false;
                }
            }
            return canConfirm;
        }
        /// <summary>
        /// Проверка ID к принадлежности конкретной радиотехнологии  (статический метод)
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="RadioTech"></param>
        /// <returns></returns>
        public static bool CompareRadioTech(int ID, string RadioTech)
        {
            bool RetB = false;
            // Поиск по таблице XNRFA_PRICE
            using (Icsm.LisRecordSet rsArticle = new Icsm.LisRecordSet(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly))
            {
                // Выборка по полям ID,ARTICLE,STANDARD для заданного значения ID и заданной технологии RadioTech
                rsArticle.Select("ID,ARTICLE,STANDARD,STATUS,DATEOUTACTION,DATESTARTACTION");

                rsArticle.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                rsArticle.SetWhere("STANDARD", IMRecordset.Operation.Eq, RadioTech);
                rsArticle.SetWhere("STATUS", IMRecordset.Operation.Eq, 1);
                rsArticle.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt , DateTime.Now);
                rsArticle.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

                rsArticle.Open();
                if (!rsArticle.IsEOF())
                {
                    RetB = true;
                }
            }
            return RetB;
        }
   


    }
    /// <summary>
    /// Описание станции сети
    /// </summary>
    class NetStationItem : NotifyPropertyChanged
    {
        public enum EnumStationType
        {
            Unknown,
            Base,
            Abonent,
            Stationary,
        }
        //--
        private UcrfDepartment curDept;
        public static bool isTechUser = false;
        public const string FieldIsChecked = "IsChecked";
        private bool _isChecked = false;
        public bool IsChecked
        {
            get { return GetThreadSafeValue(ref _isChecked); }
            set { SetThreadSafeValue(ref _isChecked, value, FieldIsChecked); }
        }
        //--
        public const string FieldId = "Id";
        private int _id = IM.NullI;
        public int Id
        {
            get { return GetThreadSafeValue(ref _id); }
            set { SetThreadSafeValue(ref _id, value, FieldId); }
        }
        //--
        public const string FieldStationTypeString = "StationTypeString";
        public string StationTypeString
        {
            get { return GetEnumStationTypeString(StationType); }
        }
        //--
        public const string FieldStationTypeNumeric = "StationTypeNumeric";
        public string StationTypeNumeric
        {
            get { return GetEnumStationTypeNumeric(StationType); }
        }
        //--
        public const string FieldStationType = "StationType";
        private EnumStationType _stationType = EnumStationType.Unknown;
        public EnumStationType StationType
        {
            get { return GetThreadSafeValue(ref _stationType); }
            protected set
            {
                SetThreadSafeValue(ref _stationType, value, FieldStationType);
                InvokeNotifyPropertyChanged(FieldStationTypeString);
            }
        }
        //--
        public const string FieldTableName = "TableName";
        private string _tableName = "";
        public string TableName
        {
            get { return GetThreadSafeValue(ref _tableName); }
            set { SetThreadSafeValue(ref _tableName, value, FieldTableName); }
        }
        //--
        public const string FieldPermission = "Permission";
        private string _permission = "";
        public string Permission
        {
            get { return GetThreadSafeValue(ref _permission); }
            set { SetThreadSafeValue(ref _permission, value, FieldPermission); }
        }
        //--
        public const string FieldPermissionDateTo = "PermissionDateTo";
        private DateTime _permissionDateTo = IM.NullT;
        public DateTime PermissionDateTo
        {
            get { return GetThreadSafeValue(ref _permissionDateTo); }
            set { SetThreadSafeValue(ref _permissionDateTo, value, FieldPermissionDateTo); }
        }
        //--
        public const string FieldIsActive = "IsActive";
        private bool _isActive = false;
        public bool IsActive
        {
            get { return GetThreadSafeValue(ref _isActive); }
            set { SetThreadSafeValue(ref _isActive, value, FieldIsActive); }
        }
        // Plan #6887 WAS_USED=1
        public const string FieldIsActiveWU = "IsActiveWU";
        private bool _isActiveWU = false;
        public bool IsActiveWU
        {
            get { return GetThreadSafeValue(ref _isActiveWU); }
            set { SetThreadSafeValue(ref _isActiveWU, value, FieldIsActiveWU); }
        }
        //--
        public const string FieldIsLicence = "IsLicence";
        private bool _isLicence = false;
        public bool IsLicence
        {
            get { return GetThreadSafeValue(ref _isLicence); }
            set { SetThreadSafeValue(ref _isLicence, value, FieldIsLicence); }
        }
        //--
        public const string FieldStandard = "Standard";
        private string _standard = "";
        public string Standard
        {
            get { return GetThreadSafeValue(ref _standard); }
            set { SetThreadSafeValue(ref _standard, value, FieldStandard); }
        }
        //--
        public const string FieldCallSign = "CallSign";
        private string _callSign = "";
        public string CallSign
        {
            get { return GetThreadSafeValue(ref _callSign); }
            set { SetThreadSafeValue(ref _callSign, value, FieldCallSign); }
        }
        //--
        public const string FieldAddress = "Address";
        private string _address = "";
        public string Address
        {
            get { return GetThreadSafeValue(ref _address); }
            set { SetThreadSafeValue(ref _address, value, FieldAddress); }
        }

        // Добавлено WAS_USED, STATUS Plan #6887
        public const string FieldWas_Used = "WAS_USED";
        private int _Was_Used = 0;
        public int Was_Used
        {
            get { return GetThreadSafeValue(ref _Was_Used); }
            set { SetThreadSafeValue(ref _Was_Used, value, FieldWas_Used); }
        }
        //---
        public const string FieldStatus = "STATUS";
        private string _Status = "";
        public string Status
        {
            get { return GetThreadSafeValue(ref _Status); }
            set { SetThreadSafeValue(ref _Status, value, FieldStatus); }
        }
        public const string FieldFreqTxStr = "FreqTxStr";
        public string FreqTxStr
        {
            get
            {
                System.Text.StringBuilder retVal = new System.Text.StringBuilder("");
                if (FreqsTx == null)
                    return retVal.ToString();
                foreach (double freq in FreqsTx)
                {
                    if (retVal.Length > 0)
                        retVal.Append("; ");
                    retVal.Append(freq);
                }
                return retVal.ToString();
            }
        }
        //--
        public const string FieldFreqRxStr = "FreqRxStr";
        public string FreqRxStr
        {
            get
            {
                System.Text.StringBuilder retVal = new System.Text.StringBuilder("");
                if (FreqsRx == null)
                    return retVal.ToString();
                foreach (double freq in FreqsRx)
                {
                    if (retVal.Length > 0)
                        retVal.Append("; ");
                    retVal.Append(freq);
                }
                return retVal.ToString();
            }
        }
        //--
        private double[] _freqsTx = new double[] { };
        public double[] FreqsTx
        {
            get { return _freqsTx; }
        }
        //--
        private double[] _freqsRx = new double[] { };
        public double[] FreqsRx
        {
            get { return _freqsRx; }
        }
        /// <summary>
        /// Загрузить параметры станции
        /// </summary>
        /// <param name="id">Id станции</param>
        /// <param name="tableName">Название таблицы</param>
        public bool Load(int id, string tableName)
        {
            curDept = CUsers.GetCurDepartment();
            bool retVal = false;
            Id = id;
            TableName = tableName;
            switch (TableName)
            {
                case PlugTbl.XfaAbonentStation:
                    StationType = EnumStationType.Abonent;
                    using (LisRecordSet rsAbonent = new LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                    {
                        bool isFirstRecord = true;
                        List<double> freqTx = new List<double>();
                        List<double> freqRx = new List<double>();
                        rsAbonent.Select("ID,WAS_USED,DOZV_NUM,DOZV_DATE_TO,Abonent.ID,Abonent.STATUS,Abonent.STANDARD");
                        rsAbonent.Select("Abonent.Freqs.TX_FREQ");
                        rsAbonent.Select("Abonent.Freqs.RX_FREQ");
                        rsAbonent.Select("Abonent.STANDARD");
                        rsAbonent.Select("Abonent.POS_ID");
                        rsAbonent.Select("Abonent.CALL_SIGN");
                        rsAbonent.SetWhere("Abonent.ID", IMRecordset.Operation.Eq, Id);
                        rsAbonent.OrderBy("Abonent.Freqs.TX_FREQ", OrderDirection.Ascending);
                        for (rsAbonent.Open(); !rsAbonent.IsEOF(); rsAbonent.MoveNext())
                        {
                            if (isFirstRecord)
                            {
                                isFirstRecord = false;
                                //IsActive = (rsAbonent.GetI("WAS_USED") == 1 && rsAbonent.GetS("Abonent.STANDARD") == "УКХ");//Plan #6887 изменен алгорим
                               
                                IsActive = ((rsAbonent.GetI("WAS_USED") == 1 && rsAbonent.GetS("Abonent.STANDARD") == "УКХ") || (rsAbonent.GetI("WAS_USED") == 1 && rsAbonent.GetS("Abonent.STANDARD") == "ЦУКХ"));//Plan #6887 изменен алгорим
                               
                                IsActiveWU = (rsAbonent.GetI("WAS_USED") == 1);//Plan #6887 WAS_USED=1
                                
                                //Plan #6887
                                Was_Used = rsAbonent.GetI("WAS_USED");
                                Status = rsAbonent.GetS("Abonent.STATUS");
                                //--
                                Permission = rsAbonent.GetS("DOZV_NUM");
                                Standard = rsAbonent.GetS("Abonent.STANDARD");
                                CallSign = rsAbonent.GetS("Abonent.CALL_SIGN");
                                PermissionDateTo = rsAbonent.GetT("DOZV_DATE_TO");
                                HelpClasses.PositionState pos = new HelpClasses.PositionState();
                                pos.LoadStatePosition(rsAbonent.GetI("Abonent.POS_ID"), PlugTbl.XfaPosition);
                                Address = pos.FullAddress;
                                retVal = true;
                            }
                            double freq = rsAbonent.GetD("Abonent.Freqs.TX_FREQ");
                            if(freq != IM.NullD)
                                freqTx.Add(freq.Round(6));
                            freq = rsAbonent.GetD("Abonent.Freqs.RX_FREQ");
                            if (freq != IM.NullD)
                                freqRx.Add(freq.Round(6));
                        }
                        _freqsTx = freqTx.ToArray();
                        _freqsRx = freqRx.ToArray();
                    }
                    break;
                case ICSMTbl.itblMobStation:
                case ICSMTbl.itblMobStation2:
                    {
                        int idAppl = ApplSource.BaseAppClass.GetApplId(Id, TableName);
                        if (curDept == UcrfDepartment.Branch)
                        {
                            if (isTechUser == false)
                            {
                                // Вызов метода ApplSource.BaseAppClass.IsTechUserProc, реализующего алгоритм определения принадлежности станции к категории "Технологических"
                                bool isTechUser_temp = ApplSource.BaseAppClass.IsTechUserProc(TableName, idAppl);
                                if (isTechUser_temp) isTechUser = true;
                            }
                        }
                        using (LisRecordSet rsMob = new LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                        {
                            if (TableName == ICSMTbl.itblMobStation)
                                rsMob.Select("ID,WAS_USED,DOZV_NUM,DOZV_DATE_TO,MobileSector1.STATUS,STANDARD");
                            else
                                rsMob.Select("ID,WAS_USED,DOZV_NUM,DOZV_DATE_TO,BaseSector1.STATUS,STANDARD");

                            rsMob.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
                            rsMob.Open();
                            if (!rsMob.IsEOF())
                            {
                                //IsActive = (rsMob.GetI("WAS_USED") == 1 && rsMob.GetS("STANDARD") == "УКХ");//MobStation1.Plan #6887 изменен алгорим
                               
                                IsActive = ((rsMob.GetI("WAS_USED") == 1 && rsMob.GetS("STANDARD") == "УКХ") || (rsMob.GetI("WAS_USED") == 1 && rsMob.GetS("STANDARD") == "ЦУКХ"));//Plan #6887 изменен алгорим
                               
                                IsActiveWU = (rsMob.GetI("WAS_USED") == 1);//Plan #6887 WAS_USED=1
                                //Plan #6887
                                Was_Used = rsMob.GetI("WAS_USED");
                                if (TableName == ICSMTbl.itblMobStation)
                                    Status = rsMob.GetS("MobileSector1.STATUS");
                                else
                                    Status = rsMob.GetS("BaseSector1.STATUS");
                                //--
                                Permission = rsMob.GetS("DOZV_NUM");
                                PermissionDateTo = rsMob.GetT("DOZV_DATE_TO");
                                retVal = true;
                            }
                        }
                        using (LisRecordSet rsMob = new LisRecordSet(TableName, IMRecordset.Mode.ReadOnly))
                        {
                            bool isFirstRecord = true;
                            List<double> freqTx = new List<double>();
                            List<double> freqRx = new List<double>();
                            rsMob.Select("ID,CLASS,STANDARD,CUST_CHB1");
                            rsMob.Select("POS_ID,CUST_TXT9");
                            rsMob.Select("AssignedFrequencies.TX_FREQ");
                            rsMob.Select("AssignedFrequencies.RX_FREQ");
                            rsMob.SetWhere("ID", IMRecordset.Operation.Eq, id);
                            for (rsMob.Open(); !rsMob.IsEOF(); rsMob.MoveNext() )
                            {
                                if (isFirstRecord)
                                {
                                    isFirstRecord = false;
                                    Standard = rsMob.GetS("STANDARD");
                                    IsLicence = (rsMob.GetI("CUST_CHB1") != 1);
                                    CallSign = rsMob.GetS("CUST_TXT9");
                                    HelpClasses.PositionState pos = new HelpClasses.PositionState();
                                    pos.LoadStatePosition(rsMob.GetI("POS_ID"), (TableName == ICSMTbl.itblMobStation) ? ICSMTbl.itblPositionWim : ICSMTbl.itblPositionMob2);
                                    Address = pos.FullAddress;
                                    switch (rsMob.GetS("CLASS").ToUpper())
                                    {
                                        default: //case "FB":
                                            StationType = EnumStationType.Base;
                                            break;
                                        case "FL":
                                            StationType = EnumStationType.Stationary;
                                            break;
                                    }
                                }
                                double freq = rsMob.GetD("AssignedFrequencies.TX_FREQ");
                                if (freq != IM.NullD)
                                    freqTx.Add(freq.Round(6));
                                freq = rsMob.GetD("AssignedFrequencies.RX_FREQ");
                                if (freq != IM.NullD)
                                    freqRx.Add(freq.Round(6));
                            }
                            _freqsTx = freqTx.ToArray();
                            _freqsRx = freqRx.ToArray();
                        }
                    }
                    break;
            }
            return retVal;
        }
        /// <summary>
        /// Сохранить станцию
        /// </summary>
        /// <param name="netId">Id сети</param>
        public void Save(int netId)
        {
            using (LisTransaction tran = new LisTransaction())
            {
                switch (TableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadWrite))
                        {
                            rs.Select("ID,NET_ID");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                            {
                                rs.Edit();
                                rs.Put("NET_ID", netId);
                                rs.Update();
                            }
                        }
                        break;
                    case ICSMTbl.itblMobStation:
                    case ICSMTbl.itblMobStation2:
                        using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite))
                        {
                            rs.Select("ID,NET_ID,STATION_ID,STATION_TABLE,TABLE_NAME");
                            rs.SetWhere("NET_ID", IMRecordset.Operation.Eq, netId);
                            rs.SetWhere("STATION_ID", IMRecordset.Operation.Eq, Id);
                            rs.SetWhere("STATION_TABLE", IMRecordset.Operation.Like, TableName);
                            rs.Open();
                            if (rs.IsEOF())
                            {
                                rs.AddNew();
                                rs.Put("ID", IM.AllocID(PlugTbl.XfaRefBaseStation, 1, -1));
                                rs.Put("NET_ID", netId);
                                rs.Put("STATION_ID", Id);
                                rs.Put("STATION_TABLE", TableName);
                                rs.Put("TABLE_NAME", PlugTbl.XfaRefBaseStation);
                                rs.Update();
                            }
                        }
                        break;
                }
                tran.Commit();
            }
        }
        /// <summary>
        /// Удаляем станцию из сети
        /// </summary>
        /// <param name="netId">Id сети</param>
        public void DeleteNet(int netId)
        {
            using (LisTransaction tran = new LisTransaction())
            {
                switch (TableName)
                {
                    case PlugTbl.XfaAbonentStation:
                        using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadWrite))
                        {
                            rs.Select("ID,NET_ID");
                            rs.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                            {
                                rs.Edit();
                                rs.Put("NET_ID", IM.NullI);
                                rs.Update();
                            }
                        }
                        break;
                    case ICSMTbl.itblMobStation:
                    case ICSMTbl.itblMobStation2:
                        using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite))
                        {
                            rs.Select("ID");
                            rs.SetWhere("NET_ID", IMRecordset.Operation.Eq, netId);
                            rs.SetWhere("STATION_ID", IMRecordset.Operation.Eq, Id);
                            rs.SetWhere("STATION_TABLE", IMRecordset.Operation.Like, TableName);
                            for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                                rs.Delete();
                        }
                        break;
                }
                tran.Commit();
            }
        }

        /// <summary>
        /// Преобразовывает EnumStationType в число 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetEnumStationTypeNumeric(EnumStationType type)
        {
            switch (type)
            {
                case EnumStationType.Abonent:
                    return "2";
                case EnumStationType.Base:
                    return "0";
                case EnumStationType.Stationary:
                    return "1";
                case EnumStationType.Unknown:
                    return "3";
                default:
                    throw new InvalidCastException(string.Format("Error type '{0}'", type));
            }
        }

        /// <summary>
        /// Преобразовывает EnumStationType в строку 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetEnumStationTypeString(EnumStationType type)
        {
            switch(type)
            {
                case EnumStationType.Abonent:
                    return "Абонент";
                case EnumStationType.Base:
                    return "БС";
                case EnumStationType.Stationary:
                    return "СА";
                case EnumStationType.Unknown:
                    return "Невідома";
                default:
                    throw new InvalidCastException(string.Format("Error type '{0}'", type));
            }
        }
        #region Notify
        protected EnumStationType GetThreadSafeValue(ref EnumStationType oldVal)
        {
            EnumStationType retVal;
            lock (InLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref EnumStationType oldVal, EnumStationType newValue, string propertyName)
        {
            bool localChange = false;
            lock (InLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }
        #endregion
    }
    /// <summary>
    /// Список станций сети
    /// </summary>
    class NetStations : BindingList<NetStationItem>
    {
        /// <summary>
        /// Список удаленных станций
        /// </summary>
        private List<NetStationItem> _deletedStation = new List<NetStationItem>();
        /// <summary>
        /// Загрузить станции сети
        /// </summary>
        /// <param name="netId">Id сети</param>
        public void LoadStations(int netId)
        {
            NetStationItem.isTechUser = false;
            using (LisProgressBar pb = new LisProgressBar("Загрузка мережі"))
            {
                int bsCount = 0;
                IM.ExecuteScalar(ref bsCount, string.Format("select COUNT(1) from %{0} where NET_ID = {1}", PlugTbl.XfaRefBaseStation, netId));
                int abCount = 0;
                IM.ExecuteScalar(ref abCount, string.Format("select COUNT(1) from %{0} where NET_ID = {1}", PlugTbl.XfaAbonentStation, netId));

                pb.SetProgress(0, bsCount+abCount);
                //Выбор базовых
                using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaRefBaseStation, IMRecordset.Mode.ReadWrite))
                {
                    rs.Select("ID,STATION_ID,STATION_TABLE");
                    rs.SetWhere("NET_ID", IMRecordset.Operation.Eq, netId);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int id = rs.GetI("STATION_ID");
                        string table = rs.GetS("STATION_TABLE");
                        AddStation(id, table);
                        pb.Increment(true);
                    }
                }
                //Выбор абонентских
                using (LisRecordSet rs = new LisRecordSet(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID,TABLE_NAME");
                    rs.SetWhere("NET_ID", IMRecordset.Operation.Eq, netId);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        int id = rs.GetI("ID");
                        string table = rs.GetS("TABLE_NAME");
                        AddStation(id, table);
                        pb.Increment(true);
                    }
                }
            }
        }
        /// <summary>
        /// Загрузить станцию в сеть
        /// </summary>
        /// <param name="id">Id станции</param>
        /// <param name="tableName">Название таблицы</param>
        public void AddStation(int id, string tableName)
        {
            NetStationItem newStation = new NetStationItem();
            newStation.Load(id, tableName);
            this.Add(newStation);
        }
        /// <summary>
        /// Сохранить станции сети
        /// </summary>
        /// <param name="netId">Id сети</param>
        public void SaveStations(int netId)
        {
            using (LisProgressBar pb = new LisProgressBar("Зберегання мережі"))
            {
                using (LisTransaction tran = new LisTransaction())
                {
                    pb.SetBig("Відалення...");
                    pb.SetProgress(0, _deletedStation.Count);
                    //Удаляем станции из сети
                    while (_deletedStation.Count > 0)
                    {
                        NetStationItem netStation = _deletedStation[0];
                        _deletedStation.RemoveAt(0);
                        netStation.DeleteNet(netId);
                        pb.Increment(true);
                    }
                    pb.SetBig("Додавання...");
                    pb.SetProgress(0, this.Count);
                    //Добавляем / обновляем станции в сети
                    foreach (NetStationItem netStation in this)
                    {
                        netStation.Save(netId);
                        pb.Increment(true);
                    }
                    tran.Commit();
                }
            }
        }
        /// <summary>
        /// Удаляем из сети
        /// </summary>
        /// <param name="index"></param>
        protected override void RemoveItem(int index)
        {
            RemoveItem(this[index]);
            base.RemoveItem(index);
        }
        /// <summary>
        /// Добавить новую станцию
        /// </summary>
        /// <param name="item">Станция</param>
        private void RemoveItem(NetStationItem item)
        {
            _deletedStation.Add(item);  //помечаем для удаления
        }
    }
}
