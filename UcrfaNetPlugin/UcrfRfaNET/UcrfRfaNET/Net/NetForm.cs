﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Binding;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.Exceptions;
using XICSM.UcrfRfaNET.UtilityClass;
using System.Data;
using System.Collections;
using System.Linq.Expressions;

namespace XICSM.UcrfRfaNET.Net
{

    internal partial class NetForm : FBaseFormClear
    {
        /// <summary>
        /// Отображает форму редактирования сети
        /// </summary>
        /// <param name="netId">Id сети</param>
        /// <returns>True - били изменения, иначе FALSE</returns>
        public static bool ShowForm(int netId)
        {
            bool retVal = false;
            using (NetForm frm = new NetForm(netId))
            {
                retVal = frm.ShowDialog() == DialogResult.OK;
            }
            return retVal;
        }
        //=================================
        private UcrfDepartment curDept;
        private NetObject _net;
        private string _enterOwnerName = null;
        private ComboBoxDictionaryList<int, string> _lstPrices1;
        //======================================
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="netId">Id сети</param>
        public NetForm(int netId)
        {
            _net = new NetObject();
            _net.Load(netId);
            InitializeComponent();
            InitForm();

       }
        /// <summary>
        /// Инициализация формы
        /// </summary>
        private void InitForm()
        {
            curDept = CUsers.GetCurDepartment();
            tbNetName.DataBindings.Add("Text", _net, NetObject.FieldName);
            tbCallSign.DataBindings.Add("Text", _net, NetObject.FieldCallSign);
            tbNote.DataBindings.Add("Text", _net, NetObject.FieldNote);
            //numWorkCount.DataBindings.Add("Value", _net, NetObject.FieldWorkCountNotNull, true, DataSourceUpdateMode.OnPropertyChanged);
            //cbAutoSetArticle.DataBindings.Add("Checked", _net, NetObject.FieldNeedSetArticleAuto);
            {
                Binding bnd = new Binding("Text", _net, NetObject.FieldWorkCountSuggest);
                bnd.Format += (sen, ev) =>
                                  {
                                      int workCount;
                                      if (int.TryParse(ev.Value.ToString(), out workCount) == false)
                                          workCount = IM.NullI;
                                      if ((workCount == 0) || (workCount == IM.NullI))
                                          ev.Value = "";
                                      else
                                          ev.Value = workCount.ToString();
                                  };
               // tbWorkCountSuggest.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Text", _net, NetObject.FieldArticleIdSuggest);
                bnd.Format += (sen, ev) =>
                                  {
                                      int idPrice;
                                      if(int.TryParse(ev.Value.ToString(), out idPrice) == false)
                                          idPrice = IM.NullI;
                                      CPrice price = new CPrice();
                                      price.Read(idPrice);
                                      ev.Value = price.zsARTICLE;
                                  };
              //  tbArticleSuggest.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Enabled", _net, NetObject.FieldArticleIdSuggest);
                bnd.Format += (sen, ev) =>
                                  {
                                      int idPrice;
                                      if (int.TryParse(ev.Value.ToString(), out idPrice) == false)
                                          idPrice = IM.NullI;
                                      if(idPrice == IM.NullI)
                                          ev.Value = false;
                                      else
                                          ev.Value = true;
                                  };
               // btnArticleUpdate.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Enabled", _net, NetObject.FieldWorkCountSuggest);
                bnd.Format += (sen, ev) =>
                                  {
                                      int workCount;
                                      if (int.TryParse(ev.Value.ToString(), out workCount) == false)
                                          workCount = IM.NullI;
                                      if (curDept == UcrfDepartment.Branch)
                                      {
                                          if ((workCount == 0) || (workCount == IM.NullI) || (!_net.CanConfirmArticleCustom()))
                                          {
                                              ev.Value = false;
                                          }
                                          else
                                              ev.Value = true;
                                      }
                                      else 
                                      {
                                          if ((workCount == 0) || (workCount == IM.NullI))
                                            {
                                          ev.Value = false;
                                            }
                                          else
                                              ev.Value = true;
                                      }
                                      
                                  };
              //  btnWorkCountUpdate.DataBindings.Add(bnd);
            }
            {
                Binding bind = new Binding("Text", _net, NetObject.FieldOwnerId);
                bind.Format += (s, ev) =>
                                   {
                                       int idOwner = (int)ev.Value;
                                       if (_enterOwnerName == null)
                                       {
                                           ev.Value = "";
                                           using (Icsm.LisRecordSet rsOwner = new Icsm.LisRecordSet(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly))
                                           {
                                               rsOwner.Select("ID,NAME");
                                               rsOwner.SetWhere("ID", IMRecordset.Operation.Eq, idOwner);
                                               rsOwner.Open();
                                               if (!rsOwner.IsEOF())
                                                   ev.Value = rsOwner.GetS("NAME");
                                           }
                                       }
                                       else
                                           ev.Value = _enterOwnerName;
                                   };
                bind.Parse += (s, ev) =>
                                  {
                                      _enterOwnerName = ev.Value.ToString();
                                      ev.Value = _net.OwnerId;
                                  };
                tbOwner.DataBindings.Add(bind);
            }
            //----
            btnOk.DataBindings.Add("Enabled", _net, NotifyPropertyChanged.FieldIsChanged);
            {
                Binding bnd = new Binding("Enabled", _net, NetObject.FieldIsConfirmed);
                bnd.Format += (sen, ev) =>
                                  {
                                      bool isConfirmed;
                                      if (Boolean.TryParse(ev.Value.ToString(), out isConfirmed) == false)
                                          isConfirmed = false;

                                      //
                                      bool setVal = !isConfirmed && _net.CanConfirmArticle();
                                      {
                                          if (curDept == UcrfDepartment.Branch)  numWorkCount.Enabled = setVal;
                                      }

                                      ev.Value = setVal;
                                      if (_net.IsConfirmed == false)
                                      {
                                          btnConfirmArticle.Text = setVal ? "Підтвердити статтю" : "Некоректні дані";
                                      }
                                      else
                                      {
                                          btnConfirmArticle.Text = "Стаття підтверджена";
                                      }
                                  };
              //  btnConfirmArticle.DataBindings.Add(bnd);
            }
            //---
            {
                Binding bnd = new Binding("Text", _net, NetObject.FieldConfirmedBy);
                bnd.Format += (sen, ev) =>
                {
                    ev.Value = CUsers.GetUserFio(ev.Value.ToString());
                };
                lblUserConfirmedArticle.DataBindings.Add(bnd);
            }
            //---
            {
                Binding bnd = new Binding("Text", _net, NetObject.FieldConfirmedDate);
                bnd.Format += (sen, ev) =>
                {
                    if (ev.Value is DateTime)
                    {
                        DateTime dt = (DateTime)ev.Value;
                        ev.Value = dt.ToStringNullT();
                    }
                    else
                    {
                        ev.Value = "";
                    }
                };
                lblDateConfirmedArticle.DataBindings.Add(bnd);
            }
            //----
            Dictionary<int, string> dictArticle = new Dictionary<int, string>();
            dictArticle.Add(IM.NullI, CLocaliz.TxT("No article"));
            using (LisProgressBar pb = new LisProgressBar("Загрузка статей"))
            {
                pb.SetProgress(0, 100);
                using (Icsm.LisRecordSet rsArticle = new Icsm.LisRecordSet(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly))
                {
                    rsArticle.Select("ID,ARTICLE,STANDARD,STATUS,DATEOUTACTION,DATESTARTACTION");
                    string department = Enum.GetName(typeof (ManagemenUDCR), ManagemenUDCR.URCM);
                    rsArticle.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
                    rsArticle.SetWhere("STATUS", IMRecordset.Operation.Eq, 1);
                    rsArticle.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
                    rsArticle.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

                    rsArticle.OrderBy("ARTICLE", OrderDirection.Ascending);
                    
                    for (rsArticle.Open(); !rsArticle.IsEOF(); rsArticle.MoveNext())
                    {
                        string articleName = string.Format("{0} ({1})", rsArticle.GetS("ARTICLE"),
                                                           rsArticle.GetS("STANDARD"));
                        int articleid = rsArticle.GetI("ID");

                        if (curDept == UcrfDepartment.Branch)
                        {
                            if (_net.CanConfirmArticleCustomize(articleid))
                            {

                                if ((dictArticle.ContainsKey(articleid) == false))
                                    dictArticle.Add(articleid, articleName);
                                pb.Increment(true);
                            }
                        }
                        else
                        {
                            if (_net.CanConfirmArticleCustomizeNew(articleid))
                            {
                                if ((dictArticle.ContainsKey(articleid) == false))
                                    dictArticle.Add(articleid, articleName);
                                pb.Increment(true);
                            }
                        }

                    }
                }
            }
            _lstPrices1 = new ComboBoxDictionaryList<int, string>();
            _lstPrices1.InitComboBox(cbArticle);
            foreach (KeyValuePair<int, string> pair in dictArticle)
                _lstPrices1.Add(new ComboBoxDictionary<int, string>(pair.Key, pair.Value));
           // cbArticle.DataBindings.Add("SelectedValue", _net, NetObject.FieldArticleId, true, DataSourceUpdateMode.OnPropertyChanged);

            if (curDept == UcrfDepartment.Branch)
            {
                if (!NetObject.CanConfirmArticleCustom(_net.ArticleId))
                {
                    cbArticle.Enabled = false;
                    //  btnAddBs.Enabled = false;
                    //  btnAddAbonent.Enabled = false;
                    //  btnDeleteStation.Enabled = false;
                }
            }
            //----
            InitGrid();
            UpdataCount();

        }

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveNet() == false)
                DialogResult = DialogResult.None;
        }
        /// <summary>
        /// Подтвердить закрытие формы
        /// </summary>
        /// <returns>TRUE - Можна закрыть форму</returns>
        protected override bool ConfirmClose()
        {
            bool retVal = true;
            if (_net.IsChanged)
            {
                const string mess = "Зберегти зміни?";
                DialogResult resDialog = MessageBox.Show(mess, "", MessageBoxButtons.YesNoCancel,
                                                         MessageBoxIcon.Question);
                switch (resDialog)
                {
                    case DialogResult.Yes:
                        retVal = SaveNet();
                        break;
                    case DialogResult.Cancel:
                        retVal = false;
                        break;
                }
            }
            return retVal;
        }
        /// <summary>
        /// Сохраняем изменения
        /// </summary>
        /// <rereturns>True все в порядке, иначе FALSE</rereturns>
        private bool SaveNet()
        {
            /*
            bool iscorrect = true;
            if ((_net.ArticleIdSuggest != IM.NullI) && (_net.ArticleIdSuggest != _net.ArticleId))
                iscorrect = false;
            if (((_net.WorkCountSuggest != IM.NullI) && (_net.WorkCountSuggest != 0)) && (_net.WorkCountSuggest != _net.WorkCount))
                iscorrect = false;

            if (ArticleLib.ArticleFunc.GetStatusForCodePriceID(_net.ArticleId) == false)
            {
                string messError = string.Format(CLocaliz.TxT("The indicated fare is not available, select another fare!"),  Environment.NewLine);
                if (MessageBox.Show(messError, "", MessageBoxButtons.OK, MessageBoxIcon.Warning) != DialogResult.OK)
                    return false;
            }
            if(iscorrect == false)
            {
                const string mess = "Стаття або кіл-ть робіт не співпадають із запропонованими. Продовжити?";
                if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return false;
            }
            if(_net.CanConfirmArticle() && _net.IsConfirmed == false)
            {
                const string mess = "Стаття та кіл-ть робіт не підтвердженні. Продовжити?";
                if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return false;
            }
             */ 
            _net.Save();
            UpdateListGrid();
            return true;
        }

        /// <summary>
        /// Выбераем нового владельца
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectOwner_Click(object sender, EventArgs e)
        {
            int count = _net.BaseStations.Length;
            if(count > 0)
            {
                string mess = "Зміна власника заборонена поки до мережі приєднано базові станції.";
                MessageBox.Show(mess, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string filter = "";
            if (string.IsNullOrEmpty(_enterOwnerName) == false)
                filter = _enterOwnerName;
            string param = "{NAME=\"*" + HelpClasses.HelpFunction.ReplaceQuotaSumbols(filter) + "*\"}";
            RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Seaching of an owner"), ICSMTbl.USERS, param);
            if (user.Id != IM.NullI)
            {
                _enterOwnerName = null;
                _net.OwnerId = user.Id;
            }
        }
        /// <summary>
        /// Добавить нового абонента 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddAbonent_Click(object sender, EventArgs e)
        {
            RecordPtr retRec = NetObject.SelectAbonent(_net.OwnerId, _net.Id);
            if ((retRec.Id > 0) && (retRec.Id != IM.NullI))
            {
                _net.StationList.AddStation(retRec.Id, retRec.Table);
                UpdataCount();
                UpdateListGrid();
                 if (curDept == UcrfDepartment.Branch)  _net.DisableConfirmArticle();
            }
        }

        /// <summary>
        /// Добавить БС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddBs_Click(object sender, EventArgs e)
        {
            int[] baseStationId = (from baseStation in _net.BaseStations select baseStation.Id).ToArray();
            RecordPtr retRec = NetObject.SelectBase(_net.OwnerId, baseStationId);
            if ((retRec.Id > 0) && (retRec.Id != IM.NullI))
            {
                _net.StationList.AddStation(retRec.Id, retRec.Table);
                UpdataCount();
                UpdateListGrid();
                if (curDept == UcrfDepartment.Branch) _net.DisableConfirmArticle();
            }
        }
        /// <summary>
        /// Удалить выбраную станцию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteStation_Click(object sender, EventArgs e)
        {
            int count = _net.SelectedStations.Length;
            if (count == 0)
            {
                string mess = "Не вибрано жодної станції.";
                MessageBox.Show(mess, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                string mess = string.Format("Ви дійсно бажаєте видалити {0} станцій?", count);
                if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    _net.DeleteSelected();
                    UpdataCount();
                    UpdateListGrid();
                    if (curDept == UcrfDepartment.Branch) _net.DisableConfirmArticle();
                }
            }
        }

            //===================================================
        /// <summary>
        /// Инициализируем грид
        /// </summary>
        public void UpdateListGrid()
        {

            dgStation.DataSource = null;
            var sales = new SortableBindingList<NetStationItem>();
            NetStationItem[] tst = new NetStationItem[_net.StationList.Count];

            int i = 0;
            foreach (NetStationItem netStation in _net.StationList)
            {
                tst[i] = netStation;
                i++;
            }
            ILRSort.Sort(tst, "StationTypeNumeric[asc]");
            for (i = 0; i < tst.Count(); i++)
            {
                sales.Add(tst[i]);
            }
            tst = null;
            dgStation.DataSource = new SortableBindingList<NetStationItem>(sales);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="name"></param>
        /// <param name="headerText"></param>
        /// <param name="width"></param>
        /// <param name="readOnly"></param>
        /// <param name="dataPropertyName"></param>
        /// <returns></returns>
        protected DataGridViewColumn AddGridColumn(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName)
        {
            if (column == null)
                column = new DataGridViewTextBoxColumn();
            if (!string.IsNullOrEmpty(name))
                column.Name = name;
            column.HeaderText = headerText;
            column.Width = width;
            column.ReadOnly = readOnly;
            column.DataPropertyName = dataPropertyName;
            dgStation.Columns.Add(column);
            return column;
        }
        //===================================================
        /// <summary>
        /// Инициализируем грид
        /// </summary>
        public virtual void InitGrid()
        {
            DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
            dataGridViewCellStyle.Format = "d";
            dataGridViewCellStyle.NullValue = null;     
      
            dgStation.Columns.Clear();
            dgStation.AutoGenerateColumns = false;
            {
                DataGridViewCheckBoxColumn columnCb = new DataGridViewCheckBoxColumn();
                columnCb.HeaderText = "";
                columnCb.Name = "gridPacketCB";
                columnCb.FlatStyle = FlatStyle.Standard;
                columnCb.ThreeState = true;
                columnCb.CellTemplate = new DataGridViewCheckBoxCell();
                columnCb.Width = 30;
                columnCb.TrueValue = true;
                columnCb.Resizable = DataGridViewTriState.False;
                columnCb.DataPropertyName = NetStationItem.FieldIsChecked;
                dgStation.Columns.Add(columnCb);
               
            }

            


            // StationType
            DataGridViewTextBoxColumn columnHide = new DataGridViewTextBoxColumn();
            columnHide.HeaderText = "Hide (Type)";
            columnHide.Name = "gridHideType";
            columnHide.Width = 3;
            columnHide.ReadOnly = true;
            columnHide.Visible = false;
            columnHide.DataPropertyName = NetStationItem.FieldStationTypeNumeric;
            dgStation.Columns.Add(columnHide);

            // StationType
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.HeaderText = CLocaliz.TxT("Type of station");
            column.Name = "gridBs";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldStationTypeString;
            dgStation.Columns.Add(column);
            // Permision
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridPermision";
            column.HeaderText = CLocaliz.TxT("Permit number");
            //column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldPermission;
            dgStation.Columns.Add(column);
            // PermisionDateTo
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridPermisionDateTo";
            column.HeaderText = CLocaliz.TxT("Permision Date To");
            column.DefaultCellStyle = dataGridViewCellStyle;
            column.Width = 100;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldPermissionDateTo;
            dgStation.Columns.Add(column);
            // FreqTx
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridFreqTx";
            column.HeaderText = CLocaliz.TxT("Freq Tx Transmitter");
            column.Width = 75;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldFreqTxStr;
            dgStation.Columns.Add(column);
            // FreqRx
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridFreqRx";
            column.HeaderText = CLocaliz.TxT("Freq Rx Receiver");
            column.Width = 75;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldFreqRxStr;
            dgStation.Columns.Add(column);
            // CallSign
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridCallSign";
            column.HeaderText = CLocaliz.TxT("Call Sign");
            //column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldCallSign;
            dgStation.Columns.Add(column);
            // Address
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridAddress";
            column.HeaderText = CLocaliz.TxT("Address");
            //column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldAddress;
            dgStation.Columns.Add(column);

            bool isDebagMode = ApplSetting.IsDebug;
            // ID
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridID";
            column.HeaderText = "ID (Debug)";
            //column.Width = 50;
            column.ReadOnly = true;
            column.Visible = isDebagMode;
            column.DataPropertyName = NetStationItem.FieldId;
            dgStation.Columns.Add(column);
            // TableName
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridTableName";
            column.HeaderText = "TableName (Debug )";
            //column.Width = 50;
            column.ReadOnly = true;
            column.Visible = isDebagMode;
            column.DataPropertyName = NetStationItem.FieldTableName;
            dgStation.Columns.Add(column);
            //добавлено WAS_USED, STATUS, STANDARD

            //WAS_USED
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridWAS_USED";
            column.HeaderText = CLocaliz.TxT("WU");
            column.Width = 30;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldWas_Used;
            dgStation.Columns.Add(column);
            
            //STATUS
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridSTATUS";
            column.HeaderText = CLocaliz.TxT("Status");
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldStatus;
            dgStation.Columns.Add(column);
            
            //STANDARD
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridSTANDARD";
            column.HeaderText = CLocaliz.TxT("Standard"); ;
            column.Width = 55;
            column.ReadOnly = true;
            column.DataPropertyName = NetStationItem.FieldStandard;
            dgStation.Columns.Add(column);


            UpdateListGrid();
          
           
        }
        /// <summary>
        /// Обновляет отображение кол-ва станций
        /// </summary>
        private void UpdataCount()
        {
            tbCountBase.Text = _net.BaseStations.Length.ToString();
            tbCountStationary.Text = _net.StationaryStations.Length.ToString();
            tbCountAbonent.Text = _net.AbonentStations.Length.ToString();
            tbCountAllActived.Text = string.Format("{0}  ({1})", _net.ActiveStations.Length.ToString(), _net.CountActiveWU.ToString());
            tbCountAll.Text = _net.StationList.Count.ToString();
            //--------
            foreach (Binding bind in tbArticleSuggest.DataBindings)
		        bind.ReadValue();
            foreach (Binding bind in tbWorkCountSuggest.DataBindings)
                bind.ReadValue();


            tbWorkCountSuggest.Text = "";
            tbArticleSuggest.Text = "";
            tbWorkCountSuggest.Enabled = false;
            tbArticleSuggest.Enabled = false;
            btnArticleUpdate.Enabled = false;
            btnWorkCountUpdate.Enabled = false;
            btnConfirmArticle.Enabled = false;
            cbAutoSetArticle.Enabled = false;
            cbArticle.Enabled = false;
            numWorkCount.Enabled = false;
        }
        /// <summary>
        /// Открываем заявку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgStation_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           if ((-1 < e.RowIndex) && (e.RowIndex < dgStation.RowCount))
            {
                var item = dgStation.Rows[e.RowIndex].DataBoundItem;
                if (item is NetStationItem)
                {
                    NetStationItem stationItem = (NetStationItem) item;
                    ApplSource.MainAppForm.ShowApplForm(stationItem.TableName, stationItem.Id, false);
                }
            }
          if ((e.RowIndex == -1) && (e.ColumnIndex == 0))
           {
               dgStation.EndEdit();
               DataGridViewColumn column = dgStation.Columns[e.ColumnIndex];
               if (column.HeaderText != "X")
               {
                   column.HeaderText = "X";
                   SetAllApplCheckBox(true);
               }
               else
               {
                   column.HeaderText = "";
                   SetAllApplCheckBox(false);
               }
               dgStation.Refresh();
           }
          
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void SetAllApplCheckBox(bool value)
        {
            foreach (NetStationItem netStation in _net.StationList)
            {
                netStation.IsChecked = value;
            }
        }
        /// <summary>
        /// Прорисовка строки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgStation_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if ((-1 < e.RowIndex) && (e.RowIndex < dgStation.RowCount))
            {
                var item = dgStation.Rows[e.RowIndex].DataBoundItem;
                if (item is NetStationItem)
                {
                    NetStationItem stationItem = (NetStationItem)item;
                    dgStation.Rows[e.RowIndex].DefaultCellStyle.BackColor = (stationItem.IsActive)
                                                                                ? System.Drawing.Color.White
                                                                                : System.Drawing.Color.Wheat;
                }
            } 

        }


        /// <summary>
        /// Установить статью по умолчания
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArticleUpdate_Click(object sender, EventArgs e)
        {
            _net.ArticleId = _net.ArticleIdSuggest;
            UpdateListGrid();
        }
        /// <summary>
        /// Установить кол-во работ по умолчания
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWorkCountUpdate_Click(object sender, EventArgs e)
        {
            _net.WorkCountNotNull = _net.WorkCountSuggest;
            UpdateListGrid();
        }

        /// <summary>
        /// Подтверждение статьи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirmArticle_Click(object sender, EventArgs e)
        {
            try
            {
                _net.SetConfirmArticle();
                UpdateListGrid();
            }
            catch(IllegalArticleException ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
