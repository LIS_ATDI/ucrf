using System;
using System.Drawing;
using System.Collections;

namespace GridCtrl
{
	/// <summary>
	/// Row provides implementation for a grid row
	/// </summary>
	/// <remarks>
	/// <para>
	/// You should inherit from Row if you want to change the appearance or function of
	/// a grid row
	/// </para>
	/// </remarks>
	public class Row : GridObject
	{
		public CellCollection		cellList		= new CellCollection();		
		
		public int Count
		{			
			get
			{
				return cellList.Count;
			}
		}

		public override ObjectType GetObjectType()
		{
			return GridObject.ObjectType.Row;
		}

		// the destructor
		~Row()
		{		
			// call Dispose with false.  Since we're in the
			// destructor call, the managed resources will be
			// disposed of anyways.
			Dispose(false);
		}

		protected override void Dispose(bool disposeManagedResources)
		{
			// process only if mananged and unmanaged resources have
			// not been disposed of.
			if (!this.disposed)
			{
				if (disposeManagedResources)
				{
					foreach (Cell c in cellList)
					{
						c.Image = null;
					}
				}
				disposed=true;
			}
			else
			{
				// Resources already disposed
			}
		}

		/// <summary>
		/// Gets a gell at a given column index
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public Cell GetCell(int c)
		{
			if (c >= cellList.Count)
				throw new GridException("Cell out of range");
			return cellList[c];
		}

		/// <summary>
		/// Draws a row of cells
		/// </summary>
		/// <param name="xOffset"></param>
		/// <param name="y"></param>
		/// <param name="g"></param>
		public virtual void Draw(int xOffset, int y, Graphics g)
		{
			int x = 0;
			int xCnt=0;			
			foreach (Cell c in cellList)
			{
				if (c.Visible)
				{
					if (/*!c.col.Header &&*/ xCnt < xOffset)
					{
						xCnt += c.Width;
						continue;
					}

					c.Draw(x,y,g);
					x += c.Width;
				}
			}
		}

		/// <summary>
		/// Constructs a row with a given height
		/// </summary>
		/// <param name="grid"></param>
		/// <param name="defaultHeight"></param>
		public Row(Grid grid, int defaultHeight)
		{
			
			this.grid = grid;
			Size = defaultHeight;
			//cellList.Add(new Cell(this));
			//count = 1;
		}
	
		/// <summary>
		/// Adds a cell to a given column
		/// </summary>
		/// <param name="width"></param>
		/// <returns></returns>
		public Cell AddCell(string value, int width)
		{
			Cell cell = new Cell(this);
			cell.Value = value;
         cell.Width = width;
			cell.FixedWidth = true;
			cellList.Add(cell);
		//	ResizeRow();			
			return cell;
		}

		/// <summary>
		/// Adds a cell to a given column
		/// </summary>
		/// <returns></returns>
		public Cell AddCell(string value)
		{
			Cell cell = new Cell(this);
			cell.Value = value;
			cell.FixedWidth = false;
			cellList.Add(cell);
		//	ResizeRow();
			return cell;
		}

		/// <summary>
		/// Adds a cell to a given column
		/// </summary>
		/// <returns></returns>
		public Cell AddCell(string value, EditStyle style)
		{
			Cell cell = new Cell(this);
			cell.Value = value;
			cell.FixedWidth = false;
			cell.cellStyle = style;
			cellList.Add(cell);
		//	ResizeRow();
			return cell;
		}

		/// <summary>
		/// Adds a cell to a given column
		/// </summary>
		/// <returns></returns>
		public Cell AddCell(object value, Cell cell)
		{			
			cellList.Add(cell);
		//	ResizeRow();
			return cell;
		}

		public void DeleteCell(int col)
		{
			if (col >= cellList.Count)
				throw new GridException("Column is out of range!");
			cellList.RemoveAt(col);
		//	ResizeRow();
		}

		/// <summary>
		/// Gets the value of a row (header) from a given column 
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		public virtual object Value(int col)
		{
			if (col >= cellList.Count)
				throw new GridException("Column number is out of range!");
			return cellList[col].Value; //grid.GetColList().IndexOf(col).ToString();
		}

		public void ReplaceCell(Cell replaceCell, Cell cell)
		{
			int index = replaceCell.Col;

			cellList.RemoveAt(index);
			cell.Attach(replaceCell.row, replaceCell.Col);
			cellList.Insert(index, cell);
		//	ResizeRow();
		}

		public void ResizeRow(Graphics g)
		{
			if (cellList.Count <= 0)
				return;
			cellList[0].Width = grid.FirstColumnWidth;
			int sumWidth = 0;
			int cellCount = 0;
			int maxheight = 0;		
			for (int i = 0; i < cellList.Count; ++i)
			{
				int tempHeight = cellList[i].ResizeCell(g);
				if (tempHeight > maxheight)
					maxheight = tempHeight;
				if (cellList[i].FixedWidth && i > 0 )//&& cellList[i].Visible)
				{
					sumWidth += cellList[i].Width;
					++cellCount;
				}
			}
			if (this.Size != maxheight)
				this.Size = maxheight;
			if (sumWidth > grid.OtherColumnsWidth)
				throw new GridException("Row's width is bigger than grid's width");

		   int cellListCount = 0;//cellList.Count - cellCount - 1;
         for (int i = 0; i < cellList.Count; i++)
            if (cellList[i].Visible)
               cellListCount++;

         cellCount = cellListCount - cellCount - 1;

            if (cellCount != 0)
            {
               int colWidth = (grid.OtherColumnsWidth - sumWidth) / cellCount;

               int sWidth = 0;

               int lastColWidth = grid.OtherColumnsWidth - sumWidth;
               for (int i = 1, j = 1; i < cellList.Count; ++i)
                  if (!cellList[i].FixedWidth)// && cellList[i].Visible)
                  {
                     if (j < cellCount)
                     {
                        cellList[i].Width = colWidth;
                        ++j;
                        lastColWidth -= colWidth;
                        sWidth += colWidth;
                     }
                     else
                     {
                        cellList[i].Width = lastColWidth;
                        sWidth += lastColWidth;
                     }
                  }
            }

		}
	}	
}
