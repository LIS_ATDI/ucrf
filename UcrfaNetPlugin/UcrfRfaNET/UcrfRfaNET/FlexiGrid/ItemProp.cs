﻿using System.Collections.Generic;

namespace GridCtrl
{
	public enum EditStyle
	{
		/// <summary>
		/// Simple cell
		/// </summary>
		esSimple,
		/// <summary>
		/// cell with button
		/// </summary>
		esEllipsis,
		/// <summary>
		/// cell with list
		/// </summary>
		esPickList
	}
  
   public class KeyedPickList
   {
      private List<string> keyList = new List<string>();
      private List<string> valueList = new List<string>();

      public void AddToPickList(string key, string value)
      {
         keyList.Add(key);
         valueList.Add(value);
      }
      public int Count
      {
         get { return keyList.Count; }         
      }

      public string GetKeyByIndex(int Index)
      {
         return keyList[Index];
      }

      public string GetValueByIndex(int Index)
      {
         return valueList[Index];
      }

      public string GetValueByKey(string key)
      {
         string result = "";
              
         for (int i = 0; i < keyList.Count; i++)
         {
            if (keyList[i]==key)
               result = valueList[i];
         }
         return result;
      }
   }	

	class ItemProp
	{
		public EditStyle edStyle;   //Стиль редактирования
		public string ButtonText;      //Текст на кнопочке
      [System.Obsolete("Use KeyPickList")]
		public List<string>  PickList;    //Указатель на список для ComboBox
      
      public KeyedPickList KeyPickList;
		
		public ItemProp()
		{
			edStyle = EditStyle.esSimple;
			PickList = null;
         KeyPickList = null;
			ButtonText = "...";
		}
	}
	
   
}
