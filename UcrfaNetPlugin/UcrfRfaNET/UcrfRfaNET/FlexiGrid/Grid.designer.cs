﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GridCtrl
{
	partial class Grid
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
					components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code


		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Grid
			// 
			this.AllowDrop = true;
			this.Name = "Grid";
			this.Size = new System.Drawing.Size(288, 200);
			this.Resize += new System.EventHandler(this.Grid_Resize);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Grid_MouseUp);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Grid_MouseMove);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Grid_MouseDown);
			this.KeyDown += new KeyEventHandler(Grid_KeyDown);
		}

		#endregion
	}
}
