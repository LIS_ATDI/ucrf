using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;

using XICSM.UcrfRfaNET;
using XICSM.UcrfRfaNET.ApplSource;

namespace GridCtrl
{
    public class CustomBinder
    {
        Object control;
        object dataSource;

        PropertyDescriptor controlPropertyDescriptor;
        PropertyDescriptor dataSourcePropertyDescriptor;

        public CustomBinder(object newControl, string controlPropertyName,
                       object dataSource, string dataSourcePropertyName)
        {
            this.control = newControl;
            this.dataSource = dataSource;


            // �������� ��������� ������ PropertyDescriptor 
            // ��� ���������� ��������� �������� ����������
            controlPropertyDescriptor =
              TypeDescriptor.GetProperties(control).Find(controlPropertyName, true);

            if (controlPropertyDescriptor == null)
                throw new ArgumentException(
                  String.Format("�� ������� ����� �������� �������� ���������� � ������ {0}",
                    controlPropertyName), "controlPropertyName");
            if (controlPropertyDescriptor.SupportsChangeEvents)
                controlPropertyDescriptor.AddValueChanged(
                  control, ControlPropertyChanged);

            // �������� ��������� ������ PropertyDescriptor 
            // ��� ���������� ��������� ��������� ������
            dataSourcePropertyDescriptor =
              TypeDescriptor.GetProperties(dataSource).Find(
                dataSourcePropertyName, true);
            if (dataSourcePropertyDescriptor == null)
                throw new ArgumentException(
                  String.Format("�� ������� ����� �������� ��������� ������ � ������ {0}",
                    dataSourcePropertyName, "dataSourcePropertyName"));

            if (dataSourcePropertyDescriptor.SupportsChangeEvents)
                dataSourcePropertyDescriptor.AddValueChanged(
                  dataSource, DataSourcePropertyChanged);

            // ��������� ������� �������� � ��������� 
            // �������� �������� �������� ����������
            DataSourcePropertyChanged(this, EventArgs.Empty);
        }

        public bool HasAttributes(string controlPropertyName,
                       object dataSource, string dataSourcePropertyName)
        {
            return (dataSource == this.dataSource &&
                controlPropertyDescriptor.Name == controlPropertyName &&
                dataSourcePropertyDescriptor.Name == controlPropertyName);
        }

        // ���������� ������� ��������� �������� �������� ����������
        private void ControlPropertyChanged(object sender, EventArgs e)
        {
            // �������� ����� �������� �������� �������� ����������
            object controlPropertyValue = controlPropertyDescriptor.GetValue(control);
            //����� ��������� �������� ��������� ������ ����� �������� ������, 
            //�.�. ���� ������� ����� �� ���������.
            //��� ����� ������������� TypeConverter-���, ������� �������� ������ 
            //������ PropertyDescriptor
            if (
              controlPropertyDescriptor.Converter.CanConvertTo(
                dataSourcePropertyDescriptor.PropertyType))
            {
                object convertedValue =
                  controlPropertyDescriptor.Converter.ConvertTo(
                    controlPropertyValue,
                    dataSourcePropertyDescriptor.PropertyType);
                //�������� �������� �������� ��������� ������
                try
                {
                    if (dataSourcePropertyDescriptor.GetValue(dataSource).ToString() != convertedValue.ToString())
                        dataSourcePropertyDescriptor.SetValue(dataSource, convertedValue);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (
              dataSourcePropertyDescriptor.Converter.CanConvertFrom(
                controlPropertyDescriptor.PropertyType))
            {
                object convertedValue =
                  dataSourcePropertyDescriptor.Converter.ConvertFrom(
                    controlPropertyValue);
                //�������� �������� �������� ��������� ������
                try
                {
                    if (dataSourcePropertyDescriptor.GetValue(dataSource).ToString() != convertedValue.ToString())
                        dataSourcePropertyDescriptor.SetValue(dataSource, convertedValue);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    //throw;
                }
                //dataSourcePropertyDescriptor.SetValue(dataSource, convertedValue);

            }
        }

        //���������� ������� ��������� �������� ��������� ������
        private void DataSourcePropertyChanged(object sender, EventArgs e)
        {
            //�������� ����� �������� �������� ��������� ������
            object dataSourceValue =
              dataSourcePropertyDescriptor.GetValue(dataSource);

            if (dataSourcePropertyDescriptor.PropertyType == controlPropertyDescriptor.PropertyType)
            {
                if (dataSourceValue != null)
                    if (controlPropertyDescriptor.GetValue(control).ToString() != dataSourceValue.ToString())
                        controlPropertyDescriptor.SetValue(control, dataSourceValue);
                return;
            }
            // ����� ��������� �������� �������� ���������� ���������� �������� 
            // ������, �.�. ���� ������� ����� �� ���������.
            // ������������� TypeConverter-���, ������� �������� ������
            // ������ PropertyDescriptor
            if (
              dataSourcePropertyDescriptor.Converter.CanConvertTo(
                controlPropertyDescriptor.PropertyType))
            {
                object convertedValue =
                  dataSourcePropertyDescriptor.Converter.ConvertTo(
                    dataSourceValue,
                    controlPropertyDescriptor.PropertyType);
                // object value = 
                //   controlPropertyDescriptor.Converter.ConvertFrom(dataSourceValue);
                //�������� �������� �������� �������� ����������
                if (controlPropertyDescriptor.GetValue(control).ToString() != convertedValue.ToString())
                    controlPropertyDescriptor.SetValue(control, convertedValue);
            }
            else if (

              controlPropertyDescriptor.Converter.CanConvertFrom(
                dataSourcePropertyDescriptor.PropertyType))
            {
                object ConvertedValue =
                  controlPropertyDescriptor.Converter.ConvertFrom(
                    dataSourceValue);
                controlPropertyDescriptor.SetValue(control, ConvertedValue);
            }
        }
        //�������� ����
    }


    /// <summary>
    /// Summary description for Cell.
    /// </summary>	
    public class Cell : Object, INotifyPropertyChanged
    {
        private List<CustomBinder> binderList = new List<CustomBinder>();
        List<CellStringProperty.EventDescriptor> eventList = new List<CellStringProperty.EventDescriptor>();

        public event PropertyChangedEventHandler PropertyChanged;


        public int HorizontalMargin { get; set; }
        private const string IsChangedField = "IsChanged";
        public bool IsChanged { get; set; }

        public bool IsBinded()
        {
            return binderList.Count > 0;
        }

        private void NotifyPropertyChanged(String info)
        {            
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
            if ((info != IsChangedField) &&
                ((info == ValueField) || (info == Value2Field))) //�������� ��������� ������ �� ���� Value � Value2
                IsChanged = true;
        }

        /*public enum HorizontalAlignmentType
        {
            Left,
            Center,
            Right
        }*/

        public enum VerticalAlignmentType
        {
            Top,
            Center,
            Bottom
        }

        public enum ImageAlignmentType
        {
            TopLeft,
            TopCenter,
            TopRight,
            MiddleLeft,
            MiddleCenter,
            MiddleRight,
            BottomLeft,
            BottomCenter,
            BottomRight,
        }

        public string Type { get; set; }  //��� ������
        public string DBField { get; set; }  //��� ���� � ��

        private int col = -1; // Attached Column
        public Row row = null; // Attached Row
        private Color clrBack = SystemColors.Window;
        private object rObject = null;
        private string cellValue1 = "";	// Default to string
        private string cellValue2 = "";	// Second string
        private string cellKey = ""; // Cell key
        private int tabOrder = -1; // Tab order for cell
        private int textHeight = 16; // Auto
        private HorizontalAlignment horzAlign = HorizontalAlignment.Center;
        private VerticalAlignmentType vertAlign = VerticalAlignmentType.Center;
        private Color textColor1 = SystemColors.WindowText;
        private Color textColor2 = Color.Red;
        private Image image = null;
        private ImageAlignmentType imageAlignment = ImageAlignmentType.MiddleLeft;
        private string fontName = Control.DefaultFont.FontFamily.GetName(0);
        private FontStyle fontStyle = FontStyle.Regular;
        private bool bSelected = false;
        private bool canEdit = true;
        private bool isValid = true;
        private string tipText = "";
        private int width = 0;
        private bool fixedWidth = false;
        private bool visible = true;
        private ItemProp pStyleForm = null;             //��������� �� ����� ����� �����	
        private List<string> valueList1 = null;
        private List<string> valueList2 = null;
        //���������� ������ ������ � ������ ������������� ������
        private int HalfWidthCell
        {
            get
            {
                int retVal = width;
                if (!string.IsNullOrEmpty(Value2) && !string.IsNullOrEmpty(Value) && (Value2 != Value))
                    retVal /= 2;
                return retVal;
            }
        }

        //private     List<string>            keyList     = new List<string>();		
        private List<string> valueList = new List<string>();

        public Cell()
        {
            pStyleForm = new ItemProp();
            Type = "";
            DBField = "";
            IsChanged = false;
        }

        public Cell(Row row)
            : this()
        {
            this.row = row;
        }
                
        public string ButtonText
        {
            get
            {
                if (pStyleForm.edStyle == EditStyle.esEllipsis)
                    return pStyleForm.ButtonText;
                else
                    return "";
            }
            set
            {
                if (pStyleForm.edStyle == EditStyle.esEllipsis)
                    pStyleForm.ButtonText = value;
            }
        }

        public List<string> PickList
        {
            get
            {
                return pStyleForm.PickList;
            }
            set
            {
                pStyleForm.PickList = value;
            }
        }

        public KeyedPickList KeyPickList
        {
            get
            {
                return pStyleForm.KeyPickList;
            }
            set
            {
                pStyleForm.KeyPickList = value;
                // ��������, ����� �������� ������������, ����� ��������� ������.
                Invalidate(true);
            }
        }

        public EditStyle cellStyle
        {
            get
            {
                return pStyleForm.edStyle;
            }
            set
            {
                pStyleForm.edStyle = value;
            }
        }

        public string TipText
        {
            get
            {
                return tipText;
            }

            set
            {
                tipText = value;
            }
        }

        public int TabOrder
        {
            set
            {
                tabOrder = value;
            }

            get
            {
                return tabOrder;

            }
        }

        public int Col
        {
            get
            {
                if (row != null)
                    return row.cellList.IndexOf(this);
                else
                    return -1;
            }
        }

        public int Width
        {
            set
            {
                width = value;
            }

            get
            {
                return width;
            }
        }

        public int TextHeight
        {
            set
            {
                textHeight = value;
            }

            get
            {
                return textHeight;
            }
        }

        public bool Selected
        {
            get
            {
                return bSelected;
            }

            set
            {
                bSelected = value;
                Invalidate(true);
            }
        }

        public bool FixedWidth
        {
            get
            {
                return fixedWidth;
            }
            set
            {
                fixedWidth = value;
                Invalidate(true);
            }
        }

        public void Attach(Row r, int c)
        {
            row = r;
            col = c;
        }

        public bool IsValid
        {
            get
            {
                return isValid;
            }

            set
            {
                if (isValid != value)
                {
                    isValid = value;                    
                }
            }
        }

        public bool CanEdit
        {
            get
            {
                return canEdit;
            }

            set
            {
                if (canEdit != value)
                {
                    canEdit = value;
                    Invalidate(true);
                    NotifyPropertyChanged("CanEdit");
                }
            }
        }

        public bool Visible
        {
            get
            {
                return visible;
            }

            set
            {
                visible = value;
            }
        }

        public Color TextColor
        {
            get
            {
                return textColor1;
            }

            set
            {
                //textColor1 = value;
                //Invalidate(false);
                if (textColor1 != value)
                {
                    textColor1 = value;
                    Invalidate(true);
                    NotifyPropertyChanged("TextColor");
                }
            }
        }

        public Color TextColor2
        {
            get
            {
                return textColor2;
            }

            set
            {
                //textColor2 = value;
                //Invalidate(false);

                //???
                if (textColor2 != value)
                {
                   textColor2 = value;
                   Invalidate(true);
                   NotifyPropertyChanged("TextColor2");
                }

            }
        }


        public FontStyle FontStyle
        {
            get
            {
                return fontStyle;
            }

            set
            {
                fontStyle = value;
                Invalidate(false);
            }
        }

        public Image Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
                Invalidate(false);
            }

        }

        public string FontName
        {
            get
            {
                return fontName;
            }

            set
            {
                fontName = value;
                Invalidate(false);
            }

        }

        public ImageAlignmentType ImageAlignment
        {
            get
            {
                return imageAlignment;
            }

            set
            {
                imageAlignment = value;

            }
        }

        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return horzAlign;
            }

            set
            {
                horzAlign = value;
                Invalidate(false);

            }
        }

        public VerticalAlignmentType VerticalAlignment
        {
            get
            {
                return vertAlign;
            }

            set
            {
                vertAlign = value;
                Invalidate(false);

            }
        }

        public Color BackColor
        {
            get
            {
                return clrBack;
            }

            set
            {
               //clrBack = value;
               //Invalidate(true);
                if (clrBack != value)
                {
                    clrBack = value;
                    Invalidate(true);
                    NotifyPropertyChanged("BackColor");
                }                
            }
        }

        private string DisplayTextValue
        {
            get
            {
                string retVal = Value;
                if ( KeyPickList != null)//(cellStyle == EditStyle.esPickList &&) KeyPickList != null)
                {
                    for (int i = 0; i < KeyPickList.Count; i++)
                        if (KeyPickList.GetKeyByIndex(i) == retVal)
                        {
                            retVal = KeyPickList.GetValueByIndex(i);
                            break;
                        }
                }
                return retVal;
            }
        }

        private string DisplayTextValue2
        {
            get
            {
                string retVal = Value2;
                /*(cellStyle == EditStyle.esPickList &&)*/
                if (KeyPickList != null)
                {
                    for (int i = 0; i < KeyPickList.Count; i++)
                        if (KeyPickList.GetKeyByIndex(i) == retVal)
                        {
                            retVal = KeyPickList.GetValueByIndex(i);
                            break;
                        }
                }
                return retVal;
            }
        }

        private const string ValueField = "Value";
        public string Value
        {
            get
            {
                return cellValue1;
            }
            set
            {
                string theNewValue = value;

                /// ===(!) Dangerous code ==============
                if (/*cellStyle == EditStyle.esPickList &&*/ KeyPickList != null)// && PickList==null)
                {
                    //cellValue1 = value;
                    for (int i = 0; i < KeyPickList.Count; i++)
                        if (KeyPickList.GetValueByIndex(i) == value)
                            theNewValue = KeyPickList.GetKeyByIndex(i);
                }

                if (cellValue1 != theNewValue)
                    CallOnBeforeChangeEventHandlers(ref theNewValue);

                if (cellValue1 != theNewValue)
                {
                    cellValue1 = theNewValue;

                    if (this.row.grid.SelectedCell == this)
                        this.row.grid.RefreshSelectedCell();

                    Invalidate(true);

                    NotifyPropertyChanged(ValueField);
                    CallOnAfterChangeEventHandlers();
                }
            }
        }

        private const string Value2Field = "Value2";
        public string Value2
        {
            get
            {
                return cellValue2;
            }

            set
            {
                if (cellValue2 != value)
                {
                    cellValue2 = value;
                    if (this.row.grid.SelectedCell == this)
                        this.row.grid.RefreshSelectedCell();
                    Invalidate(true);
                    NotifyPropertyChanged(Value2Field);
                }
            }
        }

        public string Key
        {
            get
            {
                return cellKey;
            }

            set
            {
                cellKey = value;
                Invalidate(false);
            }
        }

        public object Tag
        {
            get
            {
                return rObject;
            }

            set
            {
                rObject = value;
                Invalidate(false);
            }
        }

        public virtual void Draw(int x, int y, Graphics g)
        {            
            if (visible)
            {
                Rectangle r = new Rectangle(x, y, GetSize().Width, GetSize().Height);
                bool bSel = bSelected;

                Color clr = /*bSel ? Color.FromArgb(255, SystemColors.Highlight) :*/ Color.FromArgb(row.grid.GetGridOpacity(), clrBack);
                /*if (bSel)
                    fontColor = SystemColors.HighlightText;*/

                g.FillRectangle(new SolidBrush(clr), r);

                if (row.grid.GridLine == Grid.GridLineStyle.Both)
                    g.DrawRectangle(new Pen(row.grid.GridLineColor, 1), r);

                if (row.grid.GridLine == Grid.GridLineStyle.Horizontal)
                {
                    g.DrawLine(new Pen(row.grid.GridLineColor, 1), r.X, r.Y, r.Right, r.Y);
                    g.DrawLine(new Pen(row.grid.GridLineColor, 1), r.X, r.Bottom, r.Right, r.Bottom);
                }

                if (row.grid.GridLine == Grid.GridLineStyle.Vertical)
                {
                    g.DrawLine(new Pen(row.grid.GridLineColor, 1), r.X, r.Y, r.X, r.Bottom);
                    g.DrawLine(new Pen(row.grid.GridLineColor, 1), r.Right, r.Y, r.Right, r.Bottom);
                }

                int fontHeight = textHeight;
                if (fontHeight == -1)
                    fontHeight = r.Height - 2;

                StringFormat sf = new StringFormat();

                switch (horzAlign)
                {
                    case HorizontalAlignment.Center:
                        sf.Alignment = StringAlignment.Center;
                        break;
                    case HorizontalAlignment.Left:
                        sf.Alignment = StringAlignment.Near;
                        break;
                    case HorizontalAlignment.Right:
                        sf.Alignment = StringAlignment.Far;
                        break;
                    default:
                        sf.Alignment = StringAlignment.Near;
                        break;
                }

                switch (vertAlign)
                {
                    case VerticalAlignmentType.Center:
                        sf.LineAlignment = StringAlignment.Center;
                        break;
                    case VerticalAlignmentType.Top:
                        sf.LineAlignment = StringAlignment.Near;
                        break;
                    case VerticalAlignmentType.Bottom:
                        sf.LineAlignment = StringAlignment.Far;
                        break;
                    default:
                        sf.LineAlignment = StringAlignment.Near;
                        break;

                }

                sf.FormatFlags = StringFormatFlags.NoWrap;
                sf.Trimming = StringTrimming.None;

                Rectangle rcImage = r;
                r.X += HorizontalMargin;
                r.Width -= HorizontalMargin;

                if (image != null)
                {
                    int height = image.Size.Height;
                    if (height > r.Height)
                        height = r.Height;

                    int width = image.Size.Width;
                    if (width > r.Width)
                        width = r.Width;

                    switch (imageAlignment)
                    {
                        case ImageAlignmentType.TopLeft:
                            r.Y += height;
                            break;
                        case ImageAlignmentType.TopCenter:
                            r.Y += height;
                            break;
                        case ImageAlignmentType.TopRight:
                            r.Y += height;
                            break;

                        case ImageAlignmentType.MiddleLeft:
                            r.X += width;
                            g.DrawImage(image, rcImage.X, rcImage.Y, width, height);
                            break;
                        case ImageAlignmentType.MiddleCenter:
                            break;
                        case ImageAlignmentType.MiddleRight:
                            break;

                        case ImageAlignmentType.BottomLeft:
                            r.Height -= height;
                            break;
                        case ImageAlignmentType.BottomCenter:
                            r.Height -= height;
                            break;
                        case ImageAlignmentType.BottomRight:
                            r.Height -= height;
                            break;
                    }
                }

                //if (this != this.row.grid.SelectedCell)
                //{
                    r.Width = HalfWidthCell;
                    Font drawFont = new Font(fontName, fontHeight, fontStyle, GraphicsUnit.Pixel);
                    SolidBrush drawBrush = new SolidBrush(textColor1);
                    SolidBrush drawBrush2 = new SolidBrush(TextColor2);
                    int offset = 0;

                    string display2 = DisplayTextValue2;
                    if (DisplayTextValue != display2 && !string.IsNullOrEmpty(display2))
                    {
                        if ((valueList2 != null) && (valueList2.Count > 0))
                        {
                            offset = r.Width;
                            DrawTestCell(r, g, drawFont, valueList2.ToArray(), drawBrush, sf);
                        }
                        if ((valueList1 != null) && (valueList1.Count > 0))
                        {
                            r.X += offset;
                            DrawTestCell(r, g, drawFont, valueList1.ToArray(), drawBrush2, sf);
                        }
                    }
                    else
                    {
                        if ((valueList1 != null) && (valueList1.Count > 0))
                        {
                            r.Width = HalfWidthCell;
                            DrawTestCell(r, g, drawFont, valueList1.ToArray(), drawBrush, sf);
                        }
                    }
                //}
            }
        }

        private void DrawTestCell(Rectangle r, Graphics g, Font drawFont, string[] valueList, SolidBrush drawBrush, StringFormat sf)
        {
            r.Height = (int)g.MeasureString("A", drawFont).Height;
            foreach (string s in valueList)
            {
                g.DrawString(s, drawFont, drawBrush, r, sf);
                r.Y += (int)g.MeasureString(s, drawFont).Height + 2;
            }
        }

        protected Size GetSize()
        {
            return new Size((int)width, (int)row.Size);
        }

        public void BeginEdit()
        {

        }

        public int ResizeCell(Graphics g)
        {
            int fontHeight = textHeight;
            if (fontHeight == -1)
                fontHeight = row.Size - 2;
            Font cellFont = new Font(fontName, fontHeight, fontStyle, GraphicsUnit.Pixel);

            // ��������
            if (string.IsNullOrEmpty(cellValue1))
                cellValue1 = "";

            int cellHeight = 0;
            try
            {
                cellHeight = GridCtrl.StringEditing.BreakString(DisplayTextValue, cellFont, HalfWidthCell, g, out  valueList1);
                int cellHeight2 = GridCtrl.StringEditing.BreakString(DisplayTextValue2, cellFont, HalfWidthCell, g, out  valueList2);
                if (cellHeight < cellHeight2)
                    cellHeight = cellHeight2;
            }
            catch
            {
                cellHeight = 0;
            }
            return cellHeight;
        }

        protected void Invalidate(bool forceUpdate)
        {
            // Invalidate Cell Rect
            if (row != null)
                row.grid.InvalidateCell(this, forceUpdate);
        }

        private CustomBinder FindBinder(string controlPropertyName, object dataSource, string dataSourcePropertyName)
        {
            int count = binderList.Count;
            for (int i = 0; i < count; i++)
            {
                CustomBinder binder = binderList[i];
                if (binder.HasAttributes(controlPropertyName, dataSource, dataSourcePropertyName))
                    return binder;
            }
            return null;
        }

        public void Bind(string controlPropertyName, object dataSource, string dataSourcePropertyName)
        {
            CustomBinder binder = new CustomBinder(this, controlPropertyName, dataSource, dataSourcePropertyName);
            binderList.Add(binder);
        }

        public void Unbind(string controlPropertyName, object dataSource, string dataSourcePropertyName)
        {
            CustomBinder binder = FindBinder(controlPropertyName, dataSource, dataSourcePropertyName);
            if (binder != null)
                binderList.Remove(binder);
        }

        public void UnbindAll()
        {                       
            binderList.Clear();
        }

        public void AddOnPressButtonHandler(object objectO, string methodType)
        {
            //Type A = Type.GetType(classType);      

            CellStringProperty.EventDescriptor newEventDescriptor = new CellStringProperty.EventDescriptor();

            newEventDescriptor.EventType = CellStringProperty.EventType.OnPressButton;
            newEventDescriptor.Parent = objectO;
            newEventDescriptor.MethodInfo = objectO.GetType().GetMethod(methodType);

            if (ApplSetting.IsDebug && newEventDescriptor.MethodInfo == null)
            {
                throw new Exception(methodType + " is a bad method name");
            }
            eventList.Add(newEventDescriptor);
        }

        public void AddOnBeforeChangeHandler(object objectO, string methodType)
        {
            //Type A = Type.GetType(classType);      

            CellStringProperty.EventDescriptor newEventDescriptor = new CellStringProperty.EventDescriptor();

            newEventDescriptor.EventType = CellStringProperty.EventType.OnBeforeChange;
            newEventDescriptor.Parent = objectO;
            newEventDescriptor.MethodInfo = objectO.GetType().GetMethod(methodType);

            if (ApplSetting.IsDebug && newEventDescriptor.MethodInfo == null)
            {
                throw new Exception(methodType + " is a bad method name");
            }
            eventList.Add(newEventDescriptor);
        }

        public void AddOnAfterChangeHandler(object objectO, string methodType)
        {
            //Type A = Type.GetType(classType);      

            CellStringProperty.EventDescriptor newEventDescriptor = new CellStringProperty.EventDescriptor();

            newEventDescriptor.EventType = CellStringProperty.EventType.OnAfterChange;
            newEventDescriptor.Parent = objectO;
            newEventDescriptor.MethodInfo = objectO.GetType().GetMethod(methodType);

            if (ApplSetting.IsDebug && newEventDescriptor.MethodInfo == null)
            {
                throw new Exception(methodType + " is a bad method name");
            }
            eventList.Add(newEventDescriptor);
        }

        public void CallOnPressButtonEventHandlers()
        {
            object[] parameters = new object[] { this };

            for (int i = 0; i < eventList.Count; i++)
            {
                try
                {
                    if (eventList[i].EventType == CellStringProperty.EventType.OnPressButton)
                        eventList[i].MethodInfo.Invoke(eventList[i].Parent, parameters);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void CallOnBeforeChangeEventHandlers(ref string newValue)
        {
            object[] parameters = new[] { this, newValue.Clone() };

            for (int i = 0; i < eventList.Count; i++)
            {
                if (eventList[i].EventType == CellStringProperty.EventType.OnBeforeChange)
                {
                    eventList[i].MethodInfo.Invoke(eventList[i].Parent, parameters);
                    newValue = parameters[1].ToString();
                }
            }
        }

        public void CallOnAfterChangeEventHandlers()
        {
            object[] parameters = new object[] { this };

            for (int i = 0; i < eventList.Count; i++)
            {
                if (eventList[i].EventType == CellStringProperty.EventType.OnAfterChange)
                    eventList[i].MethodInfo.Invoke(eventList[i].Parent, parameters);
            }
        }
    }

    public class CellCollection : CollectionBase
    {
        public Cell this[int index]
        {
            get
            {
                return ((Cell)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(Cell value)
        {
            return (List.Add(value));
        }

        public int IndexOf(Cell value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, Cell value)
        {
            List.Insert(index, value);
        }

        public void Remove(Cell value)
        {
            List.Remove(value);
        }

        public bool Contains(Cell value)
        {
            // If value is not of type Cell, this will return false.
            return (List.Contains(value));
        }

        protected override void OnInsert(int index, Object value)
        {
            // Insert additional code to be run only when inserting values.
        }

        protected override void OnRemove(int index, Object value)
        {
            // Insert additional code to be run only when removing values.
        }

        protected override void OnSet(int index, Object oldValue, Object newValue)
        {
            // Insert additional code to be run only when setting values.
        }

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != Type.GetType("GridCtrl.Cell"))
                throw new ArgumentException("value must be of type Cell.", "value");
        }
    }
}
