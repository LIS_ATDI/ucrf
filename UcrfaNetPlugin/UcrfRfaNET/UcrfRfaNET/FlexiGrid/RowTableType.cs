﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows;

namespace GridCtrl
{
	class RowTableType
	{		
		protected	string  mKey;           //Ключ строки
		protected	string  mValue;         //Строка в 0-м столбце
		protected	Color   mColor;      //Цвет фона в 0-м столбце
		protected	FontCell  mFont;       //Шрифт в 0-м столбце
		protected	List<SubColumn> SubItem;		//Значение столбцов
		protected	List<string> Keys;				//Ключ строки
		protected	int Count;							//Кол-во столбцов      
		protected	List<ItemProp> ItemProps;	   //Указатель на стиль столбца
		protected	List<Color> CellColor;        //Цвет ячейки
		protected	List<FontCell> Font;          //Шрифт ячейки
		protected	List<int> TabOrder;				//Номер перехода клавишей Tab
		protected	List<bool> ReadOnly;				//Флаг "Только чтение"

		public ItemProp GetItemProps(int ACol) //Вернуть указатель на стиль ячейки
		{
			if((ACol > 0)&&(ACol < Count))
			{
				SubColumn ret = SubItem[ACol-1];
				return ret.pItemProp;
			}
			return null;  //Нулевой столбец не имеет стиля ячейки
		}
		
		public int GetCount()               //Возвращает кол-во столбцов в строке
		{
			return SubItem.Count + 1;
		}

		public void SetCount(int _NewCount)     //Устанавливает кол-во столбцов в строке
		{
			if(_NewCount < 1)
				_NewCount = 1;  //Количество столбцов меньше не может быть
			_NewCount--;
			if(SubItem != null)
				SubItem.Clear();
			SubItem = new List<SubColumn>(_NewCount);
		}

		public string GetmKey(int _ACol)    //Вернуть ключ
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{// Возвращаем ключ ячейки подстолбца
				SubColumn Tmp = SubItem[_ACol-1];
				return Tmp.KeySubCol;
			}
			return mKey; //Возвращаем ключ 0-го столбца
		}

		public void SetmKey(int _ACol, string _Key)//Установить ключ
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{// Устанавливаем ключ ячейки подстолбца
				SubColumn Tmp = SubItem[_ACol-1];
				Tmp.KeySubCol = _Key;
			}
			else mKey = _Key; //Устанавливаем ключ 0-го столбца
		}
		

		public string GetValues(int _ACol)  //Вернуть значение столбца
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{// Возвращаем значение подстолбца
				SubColumn ret = SubItem[_ACol-1];
				return ret.ValueSubCol;
			}
			return mValue;
		}

		public void SetValues(int _ACol, string _NewStr) //Установить новое значение столбца
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{// Устанавливаем значение подстолбца
				SubColumn Tmp = SubItem[_ACol-1];
				Tmp.ValueSubCol = _NewStr;
			}
			else mValue = _NewStr; //Устанавливаем значение 0-го столбца
		}

		public Color GetColor(int _ACol)       //Вернуть значение цвета
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				return Tmp.BGColor;
			}
			else return mColor;
		}

		public void SetColor(int _ACol, Color _newColor)	//Установить значение цвета
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				Tmp.BGColor = _newColor;
			}
			else mColor = _newColor;
		}

		public FontCell GetFont(int _ACol)    //Вернуть шрифт
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				return Tmp.CFont;
			}
			else return mFont;
		}

		public void SetTabOrder(int _ACol, int _newTabOrder)	//Установить значение TabOrder
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				Tmp.TabOrder = _newTabOrder;
			}
		}

		public int GetTabOrder(int _ACol)       //Вернуть TabOrder
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				return Tmp.TabOrder;
			}
			else return -1;  //0-й столбец не имеет TabOrder
		}

		public void SetReadOnly(int _ACol, bool _newReadOnly)	//Установить значение ReadOnly
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				Tmp.ReadOnly = _newReadOnly;
			}
		}

		public bool GetReadOnly(int _ACol)      //Вернуть ReadOnly
		{
			if((_ACol<0)||(_ACol>Count))
				throw new Exception("Error number column");
			if (_ACol > 0)
			{
				SubColumn Tmp = SubItem[_ACol-1];
				return Tmp.ReadOnly;
			}
			else return true;  //0-й столбец всегда true
		}
		
			// Конструкторы
		public	RowTableType()
		{
			mKey = "";                        //Ключ строки
			mValue = "";                      //Строка в 0-м столбце
			mColor = Color.White;
			mFont = new FontCell();
			mFont.TColor = System.Drawing.SystemColors.WindowText;
			mFont.Name = "MS Sans Serif";
			mFont.FSize = 8;
			Keys = new List<string>();
			ItemProps = new List<ItemProp>();    
			CellColor = new List<Color>();
			Font = new List<FontCell>();           
			TabOrder = new List<int>();
			ReadOnly = new List<bool>();
			SubItem = new List<SubColumn>();
		}

		public	void SetDefData()
		{
			mKey = "";                        //Ключ строки
			mValue = "";                      //Строка в 0-м столбце
			mColor = Color.White;
			if(mFont == null)
				mFont = new FontCell();
			mFont.TColor = System.Drawing.SystemColors.WindowText; ;
			mFont.Name = "MS Sans Serif";
			mFont.FSize = 8;
		}

		public	RowTableType(string _Value)
		{
			SetDefData();
			mValue = _Value;
		}
		public	RowTableType(string _Value, string _Key)
		{
			SetDefData();
			mValue = _Value;
			mKey = _Key;
		}

			// Работа с подстолбцами
		public	void AddSubColumn()                  //Добавляет пустой подстолбец
		{
			Count++;
			SubItem.Add(new SubColumn());
		}

		public	void AddSubColumn(string _StrSubCol) //Добавляет подстолбец с строкой
		{
			Count++;
			SubColumn sc = new SubColumn();
			sc.ValueSubCol = _StrSubCol;
			SubItem.Add(sc);
		}

	/*	public	void AddSubColumn(List<string> ListSubColString) //Добавляет подстолбецs сo строками
		{
			for(int i=0;	i<ListSubColString.Count; i++)
				AddSubColumn(ListSubColString[i]);
		}*/

		public	void DelSubColumn(int IndexSubCol)       //Удалить подстолбец с индексом
		{
			if(Count > 1)
				Count--;
			SubItem.RemoveAt(IndexSubCol - 1);
		}

			// Вспомагательные функции
		/*public	void GetListSubColStrings(List<string> ListSubString) //Заполняет список строками столбцов
		{
			for(int i=1; i<Count; i++)
				ListSubString.Add(Values[i]);
		}*/

		/*public	void GetListColStrings(List<string> ListSubString)       //Заполняет список строками столбцов
		{
			for(int i=0; i<Count; i++)
				ListSubString.Add(Values[i]);
		}*/

	}
}

