﻿using System;
using GridCtrl;

namespace XICSM.UcrfRfaNET.FlexiGrid
{
    [Serializable]
    internal class IllegalCellIndexException : Exception
    {
        public IllegalCellIndexException(string cellKey)
            : base(string.Format("Cell '{0}' does not contain index", cellKey))
        {
        }
    }
}
