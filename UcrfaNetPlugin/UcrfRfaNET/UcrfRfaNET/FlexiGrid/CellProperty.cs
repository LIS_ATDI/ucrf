﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;
using XICSM.UcrfRfaNET;
using Lis.CommonLib.Extensions;

namespace GridCtrl
{
    /// <summary>
    /// Cell для отображения списка дробных чисел
    /// </summary>
    public class CellDoubleListProperty : CellStringProperty
    {
        /// <summary>
        /// символ разделитель
        /// </summary>
        public char Separator { get; set; }
        /// <summary>
        /// символ после точки
        /// </summary>
        public char Round { get; set; }

        private double[] _doubleArray = new double[0];
        /// <summary>
        /// Список дробных чисел
        /// </summary>
        public double[] DoubleArray
        {
            get { return _doubleArray; }
            set
            {
                if (_doubleArray != value)
                    Value = ListToString(value);
            }
        }
        /// <summary>
        /// Преобразовывает список чисел в строку
        /// </summary>
        /// <param name="lst">список чисел</param>
        /// <returns>строка</returns>
        private string ListToString(double[] lst)
        {
            string retVal = "";
            if (lst == null)
                return retVal;
            foreach (double d in lst)
            {
                if (string.IsNullOrEmpty(retVal) == false)
                    retVal += Separator + " ";
                retVal += d.Round(Round);
            }
            return retVal;
        }
        /// <summary>
        /// Преобразовывает строку в список чисел
        /// </summary>
        /// <param name="str">список чисел</param>
        /// <returns>массив чисел</returns>
        private double[] StringToList(string str, out bool isError)
        {
            isError = false;
            List<double> retVal = new List<double>();
            string[] parseList = str.Split(new Char[] { Separator, ' ' });
            foreach (string dblVal in parseList)
            {
                double tmpVal = dblVal.ToDouble(IM.NullD);
                if (tmpVal != IM.NullD)
                    retVal.Add(tmpVal);
                else
                    isError = true;
            }
            return retVal.ToArray();
        }

        protected override void setString(string val)
        {
            bool isError;
            base.setString(val);
            _doubleArray = StringToList(val, out isError);
            IsValid = !isError;
        }
    }


    public class CellDateProperty : CellStringProperty
    {
        protected DateTime internalValue = IM.NullT;
        protected DateTime oldInternalValue = IM.NullT;
        public DateTime FormatString;

        public DateTime DateValue
        {
            get
            {
                return internalValue;
            }
            set
            {
                SetDate(value);
            }
        }

        public DateTime DateValue2
        {
            get
            {
                return oldInternalValue;
            }
            set
            {
                SetOldDate(value);
            }
        }

        protected virtual void SetDate(DateTime value)
        {
            if (value == IM.NullT)
                Value = "";
            else
                Value = value.ToShortDateString();

            internalValue = value;
        }

        protected virtual void SetOldDate(DateTime value)
        {
            if (value == IM.NullT)
                Value2 = "";
            else
                Value2 = value.ToShortDateString();

            oldInternalValue = value;
        }

        protected override void setString(string val)
        {
            strValue = val;
            internalValue = val.ToDataTime();
            IsValid = !((internalValue == IM.NullT) && (string.IsNullOrEmpty(val) == false));
        }

        protected override void setString2(string val)
        {
            oldStrValue = val;
            oldInternalValue = val.ToDataTime();
        }
    }

    public class CellDateNullableProperty : CellDateProperty
    {
        public string NullString { get; set; }

        public CellDateNullableProperty()
        {
            NullString = "";
        }

        protected override void SetDate(DateTime value)
        {
            if (value == IM.NullT)
                Value = NullString;
            else
                Value = value.ToShortDateString();

            internalValue = value;
        }

        protected override void SetOldDate(DateTime value)
        {
            if (value == IM.NullT)
                Value2 = NullString;
            else
                Value2 = value.ToShortDateString();

            oldInternalValue = value;
        }

        protected override void setString(string val)
        {
            internalValue = val.ToDataTime("dd.MM.yyyy");
            if (val == NullString)
            {
                strValue = val;
                return;
            }
            IsValid = (internalValue != IM.NullT) || string.IsNullOrEmpty(val) || val == NullString;
            if (IsValid)
                strValue = internalValue.ToShortDateString();
            else
                strValue = val;

        }

        protected override void setString2(string val)
        {
            oldStrValue = val;
            oldInternalValue = val.ToDataTime();

            if (!string.IsNullOrEmpty(val))
                oldStrValue = val;
            else
                oldStrValue = NullString;
        }
    }

    public class CellStringComboboxAplyProperty : CellStringProperty
    {
        protected List<string> items = new List<string>();
        private int pointer;

        public List<string> Items { get { return items; } }

        public override Cell Cell
        {
            get
            {
                return base.Cell;
            }
            set
            {
                base.Cell = value;
                base.Cell.PickList = items;
                NotifyPropertyChanged("Cell");
            }
        }

        public int Pointer
        {
            get { return pointer; }
        }

        protected override void setString(string val)
        {
            base.setString(val);
            int index = Items.IndexOf(val);
            switch (index)
            {
                case 0:
                    BackColor = ColorBgr(0);
                    pointer = 0;
                    break;
                case 1:
                    BackColor = ColorBgr(2);
                    pointer = 1;
                    break;
            }
        }
    }

    public class CellStringLookupEncodedProperty : CellStringProperty
    {
        protected List<string> items = new List<string>();
        //private int pointer = 0;

        public List<string> Items { get { return items; } }

        public override Cell Cell
        {
            get
            {
                return base.Cell;
            }
            set
            {
                base.Cell = value;
                base.Cell.PickList = items;
                NotifyPropertyChanged("Cell");
            }
        }

        protected override void setString(string val)
        {
            if (Cell.cellStyle == EditStyle.esPickList)
            {
                base.setString(val);

                if (val == Cell.KeyPickList.GetKeyByIndex(0))
                    BackColor = ColorBgr(0);
                else
                    BackColor = ColorBgr(2);
            }
            else
            {
                base.setString(val);
                BackColor = DefaultBgr();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cellProperty"></param>
        /// <param name="value1">УРЧП-шное старое свойство</param>
        /// <param name="value2">УРЗП-шное новое свойство с акта ПТК</param>
        public static void InitalizeHelper(CellStringLookupEncodedProperty cellProperty, string value1, string value2)
        {
            if (value1 != value2)
            {
                cellProperty.Cell.cellStyle = EditStyle.esPickList;
                KeyedPickList newKeyedPickList = new KeyedPickList();
                newKeyedPickList.AddToPickList(value1, cellProperty.Cell.KeyPickList.GetValueByKey(value1));
                newKeyedPickList.AddToPickList(value2, cellProperty.Cell.KeyPickList.GetValueByKey(value2));
                cellProperty.Cell.KeyPickList = newKeyedPickList;
                cellProperty.Cell.Value = value1;
            }
            else
            {
                cellProperty.Value = value2;
                cellProperty.BackColor = DefaultBgr();
                cellProperty.Cell.cellStyle = EditStyle.esSimple;
            }
        }

    }

    public class CellStringBrowsableProperty : CellStringProperty
    {
        public enum BehaviorType
        {
            Standard,
            Certificate
        }

        protected List<string> items = new List<string>();
        private int pointer = 0;

        public List<string> Items { get { return items; } }

        private BehaviorType _behavior = BehaviorType.Standard;
        public BehaviorType Behavior
        {
            get
            {
                return _behavior;
            }
            set
            {
                if (_behavior != value)
                {
                    _behavior = value;
                }
            }
        }

        //= BehaviorType.Standard;
        public override Cell Cell
        {
            get
            {
                return base.Cell;
            }
            set
            {
                base.Cell = value;
                base.Cell.PickList = items;
                NotifyPropertyChanged("Cell");
            }
        }

        public int Pointer
        {
            get { return pointer; }
        }

        protected override void setString(string val)
        {
            if (Behavior == BehaviorType.Standard && items.Count > 0)
            {
                base.setString(items[0]);
                if (items.Count > 1)
                    if (items[0] == items[1])
                        BackColor = ColorBgr(2);
                    else
                        BackColor = ColorBgr(0);
                else
                    BackColor = ColorBgr(2);

            }
            else
            {
                base.setString(val);

                switch (Behavior)
                {
                    case BehaviorType.Standard:
                        BackColor = ColorBgr(2);
                        break;
                    case BehaviorType.Certificate:
                        BackColor = DefaultBgr();
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cellProperty"></param>
        /// <param name="value1">УРЧП-шное старое свойство</param>
        /// <param name="value2">УРЗП-шное новое свойство с акта ПТК</param>
        public static void InitalizeHelper(CellStringBrowsableProperty cellProperty, string value1, string value2)
        {
            if (cellProperty.Behavior == BehaviorType.Standard && value1 != value2)
            {
                cellProperty.Items.Clear();
                cellProperty.Value = value1;
                cellProperty.Items.Add(value1);
                if (!string.IsNullOrEmpty(value2))
                    cellProperty.Items.Add(value2);
                if (cellProperty.Items.Count > 1)
                    cellProperty.BackColor = ColorBgr(0);
                else if (cellProperty.Items.Count == 1)
                    cellProperty.BackColor = ColorBgr(2);
                if (cellProperty.Items.Count > 1)
                    cellProperty.Cell.cellStyle = EditStyle.esPickList;
                else
                    cellProperty.Cell.cellStyle = EditStyle.esSimple;
            }
            else
            {
                cellProperty.Items.Clear();

                switch (cellProperty.Behavior)
                {
                    case BehaviorType.Standard:
                        cellProperty.Value = value2;
                        cellProperty.BackColor = ColorBgr(2);
                        break;
                    case BehaviorType.Certificate:
                        cellProperty.Value = value1;
                        cellProperty.BackColor = DefaultBgr();
                        break;
                }
                cellProperty.Cell.cellStyle = EditStyle.esSimple;
            }
        }
    }



    public class CellDoubleLookupProperty : CellDoubleNullableProperty
    {

        protected List<double> items = new List<double>();
        private int pointer;

        public List<double> Items { get { return items; } }

        public override Cell Cell
        {
            get
            {
                return base.Cell;
            }
            set
            {
                base.Cell = value;
                NotifyPropertyChanged("Cell");
            }
        }

        public int Pointer
        {
            get { return pointer; }
        }

        private bool _red = false;
        /// <summary>
        /// Если это свойство установлено, колмбобокс будет всегда красный
        /// Нужно для сигнализации, что мощность слишком большая, и т.д.
        /// </summary>
        public bool Red
        {
            get
            {
                return _red;
            }
            set
            {
                if (value != _red)
                {
                    _red = value;
                    Paint();
                }
            }
        }

        protected override void setString(string val)
        {
            base.setString(val);

            Paint();
        }

        private void Paint()
        {
            if (Red)
                BackColor = ColorBgr(0);
            else
            {
                if (Cell.cellStyle == EditStyle.esPickList)
                {
                    switch (Value)
                    {
                        case "1":
                            BackColor = ColorBgr(0);
                            pointer = 0;
                            break;
                        case "2":
                            BackColor = ColorBgr(2);
                            pointer = 1;
                            break;
                    }
                }
                else
                {
                    BackColor = DefaultBgr();
                }
            }
        }

        public void Initialize()
        {
            KeyedPickList keyList = new KeyedPickList();
            int i = 1;
            if (items[0] != items[1])
            {
                //Cell.KeyPickList = null;
                //Value = "";

                foreach (double d in items)
                {
                    keyList.AddToPickList(i.ToString(), DoubleToString(d));
                    i++;
                }
                Cell.cellStyle = EditStyle.esPickList;
                //BackColor = ColorBgr(0)
                Value = "1";
                Cell.KeyPickList = keyList;
            }
            else
            {
                Cell.KeyPickList = null;
                Cell.cellStyle = EditStyle.esSimple;
                //BackColor = ColorBgr(4);
                DoubleValue = items[0];
            }
        }

        public void Initialize(double topValue, double bottomValue)
        {
            Items.Clear();
            Items.Add(topValue);
            Items.Add(bottomValue);
            Initialize();
        }

        public double LookupedDoubleValue
        {
            get
            {
                if (Cell.KeyPickList == null)
                    return DoubleValue;
                else
                {
                    int index = Convert.ToInt32(Value);
                    return items[index - 1];
                }
            }
        }
    }



    public class CellStringProperty : INotifyPropertyChanged
    {
        internal enum EventType
        {
            OnPressButton = 10001,
            OnBeforeChange = 21112,
            OnAfterChange = 32223
        };

        internal class EventDescriptor
        {
            public MethodInfo MethodInfo;
            public object Parent;
            public EventType EventType;
        };

        public enum ColorIndex
        {
            Mismatch = 0,
            Match = 2,
            BadColor = 3,
            Default = 4
        };
        /// <summary>
        /// Реализует цвет фона
        /// </summary>
        /// <param name="colorType">тип цвета</param>
        /// <returns>цвет</returns>
        public static Color ColorBgr(ColorIndex colorType)
        {
            return ColorBgr((int) colorType);
        }
        /// <summary>
        /// Реализует цвет фона
        /// </summary>
        /// <param name="colorType">тип цвета: 0..1..2</param>
        /// <returns>цвет</returns>        
        public static Color ColorBgr(int colorType)
        {
            switch (colorType)
            {
                case 0:
                    return Color.LightPink;
                case 1:
                    return Color.LightYellow;
                case 2:
                    return Color.LightGreen;
                case 3: //BadColor
                    return Color.Red;
                case 4: //OkColor
                    return Color.White;
            }
            return Color.Empty;
        }

        public static Color ColorText(int colorType)
        {
            switch (colorType)
            {
                case 0:
                    return Color.Red;
                case 1:
                    return Color.Yellow;
                case 2:
                    return Color.Green;
                case 3: //BadColor
                    return Color.Red;
                case 4: //OkColor
                    return Color.Black;
            }
            return Color.Empty;
        }

        public static Color DefaultBgr()
        {
            return Color.White;
        }

        protected List<CustomBinder> binderList = new List<CustomBinder>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        protected string strValue = "";
        protected string oldStrValue = "";
        protected Color textColor = Color.Black;
        protected Color textColor2 = Color.Red;
        protected Color backColor = Color.White;
        protected bool canEdit = true;
        protected Cell cell;

        public string Value
        {
            get
            {
                return getString();
            }
            set
            {
                InternalSetString(value);
            }
        }

        protected virtual void InternalSetString(string value)
        {
            if (strValue != value)
            {
                setString(value);
                NotifyPropertyChanged("Value");
            }
        }

        public string Value2
        {
            get
            {
                return getString2();
            }
            set
            {
                InternalSetOldString(value);
            }
        }

        protected virtual void InternalSetOldString(string value)
        {
            if (oldStrValue != value)
            {
                setString2(value);
                NotifyPropertyChanged("Value2");
            }
        }

        public Color TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                if (textColor != value)
                {
                    textColor = value;
                    NotifyPropertyChanged("TextColor");
                }
            }
        }

        public Color TextColor2
        {
            get
            {
                return textColor2;
            }
            set
            {
                if (textColor2 != value)
                {
                    textColor2 = value;
                    NotifyPropertyChanged("TextColor2");
                }
            }
        }

        public Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                if (backColor != value)
                {
                    backColor = value;
                    _tmpBackColor = backColor;
                    NotifyPropertyChanged("BackColor");
                }
            }
        }

        public bool CanEdit
        {
            get
            {
                return canEdit;
            }
            set
            {
                if (canEdit != value)
                {
                    canEdit = value;
                    NotifyPropertyChanged("CanEdit");
                }
            }
        }

        public virtual Cell Cell
        {
            get { return cell; }
            set
            {
                cell = value;
                NotifyPropertyChanged("Cell");
            }
        }

        private Color _tmpBackColor;
        private bool _isValid = true;
        public bool IsValid
        {
            get { return _isValid; }
            protected set
            {
                if (_isValid != value)
                {
                    _isValid = value;
                    if (_isValid)
                    {
                        BackColor = _tmpBackColor;
                    }
                    else
                    {
                        Color tmpColor = BackColor;
                        BackColor = ColorBgr(0);
                        _tmpBackColor = tmpColor;
                    }
                    if (cell != null)
                        cell.IsValid = IsValid;
                }
            }
        }

        protected virtual string getString()
        {
            return strValue;
        }

        protected virtual void setString(string val)
        {
            strValue = val;
        }

        protected virtual string getString2()
        {
            return oldStrValue;
        }

        protected virtual void setString2(string val)
        {
            oldStrValue = val;
        }

        /// <summary>
        /// Установить другое поведение
        /// Для УРЗП важно, чтобы введенные данные были красными, 
        /// но для УРЧП они должны быть черными.
        /// 
        /// Устанавливает Value2 не в null, не в пустую, и в такую строку, которая "устойчивая к Trim".        
        /// </summary>
        public void SetRedValue()
        {
            if (string.IsNullOrEmpty(oldStrValue))
                Value2 = "\t";
        }
    };

    /*public class CellStringNullableProperty : CellStringProperty
    {
        public string NullString { get; set; }

        protected override void InternalSetString(string value)
        {
            if (strValue != value || string.IsNullOrEmpty(strValue))
            {
                setString(value);
                NotifyPropertyChanged("Value");
            }
        }

        protected override void setString(string val)
        {            
            if (string.IsNullOrEmpty(val))
                strValue = NullString;
            else
                strValue = val;
        }

        protected override void setString2(string val)
        {            
            if (string.IsNullOrEmpty(val))
                oldStrValue = NullString;
            else
                oldStrValue = val;
        }                        
    };*/

    public class CellIntegerProperty : CellStringProperty
    {
        protected int internalValue = IM.NullI;
        protected int oldInternalValue = IM.NullI;

        public int IntValue
        {
            get
            {
                return internalValue;
            }
            set
            {
                Value = value.ToString();
            }
        }

        public int IntValue2
        {
            get
            {
                return oldInternalValue;
            }
            set
            {
                Value2 = value.ToString();
            }
        }

        protected override void setString(string val)
        {
            base.setString(val);
            internalValue = val.ToInt32(IM.NullI);
            IsValid = string.IsNullOrEmpty(strValue) || internalValue != IM.NullI;
        }
    }

    public class CellDoubleProperty : CellStringProperty
    {
        protected double internalValue = IM.NullD;
        protected double oldInternalValue = IM.NullD;
        public string FormatString;

        public CellDoubleProperty()
        {
            FormatString = "N4";
        }

        public double DoubleValue
        {
            get
            {
                return internalValue;
            }
            set
            {
                if (internalValue != value) SetDouble(value);
            }
        }

        protected virtual string DoubleToString(double value)
        {
            int dec = 1;

            if (FormatString[0] == 'N')
            {
                int.TryParse(FormatString.Substring(1), out dec);
            }

            return IM.RoundDeci(value, dec).ToStringNullD();
        }

        public double DoubleValue2
        {
            get
            {
                return oldInternalValue;
            }
            set
            {
                if (oldInternalValue != value) SetOldDouble(value);
            }
        }

        protected virtual void SetDouble(double value)
        {
            if (internalValue != value)
            {
                Value = DoubleToString(value);
                internalValue = value;
            }
        }

        protected virtual void SetOldDouble(double value)
        {
            if (oldInternalValue != value)
            {
                Value2 = DoubleToString(value);
                oldInternalValue = value;
            }
        }

        protected override void setString(string val)
        {
            base.setString(val);
            internalValue = val.ToDouble(IM.NullD);
            IsValid = string.IsNullOrEmpty(strValue) || internalValue != IM.NullD;
        }

        protected override void setString2(string val)
        {
            oldStrValue = val;
            oldInternalValue = val.ToDouble(IM.NullD);
        }
    }

    public class CellDoubleBoundedProperty : CellDoubleProperty
    {
        private double _lowerBound = Double.MinValue;
        private double _upperBound = Double.MaxValue;

        public CellDoubleBoundedProperty()
            : base()
        {
        }

        public double LowerBound
        {
            get
            {
                return _lowerBound;
            }
            set
            {
                if (_lowerBound != value)
                {
                    _lowerBound = value;
                    Validate();
                }
            }
        }

        public double UpperBound
        {
            get
            {
                return _upperBound;
            }
            set
            {
                if (_upperBound != value)
                {
                    _upperBound = value;
                    Validate();
                }
            }
        }

        private void Validate()
        {
            IsValid = string.IsNullOrEmpty(strValue) ||
                (internalValue != IM.NullD) || 
                (_lowerBound <= internalValue &&
                _upperBound >= internalValue);
        }

        protected override void SetDouble(double value)
        {
            Value = DoubleToString(value);
            internalValue = value;
            Validate();
        }

        protected override void setString(string val)
        {
            base.setString(val);
            internalValue = val.ToDouble(IM.NullD);
            Validate();
        }
    }

    public class CellDoubleNullableProperty : CellDoubleProperty
    {
        public string NullString { get; set; }

        public CellDoubleNullableProperty()
        {
            NullString = "";
        }

        protected override string DoubleToString(double value)
        {
            if (value != IM.NullD)
                return value.ToStringNullD();
            else
                return NullString;
        }

        protected override void setString(string val)
        {
            base.setString(val);
            internalValue = val.ToDouble(IM.NullD);
            IsValid = string.IsNullOrEmpty(strValue) || internalValue != IM.NullD || val == NullString;
        }
    }

    public class CellDoubleBoundedNullableProperty : CellDoubleBoundedProperty
    {        
        public string NullString { get; set; }
        // Список подмен
        public Dictionary<double, string> Substitution;
        // Зазрешить запретить подмены
        public bool EnableSubstitution { get; set; }

        public CellDoubleBoundedNullableProperty()
        {
            NullString = "";
            Substitution = new Dictionary<double, string>();
            EnableSubstitution = false;
        }

        protected override string DoubleToString(double value)
        {
            if (value != IM.NullD)
            {
                if (EnableSubstitution)
                {
                    if (Substitution.ContainsKey(value))
                        return Substitution[value];                    
                }
                return value.ToStringNullD();
            }
            else
                return NullString;
        }

        protected override void setString(string val)
        {
            base.setString(val);
            internalValue = val.ToDouble(IM.NullD);
            IsValid = string.IsNullOrEmpty(strValue) || internalValue != IM.NullD || val == NullString;
        }

        protected override void setString2(string val)
        {
            base.setString2(val);
            oldInternalValue = val.ToDouble(IM.NullD);
            //IsValid = string.IsNullOrEmpty(strValue) || internalValue != IM.NullD || val == NullString;
        }
    }

    public class CellLongitudeProperty : CellDoubleProperty
    {
        public CellLongitudeProperty()
        {
            FormatString = "N3";
        }

        protected override string getString()
        {
            return HelpFunction.DmsToString(internalValue, EnumCoordLine.Lon);
        }

        protected override string getString2()
        {
            return HelpFunction.DmsToString(oldInternalValue, EnumCoordLine.Lon);
        }

        protected override void setString(string val)
        {
            double tmpInternalValue;
            if (string.IsNullOrEmpty(val))
                tmpInternalValue = oldInternalValue;
            else
                tmpInternalValue = ConvertType.StrToDMS(val);
            if (tmpInternalValue != IM.NullD)
            {
                SetDouble(tmpInternalValue);
            }
        }

        protected override void setString2(string val)
        {
            double tmpInternalValue = ConvertType.StrToDMS(val);
            if (tmpInternalValue != IM.NullD)
            {
                SetOldDouble(tmpInternalValue);
            }
        }

        protected override void InternalSetString(string value)
        {
            if (strValue != value || string.IsNullOrEmpty(value))
            {
                setString(value);
                NotifyPropertyChanged("Value");
            }
        }

        protected override void SetDouble(double value)
        {
            if (internalValue != value)
            {
                internalValue = value;

                Value = HelpFunction.DmsToString(internalValue, EnumCoordLine.Lon);
                IsValid = HelpFunction.ValidateCoords(internalValue, EnumCoordLine.Lon);
            }
        }

        protected override void SetOldDouble(double value)
        {
            if (oldInternalValue != value)
            {
                oldInternalValue = value;

                Value2 = HelpFunction.DmsToString(oldInternalValue, EnumCoordLine.Lon);
            }
        }
    }

    public class CellLatitudeProperty : CellDoubleProperty
    {
        public CellLatitudeProperty()
        {
            FormatString = "N3";
        }

        protected override string getString()
        {
            return HelpFunction.DmsToString(internalValue, EnumCoordLine.Lat);
        }

        protected override string getString2()
        {
            return HelpFunction.DmsToString(oldInternalValue, EnumCoordLine.Lat);
        }

        protected override void setString(string val)
        {
            double tmpInternalValue;
            if (!string.IsNullOrEmpty(val))
                tmpInternalValue = ConvertType.StrToDMS(val);
            else
                tmpInternalValue = oldInternalValue;

            if (tmpInternalValue != IM.NullD)
            {
                SetDouble(tmpInternalValue);
            }
        }

        protected override void setString2(string val)
        {
            double tmpInternalValue = ConvertType.StrToDMS(val);
            if (tmpInternalValue != IM.NullD)
            {
                SetOldDouble(tmpInternalValue);
            }
        }

        protected override void InternalSetString(string value)
        {
            if (strValue != value || string.IsNullOrEmpty(value))
            {
                setString(value);
                NotifyPropertyChanged("Value");
            }
        }

        protected override void SetDouble(double value)
        {
            if (internalValue != value)
            {
                internalValue = value;

                Value = HelpFunction.DmsToString(internalValue, EnumCoordLine.Lat);
                IsValid = HelpFunction.ValidateCoords(internalValue, EnumCoordLine.Lat);
            }
        }

        protected override void SetOldDouble(double value)
        {
            if (oldInternalValue != value)
            {
                oldInternalValue = value;

                Value2 = HelpFunction.DmsToString(oldInternalValue, EnumCoordLine.Lat);
            }
        }
    }
}
