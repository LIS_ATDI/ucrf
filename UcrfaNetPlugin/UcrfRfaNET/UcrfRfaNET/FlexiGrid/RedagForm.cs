﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.ComponentModel;
using System.Drawing;

namespace GridCtrl
{
	public delegate void RedagFormEventHandler(object Sender, KeyEventArgs e);
	
	class RedagForm : Panel
	{	
		public RedagForm(Component Owner): base()  //Конструктор
		{}

		public void SetValueListNData(GridCtrl.Grid _pValueListNData) //Установить указатель на TValueListNData
		{
			pValueListNData = _pValueListNData;
		}
		public void ChangeRedagForm(Rectangle rect) //Изменить позицию и стиль формы редактирования
		{
					bool blRepaint = false;
					if ((Left == (int)rect.Left) && (Top == (int)rect.Top) &&
						(Width == (int)rect.Width) && (Width == (int)rect.Width))
						blRepaint = true; // Прорисуем компоненты, так как они автоматически не прорисуются
					//Изменяем размер окна
					Left = (int)rect.Left;
					Top = (int)rect.Top;
					Width = (int)rect.Width;
					Height = (int)rect.Height;
					// Скрываем все компоненты
					if(pStyleForm != null && (pStyleForm.edStyle == EditStyle.esEllipsis))
					{//С кнопочкой
						pMemo.Height = Height;
						// Определяем ширину кнопки
						string StrButtom = pStyleForm.ButtonText;
						int WidthButtom = 50;// Canvas.WTextWidth(StrButtom) + 15;
						if(WidthButtom > Width/3)
						{// Ширина очень большая
							List<string> pStrList = new List<string>();
							WidthButtom = Width/3;
							//userControl.FlexibleGrid.BreakString(StrButtom, WidthButtom > 15 ? (WidthButtom-15):1, Canvas, pStrList);
							StrButtom = "";
							for(int i=0; i<pStrList.Count; i++)
							{
								if(StrButtom != "")
									StrButtom += "\n";
								StrButtom += pStrList[i];
							}
						}
						pMemo.Width = Width - WidthButtom;
						pBitBtn.Left = pMemo.Width;
						pBitBtn.Height = Height;
						pBitBtn.Width = Width - pMemo.Width - 1;
						pBitBtn.Text = StrButtom;
						// Отображаем нужную компоненту
						pMemo.Visible = true;
						if(Visible)pMemo.Focus();
						pBitBtn.Visible = true;
						pComboBox.Visible = false;
						if(blRepaint)
						{//Пренудительная прорисовка компонентов
							pMemo.Refresh();
							pBitBtn.Refresh();
						}
					}
					else if(pStyleForm != null && (pStyleForm.edStyle == EditStyle.esPickList) && (pStyleForm.PickList != null) && !mmReadOnly)
					{//С ComboBox
						pComboBox.Top = (Height - pComboBox.Height) / 2;
						pComboBox.Width = Width;
						// Отображаем нужную компоненту
						pComboBox.Visible = true;
						if(Visible)pComboBox.Focus();
						pBitBtn.Visible = false;
						pMemo.Visible = false;
						if(blRepaint)
						{//Пренудительная прорисовка компонентов
							pComboBox.Refresh();
						}
					}
					else
					{// Стиль по умолчанию
						pMemo.Height = Height;
						pMemo.Width = Width;
						// Отображаем нужную компоненту
						pMemo.Visible = true;
						if(Visible)pMemo.Focus();
						pComboBox.Visible = false;
						pBitBtn.Visible = false;
						if(blRepaint)
						{//Пренудительная прорисовка компонентов
							pMemo.Refresh();
						}
					}
		}
		public bool GetValue(out string StrOut)         //Возвращает редактированое значение
		{
			StrOut = "";
			if (pStyleForm != null)
			{
				if (pStyleForm.edStyle == EditStyle.esPickList)
				{//ComboBox
					StrOut = pComboBox.Text;
					return true;
				}
				else
				{
					StrOut = pMemo.Text;
					return true;  //Есть значение
				}
			}
			return false; //Нет активных компонентов
		}
		public void SetValue(string _Value, ItemProp _pTypeCol, bool _ReadOnly) //Устанавливает редактируемое значение и стиль ячейки
		{
			pStyleForm = _pTypeCol;
			mmReadOnly = _ReadOnly;
			pMemo.Clear();
			pMemo.Text = _Value;
			pMemo.SelectAll();
			pMemo.ReadOnly = mmReadOnly;
			pComboBox.Items.Clear();
			if ((pStyleForm != null) && (pStyleForm.PickList != null))
			{// можно использовать ComboBox
				pComboBox.Items.AddRange(pStyleForm.PickList.ToArray());
				pComboBox.Text = _Value;
			}
		}
			 
		private bool mmReadOnly;
		public GridCtrl.Grid pValueListNData;     //Указательна TValueListNData
		private ComboBoxTab pComboBox;            //Выбор из списка
		private TextBox pMemo;                //
		private Button pBitBtn;                  //Кнопка вызова доп. окна
		private ItemProp pStyleForm;             //Указатель на класс стиля формы		

		// Собитие нажатия клавиши
		public event RedagFormEventHandler RedagFormKeyDown;
		public void OnRedagFormKeyDown(Object Sender, KeyEventArgs e)
		{
			
			if ((Sender == pMemo) && (e.KeyCode == Keys.Return))
			{//Нажили клавишу "Enter"
				KeyEventArgs eN = new KeyEventArgs(Keys.D0);
				if (pBitBtn.Visible) //Если есть кнопка, то нажмем ее
					this.RedagFormButtomClick(pBitBtn, eN);
			}
			if ((Sender == pComboBox) && (e.KeyCode ==  Keys.Return))
			{//Изменяем значение ComboBox
				KeyEventArgs eN = new KeyEventArgs(Keys.D0);
				pComboBox.Text = pComboBox.Items[pComboBox.SelectedIndex].ToString();
			}
	//		if ((e.KeyCode != 0) && (e.KeyCode != Keys.Up) && (e.KeyCode != Keys.Down)
	//			&& (pValueListNData != null))
	//			pValueListNData.KeyDownInRedagForm(Sender, eN);
		}
		public event KeyPressEventHandler RedagFormKeyPress;		
		public event EventHandler RedagFormButtomClick;//Нажата кнопка редактирования
		public event EventHandler RichEdit1SelectionChange; //Здесь будем отсекать последний символ перевода каретки
		public virtual void OnRedagFormKeyPress(Object Sender, Keys Key)// Обработка нажатия клавиш
		{
			if ((Sender == pComboBox) && (Key == Keys.Return))
				return; //Выходим, что б ComboBox обработал клавишу
	//		if (pValueListNData != null)
	//			pValueListNData.KeyPressInRedagForm(Sender, Key);
		}
		public virtual void OnRedagFormButtomClick(Object Sender)//Нажата кнопка редактирования
		{
	//		if (pValueListNData != null && pValueListNData.OnEditButtonClick != null)
	//		{
	//			//Обновляем ячейку перед вызовом функции нажатия кнопки
	//			pValueListNData.UpdateCell(pValueListNData->ColPos, pValueListNData->RowPos);
	//			pValueListNData.OnEditButtonClick(Sender);
	//		}
	//		pMemo->SetFocus();
		}
		public virtual void OnRichEdit1SelectionChange(Object Sender) //Здесь будем отсекать последний символ перевода каретки
		{
			if (pMemo.SelectionLength > 0)
			{
				if (pMemo.SelectionLength > pMemo.TextLength)
					pMemo.SelectionLength = pMemo.TextLength;
			}
		}		
	}
}