﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;

namespace GridCtrl 
{
	class ComboBoxTab : ComboBox
	{
		protected override void WndProc(ref Message m)
		{
			if(m.Msg == 0x0100 ) // WM_KEYDOWN
			{
				if(m.WParam.ToInt32() == 0x09)// : //TAB
				{
					m.Msg = 0x0000;
					m.WParam = (IntPtr)0;
				}
				else if(m.WParam.ToInt32() == 0x1B)// : //Esc
				{
					m.Msg = 0x0000;
					m.WParam = (IntPtr)0;
				}
			}
			base.WndProc(ref m);
		}		
	}
}
