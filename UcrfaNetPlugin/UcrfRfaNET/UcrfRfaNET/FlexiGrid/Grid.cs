﻿using System;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;


namespace GridCtrl
{

	/// <summary>
	/// Summary description for Grid.
	/// </summary>
	public partial class Grid : System.Windows.Forms.UserControl
	{
        /// <summary>
        /// Флаг изменения грида
        /// </summary>
        public bool Edited
	    {
            get
            {
                bool retVal = false;
                for (int iRow = 0; iRow < GetRowCount(); iRow++ )
                {
                    if (retVal)
                        break;
                    for(int iCol = 0; iCol < GetColumnCount(iRow); iCol++)
                    {
                        Cell cell = GetCell(iRow, iCol);
                        if ((cell != null) && cell.IsChanged)
                        {
                            retVal = true;
                            break;
                        }
                    }
                }
                return retVal;
            }
            set
            {
                for (int iRow = 0; iRow < GetRowCount(); iRow++)
                {
                    for (int iCol = 0; iCol < GetColumnCount(iRow); iCol++)
                    {
                        Cell cell = GetCell(iRow, iCol);
                        if (cell != null)
                            cell.IsChanged = value;
                    }
                }
            }
	    }
		// Delegates
		public delegate void CellSelectDelegate(Cell cell);
		public delegate void BeginEditDelegate(Cell cell);
		public delegate void EndEditDelegate(Cell cell);
		public delegate void DataValidateDelegate(Cell cell);
		public delegate void CellButtonClickDelegate(Cell cell);

		// Events
		public event CellSelectDelegate OnCellSelect;
		public event DataValidateDelegate OnDataValidate;
		public event CellButtonClickDelegate OnCellButtonClick;

		public enum SelectionModeType
		{
			NoSelect,
			CellSelect,
			RowSelect
		}

		protected enum HitCode
		{
			hitNone,
			hitColumnEdge,
			hitRowEdge,
			hitColumn,
			hitRow,
			hitCell
		}

		public enum ImageStyleType
		{
			Center,
			Tile,
			Stretch,
		}

		/// <summary>
		/// Style of grid lines
		/// </summary>
		public enum GridLineStyle
		{
			None,
			Vertical,
			Horizontal,
			Both,
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		///

		#region ****** Grid Specific Variables ******
		public GridObjectCollection rowList = new GridObjectCollection();
		//public GridObjectCollection colList = new GridObjectCollection();		
		protected Color clrBk = SystemColors.ControlLight;//Color.Black;//
		protected uint defaultRowHeight = 26;
		protected uint defaultColWidth = 36;
		protected int otherColsWidth = 150;
		protected CellSizer sizerCell = new CellSizer();
		protected SelectionModeType selectMode = SelectionModeType.CellSelect;
		protected Image image;
		protected ImageStyleType imageStyle = ImageStyleType.Stretch;
		protected bool lockUpdates;
		protected GridLineStyle gridLines = GridLineStyle.Both;
		protected int Opacity = 255; // Default
		protected HScrollBarEx hScrollBar = new HScrollBarEx();
		protected VScrollBarEx vScrollBar = new VScrollBarEx();
		protected Color gridLineColor = Color.Black;//SystemColors.ActiveBorder;
		protected Cell selectedCell = null;
		protected GridObject selectedObject = null;
		protected int firstColumnWidth = 36;
		protected bool readOnly = false;
		private int HScrollMax = 0;
		private int VScrollMax = 0;
		private const int Tolerence = 2;
		private CellEditor textBox = null;
		private bool isEditMode = true;
		private ToolTip toolTip = new ToolTip();
		private bool toolTips = true;
		private Cell tipCell = null;
		private int mX = 0;
		private int mY = 0;				

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Number of rows and columns
		////////////////////////////////////////////////////////////////////////////////////////////////	
		////////////////////////////////////////////////////////////////////////////////////////////////
		#region Rows_and_columns
		public int FirstColumnWidth
		{
			set
			{
				otherColsWidth = this.Width - value - (vScrollBar.Visible ? vScrollBar.Width : 0);
			/*	foreach (Row r in rowList)
					r.ResizeRow();*/

				firstColumnWidth = value;
				Invalidate();
			}

			get
			{
				return firstColumnWidth;
			}
		}

		public int OtherColumnsWidth
		{
			set
			{
				this.Width = this.firstColumnWidth + value + (vScrollBar.Visible ? vScrollBar.Width : 0);
				/*foreach (Row r in rowList)
					r.ResizeRow();*/
				otherColsWidth = value;
				Invalidate();
			}

			get
			{
				return otherColsWidth;
			}
		}		
		
		/// <summary>
		/// Returns the number of rows (including fixed rows)
		/// </summary>
		/// <returns></returns>
		public int GetRowCount()
		{
			return rowList.Count;
		}

		/// <summary>
		/// Returns the number of columns (including fixed columns)
		/// </summary>
		/// <returns></returns>
		public int GetColumnCount(int row)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");
			
			Row r = (Row)rowList[row];
			return	r.Count;
		}

		/// <summary>
		/// Returns the number of fixed rows
		/// </summary>
		/// <returns></returns>
		public int GetFixedRowCount()
		{
			return GetRowHeaderCount();
		}

		/// <summary>
		/// Sets the number of rows (including fixed rows
		/// </summary>
		/// <param name="rows"></param>
		public void SetRowCount(int rows)
		{
			if (rows < 0)
				return;

			int rowcnt = rowList.Count;

			if (rows == rowList.Count)
				return;

			LockUpdates = true;
			if (rows > rowcnt)
			{
				for (int i = 0; i < rows - rowcnt; i++)
					AddRow();
			}
			else
			{
				for (int i = 0; i < rowcnt - rows; i++)
					DeleteRow(rowcnt - 1);
			}

			LockUpdates = false;
		}

		/// <summary>
		/// Sets the number of columns (including fixed columns)
		/// </summary>
		/// <param name="cols"></param>
		public void SetColumnCount(int row, int cols)
		{
			if (cols < 0)
				return;

			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");
			
			Row r = (Row)rowList[row];

			int colcnt = r.Count;

			if (cols == r.Count)
				return;

			LockUpdates = true;
			if (cols > colcnt)
			{
				for (int i = 0; i < cols - colcnt; i++)
					AddColumn(row, "");
			}
			else
			{
				for (int i = 0; i < colcnt - cols; i++)
					DeleteColumn(row, colcnt - 1);
			}

			LockUpdates = false;
		}

		/// <summary>
		/// Sets the number of rows
		/// </summary>
		/// <param name="rows"></param>
		public void SetFixedRowCount(int rows)
		{
			if (rows < 0)
				return;

			int rowcnt = rowList.Count;

			LockUpdates = true;
			for (int i = 0; i < Math.Max(rowcnt, GetFixedRowCount()); i++)
			{
				if (i >= rows)
					GetRow(i).Header = false;
				else
					GetRow(i).Header = true;
			}
			LockUpdates = false;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Sizing and position functions
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		#region Sizing_and_position
		/// <summary>
		/// Get Row Height
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		public int GetRowHeight(int row)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			return rowList[row].Size;
		}

		/// <summary>
		/// Set the row height
		/// </summary>
		/// <param name="row"></param>
		/// <param name="height"></param>
		public void SetRowHeight(int row, uint height)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			rowList[row].Size = (int)height;
		}

		/// <summary>
		/// Get Column Width
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		public int GetColumnWidth(int row, int col)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];
						
			if (col < 0 || col > r.Count - 1)
				throw new GridException("Column out of range");
			
			return r.cellList[col].Width;
		}

		/// <summary>
		/// Set Column Width
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		public void SetColumnWidth(int row, int col, int width)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			if (col < 0 || col > r.Count - 1)
				throw new GridException("Column out of range");

			r.cellList[col].Width = width;
		}
		#endregion
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		// General appearance and features
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		#region Appearance_and_features

		public EditStyle GetCellType(int row, int col)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			if (col < 0 || col > r.Count - 1)
				throw new GridException("Column out of range");

			return r.cellList[col].cellStyle;
		}

		public void SetCellType(int row, int col, EditStyle style)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			if (col < 0 || col > r.Count - 1)
				throw new GridException("Column out of range");

			r.cellList[col].cellStyle = style;
		}
		
		/// <summary>
		/// Enables/disables tooltips
		/// </summary>
		public bool ToolTips
		{
			get
			{
				return toolTips;
			}
			set
			{
				toolTips = value;
				toolTip.Active = false;
			}
		}
		/// <summary>
		/// Gets/Sets whether or not grid can edit
		/// </summary>
		public bool EditMode
		{
			set
			{
				CancelEditing();
				isEditMode = value;
			}

			get
			{
				return isEditMode;
			}
		}

		/// <summary>
		/// Sets/Gets grid line style
		/// </summary>
		public GridLineStyle GridLine
		{
			get
			{
				return gridLines;
			}

			set
			{
				gridLines = value;
				Invalidate();
			}
		}


		/// <summary>
		/// Sets/Gets image style
		/// </summary>
		public ImageStyleType ImageStyle
		{
			get
			{
				return imageStyle;
			}

			set
			{
				imageStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// Sets or Get the grid background image
		/// </summary>
		public Image Image
		{
			get
			{
				return this.image;
			}

			set
			{
				this.image = value;
				Invalidate();
			}
		}

		/// <summary>
		/// Sets/Gets the selection mode of the grid
		/// </summary>
		public SelectionModeType SelectionMode
		{
			get
			{
				return selectMode;
			}

			set
			{
				// Clear Previous selections
				if (selectMode == SelectionModeType.CellSelect)
					SelectCell(null);
				else
					SelectObject(null);

				selectMode = value;

				Invalidate();
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Colors/Colours
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		#region Colors
		public Color GridLineColor
		{
			set
			{
				gridLineColor = value;
				Invalidate();
			}

			get
			{
				return gridLineColor;
			}
		}


		/// <summary>
		/// Returns a row collection
		/// </summary>
		/// <returns></returns>
		public GridObjectCollection GetRowList()
		{
			return rowList;
		}

		public bool LockUpdates
		{
			set
			{
				lockUpdates = value;
			}

			get
			{
				return lockUpdates;
			}
		}


		public int GetGridOpacity()
		{
			return Opacity;
		}
		#endregion
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////
		#region Operations

		public Grid()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			vScrollBar.ValueChanged += new EventHandler(vScrollBar_ValueChanged);
			hScrollBar.ValueChanged += new EventHandler(hScrollBar_ValueChanged);

			this.SetStyle(ControlStyles.DoubleBuffer, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

			this.MouseWheel += new MouseEventHandler(Grid_MouseWheel);
			this.DoubleClick += new EventHandler(Grid_DoubleClick);

			this.BackColor = SystemColors.Window;

			hScrollBar.Visible = false;
			vScrollBar.Visible = false;

			toolTip.AutoPopDelay = 5000;
			toolTip.InitialDelay = 1000;
			toolTip.ReshowDelay = 1000;


			this.Controls.Add(hScrollBar);
			this.Controls.Add(vScrollBar);
			this.Focus();
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			//if (!lockUpdates)
			//{
				int rowOffset = 0;
				Rectangle clipRect = ClientRectangle;

				if (vScrollBar.Visible)
				{
					rowOffset = vScrollBar.Value;
					clipRect.Width -= vScrollBar.Width;

				}

				int colOffset = 0;
				if (hScrollBar.Visible)
				{
					colOffset = hScrollBar.Value;
					clipRect.Height -= hScrollBar.Height;
				}

				pe.Graphics.SetClip(clipRect);

				Draw(colOffset, rowOffset, pe.Graphics);
			//}

			// Calling the base class OnPaint
			base.OnPaint(pe);
		}

		private void Grid_Resize(object sender, System.EventArgs e)
		{
			AdjustScrollbars();
		}


		public void SetCell(int row, int col, Cell cell)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];
			
			if (col < 0 || col > r.cellList.Count - 1)
				throw new GridException("Column out of range");

			Cell replaceCell = GetCell(row, col);

			// Remove old cell
			Row rowReplace = replaceCell.row;
			rowReplace.ReplaceCell(replaceCell, cell);
		}

		public void AdjustScrollbars()
		{
			// No decide how many visible rows/columns are 
			// offscreen so we can calculate max values for scroll bars
			Size sizeDisplay = GetDisplaySize();
			Size sizeVirt = GetVirtualSize();
			bool bHVisible = false;
			bool bVVisible = false;

			if (sizeVirt.Width > ClientRectangle.Width)
			{
				hScrollBar.Top = ClientRectangle.Height - hScrollBar.Height;
				hScrollBar.Width = ClientRectangle.Width - 1;
				if (sizeDisplay.Height >= hScrollBar.Height)
					sizeDisplay.Height -= hScrollBar.Height;
				HScrollMax = sizeVirt.Width;
				hScrollBar.Visible = true;
				bHVisible = true;
			}
			else
				hScrollBar.Visible = false;

			if (sizeVirt.Height > sizeDisplay.Height)
			{
				vScrollBar.Left = ClientRectangle.Width - vScrollBar.Width;
				vScrollBar.Height = ClientRectangle.Height - 1;
				if(sizeDisplay.Width >= vScrollBar.Width)
					sizeDisplay.Width -= vScrollBar.Width;
				VScrollMax = sizeVirt.Height;
				vScrollBar.Visible = true;
				bVVisible = true;
			}
			else
				vScrollBar.Visible = false;

			hScrollBar.Minimum = 0;
			hScrollBar.Maximum = HScrollMax;

			vScrollBar.Minimum = 0;
			vScrollBar.Maximum = VScrollMax;

			hScrollBar.SmallChange = hScrollBar.LargeChange = HScrollMax > 0 ? sizeDisplay.Width : 0;
			vScrollBar.SmallChange = vScrollBar.LargeChange = VScrollMax > 0 ? sizeDisplay.Height : 0;

			if (bHVisible && bVVisible)
			{
				hScrollBar.Width = ClientRectangle.Width - vScrollBar.Width;
				vScrollBar.Height = ClientRectangle.Height - hScrollBar.Height;
			}
		}

		#endregion

		#region Grid Functions 
		/// <summary>
		/// Get the size of the grid
		/// </summary>
		/// <returns></returns>
		virtual public Size GridSize()
		{
			Size sz = new Size();
			//int maxWidth = 0;
			foreach (Row r in rowList)
				if (r.Visible)
				{
					sz.Height += r.Size;
					/*int wdth = 0;
					foreach (Cell c in r.cellList)
						wdth += c.Width;
					if (wdth > maxWidth)
						maxWidth = wdth;*/
				}
			sz.Width = this.otherColsWidth + this.firstColumnWidth;//maxWidth;
			return sz;
		}


		/// <summary>
		/// Adds a row using the default value
		/// </summary>
		/// <returns></returns>
		virtual public Row AddRow()
		{
			Row row = new Row(this, (int)defaultRowHeight);
			rowList.Add(row);
			AdjustScrollbars();
			Invalidate();
			return row;
		}

		/// <summary>
		/// Adds a row using the a predefined height
		/// </summary>
		/// <param name="height"></param>
		/// <returns></returns>
		virtual public Row AddRow(int height)
		{
			Row row = new Row(this, height);
			rowList.Add(row);
			AdjustScrollbars();
			Invalidate();
			return row;
		}

		/// <summary>
		/// Adds a column using a width
		/// </summary>
		/// <param name="width"></param>
		/// <returns></returns>
		virtual public Cell AddColumn(int row, string value, EditStyle style)
		{
			if (rowList.Count == 0)
				AddRow();

			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			Cell c = r.AddCell(value, style);
			c.TabOrder = this.MaxTabOrder() + 1;
			AdjustScrollbars();
			Invalidate();

			return c;
		}

		/// <summary>
		/// Adds a column using a width
		/// </summary>
		/// <param name="width"></param>
		/// <returns></returns>
		virtual public Cell AddColumn(int row, int width, string value)
		{
			if (rowList.Count == 0)
				AddRow();

			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			Cell c = r.AddCell(value, width);
			c.TabOrder = this.MaxTabOrder() + 1;
			AdjustScrollbars();
			Invalidate();

			return c;
		}

		/// <summary>
		/// Adds a column using a width
		/// </summary>
		/// <param name="width"></param>
		/// <returns></returns>
		virtual public Cell AddColumn(int row, string value)
		{
			if (rowList.Count == 0)
				AddRow();

			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

		    Row r = null;
            if (row >= 0 && rowList.Count > row)
                r = (Row)rowList[row];
		    Cell c = null;
            if (r!=null)
			 c = r.AddCell(value);
            if (c != null)
                c.TabOrder = this.MaxTabOrder() + 1;
			AdjustScrollbars();
			Invalidate();

			return c;
		}


        /// <summary>
        /// Adds a column using a width
        /// </summary>
        /// <param name="width"></param>
        /// <returns></returns>
        virtual public void RemoveColumn(int row)
        {
            if (rowList.Count != 0)
            {            
                if (row < 0 || row > rowList.Count - 1)
                    throw new GridException("Row out of range");

                Row r = (Row)rowList[row];

                r.DeleteCell(r.cellList.Count-1);            
                AdjustScrollbars();
                Invalidate();    
            }
        }

		/// <summary>
		/// Adds a column using a width
		/// </summary>
		/// <param name="width"></param>
		/// <returns></returns>
		virtual public Cell AddColumn(int row, Cell cell)
		{
			if (rowList.Count == 0)
				AddRow();

			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];

			Cell c = r.AddCell(cell.Value);
			c.TabOrder = this.MaxTabOrder() + 1;
			AdjustScrollbars();
			Invalidate();

			return c;
		}

		/// <summary>
		/// Gets a row object from a row number
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		virtual public GridObject GetRow(int r)
		{
			if (r > rowList.Count - 1)
				throw new GridException("Row out of range");

			return rowList[r];
		}

		/// <summary>
		/// Gets a column from a predefined column
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		virtual public Cell GetColumn(int row, int c)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];
			
			if (c > r.cellList.Count - 1)
				throw new GridException("Column out of range");

			return r.cellList[c];
		}

		/// <summary>
		/// Draws the grid
		/// </summary>
		/// <param name="xOffset"></param>
		/// <param name="yOffset"></param>
		/// <param name="g"></param>
		private void Draw(int xOffset, int yOffset, Graphics g)
		{
			// Give the canvas a change to paint
			DrawBackground(g);

			int y = 0;
			int yCnt = 0;
			otherColsWidth = Width - firstColumnWidth - (vScrollBar.Visible == true ? vScrollBar.Width : 0) - 1;
			foreach (Row r in rowList)
			{
				if (r.Visible)
				{
					r.ResizeRow(g);

					if (!r.Header && yCnt < yOffset)
					{
						yCnt += r.Size;
						continue;
					}

					if (y >= ClientRectangle.Y && y <= ClientRectangle.Bottom)
						r.Draw(xOffset, y, g);

					y += r.Size;
				}
			}
		}

		/// <summary>
		/// Override to draw grid background
		/// </summary>
		/// <param name="g"></param>
		virtual public void DrawBackground(Graphics g)
		{
			if (image != null)
				DrawImage(g);
			else
				g.FillRectangle(new SolidBrush(this.BackColor), 0, 0, ClientRectangle.Width, ClientRectangle.Height);
		}

		private void DrawImage(Graphics g)
		{
			switch (imageStyle)
			{
				case ImageStyleType.Center:
					break;
				case ImageStyleType.Stretch:
					g.DrawImage(image, ClientRectangle, new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
					break;
				case ImageStyleType.Tile:
					{
						for (int x = 0; x < ClientRectangle.Width; x += image.Width + 1)
						{
							for (int y = 0; y < ClientRectangle.Height; y += image.Height + 1)
								g.DrawImage(image, x, y, new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
						}
						break;
					}
			}
		}

		/// <summary>
		/// Gets a cell at a given row and column
		/// </summary>
		/// <param name="r"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		public Cell GetCell(int r, int c)
		{
			Cell cell = null;
			try
			{
				Row row = (Row)GetRow(r);
				cell = row.GetCell(c);

			}
			catch (GridException ge)
			{
				throw new GridException("GetCell out of range" + ge.Message);
			}

			return cell;
		}

		public Cell GetCell(int r, int c, bool bCreate)
		{
			Cell cell = null;
			try
			{
				Row row = (Row)GetRow(r);
				cell = row.GetCell(c);
			}
			catch
			{
				if (bCreate)
				{
					// For virtual grid ops
				}
				else // Return a copy
				{
					cell = CreateCell(r, c);
				}
			}

			return cell;
		}

		public bool ReadOnly
		{
			set
			{
				readOnly = value;
			}

			get
			{
				return readOnly;
			}
		}

		public void DeleteRow(int row)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			rowList.RemoveAt(row);
			Invalidate();
		}

		public void DeleteColumn(int row, int col)
		{
			if (row < 0 || row > rowList.Count - 1)
				throw new GridException("Row out of range");

			Row r = (Row)rowList[row];
			
			if (col < 0 || col > r.cellList.Count - 1)
				throw new GridException("Column out of range");

			r.DeleteCell(col);
			Invalidate();
		}

		private void Grid_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{

			this.mX = e.X;
			this.mY = e.Y;
			if (sizerCell.IsDragging)
			{
				Graphics g = Graphics.FromHwnd(this.Handle);
				sizerCell.Drag(g, new Point(e.X, e.Y));
			}
			else
			{
				Point pt = new Point(e.X, e.Y);
				GridObject gridObject;
				HandleHitTest(ref pt, out gridObject);
			}

			Cell cell = GetCellFromPoint(new Point(e.X, e.Y));
			if (ToolTips)
			{
				if (cell != null)
				{
					if (tipCell != cell)
					{
						tipCell = cell;
						toolTip.SetToolTip(this, tipCell.TipText);
					}
				}
				else
					toolTip.SetToolTip(this, "");
			}
		}

		/// <summary>
		/// Internal stuff for handling hit testing
		/// </summary>
		/// <param name="pt"></param>
		/// <param name="selectObject"></param>
		/// <returns></returns>
		protected HitCode HandleHitTest(ref Point pt, out GridObject selectObject)
		{
			// Check list
			// a.	Row/Col Resize Cursor
			// b.	Fire OnCellHitTest
			selectObject = null;

			this.Cursor = Cursors.Arrow;
			//int xOffset = 0;
			int yOffset = 0;
			int nSkipCnt;

			nSkipCnt = 0;
			int nRowPos = this.GetFirstVisibleRowPosition();

			for (int r = 0; r < rowList.Count; r++)
			{
				Row row = (Row)rowList[r];
				if (row.Count > 0)
				{
					Cell cell = GetCell(r, 0);

					if (!cell.row.Visible)
						continue;

					if (!cell.row.Header && nSkipCnt < nRowPos)
					{
						nSkipCnt += cell.row.Size;
						continue;
					}

					yOffset += cell.row.Size;

					Rectangle rcCell = new Rectangle(0, yOffset, (int)cell.Width, (int)cell.row.Size);


					if (pt.X >= rcCell.X && pt.X <= rcCell.Right)
					{
						if (pt.Y >= rcCell.Y - Tolerence && pt.Y <= rcCell.Y + Tolerence)
						{
							this.Cursor = Cursors.SizeNS;
							pt.Y = yOffset - cell.row.Size;
							selectObject = cell.row;

							if (!selectObject.CanResize)
							{
								this.Cursor = Cursors.Arrow;
								return HitCode.hitNone;
							}

							return HitCode.hitRowEdge;
						}
					}
				}
			}

			yOffset = 0;
			nSkipCnt = 0;

			int nColPos = this.GetFirstVisibleColumnPosition();

		/*	for (int c = 0; c < colList.Count; c++)
			{
				Cell cell = GetCell(0, c);
				if (!cell.col.Visible)
					continue;

				if (!cell.col.Header && nSkipCnt < nColPos)
				{
					nSkipCnt += cell.col.Size;
					continue;
				}

				xOffset += cell.col.Size;
				Rectangle rcCell = new Rectangle(xOffset, 0, (int)cell.col.Size, (int)cell.row.Size);

				if (pt.Y >= rcCell.Y && pt.Y <= rcCell.Bottom)
				{
					if (pt.X >= rcCell.X - Tolerence && pt.X <= rcCell.X + Tolerence)
					{
						this.Cursor = Cursors.SizeWE;
						pt.X = xOffset - cell.col.Size;
						selectObject = cell.col;

						if (!selectObject.CanResize)
						{
							this.Cursor = Cursors.Arrow;
							return HitCode.hitNone;
						}

						return HitCode.hitColumnEdge;
					}
				}
			}*/

			return HitCode.hitNone;
		}

      public int widthCellText(Cell cell)
      {
         string text = cell.Value;
         int fontHeight = cell.TextHeight;
         Font cellFont = new Font(cell.FontName, fontHeight, cell.FontStyle, GraphicsUnit.Pixel);
         int retWidth = (int)Graphics.FromHwnd(this.Handle).MeasureString(text, cellFont).Width;
			return retWidth;
      }

		private void Grid_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			// Cancel Editing
			CancelEditing();

			Point pt = new Point(e.X, e.Y);//this.PointToClient(Cursor.Position);
			Point ptSave = pt;			

			switch (selectMode)
			{
				case SelectionModeType.CellSelect:
					{

						Cell cell = GetCellFromPoint(pt);
						if (cell == null)
							return;

						/*if (cell.row.Header || cell.Col == 0)
							return;*/
						if(selectedCell != null)
							selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
						SelectCell(cell);
						EnsureVisible(cell);
						BeginEditing();
					
						break;
					}
				/*case SelectionModeType.RowSelect:
					{
						Cell cell = GetCellFromPoint(pt);
						if (cell == null)
							return;

						if (cell.Col == 0)
							return;

						SelectObject(cell.row);
						break;
					}
				*/
			}
		}

		public void SelectObject(GridObject obj)
		{
			if (selectedObject != null)
				selectedObject.Selected = false;

			if (obj != null)
			{
				obj.Selected = true;
			}
			selectedObject = obj;
		}

		public void SelectCell(Cell cell)
		{
			if (selectedCell != null)
			{
				selectedCell.Selected = false;
				selectedCell.row.Selected = false;				
			}

			if (cell != null)
			{
				/*if (cell.row.Header || cell.Col == 0)
					return;*/

				cell.Selected = true;
				if(OnCellSelect != null)
					OnCellSelect(cell);
			}
			selectedCell = cell;
			if (selectedCell != null)
			{
				selectedCell.row.Selected = true;				
			}
		}

		public void InvalidateCell(Cell cell, bool forceUpdate)
		{
			Invalidate(GetCellRect(cell));
		}

		public void Invalidate(GridObject gridObj)
		{
			// Need revisit this as we definitely should really opn querying object type
			switch (gridObj.GetObjectType())
			{
				case GridObject.ObjectType.Row:
					Invalidate(GetRowRect(gridObj));
					break;			
			}
		}

		public Rectangle GetRowRect(GridObject rowComp)
		{
			int yOffset = 0;

			int nRowPos = this.GetFirstVisibleRowPosition();
			int nColPos = this.GetFirstVisibleColumnPosition();

			// Locate Row / Then Column
			Rectangle rcCell = new Rectangle();
			foreach (Row row in rowList)
			{
				if (!row.Visible)
					continue;

				if (nRowPos > 0)
				{
					nRowPos--;
					continue;
				}

				if (row == rowComp)
				{
					// Found Correct col
					rcCell.Y = yOffset;
					rcCell.Height = row.Size;

					foreach (Cell c in row.cellList)
					{
						if (c.Visible)
						{
							if (nColPos > 0)
							{
								nColPos--;
								continue;
							}
							rcCell.Width += c.Width;
						}
					}
				}
				yOffset += row.Size;
			}

			return rcCell; // Didn't find it so show as empty
		}

		public Rectangle GetCellRect(Cell cell)
		{
			int yOffset = 0;

			int nRowPos = this.GetFirstVisibleRowPosition();

			int nRowSkipCnt = 0;

			// Locate Row / Then Column
			foreach (Row row in rowList)
			{
				if (!row.Visible)
					continue;

				if (!row.Header && nRowSkipCnt < nRowPos)
				{
					nRowSkipCnt += row.Size;
					continue;
				}

				if (row == cell.row)
				{
					// Found Correct col
					int nColPos = this.GetFirstVisibleColumnPosition();
					int nColSkipCnt = 0;
					// Found Correct Row
					Rectangle rcCell = new Rectangle(0, yOffset, 0, (int)row.Size);
					foreach (Cell c in row.cellList)
					{
						if (!c.Visible)
							continue;

						if (/*!col.Header &&*/ nColSkipCnt < nColPos)
						{
							nColSkipCnt += c.Width;
							continue;
						}

						rcCell.Width = c.Width;
						if (c == cell)
						{
							return rcCell;							
						}
						rcCell.Offset((int)c.Width, 0);

					}
				               
					rcCell.Width = cell.Width;
					return rcCell;
				}
				yOffset += row.Size;
			}

			return new Rectangle(); // Didn't find it so show as empty
		}		

		private void Grid_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			sizerCell.EndDrag(new Point(e.X, e.Y));//PointToClient(Cursor.Position));
		}

		private int GetRowHeaderSize()
		{
			int nSize = 0;
			foreach (Row r in rowList)
			{
				if (r.Visible)
				{
					if (r.Header)
					{
						nSize += r.Size;
						continue;
					}

					break;
				}
			}

			return nSize;
		}

		public int GetRowHeaderCount()
		{
			int cnt = 0;
			foreach (Row r in rowList)
			{
				if (r.Visible)
				{
					if (r.Header)
					{
						cnt++;
						continue;
					}

					break;
				}
			}

			return cnt;
		}

		private int GetFirstVisibleColumnPosition()
		{
			int i = 0;
			if (hScrollBar.Visible == false)
				return i;

			return hScrollBar.Value;
		}

		private int GetFirstVisibleRowPosition()
		{
			int i = 0;
			if (vScrollBar.Visible == false)
				return i;

			return vScrollBar.Value;
		}

		/// <summary>
		/// Gets the bounding grid rectangle
		/// </summary>
		/// <returns></returns>
		public Size GetBounds()
		{	
			Size sz = GridSize();
			if (sz.Width > ClientRectangle.Width)
				sz.Width = ClientRectangle.Width - (vScrollBar.Visible ? vScrollBar.Width : 0);

			if (sz.Height > ClientRectangle.Height)
				sz.Height = ClientRectangle.Height - (hScrollBar.Visible ? hScrollBar.Height : 0);

			return sz;
		}

		/// <summary>
		/// Sets or gets the Opacity value
		/// </summary>
		public int GridOpacity
		{
			get
			{
				double d = (Opacity / 2.55) + 0.5;
				return (int)d;
			}

			set
			{
				Opacity = (int)((((double)value * 0.01) * 255) + 0.5);
				Invalidate();
			}
		}

		public Cell SelectedCell
		{
			get
			{
				return selectedCell;
			}

			set
			{
				CancelEditing();
				selectedCell = value;
				BeginEditing();
			}
		}

		public void UpdateCellValue()
		{
			if (textBox != null)
			{
				textBox.RefreshValueToCell();
			}
		}

		public void RefreshSelectedCell()
		{
		/*	string value = c.Value;
			CancelEditing();
			c.Value = value;
			selectedCell = c;
			BeginEditing();*/
			if (textBox != null)
				textBox.RefreshValueToTextBox();
		}

		public Cell GetCellFromKey(string key)
		{
			foreach (Row r in rowList)
			{ 
				foreach(Cell c in r.cellList)
					if ((string)c.Key == key)
						return c;	
			}
			return null;
		}

		/// <summary>
		/// Gets a cell from a mouse position
		/// </summary>
		/// <param name="pt"></param>
		/// <returns></returns>
		/// 
		public Cell GetCellFromPoint(Point pt)
		{
			int yOffset = 0;

			int nRowPos = this.GetFirstVisibleRowPosition();

			// Locate Row / Then Column
			int nRowSkipCnt = 0;
			for (int r = 0; r < rowList.Count; r++)
			{
				Row row = (Row)GetRow(r);
				if (!row.Visible)
					continue;

				if (!row.Header && nRowSkipCnt < nRowPos)
				{
					nRowSkipCnt += row.Size;
					continue;
				}

				if (pt.Y >= yOffset && pt.Y <= yOffset + row.Size)
				{

					int nColPos = 0;//this.GetFirstVisibleColumnPosition();
					int nColSkipCnt = 0;
					// Found Correct Row
					Rectangle rcCell = new Rectangle(0, yOffset, 0, (int)row.Size);
					foreach (Cell c in row.cellList)
					{
						if (!c.Visible)
							continue;

						if (/*!col.Header &&*/ nColSkipCnt < nColPos)
						{
							nColSkipCnt += c.Width;
							continue;
						}

						rcCell.Width = c.Width;
						if (pt.X >= rcCell.X && pt.X <= rcCell.Right)
						{
							return c;//GetCell(rowList.IndexOf(row), row.cellList.IndexOf(c), false);
						}
						rcCell.Offset((int)c.Width, 0);

					}
				}

				yOffset += row.Size;
			}
			return null;
		}

		public Row GetRowFromPoint(Point pt)
		{
			int yOffset = 0;

			int nRowPos = this.GetFirstVisibleRowPosition();

			// Locate Row / Then Column
			foreach (Row row in rowList)
			{
				if (!row.Visible)
					continue;

				if (!row.Header && nRowPos > 0)
				{
					nRowPos -= row.Size;
					continue;
				}

				if (pt.Y >= yOffset && pt.Y <= yOffset + row.Size)
					return row;

				yOffset += row.Size;
			}

			return null;
		}

		public Size GetDisplaySize()
		{
			Size sz = ClientRectangle.Size;
			/*
			foreach (Row r in rowList)
			{
				if (r.Header && r.Visible)
					sz.Height -= r.Size;
			}

			foreach (Column c in colList)
			{
				if (c.Visible && c.Header)
					sz.Width -= c.Size;
			}*/

			return sz;
		}

		public Size GetVirtualSize()
		{
			Size sz = new Size();

			foreach (Row r in rowList)
			{
				if (r.Visible /*&& !r.Header*/)
					sz.Height += r.Size;
			}

			/*foreach (Column c in colList)
			{
				if (c.Visible && !c.Header)
					sz.Width += c.Size;
			}*/
			sz.Width += this.firstColumnWidth + this.otherColsWidth + (vScrollBar.Visible ? vScrollBar.Width : 0);

			return sz;
		}

		private void vScrollBar_ValueChanged(object sender, EventArgs e)
		{
			CancelEditing();
			Invalidate();
		}

		private void hScrollBar_ValueChanged(object sender, EventArgs e)
		{
			CancelEditing();
			Invalidate();
		}

		public void EnsureVisible(Row r)
		{
			int offsetFwd = 0;
			int viewCnt = 0;
			int objPos = this.GetFirstVisibleRowPosition();
			int topPos = 0;
			bool bTop = false;

			// Locate Row / Then Column
			foreach (Row row in rowList)
			{
				if (!row.Visible)
					continue;

				if (!row.Header)
					offsetFwd += row.Size;

				// This deal with a row
				if (objPos > 0)
				{
					objPos -= row.Size;
					if (row == r)
					{
						vScrollBar.Value = offsetFwd - row.Size;
						return;
					}
					continue;
				}

				if (!bTop)
				{
					topPos = offsetFwd;
					bTop = true;
				}


				// Visible rows
				viewCnt += row.Size;

				if (r == row)
				{
					if (viewCnt <= GetRowHeaderSize())
					{
						vScrollBar.Value -= row.Size;
						return;
					}

					if (viewCnt >= ClientRectangle.Height)
						vScrollBar.Value = topPos + row.Size;

					return;
				}
			}
		}		

		public Rectangle GetVisualRect()
		{
			Rectangle r = ClientRectangle;
			if (vScrollBar.Visible == true)
				r.Width -= vScrollBar.Width;

			if (hScrollBar.Visible == true)
				r.Height -= hScrollBar.Height;

			return r;
		}

		public void EnsureVisible(Cell cell)
		{
			EnsureVisible(cell.row);
			//EnsureVisible(cell.col);
		}

		/// <summary>
		/// Support for mousewheel 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Grid_MouseWheel(object sender, MouseEventArgs e)
		{
			if (vScrollBar.Visible == false)
				return;

			if (e.Delta < 0)
			{
				if (vScrollBar.Value + GetVisibleRow().Size + GetDisplaySize().Height < vScrollBar.Maximum)
					vScrollBar.Value += GetVisibleRow().Size;
				else
					vScrollBar.Value = vScrollBar.Maximum - (GetDisplaySize().Height - +GetVisibleRow().Size);
			}
			else
			{
				if (e.Delta > 0)
				{
					if (vScrollBar.Value - GetVisibleRow().Size > 0)
						vScrollBar.Value -= GetVisibleRow().Size;
					else
						vScrollBar.Value = 0;
				}
			}
		}

		private Row GetVisibleRow()
		{
			int y = 0;
			Row row = null;
			foreach (Row r in rowList)
			{
				if (r.Visible)
				{
					if (y >= vScrollBar.Value)
						return r;

					y += r.Size;
				}
			}
			return row;
		}

		/// <summary>
		/// To do implement double click on cell header (row/col autosize)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Grid_DoubleClick(object sender, EventArgs e)
		{
			Point pt = new Point(mX, mY);//this.PointToClient(Cursor.Position);
			GridObject gridObject;
			switch (HandleHitTest(ref pt, out gridObject))
			{
				case HitCode.hitColumnEdge:
					{
						break;
					}
				case HitCode.hitRowEdge:
					{
						break;
					}

				default:
					BeginEditing();
					break;
			}


		}

		public void Grid_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			this.OnMouseDoubleClick(e);
		}

		private void SelectionUp()
		{
			// Not Applicable
			if (selectMode == SelectionModeType.CellSelect && selectedCell != null)
			{
				if (rowList.IndexOf(selectedCell.row) < 1)
					return;				

			//	GridObject gridObj = rowList.GetPrevObject(selectedCell.row);
			//	Row r = (Row)gridObj;
				
				int col = selectedCell.Col;
				int row = rowList.IndexOf(selectedCell.row);
				Row r = selectedCell.row;
				do
				{
					row--;
					if(row >=  0)
						r = (Row)GetRow(row);				
				} while (r.Count <= 0 && row >= 0);

				if (row < 0)
					return;

				Cell cell = null;
				if(r.Count > col)
					cell = GetCell(row, col);
				else
					cell = GetCell(row, r.Count - 1);
				CancelEditing();
				if (selectedCell != null)
					selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
				SelectCell(cell);
				EnsureVisible(cell);
				BeginEditing();

			}
		}

		private void SelectionDown()
		{
			if (selectMode == SelectionModeType.CellSelect && selectedCell != null)
			{
				if (rowList.IndexOf(selectedCell.row) == GetRowCount() - 1)
					return;

				int col = selectedCell.Col;
				int row = rowList.IndexOf(selectedCell.row);
				Row r = selectedCell.row;
				do
				{
					row++;
					if (row < GetRowCount())
						r = (Row)GetRow(row);
				} while (r.Count <= 0 && row <= GetRowCount());

				if (row >= GetRowCount())
					return;

				Cell cell = null;
				if (r.Count > col)
					cell = GetCell(row, col);
				else
					cell = GetCell(row, r.Count - 1);

				CancelEditing();
				if (selectedCell != null)
					selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
				SelectCell(cell);
				EnsureVisible(cell);
				BeginEditing();
			}
		}

		private void SelectionLeft()
		{			
			if (selectMode == SelectionModeType.CellSelect && selectedCell != null)
			{				
				int col = selectedCell.Col;
				int row = rowList.IndexOf(selectedCell.row);
				Row r = selectedCell.row;
				Cell cell = null;
				if (selectedCell.Col == 0)
				{
					do
					{
						row--;
						if (row >= 0)
							r = (Row)GetRow(row);
					} while (r.Count <= 0 && row >= 0);

					if (row < 0)
						return;

					cell = GetCell(row, r.Count - 1);
				}
				else
					cell = GetCell(row, col - 1);

				CancelEditing();
				if (selectedCell != null)
					selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
				SelectCell(cell);
				EnsureVisible(cell);
				BeginEditing();
			}
		}

		private void SelectionRight()
		{
			if (selectMode == SelectionModeType.CellSelect && selectedCell != null)
			{
				int col = selectedCell.Col;
				int row = rowList.IndexOf(selectedCell.row);
				Row r = selectedCell.row;
				Cell cell = null;
				if (selectedCell.Col == selectedCell.row.Count - 1)
				{
					do
					{
						row++;
						if (row < GetRowCount())
							r = (Row)GetRow(row);
					} while (r.Count <= 0 && row <= GetRowCount());

					if (row >= GetRowCount())
						return;

					cell = GetCell(row, 0);
				}
				else
					cell = GetCell(row, col + 1);

				CancelEditing();
				if (selectedCell != null)
					selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
				SelectCell(cell);
				EnsureVisible(cell);
				BeginEditing();
			}
		}

		protected override void OnEnter(EventArgs e)
		{
			base.OnEnter(e);
			if (selectedCell == null)
			{
				int col = -1;
				int row = -1;
				if (FindMinTabOrder(out row, out col))
				{
					SelectCell(GetCell(row, col));
					BeginEditing();
				}
				else
                    if (GetRowCount() > 0 && GetColumnCount(0) > 0 && row>0 && col>0)
					{
						SelectCell(GetCell(row, col));
						BeginEditing();
					}
			}
		}

		protected override void OnLeave(EventArgs e)
		{
			base.OnLeave(e);
			if (selectedCell != null)
			{
				CancelEditing();
				selectedCell = null;
			}
			
		}

		protected override bool ProcessDialogKey(Keys keyData)
		{
			switch (keyData)
			{
				case Keys.LButton:
				{
					break;
				}
				case Keys.Down:
					{
						SelectionDown();
						break;
					}
				case Keys.Up:
					{
						SelectionUp();
						break;
					}
				case Keys.Left:
					{
						SelectionLeft();
						break;
					}
				case Keys.Right:
					{
						SelectionRight();
						break;
					}
            case Keys.Enter:
               {
                  if (selectedCell != null)
                  {
                     if (!TabForward())
                     {
                        Cell c = selectedCell;
                        selectedCell = null;
                        CancelEditing();
                        InvalidateCell(c, true);
                        return base.ProcessDialogKey(keyData);
                     }
                     else
                     {
                        BeginEditing();
                        return true;
                     }
                  }
                  else
                  {
                     int col = -1;
                     int row = -1;
                     if (FindMinTabOrder(out row, out col))
                     {
                        SelectCell(GetCell(row, col));
                        BeginEditing();
                        return true;
                     }
                     else
                        if (GetRowCount() > 0 && GetColumnCount(0) > 0)
                        {
                           SelectCell(GetCell(row, col));
                           BeginEditing();
                           return true;
                        }
                  }

               }
               break;
				case Keys.Tab:
					{
						if (selectedCell != null)
						{
							if (!TabForward())
							{
								Cell c = selectedCell;
								selectedCell = null;
								CancelEditing();
								InvalidateCell(c, true);
								return base.ProcessDialogKey(keyData);
							}
							else
							{
								BeginEditing();
								return true;
							}
						}
						else
						{
							int col = -1;
							int row = -1;
							if (FindMinTabOrder(out row, out col))
							{
								SelectCell(GetCell(row, col));
								BeginEditing();
								return true;
							}
							else
								if (GetRowCount() > 0 && GetColumnCount(0) > 0)
								{
									SelectCell(GetCell(row, col));
									BeginEditing();
									return true;
								}
						}

					}
					break;
			}
			return base.ProcessDialogKey(keyData);
		}

		protected bool FindTabOrder(int _findTabOrder, out int row, out int col)
		{
			row = -1;
			col = -1;
			for (int i = 0; i < rowList.Count; i++)//По строкам
			{
				Row r = (Row)GetRow(i);
				for (int j = 0; j < r.cellList.Count; j++) //По столбцам
					if (r.cellList[j].TabOrder == _findTabOrder)
					{// Нашли TabOrder
						row = i;
						col = j;
						return true;
					}
			}
			return false;
		}

		protected bool FindMinTabOrder(out int row, out int col)
		{
			row = -1;
			col = -1;
			int minTO = MaxTabOrder();
			for (int i = 0; i < rowList.Count; i++)//По строкам
			{
				Row r = (Row)rowList[i];
				for (int j = 0; j < r.cellList.Count; j++) //По столбцам
					if (r.cellList[j].TabOrder < minTO && r.cellList[j].TabOrder > -1)
					{// Нашли TabOrder
						minTO = r.cellList[j].TabOrder;
						row = i;
						col = j;
					}
			}
			if(row != -1 && col != -1)
				return true;
		    return false;
		}

		protected int MaxTabOrder()
		{
			int maxTO = -1;
			for (int i = 0; i < rowList.Count; i++)//По строкам
			{
                if (rowList.Count > i)
                {
                    Row r = (Row) rowList[i];
                    for (int j = 0; j < r.cellList.Count; j++) //По столбцам
                        if (r.cellList[j].TabOrder > maxTO) // Нашли TabOrder
                        {
                            maxTO = r.cellList[j].TabOrder;
                        }
                }
			}
			return maxTO;
		}

		protected bool TabForward()
		{
			if (selectMode != SelectionModeType.CellSelect)
				return false;

			if (selectedCell == null)
				return false;

			if (rowList.Count == 0)
				return false;

			if (selectedCell.TabOrder < 0)
				return false;
			int col = -1;
			int row = -1;
			int newTab = selectedCell.TabOrder + 1;
			if (FindTabOrder(newTab, out row, out col))
			{
				Cell c = GetCell(row, col);
				CancelEditing();
				if (selectedCell != null)
					selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
				SelectCell(c);
				EnsureVisible(c);
				return true;
			}
			else
				return false;
		}

      public void CellSelect(Cell c)
      {
         if (selectMode != SelectionModeType.CellSelect)
            return;

         if (selectedCell == null)
            return;

         if (rowList.Count == 0)
            return;

         c.row.ResizeRow(Graphics.FromHwnd(this.Handle));
         CancelEditing();
         if (selectedCell != null)
            selectedCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
         SelectCell(c);
         EnsureVisible(c);
         BeginEditing();
      }

		protected bool TabBackward()
		{
			if (selectMode != SelectionModeType.CellSelect)
				return false;

			if (selectedCell == null)
				return false;

			if (rowList.Count == 0)
				return false;

			if (selectedCell.TabOrder <= 0)
				return false;
			int col = -1;
			int row = -1;
			if (FindTabOrder(selectedCell.TabOrder - 1, out row, out col))
			{
				Cell c = GetCell(row, col);
				SelectCell(c);
				EnsureVisible(c);
				return true;
			}
			else
				return false;


			/*if (selectMode != SelectionModeType.CellSelect)
				return;

			if (selectedCell == null)
				return;

			if (colList.Count == 0 || rowList.Count == 0)
				return;

			if (selectedCell.col == colList[0])
			{
				GridObject gridObj = rowList.GetPrevObject(selectedCell.row);
				if (gridObj != null)
				{
					Cell cell = GetCell(rowList.IndexOf(gridObj), colList.Count - 1);
					SelectCell(cell);
					EnsureVisible(cell);
				}
			}
			else
			{
				GridObject gridObj = colList.GetPrevObject(selectedCell.col);
				if (gridObj != null)
				{
					Cell cell = GetCell(rowList.IndexOf(selectedCell.row), colList.IndexOf(gridObj));
					SelectCell(cell);
					EnsureVisible(cell);
				}
			}*/
		}

		private void Grid_KeyDown(object sender, KeyEventArgs e)
		{
			//if (e.KeyData == Keys.Down)
			//{				
			//	BeginEditing();
			//}
		}


		public void BeginEditing()
		{
			if (!isEditMode)
				return;

			if (selectMode != SelectionModeType.CellSelect)
				return;

			if (selectedCell == null)
				return;

		//	if (selectedCell.CanEdit == false)
		//		return;

			// Bring into view
			EnsureVisible(selectedCell);			
			//EnsureVisible(selectedCell.col);

            selectedCell.BeginEdit();

			Rectangle rect = GetCellRect(selectedCell);

			textBox = new CellEditor(this, selectedCell, rect);
			this.Controls.Add(textBox);
			textBox.Focus();
			textBox.SetFocus();
		
		}

		public void CellButtonClick(Cell cell)
		{
			if(OnCellButtonClick != null)
				OnCellButtonClick(cell);

		   cell.CallOnPressButtonEventHandlers();
		}

		public void DataValidate(Cell cell)
		{
		    if(OnDataValidate != null)
				OnDataValidate(cell);
		}

		public void CancelEditing()
		{
			if (textBox != null)
			{
				//textBox.FinishEditing            
				textBox.Dispose();
            this.Controls.Remove(textBox);
				textBox = null;
            Invalidate();
            Update();
			}
		}


		/// <summary>
		/// Override to construct your own cells
		/// </summary>
		/// <param name="r"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		virtual public Cell CreateCell(int r, int c)
		{
			// Normal Circumstances create a cell
			if (r < 0)  
				throw new GridException("Row out of range");

			if(r > rowList.Count - 1)
			{
				while(r <= rowList.Count - 1)
					AddRow();
			}

			Row row = (Row)rowList[r];

			if(c > row.cellList.Count - 1)
			{
				while (c <= row.cellList.Count - 1)
					row.AddCell(null);
			}
			return row.cellList[c];
		}

        public bool IsValid
        {
            get
            {
                bool valid = true;

                foreach (Row row in rowList)
                {
                    foreach (Cell cell in row.cellList)
                    {
                        if (cell.Visible)
                            valid = valid & cell.IsValid;
                    }
                }
                return valid;
            }
        }
	}

	#endregion
	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////
	#region Other_classes
	public class GridException : Exception
	{
		public GridException(string message)
			: base(message)
		{
		}

		public GridException(string message, Exception inner)
			: base(message, inner)
		{
		}
	}
	public class StringEditing
	{
		public static int BreakString(string _strIn, Font cellFont, int width, Graphics g, out List<string> listString)
        {
            if (g == null)
            {
                listString = null;
                return 0;
            }
            listString = new List<string>();
            int retHeight = 6;
            // Вычисляем ширину без отступов
            width -= 9;
            if (width < 1) width = 1; //Заглушка

            string[] draft = _strIn.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);  
            foreach (string str in draft)
            {
                string SubString = "";
                string WordStr = "";

                string strIn = str; //to make it modifiable
                while (strIn != "")
                {// Пока не опустошим строку
                    string Char = strIn.Substring(0, 1);
                    strIn = strIn.Remove(0, 1);  //Удаляем первый символ
                    if (Char == " ")
                    {//Нашли пробел
                        if (SubString != "" && WordStr != "")
                            SubString += " ";  //Добавим пробел
                        SubString += WordStr; //Добавляем новое слово
                        WordStr = "";         //Поиск нового слова
                    }
                    else
                    {//Символ слова
                        WordStr += Char;   //Добавляем букву к слову

                        if (width <= g.MeasureString(SubString + " " + WordStr, cellFont).Width)
                        {// Нашли подстроку
                            if (SubString != "")
                            {// Добавляем подстроку
                                listString.Add(SubString);
                                retHeight += (int)g.MeasureString(SubString, cellFont).Height;
                                SubString = "";
                            }
                            else
                            {// Добавляем слово
                                listString.Add(WordStr);
                                retHeight += (int)g.MeasureString(WordStr, cellFont).Height;
                                WordStr = "";
                            }
                        }
                    }
                }
                // Добавляе оставшиеся подстроки
                if (SubString != "")
                {
                    if ((WordStr != "") && (width > g.MeasureString(SubString + " " + WordStr, cellFont).Width))
                    {// Еще помещается подстрока
                        SubString += " " + WordStr;
                        WordStr = "";
                    }
                    listString.Add(SubString);
                    retHeight += (int)g.MeasureString(SubString, cellFont).Height;
                }
                if (WordStr != "")
                {
                    listString.Add(WordStr);
                    retHeight += (int)g.MeasureString(WordStr, cellFont).Height;
                }
            }
            return retHeight;  //Возвращаем высоту
        }
	
	}
	
		#endregion
}
