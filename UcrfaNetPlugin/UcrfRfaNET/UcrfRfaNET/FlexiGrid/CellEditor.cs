using System;
using System.Drawing;
using System.Windows.Forms;
using XICSM.UcrfRfaNET;
using Lis.CommonLib.Binding;

namespace GridCtrl
{
    /// <summary>
    /// 
    /// </summary>
    public class CellEditor : System.Windows.Forms.Panel// System.Windows.Forms.TextBox//
    {
        //public delegate void CellDataValidateDelegate(Cell cell);

        // Events
        //public event CellDataValidateDelegate OnCellDataValidate; 

        private Grid gridCtrl = null;
        private Cell selectedCell = null;
        private ComboBoxTab pComboBox = null;            //����� �� ������
        private TextBox pMemo = null;                //
        private Button pBitBtn = null;                  //������ ������ ���. ����

        public CellEditor(Grid gridCtrl, Cell selectedCell, Rectangle rect)
        {
            this.gridCtrl = gridCtrl;
            this.selectedCell = selectedCell;
            this.Font = new Font(selectedCell.FontName, selectedCell.TextHeight, selectedCell.FontStyle, GraphicsUnit.Pixel);
            this.AutoSize = false;
            this.BorderStyle = BorderStyle.FixedSingle;
            this.Bounds = rect;

            if (selectedCell.cellStyle == EditStyle.esEllipsis && !gridCtrl.ReadOnly)
            {//� ���������
                pMemo = new TextBox();
                pMemo.Height = rect.Height;
                pMemo.Multiline = true;
                pMemo.Dock = DockStyle.Left;
                pMemo.Visible = true;
                pMemo.Enabled = true;
                pMemo.ReadOnly = (selectedCell.CanEdit ? false : true);
                pMemo.TextAlign = selectedCell.HorizontalAlignment;
                pMemo.BorderStyle = BorderStyle.FixedSingle;
                //pMemo.Bounds = rect;				
                // ���������� ������ ������
                string StrButtom = selectedCell.ButtonText;
                Font drawFont = new Font(selectedCell.FontName, selectedCell.TextHeight, selectedCell.FontStyle, GraphicsUnit.Pixel);
                Graphics g = this.CreateGraphics();
                SizeF sizeT = g.MeasureString(StrButtom, drawFont);
                int WidthButtom = (int)sizeT.Width + 15; //50;// Canvas.WTextWidth(StrButtom) + 15;
                if (WidthButtom > Width / 3)
                {// ������ ����� �������
                    //List<string> pStrList = new List<string>();
                    WidthButtom = Width / 4;
                    //userControl.FlexibleGrid.BreakString(StrButtom, WidthButtom > 15 ? (WidthButtom-15):1, Canvas, pStrList);
                    //StrButton = "";
                    /*for (int i = 0; i < pStrList.Count; i++)
                    {
                        if (StrButtom != "")
                            StrButtom += "\n";
                        StrButtom += pStrList[i];
                    }*/
                }
                pMemo.Width = Width - WidthButtom;
                pMemo.Text = selectedCell.Value.ToString();
                pMemo.BackColor = selectedCell.BackColor;
                pMemo.LostFocus += new EventHandler(DataValidate);
                pMemo.MouseDoubleClick += new MouseEventHandler(CellEditor_MouseDoubleClick);
                pBitBtn = new Button();
                pBitBtn.Left = pMemo.Width;
                pBitBtn.Height = Height;
                pBitBtn.Width = Width - pMemo.Width;
                pBitBtn.Text = StrButtom;
                pBitBtn.Click += new EventHandler(pBitBtn_Click);
                pBitBtn.MouseDoubleClick += new MouseEventHandler(CellEditor_MouseDoubleClick);
                this.Controls.Add(pMemo);
                this.Controls.Add(pBitBtn);
                pMemo.SelectAll();
            }
            else if (selectedCell.cellStyle == EditStyle.esPickList && !gridCtrl.ReadOnly)
            {//� ComboBox
                pComboBox = new ComboBoxTab();

                //pComboBox.DrawMode = DrawMode.OwnerDrawFixed;
                //pComboBox.DrawItem += new DrawItemEventHandler(pComboBox_DrawItem);

                pComboBox.ItemHeight = rect.Height - 6;
                pComboBox.Width = rect.Width;
                pComboBox.BackColor = selectedCell.BackColor;
                // ���������� ������ ����������
                if (selectedCell.PickList != null && selectedCell.PickList.Count>0)
                {
                    foreach (string s in selectedCell.PickList)
                        if (!string.IsNullOrEmpty(s))
                            pComboBox.Items.Add((string)s);
                    pComboBox.Text = selectedCell.Value.ToString();
                }
                else if (selectedCell.KeyPickList != null)
                {
                    ComboBoxDictionaryList<string, string> dicList = new ComboBoxDictionaryList<string, string>();
                    for (int i = 0; i < selectedCell.KeyPickList.Count; i++)
                    {
                        string key = selectedCell.KeyPickList.GetKeyByIndex(i);
                        string val = selectedCell.KeyPickList.GetValueByIndex(i);
                        dicList.Add(new ComboBoxDictionary<string, string>(key, val));
                    }
                    dicList.InitComboBox(pComboBox);
                    pComboBox.DataBindings.Add("SelectedValue", selectedCell, "Value", true);
                }
                if (selectedCell.CanEdit == false)
                {
                    pComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
                }
                else
                {
                    pComboBox.DropDownStyle = ComboBoxStyle.DropDown;
                }

                //if (!selectedCell.IsBinded())
                pComboBox.LostFocus += new EventHandler(DataValidate);

                pComboBox.Visible = true;
                pComboBox.FlatStyle = FlatStyle.Flat;
                this.Controls.Add(pComboBox);
                pComboBox.MouseDoubleClick += new MouseEventHandler(CellEditor_MouseDoubleClick);
                pComboBox.SelectedIndexChanged += new EventHandler(CellEditor_SelectIndexChanged);
                pComboBox.SelectAll();
            }
            else
            {// ����� �� ���������
                pMemo = new TextBox();

                pMemo.Multiline = true;
                pMemo.Dock = DockStyle.Fill;
                pMemo.Visible = true;
                pMemo.Enabled = true;
                if (gridCtrl.ReadOnly)
                    pMemo.ReadOnly = true;
                else
                    pMemo.ReadOnly = (selectedCell.CanEdit ? false : true);
                pMemo.BackColor = selectedCell.BackColor;
                pMemo.TextAlign = selectedCell.HorizontalAlignment;
                pMemo.Bounds = rect;
                pMemo.LostFocus += new EventHandler(DataValidate);
                pMemo.Text = selectedCell.Value.ToString();

                pMemo.BorderStyle = BorderStyle.FixedSingle;

                // ���������� ������ ����������

                this.Controls.Add(pMemo);
                pMemo.MouseDoubleClick += new MouseEventHandler(CellEditor_MouseDoubleClick);
                pMemo.Focus();
                pMemo.SelectAll();

            }

            this.KeyDown += new KeyEventHandler(CellEditor_KeyDown);
            this.GotFocus += new EventHandler(CellEditor_GotFocus);
            this.MouseDoubleClick += new MouseEventHandler(CellEditor_MouseDoubleClick);
        }

        void CellEditor_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.gridCtrl.Grid_MouseDoubleClick(sender, e);
        }

        void CellEditor_SelectIndexChanged(object sender, EventArgs e)
        {
            if (pComboBox.SelectedIndex == -1)
                pComboBox.Tag = 99999;
            //else
            //   pComboBox.Tag = null;
        }

        public void RefreshValueToTextBox()
        {
            if (selectedCell.cellStyle == EditStyle.esSimple || selectedCell.cellStyle == EditStyle.esEllipsis)
                pMemo.Text = selectedCell.Value.ToString();
            else if (selectedCell.cellStyle == EditStyle.esPickList)
                pComboBox.Text = selectedCell.Value.ToString();
            gridCtrl.InvalidateCell(selectedCell, true);
        }

        public void RefreshValueToCell()
        {
            if (selectedCell.cellStyle == EditStyle.esSimple || selectedCell.cellStyle == EditStyle.esEllipsis)
            {
                if (pMemo != null && pMemo.Enabled)
                    selectedCell.Value = pMemo.Text;
            }
            else if (selectedCell.cellStyle == EditStyle.esPickList)
            {
                if (pComboBox != null && pMemo.Enabled)
                    selectedCell.Value = pComboBox.Text;
            }
            gridCtrl.InvalidateCell(selectedCell, true);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Enter:
                    {
                        if (selectedCell.cellStyle == EditStyle.esEllipsis && !gridCtrl.ReadOnly)
                        {
                            string value = selectedCell.Value;
                            RefreshValueToCell();
                            this.gridCtrl.DataValidate(selectedCell);
                            this.pBitBtn.PerformClick();
                            if (this.selectedCell.Value != value)
                                this.RefreshValueToTextBox();//gridCtrl.RefreshSelectedCell(this.selectedCell);
                            return true;
                        }
                        break;
                    }
            }
            return base.ProcessDialogKey(keyData);
        }

        private void CellEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Escape
                || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up
                || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right
                || e.KeyCode == Keys.Tab)
            {
                EndEditing();
            }
        }

        /*private void pComboBox_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            //e.Bounds.Height = selectedCell.TextHeight + 3;
            Rectangle rect = e.Bounds;
            rect.Height = selectedCell.TextHeight + 3;
            rect.Y = e.Bounds.Y - (e.Bounds.Height - rect.Height) * e.Index;
            // Draw the background of the ListBox control for each item.
            e.DrawBackground();
            // Define the default color of the brush as black.
            //Brush myBrush = Brushes.Black;

            Font drawFont = new Font(selectedCell.FontName, selectedCell.TextHeight, selectedCell.FontStyle, GraphicsUnit.Pixel);
            SolidBrush drawBrush = new SolidBrush(selectedCell.TextColor);

            // Draw the current item text based on the current Font 
            // and the custom brush settings.
            e.Graphics.DrawString(pComboBox.Items[e.Index].ToString(),
                 drawFont, drawBrush, rect, StringFormat.GenericDefault);
            // If the ListBox has focus, draw a focus rectangle around the selected item.
            e.DrawFocusRectangle();
        }*/


        private void CellEditor_GotFocus(object sender, EventArgs e)
        {
            //base.Focus();
            if (selectedCell.cellStyle == EditStyle.esSimple || gridCtrl.ReadOnly)
                pMemo.Show();
            else if (selectedCell.cellStyle == EditStyle.esPickList)
                pComboBox.Show();
            else
            {
                pBitBtn.Show();
                pMemo.Show();
            }
        }

        private void EndEditing()
        {
            this.Controls.Clear();
            gridCtrl.Controls.Remove(this);
            selectedCell.Value = this.Text;
        }

        private void DataValidate(object sender, System.EventArgs e)
        {
            if (selectedCell.cellStyle == EditStyle.esSimple || gridCtrl.ReadOnly)
                selectedCell.Value = pMemo.Text;
            else if (selectedCell.cellStyle == EditStyle.esPickList && pComboBox != null)
            {
                if (pComboBox.Tag == null)// || pComboBox.Tag.ToString() != "99999")
                    selectedCell.Value = pComboBox.Text;
            }
            else
            {
                selectedCell.Value = pMemo.Text;
            }
            Invalidate();
            if (!selectedCell.IsBinded())
                gridCtrl.DataValidate(selectedCell);
        }

        private void pBitBtn_Click(object sender, System.EventArgs e)
        {
            gridCtrl.CellButtonClick(selectedCell);
        }

        public void FinishEditing()
        {
            if (!gridCtrl.ReadOnly)
            {
                if (selectedCell.cellStyle == EditStyle.esSimple)
                    selectedCell.Value = pMemo.Text;
                else if (selectedCell.cellStyle == EditStyle.esPickList)
                {
                    selectedCell.Value = pComboBox.Text;
                }
                else
                {
                    selectedCell.Value = pMemo.Text;
                }
            }
        }

        public void SetFocus()
        {
            if (!gridCtrl.ReadOnly)
            {
                if (selectedCell.cellStyle == EditStyle.esSimple)
                {
                    pMemo.Focus();
                    pMemo.SelectAll();
                }
                else if (selectedCell.cellStyle == EditStyle.esPickList)
                {
                    pComboBox.Focus();
                    pComboBox.SelectAll();
                }
                else
                {
                    pMemo.Focus();
                    pMemo.SelectAll();
                }
            }
        }

    }
}
