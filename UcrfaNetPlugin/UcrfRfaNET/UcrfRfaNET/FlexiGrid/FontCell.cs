﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
//using System.Windows;

namespace GridCtrl
{
	class FontCell
	{ 
		public Color    TColor = SystemColors.WindowText;   //Цвет текста
		public int FSize = 10;    //Размер шрифта
		public string Name = "";    //Название шрифта
		public FontStyle Style = FontStyle.Regular;   //Стиль шрифта
	}
}
