﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows;

namespace GridCtrl
{
	class SubColumn
	{
		public string ValueSubCol;  //Значение ячейки
		public string KeySubCol;    //Ключ ячейки
		public ItemProp pItemProp;    //Стиль ячейки
		public Color BGColor;        //Цвет фона
		public FontCell CFont;         //Шрифт ячейки
		public int TabOrder;     //Номер выбираемого элемента при переходе Tab
		public bool ReadOnly;     //Флаг "Только чтение"
		// Конструктор

		public SubColumn()
		{
			ValueSubCol = "";
			KeySubCol="";
			BGColor = Color.White;
			CFont = new FontCell();
	      CFont.TColor = System.Drawing.SystemColors.WindowText;
		   CFont.Name = "MS Sans Serif";
			CFont.FSize = 8;
			TabOrder = -1; //Элемент не выбирается     
		}
	}
}