﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using XICSM.UcrfRfaNET.HelpClasses;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET
{
    class AllStationsCsvExport
    {
        public const int Max = 60000;

        private DateTime _exportTime;
        private int fileCount = 0;

        public AllStationsCsvExport()
        {
            _exportTime = DateTime.Now;
        }

        
        private string GetFilePath(int fileCountParam)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            string filename = "ICS_ЦОБ_УДЦР " + _exportTime.Year.ToString("D4") + "." +
                              _exportTime.Month.ToString("D2") + "." + _exportTime.Day.ToString("D2") + " " +
                              _exportTime.Hour.ToString("D2") + "." + _exportTime.Minute.ToString("D2") + "." +
                              _exportTime.Second.ToString("D2");

            if (fileCountParam > 0)
                filename = filename + "_" + fileCountParam.ToString() + ".csv";
            else
                filename = filename + ".csv";
            
            return path + "\\" + filename;            
        }

        public void Export(IMQueryMenuNode.Context context, CProgressBar pBar)
        {
            StringBuilder headerLine = new StringBuilder();

            headerLine.Append("Назва радіотехнології");
            headerLine.Append(";");
            headerLine.Append("Контрагент");
            headerLine.Append(";");
            headerLine.Append("Код ЄДРПОУ");
            headerLine.Append(";");
            headerLine.Append("Повна адреса місця встановлення");
            headerLine.Append(";");
            headerLine.Append("Назва населеного пункту");
            headerLine.Append(";");
            headerLine.Append("Район");
            headerLine.Append(";");
            headerLine.Append("Область");
            headerLine.Append(";");
            headerLine.Append("Широта");
            headerLine.Append(";");
            headerLine.Append("Довгота");
            headerLine.Append(";");
            headerLine.Append("Назва/Тип РЕЗ");
            headerLine.Append(";");
            headerLine.Append("Потужність передавача");
            headerLine.Append(";");
            headerLine.Append("Клас випромінювання");
            headerLine.Append(";");
            headerLine.Append("Клас випромінювання, звук");
            headerLine.Append(";");
            headerLine.Append("Тип антени");
            headerLine.Append(";");
            headerLine.Append("Азимут макс. випромінювання, град");
            headerLine.Append(";");
            headerLine.Append("Висота антени над рівнем землі");
            headerLine.Append(";");
            headerLine.Append("Тип поляризації");
            headerLine.Append(";");
            headerLine.Append("Номінали / смуги частот приймання, МГц");
            headerLine.Append(";");
            headerLine.Append("Номінали / смуги частот передавання, МГц");
            headerLine.Append(";");
            headerLine.Append("Номер Висновку щодо ЕМС");
            headerLine.Append(";");
            headerLine.Append("Дата видачі Висновку щодо ЕМС");
            headerLine.Append(";");
            headerLine.Append("Дійсний до Висновок щодо ЕМС");
            headerLine.Append(";");
            headerLine.Append("Номер Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Дата видачі Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Термін дії Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Номер старого Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Дата видачі старого Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Дата анулювання Дозволу на експлуатацію");
            headerLine.Append(";");
            headerLine.Append("Номер ліцензії");
            headerLine.Append(";");
            headerLine.Append("Назва супутникової мережі");
            headerLine.Append(";");
            headerLine.Append("Кількість абонентських РЕЗ");
            headerLine.Append(";");
            headerLine.Append("Нижня частота передачі");
            headerLine.Append(";");
            headerLine.Append("Нижня частота прийому");
            headerLine.Append(";");
            headerLine.Append("Верхня частота передачі");
            headerLine.Append(";");
            headerLine.Append("Верхня частота прийому");
            headerLine.Append(";");
            headerLine.Append("Статус частотного присвоєння");            

            IMRecordset rs = new IMRecordset(context.TableName, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,STANDARD,OWNER_NAME,CODE_EDRPU,ADDRESS,CITY,SUBPROVINCE,PROVINCE,LATITUDE,LONGITUDE,EQUIP_NAME,POWER,DES_EM,DES_EM_SOUND,ANT_NAME,AZIMUTH_TXT,ALTITUDE,POLARIZATION,RX_FREQ_TXT,TX_FREQ_TXT,CONCLUS_NUM,CONCLUS_DATE,CONCLUS_DATE_STOP,PERM_NUM,PERM_DATE,PERM_DATE_STOP,PERM_NUM_OLD,PERM_DATE_OLD,PERM_DATE_STOP_OLD,LICENCE_NUM,SAT_NAME,ABONENT_COUNT,TX_LOW_FREQ,RX_LOW_FREQ,TX_HIGH_FREQ,RX_HIGH_FREQ,STATUS");            
            rs.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);

            Dictionary<string, string> standard = EriFiles.GetEriCodeAndDescr("TRFAApplStandard");
            Dictionary<string, string> polarization = EriFiles.GetEriCodeAndDescr("ANT_POLAR");
            Dictionary<string, string> status = EriFiles.GetEriCodeAndDescr("TRFAStatus");

            Power power = new Power();
            StreamWriter sw = null;
            try
            {                
                fileCount = 0;
                int count = 0;

                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    if (count == 0)
                    {                        
                        if (fileCount == 1)
                        {
                            string file1 = GetFilePath(0);
                            string file2 = GetFilePath(1);

                            File.Move(file1, file2);

                            fileCount++;
                        }

                        sw = new StreamWriter(GetFilePath(fileCount), true, Encoding.Default);
                        sw.WriteLine(headerLine.ToString());
                    }

                    StringBuilder csvString = new StringBuilder();

                    string standardValue = rs.GetS("STANDARD");

                    if (standard.ContainsKey(standardValue))
                        standardValue = standard[standardValue];

                    csvString.Append(standardValue);

                    csvString.Append(";");
                    csvString.Append(rs.Get("OWNER_NAME"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("CODE_EDRPU"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("ADDRESS"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("CITY"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("SUBPROVINCE"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("PROVINCE"));
                    csvString.Append(";");
                    csvString.Append(HelpFunction.DmsToString(rs.GetD("LATITUDE").DecToDms(), EnumCoordLine.Lat));
                    csvString.Append(";");
                    csvString.Append(HelpFunction.DmsToString(rs.GetD("LONGITUDE").DecToDms(), EnumCoordLine.Lon));
                    csvString.Append(";");
                    csvString.Append(rs.Get("EQUIP_NAME"));
                    csvString.Append(";");
                    power[PowerUnits.dBm] = rs.GetD("POWER");
                    csvString.Append(power[PowerUnits.W].ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(rs.Get("DES_EM"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("DES_EM_SOUND"));
                    csvString.Append(";");
                    csvString.Append(rs.Get("ANT_NAME"));
                    csvString.Append(";");
                    csvString.Append(EncodeData(rs.Get("AZIMUTH_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(rs.GetD("ALTITUDE").ToStringNullD());
                    csvString.Append(";");

                    string polarizationValue = rs.GetS("POLARIZATION");

                    if (polarization.ContainsKey(polarizationValue))
                        polarizationValue = polarization[polarizationValue];

                    csvString.Append(polarizationValue);
                    
                    csvString.Append(";");
                    csvString.Append(EncodeData(rs.Get("RX_FREQ_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(EncodeData(rs.Get("TX_FREQ_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(rs.Get("CONCLUS_NUM"));
                    csvString.Append(";");
                    csvString.Append(rs.GetT("CONCLUS_DATE").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(rs.GetT("CONCLUS_DATE_STOP").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(rs.Get("PERM_NUM"));
                    csvString.Append(";");
                    csvString.Append(rs.GetT("PERM_DATE").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(rs.GetT("PERM_DATE_STOP").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(rs.GetS("PERM_NUM_OLD"));
                    csvString.Append(";");
                    csvString.Append(rs.GetT("PERM_DATE_OLD").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(rs.GetT("PERM_DATE_STOP_OLD").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(EncodeData(rs.Get("LICENCE_NUM").ToString()));
                    csvString.Append(";");
                    csvString.Append(rs.Get("SAT_NAME"));
                    csvString.Append(";");
                    csvString.Append(rs.GetD("ABONENT_COUNT").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(rs.GetD("TX_LOW_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(rs.GetD("RX_LOW_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(rs.GetD("TX_HIGH_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(rs.GetD("RX_HIGH_FREQ").ToStringNullD());
                    csvString.Append(";");

                    string statusValue = rs.GetS("STATUS");

                    if (status.ContainsKey(statusValue))
                        statusValue = status[statusValue];

                    csvString.Append(statusValue);
                    
                    sw.WriteLine(csvString);

                    pBar.ShowSmall(rs.GetI("ID"));
                    //pBar.ShowProgress(count2, countAll);
                    pBar.UserCanceled();

                    count++;
                    if (count >= AllStationsCsvExport.Max)
                    {
                        fileCount++;
                        sw.Close();
                        sw.Dispose();
                        count = 0;
                        sw = null;
                    }
                }                    
            }

            finally
            {
                if (rs.IsOpen())
                    rs.Close();

                rs.Destroy();                    
            }

            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }            
        }

        /*public void Export(List<int> idList)
        {                                                            
            IMRecordset r = new IMRecordset(PlugTbl.AllStations, IMRecordset.Mode.ReadOnly);
            StreamWriter sw = null;

            try
            {
                int count = 0;

                r.Select(
                    "ID,STANDARD,OWNER_NAME,CODE_EDRPU,ADDRESS,CITY,SUBPROVINCE,PROVINCE,LATITUDE,LONGITUDE,EQUIP_NAME,POWER,DES_EM,DES_EM_SOUND,ANT_NAME,AZIMUTH_TXT,ALTITUDE,POLARIZATION,RX_FREQ_TXT,TX_FREQ_TXT,CONCLUS_NUM,CONCLUS_DATE,CONCLUS_DATE_STOP,PERM_NUM,PERM_DATE,PERM_DATE_STOP,PERM_NUM_OLD,PERM_DATE_OLD,PERM_DATE_STOP_OLD,LICENCE_NUM,SAT_NAME,ABONENT_COUNT,TX_LOW_FREQ,RX_LOW_FREQ,TX_HIGH_FREQ,RX_HIGH_FREQ,STATUS");
                //r.SetWhere("ID", IMRecordset.Operation.Eq, Id);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    if (fileCount == 1)
                    {
                        File.Move(GetFilePath(0), GetFilePath(1));
                        fileCount++;
                    }

                    sw = new StreamWriter(GetFilePath(fileCount), true, Encoding.Default);
                    fileCount++;

                    StringBuilder csvString = new StringBuilder();
                    
                    csvString.Append(r.Get("STANDARD"));
                    csvString.Append(";");
                    csvString.Append(r.Get("OWNER_NAME"));
                    csvString.Append(";");
                    csvString.Append(r.Get("CODE_EDRPU"));
                    csvString.Append(";");
                    csvString.Append(r.Get("ADDRESS"));
                    csvString.Append(";");
                    csvString.Append(r.Get("CITY"));
                    csvString.Append(";");
                    csvString.Append(r.Get("SUBPROVINCE"));
                    csvString.Append(";");
                    csvString.Append(r.Get("PROVINCE"));
                    csvString.Append(";");
                    csvString.Append(HelpFunction.DmsToString(r.GetD("LATITUDE").DecToDms(), EnumCoordLine.Lat));
                    csvString.Append(";");
                    csvString.Append(HelpFunction.DmsToString(r.GetD("LONGITUDE").DecToDms(), EnumCoordLine.Lon));
                    csvString.Append(";");
                    csvString.Append(r.Get("EQUIP_NAME"));
                    csvString.Append(";");
                    csvString.Append(r.GetD("POWER").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.Get("DES_EM"));
                    csvString.Append(";");
                    csvString.Append(r.Get("DES_EM_SOUND"));
                    csvString.Append(";");
                    csvString.Append(r.Get("ANT_NAME"));
                    csvString.Append(";");
                    csvString.Append(EncodeData(r.Get("AZIMUTH_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(r.GetD("ALTITUDE").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.Get("POLARIZATION"));
                    csvString.Append(";");
                    csvString.Append(EncodeData(r.Get("RX_FREQ_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(EncodeData(r.Get("TX_FREQ_TXT").ToString()));
                    csvString.Append(";");
                    csvString.Append(r.Get("CONCLUS_NUM"));
                    csvString.Append(";");
                    csvString.Append(r.GetT("CONCLUS_DATE").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(r.GetT("CONCLUS_DATE_STOP").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(r.Get("PERM_NUM"));
                    csvString.Append(";");
                    csvString.Append(r.GetT("PERM_DATE").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(r.GetT("PERM_DATE_STOP").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(r.GetS("PERM_NUM_OLD"));
                    csvString.Append(";");
                    csvString.Append(r.GetT("PERM_DATE_OLD").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(r.GetT("PERM_DATE_STOP_OLD").ToShortDateStringNullT());
                    csvString.Append(";");
                    csvString.Append(EncodeData(r.Get("LICENCE_NUM").ToString()));
                    csvString.Append(";");
                    csvString.Append(r.Get("SAT_NAME"));
                    csvString.Append(";");
                    csvString.Append(r.GetD("ABONENT_COUNT").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.GetD("TX_LOW_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.GetD("RX_LOW_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.GetD("TX_HIGH_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.GetD("RX_HIGH_FREQ").ToStringNullD());
                    csvString.Append(";");
                    csvString.Append(r.Get("STATUS"));

                    sw.WriteLine(csvString);

                    if (count >= AllStationsCsvExport.Max)
                    {
                        sw.Close();                        
                        sw.Dispose();
                        count = 0;
                        sw = null;
                    }
                }                    
            }

            finally
            {
                if (r.IsOpen())
                    r.Close();

                r.Destroy();                    
            }

            if (sw!=null)
            {
                sw.Close();
                sw.Dispose();
            }
        }*/

        private string EncodeData(string encoded)
        {
            string encodedString = encoded.Replace(";", " | ");
            int len = encodedString.Length;
            if (len>3 && encodedString.Substring(encodedString.Length - 3, 3) == " | ")
            {
                encodedString = encodedString.Substring(0, encodedString.Length - 3);
            }
            return encodedString;
        }
    } 
}
