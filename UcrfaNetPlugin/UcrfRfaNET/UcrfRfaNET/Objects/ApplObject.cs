﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.UtilityClass;

namespace XICSM.UcrfRfaNET.Objects
{
    /// <summary>
    /// Класс для работы с объектом заявки
    /// </summary>
    internal class ApplObject : NotifyPropertyChanged
    {
        private const string TableName = PlugTbl.APPL;
        private string _changeFilePos = "";
        private string _changeDocNum = "";
        private string _changeProvince = "";
        private DateTime _changeDocDate = DateTime.Now;
        /// <summary>
        /// ID записи
        /// </summary>
        public int Id { get; set; }
        // Для изменения области мониторинга
        /// <summary>
        /// Путь к файлу изменения филии мониторинга
        /// </summary>
        public string ChangeFilePos
        {
            get { return _changeFilePos;}
            set
            {
                if(_changeFilePos != value)
                {
                    _changeFilePos = value;
                    InvokeNotifyPropertyChanged("ChangeFilePos");
                }
            }
        }
        /// <summary>
        /// Номер документа изменения филии мониторинга
        /// </summary>
        public string ChangeDocNum
        {
            get { return _changeDocNum; }
            set
            {
                if(_changeDocNum != value)
                {
                    _changeDocNum = value;
                    InvokeNotifyPropertyChanged("ChangeDocNum");
                }
            }
        }
        /// <summary>
        /// Название области, которая должна мониторить станцию
        /// </summary>
        public string ChangeProvince
        {
            get { return _changeProvince; }
            set
            {
                if (_changeProvince != value)
                {
                    _changeProvince = value;
                    InvokeNotifyPropertyChanged("ChangeProvince");
                }
            }
        }
        /// <summary>
        /// Дата прикрепления документа
        /// </summary>
        public DateTime ChangeDocDate
        {
            get { return _changeDocDate; }
            set
            {
                if ( _changeDocDate != value)
                {
                    _changeDocDate = value;
                    InvokeNotifyPropertyChanged("ChangeDocDate");
                }
            }
        }
        //-------------------------------------------------
        /// <summary>
        /// Конструктор (автоматически загружает данные)
        /// </summary>
        /// <param name="id">ID заявки. Для создания новой заявеки передайте 0 или NULLI</param>
        public ApplObject(int id)
        {
            Id = id;
            Load();
        }
        /// <summary>
        /// Загружает данные заявки
        /// </summary>
        public void Load()
        {
            IMRecordset rsAppl = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,CHANGE_FILE_POS,CHANGE_DOC_NUM,CHANGE_PROVINCE,CHANGE_DOC_DATE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            try
            {
                rsAppl.Open();
                if(!rsAppl.IsEOF())
                {
                    ChangeFilePos = rsAppl.GetS("CHANGE_FILE_POS");
                    ChangeDocNum = rsAppl.GetS("CHANGE_DOC_NUM");
                    ChangeProvince = rsAppl.GetS("CHANGE_PROVINCE");
                    ChangeDocDate = rsAppl.GetT("CHANGE_DOC_DATE");
                }
            }
            finally
            {
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
        }
        /// <summary>
        /// Сохраняет данные заявки
        /// </summary>
        public void Save()
        {
            IMRecordset rsAppl = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            rsAppl.Select("ID,CHANGE_FILE_POS,CHANGE_DOC_NUM,CHANGE_PROVINCE,CHANGE_DOC_DATE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            try
            {
                rsAppl.Open();
                if (rsAppl.IsEOF())
                {
                    rsAppl.AddNew();
                    Id = IM.AllocID(TableName, 1, -1);
                    rsAppl.Put("ID", Id);
                }
                else
                {
                    rsAppl.Edit();
                }
                rsAppl.Put("CHANGE_FILE_POS", ChangeFilePos);
                rsAppl.Put("CHANGE_DOC_NUM", ChangeDocNum);
                rsAppl.Put("CHANGE_PROVINCE", ChangeProvince);
                rsAppl.Put("CHANGE_DOC_DATE", ChangeDocDate);
                rsAppl.Update();
            }
            finally
            {
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
        }
    }
}
