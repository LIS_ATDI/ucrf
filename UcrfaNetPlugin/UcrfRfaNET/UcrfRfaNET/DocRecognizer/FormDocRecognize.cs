﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using DocRecognizer;
using DocRecognizer.Interface;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.DocRecognizer
{
    public partial class FormDocRecognize : Form
    {
        private const string DocFilter = "*";
        /// <summary>
        /// Конструктор
        /// </summary>
        public FormDocRecognize()
        {
            InitializeComponent();
            this.Visible = false;
        }
        /// <summary>
        /// Загружаем форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormDocRecognize_Load(object sender, EventArgs e)
        {
            using (IDocRecognizer tmp = Initialization.CreateDocRecognizer())
            {
                tmp.Init(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\DocRecognizer");

                using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading documents...")))
                {
                    DateTime startDate = DateTime.Now.AddDays(-PluginSetting.PluginFolderSetting.MaxDaysDocumentRecognizer).Date;
                    const int maxVal = 100000;
                    int index = 1; int MaxCountScanDoc = 0;
                    {
                        IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                        rsDocFiles.Select("ID,MEMO,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,DATE_CREATED,CREATED_BY");
                        rsDocFiles.SetWhere("DOC_TYPE", IMRecordset.Operation.Like, DocFilter);
                        rsDocFiles.SetWhere("DATE_CREATED", IMRecordset.Operation.Ge, startDate);
                        rsDocFiles.SetWhere("DOC_REF", IMRecordset.Operation.NotNull, "");
                        rsDocFiles.SetWhere("DOC_DATE", IMRecordset.Operation.NotNull, IM.NullT);
                        try
                        {
                            for (rsDocFiles.Open(); !rsDocFiles.IsEOF(); rsDocFiles.MoveNext())
                            {
                                if (pb.UserCanceled())
                                    break;
                                DocData data = new DocData();
                                data.DocNumber = rsDocFiles.GetS("DOC_REF");
                                data.BlankNum = rsDocFiles.GetS("MEMO");
                                data.PrintDate = rsDocFiles.GetT("DOC_DATE");
                                data.EndDate = rsDocFiles.GetT("EXP_DATE");
                                tmp.LoadDocuments(rsDocFiles.GetS("DOC_TYPE"), data);
                                pb.SetProgress(index, maxVal);
                                pb.SetSmall(index++);
                                MaxCountScanDoc++;
                            }
                        }
                        finally
                        {
                            rsDocFiles.Final();
                        }

                    }
                    {
                        IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                        rsDocFiles.Select("ID,MEMO,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,DATE_CREATED,CREATED_BY");
                        rsDocFiles.SetWhere("DOC_TYPE", IMRecordset.Operation.Like, "*_S");
                        rsDocFiles.SetWhere("DATE_CREATED", IMRecordset.Operation.Ge, startDate);
                        rsDocFiles.SetWhere("DOC_REF", IMRecordset.Operation.NotNull, "");
                        try
                        {
                            for (rsDocFiles.Open(); !rsDocFiles.IsEOF(); rsDocFiles.MoveNext())
                            {
                                if (pb.UserCanceled())
                                    break;
                                DocData data = new DocData();
                                data.DocNumber = rsDocFiles.GetS("DOC_REF");
                                data.BlankNum = rsDocFiles.GetS("MEMO");
                                data.PrintDate = rsDocFiles.GetT("DOC_DATE");
                                tmp.LoadScanedDocuments(rsDocFiles.GetS("DOC_TYPE").Replace("_S", ""), data);
                                pb.SetProgress(index, maxVal);
                                pb.SetSmall(index++);
                                MaxCountScanDoc++;
                            }
                        }
                        finally
                        {
                            rsDocFiles.Final();
                        }
                    }
                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Кількість документів, які було зчитано з таблиці DOCFILES - {0} ", MaxCountScanDoc));
                }
                tmp.CommitScanCopy += new CommitScanCopyDelegate(CommitScanCopy);
                IRecognizeWindow wpfForm = tmp.GetRecognizeWindow();
                ElementHost.EnableModelessKeyboardInterop(wpfForm.GetWindow());
                wpfForm.ShowDialog();
                tmp.CommitScanCopy -= new CommitScanCopyDelegate(CommitScanCopy);
                wpfForm = null;
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        /// <summary>
        /// Закомитить документы
        /// </summary>
        /// <param name="param"></param>
        void CommitScanCopy(params ICommitScanData[] param)
        {
            foreach (ICommitScanData scanData in param)
            {
                try
                {
                    //MessageBox.Show(string.Format("Успішно розпізнаних документів - {0}. Документів, що не було розпізнано - {1}", Cnt_SuccessRecognize, listDoc.Count - Cnt_SuccessRecognize));
                    CLogs.WriteInfo(ELogsWhat.ScanDoc, "Початок процедури під'єднання сканкопій");
                    IMTransaction.Begin();
                    int idDocFiles = IM.NullI;
                    IMRecordset rsDocLinks = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
                    rsDocLinks.Select("Document.DOC_TYPE,Document.DOC_REF,REC_ID,REC_TAB,Document.PATH");
                    rsDocLinks.SetWhere("Document.DOC_REF", IMRecordset.Operation.Like, scanData.Number);
                    rsDocLinks.SetWhere("Document.DOC_TYPE", IMRecordset.Operation.Like, DocFilter);
                    try
                    {
                        string fullFileName = "";
                        for (rsDocLinks.Open(); !rsDocLinks.IsEOF(); rsDocLinks.MoveNext())
                        {
                            bool isSendApplEvent = false;
                            if (idDocFiles == IM.NullI)
                            {
                                string docFolder = System.IO.Path.GetDirectoryName(rsDocLinks.GetS("Document.PATH"));
                                string folder = PluginSetting.PluginFolderSetting.FolderDocumentRecognizer;
                                if (string.IsNullOrEmpty(folder))
                                {
                                    folder = docFolder;
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Аналіз можливості використання директорії  - {0}", folder));
                                }
                                else
                                {
                                    folder = folder.Replace("%DOC_FOLDER%", docFolder);
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Аналіз можливості використання директорії  - {0}", folder));
                                }
                                CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Спроба запису сканкопії в директорію  - {0}", folder));
                                if (string.IsNullOrEmpty(folder))
                                    folder = ".";
                                else if (System.IO.Directory.Exists(folder) == false)
                                {
                                    System.IO.Directory.CreateDirectory(folder);
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Створено нову директорію - {0}", folder));
                                }
                                string newFileName = string.Format("{0}\\{1}{2}", folder, scanData.Number,
                                                                   System.IO.Path.GetExtension(scanData.ScanFileName));
                                if (System.IO.File.Exists(scanData.ScanFileName))
                                {
                                    System.IO.File.Copy(scanData.ScanFileName, newFileName, true);
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Файл - {0} було скопійовоно в  {1}", scanData.ScanFileName, newFileName));
                                }
                                else
                                {
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Файл - {0} не було скопійовоно в  {1}", scanData.ScanFileName, newFileName));
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Не знайдено Файл - {0} ", scanData.ScanFileName));
                                    //throw new Exception(string.Format("Can't find file '{0}'", scanData.ScanFileName));
                                }

                                IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadWrite);
                                rsDocFiles.Select("ID,NAME,MEMO,PATH,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,DATE_CREATED,CREATED_BY");
                                rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                                try
                                {
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Спроба створення нового запису  в таблиці  {0}", ICSMTbl.DocFiles));
                                    idDocFiles = IM.AllocID(ICSMTbl.DocFiles, 1, -1);
                                    rsDocFiles.Open();
                                    rsDocFiles.AddNew();
                                    rsDocFiles.Put("ID", idDocFiles);
                                    rsDocFiles.Put("NAME", "");
                                    rsDocFiles.Put("MEMO", scanData.BlankNumber);
                                    rsDocFiles.Put("PATH", newFileName);
                                    rsDocFiles.Put("DOC_TYPE", rsDocLinks.GetS("Document.DOC_TYPE") + "_S");
                                    rsDocFiles.Put("DOC_REF", scanData.Number);
                                    rsDocFiles.Put("DOC_DATE", scanData.DateOut);
                                    rsDocFiles.Put("EXP_DATE", scanData.DateEnd);
                                    rsDocFiles.Put("DATE_CREATED", DateTime.Now);
                                    rsDocFiles.Put("CREATED_BY", IM.ConnectedUser());
                                    rsDocFiles.Update();
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Створено новий запис - {0}  в таблиці  {1}", idDocFiles, ICSMTbl.DocFiles));

                                }
                                catch (Exception ex)
                                {
                                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Помилка створення нового запису  в таблиці  {0} {1}", ICSMTbl.DocFiles, ex.Message));
                                }
                                finally
                                {
                                    rsDocFiles.Final();
                                }
                                fullFileName = newFileName;
                            }
                            //----
                            // Копируем сылку на документы
                            string recName = rsDocLinks.GetS("REC_TAB");
                            int recId = rsDocLinks.GetI("REC_ID");
                            IMRecordset rsDocLinkCopy = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                            rsDocLinkCopy.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                            rsDocLinkCopy.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                            try
                            {
                                CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Спроба створення нового запису  в таблиці  {0}", ICSMTbl.itblDocLink));
                                rsDocLinkCopy.Open();
                                rsDocLinkCopy.AddNew();
                                rsDocLinkCopy.Put("DOC_ID", idDocFiles);
                                rsDocLinkCopy.Put("REC_ID", recId);
                                rsDocLinkCopy.Put("REC_TAB", recName);
                                rsDocLinkCopy.Put("NAME", "");
                                rsDocLinkCopy.Put("PATH", "");
                                rsDocLinkCopy.Update();
                                CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Створено новий запис REC_ID - {0} REC_TAB - {1} в таблиці  - {2}", recId, recName, ICSMTbl.itblDocLink));
                            }
                            catch (Exception ex)
                            {
                                CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Помилка створення нового запису  в таблиці  {0} {1}", ICSMTbl.itblDocLink, ex.Message));
                            }
                            finally
                            {
                                rsDocLinkCopy.Final();
                            }
                            if (recName == PlugTbl.APPL)
                            {
                                DateTime DateOut = IM.NullT;
                                IMRecordset rsAPPL = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                                rsAPPL.Select("ID,DOZV_DATE_FROM");
                                rsAPPL.SetWhere("ID", IMRecordset.Operation.Eq, recId);
                                try
                                {
                                    rsAPPL.Open();
                                    if (!rsAPPL.IsEOF())
                                    {
                                        DateOut = rsAPPL.GetT("DOZV_DATE_FROM");
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                finally
                                {
                                    rsAPPL.Final();
                                }

                                CEventLog.AddApplEvent(recId, EDocEvent.evAttachScan, DateOut, //scanData.DateOut,
                                                       scanData.Number, scanData.DateEnd, fullFileName);
                                isSendApplEvent = true;
                            }
                            if (isSendApplEvent) CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Запис в журнал подій відносно прикріплення сканкопії {0} успішно додано.", fullFileName));
                            if (!isSendApplEvent) CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Запис в журнал подій відносно прикріплення сканкопії {0} не додано.", fullFileName));
                        }

                        CLogs.WriteInfo(ELogsWhat.ScanDoc, "Кінець процедури під'єднання сканкопій");
                    }
                    catch (Exception ex)
                    {
                        CLogs.WriteInfo(ELogsWhat.ScanDoc, "Помилка: " + ex.Message);
                    }
                    finally
                    {
                        rsDocLinks.Final();
                    }
                    IMTransaction.Commit();
                    scanData.NeedDeleteScanFile = true;
                    scanData.CommitIsOk = true;
                }
                catch (Exception ex)
                {
                    CLogs.WriteInfo(ELogsWhat.ScanDoc, ex.Message);
                    IMTransaction.Rollback();
                    scanData.NeedDeleteScanFile = false;
                    scanData.ErrorMessage = ex.Message;
                }
            }
        }
    }
}
