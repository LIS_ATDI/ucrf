﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.Logs
{
    /// <summary>
    /// Класс описывает строку для отображения данных изменения
    /// </summary>
    internal class ChangeLogRow : TreeColumnRowBase
    {
        [TreeColumn("ID")]
        public IntElemCell Id { get; set; }

        [TreeColumn("Station ID")]
        public IntElemCell StationId { get; set; }

        [TreeColumn("Station table")]
        public StringElemCell StationTable { get; set; }

        [TreeColumn("Record ID")]
        public IntElemCell RecordId { get; set; }

        [TreeColumn("Record table")]
        public StringElemCell RecordTable { get; set; }

        [TreeColumn("Operation type")]
        public StringElemCell OperationType { get; set; }

        [TreeColumn("Value old")]
        public StringElemCell ValueOld { get; set; }

        [TreeColumn("Value new")]
        public StringElemCell ValueNew { get; set; }

        [TreeColumn("Value type")]
        public StringElemCell ValueType { get; set; }

        [TreeColumn("Created date")]
        public DateTimeElemCell DateCreated { get; set; }

        //===============================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public ChangeLogRow()
        {
            Id = new IntElemCell();
            StationId = new IntElemCell();
            StationTable = new StringElemCell();
            RecordId = new IntElemCell();
            RecordTable = new StringElemCell();
            OperationType = new StringElemCell();
            ValueOld = new StringElemCell();
            ValueNew = new StringElemCell();
            ValueType = new StringElemCell();
            DateCreated = new DateTimeElemCell("dd.MM.yyyy hh:mm:ss");
        }
    }
}
