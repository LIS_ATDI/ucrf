﻿using System.Windows.Forms;
using XICSM.UcrfRfaNET.Calculation;

namespace XICSM.UcrfRfaNET.Logs
{
    public partial class ChangeMessageForm : Form
    {
        private readonly ChangeMessage _changeMsgObj;
        private readonly TabsTreeColumn _tabsTree;
        public TabsTreeColumn TabsTree { get { return _tabsTree; } }
        public ChangeMessageForm(int recId)
        {
            InitializeComponent();
            //-----
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Change message form")))
            {
                const int maxCount = 5;
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetProgress(1, maxCount);
                _changeMsgObj = new ChangeMessage(recId);
                _tabsTree = new TabsTreeColumn();
                panelGrid.Controls.Add(TabsTree);
                TabsTree.Dock = DockStyle.Fill;
                //-----
                pb.SetProgress(2, maxCount);
                Text = string.Format("ID:{0}", _changeMsgObj.Id);
                tbMessage.DataBindings.Add("Text", _changeMsgObj, "Message");
                TabsTree.AddNewTab("Log", _changeMsgObj.LogRow, "ChangeLogRow");
                //----
                pb.SetProgress(3, maxCount);
                tsIdObj.Text = string.Format("{0}", _changeMsgObj.ObjId);
                tsTableObj.Text = string.Format("{0}", _changeMsgObj.ObjTable);
                tsUser.Text = string.Format("{0} ({1})", CUsers.GetUserFio(_changeMsgObj.EmplId),
                                            _changeMsgObj.CreatedDate.ToString("dd.MM.yyyy hh:mm:ss"));
            }
        }
        /// <summary>
        /// Отображает форму "ChangeMessageForm"
        /// </summary>
        /// <param name="recId">ID записи</param>
        public static void Show(int recId)
        {
            using (ChangeMessageForm frm = new ChangeMessageForm(recId))
            {
                frm.ShowDialog();
            }
        }
    }
}
