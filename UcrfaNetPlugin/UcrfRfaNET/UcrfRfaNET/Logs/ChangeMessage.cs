﻿using System;
using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.Logs
{
    class ChangeMessage
    {
        public int Id { get; protected set; }
        public int EmplId { get; protected set; }
        public int ObjId { get; protected set; }
        public string ObjTable { get; protected set; }
        public DateTime CreatedDate { get; protected set; }
        public string Message { get; protected set; }
        public List<ChangeLogRow> LogRow { get; protected set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="idRecord">ID записи</param>
        public ChangeMessage(int idRecord)
        {
            LogRow = new List<ChangeLogRow>();
            Id = idRecord;
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Change message form")))
            {
                Dictionary<string, string> tableName = HelpClasses.EriFiles.GetEriCodeAndDescr("ALL_STATIONS");
                Dictionary<string, string> fieldName = HelpClasses.EriFiles.GetEriCodeAndDescr("ALL_FIELDS");
                Dictionary<string, string> operationName = HelpClasses.EriFiles.GetEriCodeAndDescr("OperationType");
                const int maxCount = 100;
                pb.SetBig(CLocaliz.TxT("Loading data..."));
                pb.SetProgress(0, maxCount);
                using(Icsm.LisRecordSet rsMess = new Icsm.LisRecordSet(PlugTbl.XnChangeMsg, IMRecordset.Mode.ReadOnly))
                {
                    rsMess.Select("ID,OBJ_ID,OBJ_TABLE,DATE_CREATED,EMPLOYEE_ID,MESSAGE");
                    rsMess.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                    rsMess.Open();
                    if (!rsMess.IsEOF())
                    {
                        EmplId = rsMess.GetI("EMPLOYEE_ID");
                        ObjId = rsMess.GetI("OBJ_ID");
                        ObjTable = rsMess.GetS("OBJ_TABLE");
                        CreatedDate = rsMess.GetT("DATE_CREATED");
                        Message = rsMess.GetS("MESSAGE");
                    }
                    pb.Increment(true);
                }
                //---
                using (Icsm.LisRecordSet rsChange = new Icsm.LisRecordSet(PlugTbl.XnChangeLog, IMRecordset.Mode.ReadOnly))
                {
                    rsChange.Select("ID");
                    rsChange.Select("STATION_ID");
                    rsChange.Select("STATION_TBL");
                    rsChange.Select("RECORD_ID");
                    rsChange.Select("RECORD_TBL");
                    rsChange.Select("FIELD_NAME");
                    rsChange.Select("VAL_NEW");
                    rsChange.Select("VAL_TYPE");
                    rsChange.Select("VAL_OLD");
                    rsChange.Select("OPER_TYPE");
                    rsChange.Select("DATE_CREATED");
                    rsChange.SetWhere("MSG_ID", IMRecordset.Operation.Eq, Id);
                    for (rsChange.Open(); !rsChange.IsEOF(); rsChange.MoveNext())
                    {
                        ChangeLogRow row = new ChangeLogRow
                                               {
                                                   IsCheckBoxVisible = false,
                                                   Description = {Value = (fieldName.ContainsKey(rsChange.GetS("FIELD_NAME"))) ? fieldName[rsChange.GetS("FIELD_NAME")] : rsChange.GetS("FIELD_NAME")},
                                                   DateCreated = {ValueDateTime = rsChange.GetT("DATE_CREATED")},
                                                   Id = {Value = rsChange.GetI("ID")},
                                                   RecordId = {Value = rsChange.GetI("RECORD_ID")},
                                                   RecordTable = {Value = (tableName.ContainsKey(rsChange.GetS("RECORD_TBL"))) ? tableName[rsChange.GetS("RECORD_TBL")] : rsChange.GetS("RECORD_TBL")},
                                                   StationId = {Value = rsChange.GetI("STATION_ID")},
                                                   StationTable = {Value = (tableName.ContainsKey(rsChange.GetS("STATION_TBL"))) ? tableName[rsChange.GetS("STATION_TBL")] : rsChange.GetS("STATION_TBL")},
                                                   ValueNew = {Value = rsChange.GetS("VAL_NEW")},
                                                   ValueOld = {Value = rsChange.GetS("VAL_OLD")},
                                                   ValueType = {Value = rsChange.GetS("VAL_TYPE")},
                                                   OperationType = {Value = (operationName.ContainsKey(rsChange.GetS("OPER_TYPE"))) ? operationName[rsChange.GetS("OPER_TYPE")] : rsChange.GetS("OPER_TYPE")}
                                               };
                        LogRow.Add(row);
                        pb.Increment(true);
                    }
                }

            }
        }

        /// <summary>
        /// Добавляет новое сообщение в таблице изменений
        /// </summary>
        /// <param name="idRecord">ID записи</param>
        /// <param name="tableRecord">Название таблицы</param>
        /// <param name="message">Сообщение</param>
        public static void AddChangeMessage(int idRecord, string tableRecord, string message)
        {
            IMRecordset rsMsg = new IMRecordset(PlugTbl.XnChangeMsg, IMRecordset.Mode.ReadWrite);
            rsMsg.Select("ID,OBJ_ID,OBJ_TABLE,DATE_CREATED,EMPLOYEE_ID,MESSAGE");
            rsMsg.SetWhere("ID", IMRecordset.Operation.Eq, -1);
            try
            {
                rsMsg.Open();
                rsMsg.AddNew();
                rsMsg.Put("ID", IM.AllocID(PlugTbl.XnChangeMsg, 1, -1));
                rsMsg.Put("OBJ_ID", idRecord);
                rsMsg.Put("OBJ_TABLE", tableRecord);
                rsMsg.Put("EMPLOYEE_ID", CUsers.GetCurUserID());
                rsMsg.Put("MESSAGE", message);
                rsMsg.Update();
            }
            finally
            {
                if (rsMsg.IsOpen())
                    rsMsg.Close();
                rsMsg.Destroy();
            }
        }
    }
}
