﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Xml.Linq;
using System.Xml;




namespace XICSM.UcrfRfaNET
{
    /// <summary>
    /// Class that represent xml data from parus
    /// </summary>
    internal class CContragentParus
    {
        private DateTime AGNDATE;
        private string TYPEOPER;
        private string RN;
        private string AGNABBR;
        private int AGNTYPE;
        private string AGNNAME;
        private long AGNIDNUMB;
        private long ORGCODE;
        private string REASON_CODE;
        private string RESIDENT_SIGN;
        private string EMP;
        private string SEX;
        private string AGN_COMMENT;
        private int rn_num;
        private string GEOGRAPHY;
        public string GEOGRAPHY_FULL;
        #region FUNCTIONS
        string Get_Index(string Address)
        {
           string [] temp = Address.Split('#');
           if (temp.Length == 8)
              return temp[0];
           else
              return "";
        }
        string Get_OblType(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[1];
           else
              return "";
        }
      public  string Get_Obl(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[2];
           else
              return "";
        }
        string Get_RayType(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[3];
           else
              return "";
        }
   public string Get_rayon(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[4];
           else
              return "";
        }
        string Get_NasPtype(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[5];
           else
              return "";
        }
       public string Get_nasP(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[6];
           else
              return "";
        }
        string Get_Adress(string Address)
        {
           string[] temp = Address.Split('#');
           if (temp.Length == 8)
           return temp[7];
           else
              return "";
        }

        #endregion
        #region Properties
        public DateTime CUST_DAT1
        {
            get { return AGNDATE; }            
        }
        public DateTime agn_date
        {
            set { AGNDATE = value; }
        }
        public string Geography
        {
            get { return GEOGRAPHY; }
            set { GEOGRAPHY = value; }
        }
        public string INDEX
        {
            get
            {
               return Get_Index(GEOGRAPHY);
            }
        }
        public string PROVINCE
        {
            get
            {
                  return Get_Obl(GEOGRAPHY);
            }
        }

        public string CITY
        {
            get
            {
               return Get_nasP(GEOGRAPHY);
            }
        }

        public string ADDRESS
        {
            get
            {
               return Get_Adress(GEOGRAPHY);
            }
        }
        public string SUBPROVINCE
        {
            get
            {
                  return Get_rayon(GEOGRAPHY);
            }
        }

        public string CUST_TXT3
        {
            get { return TYPEOPER; }
        }
        public string typeOper
        {
            set { TYPEOPER = value; }
        }

        public int CODE
        {
            get { return rn_num; }
        }
        public string CODE_STR
        {
            get { return RN; }
        }
        public string rn
        {
            set
            {
                try { RN = value;
                rn_num = int.Parse(value);
                }
                catch { throw new Exception("Error during import data from XML RN must be an integer"); }
            }
        }

        public string FISC_NUM
        {
           get { return AGNIDNUMB.ToString(); }
        }
        public string agnabbr
        {
            set { AGNABBR = value; }
        }

        public int CUST_NBR2
        {
            get { return AGNTYPE; }
        }
        public int agntype
        {
            set { AGNTYPE = value; }
        }

        public string NAME
        {
            get { return AGNNAME; }
        }
        public string agnname
        {
            set { AGNNAME = value; }
        }

        public string REGIST_NUM_STR
        {
           get { return AGNABBR; }
        }
        public long agnidnumb
        {
            set { AGNIDNUMB = value; }
        }

        public long regist_num
        {
            get { return ORGCODE; }
        }
        public long orgcode
        {
            set { ORGCODE = value; }
        }

        public string IDENT
        {
            get { return REASON_CODE; }
        }
        public string reason_code
        {
            set { REASON_CODE = value; }
        }

        public int CUST_NBR3
        {
            get
            {
                if (RESIDENT_SIGN=="1")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string resident_sign
        {
            set { RESIDENT_SIGN = value; }
        }
        public int CUST_NBR4
        {
            get
            {
                try { return int.Parse(EMP); }
                catch { throw new Exception("Error while loading xml data field CUST_NBR4 must be integer"); }
            }
        }
        public string emp
        {
            set { EMP = value; }
        }

        public string CUST_TXT4
        {
            get
            {
                if (SEX == "0")
                    return "не задан ";
                else if (SEX == "1")
                    return "мужской ";
                else
                    return "женский";
            }
        }
        public string sex
        {
            set { SEX = value; }
        }

        public string REMARK1
        {
            get { return AGN_COMMENT; }
        }

        public string agn_comment
        {
            set { AGN_COMMENT = value; }
        }
        #endregion
    }
   
    class CParusXMLReader
    {
      private static int GetCityID(CContragentParus cparus)
       {
          int result=IM.NullI;
          string Postcode = cparus.INDEX;
          string province = cparus.Get_Obl(cparus.Geography);
          string subprovince = cparus.Get_rayon(cparus.Geography);
          string city = cparus.Get_nasP(cparus.Geography);
          if (string.IsNullOrEmpty(Postcode) && string.IsNullOrEmpty(province) && string.IsNullOrEmpty(subprovince) && string.IsNullOrEmpty(city))
             return result;
          IMRecordset rec = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
          rec.Select("ID,SUBPROVINCE,PROVINCE,NAME,CODE,POSTCODE");
          //if (!string.IsNullOrEmpty(Postcode))
          //{
          //   rec.SetWhere("POSTCODE", IMRecordset.Operation.Like, Postcode);
          //}
          if (!string.IsNullOrEmpty(province))
          {
             rec.SetWhere("PROVINCE", IMRecordset.Operation.Like, province);
          }
          if (!string.IsNullOrEmpty(subprovince))
          {
             rec.SetWhere("SUBPROVINCE", IMRecordset.Operation.Like, subprovince);
          }
          if (!string.IsNullOrEmpty(city))
          {
             rec.SetWhere("NAME", IMRecordset.Operation.Like, city);
          }
          rec.Open();
          if (!rec.IsEOF())
          {
             result= rec.GetI("ID");
             
          }
          rec.Close();
             rec.Destroy();
             return result;
       }
      private static bool IsEnteredBEfore(CContragentParus parus)
      {
         bool result=false;
         IMRecordset rec = new IMRecordset("USERS", IMRecordset.Mode.ReadOnly);
         rec.Select("ID,CODE");
         rec.SetWhere("CODE", IMRecordset.Operation.Eq, parus.CODE_STR);
         rec.Open();
         if (!rec.IsEOF())
         {
            result = true;
         }
         rec.Close();
         rec.Destroy();
         return result;
      }
        /// <summary>
        /// Big function that do all work
        /// </summary>
        /// <param name="cparus">data from xml </param>
        /// <param name="errnum">number of error if errnum=0 it means no errors</param>
        /// <returns></returns>
        private static bool import(CContragentParus cparus,ref int errnum)
        {
            if (cparus.CUST_NBR4 == 0)//we will write to users table
                return LoadUsers(cparus,ref errnum);
            else
                return LoadEmployee(cparus,ref errnum);
        }
        //=================================================
        private static bool LoadUsers(CContragentParus cparus,ref int errnum)
        {
            bool result = true;
            int new_id = ICSM.IM.NullI;
            IMRecordset set = new IMRecordset("USERS", IMRecordset.Mode.ReadWrite);
            set.Select("ID,MODIFIED_BY,CREATED_BY,CUST_DAT1,DATE_CREATED,CITY_ID,DATE_MODIFIED,CUST_TXT1,CUST_TXT3,CODE,FISC_NUM,IDENT,CUST_NBR2,CUST_NBR4,NAME,REGIST_NUM,CUST_NBR3,CUST_NBR4,CUST_TXT4,REMARK1,POSTCODE,PROVINCE,SUBPROVINCE,CITY,ADDRESS");
            set.SetWhere("CODE", IMRecordset.Operation.Eq, cparus.CODE_STR);
            try
            {
                set.Open();
                if (set.IsEOF())
                {
                    set.AddNew();
                    new_id = IM.AllocID("USERS", 1, -1);
                    set.Put("ID", new_id);
                    set.Put("CODE", cparus.CODE_STR);
                    set.Put("DATE_CREATED", cparus.CUST_DAT1);
                    set.Put("CREATED_BY", "PARUS");
                }
                else
                {
                    set.Edit();
                    new_id = set.GetI("ID");
                    set.Put("DATE_MODIFIED", cparus.CUST_DAT1);
                    set.Put("MODIFIED_BY", "PARUS");
                }
                set.Put("FISC_NUM", cparus.FISC_NUM);//
                set.Put("NAME", cparus.NAME);//
                set.Put("REGIST_NUM", cparus.REGIST_NUM_STR);//
                set.Put("CODE", cparus.CODE_STR);//
                set.Put("POSTCODE", cparus.INDEX);//
                set.Put("PROVINCE", cparus.PROVINCE);//
                set.Put("SUBPROVINCE", cparus.SUBPROVINCE);//
                set.Put("CITY", cparus.CITY);//
                set.Put("ADDRESS", cparus.ADDRESS);//
                int idCity = GetCityID(cparus);
                set.Put("CITY_ID", idCity);//
                set.Put("CUST_NBR2", cparus.CUST_NBR2);
                set.Put("CUST_NBR3", cparus.CUST_NBR3);//
                set.Put("CUST_NBR4", cparus.CUST_NBR4);//
                set.Put("CUST_TXT1", cparus.REMARK1);//
                set.Put("CUST_TXT3", cparus.CUST_TXT3);//
                set.Put("CUST_TXT4", cparus.CUST_TXT4);//
                set.Put("CUST_DAT1", cparus.CUST_DAT1);//
                set.Put("REMARK1", cparus.GEOGRAPHY_FULL);//
                set.Update();
            }
            catch (Exception)
            {
                result = false;
                if (errnum == 0)
                    errnum = (int)DRVError.de_22;
            }
            finally
            {
                if (set != null)
                { set.Destroy(); }    //destroying what we have create here           
            }
            if((result == true) && (new_id != ICSM.IM.NullI))
                GlobalDB.CGlobalXML.AddGUIDRecord(new RecordPtr("USERS", new_id), cparus.CODE);
            return result;
        }
        //=================================================
        private static bool LoadEmployee(CContragentParus cparus, ref int errnum)
        {
            bool result = true;
            int new_id = ICSM.IM.NullI;
            IMRecordset set = new IMRecordset("EMPLOYEE", IMRecordset.Mode.ReadWrite);
            set.Select("ID,MODIFIED_BY,CREATED_BY,DATE_CREATED,DATE_MODIFIED,LASTNAME,FIRSTNAME,PWD,REMARK1");
            set.SetWhere("PWD", IMRecordset.Operation.Eq, cparus.FISC_NUM);
            try
            {
                set.Open();
                if (set.IsEOF())
                {
                    set.AddNew();
                    new_id = IM.AllocID("EMPLOYEE", 1, -1);
                    set.Put("ID", new_id);
                    set.Put("PWD", cparus.FISC_NUM);
                    set.Put("DATE_CREATED", cparus.CUST_DAT1);
                    set.Put("CREATED_BY", "PARUS");
                }
                else
                {
                    set.Edit();
                    new_id = set.GetI("ID");
                    set.Put("DATE_MODIFIED", cparus.CUST_DAT1);
                    set.Put("MODIFIED_BY", "PARUS");
                }
                set.Put("REMARK1", cparus.REMARK1);
                {//trying to split employe name
                    string[] fif = cparus.NAME.Split(' ');
                    set.Put("LASTNAME", fif[0]);
                    string firs = "";
                    for (int i = 1; i < fif.Length; i++)
                    {
                        firs += fif[i];
                        firs += " ";
                    }
                    set.Put("FIRSTNAME", firs);
                }
                set.Update();
            }
            catch (Exception)
            {
                result = false;
                if (errnum == 0)
                    errnum = (int)DRVError.de_22;
            }
            finally
            {
                if (set != null)
                { set.Destroy(); }    //destroying what we have create here           
            }
            if ((result == true) && (new_id != ICSM.IM.NullI))
                GlobalDB.CGlobalXML.AddGUIDRecord(new RecordPtr("EMPLOYEE", new_id), cparus.CODE);
            return result;
        }
        /// <summary>
        /// Import data
        /// </summary>
        /// <param name="XML">Xml string</param>
        /// <param name="errlist">list for errors</param>
        public static bool XMLImport(string XML, out int errlist)
        {
            bool result = true;
            errlist = 0;
            XDocument xdoc = new XDocument();
            if (XML == "")
            {
                errlist = 23;
                return false;
            }
            XML = XML.Replace("&amp;quot;", "''");
            XML = XML.Replace("&amp;apos;", "`");
            xdoc = XDocument.Parse(XML);    
             CContragentParus parus = new CContragentParus();
            try
            {
               IEnumerable<XElement> ellis = xdoc.Root.Elements();
               XElement head = xdoc.Root.Element("DECLARHEAD");
                XElement body = xdoc.Root.Element("DECLARBODY");

                if (head == null)
                {
                   if (ellis.Count<XElement>() == 2)
                   {
                      head = ellis.ElementAt<XElement>(0);
                   }
                }
                if (body == null)
                {
                   if (ellis.Count<XElement>() == 2)
                   {
                      body = ellis.ElementAt<XElement>(1);
                   }
                }
               
                foreach (XElement xel in head.Elements())
                {
                   if (xel.Name.ToString() == "DATEOPER" || xel.Name.LocalName == "DATEOPER")
                    {
                        parus.agn_date = DateTime.Parse(xel.Value);
                    }
                   //else if (xel.Name.ToString() == "TYPEOPER" || xel.Name.LocalName == "TYPEOPER")
                   // {
                   //     parus.typeOper = xel.Value.ToString();
                   // }
                }
                //searching data from xml if there will be something wrong here parus class will throw an exception
                foreach (XElement xel in body.Elements())
                {
                   if (xel.Name.ToString() == "RN" || xel.Name.LocalName == "RN")
                    {
                       if (!xel.HasAttributes)
                        parus.rn = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "AGNABBR" || xel.Name.LocalName == "AGNABBR")
                    {
                       if (!xel.HasAttributes)
                        parus.agnabbr = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "AGNTYPE" || xel.Name.LocalName == "AGNTYPE")
                    {
                       if (!xel.HasAttributes)
                        parus.agntype = int.Parse(xel.Value.ToString());
                    }
                   else if (xel.Name.ToString() == "AGNNAME" || xel.Name.LocalName == "AGNNAME")
                    {
                       if (!xel.HasAttributes)
                        parus.agnname = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "AGNIDNUMB" || xel.Name.LocalName == "AGNIDNUMB")
                    {
                        try
                        {
                            if (!xel.HasAttributes)
                                parus.agnidnumb = long.Parse(xel.Value.ToString());
                        }
                        catch
                        {

                        }
                    }
                   else if (xel.Name.ToString() == "ORGCODE" || xel.Name.LocalName == "ORGCODE")
                    {
                        try
                        {
                            if (!xel.HasAttributes)
                                parus.orgcode = long.Parse(xel.Value.ToString());
                        }
                        catch
                        {

                        }
                    }
                   else if (xel.Name.ToString() == "REASON_CODE" || xel.Name.LocalName == "REASON_CODE")
                    {
                       if (!xel.HasAttributes)
                        parus.reason_code = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "RESIDENT_SIGN" || xel.Name.LocalName == "RNRESIDENT_SIGN")
                    {
                       if (!xel.HasAttributes)
                        parus.resident_sign = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "EMP" || xel.Name.LocalName == "EMP")
                    {
                       if (!xel.HasAttributes)
                        parus.emp = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "SEX" || xel.Name.LocalName == "SEX")
                    {
                       if (!xel.HasAttributes)
                        parus.sex = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "AGN_COMMENT" || xel.Name.LocalName == "AGN_COMMENT")
                    {
                       if (!xel.HasAttributes)
                        parus.agn_comment = xel.Value.ToString();
                    }
                   else if (xel.Name.ToString() == "GEOGRAFY" || xel.Name.LocalName == "GEOGRAFY")
                    {
                       if (!xel.HasAttributes)
                        parus.Geography = xel.Value.ToString();
                    }
                   else if (xel.Name.LocalName == "GEOGRAFYFULL")
                   {
                      if (!xel.HasAttributes)
                      {
                         parus.GEOGRAPHY_FULL = xel.Value.ToString();
                      }
                   }
                }
                import(parus, ref errlist);
            }
            catch (Exception)
            {
                result = false;
               // errlist.Add(string.Format("Error ocured during import XML with RN= {0} error is {1}", parus.CODE, e.Message));
                errlist = (int) DRVError.de_21;
            }
            return result;
        }
    }
}
