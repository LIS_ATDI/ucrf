﻿using System;
using System.Collections.Generic;
using System.Data;

namespace XICSM.UcrfRfaNET.LocalSetting
{
    internal class Config
    {
        private class ConfigItem
        {
            public int Id { get; set; }
            public string Item { get; set; }
            public string What { get; set; }
            public ConfigItem()
            {
                Id = 0;
                Item = "";
                What = "";
            }
        }

        private List<ConfigItem> _configItems = new List<ConfigItem>();
        /// <summary>
        /// Конструктор
        /// </summary>
        public Config()
        {
            Load();
        }
        /// <summary>
        /// Загрузка конфигурации
        /// </summary>
        private void Load()
        {
            if (DbSqLite.IsInited == false)
                return;
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();

                    using (IDbCommand cmd = connect.CreateCommand())
                    {
                        cmd.CommandText = string.Format(@"SELECT ID,ITEM_KEY,ITEM_DATA FROM {0}", DbTableName.Tconfig);
                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ConfigItem tmp = new ConfigItem();
                                tmp.Id = reader.GetInt32(0);
                                tmp.Item = reader.GetString(1);
                                tmp.What = reader.GetString(2);
                                _configItems.Add(tmp);
                            }
                            reader.Close();
                        }
                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
        }
        /// <summary>
        /// Сохраняет конфигурацию
        /// </summary>
        public bool Save()
        {
            if (DbSqLite.IsInited == false)
                return false;
            bool retVal = true;
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();
                    using (IDbTransaction dbTrans = connect.BeginTransaction())
                    {
                        using (IDbCommand cmd = connect.CreateCommand())
                        {
                            foreach (ConfigItem item in _configItems)
                            {
                                if (item.Id > 0)
                                {
                                    cmd.CommandText =
                                        string.Format(
                                            @"UPDATE {0} SET ITEM_KEY='{2}',ITEM_DATA='{3}' WHERE ID={1}",
                                            DbTableName.Tconfig, item.Id, item.Item, item.What);
                                }
                                else
                                {
                                    cmd.CommandText =
                                        string.Format(
                                            @"INSERT INTO {0} (ITEM_KEY,ITEM_DATA) VALUES ('{1}','{2}')",
                                            DbTableName.Tconfig, item.Item, item.What);
                                }
                                cmd.ExecuteNonQuery();
                                if (item.Id <= 0)
                                    item.Id = connect.GelLastInsertedId();
                            }
                        }
                        dbTrans.Commit();
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    retVal = false;
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                        connect.Close();
                }
            }
            return retVal;
        }
        /// <summary>
        /// Установить значение
        /// </summary>
        /// <param name="itemKey">Ключ записи</param>
        /// <param name="itemData">Данные</param>
        public void Set(string itemKey, string itemData)
        {
            foreach (ConfigItem item in _configItems)
            {
                if (item.Item == itemKey)
                {
                    item.What = itemData;
                    return;
                }
            }
            //Добавляем новую запись
            ConfigItem newItem = new ConfigItem();
            newItem.Item = itemKey;
            newItem.What = itemData;
            _configItems.Add(newItem);
        }
        /// <summary>
        /// Вернуть значение
        /// </summary>
        /// <param name="itemKey">Ключ записи</param>
        /// <returns>Значение ключа</returns>
        public string Get(string itemKey)
        {
            foreach (ConfigItem item in _configItems)
                if (item.Item == itemKey)
                    return item.What;
            return "";
        }
        /// <summary>
        /// Установить значение
        /// </summary>
        /// <param name="itemKey">Ключ записи</param>
        /// <param name="itemData">Данные</param>
        public void Set(string itemKey, double itemData)
        {
            Set(itemKey, itemData.ToString());
        }
        /// <summary>
        /// Вернуть значение
        /// </summary>
        /// <param name="itemKey">Ключ записи</param>
        /// <param name="defVal">Значение по умолчанию</param>
        /// <returns>Значение ключа</returns>
        public double Get(string itemKey, double defVal)
        {
            string tmpString = Get(itemKey);
            if (string.IsNullOrEmpty(tmpString))
                return defVal;
            double retVal = defVal;
            if (Double.TryParse(tmpString, out retVal) == false)
                retVal = defVal;
            return retVal;
        }
        /// <summary>
        /// Определяет существование ключа
        /// </summary>
        /// <param name="itemKey">Ключ записи</param>
        /// <returns>TRUE - если ключ существует, иначе FALSE</returns>
        public bool IsExist(string itemKey)
        {
            foreach (ConfigItem item in _configItems)
                if (item.Item == itemKey)
                    return true;
            return false;
        }
    }
}
