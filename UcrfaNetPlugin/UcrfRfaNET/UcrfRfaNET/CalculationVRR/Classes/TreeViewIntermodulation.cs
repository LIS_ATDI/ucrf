﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    /// <summary>
    /// Класс описывает строку для отображения помех
    /// при расчете ЭМС интермодуляции
    /// </summary>
    internal class TreeViewIntermodulation : BaseRowCalculationVrr
    {
        #region General parameters
        /// <summary>
        /// Частота
        /// </summary>
        [TreeColumn("Frequency")]
        public DoubleElemCell Frequency { get; set; }
        /// <summary>
        /// Частота приема
        /// </summary>
        [TreeColumn("Freq RX")]
        public DoubleElemCell FreqRx { get; set; }
        /// <summary>
        /// Помеха
        /// </summary>
        [TreeColumn("Pi")]
        public DoubleElemCell Pi { get; set; }
        /// <summary>
        /// IRF
        /// </summary>
        [TreeColumn("IRF (dB)")]
        public DoubleElemCell Irf { get; set; }
        /// <summary>
        /// Растояние
        /// </summary>
        [TreeColumn("Distance")]
        public DoubleElemCell Distance { get; set; }
        /// <summary>
        /// Разница частот
        /// </summary>
        [TreeColumn("Delta freq")]
        public DoubleElemCell DeltaF { get; set; }
        /// <summary>
        /// Радиотехнология
        /// </summary>
        [TreeColumn("Standard")]
        public StringElemCell Standard { get; set; }
        /// <summary>
        /// Мощность
        /// </summary>
        [TreeColumn("Power (W)")]
        public DoubleElemCell Power { get; set; }
        /// <summary>
        /// Азимут TX
        /// </summary>
        [TreeColumn("Azimuth TX, deg")]
        public DoubleElemCell AzimuthTx { get; set; }
        /// <summary>
        /// Азимут RX
        /// </summary>
        [TreeColumn("Azimuth RX, deg")]
        public DoubleElemCell AzimuthRx { get; set; }
        /// <summary>
        /// Потери распрастранения
        /// </summary>
        [TreeColumn("Loss")]
        public DoubleElemCell Loss { get; set; }
        /// <summary>
        /// КУ атненный TX
        /// </summary>
        [TreeColumn("Gtx, dBi")]
        public DoubleElemCell Gtx { get; set; }
        /// <summary>
        /// КУ атненный RX
        /// </summary>
        [TreeColumn("Grx, dBi")]
        public DoubleElemCell Grx { get; set; }
        /// <summary>
        /// Привышение порога
        /// </summary>
        [TreeColumn("Delta degradation, dB")]
        public DoubleElemCell DeltaDegradation { get; set; }
        /// <summary>
        /// Порога
        /// </summary>
        [TreeColumn("Degradation, dBm")]
        public DoubleElemCell Degradation { get; set; }
        /// <summary>
        /// Владелец
        /// </summary>
        [TreeColumn("Owner")]
        public StringElemCell Owner { get; set; }
        /// <summary>
        /// Название станции
        /// </summary>
        [TreeColumn("Station name")]
        public StringElemCell StationName { get; set; }
        /// <summary>
        /// Статус станции
        /// </summary>
        [TreeColumn("Status")]
        public StringElemCell Status { get; set; }
        #endregion
        //===============================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public TreeViewIntermodulation() : base()
        {
            Frequency = new DoubleElemCell(4);
            Pi = new DoubleElemCell(-999.0, 2);
            Irf = new DoubleElemCell(2);
            Distance = new DoubleElemCell(3);
            DeltaF = new DoubleElemCell(4);
            Standard = new StringElemCell();
            Power = new DoubleElemCell(2);
            Owner = new StringElemCell();
            StationName = new StringElemCell();
            AzimuthRx = new DoubleElemCell(0);
            AzimuthTx = new DoubleElemCell(0);
            Loss = new DoubleElemCell(2);
            Status = new StringElemCell();
            DeltaDegradation = new DoubleElemCell(2);
            Degradation = new DoubleElemCell(2);
            FreqRx = new DoubleElemCell(4);
            Gtx = new DoubleElemCell(2);
            Grx = new DoubleElemCell(2);
        }

    }
}
