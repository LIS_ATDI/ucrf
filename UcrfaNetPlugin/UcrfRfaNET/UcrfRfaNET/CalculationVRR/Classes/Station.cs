﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;
using LisUtility.Mob;
using XICSM.UcrfRfaNET;
using Lis.CommonLib.Extensions;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   internal class Station
   {
      #region MEMBERS

      public enum TypeOfStation
      {
         MOB,
         Abonent,
         MOBandAbonent,
         Block
      }
       /// <summary>
       /// Структура с расчетными данными
       /// </summary>
      public class CalculatedData
      {
          public double Irf;
          public double A1;
          public double A2;
          public double B1;
          public double B2;
          public double Gt;
          public double Gr;
          public double D;
          public double FreqTx;
          public double FreqRx;
          public double DeltaFreq;
          public double L525;
          public double Pi;
          public double Excelling;
      }
      /// <summary>
      /// Структура каналов
      /// </summary>
      public class Channel : IComparable
      {
          public string ChannelTx { get; set; }
          public string ChannelRx { get; set; }
          public double FreqRx {get; set; }
          public double FreqTx { get; set; }

          public Channel()
          {
              ChannelTx = "";
              ChannelRx = "";
              FreqRx = IM.NullD;
              FreqTx = IM.NullD;
          }

          #region Implementation of IComparable

          /// <summary>
          /// Compares the current instance with another object of the same type.
          /// </summary>
          /// <returns>
          /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than obj. Zero This instance is equal to obj. Greater than zero This instance is greater than obj. 
          /// </returns>
          /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException">obj is not the same type as this instance. </exception><filterpriority>2</filterpriority>
          public int CompareTo(object obj)
          {
              Channel objChannel = obj as Channel;
              if(objChannel == null)
                  return -1;
              int channelTx1 = ChannelTx.ToInt32(IM.NullI);
              int channelTx2 = objChannel.ChannelTx.ToInt32(IM.NullI);
              if (channelTx1 != channelTx2)
                  return channelTx1 - channelTx2;
              return FreqTx.CompareWith(objChannel.FreqTx);
          }

          #endregion
      }
      /// <summary>
      /// Структура каналов
      /// </summary>
      public class ChannelsList
      {
          private List<Channel> _channels;
          private bool _isNeedLoad = true;
          private string _chanelsTxStr = "";
          private string _chanelsRxStr = "";
          private string _freqTxStr = "";
          private string _freqRxStr = "";
          public Channel[] Channels { get { return _channels.ToArray(); } }
          public string ChannelTxString { get { CreateStrings(); return _chanelsTxStr; } }
          public string ChannelRxString { get { CreateStrings(); return _chanelsRxStr; } }
          public string FreqTxString { get { CreateStrings(); return _freqTxStr; } }
          public string FreqRxString { get { CreateStrings(); return _freqRxStr; } }
          /// <summary>
          /// Конструктор
          /// </summary>
          public ChannelsList()
          {
              _channels = new List<Channel>();
          }
          /// <summary>
          /// Добавляет новый канал
          /// </summary>
          /// <param name="cnl">Канал</param>
          public void Add(Channel cnl)
          {
              _channels.Add(cnl);
              _isNeedLoad = true;
          }
          /// <summary>
          /// Удаляет все каналы
          /// </summary>
          public void Clear()
          {
              _channels.Clear();
              _isNeedLoad = true;
          }

          private void CreateStrings()
          {
              if (_isNeedLoad == false)
                  return;
              _isNeedLoad = false;
              //-------
              _chanelsTxStr = "";
              _chanelsRxStr = "";
              _freqTxStr = "";
              _freqRxStr = "";
              _channels.Sort();
              List<List<Channel>> grouTx = new List<List<Channel>>();
              List<List<Channel>> grouRx = new List<List<Channel>>();
              int lastChanelTx = IM.NullI;
              int lastChanelRx = IM.NullI;
              foreach (Channel channel in _channels)
              {
                  int curChanelTx = channel.ChannelTx.ToInt32(IM.NullI);
                  if (lastChanelTx != (curChanelTx - 1))
                  {
                      grouTx.Add(new List<Channel>());
                  }
                  lastChanelTx = curChanelTx;
                  grouTx[grouTx.Count - 1].Add(channel);
                  //-----
                  int curChanelRx = channel.ChannelRx.ToInt32(IM.NullI);
                  if (lastChanelRx != (curChanelRx - 1))
                  {
                      grouRx.Add(new List<Channel>());
                  }
                  lastChanelRx = curChanelRx;
                  grouRx[grouRx.Count - 1].Add(channel);
              }
              //----
              foreach (List<Channel> tx in grouTx)
              {
                  switch (tx.Count)
                  {
                      case 0:
                          break;
                      case 1:
                          if (string.IsNullOrEmpty(_chanelsTxStr) == false)
                              _chanelsTxStr += "; ";
                          _chanelsTxStr += string.Format(" {0}", tx[0].ChannelTx);
                          if (string.IsNullOrEmpty(_freqTxStr) == false)
                              _freqTxStr += "; ";
                          _freqTxStr += string.Format(" {0}", tx[0].FreqTx.Round(6).ToStringNullD());
                          break;
                      default:
                          if (string.IsNullOrEmpty(_chanelsTxStr) == false)
                              _chanelsTxStr += "; ";
                          _chanelsTxStr += string.Format(" {0}-{1}", tx[0].ChannelTx, tx[tx.Count - 1].ChannelTx);
                          if (string.IsNullOrEmpty(_freqTxStr) == false)
                              _freqTxStr += "; ";
                          _freqTxStr += string.Format(" {0} - {1}", tx[0].FreqTx.Round(6).ToStringNullD(), tx[tx.Count - 1].FreqTx.Round(6).ToStringNullD());
                          break;
                  }
              }
              //----
              foreach (List<Channel> rx in grouRx)
              {
                  switch (rx.Count)
                  {
                      case 0:
                          break;
                      case 1:
                          if (string.IsNullOrEmpty(_chanelsRxStr) == false)
                              _chanelsRxStr += "; ";
                          _chanelsRxStr += string.Format(" {0}", rx[0].ChannelRx);
                          if (string.IsNullOrEmpty(_freqRxStr) == false)
                              _freqRxStr += "; ";
                          _freqRxStr += string.Format(" {0}", rx[0].FreqRx.Round(6).ToStringNullD());
                          break;
                      default:
                          if (string.IsNullOrEmpty(_chanelsRxStr) == false)
                              _chanelsRxStr += "; ";
                          _chanelsRxStr += string.Format(" {0}-{1}", rx[0].ChannelRx, rx[rx.Count - 1].ChannelRx);
                          if (string.IsNullOrEmpty(_freqRxStr) == false)
                              _freqRxStr += "; ";
                          _freqRxStr += string.Format(" {0} - {1}", rx[0].FreqRx.Round(6).ToStringNullD(), rx[rx.Count - 1].FreqRx.Round(6).ToStringNullD());
                          break;
                  }
              }
          }
      }

      public List<CalculatedData> CalcDataList { get; set; }


      public int _tableID; // id станции
      public int _ownerID; // id owner
      public double _latitude = IM.NullD; // широта
      public double _longitude = IM.NullD; // довгота
      public double _power; // потужність
      public double _lFT; // ослаблення фідера передатчика
      public double _lFR; // ослаблення фідера приймача
      public double _gMax; // максімальний коефіцієнт підсилення антени
      public string _aDiagH = string.Empty; // діаграма по горизонталі
      public string _bDiagV = string.Empty; // діаграма по вертикалі
      public double _sensitivity; // чуствительность приемника dBm
      public double _ktbf; // рівень власних шумів прийомника
      public List<ACHMob.DataElement> _maskaT = new List<ACHMob.DataElement>(); // спектральна маска передатчика
      public List<ACHMob.DataElement> _maskaR = new List<ACHMob.DataElement>(); // спектральна маска прийомника
      public List<ACHMob.DataElement> _filterT = new List<ACHMob.DataElement>(); // фильтр передатчика
      public List<ACHMob.DataElement> _filterR = new List<ACHMob.DataElement>(); // фильтр прийомника
      public double _freqFilterTX; // частота зовнішнього фільтра передатчика
      public double _freqFilterRX; // частота зовнішнього фільтра прийомника      
      public double _aboveSeaLevel; // висота над рівнем моря в м
      public double _aboveEarthLevel; // висота над рівнем землі в м
      public double _bwTX; // ширина смуги частот передавача
      public double _bwRX; // ширина смуги частот приймача
      public List<double> STFrequncyRX = new List<double>(); // частота прийомників
      public List<double> STFrequncyTX = new List<double>(); // частота передатчиків
      public List<double> L525orHata = new List<double>(); // Ослабления на пролете [dB]:
      public double _azimuth; // азимут карточки(з станції)
      public string _distance_board; // відстань до кордону
      public double _bElevation; // угол в з карточки

      public string _standart = string.Empty; // поле стандарт
      public double _noiseF; // отношение сигнал шум
      //public double _trashold; // порог
      //public double _trasholdAbonent; // порог абонента 
      public int _equipID;
      public int _rxequipID;
      public int _exfilterID;
      public int _rexfilterID;

      public List<double> IRF = new List<double>();
      public List<double> DISTANCE = new List<double>(); // відстань d
      public List<double> EVP = new List<double>(); // 
      public List<double> EXCESS = new List<double>(); // перевищення
      public List<double> DELTAFREQ = new List<double>(); // порог
      public List<double> GT = new List<double>(); // втрати в антені передатчика
      public List<double> GA = new List<double>(); // втрати в антені прийомника
      public List<double> A1 = new List<double>(); // азимут
      public List<double> A2 = new List<double>(); // азимут
      public List<double> B1 = new List<double>(); // кут місця
      public List<double> B2 = new List<double>(); // кут місця
      public List<double> CURTX = new List<double>(); //
      public List<double> CURRX = new List<double>(); //

      public double X; //
      public double Y; //
      public double Lon; //
      public double Lat; //


      public List<double> PI = new List<double>(); // при подборі частот буде находиться значення потужності помєхі куренту            
      public List<double> PI_A = new List<double>(); // при подборі частот буде находиться значення потужності помєхі абоненту курента(курент аналізіруєма станція)            
      public List<double> PIZHERTV = new List<double>(); // ячейка в яку буде ложиться потужність помехі від курента (актуальна для подбора частот)
      public List<double> PIZHERTV_A = new List<double>(); // ячейка в яку буде ложиться потужність помехі для абонента від курента (актуальна для подбора частот)
      public bool _show = false; // the station is displayed or don't displayed
      public string _table = string.Empty;
       //---------
       // Параметры, которые не нужны для расчетов
       public string OwnerName { get; set; }
       public string Address { get; set; }
       public string DesEmi { get; set; }
       public ChannelsList Chennels { get; set; }
       public string Status { get; set; }
       public string StationName { get; set; }


      public Station(string tableName, int id)
      {

         _table = tableName;
         _tableID = id;
          CalcDataList = new List<CalculatedData>();
          Chennels = new ChannelsList();
          CreateStation(tableName);


      }

      #endregion

      #region METHODS

      public static void GetOwnerData(Station curStat, ref string OwnerRemark)
      {
         IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
         r.Select("ID,Position.REMARK,Owner.NAME");
         r.SetWhere("ID", IMRecordset.Operation.Eq, curStat._tableID);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            OwnerRemark = r.GetS("Owner.NAME") + "\r\n";
            OwnerRemark += r.GetS("Position.REMARK");
         }
      }


      private void CreateStation(String table_name)
      {
          

         bool baseFilled = false;
       
         IMRecordset r = new IMRecordset(table_name, IMRecordset.Mode.ReadOnly);
         //LOG.Warning("создаю новую станцыю, её ID = " + _tableID + "\n");
         r.Select("ID,ANT_ID,RX_EQP_ID,EQUIP_ID,PWR_ANT,TX_LOSSES,TX_ADDLOSSES,GAIN,Antenna.DIAGH,Antenna.DIAGV,RX_LOSSES,Equipment.SENSITIVITY,Equipment.KTBF,Position.LONGITUDE,Position.LATITUDE,Position.ASL,Position.X,Position.Y, AGL,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,AZIMUTH,ELEVATION,STANDARD,Equipment.NOISE_F,Equipment.RXTH_10,BW,Equipment.KTBF,Equipment.C_I,DESIG_EMISSION,OWNER_ID,RxEquipment.SENSITIVITY,RxEquipment.KTBF,RxEquipment.NOISE_F,RxEquipment.C_I");
         //LOG.Warning("ID,RX_EQP_ID,EQUIP_ID,PWR_ANT,TX_LOSSES,TX_ADDLOSSES,GAIN,Antenna.DIAGH,Antenna.DIAGV,RX_LOSSES,Equipment.SENSITIVITY,Equipment.KTBF,Position.LONGITUDE,Position.LATITUDE,Position.ASL,AGL,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,AZIMUTH,ELEVATION,Equipment.NAME,Owner.NAME,STANDARD,Equipment.NOISE_F,Equipment.RXTH_10,BW,Equipment.KTBF,Equipment.C_I,DESIG_EMISSION" + "\n");
         r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {

             X = r.GetD("Position.X");
             Y = r.GetD("Position.Y");
             Lon = r.GetD("Position.LONGITUDE");
             Lat = r.GetD("Position.LATITUDE");
             

            bool fl = false;
            if ((r.GetI("ANT_ID") == IM.NullI) || (r.GetI("EQUIP_ID") == IM.NullI))
               continue;

            if (r.GetD("AssignedFrequencies.TX_FREQ") != IM.NullD)
            STFrequncyTX.Add(r.GetD("AssignedFrequencies.TX_FREQ"));

            if (r.GetD("AssignedFrequencies.RX_FREQ") != IM.NullD)
            STFrequncyRX.Add(r.GetD("AssignedFrequencies.RX_FREQ"));
            
            if (r.GetI("RX_EQP_ID") != IM.NullI)
               fl = true;

            if (!baseFilled)
            {
               _power = r.GetD("PWR_ANT");
               if (_power == IM.NullD)
                  _power = -999.0;
               else _power += 30.0;

               if (_power > 100)
                  _power = 100;

               _lFT = 0.0;
               if ((r.GetD("TX_LOSSES") != IM.NullD) && (r.GetD("TX_ADDLOSSES") != IM.NullD))
                  _lFT = r.GetD("TX_LOSSES") + r.GetD("TX_ADDLOSSES");

               _gMax = 0.0;
               if (r.GetD("GAIN") != IM.NullD)
                  _gMax = r.GetD("GAIN");

               _aDiagH = "WIEN 000ND00";
               if (string.IsNullOrEmpty(r.GetS("Antenna.DIAGH")) == false)
                  _aDiagH = r.GetS("Antenna.DIAGH");

               _bDiagV = "WIEN 000ND00";
               if (string.IsNullOrEmpty(r.GetS("Antenna.DIAGV")) == false)
                   _bDiagV = r.GetS("Antenna.DIAGV");

               _lFR = 0.0;
               if (r.GetD("RX_LOSSES") != IM.NullD)
                  _lFR = r.GetD("RX_LOSSES");

               if (fl)
               {
                  _sensitivity = r.GetD("RxEquipment.SENSITIVITY");
                  _ktbf = r.GetD("RxEquipment.KTBF");
               }
               else
               {
                  _sensitivity = r.GetD("Equipment.SENSITIVITY");
                  _ktbf = r.GetD("Equipment.KTBF");
               }
               _longitude = r.GetD("Position.LONGITUDE");
               _latitude = r.GetD("Position.LATITUDE");

               _aboveSeaLevel = 0.0;
               if (r.GetD("Position.ASL") != IM.NullD)
                  _aboveSeaLevel = r.GetD("Position.ASL");

               _aboveEarthLevel = 30.0;
               if (r.GetD("AGL") != IM.NullD)
                  _aboveEarthLevel = r.GetD("AGL");

               _azimuth = 0.0;
               if (r.GetD("AZIMUTH") != IM.NullD)
                  _azimuth = r.GetD("AZIMUTH");

               _bElevation = 0.0;
               if (r.GetD("ELEVATION") != IM.NullD)
                  _bElevation = r.GetD("ELEVATION");

               //_adress = r.GetS("Equipment.NAME");
               //_owner = r.GetS("Owner.NAME");
               _standart = r.GetS("STANDARD");
               if (fl)
                  _noiseF = r.GetD("RxEquipment.NOISE_F");
               else
                  _noiseF = r.GetD("Equipment.NOISE_F");

               //LOG.Warning("Достаю BW" + "\n");

               _bwTX = r.GetD("BW");
               if (_bwTX == IM.NullD || _bwTX == 0.0)
               {
                  //LOG.Warning("парсю BW" + "\n");
                  string aaa = r.GetS("DESIG_EMISSION");

                  _bwTX = XICSM.UcrfRfaNET.Station.ParserForSRTBW(aaa);
                 

                  if (_bwTX <= 0.0)
                     _bwTX = 0.025;
               }
               else
               {
                  _bwTX /= 1000.0;
               }

               //LOG.Warning("BW = " + _bwTX + "\n");
               _bwRX = _bwTX;

               _equipID = r.GetI("EQUIP_ID");
               if (_equipID == IM.NullD || _equipID == 0)
               {
                  _longitude = 0.0;
                  _latitude = 0.0;
               }

               _rxequipID = r.GetI("RX_EQP_ID");
               if (_rxequipID == IM.NullI)
                  _rxequipID = _equipID;

               double ci = IM.NullD;
               if (fl)
               {
                   ci = r.GetD("RxEquipment.C_I");
               }
               else
               {
                   ci = r.GetD("Equipment.C_I");
               }


               if (_ktbf == IM.NullD)
               {
                   if ((_sensitivity != IM.NullD) && (ci != IM.NullD))
                   {
                       _ktbf = _sensitivity - ci;
                   }
                   else
                   {
                       double k = 1.38 * Math.Pow(10, -14);
                       _ktbf = _bwTX * 400 * k;
                       _ktbf = 10 * Math.Log10(_ktbf);
                   }
               } 
               if (_sensitivity == IM.NullD)
               {
                   if (ci == IM.NullD)
                       ci = 9;
                   _sensitivity = _ktbf + ci; 
               }

               
               _ownerID = r.GetI("OWNER_ID");

               baseFilled = true;
            }
         }
         r.Close();
         r.Destroy();




         

         
       


           
         //LOG.Warning("достаю маску передатчика" + "\n");
         // filling the data of mask transmitter
         string table_eqipment = ICSMTbl.itblEquipPmrMpt;  // тут вставочка небольшая для того чтоб станции СРТ работали
         if (table_name == ICSMTbl.itblMobStation2)
             table_eqipment = "EQUIP_MOB2_MPT";






         r = new IMRecordset(table_eqipment, IMRecordset.Mode.ReadOnly);
         r.Select("EQUIP_ID,TYPE,FREQ,ATTN");
         r.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, _equipID);
         r.SetWhere("TYPE", IMRecordset.Operation.Like, "TS");
         r.OrderBy("FREQ", OrderDirection.Ascending);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            ACHMob.DataElement mask = new ACHMob.DataElement();
            mask.frequency = r.GetD("FREQ");
            mask.losses = r.GetD("ATTN");
            _maskaT.Add(mask);
         }
         r.Close();
         r.Destroy();




   
         //LOG.Warning("достаю маску прийомника" + "\n");
         // filling the data of mask receiver
         
         r = new IMRecordset(table_eqipment, IMRecordset.Mode.ReadOnly);
         r.Select("EQUIP_ID,TYPE,FREQ,ATTN");
         r.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, _rxequipID);
         r.SetWhere("TYPE", IMRecordset.Operation.Like, "RF");
         r.OrderBy("FREQ", OrderDirection.Ascending);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            ACHMob.DataElement mask = new ACHMob.DataElement();
            mask.frequency = r.GetD("FREQ");
            mask.losses = r.GetD("ATTN");
            _maskaR.Add(mask);
         }
         r.Close();
         r.Destroy();





         // filling the ids of filter receiver and transmitter
         r = new IMRecordset(PlugTbl._itblXnrfaStatExfltr, IMRecordset.Mode.ReadOnly);
         r.Select("ID,OBJ_ID,OBJ_TABLE,EX_FILTER_ID,IS_TX");
         r.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, _tableID);
         r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, table_name);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            int t = r.GetI("IS_TX");
            if (t > 0)
            {
                _exfilterID = r.GetI("EX_FILTER_ID");
            }
            else
            {
                _rexfilterID = r.GetI("EX_FILTER_ID");
            }
         }
         r.Close();
         r.Destroy();


         
         //LOG.Warning("достаю фильтр передатчика" + "\n");
         // filling the params of filter transmitter
         r = new IMRecordset(PlugTbl._itblXnrfaFilterParam, IMRecordset.Mode.ReadOnly);
         r.Select("EXT_FILT_ID,DELTA_FREQ,LOSS");
         r.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _exfilterID);
         r.OrderBy("DELTA_FREQ", OrderDirection.Ascending);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            ACHMob.DataElement filter = new ACHMob.DataElement();
            filter.frequency = r.GetD("DELTA_FREQ");
            filter.losses = r.GetD("LOSS");
            _filterT.Add(filter);
         }
         r.Close();
         r.Destroy();



         
         //LOG.Warning("достаю фильтр прийомника" + "\n");
         // filling the params of filter receiver
         r = new IMRecordset(PlugTbl._itblXnrfaFilterParam, IMRecordset.Mode.ReadOnly);
         r.Select("EXT_FILT_ID,DELTA_FREQ,LOSS");
         r.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _rexfilterID);
         r.OrderBy("DELTA_FREQ", OrderDirection.Ascending);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            ACHMob.DataElement filter = new ACHMob.DataElement();
            filter.frequency = r.GetD("DELTA_FREQ");
            filter.losses = r.GetD("LOSS");
            _filterR.Add(filter);
         }
         r.Close();
         r.Destroy();



         
         //LOG.Warning("достаю частоту передатчика" + "\n");
         r = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
         r.Select("ID,FREQ");
         r.SetWhere("ID", IMRecordset.Operation.Eq, _exfilterID);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            _freqFilterTX = r.GetD("FREQ");
         }
         r.Close();
         r.Destroy();




       
         //LOG.Warning("достаю частоту прийомника" + "\n");
         r = new IMRecordset(PlugTbl._itblXnrfaExternFilter, IMRecordset.Mode.ReadOnly);
         r.Select("ID,FREQ");
         r.SetWhere("ID", IMRecordset.Operation.Eq, _rexfilterID);
         for (r.Open(); !r.IsEOF(); r.MoveNext())
         {
            _freqFilterRX = r.GetD("FREQ");
         }
         r.Close();
         r.Destroy();




         
         //Заглушка и если станция СРТ попадает в диапазон 1980-2000МГц и не имеет внешнего фильтра. То ставим туду фильтр по умолчанию.
          /*
         Double freq_for_chek_filtr = 0;
         if (STFrequncyTX.Count>0)
         {
             freq_for_chek_filtr = STFrequncyTX[0];
         }
         if ((table_name == ICSMTbl.itblMobStation2) && (freq_for_chek_filtr >= 1980) && (freq_for_chek_filtr<=2000) && (_filterT.Count == 0))
         {
             ACHMob.DataElement mask = new ACHMob.DataElement();
             mask.frequency = -60;
             mask.losses = 60;
             _filterT.Add(mask);
             mask.frequency = -1.8;
             mask.losses = 60;
             _filterT.Add(mask);
             mask.frequency = 0;
             mask.losses = 0;
             _filterT.Add(mask);
             mask.frequency = 20;
             mask.losses = 0;
             _filterT.Add(mask);
             _freqFilterTX = 1980;
         }
           */ 
      }

      #endregion



      /// <summary>
      /// Загружем дополнительные данные о станции
      /// </summary>
      public void LoadAdditionalData()
      {
          Chennels.Clear();
          bool isFirst = true;
          IMRecordset r = new IMRecordset(_table, IMRecordset.Mode.ReadOnly);
          r.Select("ID,Position.REMARK,Owner.NAME,DESIG_EMISSION");
          r.Select("AssignedFrequencies.ChannelTx.CHANNEL");
          r.Select("AssignedFrequencies.ChannelRx.CHANNEL");
          r.Select("AssignedFrequencies.TX_FREQ");
          r.Select("AssignedFrequencies.RX_FREQ");
          r.Select("Position.ADDRESS");
          r.Select("Position.PROVINCE");
          r.Select("Position.SUBPROVINCE");
          r.Select("Position.POSTCODE");
          r.Select("Position.CITY");
          r.Select("Position.TOWER");
          r.Select("Position.CUST_TXT1");
          r.Select("STATUS,NAME");
          r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
          for(r.Open();!r.IsEOF();r.MoveNext())
          {
              if (isFirst == true)
              {
                  isFirst = false;
                  OwnerName = r.GetS("Owner.NAME");
                  //Address = r.GetS("Position.REMARK");
                  DesEmi = r.GetS("DESIG_EMISSION");
                  Status = r.GetS("STATUS");
                  StationName = r.GetS("NAME");

                  // сборка данных Область, район, населенный пункт, адрес
                  String Temp = "";

                  //if (r.GetS("Position.POSTCODE").Trim() != "")
                  //{
                  //    Temp = r.GetS("Position.POSTCODE");
                  //}

                  if ((r.GetS("Position.PROVINCE").Trim() != "") && (r.GetS("Position.PROVINCE").Trim() != "АР Крим") && (r.GetS("Position.PROVINCE").Trim() != "Севастополь") && (r.GetS("Position.PROVINCE").Trim() != "Київ"))
                  {
                      Temp =  r.GetS("Position.PROVINCE") + " обл.";
                  }
                  //else if ((rs.GetS("Owner.PROVINCE").Trim() != "") && ((rs.GetS("Owner.PROVINCE").Trim() == "Київська") || (rs.GetS("Owner.PROVINCE").Trim() == "Севастополь") || (rs.GetS("Owner.PROVINCE").Trim() == "Київ") || (rs.GetS("Owner.PROVINCE").Trim() == "АР Крим")))
                  //{

                  //}
                  else if ((r.GetS("Position.PROVINCE").Trim() != "") && (r.GetS("Position.PROVINCE").Trim() == "АР Крим"))
                  {
                      Temp = Temp + ", " + r.GetS("Position.PROVINCE");
                  }
                  if (r.GetS("Position.SUBPROVINCE").Trim() != "")
                  {
                      if (r.GetS("Position.PROVINCE").Trim() != "")
                      {
                          Temp = Temp + ", " + r.GetS("Position.SUBPROVINCE") + " р-н";
                      }
                      if (r.GetS("Position.PROVINCE").Trim() == "")
                      {
                          Temp = Temp + r.GetS("Position.SUBPROVINCE") + " р-н";
                      }
                  }
                  if (r.GetS("Position.CITY").Trim() != "")
                  {
                      if ((r.GetS("Position.SUBPROVINCE").Trim() == "") && (Temp.Trim() != ""))
                      {
                          Temp = Temp + ", м." + r.GetS("Position.CITY") + "";
                      }
                      if ((r.GetS("Position.SUBPROVINCE").Trim() == "") && (Temp.Trim() == ""))
                      {
                          Temp = Temp + "м." + r.GetS("Position.CITY") + "";
                      }

                      if ((r.GetS("Position.SUBPROVINCE").Trim() != "") && (Temp.Trim() != ""))
                      {
                          Temp = Temp + ", с." + r.GetS("Position.CITY") + "";
                      }
                      if ((r.GetS("Position.SUBPROVINCE").Trim() != "") && (Temp.Trim() == ""))
                      {
                          Temp = Temp + ", с." + r.GetS("Position.CITY") + "";
                      }
                  }
                  if (r.GetS("Position.ADDRESS").Trim() != "")
                  {
                      if (Temp.Trim() != "")
                      {
                          Temp = Temp + ", " + r.GetS("Position.ADDRESS");
                      }
                      if (Temp.Trim() == "")
                      {
                          Temp = r.GetS("Position.ADDRESS");
                      }
                  }

                  if ((r.GetS("Position.TOWER").Trim() != "будівля") && (!string.IsNullOrEmpty(r.GetS("Position.TOWER")))) Temp += ", " + r.GetS("Position.TOWER");

                  

                  Address = Temp;



              }
              Channel cnl = new Channel();
              cnl.ChannelTx = r.GetS("AssignedFrequencies.ChannelTx.CHANNEL");
              cnl.ChannelRx = r.GetS("AssignedFrequencies.ChannelRx.CHANNEL");
              cnl.FreqTx = r.GetD("AssignedFrequencies.TX_FREQ");
              cnl.FreqRx = r.GetD("AssignedFrequencies.RX_FREQ");
              Chennels.Add(cnl);
          }
          if (r.IsOpen())
              r.Close();
          r.Destroy();
      }
      /// <summary>
      /// Копирует дополнителоьные данные о станции из другой станции
      /// </summary>
      /// <param name="src">Станция, из которой необходимо скопировать данные</param>
      public void CopyAdditionalData(Station src)
      {
          OwnerName = src.OwnerName;
          Address = src.Address;
          DesEmi = src.DesEmi;
          Status = src.Status;
          StationName = src.StationName;
          Chennels = Chennels;
      }
   }
}
