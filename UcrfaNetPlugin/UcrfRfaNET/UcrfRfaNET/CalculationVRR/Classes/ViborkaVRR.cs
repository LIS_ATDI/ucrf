﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSPosition;
using ICSM;
using LisUtility;
using Distribution;
using Diagramm;
using LisUtility.Mob;
using XICSM.Intermodulation;
using System.Windows.Media;
using ComponentsLib;
using System.Collections;
using XICSM.UcrfRfaNET.Calculation.ResultForm;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    partial class ViborkaVRR
    {
        #region MEMBERS
        public struct PodborResult
        {
            public double FREQ;   //Частота (МГц)
            public double PIP;    //Помехи для станции
            public double PIZ;    //Помехи от станции
            public double PIPA;   //Помеха абонентам станции (абоненти расматриваемой станции - помеха станции из выборки)
            public double PIZA;   //Помеха абонентам от станции (абоненти станции из выборки - помеха расматриваемой станции)
        }
        public enum TypeOfFrequency // тип розрахунку по частоті
        {
            Zhertva, // жертва
            Pomeha // помеха
        }
        private List<int> _ListZhertvaMob = new List<int>(); // тут будут храниться станции жертвы из таблицы MOB                                               (1)
        private List<int> _ListPomehaMob = new List<int>(); // тут будут храниться станции плмехи из таблицы MOB                                                (2)
        private List<int> _ListZhertvaBlock = new List<int>(); // тут будут храниться станции жертвы блокировки из таблицы MOB                                  (3)
        private List<int> _ListPomehaBlock = new List<int>(); // тут будут храниться помехи блокировки из таблицы MOB                                           (4)
        private List<int> _ListPomehaMobSRT = new List<int>(); //вводится специально для диапазона 1980-2000 как станции источники помех таблица СРТ            (5)
        private List<int> _ListPomehaBlockSRT = new List<int>(); //вводится специально для диапазона 1980-2000 как станции источники помех СРТ                  (6)
        private List<int> _ListPomehaIntermodSRT = new List<int>(); //вводится специально для диапазона 1980-2000 как станции источники помех СРТ               (7)
        private List<int> _ListZhertvaAbonent = new List<int>(); 
        private List<int> _ListPomehaAbonent = new List<int>();
        private List<int> _ListPodbor = new List<int>();
        private List<Station> _stListPodbor = new List<Station>();
        private List<Station> _stListZhertvaMob = new List<Station>(); //                                                                                       (1)
        private List<List<Station>> _stListZhertvaMobSectors = new List<List<Station>>();//                                                                     (1)   
        private List<Station> _stListPomehaMob = new List<Station>();//                                                                                         (2)  
        private List<List<Station>> _stListPomehaMobSectors = new List<List<Station>>();//                                                                      (2)
        private List<Station> _stListZhertvaBlock = new List<Station>();//                                                                                      (3)
        private List<List<Station>> _stListZhertvaBlockSectors = new List<List<Station>>();//                                                                   (3)
        private List<Station> _stListPomehaBlock = new List<Station>();//                                                                                       (4)
        private List<List<Station>> _stListPomehaBlockSectors = new List<List<Station>>();//                                                                    (4)
        
        private List<Station> _stListPomehaMobSRT = new List<Station>(); //                                                                                     (5)
        private List<List<Station>> _stListPomehaMobSRTSectors = new List<List<Station>>();  //                                                                 (5)
        private List<Station> _stListPomehaBlockSRT = new List<Station>(); //                                                                                   (6)
        private List<List<Station>> _stListPomehaBlockSRTSectors = new List<List<Station>>(); //                                                                (6)
        private List<Station> _stListPomehaIntermodSRT = new List<Station>();//                                                                                 (7)

        private List<IntermodulationInterference> _stListZhertvaInter = new List<IntermodulationInterference>();//станции жертвы интермодуляции из таблицы MOB  (8)
        private List<List<IntermodulationInterference>> _stListZhertvaInterSector = new List<List<IntermodulationInterference>>();//                            (8)                                  
        private List<IntermodulationInterference> _stListPomehaInter = new List<IntermodulationInterference>();//станции помехи интермодуляции из таблицы MOB   (9)
        private List<List<IntermodulationInterference>> _stListPomehaInterSector = new List<List<IntermodulationInterference>>();//                             (9)



        private List<int> _ListZhertvaInterMOB_ = new List<int>();//                                                                                            (10)
        private List<int> _ListPomehaInterMOB_ = new List<int>();//                                                                                             (11)
        private List<Station> _stListZhertvaInterMOB_ = new List<Station>();// Жертвы от интермодуляции используется для расчетов с СРТ                         (10)
        private List<Station> _stListPomehaInterMOB_ = new List<Station>();//  Помехи    интермодуляции используется для расчетов с СРТ                         (11)

        private List<Station> _stListZhertvaAbonent = new List<Station>();
        private List<List<Station>> _stListZhertvaAbonentSectors = new List<List<Station>>();
        private List<Station> _stListPomehaAbonent = new List<Station>();
        private List<List<Station>> _stListPomehaAbonentSectors = new List<List<Station>>();
        private List<Station> _stListZhertvaMOBandAbonent = new List<Station>();
        private List<List<Station>> _stListZhertvaMOBandAbonentSector = new List<List<Station>>();
        private List<Station> _stListPomehaMOBandAbonent = new List<Station>();
        private List<List<Station>> _stListPomehaMOBandAbonentSector = new List<List<Station>>();

        private List<Station> _curStation;

        private List<TreeColumnRowBase> lstShowingData_SRT_intermod_2_1;
        private List<TreeColumnRowBase> lstShowingData_SRT_intermod_1_1_1;
        private List<TreeColumnRowBase> lstShowingData_SRT_MOB_intermod_2_1;
        private List<TreeColumnRowBase> lstShowingData_SRT_MOB_intermod_1_1_1;
        private List<TreeColumnRowBase> lstShowingData_MOB_to_SRT_intermod_2_1; // лист с интермодуляционной помехой. Станции МОБ создают помеху станции СРТ. Расчет из СРТ.
        private List<TreeColumnRowBase> lstShowingData_MOB_to_SRT_intermod_1_1_1; // лист с интермодуляционной помехой. Станции МОБ создают помеху станции СРТ. Расчет из СРТ.

        private double _radiusMob;
        private double _radiusInter;
        private double _radiusBlock;
        private double _freqInterMax;
        private double _freqInterMin;
        private double _PIMob;
        private double _PIInter;
        private double _PIBlock;
        private double _G;
        private double? _R;
        private bool _needMOB;
        private bool _needAbonent;
        private bool _needInter;
        private bool _needBlock;
        private bool _needPRD;
        private bool _needPRM;
        private bool _needOwner;
        private bool _podborAbonent;
        private bool _needSRT;
        private bool _needFreq;
        private bool _need_1_1_1;
        private double _podborRoznos;
        private double _podborStep;
        private int countRecord = 0;

        #endregion

        ////////////////////////////////////////////////////////////////////////////
        // CONSTRUCTOR
        ////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Конструктор
        /// </summary>
        public ViborkaVRR(List<Station> currentStation, double radiusMob, double radiusInter,
           double radiusBlock, double freqInterMax, double freqInterMin, double PIMob,
           double PIInter, double PIBlock, double G, double? R, bool needMOB,
           bool needAbonent, bool needInter, bool needBlock, bool needPRD, bool needPRM, bool needOwner, bool needSRT)
        {
            _curStation = currentStation;
            _radiusMob = radiusMob;
            _radiusInter = radiusInter;
            _radiusBlock = radiusBlock;

            //_freqDiapazonMob = freqDiapazonMob;
            _freqInterMax = Math.Max(freqInterMax, freqInterMin);
            _freqInterMin = Math.Min(freqInterMax, freqInterMin);
            //_freqDiapazonBlock = freqDiapazonBlock;

            _PIMob = PIMob;
            _PIInter = PIInter;
            _PIBlock = PIBlock;

            _G = G;
            _R = R;

            _needMOB = needMOB;
            _needAbonent = needAbonent;
            _needInter = needInter;
            _needBlock = needBlock;
            _needPRD = needPRD;
            _needPRM = needPRM;
            _needOwner = needOwner;
            _needSRT = needSRT;

        }

        public ViborkaVRR(Station currentStation, double podborMin, double podborMax,
           double podborStep, double PodborRoznos, double radius, bool needOwner, bool podborAbonent)
        {
            _curStation = new List<Station>();
            _curStation.Add(currentStation);
            _radiusMob = radius;
            _freqInterMax = Math.Max(podborMax, podborMin);
            _freqInterMin = Math.Min(podborMax, podborMin);
            _podborRoznos = PodborRoznos;
            _podborStep = podborStep;
            _needFreq = true;
            _needOwner = needOwner;
            _podborAbonent = podborAbonent;
        }
        public ViborkaVRR(int[] _cur_station_SRT_id_list, Double _R_mob, Double _R_block, Double _R_interf, Double _porog, Boolean _chbCoch, Boolean _chbinter, Boolean _chbblock, Boolean _chb_1_1_1,Double freqMax, Double freqMin)
        { 
            // даный запуск идет если мы пускаем процедуру расчета из СРТ Т.е. из формы расчета для SRT была запущенна данная процедура
            _need_1_1_1 = _chb_1_1_1;
            _needMOB = _chbCoch;
            _needInter = _chbinter;
            _needBlock = _chbblock;
            _radiusMob = _R_mob;
            _radiusInter = _R_interf;
            _radiusBlock = _R_block;
            _freqInterMax = freqMax;//Частоты исключительно для мобильных станций, которые мы стремимся поместить в выборку
            _freqInterMin = freqMin;//Частоты исключительно для мобильных станций, которые мы стремимся поместить в выборку
            _PIMob = _porog;
            _curStation = new List<Station>();
            _needPRD = true;
            _needPRM = true;
            _needSRT = true;
            foreach (int id in _cur_station_SRT_id_list)
            {
                Station curStation = new Station(ICSMTbl.itblMobStation2 ,id);
                curStation.LoadAdditionalData();
                _curStation.Add(curStation);
            }
            // т.е. в данній момент все должно быть заполнено. 
        }

        #region CALCULATE

        /// <summary>
        /// Main calculation method
        /// </summary>
        public List<string[]> Calculate(int freqPlanId, string planName)
        {
            if (_curStation.Count == 0) //Нет станций
            {               
                return new List<string[]>();
            }
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculation...")))
            {
                pBar.UserCanceled();
                if (_needFreq)
                {
                    //Подбор частот
                    return CalculateFreq(freqPlanId, planName, _curStation[0]);
                }

                //ПОИСК станций, которые попадают в радиус
                if (_needMOB)
                    this.CalculateOnRadius(_curStation[0], Station.TypeOfStation.MOB);
                if (_needAbonent)
                    this.CalculateOnRadius(_curStation[0], Station.TypeOfStation.Abonent);
                if (_needBlock)
                    this.CalculateOnRadius(_curStation[0], Station.TypeOfStation.Block);
                if ((_needSRT)&&(_needMOB))
                    this.CalculateOnRadiusSRT(_curStation[0], Station.TypeOfStation.MOB, 1980, 2000); // в данном случае мы нашли станции СРТ которые попадают в диапазон частот и растояний храняться в переменной _ListPomehaMobSRT
                if ((_needSRT) && (_needBlock))
                    this.CalculateOnRadiusSRT(_curStation[0], Station.TypeOfStation.Block, 1980, 2000); // в данном случае мы нашли станции СРТ которые попадают в диапазон частот и растояний храняться в переменной _ListPomehaBlockSRT и _ListPomehaIntermodSRT

                for (int indexSector = 0; indexSector < _curStation.Count; indexSector++) // Дальше пошла работа по секторам т.е. по станциям 
                {
                    Station curSector = _curStation[indexSector];
                    //Обнуляем выбранные станции
                    _stListZhertvaMob = new List<Station>();
                    _stListPomehaMob = new List<Station>();
                    _stListZhertvaAbonent = new List<Station>();
                    _stListPomehaAbonent = new List<Station>();
                    _stListZhertvaBlock = new List<Station>();
                    _stListPomehaBlock = new List<Station>();
                    _stListZhertvaInter = new List<IntermodulationInterference>();
                    _stListPomehaInter = new List<IntermodulationInterference>();
                    _stListPomehaMobSRT = new List<Station>(); // новая переменная для СРТ 1980-2000
                    _stListPomehaIntermodSRT = new List<Station>();// новая переменная для СРТ 1980-2000
                    _stListPomehaBlockSRT = new List<Station>();// новая переменная для СРТ 1980-2000
                    //ЗАГРУЖАЕМ данные по выбраным станциям
                    this.FillingStations();  //заливается все только не заливаются станции дляч расчета интермодуляции 
                    //ПОДСЧЕТ
                    if (_needMOB)
                        CalculatePi(_stListZhertvaMob, _stListPomehaMob, curSector, Station.TypeOfStation.MOB); // Расчет помехи по основному и соседнему каналу 
                    if (_needBlock)
                        CalculatePi(_stListZhertvaBlock, _stListPomehaBlock, curSector, Station.TypeOfStation.Block); // Расчет помехи блокирования 
                    if (_needAbonent)
                        CalculatePi(_stListZhertvaAbonent, _stListPomehaAbonent, curSector, Station.TypeOfStation.Abonent); // Расчет помехи абонентам
                    if (_needInter) //расчет помех интермодуляции
                    {
                        if (_needPRM)
                        {
                            _stListZhertvaInter = CalculatePomehaInter(ICSMTbl.itblMobStation, curSector._tableID, _G, _PIInter, _freqInterMin, _freqInterMax, -1000, curSector._ktbf + _PIMob, _radiusInter, 1, 130);
                        }
                        if (_needPRD)
                        {
                            _stListPomehaInter = CalculatePomehaInter(ICSMTbl.itblMobStation, curSector._tableID, _G, _PIInter, _freqInterMin, _freqInterMax, -1000, curSector._ktbf + _PIMob, _radiusInter, 0, 130);
                        }
                    }
                    // Расчет санций СРТ по соседнему каналу 
                    if ((_needMOB) && (_needSRT))
                    {
                        CalculatePi(_stListZhertvaMob, _stListPomehaMobSRT, curSector, Station.TypeOfStation.MOB); // Расчет помехи по основному и соседнему каналу 
                    }
                    // Расчет санций СРТ блокировка
                    if ((_needBlock) && (_needSRT))
                    {
                        CalculatePi(_stListZhertvaBlock, _stListPomehaBlockSRT, curSector, Station.TypeOfStation.Block); // Расчет помехи по основному и соседнему каналу 
                    }
                    // Дальше происходит чистка станций
                    if (_needMOB)
                    {
                        if (_needPRM)
                            this.CheckOnAllowablePI(ref _stListZhertvaMob, _PIMob);
                        if (_needPRD)
                            this.CheckOnAllowablePI(ref _stListPomehaMob, _PIMob);
                    }
                    if (_needAbonent)
                    {
                        if (_needPRM)
                            this.CheckOnAllowablePI(ref _stListZhertvaAbonent, _PIMob);
                        if (_needPRD)
                            this.CheckOnAllowablePI(ref _stListPomehaAbonent, _PIMob);
                    }
                    if (_needBlock)
                    {
                        if (_needPRM)
                        {
                            this.CheckOnAllowableMinAndMaxFreq(_stListZhertvaBlock);
                            this.CheckOnAllowablePI(ref _stListZhertvaBlock, _PIBlock);
                        }
                        if (_needPRD)
                        {
                            this.CheckOnAllowableMinAndMaxFreq(_stListPomehaBlock);
                            this.CheckOnAllowablePI(ref _stListPomehaBlock, _PIBlock);
                        }
                    }
                    
                    // Чистим станции СРТ
                    if ((_needMOB)&&(_needSRT))
                    {
                        if (_needPRD)
                            this.CheckOnAllowablePI(ref _stListPomehaMobSRT, _PIMob);
                    }
                    if ((_needBlock) && (_needSRT))
                    {
                        if (_needPRD)
                        {
                            this.CheckOnAllowableMinAndMaxFreq(_stListPomehaBlockSRT);
                            this.CheckOnAllowablePI(ref _stListPomehaBlockSRT, _PIBlock);
                        }
                     }
                    //Сохраняем подсчитанные данные
                    _stListZhertvaMobSectors.Add(_stListZhertvaMob);
                    _stListPomehaMobSectors.Add(_stListPomehaMob);
                    _stListPomehaMobSRTSectors.Add(_stListPomehaMobSRT); // Для станций СРТ диапазона 1980-2000МГц
                    _stListZhertvaBlockSectors.Add(_stListZhertvaBlock);
                    _stListPomehaBlockSectors.Add(_stListPomehaBlock);
                    _stListPomehaBlockSRTSectors.Add(_stListPomehaBlockSRT);// Для станций СРТ диапазона 1980-2000МГц
                    _stListZhertvaInterSector.Add(_stListZhertvaInter);
                    _stListPomehaInterSector.Add(_stListPomehaInter);
                    _stListZhertvaAbonentSectors.Add(_stListZhertvaAbonent);
                    _stListPomehaAbonentSectors.Add(_stListPomehaAbonent);
                }
                // Расчет санций СРТ интермодуляция 
                if ((_needInter) && (_needSRT))
                {
                    SRT_intermodulation SRT_inter = new SRT_intermodulation(_curStation, _stListPomehaIntermodSRT, _PIMob);
                    lstShowingData_SRT_intermod_2_1 = SRT_inter.calc_intermod_2_1_for_MOB();
                    lstShowingData_SRT_intermod_1_1_1 = SRT_inter.calc_intermod_1_1_1_for_MOB();
                }
                return new List<string[]>();
            }
        }
        /// <summary>
        /// Функция для расчета помех(основной и соседний канал, блокирование, интермодуляция). Помехи считаются между станциями таблиц Mob_station и Mob_station2
        /// </summary>
        /// <param name="f_min_srt">Минимальная частота станций находящихся в таблице Mob_station2, МГц</param>
        /// <param name="f_max_srt">Максимальная частота станций находящихся в таблице Mob_station2, МГц</param>
        public void Calculate_mob_srt(Double f_min_srt, Double f_max_srt)
        { 
            //Ну это отдельная функция для запуска расчетов непосредственно для определения помех между СРТ и МОБ Запуск функции инициирован со стороны SRT
            if (_curStation.Count == 0) //Нет станций
            {
                return;
            }
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculation...")))
            {
                pBar.UserCanceled();
                //ПОИСК станций, которые попадают в радиус
                if (_needMOB)
                    this.CalculateOnRadius(_curStation[0], Station.TypeOfStation.MOB);
                if (_needBlock || _needInter)
                    this.CalculateOnRadius(_curStation[0], Station.TypeOfStation.Block);

                if (_needInter) // т.е. если всеже нужны и станции СРТ которые мешают
                {
                    this.CalculateOnRadiusSRT(_curStation[0], Station.TypeOfStation.Block, f_min_srt, f_max_srt); // в данном случае мы нашли станции СРТ которые попадают в диапазон частот и растояний храняться в переменной _ListPomehaBlockSRT и _ListPomehaIntermodSRT
                    //итак для расчетов интермодуляции жертвы храниться здесь _ListZhertvaBlock а помехи єто текущая станция и _ListPomehaIntermodSRT
                }
                for (int indexSector = 0; indexSector < _curStation.Count; indexSector++) // Дальше пошла работа по секторам т.е. по станциям 
                {
                    Station curSector = _curStation[indexSector];
                    //Обнуляем выбранные станции
                    _stListZhertvaMob = new List<Station>();
                    _stListPomehaMob = new List<Station>();
                    _stListZhertvaAbonent = new List<Station>();
                    _stListPomehaAbonent = new List<Station>();
                    _stListZhertvaBlock = new List<Station>(); // а для расчета интермодуляци тут будут храниться 
                    _stListPomehaBlock = new List<Station>();
                    _stListZhertvaInter = new List<IntermodulationInterference>();
                    _stListPomehaInter = new List<IntermodulationInterference>();
                    _stListZhertvaInterMOB_ = new List<Station>(); // новая переменная                  
                    _stListPomehaMobSRT = new List<Station>(); // новая переменная для СРТ 1980-2000
                    _stListPomehaIntermodSRT = new List<Station>();// новая переменная для СРТ 1980-2000
                    _stListPomehaBlockSRT = new List<Station>();// новая переменная для СРТ 1980-2000
                    _stListPomehaInterMOB_ = new List<Station>();// новая переменная для станций мобильной связи и помех для СРТ
                    _stListZhertvaInterMOB_ = new List<Station>();// новая переменная для станций мобильной связи и помех от СРТ

                    //ЗАГРУЖАЕМ данные по выбраным станциям
                    this.FillingStations();  //заливается все только не заливаются станции дляч расчета интермодуляции 
                    // Нужна функция по перфорации частот
                  
                    this.dell_freq_for_calc();
                    //

                    //ПОДСЧЕТ
                    if (_needMOB)
                        CalculatePi(_stListZhertvaMob, _stListPomehaMob, curSector, Station.TypeOfStation.MOB); // Расчет помехи по основному и соседнему каналу 
                    if (_needBlock)
                        CalculatePi(_stListZhertvaBlock, _stListPomehaBlock, curSector, Station.TypeOfStation.Block); // Расчет помехи блокирования 
                    
                    // Дальше происходит чистка станций
                    if (_needMOB)
                    {
                        this.CheckOnAllowablePI(ref _stListZhertvaMob, _PIMob);
                        this.CheckOnAllowablePI(ref _stListPomehaMob, _PIMob);
                    }
                    if (_needBlock)
                    {
                        this.CheckOnAllowableMinAndMaxFreq(_stListZhertvaBlock);
                        this.CheckOnAllowablePI(ref _stListZhertvaBlock, 60);
                        this.CheckOnAllowableMinAndMaxFreq(_stListPomehaBlock);
                        this.CheckOnAllowablePI(ref _stListPomehaBlock, 60);
                    }
                    //Сохраняем подсчитанные данные
                    _stListZhertvaMobSectors.Add(_stListZhertvaMob);
                    _stListPomehaMobSectors.Add(_stListPomehaMob);
                    _stListZhertvaBlockSectors.Add(_stListZhertvaBlock);
                    _stListPomehaBlockSectors.Add(_stListPomehaBlock);
                }
                // Расчет санций СРТ интермодуляция 
                if ((_needInter))
                {
                    // сначала производится расчет для случая когда станция текущая и станции из выборки СРТ создают помеху некой станции из таблици MOB
                    SRT_intermodulation SRT_inter = new SRT_intermodulation(_curStation, _stListPomehaIntermodSRT, _stListZhertvaInterMOB_, _PIMob);
                    lstShowingData_SRT_MOB_intermod_2_1 = SRT_inter.calc_intermod_2_1_for_MOB_SRT();
                    if (_need_1_1_1) { lstShowingData_SRT_MOB_intermod_1_1_1 = SRT_inter.calc_intermod_1_1_1_for_MOB_SRT(); }
                    //расчет производится для такого случая когда станции МОБ создают помеху для станций СРТ т.е. жертва наша станций
                    SRT_intermodulation SRT_inter_ = new SRT_intermodulation(_curStation, _stListPomehaInterMOB_, _PIMob);
                    lstShowingData_MOB_to_SRT_intermod_2_1 = SRT_inter_.calc_intermod_2_1_for_MOB();
                    if (_need_1_1_1) { lstShowingData_MOB_to_SRT_intermod_1_1_1 = SRT_inter_.calc_intermod_1_1_1_for_MOB(); }
                }
                return;
            }
        }

        #region RADIUS
        /// <summary>
        /// checks the stations on value of radius and frequency
        /// </summary>
        private void CalculateOnRadius(Station curStat, Station.TypeOfStation typeOfSearch)
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculation VRR")))
            {
                pBar.ShowBig(CLocaliz.TxT("Search stations ..."));
                double radius = 0;
                if (typeOfSearch == Station.TypeOfStation.MOB || typeOfSearch == Station.TypeOfStation.Abonent)
                    radius = _radiusMob;
                if (typeOfSearch == Station.TypeOfStation.Block)
                    radius = _radiusBlock;

                RecordPos positionRadius = Position.GetRangeCoordinates(curStat._longitude,
                   curStat._latitude, radius, "Position.X", "Position.Y");

                string sqlQuery = string.Format("(({2} < {0}) AND ({0} < {3})) OR (({2} < {1}) AND ({1} < {3}))", "[AssignedFrequencies.TX_FREQ]", "[AssignedFrequencies.RX_FREQ]", _freqInterMin.ToString().Replace(',', '.'), _freqInterMax).ToString().Replace(',', '.');

                IMRecordset r = new IMRecordset(ICSMTbl.itblMobStation, IMRecordset.Mode.ReadOnly);
                r.Select("ID,TX_LOSSES,TX_ADDLOSSES,RX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,OWNER_ID");
                r.Select("STATUS");
                r.Select("STANDARD");
                r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                r.SetAdditional(sqlQuery);
                r.OrderBy("ID", OrderDirection.Ascending);

                int lastTablID = 0;
                int lastTablZ = 0;
                int lastTablP = 0;

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                        continue; //Отбрасываем ненужные станции

                    double freqTX = r.GetD("AssignedFrequencies.TX_FREQ");
                    double freqRX = r.GetD("AssignedFrequencies.RX_FREQ");
                    double bwTX = r.GetD("BW");
                    int tableID = r.GetI("ID");
                    int ownerID = r.GetI("OWNER_ID");

                    //if (tableID == 3917)
                    //{
                    //_ListZhertvaMob.Add(tableID);
                    //_ListPomehaMob.Add(tableID);
                    //}

                    if (curStat._tableID == tableID)
                        continue;

                    string standard = r.GetS("STANDARD");
                    if((curStat._standart == CRadioTech.GSM_1800) || (curStat._standart == CRadioTech.GSM_900))
                    {
                        if((standard == CRadioTech.GSM_1800) || (standard == CRadioTech.GSM_900))
                            continue;
                    }

                    if (_needOwner)
                        if (curStat._ownerID == ownerID)
                            continue;

                    if (lastTablID != tableID) // на станції передатчиків чи приймачів може бути багато
                    {
                        if (_needFreq)
                        {
                            bool exit = false;
                            if (CalculateOnFrequencyPodbor(freqTX, Math.Max(curStat._bwTX, bwTX), ref exit))
                            {
                                lastTablID = tableID;
                                _ListPodbor.Add(tableID);
                            }
                            else
                            {
                                if (exit)
                                    lastTablID = tableID;
                            }

                            continue;
                        }

                        if (typeOfSearch == Station.TypeOfStation.Abonent)
                        {
                            bool exit = false;
                            if (CalculateOnFrequency(freqTX, curStat._bwTX, false, standard, curStat, ref exit))
                            {
                                lastTablID = tableID;
                                if (_needPRM)
                                    _ListZhertvaAbonent.Add(tableID);
                                if (_needPRD)
                                    _ListPomehaAbonent.Add(tableID);
                            }
                            else
                            {
                                if (exit)
                                    lastTablID = tableID;
                            }

                            continue;
                        }

                        if (typeOfSearch == Station.TypeOfStation.Block)
                        {
                            if (_needPRM)
                            {
                                if (lastTablZ != tableID)
                                {
                                    bool exit = false;
                                    if (CalculateOnFrequencyBlock(freqRX, curStat._bwRX, ref exit))
                                    {
                                        lastTablZ = tableID;
                                        _ListZhertvaBlock.Add(tableID);
                                        _ListZhertvaInterMOB_.Add(tableID);
                                    }
                                    else
                                    {
                                        if (exit)
                                            lastTablZ = tableID;
                                    }
                                }
                            }

                            if (_needPRD)
                            {
                                if (lastTablP != tableID)
                                {
                                    bool exit = false;
                                    if (CalculateOnFrequencyBlock(freqTX, curStat._bwTX, ref exit))
                                    {
                                        lastTablP = tableID;
                                        _ListPomehaBlock.Add(tableID);
                                        _ListPomehaInterMOB_.Add(tableID);
                                        if (lastTablZ == lastTablP)
                                            lastTablID = tableID;
                                    }
                                    else
                                    {
                                        if (exit)
                                            lastTablP = tableID;
                                        if (lastTablZ == lastTablP)
                                            lastTablID = tableID;
                                    }
                                }
                            }
                        }

                        if (_needPRM)
                        {
                            if (lastTablZ != tableID)
                            {
                                bool exit = false;
                                if (CalculateOnFrequency(freqRX, curStat._bwRX, false, standard, curStat, ref exit))
                                {
                                    //lastTablID = tableID;
                                    lastTablZ = tableID;
                                    _ListZhertvaMob.Add(tableID);
                                }
                                else
                                {
                                    if (exit)
                                        lastTablZ = tableID;
                                }
                            }
                        }

                        if (_needPRD)
                        {
                            if (lastTablP != tableID)
                            {
                                bool exit = false;
                                if (CalculateOnFrequency(freqTX, curStat._bwTX, true, standard, curStat, ref exit))
                                {
                                    //lastTablID = tableID;
                                    lastTablP = tableID;
                                    _ListPomehaMob.Add(tableID);
                                    if (lastTablZ == lastTablP)
                                        lastTablID = tableID;
                                }
                                else
                                {
                                    if (exit)
                                        lastTablP = tableID;
                                    if (lastTablZ == lastTablP)
                                        lastTablID = tableID;
                                }
                            }
                        }
                    }

                    pBar.ShowSmall(countRecord++);
                    pBar.UserCanceled();
                }
            }
        }

        /// <summary>
        /// Функция для создание выборки станций СРТ в диапазоне 1980-2000 МГц предположительно источник помехи станция СРТ
        /// </summary>
        /// <param name="curStat">анализируемая станция</param>
        /// <param name="typeOfSearch">тип радиуса для поиска - основной и соседний канал или блокирование или интермодуляция</param>
        private void CalculateOnRadiusSRT(Station curStat, Station.TypeOfStation typeOfSearch, Double fmin, Double fmax)
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculation SRT")))
            {
                pBar.ShowBig(CLocaliz.TxT("Search stations ..."));
                double radius = 0;
                if (typeOfSearch == Station.TypeOfStation.MOB)
                    radius = _radiusMob;
                if (typeOfSearch == Station.TypeOfStation.Block)
                    radius = _radiusBlock;

                RecordPos positionRadius = Position.GetRangeCoordinates(curStat._longitude,
                   curStat._latitude, radius, "Position.X", "Position.Y");

                string sqlQuery = string.Format("(({2} < {0}) AND ({0} < {3})) OR (({2} < {1}) AND ({1} < {3}))", "[AssignedFrequencies.TX_FREQ]", "[AssignedFrequencies.RX_FREQ]", _freqInterMin.ToString().Replace(',', '.'), _freqInterMax).ToString().Replace(',', '.');

                IMRecordset r = new IMRecordset(ICSMTbl.MobStation2, IMRecordset.Mode.ReadOnly);
                r.Select("ID,TX_LOSSES,TX_ADDLOSSES,RX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW,AssignedFrequencies.TX_FREQ,AssignedFrequencies.RX_FREQ,OWNER_ID");
                r.Select("STATUS");
                r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                r.SetAdditional(sqlQuery);
                r.OrderBy("ID", OrderDirection.Ascending);

                int lastTablID = 0;


                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                        continue; //Отбрасываем ненужные станции

                    double freqTX = r.GetD("AssignedFrequencies.TX_FREQ");
                    int tableID = r.GetI("ID");

                    //if (tableID == 3917)
                    //{
                    //_ListZhertvaMob.Add(tableID);
                    //_ListPomehaMob.Add(tableID);
                    //}

                    if (lastTablID != tableID) // на станції передатчиків чи приймачів може бути багато
                    {
                        if (typeOfSearch == Station.TypeOfStation.Block) //заливка для станций интермодуляции и блокировки
                        {
                                    if ((freqTX>=fmin) && (freqTX<=fmax))
                                    {
                                        lastTablID = tableID;
                                        _ListPomehaBlockSRT.Add(tableID);
                                        _ListPomehaIntermodSRT.Add(tableID);
                                     }
                        }
                        if (typeOfSearch == Station.TypeOfStation.MOB) //заливка для станций помехи по соседнему каналу.
                        {
                                if ((freqTX >= fmin) && (freqTX <= fmax))
                                {
                                    lastTablID = tableID;
                                    _ListPomehaMobSRT.Add(tableID);
                                }
                            }
                    }
                    pBar.ShowSmall(countRecord++);
                    pBar.UserCanceled();
                }
            }
        }
        // checks on frequency
        private bool CalculateOnFrequencyPodbor(double freq, double bwTx, ref bool exit) // _bwANALIZ  ширина смуги пропускання передавача чи приймача
        {
            double predel1 = _freqInterMin - bwTx * 2; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
            double predel2 = _freqInterMax + bwTx * 2; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

            if (freq > predel1 && freq < predel2)
                return true;

            predel1 = _freqInterMin - bwTx * 2 - _podborRoznos; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
            predel2 = _freqInterMax + bwTx * 2 + _podborRoznos; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

            if (freq > predel1 && freq < predel2)
                return true;

            predel1 = _freqInterMin - 100; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
            predel2 = _freqInterMax + 100; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

            if (freq > predel1 && freq < predel2)
            {
                exit = false;
                return false;
            }
            else
            {
                exit = true;
                return false;
            }
        }

        /// <summary>
        /// Проверка по частотам
        /// </summary>
        /// <param name="freq">частота выбран. станции (Прием или передача)</param>
        /// <param name="bw">BW выбран. станции</param>
        /// <param name="isTx">true - freq = TX, иначе = RX</param>
        /// <param name="standard">стандарт выбран. станции</param>
        /// <param name="curStation">анализ. станция</param>
        /// <param name="exit">выход</param>
        /// <returns>true - Добавлять в анализ, иначе нет</returns>
        private bool CalculateOnFrequency(double freq, double bw, bool isTx, string standard, Station curStation, ref bool exit)
        {
            if (((standard == "УКХ") && (curStation._standart == standard)) || ((standard == "GSM-1800")&&(curStation._table == "MOB_STATION2")))//нововведение для LTE
            {
                //для УКХ
                int k = 1;
                //для СРТ = the table = mob_station2
                if ((standard == "GSM-1800") && (curStation._table == "MOB_STATION2")) { k = 3; }

                double[] freqs = (isTx) ? curStation.STFrequncyRX.ToArray() : curStation.STFrequncyTX.ToArray();
                double bwCur = (isTx) ? curStation._bwRX : curStation._bwTX;
                foreach (double fr in freqs)
                {
                    double predel1 = fr - Math.Max(0.0125, k*Math.Max(bwCur, bw)) - 0.0001; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
                    double predel2 = fr + Math.Max(0.0125, k*Math.Max(bwCur, bw)) + 0.0001; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;
                    if (freq > predel1 && freq < predel2)
                        return true;
                }
                return false;
            }
            else
            {
                double predel1 = _freqInterMin - bw / 2; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
                double predel2 = _freqInterMax + bw / 2; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

                if (freq > predel1 && freq < predel2)
                    return true;
                else
                {
                    predel1 = _freqInterMin - 100; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
                    predel2 = _freqInterMax + 100; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

                    if (freq > predel1 && freq < predel2)
                    {
                        exit = false;
                        return false;
                    }
                    else
                    {
                        exit = true;
                        return false;
                    }
                }
            }
        }
        // checks on frequency
        private bool CalculateOnFrequencyBlock(double freq, double bwTx, ref bool exit) // _bwANALIZ  ширина смуги пропускання передавача чи приймача
        {
            double predel1 = _freqInterMin - bwTx / 2; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
            double predel2 = _freqInterMax + bwTx / 2;//freq + _freqDiapazonBlock + (bwTx + bwRx) / 2;

            if (freq > predel1 && freq < predel2)
                return true;
            else
            {
                predel1 = _freqInterMin - 100; //freq - _freqDiapazonMob - (bwTx + bwRx) / 2;
                predel2 = _freqInterMax + 100; //freq + _freqDiapazonMob + (bwTx + bwRx) / 2;

                if (freq > predel1 && freq < predel2)
                {
                    exit = false;
                    return false;
                }
                else
                {
                    exit = true;
                    return false;
                }
            }
        }

        // checks on frequency
        //private bool CalculateOnFrequencyInter(double freq, TypeOfFrequency typ, double bwTx, double bwRx, Station curStat) // _bwANALIZ  ширина смуги пропускання передавача чи приймача
        //{
        //   double predel1 = freq - _freqDiapazonInter - (bwTx + bwRx) / 2;
        //   double predel2 = freq + _freqDiapazonInter + (bwTx + bwRx) / 2;

        //   if (typ == TypeOfFrequency.Zhertva)
        //   {
        //      foreach (double stantionFreq in curStat.STFrequncyTX)
        //      {
        //         if (stantionFreq > predel1 && stantionFreq < predel2)
        //            return true;
        //      }

        //      return false;
        //   }
        //   else
        //   {
        //      foreach (double stantionFreq in curStat.STFrequncyRX)
        //      {
        //         if (stantionFreq > predel1 && stantionFreq < predel2)
        //            return true;
        //      }

        //      return false;
        //   }
        //}
        /// <summary>
        /// Загружает данные о станциях, которые попали в радиус
        /// </summary>
        private void FillingStations()
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("FillingStations")))
            {
                pBar.ShowBig(CLocaliz.TxT("Filling stations by data...."));
                countRecord = 0;
                IEnumerable<int> distId;

                if (_needFreq)
                {
                    distId = _ListPodbor.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPodbor.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                    return;
                }

                //////////////////////////////////////////////////////////////////
                // MOB
                //////////////////////////////////////////////////////////////////
                if (_needPRM)
                {
                    distId = _ListZhertvaMob.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListZhertvaMob.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
                if (_needPRD)
                {
                    distId = _ListPomehaMob.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaMob.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }

                if ((_needSRT) && (_needPRD)) // это заливка мобильных станций для случая СРТ 1980-2000 МГц
                {
                    distId = _ListPomehaMobSRT.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation2, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaMobSRT.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
                //////////////////////////////////////////////////////////////////
                // ABONENT
                //////////////////////////////////////////////////////////////////
                distId = _ListZhertvaAbonent.Distinct();
                foreach (int id in distId)
                {
                    Station st = new Station(ICSMTbl.itblMobStation, id);
                    if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                        continue;
                    _stListZhertvaAbonent.Add(st);
                    pBar.ShowSmall(countRecord++);
                    pBar.UserCanceled();
                }

                distId = _ListPomehaAbonent.Distinct();
                foreach (int id in distId)
                {
                    Station st = new Station(ICSMTbl.itblMobStation, id);
                    if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                        continue;
                    _stListPomehaAbonent.Add(st);
                    pBar.ShowSmall(countRecord++);
                    pBar.UserCanceled();
                }
                //////////////////////////////////////////////////////////////////
                // BLOCK
                //////////////////////////////////////////////////////////////////
                if (_needPRM)
                {
                    distId = _ListZhertvaBlock.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListZhertvaBlock.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
                if (_needPRD)
                {
                    distId = _ListPomehaBlock.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaBlock.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
                if ((_needSRT) && (_needPRD))
                {
                    distId = _ListPomehaBlockSRT.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation2, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaBlockSRT.Add(st);

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
                if ((_needSRT) && (_needPRD)&&(_needInter))
                {
                    distId = _ListPomehaIntermodSRT.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation2, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaIntermodSRT.Add(st);
                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                    distId = _ListPomehaInterMOB_.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListPomehaInterMOB_.Add(st);
                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }

                }
                if ((_needSRT) && (_needPRM) && (_needInter))
                {
                    distId = _ListZhertvaInterMOB_.Distinct();
                    foreach (int id in distId)
                    {
                        Station st = new Station(ICSMTbl.itblMobStation, id);
                        if (st._longitude == 0.0 || st._latitude == 0.0 || st._longitude == IM.NullD || st._latitude == IM.NullD)
                            continue;
                        _stListZhertvaInterMOB_.Add(st);
                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();
                    }
                }
            }
        }
        /// <summary>
        /// Функция для перфорации частот у станции не дописана
        /// </summary>
        private void dell_freq_for_calc()
        {
            Double k_ajast = 4; //для соседнего канала 
            Double k_inter_blok = 5; //для блокировки и интермодуляции

            if (!(_curStation[0]._table == ICSMTbl.itblMobStation2)){ return; } // Если анализ не из таблицы для СРТ станций то надо выйти
            // Находим частоты минимум и максимум для текущей станции
            List<Double> _fTX_max = new List<Double>();
            List<Double> _fTX_min = new List<Double>();
            List<Double> _fRX_max = new List<Double>();
            List<Double> _fRX_min = new List<Double>();
            foreach (Station cur_st in _curStation)
            {
                _fTX_max.Add(cur_st.STFrequncyTX.Max());
                _fTX_min.Add(cur_st.STFrequncyTX.Min());
                _fRX_max.Add(cur_st.STFrequncyRX.Max());
                _fRX_min.Add(cur_st.STFrequncyRX.Min());
            }
            Double fTX_max = _fTX_max.Max();
            Double fTX_min = _fTX_min.Min();
            Double fRX_max = _fRX_max.Max();
            Double fRX_min = _fRX_min.Min();
            // Нашли минимальные и максимальные частоты
            List<Double> rx_del = new List<double>();
            List<Double> tx_del = new List<double>();
            List<int> st_rem = new List<int>();
            // _stListZhertvaMob - чистим
            int j = 0;
            foreach (Station st in _stListZhertvaMob)
            {
                for (int i = 0; i<st.STFrequncyRX.Count; i++)
                {
                    if (Math.Max(fTX_min - st.STFrequncyRX[i], st.STFrequncyRX[i] - fTX_max) > k_ajast * (_curStation[0]._bwTX + st._bwRX))
                    {rx_del.Add(st.STFrequncyRX[i]);tx_del.Add(st.STFrequncyTX[i]);}
                }
                foreach (Double fRX in rx_del){_stListZhertvaMob[j].STFrequncyRX.Remove(fRX);}
                foreach (Double fTX in tx_del){_stListZhertvaMob[j].STFrequncyTX.Remove(fTX);}
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            {_stListZhertvaMob.RemoveAt(st_rem[i-1]);}
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListZhertvaMob - очистили
            // _stListPomehaMob - чистим
            j = 0;
            foreach (Station st in _stListPomehaMob)
            {
                for (int i = 0; i < st.STFrequncyTX.Count; i++)
                {
                    if (Math.Max(fRX_min - st.STFrequncyTX[i], st.STFrequncyTX[i] - fRX_max) > k_ajast * (_curStation[0]._bwRX + st._bwTX))
                    { rx_del.Add(st.STFrequncyRX[i]); tx_del.Add(st.STFrequncyTX[i]); }
                }
                foreach (Double fRX in rx_del) { _stListPomehaMob[j].STFrequncyRX.Remove(fRX); }
                foreach (Double fTX in tx_del) { _stListPomehaMob[j].STFrequncyTX.Remove(fTX); }
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            { _stListPomehaMob.RemoveAt(st_rem[i - 1]); }
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListPomehaMob - очистили
            // _stListZhertvaBlock - чистим
            j = 0;
            foreach (Station st in _stListZhertvaBlock)
            {
                for (int i = 0; i < st.STFrequncyRX.Count; i++)
                {
                    if (Math.Max(fTX_min - st.STFrequncyRX[i], st.STFrequncyRX[i] - fTX_max) > k_inter_blok * (_curStation[0]._bwTX + st._bwRX))
                    { rx_del.Add(st.STFrequncyRX[i]); tx_del.Add(st.STFrequncyTX[i]); }
                }
                foreach (Double fRX in rx_del) { _stListZhertvaBlock[j].STFrequncyRX.Remove(fRX); }
                foreach (Double fTX in tx_del) { _stListZhertvaBlock[j].STFrequncyTX.Remove(fTX); }
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            { _stListZhertvaBlock.RemoveAt(st_rem[i - 1]); }
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListZhertvaBlock - очистили
            // _stListPomehaBlock - чистим
            j = 0;
            foreach (Station st in _stListPomehaBlock)
            {
                for (int i = 0; i < st.STFrequncyTX.Count; i++)
                {
                    if (Math.Max(fRX_min - st.STFrequncyTX[i], st.STFrequncyTX[i] - fRX_max) > k_inter_blok * (_curStation[0]._bwRX + st._bwTX))
                    { rx_del.Add(st.STFrequncyRX[i]); tx_del.Add(st.STFrequncyTX[i]); }
                }
                foreach (Double fRX in rx_del) { _stListPomehaBlock[j].STFrequncyRX.Remove(fRX); }
                foreach (Double fTX in tx_del) { _stListPomehaBlock[j].STFrequncyTX.Remove(fTX); }
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            { _stListPomehaBlock.RemoveAt(st_rem[i - 1]); }
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListPomehaBlock - очистили

            // _stListZhertvaInterMOB_ - чистим
            j = 0;
            foreach (Station st in _stListZhertvaInterMOB_)
            {
                for (int i = 0; i < st.STFrequncyRX.Count; i++)
                {
                    if (Math.Max(fTX_min - st.STFrequncyRX[i], st.STFrequncyRX[i] - fTX_max) > k_inter_blok * (_curStation[0]._bwTX + st._bwRX))
                    { rx_del.Add(st.STFrequncyRX[i]); tx_del.Add(st.STFrequncyTX[i]); }
                }
                foreach (Double fRX in rx_del) { _stListZhertvaInterMOB_[j].STFrequncyRX.Remove(fRX); }
                foreach (Double fTX in tx_del) { _stListZhertvaInterMOB_[j].STFrequncyTX.Remove(fTX); }
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            { _stListZhertvaInterMOB_.RemoveAt(st_rem[i - 1]); }
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListZhertvaInterMOB_ - очистили
            // _stListPomehaInterMOB_ - чистим
            j = 0;
            foreach (Station st in _stListPomehaInterMOB_)
            {
                for (int i = 0; i < st.STFrequncyTX.Count; i++)
                {
                    if (Math.Max(fRX_min - st.STFrequncyTX[i], st.STFrequncyTX[i] - fRX_max) > k_inter_blok * (_curStation[0]._bwRX + st._bwTX))
                    { rx_del.Add(st.STFrequncyRX[i]); tx_del.Add(st.STFrequncyTX[i]); }
                }
                foreach (Double fRX in rx_del) { _stListPomehaInterMOB_[j].STFrequncyRX.Remove(fRX); }
                foreach (Double fTX in tx_del) { _stListPomehaInterMOB_[j].STFrequncyTX.Remove(fTX); }
                if (st.STFrequncyRX.Count == 0) { st_rem.Add(j); }
                j++;
            }
            for (int i = st_rem.Count; i > 0; i = i - 1)
            { _stListPomehaInterMOB_.RemoveAt(st_rem[i - 1]); }
            rx_del.Clear(); tx_del.Clear(); st_rem.Clear();
            // _stListPomehaInterMOB_ - очистили

        }
        #endregion

        /// <summary>
        /// Подсчет PI
        /// </summary>
        /// <param name="stList">Список станций</param>
        /// <param name="curStation">Текущая станция</param>
        private void CalculatePi(List<Station> stList, Station curStation)
        {
            foreach (Station st in stList)
            {
                this.CalculatePZA(st, curStation);
            }
        }
        /// <summary>
        /// Подсчет PI
        /// </summary>
        /// <param name="stListZhertva">Список станций жертв</param>
        /// <param name="stListPomeha">Список станций помех</param>
        /// <param name="curStation">Текущая станция</param>
        /// <param name="typeOfStation">Тип расчета</param>
        private void CalculatePi(List<Station> stListZhertva, List<Station> stListPomeha, Station curStation, Station.TypeOfStation typeOfStation)
        {
            if (typeOfStation == Station.TypeOfStation.MOB)
            {
                countRecord = 0;
                if (_needPRM)
                {
                    foreach (Station st in stListZhertva)
                    {
                        CalculateZhertvaMOB(st, curStation);
                    }
                }
                if (_needPRD)
                {
                    foreach (Station st in stListPomeha)
                    {
                        CalculatePomehaMOB(st, curStation);
                    }
                }
            }

            if (typeOfStation == Station.TypeOfStation.Abonent)
            {
                countRecord = 0;
                if (_needPRM)
                {
                    foreach (Station st in stListZhertva)
                    {
                        CalculateZhertvaAbonent(st, curStation);
                    }
                }
                if (_needPRD)
                {
                    foreach (Station st in stListPomeha)
                    {
                        CalculatePomehaAbonent(st, curStation);
                    }
                }
            }

            if (typeOfStation == Station.TypeOfStation.Block)
            {
                if (_needPRM)
                {
                    countRecord = 0;
                    foreach (Station st in stListZhertva)
                    {
                        CalculateZhertvaBlock(st, curStation);
                    }
                }
                if (_needPRD)
                {
                    countRecord = 0;
                    foreach (Station st in stListPomeha)
                    {
                        CalculatePomehaBlock(st, curStation);
                    }
                }
            }
        }
        #endregion
    }
}
