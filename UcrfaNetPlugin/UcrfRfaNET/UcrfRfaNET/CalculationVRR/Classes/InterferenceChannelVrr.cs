﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    /// <summary>
    /// Класс описывает строку для отображения помех
    /// при расчете ЭМС для зарубежных линков в TreeColumnView
    /// </summary>
    internal class InterferenceChannelVrr : BaseRowCalculationVrr
    {
        #region General parameters
        /// <summary>
        /// Частота
        /// </summary>
        [TreeColumn("Frequency (MGz)")]
        public DoubleElemCell Frequency { get; set; }
        /// <summary>
        /// Помеха
        /// </summary>
        [TreeColumn("Pi")]
        public DoubleElemCell Pi { get; set; }
        /// <summary>
        /// IRF
        /// </summary>
        [TreeColumn("IRF (dB)")]
        public DoubleElemCell Irf { get; set; }
        /// <summary>
        /// Привышение порога
        /// </summary>
        [TreeColumn("Delta degradation, dB")]
        public DoubleElemCell DeltaDegradation { get; set; }
        /// <summary>
        /// Растояние
        /// </summary>
        [TreeColumn("Distance")]
        public DoubleElemCell Distance { get; set; }
        /// <summary>
        /// Частота приема
        /// </summary>
        [TreeColumn("Freq RX")]
        public DoubleElemCell FreqRx { get; set; }
        /// <summary>
        /// Частота передачи
        /// </summary>
        [TreeColumn("Freq TX")]
        public DoubleElemCell FreqTx { get; set; }
        /// <summary>
        /// Разница частот
        /// </summary>
        [TreeColumn("Delta freq")]
        public DoubleElemCell DelraF { get; set; }
        /// <summary>
        /// Радиотехнология
        /// </summary>
        [TreeColumn("Standard")]
        public StringElemCell Standard { get; set; }
        /// <summary>
        /// Мощность
        /// </summary>
        [TreeColumn("Power (W)")]
        public DoubleElemCell Power { get; set; }
        /// <summary>
        /// Владелец
        /// </summary>
        [TreeColumn("Owner")]
        public StringElemCell Owner { get; set; }
        /// <summary>
        /// Название станции
        /// </summary>
        [TreeColumn("Station name")]
        public StringElemCell StationName { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        [TreeColumn("Status")]
        public StringElemCell Status { get; set; }
        #endregion
        //===============================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public InterferenceChannelVrr() : base()
        {
            Frequency = new DoubleElemCell(4);
            Pi = new DoubleElemCell(-999.0, 2);
            Irf = new DoubleElemCell(2);
            DeltaDegradation = new DoubleElemCell(2);
            Distance = new DoubleElemCell(3);
            FreqRx = new DoubleElemCell(4);
            FreqTx = new DoubleElemCell(4);
            DelraF = new DoubleElemCell(4);
            Standard = new StringElemCell();
            Power = new DoubleElemCell(2);
            Owner = new StringElemCell();
            StationName = new StringElemCell();
            Status = new StringElemCell();
        }
    }
}
