﻿using ComponentsLib;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    /// <summary>
    /// Базовый класс описывает строку для отображения данных
    /// при расчете ЭМС для ВРР в TreeColumnView
    /// </summary>
    internal class BaseRowCalculationVrr : TreeColumnRowBase
    {
        /// <summary>
        /// ID станции
        /// </summary>
        [TreeColumn("ID station")]
        public IntElemCell StationId { get; set; }
        /// <summary>
        /// Название таблицы
        /// </summary>
        [TreeColumn("Table name")]
        public StringElemCell TableName { get; set; }
        //------
        #region LocalValue
        public bool IsShowed { get; set; }
        public double XPowerTx { get; set; }
        public double XFreqTx { get; set; }
        public double XDeltaFreqTx { get; set; }
        public double XLftTx { get; set; }
        public double XGmaxTx { get; set; }
        public double XAz1Tx { get; set; }
        public double XAz2Tx { get; set; }
        public double XDeltaGTx { get; set; }
        public double XDistance { get; set; }
        public double XLorH { get; set; }
        public double XKtbfRx { get; set; }
        public double XFreqRx { get; set; }
        public double XDeltaFreqRx { get; set; }
        public double XLftRx { get; set; }
        public double XGmaxRx { get; set; }
        public double XAz1Rx { get; set; }
        public double XAz2Rx { get; set; }
        public double XDeltaGRx { get; set; }
        public string XDisEmis { get; set; }
        public double XHeigthAntennaInterf { get; set; }
        public double XHeigthAntennaVictim { get; set; }
        #endregion
        //===============================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public BaseRowCalculationVrr()
            : base()
        {
            StationId = new IntElemCell();
            TableName = new StringElemCell();
            //---
            IsShowed = true;
        }
    }
}
