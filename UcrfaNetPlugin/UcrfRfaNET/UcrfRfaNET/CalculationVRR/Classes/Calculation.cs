﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSPosition;
using ICSM;
using LisUtility;
using Distribution;
using Diagramm;
using LisUtility.Mob;
using XICSM.Intermodulation;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class ViborkaVRR
   {
      #region POMEHA AND ZHERTVAs

      //////////////////////////////////////////////////////////////////////
      //POMEHA MOB
      //////////////////////////////////////////////////////////////////////
      private void CalculatePomehaMOB(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate MOB stations..."));

            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               double TX = st.STFrequncyTX[i];
               //double RX = st.STFrequncyRX[i];

               foreach (double curRX in curStat.STFrequncyRX)
                {
                   //if (Math.Abs(TX - curRX) > 5 * (st._bwTX + curStat._bwRX)){continue;} //ввод ограничения по разнице частот 
                   double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(st._longitude, st._latitude,
                      curStat._longitude, curStat._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(st._azimuth - a1);
                  double deltaA2 = Math.Abs(curStat._azimuth - a2);
                  double deltaB1 = Math.Abs(st._bElevation - b1);
                  double deltaB2 = Math.Abs(curStat._bElevation - b2);

                  double l525 = 60;

                  if (d != 0)
                  {
                      if (st._standart == "УКХ")
                      {
                          double e;
                          if(curStat._aboveEarthLevel > st._aboveEarthLevel)
                              e = _1546_4.Get_E(curStat._aboveEarthLevel, curStat._aboveEarthLevel, d, TX, 1, curStat._aboveSeaLevel, st._aboveEarthLevel);
                          else
                              e = _1546_4.Get_E(st._aboveEarthLevel, st._aboveEarthLevel, d, TX, 1, st._aboveSeaLevel, curStat._aboveEarthLevel);
                          l525 = 139.3 - e + 20.0*Math.Log10(TX);
                      }
                      else
                      {
                          if (TX >= 500 || d <= 5)
                          {
                              double TXggc = TX/1000; // переводим в ГГц
                              // calculates L525
                              l525 = L525.GetL525(TXggc, d);
                          }
                          else
                          {
                              // calculates HATA
                              l525 = HATA.GetL(TX, d, curStat._aboveEarthLevel,
                                               st._aboveEarthLevel);
                          }
                      }
                  }

                  AntennaDiagramm diagH = new AntennaDiagramm();
                  diagH.SetMaximalGain(st._gMax);
                  diagH.Build(st._aDiagH);
                  double GH = diagH.GetLossesAmount(deltaA1);

                  AntennaDiagramm diagV = new AntennaDiagramm();
                  diagV.SetMaximalGain(st._gMax);
                  diagV.Build(st._bDiagV);
                  double GV = diagV.GetLossesAmount(deltaB1);

                  double GT = Math.Min(GH, GV);

                  AntennaDiagramm diagH_ = new AntennaDiagramm();
                  diagH_.SetMaximalGain(curStat._gMax);
                  diagH_.Build(curStat._aDiagH);
                  GH = diagH_.GetLossesAmount(deltaA2);

                  AntennaDiagramm diagV_ = new AntennaDiagramm();
                  diagV_.SetMaximalGain(curStat._gMax);
                  diagV_.Build(curStat._bDiagV);
                  GV = diagV_.GetLossesAmount(deltaB2);

                  double Ga = Math.Min(GH, GV);

                  double irf = 0;
                  
                  irf = ACHMob.GetIRF(TX, curRX, st._freqFilterTX, curStat._freqFilterRX,
                     st._bwTX, false, st._maskaT, curStat._maskaR, st._filterT, curStat._filterR);
                 
                  

                  double evp = st._power + GT - st._lFT;

                  double PI = evp - l525 - curStat._lFR + Ga - irf;

                  double excess = PI - curStat._ktbf;

                  double deltafreq = TX - curRX;

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(TX);
                  st.CURRX.Add(curRX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      //////////////////////////////////////////////////////////////////////
      //ZHERTVA MOB
      //////////////////////////////////////////////////////////////////////
      private void CalculateZhertvaMOB(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate MOB stations..."));

            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               //double TX = st.STFrequncyTX[i];
               double RX = st.STFrequncyRX[i];

               foreach (double curTX in curStat.STFrequncyTX)
               {
                   //if (Math.Abs(RX-curTX)>5*(st._bwRX + curStat._bwTX)){continue;} //ввод ограничения по разнице частот 
                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
                      st._longitude, st._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(curStat._azimuth - a1);
                  double deltaA2 = Math.Abs(st._azimuth - a2);
                  double deltaB1 = Math.Abs(curStat._bElevation - b1);
                  double deltaB2 = Math.Abs(st._bElevation - b2);

                  double l525 = 60;

                  if (d != 0)
                  {
                      if (st._standart == "УКХ")
                      {
                          double e;
                          if (curStat._aboveEarthLevel > st._aboveEarthLevel)
                              e = _1546_4.Get_E(curStat._aboveEarthLevel, curStat._aboveEarthLevel, d, RX, 1, curStat._aboveSeaLevel, st._aboveEarthLevel);
                          else
                              e = _1546_4.Get_E(st._aboveEarthLevel, st._aboveEarthLevel, d, RX, 1, st._aboveSeaLevel, curStat._aboveEarthLevel);
                          l525 = 139.3 - e + 20.0 * Math.Log10(RX);
                      }
                      else
                      {
                          if (curTX >= 500 || d <= 5)
                          {
                              // calculates L525
                              double TXggc = curTX/1000; // переводим в ГГц
                              l525 = L525.GetL525(TXggc, d);
                          }
                          else
                          {
                              // calculates HATA
                              l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel,
                                               st._aboveEarthLevel);
                          }
                      }
                  }

                  AntennaDiagramm diagH = new AntennaDiagramm();
                  diagH.SetMaximalGain(st._gMax);
                  diagH.Build(st._aDiagH);
                  double GH = diagH.GetLossesAmount(deltaA2);

                  AntennaDiagramm diagV = new AntennaDiagramm();
                  diagV.SetMaximalGain(st._gMax);
                  diagV.Build(st._bDiagV);
                  double GV = diagV.GetLossesAmount(deltaB2);
                  
                  double GT = Math.Min(GH, GV);

                  AntennaDiagramm diagH_ = new AntennaDiagramm();
                  diagH_.SetMaximalGain(curStat._gMax);
                  diagH_.Build(curStat._aDiagH);
                  GH = diagH_.GetLossesAmount(deltaA1);

                  AntennaDiagramm diagV_ = new AntennaDiagramm();
                  diagV_.SetMaximalGain(curStat._gMax);
                  diagV_.Build(curStat._bDiagV);
                  GV = diagV_.GetLossesAmount(deltaB1);

                  double Ga = Math.Min(GH, GV);

                  double irf = ACHMob.GetIRF(curTX, RX, curStat._freqFilterTX, st._freqFilterRX,
                     curStat._bwTX, false, curStat._maskaT, st._maskaR, curStat._filterT, st._filterR);

                  double evp = curStat._power + Ga - curStat._lFT;

                  double PI = evp - l525 - st._lFR + GT - irf;

                  double excess = PI - st._ktbf;

                  double deltafreq = curTX - RX;

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(curTX);
                  st.CURRX.Add(RX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      //////////////////////////////////////////////////////////////////////
      //POMEHA ABONENT
      //////////////////////////////////////////////////////////////////////
      private void CalculatePomehaAbonent(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate Abonent stations..."));

            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               double TX = st.STFrequncyTX[i];
               //double RX = st.STFrequncyRX[i];

               foreach (double curTX in curStat.STFrequncyTX)
               {
                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(st._longitude, st._latitude,
                      curStat._longitude, curStat._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(st._azimuth - a1);
                  double deltaA2 = Math.Abs(curStat._azimuth - a2);
                  double deltaB1 = Math.Abs(st._bElevation - b1);
                  double deltaB2 = Math.Abs(curStat._bElevation - b2);

                  double l525 = 0;

                  double r = R(ref d, out l525, TX, curStat, st); // for abonents stations

                  AntennaDiagramm diagH = new AntennaDiagramm();
                  diagH.SetMaximalGain(st._gMax);
                  diagH.Build(st._aDiagH);
                  double GH = diagH.GetLossesAmount(deltaA1);

                  AntennaDiagramm diagV = new AntennaDiagramm();
                  diagV.SetMaximalGain(st._gMax);
                  diagV.Build(st._bDiagV);
                  double GV = diagV.GetLossesAmount(deltaB1);

                  double GT = Math.Min(GH, GV);
                  
                  AntennaDiagramm diagH_ = new AntennaDiagramm();
                  diagH_.SetMaximalGain(curStat._gMax);
                  diagH_.Build(curStat._aDiagH);
                  GH = diagH_.GetLossesAmount(deltaA2);

                  AntennaDiagramm diagV_ = new AntennaDiagramm();
                  diagV_.SetMaximalGain(curStat._gMax);
                  diagV_.Build(curStat._bDiagV);
                  GV = diagV_.GetLossesAmount(deltaB2);
                  
                   double Ga = 0; // Math.Min(GH, GV);

                  double irf = ACHMob.GetIRF(TX, curTX, st._freqFilterTX, curStat._freqFilterRX,
                     st._bwTX, false, st._maskaT, curStat._maskaR, st._filterT, curStat._filterR);

                  double evp = st._power + GT - st._lFT;

                  double PI = evp - l525 - curStat._lFR + Ga - irf;

                  double excess = PI - curStat._ktbf;

                  double deltafreq = Math.Abs(TX - curTX);

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(TX);
                  st.CURRX.Add(curTX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      //////////////////////////////////////////////////////////////////////
      //ZHERTVA ABONENT
      //////////////////////////////////////////////////////////////////////
      private void CalculateZhertvaAbonent(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate Abonent stations..."));

            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               //double TX = st.STFrequncyTX[i];
               double TX = st.STFrequncyTX[i];

               foreach (double curTX in curStat.STFrequncyTX)
               {
                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
                      st._longitude, st._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(curStat._azimuth - a1);
                  //double deltaA2 = Math.Abs(st._azimuth - a2);
                  double deltaB1 = Math.Abs(curStat._bElevation - b1);
                  //double deltaB2 = Math.Abs(st._bElevation - b2);

                  double l525 = 0;

                  double r = R(ref d, out l525, curTX, st, curStat); // for abonents stations


                  //AntennaDiagramm diagH = new AntennaDiagramm();
                  //diagH.SetMaximalGain(st._gMax);
                  //diagH.Build(st._aDiagH);

                  //AntennaDiagramm diagV = new AntennaDiagramm();
                  //diagV.SetMaximalGain(st._gMax);
                  //diagV.Build(st._bDiagV);


                  //double GH = diagH.GetLossesAmount(deltaA2);
                  //double GV = diagV.GetLossesAmount(deltaB2);
                  AntennaDiagramm diagH = new AntennaDiagramm();
                  AntennaDiagramm diagV = new AntennaDiagramm();

                  double GT = 0;//Math.Min(GH, GV);

                  diagH.SetMaximalGain(curStat._gMax);
                  diagH.Build(curStat._aDiagH);

                  diagV.SetMaximalGain(curStat._gMax);
                  diagV.Build(curStat._bDiagV);

                  double GH = diagH.GetLossesAmount(deltaA1);
                  double GV = diagV.GetLossesAmount(deltaB1);

                  double Ga = Math.Min(GH, GV);

                  double irf = ACHMob.GetIRF(curTX, TX, curStat._freqFilterTX, st._freqFilterRX,
                     curStat._bwTX, false, curStat._maskaT, st._maskaR, curStat._filterT, st._filterR);

                  double evp = curStat._power + Ga - curStat._lFT;

                  double PI = evp - l525 - st._lFR + GT - irf;

                  double excess = PI - st._ktbf;

                  double deltafreq = Math.Abs(curTX - TX);

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(TX);
                  st.CURRX.Add(curTX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      private double R(ref double d, out double l525, double curTX, Station st, Station curStat)
      {
         if (_R != null)
         {
            d = d - (double)_R;
            l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel,
                  st._aboveEarthLevel);
            return (double)_R;
         }
         else
         {
            double r = 5;
            double Rs;
            do
            {
               if (r > 70)
               {
                  d = d - r;
                  l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel,
                     st._aboveEarthLevel);
                  return r;
               }

               if (d - r <= 0)
               {
                  d = 0;
                  l525 = 60;
                  return r;
               }

               // calculates HATA
               double lR525 = HATA.GetL(curTX, r, st._aboveEarthLevel, 2.0);

               Rs = st._power - st._lFT + st._gMax - lR525;

               if (st._ktbf >= Rs)
               {
                  r /= 1.2;
                  d -= r;
                  l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel, st._aboveEarthLevel);

                  return r;
               }

               r *= 1.2;
            } while (st._ktbf < Rs);

            l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel, st._aboveEarthLevel);
            return 0;
         }
      }

      #endregion

      #region INTERMODULATION

      ///////////////////////////////////////////////////////////////////////
      // POMEHA INTERMODULATION
      ///////////////////////////////////////////////////////////////////////
      private List<IntermodulationInterference> CalculatePomehaInter(string _nameTable, int _id,
         double preselectorGain, double inputStageLinearity, double minFrequency,
         double maxFrequency, double minPower, double minSumPower, double range,
         int TypeCalculation, double k)
      {
         IntermodulationMainForm inter = new IntermodulationMainForm(_nameTable, _id,
         preselectorGain, inputStageLinearity, minFrequency, maxFrequency,
         minPower, minSumPower, range, TypeCalculation, k);

         inter.FindMethod();
         return inter.GetData();
      }

      /////////////////////////////////////////////////////////////////////////
      //// ZHERTVA INTERMODULATION
      /////////////////////////////////////////////////////////////////////////
      //private void CalculateZhertvaInter(string _nameTable, int _id,
      //   double preselectorGain, double inputStageLinearity, double minFrequency,
      //   double maxFrequency, double minPower, double minSumPower, double range,
      //   int TypeCalculation, double k)
      //{
      //   IntermodulationMainForm inter = new IntermodulationMainForm(_nameTable, _id,
      //   preselectorGain, inputStageLinearity, minFrequency, maxFrequency,
      //   minPower, minSumPower, range, TypeCalculation, k);

      //   inter.FindMethod();
      //   _stListZhertvaInter = inter.GetData();
      //}

      //////////////////////////////////////////////////////////////////////
      //POMEHA BLOCK
      //////////////////////////////////////////////////////////////////////
      private void CalculatePomehaBlock(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate Block stations..."));
            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               double TX = st.STFrequncyTX[i];
               //double RX = st.STFrequncyRX[i];

               foreach (double curRX in curStat.STFrequncyRX)
               {
                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(st._longitude, st._latitude,
                      curStat._longitude, curStat._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(st._azimuth - a1);
                  double deltaA2 = Math.Abs(curStat._azimuth - a2);
                  double deltaB1 = Math.Abs(st._bElevation - b1);
                  double deltaB2 = Math.Abs(curStat._bElevation - b2);

                  double l525 = 60;

                  if (d != 0)
                     if (TX >= 500 || d <= 5)
                     {
                        double TXggc = TX / 1000; // переводим в ГГц
                        // calculates L525
                        l525 = L525.GetL525(TXggc, d);
                     }
                     else
                     {
                        // calculates HATA
                        l525 = HATA.GetL(TX, d, curStat._aboveEarthLevel,
                           st._aboveEarthLevel);
                     }

                  AntennaDiagramm diagH = new AntennaDiagramm();
                  diagH.SetMaximalGain(st._gMax);
                  diagH.Build(st._aDiagH);
                  double GH = diagH.GetLossesAmount(deltaA1);

                  AntennaDiagramm diagV = new AntennaDiagramm();
                  diagV.SetMaximalGain(st._gMax);
                  diagV.Build(st._bDiagV);
                  double GV = diagV.GetLossesAmount(deltaB1);

                  double GT = Math.Min(GH, GV);

                  AntennaDiagramm diagH_ = new AntennaDiagramm();
                  diagH_.SetMaximalGain(curStat._gMax);
                  diagH_.Build(curStat._aDiagH);
                  GH = diagH_.GetLossesAmount(deltaA2);

                  AntennaDiagramm diagV_ = new AntennaDiagramm();
                  diagV_.SetMaximalGain(curStat._gMax);
                  diagV_.Build(curStat._bDiagV);
                  GV = diagV_.GetLossesAmount(deltaB2);

                  double Ga = Math.Min(GH, GV);

                  double irf;
                  if (st._table == "MOB_STATION2") // временная метка на тот случай если производится расчет станции СРТ (пока временно)
                  {
                       irf = ACHMob.GetIRF(TX, curRX, st._freqFilterTX, curStat._freqFilterRX,
                           st._bwTX, true, st._maskaT, new List<ACHMob.DataElement>(), new List<ACHMob.DataElement>(), curStat._filterR);
                  }
                  else
                  {
                      irf = ACHMob.GetIRF(TX, curRX, st._freqFilterTX, curStat._freqFilterRX,
                         st._bwTX, true, st._maskaT, new List<ACHMob.DataElement>(), st._filterT, curStat._filterR);
                  }

                  double evp = st._power + GT - st._lFT;

                  double PI = evp - l525 - curStat._lFR + Ga - irf;

                  double excess = PI - curStat._ktbf - _PIBlock;

                  double deltafreq = TX - curRX;
                  
                   //Фильт по просьбе Казачкова 
                  if (deltafreq > 100)
                  {
                      excess = -1000;
                  }

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(TX);
                  st.CURRX.Add(curRX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      //////////////////////////////////////////////////////////////////////
      //ZHERTVA BLOCK
      //////////////////////////////////////////////////////////////////////
      private void CalculateZhertvaBlock(Station st, Station curStat)
      {
         using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate PI")))
         {
            pBar.ShowBig(CLocaliz.TxT("Calculate Block stations..."));
            for (int i = 0; i < st.STFrequncyTX.Count; i++)
            {
               //double TX = st.STFrequncyTX[i];
               double RX = st.STFrequncyRX[i];

               foreach (double curTX in curStat.STFrequncyTX)
               {
                  double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                            st._longitude, st._latitude);

                  // receive azimuths
                  double a1 = 0, a2 = 180; // азимуты
                  double b1 = 0, b2 = 0; // углы места
                  StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
                      st._longitude, st._latitude, ref a1, ref a2);

                  // receive corner place
                  if (d != 0)
                     StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                        ref b2);

                  double deltaA1 = Math.Abs(curStat._azimuth - a1);
                  double deltaA2 = Math.Abs(st._azimuth - a2);
                  double deltaB1 = Math.Abs(curStat._bElevation - b1);
                  double deltaB2 = Math.Abs(st._bElevation - b2);

                  double l525 = 60;

                  if (d != 0)
                     if (curTX >= 500 || d <= 5)
                     {
                        // calculates L525
                        double TXggc = curTX / 1000; // переводим в ГГц
                        l525 = L525.GetL525(TXggc, d);
                     }
                     else
                     {
                        // calculates HATA
                        l525 = HATA.GetL(curTX, d, curStat._aboveEarthLevel,
                           st._aboveEarthLevel);
                     }

                  AntennaDiagramm diagH = new AntennaDiagramm();
                  diagH.SetMaximalGain(st._gMax);
                  diagH.Build(st._aDiagH);
                  double GH = diagH.GetLossesAmount(deltaA2);

                  AntennaDiagramm diagV = new AntennaDiagramm();
                  diagV.SetMaximalGain(st._gMax);
                  diagV.Build(st._bDiagV);
                  double GV = diagV.GetLossesAmount(deltaB2);

                  double GT = Math.Min(GH, GV);
                  
                  AntennaDiagramm diagH_ = new AntennaDiagramm();
                  diagH_.SetMaximalGain(curStat._gMax);
                  diagH_.Build(curStat._aDiagH);
                  GH = diagH_.GetLossesAmount(deltaA1);

                  AntennaDiagramm diagV_ = new AntennaDiagramm();
                  diagV_.SetMaximalGain(curStat._gMax);
                  diagV_.Build(curStat._bDiagV);
                  GV = diagV_.GetLossesAmount(deltaB1);

                  double Ga = Math.Min(GH, GV);

                  double irf = ACHMob.GetIRF(curTX, RX, curStat._freqFilterTX, st._freqFilterRX,
                     curStat._bwTX, true, curStat._maskaT, new List<ACHMob.DataElement>(), curStat._filterT, st._filterR);

                  double evp = curStat._power + Ga - curStat._lFT;

                  double PI = evp - l525 - st._lFR + GT - irf;

                  double excess = PI - st._ktbf - _PIBlock;

                  double deltafreq = curTX - RX;

                  //Фильт по просьбе Казачкова 
                  if (deltafreq > 100)
                  {
                      excess = -1000;
                  }

                  st.DISTANCE.Add(d);
                  st.EVP.Add(evp);
                  st.EXCESS.Add(excess);
                  st.L525orHata.Add(l525);
                  st.DELTAFREQ.Add(deltafreq);
                  st.GT.Add(GT);
                  st.GA.Add(Ga);
                  st.A1.Add(a1);
                  st.A2.Add(a2);
                  st.B1.Add(b1);
                  st.B2.Add(b2);
                  st.PI.Add(PI);
                  st.IRF.Add(irf);
                  st.CURTX.Add(curTX);
                  st.CURRX.Add(RX);

                  pBar.ShowSmall(countRecord++);
                  pBar.UserCanceled();
               }
            }
         }
      }

      #endregion

      #region POMEHA AND ZHERTVA AND ABONENT FOR PODBOR

      private void CalculatePZA(Station st, Station curStat)
      {
         for (int i = 0; i < st.STFrequncyTX.Count; i++)
         {
            double TX = st.STFrequncyTX[i];
            //double RX = st.STFrequncyRX[i];

            foreach (double curRX in curStat.STFrequncyRX)
            {
               double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
                         st._longitude, st._latitude);

               // receive azimuths
               double a1 = 0, a2 = 180; // азимуты
               double b1 = 0, b2 = 0; // углы места
               StationUtility.AntennaAzimuth(st._longitude, st._latitude,
                   curStat._longitude, curStat._latitude, ref a1, ref a2);

               // receive corner place
               if (d != 0)
                  StationUtility.CornerPlace(d, curStat._aboveSeaLevel, st._aboveSeaLevel, ref b1,
                     ref b2);

               double deltaA1 = Math.Abs(st._azimuth - a1);
               double deltaA2 = Math.Abs(curStat._azimuth - a2);
               double deltaB1 = Math.Abs(st._bElevation - b1);
               double deltaB2 = Math.Abs(curStat._bElevation - b2);

               double l525AbonentZ = 60;
               double l525AbonentP = 60;
               double l525 = 60;

               if (d != 0)
               {
                  l525 = HATA.GetL(TX, d, curStat._aboveEarthLevel,
                     st._aboveEarthLevel);

                  double dAbonent;
                  if (_R != null)
                  {
                     dAbonent = d - (double)_R;
                  }
                  else
                     dAbonent = d;


                  if (dAbonent <= 1)
                     dAbonent = 0;
                  else
                  {
                     l525AbonentP = HATA.GetL(TX, d, curStat._aboveEarthLevel, 2);
                     l525AbonentZ = HATA.GetL(TX, d, st._aboveEarthLevel, 2);
                  }
               }

               AntennaDiagramm diagH = new AntennaDiagramm();
               diagH.SetMaximalGain(st._gMax);
               diagH.Build(st._aDiagH);
               double GH = diagH.GetLossesAmount(deltaA1);

               AntennaDiagramm diagV = new AntennaDiagramm();
               diagV.SetMaximalGain(st._gMax);
               diagV.Build(st._bDiagV);
               double GV = diagV.GetLossesAmount(deltaB1);

               double GT = Math.Min(GH, GV);

               AntennaDiagramm diagH_ = new AntennaDiagramm();
               diagH_.SetMaximalGain(curStat._gMax);
               diagH_.Build(curStat._aDiagH);
               GH = diagH_.GetLossesAmount(deltaA2);
               AntennaDiagramm diagV_ = new AntennaDiagramm();
               diagV_.SetMaximalGain(curStat._gMax);
               diagV_.Build(curStat._bDiagV);
               GV = diagV_.GetLossesAmount(deltaB2);
               
               double Ga = Math.Min(GH, GV);

               double evp = st._power + GT - st._lFT;
               double PI = evp - l525 - curStat._lFR + Ga;
               double PIAbonent = evp - l525AbonentP - curStat._lFR;

               evp = curStat._power + GT - curStat._lFT;
               double PIZhertva = evp - l525 - st._lFR + Ga;
               double PIZhertvaAbonent = evp - l525AbonentZ - st._lFR;

               st.PI.Add(PI);
               st.PI_A.Add(PIAbonent);
               st.PIZHERTV.Add(PIZhertva);
               st.PIZHERTV_A.Add(PIZhertvaAbonent);
            }
         }
      }

      #endregion

       private List<string[]> CalculateFreq(int freqPlanId, string planName, Station curStation)
       {
           this.CalculateOnRadius(curStation, Station.TypeOfStation.MOB);
           this.FillingStations();
           this.CalculatePi(_stListPodbor, curStation); // розщитуэм PI(помеха станции анализируемой от станции выборки), PIZHERTV(помеха от станции анализируемой дляч станции выборки), PIABONENT(помеха абоненту анализируемой станции от станции выборки), PIZHERTVABONENT(помеха абонентам от анализируемой станции станциям выборки)

           List<PodborResult> result = new List<PodborResult>();
           {
               IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
               rsFreqPlan.Select("FREQ");
               rsFreqPlan.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, freqPlanId);
               rsFreqPlan.SetAdditional("[PARITY] IN ('N','U','H')");
               try
               {
                   for (rsFreqPlan.Open(); !rsFreqPlan.IsEOF(); rsFreqPlan.MoveNext())
                   {
                       PodborResult podbor = new PodborResult();
                       podbor.FREQ = rsFreqPlan.GetD("FREQ");
                       result.Add(podbor);
                   }
               }
               finally
               {
                   rsFreqPlan.Close();
                   rsFreqPlan.Destroy();
               }
           }

           if (IMProgress.IsDisplayed() == false)
               IMProgress.Create("Аналіз частот");
           IMProgress.ShowBig(planName);

           for (int i = 0; i < result.Count; i++)
           {
               double freq = result[i].FREQ - _podborRoznos;
               IMProgress.ShowSmall(result[i].FREQ.ToString("F4"));
               IMProgress.ShowProgress(i, result.Count);
               double pi = -9999;
               foreach (Station st in _stListPodbor) // розрахунок помэхи станции на частоте freq
               {
                   for (int i1 = 0; i1 < st.STFrequncyTX.Count; i1++)
                   {
                       double fTX = st.STFrequncyTX[i1];
                       double sBW = st._bwTX;

                       double piii = -999;   //????
                       if (st.PI.Count > i1)
                           piii = st.PI[i1];

                       double predel1 = (fTX - sBW / 2 - curStation._bwRX / 2 + 0.00001).Round(6);
                       double predel2 = (fTX + sBW / 2 + curStation._bwRX / 2 - 0.00001).Round(6);

                              
                       if (predel1 < freq && freq < predel2)
                       {
                           pi = 10 * Math.Log10(Math.Pow(10, piii / 10) + Math.Pow(10, pi / 10));
                           continue;
                       }

                       predel1 = (fTX - sBW - curStation._bwRX + 0.00001).Round(6);
                       predel2 = (fTX + sBW + curStation._bwRX - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           pi = 10 * Math.Log10(Math.Pow(10, (piii - 30) / 10) + Math.Pow(10, pi / 10));
                       }
                   }
               }

               PodborResult tmp = result[i];
               tmp.PIZ = pi;
               result[i] = tmp;

               /////////////////////////////////////////////////////////////
               //////////////////////////////////////////////////////////////
               //////////////////////////////////////////////////////////////
               pi = -9999;
               freq = result[i].FREQ;

               foreach (Station st in _stListPodbor) // розрахунок помехи от станции на частоте freq
               {
                   for (int i1 = 0; i1 < st.STFrequncyRX.Count; i1++)
                   {
                       if (st.PIZHERTV.Count <= i1)  //?????
                           continue;

                       double fRX = st.STFrequncyRX[i1];
                       double sBW = st._bwTX;

                       double predel1 = (fRX - sBW / 2 - curStation._bwRX / 2 + 0.00001).Round(6);
                       double predel2 = (fRX + sBW / 2 + curStation._bwRX / 2 - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           if (st.PIZHERTV[i1] > pi)
                               pi = st.PIZHERTV[i1];

                           continue;
                       }

                       predel1 = (fRX - sBW - curStation._bwRX + 0.00001).Round(6);
                       predel2 = (fRX + sBW + curStation._bwRX - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           if (st.PIZHERTV[i1] - 30 > pi)
                               pi = st.PIZHERTV[i1] - 30;
                       }
                   }
               }

               tmp = result[i];
               tmp.PIP = pi;
               result[i] = tmp;

               /////////////////////////////////////////////////////////
               /////////////////////////////////////////////////////////
               /////////////////////////////////////////////////////////

               freq = result[i].FREQ;

               pi = -9999;
               foreach (Station st in _stListPodbor) // розрахунок помэхи абонентам станции на частоте freq
               {
                   for (int i1 = 0; i1 < st.STFrequncyTX.Count; i1++)
                   {
                       double piii = -999;   //????
                       if (st.PI_A.Count > i1)
                           piii = st.PI_A[i1];

                       double fTX = st.STFrequncyTX[i1];
                       double sBW = st._bwTX;

                       double predel1 = (fTX - sBW / 2 - curStation._bwRX / 2 + 0.00001).Round(6);
                       double predel2 = (fTX + sBW / 2 + curStation._bwRX / 2 - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           pi = 10 * Math.Log10(Math.Pow(10, piii / 10) + Math.Pow(10, pi / 10));
                           continue;
                       }

                       predel1 = (fTX - sBW - curStation._bwRX + 0.00001).Round(6);
                       predel2 = (fTX + sBW + curStation._bwRX - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           pi = 10 * Math.Log10(Math.Pow(10, (piii - 30) / 10) + Math.Pow(10, pi / 10));
                       }
                   }
               }

               tmp = result[i];
               tmp.PIZA = pi;
               result[i] = tmp;

               /////////////////////////////////////////////////////////////
               //////////////////////////////////////////////////////////////
               //////////////////////////////////////////////////////////////

               pi = -9999;
               freq = result[i].FREQ;

               foreach (Station st in _stListPodbor) // розрахунок помехи от станции для абонентов на частоте freq
               {
                   for (int i1 = 0; i1 < st.STFrequncyTX.Count; i1++)
                   {
                       if (st.PIZHERTV_A.Count <= i1)  //?????
                           continue;

                       double fTX = st.STFrequncyTX[i1];
                       double sBW = st._bwTX;

                       double predel1 = (fTX - sBW / 2 - curStation._bwRX / 2 + 0.00001).Round(6);
                       double predel2 = (fTX + sBW / 2 + curStation._bwRX / 2 - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           if (st.PIZHERTV_A[i1] > pi)
                               pi = st.PIZHERTV_A[i1];

                           continue;
                       }

                       predel1 = (fTX - sBW - curStation._bwTX + 0.00001).Round(6);
                       predel2 = (fTX + sBW + curStation._bwTX - 0.00001).Round(6);

                       if (predel1 < freq && freq < predel2)
                       {
                           if (st.PIZHERTV_A[i1] - 30 > pi)
                               pi = st.PIZHERTV_A[i1] - 30;
                       }
                   }
               }

               tmp = result[i];
               tmp.PIPA = pi;
               result[i] = tmp;
           }

           IMProgress.Destroy();

           List<string[]> outPutList = new List<string[]>();
           foreach (PodborResult elem in result)
           {
               string str3 = "-";
               string str4 = "-";
               if (_podborAbonent)
               {
                   str3 = elem.PIPA.ToString("F1");
                   str4 = elem.PIZA.ToString("F1");
               }

               string[] strRow = new string[] {
                                                  elem.FREQ.ToString("F4"),
                                                  elem.PIZ.ToString("F1"),
                                                  elem.PIP.ToString("F1"),
                                                  str3,
                                                  str4 };

               outPutList.Add(strRow);
           }
           return outPutList;
       }

   }
}
