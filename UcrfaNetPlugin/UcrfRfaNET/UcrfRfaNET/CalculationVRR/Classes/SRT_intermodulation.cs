﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSPosition;
using ICSM;
using LisUtility;
using Distribution;
using Diagramm;
using LisUtility.Mob;
using XICSM.Intermodulation;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using ComponentsLib;
using XICSM.UcrfRfaNET.Calculation.ResultForm;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    public class SRT_intermodulation
    {
        private List<Station> cur_st_list;
        private List<Station> st_list;
        private List<Station> st_list_vict;
        private Double G = 12;
        private Double IP3 = 28;
        private Double porog_interf;
        private Double max_d_f = 30; // Максимльный разнос частот при котором будем искать помеху 1+1-1
        /// <summary>
        /// вход в функцию расчета интермодуляции для станций СРТ которіе создают помехи станциям МОБ
        /// </summary>
        /// <param name="_cur_station">станция жертва</param>
        /// <param name="_st">список станций помех</param>
        /// <param name="_porog_interf">Допустимый порог уровня помехи по отношению к собственным шумам</param>
        internal SRT_intermodulation(List<Station> _cur_station, List<Station> _st, Double _porog_interf)
        {
            st_list = _st;
            cur_st_list = _cur_station;
            porog_interf = _porog_interf;
            // И дозагрузка станций 
            foreach (Station st in st_list)
            {
                st.LoadAdditionalData();
            }
        }
        /// <summary>
        /// вход в функцию расчета интермодуляции для станций СРТ которіе создают помехи станциям МОБ
        /// </summary>
        /// <param name="_cur_station">станция жертва</param>
        /// <param name="_st">список станций помех</param>
        ///  <param name="_st_vikt">список станций жертв</param>
        /// <param name="_porog_interf">Допустимый порог уровня помехи по отношению к собственным шумам</param>
        internal SRT_intermodulation(List<Station> _cur_station, List<Station> _st, List<Station> _st_vikt, Double _porog_interf)
        {
            st_list = _st;
            cur_st_list = _cur_station;
            porog_interf = _porog_interf;
            st_list_vict = _st_vikt;
            // И дозагрузка станций 
            foreach (Station st in st_list)
            {
                st.LoadAdditionalData();
            }
            foreach (Station st in st_list_vict)
            {
                st.LoadAdditionalData();
            }
        }
        public List<TreeColumnRowBase> calc_intermod_2_1_for_MOB()
        {
            Color colorOfStation = Colors.LightCoral;                                                // Заливка паралельно
            List<TreeColumnRowBase> lstShowingData_SRT_intermod = new List<TreeColumnRowBase>();     // создание переменной заливки
            int indexSector = 0;
            foreach (Station cur_st in cur_st_list)                                                  // Цикл по станциям (секторам) жертвам
            {
                //чистка станций от расчетов
                foreach (Station st in st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }
                TreeViewIntermodulation recordSector = new TreeViewIntermodulation();                // создание переменной для заливки сектора
                indexSector++;
                foreach (Double FrequncyRX in cur_st.STFrequncyRX)                                   // Цикл по частотам
                {
                    TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();              // Создание переменной для заливки частоты    
                    int indexCounter = 1;
                    foreach (Station st1 in st_list)
                    {
                        foreach (Double FrequncyTX1 in st1.STFrequncyTX)
                        {
                            foreach (Station st2 in st_list)
                            {
                                foreach (Double FrequncyTX2 in st2.STFrequncyTX)
                                {
                                    // тут добрались до конкретных частот 
                                    // условия появления интермодуляционных помех
                                    if ((intermod_hit_2_1(st1._bwTX, FrequncyTX1, st2._bwTX, FrequncyTX2, cur_st._bwRX, FrequncyRX)) && ((st1._tableID != st2._tableID) || (FrequncyTX1 != FrequncyTX2)))
                                    {
                                        // таки да мы попали надо провести расчет помех
                                        // Условия чтоб лишний раз не считать. 
                                        int indP1 = 0; // индекс помехи 1
                                        int indP2 = 0; // индекс помехи 2
                                        if (st1.PI.Count == 0)
                                        {
                                            // т.е. расчетов ранее не было вобще 
                                            calc_intermod_pi_st(cur_st, FrequncyRX, st1, FrequncyTX1);
                                        }
                                        else
                                        {
                                            Boolean popad = false;
                                            for (int i = 0; i < st1.CURTX.Count; i++)
                                            {
                                                if ((st1.CURTX[i] == FrequncyTX1) && (st1.CURRX[i] == FrequncyRX))
                                                {
                                                    popad = true;
                                                    indP1 = i;
                                                    break;
                                                }
                                            }
                                            if (!popad)
                                            {
                                                calc_intermod_pi_st(cur_st, FrequncyRX, st1, FrequncyTX1);
                                                indP1 = st1.PI.Count - 1;
                                            }
                                        }
                                        if (st2.PI.Count == 0)
                                        {
                                            // т.е. расчетов ранее не было вобще 
                                            calc_intermod_pi_st(cur_st, FrequncyRX, st2, FrequncyTX2);
                                        }
                                        else
                                        {
                                            Boolean popad = false;
                                            for (int i = 0; i < st2.CURTX.Count; i++)
                                            {
                                                if ((st2.CURTX[i] == FrequncyTX2) && (st2.CURRX[i] == FrequncyRX))
                                                {
                                                    popad = true;
                                                    indP2 = i;
                                                    break;
                                                }
                                            }
                                            if (!popad)
                                            {
                                                calc_intermod_pi_st(cur_st, FrequncyRX, st2, FrequncyTX2);
                                                indP2 = st2.PI.Count - 1;
                                            }
                                        }
                                        // тут все расчеті произведены осталось тупа посчитать уровень помехи.
                                        Double P1 = st1.PI[indP1] - calc_loss_preselector(st1.DELTAFREQ[indP1], cur_st._bwRX);
                                        Double P2 = st2.PI[indP2] - calc_loss_preselector(st2.DELTAFREQ[indP2], cur_st._bwRX);
                                        Double Pinterfirence = calc_pow_intermod_2_1(P1, P2) - add_loss_for_UMTS_5_2_1(st1.CURTX[indP1], st2.CURTX[indP2], st1.CURRX[indP1], st2._bwTX, cur_st._bwRX);
                                        if (Pinterfirence - porog_interf > cur_st._ktbf)
                                        {
                                            // вот теперь можно говорить о том что есть помеха на данной частоте и следовательно надо формировать запись для вывода 
                                            TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();                            // Создание записи помехи                                 
                                            recordFreq.Children.Add(recordIntermod);                                                           // Добавление помехи в частоту      
                                            recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++); //Добавление данных помехи
                                            TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();                            // Создание записи станции1  
                                            recordIntermod.Children.Add(recordStation1);                                                       // Добавление данных станции1 
                                            recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                                            recordStation1.Description.BackColor = colorOfStation;
                                            recordStation1.TableName.Value = st1._table;
                                            recordStation1.StationId.Value = st1._tableID;
                                            recordStation1.Owner.Value = st1.OwnerName;
                                            recordStation1.StationName.Value = st1.StationName;
                                            recordStation1.Frequency.Value = st1.CURTX[indP1];
                                            recordStation1.Power.Value = st1._power;
                                            recordStation1.AzimuthTx.Value = st1.A1[indP1];
                                            recordStation1.AzimuthRx.Value = st1.A2[indP1];
                                            recordStation1.Gtx.Value = st1.GT[indP1];
                                            recordStation1.Grx.Value = st1.GA[indP1];
                                            recordStation1.Distance.Value = st1.DISTANCE[indP1];
                                            recordStation1.Loss.Value = st1.L525orHata[indP1];
                                            recordStation1.Irf.Value = st1.IRF[indP1];
                                            recordStation1.DeltaF.Value = st1.DELTAFREQ[indP1];
                                            recordStation1.Pi.Value = st1.PI[indP1];
                                            recordStation1.Pi.BackColor = colorOfStation;
                                            recordStation1.Status.Value = st1.Status;
                                            recordStation1.Standard.Value = st1._standart;
                                            recordStation1.Standard.BackColor = colorOfStation;
                                            recordStation1.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                            recordStation1.DeltaDegradation.BackColor = colorOfStation;
                                            recordStation1.Degradation.Value = cur_st._ktbf;
                                            //----------
                                            recordStation1.XPowerTx = st1._power;
                                            recordStation1.XFreqTx = st1.CURTX[indP1];
                                            recordStation1.XDeltaFreqTx = st1._bwTX;
                                            recordStation1.XLftTx = st1._lFT;
                                            recordStation1.XGmaxTx = st1._gMax;
                                            recordStation1.XAz1Tx = st1._azimuth;
                                            recordStation1.XAz2Tx = st1.A1[indP1];
                                            recordStation1.XDeltaGTx = st1.GT[indP1];
                                            recordStation1.XDistance = st1.DISTANCE[indP1];
                                            recordStation1.XLorH = st1.L525orHata[indP1];
                                            recordStation1.XKtbfRx = cur_st._ktbf;
                                            recordStation1.XFreqRx = st1.CURRX[indP1];
                                            recordStation1.XDeltaFreqRx = cur_st._bwRX;
                                            recordStation1.XLftRx = cur_st._lFR;
                                            recordStation1.XGmaxRx = cur_st._gMax;
                                            recordStation1.XAz1Rx = cur_st._azimuth;
                                            recordStation1.XAz2Rx = st1.A2[indP1];
                                            recordStation1.XDeltaGRx = st1.GA[indP1];
                                            recordStation1.XDisEmis = st1.DesEmi;
                                            recordStation1.XHeigthAntennaInterf = st1._aboveEarthLevel;
                                            recordStation1.XHeigthAntennaVictim = cur_st._aboveEarthLevel;
                                            // =================================
                                            TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();                             // Создание записи станции2 
                                            recordIntermod.Children.Add(recordStation2);                                                        // Добавление данных станции1 
                                            recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                                            recordStation2.Description.BackColor = colorOfStation;
                                            recordStation2.TableName.Value = st2._table; ;
                                            recordStation2.StationId.Value = st2._tableID;
                                            recordStation2.Owner.Value = st2.OwnerName;
                                            recordStation2.StationName.Value = st2.StationName;
                                            recordStation2.Frequency.Value = st2.CURTX[indP2];
                                            recordStation2.Power.Value = st2._power;
                                            recordStation2.AzimuthTx.Value = st2.A1[indP2];
                                            recordStation2.AzimuthRx.Value = st2.A2[indP2];
                                            recordStation2.Gtx.Value = st2.GT[indP2];
                                            recordStation2.Grx.Value = st2.GA[indP2];
                                            recordStation2.Distance.Value = st2.DISTANCE[indP2];
                                            recordStation2.Loss.Value = st2.L525orHata[indP2];
                                            recordStation2.Irf.Value = st2.IRF[indP2];
                                            recordStation2.DeltaF.Value = st2.DELTAFREQ[indP2];
                                            recordStation2.Pi.Value = st2.PI[indP2];
                                            recordStation2.Pi.BackColor = colorOfStation;
                                            recordStation2.Status.Value = st2.Status;
                                            recordStation2.Standard.Value = st2._standart;
                                            recordStation2.Standard.BackColor = colorOfStation;
                                            recordStation2.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                            recordStation2.DeltaDegradation.BackColor = colorOfStation;
                                            recordStation2.Degradation.Value = cur_st._ktbf;
                                            //----------
                                            recordStation2.XPowerTx = st2._power;
                                            recordStation2.XFreqTx = st2.CURTX[indP2];
                                            recordStation2.XDeltaFreqTx = st2._bwTX;
                                            recordStation2.XLftTx = st2._lFT;
                                            recordStation2.XGmaxTx = st2._gMax;
                                            recordStation2.XAz1Tx = st2._azimuth;
                                            recordStation2.XAz2Tx = st2.A1[indP2];
                                            recordStation2.XDeltaGTx = st2.GT[indP2];
                                            recordStation2.XDistance = st2.DISTANCE[indP2];
                                            recordStation2.XLorH = st2.L525orHata[indP2];
                                            recordStation2.XKtbfRx = cur_st._ktbf;
                                            recordStation2.XFreqRx = st2.CURRX[indP2];
                                            recordStation2.XDeltaFreqRx = cur_st._bwRX;
                                            recordStation2.XLftRx = cur_st._lFR;
                                            recordStation2.XGmaxRx = cur_st._gMax;
                                            recordStation2.XAz1Rx = cur_st._azimuth;
                                            recordStation2.XAz2Rx = st2.A2[indP2];
                                            recordStation2.XDeltaGRx = st2.GA[indP2];
                                            recordStation2.XDisEmis = st2.DesEmi;
                                            recordStation2.XHeigthAntennaInterf = st2._aboveEarthLevel;
                                            recordStation2.XHeigthAntennaVictim = cur_st._aboveEarthLevel;
                                            // =======================================
                                            //Заполнение помехи
                                            recordIntermod.TableName.Value = cur_st._table;
                                            recordIntermod.StationId.Value = cur_st._tableID;
                                            recordIntermod.Owner.Value = cur_st.OwnerName;
                                            recordIntermod.StationName.Value = "";
                                            recordIntermod.Frequency.Value = st1.CURRX[indP1];
                                            recordIntermod.Power.Value = Math.Max(st1._power, st1._power);
                                            recordIntermod.AzimuthTx.Value = cur_st._azimuth;
                                            recordIntermod.AzimuthRx.Value = cur_st._azimuth;
                                            recordIntermod.Gtx.Value = cur_st._gMax;
                                            recordIntermod.Grx.Value = cur_st._gMax;
                                            recordIntermod.Distance.Value = 0.0;
                                            recordIntermod.Loss.Value = 0.0;
                                            recordIntermod.Irf.Value = 0.0;
                                            recordIntermod.DeltaF.Value = 0.0;
                                            recordIntermod.Pi.Value = Pinterfirence;
                                            recordIntermod.Status.Value = cur_st.Status;
                                            recordIntermod.Standard.Value = cur_st._standart;
                                            recordIntermod.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                            recordIntermod.Degradation.Value = cur_st._ktbf;
                                            if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                                                CopyRow(recordFreq, recordIntermod);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), FrequncyRX.ToStringNullD(), recordFreq.Children.Count);
                    recordFreq.Owner.Value = cur_st.OwnerName;
                    if (recordSector.Pi.Value < recordFreq.Pi.Value)
                        CopyRow(recordSector, recordFreq);

                    if (recordFreq.Children.Count > 0)
                        recordSector.Children.Add(recordFreq);
                }
                recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
                recordSector.Owner.Value = cur_st.OwnerName;
                if (recordSector.Children.Count > 0)
                    lstShowingData_SRT_intermod.Add(recordSector);
            }
            return lstShowingData_SRT_intermod;
        }
        public List<TreeColumnRowBase> calc_intermod_1_1_1_for_MOB()
        {
            Color colorOfStation = Colors.LightCoral;                                                // Заливка паралельно
            List<TreeColumnRowBase> lstShowingData_SRT_intermod = new List<TreeColumnRowBase>();     // создание переменной заливки
            int indexSector = 0;
            foreach (Station cur_st in cur_st_list)                                                  // Цикл по станциям (секторам) жертвам
            {
                //чистка станций от расчетов
                foreach (Station st in st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }
                TreeViewIntermodulation recordSector = new TreeViewIntermodulation();                // создание переменной для заливки сектора
                indexSector++;
                foreach (Double FrequncyRX in cur_st.STFrequncyRX)                                   // Цикл по частотам
                {
                    TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();              // Создание переменной для заливки частоты    
                    int indexCounter = 1;
                    foreach (Station st1 in st_list)
                    {
                        foreach (Double FrequncyTX1 in st1.STFrequncyTX)
                        {
                            if (Math.Abs(FrequncyTX1 - FrequncyRX) > max_d_f) { continue; }
                            foreach (Station st2 in st_list)
                            {
                                foreach (Double FrequncyTX2 in st2.STFrequncyTX)
                                {
                                    if (Math.Abs(FrequncyTX2 - FrequncyRX) > max_d_f) { continue; }
                                    foreach (Station st3 in st_list)
                                    {
                                        foreach (Double FrequncyTX3 in st2.STFrequncyTX)
                                        {
                                            if (Math.Abs(FrequncyTX3 - FrequncyRX) > max_d_f) { continue; }
                                            // тут добрались до конкретных частот 
                                            // условия появления интермодуляционных помех
                                            if ((intermod_hit_1_1_1(st1._bwTX, FrequncyTX1, st2._bwTX, FrequncyTX2, st3._bwTX, FrequncyTX3, cur_st._bwRX, FrequncyRX)) && ((st1._tableID != st2._tableID) || (FrequncyTX1 != FrequncyTX2)) && ((st2._tableID != st3._tableID) || (FrequncyTX2 != FrequncyTX3)) && ((st1._tableID != st3._tableID) || (FrequncyTX1 != FrequncyTX3)))
                                            {
                                                // таки да мы попали надо провести расчет помех
                                                // Условия чтоб лишний раз не считать. 
                                                int indP1 = 0; // индекс помехи 1
                                                int indP2 = 0; // индекс помехи 2
                                                int indP3 = 0; // индекс помехи 2
                                                if (st1.PI.Count == 0)
                                                {
                                                    // т.е. расчетов ранее не было вобще 
                                                    calc_intermod_pi_st(cur_st, FrequncyRX, st1, FrequncyTX1);
                                                }
                                                else
                                                {
                                                    Boolean popad = false;
                                                    for (int i = 0; i < st1.CURTX.Count; i++)
                                                    {
                                                        if ((st1.CURTX[i] == FrequncyTX1) && (st1.CURRX[i] == FrequncyRX))
                                                        {
                                                            popad = true;
                                                            indP1 = i;
                                                            break;
                                                        }
                                                    }
                                                    if (!popad)
                                                    {
                                                        calc_intermod_pi_st(cur_st, FrequncyRX, st1, FrequncyTX1);
                                                        indP1 = st1.PI.Count - 1;
                                                    }
                                                }
                                                if (st2.PI.Count == 0)
                                                {
                                                    // т.е. расчетов ранее не было вобще 
                                                    calc_intermod_pi_st(cur_st, FrequncyRX, st2, FrequncyTX2);
                                                }
                                                else
                                                {
                                                    Boolean popad = false;
                                                    for (int i = 0; i < st2.CURTX.Count; i++)
                                                    {
                                                        if ((st2.CURTX[i] == FrequncyTX2) && (st2.CURRX[i] == FrequncyRX))
                                                        {
                                                            popad = true;
                                                            indP2 = i;
                                                            break;
                                                        }
                                                    }
                                                    if (!popad)
                                                    {
                                                        calc_intermod_pi_st(cur_st, FrequncyRX, st2, FrequncyTX2);
                                                        indP2 = st2.PI.Count - 1;
                                                    }
                                                }
                                                if (st3.PI.Count == 0)
                                                {
                                                    // т.е. расчетов ранее не было вобще 
                                                    calc_intermod_pi_st(cur_st, FrequncyRX, st3, FrequncyTX3);
                                                }
                                                else
                                                {
                                                    Boolean popad = false;
                                                    for (int i = 0; i < st3.CURTX.Count; i++)
                                                    {
                                                        if ((st3.CURTX[i] == FrequncyTX3) && (st3.CURRX[i] == FrequncyRX))
                                                        {
                                                            popad = true;
                                                            indP3 = i;
                                                            break;
                                                        }
                                                    }
                                                    if (!popad)
                                                    {
                                                        calc_intermod_pi_st(cur_st, FrequncyRX, st3, FrequncyTX3);
                                                        indP3 = st3.PI.Count - 1;
                                                    }
                                                }
                                                // тут все расчеті произведены осталось тупа посчитать уровень помехи.
                                                Double P1 = st1.PI[indP1] - calc_loss_preselector(st1.DELTAFREQ[indP1], cur_st._bwRX);
                                                Double P2 = st2.PI[indP2] - calc_loss_preselector(st2.DELTAFREQ[indP2], cur_st._bwRX);
                                                Double P3 = st3.PI[indP3] - calc_loss_preselector(st3.DELTAFREQ[indP3], cur_st._bwRX);
                                                Double Pinterfirence = calc_pow_intermod_1_1_1(P1, P2, P3) - add_loss_for_UMTS_5_1_1_1(st1.CURTX[indP1], st2.CURTX[indP2], st3.CURTX[indP3], st1.CURRX[indP1],st1._bwTX ,cur_st._bwRX);
                                                if (Pinterfirence - porog_interf > cur_st._ktbf)
                                                {
                                                    // вот теперь можно говорить о том что есть помеха на данной частоте и следовательно надо формировать запись для вывода 
                                                    TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();                            // Создание записи помехи                                 
                                                    recordFreq.Children.Add(recordIntermod);                                                           // Добавление помехи в частоту      
                                                    recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++); //Добавление данных помехи
                                                    TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();                            // Создание записи станции1  
                                                    recordIntermod.Children.Add(recordStation1);                                                       // Добавление данных станции1 
                                                    recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                                                    recordStation1.Description.BackColor = colorOfStation;
                                                    recordStation1.TableName.Value = st1._table;
                                                    recordStation1.StationId.Value = st1._tableID;
                                                    recordStation1.Owner.Value = st1.OwnerName;
                                                    recordStation1.StationName.Value = st1.StationName;
                                                    recordStation1.Frequency.Value = st1.CURTX[indP1];
                                                    recordStation1.Power.Value = st1._power;
                                                    recordStation1.AzimuthTx.Value = st1.A1[indP1];
                                                    recordStation1.AzimuthRx.Value = st1.A2[indP1];
                                                    recordStation1.Gtx.Value = st1.GT[indP1];
                                                    recordStation1.Grx.Value = st1.GA[indP1];
                                                    recordStation1.Distance.Value = st1.DISTANCE[indP1];
                                                    recordStation1.Loss.Value = st1.L525orHata[indP1];
                                                    recordStation1.Irf.Value = st1.IRF[indP1];
                                                    recordStation1.DeltaF.Value = st1.DELTAFREQ[indP1];
                                                    recordStation1.Pi.Value = st1.PI[indP1];
                                                    recordStation1.Pi.BackColor = colorOfStation;
                                                    recordStation1.Status.Value = st1.Status;
                                                    recordStation1.Standard.Value = st1._standart;
                                                    recordStation1.Standard.BackColor = colorOfStation;
                                                    recordStation1.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                                    recordStation1.DeltaDegradation.BackColor = colorOfStation;
                                                    recordStation1.Degradation.Value = cur_st._ktbf;
                                                    //----------
                                                    recordStation1.XPowerTx = st1._power;
                                                    recordStation1.XFreqTx = st1.CURTX[indP1];
                                                    recordStation1.XDeltaFreqTx = st1._bwTX;
                                                    recordStation1.XLftTx = st1._lFT;
                                                    recordStation1.XGmaxTx = st1._gMax;
                                                    recordStation1.XAz1Tx = st1._azimuth;
                                                    recordStation1.XAz2Tx = st1.A1[indP1];
                                                    recordStation1.XDeltaGTx = st1.GT[indP1];
                                                    recordStation1.XDistance = st1.DISTANCE[indP1];
                                                    recordStation1.XLorH = st1.L525orHata[indP1];
                                                    recordStation1.XKtbfRx = cur_st._ktbf;
                                                    recordStation1.XFreqRx = st1.CURRX[indP1];
                                                    recordStation1.XDeltaFreqRx = cur_st._bwRX;
                                                    recordStation1.XLftRx = cur_st._lFR;
                                                    recordStation1.XGmaxRx = cur_st._gMax;
                                                    recordStation1.XAz1Rx = cur_st._azimuth;
                                                    recordStation1.XAz2Rx = st1.A2[indP1];
                                                    recordStation1.XDeltaGRx = st1.GA[indP1];
                                                    recordStation1.XDisEmis = st1.DesEmi;
                                                    recordStation1.XHeigthAntennaInterf = st1._aboveEarthLevel;
                                                    recordStation1.XHeigthAntennaVictim = cur_st._aboveEarthLevel;
                                                    // =================================
                                                    TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();                             // Создание записи станции2 
                                                    recordIntermod.Children.Add(recordStation2);                                                        // Добавление данных станции1 
                                                    recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                                                    recordStation2.Description.BackColor = colorOfStation;
                                                    recordStation2.TableName.Value = st2._table; ;
                                                    recordStation2.StationId.Value = st2._tableID;
                                                    recordStation2.Owner.Value = st2.OwnerName;
                                                    recordStation2.StationName.Value = st2.StationName;
                                                    recordStation2.Frequency.Value = st2.CURTX[indP2];
                                                    recordStation2.Power.Value = st2._power;
                                                    recordStation2.AzimuthTx.Value = st2.A1[indP2];
                                                    recordStation2.AzimuthRx.Value = st2.A2[indP2];
                                                    recordStation2.Gtx.Value = st2.GT[indP2];
                                                    recordStation2.Grx.Value = st2.GA[indP2];
                                                    recordStation2.Distance.Value = st2.DISTANCE[indP2];
                                                    recordStation2.Loss.Value = st2.L525orHata[indP2];
                                                    recordStation2.Irf.Value = st2.IRF[indP2];
                                                    recordStation2.DeltaF.Value = st2.DELTAFREQ[indP2];
                                                    recordStation2.Pi.Value = st2.PI[indP2];
                                                    recordStation2.Pi.BackColor = colorOfStation;
                                                    recordStation2.Status.Value = st2.Status;
                                                    recordStation2.Standard.Value = st2._standart;
                                                    recordStation2.Standard.BackColor = colorOfStation;
                                                    recordStation2.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                                    recordStation2.DeltaDegradation.BackColor = colorOfStation;
                                                    recordStation2.Degradation.Value = cur_st._ktbf;
                                                    //----------
                                                    recordStation2.XPowerTx = st2._power;
                                                    recordStation2.XFreqTx = st2.CURTX[indP2];
                                                    recordStation2.XDeltaFreqTx = st2._bwTX;
                                                    recordStation2.XLftTx = st2._lFT;
                                                    recordStation2.XGmaxTx = st2._gMax;
                                                    recordStation2.XAz1Tx = st2._azimuth;
                                                    recordStation2.XAz2Tx = st2.A1[indP2];
                                                    recordStation2.XDeltaGTx = st2.GT[indP2];
                                                    recordStation2.XDistance = st2.DISTANCE[indP2];
                                                    recordStation2.XLorH = st2.L525orHata[indP2];
                                                    recordStation2.XKtbfRx = cur_st._ktbf;
                                                    recordStation2.XFreqRx = st2.CURRX[indP2];
                                                    recordStation2.XDeltaFreqRx = cur_st._bwRX;
                                                    recordStation2.XLftRx = cur_st._lFR;
                                                    recordStation2.XGmaxRx = cur_st._gMax;
                                                    recordStation2.XAz1Rx = cur_st._azimuth;
                                                    recordStation2.XAz2Rx = st2.A2[indP2];
                                                    recordStation2.XDeltaGRx = st2.GA[indP2];
                                                    recordStation2.XDisEmis = st2.DesEmi;
                                                    recordStation2.XHeigthAntennaInterf = st2._aboveEarthLevel;
                                                    recordStation2.XHeigthAntennaVictim = cur_st._aboveEarthLevel;
                                                    // ==========
                                                    // создание станции 3
                                                    TreeViewIntermodulation recordStation3 = new TreeViewIntermodulation();                             // Создание записи станции3 
                                                    recordIntermod.Children.Add(recordStation3);                                                        // Добавление данных станции3 
                                                    recordStation3.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "3");
                                                    recordStation3.Description.BackColor = colorOfStation;
                                                    recordStation3.TableName.Value = st3._table; ;
                                                    recordStation3.StationId.Value = st3._tableID;
                                                    recordStation3.Owner.Value = st3.OwnerName;
                                                    recordStation3.StationName.Value = st3.StationName;
                                                    recordStation3.Frequency.Value = st3.CURTX[indP3];
                                                    recordStation3.Power.Value = st3._power;
                                                    recordStation3.AzimuthTx.Value = st3.A1[indP3];
                                                    recordStation3.AzimuthRx.Value = st3.A2[indP3];
                                                    recordStation3.Gtx.Value = st3.GT[indP3];
                                                    recordStation3.Grx.Value = st3.GA[indP3];
                                                    recordStation3.Distance.Value = st3.DISTANCE[indP3];
                                                    recordStation3.Loss.Value = st3.L525orHata[indP3];
                                                    recordStation3.Irf.Value = st3.IRF[indP3];
                                                    recordStation3.DeltaF.Value = st3.DELTAFREQ[indP3];
                                                    recordStation3.Pi.Value = st3.PI[indP3];
                                                    recordStation3.Pi.BackColor = colorOfStation;
                                                    recordStation3.Status.Value = st3.Status;
                                                    recordStation3.Standard.Value = st3._standart;
                                                    recordStation3.Standard.BackColor = colorOfStation;
                                                    recordStation3.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                                    recordStation3.DeltaDegradation.BackColor = colorOfStation;
                                                    recordStation3.Degradation.Value = cur_st._ktbf;
                                                    //----------
                                                    recordStation3.XPowerTx = st3._power;
                                                    recordStation3.XFreqTx = st3.CURTX[indP3];
                                                    recordStation3.XDeltaFreqTx = st3._bwTX;
                                                    recordStation3.XLftTx = st3._lFT;
                                                    recordStation3.XGmaxTx = st3._gMax;
                                                    recordStation3.XAz1Tx = st3._azimuth;
                                                    recordStation3.XAz2Tx = st3.A1[indP3];
                                                    recordStation3.XDeltaGTx = st3.GT[indP3];
                                                    recordStation3.XDistance = st3.DISTANCE[indP3];
                                                    recordStation3.XLorH = st3.L525orHata[indP3];
                                                    recordStation3.XKtbfRx = cur_st._ktbf;
                                                    recordStation3.XFreqRx = st3.CURRX[indP3];
                                                    recordStation3.XDeltaFreqRx = cur_st._bwRX;
                                                    recordStation3.XLftRx = cur_st._lFR;
                                                    recordStation3.XGmaxRx = cur_st._gMax;
                                                    recordStation3.XAz1Rx = cur_st._azimuth;
                                                    recordStation3.XAz2Rx = st3.A2[indP3];
                                                    recordStation3.XDeltaGRx = st3.GA[indP3];
                                                    recordStation3.XDisEmis = st3.DesEmi;
                                                    recordStation3.XHeigthAntennaInterf = st3._aboveEarthLevel;
                                                    recordStation3.XHeigthAntennaVictim = cur_st._aboveEarthLevel;
                                                    // =======================================
                                                    //Заполнение помехи
                                                    recordIntermod.TableName.Value = cur_st._table;
                                                    recordIntermod.StationId.Value = cur_st._tableID;
                                                    recordIntermod.Owner.Value = cur_st.OwnerName;
                                                    recordIntermod.StationName.Value = "";
                                                    recordIntermod.Frequency.Value = st1.CURRX[indP1];
                                                    recordIntermod.Power.Value = Math.Max(st1._power, st1._power);
                                                    recordIntermod.AzimuthTx.Value = cur_st._azimuth;
                                                    recordIntermod.AzimuthRx.Value = cur_st._azimuth;
                                                    recordIntermod.Gtx.Value = cur_st._gMax;
                                                    recordIntermod.Grx.Value = cur_st._gMax;
                                                    recordIntermod.Distance.Value = 0.0;
                                                    recordIntermod.Loss.Value = 0.0;
                                                    recordIntermod.Irf.Value = 0.0;
                                                    recordIntermod.DeltaF.Value = 0.0;
                                                    recordIntermod.Pi.Value = Pinterfirence;
                                                    recordIntermod.Status.Value = cur_st.Status;
                                                    recordIntermod.Standard.Value = cur_st._standart;
                                                    recordIntermod.DeltaDegradation.Value = Pinterfirence - cur_st._ktbf;
                                                    recordIntermod.Degradation.Value = cur_st._ktbf;
                                                    if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                                                        CopyRow(recordFreq, recordIntermod);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), FrequncyRX.ToStringNullD(), recordFreq.Children.Count);
                    recordFreq.Owner.Value = cur_st.OwnerName;
                    if (recordSector.Pi.Value < recordFreq.Pi.Value)
                        CopyRow(recordSector, recordFreq);

                    if (recordFreq.Children.Count > 0)
                        recordSector.Children.Add(recordFreq);
                }
                recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
                recordSector.Owner.Value = cur_st.OwnerName;
                if (recordSector.Children.Count > 0)
                    lstShowingData_SRT_intermod.Add(recordSector);
            }
            return lstShowingData_SRT_intermod;
        }
        public List<TreeColumnRowBase> calc_intermod_2_1_for_MOB_SRT()
        {
            Color colorOfStation = Colors.LightCoral;                                                // Заливка паралельно
            List<TreeColumnRowBase> lstShowingData_SRT_intermod = new List<TreeColumnRowBase>();     // создание переменной заливки
            int indexSector = 0;
            foreach (Station st_vict in st_list_vict)                                                  // Цикл по станциям (секторам) жертвам
            {
                //чистка станций от расчетов
                foreach (Station st in st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }
                foreach (Station st in cur_st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }

                TreeViewIntermodulation recordSector = new TreeViewIntermodulation();                // создание переменной для заливки сектора
                indexSector++;
                foreach (Double FrequncyRX in st_vict.STFrequncyRX)                                   // Цикл по частотам
                {
                    TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();              // Создание переменной для заливки частоты    
                    int indexCounter = 1;
                    foreach (Station cur_st in cur_st_list)
                    {
                        foreach (Double FrequncyCur in cur_st.STFrequncyTX)
                        {
                            foreach (Station st in st_list)
                            {
                                foreach (Double FrequncyTX in st.STFrequncyTX)
                                {
                                    // тут добрались до конкретных частот 
                                    // условия появления интермодуляционных помех (а их два)
                                    if ((intermod_hit_2_1(cur_st._bwTX, FrequncyCur, st._bwTX, FrequncyTX, st_vict._bwRX, FrequncyRX)))
                                    {
                                        // таки да мы попали надо провести расчет помех
                                        // Условия чтоб лишний раз не считать. 
                                        int indcur = 0; // индекс помехи 1
                                        int indst = 0; // индекс помехи 2
                                        calc_pow_qvick(cur_st, st_vict, FrequncyRX, FrequncyCur, ref indcur);
                                        calc_pow_qvick(st, st_vict, FrequncyRX, FrequncyTX, ref indst);
                                        
                                        // тут все расчеті произведены осталось тупа посчитать уровень помехи.
                                        Double P1 = cur_st.PI[indcur] - calc_loss_preselector(cur_st.DELTAFREQ[indcur], st_vict._bwRX);
                                        Double P2 = st.PI[indst] - calc_loss_preselector(st.DELTAFREQ[indst], cur_st._bwRX);
                                        Double Pinterfirence = calc_pow_intermod_2_1(P1, P2) - add_loss_for_UMTS_5_2_1(cur_st.CURTX[indcur], st.CURTX[indst], cur_st.CURRX[indcur], cur_st._bwTX, st_vict._bwRX);
                                        if (Pinterfirence - porog_interf > cur_st._ktbf)
                                        {
                                            // вот теперь можно говорить о том что есть помеха на данной частоте и следовательно надо формировать запись для вывода 
                                            TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();                            // Создание записи помехи                                 
                                            recordFreq.Children.Add(recordIntermod);                                                           // Добавление помехи в частоту      
                                            recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++); //Добавление данных помехи
                                            TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();                            // Создание записи станции1  
                                            recordIntermod.Children.Add(recordStation1);                                                       // Добавление данных станции1 
                                            recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                                            fill_recordStation(ref recordStation1, cur_st, st_vict, indcur, Pinterfirence, colorOfStation);
                                            // =================================
                                            TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();                             // Создание записи станции2 
                                            recordIntermod.Children.Add(recordStation2);                                                        // Добавление данных станции1 
                                            recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                                            fill_recordStation(ref recordStation2, st, st_vict, indst, Pinterfirence, colorOfStation);
                                            // =======================================
                                            //Заполнение помехи
                                            fill_recordIntermod(ref recordIntermod, st_vict, Pinterfirence, cur_st.CURRX[indcur], Math.Max(st._power, cur_st._power));
                                            if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                                                CopyRow(recordFreq, recordIntermod);
                                        }
                                    }
                                    if ((intermod_hit_2_1(st._bwTX, FrequncyTX, cur_st._bwTX, FrequncyCur, st_vict._bwRX, FrequncyRX)))
                                    {
                                        // таки да мы попали надо провести расчет помех
                                        // Условия чтоб лишний раз не считать. 
                                        int indcur = 0; // индекс помехи 1
                                        int indst = 0; // индекс помехи 2
                                        calc_pow_qvick(cur_st, st_vict, FrequncyRX, FrequncyCur, ref indcur);
                                        calc_pow_qvick(st, st_vict, FrequncyRX, FrequncyTX, ref indst);
                                        // тут все расчеті произведены осталось тупа посчитать уровень помехи.
                                        Double P2 = cur_st.PI[indcur] - calc_loss_preselector(cur_st.DELTAFREQ[indcur], st_vict._bwRX);
                                        Double P1 = st.PI[indst] - calc_loss_preselector(st.DELTAFREQ[indst], cur_st._bwRX);
                                        Double Pinterfirence = calc_pow_intermod_2_1(P1, P2) - add_loss_for_UMTS_5_2_1(cur_st.CURTX[indcur], st.CURTX[indst], cur_st.CURRX[indcur], st._bwTX, st_vict._bwRX);
                                        if (Pinterfirence - porog_interf > cur_st._ktbf)
                                        {
                                            // вот теперь можно говорить о том что есть помеха на данной частоте и следовательно надо формировать запись для вывода 
                                            TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();                            // Создание записи помехи                                 
                                            recordFreq.Children.Add(recordIntermod);                                                           // Добавление помехи в частоту      
                                            recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++); //Добавление данных помехи
                                            TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();                            // Создание записи станции1  
                                            recordIntermod.Children.Add(recordStation1);                                                       // Добавление данных станции1 
                                            recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                                            fill_recordStation(ref recordStation1, st, st_vict, indst, Pinterfirence, colorOfStation);
                                            // =================================
                                            TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();                             // Создание записи станции2 
                                            recordIntermod.Children.Add(recordStation2);                                                        // Добавление данных станции1 
                                            recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                                            fill_recordStation(ref recordStation2, cur_st, st_vict, indcur, Pinterfirence, colorOfStation);
                                            // =======================================
                                            //Заполнение помехи
                                            fill_recordIntermod(ref recordIntermod, st_vict, Pinterfirence, cur_st.CURRX[indcur], Math.Max(st._power, cur_st._power));
                                            if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                                                CopyRow(recordFreq, recordIntermod);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), FrequncyRX.ToStringNullD(), recordFreq.Children.Count);
                    recordFreq.Owner.Value = st_vict.OwnerName;
                    if (recordSector.Pi.Value < recordFreq.Pi.Value)
                        CopyRow(recordSector, recordFreq);

                    if (recordFreq.Children.Count > 0)
                        recordSector.Children.Add(recordFreq);
                }
                recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
                recordSector.Owner.Value = st_vict.OwnerName;
                if (recordSector.Children.Count > 0)
                    lstShowingData_SRT_intermod.Add(recordSector);
            }
            return lstShowingData_SRT_intermod;
        }
        public List<TreeColumnRowBase> calc_intermod_1_1_1_for_MOB_SRT()
        {
            Color colorOfStation = Colors.LightCoral;                                                // Заливка паралельно
            List<TreeColumnRowBase> lstShowingData_SRT_intermod = new List<TreeColumnRowBase>();     // создание переменной заливки
            int indexSector = 0;
            foreach (Station st_vict in st_list_vict)                                                  // Цикл по станциям (секторам) жертвам
            {
                //чистка станций от расчетов
                foreach (Station st in st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }
                foreach (Station st in cur_st_list)
                {
                    st.DISTANCE.Clear();
                    st.EVP.Clear();
                    st.L525orHata.Clear();
                    st.DELTAFREQ.Clear();
                    st.GT.Clear();
                    st.GA.Clear();
                    st.A1.Clear();
                    st.A2.Clear();
                    st.B1.Clear();
                    st.B2.Clear();
                    st.PI.Clear();
                    st.IRF.Clear();
                    st.CURTX.Clear();
                    st.CURRX.Clear();
                }
                TreeViewIntermodulation recordSector = new TreeViewIntermodulation();                // создание переменной для заливки сектора
                indexSector++;
                foreach (Double FrequncyRX in st_vict.STFrequncyRX)                                   // Цикл по частотам
                {
                    TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();              // Создание переменной для заливки частоты    
                    int indexCounter = 1;
                    foreach (Station cur_st in cur_st_list)
                    {
                        foreach (Double FrequncyCur in cur_st.STFrequncyTX)
                        {
                            foreach (Station st1 in st_list)
                            {
                                foreach (Double FrequncyTX1 in st1.STFrequncyTX)
                                {
                                    foreach (Station st2 in st_list)
                                    {
                                        foreach (Double FrequncyTX2 in st2.STFrequncyTX)
                                        {
                                            // тут добрались до конкретных частот 
                                            // условия появления интермодуляционных помех
                                            calc_itermod_combin_for_1_1_1(cur_st, FrequncyCur, st1, FrequncyTX1, st2, FrequncyTX2, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                            calc_itermod_combin_for_1_1_1(cur_st, FrequncyCur, st2, FrequncyTX2, st1, FrequncyTX1, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                            calc_itermod_combin_for_1_1_1(st1, FrequncyTX1, cur_st, FrequncyCur, st2, FrequncyTX2, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                            calc_itermod_combin_for_1_1_1(st2, FrequncyTX2, cur_st, FrequncyCur, st1, FrequncyTX1, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                            calc_itermod_combin_for_1_1_1(st1, FrequncyTX1, st2, FrequncyTX2, cur_st, FrequncyCur, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                            calc_itermod_combin_for_1_1_1(st2, FrequncyTX2, st1, FrequncyTX1, cur_st, FrequncyCur, st_vict, FrequncyRX, ref indexCounter, ref recordFreq, colorOfStation);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), FrequncyRX.ToStringNullD(), recordFreq.Children.Count);
                    recordFreq.Owner.Value = st_vict.OwnerName;
                    if (recordSector.Pi.Value < recordFreq.Pi.Value)
                        CopyRow(recordSector, recordFreq);

                    if (recordFreq.Children.Count > 0)
                        recordSector.Children.Add(recordFreq);
                }
                recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
                recordSector.Owner.Value = st_vict.OwnerName;
                if (recordSector.Children.Count > 0)
                    lstShowingData_SRT_intermod.Add(recordSector);
            }
            return lstShowingData_SRT_intermod;
        }
        private Boolean intermod_hit_2_1(Double BWTR1, Double FTR1, Double BWTR2, Double FTR2, Double BWRS, Double FRS)
        {
            Double f1 = 2 * FTR1 + BWTR1 - FTR2 + BWTR2 / 2;
            Double f2 = 2 * FTR1 - BWTR1 - FTR2 - BWTR2 / 2;
            if (((f1 > FRS) && (f2 < FRS)) || ((Math.Abs(FRS - f1) <= BWRS / 2.0) || (Math.Abs(FRS - f2) <= BWRS / 2.0))) // тут временная правка для отладки
            {
                return true;
            }
            else
            {
                return false;
            }

        }// функция определения есть ли интермодуляционная помеха по типу 2+1
        private Boolean intermod_hit_1_1_1(Double BWTR1, Double FTR1, Double BWTR2, Double FTR2, Double BWTR3, Double FTR3, Double BWRS, Double FRS)
        {
            Double f1 = FTR1 + BWTR1 / 2 + FTR2 + BWTR2 / 2 - FTR3 + BWTR3 / 2;
            Double f2 = FTR1 - BWTR1 / 2 + FTR2 - BWTR2 / 2 - FTR3 - BWTR3 / 2;
            if (((f1 > FRS) && (f2 < FRS)) || ((Math.Abs(FRS - f1) <= BWRS / 2.0) || (Math.Abs(FRS - f2) <= BWRS / 2.0))) // тут временная правка для отладки
            {
                return true;
            }
            else
            {
                return false;
            }

        }// функция определения есть ли интермодуляционная помеха по типу 1+1+1
        private void calc_intermod_pi_st(Station cur_st, Double FRX, Station st, Double FTX)
        {
            double d = StationUtility.AntennaLength(cur_st._longitude, cur_st._latitude, st._longitude, st._latitude);
            // receive azimuths
            double a1 = 0, a2 = 180; // азимуты
            double b1 = 0, b2 = 0; // углы места
            StationUtility.AntennaAzimuth(st._longitude, st._latitude, cur_st._longitude, cur_st._latitude, ref a1, ref a2);
            // receive corner place
            if (d != 0)
                StationUtility.CornerPlace(d, cur_st._aboveSeaLevel, st._aboveSeaLevel, ref b1, ref b2);

            double deltaA1 = Math.Abs(st._azimuth - a1);
            double deltaA2 = Math.Abs(cur_st._azimuth - a2);
            double deltaB1 = Math.Abs(st._bElevation - b1);
            double deltaB2 = Math.Abs(cur_st._bElevation - b2);
            double l525 = 60;
            if (d != 0)
                if (FTX >= 500 || d <= 5)
                {
                    double TXggc = FTX / 1000; // переводим в ГГц
                    // calculates L525
                    l525 = L525.GetL525(TXggc, d);
                }
                else
                {
                    // calculates HATA
                    l525 = HATA.GetL(FTX, d, cur_st._aboveEarthLevel, st._aboveEarthLevel);
                }

            AntennaDiagramm diagH = new AntennaDiagramm();
            diagH.SetMaximalGain(st._gMax);
            diagH.Build(st._aDiagH);
            double GH = diagH.GetLossesAmount(deltaA1);
            AntennaDiagramm diagV = new AntennaDiagramm();
            diagV.SetMaximalGain(st._gMax);
            diagV.Build(st._bDiagV);
            double GV = diagV.GetLossesAmount(deltaB1);
            double GT = Math.Min(GH, GV);
            AntennaDiagramm diagH_ = new AntennaDiagramm();
            diagH_.SetMaximalGain(cur_st._gMax);
            diagH_.Build(cur_st._aDiagH);
            GH = diagH_.GetLossesAmount(deltaA2);
            AntennaDiagramm diagV_ = new AntennaDiagramm();
            diagV_.SetMaximalGain(cur_st._gMax);
            diagV_.Build(cur_st._bDiagV);
            GV = diagV_.GetLossesAmount(deltaB2);
            double Ga = Math.Min(GH, GV);


            double irf;
            if (st._table == "MOB_STATION2") // временная метка на тот случай если производится расчет станции СРТ (пока временно)
            {
                irf = ACHMob.GetIRF(FTX, FRX, st._freqFilterTX, cur_st._freqFilterRX, st._bwTX, true, st._maskaT, new List<ACHMob.DataElement>(), new List<ACHMob.DataElement>(), cur_st._filterR);
            }
            else
            {
                irf = ACHMob.GetIRF(FTX, FRX, st._freqFilterTX, cur_st._freqFilterRX, st._bwTX, true, st._maskaT, new List<ACHMob.DataElement>(), st._filterT, cur_st._filterR);
            }
            double evp = st._power + GT - st._lFT;
            double PI = evp - l525 - cur_st._lFR + Ga - irf;
            double deltafreq = FTX - FRX;
            st.DISTANCE.Add(d);
            st.EVP.Add(evp);
            st.L525orHata.Add(l525);
            st.DELTAFREQ.Add(deltafreq);
            st.GT.Add(GT);
            st.GA.Add(Ga);
            st.A1.Add(a1);
            st.A2.Add(a2);
            st.B1.Add(b1);
            st.B2.Add(b2);
            st.PI.Add(PI);
            st.IRF.Add(irf);
            st.CURTX.Add(FTX);
            st.CURRX.Add(FRX);
        } // Расчет помех между станциями на заданных частотах
        private Double calc_loss_preselector(Double df, Double BF)
        {
            BF = 40; // просто тупа задал
            return 60.0 * Math.Log10((2.0 * df / BF) * (2.0 * df / BF) + 1.0);
        }// расчет ослабления вносимого перселектором в дБ
        private Double calc_pow_intermod_2_1(Double P1, Double P2)
        {
            Double Pein = (2.0 * P1 + P2) / 3.0;
            Double Pimp = 3.0 * (Pein + G) - 2.0 * IP3;
            Double Pint = Pimp - G;
            return Pint;
        }// Расчет мощности интермодуляционной помехи
        private Double calc_pow_intermod_1_1_1(Double P1, Double P2, Double P3)
        {
            Double Pein = (P1 + P2 + P3) / 3.0;
            Double Pimp = 3.0 * (Pein + G) - 2.0 * IP3 + 6;
            Double Pint = Pimp - G;
            return Pint;
        }// Расчет мощности интермодуляционной помехи
        private Double add_loss_for_UMTS_5_2_1(Double FTX1, Double FTX2, Double FRX, Double dfTX, Double dfRX)
        {
            Double f_inter_centr = 2 * FTX1 - FTX2;
            Double delta = Math.Abs(f_inter_centr - FRX);
            if (dfRX >= dfTX)
            {
                delta = delta * (5 / dfTX) - (dfRX * (5 / dfTX)/ 2 - 2.5);
            }
            else 
            {
                delta = delta * (5 / dfTX);
            }
            Double loss;
            if (delta <= 0.1)
            {
                loss = 3;
            }
            else
            {
                if (delta <= 1)
                {
                    loss = 3.2;
                }
                else
                {
                    if (delta <= 2)
                    {
                        loss = 3.4;
                    }
                    else
                    {
                        if (delta <= 3)
                        {
                            loss = 3.9;
                        }
                        else
                        {
                            if (delta <= 4)
                            {
                                loss = 4.8;
                            }
                            else
                            {
                                if (delta <= 5)
                                {
                                    loss = 6.2;
                                }
                                else
                                {
                                    if (delta <= 6)
                                    {
                                        loss = 8.2;
                                    }
                                    else
                                    {
                                        if (delta <= 7)
                                        {
                                            loss = 10.8;
                                        }
                                        else
                                        {
                                            if (delta <= 8)
                                            {
                                                loss = 14.4;
                                            }
                                            else
                                            {
                                                if (delta <= 9)
                                                {
                                                    loss = 21;
                                                }
                                                else
                                                {
                                                    loss = 999;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            loss = loss - 10*Math.Log10(dfTX/dfRX);
            if (loss > 0)
            {
                return loss;
            }
            else
            {
                return 0;
            }
          }//Поправка на неравномерность спектра
        private Double add_loss_for_UMTS_5_1_1_1(Double FTX1, Double FTX2, Double FTX3, Double FRX, Double dfTX, Double dfRX)
        {
            Double f_inter_centr = FTX1 + FTX2 - FTX3;
            Double delta = Math.Abs(f_inter_centr - FRX);
            if (dfRX >= dfTX)
            {
                delta = delta * (5 / dfTX) - (dfRX * (5 / dfTX) / 2 - 2.5);
            }
            else
            {
                delta = delta * (5 / dfTX);
            }
            Double loss;
            if (delta <= 0.1)
            {
                loss = 1.8;
            }
            else
            {
                if (delta <= 1)
                {
                    loss = 2;
                }
                else
                {
                    if (delta <= 2)
                    {
                        loss = 2.7;
                    }
                    else
                    {
                        if (delta <= 3)
                        {
                            loss = 3.9;
                        }
                        else
                        {
                            if (delta <= 4)
                            {
                                loss = 5.6;
                            }
                            else
                            {
                                if (delta <= 5)
                                {
                                    loss = 7.9;
                                }
                                else
                                {
                                    if (delta <= 6)
                                    {
                                        loss = 10.9;
                                    }
                                    else
                                    {
                                        if (delta <= 7)
                                        {
                                            loss = 14.7;
                                        }
                                        else
                                        {
                                            if (delta <= 8)
                                            {
                                                loss = 20.1;
                                            }
                                            else
                                            {
                                                if (delta <= 9)
                                                {
                                                    loss = 29.6;
                                                }
                                                else
                                                {
                                                    loss = 999;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            loss = loss - 10 * Math.Log10(dfTX / dfRX);
            if (loss > 0)
            {
                return loss;
            }
            else
            {
                return 0;
            }
        }//Поправка на неравномерность спектра
        private void CopyRow(TreeViewIntermodulation recordDest, TreeViewIntermodulation recordSourc)
        {
            recordDest.StationId.Value = recordSourc.StationId.Value;
            recordDest.TableName.Value = recordSourc.TableName.Value;
            recordDest.Pi.Value = recordSourc.Pi.Value;
            recordDest.Irf.Value = recordSourc.Irf.Value;
            recordDest.DeltaDegradation.Value = recordSourc.DeltaDegradation.Value;
            recordDest.Frequency.Value = recordSourc.Frequency.Value;
            recordDest.FreqRx.Value = recordSourc.FreqRx.Value;
            recordDest.DeltaF.Value = recordSourc.DeltaF.Value;
            recordDest.Power.Value = recordSourc.Power.Value;
            recordDest.Distance.Value = recordSourc.Distance.Value;
            recordDest.Grx.Value = recordSourc.Grx.Value;
            recordDest.Gtx.Value = recordSourc.Gtx.Value;
            recordDest.Degradation.Value = recordSourc.Degradation.Value;
            recordDest.Status.Value = recordSourc.Status.Value;
            recordDest.Loss.Value = recordSourc.Loss.Value;
            recordDest.AzimuthTx.Value = recordSourc.AzimuthTx.Value;
            recordDest.AzimuthRx.Value = recordSourc.AzimuthRx.Value;
            recordDest.StationName.Value = recordSourc.StationName.Value;
            recordDest.Owner.Value = recordSourc.Owner.Value;
            recordDest.Standard.Value = recordSourc.Standard.Value;
        }// копирует строки
        private void fill_recordStation(ref TreeViewIntermodulation recordStation, Station st, Station st_vict, int st_id, Double Pinterfirence, Color colorOfStation)
        {
            recordStation.Description.BackColor = colorOfStation;
            recordStation.TableName.Value = st._table;
            recordStation.StationId.Value = st._tableID;
            recordStation.Owner.Value = st.OwnerName;
            recordStation.StationName.Value = st.StationName;
            recordStation.Frequency.Value = st.CURTX[st_id];
            recordStation.Power.Value = st._power;
            recordStation.AzimuthTx.Value = st.A1[st_id];
            recordStation.AzimuthRx.Value = st.A2[st_id];
            recordStation.Gtx.Value = st.GT[st_id];
            recordStation.Grx.Value = st.GA[st_id];
            recordStation.Distance.Value = st.DISTANCE[st_id];
            recordStation.Loss.Value = st.L525orHata[st_id];
            recordStation.Irf.Value = st.IRF[st_id];
            recordStation.DeltaF.Value = st.DELTAFREQ[st_id];
            recordStation.Pi.Value = st.PI[st_id];
            recordStation.Pi.BackColor = colorOfStation;
            recordStation.Status.Value = st.Status;
            recordStation.Standard.Value = st._standart;
            recordStation.Standard.BackColor = colorOfStation;
            recordStation.DeltaDegradation.Value = Pinterfirence - st_vict._ktbf;
            recordStation.DeltaDegradation.BackColor = colorOfStation;
            recordStation.Degradation.Value = st_vict._ktbf;
            recordStation.XFreqTx = st.CURTX[st_id];
            recordStation.XDeltaFreqTx = st._bwTX;
            recordStation.XLftTx = st._lFT;
            recordStation.XGmaxTx = st._gMax;
            recordStation.XAz1Tx = st._azimuth;
            recordStation.XAz2Tx = st.A1[st_id];
            recordStation.XDeltaGTx = st.GT[st_id];
            recordStation.XDistance = st.DISTANCE[st_id];
            recordStation.XLorH = st.L525orHata[st_id];
            recordStation.XKtbfRx = st_vict._ktbf;
            recordStation.XFreqRx = st.CURRX[st_id];
            recordStation.XDeltaFreqRx = st_vict._bwRX;
            recordStation.XLftRx = st_vict._lFR;
            recordStation.XGmaxRx = st_vict._gMax;
            recordStation.XAz1Rx = st_vict._azimuth;
            recordStation.XAz2Rx = st.A2[st_id];
            recordStation.XDeltaGRx = st.GA[st_id];
            recordStation.XDisEmis = st.DesEmi;
            recordStation.XHeigthAntennaInterf = st._aboveEarthLevel;
            recordStation.XHeigthAntennaVictim = st_vict._aboveEarthLevel;
        } // вспомогательная функция - нужна для заливки данных в одну строку отображения
        private void fill_recordIntermod(ref TreeViewIntermodulation recordIntermod, Station st_vict, Double Pinterfirence, Double freq_rx, Double pow)
        {
            recordIntermod.TableName.Value = st_vict._table;
            recordIntermod.StationId.Value = st_vict._tableID;
            recordIntermod.Owner.Value = st_vict.OwnerName;
            recordIntermod.StationName.Value = "";
            recordIntermod.Frequency.Value = freq_rx;
            recordIntermod.Power.Value = pow;
            recordIntermod.AzimuthTx.Value = st_vict._azimuth;
            recordIntermod.AzimuthRx.Value = st_vict._azimuth;
            recordIntermod.Gtx.Value = st_vict._gMax;
            recordIntermod.Grx.Value = st_vict._gMax;
            recordIntermod.Distance.Value = 0.0;
            recordIntermod.Loss.Value = 0.0;
            recordIntermod.Irf.Value = 0.0;
            recordIntermod.DeltaF.Value = 0.0;
            recordIntermod.Pi.Value = Pinterfirence;
            recordIntermod.Status.Value = st_vict.Status;
            recordIntermod.Standard.Value = st_vict._standart;
            recordIntermod.DeltaDegradation.Value = Pinterfirence - st_vict._ktbf;
            recordIntermod.Degradation.Value = st_vict._ktbf;
        }
        private void calc_pow_qvick(Station st, Station st_vict, Double FrequncyRX, Double FrequncyTX, ref int indst)
        {
            indst = 0; 
            if (st.PI.Count == 0)
            {
                // т.е. расчетов ранее не было вобще 
                calc_intermod_pi_st(st_vict, FrequncyRX, st, FrequncyTX);
            }
            else
            {
                Boolean popad = false;
                for (int i = 0; i < st.CURTX.Count; i++)
                {
                    if ((st.CURTX[i] == FrequncyTX) && (st.CURRX[i] == FrequncyRX))
                    {
                        popad = true;
                        indst = i;
                        break;
                    }
                }
                if (!popad)
                {
                    calc_intermod_pi_st(st_vict, FrequncyRX, st, FrequncyTX);
                    indst = st.PI.Count - 1;
                }
            }
        }// тут быстрый расчет помех между двух станций. быстрый потомучто если расчет был то просто возвращаем индекс с расчетом
        private void calc_itermod_combin_for_1_1_1(Station cur_st, Double FrequncyCur, Station st1, Double FrequncyTX1, Station st2, Double FrequncyTX2, Station st_vict, Double FrequncyRX, ref int indexCounter, ref TreeViewIntermodulation recordFreq, Color colorOfStation)
        {
            if ((intermod_hit_1_1_1(cur_st._bwTX, FrequncyCur, st1._bwTX, FrequncyTX1, st2._bwTX, FrequncyTX2, st_vict._bwRX, FrequncyRX)))
            {
                // таки да мы попали надо провести расчет помех
                // Условия чтоб лишний раз не считать. 
                int indcur = 0; // индекс помехи 1
                int indst1 = 0; // индекс помехи 2
                int indst2 = 0; // индекс помехи 2
                calc_pow_qvick(cur_st, st_vict, FrequncyRX, FrequncyCur, ref indcur);
                calc_pow_qvick(st1, st_vict, FrequncyRX, FrequncyTX1, ref indst1);
                calc_pow_qvick(st2, st_vict, FrequncyRX, FrequncyTX2, ref indst2);

                // тут все расчеті произведены осталось тупа посчитать уровень помехи.
                Double P1 = cur_st.PI[indcur] - calc_loss_preselector(cur_st.DELTAFREQ[indcur], st_vict._bwRX);
                Double P2 = st1.PI[indst1] - calc_loss_preselector(st1.DELTAFREQ[indst1], st_vict._bwRX);
                Double P3 = st2.PI[indst2] - calc_loss_preselector(st2.DELTAFREQ[indst2], st_vict._bwRX);
                Double Pinterfirence = calc_pow_intermod_1_1_1(P1, P2, P3) - add_loss_for_UMTS_5_1_1_1(cur_st.CURTX[indcur], st1.CURTX[indst1], st2.CURTX[indst2], cur_st.CURRX[indcur], cur_st._bwTX, st_vict._bwRX);
                if (Pinterfirence - porog_interf > st_vict._ktbf)
                {
                    // вот теперь можно говорить о том что есть помеха на данной частоте и следовательно надо формировать запись для вывода 
                    TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();                            // Создание записи помехи                                 
                    recordFreq.Children.Add(recordIntermod);                                                           // Добавление помехи в частоту      
                    recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++); //Добавление данных помехи
                    TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();                            // Создание записи станции1  
                    recordIntermod.Children.Add(recordStation1);                                                       // Добавление данных станции1 
                    recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                    fill_recordStation(ref recordStation1, cur_st, st_vict, indcur, Pinterfirence, colorOfStation);
                    // ================================= // создание станции 2
                    TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();                             // Создание записи станции2 
                    recordIntermod.Children.Add(recordStation2);                                                        // Добавление данных станции1 
                    recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                    fill_recordStation(ref recordStation2, st1, st_vict, indst1, Pinterfirence, colorOfStation);
                    // ==========// создание станции 3
                    TreeViewIntermodulation recordStation3 = new TreeViewIntermodulation();                             // Создание записи станции3 
                    recordIntermod.Children.Add(recordStation3);                                                        // Добавление данных станции3 
                    recordStation3.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "3");
                    fill_recordStation(ref recordStation3, st2, st_vict, indst2, Pinterfirence, colorOfStation);
                    // =======================================
                    //Заполнение помехи
                    fill_recordIntermod(ref recordIntermod, st_vict, Pinterfirence, cur_st.CURRX[indcur], Math.Max(Math.Max(st1._power, st2._power), cur_st._power));
                    if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                        CopyRow(recordFreq, recordIntermod);
                }
            }
        }// определяет наличии помехи интермодуляции если есть то ее считает и заливает в форму отчета
    }
    }
  

   

