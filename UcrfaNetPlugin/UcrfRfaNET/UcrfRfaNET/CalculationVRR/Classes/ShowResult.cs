﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using ComponentsLib;
using ICSM;
using XICSM.Intermodulation;
using System.Collections;
using XICSM.UcrfRfaNET.Calculation.ResultForm;
using IdwmNET;
using NearestCountryItem = IdwmNET.NearestCountryItem;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class ViborkaVRR
   {

       private static Idwm _idwmVal;
       public static bool _isIdwmInit;
       public static bool isOne;


       private static string GetCalcBorder(double X, double Y, double LonDec, double LatDec)
       {
           try
           {
               if (!_isIdwmInit)
               {
                   _idwmVal = new Idwm();
                   _isIdwmInit = _idwmVal.Init(11); //Init IDWM
               }

           }
           catch
           {
               _isIdwmInit = false;
           }

           string Out = "";
           if (_isIdwmInit)
           {
               if ((X != IM.NullD) && (Y != IM.NullD))
               {

                   float lonDec = (float)LonDec;
                   float latDec = (float)LatDec;
                   string[] exclude = { "UKR" };
                   NearestCountryItem[] nearestCountry = _idwmVal.GetNearestCountries(Idwm.DecToRadian(lonDec), Idwm.DecToRadian(latDec), 1000, exclude, 100);
                   string outStr = "~";
                   if (nearestCountry.Length > 0)
                       //outStr = (((double)(nearestCountry[0].distance)).Round(2)).ToString();
                       outStr = string.Format("{0} Km ({1})", ((double)(nearestCountry[0].distance)).Round(2), nearestCountry[0].country);
                   Out = outStr;
               }
           }
           return Out;
       }


       /// <summary>
       /// Форимуем данные для отображения и отображаем результат подсчета
       /// </summary>
       public void ShowResultForm()
       {
           foreach (Station curStation in _curStation)
               curStation.LoadAdditionalData();
           LoadAdditionalDataOfStations(_stListZhertvaMobSectors,
                                        _stListPomehaMobSectors,
                                        _stListPomehaMobSRTSectors,
                                        _stListZhertvaBlockSectors,
                                        _stListPomehaBlockSectors,
                                        _stListPomehaBlockSRTSectors,
                                        _stListZhertvaAbonentSectors,
                                        _stListPomehaAbonentSectors);
           using (FrmCalculationResultVrr resultCalculationForm = new FrmCalculationResultVrr())
           {
               using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Generating the result form...")))
               {
                   const int maxCounter = 10;
                   int curCounter = 1;
                   resultCalculationForm.LblAzimuth = "";
                   resultCalculationForm.LblDistanceBord = "";
                   foreach (Station st in _curStation)
                   {
                       resultCalculationForm._ListRowCalc.Add(st._tableID, st._azimuth);
                       resultCalculationForm.IdsOfCurStation.Add(st._tableID);
                       if (string.IsNullOrEmpty(resultCalculationForm.LblAzimuth) == false)
                           resultCalculationForm.LblAzimuth += "; ";
                       resultCalculationForm.LblAzimuth += st._azimuth.ToStringNullD();
                   }



                   resultCalculationForm.LblDistanceBord = GetCalcBorder(_curStation[0].X, _curStation[0].Y, _curStation[0].Lon, _curStation[0].Lat);

                   //resultCalculationForm.LblDistanceBord = _curStation[0]._distance_board.ToString();
                   resultCalculationForm.TableNameOfCurStation = _curStation[0]._table;
                   resultCalculationForm.LblOwner = _curStation[0].OwnerName;
                   resultCalculationForm.LblAddress = _curStation[0].Address;
                   {
                       HashSet<string> numChannel = new HashSet<string>();
                       Station.ChannelsList channels = new Station.ChannelsList();
                       foreach (Station st in _curStation)
                           foreach (Station.Channel channel in st.Chennels.Channels)
                           {
                               if (numChannel.Contains(channel.ChannelTx) == false)
                               {
                                   channels.Add(channel);
                                   numChannel.Add(channel.ChannelTx);
                               }

                           }
                       resultCalculationForm.LblFreq = string.Format("{0} / {2} / {1} / {3}", channels.FreqTxString,
                                                                     channels.FreqRxString,
                                                                     channels.ChannelTxString,
                                                                     channels.ChannelTxString);
                   }
                   resultCalculationForm.LblTypeStation = _curStation[0]._standart;
                   resultCalculationForm.LblClass = _curStation[0].DesEmi;
                   resultCalculationForm.LblStationName = _curStation[0].StationName;
                   resultCalculationForm.LblPower = _curStation[0]._power;
                   resultCalculationForm.LblSencetivity = _curStation[0]._sensitivity;
                   resultCalculationForm.LblG = _curStation[0]._gMax;
                   resultCalculationForm.LblH = _curStation[0]._aboveEarthLevel;
                   //------
                   resultCalculationForm.LblDiapazon = string.Format("{0} - {1}", _freqInterMin, _freqInterMax);
                   resultCalculationForm.LblRadius = _radiusMob;
                   resultCalculationForm.LblRadInterm = _radiusInter;
                   resultCalculationForm.LblPorog = _PIMob;
                   //---
                   resultCalculationForm.StationName = _curStation[0].StationName;
                   //------
                   //===========
                   // MOB
                   //Формируем список всех выбраных станций (Жертвы в основном и соседнем канале)
                   List<TreeColumnRowBase> lstShowingData = GenerateDataSacrificeChannel(_stListZhertvaMobSectors);
                   _stListZhertvaMobSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Sacrifice;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Sacrifice"),
                                                                                            lstShowingData, "SacrificeEmc");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = GenerateDataInterferenceChannel(_stListPomehaMobSectors);
                   _stListPomehaMobSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Interference Co-Ch"),
                                                                                            lstShowingData, "InterferenceEmc");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   //==========
                   
                   //MOB SRT 
                   lstShowingData = GenerateDataInterferenceChannel(_stListPomehaMobSRTSectors);
                   _stListPomehaMobSRTSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Interference SRT"),
                                                                                            lstShowingData, "InterferenceSRT");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   //==========

                   // Intermod
                   lstShowingData = GenerateDataVictimIntermodulation(_stListZhertvaInterSector);
                   _stListZhertvaInterSector.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Victim;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Victim Intermodulation"), lstShowingData, "VictimIntermodulation");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = GenerateDataInterferenceIntermodulation(_stListPomehaInterSector);
                   _stListPomehaInterSector.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab =
                           resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Interference Intermodulation"), lstShowingData,
                                                                    "InterferenceIntermodulation");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   //===============
                   //Intermod SRT 
                   
                   lstShowingData = lstShowingData_SRT_intermod_2_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Interference Intermodulation SRT 2-1"), lstShowingData, "InterferenceIntermodulationSRT2-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   
                   lstShowingData = lstShowingData_SRT_intermod_1_1_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Interference Intermodulation SRT 1+1-1"), lstShowingData, "InterferenceIntermodulationSRT1+1-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   
                   lstShowingData = lstShowingData_SRT_MOB_intermod_2_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Victim;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Victim Intermodulation SRT 2-1"), lstShowingData, "VictimIntermodulationSRT2-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = lstShowingData_SRT_MOB_intermod_1_1_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Victim;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Victim Intermodulation SRT 1+1-1"), lstShowingData, "VictimIntermodulationSRT1+1-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = lstShowingData_MOB_to_SRT_intermod_2_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Victim;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Victim Intermodulation MOB 2-1"), lstShowingData, "VictimIntermodulationMOB2-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = lstShowingData_MOB_to_SRT_intermod_1_1_1;
                   if (lstShowingData != null)
                   {
                       if (lstShowingData.Count > 0)
                       {
                           resultCalculationForm.TypeResCalc = enumTypeCalc.Victim;
                           TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Victim Intermodulation MOB 1+1-1"), lstShowingData, "VictimIntermodulationMOB1+1-1");
                           newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                           newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                       }
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   //===============
                   // Block
                   lstShowingData = GenerateDataSacrificeBlock(_stListZhertvaBlockSectors);
                   _stListZhertvaBlockSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Sacrifice;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Block Victim"),
                                                                                            lstShowingData, "BlockVictim");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = GenerateDataInterferenceBlock(_stListPomehaBlockSectors);
                   _stListPomehaBlockSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Block Interference"),
                                                                                            lstShowingData, "BlockInterference");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   //================

                   // Block SRT
                   lstShowingData = GenerateDataInterferenceBlock(_stListPomehaBlockSRTSectors);
                   _stListPomehaBlockSRTSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Block Interference SRT"),
                                                                                            lstShowingData, "BlockInterferenceSRT");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
                   //================

                   // Abonent
                   lstShowingData = GenerateDataSacrificeAbonent(_stListZhertvaAbonentSectors);
                   _stListZhertvaAbonentSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Sacrifice;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Abonent Victim (CurStation -> Abonent)"),
                                                                                            lstShowingData, "AbonentVictim");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);

                   lstShowingData = GenerateDataInterferenceAbonent(_stListPomehaAbonentSectors);
                   _stListPomehaAbonentSectors.Clear(); //Данные уже не нужны
                   if (lstShowingData.Count > 0)
                   {
                       resultCalculationForm.TypeResCalc = enumTypeCalc.Interference;
                       TreeColumnWinForms newTab = resultCalculationForm.TabsTree.AddNewTab(CLocaliz.TxT("Abonent Interference (Station -> Abonent)"),
                                                                                            lstShowingData, "AbonentInterference");
                       newTab.TreeColumn.OnSelectedItemChanged += resultCalculationForm.SelectedItemChanged;
                       newTab.TreeColumn.OnSelectedItemDoubleClick += resultCalculationForm.MouseDoubleClickRow;
                   }
                   pBar.ShowProgress(curCounter++, maxCounter);
               }
               resultCalculationForm.ShowDialog();
           }
       }

       #region Intermodulation
       /// <summary>
       /// Формирует данные для отображения интермод. помех
       /// </summary>
       /// <param name="listPomehaInterSector">список интермод. помех</param>
       /// <returns>Список данных для отображения</returns>
       private List<TreeColumnRowBase> GenerateDataInterferenceIntermodulation(List<List<IntermodulationInterference>> listPomehaInterSector)
       {
           Color colorOfStation = Colors.LightCoral;
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           for (int indexSector = 0; indexSector < listPomehaInterSector.Count; indexSector++)
           {
               TreeViewIntermodulation recordSector = new TreeViewIntermodulation();
               Station curStation = _curStation[indexSector];
               List<IntermodulationInterference> sector = listPomehaInterSector[indexSector];
               while (sector.Count > 0)
               {
                   int indexCounter = 1;
                   int indexStation = 0;
                   double curFreq = IM.NullD;
                   curFreq = sector[indexStation].Receiver.Frequency;

                   TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();
                   
                   while (indexStation < sector.Count)
                   {
                       IntermodulationInterference station = sector[indexStation];
                       if (Math.Abs(curFreq - station.Receiver.Frequency) < 0.0001)
                       {
                           TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();
                           if (!(_needOwner && (station.Transmitter1.Station.GetOwner() == curStation.OwnerName) &&
                                (station.Transmitter2.Station.GetOwner() == curStation.OwnerName)))
                           {
                               recordFreq.Children.Add(recordIntermod);
                               recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Interference"), indexCounter++);
                           }
                           //-----------
                           // Станция 1
                           {
                               TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();
                               recordIntermod.Children.Add(recordStation1);
                               recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                               recordStation1.Description.BackColor = colorOfStation;
                               recordStation1.TableName.Value = station.Transmitter1.Station.TableName;
                               recordStation1.StationId.Value = station.Transmitter1.Station.GetID();
                               recordStation1.Owner.Value = station.Transmitter1.Station.GetOwner();
                               recordStation1.StationName.Value = station.Transmitter1.Station.GetName();
                               recordStation1.Frequency.Value = station.Transmitter1.Frequency;
                               recordStation1.Power.Value = station.Transmitter1.Station.GetPower().dBm2W();
                               recordStation1.AzimuthTx.Value = station.TransmitterAzimuth1;
                               recordStation1.AzimuthRx.Value = station.ReceiverAzimuth1;
                               recordStation1.Gtx.Value = station.TransmitterGain1;
                               recordStation1.Grx.Value = station.ReceiverGain1;
                               recordStation1.Distance.Value = station.Range1;
                               if (station.Range1 < 0.0001)
                                   recordStation1.Loss.Value = 60.0;
                               else
                                   recordStation1.Loss.Value = 92.45 + 20.0 * Math.Log10(curFreq / 1000.0 * station.Range1);
                               recordStation1.Irf.Value = station.Preselector1;
                               recordStation1.DeltaF.Value = Math.Abs(curFreq - station.Transmitter1.Frequency);
                               recordStation1.Pi.Value = station.Power1.dBm2W();
                               recordStation1.Pi.BackColor = colorOfStation;
                               recordStation1.Status.Value = station.Transmitter1.Station.Status;
                               recordStation1.Standard.Value = station.Transmitter1.Station.GetStandard();
                               recordStation1.Standard.BackColor = colorOfStation;
                               recordStation1.DeltaDegradation.Value = station.SumPower - curStation._ktbf;
                               recordStation1.DeltaDegradation.BackColor = colorOfStation;
                               recordStation1.Degradation.Value = curStation._ktbf;
                               //----------
                               recordStation1.XPowerTx = station.Transmitter1.Station.GetPower();
                               recordStation1.XFreqTx = station.Transmitter1.Frequency;
                               recordStation1.XDeltaFreqTx = station.Transmitter1.Station.GetBandwidth();
                               recordStation1.XLftTx = station.Transmitter1.Station.GetTxLosses();
                               recordStation1.XGmaxTx = station.Transmitter1.Station.GetMaxGain();
                               recordStation1.XAz1Tx = station.Transmitter1.Station.GetAzimuth();
                               recordStation1.XAz2Tx = station.TransmitterAzimuth1;
                               recordStation1.XDeltaGTx = station.TransmitterGain1;
                               recordStation1.XDistance = station.Range1;
                               recordStation1.XLorH = recordStation1.Loss.Value;
                               recordStation1.XKtbfRx = curStation._ktbf;
                               recordStation1.XFreqRx = curFreq;
                               recordStation1.XDeltaFreqRx = curStation._bwRX;
                               recordStation1.XLftRx = curStation._lFR;
                               recordStation1.XGmaxRx = curStation._gMax;
                               recordStation1.XAz1Rx = curStation._azimuth;
                               recordStation1.XAz2Rx = station.ReceiverAzimuth1;
                               recordStation1.XDeltaGRx = station.ReceiverGain1;
                               recordStation1.XDisEmis = station.Transmitter1.Station.DesEmi;
                               recordStation1.XHeigthAntennaInterf = station.Transmitter1.Station.GetHeight();
                               recordStation1.XHeigthAntennaVictim = curStation._aboveEarthLevel;
                           }
                           // Станция 2
                           {
                               TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();
                               recordIntermod.Children.Add(recordStation2);
                               recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                               recordStation2.Description.BackColor = colorOfStation;
                               recordStation2.TableName.Value = station.Transmitter2.Station.TableName;
                               recordStation2.StationId.Value = station.Transmitter2.Station.GetID();
                               recordStation2.Owner.Value = station.Transmitter2.Station.GetOwner();
                               recordStation2.StationName.Value = station.Transmitter2.Station.GetName();
                               recordStation2.Frequency.Value = station.Transmitter2.Frequency;
                               recordStation2.Power.Value = station.Transmitter2.Station.GetPower().dBm2W();
                               recordStation2.AzimuthTx.Value = station.TransmitterAzimuth2;
                               recordStation2.AzimuthRx.Value = station.ReceiverAzimuth2;
                               recordStation2.Gtx.Value = station.TransmitterGain2;
                               recordStation2.Grx.Value = station.ReceiverGain2;
                               recordStation2.Distance.Value = station.Range2;
                               if (station.Range2 < 0.0001)
                                   recordStation2.Loss.Value = 60.0;
                               else
                                   recordStation2.Loss.Value = 92.45 + 20.0 * Math.Log10(curFreq / 1000.0 * station.Range2);
                               recordStation2.Irf.Value = station.Preselector2;
                               recordStation2.DeltaF.Value = Math.Abs(curFreq - station.Transmitter2.Frequency);
                               recordStation2.Pi.Value = station.Power2;
                               recordStation2.Pi.BackColor = colorOfStation;
                               recordStation2.Status.Value = station.Transmitter2.Station.Status;
                               recordStation2.Standard.Value = station.Transmitter2.Station.GetStandard();
                               recordStation2.Standard.BackColor = colorOfStation;
                               recordStation2.DeltaDegradation.Value = station.SumPower - curStation._ktbf;
                               recordStation2.DeltaDegradation.BackColor = colorOfStation;
                               recordStation2.Degradation.Value = curStation._ktbf;
                               //----------
                               recordStation2.XPowerTx = station.Transmitter2.Station.GetPower();
                               recordStation2.XFreqTx = station.Transmitter2.Frequency;
                               recordStation2.XDeltaFreqTx = station.Transmitter2.Station.GetBandwidth();
                               recordStation2.XLftTx = station.Transmitter2.Station.GetTxLosses();
                               recordStation2.XGmaxTx = station.Transmitter2.Station.GetMaxGain();
                               recordStation2.XAz1Tx = station.Transmitter2.Station.GetAzimuth();
                               recordStation2.XAz2Tx = station.TransmitterAzimuth2;
                               recordStation2.XDeltaGTx = station.TransmitterGain2;
                               recordStation2.XDistance = station.Range2;
                               recordStation2.XLorH = recordStation2.Loss.Value;
                               recordStation2.XKtbfRx = curStation._ktbf;
                               recordStation2.XFreqRx = curFreq;
                               recordStation2.XDeltaFreqRx = curStation._bwRX;
                               recordStation2.XLftRx = curStation._lFR;
                               recordStation2.XGmaxRx = curStation._gMax;
                               recordStation2.XAz1Rx = curStation._azimuth;
                               recordStation2.XAz2Rx = station.ReceiverAzimuth2;
                               recordStation2.XDeltaGRx = station.ReceiverGain2;
                               recordStation2.XDisEmis = station.Transmitter2.Station.DesEmi;
                               recordStation2.XHeigthAntennaInterf = station.Transmitter2.Station.GetHeight();
                               recordStation2.XHeigthAntennaVictim = curStation._aboveEarthLevel;
                           }
                           //---------------------
                           //Заполнение помехи
                           recordIntermod.TableName.Value = curStation._table;
                           recordIntermod.StationId.Value = curStation._tableID;
                           recordIntermod.Owner.Value = curStation.OwnerName;
                           recordIntermod.StationName.Value = "";
                           recordIntermod.Frequency.Value = curFreq;
                           recordIntermod.Power.Value = Math.Max(station.Transmitter1.Station.GetPower(), station.Transmitter2.Station.GetPower()).dBm2W();
                           recordIntermod.AzimuthTx.Value = curStation._azimuth;
                           recordIntermod.AzimuthRx.Value = curStation._azimuth;
                           recordIntermod.Gtx.Value = curStation._gMax;
                           recordIntermod.Grx.Value = curStation._gMax;
                           recordIntermod.Distance.Value = 0.0;
                           recordIntermod.Loss.Value = 0.0;
                           recordIntermod.Irf.Value = 0.0;
                           recordIntermod.DeltaF.Value = 0.0;
                           recordIntermod.Pi.Value = station.SumPower;
                           recordIntermod.Status.Value = curStation.Status;
                           recordIntermod.Standard.Value = curStation._standart;
                           recordIntermod.DeltaDegradation.Value = station.SumPower - curStation._ktbf;
                           recordIntermod.Degradation.Value = curStation._ktbf;
                           //----
                           if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                               CopyRow(recordFreq, recordIntermod);
                           //----
                           sector.RemoveAt(indexStation);
                       }
                       else
                           indexStation++;
                   }
                   recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), curFreq.ToStringNullD(), recordFreq.Children.Count);
                   recordFreq.Owner.Value = curStation.OwnerName;
                   if (recordSector.Pi.Value < recordFreq.Pi.Value)
                       CopyRow(recordSector, recordFreq);

                   if (recordFreq.Children.Count>0)
                       recordSector.Children.Add(recordFreq);
               }
               recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
               recordSector.Owner.Value = curStation.OwnerName;
               if (recordSector.Children.Count > 0)
                   lstShowingData.Add(recordSector);
           }
           return lstShowingData;
       }
       /// <summary>
       /// Формирует данные для отображения интермод. жертв
       /// </summary>
       /// <param name="listZhertvaInterSector">список интермод. жертв</param>
       /// <returns>Список данных для отображения</returns>
       private List<TreeColumnRowBase> GenerateDataVictimIntermodulation(List<List<IntermodulationInterference>> listZhertvaInterSector)
       {
           Color colorOfStation = Colors.LightCoral;
           Color colorOfCurStation = Colors.Green;
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           for (int indexSector = 0; indexSector < listZhertvaInterSector.Count; indexSector++)
           {
               TreeViewIntermodulation recordSector = new TreeViewIntermodulation();
               Station curStation = _curStation[indexSector];
               List<IntermodulationInterference> sector = listZhertvaInterSector[indexSector];
               while (sector.Count > 0)
               {
                   int indexCounter = 1;
                   int indexStation = 0;
                   double curFreq = IM.NullD;
                   curFreq = sector[indexStation].Receiver.Frequency;

                   TreeViewIntermodulation recordFreq = new TreeViewIntermodulation();
                   
                   while (indexStation < sector.Count)
                   {
                       IntermodulationInterference station = sector[indexStation];
                       
                       if (Math.Abs(curFreq - station.Receiver.Frequency) < 0.0001)
                       {
                           TreeViewIntermodulation recordIntermod = new TreeViewIntermodulation();
                           if (!(_needOwner && (station.Transmitter1.Station.GetOwner() == station.Receiver.Station.GetOwner()) &&
                                (station.Transmitter2.Station.GetOwner() == station.Receiver.Station.GetOwner())))
                           {
                               recordFreq.Children.Add(recordIntermod);
                               recordIntermod.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Victim"), indexCounter++);
                           }
                           //-----------
                           // Станция 1
                           {
                               TreeViewIntermodulation recordStation1 = new TreeViewIntermodulation();
                               recordIntermod.Children.Add(recordStation1);
                               recordStation1.StationId.Value = station.Transmitter1.Station.GetID();
                               recordStation1.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "1");
                               recordStation1.Owner.Value = station.Transmitter1.Station.GetOwner();

                               if (recordStation1.StationId.Value == curStation._tableID)
                                   recordStation1.Description.BackColor = colorOfCurStation;
                               else
                                   recordStation1.Description.BackColor = colorOfStation;

                               recordStation1.TableName.Value = station.Transmitter1.Station.TableName;
                               recordStation1.StationName.Value = station.Transmitter1.Station.GetName();
                               recordStation1.Frequency.Value = station.Transmitter1.Frequency;
                               recordStation1.Power.Value = station.Transmitter1.Station.GetPower().dBm2W();
                               recordStation1.AzimuthTx.Value = station.TransmitterAzimuth1;
                               recordStation1.AzimuthRx.Value = station.ReceiverAzimuth1;
                               recordStation1.Gtx.Value = station.TransmitterGain1;
                               recordStation1.Grx.Value = station.ReceiverGain1;
                               recordStation1.Distance.Value = station.Range1;
                               if (station.Range1 < 0.0001)
                                   recordStation1.Loss.Value = 60.0;
                               else
                                  recordStation1.Loss.Value = 92.45 + 20.0 * Math.Log10(curFreq / 1000.0 * station.Range1);
                               recordStation1.Irf.Value = station.Preselector1;
                               recordStation1.DeltaF.Value = Math.Abs(curFreq - station.Transmitter1.Frequency);
                               recordStation1.Pi.Value = station.Power1;
                               recordStation1.Pi.BackColor = colorOfStation;
                               recordStation1.Status.Value = station.Transmitter1.Station.Status;
                               recordStation1.Standard.Value = station.Transmitter1.Station.GetStandard();
                               recordStation1.Standard.BackColor = colorOfStation;
                               recordStation1.DeltaDegradation.Value = station.SumPower - station.Receiver.Station._ktbf;
                               recordStation1.DeltaDegradation.BackColor = colorOfStation;
                               recordStation1.Degradation.Value = station.Receiver.Station._ktbf;
                               //----------
                               recordStation1.XPowerTx = station.Transmitter1.Station.GetPower();
                               recordStation1.XFreqTx = station.Transmitter1.Frequency;
                               recordStation1.XDeltaFreqTx = station.Transmitter1.Station.GetBandwidth();
                               recordStation1.XLftTx = station.Transmitter1.Station.GetTxLosses();
                               recordStation1.XGmaxTx = station.Transmitter1.Station.GetMaxGain();
                               recordStation1.XAz1Tx = station.Transmitter1.Station.GetAzimuth();
                               recordStation1.XAz2Tx = station.TransmitterAzimuth1;
                               recordStation1.XDeltaGTx = station.TransmitterGain1;
                               recordStation1.XDistance = station.Range1;
                               recordStation1.XLorH = recordStation1.Loss.Value;
                               recordStation1.XKtbfRx = station.Receiver.Station._ktbf;
                               recordStation1.XFreqRx = curFreq;
                               recordStation1.XDeltaFreqRx = station.Receiver.Station.GetBandwidth();
                               recordStation1.XLftRx = station.Receiver.Station.GetRxLosses();
                               recordStation1.XGmaxRx = station.Receiver.Station.GetMaxGain();
                               recordStation1.XAz1Rx = station.Receiver.Station.GetAzimuth();
                               recordStation1.XAz2Rx = station.ReceiverAzimuth1;
                               recordStation1.XDeltaGRx = station.ReceiverGain1;
                               recordStation1.XDisEmis = station.Transmitter1.Station.DesEmi;
                               recordStation1.XHeigthAntennaInterf = station.Transmitter1.Station.GetHeight();
                               recordStation1.XHeigthAntennaVictim = curStation._aboveEarthLevel;
                           }
                           // Станция 2
                           {
                               TreeViewIntermodulation recordStation2 = new TreeViewIntermodulation();
                               recordIntermod.Children.Add(recordStation2);
                               recordStation2.StationId.Value = station.Transmitter2.Station.GetID();
                               recordStation2.Description.Value = string.Format("{0} {1}", CLocaliz.TxT("Station"), "2");
                               recordStation2.Owner.Value = station.Transmitter2.Station.GetOwner();

                               if (recordStation2.StationId.Value == curStation._tableID)
                                   recordStation2.Description.BackColor = colorOfCurStation;
                               else
                                   recordStation2.Description.BackColor = colorOfStation;

                               recordStation2.TableName.Value = station.Transmitter2.Station.TableName;
                               recordStation2.StationName.Value = station.Transmitter2.Station.GetName();
                               recordStation2.Frequency.Value = station.Transmitter2.Frequency;
                               recordStation2.Power.Value = station.Transmitter2.Station.GetPower().dBm2W();
                               recordStation2.AzimuthTx.Value = station.TransmitterAzimuth2;
                               recordStation2.AzimuthRx.Value = station.ReceiverAzimuth2;
                               recordStation2.Gtx.Value = station.TransmitterGain2;
                               recordStation2.Grx.Value = station.ReceiverGain2;
                               recordStation2.Distance.Value = station.Range2;
                               if (station.Range2 < 0.0001)
                                   recordStation2.Loss.Value = 60.0;
                               else
                                   recordStation2.Loss.Value = 92.45 + 20.0 * Math.Log10(curFreq / 1000.0 * station.Range2);
                               recordStation2.Irf.Value = station.Preselector2;
                               recordStation2.DeltaF.Value = Math.Abs(curFreq - station.Transmitter2.Frequency);
                               recordStation2.Pi.Value = station.Power2;
                               recordStation2.Pi.BackColor = colorOfStation;
                               recordStation2.Status.Value = station.Transmitter2.Station.Status;
                               recordStation2.Standard.Value = station.Transmitter2.Station.GetStandard();
                               recordStation2.Standard.BackColor = colorOfStation;
                               recordStation2.DeltaDegradation.Value = station.SumPower - station.Receiver.Station._ktbf;
                               recordStation2.DeltaDegradation.BackColor = colorOfStation;
                               recordStation2.Degradation.Value = station.Receiver.Station._ktbf;
                               //----------
                               recordStation2.XPowerTx = station.Transmitter2.Station.GetPower();
                               recordStation2.XFreqTx = station.Transmitter2.Frequency;
                               recordStation2.XDeltaFreqTx = station.Transmitter2.Station.GetBandwidth();
                               recordStation2.XLftTx = station.Transmitter2.Station.GetTxLosses();
                               recordStation2.XGmaxTx = station.Transmitter2.Station.GetMaxGain();
                               recordStation2.XAz1Tx = station.Transmitter2.Station.GetAzimuth();
                               recordStation2.XAz2Tx = station.TransmitterAzimuth2;
                               recordStation2.XDeltaGTx = station.TransmitterGain2;
                               recordStation2.XDistance = station.Range2;
                               recordStation2.XLorH = recordStation2.Loss.Value;
                               recordStation2.XKtbfRx = station.Receiver.Station._ktbf;
                               recordStation2.XFreqRx = curFreq;
                               recordStation2.XDeltaFreqRx = station.Receiver.Station.GetBandwidth();
                               recordStation2.XLftRx = station.Receiver.Station.GetRxLosses();
                               recordStation2.XGmaxRx = station.Receiver.Station.GetMaxGain();
                               recordStation2.XAz1Rx = station.Receiver.Station.GetAzimuth();
                               recordStation2.XAz2Rx = station.ReceiverAzimuth2;
                               recordStation2.XDeltaGRx = station.ReceiverGain2;
                               recordStation2.XDisEmis = station.Transmitter2.Station.DesEmi;
                               recordStation2.XHeigthAntennaInterf = station.Transmitter2.Station.GetHeight();
                               recordStation2.XHeigthAntennaVictim = curStation._aboveEarthLevel;
                           }
                           //---------------------
                           //Заполнение помехи
                           recordIntermod.TableName.Value = station.Receiver.Station.TableName;
                           recordIntermod.StationId.Value = station.Receiver.Station.GetID();
                           recordIntermod.Owner.Value = station.Receiver.Station.GetOwner();
                           recordIntermod.StationName.Value = "";
                           recordIntermod.Frequency.Value = curFreq;
                           recordIntermod.Power.Value = station.Receiver.Station.GetPower().dBm2W();
                           recordIntermod.AzimuthTx.Value = station.Receiver.Station.GetAzimuth();
                           recordIntermod.AzimuthRx.Value = station.Receiver.Station.GetAzimuth();
                           recordIntermod.Gtx.Value = station.Receiver.Station.GetMaxGain();
                           recordIntermod.Grx.Value = station.Receiver.Station.GetMaxGain();
                           recordIntermod.Distance.Value = 0.0;
                           recordIntermod.Loss.Value = 0.0;
                           recordIntermod.Irf.Value = 0.0;
                           recordIntermod.DeltaF.Value = 0.0;
                           recordIntermod.Pi.Value = station.SumPower;
                           recordIntermod.Status.Value = station.Receiver.Station.Status;
                           recordIntermod.Standard.Value = station.Receiver.Station.GetStandard();
                           recordIntermod.DeltaDegradation.Value = station.SumPower - station.Receiver.Station._ktbf;
                           recordIntermod.Degradation.Value = station.Receiver.Station._ktbf;
                           recordIntermod.IsShowed = false;
                           //----
                           if (recordFreq.Pi.Value < recordIntermod.Pi.Value)
                               CopyRow(recordFreq, recordIntermod);
                           //----
                           sector.RemoveAt(indexStation);
                       }
                       else
                           indexStation++;
                   }                   
                   recordFreq.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Frequency"), curFreq.ToStringNullD(), recordFreq.Children.Count);
                   recordFreq.Owner.Value = curStation.OwnerName;
                   recordFreq.IsShowed = false;
                   if (recordSector.Pi.Value < recordFreq.Pi.Value)
                       CopyRow(recordSector, recordFreq);
                   if (recordFreq.Children.Count>0)     
                       recordSector.Children.Add(recordFreq);
               }
               recordSector.Description.Value = string.Format("{0} {1} ({2})", CLocaliz.TxT("Sector"), indexSector, recordSector.Children.Count);
               recordSector.Owner.Value = curStation.OwnerName;
               if (recordSector.Children.Count > 0)
                   lstShowingData.Add(recordSector);
               recordSector.IsShowed = false;
           }
           return lstShowingData;
       }
       #endregion

       #region Channels
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Основной и соседние канал (Помехи)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataInterferenceChannel(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Помехи в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       CheckOnMaxDataForInterferance(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqRx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqRx;
                           recordFreq.FreqTx.Value = curData.FreqTx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = station._standart;
                           recordFreq.Power.Value = station._power.dBm2W();
                           recordFreq.Owner.Value = station.OwnerName;
                           recordFreq.StationName.Value = station.StationName;
                           recordFreq.Status.Value = station.Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = station._power;
                           recordFreq.XFreqTx = curData.FreqTx;
                           recordFreq.XDeltaFreqTx = station._bwTX;
                           recordFreq.XLftTx = station._lFT;
                           recordFreq.XGmaxTx = station._gMax;
                           recordFreq.XAz1Tx = station._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gt;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = _curStation[indexSector]._ktbf;
                           recordFreq.XFreqRx = curData.FreqRx;
                           recordFreq.XDeltaFreqRx = _curStation[indexSector]._bwRX;
                           recordFreq.XLftRx = _curStation[indexSector]._lFR;
                           recordFreq.XGmaxRx = _curStation[indexSector]._gMax;
                           recordFreq.XAz1Rx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gr;
                           recordFreq.XDisEmis = station.DesEmi;
                           recordFreq.XHeigthAntennaInterf = station._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = _curStation[indexSector]._aboveEarthLevel;
                           //-----
                           if ((ApplSetting.IsDebug == false) ||
                               (NeedShowYkxChannel(_curStation[indexSector]._standart,
                                                   station._standart,
                                                   recordFreq.DelraF.Value,
                                                   recordFreq.XDeltaFreqRx,
                                                   recordFreq.XDeltaFreqTx)))
                           {

                               recordSector.Children.Add(recordFreq);
                               //Обновляем значения сектора
                               if (recordSector.Pi.Value < recordFreq.Pi.Value)
                                   CopyRow(recordSector, recordFreq);
                           }
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       recordSector.Owner.Value = _curStation[indexSector].OwnerName;
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                       recordInTree.Owner.Value = _curStation[indexSector].OwnerName;
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Основной и соседние канал (Жертвы)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataSacrificeChannel(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Жертвы в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       CheckOnMaxDataForSacrifice(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqTx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqRx;
                           recordFreq.FreqTx.Value = curData.FreqTx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = _curStation[indexSector]._standart;
                           recordFreq.Power.Value = _curStation[indexSector]._power.dBm2W();
                           recordFreq.Owner.Value = _curStation[indexSector].OwnerName;
                           recordFreq.StationName.Value = station.StationName;
                           recordFreq.Status.Value = station.Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = _curStation[indexSector]._power;
                           recordFreq.XFreqTx = curData.FreqTx;
                           recordFreq.XDeltaFreqTx = _curStation[indexSector]._bwTX;
                           recordFreq.XLftTx = _curStation[indexSector]._lFT;
                           recordFreq.XGmaxTx = _curStation[indexSector]._gMax;
                           recordFreq.XAz1Tx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gr;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = station._ktbf;
                           recordFreq.XFreqRx = curData.FreqRx;
                           recordFreq.XDeltaFreqRx = station._bwRX;
                           recordFreq.XLftRx = station._lFR;
                           recordFreq.XGmaxRx = station._gMax;
                           recordFreq.XAz1Rx = station._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gt;
                           recordFreq.XDisEmis = _curStation[indexSector].DesEmi;
                           recordFreq.XHeigthAntennaInterf = _curStation[indexSector]._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = station._aboveEarthLevel;
                           //-----
                           if ((ApplSetting.IsDebug == false) ||
                               (NeedShowYkxChannel(_curStation[indexSector]._standart,
                                                   station._standart,
                                                   recordFreq.DelraF.Value,
                                                   recordFreq.XDeltaFreqRx,
                                                   recordFreq.XDeltaFreqTx)))
                           {
                               recordSector.Children.Add(recordFreq);
                               //Обновляем значения сектора
                               if (recordSector.Pi.Value < recordFreq.Pi.Value)
                                   CopyRow(recordSector, recordFreq);
                           }
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       recordSector.Owner.Value = station.OwnerName;
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                       recordInTree.Owner.Value = station.OwnerName;
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       /// <summary>
       /// проверка необходимости отображения данных
       /// </summary>
       /// <param name="standard1">стандарт станции расчета</param>
       /// <param name="standard2">стандарт текущей станции</param>
       /// <param name="dF">дельта частота текущей станции</param>
       /// <param name="dFrx">дельта частота приемника</param>
       /// <param name="dFtx">дельта частота передатчика</param>
       /// <returns>TRUE - выводить станцию</returns>
       private bool NeedShowYkxChannel(string standard1, string standard2, double dF, double dFrx, double dFtx)
       {
           bool retVal = true;
           if((standard1 == "УКХ") &&
               (standard1 == standard2) &&
               (dF > GetDeltaFreq(dFrx)) &&
               (dF > GetDeltaFreq(dFtx)))
               retVal = false;
           return retVal;
       }
       /// <summary>
       /// Формирует корректное значение дельти частоты
       /// </summary>
       /// <param name="deltaFreq">текущая дельта частот</param>
       /// <returns>корректное значение дельти частоты</returns>
       private double GetDeltaFreq(double deltaFreq)
       {
           if (deltaFreq < 12.5)
               return 12.5;
           else if (deltaFreq < 25.0)
               return 25.0;
           return deltaFreq;
       }
       #endregion

       #region Blocks
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Блокировки (Помехи)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataInterferenceBlock(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Помехи в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       CheckOnMaxDataForInterferance(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqRx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqRx;
                           recordFreq.FreqTx.Value = curData.FreqTx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = station._standart;
                           recordFreq.Power.Value = station._power.dBm2W();
                           recordFreq.Owner.Value = station.OwnerName;
                           recordFreq.StationName.Value = station.StationName;
                           recordFreq.Status.Value = station.Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = station._power;
                           recordFreq.XFreqTx = curData.FreqTx;
                           recordFreq.XDeltaFreqTx = station._bwTX;
                           recordFreq.XLftTx = station._lFT;
                           recordFreq.XGmaxTx = station._gMax;
                           recordFreq.XAz1Tx = station._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gt;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = _curStation[indexSector]._ktbf;
                           recordFreq.XFreqRx = curData.FreqRx;
                           recordFreq.XDeltaFreqRx = _curStation[indexSector]._bwRX;
                           recordFreq.XLftRx = _curStation[indexSector]._lFR;
                           recordFreq.XGmaxRx = _curStation[indexSector]._gMax;
                           recordFreq.XAz1Rx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gr;
                           recordFreq.XDisEmis = station.DesEmi;
                           recordFreq.XHeigthAntennaInterf = station._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = _curStation[indexSector]._aboveEarthLevel;
                           //-----
                           recordSector.Children.Add(recordFreq);
                           //Обновляем значения сектора
                           if (recordSector.Pi.Value < recordFreq.Pi.Value)
                               CopyRow(recordSector, recordFreq);
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       recordSector.Owner.Value = _curStation[indexSector].OwnerName;
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           recordInTree.Owner.Value = _curStation[indexSector].OwnerName;
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Блокировки (Жертвы)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataSacrificeBlock(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Жертвы в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       // Требование Козачкова убрать объединение записей (№117)
                       //CheckOnMaxDataForSacrifice(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqTx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqRx;
                           recordFreq.FreqTx.Value = curData.FreqTx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = _curStation[indexSector]._standart;
                           recordFreq.Power.Value = _curStation[indexSector]._power.dBm2W();
                           recordFreq.Owner.Value = _curStation[indexSector].OwnerName;
                           recordFreq.StationName.Value = _curStation[indexSector].StationName;
                           recordFreq.Status.Value = _curStation[indexSector].Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = _curStation[indexSector]._power;
                           recordFreq.XFreqTx = curData.FreqTx;
                           recordFreq.XDeltaFreqTx = _curStation[indexSector]._bwTX;
                           recordFreq.XLftTx = _curStation[indexSector]._lFT;
                           recordFreq.XGmaxTx = _curStation[indexSector]._gMax;
                           recordFreq.XAz1Tx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gr;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = station._ktbf;
                           recordFreq.XFreqRx = curData.FreqRx;
                           recordFreq.XDeltaFreqRx = station._bwRX;
                           recordFreq.XLftRx = station._lFR;
                           recordFreq.XGmaxRx = station._gMax;
                           recordFreq.XAz1Rx = station._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gt;
                           recordFreq.XDisEmis = _curStation[indexSector].DesEmi;
                           recordFreq.XHeigthAntennaInterf = _curStation[indexSector]._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = station._aboveEarthLevel;
                           //-----
                           recordSector.Children.Add(recordFreq);
                           //Обновляем значения сектора
                           if (recordSector.Pi.Value < recordFreq.Pi.Value)
                               CopyRow(recordSector, recordFreq);
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       recordSector.Owner.Value = station.OwnerName;
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           recordInTree.Owner.Value = station.OwnerName;
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       #endregion

       #region Abonent
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Абонент (Помехи)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataInterferenceAbonent(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Помехи в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       CheckOnMaxDataForInterferance(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqRx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqRx;
                           recordFreq.FreqTx.Value = curData.FreqTx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = station._standart;
                           recordFreq.Power.Value = station._power.dBm2W();
                           recordFreq.Owner.Value = station.OwnerName;
                           recordFreq.StationName.Value = station.StationName;
                           recordFreq.Status.Value = station.Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = station._power;
                           recordFreq.XFreqTx = curData.FreqTx;
                           recordFreq.XDeltaFreqTx = station._bwTX;
                           recordFreq.XLftTx = station._lFT;
                           recordFreq.XGmaxTx = station._gMax;
                           recordFreq.XAz1Tx = station._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gt;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = _curStation[indexSector]._ktbf;
                           recordFreq.XFreqRx = curData.FreqRx;
                           recordFreq.XDeltaFreqRx = _curStation[indexSector]._bwRX;
                           recordFreq.XLftRx = _curStation[indexSector]._lFR;
                           recordFreq.XGmaxRx = curData.Gr;
                           recordFreq.XAz1Rx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gr;
                           recordFreq.XDisEmis = station.DesEmi;
                           recordFreq.XHeigthAntennaInterf = station._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = _curStation[indexSector]._aboveEarthLevel;
                           //-----
                           recordSector.Children.Add(recordFreq);
                           //Обновляем значения сектора
                           if (recordSector.Pi.Value < recordFreq.Pi.Value)
                               CopyRow(recordSector, recordFreq);
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       /// <summary>
       /// Генерирует данные для отображения в форме разультата
       /// Абонент (Жертвы)
       /// </summary>
       /// <param name="list">список станций по секторам</param>
       /// <returns>Данные для отображения в форме результата</returns>
       private List<TreeColumnRowBase> GenerateDataSacrificeAbonent(List<List<Station>> list)
       {
           //Формируем список всех выбраных станций (Жертвы в основном и соседнем канале)
           List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();
           List<int> lstStationId = GetAllStationsId(list);
           foreach (int stationId in lstStationId)
           {
               InterferenceChannelVrr recordInTree = new InterferenceChannelVrr();
               string stationName = "";
               //По секторам
               for (int indexSector = 0; indexSector < list.Count; indexSector++)
               {
                   List<Station> blockSector = list[indexSector];
                   //По станциям
                   for (int indexStation = 0; indexStation < blockSector.Count; indexStation++)
                   {
                       Station station = blockSector[indexStation];
                       if (station._tableID != stationId)
                           continue;

                       ConvertStationData(station);
                       CheckOnMaxDataForSacrifice(station);
                       stationName = station.StationName;

                       //Добавляем сектор
                       InterferenceChannelVrr recordSector = new InterferenceChannelVrr();
                       //По частотам
                       for (int indexFreq = 0; indexFreq < station.CalcDataList.Count; indexFreq++)
                       {
                           Station.CalculatedData curData = station.CalcDataList[indexFreq];

                           if (curData.Excelling < _PIMob)
                               continue;
                           //-----
                           InterferenceChannelVrr recordFreq = new InterferenceChannelVrr();
                           recordFreq.IsCheckBoxVisible = false;
                           //recordFreq.Description.Value = CLocaliz.TxT("Frequency");
                           recordFreq.Description.Value = " ";
                           recordFreq.Frequency.Value = curData.FreqRx;
                           recordFreq.Pi.Value = curData.Pi;
                           recordFreq.Irf.Value = curData.Irf;
                           recordFreq.DeltaDegradation.Value = curData.Excelling;
                           recordFreq.Distance.Value = curData.D;
                           recordFreq.FreqRx.Value = curData.FreqTx;
                           recordFreq.FreqTx.Value = curData.FreqRx;
                           recordFreq.DelraF.Value = curData.DeltaFreq;
                           recordFreq.Standard.Value = _curStation[indexSector]._standart;
                           recordFreq.Power.Value = _curStation[indexSector]._power.dBm2W();
                           recordFreq.Owner.Value = _curStation[indexSector].OwnerName;
                           recordFreq.StationName.Value = _curStation[indexSector].StationName;
                           recordFreq.Status.Value = _curStation[indexSector].Status;
                           recordFreq.StationId.Value = station._tableID;
                           recordFreq.TableName.Value = station._table;
                           //-----
                           recordFreq.XPowerTx = _curStation[indexSector]._power;
                           recordFreq.XFreqTx = curData.FreqRx;
                           recordFreq.XDeltaFreqTx = _curStation[indexSector]._bwTX;
                           recordFreq.XLftTx = _curStation[indexSector]._lFT;
                           recordFreq.XGmaxTx = _curStation[indexSector]._gMax;
                           recordFreq.XAz1Tx = _curStation[indexSector]._azimuth;
                           recordFreq.XAz2Tx = curData.A1;
                           recordFreq.XDeltaGTx = curData.Gr;
                           recordFreq.XDistance = curData.D;
                           recordFreq.XLorH = curData.L525;
                           recordFreq.XKtbfRx = station._ktbf;
                           recordFreq.XFreqRx = curData.FreqTx;
                           recordFreq.XDeltaFreqRx = station._bwRX;
                           recordFreq.XLftRx = station._lFR;
                           recordFreq.XGmaxRx = curData.Gt;
                           recordFreq.XAz1Rx = station._azimuth;
                           recordFreq.XAz2Rx = curData.A2;
                           recordFreq.XDeltaGRx = curData.Gt;
                           recordFreq.XDisEmis = _curStation[indexSector].DesEmi;
                           recordFreq.XHeigthAntennaInterf = _curStation[indexSector]._aboveEarthLevel;
                           recordFreq.XHeigthAntennaVictim = station._aboveEarthLevel;
                           //-----
                           recordSector.Children.Add(recordFreq);
                           //Обновляем значения сектора
                           if (recordSector.Pi.Value < recordFreq.Pi.Value)
                               CopyRow(recordSector, recordFreq);
                       }
                       //-----
                       recordSector.Description.Value = string.Format("{2} {0} ({1})", indexSector, recordSector.Children.Count, CLocaliz.TxT("Sector"));
                       if (recordSector.Children.Count > 0)
                       {
                           recordInTree.Children.Add(recordSector);
                           //Обновляем значения станции
                           if (recordInTree.Pi.Value < recordSector.Pi.Value)
                               CopyRow(recordInTree, recordSector);
                       }
                   }
               }
               recordInTree.Description.Value = string.Format("{2} {0} ({1})", stationName, recordInTree.Children.Count, CLocaliz.TxT("Station"));
               if (recordInTree.Children.Count > 0)
                   lstShowingData.Add(recordInTree);
           }
           return lstShowingData;
       }
       #endregion

       /// <summary>
       /// Копирует данные строки
       /// </summary>
       /// <param name="recordDest">строка получатель</param>
       /// <param name="recordSourc">строка источник</param>
       private void CopyRow(InterferenceChannelVrr recordDest, InterferenceChannelVrr recordSourc)
       {
           recordDest.StationId.Value = recordSourc.StationId.Value;
           recordDest.TableName.Value = recordSourc.TableName.Value;
           recordDest.Pi.Value = recordSourc.Pi.Value;
           recordDest.Irf.Value = recordSourc.Irf.Value;
           recordDest.DeltaDegradation.Value = recordSourc.DeltaDegradation.Value;
           recordDest.Frequency.Value = recordSourc.Frequency.Value;
           recordDest.FreqRx.Value = recordSourc.FreqRx.Value;
           recordDest.FreqTx.Value = recordSourc.FreqTx.Value;
           recordDest.DelraF.Value = recordSourc.DelraF.Value;
           recordDest.Power.Value = recordSourc.Power.Value;
           recordDest.Distance.Value = recordSourc.Distance.Value;
           //-----
           recordDest.XPowerTx = recordSourc.XPowerTx;
           recordDest.XFreqTx = recordSourc.XFreqTx;
           recordDest.XDeltaFreqTx = recordSourc.XDeltaFreqTx;
           recordDest.XLftTx = recordSourc.XLftTx;
           recordDest.XGmaxTx = recordSourc.XGmaxTx;
           recordDest.XAz1Tx = recordSourc.XAz1Tx;
           recordDest.XAz2Tx = recordSourc.XAz2Tx;
           recordDest.XDeltaGTx = recordSourc.XDeltaGTx;
           recordDest.XDistance = recordSourc.XDistance;
           recordDest.XLorH = recordSourc.XLorH;
           recordDest.XKtbfRx = recordSourc.XKtbfRx;
           recordDest.XFreqRx = recordSourc.XFreqRx;
           recordDest.XDeltaFreqRx = recordSourc.XDeltaFreqRx;
           recordDest.XLftRx = recordSourc.XLftRx;
           recordDest.XGmaxRx = recordSourc.XGmaxRx;
           recordDest.XAz1Rx = recordSourc.XAz1Rx;
           recordDest.XAz2Rx = recordSourc.XAz2Rx;
           recordDest.XDeltaGRx = recordSourc.XDeltaGRx;
           recordDest.XDisEmis = recordSourc.XDisEmis;
           recordDest.XHeigthAntennaInterf = recordSourc.XHeigthAntennaInterf;
           recordDest.XHeigthAntennaVictim = recordSourc.XHeigthAntennaVictim;
       }
       /// <summary>
       /// Копирует данные строки
       /// </summary>
       /// <param name="recordDest">строка получатель</param>
       /// <param name="recordSourc">строка источник</param>
       private void CopyRow(TreeViewIntermodulation recordDest, TreeViewIntermodulation recordSourc)
       {
           recordDest.StationId.Value = recordSourc.StationId.Value;
           recordDest.TableName.Value = recordSourc.TableName.Value;
           recordDest.Pi.Value = recordSourc.Pi.Value;
           recordDest.Irf.Value = recordSourc.Irf.Value;
           recordDest.DeltaDegradation.Value = recordSourc.DeltaDegradation.Value;
           recordDest.Frequency.Value = recordSourc.Frequency.Value;
           recordDest.FreqRx.Value = recordSourc.FreqRx.Value;
           recordDest.DeltaF.Value = recordSourc.DeltaF.Value;
           recordDest.Power.Value = recordSourc.Power.Value;
           recordDest.Distance.Value = recordSourc.Distance.Value;
           recordDest.Grx.Value = recordSourc.Grx.Value;
           recordDest.Gtx.Value = recordSourc.Gtx.Value;
           recordDest.Degradation.Value = recordSourc.Degradation.Value;
           recordDest.Status.Value = recordSourc.Status.Value;
           recordDest.Loss.Value = recordSourc.Loss.Value;
           recordDest.AzimuthTx.Value = recordSourc.AzimuthTx.Value;
           recordDest.AzimuthRx.Value = recordSourc.AzimuthRx.Value;
           recordDest.StationName.Value = recordSourc.StationName.Value;
           recordDest.Owner.Value = recordSourc.Owner.Value;
           recordDest.Standard.Value = recordSourc.Standard.Value;
       }
       //TODO после рефокторинга станции это нужно удалить
       /// <summary>
       /// Изменяет структуру хранения данных в станции
       /// </summary>
       /// <param name="st">станция</param>
       private void ConvertStationData(Station st)
       {
           st.CalcDataList.Clear();
           for (int index = 0; index < st.CURRX.Count; index++)
           {
               Station.CalculatedData newData = new Station.CalculatedData();
               newData.Irf = st.IRF[index];
               newData.A1 = st.A1[index];
               newData.A2 = st.A2[index];
               newData.B1 = st.B1[index];
               newData.B2 = st.B2[index];
               newData.Gt = st.GT[index];
               newData.Gr = st.GA[index];
               newData.D = st.DISTANCE[index];
               newData.FreqTx = st.CURTX[index];
               newData.FreqRx = st.CURRX[index];
               newData.DeltaFreq = st.DELTAFREQ[index];
               newData.L525 = st.L525orHata[index];
               newData.Pi = st.PI[index];
               newData.Excelling = st.EXCESS[index];
               st.CalcDataList.Add(newData);
           }
       }
       /// <summary>
       /// Загружает дополнительные данные о станции
       /// Ускоренная версия
       /// </summary>
       /// <param name="list">Список стаций</param>
       private void LoadAdditionalDataOfStations(params List<List<Station>>[] list)
       {
           const string progressMessage = "Loading additional data of stations";
           using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT(progressMessage)))
           {
               pb.SetBig(progressMessage);
               int curCounter = 0;
               int countSectors = 0;
               //------
               //Подсчет кол-ва записей
               {
                   foreach (List<List<Station>> listSectors in list)
                       countSectors += listSectors.Count;
               }
               Dictionary<string, Station> dictLoadedStation = new Dictionary<string, Station>();
               foreach (List<List<Station>> listSectors in list)
               {
                   foreach (List<Station> sector in listSectors)
                   {
                       int curStation = 0;
                       int allCountStation = sector.Count;
                       foreach (Station station in sector)
                       {
                           string key = string.Format("{0}_{1}", station._table, station._tableID);
                           if (dictLoadedStation.ContainsKey(key))
                           {
                               station.CopyAdditionalData(dictLoadedStation[key]);
                           }
                           else
                           {
                               station.LoadAdditionalData();
                               dictLoadedStation[key] = station;
                           }
                           pb.SetProgress(curStation++, allCountStation);
                       }
                       curCounter++;
                       pb.SetSmall(curCounter, countSectors);
                   }
               }
           }
       }

       /// <summary>
       /// Удаляем повтор. частоты для станции, оставляем частоты с максимальными значениями (Помехи)
       /// </summary>
       /// <param name="st">Станция</param>
       private void CheckOnMaxDataForInterferance(Station st)
       {
           int indexH = 0;
           while (indexH < (st.CalcDataList.Count-1))
           {
               int indexL = indexH + 1;
               while (indexL < st.CalcDataList.Count)
               {
                   if (Math.Abs(st.CalcDataList[indexH].FreqRx - st.CalcDataList[indexL].FreqRx) < 0.0001)
                   {
                       if (st.CalcDataList[indexH].Pi > st.CalcDataList[indexL].Pi)
                           st.CalcDataList.RemoveAt(indexL);
                       else
                       {
                           st.CalcDataList.RemoveAt(indexH);
                           break;
                       }
                   }
                   else
                       indexL++;
               }
               if (indexL >= st.CalcDataList.Count)
                   indexH++;
           }
       }
       /// <summary>
       /// Удаляем повтор. частоты для станции, оставляем частоты с максимальными значениями (Жертвы)
       /// </summary>
       /// <param name="st">Станция</param>
       private void CheckOnMaxDataForSacrifice(Station st)
       {
           int indexH = 0;
           while (indexH < (st.CalcDataList.Count - 1))
           {
               int indexL = indexH + 1;
               while (indexL < st.CalcDataList.Count)
               {
                   if (Math.Abs(st.CalcDataList[indexH].FreqTx - st.CalcDataList[indexL].FreqTx) < 0.0001)
                   {
                       if (st.CalcDataList[indexH].Pi > st.CalcDataList[indexL].Pi)
                           st.CalcDataList.RemoveAt(indexL);
                       else
                       {
                           st.CalcDataList.RemoveAt(indexH);
                           break;
                       }
                   }
                   else
                       indexL++;
               }
               if (indexL >= st.CalcDataList.Count)
                   indexH++;
           }
       }
       /// <summary>
       /// Добавляет новые ID станций в список всех станций
       /// </summary>
       /// <param name="list">список станций по каждому сектору</param>
       private List<int> GetAllStationsId(List<List<Station>> list)
       {
           List<int> retVal = new List<int>();
           foreach (List<Station> blockSector in list)
           {
               foreach (Station station in blockSector)
               {
                   int id = station._tableID;
                   if (retVal.Contains(id) == false)
                       retVal.Add(id);
               }
           }
           return retVal;
       }

      public void ShowResult()
      {
         //using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("FillingStations")))
         //{
         //   pBar.UserCanceled();

         //   Map.TxsOnMap.ClearAllStationList();
         //   Map.TxOnMap tx = new TxOnMap(_curStat._table, _curStat._tableID);
         //   Map.TxsOnMap.AddSpecStation(tx);

         //   List<RecordPtr> listData = new List<RecordPtr>();
         //   List<string> listCaption = new List<string>();

         //   if (_needMOB && _needAbonent)
         //   {
         //      if ((_needPRM)&&(_stListZhertvaMob.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of MOB"));
         //         listData.Add(FillBagStations(_stListZhertvaMob, Station.TypeOfStation.MOBandAbonent, TypeOfFrequency.Zhertva));
         //         listCaption.Add("Жертви по основоному і сусудньому каналу (" + _stListZhertvaMob.Count + ")");
         //      }
         //      if ((_needPRD)&&(_stListPomehaMob.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of MOB"));
         //         listData.Add(FillBagStations(_stListPomehaMob, Station.TypeOfStation.MOBandAbonent, TypeOfFrequency.Pomeha));
         //         listCaption.Add("Завади по основоному і сусудньому каналу (" + _stListPomehaMob.Count + ")");
         //      }
         //      if ((_needPRM)&&(_stListZhertvaAbonent.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Abonent"));
         //         listData.Add(FillBagStations(_stListZhertvaAbonent, Station.TypeOfStation.Abonent, TypeOfFrequency.Zhertva));
         //         listCaption.Add("Жертви по основоному і сусудньому каналу abonent (" + _stListZhertvaAbonent.Count + ")");
         //      }
         //      if ((_needPRD)&&(_stListPomehaAbonent.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Abonent"));
         //         listData.Add(FillBagStations(_stListPomehaAbonent, Station.TypeOfStation.Abonent, TypeOfFrequency.Pomeha));
         //         listCaption.Add("Завади по основоному і сусудньому каналу abonent (" + _stListPomehaAbonent.Count + ")");
         //      }
         //   }
         //   else
         //   {
         //      if (_needMOB)
         //      {
         //         if ((_needPRM)&&(_stListZhertvaMob.Count > 0))
         //         {
         //            pBar.ShowBig(CLocaliz.TxT("Filling stations of MOB"));
         //            listData.Add(FillBagStations(_stListZhertvaMob, Station.TypeOfStation.MOB, TypeOfFrequency.Zhertva));
         //            listCaption.Add("Жертви по основоному і сусудньому каналу (" + _stListZhertvaMob.Count + ")");
         //         }
         //         if ((_needPRD)&&(_stListPomehaMob.Count > 0))
         //         {
         //            pBar.ShowBig(CLocaliz.TxT("Filling stations of MOB"));
         //            listData.Add(FillBagStations(_stListPomehaMob, Station.TypeOfStation.MOB, TypeOfFrequency.Pomeha));
         //            listCaption.Add("Завади по основоному і сусудньому каналу (" + _stListPomehaMob.Count + ")");
         //         }
         //      }
         //      if (_needAbonent)
         //      {
         //         if ((_needPRM)&&(_stListZhertvaAbonent.Count > 0))
         //         {
         //            pBar.ShowBig(CLocaliz.TxT("Filling stations of Abonent"));
         //            listData.Add(FillBagStations(_stListZhertvaAbonent, Station.TypeOfStation.Abonent, TypeOfFrequency.Zhertva));
         //            listCaption.Add("Жертви по основоному і сусудньому каналу abonent (" + _stListZhertvaAbonent.Count + ")");
         //         }
         //         if ((_needPRD)&&(_stListPomehaAbonent.Count > 0))
         //         {
         //            pBar.ShowBig(CLocaliz.TxT("Filling stations of Abonent"));
         //            listData.Add(FillBagStations(_stListPomehaAbonent, Station.TypeOfStation.Abonent, TypeOfFrequency.Pomeha));
         //            listCaption.Add("Завади по основоному і сусудньому каналу abonent (" + _stListPomehaAbonent.Count + ")");
         //         }
         //      }
         //   }

         //   if (_needBlock)
         //   {
         //      if ((_needPRM)&&(_stListZhertvaBlock.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Block"));
         //         listData.Add(FillBagStations(_stListZhertvaBlock, Station.TypeOfStation.Block, TypeOfFrequency.Zhertva));
         //         listCaption.Add("Жертви блокування (" + _stListZhertvaBlock.Count + ")");
         //      }
         //      if ((_needPRD)&&(_stListPomehaBlock.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Block"));
         //         listData.Add(FillBagStations(_stListPomehaBlock, Station.TypeOfStation.Block, TypeOfFrequency.Pomeha));
         //         listCaption.Add("Завади блокування (" + _stListPomehaBlock.Count + ")");
         //      }
         //   }

         //   if (_needInter)
         //   {
         //      if ((_needPRM)&&(_stListPomehaInter.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Intermodulation"));
         //         listData.Add(FillBagStations(_stListPomehaInter, TypeOfFrequency.Pomeha));
         //         listCaption.Add("Жертви інтермодуляції (" + _stListPomehaInter.Count + ")");
         //      }
         //      if ((_needPRD)&&(_stListZhertvaInter.Count > 0))
         //      {
         //         pBar.ShowBig(CLocaliz.TxT("Filling stations of Intermodulation"));
         //         listData.Add(FillBagStations(_stListZhertvaInter, TypeOfFrequency.Zhertva));
         //         listCaption.Add("Завади інтермодуляції (" + _stListZhertvaInter.Count + ")");
         //      }
         //   }

         //   string ownerRemark = string.Empty;
         //   Station.GetOwnerData(_curStat, ref ownerRemark);

         //   if (listData.Count == 0)
         //      System.Windows.Forms.MessageBox.Show(CLocaliz.TxT("There are not stations for displaying"), CLocaliz.TxT("Info"), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

         //   IMBag.Display2(listData.ToArray(), listCaption.ToArray(), ownerRemark, null);
         //   foreach (RecordPtr h in listData)
         //   {
         //      IMBag.DeleteBag(h);
         //   }
         //   Map.TxsOnMap.ClearAllStationList();
         //}
      }

      private RecordPtr FillBagStations(List<IntermodulationInterference> stList, TypeOfFrequency typeFreq)
      {
         IMRecordset r = null;
         RecordPtr b1;

         Hashtable hash = new Hashtable();

         b1 = IMBag.CreateTemporary(ICSMTbl.itblMobStation);
         r = new IMRecordset(ICSMTbl.itblBgMobStation, IMRecordset.Mode.ReadWrite);
         r.Select("BAG_ID,OBJ_ID,IRF,P1,P2,Pi,Pi_AB,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,DELTA_THR,TX_FREQ,RX_FREQ,DELTA_FREQ_BB");
         r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
         r.Open();

         foreach (IntermodulationInterference st in stList)
         {
            //if (!st._show)
            //   continue;

            int id;
            if (typeFreq == TypeOfFrequency.Pomeha)
               id = st.Receiver.Station.GetID();
            else
               id = st.Transmitter1.Station.GetID();

            if (hash.ContainsKey(id))
               continue;
            else
               hash.Add(id, id);

            r.AddNew();
            r.Put("BAG_ID", b1.Id);

            r.Put("OBJ_ID", id);
            //if (typeFreq == TypeOfFrequency.Pomeha)
            //   r.Put("OBJ_ID", id)//st.Receiver.Station.GetID());
            //else
            //   r.Put("OBJ_ID", st.Transmitter1.Station.GetID());

            r.Put("Pi", st.SumPower);
            r.Put("IRF", st.Preselector1);
            r.Put("AZIMUTH_SACRIFICE", st.TransmitterAzimuth1);
            r.Put("AZIMUTH_NOISE", st.ReceiverAzimuth1);
            r.Put("P1", st.Power1);
            r.Put("P2", st.Power2);

            r.Put("DELTA_FREQ_BB", Math.Abs(st.Transmitter1.Frequency - st.Receiver.Frequency));

            r.Put("TX_FREQ", st.Transmitter1.Frequency);
            r.Put("RX_FREQ", st.Receiver.Frequency);

            //DONT COMMIT!
            //double delta = st.SumPower - st.Receiver.Station._sensitivity - _PIMob;
            //r.Put("DELTA_THR", delta);

            r.Update();
         }

         r.Close();
         r.Destroy();
         return b1;
      }

      private RecordPtr FillBagStations(List<Station> stList, Station.TypeOfStation typeOfStation, TypeOfFrequency typeOfFrequency)
      {
         IMRecordset r = null;
         RecordPtr b1;

         b1 = IMBag.CreateTemporary(ICSMTbl.itblMobStation);
         r = new IMRecordset(ICSMTbl.itblBgMobStation, IMRecordset.Mode.ReadWrite);
         r.Select("BAG_ID,OBJ_ID,IRF,Pi,Pi_AB,AZIMUTH_SACRIFICE,AZIMUTH_NOISE,LOSS,DELTA_THR,TX_FREQ,RX_FREQ,DELTA_FREQ_BB,DELTA_FREQ_BA,DISTANCE,THRESHOLD");
         r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
         r.Open();

         if (typeOfStation != Station.TypeOfStation.MOBandAbonent)
         {
            foreach (Station st in stList)
            {
               //if (!st._show)
               //   continue;

               r.AddNew();
               r.Put("BAG_ID", b1.Id);
               r.Put("OBJ_ID", st._tableID);
               r.Put("Pi", (st.PI[0] - 30).Round(2));
               r.Put("IRF", st.IRF[0].Round(2));
               r.Put("AZIMUTH_SACRIFICE", st.A1[0].Round(1));
               r.Put("AZIMUTH_NOISE", st.A2[0].Round(1));
               r.Put("LOSS", st.L525orHata[0].Round(2));
               r.Put("TX_FREQ", st.CURTX[0].Round(5));
               r.Put("RX_FREQ", st.CURRX[0].Round(5));
               r.Put("DELTA_THR", st.EXCESS[0].Round(2));

               r.Put("DELTA_FREQ_BB", Math.Abs(st.CURTX[0] - st.CURRX[0]).Round(5));

               r.Put("DISTANCE", st.DISTANCE[0].Round(3));

               //if (typeOfFrequency == TypeOfFrequency.Pomeha)
               //   r.Put("THRESHOLD", _curStat._sensitivity.Round(2));
               //else
               //   r.Put("THRESHOLD", st._sensitivity.Round(2));


               r.Update();
            }
         }
         else
         {
            try
            {

               foreach (Station st in stList)
               {
                  r.AddNew();
                  r.Put("BAG_ID", b1.Id);
                  r.Put("OBJ_ID", st._tableID);
                  r.Put("Pi", (st.PI[0] - 30).Round(2));

                  if (st.PI.Count > 1)
                     r.Put("Pi_AB", (st.PI[1] - 30).Round(2));

                  r.Put("IRF", st.IRF[0].Round(2));
                  r.Put("AZIMUTH_SACRIFICE", st.A1[0].Round(1));
                  r.Put("AZIMUTH_NOISE", st.A2[0].Round(1));
                  r.Put("LOSS", st.L525orHata[0].Round(2));
                  r.Put("TX_FREQ", st.CURTX[0].Round(5));
                  r.Put("RX_FREQ", st.CURRX[0].Round(5));
                  r.Put("DELTA_THR", st.EXCESS[0].Round(2));

                  r.Put("DELTA_FREQ_BB", Math.Abs(st.CURTX[0] - st.CURRX[0]).Round(5));
                  if (st.CURTX.Count > 1)
                     r.Put("DELTA_FREQ_BA", Math.Abs(st.CURTX[1] - st.CURRX[1]).Round(5));

                  r.Put("DISTANCE", st.DISTANCE[0].Round(3));

                  //if (typeOfFrequency == TypeOfFrequency.Pomeha)
                  //   r.Put("THRESHOLD", _curStat._sensitivity.Round(2));
                  //else
                  //   r.Put("THRESHOLD", st._sensitivity.Round(2));

                  r.Update();
               }
            }
            catch
            {

            }
         }

         r.Close();
         r.Destroy();
         return b1;
      }

      private void CheckOnAllowableMinAndMaxFreq(List<Station> stList)
      {
         //List<Station> dublicate = new List<Station>();
         List<double> rxFreq = new List<double>();
         List<double> txFreq = new List<double>();
         foreach (Station st in stList)
         {
            foreach (double freqRX in st.STFrequncyRX)
            {
               if (freqRX >= _freqInterMin && freqRX <= _freqInterMax)
                  rxFreq.Add(freqRX);
            }
            foreach (double freqTX in st.STFrequncyTX)
            {
               if (freqTX >= _freqInterMin && freqTX <= _freqInterMax)
                  txFreq.Add(freqTX);
            }

            st.STFrequncyRX = rxFreq;
            st.STFrequncyTX = txFreq;
         }
         //stList.Clear();
         //stList = dublicate;
      }
       /// <summary>
       /// Метод чистит станции. Те станци которые не содержат помех (согласно порогу) нафиг
       /// </summary>
       /// <param name="stList">Станции </param>
       /// <param name="PI"></param>
      private void CheckOnAllowablePI(ref List<Station> stList, double PI)
      {
         List<Station> dublicate = new List<Station>();
         if (stList.Count > 0)
         {
            foreach (Station st in stList)
            {
               if (CheckOnMaxData(st, PI))
                  dublicate.Add(st);
            }

            stList.Clear();
            stList = dublicate;
         }
      }
       /// <summary>
       /// Метод чистит станции при помехах интермодуляционных. Те станци которые не содержат помех (согласно порогу) нафиг
       /// </summary>
       /// <param name="stList">станции</param>
       /// <param name="PI"></param>
       /// <param name="typeOf"></param>
      private void CheckOnAllowablePI(ref List<IntermodulationInterference> stList, double PI,
         TypeOfFrequency typeOf)
      {
         List<IntermodulationInterference> unique = new List<IntermodulationInterference>();

         if (stList.Count > 0)
         {
            int? lastID = null;
            int lastIterator = 0;
            double maxPI = 0;
            bool firstProhod = false;

            for (int i = 0; i < stList.Count; i++)
            {
               int curId;
               if (typeOf == TypeOfFrequency.Pomeha)
                  curId = stList[i].Receiver.Station.GetID();
               else
                  curId = stList[i].Transmitter1.Station.GetID();

               if (curId == lastID)
               {
                  if (maxPI < stList[i].SumPower)
                  {
                     lastIterator = i;
                     maxPI = stList[i].SumPower;
                  }
               }
               else
               {
                  if (firstProhod)
                     unique.Add(stList[lastIterator]);
                  else
                     firstProhod = true;

                  lastIterator = i;
                  maxPI = stList[i].SumPower;
                  lastID = curId;
               }
            }

            unique.Add(stList[lastIterator]);

            stList.Clear();
            stList = unique;
         }
      }

      private bool CheckOnMaxData(Station st, double comparePI)
      {
         if (st.EXCESS.Count > 0)
             return true;
         else
            return false;
      }

      private List<Station> SeparationData(List<Station> stList1, List<Station> stList2)
      {
         List<Station> stUnique = new List<Station>(); // the list which contains unique station
         List<int> dublicateIDList = new List<int>(); // the list with dublicate id
         List<Station> tmpMin;
         List<Station> tmpMax;
         Hashtable hashList = new Hashtable();

         // checks on minimal size of list
         bool minList1 = stList1.Count < stList2.Count ? true : false;
         
         if (minList1)
         {
            tmpMin = new List<Station>(stList1);
            tmpMax = new List<Station>(stList2);
         }
         else
         {
            tmpMin = new List<Station>(stList2);
            tmpMax = new List<Station>(stList1);
         }

         foreach (Station st in tmpMax)
         {
            hashList.Add(st._tableID, st);
         }

         for (int i = 0; i < tmpMin.Count; i++)
         {
            Station st = tmpMin[i];
            if (hashList.ContainsKey(st._tableID))
            {
               Station stDublicate = (Station)hashList[st._tableID];
               stDublicate.PI.Add(st.PI[0]);
               stDublicate.CURTX.Add((st.CURTX[0]));
               stDublicate.CURRX.Add((st.CURRX[0]));
               stUnique.Add(stDublicate);
               dublicateIDList.Add(st._tableID);
            }
            else
            {
               stUnique.Add(st);
            }
         }

         foreach (int ID in dublicateIDList)
         {
            hashList.Remove(ID);
         }

         ICollection collect = hashList.Values;

         foreach (Station st in collect)
         {
            stUnique.Add(st);
         }

         return stUnique;
      }
   }
}
