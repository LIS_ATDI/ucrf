﻿namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class FormVRR
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.BtnCalculate = new System.Windows.Forms.Button();
          this.TbxRadius = new System.Windows.Forms.TextBox();
          this.TbxPI = new System.Windows.Forms.TextBox();
          this.ChbMob = new System.Windows.Forms.CheckBox();
          this.ChbAbonent = new System.Windows.Forms.CheckBox();
          this.label1 = new System.Windows.Forms.Label();
          this.label3 = new System.Windows.Forms.Label();
          this.ChbIntermodulation = new System.Windows.Forms.CheckBox();
          this.ChbBlock = new System.Windows.Forms.CheckBox();
          this.ChbOwner = new System.Windows.Forms.CheckBox();
          this.BtnCalcFreq = new System.Windows.Forms.Button();
          this.groupBox3 = new System.Windows.Forms.GroupBox();
          this.ChbR = new System.Windows.Forms.CheckBox();
          this.TbxR = new System.Windows.Forms.TextBox();
          this.TbxFreqInterMin = new System.Windows.Forms.TextBox();
          this.label16 = new System.Windows.Forms.Label();
          this.TbxFreqInterMax = new System.Windows.Forms.TextBox();
          this.TbxRadiusInter = new System.Windows.Forms.TextBox();
          this.label5 = new System.Windows.Forms.Label();
          this.TbxPIBlock = new System.Windows.Forms.TextBox();
          this.label12 = new System.Windows.Forms.Label();
          this.label13 = new System.Windows.Forms.Label();
          this.label14 = new System.Windows.Forms.Label();
          this.textBox1 = new System.Windows.Forms.TextBox();
          this.textBox2 = new System.Windows.Forms.TextBox();
          this.textBox5 = new System.Windows.Forms.TextBox();
          this.BtnClose = new System.Windows.Forms.Button();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.ChbCoordinat = new System.Windows.Forms.CheckBox();
          this.ChbPRM = new System.Windows.Forms.CheckBox();
          this.ChbPRD = new System.Windows.Forms.CheckBox();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.label9 = new System.Windows.Forms.Label();
          this.groupBox4 = new System.Windows.Forms.GroupBox();
          this.buttonDelete = new System.Windows.Forms.Button();
          this.buttonOK = new System.Windows.Forms.Button();
          this.dataGridViewPlan = new System.Windows.Forms.DataGridView();
          this.ColumnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.label8 = new System.Windows.Forms.Label();
          this.TbxPodborRadius = new System.Windows.Forms.TextBox();
          this.ChbPodborAbonent = new System.Windows.Forms.CheckBox();
          this.BtnDetailCalc = new System.Windows.Forms.Button();
          this.ChbSRT = new System.Windows.Forms.CheckBox();
          this.groupBox3.SuspendLayout();
          this.groupBox1.SuspendLayout();
          this.groupBox2.SuspendLayout();
          this.groupBox4.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlan)).BeginInit();
          this.SuspendLayout();
          // 
          // BtnCalculate
          // 
          this.BtnCalculate.Enabled = false;
          this.BtnCalculate.Location = new System.Drawing.Point(627, 384);
          this.BtnCalculate.Name = "BtnCalculate";
          this.BtnCalculate.Size = new System.Drawing.Size(137, 23);
          this.BtnCalculate.TabIndex = 3;
          this.BtnCalculate.Text = "Розрахунок ЕМС";
          this.BtnCalculate.UseVisualStyleBackColor = true;
          this.BtnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
          // 
          // TbxRadius
          // 
          this.TbxRadius.Location = new System.Drawing.Point(19, 59);
          this.TbxRadius.Name = "TbxRadius";
          this.TbxRadius.Size = new System.Drawing.Size(95, 20);
          this.TbxRadius.TabIndex = 2;
          this.TbxRadius.Text = "50";
          this.TbxRadius.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // TbxPI
          // 
          this.TbxPI.Location = new System.Drawing.Point(19, 115);
          this.TbxPI.Name = "TbxPI";
          this.TbxPI.Size = new System.Drawing.Size(95, 20);
          this.TbxPI.TabIndex = 4;
          this.TbxPI.Text = "0";
          this.TbxPI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // ChbMob
          // 
          this.ChbMob.AutoSize = true;
          this.ChbMob.Checked = true;
          this.ChbMob.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbMob.Location = new System.Drawing.Point(19, 21);
          this.ChbMob.Name = "ChbMob";
          this.ChbMob.Size = new System.Drawing.Size(159, 17);
          this.ChbMob.TabIndex = 0;
          this.ChbMob.Text = "Основний / сусідній канал";
          this.ChbMob.UseVisualStyleBackColor = true;
          // 
          // ChbAbonent
          // 
          this.ChbAbonent.AutoSize = true;
          this.ChbAbonent.Location = new System.Drawing.Point(19, 171);
          this.ChbAbonent.Name = "ChbAbonent";
          this.ChbAbonent.Size = new System.Drawing.Size(187, 17);
          this.ChbAbonent.TabIndex = 6;
          this.ChbAbonent.Text = "Враховувати завади абонентам";
          this.ChbAbonent.UseVisualStyleBackColor = true;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(122, 62);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(237, 13);
          this.label1.TabIndex = 10;
          this.label1.Text = "Максимальна відстань для пошуку завад, км";
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(122, 118);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(242, 13);
          this.label3.TabIndex = 12;
          this.label3.Text = "Граничне значення перевищення \"порогу\", дБ";
          this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // ChbIntermodulation
          // 
          this.ChbIntermodulation.AutoSize = true;
          this.ChbIntermodulation.Checked = true;
          this.ChbIntermodulation.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbIntermodulation.Location = new System.Drawing.Point(19, 48);
          this.ChbIntermodulation.Name = "ChbIntermodulation";
          this.ChbIntermodulation.Size = new System.Drawing.Size(103, 17);
          this.ChbIntermodulation.TabIndex = 1;
          this.ChbIntermodulation.Text = "Інтермодуляція";
          this.ChbIntermodulation.UseVisualStyleBackColor = true;
          // 
          // ChbBlock
          // 
          this.ChbBlock.AutoSize = true;
          this.ChbBlock.Checked = true;
          this.ChbBlock.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbBlock.Location = new System.Drawing.Point(19, 75);
          this.ChbBlock.Name = "ChbBlock";
          this.ChbBlock.Size = new System.Drawing.Size(86, 17);
          this.ChbBlock.TabIndex = 2;
          this.ChbBlock.Text = "Блокування";
          this.ChbBlock.UseVisualStyleBackColor = true;
          // 
          // ChbOwner
          // 
          this.ChbOwner.AutoSize = true;
          this.ChbOwner.Checked = true;
          this.ChbOwner.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbOwner.Location = new System.Drawing.Point(19, 102);
          this.ChbOwner.Name = "ChbOwner";
          this.ChbOwner.Size = new System.Drawing.Size(236, 17);
          this.ChbOwner.TabIndex = 3;
          this.ChbOwner.Text = "Не враховувати станції даного оператору";
          this.ChbOwner.UseVisualStyleBackColor = true;
          // 
          // BtnCalcFreq
          // 
          this.BtnCalcFreq.Location = new System.Drawing.Point(636, 174);
          this.BtnCalcFreq.Name = "BtnCalcFreq";
          this.BtnCalcFreq.Size = new System.Drawing.Size(103, 23);
          this.BtnCalcFreq.TabIndex = 6;
          this.BtnCalcFreq.Text = "Підбір частоти";
          this.BtnCalcFreq.UseVisualStyleBackColor = true;
          this.BtnCalcFreq.Click += new System.EventHandler(this.BtnCalcFreq_Click);
          // 
          // groupBox3
          // 
          this.groupBox3.Controls.Add(this.ChbMob);
          this.groupBox3.Controls.Add(this.ChbIntermodulation);
          this.groupBox3.Controls.Add(this.ChbBlock);
          this.groupBox3.Controls.Add(this.ChbOwner);
          this.groupBox3.Location = new System.Drawing.Point(12, 12);
          this.groupBox3.Name = "groupBox3";
          this.groupBox3.Size = new System.Drawing.Size(333, 135);
          this.groupBox3.TabIndex = 0;
          this.groupBox3.TabStop = false;
          this.groupBox3.Text = "Тип завади";
          // 
          // ChbR
          // 
          this.ChbR.AutoSize = true;
          this.ChbR.Location = new System.Drawing.Point(413, 171);
          this.ChbR.Name = "ChbR";
          this.ChbR.Size = new System.Drawing.Size(185, 17);
          this.ChbR.TabIndex = 7;
          this.ChbR.Text = "Зона обслуговування абонента";
          this.ChbR.UseVisualStyleBackColor = true;
          this.ChbR.CheckedChanged += new System.EventHandler(this.ChbR_CheckedChanged);
          // 
          // TbxR
          // 
          this.TbxR.Enabled = false;
          this.TbxR.Location = new System.Drawing.Point(657, 169);
          this.TbxR.Name = "TbxR";
          this.TbxR.Size = new System.Drawing.Size(95, 20);
          this.TbxR.TabIndex = 8;
          this.TbxR.Text = "0";
          this.TbxR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // TbxFreqInterMin
          // 
          this.TbxFreqInterMin.Location = new System.Drawing.Point(19, 31);
          this.TbxFreqInterMin.Name = "TbxFreqInterMin";
          this.TbxFreqInterMin.Size = new System.Drawing.Size(95, 20);
          this.TbxFreqInterMin.TabIndex = 0;
          this.TbxFreqInterMin.Text = "860";
          this.TbxFreqInterMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // label16
          // 
          this.label16.AutoSize = true;
          this.label16.Location = new System.Drawing.Point(122, 90);
          this.label16.Name = "label16";
          this.label16.Size = new System.Drawing.Size(333, 13);
          this.label16.TabIndex = 11;
          this.label16.Text = "Максимальна відстань для пошуку інтермодуляційних завад, км";
          // 
          // TbxFreqInterMax
          // 
          this.TbxFreqInterMax.Location = new System.Drawing.Point(125, 31);
          this.TbxFreqInterMax.Name = "TbxFreqInterMax";
          this.TbxFreqInterMax.Size = new System.Drawing.Size(95, 20);
          this.TbxFreqInterMax.TabIndex = 1;
          this.TbxFreqInterMax.Text = "920";
          this.TbxFreqInterMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // TbxRadiusInter
          // 
          this.TbxRadiusInter.Location = new System.Drawing.Point(19, 87);
          this.TbxRadiusInter.Name = "TbxRadiusInter";
          this.TbxRadiusInter.Size = new System.Drawing.Size(95, 20);
          this.TbxRadiusInter.TabIndex = 3;
          this.TbxRadiusInter.Text = "5";
          this.TbxRadiusInter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(122, 146);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(482, 13);
          this.label5.TabIndex = 13;
          this.label5.Text = "Допустиме значення перевищення потужності завади блокування над чутливістю станці" +
              "ї;, дБ";
          this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // TbxPIBlock
          // 
          this.TbxPIBlock.Location = new System.Drawing.Point(19, 143);
          this.TbxPIBlock.Name = "TbxPIBlock";
          this.TbxPIBlock.Size = new System.Drawing.Size(95, 20);
          this.TbxPIBlock.TabIndex = 5;
          this.TbxPIBlock.Text = "60";
          this.TbxPIBlock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // label12
          // 
          this.label12.AutoSize = true;
          this.label12.Location = new System.Drawing.Point(9, 34);
          this.label12.Name = "label12";
          this.label12.Size = new System.Drawing.Size(275, 17);
          this.label12.TabIndex = 7;
          this.label12.Text = "Діапазон частот для пошуку завад, МГц";
          // 
          // label13
          // 
          this.label13.Location = new System.Drawing.Point(9, 65);
          this.label13.Name = "label13";
          this.label13.Size = new System.Drawing.Size(236, 36);
          this.label13.TabIndex = 6;
          this.label13.Text = "Максимальна відстань для пошуку завад, км";
          // 
          // label14
          // 
          this.label14.Location = new System.Drawing.Point(9, 101);
          this.label14.Name = "label14";
          this.label14.Size = new System.Drawing.Size(275, 53);
          this.label14.TabIndex = 8;
          this.label14.Text = "Граничне значення перевищення \"порогу\", дБ";
          this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // textBox1
          // 
          this.textBox1.Location = new System.Drawing.Point(321, 31);
          this.textBox1.Name = "textBox1";
          this.textBox1.Size = new System.Drawing.Size(95, 20);
          this.textBox1.TabIndex = 2;
          // 
          // textBox2
          // 
          this.textBox2.Location = new System.Drawing.Point(321, 73);
          this.textBox2.Name = "textBox2";
          this.textBox2.Size = new System.Drawing.Size(95, 20);
          this.textBox2.TabIndex = 1;
          // 
          // textBox5
          // 
          this.textBox5.Location = new System.Drawing.Point(321, 116);
          this.textBox5.Name = "textBox5";
          this.textBox5.Size = new System.Drawing.Size(95, 20);
          this.textBox5.TabIndex = 3;
          // 
          // BtnClose
          // 
          this.BtnClose.Location = new System.Drawing.Point(676, 654);
          this.BtnClose.Name = "BtnClose";
          this.BtnClose.Size = new System.Drawing.Size(75, 23);
          this.BtnClose.TabIndex = 6;
          this.BtnClose.Text = "Вийти";
          this.BtnClose.UseVisualStyleBackColor = true;
          this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.ChbSRT);
          this.groupBox1.Controls.Add(this.ChbCoordinat);
          this.groupBox1.Controls.Add(this.ChbPRM);
          this.groupBox1.Controls.Add(this.ChbPRD);
          this.groupBox1.Location = new System.Drawing.Point(351, 12);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(419, 135);
          this.groupBox1.TabIndex = 1;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Враховувати";
          // 
          // ChbCoordinat
          // 
          this.ChbCoordinat.AutoSize = true;
          this.ChbCoordinat.Location = new System.Drawing.Point(20, 75);
          this.ChbCoordinat.Name = "ChbCoordinat";
          this.ChbCoordinat.Size = new System.Drawing.Size(163, 17);
          this.ChbCoordinat.TabIndex = 2;
          this.ChbCoordinat.Text = "Станції, що координуються";
          this.ChbCoordinat.UseVisualStyleBackColor = true;
          // 
          // ChbPRM
          // 
          this.ChbPRM.AutoSize = true;
          this.ChbPRM.Checked = true;
          this.ChbPRM.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbPRM.Location = new System.Drawing.Point(20, 21);
          this.ChbPRM.Name = "ChbPRM";
          this.ChbPRM.Size = new System.Drawing.Size(50, 17);
          this.ChbPRM.TabIndex = 0;
          this.ChbPRM.Text = "ПРМ";
          this.ChbPRM.UseVisualStyleBackColor = true;
          // 
          // ChbPRD
          // 
          this.ChbPRD.AutoSize = true;
          this.ChbPRD.Checked = true;
          this.ChbPRD.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbPRD.Location = new System.Drawing.Point(20, 48);
          this.ChbPRD.Name = "ChbPRD";
          this.ChbPRD.Size = new System.Drawing.Size(50, 17);
          this.ChbPRD.TabIndex = 1;
          this.ChbPRD.Text = "ПРД";
          this.ChbPRD.UseVisualStyleBackColor = true;
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.ChbR);
          this.groupBox2.Controls.Add(this.label5);
          this.groupBox2.Controls.Add(this.TbxR);
          this.groupBox2.Controls.Add(this.TbxFreqInterMin);
          this.groupBox2.Controls.Add(this.ChbAbonent);
          this.groupBox2.Controls.Add(this.TbxPIBlock);
          this.groupBox2.Controls.Add(this.label9);
          this.groupBox2.Controls.Add(this.TbxFreqInterMax);
          this.groupBox2.Controls.Add(this.label3);
          this.groupBox2.Controls.Add(this.label1);
          this.groupBox2.Controls.Add(this.TbxRadius);
          this.groupBox2.Controls.Add(this.TbxPI);
          this.groupBox2.Controls.Add(this.label16);
          this.groupBox2.Controls.Add(this.TbxRadiusInter);
          this.groupBox2.Location = new System.Drawing.Point(12, 153);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Size = new System.Drawing.Size(758, 215);
          this.groupBox2.TabIndex = 2;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "Завдання параметрів розрахунків";
          // 
          // label9
          // 
          this.label9.AutoSize = true;
          this.label9.Location = new System.Drawing.Point(226, 34);
          this.label9.Name = "label9";
          this.label9.Size = new System.Drawing.Size(210, 13);
          this.label9.TabIndex = 9;
          this.label9.Text = "Діапазон частот для пошуку завад, МГц";
          // 
          // groupBox4
          // 
          this.groupBox4.Controls.Add(this.buttonDelete);
          this.groupBox4.Controls.Add(this.buttonOK);
          this.groupBox4.Controls.Add(this.dataGridViewPlan);
          this.groupBox4.Controls.Add(this.label8);
          this.groupBox4.Controls.Add(this.TbxPodborRadius);
          this.groupBox4.Controls.Add(this.BtnCalcFreq);
          this.groupBox4.Controls.Add(this.ChbPodborAbonent);
          this.groupBox4.Location = new System.Drawing.Point(12, 413);
          this.groupBox4.Name = "groupBox4";
          this.groupBox4.Size = new System.Drawing.Size(759, 219);
          this.groupBox4.TabIndex = 4;
          this.groupBox4.TabStop = false;
          this.groupBox4.Text = "Завдання параметрів підбіру частоти";
          // 
          // buttonDelete
          // 
          this.buttonDelete.Location = new System.Drawing.Point(636, 114);
          this.buttonDelete.Name = "buttonDelete";
          this.buttonDelete.Size = new System.Drawing.Size(103, 23);
          this.buttonDelete.TabIndex = 5;
          this.buttonDelete.Text = "Delete plan";
          this.buttonDelete.UseVisualStyleBackColor = true;
          this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
          // 
          // buttonOK
          // 
          this.buttonOK.Location = new System.Drawing.Point(636, 72);
          this.buttonOK.Name = "buttonOK";
          this.buttonOK.Size = new System.Drawing.Size(103, 23);
          this.buttonOK.TabIndex = 4;
          this.buttonOK.Text = "Select plan";
          this.buttonOK.UseVisualStyleBackColor = true;
          this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
          // 
          // dataGridViewPlan
          // 
          this.dataGridViewPlan.AllowUserToAddRows = false;
          this.dataGridViewPlan.AllowUserToDeleteRows = false;
          this.dataGridViewPlan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
          this.dataGridViewPlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridViewPlan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnID,
            this.ColumnName});
          this.dataGridViewPlan.Location = new System.Drawing.Point(13, 59);
          this.dataGridViewPlan.MultiSelect = false;
          this.dataGridViewPlan.Name = "dataGridViewPlan";
          this.dataGridViewPlan.ReadOnly = true;
          this.dataGridViewPlan.RowHeadersWidth = 4;
          this.dataGridViewPlan.Size = new System.Drawing.Size(602, 138);
          this.dataGridViewPlan.TabIndex = 3;
          // 
          // ColumnID
          // 
          this.ColumnID.HeaderText = "ID";
          this.ColumnID.Name = "ColumnID";
          this.ColumnID.ReadOnly = true;
          this.ColumnID.Visible = false;
          // 
          // ColumnName
          // 
          this.ColumnName.HeaderText = "Name";
          this.ColumnName.Name = "ColumnName";
          this.ColumnName.ReadOnly = true;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(10, 31);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(245, 13);
          this.label8.TabIndex = 0;
          this.label8.Text = "Максимальна відстань врахування станцій, км";
          // 
          // TbxPodborRadius
          // 
          this.TbxPodborRadius.Location = new System.Drawing.Point(328, 28);
          this.TbxPodborRadius.Name = "TbxPodborRadius";
          this.TbxPodborRadius.Size = new System.Drawing.Size(95, 20);
          this.TbxPodborRadius.TabIndex = 1;
          this.TbxPodborRadius.Text = "180";
          // 
          // ChbPodborAbonent
          // 
          this.ChbPodborAbonent.AutoSize = true;
          this.ChbPodborAbonent.Location = new System.Drawing.Point(550, 27);
          this.ChbPodborAbonent.Name = "ChbPodborAbonent";
          this.ChbPodborAbonent.Size = new System.Drawing.Size(137, 17);
          this.ChbPodborAbonent.TabIndex = 2;
          this.ChbPodborAbonent.Text = "Врахування абонентів";
          this.ChbPodborAbonent.UseVisualStyleBackColor = true;
          // 
          // BtnDetailCalc
          // 
          this.BtnDetailCalc.Location = new System.Drawing.Point(31, 654);
          this.BtnDetailCalc.Name = "BtnDetailCalc";
          this.BtnDetailCalc.Size = new System.Drawing.Size(159, 23);
          this.BtnDetailCalc.TabIndex = 5;
          this.BtnDetailCalc.Text = "Detail Calculation";
          this.BtnDetailCalc.UseVisualStyleBackColor = true;
          this.BtnDetailCalc.Click += new System.EventHandler(this.BtnDetailCalc_Click);
          // 
          // ChbSRT
          // 
          this.ChbSRT.AutoSize = true;
          this.ChbSRT.Location = new System.Drawing.Point(20, 102);
          this.ChbSRT.Name = "ChbSRT";
          this.ChbSRT.Size = new System.Drawing.Size(240, 17);
          this.ChbSRT.TabIndex = 3;
          this.ChbSRT.Text = "Передавачі СРТ в діапазоні 1980-2000МГц";
          this.ChbSRT.UseVisualStyleBackColor = true;
          this.ChbSRT.Visible = false;
          // 
          // FormVRR
          // 
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.ClientSize = new System.Drawing.Size(783, 699);
          this.Controls.Add(this.BtnDetailCalc);
          this.Controls.Add(this.groupBox4);
          this.Controls.Add(this.groupBox2);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.BtnClose);
          this.Controls.Add(this.groupBox3);
          this.Controls.Add(this.BtnCalculate);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.Name = "FormVRR";
          this.Text = "Попередній розрахунок ЕМС";
          this.Load += new System.EventHandler(this.FormVRR_Load);
          this.groupBox3.ResumeLayout(false);
          this.groupBox3.PerformLayout();
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          this.groupBox4.ResumeLayout(false);
          this.groupBox4.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlan)).EndInit();
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button BtnCalculate;
      private System.Windows.Forms.TextBox TbxRadius;
      private System.Windows.Forms.TextBox TbxPI;
      private System.Windows.Forms.CheckBox ChbMob;
      private System.Windows.Forms.CheckBox ChbAbonent;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.CheckBox ChbIntermodulation;
      private System.Windows.Forms.CheckBox ChbBlock;
      private System.Windows.Forms.CheckBox ChbOwner;
      private System.Windows.Forms.Button BtnCalcFreq;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.TextBox TbxFreqInterMax;
      private System.Windows.Forms.TextBox TbxRadiusInter;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.TextBox textBox2;
      private System.Windows.Forms.TextBox textBox5;
      private System.Windows.Forms.Button BtnClose;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox TbxPIBlock;
      private System.Windows.Forms.TextBox TbxFreqInterMin;
      private System.Windows.Forms.CheckBox ChbR;
      private System.Windows.Forms.TextBox TbxR;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.CheckBox ChbCoordinat;
      private System.Windows.Forms.CheckBox ChbPRM;
      private System.Windows.Forms.CheckBox ChbPRD;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.CheckBox ChbPodborAbonent;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox TbxPodborRadius;
      private System.Windows.Forms.DataGridView dataGridViewPlan;
      private System.Windows.Forms.Button buttonDelete;
      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
      private System.Windows.Forms.Button BtnDetailCalc;
      private System.Windows.Forms.CheckBox ChbSRT;
   }
}