﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using ComponentsLib;
using ICSM;
using XICSM.UcrfRfaNET.Calculation;
using XICSM.UcrfRfaNET.ApplSource;
using System.Collections;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    //Форма отображения результатов расчета ЭМС в ВРР
    public partial class FrmCalculationResultVrr : Form
    {
        public enumTypeCalc TypeResCalc { get; set; }
        public Dictionary<int, double> _ListRowCalc { get; set; }
        private TabsTreeColumn _tabsTree;
        private UcGroupLabel _ucRx;
        private UcGroupLabel _ucTx;
        private UcGroupLabel _ucAntennaRx;
        private UcGroupLabel _ucAntennaTx;
        public TabsTreeColumn TabsTree { get { return _tabsTree; } }
        public string TableNameOfCurStation { get; set; }
        public List<int> IdsOfCurStation { get; set; }
        #region Binding
        public string LblOwner { get; set; }
        public string LblAddress { get; set; }
        public string LblFreq { get; set; }
        public string LblTypeStation { get; set; }
        public string LblClass { get; set; }
        public string LblStationName { get; set; }
        public double LblPower { get; set; }
        public double LblSencetivity { get; set; }
        public double LblG { get; set; }
        public double LblH { get; set; }
        public string LblAzimuth { get; set; }
        public string LblDistanceBord { get; set; }
        public string LblDiapazon { get; set; }
        public double LblRadius { get; set; }
        public double LblRadInterm { get; set; }
        public double LblPorog { get; set; }
        #endregion
        public string StationName { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public FrmCalculationResultVrr()
        {
            InitializeComponent();
            _ListRowCalc = new Dictionary<int, double>();
            //----------
            TableNameOfCurStation = "";
            IdsOfCurStation = new List<int>();
            //----------
            _tabsTree = new TabsTreeColumn();
            pnlTabTreeResult.Controls.Add(TabsTree);
            TabsTree.Dock = DockStyle.Fill;
            TabsTree.BackColor = Color.White;
            //----------
            _ucRx = new UcGroupLabel();
            _ucRx.Name = "_ucRx";
            pnlRx.Controls.Add(_ucRx);
            _ucRx.Dock = DockStyle.Fill;
            //----------
            _ucTx = new UcGroupLabel();
            _ucTx.Name = "_ucTx";
            pnlTx.Controls.Add(_ucTx);
            _ucTx.Dock = DockStyle.Fill;
            //----------
            _ucAntennaRx = new UcGroupLabel();
            pnlAntennaRx.Controls.Add(_ucAntennaRx);
            _ucAntennaRx.Dock = DockStyle.Fill;
            //----------
            _ucAntennaTx = new UcGroupLabel();
            pnlAntennaTx.Controls.Add(_ucAntennaTx);
            _ucAntennaTx.Dock = DockStyle.Fill;
            //---
            _ucRx.Lbl1 = "ПРМ";
            _ucTx.Lbl1 = "ПРД";
            _ucAntennaRx.Lbl1 = "Антенна ПРМ";
            _ucAntennaTx.Lbl1 = "Антенна ПРД";
            //---
            dgView.RowCount = 2;
            dgView[0, 0].Value = "Параметр";
            dgView[0, 1].Value = "Значення";
            //---
            lblRed.Left = pnlTx.Left - 2;
            lblRed.Top = pnlTx.Top - 2;
            lblRed.Width = pnlTx.Width + 4;
            lblRed.Height = pnlTx.Height + 4;
            //--------
            lblGreen.Left = pnlRx.Left - 2;
            lblGreen.Top = pnlRx.Top - 2;
            lblGreen.Width = pnlRx.Width + 4;
            lblGreen.Height = pnlRx.Height + 4;
            //--------
            UpdateAboutStation(null);
            ResizedWindow();
            CLocaliz.TxT(this);
        }
        /// <summary>
        /// Изменилася выбраная станция
        /// </summary>
        public void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
                return;
            BaseRowCalculationVrr newRow = e.NewValue as BaseRowCalculationVrr;
            if (newRow != null)
            {
                // формируем данные
                UpdateAboutStation(newRow);
            }
        }
        /// <summary>
        /// Обновление детальных параметров станции
        /// </summary>
        /// <param name="row">данные о станции</param>
        private void UpdateAboutStation(BaseRowCalculationVrr row)
        {
            //----
            for (int i = 1; i < dgView.ColumnCount; i++)
            {
                dgView[i, 0].Value = "";
                dgView[i, 1].Value = "";
            }
            if ((row == null) || (row.IsShowed == false))
            {
                _ucRx.Lbl2 = "";
                _ucRx.Lbl3 = "";
                _ucRx.Lbl4 = "";
                //----
                _ucTx.Lbl2 = "";
                _ucTx.Lbl3 = "";
                _ucTx.Lbl4 = "";
                //----
                _ucAntennaRx.Lbl2 = "";
                _ucAntennaRx.Lbl3 = "";
                _ucAntennaRx.Lbl4 = "";
                _ucAntennaRx.Lbl5 = "";
                //----
                _ucAntennaTx.Lbl2 = "";
                _ucAntennaTx.Lbl3 = "";
                _ucAntennaTx.Lbl4 = "";
                _ucAntennaTx.Lbl5 = "";
                //----
                lblLftTx.Text = "";
                lblLftRx.Text = "";
                //----
                lblDistance.Text = "";
                lblLorH.Text = "";
            }
            else
            {
                _ucTx.Lbl2 = string.Format("P: {0}, дБВт", row.XPowerTx.Round(1) - 30.0);
                _ucTx.Lbl3 = string.Format("fпрд: {0}, МГц", row.XFreqTx);
                _ucTx.Lbl4 = string.Format("df: {0}, кГц", row.XDeltaFreqTx * 1000.0);
                //-----
                lblLftTx.Text = string.Format("АФТ L: {0}, дБ", row.XLftTx.Round(1));
                //----
                _ucAntennaTx.Lbl2 = string.Format("Gmax: {0}, дБи", row.XGmaxTx.Round(1));
                _ucAntennaTx.Lbl3 = string.Format("A: {0}, град", row.XAz1Tx.Round(0));
                _ucAntennaTx.Lbl4 = string.Format("Aж: {0}, град", row.XAz2Tx.Round(0));
                _ucAntennaTx.Lbl5 = string.Format("G(d): {0}, дБи", row.XDeltaGTx.Round(1));
                //----
                lblDistance.Text = string.Format("R: {0}, Км", row.XDistance.Round(3));
                lblLorH.Text = string.Format("L: {0}, дБ", row.XLorH.Round(1));
                //----
                _ucAntennaRx.Lbl2 = string.Format("Gmax: {0}, дБи", row.XGmaxRx.Round(1));
                _ucAntennaRx.Lbl3 = string.Format("A: {0}, град", row.XAz1Rx.Round(0));
                _ucAntennaRx.Lbl4 = string.Format("Aз: {0}, град", row.XAz2Rx.Round(0));
                _ucAntennaRx.Lbl5 = string.Format("G(d): {0}, дБи", row.XDeltaGRx.Round(1));
                //-----
                lblLftRx.Text = string.Format("АФТ L: {0}, дБ", row.XLftRx.Round(1));
                //----
                _ucRx.Lbl2 = string.Format("KTBF: {0}, дБм", row.XKtbfRx.Round(1));
                _ucRx.Lbl3 = string.Format("fпрм: {0}, МГц", row.XFreqRx);
                _ucRx.Lbl4 = string.Format("df: {0}, кГц", row.XDeltaFreqRx * 1000.0);
                //----
                dgView[1, 0].Value = "Клас випромінювання";
                dgView[1, 1].Value = row.XDisEmis;
                dgView[2, 0].Value = "Висота станції завади";
                dgView[2, 1].Value = row.XHeigthAntennaInterf.Round(0);
                dgView[3, 0].Value = "Висота станції жертви";
                dgView[3, 1].Value = row.XHeigthAntennaVictim.Round(0);
            }
        }
        /// <summary>
        /// Печать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            string fileName = HelpClasses.PluginSetting.PluginFolderSetting.FolderCalculatedEmc +
                              string.Format("\\{0}_{1}.csv", StationName, IdsOfCurStation[0]);
            PrintToFile(fileName);
        }

        /// <summary>
        /// Изменился размер окна
        /// </summary>
        private void FrmCalculationResultVrr_ResizeEnd(object sender, EventArgs e)
        {
            ResizedWindow();
        }
        /// <summary>
        /// Изменился размер окна
        /// </summary>
        private void FrmCalculationResultVrr_Resize(object sender, EventArgs e)
        {
            ResizedWindow();
        }
        /// <summary>
        /// Изменился размер окна
        /// </summary>
        private void ResizedWindow()
        {
            int left = ((this.Width - pnldataAboutCurStation.Width) / 2) - 3;
            pnldataAboutCurStation.Left = left;
            pnlUpdatedData.Left = left;
            btnOnmap.Left = ((this.Width - btnOnmap.Width) / 2) - 3;
            button1.Left = ((this.Width - button1.Width) / 4) - 3;
        }
        /// <summary>
        /// Отображаем окно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCalculationResultVrr_Load(object sender, EventArgs e)
        {
            lblOwner.Text = string.Format("Власник: {0}", LblOwner);
            lblAddress.Text = string.Format("Адреса: {0}", LblAddress);
            lblFreq.Text = string.Format("Частоти, МГц/канали: {0}", LblFreq);
            lblTypeStation.Text = string.Format("Радіотехнологія: {0}", LblTypeStation);
            lblClass.Text = string.Format("Клас: {0}", LblClass);
            lblStationName.Text = string.Format("Озн. станції: {0}", LblStationName);
            lblPower.Text = string.Format("Рпд, Вт: {0}", LblPower.dBm2W().Round(4));
            lblSencitivity.Text = string.Format("Чутлив., дБм: {0}", LblSencetivity.Round(5));
            lblG.Text = string.Format("G, дБи: {0}", LblG.Round(5));
            lblH.Text = string.Format("Hант, м: {0}", LblH.Round(5));
            lblAzimuth.Text = string.Format("Азимути, град: {0}", LblAzimuth);
            lblDistanceBord.Text = string.Format("Відст. до корд.: {0}", LblDistanceBord);
            lblDiapazon.Text = string.Format("Діапазон, МГц: {0}", LblDiapazon);
            lblR.Text = string.Format("Радіус, км: {0}", LblRadius.Round(3));
            lblRadIntermod.Text = string.Format("Радіус для інтермод., км: {0}", LblRadInterm.Round(3));
            lblPorog.Text = string.Format("Доп. перев. порогу, дБ: {0}", LblPorog.Round(5));
            //------
            lblNointerference.Enabled = (TabsTree.ListOfItemSources.Count == 0);
            lblNointerference.Visible = lblNointerference.Enabled;
        }
        /// <summary>
        /// Двойное нажатие на станции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="row"></param>
        public void MouseDoubleClickRow(object sender, System.Windows.Input.MouseButtonEventArgs e, TreeColumnRowBase row)
        {
            this.Enabled = false;
            BaseRowCalculationVrr tmpRow = row as BaseRowCalculationVrr;
            if (tmpRow == null)
                return;
            if ((string.IsNullOrEmpty(tmpRow.TableName.Value) == true) ||
                (tmpRow.StationId.Value == 0) ||
                (tmpRow.StationId.Value == IM.NullI))
                return;
            RecordPtr recPtr = new RecordPtr(tmpRow.TableName.Value, tmpRow.StationId.Value);
            recPtr.UserEdit();
            this.Enabled = true;
        }

        private Dictionary<string, DozvilExtended> GenerateConclusion()
        {


            Dictionary<string, DozvilExtended> Dict = new Dictionary<string, DozvilExtended>();
            List<BaseRowCalculationVrr> lstRow = new List<BaseRowCalculationVrr>();
            int indexTab = TabsTree.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TabsTree.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                lstRow.AddRange(DistinctById(LoadSelectedRows(curItemSource)));

            }
            if (lstRow.Count > 0)
            {

                foreach (BaseRowCalculationVrr id in lstRow)
                {




                    IMRecordset rs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE,DOZV_NUM,DOZV_NUM_NEW, MobileSector1.Position.PROVINCE,BaseSector1.Position.PROVINCE");
                    rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, id.TableName.Value.ToString());
                    rs.SetAdditional(string.Format("[OBJ_ID1]={0} OR [OBJ_ID2]={0} OR [OBJ_ID3]={0}  OR [OBJ_ID4]={0}   OR [OBJ_ID5]={0}  OR [OBJ_ID6]={0}", int.Parse(id.StationId.Value.ToString())));
                    rs.Open();




                    /*
                                    IMRecordset rs = new IMRecordset("XV_PERM_ALL", IMRecordset.Mode.ReadOnly);
                                    rs.Select("APPL_ID,PERM_NUM,FILE_PATH,TABLE_ID,TABLE_NAME,Application.PayOwner.PROVINCE,Application.MobileSector1.Position.PROVINCE,Application.BaseSector1.Position.PROVINCE");
                                    rs.SetWhere("TABLE_ID", IMRecordset.Operation.Eq, int.Parse(id.StationId.Value.ToString()));
                                    rs.SetAdditional(string.Format("[TABLE_NAME]='{0}'",id.TableName.Value.ToString()));
                                    rs.Open();
                     */



                    if (!rs.IsEOF())
                    {



                        string NumDzv = "";

                        //if (!string.IsNullOrEmpty(rs.GetS("PERM_NUM")))
                        if (!string.IsNullOrEmpty(rs.GetS("DOZV_NUM")))
                        {
                            NumDzv = rs.GetS("DOZV_NUM");
                        }
                        else if (string.IsNullOrEmpty(NumDzv))
                        {
                            NumDzv = rs.GetS("DOZV_NUM_NEW");
                        }

                        string OutStr = "";
                        string CurrFieldProvince = "";
                        switch (id.TableName.Value.ToString())
                        {
                            case "MOB_STATION":
                                //CurrFieldProvince = rs.GetS("Application.MobileSector1.Position.PROVINCE");
                                CurrFieldProvince = rs.GetS("MobileSector1.Position.PROVINCE");
                                break;
                            case "MOB_STATION2":
                                //CurrFieldProvince = rs.GetS("Application.BaseSector1.Position.PROVINCE");
                                CurrFieldProvince = rs.GetS("BaseSector1.Position.PROVINCE");
                                break;

                        }

                        switch (CurrFieldProvince)
                        {

                            case "АР Крим":
                                OutStr = "Кримська філія";
                                break;

                            case "Вінницька":
                            case "Житомирська":
                            case "Хмельницька":
                                OutStr = "Подільська філія";
                                break;


                            case "Закарпатська":
                            case "Івано-Франківська":
                            case "Чернівецька":
                                OutStr = "Карпатська філія";
                                break;


                            case "Київ":
                            case "Київська":
                                OutStr = "ДРЗП УДЦР";
                                break;

                            case "Черкаська":
                            case "Кіровоградська":
                            case "Дніпропетровська":
                            case "Запорізька":
                            case "Донецька":
                                OutStr = "Центральна філія";
                                break;




                            case "Миколаївська":
                            case "Одеська":
                            case "Херсонська":
                                OutStr = "Південна філія";
                                break;


                            case "Рівненська":
                            case "Тернопільська":
                            case "Львівська":
                            case "Волинська":
                                OutStr = "Західна філія";
                                break;

                            case "Сумська":
                            case "Чернігівська":
                            case "Полтавська":
                            case "Харківська":
                            case "Луганська":
                                OutStr = "Північно-Східна філія";
                                break;

                        }



                        //////////////////////
                        DozvilExtended extend = new DozvilExtended();
                        IMRecordset rs_users = new IMRecordset(id.TableName.Value.ToString(), IMRecordset.Mode.ReadOnly);
                        rs_users.Select("ID,OWNER_ID,STANDARD,Owner.NAME");
                        rs_users.SetWhere("ID", IMRecordset.Operation.Eq, int.Parse(id.StationId.Value.ToString()));
                        rs_users.Open();

                        if (!rs_users.IsEOF())
                        {
                            extend.ID_Owner = rs_users.GetI("OWNER_ID");
                            extend.NameOwner = rs_users.GetS("Owner.NAME");


                            if (extend.ID_Owner > 0)
                            {
                                IMRecordset rs_user_name = new IMRecordset("USERS", IMRecordset.Mode.ReadOnly);
                                rs_user_name.Select("ID,NAME");
                                rs_user_name.SetWhere("ID", IMRecordset.Operation.Eq, extend.ID_Owner);
                                rs_user_name.Open();

                                if (!rs_user_name.IsEOF())
                                {
                                    extend.NameOwner = rs_user_name.GetS("NAME");
                                }
                            }




                            //extend.RadioTech = rs_users.GetS("STANDARD");
                            extend.RadioTech = LblTypeStation;
                            extend.NameFilial = OutStr;
                        }

                        /////////////////////

                        if (!Dict.ContainsKey(NumDzv))
                        {
                            if (!string.IsNullOrEmpty(NumDzv))
                                Dict.Add(NumDzv, extend);
                        }

                    }



                    if (rs.IsOpen() == true)
                        rs.Close();
                    rs.Destroy();

                }


            }
            return Dict;
        }

        /// <summary>
        /// Отправить выбранные станции в Телеком
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToIcaTelecom_Click(object sender, EventArgs e)
        {
            List<BaseRowCalculationVrr> lstRow = new List<BaseRowCalculationVrr>();
            int indexTab = TabsTree.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TabsTree.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                lstRow.AddRange(DistinctById(LoadSelectedRows(curItemSource)));
            }
            if (lstRow.Count > 0)
            {
                List<string> Lst_Tables = new List<string>();
                foreach (BaseRowCalculationVrr row in lstRow)
                {
                    if (!Lst_Tables.Contains(row.TableName.Value))
                        Lst_Tables.Add(row.TableName.Value);
                }
                List<RecordPtr> lstBag = new List<RecordPtr>();
                List<string> lstCaption = new List<string>();


                RecordPtr bag;
                foreach (string val_tbl in Lst_Tables)
                {
                    bag = IMBag.CreateTemporary(val_tbl);
                    lstBag.Add(bag);
                    lstCaption.Add(CLocaliz.TxT(TabsTree.tabControl.TabPages[indexTab].Text));
                    foreach (BaseRowCalculationVrr row in lstRow)
                    {
                        if (row.TableName.Value == val_tbl)
                        {
                            IMRecordset r = new IMRecordset("BG_" + val_tbl, IMRecordset.Mode.ReadWrite);
                            r.Select("BAG_ID,OBJ_ID");
                            r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                            r.Open();
                            r.AddNew();
                            r.Put("BAG_ID", bag.Id);
                            r.Put("OBJ_ID", row.StationId.Value);
                            r.Update();
                            if (r.IsOpen() == true)
                                r.Close();
                            r.Destroy();
                        }
                    }
                }

                
                //------------------
                //Добавление текущей станции в Bag
                bag = IMBag.CreateTemporary(TableNameOfCurStation);
                lstBag.Add(bag);
                lstCaption.Add(CLocaliz.TxT("Current station"));
                foreach (int id in IdsOfCurStation)
                {
                    IMRecordset r = new IMRecordset("BG_" + TableNameOfCurStation, IMRecordset.Mode.ReadWrite);
                    r.Select("BAG_ID,OBJ_ID");
                    r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                    r.Open();
                    r.AddNew();
                    r.Put("BAG_ID", bag.Id);
                    r.Put("OBJ_ID", id);
                    r.Update();
                    if (r.IsOpen() == true)
                        r.Close();
                    r.Destroy();
                }
                IMBag.Display2(lstBag.ToArray(), lstCaption.ToArray(), null, null);
                foreach (RecordPtr bagItem in lstBag)
                    IMBag.DeleteBag(bagItem);
            }
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");
        }
        /// <summary>
        /// Отобразитьь выделенные станции на карте
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOnmap_Click(object sender, EventArgs e)
        {
            
            Map.TxsOnMap.ClearAllStationList();
            Map.TxsOnMap.InitMap();

            if (IdsOfCurStation.Count > 0)
            {

                
                List<BaseRowCalculationVrr> lstRow_IDS = new List<BaseRowCalculationVrr>();
                int indexTab_IDS = TabsTree.tabControl.SelectedIndex;
                if ((indexTab_IDS > -1) && (indexTab_IDS < TabsTree.ListOfItemSources.Count))
                {
                    IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab_IDS];
                    lstRow_IDS.AddRange(LoadSelectedRows(curItemSource));
                }



                List<int> ValContains = new List<int>();
                foreach (BaseRowCalculationVrr bs in lstRow_IDS)
                {
                    foreach (KeyValuePair<int, double> item in _ListRowCalc)
                    {
                        if ((TypeResCalc == enumTypeCalc.Interference) || (TypeResCalc == enumTypeCalc.Victim))
                        {
                            if (bs.XAz1Rx.Round(0) == item.Value.Round(0))
                            {
                                if (!ValContains.Contains(item.Key))
                                    ValContains.Add(item.Key);
                            }
                        }
                        else if (TypeResCalc == enumTypeCalc.Sacrifice)  
                        {
                            if (bs.XAz1Tx.Round(0) == item.Value.Round(0))
                            {
                                if (!ValContains.Contains(item.Key))
                                    ValContains.Add(item.Key);
                            }
                        }
                    }
                }

               foreach (int item in ValContains)
               {
                
                    Map.TxOnMap curStn = new Map.TxOnMap(TableNameOfCurStation,item, Color.Green);
                    curStn.width = 10;
                    curStn.IsCalculation = true;
                    Map.TxsOnMap.AddTx(curStn);
                  
                }
            }
            else
            {
                Map.TxOnMap curStn = new Map.TxOnMap(TableNameOfCurStation, IM.NullI, Color.Green);
                curStn.width = 10;
                curStn.IsCalculation = true;
                Map.TxsOnMap.AddTx(curStn);
            }

            
            bool isAddedStation = false;
            List<BaseRowCalculationVrr> lstRow = new List<BaseRowCalculationVrr>();
            int indexTab = TabsTree.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TabsTree.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                lstRow.AddRange(DistinctById(LoadSelectedRows(curItemSource)));
            }
            if (lstRow.Count > 0)
            {
                foreach (BaseRowCalculationVrr row in lstRow)
                {
                    Map.TxOnMap tx = new Map.TxOnMap(row.TableName.Value, row.StationId.Value);
                    tx.IsCalculation = true;
                    Map.TxsOnMap.InitMap();
                    Map.TxsOnMap.AddTx(tx);
                    isAddedStation = true;
                }
            }
            if (isAddedStation == true)
                Map.TxsOnMap.ShowMap();
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");
        }
        /// <summary>
        /// Рекурсивная загрузка отмеченных строк дерева
        /// </summary>
        /// <param name="curItemSource">Список строк</param>
        /// <returns>Список выделенныз строк</returns>
        private List<BaseRowCalculationVrr> LoadSelectedRows(IEnumerable curItemSource)
        {
            List<BaseRowCalculationVrr> retVal = new List<BaseRowCalculationVrr>();
            foreach (object source in curItemSource)
            {
                BaseRowCalculationVrr tmpSource = source as BaseRowCalculationVrr;
                if (tmpSource == null)
                    continue;
                //Обработка текущего уровня
                if (tmpSource.IsChecked == true)
                    retVal.Add(tmpSource);
                //Обрабатываем подуровни
                if (tmpSource.Children.Count > 0)
                    retVal.AddRange(LoadSelectedRows(tmpSource.Children));
            }
            return retVal;
        }
        /// <summary>
        /// Одаляет из списка строки с однинаковыми ID станции
        /// </summary>
        /// <param name="rows">Список строк</param>
        /// <returns>Строка без дубликатов ID</returns>
        private List<BaseRowCalculationVrr> DistinctById(List<BaseRowCalculationVrr> rows)
        {
            List<BaseRowCalculationVrr> retVal = new List<BaseRowCalculationVrr>();
            HashSet<int> idHash = new HashSet<int>();
            foreach (BaseRowCalculationVrr row in rows)
            {
                int id = row.StationId.Value;
                if (idHash.Contains(id))
                    continue;
                idHash.Add(id);
                retVal.Add(row);
            }
            return retVal;
        }
        /// <summary>
        /// Печатает в файл строки
        /// </summary>
        /// <param name="curItemSource">список строк</param>
        /// <param name="sw">Поток, куда писать</param>
        /// <param name="orderProperties">Список колонок</param>
        private void SaveColumnToFile(IEnumerable curItemSource, StreamWriter sw, PropertyDescription[] orderProperties)
        {
            foreach (object source in curItemSource)
            {
                string outLine = "";
                // Description
                System.Reflection.PropertyInfo curProperty = source.GetType().GetProperty("Description");
                if (curProperty != null)
                {
                    object objProperty = curProperty.GetValue(source, null);
                    if (objProperty != null)
                        outLine += string.Format("{0};", objProperty);
                }
                // Все остальные
                foreach (PropertyDescription property in orderProperties)
                {
                    if (property.NamePropertyShort == "Description")
                        continue;
                    curProperty = source.GetType().GetProperty(property.NamePropertyShort);
                    if(curProperty == null)
                        continue;
                    object objProperty = curProperty.GetValue(source, null);
                    if(objProperty == null)
                        continue;
                    outLine += string.Format("{0};", objProperty);
                }
                sw.WriteLine(outLine);
                TreeColumnRowBase row = source as TreeColumnRowBase;
                if (row != null)
                    SaveColumnToFile(row.Children, sw, orderProperties);
            }
        }
        /// <summary>
        /// Создает файл с результатами расчетов
        /// </summary>
        /// <param name="fileName"></param>
        private void PrintToFile(string fileName)
        {
            try
            {
                // Формируем файл
                StreamWriter sw = new StreamWriter(fileName, false, System.Text.Encoding.Default);
                // Шапка
                sw.WriteLine("Параметри станції, що аналізуються:");
                sw.WriteLine();
                sw.WriteLine(string.Format("Власник:;{0}", LblOwner));
                sw.WriteLine(string.Format("Адреса:;{0}", LblAddress));
                sw.WriteLine(string.Format("Частоти, МГц / канали:;{0}", LblFreq));
                sw.WriteLine(string.Format("Радіотехнологія:;{0}", LblTypeStation));
                sw.WriteLine(string.Format("Клас випромінювання:;{0}", LblClass));
                sw.WriteLine(string.Format("Рпд, Вт:;{0}", LblPower.dBm2W().Round(2)));
                sw.WriteLine(string.Format("Чутливість, дБм:;{0}", LblSencetivity));
                sw.WriteLine(string.Format("G, дБи:;{0}", LblG));
                sw.WriteLine(string.Format("Hант, м:;{0}", LblH));
                sw.WriteLine(string.Format("Азимути, град:;{0}", LblAzimuth));
                sw.WriteLine(string.Format("Відст. до корд.: {0}", LblDistanceBord));
                sw.WriteLine();
                sw.WriteLine();
                sw.WriteLine("Параметри розрахунку");
                sw.WriteLine(string.Format("Діапазон, МГц:;{0}", LblDiapazon));
                sw.WriteLine(string.Format("Радіус, км:;{0}", LblRadius));
                sw.WriteLine(string.Format("Радіус для інтермодуляції, км:;{0}", LblRadInterm));
                sw.WriteLine(string.Format("Доп. перевищення порогу, дБ:;{0}", LblPorog));
                sw.WriteLine();
                sw.WriteLine();

                if (TabsTree.tabControl.TabPages.Count == 0)
                {
                    sw.WriteLine("Завади відсутні");
                    sw.WriteLine();
                    sw.WriteLine();
                }
                //Вкладки
                for (int indexTab = 0; indexTab < TabsTree.tabControl.TabPages.Count; indexTab++)
                {
                    TabPage page = TabsTree.tabControl.TabPages[indexTab];
                    //Проверки
                    if (page.Controls.Count == 0)
                        continue;
                    if ((indexTab < 0) || (indexTab >= TabsTree.ListOfItemSources.Count))
                        continue;
                    Calculation.ResultForm.TreeColumnWinForms curTab =
                        page.Controls[0] as Calculation.ResultForm.TreeColumnWinForms;
                    if (curTab == null)
                        continue;
                    // Вывод
                    string outLine = "";
                    sw.WriteLine(page.Text);
                    foreach (string header in curTab.TreeColumn.GetHeaderNames())
                        outLine += string.Format("{0};", header);
                    sw.WriteLine(outLine);
                    //---
                    IEnumerable curItemSource = TabsTree.ListOfItemSources[indexTab];
                    SaveColumnToFile(curItemSource, sw, curTab.TreeColumn.GetSelectedProperties());
                    sw.WriteLine();
                    sw.WriteLine();
                }
                sw.WriteLine("Виконавець:;{0};", CUsers.GetUserFio());
                //-------
                sw.Flush(); // очистка буфера
                sw.Close();
                //-------
                //Создаем собития в заявках
                RecordPtr rec = new RecordPtr(TableNameOfCurStation, IdsOfCurStation[0]);
                PrintDocs print = new PrintDocs();
                print.LoadDocLink(Documents.DocType.CALCUL_EMC, "", fileName, DateTime.Now, IM.NullT,
                                  rec.Applications(),true);
                if (System.Windows.Forms.MessageBox.Show(string.Format(CLocaliz.TxT("File '{0}' was created. Do you want to open it?"), fileName), CLocaliz.TxT("Message"),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(fileName);
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, CLocaliz.TxT("Error"));
            }
        }

        /// <summary>
        /// Метод, выполняющий отправку сформированных данных в раздел "Висновок" формы MainForm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string TemplateDozvAdditional = "";
                BaseAppClass obj = BaseAppClass.GetAppl(MainAppForm.StatNumApplIdOpened, true);
                if (obj!=null)
                {
                    Dictionary<string, DozvilExtended> Dic = GenerateConclusion();
                    string OutStr = ""; string OutFieldOwner=""; string OutFieldRadioTech="";
                    foreach (KeyValuePair<string, DozvilExtended> item in Dic)
                    {
                        OutStr = OutStr + "№" + item.Key + ", ";
                        if (!OutFieldOwner.Contains(item.Value.NameOwner)) OutFieldOwner = OutFieldOwner + item.Value.NameOwner + ", ";
                        if (!OutFieldRadioTech.Contains(item.Value.RadioTech)) OutFieldRadioTech = OutFieldRadioTech + item.Value.RadioTech + ", ";
                    }
                    if (!string.IsNullOrEmpty(OutStr)) OutStr = OutStr.Remove(OutStr.Length - 2, 2);
                    if (!string.IsNullOrEmpty(OutFieldOwner)) OutFieldOwner = OutFieldOwner.Remove(OutFieldOwner.Length - 2, 2);
                    if (!string.IsNullOrEmpty(OutFieldRadioTech)) OutFieldRadioTech = OutFieldRadioTech.Remove(OutFieldRadioTech.Length - 2, 2);


                    string OutStrProv = "";
                    List<string> strprov = new List<string>();
                    foreach (KeyValuePair<string, DozvilExtended> item in Dic)
                    {
                        if (!strprov.Contains(item.Value.NameFilial))
                            strprov.Add(item.Value.NameFilial);
                    }
                    foreach (string item in strprov)
                    {
                        OutStrProv = OutStrProv + item + ", ";
                    }
                    if (!string.IsNullOrEmpty(OutStrProv)) OutStrProv = OutStrProv.Remove(OutStrProv.Length - 2, 2);
                    if (!string.IsNullOrEmpty(OutStr))
                    {
                        TemplateDozvAdditional = string.Format("З метою експериментального визначення забезпечення необхідної якості зв’язку в мережі Заявника і умов виконання електромагнітної сумісності (ЕМС) з РЕЗ {2} (дозвіл {0}) у запланованій зоні використання визначена необхідність проведення тестових випробувань (ТВ) заявленого РЕЗ {3}. ТВ організуються Заявником згідно \"Порядку проведення приймальних випробувань РЕЗ (ВП) на місці експлуатації\". Графік тестових включень для проведення ТВ погоджує {1}. Тривалість ТВ становить 2 (два) місяці у межах терміну дії цього висновку. Результат ТВ зазначається у протоколі, один примірник якого надається УДЦР, разом із заявою про видачу дозволу на експлуатацію. Рішення про видачу дозволу на експлуатацію приймається УДЦР з урахуванням результатів ТВ.", OutStr, OutStrProv, OutFieldOwner, OutFieldRadioTech);
                        CallBackFunction.callbackEventHandler(obj.SCVisnovok = obj.SCVisnovok + (obj.SCVisnovok.Length > 0 ? Environment.NewLine : "") + TemplateDozvAdditional);
                        System.Windows.Forms.MessageBox.Show(CLocaliz.TxT("The data sent to the section 'Conclusion'"));
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(CLocaliz.TxT("The data is not sent to the section of the 'Conclusion'." + Environment.NewLine + CLocaliz.TxT("Probably no data in the fields 'DOZV_NUM' or 'DOZV_NUM_NEW' table 'XNRFA_APPL'.")));
                    }
                }
        }
    }

    public enum enumTypeCalc
    {
        Sacrifice,
        Interference,
        Victim
    }


    public sealed class DozvilExtended
    {
        public int ID_Owner { get; set; }
        public string NameOwner { get; set; }
        public string RadioTech { get; set; }
        public string NameFilial { get; set; }

    }
    
}
