﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   public partial class SelectSectorForm : Form
   {
      private static int selIndex = 0;

      public static int GetSelIndex()
      {
         return selIndex;
      }
      
      List<int> SectorIdList = new List<int>();

      public SelectSectorForm()
      {
          InitializeComponent();
      }

      public SelectSectorForm(bool showAllSectorBtn): this()
      {
         btnAllSectors.Enabled = showAllSectorBtn;
         btnAllSectors.Visible = btnAllSectors.Enabled;
      }

      public void InitList()
      {
         SectorListBox.Items.Clear();
         SectorIdList.Clear();
      }

      public void AddSector(int SectorID)
      {
         SectorIdList.Add(SectorID);
         SectorListBox.Items.Add(string.Format("{0}-й сектор {1}", SectorIdList.Count, SectorID));
      }

      public int GetSelectedIndex()
      {
         int si = SectorListBox.SelectedIndex;
         selIndex = SectorListBox.SelectedIndex;
         if (si>=0)
         {
            return SectorIdList[si];
         } else {
            return -1;
         }
      }

      private void SelectSectorForm_Shown(object sender, EventArgs e)
      {
         int ind = GetSelIndex();
         if(ind > SectorListBox.Items.Count - 1)
            ind = 0;
         SectorListBox.SelectedIndex = ind;
      }
   }
}
