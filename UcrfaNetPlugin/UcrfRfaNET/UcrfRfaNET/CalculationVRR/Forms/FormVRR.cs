﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Globalization;
using ICSM;
using XICSM.Intermodulation;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   public partial class FormVRR : FBaseForm
   {

      #region MEMBERS

       /// <summary>
       /// Список станций, которые алализируются
       /// </summary>
       private List<Station> _listStation = new List<Station>();
       /// <summary>
       /// Название таблицы
       /// </summary>
       private string _tableName = string.Empty;
       /// <summary>
       /// Текущая локаль
       /// </summary>
       private NumberFormatInfo _provider;

       //private int _tableID;
      //private Station _currStat;
      
      List<int> FreqPlanListId = new List<int>();

      #endregion

      /// <summary>
      /// Конструктор
      /// </summary>
      protected FormVRR()
      {
          InitializeComponent();
          CLocaliz.TxT(this);

          for (int i = 0; i < dataGridViewPlan.Columns.Count; i++)
          {
              string tmpLocal = dataGridViewPlan.Columns[i].HeaderText;
              dataGridViewPlan.Columns[i].HeaderText = CLocaliz.TxT(tmpLocal);
          }
          _provider = new NumberFormatInfo();
      }
       /// <summary>
       /// Загружает список станций из заявки
       /// </summary>
       /// <param name="applId">ID заявки</param>
       private void LoadStationFromAppl(int applId)
       {
           IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
           rsAppl.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
           rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
           try
           {
               rsAppl.Open();
               _tableName = rsAppl.GetS("OBJ_TABLE");
               for (int i = 1; i < 7; i++)
               {
                   int tmpId = rsAppl.GetI("OBJ_ID" + i);
                   if (tmpId != IM.NullI)
                   {
                       Station tmpStation = new Station(_tableName, tmpId);
                       _listStation.Add(tmpStation);
                   }
               }
           }
           finally
           {
               if (rsAppl.IsOpen())
                   rsAppl.Close();
               rsAppl.Destroy();
           }
       }
       /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="tableName">Название таблицы</param>
      /// <param name="stationId">ID станции</param>
      public FormVRR(string tableName, int stationId)
           : this()
      {
          _tableName = tableName;
          int applId = ApplSource.BaseAppClass.GetApplId(stationId, tableName);
          if(applId == IM.NullI)
          {
              Station tmpStation = new Station(_tableName, stationId);
              _listStation.Add(tmpStation);
          }
          else
              LoadStationFromAppl(applId);
      }

      #region METHODS
      /// <summary>
      /// Заполняем форму данными
      /// </summary>
      private void FillsData()
      {
          if ((_listStation.Count == 0) || (string.IsNullOrEmpty(_tableName)))
          {
              MessageBox.Show("Can't find the station");
              return;
          }
          //Загружаем данные по станции
          Station currStat = _listStation[0];

          if (currStat.STFrequncyRX.Count > 0 ||
              currStat.STFrequncyTX.Count > 0)
          {
              BtnCalculate.Enabled = true;
          }

          switch (currStat._standart)
          {
              case "CDMA-450":
              case "CDMA 450":
              case "CDMA450":
                  TbxFreqInterMin.Text = "440";
                  TbxFreqInterMax.Text = "470";
                  TbxRadius.Text = "50";
                  TbxRadiusInter.Text = "5";
                  break;

              case "CDMA-800":
              case "CDMA 800":
              case "CDMA800":
                  TbxFreqInterMin.Text = "869";
                  TbxFreqInterMax.Text = "915";
                  TbxRadius.Text = "10";
                  TbxRadiusInter.Text = "2";
                  ChbPRD.Checked = false;
                  ChbPRM.Checked = true;
                  break;

              case "GSM-900":
              case "GSM 900":
              case "GSM900":
                  TbxFreqInterMin.Text = "869";
                  TbxFreqInterMax.Text = "888";
                  TbxRadius.Text = "10";
                  TbxRadiusInter.Text = "2";
                  ChbPRD.Checked = true;
                  ChbPRM.Checked = false;
                  break;

              case "GSM-1800":
              case "GSM 1800":
              case "GSM1800":
                  TbxFreqInterMin.Text = "1700";
                  TbxFreqInterMax.Text = "1900";
                  TbxRadius.Text = "5";
                  TbxRadiusInter.Text = "1";
                  ChbPRD.Checked = true;
                  ChbPRM.Checked = false;
                  break;

              case "DECT":
                  TbxFreqInterMin.Text = "1870";
                  TbxFreqInterMax.Text = "1900";
                  TbxRadius.Text = "1";
                  TbxRadiusInter.Text = "1";
                  ChbPRD.Checked = false;
                  ChbPRM.Checked = true;
                  break;

              case "IMT-2000":
              case "IMT 2000":
              case "IMT2000":
              case "UMTS":
                  TbxFreqInterMin.Text = "1880";
                  TbxFreqInterMax.Text = "2200";
                  TbxRadius.Text = "1";
                  TbxRadiusInter.Text = "1";
                  ChbPRD.Checked = true;
                  ChbPRM.Checked = false;
                  if ((currStat.STFrequncyRX[0]<1980)&&(currStat.STFrequncyRX[0]>1920)) //пока чтоб не усложнять работа только на одну частоту
                  {
                      ChbSRT.Visible = true;
                      ChbSRT.Checked = true;
                      TbxRadius.Text = "3";
                      TbxPIBlock.Text = "70";
                  }
                  break;

              default:
                  double minFreq = 0, maxFreq = 0;
                  if (currStat.STFrequncyRX.Count > 0)
                      currStat.STFrequncyRX.Sort();
                  if (currStat.STFrequncyTX.Count > 0)
                      currStat.STFrequncyTX.Sort();

                  if ((currStat.STFrequncyTX.Count > 0) && (currStat.STFrequncyRX.Count > 0))
                  {
                      double bw = currStat._bwTX;
                      if (bw < 0.0125)
                          bw = 0.0125;
                      if((0.0125 < bw) && (bw < 0.025))
                          bw = 0.025;
                      minFreq = Math.Min(currStat.STFrequncyRX[0], currStat.STFrequncyTX[0]) - bw * 1.5 + 0.0001;
                      maxFreq = Math.Max(currStat.STFrequncyRX[currStat.STFrequncyRX.Count - 1],
                                         currStat.STFrequncyTX[currStat.STFrequncyTX.Count - 1]) + bw * 1.5 - 0.0001;
                  }

                  TbxFreqInterMin.Text = minFreq.Round(5).ToString();
                  TbxFreqInterMax.Text = maxFreq.Round(5).ToString();
                  TbxRadius.Text = "150";
                  TbxRadiusInter.Text = "5";
                  ChbPRD.Checked = true;
                  ChbPRM.Checked = true;
                  break;
          }
      }

       #endregion

      #region EVENTS

      /// <summary>
      /// Загрузка формы
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void FormVRR_Load(object sender, EventArgs e)
      {
          FillsData();
      }
       /// <summary>
       /// Подсчет частот
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
      private void BtnCalcFreq_Click(object sender, EventArgs e)
       {
           double radius = TbxPodborRadius.Text.ToDouble(IM.NullD);
           if ((radius != IM.NullD) && (_listStation.Count > 0))
           {
               Station currStat = _listStation[0];
               List<string[]> listRows = new List<string[]>();

               foreach (int freqPlanId in FreqPlanListId)
               {
                   //------------------------
                   // Вытаскиваем данные
                   IMRecordset rsFreqPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                   rsFreqPlan.Select("NAME");
                   rsFreqPlan.SetWhere("ID", IMRecordset.Operation.Eq, freqPlanId);
                   try
                   {
                       rsFreqPlan.Open();
                       if (!rsFreqPlan.IsEOF())
                       {
                           string nameFreqPlan = rsFreqPlan.GetS("NAME");
                           double freqMin = IM.NullD;
                           double freqMax = IM.NullD;
                           double duplex = IM.NullD;

                           //------------------------
                           //Частотные каналы
                           IMRecordset rsFreqChan = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
                           rsFreqChan.Select("PARITY,CHANNEL,FREQ");
                           rsFreqChan.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, freqPlanId);
                           rsFreqChan.OrderBy("FREQ", OrderDirection.Ascending);
                           try
                           {
                               rsFreqChan.Open();
                               if (!rsFreqChan.IsEOF())
                               {
                                   duplex = 0.0;

                                   if (rsFreqChan.GetS("PARITY") == "N")
                                   {
                                       freqMin = rsFreqChan.GetD("FREQ");
                                       freqMax = rsFreqChan.GetD("FREQ");

                                       for (; !rsFreqChan.IsEOF(); rsFreqChan.MoveNext())
                                       {
                                           if (freqMin > rsFreqChan.GetD("FREQ"))
                                               freqMin = rsFreqChan.GetD("FREQ");
                                           if (freqMax < rsFreqChan.GetD("FREQ"))
                                               freqMax = rsFreqChan.GetD("FREQ");
                                       }
                                   }
                                   else
                                   {
                                       freqMin = 10000000;
                                       freqMax = 0;

                                       bool isFindDuplex = false;
                                       string numberChannel = rsFreqChan.GetS("CHANNEL");
                                       double tmpDuplex = rsFreqChan.GetD("FREQ");
                                       rsFreqChan.MoveNext();

                                       for (; !rsFreqChan.IsEOF(); rsFreqChan.MoveNext())
                                       {
                                           if ((isFindDuplex == false) && (numberChannel == rsFreqChan.GetS("CHANNEL")))
                                           {
                                               isFindDuplex = true;
                                               if ((rsFreqChan.GetS("PARITY") == "D") &&
                                                   (rsFreqChan.GetS("PARITY") == "L"))
                                                   duplex = tmpDuplex - rsFreqChan.GetD("FREQ");
                                               else duplex = rsFreqChan.GetD("FREQ") - tmpDuplex;
                                           }
                                           if (freqMin > rsFreqChan.GetD("FREQ") &&
                                               ((rsFreqChan.GetS("PARITY") == "U") || (rsFreqChan.GetS("PARITY") == "H")))
                                               freqMin = rsFreqChan.GetD("FREQ");

                                           if (freqMax < rsFreqChan.GetD("FREQ") &&
                                               ((rsFreqChan.GetS("PARITY") == "U") || (rsFreqChan.GetS("PARITY") == "H")))
                                               freqMax = rsFreqChan.GetD("FREQ");
                                       }
                                   }
                               }
                           }
                           finally
                           {
                               rsFreqChan.Close();
                               rsFreqChan.Destroy();
                           }

                           //----------------------
                           // Проверка параметров
                           if ((freqMax == IM.NullD) || (freqMin == IM.NullD) || (duplex == IM.NullD))
                               MessageBox.Show("Частотний план \"" + nameFreqPlan + "\" не повний",
                                               "Вибір параметрів чатот.плана");
                           else
                           {
                               // Все ОК
                               //устанавливаем частоты если их нет
                               if (currStat.STFrequncyRX.Count == 0)
                               {
                                   if (currStat.STFrequncyTX.Count > 0)
                                       currStat.STFrequncyRX.AddRange(currStat.STFrequncyTX);
                                   else
                                       currStat.STFrequncyRX.Add(freqMin);
                               }
                               if (currStat.STFrequncyTX.Count == 0)
                               {
                                   if (currStat.STFrequncyRX.Count > 0)
                                       currStat.STFrequncyTX.AddRange(currStat.STFrequncyRX);
                                   else
                                       currStat.STFrequncyTX.Add(freqMin);
                               }
                               ViborkaVRR viborka = new ViborkaVRR(currStat, freqMin, freqMax, IM.NullD, duplex, radius,
                                                                   ChbOwner.Checked, ChbPodborAbonent.Checked);
                               listRows.AddRange(viborka.Calculate(freqPlanId, nameFreqPlan));
                           }
                       }
                   }
                   finally
                   {
                       rsFreqPlan.Close();
                       rsFreqPlan.Destroy();
                   }
               }

               if (listRows.Count > 0)
               {
                   FShowFreqAssign frm = new FShowFreqAssign(listRows, currStat);
                   frm.ListIdFreqPlan.AddRange(FreqPlanListId);
                   frm.RadiusSelectFreq = radius;
                   frm.ShowDialog();
                   frm.Dispose();
               }
               else
                   MessageBox.Show("В частотних планах відсутні дозволені частоти");
           }
           else
           {
               MessageBox.Show("Не всы параметри введені!");
           }
       }
       /// <summary>
       /// Расчитать ЕМС
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
       private void BtnCalculate_Click(object sender, EventArgs e)
      {
           CalculateEms();
      }
       /// <summary>
       /// Функция подсчета ЕМС
       /// </summary>
       private void CalculateEms()
       {

           double radiusMob = 0, radiusInter = 0, radiusBlock = 0,
              freqInterMax = 0, freqInterMin = 0, PIMob = 0, PIBlock = 0,
              PIInter = 0, G = 0;
           double? R = null;

           if (ChbMob.Checked)
           {
               if (TbxRadius.TextLength > 0 && TbxFreqInterMax.TextLength > 0 &&
                  TbxPI.TextLength > 0 && TbxFreqInterMin.TextLength > 0)
               {
                   radiusMob = ConvertType.ToDouble(TbxRadius.Text, 0);
                   freqInterMax = ConvertType.ToDouble(TbxFreqInterMax.Text, 0);
                   freqInterMin = ConvertType.ToDouble(TbxFreqInterMin.Text, 0);
                   PIMob = ConvertType.ToDouble(TbxPI.Text, 0);
               }
               else
               {
                   MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                   return;
               }
           }

           if (ChbIntermodulation.Checked)
           {
               if (TbxFreqInterMax.TextLength > 0 && TbxRadiusInter.TextLength > 0 &&
                  TbxPI.TextLength > 0 && TbxFreqInterMin.TextLength > 0 &&
                  TbxPI.TextLength > 0)
               {
                   radiusInter = ConvertType.ToDouble(TbxRadiusInter.Text, 0);
                   freqInterMax = ConvertType.ToDouble(TbxFreqInterMax.Text, 0);
                   freqInterMin = ConvertType.ToDouble(TbxFreqInterMin.Text, 0);
                   PIInter = 23;//ConvertType.ToDouble(TbxPI.Text, 0);
                   G = 19;//ConvertType.ToDouble(TbxG.Text, 0);
                   PIMob = ConvertType.ToDouble(TbxPI.Text, 0);
               }
               else
               {
                   MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                   return;
               }
           }

           if (ChbBlock.Checked)
           {
               if (TbxFreqInterMax.TextLength > 0 && TbxFreqInterMin.TextLength > 0 &&
                  TbxRadiusInter.TextLength > 0 && TbxPIBlock.TextLength > 0)
               {
                   radiusBlock = ConvertType.ToDouble(TbxRadiusInter.Text, 0);
                   freqInterMax = ConvertType.ToDouble(TbxFreqInterMax.Text, 0);
                   freqInterMin = ConvertType.ToDouble(TbxFreqInterMin.Text, 0);
                   PIBlock = ConvertType.ToDouble(TbxPIBlock.Text, 0);
               }
               else
               {
                   MessageBox.Show("Please filled all empty fields", "Please filled all empty fields",
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                   return;
               }
           }

           if (!ChbBlock.Checked && !ChbIntermodulation.Checked && !ChbMob.Checked &&
              !ChbAbonent.Checked)
           {
               return;
           }

           //try
           //{

           if (ChbR.Checked)
           {
               R = ConvertType.ToDouble(TbxR.Text, 0);
           }


           ViborkaVRR viborka = new ViborkaVRR(_listStation, radiusMob, radiusInter,
              radiusBlock, freqInterMax, freqInterMin, PIMob, PIInter, PIBlock, G,
              R, ChbMob.Checked, ChbAbonent.Checked, ChbIntermodulation.Checked,
              ChbBlock.Checked, ChbPRD.Checked, ChbPRM.Checked, ChbOwner.Checked, ChbSRT.Checked);
           viborka.Calculate(0, "");
           viborka.ShowResultForm();
       }

       private void Tbx_KeyPressOnDigit(object sender, KeyPressEventArgs e)
      {
         // Если это не цифра.
         if ((!Char.IsDigit(e.KeyChar)) && (e.KeyChar != 45) && (e.KeyChar != '\b'))
         {
            // Запрет на ввод более одной десятичной точки.
            TextBox tb = (TextBox)sender;
            char separator = _provider.CurrencyGroupSeparator[0];
            if (e.KeyChar != separator || tb.Text.IndexOf(separator) != -1)
            {
               e.Handled = true;
            }
         }
      }

      private void BtnClose_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      private void ChbR_CheckedChanged(object sender, EventArgs e)
      {
         if (ChbR.Checked)
         {
            TbxR.Enabled = true;
         }
         else
         {
            TbxR.Enabled = false;
         }
      }

      private void BtnDetailCalc_Click(object sender, EventArgs e)
      {
         double radiusInter = 0, freqInterMax = 0, freqInterMin = 0, PIMob = 0, PIInter = 0, G = 0;

         if (TbxFreqInterMax.TextLength > 0 && TbxRadiusInter.TextLength > 0 &&
               TbxPI.TextLength > 0 && TbxFreqInterMin.TextLength > 0 &&
               TbxPI.TextLength > 0 && _listStation.Count > 0)
         {
            radiusInter = ConvertType.ToDouble(TbxRadiusInter.Text, 0);
            freqInterMax = ConvertType.ToDouble(TbxFreqInterMax.Text, 0);
            freqInterMin = ConvertType.ToDouble(TbxFreqInterMin.Text, 0);
            PIInter = 23;
            G = 19;
            PIMob = ConvertType.ToDouble(TbxPI.Text, 0);

             Station currStat = _listStation[0];
            IntermodulationMainForm inter = new IntermodulationMainForm(ICSMTbl.itblMobStation,
               currStat._tableID, G, PIInter, freqInterMin, freqInterMax, -1000, currStat._sensitivity
               + PIMob, radiusInter, 130);
            inter.ShowDialog();
            inter.Dispose();
         }
      }

      #endregion

      //=========================================
      /// <summary>
      /// Добавить частотный план
      /// </summary>
      private void buttonOK_Click(object sender, EventArgs e)
      {
         // Выбераем запись из таблицы
         RecordPtr RecFreqPlan = ICSM.RecordPtr.UserSearch(CLocaliz.TxT("Seaching plan"), ICSMTbl.itblFreqPlan, "");
         if ((RecFreqPlan.Id > 0) && (RecFreqPlan.Id < IM.NullI))
         {
            FreqPlanListId.Add(RecFreqPlan.Id);
            UpdateFreqPlan();
         }
      }
      //=========================================
      /// <summary>
      /// Удалить частотный план
      /// </summary>
      private void buttonDelete_Click(object sender, EventArgs e)
      {
         try
         {
            int ID = Convert.ToInt32(dataGridViewPlan.Rows[dataGridViewPlan.CurrentCell.RowIndex].Cells[0].Value.ToString());
            int index = FreqPlanListId.IndexOf(ID);
            if (index > -1)
               FreqPlanListId.RemoveAt(index);
            UpdateFreqPlan();
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }
      //=========================================
      /// <summary>
      /// Обновить список частотных планов
      /// </summary>
      private void UpdateFreqPlan()
      {
         dataGridViewPlan.Rows.Clear();
         foreach (int elem in FreqPlanListId)
         {
            RecordPtr RecFreqPlan = new RecordPtr(ICSMTbl.itblFreqPlan, elem);
            IMObject freqPlan = IMObject.LoadFromDB(RecFreqPlan);
            string[] row = new string[] {
               elem.ToString(),
               freqPlan.GetS("NAME")};
            dataGridViewPlan.Rows.Add(row);
         }
      }
   }
}
