﻿namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class FShowFreqAssign
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.dataGridFreq = new System.Windows.Forms.DataGridView();
         this.grpBoxInterference = new System.Windows.Forms.GroupBox();
         this.btnToCsvFile = new System.Windows.Forms.Button();
         this.btnChart = new System.Windows.Forms.Button();
         ((System.ComponentModel.ISupportInitialize)(this.dataGridFreq)).BeginInit();
         this.grpBoxInterference.SuspendLayout();
         this.SuspendLayout();
         // 
         // dataGridFreq
         // 
         this.dataGridFreq.AllowUserToAddRows = false;
         this.dataGridFreq.AllowUserToDeleteRows = false;
         this.dataGridFreq.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dataGridFreq.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dataGridFreq.Location = new System.Drawing.Point(3, 16);
         this.dataGridFreq.MultiSelect = false;
         this.dataGridFreq.Name = "dataGridFreq";
         this.dataGridFreq.ReadOnly = true;
         this.dataGridFreq.RowHeadersWidth = 4;
         this.dataGridFreq.RowTemplate.Height = 24;
         this.dataGridFreq.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dataGridFreq.Size = new System.Drawing.Size(817, 401);
         this.dataGridFreq.TabIndex = 0;
         this.dataGridFreq.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dataGridFreq_SortCompare);
         this.dataGridFreq.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridFreq_CellFormatting);
         // 
         // grpBoxInterference
         // 
         this.grpBoxInterference.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                     | System.Windows.Forms.AnchorStyles.Left)
                     | System.Windows.Forms.AnchorStyles.Right)));
         this.grpBoxInterference.Controls.Add(this.dataGridFreq);
         this.grpBoxInterference.Location = new System.Drawing.Point(12, 12);
         this.grpBoxInterference.Name = "grpBoxInterference";
         this.grpBoxInterference.Size = new System.Drawing.Size(823, 420);
         this.grpBoxInterference.TabIndex = 1;
         this.grpBoxInterference.TabStop = false;
         // 
         // btnToCsvFile
         // 
         this.btnToCsvFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnToCsvFile.Location = new System.Drawing.Point(760, 438);
         this.btnToCsvFile.Name = "btnToCsvFile";
         this.btnToCsvFile.Size = new System.Drawing.Size(75, 23);
         this.btnToCsvFile.TabIndex = 2;
         this.btnToCsvFile.Text = "To csv file";
         this.btnToCsvFile.UseVisualStyleBackColor = true;
         this.btnToCsvFile.Click += new System.EventHandler(this.btnToCsvFile_Click);
         // 
         // btnChart
         // 
         this.btnChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnChart.Location = new System.Drawing.Point(648, 438);
         this.btnChart.Name = "btnChart";
         this.btnChart.Size = new System.Drawing.Size(75, 23);
         this.btnChart.TabIndex = 3;
         this.btnChart.Text = "Chart";
         this.btnChart.UseVisualStyleBackColor = true;
         this.btnChart.Click += new System.EventHandler(this.btnChart_Click);
         // 
         // FShowFreqAssign
         // 
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
         this.ClientSize = new System.Drawing.Size(847, 473);
         this.Controls.Add(this.btnChart);
         this.Controls.Add(this.btnToCsvFile);
         this.Controls.Add(this.grpBoxInterference);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MinimizeBox = false;
         this.Name = "FShowFreqAssign";
         this.ShowInTaskbar = false;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "FShowFreqAssign";
         ((System.ComponentModel.ISupportInitialize)(this.dataGridFreq)).EndInit();
         this.grpBoxInterference.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.DataGridView dataGridFreq;
      private System.Windows.Forms.GroupBox grpBoxInterference;
      private System.Windows.Forms.Button btnToCsvFile;
      private System.Windows.Forms.Button btnChart;
   }
}