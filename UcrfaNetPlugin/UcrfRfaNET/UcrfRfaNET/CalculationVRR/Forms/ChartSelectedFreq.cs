﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    public partial class ChartSelectedFreq : Form
    {
        /// <summary>
        /// Структура для хранения коорднинат
        /// </summary>
        public struct Coordinate
        {
            public double X;
            public double Y;
        }
        /// <summary>
        /// Размер прокрутки
        /// </summary>
        public double ScrollSize { get; set; }
        /// <summary>
        /// Минимальное значение оси X
        /// </summary>
        public double MinX { get; set; }
        /// <summary>
        /// Максимальное значение оси X
        /// </summary>
        public double MaxX { get; set; }
        /// <summary>
        /// Максимальное значение оси Y
        /// </summary>
        private const double MaxY = -35;
        /// <summary>
        /// Минимальное значение оси Y
        /// </summary>
        private const double MinY = -130;

        private const double IntervalX = 0.025;
        /// <summary>
        /// Конструктор
        /// </summary>
        public ChartSelectedFreq()
        {
            InitializeComponent();
            CLocaliz.TxT(this);
            //------
            ScrollSize = 1.8;
        }
        /// <summary>
        /// Заполнить график данными
        /// </summary>
        /// <param name="dataBinding">данные для графика</param>
        public void FillChart(List<ChartSelectedFreq.Coordinate> dataBinding)
        {
            // заполнение грида
            Series series = chart1.Series[0];
            series.ChartType = SeriesChartType.RangeColumn;
            series.XValueType = ChartValueType.Double;
            series.YValueType = ChartValueType.Double;
            series.XValueMember = "X";
            series.YValueMembers = "Y";
            foreach (ChartSelectedFreq.Coordinate coordinate in dataBinding)
            {
                series.Points.AddXY(coordinate.X, coordinate.Y + MinY, MinY);
            }

            ChartArea chartArea = chart1.ChartAreas[series.ChartArea];

            // set view range to [0,max]
            chartArea.AxisX.Minimum = MinX;
            chartArea.AxisX.Maximum = MaxX;
            chartArea.AxisX.Interval = IntervalX;
            _curViewPosition = MinX;
            // set view range to [0,max]
            chartArea.AxisY.Minimum = MinY;
            chartArea.AxisY.Maximum = MaxY;


            chartArea.CursorX.IsUserSelectionEnabled = true;
            chartArea.CursorX.Interval = 0.05;
            chartArea.CursorX.AutoScroll = true;

            // let's zoom to [0,blockSize] (e.g. [0,100])
            chartArea.AxisX.ScaleView.Zoomable = true;
            chartArea.AxisX.ScaleView.SizeType = DateTimeIntervalType.Number;
            chartArea.AxisX.ScaleView.SmallScrollMinSize = 0.005;
            chartArea.AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;
            SetZoom();

            series["PixelPointWidth"] = CalculateWidthOfBar().ToString();
        }
        /// <summary>
        /// Закрашивание колонок
        /// </summary>
        private void chart1_Customize(object sender, EventArgs e)
        {
            foreach (DataPoint point in chart1.Series[0].Points)
            {
                if (point.YValues[0] >= -100)
                    point.Color = Color.Red;
                else
                    point.Color = Color.Green;
            }
        }
        /// <summary>
        /// Подсчитывает ширину бара
        /// </summary>
        /// <returns>ширину бара</returns>
        private int CalculateWidthOfBar()
        {
            ChartArea chartArea = chart1.ChartAreas[chart1.Series[0].ChartArea];
            double curScale = chartArea.AxisX.ScaleView.ViewMaximum - chartArea.AxisX.ScaleView.ViewMinimum;
            double minScale = ScrollSize;
            double scaleSize = (curScale / minScale) + 0.5;
            int pixel = 6 - (int)scaleSize;
            if (pixel < 1) pixel = 1;
            if (pixel > 6) pixel = 6;
            return pixel;
        }

        private int _scaleIndex = 1;
        /// <summary>
        /// Увеличить индекс Zoom
        /// </summary>
        private void IncreaseScale()
        {
            _scaleIndex++;
        }
        /// <summary>
        /// Уменьшить индкус Zoom
        /// </summary>
        private void DecreaseScale()
        {
            if (_scaleIndex > 1)
            {
                _scaleIndex--;
            }
        }
        /// <summary>
        /// Возвращает значение увеличения
        /// </summary>
        /// <returns></returns>
        private double GetScale()
        {
            return ScrollSize * (double) (_scaleIndex + 1);
        }
        /// <summary>
        /// Установить ZOOM
        /// </summary>
        private void SetZoom()
        {
            double scaleValue = GetScale() / 2.0;
            double curPositionTmp = _curViewPosition + scaleValue;
            ChartArea chartArea = chart1.ChartAreas[chart1.Series[0].ChartArea];
            double minVal = curPositionTmp - scaleValue;
            double maxnVal = curPositionTmp + scaleValue;
            bool isMin = false;
            bool isMax = false;
            if (minVal < chartArea.AxisX.Minimum)
            {
                minVal = chartArea.AxisX.Minimum;
                isMin = true;
            }
            if (maxnVal > chartArea.AxisX.Maximum)
            {
                maxnVal = chartArea.AxisX.Maximum;
                isMax = true;
                minVal = maxnVal - scaleValue * 2.0;
                if (minVal < chartArea.AxisX.Minimum)
                {
                    minVal = chartArea.AxisX.Minimum;
                    isMin = true;
                }
            }
            chartArea.AxisX.Interval = IntervalX * (_scaleIndex + 1);
            _curViewPosition = minVal;
            if (isMax && isMin)
            {
                chartArea.AxisX.ScaleView.ZoomReset();
                DecreaseScale();
            }
            else
            {
                chartArea.AxisX.ScaleView.Zoom(minVal, maxnVal);
            }
            chart1.Series[0]["PixelPointWidth"] = CalculateWidthOfBar().ToString();// "6"; //6
            //---
            toolStripButtonZoomDecrease.Enabled = (_scaleIndex > 1);
            toolStripButtonZoomIncrease.Enabled = !(isMax & isMin);
        }

        private double _curViewPosition = 0;
        /// <summary>
        /// Изменилась позиция
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chart1_AxisViewChanged(object sender, ViewEventArgs e)
        {
            if (e.Axis.AxisName == AxisName.X)
                _curViewPosition = e.NewPosition;
        }
        /// <summary>
        /// Кнопка увеличить зум
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonZoomIncrease_Click(object sender, EventArgs e)
        {
            IncreaseZoom();
        }
        /// <summary>
        /// Кнопка уменьшить зум
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonZoomDecrease_Click(object sender, EventArgs e)
        {
            DecreaseZoom();
        }
        /// <summary>
        /// Умеличить зум
        /// </summary>
        private void IncreaseZoom()
        {
            IncreaseScale();
            SetZoom();
        }
        /// <summary>
        /// Уменьшить зум
        /// </summary>
        private void DecreaseZoom()
        {
            DecreaseScale();
            SetZoom();
        }
    }
}
