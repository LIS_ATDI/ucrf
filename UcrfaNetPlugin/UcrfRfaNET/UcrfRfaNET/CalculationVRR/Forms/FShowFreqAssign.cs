﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
   internal partial class FShowFreqAssign : Form
   {
      public double RadiusSelectFreq { get; set; }
      private Station _station;
      private double _minFreq;
      private double _maxFreq;
      private List<int> _listIdFreqPlan = new List<int>();
      public List<int> ListIdFreqPlan { get { return _listIdFreqPlan; } }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="listOut">Список отображаемых строк в гриде (данные для отображения)</param>
      /// <param name="curStation">RecordPtr анализируемой станции</param>
      public FShowFreqAssign(List<string[]> listOut, Station curStation)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         //----
         _station = curStation;
         // create datagrid columns
         // FREQ
         DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
         column.Name = "lisFreq";
         column.HeaderText = CLocaliz.TxT("FREQ");
         column.Width = 50;
         dataGridFreq.Columns.Add(column);
         // INTERFERANCE FOR A STATION
         column = new DataGridViewTextBoxColumn();
         column.Name = "lisInterForStation";
         column.HeaderText = CLocaliz.TxT("INTERFERANCE FOR A STATION");
         column.Width = 50;
         dataGridFreq.Columns.Add(column);
         // INTERFERANCE FROM A STATION
         column = new DataGridViewTextBoxColumn();
         column.Name = "lisInterFromStation";
         column.HeaderText = CLocaliz.TxT("INTERFERANCE FROM A STATION");
         column.Width = 50;
         dataGridFreq.Columns.Add(column);
         // INTERFERANCE FOR AN ABONENT OF A STATION
         column = new DataGridViewTextBoxColumn();
         column.Name = "lisInterForAbonentStation";
         column.HeaderText = CLocaliz.TxT("INTERFERANCE FOR AN ABONENT OF A STATION");
         column.Width = 50;
         dataGridFreq.Columns.Add(column);
         // INTERFERANCE BY AN ABONENT FROM A STATION
         column = new DataGridViewTextBoxColumn();
         column.Name = "lisInterByAbonentFromStation";
         column.HeaderText = CLocaliz.TxT("INTERFERANCE BY AN ABONENT FROM A STATION");
         column.Width = 50;
         dataGridFreq.Columns.Add(column);

         _minFreq = 9999999.0;
         _maxFreq = 0.0;
         // show list
         foreach(string[] strRow in listOut)
         {
            dataGridFreq.Rows.Add(strRow);
            double tmpValue = strRow[0].ToDouble(IM.NullD);
            if(tmpValue != IM.NullD)
            {
               _minFreq = Math.Min(tmpValue, _minFreq);
               _maxFreq = Math.Max(tmpValue, _maxFreq);
      }
         }
      }
      //===================================================
      /// <summary>
      /// Сортирует колонки в соответсвии с правилами соритировки чисел
      /// </summary>
      private void dataGridFreq_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
      {
         e.Handled = true;
         if ((e.CellValue1.ToString() == "-") || (e.CellValue2.ToString() == "-"))
            return;
         double cell1 = ConvertType.ToDouble(e.CellValue1.ToString(), IM.NullD);
         double cell2 = ConvertType.ToDouble(e.CellValue2.ToString(), IM.NullD);
         if ((cell1 != IM.NullD) && (cell2 != IM.NullD))
         {
            if (cell1 > cell2)
               e.SortResult = 1;
            else if (cell1 < cell2)
               e.SortResult = -1;
            else
               e.SortResult = 0;
            e.Handled = true;
         }
      }
      /// <summary>
      /// Форматирование ячейки
      /// </summary>
      private void dataGridFreq_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
      {
         if (e.ColumnIndex > 0)
         {
            double interference = e.Value.ToString().ToDouble(IM.NullD);
            if (interference != IM.NullD)
            {
               if (interference > -100.0)
               {
                  e.CellStyle.BackColor = Color.Red;
   }
               else if (interference < -130.0)
               {
                  e.CellStyle.BackColor = Color.LightGreen;
}
               else
               {
                  e.CellStyle.BackColor = Color.White;
               }
            }
         }
      }
      /// <summary>
      /// Отображает форму с графиком помех
      /// </summary>
      private void btnChart_Click(object sender, EventArgs e)
      {
         List<ChartSelectedFreq.Coordinate> dataForChart = GetDataForChart(1);
         if (dataForChart.Count == 0)
         {
            MessageBox.Show("Завад не знайдено", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
         }

         using (ChartSelectedFreq frm = new ChartSelectedFreq())
         {
            frm.MaxX = _maxFreq + 0.025;
            frm.MinX = _minFreq - 0.025;
            frm.FillChart(dataForChart);
            frm.ShowDialog();
         }
      }
      /// <summary>
      /// Возвращает список данных для отображения на графике
      /// </summary>
      /// <param name="indexColumn">индекс колонки, из которой брать данные</param>
      /// <returns>Список данных для отображения на графике</returns>
      private List<ChartSelectedFreq.Coordinate> GetDataForChart(int indexColumn)
      {
         List<ChartSelectedFreq.Coordinate> retVal = new List<ChartSelectedFreq.Coordinate>();
         foreach (DataGridViewRow row in dataGridFreq.Rows)
         {
            double x = row.Cells[0].Value.ToString().ToDouble(IM.NullD);
            double y = row.Cells[indexColumn].Value.ToString().ToDouble(IM.NullD);
            if((x != IM.NullD) &&
               (y != IM.NullD) &&
               (y > -130.0))         //Помехи < -130 принимаются как незначительные
            {
               //Вычисляем "Y" относителный шкалы вывода
               double y1 = Math.Min(y + 130.0, 90);  // Значения > -40 преобразов. в -40
               retVal.Add(new ChartSelectedFreq.Coordinate {X = x, Y = y1});
            }
         }
         return retVal;
      }
      /// <summary>
      /// Создает файл csv
      /// </summary>
      private void btnToCsvFile_Click(object sender, EventArgs e)
      {
         CreateCsvFile();
      }
      /// <summary>
      /// Создать csv file
      /// </summary>
      private void CreateCsvFile()
      {
         // Формируем файл
         string fullPath = HelpClasses.PluginSetting.PluginFolderSetting.FolderSelectionFreq + "\\" + string.Format("ПЧ_{0}_{1}.csv", _station._table, _station._tableID);
         try
         {
            StreamWriter sw = new StreamWriter(fullPath, false, System.Text.Encoding.Default);
            //-------
            // Параметры станции
            double pow = Math.Pow(10, (_station._power - 30)/10).Round(3);
            string oneLine = string.Format("{0};{1};{2};{3}", "Потужність передавача (Вт)", pow, "Висота станції над поверхнітю землі (м)", _station._aboveEarthLevel);
            sw.WriteLine(oneLine);
            sw.WriteLine();
            oneLine = string.Format("{0};{1};{2};{3}-{4}", "Радіус пошуку завад (Км)", RadiusSelectFreq, "Діапазон частот, що аналізувався (МГц)", _minFreq, _maxFreq);
            sw.WriteLine(oneLine);
            oneLine = string.Format("{0}", "Перелік частотних планів, що аналізувалися");
            sw.WriteLine(oneLine);
            //Список ЧП
            {
               foreach (int id in ListIdFreqPlan)
               {
                  IMRecordset rsPlan = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
                  rsPlan.Select("ID,NAME");
                  rsPlan.SetWhere("ID", IMRecordset.Operation.Eq, id);
                  rsPlan.Open();
                  if(!rsPlan.IsEOF())
                  {
                     oneLine = string.Format(";{0}", rsPlan.GetS("NAME"));
                     sw.WriteLine(oneLine);
                  }
                  if (rsPlan.IsOpen())
                     rsPlan.Close();
                  rsPlan.Destroy();
               }
            }
            //---
            sw.WriteLine();
            // Шапка данных
            oneLine = string.Format("{0};{1};{2};{3};{4}",
               "Частота (МГц)",
               "Завада станції (дБм)",
               "Завада від станції (дБм)",
               "Завада абонентам (дБм)",
               "Завада абонентам від станції (дБм)");
            sw.WriteLine(oneLine);
            // Данные
            foreach (DataGridViewRow row in dataGridFreq.Rows)
            {
               oneLine = "";
               foreach (DataGridViewCell cell in row.Cells)
               {
                  oneLine += cell.Value + ";";
               }
               sw.WriteLine(oneLine);
            }
            //-------
            sw.WriteLine();
            string fio = "";
            {
               IMRecordset rsUser = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
               rsUser.Select("ID,LASTNAME,FIRSTNAME");
               rsUser.SetWhere("ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
               rsUser.Open();
               if(!rsUser.IsEOF())
               {
                  fio = string.Format("{0} {1}", rsUser.GetS("LASTNAME"), rsUser.GetS("FIRSTNAME"));
               }
               if(rsUser.IsOpen())
                  rsUser.Close();
               rsUser.Destroy();
            }
            oneLine = string.Format("Виконавець:;;{0};", fio);
            sw.WriteLine(oneLine);
            //-------
            sw.Flush(); // очистка буфера
            sw.Close();
            //-------
            //Создаем собития в заявках
            RecordPtr rec = new RecordPtr(_station._table, _station._tableID);
            PrintDocs print = new PrintDocs();
            print.LoadDocLink(Documents.DocType.SELECTION_FREQ, "", fullPath, DateTime.Now, IM.NullT, rec.Applications(), true);
            if(MessageBox.Show(string.Format(CLocaliz.TxT("File '{0}' was created. Do you want to open it?"), fullPath), CLocaliz.TxT("Message"),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
               System.Diagnostics.Process.Start(fullPath);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message, CLocaliz.TxT("Error"));
         }
      }
   }
}
