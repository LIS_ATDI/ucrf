﻿namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class ChartSelectedFreq
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
          System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
          this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
          this.toolStrip1 = new System.Windows.Forms.ToolStrip();
          this.toolStripButtonZoomIncrease = new System.Windows.Forms.ToolStripButton();
          this.toolStripButtonZoomDecrease = new System.Windows.Forms.ToolStripButton();
          ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
          this.toolStrip1.SuspendLayout();
          this.SuspendLayout();
          // 
          // chart1
          // 
          this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.chart1.BackColor = System.Drawing.Color.Transparent;
          chartArea2.AxisX.Interval = 0.025;
          chartArea2.AxisX.LabelStyle.Format = "F3";
          chartArea2.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
          chartArea2.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
          chartArea2.AxisX.ScaleView.Zoomable = false;
          chartArea2.AxisX.ScrollBar.ButtonStyle = System.Windows.Forms.DataVisualization.Charting.ScrollBarButtonStyles.SmallScroll;
          chartArea2.AxisY.Interval = 20;
          chartArea2.AxisY.MajorGrid.Interval = 20;
          chartArea2.AxisY.MajorGrid.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
          chartArea2.AxisY.MinorGrid.Enabled = true;
          chartArea2.AxisY.MinorGrid.Interval = 20;
          chartArea2.AxisY.MinorGrid.IntervalOffset = 10;
          chartArea2.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
          chartArea2.AxisY.Title = "дБм";
          chartArea2.Name = "ChartArea1";
          this.chart1.ChartAreas.Add(chartArea2);
          this.chart1.Location = new System.Drawing.Point(12, 28);
          this.chart1.Name = "chart1";
          series2.ChartArea = "ChartArea1";
          series2.LabelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
          series2.Name = "Series1";
          series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
          series2.YValuesPerPoint = 2;
          series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
          this.chart1.Series.Add(series2);
          this.chart1.Size = new System.Drawing.Size(974, 612);
          this.chart1.TabIndex = 0;
          this.chart1.Text = "chart1";
          this.chart1.Customize += new System.EventHandler(this.chart1_Customize);
          this.chart1.AxisViewChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ViewEventArgs>(this.chart1_AxisViewChanged);
          // 
          // toolStrip1
          // 
          this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonZoomIncrease,
            this.toolStripButtonZoomDecrease});
          this.toolStrip1.Location = new System.Drawing.Point(0, 0);
          this.toolStrip1.Name = "toolStrip1";
          this.toolStrip1.Size = new System.Drawing.Size(998, 25);
          this.toolStrip1.TabIndex = 5;
          this.toolStrip1.Text = "toolStrip1";
          // 
          // toolStripButtonZoomIncrease
          // 
          this.toolStripButtonZoomIncrease.Image = global::XICSM.UcrfRfaNET.Properties.Resources.zoom_in;
          this.toolStripButtonZoomIncrease.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonZoomIncrease.Name = "toolStripButtonZoomIncrease";
          this.toolStripButtonZoomIncrease.Size = new System.Drawing.Size(79, 22);
          this.toolStripButtonZoomIncrease.Text = "Збільшити";
          this.toolStripButtonZoomIncrease.Click += new System.EventHandler(this.toolStripButtonZoomIncrease_Click);
          // 
          // toolStripButtonZoomDecrease
          // 
          this.toolStripButtonZoomDecrease.Image = global::XICSM.UcrfRfaNET.Properties.Resources.zoom_out;
          this.toolStripButtonZoomDecrease.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripButtonZoomDecrease.Name = "toolStripButtonZoomDecrease";
          this.toolStripButtonZoomDecrease.Size = new System.Drawing.Size(77, 22);
          this.toolStripButtonZoomDecrease.Text = "Зменшити";
          this.toolStripButtonZoomDecrease.Click += new System.EventHandler(this.toolStripButtonZoomDecrease_Click);
          // 
          // ChartSelectedFreq
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(998, 652);
          this.Controls.Add(this.toolStrip1);
          this.Controls.Add(this.chart1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.MinimizeBox = false;
          this.Name = "ChartSelectedFreq";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Результати підбору частот";
          ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
          this.toolStrip1.ResumeLayout(false);
          this.toolStrip1.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
      private System.Windows.Forms.ToolStrip toolStrip1;
      private System.Windows.Forms.ToolStripButton toolStripButtonZoomIncrease;
      private System.Windows.Forms.ToolStripButton toolStripButtonZoomDecrease;
   }
}