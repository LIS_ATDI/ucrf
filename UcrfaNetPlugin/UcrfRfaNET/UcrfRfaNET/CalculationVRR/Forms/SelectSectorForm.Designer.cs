﻿namespace XICSM.UcrfRfaNET.CalculationVRR
{
   partial class SelectSectorForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.SectorLabel = new System.Windows.Forms.Label();
          this.CalculationButton = new System.Windows.Forms.Button();
          this.SectorListBox = new System.Windows.Forms.ListBox();
          this.btnAllSectors = new System.Windows.Forms.Button();
          this.SuspendLayout();
          // 
          // SectorLabel
          // 
          this.SectorLabel.AutoSize = true;
          this.SectorLabel.Location = new System.Drawing.Point(12, 15);
          this.SectorLabel.Name = "SectorLabel";
          this.SectorLabel.Size = new System.Drawing.Size(46, 13);
          this.SectorLabel.TabIndex = 0;
          this.SectorLabel.Text = "Сектор:";
          // 
          // CalculationButton
          // 
          this.CalculationButton.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.CalculationButton.Location = new System.Drawing.Point(179, 100);
          this.CalculationButton.Name = "CalculationButton";
          this.CalculationButton.Size = new System.Drawing.Size(91, 23);
          this.CalculationButton.TabIndex = 2;
          this.CalculationButton.Text = "Розрахувати";
          this.CalculationButton.UseVisualStyleBackColor = true;
          // 
          // SectorListBox
          // 
          this.SectorListBox.FormattingEnabled = true;
          this.SectorListBox.Location = new System.Drawing.Point(72, 12);
          this.SectorListBox.Name = "SectorListBox";
          this.SectorListBox.Size = new System.Drawing.Size(198, 82);
          this.SectorListBox.TabIndex = 3;
          // 
          // btnAllSectors
          // 
          this.btnAllSectors.DialogResult = System.Windows.Forms.DialogResult.Retry;
          this.btnAllSectors.Location = new System.Drawing.Point(12, 100);
          this.btnAllSectors.Name = "btnAllSectors";
          this.btnAllSectors.Size = new System.Drawing.Size(107, 23);
          this.btnAllSectors.TabIndex = 4;
          this.btnAllSectors.Text = "По всім секторам";
          this.btnAllSectors.UseVisualStyleBackColor = true;
          // 
          // SelectSectorForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(282, 130);
          this.Controls.Add(this.btnAllSectors);
          this.Controls.Add(this.SectorListBox);
          this.Controls.Add(this.CalculationButton);
          this.Controls.Add(this.SectorLabel);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "SelectSectorForm";
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Виберіть сектор для разрахунку ЕМС";
          this.Shown += new System.EventHandler(this.SelectSectorForm_Shown);
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label SectorLabel;
      private System.Windows.Forms.Button CalculationButton;
      private System.Windows.Forms.ListBox SectorListBox;
      private System.Windows.Forms.Button btnAllSectors;
   }
}