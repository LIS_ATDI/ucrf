﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.CalculationVRR
{
    internal partial class UcGroupLabel : UserControl, INotifyPropertyChanged
    {
        private string _lbl1 = "";
        public string Lbl1
        {
            get { return _lbl1; }
            set
            {
                if (value != _lbl1)
                {
                    _lbl1 = value;
                    NotifyPropertyChanged("Lbl1");
                }
            }
        }
        private string _lbl2 = "";
        public string Lbl2
        {
            get { return _lbl2; }
            set
            {
                if (value != _lbl2)
                {
                    _lbl2 = value;
                    NotifyPropertyChanged("Lbl2");
                }
            }
        }
        private string _lbl3 = "";
        public string Lbl3
        {
            get { return _lbl3; }
            set
            {
                if (value != _lbl3)
                {
                    _lbl3 = value;
                    NotifyPropertyChanged("Lbl3");
                }
            }
        }
        private string _lbl4 = "";
        public string Lbl4
        {
            get { return _lbl4; }
            set
            {
                if (value != _lbl4)
                {
                    _lbl4 = value;
                    NotifyPropertyChanged("Lbl4");
                }
            }
        }
        private string _lbl5 = "";
        public string Lbl5
        {
            get { return _lbl5; }
            set
            {
                if (value != _lbl5)
                {
                    _lbl5 = value;
                    NotifyPropertyChanged("Lbl5");
                }
            }
        }
        //-----------
        public UcGroupLabel()
        {
            InitializeComponent();
            //------
            lbl1.DataBindings.Add("Text", this, "Lbl1");
            lbl2.DataBindings.Add("Text", this, "Lbl2");
            lbl3.DataBindings.Add("Text", this, "Lbl3");
            lbl4.DataBindings.Add("Text", this, "Lbl4");
            lbl5.DataBindings.Add("Text", this, "Lbl5");
        }
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion
    }
}
