﻿namespace XICSM.UcrfRfaNET.CalculationVRR
{
    partial class FrmCalculationResultVrr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTabTreeResult = new System.Windows.Forms.Panel();
            this.lblNointerference = new System.Windows.Forms.Label();
            this.btnToIcaTelecom = new System.Windows.Forms.Button();
            this.btnOnmap = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pnlUpdatedData = new System.Windows.Forms.Panel();
            this.dgView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblLorH = new System.Windows.Forms.Label();
            this.lblDistance = new System.Windows.Forms.Label();
            this.pnlLftRx = new System.Windows.Forms.Panel();
            this.lblLftRx = new System.Windows.Forms.Label();
            this.pnlRx = new System.Windows.Forms.Panel();
            this.pnlLftTx = new System.Windows.Forms.Panel();
            this.lblLftTx = new System.Windows.Forms.Label();
            this.pnlTx = new System.Windows.Forms.Panel();
            this.pnlAntennaRx = new System.Windows.Forms.Panel();
            this.pnlAntennaTx = new System.Windows.Forms.Panel();
            this.lblGreen = new System.Windows.Forms.Label();
            this.lblRed = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnldataAboutCurStation = new System.Windows.Forms.Panel();
            this.lblStationName = new System.Windows.Forms.Label();
            this.lblPorog = new System.Windows.Forms.Label();
            this.lblRadIntermod = new System.Windows.Forms.Label();
            this.lblR = new System.Windows.Forms.Label();
            this.lblDiapazon = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAzimuth = new System.Windows.Forms.Label();
            this.lblH = new System.Windows.Forms.Label();
            this.lblG = new System.Windows.Forms.Label();
            this.lblSencitivity = new System.Windows.Forms.Label();
            this.lblPower = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.lblTypeStation = new System.Windows.Forms.Label();
            this.lblFreq = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblOwner = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblDistanceBord = new System.Windows.Forms.Label();
            this.pnlTabTreeResult.SuspendLayout();
            this.pnlUpdatedData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.pnlLftRx.SuspendLayout();
            this.pnlLftTx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnldataAboutCurStation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTabTreeResult
            // 
            this.pnlTabTreeResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTabTreeResult.BackColor = System.Drawing.Color.White;
            this.pnlTabTreeResult.Controls.Add(this.lblNointerference);
            this.pnlTabTreeResult.Location = new System.Drawing.Point(12, 210);
            this.pnlTabTreeResult.Name = "pnlTabTreeResult";
            this.pnlTabTreeResult.Size = new System.Drawing.Size(920, 315);
            this.pnlTabTreeResult.TabIndex = 0;
            // 
            // lblNointerference
            // 
            this.lblNointerference.BackColor = System.Drawing.Color.Transparent;
            this.lblNointerference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNointerference.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNointerference.Location = new System.Drawing.Point(0, 0);
            this.lblNointerference.Name = "lblNointerference";
            this.lblNointerference.Size = new System.Drawing.Size(920, 315);
            this.lblNointerference.TabIndex = 0;
            this.lblNointerference.Text = "There are neither sacrifices nor interferences";
            this.lblNointerference.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnToIcaTelecom
            // 
            this.btnToIcaTelecom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnToIcaTelecom.Location = new System.Drawing.Point(13, 707);
            this.btnToIcaTelecom.Name = "btnToIcaTelecom";
            this.btnToIcaTelecom.Size = new System.Drawing.Size(96, 23);
            this.btnToIcaTelecom.TabIndex = 1;
            this.btnToIcaTelecom.Text = "To ICSTelecom";
            this.btnToIcaTelecom.UseVisualStyleBackColor = true;
            this.btnToIcaTelecom.Click += new System.EventHandler(this.btnToIcaTelecom_Click);
            // 
            // btnOnmap
            // 
            this.btnOnmap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOnmap.Location = new System.Drawing.Point(411, 707);
            this.btnOnmap.Name = "btnOnmap";
            this.btnOnmap.Size = new System.Drawing.Size(107, 23);
            this.btnOnmap.TabIndex = 2;
            this.btnOnmap.Text = "OnMap";
            this.btnOnmap.UseVisualStyleBackColor = true;
            this.btnOnmap.Click += new System.EventHandler(this.btnOnmap_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(812, 707);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(118, 23);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pnlUpdatedData
            // 
            this.pnlUpdatedData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlUpdatedData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUpdatedData.Controls.Add(this.dgView);
            this.pnlUpdatedData.Controls.Add(this.lblLorH);
            this.pnlUpdatedData.Controls.Add(this.lblDistance);
            this.pnlUpdatedData.Controls.Add(this.pnlLftRx);
            this.pnlUpdatedData.Controls.Add(this.pnlRx);
            this.pnlUpdatedData.Controls.Add(this.pnlLftTx);
            this.pnlUpdatedData.Controls.Add(this.pnlTx);
            this.pnlUpdatedData.Controls.Add(this.pnlAntennaRx);
            this.pnlUpdatedData.Controls.Add(this.pnlAntennaTx);
            this.pnlUpdatedData.Controls.Add(this.lblGreen);
            this.pnlUpdatedData.Controls.Add(this.lblRed);
            this.pnlUpdatedData.Controls.Add(this.pictureBox5);
            this.pnlUpdatedData.Controls.Add(this.pictureBox4);
            this.pnlUpdatedData.Controls.Add(this.pictureBox3);
            this.pnlUpdatedData.Controls.Add(this.pictureBox2);
            this.pnlUpdatedData.Controls.Add(this.pictureBox1);
            this.pnlUpdatedData.Location = new System.Drawing.Point(12, 531);
            this.pnlUpdatedData.Name = "pnlUpdatedData";
            this.pnlUpdatedData.Size = new System.Drawing.Size(920, 157);
            this.pnlUpdatedData.TabIndex = 4;
            // 
            // dgView
            // 
            this.dgView.AllowUserToAddRows = false;
            this.dgView.AllowUserToDeleteRows = false;
            this.dgView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.ColumnHeadersVisible = false;
            this.dgView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgView.Location = new System.Drawing.Point(10, 94);
            this.dgView.Name = "dgView";
            this.dgView.ReadOnly = true;
            this.dgView.RowHeadersWidth = 5;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgView.Size = new System.Drawing.Size(899, 48);
            this.dgView.TabIndex = 9;
            this.dgView.TabStop = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 5;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 5;
            // 
            // lblLorH
            // 
            this.lblLorH.Location = new System.Drawing.Point(407, 65);
            this.lblLorH.Name = "lblLorH";
            this.lblLorH.Size = new System.Drawing.Size(100, 23);
            this.lblLorH.TabIndex = 8;
            this.lblLorH.Text = "lblLorH";
            this.lblLorH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDistance
            // 
            this.lblDistance.Location = new System.Drawing.Point(407, 15);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(100, 23);
            this.lblDistance.TabIndex = 7;
            this.lblDistance.Text = "lblDistance";
            this.lblDistance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlLftRx
            // 
            this.pnlLftRx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLftRx.Controls.Add(this.lblLftRx);
            this.pnlLftRx.Location = new System.Drawing.Point(683, 35);
            this.pnlLftRx.Name = "pnlLftRx";
            this.pnlLftRx.Size = new System.Drawing.Size(95, 31);
            this.pnlLftRx.TabIndex = 6;
            // 
            // lblLftRx
            // 
            this.lblLftRx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLftRx.Location = new System.Drawing.Point(0, 0);
            this.lblLftRx.Name = "lblLftRx";
            this.lblLftRx.Size = new System.Drawing.Size(93, 29);
            this.lblLftRx.TabIndex = 0;
            this.lblLftRx.Text = "lblLftRx";
            this.lblLftRx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlRx
            // 
            this.pnlRx.Location = new System.Drawing.Point(805, 15);
            this.pnlRx.Name = "pnlRx";
            this.pnlRx.Size = new System.Drawing.Size(104, 63);
            this.pnlRx.TabIndex = 5;
            // 
            // pnlLftTx
            // 
            this.pnlLftTx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLftTx.Controls.Add(this.lblLftTx);
            this.pnlLftTx.Location = new System.Drawing.Point(145, 35);
            this.pnlLftTx.Name = "pnlLftTx";
            this.pnlLftTx.Size = new System.Drawing.Size(95, 31);
            this.pnlLftTx.TabIndex = 3;
            // 
            // lblLftTx
            // 
            this.lblLftTx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLftTx.Location = new System.Drawing.Point(0, 0);
            this.lblLftTx.Name = "lblLftTx";
            this.lblLftTx.Size = new System.Drawing.Size(93, 29);
            this.lblLftTx.TabIndex = 0;
            this.lblLftTx.Text = "lblLftTx";
            this.lblLftTx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTx
            // 
            this.pnlTx.Location = new System.Drawing.Point(8, 15);
            this.pnlTx.Name = "pnlTx";
            this.pnlTx.Size = new System.Drawing.Size(104, 63);
            this.pnlTx.TabIndex = 2;
            // 
            // pnlAntennaRx
            // 
            this.pnlAntennaRx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAntennaRx.Location = new System.Drawing.Point(536, 15);
            this.pnlAntennaRx.Name = "pnlAntennaRx";
            this.pnlAntennaRx.Size = new System.Drawing.Size(104, 73);
            this.pnlAntennaRx.TabIndex = 1;
            // 
            // pnlAntennaTx
            // 
            this.pnlAntennaTx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAntennaTx.Location = new System.Drawing.Point(272, 15);
            this.pnlAntennaTx.Name = "pnlAntennaTx";
            this.pnlAntennaTx.Size = new System.Drawing.Size(104, 73);
            this.pnlAntennaTx.TabIndex = 0;
            // 
            // lblGreen
            // 
            this.lblGreen.BackColor = System.Drawing.Color.Green;
            this.lblGreen.Location = new System.Drawing.Point(762, 4);
            this.lblGreen.Name = "lblGreen";
            this.lblGreen.Size = new System.Drawing.Size(100, 23);
            this.lblGreen.TabIndex = 16;
            // 
            // lblRed
            // 
            this.lblRed.BackColor = System.Drawing.Color.Red;
            this.lblRed.Location = new System.Drawing.Point(117, 4);
            this.lblRed.Name = "lblRed";
            this.lblRed.Size = new System.Drawing.Size(100, 23);
            this.lblRed.TabIndex = 15;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::XICSM.UcrfRfaNET.Properties.Resources.line;
            this.pictureBox5.Location = new System.Drawing.Point(112, 41);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(33, 20);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 14;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::XICSM.UcrfRfaNET.Properties.Resources.line;
            this.pictureBox4.Location = new System.Drawing.Point(240, 41);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(32, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::XICSM.UcrfRfaNET.Properties.Resources.line;
            this.pictureBox3.Location = new System.Drawing.Point(778, 40);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::XICSM.UcrfRfaNET.Properties.Resources.line;
            this.pictureBox2.Location = new System.Drawing.Point(640, 40);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(43, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::XICSM.UcrfRfaNET.Properties.Resources.arrow;
            this.pictureBox1.Location = new System.Drawing.Point(376, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // pnldataAboutCurStation
            // 
            this.pnldataAboutCurStation.BackColor = System.Drawing.Color.White;
            this.pnldataAboutCurStation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldataAboutCurStation.Controls.Add(this.lblDistanceBord);
            this.pnldataAboutCurStation.Controls.Add(this.lblStationName);
            this.pnldataAboutCurStation.Controls.Add(this.lblPorog);
            this.pnldataAboutCurStation.Controls.Add(this.lblRadIntermod);
            this.pnldataAboutCurStation.Controls.Add(this.lblR);
            this.pnldataAboutCurStation.Controls.Add(this.lblDiapazon);
            this.pnldataAboutCurStation.Controls.Add(this.label2);
            this.pnldataAboutCurStation.Controls.Add(this.lblAzimuth);
            this.pnldataAboutCurStation.Controls.Add(this.lblH);
            this.pnldataAboutCurStation.Controls.Add(this.lblG);
            this.pnldataAboutCurStation.Controls.Add(this.lblSencitivity);
            this.pnldataAboutCurStation.Controls.Add(this.lblPower);
            this.pnldataAboutCurStation.Controls.Add(this.lblClass);
            this.pnldataAboutCurStation.Controls.Add(this.lblTypeStation);
            this.pnldataAboutCurStation.Controls.Add(this.lblFreq);
            this.pnldataAboutCurStation.Controls.Add(this.lblAddress);
            this.pnldataAboutCurStation.Controls.Add(this.lblOwner);
            this.pnldataAboutCurStation.Controls.Add(this.label1);
            this.pnldataAboutCurStation.Location = new System.Drawing.Point(12, 12);
            this.pnldataAboutCurStation.Name = "pnldataAboutCurStation";
            this.pnldataAboutCurStation.Size = new System.Drawing.Size(920, 192);
            this.pnldataAboutCurStation.TabIndex = 5;
            // 
            // lblStationName
            // 
            this.lblStationName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStationName.Location = new System.Drawing.Point(6, 96);
            this.lblStationName.Name = "lblStationName";
            this.lblStationName.Size = new System.Drawing.Size(280, 19);
            this.lblStationName.TabIndex = 16;
            this.lblStationName.Text = "lblStationName";
            // 
            // lblPorog
            // 
            this.lblPorog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPorog.Location = new System.Drawing.Point(628, 158);
            this.lblPorog.Name = "lblPorog";
            this.lblPorog.Size = new System.Drawing.Size(197, 19);
            this.lblPorog.TabIndex = 15;
            this.lblPorog.Text = "lblPorog";
            // 
            // lblRadIntermod
            // 
            this.lblRadIntermod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRadIntermod.Location = new System.Drawing.Point(437, 158);
            this.lblRadIntermod.Name = "lblRadIntermod";
            this.lblRadIntermod.Size = new System.Drawing.Size(192, 19);
            this.lblRadIntermod.TabIndex = 14;
            this.lblRadIntermod.Text = "lblRadIntermod";
            // 
            // lblR
            // 
            this.lblR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblR.Location = new System.Drawing.Point(326, 158);
            this.lblR.Name = "lblR";
            this.lblR.Size = new System.Drawing.Size(112, 19);
            this.lblR.TabIndex = 13;
            this.lblR.Text = "lblR";
            // 
            // lblDiapazon
            // 
            this.lblDiapazon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDiapazon.Location = new System.Drawing.Point(95, 158);
            this.lblDiapazon.Name = "lblDiapazon";
            this.lblDiapazon.Size = new System.Drawing.Size(232, 19);
            this.lblDiapazon.TabIndex = 12;
            this.lblDiapazon.Text = "lblDiapazon";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(2, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(916, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Параметри розрахунку";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAzimuth
            // 
            this.lblAzimuth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAzimuth.Location = new System.Drawing.Point(490, 96);
            this.lblAzimuth.Name = "lblAzimuth";
            this.lblAzimuth.Size = new System.Drawing.Size(220, 19);
            this.lblAzimuth.TabIndex = 10;
            this.lblAzimuth.Text = "lblAzimuth";
            // 
            // lblH
            // 
            this.lblH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblH.Location = new System.Drawing.Point(709, 115);
            this.lblH.Name = "lblH";
            this.lblH.Size = new System.Drawing.Size(198, 19);
            this.lblH.TabIndex = 9;
            this.lblH.Text = "lblH";
            // 
            // lblG
            // 
            this.lblG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblG.Location = new System.Drawing.Point(562, 115);
            this.lblG.Name = "lblG";
            this.lblG.Size = new System.Drawing.Size(148, 19);
            this.lblG.TabIndex = 8;
            this.lblG.Text = "lblG";
            // 
            // lblSencitivity
            // 
            this.lblSencitivity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSencitivity.Location = new System.Drawing.Point(382, 115);
            this.lblSencitivity.Name = "lblSencitivity";
            this.lblSencitivity.Size = new System.Drawing.Size(181, 19);
            this.lblSencitivity.TabIndex = 7;
            this.lblSencitivity.Text = "lblSencitivity";
            // 
            // lblPower
            // 
            this.lblPower.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPower.Location = new System.Drawing.Point(238, 115);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(145, 19);
            this.lblPower.TabIndex = 6;
            this.lblPower.Text = "lblPower";
            // 
            // lblClass
            // 
            this.lblClass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClass.Location = new System.Drawing.Point(6, 115);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(233, 19);
            this.lblClass.TabIndex = 5;
            this.lblClass.Text = "lblClass";
            // 
            // lblTypeStation
            // 
            this.lblTypeStation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTypeStation.Location = new System.Drawing.Point(285, 96);
            this.lblTypeStation.Name = "lblTypeStation";
            this.lblTypeStation.Size = new System.Drawing.Size(206, 19);
            this.lblTypeStation.TabIndex = 4;
            this.lblTypeStation.Text = "lblTypeStation";
            // 
            // lblFreq
            // 
            this.lblFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFreq.Location = new System.Drawing.Point(6, 58);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(901, 38);
            this.lblFreq.TabIndex = 3;
            this.lblFreq.Text = "lblFreq";
            this.lblFreq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddress
            // 
            this.lblAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAddress.Location = new System.Drawing.Point(506, 20);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(401, 38);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "lblAddress";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOwner
            // 
            this.lblOwner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOwner.Location = new System.Drawing.Point(6, 20);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(501, 38);
            this.lblOwner.TabIndex = 1;
            this.lblOwner.Text = "lblOwner";
            this.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOwner.UseMnemonic = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(916, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Параметри станції, що аналізується";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(215, 707);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "OU TV";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblDistanceBord
            // 
            this.lblDistanceBord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDistanceBord.Location = new System.Drawing.Point(709, 96);
            this.lblDistanceBord.Name = "lblDistanceBord";
            this.lblDistanceBord.Size = new System.Drawing.Size(198, 19);
            this.lblDistanceBord.TabIndex = 17;
            this.lblDistanceBord.Text = "label3";
            // 
            // FrmCalculationResultVrr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 742);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pnldataAboutCurStation);
            this.Controls.Add(this.pnlUpdatedData);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnOnmap);
            this.Controls.Add(this.btnToIcaTelecom);
            this.Controls.Add(this.pnlTabTreeResult);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(950, 640);
            this.Name = "FrmCalculationResultVrr";
            this.Text = "Calculation result";
            this.Load += new System.EventHandler(this.FrmCalculationResultVrr_Load);
            this.ResizeEnd += new System.EventHandler(this.FrmCalculationResultVrr_ResizeEnd);
            this.Resize += new System.EventHandler(this.FrmCalculationResultVrr_Resize);
            this.pnlTabTreeResult.ResumeLayout(false);
            this.pnlUpdatedData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.pnlLftRx.ResumeLayout(false);
            this.pnlLftTx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnldataAboutCurStation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTabTreeResult;
        private System.Windows.Forms.Button btnToIcaTelecom;
        private System.Windows.Forms.Button btnOnmap;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Panel pnlUpdatedData;
        private System.Windows.Forms.Panel pnldataAboutCurStation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblFreq;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label lblTypeStation;
        private System.Windows.Forms.Label lblAzimuth;
        private System.Windows.Forms.Label lblH;
        private System.Windows.Forms.Label lblG;
        private System.Windows.Forms.Label lblSencitivity;
        private System.Windows.Forms.Label lblPorog;
        private System.Windows.Forms.Label lblRadIntermod;
        private System.Windows.Forms.Label lblR;
        private System.Windows.Forms.Label lblDiapazon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlAntennaTx;
        private System.Windows.Forms.Panel pnlAntennaRx;
        private System.Windows.Forms.Panel pnlLftTx;
        private System.Windows.Forms.Panel pnlTx;
        private System.Windows.Forms.Panel pnlLftRx;
        private System.Windows.Forms.Panel pnlRx;
        private System.Windows.Forms.Label lblLftRx;
        private System.Windows.Forms.Label lblLftTx;
        private System.Windows.Forms.Label lblLorH;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblGreen;
        private System.Windows.Forms.Label lblRed;
        private System.Windows.Forms.Label lblNointerference;
        private System.Windows.Forms.Label lblStationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblDistanceBord;
    }
}