﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ICSM;
using System.Globalization;
using ZedGraph;

namespace XICSM.UcrfRfaNET.AdditionFillters
{
   public partial class AdditionFilters : Form
   {
      #region MEMBERS

      public enum StateItem
      {
         Nothing,
         Delete,
         Update,
         Insert
      }

      public struct ParamsData
      {
         public int id;
         public int extFilterID;
         public double deltaFreq;
         public double loss;
         public StateItem state;
      }
      List<ParamsData> paramsDataList = new List<ParamsData>();

      private int _mainID;
      private double _maxFreq;
      private double _minFreq;
      private double _freq;
      private double _maxPower;
      private NumberFormatInfo provider;
      private bool _newRecord;
      private ZedGraphControl GraphControl;

      #endregion

      // конструктор для нового запису
      public AdditionFilters(string tableName)
         : this(tableName, 0)
      {
         _newRecord = true;
      }

      // Фунция изменения размера и положения графика в зависимости от размера формы
      private void SetSize()
      {
         GraphControl.Location = new Point(1, 1); // задаем положение графика
         GraphControl.Size = new Size(GraphControl.Parent.Width - 1, GraphControl.Parent.Height - 1); // размеры графика
      }

      // конструктор для едіта форми
      public AdditionFilters(string tableName, int idTable)
      {
         InitializeComponent();
         _newRecord = false;
         provider = new NumberFormatInfo();

         GraphControl = new ZedGraphControl();
         GraphControl.Parent = GraphPanel;
         //GraphControl.Parent = this;
         SetSize();

         FiilDataWithDB(tableName, idTable);

         CLocaliz.TxT(this);
         BtnOk.Enabled = ((IM.TableRight(tableName) & IMTableRight.Update) == IMTableRight.Update);
      }

      #region METHODS

      /// <summary>
      /// метод заповняє текстові поля з бд
      /// </summary>
      private void FiilDataWithDB(string tableName, int idTable)
      {
         if ((tableName == "") || (idTable == 0))
            return;

         IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
         r.Select("ID,TYPE,NAME,MAX_POWER,FREQ,MAX_FREQ,MIN_FREQ,NOTE,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE");
         r.SetWhere("ID", IMRecordset.Operation.Eq, idTable);
         r.Open();
         if (!r.IsEOF())
         {
            _mainID = r.GetI("ID");
            TbxType.Text = r.GetS("TYPE");
            TbxName.Text = r.GetS("NAME");
            _maxPower = r.GetD("MAX_POWER");
            _freq = r.GetD("FREQ");
            _maxFreq = r.GetD("MAX_FREQ");

            if (_maxPower == IM.NullD)
               _maxPower = 0;

            if (_freq == IM.NullD)
               _freq = 0;

            if (_maxFreq == IM.NullD)
               _maxFreq = 0;

            _minFreq = r.GetD("MIN_FREQ");
            if (_minFreq == IM.NullD)
               _minFreq = 0;

            TbxNote.Text = r.GetS("NOTE");
            labelOutCreatedBy.Text = r.GetS("CREATED_BY");
            labelOutCreatedDate.Text = r.GetT("CREATED_DATE").ToString("dd MMM yyyy");

            string modifiedBy = r.GetS("MODIFIED_BY");
            if ((modifiedBy != null) && (modifiedBy != ""))
            {
               labelOutModifiedBy.Text = modifiedBy;
               labelOutModifiedDate.Text = r.GetT("MODIFIED_DATE").ToString("dd MMM yyyy");
            }

            r.Close();
            r.Destroy();

            r = new IMRecordset("XNRFA_FILTER_PARAM", IMRecordset.Mode.ReadOnly);
            r.Select("ID,EXT_FILT_ID,DELTA_FREQ,LOSS");
            r.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _mainID);
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               ParamsData paramsData = new ParamsData();
               paramsData.id = r.GetI("ID");
               paramsData.extFilterID = r.GetI("EXT_FILT_ID");
               paramsData.deltaFreq = r.GetD("DELTA_FREQ");
               paramsData.loss = r.GetD("LOSS");
               paramsData.state = StateItem.Nothing;
               paramsDataList.Add(paramsData);
            }
            r.Close();
            r.Destroy();

            DisplayedData(false); // відображити всі дані
         }
      }

      /// <summary>
      /// метод для перевірки введених даних
      /// </summary>
      private void CheckData()
      {
         try
         {
            if (TbxMaxFreq.Text.Length > 0)
               _maxFreq = Convert.ToDouble(TbxMaxFreq.Text, provider);            

            if (TbxMinFreq.Text.Length > 0)
               _minFreq = Convert.ToDouble(TbxMinFreq.Text, provider);

            if (TbxFreq.Text.Length > 0)
               _freq = Convert.ToDouble(TbxFreq.Text, provider);

            if (TbxMaxPower.Text.Length > 0)
               _maxPower = Convert.ToDouble(TbxMaxPower.Text, provider);
         }
         catch(Exception e)
         {
            MessageBox.Show("Exception: " + e.Message);
            throw e;
         }         
      }
      /// <summary>
      /// функція для відображення та оновлення данних
      /// </summary>
      private void DisplayedData(bool onlyListView)
      {
         if (!onlyListView)
         {
            TbxMaxFreq.Text = _maxFreq.ToString();
            TbxMinFreq.Text = _minFreq.ToString();
            TbxFreq.Text = _freq.ToString();
            TbxMaxPower.Text = _maxPower.ToString();
         }

         ParamsComparer pc = new ParamsComparer();
         paramsDataList.Sort(pc);
         LsvDataContent.Items.Clear();
         PointPairList listGraph = new PointPairList();

         foreach (ParamsData item in paramsDataList)
         {
            if (item.state != StateItem.Delete)
            {
               ListViewItem lvItem = new ListViewItem();
               lvItem.Text = item.deltaFreq.ToString(); // delta frequency
               lvItem.SubItems.Add(item.loss.ToString()); // рівень
               lvItem.Tag = item; // ParamsData з бази, якщо ID не null значить запис дійсно существує
               LsvDataContent.Items.Add(lvItem);
               listGraph.Add(item.deltaFreq, -item.loss);
            }
         }

         GraphPane myPane = GraphControl.GraphPane;
         myPane.CurveList.Clear();

         // Задаем название графика и сторон
         myPane.Title.Text = "";
         myPane.Title.IsVisible = false;
         myPane.Legend.IsVisible = false;
         myPane.XAxis.Title.Text = "dF";
         myPane.XAxis.Title.FontSpec.Size = 35;
         myPane.XAxis.Scale.FontSpec.Size = 35;
         myPane.YAxis.Title.Text = "Loss";
         myPane.YAxis.Title.FontSpec.Size = 35;
         myPane.YAxis.Scale.FontSpec.Size = 35;
         // --------------------------------
         LineItem myCurve = myPane.AddCurve("Sin", listGraph, Color.Red, SymbolType.Circle); // отрисовываем график
         GraphControl.BackColor = Color.Black;
         GraphControl.AxisChange();
         GraphControl.Refresh();

         if (LsvDataContent.Items.Count > 0)
         {
            try
            {
               CheckData();

               ParamsData data = (ParamsData)LsvDataContent.Items[0].Tag;
               double freqMin = _freq + data.deltaFreq;
               TbxMinFreq.Text = freqMin.ToString();
               _minFreq = freqMin;

               int lastItem = LsvDataContent.Items.Count - 1;
               data = (ParamsData)LsvDataContent.Items[lastItem].Tag;
               double freqMax = _freq + data.deltaFreq;
               TbxMaxFreq.Text = freqMax.ToString();
               _maxFreq = freqMax;
            }
            catch
            {
            	
            }
         }
      }      

      #endregion

      #region EVENTS

      private void LsvDataContent_SelectedIndexChanged(object sender, EventArgs e)
      {
         ListView.SelectedListViewItemCollection items = LsvDataContent.SelectedItems;
         if (items.Count > 0)
         {
            ParamsData dataSelect = (ParamsData)items[0].Tag;
            TbxDeltaFreq.Text = dataSelect.deltaFreq.ToString();
            TbxLoss.Text = dataSelect.loss.ToString();

            BtnDelete.Enabled = true;
            BtnUpdate.Enabled = true;
         }
         else
         {
            BtnDelete.Enabled = false;
            BtnUpdate.Enabled = false;
            TbxDeltaFreq.Text = "";
            TbxLoss.Text = "";
         }
      }

      private void BtnInsert_Click(object sender, EventArgs e)
      {
         if (TbxDeltaFreq.Text.Length > 0 && TbxLoss.Text.Length > 0)
         {
            ParamsData dataParams = new ParamsData();

            dataParams.deltaFreq = ConvertType.ToDouble(TbxDeltaFreq.Text, 0);
            dataParams.loss = ConvertType.ToDouble(TbxLoss.Text, 0);
            dataParams.state = StateItem.Insert;

            paramsDataList.Add(dataParams);
            DisplayedData(true); // обновити тільки дані ЛістВю               
         }
      }

      private void BtnUpdate_Click(object sender, EventArgs e)
      {
         if (TbxDeltaFreq.Text.Length > 0 && TbxLoss.Text.Length > 0)
         {
            ListView.SelectedListViewItemCollection items = LsvDataContent.SelectedItems;
            if (items.Count > 0)
            {
               ParamsData data = (ParamsData)items[0].Tag;
               int index = paramsDataList.IndexOf(data);
               if (index > -1)
               {
                  data = paramsDataList[index];
                  data.deltaFreq = ConvertType.ToDouble(TbxDeltaFreq.Text, 0);
                  data.loss = ConvertType.ToDouble(TbxLoss.Text, 0);
                  if (data.id != 0)
                     data.state = StateItem.Update;

                  paramsDataList[index] = data;
                  DisplayedData(true);
               }
            }
         }
      }

      private void BtnDelete_Click(object sender, EventArgs e)
      {
         ListView.SelectedListViewItemCollection items = LsvDataContent.SelectedItems;
         if (items.Count > 0)
         {
            ParamsData data = (ParamsData)items[0].Tag;
            int index = paramsDataList.IndexOf(data);
            if (index > -1)
            {
               data = paramsDataList[index];
               data.state = StateItem.Delete;
               paramsDataList[index] = data;
               DisplayedData(true);
            }
         }
      }

      // збереження результатів
      private void BtnOk_Click(object sender, EventArgs e)
      {
         if (TbxType.Text.Length == 0 || TbxName.Text.Length == 0 || 
             TbxMaxPower.Text.Length == 0 || TbxFreq.Text.Length == 0)
         {
            MessageBox.Show(CLocaliz.TxT("You must fill all fields with symbol '*'"), CLocaliz.Warning);
            return;
         }

         if (TbxType.Text.Length > 0 && TbxName.Text.Length > 0 && TbxMaxPower.Text.Length > 0 && TbxFreq.Text.Length > 0)
         {
             DisplayedData(true);
             using(Icsm.LisTransaction tr = new Icsm.LisTransaction())
             {
                 IMRecordset r = new IMRecordset("XNRFA_EXTERN_FILTERS", IMRecordset.Mode.ReadWrite);
                 r.Select("ID,TYPE,NAME,MAX_POWER,FREQ,MAX_FREQ,MIN_FREQ,NOTE,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE");

                 // перевіряємо чи це нова запис чи обновляємо существующу
                 if (!_newRecord)
                     r.SetWhere("ID", IMRecordset.Operation.Eq, _mainID);
                 else
                     r.SetWhere("ID", IMRecordset.Operation.Eq, 0);

                 r.Open();

                 // перевіряємо чи це нова запис чи обновляємо существующу
                 if (!_newRecord)
                 {
                     if (!r.IsEOF())
                         r.Edit();
                     else
                     {
                         r.Close();
                         r.Destroy();
                         return;
                     }
                 }
                 else
                     r.AddNew();

                 r.Put("TYPE", TbxType.Text);
                 r.Put("NAME", TbxName.Text);
                 r.Put("MAX_POWER", _maxPower);
                 r.Put("FREQ", _freq);

                 r.Put("MAX_FREQ", _maxFreq);
                 r.Put("MIN_FREQ", _minFreq);

                 r.Put("NOTE", TbxNote.Text);

                 if (!_newRecord)
                 {
                     r.Put("MODIFIED_BY", IM.ConnectedUser());
                     r.Put("MODIFIED_DATE", DateTime.Now);
                 }
                 else
                 {
                     _mainID = IM.AllocID("XNRFA_EXTERN_FILTERS", 1, -1);
                     r.Put("ID", _mainID);
                     r.Put("CREATED_BY", IM.ConnectedUser());
                     r.Put("CREATED_DATE", DateTime.Now);
                 }

                 r.Update();
                 r.Close();
                 r.Destroy();

                 if (!_newRecord)
                 {
                     r = new IMRecordset("XNRFA_FILTER_PARAM", IMRecordset.Mode.ReadWrite);
                     r.Select("ID,EXT_FILT_ID,DELTA_FREQ,LOSS");
                     r.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _mainID);

                     for (r.Open(); !r.IsEOF(); r.MoveNext())
                     {
                         int id = r.GetI("ID");

                         foreach (ParamsData data in paramsDataList)
                         {
                             if (data.id != 0)
                             {
                                 if (data.id.Equals(id))
                                 {
                                     if (data.state == StateItem.Update)
                                     {
                                         r.Edit();
                                         r.Put("DELTA_FREQ", data.deltaFreq);
                                         r.Put("LOSS", data.loss);
                                         r.Update();
                                         break;
                                     }

                                     if (data.state == StateItem.Delete)
                                     {
                                         r.Delete();
                                         break;
                                     }
                                 }
                             }
                         }
                     }
                     r.Close();
                     r.Destroy();
                 }

                 r = new IMRecordset("XNRFA_FILTER_PARAM", IMRecordset.Mode.ReadWrite);
                 r.Select("ID,EXT_FILT_ID,DELTA_FREQ,LOSS");
                 r.SetWhere("ID", IMRecordset.Operation.Eq, 0);
                 r.Open();

                 foreach (ParamsData data in paramsDataList)
                 {
                     if (data.state == StateItem.Insert)
                     {
                         int id = IM.AllocID("XNRFA_FILTER_PARAM", 1, -1);
                         r.AddNew();
                         r.Put("ID", id);
                         r.Put("EXT_FILT_ID", _mainID);
                         r.Put("DELTA_FREQ", data.deltaFreq);
                         r.Put("LOSS", data.loss);
                         r.Update();
                     }
                 }
                 r.Close();
                 r.Destroy();

                 tr.Commit();
                 this.Close();
             }
         }
      }

      private void Tbx_KeyPressOnDigit(object sender, KeyPressEventArgs e)
      {
         // Если это не цифра.
         if ((!Char.IsDigit(e.KeyChar)) && (e.KeyChar != 45) && (e.KeyChar != '\b'))
         {
            // Запрет на ввод более одной десятичной точки.
            TextBox tb = (TextBox)sender;
            char separator = provider.CurrencyGroupSeparator[0];
            if (e.KeyChar != separator || tb.Text.IndexOf(separator) != -1)
            {
               e.Handled = true;
            }
         }
      }

      private void BtnCancel_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      #endregion

      /// <summary>
      /// клас для сортування данних
      /// </summary>
      public class ParamsComparer : IComparer<ParamsData>
      {
         public int Compare(ParamsData x, ParamsData y)
         {
            int retval = x.deltaFreq.CompareTo(y.deltaFreq);

            if (retval != 0)
            {
               return retval;
            }
            else
            {
               return x.loss.CompareTo(y.loss);
            }
         }
      }

      private void TbxFreq_Leave(object sender, EventArgs e)
      {
         CheckData();
         DisplayedData(false);
      }

      private void TbxLoss_Enter(object sender, EventArgs e)
      {
         if (sender is TextBox)
         {
            TextBox tmpTextBox = sender as TextBox;
            tmpTextBox.SelectAll();
         }
      }
   }
}