﻿namespace XICSM.UcrfRfaNET.AdditionFillters
{
   partial class AdditionFilters
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.label14 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.TbxFreq = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.TbxMaxFreq = new System.Windows.Forms.TextBox();
         this.TbxMinFreq = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.TbxMaxPower = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.TbxName = new System.Windows.Forms.TextBox();
         this.TbxType = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.TbxNote = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.label12 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.BtnUpdate = new System.Windows.Forms.Button();
         this.BtnInsert = new System.Windows.Forms.Button();
         this.BtnDelete = new System.Windows.Forms.Button();
         this.TbxLoss = new System.Windows.Forms.TextBox();
         this.TbxDeltaFreq = new System.Windows.Forms.TextBox();
         this.label10 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.LsvDataContent = new System.Windows.Forms.ListView();
         this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
         this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
         this.GraphPanel = new System.Windows.Forms.Panel();
         this.BtnCancel = new System.Windows.Forms.Button();
         this.BtnOk = new System.Windows.Forms.Button();
         this.panel2 = new System.Windows.Forms.Panel();
         this.labelOutModifiedDate = new System.Windows.Forms.Label();
         this.labelOutModifiedBy = new System.Windows.Forms.Label();
         this.labelOutCreatedDate = new System.Windows.Forms.Label();
         this.labelOutCreatedBy = new System.Windows.Forms.Label();
         this.labelModyfiedDate = new System.Windows.Forms.Label();
         this.labelModifiedBy = new System.Windows.Forms.Label();
         this.labelCreatedDate = new System.Windows.Forms.Label();
         this.labelCreatedBy = new System.Windows.Forms.Label();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.panel2.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.label14);
         this.groupBox1.Controls.Add(this.label8);
         this.groupBox1.Controls.Add(this.label6);
         this.groupBox1.Controls.Add(this.label11);
         this.groupBox1.Controls.Add(this.TbxFreq);
         this.groupBox1.Controls.Add(this.label5);
         this.groupBox1.Controls.Add(this.TbxMaxFreq);
         this.groupBox1.Controls.Add(this.TbxMinFreq);
         this.groupBox1.Controls.Add(this.label4);
         this.groupBox1.Controls.Add(this.TbxMaxPower);
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.TbxName);
         this.groupBox1.Controls.Add(this.TbxType);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Location = new System.Drawing.Point(9, 10);
         this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
         this.groupBox1.Size = new System.Drawing.Size(333, 161);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "General parametrs";
         // 
         // label14
         // 
         this.label14.AutoSize = true;
         this.label14.Location = new System.Drawing.Point(294, 125);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(29, 13);
         this.label14.TabIndex = 18;
         this.label14.Text = "MHz";
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(294, 100);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(29, 13);
         this.label8.TabIndex = 17;
         this.label8.Text = "MHz";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(294, 76);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(28, 13);
         this.label6.TabIndex = 16;
         this.label6.Text = "dBm";
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(219, 125);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(10, 13);
         this.label11.TabIndex = 15;
         this.label11.Text = "-";
         // 
         // TbxFreq
         // 
         this.TbxFreq.Location = new System.Drawing.Point(158, 97);
         this.TbxFreq.Margin = new System.Windows.Forms.Padding(2);
         this.TbxFreq.Name = "TbxFreq";
         this.TbxFreq.Size = new System.Drawing.Size(134, 20);
         this.TbxFreq.TabIndex = 10;
         this.TbxFreq.Leave += new System.EventHandler(this.TbxFreq_Leave);
         this.TbxFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         this.TbxFreq.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(4, 100);
         this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(87, 13);
         this.label5.TabIndex = 9;
         this.label5.Text = "Main frequency *";
         // 
         // TbxMaxFreq
         // 
         this.TbxMaxFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.TbxMaxFreq.Enabled = false;
         this.TbxMaxFreq.Location = new System.Drawing.Point(234, 121);
         this.TbxMaxFreq.Margin = new System.Windows.Forms.Padding(2);
         this.TbxMaxFreq.Name = "TbxMaxFreq";
         this.TbxMaxFreq.Size = new System.Drawing.Size(58, 20);
         this.TbxMaxFreq.TabIndex = 8;
         this.TbxMaxFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.TbxMaxFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         // 
         // TbxMinFreq
         // 
         this.TbxMinFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.TbxMinFreq.Enabled = false;
         this.TbxMinFreq.Location = new System.Drawing.Point(158, 121);
         this.TbxMinFreq.Margin = new System.Windows.Forms.Padding(2);
         this.TbxMinFreq.Name = "TbxMinFreq";
         this.TbxMinFreq.Size = new System.Drawing.Size(58, 20);
         this.TbxMinFreq.TabIndex = 7;
         this.TbxMinFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.TbxMinFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(4, 123);
         this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(140, 13);
         this.label4.TabIndex = 6;
         this.label4.Text = "Range of normal frequencys";
         // 
         // TbxMaxPower
         // 
         this.TbxMaxPower.Location = new System.Drawing.Point(158, 73);
         this.TbxMaxPower.Margin = new System.Windows.Forms.Padding(2);
         this.TbxMaxPower.Name = "TbxMaxPower";
         this.TbxMaxPower.Size = new System.Drawing.Size(134, 20);
         this.TbxMaxPower.TabIndex = 5;
         this.TbxMaxPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         this.TbxMaxPower.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(4, 76);
         this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(91, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "Maximum Power *";
         // 
         // TbxName
         // 
         this.TbxName.Location = new System.Drawing.Point(158, 49);
         this.TbxName.Margin = new System.Windows.Forms.Padding(2);
         this.TbxName.Name = "TbxName";
         this.TbxName.Size = new System.Drawing.Size(165, 20);
         this.TbxName.TabIndex = 3;
         this.TbxName.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // TbxType
         // 
         this.TbxType.Location = new System.Drawing.Point(158, 26);
         this.TbxType.Margin = new System.Windows.Forms.Padding(2);
         this.TbxType.Name = "TbxType";
         this.TbxType.Size = new System.Drawing.Size(165, 20);
         this.TbxType.TabIndex = 2;
         this.TbxType.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(4, 52);
         this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(42, 13);
         this.label2.TabIndex = 1;
         this.label2.Text = "Name *";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(4, 28);
         this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(38, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Type *";
         // 
         // TbxNote
         // 
         this.TbxNote.Location = new System.Drawing.Point(9, 337);
         this.TbxNote.Margin = new System.Windows.Forms.Padding(2);
         this.TbxNote.Multiline = true;
         this.TbxNote.Name = "TbxNote";
         this.TbxNote.Size = new System.Drawing.Size(333, 82);
         this.TbxNote.TabIndex = 14;
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(6, 322);
         this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(30, 13);
         this.label7.TabIndex = 13;
         this.label7.Text = "Note";
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.label12);
         this.groupBox2.Controls.Add(this.label13);
         this.groupBox2.Controls.Add(this.BtnUpdate);
         this.groupBox2.Controls.Add(this.BtnInsert);
         this.groupBox2.Controls.Add(this.BtnDelete);
         this.groupBox2.Controls.Add(this.TbxLoss);
         this.groupBox2.Controls.Add(this.TbxDeltaFreq);
         this.groupBox2.Controls.Add(this.label10);
         this.groupBox2.Controls.Add(this.label9);
         this.groupBox2.Controls.Add(this.LsvDataContent);
         this.groupBox2.Location = new System.Drawing.Point(346, 11);
         this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
         this.groupBox2.Size = new System.Drawing.Size(348, 160);
         this.groupBox2.TabIndex = 1;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Filter\'s characteristics";
         // 
         // label12
         // 
         this.label12.AutoSize = true;
         this.label12.Location = new System.Drawing.Point(308, 58);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(20, 13);
         this.label12.TabIndex = 23;
         this.label12.Text = "dB";
         // 
         // label13
         // 
         this.label13.AutoSize = true;
         this.label13.Location = new System.Drawing.Point(308, 34);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(29, 13);
         this.label13.TabIndex = 22;
         this.label13.Text = "MHz";
         // 
         // BtnUpdate
         // 
         this.BtnUpdate.Enabled = false;
         this.BtnUpdate.Location = new System.Drawing.Point(285, 116);
         this.BtnUpdate.Margin = new System.Windows.Forms.Padding(2);
         this.BtnUpdate.Name = "BtnUpdate";
         this.BtnUpdate.Size = new System.Drawing.Size(53, 24);
         this.BtnUpdate.TabIndex = 21;
         this.BtnUpdate.Text = "Update";
         this.BtnUpdate.UseVisualStyleBackColor = true;
         this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
         // 
         // BtnInsert
         // 
         this.BtnInsert.Location = new System.Drawing.Point(228, 116);
         this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
         this.BtnInsert.Name = "BtnInsert";
         this.BtnInsert.Size = new System.Drawing.Size(53, 24);
         this.BtnInsert.TabIndex = 20;
         this.BtnInsert.Text = "Insert";
         this.BtnInsert.UseVisualStyleBackColor = true;
         this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
         // 
         // BtnDelete
         // 
         this.BtnDelete.Enabled = false;
         this.BtnDelete.Location = new System.Drawing.Point(172, 116);
         this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
         this.BtnDelete.Name = "BtnDelete";
         this.BtnDelete.Size = new System.Drawing.Size(53, 24);
         this.BtnDelete.TabIndex = 19;
         this.BtnDelete.Text = "Delete";
         this.BtnDelete.UseVisualStyleBackColor = true;
         this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
         // 
         // TbxLoss
         // 
         this.TbxLoss.Location = new System.Drawing.Point(228, 57);
         this.TbxLoss.Margin = new System.Windows.Forms.Padding(2);
         this.TbxLoss.Name = "TbxLoss";
         this.TbxLoss.Size = new System.Drawing.Size(75, 20);
         this.TbxLoss.TabIndex = 18;
         this.TbxLoss.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         this.TbxLoss.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // TbxDeltaFreq
         // 
         this.TbxDeltaFreq.Location = new System.Drawing.Point(228, 31);
         this.TbxDeltaFreq.Margin = new System.Windows.Forms.Padding(2);
         this.TbxDeltaFreq.Name = "TbxDeltaFreq";
         this.TbxDeltaFreq.Size = new System.Drawing.Size(75, 20);
         this.TbxDeltaFreq.TabIndex = 16;
         this.TbxDeltaFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
         this.TbxDeltaFreq.Enter += new System.EventHandler(this.TbxLoss_Enter);
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(169, 61);
         this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(29, 13);
         this.label10.TabIndex = 17;
         this.label10.Text = "Loss";
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(169, 34);
         this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(19, 13);
         this.label9.TabIndex = 16;
         this.label9.Text = "dF";
         // 
         // LsvDataContent
         // 
         this.LsvDataContent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
         this.LsvDataContent.FullRowSelect = true;
         this.LsvDataContent.GridLines = true;
         this.LsvDataContent.Location = new System.Drawing.Point(14, 25);
         this.LsvDataContent.Margin = new System.Windows.Forms.Padding(2);
         this.LsvDataContent.MultiSelect = false;
         this.LsvDataContent.Name = "LsvDataContent";
         this.LsvDataContent.Size = new System.Drawing.Size(143, 115);
         this.LsvDataContent.TabIndex = 0;
         this.LsvDataContent.UseCompatibleStateImageBehavior = false;
         this.LsvDataContent.View = System.Windows.Forms.View.Details;
         this.LsvDataContent.SelectedIndexChanged += new System.EventHandler(this.LsvDataContent_SelectedIndexChanged);
         // 
         // columnHeader1
         // 
         this.columnHeader1.Text = "dF";
         this.columnHeader1.Width = 56;
         // 
         // columnHeader2
         // 
         this.columnHeader2.Text = "Loss";
         // 
         // GraphPanel
         // 
         this.GraphPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.GraphPanel.Location = new System.Drawing.Point(9, 176);
         this.GraphPanel.Name = "GraphPanel";
         this.GraphPanel.Size = new System.Drawing.Size(685, 134);
         this.GraphPanel.TabIndex = 22;
         // 
         // BtnCancel
         // 
         this.BtnCancel.Location = new System.Drawing.Point(630, 440);
         this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
         this.BtnCancel.Name = "BtnCancel";
         this.BtnCancel.Size = new System.Drawing.Size(64, 19);
         this.BtnCancel.TabIndex = 25;
         this.BtnCancel.Text = "Cancel";
         this.BtnCancel.UseVisualStyleBackColor = true;
         this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
         // 
         // BtnOk
         // 
         this.BtnOk.Location = new System.Drawing.Point(545, 440);
         this.BtnOk.Margin = new System.Windows.Forms.Padding(2);
         this.BtnOk.Name = "BtnOk";
         this.BtnOk.Size = new System.Drawing.Size(65, 19);
         this.BtnOk.TabIndex = 22;
         this.BtnOk.Text = "Ok";
         this.BtnOk.UseVisualStyleBackColor = true;
         this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
         // 
         // panel2
         // 
         this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.panel2.Controls.Add(this.labelOutModifiedDate);
         this.panel2.Controls.Add(this.labelOutModifiedBy);
         this.panel2.Controls.Add(this.labelOutCreatedDate);
         this.panel2.Controls.Add(this.labelOutCreatedBy);
         this.panel2.Controls.Add(this.labelModyfiedDate);
         this.panel2.Controls.Add(this.labelModifiedBy);
         this.panel2.Controls.Add(this.labelCreatedDate);
         this.panel2.Controls.Add(this.labelCreatedBy);
         this.panel2.Location = new System.Drawing.Point(347, 337);
         this.panel2.Name = "panel2";
         this.panel2.Size = new System.Drawing.Size(347, 82);
         this.panel2.TabIndex = 16;
         // 
         // labelOutModifiedDate
         // 
         this.labelOutModifiedDate.AutoSize = true;
         this.labelOutModifiedDate.Location = new System.Drawing.Point(167, 59);
         this.labelOutModifiedDate.Name = "labelOutModifiedDate";
         this.labelOutModifiedDate.Size = new System.Drawing.Size(25, 13);
         this.labelOutModifiedDate.TabIndex = 7;
         this.labelOutModifiedDate.Text = "      ";
         // 
         // labelOutModifiedBy
         // 
         this.labelOutModifiedBy.AutoSize = true;
         this.labelOutModifiedBy.Location = new System.Drawing.Point(167, 41);
         this.labelOutModifiedBy.Name = "labelOutModifiedBy";
         this.labelOutModifiedBy.Size = new System.Drawing.Size(25, 13);
         this.labelOutModifiedBy.TabIndex = 6;
         this.labelOutModifiedBy.Text = "      ";
         // 
         // labelOutCreatedDate
         // 
         this.labelOutCreatedDate.AutoSize = true;
         this.labelOutCreatedDate.Location = new System.Drawing.Point(167, 22);
         this.labelOutCreatedDate.Name = "labelOutCreatedDate";
         this.labelOutCreatedDate.Size = new System.Drawing.Size(25, 13);
         this.labelOutCreatedDate.TabIndex = 5;
         this.labelOutCreatedDate.Text = "      ";
         // 
         // labelOutCreatedBy
         // 
         this.labelOutCreatedBy.AutoSize = true;
         this.labelOutCreatedBy.Location = new System.Drawing.Point(167, 4);
         this.labelOutCreatedBy.Name = "labelOutCreatedBy";
         this.labelOutCreatedBy.Size = new System.Drawing.Size(25, 13);
         this.labelOutCreatedBy.TabIndex = 4;
         this.labelOutCreatedBy.Text = "      ";
         // 
         // labelModyfiedDate
         // 
         this.labelModyfiedDate.AutoSize = true;
         this.labelModyfiedDate.Location = new System.Drawing.Point(6, 59);
         this.labelModyfiedDate.Name = "labelModyfiedDate";
         this.labelModyfiedDate.Size = new System.Drawing.Size(91, 13);
         this.labelModyfiedDate.TabIndex = 3;
         this.labelModyfiedDate.Text = "MODIFIED DATE";
         // 
         // labelModifiedBy
         // 
         this.labelModifiedBy.AutoSize = true;
         this.labelModifiedBy.Location = new System.Drawing.Point(6, 41);
         this.labelModifiedBy.Name = "labelModifiedBy";
         this.labelModifiedBy.Size = new System.Drawing.Size(76, 13);
         this.labelModifiedBy.TabIndex = 2;
         this.labelModifiedBy.Text = "MODIFIED BY";
         // 
         // labelCreatedDate
         // 
         this.labelCreatedDate.AutoSize = true;
         this.labelCreatedDate.Location = new System.Drawing.Point(6, 22);
         this.labelCreatedDate.Name = "labelCreatedDate";
         this.labelCreatedDate.Size = new System.Drawing.Size(90, 13);
         this.labelCreatedDate.TabIndex = 1;
         this.labelCreatedDate.Text = "CREATED DATE";
         // 
         // labelCreatedBy
         // 
         this.labelCreatedBy.AutoSize = true;
         this.labelCreatedBy.Location = new System.Drawing.Point(7, 4);
         this.labelCreatedBy.Name = "labelCreatedBy";
         this.labelCreatedBy.Size = new System.Drawing.Size(75, 13);
         this.labelCreatedBy.TabIndex = 0;
         this.labelCreatedBy.Text = "CREATED BY";
         // 
         // AdditionFilters
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(705, 475);
         this.Controls.Add(this.GraphPanel);
         this.Controls.Add(this.TbxNote);
         this.Controls.Add(this.panel2);
         this.Controls.Add(this.label7);
         this.Controls.Add(this.BtnCancel);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.BtnOk);
         this.Controls.Add(this.groupBox1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Margin = new System.Windows.Forms.Padding(2);
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "AdditionFilters";
         this.ShowInTaskbar = false;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "External filter";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.panel2.ResumeLayout(false);
         this.panel2.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox TbxMaxPower;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox TbxName;
      private System.Windows.Forms.TextBox TbxType;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox TbxNote;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox TbxFreq;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox TbxMaxFreq;
      private System.Windows.Forms.TextBox TbxMinFreq;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.ListView LsvDataContent;
      private System.Windows.Forms.Button BtnInsert;
      private System.Windows.Forms.Button BtnDelete;
      private System.Windows.Forms.TextBox TbxLoss;
      private System.Windows.Forms.TextBox TbxDeltaFreq;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Button BtnUpdate;
      private System.Windows.Forms.Button BtnCancel;
      private System.Windows.Forms.Button BtnOk;
      private System.Windows.Forms.ColumnHeader columnHeader1;
      private System.Windows.Forms.ColumnHeader columnHeader2;
      private System.Windows.Forms.Panel GraphPanel;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.Label labelCreatedBy;
      private System.Windows.Forms.Label labelOutModifiedBy;
      private System.Windows.Forms.Label labelOutCreatedDate;
      private System.Windows.Forms.Label labelOutCreatedBy;
      private System.Windows.Forms.Label labelModyfiedDate;
      private System.Windows.Forms.Label labelModifiedBy;
      private System.Windows.Forms.Label labelCreatedDate;
      private System.Windows.Forms.Label labelOutModifiedDate;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label14;
   }
}