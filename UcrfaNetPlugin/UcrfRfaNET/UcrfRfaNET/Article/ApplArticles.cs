﻿using System;
using ICSM;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.UtilityClass;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Article
{
    internal class ApplArticles : NotifyPropertyChanged
    {
        public static ArticlesManager artManager = new ArticlesManager();
        public int ApplID;
        //======================================
        public const string FieldArticleId1 = "ArticleId1";
        public const string FieldTableName = "TableName";
        private int _articleId1;
        public int ArticleId1
        {
            get { return GetThreadSafeValue(ref _articleId1); }
            set
            {
                SetThreadSafeValue(ref _articleId1, value, FieldArticleId1);
                ClearConfirmArticle();
            }
        }

        private string _TableName;
        public string TableName
        {
            get { return GetThreadSafeValue(ref _TableName); }
            set
            {
                SetThreadSafeValue(ref _TableName, value, FieldTableName);
            }
        }

        public const string FieldWorkCount1 = "WorkCount1";
        private int _workCount1;
        public int WorkCount1
        {
            get { return GetThreadSafeValue(ref _workCount1); }
            set
            {
                SetThreadSafeValue(ref _workCount1, value, FieldWorkCount1);
                ClearConfirmArticle();
            }
        }

        public const string FieldArticleId2 = "ArticleId2";
        private int _articleId2;
        public int ArticleId2
        {
            get { return GetThreadSafeValue(ref _articleId2); }
            set
            {
                SetThreadSafeValue(ref _articleId2, value, FieldArticleId2);
                ClearConfirmArticle();
            }
        }

        public const string FieldWorkCount2 = "WorkCount2";
        private int _workCount2;
        public int WorkCount2
        {
            get { return GetThreadSafeValue(ref _workCount2); }
            set
            {
                SetThreadSafeValue(ref _workCount2, value, FieldWorkCount2);
                ClearConfirmArticle();
            }
        }
        //--
        public const string FieldIsConfirmed = "IsConfirmed";
        private bool _isConfirmed = false;
        public bool IsConfirmed
        {
            get { return GetThreadSafeValue(ref _isConfirmed); }
            set { SetThreadSafeValue(ref _isConfirmed, value, FieldIsConfirmed); }
        }
        //--
        public const string FieldConfirmedBy = "ConfirmedBy";
        private string _confirmedBy = "";
        public string ConfirmedBy
        {
            get { return GetThreadSafeValue(ref _confirmedBy); }
            set { SetThreadSafeValue(ref _confirmedBy, value, FieldConfirmedBy); }
        }
        //--
        public const string FieldConfirmedDate = "ConfirmedDate";
        private DateTime _confirmedDate = IM.NullT;
        public DateTime ConfirmedDate
        {
            get { return GetThreadSafeValue(ref _confirmedDate); }
            set { SetThreadSafeValue(ref _confirmedDate, value, FieldConfirmedDate); }
        }
        /// <summary>
        /// Загрузка статьи
        /// </summary>
        /// <param name="applId">ID заявки</param>
        public void Load(int applId)
        {
            ApplID = applId;
            using (LisProgressBar pb = new LisProgressBar("Loading article..."))
            {
                pb.SetBig("Loading current article");
                using (Icsm.LisRecordSet rsAppl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                {
                    rsAppl.Select("ID,PRICE_ID,WORKS_COUNT,PRICE_ID2,WORKS_COUNT2,OBJ_TABLE");
                    rsAppl.Select("IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                    rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                    rsAppl.Open();
                    if (!rsAppl.IsEOF())
                    {
                        ArticleId1 = rsAppl.GetI("PRICE_ID");
                        ArticleId2 = rsAppl.GetI("PRICE_ID2");
                        int workCountTmp = rsAppl.GetI("WORKS_COUNT");
                        WorkCount1 = (workCountTmp == IM.NullI) ? 0 : workCountTmp;
                        workCountTmp = rsAppl.GetI("WORKS_COUNT2");
                        WorkCount2 = (workCountTmp == IM.NullI) ? 0 : workCountTmp;
                        IsConfirmed = (rsAppl.GetI("IS_CONFIRMED") == 1) ? true : false;
                        ConfirmedBy = rsAppl.GetS("CONFIRMED_BY");
                        ConfirmedDate = rsAppl.GetT("CONFIRMED_DATE");
                        TableName = rsAppl.GetS("OBJ_TABLE");
                    }
                }
                IsChanged = false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        public void Save_SQL(int applId)
        {
            try  {
                string SQL = string.Format("UPDATE %XNRFA_APPL SET PRICE_ID={0},PRICE_ID2={1},WORKS_COUNT={2},WORKS_COUNT2={3},IS_CONFIRMED={4},CONFIRMED_BY='{5}',CONFIRMED_DATE=TO_DATE('{6}', 'dd.mm.yyyy') WHERE ID = {7}", ArticleId1 != IM.NullI ? ArticleId1.ToString() : "NULL", ArticleId2 != IM.NullI ? ArticleId2.ToString() : "NULL", (WorkCount1 == 0) ? "NULL" : WorkCount1.ToString(), (WorkCount2 == 0) ? "NULL" : WorkCount2.ToString(), IsConfirmed ? 1 : 0, ConfirmedBy, ConfirmedDate.ToShortDateString(), applId);
                IM.Execute(SQL);
            }
            catch (Exception ex) {
                CLogs.WriteError(ELogsWhat.Appl, " Помилка при підтвердженні статті " + ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Сохранение статьи
        /// </summary>
        /// <param name="applId">ID заявки</param>
        public void Save(int applId)
        {
            using (LisProgressBar pb = new LisProgressBar("Saving article..."))
            {
                pb.SetBig("Saving article...");
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    using (Icsm.LisRecordSet rsAppl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadWrite))
                    {
                        rsAppl.Select("ID,PRICE_ID,WORKS_COUNT,PRICE_ID2,WORKS_COUNT2");
                        rsAppl.Select("IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                        rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                        rsAppl.Open();
                        if (!rsAppl.IsEOF())
                        {
                            rsAppl.Edit();
                            rsAppl.Put("PRICE_ID", ArticleId1);
                            rsAppl.Put("PRICE_ID2", ArticleId2);
                            rsAppl.Put("WORKS_COUNT", (WorkCount1 == 0) ? IM.NullI : WorkCount1);
                            rsAppl.Put("WORKS_COUNT2", (WorkCount2 == 0) ? IM.NullI : WorkCount2);
                            rsAppl.Put("IS_CONFIRMED", IsConfirmed ? 1 : 0);
                            rsAppl.Put("CONFIRMED_BY", ConfirmedBy);
                            rsAppl.Put("CONFIRMED_DATE", ConfirmedDate);
                            rsAppl.Update();
                        }
                    }
                    tr.Commit();
                    IsChanged = false;
                }
            }
        }

       public void ClearHide(int applId)
        {
            using (LisProgressBar pb = new LisProgressBar("Saving article..."))
            {
                pb.SetBig("Saving article...");
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    using (Icsm.LisRecordSet rsAppl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadWrite))
                    {
                        rsAppl.Select("ID,PRICE_ID,WORKS_COUNT,PRICE_ID2,WORKS_COUNT2");
                        rsAppl.Select("IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                        rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                        rsAppl.Open();
                        if (!rsAppl.IsEOF())
                        {
                            rsAppl.Edit();
                            rsAppl.Put("PRICE_ID", IM.NullI);
                            rsAppl.Put("PRICE_ID2", IM.NullI);
                            rsAppl.Put("WORKS_COUNT", IM.NullI);
                            rsAppl.Put("WORKS_COUNT2", IM.NullI);
                            rsAppl.Put("IS_CONFIRMED", 0);
                            rsAppl.Put("CONFIRMED_BY", ConfirmedBy);
                            rsAppl.Put("CONFIRMED_DATE", ConfirmedDate);
                            rsAppl.Update();
                        }
                    }
                    tr.Commit();
                }
            }
        }
        /// <summary>
        /// Очищаем поля подтверждения статьи
        /// </summary>
        public void ClearConfirmArticle()
        {
            IsConfirmed = false;
            ConfirmedBy = "";
            ConfirmedDate = IM.NullT;
        }
        /// <summary>
        /// устаналиваем поля подтверждения статьи
        /// </summary>
        public void SetConfirmArticle()
        {
            if (ArticleLib.ArticleFunc.GetStatusForCodePriceID(ArticleId1) == false)
            {
                string messError = string.Format(CLocaliz.TxT("The indicated fare is not available, select another fare!"), Environment.NewLine);
                MessageBox.Show(messError, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                IsConfirmed=false;
                ConfirmedBy = ""; ConfirmedDate = IM.NullT;
                return;
            }

            if (CanConfirmArticle() == false)
            {
                string messError = string.Format("Стаття не може бути підтверджена для заявки ID:{0}{1}Продовжити?", ApplID, Environment.NewLine);
                if (MessageBox.Show(messError, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    IsConfirmed = false;
                ConfirmedBy = ""; ConfirmedDate = IM.NullT;
                return;
            }

            ConfirmedBy = CUsers.ConnectedUser();
            ConfirmedDate = DateTime.Now;
            IsConfirmed = true;
           
        }
        /// <summary>
        /// Определяет возможность подтвердить статью
        /// </summary>
        public bool CanConfirmArticle()
        {
            bool canConfirm;
            if (ArticleId1 != IM.NullI && (WorkCount1 != IM.NullI) && (WorkCount1 != 0))
            {
                canConfirm = true;
            }
            else
            {
                canConfirm = false;
            }
            bool canConfirm2 = true;
            if (ArticleId2 != IM.NullI)
                if ((WorkCount2 != IM.NullI) && (WorkCount2 == 0))
                    canConfirm2 = false;
            return canConfirm && canConfirm2;
            
            /*
            bool canConfirm = false;
            canConfirm = ((ArticleId1 != IM.NullI) && (ArticleId1 < 2147483647) && (WorkCount1 != IM.NullI) && (WorkCount1 < 2147483647));
            bool canConfirm2 = false;
            canConfirm2 = ((ArticleId2 != IM.NullI) && (ArticleId2 < 2147483647) && (WorkCount2 != IM.NullI) && (WorkCount2 < 2147483647));

            return canConfirm && canConfirm2;      
             */ 
        }


        /// <summary>
        /// Получить подсказку по статье
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        public static int GetArticle(int appId)
        {
            string GetArticle = "";
            int priceId = -1;
            artManager.InitArticles();
            using (BaseAppClass app = BaseAppClass.GetBaseAppl(appId))
            {
                if (app != null)
                {
                    if ((app.radioTech == CRadioTech.YKX) || (app.radioTech == CRadioTech.ЦУКХ))
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                            double value = IM.NullD;
                            LimitType type = mobApp.GetAppTypeAndLimitValue(out value);
                            if (type == LimitType.noLimit)
                            {
                                GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                            }
                            else
                            {
                                GetArticle = artManager.GetArticle(app.radioTech, type, value);
                            }
                        }
                    }
                    else if (app.radioTech == CRadioTech.RBSS)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                            string cl = mobApp.GetClass();
                            if (cl == "FC")
                                GetArticle=artManager.GetArticle(app.radioTech, LimitType.coastStaLimit, IM.NullD);
                            else if (cl == "MS")
                            {
                                GetArticle=artManager.GetArticle(app.radioTech, LimitType.seaStaLimit, IM.NullD);
                            }
                            
                        }
                        else if (app != null && app.appType == AppType.AppAR_3)
                        {
                           GetArticle = artManager.GetArticle(app.radioTech, LimitType.coastStaLimit, IM.NullD);
                        }
                    }
                    else if (app.radioTech == CRadioTech.KX)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                            double pow = mobApp.GetPower();
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow);
                        }
                    }
                    else if (app.radioTech == CRadioTech.PADG)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.TETRA || app.radioTech == CRadioTech.TRUNK)
                    {
                        if ((app != null) && ((app.appType == AppType.AppTR) || (app.appType == AppType.AppAR_3)))
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }

                    else if (app.radioTech == CRadioTech.GSM_900 || app.radioTech == CRadioTech.GSM_1800)
                    {
                        if (app != null && app.appType == AppType.AppRR)
                        {
                            MobStaApp mobApp = (MobStaApp)app;
                            LimitType type = mobApp.GetAppTypeAndLimitValueGSM();
                            if (type != LimitType.noLimit)
                            {
                                GetArticle = artManager.GetArticle(app.radioTech, type, IM.NullD);
                            }
                            else
                            {
                             
                            }
                        }
                    }
                    else if (app.radioTech == CRadioTech.UMTS)
                    {
                        if (app != null && app.appType == AppType.AppRR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.CDMA_450 || app.radioTech == CRadioTech.CDMA_800)
                    {
                        if (app != null && app.appType == AppType.AppRR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.DAMPS)
                    {
                        if (app != null && app.appType == AppType.AppRR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.DECT)
                    {
                        if (app != null && app.appType == AppType.AppRR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.PD)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.SHR || app.radioTech == CRadioTech.MsR || app.radioTech == CRadioTech.MmR)
                    {
                        if (app != null && app.appType == AppType.AppBS)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                  
                    else if (app.radioTech == CRadioTech.RS)
                    {
                        if (app != null && app.appType == AppType.AppRS)
                        {
                            MicrowaveApp mobApp = (MicrowaveApp)app;
                            double fr = mobApp.st1[0].NomFreq;
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.freqLimit, fr);
                        }
                    }
                    else if (app.radioTech == CRadioTech.AAB)
                    {
                        if (app != null && app.appType == AppType.AppR2)
                        {
                            FmAnalogApp mobApp = (FmAnalogApp)app;
                            double pow = mobApp.GetPower();
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow);
                        }
                    }
                    else if (app.radioTech == CRadioTech.TDAB)
                    {
                        if (app != null && app.appType == AppType.AppR2d)
                        {
                            FmDigitalApp mobApp = (FmDigitalApp)app;
                            double pow = mobApp.GetPower();
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow);
                        }
                    }
                    else if (app.radioTech == CRadioTech.ATM)
                    {
                        if (app != null && app.appType == AppType.AppTV2)
                        {
                            CTvAnalog mobApp = (CTvAnalog)app;
                            double pow = mobApp.GetPower();
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow);
                        }
                    }
                    else if ((app.radioTech == CRadioTech.DVBT) || (app.radioTech == CRadioTech.DVBT2))
                    {
                        if (app != null && app.appType == AppType.AppTV2d)
                        {
                            CTvDigital mobApp = (CTvDigital)app;
                            double pow = mobApp.GetPower();
                            GetArticle = artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow);
                        }
                    }
                    else if ((app.radioTech == CRadioTech.RPATL) || (app.radioTech == CRadioTech.RPS))
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.ROPS)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else if (app.radioTech == CRadioTech.RRK)
                    {
                        if (app != null && app.appType == AppType.AppTR)
                        {
                            GetArticle = artManager.GetArticleByRadioTech(app.radioTech);
                        }
                    }
                    else  if (app.radioTech == CRadioTech.ZS)
                    {
                        GetArticle = ArticleLib.ArticleFunc.GetNameArticle(ArticleLib.ArticleUpdate.GetArticleSimple(appId));
                    }
                    else if (app.radioTech == CRadioTech.VP)
                    {
                        GetArticle = ArticleLib.ArticleFunc.GetNameArticle(ArticleLib.ArticleUpdate.GetArticleSimple(appId));
                    }
                    else
                    {
                        CLogs.WriteWarning(ELogsWhat.AutoSetArticle, string.Format("Can't get article for standard {0}", app.radioTech));
                    }
                }


                if (app.arts.ContainsKey(GetArticle))
                    priceId = app.arts[GetArticle];
            else
                priceId = IM.NullI;
            }

            return priceId;
        }

        /// <summary>
        /// Получить подсказку по количеству работ
        /// </summary>
        public static int GetWorkCount(int appId)
        {
          int retVal = 0;
         

          using (LisProgressBar pb = new LisProgressBar("Count updating 1"))
          {
              IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
              r.Select("ID,OBJ_ID1,OBJ_TABLE");
              //r.SetWhere("WORKS_COUNT", IMRecordset.Operation.Null, IM.NullI);
              r.SetWhere("PRICE_ID", IMRecordset.Operation.NotNull, IM.NullI);
              //r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
              r.SetWhere("ID", IMRecordset.Operation.Eq, appId);

              pb.SetBig("Works' count updating");
              try
              {
                  const int maxCounter = 50000;
                  int recCount = 0;
                  int recId = 0;
                  string tableName = "";
                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                      if (pb.UserCanceled())
                          break;
                      recId = r.GetI("OBJ_ID1");
                      tableName = r.GetS("OBJ_TABLE");

                      pb.SetBig(string.Format("{0}:{1}", tableName, recId));
                      pb.SetSmall(recCount++);
                      pb.SetProgress(recCount, maxCounter);
                      retVal = ArticleLib.CountWork.CountWorkAppl(appId);
                  }
              }
              finally
              {
                  if (r.IsOpen())
                      r.Close();
                  r.Destroy();
              }
          }
          if ((retVal == IM.NullI) || (retVal == 0))
          {
              using (LisProgressBar pb = new LisProgressBar("Count updating 2"))
              {
                  IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                  r.Select("ID,WORKS_COUNT,OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                  //r.SetWhere("WORKS_COUNT", IMRecordset.Operation.Null, IM.NullI);
                  //r.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                  r.SetWhere("PRICE_ID", IMRecordset.Operation.NotNull, IM.NullI);
                  //r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
                  r.SetWhere("ID", IMRecordset.Operation.Eq, appId);

                  pb.SetBig("Works' count updating");
                  try
                  {
                      int recCount = 0;
                      for (r.Open(); !r.IsEOF(); r.MoveNext())
                      {
                          if (pb.UserCanceled())
                              continue;
                          pb.SetSmall(++recCount);
                          DateTime s1 = IM.NullT;
                          List<int> ID = new List<int>();
                          for (int i = 1; i <= 6; ++i)
                          {
                              if (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI)
                                  ID.Add(r.GetI("OBJ_ID" + i.ToString()));
                          }
                          string s2 = "";
                          string s3 = "";
                          try
                          {
                              int wrkCount = BaseAppClass.GetDataForURCM(r.GetS("OBJ_TABLE"), ID, null, ref s2, ref s3);
                              if ((wrkCount != 0) && (wrkCount != IM.NullI))
                              {
                                  retVal = wrkCount;
                              }
                          }
                          catch (Exception e)
                          {
                              CLogs.WriteError(ELogsWhat.Appl, e);
                          }
                      }
                  }
                  finally
                  {
                      r.Close();
                      r.Destroy();
                  }
              }
          }

         

          return retVal;

        }


        public static int GetWorkCountExtended(int appId)
        {
            int retVal = 0;


            using (LisProgressBar pb = new LisProgressBar("Count updating 1"))
            {
                IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                r.Select("ID,OBJ_ID1,OBJ_TABLE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                pb.SetBig("Works' count updating");
                try
                {
                    const int maxCounter = 50000;
                    int recCount = 0;
                    int recId = 0;
                    string tableName = "";
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        if (pb.UserCanceled())
                            break;
                        recId = r.GetI("OBJ_ID1");
                        tableName = r.GetS("OBJ_TABLE");

                        pb.SetBig(string.Format("{0}:{1}", tableName, recId));
                        pb.SetSmall(recCount++);
                        pb.SetProgress(recCount, maxCounter);
                        retVal = ArticleLib.CountWork.CountWorkAppl(appId);
                    }
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
            }
            if ((retVal == IM.NullI) || (retVal == 0))
            {
                using (LisProgressBar pb = new LisProgressBar("Count updating 2"))
                {
                    IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                    r.Select("ID,WORKS_COUNT,OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                    pb.SetBig("Works' count updating");
                    try
                    {
                        int recCount = 0;
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            if (pb.UserCanceled())
                                continue;
                            pb.SetSmall(++recCount);
                            DateTime s1 = IM.NullT;
                            List<int> ID = new List<int>();
                            for (int i = 1; i <= 6; ++i)
                            {
                                if (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI)
                                    ID.Add(r.GetI("OBJ_ID" + i.ToString()));
                            }
                            string s2 = "";
                            string s3 = "";
                            try
                            {
                                int wrkCount = BaseAppClass.GetDataForURCM(r.GetS("OBJ_TABLE"), ID, null, ref s2, ref s3);
                                if ((wrkCount != 0) && (wrkCount != IM.NullI))
                                {
                                    retVal = wrkCount;
                                }
                            }
                            catch (Exception e)
                            {
                                CLogs.WriteError(ELogsWhat.Appl, e);
                            }
                        }
                    }
                    finally
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
            }



            return retVal;

        }



    }
}
