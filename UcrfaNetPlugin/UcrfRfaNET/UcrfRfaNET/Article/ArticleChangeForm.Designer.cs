﻿namespace XICSM.UcrfRfaNET.Article
{
    partial class ArticleChangeForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numWorkCount1 = new System.Windows.Forms.NumericUpDown();
            this.btnWorkCountUpdate1 = new System.Windows.Forms.Button();
            this.tbWorkCountSuggest1 = new System.Windows.Forms.TextBox();
            this.btnArticleUpdate1 = new System.Windows.Forms.Button();
            this.tbArticleSuggest1 = new System.Windows.Forms.TextBox();
            this.cbArticle1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numWorkCount2 = new System.Windows.Forms.NumericUpDown();
            this.btnWorkCountUpdate2 = new System.Windows.Forms.Button();
            this.tbWorkCountSuggest2 = new System.Windows.Forms.TextBox();
            this.btnArticleUpdate2 = new System.Windows.Forms.Button();
            this.tbArticleSuggest2 = new System.Windows.Forms.TextBox();
            this.cbArticle2 = new System.Windows.Forms.ComboBox();
            this.btnConfirmArticle = new System.Windows.Forms.Button();
            this.gbConfirmArticle = new System.Windows.Forms.GroupBox();
            this.lblDateConfirmedArticle = new System.Windows.Forms.Label();
            this.lblUserConfirmedArticle = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount2)).BeginInit();
            this.gbConfirmArticle.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(369, 115);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(93, 25);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Вибрати";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(488, 115);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 25);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Відмінити";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numWorkCount1);
            this.groupBox1.Controls.Add(this.btnWorkCountUpdate1);
            this.groupBox1.Controls.Add(this.tbWorkCountSuggest1);
            this.groupBox1.Controls.Add(this.btnArticleUpdate1);
            this.groupBox1.Controls.Add(this.tbArticleSuggest1);
            this.groupBox1.Controls.Add(this.cbArticle1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 91);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Стаття та кіл-сть робіт №1";
            // 
            // numWorkCount1
            // 
            this.numWorkCount1.Location = new System.Drawing.Point(6, 49);
            this.numWorkCount1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numWorkCount1.Name = "numWorkCount1";
            this.numWorkCount1.Size = new System.Drawing.Size(121, 20);
            this.numWorkCount1.TabIndex = 3;
            // 
            // btnWorkCountUpdate1
            // 
            this.btnWorkCountUpdate1.Location = new System.Drawing.Point(142, 46);
            this.btnWorkCountUpdate1.Name = "btnWorkCountUpdate1";
            this.btnWorkCountUpdate1.Size = new System.Drawing.Size(38, 23);
            this.btnWorkCountUpdate1.TabIndex = 4;
            this.btnWorkCountUpdate1.Text = "<-";
            this.btnWorkCountUpdate1.UseVisualStyleBackColor = true;
            this.btnWorkCountUpdate1.Click += new System.EventHandler(this.btnWorkCountUpdate1_Click);
            // 
            // tbWorkCountSuggest1
            // 
            this.tbWorkCountSuggest1.Location = new System.Drawing.Point(195, 46);
            this.tbWorkCountSuggest1.Name = "tbWorkCountSuggest1";
            this.tbWorkCountSuggest1.ReadOnly = true;
            this.tbWorkCountSuggest1.Size = new System.Drawing.Size(80, 20);
            this.tbWorkCountSuggest1.TabIndex = 5;
            // 
            // btnArticleUpdate1
            // 
            this.btnArticleUpdate1.Location = new System.Drawing.Point(142, 19);
            this.btnArticleUpdate1.Name = "btnArticleUpdate1";
            this.btnArticleUpdate1.Size = new System.Drawing.Size(38, 23);
            this.btnArticleUpdate1.TabIndex = 1;
            this.btnArticleUpdate1.Text = "<-";
            this.btnArticleUpdate1.UseVisualStyleBackColor = true;
            this.btnArticleUpdate1.Click += new System.EventHandler(this.btnArticleUpdate1_Click);
            // 
            // tbArticleSuggest1
            // 
            this.tbArticleSuggest1.Location = new System.Drawing.Point(195, 19);
            this.tbArticleSuggest1.Name = "tbArticleSuggest1";
            this.tbArticleSuggest1.ReadOnly = true;
            this.tbArticleSuggest1.Size = new System.Drawing.Size(80, 20);
            this.tbArticleSuggest1.TabIndex = 2;
            // 
            // cbArticle1
            // 
            this.cbArticle1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArticle1.FormattingEnabled = true;
            this.cbArticle1.Location = new System.Drawing.Point(6, 19);
            this.cbArticle1.Name = "cbArticle1";
            this.cbArticle1.Size = new System.Drawing.Size(121, 21);
            this.cbArticle1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numWorkCount2);
            this.groupBox2.Controls.Add(this.btnWorkCountUpdate2);
            this.groupBox2.Controls.Add(this.tbWorkCountSuggest2);
            this.groupBox2.Controls.Add(this.btnArticleUpdate2);
            this.groupBox2.Controls.Add(this.tbArticleSuggest2);
            this.groupBox2.Controls.Add(this.cbArticle2);
            this.groupBox2.Location = new System.Drawing.Point(299, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(281, 91);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Стаття та кіл-сть робіт №2";
            // 
            // numWorkCount2
            // 
            this.numWorkCount2.Location = new System.Drawing.Point(6, 49);
            this.numWorkCount2.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numWorkCount2.Name = "numWorkCount2";
            this.numWorkCount2.Size = new System.Drawing.Size(121, 20);
            this.numWorkCount2.TabIndex = 3;
            // 
            // btnWorkCountUpdate2
            // 
            this.btnWorkCountUpdate2.Location = new System.Drawing.Point(142, 46);
            this.btnWorkCountUpdate2.Name = "btnWorkCountUpdate2";
            this.btnWorkCountUpdate2.Size = new System.Drawing.Size(38, 23);
            this.btnWorkCountUpdate2.TabIndex = 4;
            this.btnWorkCountUpdate2.Text = "<-";
            this.btnWorkCountUpdate2.UseVisualStyleBackColor = true;
            this.btnWorkCountUpdate2.Click += new System.EventHandler(this.btnWorkCountUpdate2_Click);
            // 
            // tbWorkCountSuggest2
            // 
            this.tbWorkCountSuggest2.Location = new System.Drawing.Point(195, 46);
            this.tbWorkCountSuggest2.Name = "tbWorkCountSuggest2";
            this.tbWorkCountSuggest2.ReadOnly = true;
            this.tbWorkCountSuggest2.Size = new System.Drawing.Size(80, 20);
            this.tbWorkCountSuggest2.TabIndex = 5;
            // 
            // btnArticleUpdate2
            // 
            this.btnArticleUpdate2.Location = new System.Drawing.Point(142, 19);
            this.btnArticleUpdate2.Name = "btnArticleUpdate2";
            this.btnArticleUpdate2.Size = new System.Drawing.Size(38, 23);
            this.btnArticleUpdate2.TabIndex = 1;
            this.btnArticleUpdate2.Text = "<-";
            this.btnArticleUpdate2.UseVisualStyleBackColor = true;
            this.btnArticleUpdate2.Click += new System.EventHandler(this.btnArticleUpdate2_Click);
            // 
            // tbArticleSuggest2
            // 
            this.tbArticleSuggest2.Location = new System.Drawing.Point(195, 19);
            this.tbArticleSuggest2.Name = "tbArticleSuggest2";
            this.tbArticleSuggest2.ReadOnly = true;
            this.tbArticleSuggest2.Size = new System.Drawing.Size(80, 20);
            this.tbArticleSuggest2.TabIndex = 2;
            // 
            // cbArticle2
            // 
            this.cbArticle2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArticle2.FormattingEnabled = true;
            this.cbArticle2.Location = new System.Drawing.Point(6, 19);
            this.cbArticle2.Name = "cbArticle2";
            this.cbArticle2.Size = new System.Drawing.Size(121, 21);
            this.cbArticle2.TabIndex = 0;
            // 
            // btnConfirmArticle
            // 
            this.btnConfirmArticle.ForeColor = System.Drawing.Color.Red;
            this.btnConfirmArticle.Location = new System.Drawing.Point(12, 116);
            this.btnConfirmArticle.Name = "btnConfirmArticle";
            this.btnConfirmArticle.Size = new System.Drawing.Size(260, 23);
            this.btnConfirmArticle.TabIndex = 4;
            this.btnConfirmArticle.Text = "Підтвердити статтю";
            this.btnConfirmArticle.UseVisualStyleBackColor = true;
            this.btnConfirmArticle.Click += new System.EventHandler(this.btnConfirmArticle_Click);
            // 
            // gbConfirmArticle
            // 
            this.gbConfirmArticle.Controls.Add(this.lblDateConfirmedArticle);
            this.gbConfirmArticle.Controls.Add(this.lblUserConfirmedArticle);
            this.gbConfirmArticle.Location = new System.Drawing.Point(12, 145);
            this.gbConfirmArticle.Name = "gbConfirmArticle";
            this.gbConfirmArticle.Size = new System.Drawing.Size(260, 60);
            this.gbConfirmArticle.TabIndex = 5;
            this.gbConfirmArticle.TabStop = false;
            this.gbConfirmArticle.Text = "Підтвердив статтю";
            // 
            // lblDateConfirmedArticle
            // 
            this.lblDateConfirmedArticle.AutoSize = true;
            this.lblDateConfirmedArticle.Location = new System.Drawing.Point(6, 36);
            this.lblDateConfirmedArticle.Name = "lblDateConfirmedArticle";
            this.lblDateConfirmedArticle.Size = new System.Drawing.Size(116, 13);
            this.lblDateConfirmedArticle.TabIndex = 1;
            this.lblDateConfirmedArticle.Text = "lblDateConfirmedArticle";
            // 
            // lblUserConfirmedArticle
            // 
            this.lblUserConfirmedArticle.AutoSize = true;
            this.lblUserConfirmedArticle.Location = new System.Drawing.Point(6, 16);
            this.lblUserConfirmedArticle.Name = "lblUserConfirmedArticle";
            this.lblUserConfirmedArticle.Size = new System.Drawing.Size(115, 13);
            this.lblUserConfirmedArticle.TabIndex = 0;
            this.lblUserConfirmedArticle.Text = "lblUserConfirmedArticle";
            // 
            // ArticleChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 217);
            this.Controls.Add(this.gbConfirmArticle);
            this.Controls.Add(this.btnConfirmArticle);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "ArticleChangeForm";
            this.Text = "Оберіть статтю";
            this.Load += new System.EventHandler(this.FArticleChoose_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkCount2)).EndInit();
            this.gbConfirmArticle.ResumeLayout(false);
            this.gbConfirmArticle.PerformLayout();
            this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button btnOk;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button btnArticleUpdate1;
      private System.Windows.Forms.TextBox tbArticleSuggest1;
      private System.Windows.Forms.ComboBox cbArticle1;
      private System.Windows.Forms.Button btnWorkCountUpdate1;
      private System.Windows.Forms.TextBox tbWorkCountSuggest1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btnWorkCountUpdate2;
      private System.Windows.Forms.TextBox tbWorkCountSuggest2;
      private System.Windows.Forms.Button btnArticleUpdate2;
      private System.Windows.Forms.TextBox tbArticleSuggest2;
      private System.Windows.Forms.ComboBox cbArticle2;
      private System.Windows.Forms.NumericUpDown numWorkCount1;
      private System.Windows.Forms.NumericUpDown numWorkCount2;
      private System.Windows.Forms.Button btnConfirmArticle;
      private System.Windows.Forms.GroupBox gbConfirmArticle;
      private System.Windows.Forms.Label lblDateConfirmedArticle;
      private System.Windows.Forms.Label lblUserConfirmedArticle;
   }
}