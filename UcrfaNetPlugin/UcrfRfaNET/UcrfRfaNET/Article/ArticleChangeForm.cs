﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Extensions;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.UtilityClass;
using ArticleLib;

namespace XICSM.UcrfRfaNET.Article
{
    public partial class ArticleChangeForm : FBaseForm
    {
        /// <summary>
        /// Update articles of station(s)
        /// </summary>
        /// <param name="applIdList"></param>
        public static bool UpdateArticle(params int[] applIdList)
        {
            if (CanUpdateArticle() == false)
            {
                MessageBox.Show(CLocaliz.TxT("You don't have rights to update article."), "", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }
            using (ArticleChangeForm fa = new ArticleChangeForm(applIdList))
            {
                fa.ShowDialog();
            }
            return true;
        }
        /// <summary>
        /// Can user update article
        /// </summary>
        /// <returns></returns>
        public static bool CanUpdateArticle()
        {
            //IMTableRight rights = TableRights.GetRight(PlugTbl.APPL);
            //return ((rights & IMTableRight.Update) == IMTableRight.Update);
            return true;
        }
        //=================================================
        //=================================================
        //=================================================
        private const string FieldArticleIdSuggest1 = "ArticleIdSuggest1";
        private int _articleIdSuggest1;
        public int ArticleIdSuggest1
        {
            get { return _articleIdSuggest1; }
            set
            {
                if(_articleIdSuggest1 != value)
                {
                    _articleIdSuggest1 = value;
                    InvokeNotifyPropertyChanged(FieldArticleIdSuggest1);
                }
            }
        }

        private const string FieldWorkCountSuggest1 = "WorkCountSuggest1";
        private int _workCountSuggest1;
        public int WorkCountSuggest1
        {
            get { return _workCountSuggest1; }
            set
            {
                if (_workCountSuggest1 != value)
                {
                    _workCountSuggest1 = value;
                    InvokeNotifyPropertyChanged(FieldWorkCountSuggest1);
                }
            }
        }

        private const string FieldArticleIdSuggest2 = "ArticleIdSuggest2";
        private int _articleIdSuggest2;
        public int ArticleIdSuggest2
        {
            get { return _articleIdSuggest2; }
            set
            {
                if (_articleIdSuggest2 != value)
                {
                    _articleIdSuggest2 = value;
                    InvokeNotifyPropertyChanged(FieldArticleIdSuggest2);
                }
            }
        }

        private const string FieldWorkCountSuggest2 = "WorkCountSuggest2";
        private int _workCountSuggest2;
        public int WorkCountSuggest2
        {
            get { return _workCountSuggest2; }
            set
            {
                if (_workCountSuggest2 != value)
                {
                    _workCountSuggest2 = value;
                    InvokeNotifyPropertyChanged(FieldWorkCountSuggest2);
                }
            }
        }

        private UcrfDepartment curDept;
        private readonly ComboBoxDictionaryList<int, string> _lstPrices1 = new ComboBoxDictionaryList<int, string>();
        private readonly ComboBoxDictionaryList<int, string> _lstPrices2 = new ComboBoxDictionaryList<int, string>();
        private ApplArticles _applArticle;
        private int[] _applIds;
        private int[] _stationIds;
        /// <summary>
        /// Constructor
        /// </summary>
        private ArticleChangeForm(params int[] applIds)
        {
            _applArticle = new ApplArticles();
            _applIds = applIds;
            InitializeComponent();
            //----
            InitForm();
        }
        /// <summary>
        /// Инициализация формы
        /// </summary>
        private void InitForm()
        {
            curDept = CUsers.GetCurDepartment();
            ArticleIdSuggest1 = IM.NullI;
            ArticleIdSuggest2 = IM.NullI;
            WorkCountSuggest1 = 0;
            WorkCountSuggest2 = 0;
            if (_applIds.Length == 1)
            {
                _applArticle.Load(_applIds[0]);
                int[] art_id  = ArticleLib.ArticleUpdate.GetArticle(_applIds[0]);
                _stationIds = art_id;
                if ((art_id != null))
                {
                    if (art_id.Length > 1)
                    {
                        for (int c = 0; c < 2; c++)
                        {
                            int t_id = art_id[c];
                            if (art_id[c] == IM.NullI)
                            {
                                t_id = ApplArticles.GetArticle(_applIds[0]);
                            }

                            if (c == 0) ArticleIdSuggest1 = art_id[c];
                            if (c == 1) ArticleIdSuggest2 = art_id[c];
                        }
                    }
                    else if (art_id.Length == 1)
                    {
                        if (art_id[0] == IM.NullI)
                        {
                            int t_id = ApplArticles.GetArticle(_applIds[0]);
                            ArticleIdSuggest1 = t_id;
                        }
                        else ArticleIdSuggest1 = art_id[0];
                    }

                    if (art_id.Length == 1)
                    {
                        int count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], IM.NullI, ArticleIdSuggest1);
                        if ((count_art == IM.NullI))
                        {
                            count_art = ApplArticles.GetWorkCount(_applIds[0]);
                        }
                        WorkCountSuggest1 = count_art;
                    }
                    else if (art_id.Length == 2)
                    {
                        int count_art1 = ArticleLib.CountWork.CountWorkByArticle(IM.NullI, art_id[0], ArticleIdSuggest1);
                        if ((count_art1 == IM.NullI))
                        {
                            count_art1 = ArticleLib.Abonent.GetCountWorkExt_(_applIds[0],  ArticleIdSuggest1,1);
                        }

                        int count_art2 = ArticleLib.CountWork.CountWorkByArticle(IM.NullI, art_id[1], ArticleIdSuggest2);
                        if ((count_art2 == IM.NullI))
                        {
                            count_art2 = ArticleLib.Abonent.GetCountWorkExt_(_applIds[0],  ArticleIdSuggest2,2);
                            //count_art2 = ApplArticles.GetWorkCount(_applIds[0]);
                            //if (count_art2 == 0) count_art2 = ApplArticles.GetWorkCount(_applIds[0]);
                        }

                        WorkCountSuggest1 = count_art1;
                        WorkCountSuggest2 = count_art2;
                    }
                    if ((art_id.Length == 0) || (WorkCountSuggest1 == IM.NullI) || (WorkCountSuggest1 == 0))
                    {
                        ArticleIdSuggest1 = ArticleLib.ArticleUpdate.GetArticleSimple(_applIds[0]);
                        if ((ArticleIdSuggest1 == IM.NullI))
                        {
                            ArticleIdSuggest1 = ApplArticles.GetArticle(_applIds[0]);
                        }
                        WorkCountSuggest1 = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], ArticleIdSuggest1, ArticleIdSuggest1);

                        // вставка 26.10.2016 (для SHIP)
                        if (_applArticle.TableName == "SHIP") {
                            if ((ArticleIdSuggest2 != IM.NullI) && (WorkCountSuggest2 == IM.NullI)) {
                                WorkCountSuggest2 = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], ArticleIdSuggest2, ArticleIdSuggest2);
                            }
                            if ((ArticleIdSuggest2 != IM.NullI) && (WorkCountSuggest2 == IM.NullI))
                                ArticleIdSuggest2 = IM.NullI;
                        }

                        if ((WorkCountSuggest1 == IM.NullI))
                        {
                               int[] Al_ = ArticleLib.ArticleUpdate.GetStationsIdFromAppl(_applIds[0]);
                               if (Al_.Length == 1)
                               {
                                  WorkCountSuggest1 = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], IM.NullI, ArticleIdSuggest1);
                               }
                               else if (Al_.Length >= 2)
                               {
                                   WorkCountSuggest1 = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], Al_[0], ArticleIdSuggest1);

                                   WorkCountSuggest2 = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], Al_[1], ArticleIdSuggest2);
                               }


                               if ((ArticleIdSuggest1 == IM.NullI) || (ArticleIdSuggest1 == 0))
                               {
                                   ArticleIdSuggest1 = Article.ApplArticles.GetArticle(_applIds[0]);
                               }

                               if ((WorkCountSuggest1 == IM.NullI) || (WorkCountSuggest1 == 0))
                               {
                                   WorkCountSuggest1 = Article.ApplArticles.GetWorkCountExtended(_applIds[0]);
                               }
                               
                                    
                            

                        }
                    }
                }

               

                
                
               
            }
            //=====
            numWorkCount1.DataBindings.Add("Value", _applArticle, ApplArticles.FieldWorkCount1, true, DataSourceUpdateMode.OnPropertyChanged);
            numWorkCount2.DataBindings.Add("Value", _applArticle, ApplArticles.FieldWorkCount2, true, DataSourceUpdateMode.OnPropertyChanged);
            {
                Binding bnd = new Binding("Text", this, FieldWorkCountSuggest1);
                bnd.Format += WorkCountSuggestFormat;
                tbWorkCountSuggest1.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Text", this, FieldWorkCountSuggest2);
                bnd.Format += WorkCountSuggestFormat;
                tbWorkCountSuggest2.DataBindings.Add(bnd);
            }
            {
                //btnWorkCountUpdate1
                Binding bnd = new Binding("Enabled", this, FieldWorkCountSuggest1);
                bnd.Format += WorkCountButtomEnable;
                btnWorkCountUpdate1.DataBindings.Add(bnd);
            }
            {
                //btnWorkCountUpdate2
                Binding bnd = new Binding("Enabled", this, FieldWorkCountSuggest2);
                bnd.Format += WorkCountButtomEnable;
                btnWorkCountUpdate2.DataBindings.Add(bnd);
            }
            {
                //btnArticleUpdate1
                Binding bnd = new Binding("Enabled", this, FieldArticleIdSuggest1);
                bnd.Format += ArticleButtomEnable;
                btnArticleUpdate1.DataBindings.Add(bnd);
            }
            {
                //btnArticleUpdate2
                Binding bnd = new Binding("Enabled", this, FieldArticleIdSuggest2);
                bnd.Format += ArticleButtomEnable;
                btnArticleUpdate2.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Text", this, FieldArticleIdSuggest1);
                bnd.Format += ArticleSuggestFormat;
                tbArticleSuggest1.DataBindings.Add(bnd);
            }
            {
                Binding bnd = new Binding("Text", this, FieldArticleIdSuggest2);
                bnd.Format += ArticleSuggestFormat;
                tbArticleSuggest2.DataBindings.Add(bnd);
            }
            _lstPrices1.InitComboBox(cbArticle1);
            _lstPrices2.InitComboBox(cbArticle2);
            const string msgProgresBar = "Loading all articles...";
            using(LisProgressBar pb = new LisProgressBar(msgProgresBar))
            {
                Dictionary<int, string> articles = new Dictionary<int, string>();
                articles.Add(IM.NullI, CLocaliz.TxT("No article"));
                pb.SetBig(msgProgresBar);
                pb.SetProgress(0, 200);
                using(Icsm.LisRecordSet rsArticle = new Icsm.LisRecordSet(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly))
                {
                    rsArticle.Select("ID,ARTICLE,STANDARD,STATUS,DATEOUTACTION,DATESTARTACTION");
                    string department = Enum.GetName(typeof (ManagemenUDCR), ManagemenUDCR.URCM);
                    rsArticle.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
                    rsArticle.SetWhere("STATUS", IMRecordset.Operation.Eq,1);
                    rsArticle.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
                    rsArticle.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

                    rsArticle.OrderBy("ARTICLE", OrderDirection.Ascending);
                    for (rsArticle.Open(); !rsArticle.IsEOF(); rsArticle.MoveNext())
                    {
                        string articleName = string.Format("{0} ({1})", rsArticle.GetS("ARTICLE"),
                                                           rsArticle.GetS("STANDARD"));
                        int articleid = rsArticle.GetI("ID");
                        if ((articles.ContainsKey(articleid) == false))
                            articles.Add(articleid, articleName);
                        pb.Increment(true);
                    }
                }
                foreach (KeyValuePair<int, string> pair in articles)
                    _lstPrices1.Add(new ComboBoxDictionary<int, string>(pair.Key, pair.Value));
                foreach (KeyValuePair<int, string> pair in articles)
                    _lstPrices2.Add(new ComboBoxDictionary<int, string>(pair.Key, pair.Value));
            }
            cbArticle1.DataBindings.Add("SelectedValue", _applArticle, ApplArticles.FieldArticleId1, true, DataSourceUpdateMode.OnPropertyChanged);
            cbArticle2.DataBindings.Add("SelectedValue", _applArticle, ApplArticles.FieldArticleId2, true, DataSourceUpdateMode.OnPropertyChanged);
//////////////////////////////////////           
            //---
            btnOk.DataBindings.Add("Enabled", _applArticle, NotifyPropertyChanged.FieldIsChanged);
            {
                Binding bnd = new Binding("Enabled", _applArticle, ApplArticles.FieldIsConfirmed);
                bnd.Format += (sen, ev) =>
                {
                    bool isConfirmed;
                    if (Boolean.TryParse(ev.Value.ToString(), out isConfirmed) == false)
                        isConfirmed = false;
                    bool setVal = !isConfirmed && _applArticle.CanConfirmArticle();
                    ev.Value = setVal;
                    if (_applArticle.IsConfirmed == false)
                    {
                        btnConfirmArticle.Text = setVal ? "Підтвердити статтю" : "Некоректні дані";
                    }
                    else
                    {
                        btnConfirmArticle.Text = "Стаття підтверджена";
                    }
                };
                btnConfirmArticle.DataBindings.Add(bnd);
            }
            //---
            {
                Binding bnd = new Binding("Text", _applArticle, ApplArticles.FieldConfirmedBy);
                bnd.Format += (sen, ev) =>
                                  {
                                      ev.Value = CUsers.GetUserFio(ev.Value.ToString());
                                  };
                lblUserConfirmedArticle.DataBindings.Add(bnd);
            }
            //---
            {
                Binding bnd = new Binding("Text", _applArticle, ApplArticles.FieldConfirmedDate);
                bnd.Format += (sen, ev) =>
                {
                    if (ev.Value is DateTime)
                    {
                        DateTime dt = (DateTime)ev.Value;
                        ev.Value = dt.ToStringNullT();
                    }
                    else
                    {
                        ev.Value = "";
                    }
                };
                lblDateConfirmedArticle.DataBindings.Add(bnd);
            }

            //если текущее подразделение - филиалы, то деактивировать все элементы управления
            if (curDept == UcrfDepartment.Branch)
            {
                if (ArticleLib.CountWork.GetStandradOnAppl(_applArticle.ApplID) == CRadioTech.RBSS)
                {
                    cbArticle1.Enabled = true;
                    cbArticle2.Enabled = false;
                    btnOk.Enabled = false;
                    btnCancel.Enabled = true;
                    btnArticleUpdate1.Enabled = true;
                    btnArticleUpdate2.Enabled = false;
                    btnWorkCountUpdate1.Enabled = true;
                    btnWorkCountUpdate2.Enabled = false;
                    numWorkCount1.Enabled = true;
                    numWorkCount2.Enabled = false;
                }
                else
                {
                    cbArticle1.Enabled = false;
                    cbArticle2.Enabled = false;
                    btnOk.Enabled = false;
                    btnCancel.Enabled = true;
                    btnArticleUpdate1.Enabled = false;
                    btnArticleUpdate2.Enabled = false;
                    btnWorkCountUpdate1.Enabled = false;
                    btnWorkCountUpdate2.Enabled = false;
                    numWorkCount1.Enabled = false;
                    numWorkCount2.Enabled = false;
                }
            }
        }
        /// <summary>
        /// Form loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FArticleChoose_Load(object sender, EventArgs e)
        {
            IsChanged = false;
        }
        /// <summary>
        /// + Convert article ID to article name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ArticleSuggestFormat(object sender, ConvertEventArgs e)
        {
            int idPrice = e.Value.ToString().ToInt32(IM.NullI);
            CPrice price = new CPrice();
            price.Read(idPrice);
            e.Value = price.zsARTICLE;
        }
        /// <summary>
        /// + Convert work counter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkCountSuggestFormat(object sender, ConvertEventArgs e)
        {
            int workCount = e.Value.ToString().ToInt32(IM.NullI);
            if ((workCount == 0) || (workCount == IM.NullI))
                e.Value = "";
            else
                e.Value = workCount.ToString();
        }
        /// <summary>
        /// +
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ArticleButtomEnable(object sender, ConvertEventArgs e)
        {
            int articleId = e.Value.ToString().ToInt32(IM.NullI);
            if ((articleId == 0) || (articleId == IM.NullI))
                e.Value = false;
            else
                e.Value = true;
        }
        /// <summary>
        /// +
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkCountButtomEnable(object sender, ConvertEventArgs e)
        {
            int workCount = e.Value.ToString().ToInt32(IM.NullI);
            if ((workCount == 0) || (workCount == IM.NullI))
                e.Value = false;
            else
                e.Value = true;
        }
        /// <summary>
        /// Set work count 1 by default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWorkCountUpdate1_Click(object sender, EventArgs e)
        {
            _applArticle.WorkCount1 = WorkCountSuggest1;
        }
        /// <summary>
        /// Set work count 2 by default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWorkCountUpdate2_Click(object sender, EventArgs e)
        {
            _applArticle.WorkCount2 = WorkCountSuggest2;
        }
        /// <summary>
        /// Set article by default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArticleUpdate1_Click(object sender, EventArgs e)
        {
            _applArticle.ArticleId1 = ArticleIdSuggest1;
            //SaveHide();
            //int count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], _stationIds[0], ArticleIdSuggest1);
            //if ((count_art == IM.NullI))
            //{
                //count_art = ApplArticles.GetWorkCount(_applIds[0]);
            //}

            int count_art = ArticleLib.CountWork.CountWorkByArticle(IM.NullI, _stationIds.Length>0 ?_stationIds[0]:IM.NullI, ArticleIdSuggest1);
            if ((count_art == IM.NullI))
            {
                count_art = ArticleLib.Abonent.GetCountWorkExt_(_applIds[0], ArticleIdSuggest1, 1);
                if ((count_art == IM.NullI))
                {
                    count_art = ApplArticles.GetWorkCount(_applIds[0]);

                    if ((count_art == IM.NullI) || (count_art == 0))
                    {
                        count_art = Article.ApplArticles.GetWorkCountExtended(_applIds[0]);

                        if ((count_art == IM.NullI) || (count_art == 0))
                        {
                            count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], IM.NullI, ArticleIdSuggest1);
                        }

                    }
                }
            }

            WorkCountSuggest1 = count_art;
        }
        /// <summary>
        /// Set article by default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArticleUpdate2_Click(object sender, EventArgs e)
        {
            _applArticle.ArticleId2 = ArticleIdSuggest2;
            int count_art = IM.NullI;
            /*
            if (_stationIds.Length == 2) count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], _stationIds[1], ArticleIdSuggest1);
            if (_stationIds.Length == 1) count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], IM.NullI, ArticleIdSuggest1);
            if ((count_art == IM.NullI))
            {
                count_art = ApplArticles.GetWorkCount(_applIds[0]);
            }
             */

            count_art = ArticleLib.CountWork.CountWorkByArticle(IM.NullI, _stationIds.Length > 1 ? _stationIds[1] : IM.NullI, ArticleIdSuggest2);
            if ((count_art == IM.NullI))
            {
                count_art = ArticleLib.Abonent.GetCountWorkExt_(_applIds[0], ArticleIdSuggest2, 2);
            }

            // вставка 26.10.2016 (для SHIP)
            if ((ArticleIdSuggest2 != IM.NullI) && (count_art == IM.NullI)){
                count_art = ArticleLib.CountWork.CountWorkByArticle(_applIds[0], ArticleIdSuggest2, ArticleIdSuggest2);
            }

            WorkCountSuggest2 = count_art;
        }
        /// <summary>
        /// Confirm closing
        /// </summary>
        /// <returns></returns>
        protected override bool ConfirmClose()
        {
            bool retVal = true;
            if (_applArticle.IsChanged)
            {
                const string mess = "Зберегти зміни?";
                DialogResult resDialog = MessageBox.Show(mess, "", MessageBoxButtons.YesNoCancel,
                                                         MessageBoxIcon.Question);
                switch (resDialog)
                {
                    case DialogResult.Yes:
                        retVal = Save();
                        break;
                    case DialogResult.Cancel:
                        retVal = false;
                        break;
                }
            }
            return retVal;
        }
        /// <summary>
        /// Сохранить изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Save() == false)
                DialogResult = DialogResult.None;

        }
        /// <summary>
        /// Сохраняем изменения
        /// </summary>
        /// <returns>TRUE если все ОК, иначе FALSE</returns>
        private bool Save()
        {
            if (_applArticle.CanConfirmArticle() && _applArticle.IsConfirmed == false)
            {
                const string mess = "Стаття та кіл-ть робіт не підтвердженні. Продовжити?";
                if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return false;
            }
            try
            {
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    foreach (int applId in _applIds)
                        _applArticle.Save(applId);
                    tr.Commit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Подтверждение статьи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirmArticle_Click(object sender, EventArgs e)
        {
            try
            {
                _applArticle.SetConfirmArticle();
            }
            catch (Exceptions.IllegalArticleException ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
           
        }

    }
}
