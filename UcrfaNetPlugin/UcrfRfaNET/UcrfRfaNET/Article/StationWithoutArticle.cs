﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ICSM;
using XICSM.UcrfRfaNET.Icsm;

namespace XICSM.UcrfRfaNET.Article
{
    internal class StationWithoutArticle
    {

        public static void FindStationWithoutArticle(int ownerId, out int[] applIds, out int[] netIds, List<string> Province)
        {

            string str = "";
            string title = CLocaliz.TxT("Looking for stations without articles");
            HashSet<int> listApplIdBad = new HashSet<int>();
            HashSet<int> listApplIdOk = new HashSet<int>();
            HashSet<int> listNetIdBad = new HashSet<int>();
            HashSet<int> listNetIdOk = new HashSet<int>();
            // Централизованные станции





            using (LisProgressBar pb = new LisProgressBar(title))
            {
                pb.SetBig(CLocaliz.TxT("Loading centralized stations"));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                pb.SetProgress(0, 10000);

                



                using (LisRecordSet rsAllStation = new LisRecordSet(PlugTbl.xnrfa_urchmpay, IMRecordset.Mode.ReadOnly))
                {
                    rsAllStation.Select("ID,NET_ID");
                    rsAllStation.Select("PRICE_ID");
                    rsAllStation.Select("PRICE_ID2");
                    rsAllStation.Select("WORKS_COUNT");
                    rsAllStation.Select("WORKS_COUNT2");
                    rsAllStation.Select("PRICE_ID");
                    rsAllStation.Select("IS_CONFIRMED");
                    rsAllStation.Select("PRICE_STATUS");
                    rsAllStation.Select("PROVINCE");
                    rsAllStation.Select("PROVINCE2");
                    rsAllStation.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, ownerId);

                    for (rsAllStation.Open(); !rsAllStation.IsEOF(); rsAllStation.MoveNext())
                    {
                        if ((Province != null) && (Province.Count > 0))
                        {
                            foreach (string prov in Province)
                            {
                                if ((rsAllStation.GetS("PROVINCE").TrimStart().TrimEnd().Contains(prov.TrimStart().TrimEnd())) || ((rsAllStation.GetS("PROVINCE2").TrimStart().TrimEnd().Contains(prov.TrimStart().TrimEnd()))))
                                {


                                    if (pb.UserCanceled())
                                        break;
                                    pb.Increment(true);

                                    // Для проверки
                                    //str += "ID: " + rsAllStation.GetI("ID") + " PRICE_ID: " + rsAllStation.GetI("PRICE_ID") + " WORKS_COUNT: " + rsAllStation.GetI("WORKS_COUNT") + " PRICE_ID2: " + rsAllStation.GetI("PRICE_ID2") + " WORKS_COUNT2: " + rsAllStation.GetI("WORKS_COUNT2") + " IS_CONFIRMED:" + rsAllStation.GetI("IS_CONFIRMED") + " NET_ID: " + rsAllStation.GetI("NET_ID") + System.Environment.NewLine;

                                    /*
                                    int netId = rsAllStation.GetI("NET_ID");
                                    //if (listNetIdOk.Contains(netId) || listNetIdBad.Contains(netId))
                                    //    continue;

                                    if (netId != IM.NullI)
                                    {
                                        //Есть сеть

                                        int netPriceId = rsAllStation.GetI("PRICE_ID");
                                        bool isNetConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                                        bool isActivated = ArticleLib.ArticleFunc.GetStatusForCodePriceID(netPriceId);//(rsAllStation.GetI("Application.Price.STATUS")==1);


                                        if (netPriceId != IM.NullI)
                                        {
                                            //Нашли сеть
                                            if ((isNetConfirmed) && (isActivated))
                                            {
                                                if (!listNetIdOk.Contains(netId))
                                                    listNetIdOk.Add(netId);
                                            }
                                            else
                                            {
                                                if (!listNetIdBad.Contains(netId))
                                                listNetIdBad.Add(netId);
                                            }
                                            //continue;
                                        }
                                    }

                                    */


                                  




                                    //Сети нет или у сети не установлена статья
                                    int applId = rsAllStation.GetI("ID");
                                    if (listApplIdOk.Contains(applId) || listApplIdBad.Contains(applId))
                                        continue;

                                    int applPriceId = rsAllStation.GetI("PRICE_ID");
                                    int applWorkCount = rsAllStation.GetI("WORKS_COUNT");
                                    //bool isActivated_ = ArticleLib.ArticleFunc.GetStatusForCodePriceID(applPriceId);
                                    bool isActivated_ = (rsAllStation.GetI("PRICE_STATUS")==1);
                                    int applPriceId2 = rsAllStation.GetI("PRICE_ID2");
                                    int applWorkCount2 = rsAllStation.GetI("WORKS_COUNT2");
                                    bool isApplConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                                    //bool isCorrect = ((isActivated_==1) && (applPriceId != IM.NullI) && (applWorkCount != IM.NullI) && (applWorkCount > 0)) || ((applPriceId == IM.NullI) && isApplConfirmed);
                                    bool isCorrect = ((isActivated_) && (applPriceId != IM.NullI) && (applPriceId < 2147483647) && (applWorkCount != IM.NullI) && (applWorkCount < 2147483647) && (isApplConfirmed));
                                    bool isCorrect2 = true;
                                    if (applPriceId2 != IM.NullI)
                                        if ((applWorkCount2 == IM.NullI) || (applWorkCount2 == 0) || (applWorkCount2 == 2147483647))
                                            isCorrect2 = false;

                                    if (isCorrect && isCorrect2 && isApplConfirmed)
                                        listApplIdOk.Add(applId);
                                    else
                                        listApplIdBad.Add(applId);
                                }
                            }
                        }
                    }
                }

            }

          //  MessageBox.Show(str);
            applIds = listApplIdBad.ToArray();
            netIds = listNetIdBad.ToArray();
        }

        public static void FindStationWithoutArticle(int ownerId, out int[] applIds, out int[] netIds)
        {
            string title = CLocaliz.TxT("Looking for stations without articles");
            HashSet<int> listApplIdBad = new HashSet<int>();
            HashSet<int> listApplIdOk = new HashSet<int>();
            HashSet<int> listNetIdBad = new HashSet<int>();
            HashSet<int> listNetIdOk = new HashSet<int>();
            // Централизованные станции
            using (LisProgressBar pb = new LisProgressBar(title))
            {
                pb.SetBig(CLocaliz.TxT("Loading centralized stations"));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                pb.SetProgress(0, 10000);
                using (LisRecordSet rsAllStation = new LisRecordSet(PlugTbl.xnrfa_urchmpay, IMRecordset.Mode.ReadOnly))
                {
                    rsAllStation.Select("ID,NET_ID");
                    rsAllStation.Select("PRICE_ID");
                    rsAllStation.Select("PRICE_ID2");
                    rsAllStation.Select("WORKS_COUNT");
                    rsAllStation.Select("WORKS_COUNT2");
                    rsAllStation.Select("PRICE_STATUS");
                    rsAllStation.Select("IS_CONFIRMED");
                    rsAllStation.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, ownerId);
                    //rsAllStation.SetWhere("Application.WAS_USED", IMRecordset.Operation.Eq, 1);
                    for (rsAllStation.Open(); !rsAllStation.IsEOF(); rsAllStation.MoveNext())
                    {

                        if (pb.UserCanceled())
                            break;
                        pb.Increment(true);
                        int netId = rsAllStation.GetI("NET_ID");
                        if (listNetIdOk.Contains(netId) || listNetIdBad.Contains(netId))
                            continue;

                        if (netId != IM.NullI)
                        {
                            //Есть сеть
                            int netPriceId = rsAllStation.GetI("PRICE_ID");
                            bool isNetConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                            bool isActivated = ArticleLib.ArticleFunc.GetStatusForCodePriceID(netPriceId);
                            if (netPriceId != IM.NullI)
                            {
                                //Нашли сеть
                                if ((isNetConfirmed) && (isActivated))
                                    listNetIdOk.Add(netId);
                                else
                                    listNetIdBad.Add(netId);
                                continue;
                            }
                        }
                        //Сети нет или у сети не установлена статья
                        int applId = rsAllStation.GetI("ID");
                        if (listApplIdOk.Contains(applId) || listApplIdBad.Contains(applId))
                            continue;

                        int applPriceId = rsAllStation.GetI("PRICE_ID");
                        //bool isActivated_ = ArticleLib.ArticleFunc.GetStatusForCodePriceID(applPriceId);
                        bool isActivated_ = (rsAllStation.GetI("PRICE_STATUS") == 1);

                        int applWorkCount = rsAllStation.GetI("WORKS_COUNT");
                        int applPriceId2 = rsAllStation.GetI("PRICE_ID2");
                        int applWorkCount2 = rsAllStation.GetI("WORKS_COUNT2");
                        bool isApplConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                        //bool isCorrect = ((isActivated_==1) && (applPriceId != IM.NullI) && (applWorkCount != IM.NullI) && (applWorkCount > 0)) || ((applPriceId == IM.NullI) && isApplConfirmed);
                        bool isCorrect = ((isActivated_) && (applPriceId != IM.NullI) && (applPriceId < 2147483647) && (applWorkCount != IM.NullI) && (applWorkCount < 2147483647) && (isApplConfirmed));
                        
                        bool isCorrect2 = true;
                        if (applPriceId2 != IM.NullI)
                            if ((applWorkCount2 == IM.NullI) || (applWorkCount2 == 0) || (applWorkCount2 == 2147483647))
                                isCorrect2 = false;

                        if (isCorrect && isCorrect2 && isApplConfirmed)
                            listApplIdOk.Add(applId);
                        else
                            listApplIdBad.Add(applId);
                    }
                }
            }

            applIds = listApplIdBad.ToArray();
            netIds = listNetIdBad.ToArray();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="ownerId"></param>
        /// <param name="applIds"></param>
        /// <param name="netIds"></param>
        public static void FindStationWithoutArticleImportOrder(int ownerId, out int[] applIds, out int[] netIds)
        {
            string title = CLocaliz.TxT("Looking for stations without articles");
            HashSet<int> listApplIdBad = new HashSet<int>();
            HashSet<int> listApplIdOk = new HashSet<int>();
            HashSet<int> listNetIdBad = new HashSet<int>();
            HashSet<int> listNetIdOk = new HashSet<int>();
            
            // Централизованные станции
            using (LisProgressBar pb = new LisProgressBar(title))
            {
                pb.SetBig(CLocaliz.TxT("Loading stations"));
                pb.SetSmall(CLocaliz.TxT("Please wait"));
                pb.SetProgress(0, 10000);

                List<int> L_ID_Import_Order = new List<int>();
                using (IMRecordset rsAllStation_import = new IMRecordset("XNRFA_IMPORT_ORDER", IMRecordset.Mode.ReadOnly))
                {
                    rsAllStation_import.Select("ID,OWNER_ID,EMPLOYEE_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF");
                    rsAllStation_import.SetWhere("EMPLOYEE_ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
                    rsAllStation_import.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerId);
                    for (rsAllStation_import.Open(); !rsAllStation_import.IsEOF(); rsAllStation_import.MoveNext())
                    {
                        if (!L_ID_Import_Order.Contains(rsAllStation_import.GetI("ID")))
                            L_ID_Import_Order.Add(rsAllStation_import.GetI("ID"));
                    }
                }

                //using (LisRecordSet rsAllStation_import = new LisRecordSet("XNRFA_IMPORT_ORDER" , IMRecordset.Mode.ReadOnly))
                //{
                    //rsAllStation_import.Select("ID,OWNER_ID,EMPLOYEE_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF");
                    //rsAllStation_import.SetWhere("EMPLOYEE_ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
                    //rsAllStation_import.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerId);
                    //for (rsAllStation_import.Open(); !rsAllStation_import.IsEOF(); rsAllStation_import.MoveNext())
                    //{

                        //if (pb.UserCanceled())
                            //break;
                        //pb.Increment(true);

                        //if (rsAllStation_import.GetI("APPL_ID") != IM.NullI)
                        //{
                            using (LisRecordSet rsAllStation = new LisRecordSet(PlugTbl.xnrfa_urchmpay, IMRecordset.Mode.ReadOnly))
                            {
                                rsAllStation.Select("ID,NET_ID");
                                rsAllStation.Select("PRICE_ID");
                                rsAllStation.Select("PRICE_ID2");
                                rsAllStation.Select("WORKS_COUNT");
                                rsAllStation.Select("WORKS_COUNT2");
                                rsAllStation.Select("PRICE_STATUS");
                                rsAllStation.Select("IS_CONFIRMED");
                                rsAllStation.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, ownerId);
                                //rsAllStation.SetWhere("ID", IMRecordset.Operation.Eq, rsAllStation_import.GetI("APPL_ID"));

                                //rsAllStation.SetWhere("Application.WAS_USED", IMRecordset.Operation.Eq, 1);
                                for (rsAllStation.Open(); !rsAllStation.IsEOF(); rsAllStation.MoveNext())
                                {
                                    
                                    if (pb.UserCanceled())
                                        break;
                                    pb.Increment(true);

                                    int APP_IDS = rsAllStation.GetI("ID");
                                    if (!L_ID_Import_Order.Contains(APP_IDS))
                                        continue;

                                    int netId = rsAllStation.GetI("NET_ID");
                                    if (listNetIdOk.Contains(netId) || listNetIdBad.Contains(netId))
                                        continue;

                                    if (netId != IM.NullI)
                                    {
                                        //Есть сеть
                                        int netPriceId = rsAllStation.GetI("PRICE_ID");
                                        bool isNetConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                                        bool isActivated = ArticleLib.ArticleFunc.GetStatusForCodePriceID(netPriceId);
                                        if (netPriceId != IM.NullI)
                                        {
                                            //Нашли сеть
                                            if ((isNetConfirmed) && (isActivated))
                                                listNetIdOk.Add(netId);
                                            else
                                                listNetIdBad.Add(netId);
                                            continue;
                                        }
                                    }
                                    //Сети нет или у сети не установлена статья
                                    int applId = rsAllStation.GetI("ID");
                                    if (listApplIdOk.Contains(applId) || listApplIdBad.Contains(applId))
                                        continue;

                                    int applPriceId = rsAllStation.GetI("PRICE_ID");
                                    //bool isActivated_ = ArticleLib.ArticleFunc.GetStatusForCodePriceID(applPriceId);
                                    bool isActivated_ = (rsAllStation.GetI("PRICE_STATUS") == 1);

                                    int applWorkCount = rsAllStation.GetI("WORKS_COUNT");
                                    int applPriceId2 = rsAllStation.GetI("PRICE_ID2");
                                    int applWorkCount2 = rsAllStation.GetI("WORKS_COUNT2");
                                    bool isApplConfirmed = (rsAllStation.GetI("IS_CONFIRMED") == 1);
                                    //bool isCorrect = ((isActivated_==1) && (applPriceId != IM.NullI) && (applWorkCount != IM.NullI) && (applWorkCount > 0)) || ((applPriceId == IM.NullI) && isApplConfirmed);
                                    bool isCorrect = ((isActivated_) && (applPriceId != IM.NullI) && (applPriceId < 2147483647) && (applWorkCount != IM.NullI) && (applWorkCount < 2147483647) && (isApplConfirmed));

                                    bool isCorrect2 = true;
                                    if (applPriceId2 != IM.NullI)
                                        if ((applWorkCount2 == IM.NullI) || (applWorkCount2 == 0) || (applWorkCount2 == 2147483647))
                                            isCorrect2 = false;

                                    if (isCorrect && isCorrect2 && isApplConfirmed)
                                        listApplIdOk.Add(applId);
                                    else
                                        listApplIdBad.Add(applId);
                                }
                            }
                        
                
            }

            applIds = listApplIdBad.ToArray();
            netIds = listNetIdBad.ToArray();
        }

    }


}
