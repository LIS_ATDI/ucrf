﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;
using System.Drawing;
using XICSM.UcrfRfaNET.AppPacket;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using XICSM.UcrfRfaNET.ApplSource;
using System.Linq;
using System.Configuration;
using XICSM.UcrfRfaNET.HelpClasses.Classes;

namespace XICSM.UcrfRfaNET
{
   public partial class FPacket : Form, IDisposable
   {
      //===================================================
      protected CPacket packet = null;
      private SettingsGrid _settings;
      protected string PacketTableName { get; set; }
      protected bool inited = false; //Загрузка элементов формы
      public static EPacketType _PacketType;
      private const string ClmNamePacketState = "gridPacketState";
      private const string ClmNamePacketTrCount = "gridPacketTrCount";
      private const string ClmNameDocDate = "gridDocDate";
      private const string ClmNameDocEndDate = "gridDocEndDate";
      private const string ClmNameDocUrcmDate = "gridDocUrcmDate";
      private const string ClmNamePtkDateFrom = "gridPtkDateFrom";
      private const string ClmNamePtkDateTo = "gridPtkDateTo";
      private const string ClmNameIsTv = "gridIsTv";
      private const string ClmNameIsNv = "gridIsNv";
      //------
      private Color _okColor = Color.Black;
      private Color _badColor = Color.Crimson;
      //------
      private Font _oldOne = null;
      private Font _boldy = null;
      private Font _red = null;

      private UcrfDepartment currentRole = CUsers.GetCurDepartment();
      private string userRegion = "";
      private bool status_click_select_RCP = false;
      private List<LinkDocFiles> LstDocFilesArray = null;
      private List<int> Mass_ = new List<int>();
      private List<int> Mass_Street = new List<int>();
      private List<int> Mass_Distinct = new List<int>();
      public static int CountAddressChangeMass = 0;
      private List<DataGridViewColumn> ColumnsLstMain = new List<DataGridViewColumn>();
      //===================================================
      /// <summary>
      /// Конструктор 1
      /// </summary>
      protected FPacket()  // make this constructor unavailable for program
      {
         InitializeComponent();
         CLocaliz.TxT(this);   //Локализуем
         userRegion = CUsers.GetUserProvince().Trim();
      }
      //===================================================
      /// <summary>
      /// Конструктор 2
      /// </summary>
      public FPacket(int idPacketIn, string tableName) : this()
      {
          _settings = new SettingsGrid(Name);
         LstDocFilesArray = new List<LinkDocFiles>();
         PacketTableName = tableName;
         // Загружаем класс бизнес логики пакета
         packet = new CPacket(idPacketIn);
         //lblAttachedDocs.Text = CLocaliz.TxT("Attached documents:");
         //
         string tmpStandart = packet.Standart;
         // Тип
         string type = GetApplType();
         cbPacketType.DataSource = packet.GetListAppl();
         cbPacketType.Text = type;
         if (string.IsNullOrEmpty(type))
             cbPacketType.SelectedIndex = -1;
         // Стандарт
         packet.Standart = tmpStandart;
         packet.standList.InitComboBox(cbStandardNew);
         cbStandardNew.DataBindings.Add("SelectedValue", packet, "Standart", true);
         txtBoxStatus.DataBindings.Add("Text", packet, "ForeighnStatus", true);
         // 

         // Контент
         int tmpIndex = packet.PacketTypeIndex;
         cbPacketContent.DataSource = packet.PacketTypeList;
         cbPacketContent.SelectedIndex = tmpIndex;

         packet.ApplGrid = dataGridAppl;
         InitApplGrid();
         inited = true;
         UpdateCheckDates();
         SetFont(this.Font);

         //packet.RefreshRows();

         //if (packet.ApplType != AppType.AppUnknown)
             //packet.RefreshRows();
         //else packet.RefreshRowsSpecial(GetAppTypePacket());
         if (packet.ListSelectedColumns != null) {
             if (packet.ListSelectedColumns.Count > 0)
                 packet.RefreshRowsSpecial(GetAppTypePacket());
         }

         UpdateComponentForm();
         
         //----
         {
            Binding applCountBinding = new Binding("Text", packet, "CountAppl");
            applCountBinding.Format += (object sender, ConvertEventArgs e) =>
                                             {
                                                 e.Value = string.Format("{1} ({0})", e.Value, CLocaliz.TxT("Packet's documents"));
                                             };
            gbApplList.DataBindings.Add(applCountBinding);
         }
         //CallBackFunction.callbackEventFunc callbackEventHandlerFunc = new CallBackFunction.callbackEventFunc( this.GetLinkDocFiles);

         

         for (int i = 0; i < dataGridViewLinkFiles.Columns.Count;i++)
             dataGridViewLinkFiles.Columns[i].HeaderText = CLocaliz.TxT(dataGridViewLinkFiles.Columns[i].Name);

             
         tabControlLinkFiles.Width = this.Width - tabControlLinkFiles.Left - 45;

         editRecordToolStripMenuItem.Text = CLocaliz.TxT("Edit record");
         this.formOfControlToolStripMenuItem.Text = CLocaliz.TxT("Form of control");
         _PacketType = packet.PacketType;

      }
      //=====================================================================================
      /// <summary>
      /// Обработчик нажатия левой кнопки мыши на заголовок столбца
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void dataGridAppl_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
      {
          // если выбрано поле SID, то включить сортировку
          //if (e.ColumnIndex == 6)
        //if (dataGridAppl.Columns[e.ColumnIndex].HeaderText.ToString()=="SID")
          //{
              DataGridViewColumn newColumn = dataGridAppl.Columns[e.ColumnIndex];
              DataGridViewColumn oldColumn = dataGridAppl.SortedColumn;
              ListSortDirection direction;

              // Если столбец null, то DataGridView не выполняет сортировку.
              if (oldColumn != null)
              {
                 
                  if (oldColumn == newColumn &&
                      dataGridAppl.SortOrder == SortOrder.Ascending)
                  {
                      direction = ListSortDirection.Descending;
                  }
                  else
                  {
                 
                      direction = ListSortDirection.Ascending;
                      oldColumn.HeaderCell.SortGlyphDirection = SortOrder.None;
                  }
              }
              else
              {
                  direction = ListSortDirection.Ascending;
              }

              // сортировка выбранного столбца
              dataGridAppl.Sort(newColumn, direction);
              newColumn.HeaderCell.SortGlyphDirection =
                  direction == ListSortDirection.Ascending ?
                  SortOrder.Ascending : SortOrder.Descending;

              RenderApplGrid(true);
          //}
      }

      //=====================================================================================
      private void dataGridAppl_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
      {
          foreach (DataGridViewColumn column in dataGridAppl.Columns)
          {
              column.SortMode = DataGridViewColumnSortMode.Programmatic;
          }
      }
      //=====================================================================================
      public void GetLinkDocFiles()
      {

          

          LstDocFilesArray.Clear();
          IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly );
          rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
          rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, packet.PacketId);
          //rsDocLink.SetAdditional("[REC_TAB]='XNRFA_PACKET'");
          rsDocLink.SetWhere("REC_TAB", IMRecordset.Operation.Eq, "XNRFA_PACKET");
          try
          {

              rsDocLink.Open();
              for (; !rsDocLink.IsEOF(); rsDocLink.MoveNext())
              {

                  int DOC_ID = rsDocLink.GetI("DOC_ID");


                  ////////////////
                  IMRecordset rsAps = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                  rsAps.Select("ID,MEMO,DOC_REF, DOC_DATE, PATH,DOC_TYPE,CREATED_BY, DATE_CREATED");
                  rsAps.SetWhere("ID", IMRecordset.Operation.Eq, DOC_ID);
                  //rsAps.SetWhere("MEMO", IMRecordset.Operation.Eq, packet.PacketId);



                  try
                  {
                      Documents.Documents Doc = new Documents.Documents();
                      rsAps.Open();
                      for (; !rsAps.IsEOF(); rsAps.MoveNext())
                      {
                          LinkDocFiles LnkDoc_ = new LinkDocFiles();
                          LnkDoc_.ID = rsAps.GetI("ID");
                          if (rsAps.GetS("DOC_TYPE") != "") { LnkDoc_.DOC_TYPE = Doc.GetHumanNameOfDoc(rsAps.GetS("DOC_TYPE")); } else { LnkDoc_.DOC_TYPE = ""; };
                          LnkDoc_.CREATED_BY = rsAps.GetS("CREATED_BY");
                          LnkDoc_.DOC_DATE = rsAps.GetT("DOC_DATE");
                          LnkDoc_.DOC_REF = rsAps.GetS("DOC_REF");
                          LnkDoc_.DATE_CREATED = rsAps.GetT("DATE_CREATED");
                          LnkDoc_.PATH = rsAps.GetS("PATH");
                          LstDocFilesArray.Add(LnkDoc_);

                      }


                      dataGridViewLinkFiles.Rows.Clear();

                      var out_list = from ui in LstDocFilesArray
                                     orderby ui.DATE_CREATED descending
                                     select ui;

                      
                      foreach (LinkDocFiles item in out_list )
                      {
                          dataGridViewLinkFiles.Rows.Add(new object[] { item.DOC_TYPE, item.DOC_REF, item.DOC_DATE, item.CREATED_BY, item.ID, item.PATH });
                      }
                  }
                  finally
                  {
                      rsAps.Close();
                      rsAps.Dispose();
                  }


                  /////////////////

              }

              
          }
          finally
          {
              rsDocLink.Close();
              rsDocLink.Destroy();
          }








       
                           
      }
      //===================================================
      /// <summary>
      /// Обновляем компоненты формы
      /// </summary>
      protected void UpdateComponentForm()
      {
         //panelParametrs.Enabled = packet.ReadOnly;

         //panelButton.Enabled = !packet.ReadOnly;
         //if (packet.department == ManagemenUDCROfICSM.URZP)
         //   buttonNewAppl.Enabled = false;
         //else
         //   buttonNewAppl.Enabled = !packet.NewRecord;
         //buttonPacketPrint.Enabled = !packet.NewRecord;
         //buttonDeleteAppl.Enabled = !packet.NewRecord;
         //buttonAttachDoc.Enabled = !packet.NewRecord;
          

         buttonSave.Enabled = false;
         cbPacketType.Enabled = packet.NewRecord;
         tbOwner.Text = packet.OwnerName;
         tbInNumber.Text = packet.NumberIn;
         tbOutNumber.Text = packet.NumberOut;
         dateInPacket.Value = packet.DateIn;
         dateOutPacket.Value = packet.DateOut;
         buttonSelectAppl.Enabled = true;
         buttonRetriveAll.Enabled = true;
         if ((packet.NewRecord == true) || (packet.PacketType == EPacketType.PctLICENCE))
            buttonSelectAppl.Enabled = false;

         UcrfDepartment curUserDepart = CUsers.GetCurDepartment();
         bool branchUserMatch = CUsers.GetUserRegion(packet.CreatedBy) == CUsers.GetUserRegion(IM.ConnectedUser());

         //Блокировка меню
         urzpMenu.Enabled = (curUserDepart == UcrfDepartment.URZP) ? !packet.NewRecord : false;
         mainMenu.Enabled = (curUserDepart == UcrfDepartment.URCP) ? !packet.NewRecord : false;

         if (curUserDepart == UcrfDepartment.Branch)
         {
             mainMenu.Enabled = branchUserMatch && !packet.NewRecord && (packet.PacketType != EPacketType.PckPTKNVTV);
             urzpMenu.Enabled = branchUserMatch && !packet.NewRecord && (packet.PacketType == EPacketType.PckPTKNVTV);
             
             mainMenu.Text = "Дозволи";
             urzpMenu.Text = "ПТК/НВ/ТВ";
             
             DRVMemo.Text = "Заявка на виставлення рахунку...";
             ShowAllApplToDrvFromUrcm.Text = "Всі заявки на виставлення рахунку...";
             службоваЗапискаДоДРВToolStripMenuItem.Text = "Заявка на виставлення рахунку...";
             ShowAllApplToDrvFromUrzp.Text = "Всі заявки на виставлення рахунку...";
         }
         
          // права доступа на сохранение
         bool changeEnabled = (packet.RoleCreatedUnder == curUserDepart) 
                                && (curUserDepart != UcrfDepartment.Branch ||
                                    (curUserDepart == UcrfDepartment.Branch) && branchUserMatch)
                                && (packet.PacketType != EPacketType.PckPTKNVTV ||
                                    (packet.PacketType == EPacketType.PckPTKNVTV) && (packet.PacketState != StatePacket.SentToUrcp));
         buttonSave.Enabled = changeEnabled;
         // исключим из прав не сохранённые в БД новые записи
         changeEnabled = changeEnabled && !packet.NewRecord;
         buttonSelectAppl.Enabled = changeEnabled;
         //buttonRetriveAll.Enabled = changeEnabled;
         buttonDeleteAppl.Enabled = changeEnabled;
         // плюс спец. условия
         buttonNewAppl.Enabled = changeEnabled && (packet.PacketType != EPacketType.PckPTKNVTV) && (packet.PacketType != EPacketType.PckDuplicate) && (packet.PacketType != EPacketType.PctANNUL) && (packet.PacketType != EPacketType.PctPROLONG);
         buttonPacketPrint.Enabled = changeEnabled || ((packet.PacketType == EPacketType.PckPTKNVTV) && (packet.PacketState == StatePacket.SentToUrcp) && (curUserDepart == UcrfDepartment.URCP));
         buttonAttachDoc.Enabled = changeEnabled || ((packet.PacketType == EPacketType.PckPTKNVTV) && (packet.PacketState == StatePacket.SentToUrcp) && (curUserDepart == UcrfDepartment.URCP));
                     
                        
         //if (!packet.canEdit)
         //{
         //   this.panelButton.Enabled = false;
         //   this.menuPacket.Enabled = false;
         //}

         CheckAddDate();

         GetLinkDocFiles();


       
      }
      //===================================================
      /// <summary>
      /// Сохраняем изменения
      /// </summary>
      protected void buttonSave_Click(object sender, EventArgs e)
      {
          PacketSave();
         
          CheckAddDate();
      }

      /// <summary>
      /// Виводимо пункт меню "Внести дату видачі" 
      /// </summary>
      private void CheckAddDate()
      {
          if (mainMenu.Enabled)
          {
              /*
              if (packet.ApplType == AppType.AppTR_З)
              {
                  AddDateMenuItemAbonent.Visible = true;
                  AddDateMenuItemAbonent.Enabled = true;
                  AddDateMenuItem.Enabled = false;
                  AddDateMenuItem.Visible = false;
              }
              else
              {
                  AddDateMenuItem.Enabled = false;
                  AddDateMenuItem.Visible = false;
                  AddDateMenuItemAbonent.Visible = false;
                  AddDateMenuItemAbonent.Enabled = false;
              }
               */


              if (((packet.ApplType == AppType.AppZRS) || (packet.ApplType == AppType.AppAR_3) || (packet.ApplType == AppType.AppTR_З) || (packet.ApplType == AppType.AppA3)))
              {
                  AddDateMenuItem.Enabled = true;
                  AddDateMenuItem.Visible = true;
                  AddDateMenuItemAbonent.Visible = false;
                  AddDateMenuItemAbonent.Enabled = false;

                  if (packet.ApplType == AppType.AppTR_З)
                  {
                      AddDateMenuItemAbonent.Visible = true;
                      AddDateMenuItemAbonent.Enabled = true;
                      AddDateMenuItem.Enabled = false;
                      AddDateMenuItem.Visible = false;
                  }
              }
              else
              {
                  AddDateMenuItem.Enabled = false;
                  AddDateMenuItem.Visible = false;
                  AddDateMenuItemAbonent.Visible = false;
                  AddDateMenuItemAbonent.Enabled = false;
                  
                  if (packet.ApplType == AppType.AppAP3)
                  {
                      AddDateMenuItem.Enabled = true;
                      AddDateMenuItem.Visible = true;
                      AddDateMenuItemAbonent.Visible = false;
                      AddDateMenuItemAbonent.Enabled = false;
                  }
              }
               
          }
      }

       //===================================================
      /// <summary>
      /// Создаем новую заявку
      /// </summary>
      private void buttonNewAppl_Click(object sender, EventArgs e)
      {
         packet.CreateNewApp(this);
         RefreshForm();
      }
      //===================================================
      /// <summary>
      /// Выбераем владельца
      /// </summary>
      private void tbOwner_KeyDown(object sender, KeyEventArgs e)
      {
         if (/*e.Control && */e.KeyCode == Keys.Enter && packet.NewRecord == true)
         {
            packet.SelectOwner(tbOwner.Text);
            tbOwner.Text = packet.OwnerName;
         }
      }
      //===================================================
      /// <summary>
      /// Обновляем параметры
      /// </summary>
      private void tbInNumber_Validating(object sender, CancelEventArgs e)
      {
         packet.NumberIn = tbInNumber.Text;
      }
      //===================================================
      /// <summary>
      /// Обновляем параметры
      /// </summary>
      private void tbOutNumber_Validating(object sender, CancelEventArgs e)
      {
         packet.NumberOut = tbOutNumber.Text;
      }
      //=================================================
      /// <summary>
      /// Обработка клавищ формы
      /// </summary>
      protected void PacketForm_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.KeyCode == Keys.Escape)
         {
            // Выход
            GC.Collect();
            GC.WaitForPendingFinalizers();
            this.Close();
         }

         if (packet.ReadOnly == true)
            return;

         if (e.KeyCode == Keys.F5)
         {// Сохранить
            buttonSave.Select();
            PacketSave();
         }
         else if (e.KeyCode == Keys.F6)
         {// Печать
            buttonPacketPrint.Select();
            PacketPrint();
         }
         else if ((e.KeyCode == Keys.D) && (packet.ApplType == AppType.AppAR_3))
         {
            List<int> selected = new List<int>(packet.GelListIdSelectedAppl());
            if (selected.Count > 0)
            {
               using (EnterDateForm dateForm = new EnterDateForm())
               {
                  dateForm.Owner = this as Form;
                  if (dateForm.ShowDialog() == DialogResult.OK)
                  {
                     foreach (int applId in selected)
                     {
                        ApplSource.AbonentStation station = ApplSource.BaseStation.GetStationFromAppl(applId) as ApplSource.AbonentStation;
                        if (station != null)
                        {
                           station.PermDatePrint = dateForm.dtpDate.Value;
                           station.Save();
                        }
                     }
                  }
               }
            }
         }
      }
      //===================================================
      /// <summary>
      /// Обновляем параметры
      /// </summary>
      private void dateOutPacket_Validating(object sender, CancelEventArgs e)
      {
         packet.DateOut = dateOutPacket.Value;
         if (dateInPacket.Value.TruncateByDay() < dateOutPacket.Value.TruncateByDay())
         {// Дата не верная
            //MessageBox.Show(CLocaliz.TxT("Перевірте Вихідну Дату"), CLocaliz.TxT("Зауваження"));
            //dateOutPacket.Value = packet.DateOut;
         }
      }

      /// <summary>
      /// Дата видачі
      /// </summary>
      private DateTime dateIssue;
      //===================================================
      /// <summary>
      /// Обновляем параметры
      /// </summary>
      protected void dateInPacket_Validating(object sender, CancelEventArgs e)
      {
         packet.DateIn = dateInPacket.Value;
         if (dateInPacket.Value.TruncateByDay() < dateOutPacket.Value.TruncateByDay())
         {// Дата не верная
             //MessageBox.Show(CLocaliz.TxT("Перевірте Вхідну Дату"), CLocaliz.TxT("Зауваження"));
             //dateInPacket.Value = packet.DateIn;
         }

          //from "URZP Packet form"
          //TODO - remove this from here after refactoring. Just DataBind all these componenys with packet.DateIn
         UpdateCheckDates();
      }
      //===================================================
      /// <summary>
      /// Открываем заявку
      /// </summary>
      private void dataGridAppl_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
      {
         int row = e.RowIndex;
         try
         {
            if (row >= 0)
            {
               string tableAppl = "";
               if (PacketTableName == PlugTbl.XvPacket)
                   tableAppl = PlugTbl.XvAppl;
               packet.ShowApp(tableAppl, this);
               RenderApplGrid(false);
            }
            else if((row == -1) && (e.ColumnIndex == 0))
            {
               dataGridAppl.EndEdit();
               DataGridViewColumn column = dataGridAppl.Columns[e.ColumnIndex];
               if (column.HeaderText != "X")
               {
                   column.HeaderText = "X";
                   packet.SetAllApplCheckBox(true);
               }
               else
               {
                   column.HeaderText = "";
                   packet.SetAllApplCheckBox(false);
               }
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      

       //===================================================
      /// <summary>
      /// Изменили тип пакета
      /// </summary>
      protected virtual void cbPacketContent_SelectedValueChanged(object sender, EventArgs e)
      {
          packet.PacketTypeIndex = cbPacketContent.SelectedIndex;
         _PacketType = packet.PacketType;
          //foreach (string cbPacketContent.Items)
         for (int j = 0; j < cbPacketContent.Items.Count; j++) {
             if (cbPacketContent.Items[j] != null) {
                 if (cbPacketContent.Items[j].ToString() == _PacketType.ToString()) {
                     packet.PacketTypeIndex = j;
                     break;
                 }
             }
         }
          //packet.PacketTypeIndex = cbPacketContent.Items.IndexOf(_PacketType);
         if (packet.PacketType == EPacketType.PckSetCallSign) {
             packet.ListAppType.Clear();
             packet.ListAppType.Add("TP", AppType.AppTR);
             packet.ListAppType.Add("ЗРС", AppType.AppZRS);
             string type = GetApplType();
             cbPacketType.DataSource = packet.GetListAppl();
             cbPacketType.Text = type;
             if (string.IsNullOrEmpty(type)) {
                 cbPacketType.SelectedIndex = -1;
             }
             else {

             }
         }
         InitApplGrid();
         if (packet.ListSelectedColumns != null) {
             if (packet.ListSelectedColumns.Count > 0)
                 packet.RefreshRowsSpecial(GetAppTypePacket());
         }
         UpdateCheckDates();
         
      }

      /// <summary>
      /// Изменяет вид формы пакета в зависимости от его типа
      /// </summary>
      protected void UpdateCheckDates()
      {
          if (inited)
          {
              tabPage1.Text = CLocaliz.TxT("Attached Docs");
              tabPage2.Text = CLocaliz.TxT("URZP Panel");

              
              if ((currentRole == UcrfDepartment.URZP) || ((currentRole == UcrfDepartment.Branch) && (packet.PacketType== EPacketType.PckPTKNVTV)))
              {
                  DateTime inDate = packet.DateIn;
                  tbDate25.Text = inDate.AddDays(25).ToShortDateString();
                  tbDate35.Text = inDate.AddDays(35).ToShortDateString();
                  tbDate45.Text = inDate.AddDays(45).ToShortDateString();
                  tbDate5.Text = inDate.AddDays(5).ToShortDateString();
                  DateTime date5 = inDate.AddDays(5);
                  TimeSpan cntDays = date5 - DateTime.Now;
                  tbDays5.Text = (cntDays.Days >= 0 ? cntDays.Days : 0).ToString();
                  DateTime date25 = inDate.AddDays(25);
                  TimeSpan cntDays25 = date25 - DateTime.Now;
                  tbDays25.Text = (cntDays25.TotalDays >= 0 ? cntDays25.Days : 0).ToString();
                  DateTime date35 = inDate.AddDays(35);
                  TimeSpan cntDays35 = date35 - DateTime.Now;
                  tbDays35.Text = (cntDays35.Days >= 0 ? cntDays35.Days : 0).ToString();
                  DateTime date45 = inDate.AddDays(45);
                  TimeSpan cntDays45 = date45 - DateTime.Now;
                  tbDays45.Text = (cntDays45.Days >= 0 ? cntDays45.Days : 0).ToString();
                  tbCounter.DataBindings.Clear();
                  tbCounter.DataBindings.Add("Text", packet, "TotalTrsmCnt");

                  tabControlLinkFiles.SelectedTab = tabPage2;
              }
              else
              {
                  tabControlLinkFiles.Controls.Remove(tabPage2);
              }


              /*

              if (DoWeShowPtkInfo()) // now unified with table view columns
              {
                  //pnViewLinkFiles.Visible = false;
                  //pnCheckDates.Visible = true;

                  DateTime inDate = packet.DateIn;
                  tbDate25.Text = inDate.AddDays(25).ToShortDateString();
                  tbDate35.Text = inDate.AddDays(35).ToShortDateString();
                  tbDate45.Text = inDate.AddDays(45).ToShortDateString();
                  tbDate5.Text = inDate.AddDays(5).ToShortDateString();
                  DateTime date5 = inDate.AddDays(5);
                  TimeSpan cntDays = date5 - DateTime.Now;
                  tbDays5.Text = (cntDays.Days >= 0 ? cntDays.Days : 0).ToString();
                  DateTime date25 = inDate.AddDays(25);
                  TimeSpan cntDays25 = date25 - DateTime.Now;
                  tbDays25.Text = (cntDays25.TotalDays >= 0 ? cntDays25.Days : 0).ToString();
                  DateTime date35 = inDate.AddDays(35);
                  TimeSpan cntDays35 = date35 - DateTime.Now;
                  tbDays35.Text = (cntDays35.Days >= 0 ? cntDays35.Days : 0).ToString();
                  DateTime date45 = inDate.AddDays(45);
                  TimeSpan cntDays45 = date45 - DateTime.Now;
                  tbDays45.Text = (cntDays45.Days >= 0 ? cntDays45.Days : 0).ToString();
                  tbCounter.DataBindings.Clear();
                  tbCounter.DataBindings.Add("Text", packet, "TotalTrsmCnt");
              }
              else
              {
                 // pnCheckDates.Visible = false;
                 // pnViewLinkFiles.Visible = true;
              }
               */ 
          }
      }

      //===================================================
      /// <summary>
      /// Выбрать заявку
      /// </summary>
      private void buttonSelectAppl_Click(object sender, EventArgs e)
      {
         packet.SelectNewAppl();
         RefreshForm();
      }
      //===================================================
      /// <summary>
      /// Изменили департамент
      /// </summary>
      private void cbPacketType_SelectedValueChanged(object sender, EventArgs e)
      {
         packet.SetApplType(cbPacketType.Text);
         InitApplGrid();
         packet.UpdateStandard();
         if (packet.PacketType == EPacketType.PckSetCallSign)
         {
             if (cbPacketType.Text == "TP")
             {
                 Lis.CommonLib.Binding.ComboBoxDictionaryList<string, string> LK = new Lis.CommonLib.Binding.ComboBoxDictionaryList<string, string>();
                 string Descr = "";
                 for (int i = 0; i < packet.standList.Count(); i++)
                 {
                     if (packet.standList[i].Key == "РБСС")
                     {
                         Descr = packet.standList[i].Description;
                         packet.standList.Remove(packet.standList[i]);
                     }
                 }
                 if (Descr != "")
                 {
                     LK = packet.standList;
                     LK.Insert(0, new Lis.CommonLib.Binding.ComboBoxDictionary<string, string>("РБСС", Descr));
                     packet.standList = LK;
                     packet.Standart = "РБСС";
                     cbStandardNew.SelectedValue = packet.Standart;
                     cbStandardNew.Refresh();
                 }
             }
         }
      }
      //=================================================
      /// <summary>
      /// Пакетная печать
      /// </summary>
      private void buttonPacketPrint_Click(object sender, EventArgs e)
      {
         //packet.SetAllApplCheckBox(true);
         PacketPrint();
         //packet.SetAllApplCheckBox(false);
      }
      //=================================================
      /// <summary>
      /// Выдаляемо заявку
      /// </summary>
      private void buttonDeleteAppl_Click(object sender, EventArgs e)
      {
         packet.DeleteAppl();
         RefreshForm();
      }
      //================================================
      /// <summary>
      /// Пакетная печать документов
      /// </summary>
      private void PacketPrint()
      {
         packet.PrintPacket();
      }
      //===================================================
      /// <summary>
      /// Сохранить пакет
      /// </summary>
      private void PacketSave()
      {
         try
         {
            packet.SetApplType(cbPacketType.Text);
            if (TestValueBeforeSave() == false)
                return;
            packet.Save();
            UpdateComponentForm();
         }
         catch (IMException ex) { MessageBox.Show(ex.Message, "Помилка збереження"); }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      private int CountChangeStreet()
      {
          int count = 0;
          Mass_Distinct.AddRange(Mass_);
          Mass_Distinct.AddRange(Mass_Street);
          Mass_Distinct.Distinct();
          

          for (int i=0; i<packet.Applications.Count(); i++)
          {
              if (Mass_Distinct.Contains(packet.Applications[i].ApplId))
                  count++;
          }

          return count;
      }

      private void DRVMemo_Click(object sender, EventArgs e)
      {
          if (Mass_.Count + Mass_Street.Count > 0)
              MessageBox.Show(CLocaliz.TxT("The package found RFI that have a need to change the address, in the count:") + string.Format("{0}", +CountChangeStreet()));

         dataGridAppl.EndEdit();
         if (packet.GelListIdSelectedAppl().Length == 0)
         {
            MessageBox.Show(CLocaliz.TxT("Please, select application."), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
         }
         tbOwner.Focus();
         ShowDRVMemo(ManagemenUDCR.URCP);
      }
      //===================================================
      /// <summary>
      /// Attaches scan document to Application
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void buttonAttachDoc_Click(object sender, EventArgs e)
      {
         //packet.SetAllApplCheckBox(true);
         packet.AttachDoc();
         //packet.SetAllApplCheckBox(false);
         GetLinkDocFiles();
      }
      //===================================================
      /// <summary>
      /// Открывает заявку на выставление счета от УРЗП
      /// </summary>
      private void URZPmemoToDRV(object sender, EventArgs e)
      {
          if (Mass_.Count + Mass_Street.Count > 0)
              MessageBox.Show(CLocaliz.TxT("The package found RFI that have a need to change the address, in the count:") + string.Format("{0}", +CountChangeStreet()));

         dataGridAppl.EndEdit();
         if (packet.GelListIdSelectedAppl().Length == 0)
         {
            MessageBox.Show(CLocaliz.TxT("Please, select application."), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
         }
         tbOwner.Focus();
         ShowDRVMemo(ManagemenUDCR.URZP);
      }


      protected virtual void RefreshForm()
      { }

      //===================================================
      /// <summary>
      /// Изменение данных в гриде с заявками
      /// </summary>
      protected virtual void dataGridAppl_CellValueChanged(object sender, DataGridViewCellEventArgs e)
      {
          RenderCell(e.ColumnIndex, e.RowIndex, true);
      }

      private void dataGridAppl_DataError(object sender, DataGridViewDataErrorEventArgs e)
      {

      }
      //Изменить статус заявки
      private void menuSendToUrcp_Click(object sender, EventArgs e)
      {
          if (DoWeShowPtkInfo())
          {
              bool canSendToUrcp = true;
              foreach (PacketRowBase rowUrzp in packet.Applications)
              {
                  if (rowUrzp.PtkStatusCode != "ENDW")
                  {
                      canSendToUrcp = false;
                      break;
                  }
              }
              if ((canSendToUrcp == false) &&
                  MessageBox.Show(CLocaliz.TxT("Not all applications have been processed.") +
                                Environment.NewLine +
                                CLocaliz.TxT("Do you want to continue?"), CLocaliz.TxT("Confirmation"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                  return;
              DialogResult res = MessageBox.Show(CLocaliz.TxT("Пакет буде відправлено до УРЧП для подальшої обробки. Продовжити?"), CLocaliz.TxT("Question"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
              if (res == DialogResult.Yes)
                  packet.PacketState = StatePacket.SentToUrcp;

              UpdateComponentForm();
          }
      }
      /// <summary>
      /// Печать пустого планка ПТК
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void PrintEmptyPtk_Click(object sender, EventArgs e)
      {
          const string reportName = "NAPOVNENNYA_PTK";
          const string tableName = PlugTbl.itblXnrfaPacket;
          string fileName = "";
          IcsmReport[] masRep = IM.ReportsGetList(tableName, string.Format("*{0}*", reportName));
          if (masRep.Length == 0)
          {
              MessageBox.Show(string.Format("Can't find IRP file by filter \"{0}\"", reportName));
              return;
          }
           using (SaveFileDialog savePtkFileDialog = new SaveFileDialog())
              {
                  savePtkFileDialog.Filter = "RTF|*.rtf";
                  savePtkFileDialog.Title = CLocaliz.TxT("Save file");
                  if (savePtkFileDialog.ShowDialog() == DialogResult.OK)
                  {
                      if (savePtkFileDialog.FileName == "")
                      {
                          MessageBox.Show(CLocaliz.TxT("File name is empty"));
                          return;
                      }
                      fileName = savePtkFileDialog.FileName;
                  }
              }
          IM.AdminDisconnect();
          RecordPtr rec = new RecordPtr(tableName, packet.PacketId);
          rec.PrintRTFReport(System.IO.Path.GetFileName(masRep[0].Path), PrintDocs.Language, fileName, "", false);
      }


      /// <summary>
      /// Реакція на вибір пункту меню "УРЧП->Внести дату видачі"
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void AddDateMenuItem_Click(object sender, EventArgs e)
      {
          if (packet.GelListIdSelectedAppl().Length == 0)
          {
              MessageBox.Show(CLocaliz.TxT("Please, select application."), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Error);
              return;
          }
          tbOwner.Focus();
          //Виведення діалогового вікна для введення дати видачі
          bool isRepeat = true;
          bool isCorrectValue = false;
          while(isRepeat)
          {
              isRepeat = false;
              isCorrectValue = false;
              IMDialog dialogDate = new IMDialog();
              dialogDate.Add("txtDateName", "", dateIssue, CLocaliz.TxT("Date of issue"));
              if (dialogDate.Enter())
              {
                  dateIssue = dialogDate.Get("txtDateName").ToString().ToDataTime();
                  if (dateIssue.Date < DateTime.Now.Date)
                  {

                      if (MessageBox.Show(string.Format("Перевірте правильність внесення дати видачі. Дата  '{0}' коректна?", dateIssue.Date.ToShortDateString()), "",
                                      MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Question) == DialogResult.Yes)
                      {
                          isCorrectValue = true;
                      }
                      else
                      {
                          isRepeat = true;
                      }
                  }
                  else
                  {
                      isCorrectValue = true;
                  }
              }
          }
          
          if (isCorrectValue)
              packet.ValidFrom(dateIssue); //Внесення дати друку
      }
      /// <summary>
      /// Отобразить служебки в ДРВ
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void ShowAllApplToDrvFromUrcm_Click(object sender, EventArgs e)
      {
          ShowApplPay(ManagemenUDCR.URCP);
      }
      /// <summary>
      /// Отобразить служебки в ДРВ
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void ShowAllApplToDrvFromUrzp_Click(object sender, EventArgs e)
      {
          ShowApplPay(ManagemenUDCR.URZP);
      }
      /// <summary>
      /// Отобразить служебки
      /// </summary>
      private void ShowApplPay(ManagemenUDCR department)
      {
          StringBuilder sbSql = new StringBuilder(1000);
          sbSql.Append(string.Format("([PACKET_ID] = {0})", packet.PacketId));
          sbSql.Append(string.Format(" AND ([DEPARTMENT] LIKE '{0}')", Enum.GetName(typeof(ManagemenUDCR), department)));
          IMDBList.Show("Службові записки", PlugTbl.itblXnrfaDrvAppl, sbSql.ToString(), "ID", OrderDirection.Ascending, 0, 0);
      }

      private void FPacket_Load(object sender, EventArgs e)
      {
          if (packet != null)
          {

              Mass_ = new List<int>();
              Mass_Street = new List<int>();
              RenderCity();
              RenderApplGrid(true);
              CountAddressChangeMass = Mass_.Count + Mass_Street.Count;
              if (Mass_.Count + Mass_Street.Count > 0)
                  MessageBox.Show(CLocaliz.TxT("The package found RFI that have a need to change the address, in the count:") + string.Format("{0}", +CountChangeStreet()));
          }
      }

      protected DataGridViewColumn AddGridColumn(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName, int DisplayIndex)
      {
          if (column == null)
              column = new DataGridViewTextBoxColumn();
          if (!string.IsNullOrEmpty(name))
              column.Name = name;
          column.HeaderText = headerText;
          column.Width = width;
          column.ReadOnly = readOnly;
          column.DataPropertyName = dataPropertyName;
          column.DisplayIndex = DisplayIndex;
          dataGridAppl.Columns.Add(column);
          return column;
      }
      protected DataGridViewColumn AddGridColumnMain(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName)
      {
          if (column == null)
              column = new DataGridViewTextBoxColumn();
          if (!string.IsNullOrEmpty(name))
              column.Name = name;
          column.HeaderText = headerText;
          column.Width = width;
          column.ReadOnly = readOnly;
          column.DataPropertyName = dataPropertyName;
          return column;
      }

      protected DataGridViewColumn AddGridColumn(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName)
      {
          if (column == null)
              column = new DataGridViewTextBoxColumn();
          if (!string.IsNullOrEmpty(name))
              column.Name = name;
          column.HeaderText = headerText;
          column.Width = width;
          column.ReadOnly = readOnly;
          column.DataPropertyName = dataPropertyName;
          dataGridAppl.Columns.Add(column);
          return column;
      }
      /// <summary>
      /// 
      /// </summary>
      private void UpdateListColumns()
      {
          dataGridAppl.Columns.Clear();
          dataGridAppl.AutoGenerateColumns = false;
          DataGridViewCheckBoxColumn columnCb = new DataGridViewCheckBoxColumn();
          AddGridColumnMain(columnCb, "gridPacketCB", "", 30, true, "IsSelected");
          columnCb.FlatStyle = FlatStyle.Standard;
          columnCb.ThreeState = true;
          columnCb.CellTemplate = new DataGridViewCheckBoxCell();
          columnCb.TrueValue = true;
          columnCb.Resizable = DataGridViewTriState.False;
          dataGridAppl.Columns.Add(columnCb);
          dataGridAppl.Columns.Add(AddGridColumnMain(null, "gridPacketNumber", CLocaliz.TxT("Number"), 50, true, "NumRow"));

          ColumnsLstMain.Clear();
          DataGridViewColumn cl_packet_id = AddGridColumnMain(null, "gridPacketID", CLocaliz.TxT("ID"), 50, true, "ApplId");
          cl_packet_id.Visible = false;
          ColumnsLstMain.Add(cl_packet_id);
          ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketAddress", CLocaliz.TxT("Address"), 50, true, "FullPosition"));
          ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketCallSign", CLocaliz.TxT("CallSign"), 40, true, "CallSign"));

          if (DoWeShowPtkInfo())
          {
              ColumnsLstMain.Add(AddGridColumnMain(null, "gridNumConclusion", CLocaliz.TxT("Number conclusion"), 50, true, "DocNumConclusion"));
          }
          else
          {
              if (packet != null & packet.PacketType == EPacketType.PctANNUL)
                  ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketDocNumPerm", CLocaliz.TxT("# permission"), 50, true, "DocNumPerm"));
          }
          ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketCreatedBy", CLocaliz.TxT("APPL CREATED BY"), 100, true, "Fio"));
          ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketCreatedDate", CLocaliz.TxT("Created date"), 100, true, "CreatedDate"));

          if (!DoWeShowPtkInfo())
              //if (packet != null & packet.ApplType == AppType.AppRR)
              if (packet != null)
                  ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketSid", CLocaliz.TxT("SID"), 50, true, "Sid"));


          ColumnsLstMain.Add(AddGridColumnMain(null, "gridPacketStandard", CLocaliz.TxT("APPL STANDARD"), 100, true, "Standard"));

          if (DoWeShowPtkInfo())
          {
              DataGridViewComboBoxColumn columnS = new DataGridViewComboBoxColumn();
              ColumnsLstMain.Add(AddGridColumnMain(columnS, ClmNamePacketState, CLocaliz.TxT("State"), 100, false, "PtkStatus"));
              {
                  List<string> lstDescr = EriFiles.GetEriDescrList(PacketRowBase.EriPtkStatus);
                  foreach (string descr in lstDescr)
                      columnS.Items.Add(descr);
              }

              if ((packet.PacketType != EPacketType.PctNVTVURZP) || (packet.PacketType != EPacketType.PckPTKNVTV))
                  ColumnsLstMain.Add(AddGridColumnMain(null, ClmNamePacketTrCount, CLocaliz.TxT("Transmitter count"), 70, false, "TransmCnt"));

              ColumnsLstMain.Add(AddGridColumnMain(null, "gridDocNumTv", CLocaliz.TxT("Document number TV"), 150, false, "DocNumTv"));
              ColumnsLstMain.Add(AddGridColumnMain(null, ClmNameDocDate, CLocaliz.TxT("Document date"), 150, false, "DocDate"));
              ColumnsLstMain.Add(AddGridColumnMain(null, ClmNameDocEndDate, CLocaliz.TxT("Document end date"), 150, false, "DocEndDate"));
              ColumnsLstMain.Add(AddGridColumnMain(null, "gridDocNumUrcm", CLocaliz.TxT("Document number URCM"), 150, false, "DocNumUrcm"));
              ColumnsLstMain.Add(AddGridColumnMain(null, ClmNameDocUrcmDate, CLocaliz.TxT("Document URCM date"), 150, false, "DocUrcmDate"));

              if ((packet.PacketType != EPacketType.PctNVTVURZP) || (packet.PacketType != EPacketType.PckPTKNVTV))
              {
                  ColumnsLstMain.Add(AddGridColumnMain(null, ClmNamePtkDateFrom, CLocaliz.TxT("PTK date from"), 150, false, "PtkDateFrom"));
                  ColumnsLstMain.Add(AddGridColumnMain(null, ClmNamePtkDateTo, CLocaliz.TxT("PTK date to"), 150, false, "PtkDateTo"));

                  columnCb = new DataGridViewCheckBoxColumn();
                  ColumnsLstMain.Add(AddGridColumnMain(columnCb, ClmNameIsTv, CLocaliz.TxT("Is TV"), 30, false, "IsTv"));
                  columnCb.FlatStyle = FlatStyle.Standard;
                  columnCb.ThreeState = true;
                  columnCb.CellTemplate = new DataGridViewCheckBoxCell();

                  columnCb = new DataGridViewCheckBoxColumn();
                  ColumnsLstMain.Add(AddGridColumnMain(columnCb, ClmNameIsNv, CLocaliz.TxT("Is NV"), 30, false, "IsNv"));
                  columnCb.FlatStyle = FlatStyle.Standard;
                  columnCb.ThreeState = true;
                  columnCb.CellTemplate = new DataGridViewCheckBoxCell();
              }
              ColumnsLstMain.Add(AddGridColumnMain(null, "gridRemark", CLocaliz.TxT("Remark"), 150, false, "Remark"));
          }
          else
          {
              ColumnsLstMain.Add(AddGridColumnMain(null, "gridApplStatus", CLocaliz.TxT("Status"), 100, true, "Status"));
              ColumnsLstMain.Add(AddGridColumnMain(null, "gridApplStatusCoord", CLocaliz.TxT("Status coord"), 100, true, "ForeighnStatus"));
          }

      }

      public void CheckColumns()
      {
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketAddress") == null) {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketAddress" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketCallSign") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketCallSign" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridNumConclusion") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridNumConclusion" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketDocNumPerm") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketDocNumPerm" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketCreatedBy") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketCreatedBy" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketCreatedDate") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketCreatedDate" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketSid") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketSid" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketStandard") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketStandard" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketState") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketState" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketTrCount") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketTrCount" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridDocNumTv") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridDocNumTv" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridDocDate") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridDocDate" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridDocEndDate") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridDocEndDate" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridDocNumUrcm") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridDocNumUrcm" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridDocUrcmDate") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridDocUrcmDate" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPtkDateFrom") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPtkDateFrom" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPtkDateTo") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPtkDateTo" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridIsTv") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridIsTv" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridIsNv") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridIsNv" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridRemark") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridRemark" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridApplStatus") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridApplStatus" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridApplStatusCoord") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridApplStatusCoord" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          }
          if (ColumnsLstMain.Find(r => r.Name == "gridPacketID") == null)
          {
              ExtendedColumnsProperty f = packet.ListAllColumns.Find(r => r.PathToColumn == "gridPacketID" && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
              if (f != null) packet.ListAllColumns.Remove(f);
          } 
       
      }
      //===================================================
      /// <summary>
      /// Инициализируем грид
      /// </summary>
      /// <param name="applGrid">Грид</param>
      public virtual void InitApplGrid()
      {
          UpdateListColumns();
          CheckColumns();

          //////////////////////////////////////////////////////////////////////////////////////////////////////////////
          /*
          List<ColumnInfoGridView> L_FND_MAINX = _settings.ColumnInfo.FindAll(r => r.appType == AppType.AppUnknown);
          foreach (ColumnInfoGridView colInfo in L_FND_MAINX)
          {
              DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
              if (col != null)
              {
                  ExtendedColumnsProperty Fnd_ = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
                  if (Fnd_ != null)
                  {
                      if (!packet.ListSelectedColumns.Contains(Fnd_))
                      {
                          packet.ListSelectedColumns.Add(Fnd_);
                          DataGridViewColumn col_x = ColumnsLstMain.Find(r => r.Name == Fnd_.PathToColumn);
                          if (col_x != null)
                          {
                              if (!dataGridAppl.Columns.Contains(col_x)) { dataGridAppl.Columns.Add(col_x); }
                          }
                      }
                  }
              }
          }
           */ 
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////

          if ((packet.ListSelectedColumns != null) && (packet.isDefaultListColumns == false))
          {
              foreach (ExtendedColumnsProperty item in packet.ListSelectedColumns)
                  if (item.TypeRadiosys != AppType.AppUnknown)
                      AddGridColumn(null, item.NameProperty, CLocaliz.TxT(item.NameProperty), 100, true, item.NameProperty);
                  else
                  {
                      DataGridViewColumn fnd = ColumnsLstMain.Find(r => r.DataPropertyName == item.NameProperty && item.Number_List == 1);
                      if (fnd != null)
                      {
                          dataGridAppl.Columns.Add(fnd);
                      }
                  }
              if (dataGridAppl.Columns.Count > 0)
                  dataGridAppl.DataSource = packet.Applications;
          }

          if (packet.isDefaultListColumns)
          {
              List<ColumnInfoGridView> L_FND_MAIN = _settings.ColumnInfo.FindAll(r => r.appType == AppType.AppUnknown);
              if (L_FND_MAIN != null)
              {
                  L_FND_MAIN.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                  foreach (ColumnInfoGridView colInfo in L_FND_MAIN)
                  {
                      DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                      if (col != null)
                      {
                          col.Width = colInfo.Width;
                          if (!dataGridAppl.Columns.Contains(col)) { dataGridAppl.Columns.Add(col); }
                      }
                      else
                      {
                          if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                          ExtendedColumnsProperty Fnd_ = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
                          if (Fnd_ != null)
                          {
                              if (!packet.ListSelectedColumns.Contains(Fnd_))
                              {
                                  packet.ListSelectedColumns.Add(Fnd_);
                                  DataGridViewColumn col_x = ColumnsLstMain.Find(r => r.Name == Fnd_.PathToColumn);
                                  if (col_x != null)
                                  {
                                      if (!dataGridAppl.Columns.Contains(col_x)) { dataGridAppl.Columns.Add(col_x); }
                                  }
                              }
                          }
                      }
                  }
              }
              ////

              if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
              if (packet.ListSelectedColumns.Count == 0)
              {
                  foreach (DataGridViewColumn item in ColumnsLstMain)
                  {
                      if (!dataGridAppl.Columns.Contains(item))
                      {
                          dataGridAppl.Columns.Add(item);
                          ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.PathToColumn == item.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
                          if (Fnd != null)
                          {
                              if (packet.ListSelectedColumns != null)
                              {
                                  if (!packet.ListSelectedColumns.Contains(Fnd))
                                      packet.ListSelectedColumns.Add(Fnd);
                              }
                          }
                      }
                  }
              }

              if (dataGridAppl.Columns.Count > 0)
                  dataGridAppl.DataSource = packet.Applications;
              packet.isDefaultListColumns = false;
          }
          LoadComponentSettings();
      }


      private bool DoWeShowPtkInfo()
      {
          //return packet.PacketType == EPacketType.PckPTKNVTV;
          return (currentRole == UcrfDepartment.URZP && userRegion == "") || (currentRole == UcrfDepartment.Branch && packet.PacketType == EPacketType.PckPTKNVTV);
      }

      /// <summary>
      /// Обновляет значения всех ячеек грида
      /// </summary>
      public void RenderApplGrid(bool isUpdate)
      {
          for (int rowIndex = 0; rowIndex < dataGridAppl.RowCount; rowIndex++)
          {
              for (int columnIndex = 0; columnIndex < dataGridAppl.ColumnCount; columnIndex++)
              {
                  RenderCell(columnIndex, rowIndex, isUpdate);
              }
          }
      }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="isUpdate"></param>
      public void RenderCity()
      {
          for (int rowIndex = 0; rowIndex < dataGridAppl.RowCount; rowIndex++)
          {
                  ScanCity(rowIndex);
          }
      }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="columnIndex"></param>
       /// <param name="rowIndex"></param>
      private void ScanCity(int rowIndex)
      {
          PacketRowBase appl = packet.Applications[rowIndex];
                      Dictionary<int, PositionState2> dic = BaseAppClass.GetPositions(PlugTbl.APPL, appl.ApplId);
                      foreach (KeyValuePair<int, PositionState2> item in dic)
                      {

                          IMObject rsCity = null;
                          try
                          {
                              rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, item.Value.CityId);
                          }
                          catch
                          {
                              rsCity = null;
                          }
                          if (rsCity != null)
                          {
                              if ((rsCity.GetS("ADM_CODE").Contains("_")) || (rsCity.GetS("CODE").Contains("_")))
                              {
                                  if (!Mass_.Contains(appl.ApplId))
                                        Mass_.Add(appl.ApplId);
                              }
                          }
                          if (rsCity != null)
                          {
                              if (StreetChangeForm.Load(item.Value.StreetType, item.Value.StreetName, rsCity.GetS("CODE")))
                              {
                                  if (!Mass_Street.Contains(appl.ApplId))
                                      Mass_Street.Add(appl.ApplId);
                              }
                          }

                          rsCity = null;
                      }
                      GC.SuppressFinalize(dic);


      }

      /// <summary>
      /// Обработка ячеек грида, если они изменились
      /// </summary>
      /// <param name="columnIndex">индекс колонки</param>
      /// <param name="rowIndex">индекс строки</param>
      private void RenderCell(int columnIndex, int rowIndex, bool isUpdate)
      {
          if ((rowIndex < 0) || (rowIndex >= dataGridAppl.RowCount) || (columnIndex < 0) ||
              (columnIndex >= dataGridAppl.ColumnCount))
              return;

          DataGridViewColumn column = dataGridAppl.Columns[columnIndex];
          DataGridViewCell cell = dataGridAppl.Rows[rowIndex].Cells[columnIndex];
          PacketRowBase appl = packet.Applications[rowIndex];

          if (isUpdate)
          {
              if (Mass_.Contains(appl.ApplId))
                  cell.Style.BackColor = Color.Aqua;//System.Drawing.Color.FromArgb(236,183,0);

              if (Mass_Street.Contains(appl.ApplId))
                  cell.Style.BackColor = System.Drawing.Color.FromArgb(236,183,0);
              
          }
            
          if (column.Name == ClmNamePacketState)
          {
              if (appl.PtkStatusCode.Contains("ISWK") == false)
              {
                  cell.Style.Font = this._boldy;
                  cell.Style.ForeColor = Color.Black;
              }
              else
              {
                  cell.Style.Font = this._red;
                  cell.Style.ForeColor = Color.Red;
              }
          }
          else if (column.Name == ClmNamePacketTrCount)
          {
              packet.TotalTrsmCnt = IM.NullI;
          }
          else if (column.Name == ClmNameDocDate)
          {
              cell.Style.ForeColor = (TestDate(packet.DateIn, appl.DocDate, 65)) ? _okColor : _badColor;
          }
          else if (column.Name == ClmNameDocEndDate)
          {
              cell.Style.ForeColor = (TestDate(packet.DateIn, appl.DocEndDate, 65)) ? _okColor : _badColor;
              cell.Style.SelectionForeColor = cell.Style.ForeColor; // не робить чому сь
          }
          else if (column.Name == ClmNameDocUrcmDate)
          {
              cell.Style.ForeColor = (TestDate(packet.DateIn, appl.DocUrcmDate, 65)) ? _okColor : _badColor;
          }
          else if (column.Name == ClmNamePtkDateFrom)
          {
              cell.Style.ForeColor = (TestDate(packet.DateIn, appl.PtkDateFrom, 30)) ? _okColor : _badColor;
          }
          else if (column.Name == ClmNamePtkDateTo)
          {
              cell.Style.ForeColor = (TestDate(packet.DateIn, appl.PtkDateTo, 30)) ? _okColor : _badColor;
          }
          else if (column.Name == ClmNameIsTv)
          {
              //#4979
              try {
                  DataGridViewCell cellPtkDateFrom = dataGridAppl.Rows[rowIndex].Cells[ClmNamePtkDateFrom];
                  if (cellPtkDateFrom != null)
                  {
                      //cellPtkDateFrom.ReadOnly = appl.IsTv;
                      if (appl.IsTv)
                          cellPtkDateFrom.Style.ForeColor = _okColor;
                      if (appl.IsTv)
                          appl.PtkDateFrom = IM.NullT;
                  }
              }
              catch (Exception) { }
              try {
                  DataGridViewCell cellPtkDateTo = dataGridAppl.Rows[rowIndex].Cells[ClmNamePtkDateTo];
                  if (cellPtkDateTo != null)
                  {
                      //cellPtkDateTo.ReadOnly = appl.IsTv;
                      if (appl.IsTv)
                          cellPtkDateTo.Style.ForeColor = _okColor;
                      if (appl.IsTv)
                          appl.PtkDateTo = IM.NullT;
                  }
              }
              catch (Exception) { }
          }
          else if (column.Name == "gridPacketAddress")
          {
             
          }
          appl = null;
      }

      /// <summary>
      /// Проверяет попадание даты в диапазон
      /// </summary>
      /// <param name="startDate">Дата начала диапазона</param>
      /// <param name="curDate">Текущая дата</param>
      /// <param name="addDays">Кол-во дней диапазона</param>
      /// <returns>TRUE - ели текущая дата попадает в дипазон, иначе FALSE</returns>
      private bool TestDate(DateTime startDate, DateTime curDate, int addDays)
      {
          if (curDate == IM.NullT)
              return true;
          DateTime tmpCurDate = new DateTime(curDate.Year, curDate.Month, curDate.Day, 0, 0, 0, 0, DateTimeKind.Local);
          DateTime tmpStartDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0, DateTimeKind.Local);
          DateTime tmpMaxDate = tmpStartDate.AddDays(addDays);
          return ((tmpStartDate <= tmpCurDate) && (tmpCurDate <= tmpMaxDate));
      }

      //===================================================
      /// <summary>
      /// Обобщённый тип текущей заявки, для отображения
      /// </summary>
      public string GetApplType()
      {
          AppType appType = packet.ApplType;
          if (DoWeShowPtkInfo())
          {
              if (appType == AppType.AppRR || appType == AppType.AppTR || appType == AppType.AppTR_З)
                  appType = AppType.AppRRTR;
              else if (appType == AppType.AppTV2 || appType == AppType.AppTV2d)
                  appType = AppType.AppTV2TV2d;
              else if (appType == AppType.AppR2 || appType == AppType.AppR2d)
                  appType = AppType.AppR2R2d;
          }
          foreach (var pair in packet.ListAppType)
          {
              if (pair.Value == appType)
                  return pair.Key;
          }
          return "";
      }

      public void SetFont(Font f)
      {
          this._oldOne = f;
          _boldy = new Font(_oldOne, FontStyle.Bold);
          _red = new Font(_oldOne, FontStyle.Regular);
      }

      private bool TestValueBeforeSave()
      {
          StringBuilder message = new StringBuilder(100);
          foreach (DataGridViewRow row in dataGridAppl.Rows)
          {
              foreach (DataGridViewCell cell in row.Cells)
              {
                  if (cell.Style.ForeColor == _badColor)
                      message.AppendLine(string.Format("строка {0}, колонка \"{1}\"",
                                                       cell.OwningRow.Index + 1,
                                                       cell.OwningColumn.HeaderText));
              }
          }
          bool retVal = true;
          if (message.Length > 0)
          {
              StringBuilder outMessage = new StringBuilder(200);
              outMessage.AppendLine("Невірні дані в:");
              outMessage.AppendLine(message.ToString());
              outMessage.AppendLine("Продовжити?");
              retVal = (MessageBox.Show(outMessage.ToString(), "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
          }
          return retVal;
      }
      //===================================================
      /// <summary>
      /// Открытие формы служебной записки на выставление счёта
      /// </summary>
      public void ShowDRVMemo(ManagemenUDCR menuRole)
      {
          List<int> listApplId = new List<int>(packet.GelListIdSelectedAppl());

          if (listApplId.Count != 0)
          {
              if (menuRole == ManagemenUDCR.URCP)
              {
                  CDRVMemo cMemo = new CDRVMemo(packet, menuRole);
                  cMemo.ShowForm();
              }
              else
              {
                  CDRVMemoURZP cMemo = new CDRVMemoURZP(packet);
                  cMemo.ShowForm();
              }
          }
          else
              MessageBox.Show("Виберiть заявки, по яких треба сформувати рахунок");

      }

      private void mniUpdateLicList_Click(object sender, EventArgs e)
      {
          int[] licList = packet.GelListIdSelectedAppl();
          if (licList.Length == 0)
          {
              MessageBox.Show("Помітіть РЧП для оновлення", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
              return;
          }

          if (FLicParams.Config() == DialogResult.OK)
              Plugin.UpdateLicenceList(licList);
          
      }

      private void dataGridAppl_CellClick(object sender, DataGridViewCellEventArgs e)
      {
          if (e.ColumnIndex < dataGridAppl.Columns.Count && dataGridAppl.Columns[e.ColumnIndex].Name == "gridPacketCB")
              if (e.RowIndex >= 0 && e.RowIndex < packet.Applications.Count)
                  packet.Applications[e.RowIndex].IsSelected = !packet.Applications[e.RowIndex].IsSelected;
      }

      private void buttonRetriveAll_Click(object sender, EventArgs e)
      {
          if (status_click_select_RCP)
          {
              buttonRetriveAll.Text=CLocaliz.TxT("Retrieve all");
              status_click_select_RCP = false;
              packet.SetAllApplCheckBox(false);
              RefreshForm();
          }
          else
          {
              buttonRetriveAll.Text=CLocaliz.TxT("Cancel the Allocation");
              status_click_select_RCP = true;
              packet.SetAllApplCheckBox(true);
              RefreshForm();
          }
      }

       private void PrintPermissionsMenuItem_Click(object sender, EventArgs e)
      {
          dataGridAppl.EndEdit();
          AppPacket.Forms.PrintForm form = new AppPacket.Forms.PrintForm(packet);
          form.Owner = this;
          form.ShowDialog();
      }


      private void MenuItemVISN_ems_Click(object sender, EventArgs e)
      {
          string tableAppl = "";
          if (PacketTableName == PlugTbl.XvPacket)
              tableAppl = PlugTbl.XvAppl;
          packet.UCRFEMS(tableAppl);
      }

      private void MenuItemDOZV_Click(object sender, EventArgs e)
      {
          string tableAppl = "";
          if (PacketTableName == PlugTbl.XvPacket)
              tableAppl = PlugTbl.XvAppl;
          packet.eMSPermissionTool(tableAppl);

      }

      private void pnViewLinkFiles_MouseDoubleClick(object sender, MouseEventArgs e)
      {
        
      }

      void dataGridViewLinkFiles_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
      {
          if (dataGridViewLinkFiles.Rows.Count > 0)
          {
              LinkDocFiles lnk_ = LstDocFilesArray.Find(r => r.ID == int.Parse(dataGridViewLinkFiles.CurrentRow.Cells[4].Value.ToString()));
              if (lnk_!=null)
                System.Diagnostics.Process.Start(lnk_.PATH.ToString());
             
          }
      }


      private void editRecordToolStripMenuItem_Click(object sender, EventArgs e)
      {

      }

      private void dataGridViewLinkFiles_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
      {
          if (e.Button == System.Windows.Forms.MouseButtons.Right)
          {
              dataGridViewLinkFiles.Rows[e.RowIndex].Selected = true;
              dataGridViewLinkFiles.CurrentCell = dataGridViewLinkFiles[0, e.RowIndex];
          }

      }

      private void FPacket_Resize(object sender, EventArgs e)
      {
          tabControlLinkFiles.Width = this.Width - tabControlLinkFiles.Left - 45;
      }

      private void editRecordToolStripMenuItem_Click_1(object sender, EventArgs e)
      {
          //MessageBox.Show(dataGridViewLinkFiles.CurrentRow.ToString());
          if (dataGridViewLinkFiles.Rows.Count > 0)
          {
              if (dataGridViewLinkFiles.CurrentRow.Index != -1)
              {
                 // MessageBox.Show(dataGridViewLinkFiles.Rows[dataGridViewLinkFiles.CurrentRow.Index].Cells[0].Value.ToString());
                  AdditionalDataForm dataFormAdditional = new AdditionalDataForm(packet.PacketId, int.Parse(dataGridViewLinkFiles.Rows[dataGridViewLinkFiles.CurrentRow.Index].Cells[4].Value.ToString()), true);
                  dataFormAdditional.ShowDialog();
              }
          }
      }

      private void formOfControlToolStripMenuItem_Click(object sender, EventArgs e)
      {
          FormEditedPacket frmEditPacket = new FormEditedPacket(packet.PacketId);
          frmEditPacket.ShowDialog();
      }

      private void dataGridAppl_CellContentClick(object sender, DataGridViewCellEventArgs e)
      {

      }

      private void FPacket_FormClosing(object sender, FormClosingEventArgs e)
      {
          CountAddressChangeMass = 0;
      }

      private void FPacket_FormClosing_1(object sender, FormClosingEventArgs e)
      {
          GC.Collect();
          GC.WaitForPendingFinalizers();
      }

      private void AddDateMenuItemAbonent_Click(object sender, EventArgs e)
      {

      }

      private void DozvMenuItem_Click(object sender, EventArgs e)
      {
          if (packet.GelListIdSelectedAppl().Length == 0)
          {
              MessageBox.Show(CLocaliz.TxT("Please, select application."), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Error);
              return;
          }

          tbOwner.Focus();
          //Виведення діалогового вікна для введення дати видачі
          bool isRepeat = true;
          bool isCorrectValue = false;
          while (isRepeat)
          {
              isRepeat = false;
              isCorrectValue = false;
              IMDialog dialogDate = new IMDialog();
              dialogDate.Add("txtDateName", "", dateIssue, CLocaliz.TxT("Date of issue"));
              if (dialogDate.Enter())
              {
                  dateIssue = dialogDate.Get("txtDateName").ToString().ToDataTime();
                  if (dateIssue.Date < DateTime.Now.Date)
                  {

                      if (MessageBox.Show(string.Format("Перевірте правильність внесення дати видачі. Дата  '{0}' коректна?", dateIssue.Date.ToShortDateString()), "",
                                      MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Question) == DialogResult.Yes)
                      {
                          isCorrectValue = true;
                      }
                      else
                      {
                          isRepeat = true;
                      }
                  }
                  else
                  {
                      isCorrectValue = true;
                  }


              }
          }

          if (isCorrectValue)
              packet.ValidFrom_XFA_ABONNET_STATION(dateIssue, Documents.DocType.DOZV);


      }


      private void EMCMenuItem_Click(object sender, EventArgs e)
      {
          if (packet.GelListIdSelectedAppl().Length == 0)
          {
              MessageBox.Show(CLocaliz.TxT("Please, select application."), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Error);
              return;
          }
          tbOwner.Focus();
          //Виведення діалогового вікна для введення дати видачі
          bool isRepeat = true;
          bool isCorrectValue = false;
          while (isRepeat)
          {
              isRepeat = false;
              isCorrectValue = false;
              IMDialog dialogDate = new IMDialog();
              dialogDate.Add("txtDateName", "", dateIssue, CLocaliz.TxT("Date of issue"));
              if (dialogDate.Enter())
              {
                  dateIssue = dialogDate.Get("txtDateName").ToString().ToDataTime();
                  if (dateIssue.Date < DateTime.Now.Date)
                  {

                      if (MessageBox.Show(string.Format("Перевірте правильність внесення дати видачі. Дата  '{0}' коректна?", dateIssue.Date.ToShortDateString()), "",
                                      MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Question) == DialogResult.Yes)
                      {
                          isCorrectValue = true;
                      }
                      else
                      {
                          isRepeat = true;
                      }
                  }
                  else
                  {
                      isCorrectValue = true;
                  }
              }
          }

          if (isCorrectValue)
              packet.ValidFrom_XFA_ABONNET_STATION(dateIssue, Documents.DocType.VISN);
      }

      private void contextMenuStrip_Grid_Opening(object sender, CancelEventArgs e)
      {

      }

      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public AppType GetAppTypePacket()
      {
          AppType AppTypePacket = AppType.AppUnknown;
          using (IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly))
          {
              rsPacket.Select("ID,TYPE");
              rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packet.PacketId);
              try
              {
                  rsPacket.Open();
                  if (!rsPacket.IsEOF())
                  {
                      AppTypePacket = appTypeConvert.StringToAppType(rsPacket.GetS("TYPE"));
                  }
              }
              catch (Exception ex) { }
              rsPacket.Close();
          }
          return AppTypePacket;
      }

      private void toolStrip_OpenDesigner_Click(object sender, EventArgs e)
      {
          using (FChannels fChannel = new FChannels("Вибір стовпця", "Перелік стовпців"))
          {
              List<ExtendedColumnsProperty> porp_column_all = packet.ListAllColumns.FindAll(r => r.TypeRadiosys == GetAppTypePacket() || ((r.TypeRadiosys == AppType.AppUnknown) && (r.Number_List == 1)));
              if (porp_column_all != null)
              {
                  foreach (ExtendedColumnsProperty item in porp_column_all)
                  {
                      fChannel.CLBAllChannel.Items.Add(item);
                  }
              }
              if (packet.ListSelectedColumns != null)
              {
                  foreach (ExtendedColumnsProperty item in packet.ListSelectedColumns)
                  {
                      fChannel.CLBSelectChannel.Items.Add(item);
                  }
              }
              if (fChannel.CLBSelectChannel.Items.Count > 0)
              {
                  foreach (ExtendedColumnsProperty item in fChannel.CLBSelectChannel.Items)
                  {
                      if (porp_column_all.Find(r => r.TypeRadiosys == item.TypeRadiosys && r.CaptionProperty == item.CaptionProperty && r.NameProperty == item.NameProperty && r.PathToColumn == item.PathToColumn) != null)
                      {
                          ExtendedColumnsProperty bnd = porp_column_all.Find(r => r.TypeRadiosys == item.TypeRadiosys && r.CaptionProperty == item.CaptionProperty && r.NameProperty == item.NameProperty && r.PathToColumn == item.PathToColumn);
                          fChannel.CLBAllChannel.Items.Remove(bnd);
                      }
                  }
              }
              if (fChannel.ShowDialog() == DialogResult.OK)
              {
                  packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                  if (fChannel.CLBSelectChannel.Items.Count > 0)
                  {
                      for (int i = 0; i < fChannel.CLBSelectChannel.Items.Count; i++)
                      {
                          packet.ListSelectedColumns.Add(((ExtendedColumnsProperty)(fChannel.CLBSelectChannel.Items[i])));
                      }
                  }
                  else
                  {
                      packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                  }
              }
          }
          SaveComponentSettings();
          InitApplGrid();
      }


      /// <summary>
      /// Загрузка структуры
      /// </summary>
      public void LoadComponentSettings()
      {
          try
          {
              AppType TypePacket = GetAppTypePacket();
              List<ColumnInfoGridView> L_FND_MAIN = _settings.ColumnInfo.FindAll(r => r.appType == AppType.AppUnknown);
              if (L_FND_MAIN != null)
              {
                  L_FND_MAIN.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                  /*
                      foreach (ExtendedColumnsProperty rx in packet.ListSelectedColumns) {
                             bool isCheck = false;
                             foreach (ColumnInfoGridView colInfo in L_FND_MAIN) {
                                 if (rx.TypeRadiosys == AppType.AppUnknown && rx.Number_List == 1) {
                                     if (rx.PathToColumn==colInfo.Name) {
                                         isCheck = true;
                                     }
                                 }
                             }
                             if (isCheck==false)
                               packet.ListSelectedColumns.Remove(rx);
                      }
                   */



                  foreach (ColumnInfoGridView colInfo in L_FND_MAIN)
                  {
                      DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                      if (col != null) { col.Width = colInfo.Width; }
                      else
                      {
                          if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                          ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
                          if (Fnd != null)
                          {
                              if (!packet.ListSelectedColumns.Contains(Fnd))
                                  packet.ListSelectedColumns.Add(Fnd);
                          }
                      }
                  }
              }
              List<ColumnInfoGridView> L_FND = _settings.ColumnInfo.FindAll(r => r.appType == TypePacket);
              if (L_FND != null)
              {
                  L_FND.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                  foreach (ColumnInfoGridView colInfo in L_FND)
                  {
                      DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                      if (col != null) { col.Width = colInfo.Width; }
                      else
                      {
                          if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                          ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.NameProperty == colInfo.Name.Replace("grid", "") && r.TypeRadiosys == TypePacket);
                          if (Fnd != null)
                          {
                              if (!packet.ListSelectedColumns.Contains(Fnd))
                                  packet.ListSelectedColumns.Add(Fnd);
                          }
                          ExtendedColumnsProperty Fnd_unknown = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1);
                          if (Fnd_unknown == null)
                              AddGridColumn(null, colInfo.Name, CLocaliz.TxT(colInfo.Name), colInfo.Width, true, colInfo.Name);
                      }
                  }
                  int idx = 0;
                  foreach (ColumnInfoGridView colInfo in L_FND)
                  {
                      DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                      if (col != null)
                      {
                          col.Width = colInfo.Width;
                          if (colInfo.DisplayIndex <= (dataGridAppl.Columns.Count - 1))
                              col.DisplayIndex = colInfo.DisplayIndex;
                          else col.DisplayIndex = idx;
                      }
                      idx++;
                  }
                  if (packet.ListSelectedColumns != null)
                  {
                      if (packet.ListSelectedColumns.Count > 0)
                          packet.RefreshRowsSpecial(GetAppTypePacket());
                  }
              }
          }
          catch (Exception ex)
          {
              ResetComponentSettings();
          }
      }

      public void ResetComponentSettings()
      {
          //_settings.Reset();
          ClearComponentSetting();
      }

      public void ClearComponentSetting()
      {
          List<ColumnInfoGridView> L_Del = _settings.ColumnInfo.FindAll(r => r.appType == GetAppTypePacket() || (r.appType == AppType.AppUnknown));
          foreach (ColumnInfoGridView del in L_Del)
              _settings.ColumnInfo.Remove(del);
      }

      //Сохранение структуры
      public void SaveComponentSettings()
      {
          //_settings.ColumnInfo.Clear();
          AppType TypePacket = GetAppTypePacket();
          ClearComponentSetting();
          foreach (DataGridViewColumn col in dataGridAppl.Columns)
          {
              if (packet.ListSelectedColumns != null) {
                  if (((packet.ListSelectedColumns.Find(r => r.NameProperty == col.Name && r.TypeRadiosys == TypePacket) == null) && (packet.ListAllColumns.Find(r => r.NameProperty == col.Name && r.TypeRadiosys == TypePacket) != null) )) {
                      continue;
                  }
                  if ((packet.ListSelectedColumns.Find(r => r.PathToColumn == col.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1) == null) && (packet.ListAllColumns.Find(r => r.PathToColumn == col.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1) != null) )
                      continue;
              }
              ColumnInfoGridView colInfo = new ColumnInfoGridView();
              colInfo.Name = col.Name;
              colInfo.Width = col.Width;
              colInfo.appType = GetAppTypePacket();
              colInfo.DisplayIndex = col.DisplayIndex;
              //if (packet.ListSelectedColumns.Find(r => r.PathToColumn == col.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 1) != null)
                  //colInfo.appType = AppType.AppUnknown;
              if (ColumnsLstMain.Find(r => r.Name == col.Name) != null)
                  colInfo.appType = AppType.AppUnknown;
              _settings.ColumnInfo.Add(colInfo);
          }
          _settings.Save();
      }

      private void FPacket_FormClosing_2(object sender, FormClosingEventArgs e)
      {
          SaveComponentSettings();
      }

      private void resetSettingsToolStripMenuItem_Click(object sender, EventArgs e)
      {
          DialogResult DA = MessageBox.Show("Очистити попередні налагодження відображення пекетних даних? (Так/Ні)", "", MessageBoxButtons.YesNo);
          if (DA == System.Windows.Forms.DialogResult.Yes)
          {
              ResetComponentSettings();
              if (packet.ListSelectedColumns != null)
                  packet.ListSelectedColumns.Clear();
              packet.isDefaultListColumns = true;
              InitApplGrid();
              if (packet.ListSelectedColumns != null)
              {
                  if (packet.ListSelectedColumns.Count > 0)
                      packet.RefreshRowsSpecial(GetAppTypePacket());
              }
          }
      }
     

   }

   public class ColumnInfoGridView
   {
       public ColumnInfoGridView() { }
       public String Name;         // имя столбца
       public Int32 Width;         // ширина столбца
       public Int32 DisplayIndex;  // порядок расположения
       public AppType appType;     // тип заявки

   }

   public class SettingsGrid : ApplicationSettingsBase
   {
       public SettingsGrid(string settingsKey) : base(settingsKey) { }

       [UserScopedSetting()]
       [DefaultSettingValueAttribute("")]
       public List<ColumnInfoGridView> ColumnInfo
       {
           get { return (List<ColumnInfoGridView>)this["ColumnInfo"]; }
           set { this["ColumnInfo"] = value; }
       }
   }

    public class LinkDocFiles
    {
        public string DOC_TYPE { get; set; }
        public string DOC_REF { get; set; }
        public DateTime DOC_DATE { get; set; }
        public DateTime EXP_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string PATH { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public int ID { get; set; }
    }

    
}
