﻿namespace XICSM.UcrfRfaNET.AppPacket.Forms
{
    partial class PrintErrorListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblDescription;
            this.lvErrorList = new System.Windows.Forms.ListView();
            this.errorDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            lblDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDescription
            // 
            lblDescription.AutoSize = true;
            lblDescription.Location = new System.Drawing.Point(9, 6);
            lblDescription.Name = "lblDescription";
            lblDescription.Size = new System.Drawing.Size(244, 13);
            lblDescription.TabIndex = 1;
            lblDescription.Text = "Під час друкування виникли наступні помилки:";
            // 
            // lvErrorList
            // 
            this.lvErrorList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvErrorList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.errorDescription});
            this.lvErrorList.FullRowSelect = true;
            this.lvErrorList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvErrorList.HideSelection = false;
            this.lvErrorList.Location = new System.Drawing.Point(12, 25);
            this.lvErrorList.Name = "lvErrorList";
            this.lvErrorList.Size = new System.Drawing.Size(476, 260);
            this.lvErrorList.TabIndex = 0;
            this.lvErrorList.UseCompatibleStateImageBehavior = false;
            this.lvErrorList.View = System.Windows.Forms.View.Details;
            // 
            // errorDescription
            // 
            this.errorDescription.Text = "Опис помилки";
            this.errorDescription.Width = 379;
            // 
            // PrintErrorListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 297);
            this.Controls.Add(lblDescription);
            this.Controls.Add(this.lvErrorList);
            this.MinimizeBox = false;
            this.Name = "PrintErrorListForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Список помилок";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader errorDescription;
        public System.Windows.Forms.ListView lvErrorList;
    }
}