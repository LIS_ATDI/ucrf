﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.AppPacket.Forms
{
    public partial class PrintForm : Form
    {
        private CPacket _packet;
        public String BlankNumber { get; set; }

        public PrintForm(CPacket packet)
        {
            _packet = packet;
            InitializeComponent();
        }

        private void rbPrintOneByOne_CheckedChanged(object sender, EventArgs e)
        {
            tbBlankNumber.Enabled = !rbPrintOneByOne.Checked;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cbDocType.SelectedIndex == -1)
            {
                MessageBox.Show("Тип документу має бути вказаний", CLocaliz.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbDocType.Focus();
                return;
            }
            List<string> _errorList = new List<string>();
            _errorList = _packet.Print(BlankNumber, true, this, !rbPrintOneByOne.Checked, cbDocType.SelectedIndex == 0 ? "DOZV" : "VISN");                
            
            if (_errorList.Count > 0)
            {
                PrintErrorListForm errorForm = new PrintErrorListForm();
                errorForm.Owner = this;
                foreach(string error in _errorList)
                {
                    errorForm.lvErrorList.Items.Add(error);
                }
                errorForm.ShowDialog();
            }
            Close();
        }

        private void PrintForm_Load(object sender, EventArgs e)
        {
            tbBlankNumber.DataBindings.Add("Text", this, "BlankNumber");
        }
    }
}
