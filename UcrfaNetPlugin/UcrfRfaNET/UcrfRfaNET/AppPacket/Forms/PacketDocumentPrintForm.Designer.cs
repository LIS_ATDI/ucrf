﻿namespace XICSM.UcrfRfaNET.AppPacket.Forms
{
    partial class PacketDocumentPrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblBlankNumber;
            this.lblPrintSymbol = new System.Windows.Forms.Label();
            this.tbBlankNumber = new System.Windows.Forms.TextBox();
            this.btnView = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            lblBlankNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBlankNumber
            // 
            lblBlankNumber.AutoSize = true;
            lblBlankNumber.Location = new System.Drawing.Point(85, 71);
            lblBlankNumber.Name = "lblBlankNumber";
            lblBlankNumber.Size = new System.Drawing.Size(82, 13);
            lblBlankNumber.TabIndex = 1;
            lblBlankNumber.Text = "Номер бланку:";
            // 
            // lblPrintSymbol
            // 
            this.lblPrintSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPrintSymbol.Location = new System.Drawing.Point(12, 9);
            this.lblPrintSymbol.Name = "lblPrintSymbol";
            this.lblPrintSymbol.Size = new System.Drawing.Size(341, 44);
            this.lblPrintSymbol.TabIndex = 0;
            this.lblPrintSymbol.Text = "Друк Дозволу № KKKK-RR-NNNNN";
            this.lblPrintSymbol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbBlankNumber
            // 
            this.tbBlankNumber.Location = new System.Drawing.Point(173, 68);
            this.tbBlankNumber.Name = "tbBlankNumber";
            this.tbBlankNumber.Size = new System.Drawing.Size(106, 20);
            this.tbBlankNumber.TabIndex = 2;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(15, 120);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "Перегляд";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPrint.Location = new System.Drawing.Point(96, 120);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "Друк";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(278, 120);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Скасувати";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PacketDocumentPrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 160);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.tbBlankNumber);
            this.Controls.Add(lblBlankNumber);
            this.Controls.Add(this.lblPrintSymbol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PacketDocumentPrintForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Друк пакету документу";
            this.Load += new System.EventHandler(this.PacketDocumentPrintForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrintSymbol;
        private System.Windows.Forms.TextBox tbBlankNumber;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCancel;
    }
}