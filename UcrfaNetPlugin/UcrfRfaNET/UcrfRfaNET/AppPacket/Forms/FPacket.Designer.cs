﻿namespace XICSM.UcrfRfaNET
{
   partial class FPacket
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacket));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbApplList = new System.Windows.Forms.GroupBox();
            this.dataGridAppl = new System.Windows.Forms.DataGridView();
            this.buttonNewAppl = new System.Windows.Forms.Button();
            this.buttonSelectAppl = new System.Windows.Forms.Button();
            this.buttonDeleteAppl = new System.Windows.Forms.Button();
            this.buttonPacketPrint = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.tbOwner = new System.Windows.Forms.TextBox();
            this.cbPacketContent = new System.Windows.Forms.ComboBox();
            this.tbInNumber = new System.Windows.Forms.TextBox();
            this.tbOutNumber = new System.Windows.Forms.TextBox();
            this.cbStandardNew = new System.Windows.Forms.ComboBox();
            this.cbPacketType = new System.Windows.Forms.ComboBox();
            this.dateInPacket = new System.Windows.Forms.DateTimePicker();
            this.dateOutPacket = new System.Windows.Forms.DateTimePicker();
            this.menuPacket = new System.Windows.Forms.MenuStrip();
            this.mainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.DRVMemo = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowAllApplToDrvFromUrcm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mniUpdateLicList = new System.Windows.Forms.ToolStripMenuItem();
            this.PrintPermissionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.AddDateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddDateMenuItemAbonent = new System.Windows.Forms.ToolStripMenuItem();
            this.EMCMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DozvMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemPrintDozv = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemVISN_ems = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDOZV = new System.Windows.Forms.ToolStripMenuItem();
            this.formOfControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urzpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.службоваЗапискаДоДРВToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowAllApplToDrvFromUrzp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSendToUrcp = new System.Windows.Forms.ToolStripMenuItem();
            this.PrintEmptyPtk = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonAttachDoc = new System.Windows.Forms.Button();
            this.panelParametrs = new System.Windows.Forms.Panel();
            this.tabControlLinkFiles = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnViewLinkFiles = new System.Windows.Forms.Panel();
            this.dataGridViewLinkFiles = new System.Windows.Forms.DataGridView();
            this.DOC_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOC_REF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOC_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATED_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuAdditional = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnCheckDates = new System.Windows.Forms.Panel();
            this.lblDays = new System.Windows.Forms.Label();
            this.lblTrCount = new System.Windows.Forms.Label();
            this.lblDates = new System.Windows.Forms.Label();
            this.tbDate5 = new System.Windows.Forms.TextBox();
            this.tbDate25 = new System.Windows.Forms.TextBox();
            this.tbDate35 = new System.Windows.Forms.TextBox();
            this.tbDate45 = new System.Windows.Forms.TextBox();
            this.tbDays5 = new System.Windows.Forms.TextBox();
            this.tbDays35 = new System.Windows.Forms.TextBox();
            this.tbDays25 = new System.Windows.Forms.TextBox();
            this.tbDays45 = new System.Windows.Forms.TextBox();
            this.tbCounter = new System.Windows.Forms.TextBox();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl25 = new System.Windows.Forms.Label();
            this.lbl35 = new System.Windows.Forms.Label();
            this.lbl45 = new System.Windows.Forms.Label();
            this.txtBoxStatus = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.panelButton = new System.Windows.Forms.Panel();
            this.buttonRetriveAll = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip_Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStrip_OpenDesigner = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbApplList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAppl)).BeginInit();
            this.menuPacket.SuspendLayout();
            this.panelParametrs.SuspendLayout();
            this.tabControlLinkFiles.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnViewLinkFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLinkFiles)).BeginInit();
            this.contextMenuAdditional.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnCheckDates.SuspendLayout();
            this.panelButton.SuspendLayout();
            this.contextMenuStrip_Grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Packet owner";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(8, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Packet content";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "In packet number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Out packet number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(294, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(294, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(8, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Packet type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(226, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Standard";
            // 
            // gbApplList
            // 
            this.gbApplList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbApplList.Controls.Add(this.dataGridAppl);
            this.gbApplList.Location = new System.Drawing.Point(19, 245);
            this.gbApplList.Name = "gbApplList";
            this.gbApplList.Size = new System.Drawing.Size(905, 236);
            this.gbApplList.TabIndex = 16;
            this.gbApplList.TabStop = false;
            this.gbApplList.Text = "Packet\'s documents";
            // 
            // dataGridAppl
            // 
            this.dataGridAppl.AllowUserToAddRows = false;
            this.dataGridAppl.AllowUserToDeleteRows = false;
            this.dataGridAppl.AllowUserToOrderColumns = true;
            this.dataGridAppl.AllowUserToResizeRows = false;
            this.dataGridAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridAppl.BackgroundColor = System.Drawing.Color.White;
            this.dataGridAppl.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridAppl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridAppl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAppl.Location = new System.Drawing.Point(6, 19);
            this.dataGridAppl.MultiSelect = false;
            this.dataGridAppl.Name = "dataGridAppl";
            this.dataGridAppl.RowHeadersVisible = false;
            this.dataGridAppl.RowHeadersWidth = 15;
            this.dataGridAppl.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridAppl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridAppl.Size = new System.Drawing.Size(893, 211);
            this.dataGridAppl.TabIndex = 0;
            this.dataGridAppl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridAppl_CellClick);
            this.dataGridAppl.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridAppl_CellMouseDoubleClick);
            this.dataGridAppl.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridAppl_CellValueChanged);
            this.dataGridAppl.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridAppl_ColumnHeaderMouseClick);
            this.dataGridAppl.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridAppl_DataBindingComplete);
            this.dataGridAppl.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridAppl_DataError);
            // 
            // buttonNewAppl
            // 
            this.buttonNewAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNewAppl.Location = new System.Drawing.Point(6, 10);
            this.buttonNewAppl.Name = "buttonNewAppl";
            this.buttonNewAppl.Size = new System.Drawing.Size(100, 23);
            this.buttonNewAppl.TabIndex = 18;
            this.buttonNewAppl.Text = "New appl";
            this.buttonNewAppl.UseVisualStyleBackColor = true;
            this.buttonNewAppl.Click += new System.EventHandler(this.buttonNewAppl_Click);
            // 
            // buttonSelectAppl
            // 
            this.buttonSelectAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSelectAppl.Enabled = false;
            this.buttonSelectAppl.Location = new System.Drawing.Point(120, 10);
            this.buttonSelectAppl.Name = "buttonSelectAppl";
            this.buttonSelectAppl.Size = new System.Drawing.Size(100, 23);
            this.buttonSelectAppl.TabIndex = 19;
            this.buttonSelectAppl.Text = "Select appl";
            this.buttonSelectAppl.UseVisualStyleBackColor = true;
            this.buttonSelectAppl.Click += new System.EventHandler(this.buttonSelectAppl_Click);
            // 
            // buttonDeleteAppl
            // 
            this.buttonDeleteAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeleteAppl.Location = new System.Drawing.Point(229, 10);
            this.buttonDeleteAppl.Name = "buttonDeleteAppl";
            this.buttonDeleteAppl.Size = new System.Drawing.Size(100, 23);
            this.buttonDeleteAppl.TabIndex = 20;
            this.buttonDeleteAppl.Text = "Delete appl";
            this.buttonDeleteAppl.UseVisualStyleBackColor = true;
            this.buttonDeleteAppl.Click += new System.EventHandler(this.buttonDeleteAppl_Click);
            // 
            // buttonPacketPrint
            // 
            this.buttonPacketPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPacketPrint.Location = new System.Drawing.Point(338, 10);
            this.buttonPacketPrint.Name = "buttonPacketPrint";
            this.buttonPacketPrint.Size = new System.Drawing.Size(100, 23);
            this.buttonPacketPrint.TabIndex = 21;
            this.buttonPacketPrint.Text = "Packet print";
            this.buttonPacketPrint.UseVisualStyleBackColor = true;
            this.buttonPacketPrint.Click += new System.EventHandler(this.buttonPacketPrint_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(764, 10);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(130, 23);
            this.buttonSave.TabIndex = 17;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // tbOwner
            // 
            this.tbOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOwner.Location = new System.Drawing.Point(128, 13);
            this.tbOwner.Name = "tbOwner";
            this.tbOwner.Size = new System.Drawing.Size(761, 20);
            this.tbOwner.TabIndex = 1;
            this.tbOwner.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbOwner_KeyDown);
            // 
            // cbPacketContent
            // 
            this.cbPacketContent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPacketContent.FormattingEnabled = true;
            this.cbPacketContent.Location = new System.Drawing.Point(128, 39);
            this.cbPacketContent.Name = "cbPacketContent";
            this.cbPacketContent.Size = new System.Drawing.Size(342, 21);
            this.cbPacketContent.TabIndex = 3;
            this.cbPacketContent.SelectedValueChanged += new System.EventHandler(this.cbPacketContent_SelectedValueChanged);
            // 
            // tbInNumber
            // 
            this.tbInNumber.Location = new System.Drawing.Point(128, 93);
            this.tbInNumber.MaxLength = 40;
            this.tbInNumber.Name = "tbInNumber";
            this.tbInNumber.Size = new System.Drawing.Size(160, 20);
            this.tbInNumber.TabIndex = 9;
            this.tbInNumber.Validating += new System.ComponentModel.CancelEventHandler(this.tbInNumber_Validating);
            // 
            // tbOutNumber
            // 
            this.tbOutNumber.Location = new System.Drawing.Point(128, 66);
            this.tbOutNumber.MaxLength = 40;
            this.tbOutNumber.Name = "tbOutNumber";
            this.tbOutNumber.Size = new System.Drawing.Size(160, 20);
            this.tbOutNumber.TabIndex = 5;
            this.tbOutNumber.Validating += new System.ComponentModel.CancelEventHandler(this.tbOutNumber_Validating);
            // 
            // cbStandardNew
            // 
            this.cbStandardNew.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStandardNew.DropDownWidth = 50;
            this.cbStandardNew.FormattingEnabled = true;
            this.cbStandardNew.Location = new System.Drawing.Point(338, 118);
            this.cbStandardNew.Name = "cbStandardNew";
            this.cbStandardNew.Size = new System.Drawing.Size(132, 21);
            this.cbStandardNew.TabIndex = 15;
            // 
            // cbPacketType
            // 
            this.cbPacketType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPacketType.FormattingEnabled = true;
            this.cbPacketType.Location = new System.Drawing.Point(128, 118);
            this.cbPacketType.Name = "cbPacketType";
            this.cbPacketType.Size = new System.Drawing.Size(92, 21);
            this.cbPacketType.TabIndex = 13;
            this.cbPacketType.SelectedValueChanged += new System.EventHandler(this.cbPacketType_SelectedValueChanged);
            // 
            // dateInPacket
            // 
            this.dateInPacket.Location = new System.Drawing.Point(338, 92);
            this.dateInPacket.Name = "dateInPacket";
            this.dateInPacket.Size = new System.Drawing.Size(132, 20);
            this.dateInPacket.TabIndex = 11;
            this.dateInPacket.Validating += new System.ComponentModel.CancelEventHandler(this.dateInPacket_Validating);
            // 
            // dateOutPacket
            // 
            this.dateOutPacket.Location = new System.Drawing.Point(338, 66);
            this.dateOutPacket.Name = "dateOutPacket";
            this.dateOutPacket.Size = new System.Drawing.Size(132, 20);
            this.dateOutPacket.TabIndex = 7;
            this.dateOutPacket.Validating += new System.ComponentModel.CancelEventHandler(this.dateOutPacket_Validating);
            // 
            // menuPacket
            // 
            this.menuPacket.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenu,
            this.urzpMenu});
            this.menuPacket.Location = new System.Drawing.Point(0, 0);
            this.menuPacket.Name = "menuPacket";
            this.menuPacket.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuPacket.Size = new System.Drawing.Size(936, 24);
            this.menuPacket.TabIndex = 22;
            this.menuPacket.Text = "menuPacket";
            // 
            // mainMenu
            // 
            this.mainMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DRVMemo,
            this.ShowAllApplToDrvFromUrcm,
            this.toolStripMenuItem1,
            this.mniUpdateLicList,
            this.PrintPermissionsMenuItem,
            this.toolStripMenuItem2,
            this.AddDateMenuItem,
            this.AddDateMenuItemAbonent,
            this.MenuItemPrintDozv,
            this.formOfControlToolStripMenuItem});
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(50, 20);
            this.mainMenu.Text = "УРЧП";
            // 
            // DRVMemo
            // 
            this.DRVMemo.Name = "DRVMemo";
            this.DRVMemo.Size = new System.Drawing.Size(285, 22);
            this.DRVMemo.Text = "Службова записка до ДРВ";
            this.DRVMemo.Click += new System.EventHandler(this.DRVMemo_Click);
            // 
            // ShowAllApplToDrvFromUrcm
            // 
            this.ShowAllApplToDrvFromUrcm.Name = "ShowAllApplToDrvFromUrcm";
            this.ShowAllApplToDrvFromUrcm.Size = new System.Drawing.Size(285, 22);
            this.ShowAllApplToDrvFromUrcm.Text = "Показати всі службові записки до ДРВ";
            this.ShowAllApplToDrvFromUrcm.Click += new System.EventHandler(this.ShowAllApplToDrvFromUrcm_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(282, 6);
            // 
            // mniUpdateLicList
            // 
            this.mniUpdateLicList.Name = "mniUpdateLicList";
            this.mniUpdateLicList.Size = new System.Drawing.Size(285, 22);
            this.mniUpdateLicList.Text = "Оновити список ліцензій";
            this.mniUpdateLicList.Click += new System.EventHandler(this.mniUpdateLicList_Click);
            // 
            // PrintPermissionsMenuItem
            // 
            this.PrintPermissionsMenuItem.Name = "PrintPermissionsMenuItem";
            this.PrintPermissionsMenuItem.Size = new System.Drawing.Size(285, 22);
            this.PrintPermissionsMenuItem.Text = "Друкувати дозвільні документи";
            this.PrintPermissionsMenuItem.Click += new System.EventHandler(this.PrintPermissionsMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(282, 6);
            // 
            // AddDateMenuItem
            // 
            this.AddDateMenuItem.Enabled = false;
            this.AddDateMenuItem.Name = "AddDateMenuItem";
            this.AddDateMenuItem.Size = new System.Drawing.Size(285, 22);
            this.AddDateMenuItem.Text = "Внести дату видачі";
            this.AddDateMenuItem.Visible = false;
            this.AddDateMenuItem.Click += new System.EventHandler(this.AddDateMenuItem_Click);
            // 
            // AddDateMenuItemAbonent
            // 
            this.AddDateMenuItemAbonent.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EMCMenuItem,
            this.DozvMenuItem});
            this.AddDateMenuItemAbonent.Name = "AddDateMenuItemAbonent";
            this.AddDateMenuItemAbonent.Size = new System.Drawing.Size(285, 22);
            this.AddDateMenuItemAbonent.Text = "Внести дату видачі";
            this.AddDateMenuItemAbonent.Click += new System.EventHandler(this.AddDateMenuItemAbonent_Click);
            // 
            // EMCMenuItem
            // 
            this.EMCMenuItem.Name = "EMCMenuItem";
            this.EMCMenuItem.Size = new System.Drawing.Size(214, 22);
            this.EMCMenuItem.Text = "Висновку щодо ЕМС";
            this.EMCMenuItem.Click += new System.EventHandler(this.EMCMenuItem_Click);
            // 
            // DozvMenuItem
            // 
            this.DozvMenuItem.Name = "DozvMenuItem";
            this.DozvMenuItem.Size = new System.Drawing.Size(214, 22);
            this.DozvMenuItem.Text = "Дозволу на експлуатацію";
            this.DozvMenuItem.Click += new System.EventHandler(this.DozvMenuItem_Click);
            // 
            // MenuItemPrintDozv
            // 
            this.MenuItemPrintDozv.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemVISN_ems,
            this.MenuItemDOZV});
            this.MenuItemPrintDozv.Name = "MenuItemPrintDozv";
            this.MenuItemPrintDozv.Size = new System.Drawing.Size(285, 22);
            this.MenuItemPrintDozv.Text = "Особливі умови";
            this.MenuItemPrintDozv.ToolTipText = "Викликає механізм автоматизованого формування особливих умов щодо ЕМС або Дозволу" +
    " для вибраних заяв у пакеті";
            // 
            // MenuItemVISN_ems
            // 
            this.MenuItemVISN_ems.Name = "MenuItemVISN_ems";
            this.MenuItemVISN_ems.Size = new System.Drawing.Size(307, 22);
            this.MenuItemVISN_ems.Text = "Особливі умови Висновку щодо ЕМС";
            this.MenuItemVISN_ems.ToolTipText = "Автоматизоване(пакетне) формування особливих умов щодо ЕМС";
            this.MenuItemVISN_ems.Click += new System.EventHandler(this.MenuItemVISN_ems_Click);
            // 
            // MenuItemDOZV
            // 
            this.MenuItemDOZV.Name = "MenuItemDOZV";
            this.MenuItemDOZV.Size = new System.Drawing.Size(307, 22);
            this.MenuItemDOZV.Text = "Особливі умови Дозволу на експлуатацію";
            this.MenuItemDOZV.ToolTipText = "Автоматизоване(пакетне) формування особливих умов щодо Дозволу";
            this.MenuItemDOZV.Click += new System.EventHandler(this.MenuItemDOZV_Click);
            // 
            // formOfControlToolStripMenuItem
            // 
            this.formOfControlToolStripMenuItem.Name = "formOfControlToolStripMenuItem";
            this.formOfControlToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.formOfControlToolStripMenuItem.Text = "Form of control";
            this.formOfControlToolStripMenuItem.Click += new System.EventHandler(this.formOfControlToolStripMenuItem_Click);
            // 
            // urzpMenu
            // 
            this.urzpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.службоваЗапискаДоДРВToolStripMenuItem,
            this.ShowAllApplToDrvFromUrzp,
            this.menuSendToUrcp,
            this.PrintEmptyPtk});
            this.urzpMenu.Name = "urzpMenu";
            this.urzpMenu.Size = new System.Drawing.Size(49, 20);
            this.urzpMenu.Text = "УРЗП";
            // 
            // службоваЗапискаДоДРВToolStripMenuItem
            // 
            this.службоваЗапискаДоДРВToolStripMenuItem.Name = "службоваЗапискаДоДРВToolStripMenuItem";
            this.службоваЗапискаДоДРВToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.службоваЗапискаДоДРВToolStripMenuItem.Text = "Службова записка до ДРВ";
            this.службоваЗапискаДоДРВToolStripMenuItem.Click += new System.EventHandler(this.URZPmemoToDRV);
            // 
            // ShowAllApplToDrvFromUrzp
            // 
            this.ShowAllApplToDrvFromUrzp.Name = "ShowAllApplToDrvFromUrzp";
            this.ShowAllApplToDrvFromUrzp.Size = new System.Drawing.Size(285, 22);
            this.ShowAllApplToDrvFromUrzp.Text = "Показати всі службові записки до ДРВ";
            this.ShowAllApplToDrvFromUrzp.Click += new System.EventHandler(this.ShowAllApplToDrvFromUrzp_Click);
            // 
            // menuSendToUrcp
            // 
            this.menuSendToUrcp.Name = "menuSendToUrcp";
            this.menuSendToUrcp.Size = new System.Drawing.Size(285, 22);
            this.menuSendToUrcp.Text = "Передано до УРЧП";
            this.menuSendToUrcp.Click += new System.EventHandler(this.menuSendToUrcp_Click);
            // 
            // PrintEmptyPtk
            // 
            this.PrintEmptyPtk.Name = "PrintEmptyPtk";
            this.PrintEmptyPtk.Size = new System.Drawing.Size(285, 22);
            this.PrintEmptyPtk.Text = "Дані для наповнення акту ПТК";
            this.PrintEmptyPtk.Click += new System.EventHandler(this.PrintEmptyPtk_Click);
            // 
            // buttonAttachDoc
            // 
            this.buttonAttachDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAttachDoc.Location = new System.Drawing.Point(444, 10);
            this.buttonAttachDoc.Name = "buttonAttachDoc";
            this.buttonAttachDoc.Size = new System.Drawing.Size(100, 23);
            this.buttonAttachDoc.TabIndex = 22;
            this.buttonAttachDoc.Text = "Attach doc";
            this.buttonAttachDoc.UseVisualStyleBackColor = true;
            this.buttonAttachDoc.Click += new System.EventHandler(this.buttonAttachDoc_Click);
            // 
            // panelParametrs
            // 
            this.panelParametrs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelParametrs.Controls.Add(this.tabControlLinkFiles);
            this.panelParametrs.Controls.Add(this.txtBoxStatus);
            this.panelParametrs.Controls.Add(this.lblStatus);
            this.panelParametrs.Controls.Add(this.tbOwner);
            this.panelParametrs.Controls.Add(this.label1);
            this.panelParametrs.Controls.Add(this.dateInPacket);
            this.panelParametrs.Controls.Add(this.label2);
            this.panelParametrs.Controls.Add(this.dateOutPacket);
            this.panelParametrs.Controls.Add(this.label4);
            this.panelParametrs.Controls.Add(this.label3);
            this.panelParametrs.Controls.Add(this.label6);
            this.panelParametrs.Controls.Add(this.label5);
            this.panelParametrs.Controls.Add(this.label7);
            this.panelParametrs.Controls.Add(this.cbPacketType);
            this.panelParametrs.Controls.Add(this.label8);
            this.panelParametrs.Controls.Add(this.tbInNumber);
            this.panelParametrs.Controls.Add(this.cbPacketContent);
            this.panelParametrs.Controls.Add(this.cbStandardNew);
            this.panelParametrs.Controls.Add(this.tbOutNumber);
            this.panelParametrs.Location = new System.Drawing.Point(19, 27);
            this.panelParametrs.Name = "panelParametrs";
            this.panelParametrs.Size = new System.Drawing.Size(905, 212);
            this.panelParametrs.TabIndex = 23;
            // 
            // tabControlLinkFiles
            // 
            this.tabControlLinkFiles.Controls.Add(this.tabPage1);
            this.tabControlLinkFiles.Controls.Add(this.tabPage2);
            this.tabControlLinkFiles.Location = new System.Drawing.Point(487, 69);
            this.tabControlLinkFiles.Name = "tabControlLinkFiles";
            this.tabControlLinkFiles.SelectedIndex = 0;
            this.tabControlLinkFiles.Size = new System.Drawing.Size(416, 140);
            this.tabControlLinkFiles.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnViewLinkFiles);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(408, 114);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Attached Docs";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnViewLinkFiles
            // 
            this.pnViewLinkFiles.AutoScroll = true;
            this.pnViewLinkFiles.AutoSize = true;
            this.pnViewLinkFiles.Controls.Add(this.dataGridViewLinkFiles);
            this.pnViewLinkFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnViewLinkFiles.Location = new System.Drawing.Point(3, 3);
            this.pnViewLinkFiles.Name = "pnViewLinkFiles";
            this.pnViewLinkFiles.Size = new System.Drawing.Size(402, 108);
            this.pnViewLinkFiles.TabIndex = 1;
            this.pnViewLinkFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pnViewLinkFiles_MouseDoubleClick);
            // 
            // dataGridViewLinkFiles
            // 
            this.dataGridViewLinkFiles.AllowUserToAddRows = false;
            this.dataGridViewLinkFiles.AllowUserToDeleteRows = false;
            this.dataGridViewLinkFiles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridViewLinkFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLinkFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DOC_TYPE,
            this.DOC_REF,
            this.DOC_DATE,
            this.CREATED_BY,
            this.ID,
            this.PATH});
            this.dataGridViewLinkFiles.ContextMenuStrip = this.contextMenuAdditional;
            this.dataGridViewLinkFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewLinkFiles.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewLinkFiles.MultiSelect = false;
            this.dataGridViewLinkFiles.Name = "dataGridViewLinkFiles";
            this.dataGridViewLinkFiles.ReadOnly = true;
            this.dataGridViewLinkFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLinkFiles.Size = new System.Drawing.Size(402, 108);
            this.dataGridViewLinkFiles.TabIndex = 0;
            this.dataGridViewLinkFiles.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewLinkFiles_CellMouseUp);
            this.dataGridViewLinkFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewLinkFiles_MouseDoubleClick);
            // 
            // DOC_TYPE
            // 
            this.DOC_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DOC_TYPE.HeaderText = "DOC_TYPE";
            this.DOC_TYPE.Name = "DOC_TYPE";
            this.DOC_TYPE.ReadOnly = true;
            // 
            // DOC_REF
            // 
            this.DOC_REF.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DOC_REF.HeaderText = "DOC_REF";
            this.DOC_REF.Name = "DOC_REF";
            this.DOC_REF.ReadOnly = true;
            // 
            // DOC_DATE
            // 
            this.DOC_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DOC_DATE.HeaderText = "DOC_DATE";
            this.DOC_DATE.Name = "DOC_DATE";
            this.DOC_DATE.ReadOnly = true;
            // 
            // CREATED_BY
            // 
            this.CREATED_BY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CREATED_BY.HeaderText = "CREATED_BY";
            this.CREATED_BY.Name = "CREATED_BY";
            this.CREATED_BY.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // PATH
            // 
            this.PATH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PATH.HeaderText = "PATH";
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Visible = false;
            // 
            // contextMenuAdditional
            // 
            this.contextMenuAdditional.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editRecordToolStripMenuItem});
            this.contextMenuAdditional.Name = "contextMenuAdditional";
            this.contextMenuAdditional.Size = new System.Drawing.Size(132, 26);
            // 
            // editRecordToolStripMenuItem
            // 
            this.editRecordToolStripMenuItem.Name = "editRecordToolStripMenuItem";
            this.editRecordToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.editRecordToolStripMenuItem.Text = "Edit record";
            this.editRecordToolStripMenuItem.Click += new System.EventHandler(this.editRecordToolStripMenuItem_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnCheckDates);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(408, 114);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "URZP Panel";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnCheckDates
            // 
            this.pnCheckDates.Controls.Add(this.lblDays);
            this.pnCheckDates.Controls.Add(this.lblTrCount);
            this.pnCheckDates.Controls.Add(this.lblDates);
            this.pnCheckDates.Controls.Add(this.tbDate5);
            this.pnCheckDates.Controls.Add(this.tbDate25);
            this.pnCheckDates.Controls.Add(this.tbDate35);
            this.pnCheckDates.Controls.Add(this.tbDate45);
            this.pnCheckDates.Controls.Add(this.tbDays5);
            this.pnCheckDates.Controls.Add(this.tbDays35);
            this.pnCheckDates.Controls.Add(this.tbDays25);
            this.pnCheckDates.Controls.Add(this.tbDays45);
            this.pnCheckDates.Controls.Add(this.tbCounter);
            this.pnCheckDates.Controls.Add(this.lbl5);
            this.pnCheckDates.Controls.Add(this.lbl25);
            this.pnCheckDates.Controls.Add(this.lbl35);
            this.pnCheckDates.Controls.Add(this.lbl45);
            this.pnCheckDates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCheckDates.Location = new System.Drawing.Point(3, 3);
            this.pnCheckDates.Name = "pnCheckDates";
            this.pnCheckDates.Size = new System.Drawing.Size(402, 108);
            this.pnCheckDates.TabIndex = 19;
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(4, 59);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(93, 13);
            this.lblDays.TabIndex = 55;
            this.lblDays.Text = "Залишилось днів";
            // 
            // lblTrCount
            // 
            this.lblTrCount.AutoSize = true;
            this.lblTrCount.Location = new System.Drawing.Point(210, 88);
            this.lblTrCount.Name = "lblTrCount";
            this.lblTrCount.Size = new System.Drawing.Size(107, 13);
            this.lblTrCount.TabIndex = 49;
            this.lblTrCount.Text = "Всього передавачів";
            // 
            // lblDates
            // 
            this.lblDates.AutoSize = true;
            this.lblDates.Location = new System.Drawing.Point(8, 32);
            this.lblDates.Name = "lblDates";
            this.lblDates.Size = new System.Drawing.Size(89, 13);
            this.lblDates.TabIndex = 54;
            this.lblDates.Text = "Контрольні дати";
            // 
            // tbDate5
            // 
            this.tbDate5.BackColor = System.Drawing.SystemColors.Window;
            this.tbDate5.Location = new System.Drawing.Point(103, 29);
            this.tbDate5.Name = "tbDate5";
            this.tbDate5.ReadOnly = true;
            this.tbDate5.Size = new System.Drawing.Size(68, 20);
            this.tbDate5.TabIndex = 41;
            this.tbDate5.TabStop = false;
            // 
            // tbDate25
            // 
            this.tbDate25.BackColor = System.Drawing.SystemColors.Window;
            this.tbDate25.Location = new System.Drawing.Point(177, 29);
            this.tbDate25.Name = "tbDate25";
            this.tbDate25.ReadOnly = true;
            this.tbDate25.Size = new System.Drawing.Size(67, 20);
            this.tbDate25.TabIndex = 42;
            this.tbDate25.TabStop = false;
            // 
            // tbDate35
            // 
            this.tbDate35.BackColor = System.Drawing.SystemColors.Window;
            this.tbDate35.Location = new System.Drawing.Point(250, 29);
            this.tbDate35.Name = "tbDate35";
            this.tbDate35.ReadOnly = true;
            this.tbDate35.Size = new System.Drawing.Size(67, 20);
            this.tbDate35.TabIndex = 43;
            this.tbDate35.TabStop = false;
            // 
            // tbDate45
            // 
            this.tbDate45.BackColor = System.Drawing.SystemColors.Window;
            this.tbDate45.Location = new System.Drawing.Point(323, 29);
            this.tbDate45.Name = "tbDate45";
            this.tbDate45.ReadOnly = true;
            this.tbDate45.Size = new System.Drawing.Size(69, 20);
            this.tbDate45.TabIndex = 44;
            this.tbDate45.TabStop = false;
            // 
            // tbDays5
            // 
            this.tbDays5.BackColor = System.Drawing.SystemColors.Window;
            this.tbDays5.Location = new System.Drawing.Point(103, 56);
            this.tbDays5.Name = "tbDays5";
            this.tbDays5.ReadOnly = true;
            this.tbDays5.Size = new System.Drawing.Size(68, 20);
            this.tbDays5.TabIndex = 45;
            this.tbDays5.TabStop = false;
            // 
            // tbDays35
            // 
            this.tbDays35.BackColor = System.Drawing.SystemColors.Window;
            this.tbDays35.Location = new System.Drawing.Point(250, 57);
            this.tbDays35.Name = "tbDays35";
            this.tbDays35.ReadOnly = true;
            this.tbDays35.Size = new System.Drawing.Size(67, 20);
            this.tbDays35.TabIndex = 47;
            this.tbDays35.TabStop = false;
            // 
            // tbDays25
            // 
            this.tbDays25.BackColor = System.Drawing.SystemColors.Window;
            this.tbDays25.Location = new System.Drawing.Point(177, 56);
            this.tbDays25.Name = "tbDays25";
            this.tbDays25.ReadOnly = true;
            this.tbDays25.Size = new System.Drawing.Size(67, 20);
            this.tbDays25.TabIndex = 48;
            this.tbDays25.TabStop = false;
            // 
            // tbDays45
            // 
            this.tbDays45.BackColor = System.Drawing.SystemColors.Window;
            this.tbDays45.Location = new System.Drawing.Point(323, 57);
            this.tbDays45.Name = "tbDays45";
            this.tbDays45.ReadOnly = true;
            this.tbDays45.Size = new System.Drawing.Size(69, 20);
            this.tbDays45.TabIndex = 46;
            this.tbDays45.TabStop = false;
            // 
            // tbCounter
            // 
            this.tbCounter.Location = new System.Drawing.Point(324, 82);
            this.tbCounter.Name = "tbCounter";
            this.tbCounter.ReadOnly = true;
            this.tbCounter.Size = new System.Drawing.Size(68, 20);
            this.tbCounter.TabIndex = 56;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(142, 6);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(19, 13);
            this.lbl5.TabIndex = 50;
            this.lbl5.Text = "+5";
            // 
            // lbl25
            // 
            this.lbl25.AutoSize = true;
            this.lbl25.Location = new System.Drawing.Point(198, 6);
            this.lbl25.Name = "lbl25";
            this.lbl25.Size = new System.Drawing.Size(25, 13);
            this.lbl25.TabIndex = 51;
            this.lbl25.Text = "+25";
            // 
            // lbl35
            // 
            this.lbl35.AutoSize = true;
            this.lbl35.Location = new System.Drawing.Point(270, 6);
            this.lbl35.Name = "lbl35";
            this.lbl35.Size = new System.Drawing.Size(25, 13);
            this.lbl35.TabIndex = 52;
            this.lbl35.Text = "+35";
            // 
            // lbl45
            // 
            this.lbl45.AutoSize = true;
            this.lbl45.Location = new System.Drawing.Point(343, 6);
            this.lbl45.Name = "lbl45";
            this.lbl45.Size = new System.Drawing.Size(25, 13);
            this.lbl45.TabIndex = 53;
            this.lbl45.Text = "+45";
            // 
            // txtBoxStatus
            // 
            this.txtBoxStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxStatus.Location = new System.Drawing.Point(527, 39);
            this.txtBoxStatus.MaximumSize = new System.Drawing.Size(80, 20);
            this.txtBoxStatus.MinimumSize = new System.Drawing.Size(80, 20);
            this.txtBoxStatus.Name = "txtBoxStatus";
            this.txtBoxStatus.ReadOnly = true;
            this.txtBoxStatus.Size = new System.Drawing.Size(80, 20);
            this.txtBoxStatus.TabIndex = 17;
            this.txtBoxStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(484, 43);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 16;
            this.lblStatus.Text = "Status";
            // 
            // panelButton
            // 
            this.panelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelButton.Controls.Add(this.buttonRetriveAll);
            this.panelButton.Controls.Add(this.buttonNewAppl);
            this.panelButton.Controls.Add(this.buttonSelectAppl);
            this.panelButton.Controls.Add(this.buttonAttachDoc);
            this.panelButton.Controls.Add(this.buttonDeleteAppl);
            this.panelButton.Controls.Add(this.buttonSave);
            this.panelButton.Controls.Add(this.buttonPacketPrint);
            this.panelButton.Location = new System.Drawing.Point(19, 487);
            this.panelButton.Name = "panelButton";
            this.panelButton.Size = new System.Drawing.Size(905, 41);
            this.panelButton.TabIndex = 24;
            // 
            // buttonRetriveAll
            // 
            this.buttonRetriveAll.Location = new System.Drawing.Point(550, 10);
            this.buttonRetriveAll.Name = "buttonRetriveAll";
            this.buttonRetriveAll.Size = new System.Drawing.Size(140, 23);
            this.buttonRetriveAll.TabIndex = 23;
            this.buttonRetriveAll.Text = "Retrieve all";
            this.buttonRetriveAll.UseVisualStyleBackColor = true;
            this.buttonRetriveAll.Click += new System.EventHandler(this.buttonRetriveAll_Click);
            // 
            // contextMenuStrip_Grid
            // 
            this.contextMenuStrip_Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrip_OpenDesigner,
            this.resetSettingsToolStripMenuItem});
            this.contextMenuStrip_Grid.Name = "contextMenuStrip_Grid";
            this.contextMenuStrip_Grid.Size = new System.Drawing.Size(275, 70);
            this.contextMenuStrip_Grid.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Grid_Opening);
            // 
            // toolStrip_OpenDesigner
            // 
            this.toolStrip_OpenDesigner.Name = "toolStrip_OpenDesigner";
            this.toolStrip_OpenDesigner.Size = new System.Drawing.Size(274, 22);
            this.toolStrip_OpenDesigner.Text = "Панель управління переліком полів";
            this.toolStrip_OpenDesigner.Click += new System.EventHandler(this.toolStrip_OpenDesigner_Click);
            // 
            // resetSettingsToolStripMenuItem
            // 
            this.resetSettingsToolStripMenuItem.Name = "resetSettingsToolStripMenuItem";
            this.resetSettingsToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.resetSettingsToolStripMenuItem.Text = "Перелік полів за замовченням";
            this.resetSettingsToolStripMenuItem.Click += new System.EventHandler(this.resetSettingsToolStripMenuItem_Click);
            // 
            // FPacket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 540);
            this.ContextMenuStrip = this.contextMenuStrip_Grid;
            this.Controls.Add(this.panelButton);
            this.Controls.Add(this.panelParametrs);
            this.Controls.Add(this.gbApplList);
            this.Controls.Add(this.menuPacket);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuPacket;
            this.Name = "FPacket";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пакет заявок";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPacket_FormClosing_2);
            this.Load += new System.EventHandler(this.FPacket_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PacketForm_KeyDown);
            this.Resize += new System.EventHandler(this.FPacket_Resize);
            this.gbApplList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAppl)).EndInit();
            this.menuPacket.ResumeLayout(false);
            this.menuPacket.PerformLayout();
            this.panelParametrs.ResumeLayout(false);
            this.panelParametrs.PerformLayout();
            this.tabControlLinkFiles.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.pnViewLinkFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLinkFiles)).EndInit();
            this.contextMenuAdditional.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.pnCheckDates.ResumeLayout(false);
            this.pnCheckDates.PerformLayout();
            this.panelButton.ResumeLayout(false);
            this.contextMenuStrip_Grid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      protected System.Windows.Forms.Label label1;
      protected System.Windows.Forms.Label label2;
      protected System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      protected System.Windows.Forms.GroupBox gbApplList;
      protected System.Windows.Forms.Button buttonNewAppl;
      protected System.Windows.Forms.Button buttonSelectAppl;
      protected System.Windows.Forms.Button buttonDeleteAppl;
      protected System.Windows.Forms.Button buttonPacketPrint;
      protected System.Windows.Forms.Button buttonSave;
      protected System.Windows.Forms.TextBox tbOwner;
      protected System.Windows.Forms.ComboBox cbPacketContent;
      protected System.Windows.Forms.TextBox tbInNumber;
      protected System.Windows.Forms.TextBox tbOutNumber;
      protected System.Windows.Forms.ComboBox cbStandardNew;
      protected System.Windows.Forms.ComboBox cbPacketType;
      protected System.Windows.Forms.DateTimePicker dateInPacket;
      protected System.Windows.Forms.DateTimePicker dateOutPacket;
      protected System.Windows.Forms.DataGridView dataGridAppl;
      protected System.Windows.Forms.MenuStrip menuPacket;
      protected System.Windows.Forms.Button buttonAttachDoc;
      protected System.Windows.Forms.ToolStripMenuItem mainMenu;
      protected System.Windows.Forms.ToolStripMenuItem DRVMemo;
      protected System.Windows.Forms.ToolStripMenuItem urzpMenu;
      protected System.Windows.Forms.ToolStripMenuItem службоваЗапискаДоДРВToolStripMenuItem;
      protected System.Windows.Forms.Panel panelParametrs;
      protected System.Windows.Forms.Panel panelButton;
      private System.Windows.Forms.ToolStripMenuItem menuSendToUrcp;
      private System.Windows.Forms.ToolStripMenuItem PrintEmptyPtk;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem PrintPermissionsMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
      private System.Windows.Forms.ToolStripMenuItem AddDateMenuItem;
      protected System.Windows.Forms.TextBox txtBoxStatus;
      private System.Windows.Forms.Label lblStatus;
      private System.Windows.Forms.ToolStripMenuItem ShowAllApplToDrvFromUrcm;
      private System.Windows.Forms.ToolStripMenuItem ShowAllApplToDrvFromUrzp;
      private System.Windows.Forms.Panel pnCheckDates;
      private System.Windows.Forms.Label lblDays;
      private System.Windows.Forms.Label lblTrCount;
      private System.Windows.Forms.Label lblDates;
      private System.Windows.Forms.TextBox tbDate5;
      private System.Windows.Forms.TextBox tbDate25;
      private System.Windows.Forms.TextBox tbDate35;
      private System.Windows.Forms.TextBox tbDate45;
      private System.Windows.Forms.TextBox tbDays5;
      private System.Windows.Forms.TextBox tbDays35;
      private System.Windows.Forms.TextBox tbDays25;
      private System.Windows.Forms.TextBox tbDays45;
      private System.Windows.Forms.TextBox tbCounter;
      private System.Windows.Forms.Label lbl5;
      private System.Windows.Forms.Label lbl25;
      private System.Windows.Forms.Label lbl35;
      private System.Windows.Forms.Label lbl45;
      private System.Windows.Forms.ToolStripMenuItem mniUpdateLicList;
      private System.Windows.Forms.Button buttonRetriveAll;
      private System.Windows.Forms.ToolStripMenuItem MenuItemPrintDozv;
      private System.Windows.Forms.ToolStripMenuItem MenuItemVISN_ems;
      private System.Windows.Forms.ToolStripMenuItem MenuItemDOZV;
      private System.Windows.Forms.ToolTip toolTip1;
      public System.Windows.Forms.DataGridView dataGridViewLinkFiles;
      private System.Windows.Forms.Panel pnViewLinkFiles;
      private System.Windows.Forms.ContextMenuStrip contextMenuAdditional;
      private System.Windows.Forms.ToolStripMenuItem editRecordToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem formOfControlToolStripMenuItem;
      private System.Windows.Forms.TabControl tabControlLinkFiles;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.DataGridViewTextBoxColumn DOC_TYPE;
      private System.Windows.Forms.DataGridViewTextBoxColumn DOC_REF;
      private System.Windows.Forms.DataGridViewTextBoxColumn DOC_DATE;
      private System.Windows.Forms.DataGridViewTextBoxColumn CREATED_BY;
      private System.Windows.Forms.DataGridViewTextBoxColumn ID;
      private System.Windows.Forms.DataGridViewTextBoxColumn PATH;
      private System.Windows.Forms.ToolStripMenuItem AddDateMenuItemAbonent;
      private System.Windows.Forms.ToolStripMenuItem EMCMenuItem;
      private System.Windows.Forms.ToolStripMenuItem DozvMenuItem;
      private System.Windows.Forms.ContextMenuStrip contextMenuStrip_Grid;
      private System.Windows.Forms.ToolStripMenuItem toolStrip_OpenDesigner;
      private System.Windows.Forms.ToolStripMenuItem resetSettingsToolStripMenuItem;
   }
}