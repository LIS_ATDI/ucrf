﻿namespace XICSM.UcrfRfaNET.AppPacket.Forms
{
    partial class PrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbPrintAll = new System.Windows.Forms.RadioButton();
            this.rbPrintOneByOne = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBlankNumber = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDocType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // rbPrintAll
            // 
            this.rbPrintAll.AutoSize = true;
            this.rbPrintAll.Location = new System.Drawing.Point(21, 65);
            this.rbPrintAll.Name = "rbPrintAll";
            this.rbPrintAll.Size = new System.Drawing.Size(111, 17);
            this.rbPrintAll.TabIndex = 0;
            this.rbPrintAll.Text = "Друк всіх одразу";
            this.rbPrintAll.UseVisualStyleBackColor = true;
            this.rbPrintAll.CheckedChanged += new System.EventHandler(this.rbPrintOneByOne_CheckedChanged);
            // 
            // rbPrintOneByOne
            // 
            this.rbPrintOneByOne.AutoSize = true;
            this.rbPrintOneByOne.Checked = true;
            this.rbPrintOneByOne.Location = new System.Drawing.Point(21, 88);
            this.rbPrintOneByOne.Name = "rbPrintOneByOne";
            this.rbPrintOneByOne.Size = new System.Drawing.Size(106, 17);
            this.rbPrintOneByOne.TabIndex = 1;
            this.rbPrintOneByOne.TabStop = true;
            this.rbPrintOneByOne.Text = "Друк по одному";
            this.rbPrintOneByOne.UseVisualStyleBackColor = true;
            this.rbPrintOneByOne.CheckedChanged += new System.EventHandler(this.rbPrintOneByOne_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Номер першого бланку:";
            // 
            // tbBlankNumber
            // 
            this.tbBlankNumber.Enabled = false;
            this.tbBlankNumber.Location = new System.Drawing.Point(156, 65);
            this.tbBlankNumber.Name = "tbBlankNumber";
            this.tbBlankNumber.Size = new System.Drawing.Size(186, 20);
            this.tbBlankNumber.TabIndex = 3;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(68, 118);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(102, 23);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "Розпочати друк";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(197, 118);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(102, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Скасувати";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Документи:";
            // 
            // cbDocType
            // 
            this.cbDocType.FormattingEnabled = true;
            this.cbDocType.Items.AddRange(new object[] {
            "Дозволи",
            "Висновки"});
            this.cbDocType.Location = new System.Drawing.Point(155, 12);
            this.cbDocType.Name = "cbDocType";
            this.cbDocType.Size = new System.Drawing.Size(187, 21);
            this.cbDocType.TabIndex = 7;
            // 
            // PrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 160);
            this.Controls.Add(this.cbDocType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.tbBlankNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbPrintOneByOne);
            this.Controls.Add(this.rbPrintAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrintForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Друк на принтер";
            this.Load += new System.EventHandler(this.PrintForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbPrintAll;
        private System.Windows.Forms.RadioButton rbPrintOneByOne;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBlankNumber;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbDocType;
    }
}