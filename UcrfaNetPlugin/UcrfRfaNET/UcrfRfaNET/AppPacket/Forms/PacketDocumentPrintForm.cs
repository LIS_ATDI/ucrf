﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lis.OfficeBridge;

namespace XICSM.UcrfRfaNET.AppPacket.Forms
{
    public partial class PacketDocumentPrintForm : Form
    {
        public bool Cancelled { get; set; }
        private string _filePath;
        public string BlankNumber { get; set; }
        WordBridge word = new WordBridge();

        public PacketDocumentPrintForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Cancelled = true;
        }

        public void SetInfo(string permNum, string filePath)
        {
            _filePath = filePath;
            lblPrintSymbol.Text = "Друк Дозволу № " + permNum;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {                
                word.Init();
                word.OpenFile(_filePath);
                word.ShowWord(true);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }            
        }

        private void PacketDocumentPrintForm_Load(object sender, EventArgs e)
        {
            tbBlankNumber.DataBindings.Add("Text", this, "BlankNumber");
        }
    }
}
