﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.UtilityClass;
using System.Dynamic;
using System.Collections;
using System.ComponentModel;
using Microsoft.CSharp;
using Microsoft.Runtime;

namespace XICSM.UcrfRfaNET.AppPacket
{
    public class PacketRowBase : NotifyPropertyChanged
    {
        public IDictionary<string, object> flex = new ExpandoObject();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Val"></param>
        /// <returns></returns>
        public  bool AddNewProperty(string Name, object Val)
        {
            flex[Name] = Val;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public object GetValProperty(string Name)
        {
            if (flex.ContainsKey(Name)) {
                return flex[Name];
            }
            else return null;
        }


        //-----
        private bool _isSelected = false;
        /// <summary>
        /// Выбрана
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    InvokeNotifyPropertyChanged("IsSelected");
                }
            }
        }
        //----
        private int _numRow = 0;
        /// <summary>
        /// Номер записи
        /// </summary>
        public int NumRow
        {
            get { return _numRow; }
            set
            {
                if (value != _numRow)
                {
                    _numRow = value;
                    InvokeNotifyPropertyChanged("NumRow");
                }
            }
        }
        //-----
        private int _applId = 0;
        /// <summary>
        /// ID заявки
        /// </summary>
        public int ApplId
        {
            get { return _applId; }
            set
            {
                if (value != _applId)
                {
                    _applId = value;
                    InvokeNotifyPropertyChanged("ApplId");
                }
            }
        }
        //------
        private DateTime _createdDate = IM.NullT;
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set
            {
                if (value != _createdDate)
                {
                    _createdDate = value;
                    InvokeNotifyPropertyChanged("CreatedDate");
                }
            }
        }
        private string _fullPosition = "";
        /// <summary>
        /// Полный адрес
        /// </summary>
        public string FullPosition
        {
            get { return _fullPosition; }
            set
            {
                if (value != _fullPosition)
                {
                    _fullPosition = value;
                    InvokeNotifyPropertyChanged("FullPosition");
                }
            }
        }

        private bool _posDiffersFromAdmSite;
        /// <summary>
        /// Сайт отличается от Адм.Сайта
        /// </summary>
        public bool PosDiffersFromAdmSite
        {
            get { return _posDiffersFromAdmSite; }
            set
            {
                if (value != _posDiffersFromAdmSite)
                {
                    _posDiffersFromAdmSite = value;
                    InvokeNotifyPropertyChanged("PosDiffersFromAdmSite");
                }
            }
        }

        private string _sid = "";
        /// <summary>
        /// идентификатор станции
        /// </summary>
        public string Sid
        {
            get { return _sid; }
            set
            {
                if (value != _sid)
                {
                    _sid = value;
                    InvokeNotifyPropertyChanged("Sid");
                }
            }
        }

        //------
        private string _fio = "";
        /// <summary>
        /// ФИО
        /// </summary>
        public string Fio
        {
            get { return _fio; }
            set
            {
                if (value != _fio)
                {
                    _fio = value;
                    InvokeNotifyPropertyChanged("Fio");
                }
            }
        }
        //------
        private string _standard = "";
        /// <summary>
        /// Стандарт
        /// </summary>
        public string Standard
        {
            get { return _standard; }
            set
            {
                if (value != _standard)
                {
                    _standard = value;
                    InvokeNotifyPropertyChanged("Standard");
                }
            }
        }
        //------
        private string _status = "";
        /// <summary>
        /// Стандарт
        /// </summary>
        public string Status
        {
            get { return _status; }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    InvokeNotifyPropertyChanged("Status");
                }
            }
        }
        //-----
        private bool _isTv = false;
        /// <summary>
        /// Выбрана
        /// </summary>
        public bool IsTv
        {
            get { return _isTv; }
            set
            {
                if (value != _isTv)
                {
                    _isTv = value;
                    InvokeNotifyPropertyChanged("IsTv");
                }
            }
        }
        //-----
        private bool _isNv = false;
        /// <summary>
        /// Выбрана
        /// </summary>
        public bool IsNv
        {
            get { return _isNv; }
            set
            {
                if (value != _isNv)
                {
                    _isNv = value;
                    InvokeNotifyPropertyChanged("IsNv");
                }
            }
        }
        //------
        private DateTime _docDate = IM.NullT;
        /// <summary>
        /// Дата документа
        /// </summary>
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                if (value != _docDate)
                {
                    _docDate = value;
                    InvokeNotifyPropertyChanged("DocDate");
                }
            }
        }
        //------
        private DateTime _docEndDate = IM.NullT;
        /// <summary>
        /// Дата онончания документа
        /// </summary>
        public DateTime DocEndDate
        {
            get { return _docEndDate; }
            set
            {
                if (value != _docEndDate)
                {
                    _docEndDate = value;
                    InvokeNotifyPropertyChanged("DocEndDate");
                }
            }
        }
        //------
        private DateTime _docUrcmDate = IM.NullT;
        /// <summary>
        /// Дата УРЧМ документа
        /// </summary>
        public DateTime DocUrcmDate
        {
            get { return _docUrcmDate; }
            set
            {
                if (value != _docUrcmDate)
                {
                    _docUrcmDate = value;
                    InvokeNotifyPropertyChanged("DocUrcmDate");
                }
            }
        }
        //------
        private DateTime _ptkDateFrom = IM.NullT;
        /// <summary>
        /// Дата ПТК от
        /// </summary>
        public DateTime PtkDateFrom
        {
            get { return _ptkDateFrom; }
            set
            {
                if (value != _ptkDateFrom)
                {
                    _ptkDateFrom = value;
                    InvokeNotifyPropertyChanged("PtkDateFrom");
                }
            }
        }
        //------
        private DateTime _ptkDateTo = IM.NullT;
        /// <summary>
        /// Дата ПТК от
        /// </summary>
        public DateTime PtkDateTo
        {
            get { return _ptkDateTo; }
            set
            {
                if (value != _ptkDateTo)
                {
                    _ptkDateTo = value;
                    InvokeNotifyPropertyChanged("PtkDateTo");
                }
            }
        }
        //------
        public const string EriPtkStatus = "TRFAPtkStatus";
        private Dictionary<string, string> _statusDictionary = null;
        private string _ptkStatus = "";
        private string _foreighnStatus = "";
        /// <summary>
        /// Птк статус
        /// </summary>
        public string PtkStatus
        {
            get
            {
                string retVal = "";
                if (_statusDictionary == null)
                    _statusDictionary = EriFiles.GetEriCodeAndDescr(EriPtkStatus);
                if (_statusDictionary.ContainsKey(_ptkStatus))
                    retVal = _statusDictionary[_ptkStatus];
                return retVal;
            }
            set
            {
                if (_statusDictionary == null)
                    _statusDictionary = EriFiles.GetEriCodeAndDescr(EriPtkStatus);
                string key = "";
                if (_statusDictionary.ContainsValue(value))
                {
                    foreach (KeyValuePair<string, string> valuePair in _statusDictionary)
                    {
                        if (valuePair.Value == value)
                        {
                            key = valuePair.Key;
                            break;
                        }
                    }
                }
                if (key != _ptkStatus)
                {
                    _ptkStatus = key;
                    InvokeNotifyPropertyChanged("PtkStatus");
                }
            }        
        }

        /// <summary>
        /// Статус
        /// </summary>
        public string ForeighnStatus
        {
            get
            {
                return _foreighnStatus;
            }
            set
            {
                if (value != _foreighnStatus)
                {
                    _foreighnStatus = value;
                    InvokeNotifyPropertyChanged("ForeighnStatus");                    
                }
            }
        }

        /// <summary>
        /// Статус ПТК код
        /// </summary>
        public string PtkStatusCode
        {
            get
            {
                return _ptkStatus;
            }
            set
            {
                if (value != _ptkStatus)
                {
                    _ptkStatus = value;
                    InvokeNotifyPropertyChanged("PtkStatusCode");
                    InvokeNotifyPropertyChanged("PtkStatus");  //Вызывем обновление статуса с описанием
                }
            }
        }
        //------
        private int _transmCounter = 0;
        /// <summary>
        /// счетч. станций
        /// </summary>
        public int TransmCnt
        {
            get { return _transmCounter; }
            set
            {
                if (value != _transmCounter)
                {
                    _transmCounter = value;
                    InvokeNotifyPropertyChanged("TransmCnt");
                }
            }
        }
        //------
        private string _docNumConclusion = "";
        /// <summary>
        /// Номер документа TV
        /// </summary>
        public string DocNumConclusion
        {
            get { return _docNumConclusion; }
            set
            {
                if (value != _docNumConclusion)
                {
                    _docNumConclusion = value;
                    InvokeNotifyPropertyChanged("DocNumConclusion");
                }
            }
        }
        //------
        private string _docNumTv = "";
        /// <summary>
        /// Номер документа TV
        /// </summary>
        public string DocNumTv
        {
            get { return _docNumTv; }
            set
            {
                if (value != _docNumTv)
                {
                    _docNumTv = value;
                    InvokeNotifyPropertyChanged("DocNumTv");
                }
            }
        }
        //------
        private string _docNumUrcm = "";
        /// <summary>
        /// Номер документа УРЗП
        /// </summary>
        public string DocNumUrcm
        {
            get { return _docNumUrcm; }
            set
            {
                if (value != _docNumUrcm)
                {
                    _docNumUrcm = value;
                    InvokeNotifyPropertyChanged("DocNumUrcm");
                }
            }
        }
        //------
        private string _docNumPerm = "";
        /// <summary>
        /// Номер разрешения
        /// </summary>
        public string DocNumPerm
        {
            get { return _docNumPerm; }
            set
            {
                if (value != _docNumPerm)
                {
                    _docNumPerm = value;
                    InvokeNotifyPropertyChanged("DocNumPerm");
                }
            }
        }
        //------
        private string _remark = "";
        /// <summary>
        /// Примечание
        /// </summary>
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (value != _remark)
                {
                    _remark = value;
                    InvokeNotifyPropertyChanged("Remark");
                }
            }
        }

        private string _callSign = "";
        public string CallSign
        {
            get { return _callSign; }
            set
            {
                if (value != _callSign)
                {
                    _callSign = value;
                    InvokeNotifyPropertyChanged("CallSign");
                }
            }
        }

        //////////////////////////////////////////////////////////
        // Доработка от 04.12.2016
        //////////////////////////////////////////////////////////
        #region A3
        //
        private string _STANDARD_A3 = "";
        public string STANDARD_A3
        {
            get { return _STANDARD_A3; }
            set
            {
                if (value != _STANDARD_A3)
                {
                    _STANDARD_A3 = value;
                    InvokeNotifyPropertyChanged("STANDARD_A3");
                }
            }
        }

        private string _TYPE_STAT_A3 = "";
        public string TYPE_STAT_A3
        {
            get { return _TYPE_STAT_A3; }
            set
            {
                if (value != _TYPE_STAT_A3)
                {
                    _TYPE_STAT_A3 = value;
                    InvokeNotifyPropertyChanged("TYPE_STAT_A3");
                }
            }
        }

        private string _ADDRESS_A3 = "";
        public string ADDRESS_A3
        {
            get { return _ADDRESS_A3; }
            set
            {
                if (value != _ADDRESS_A3)
                {
                    _ADDRESS_A3 = value;
                    InvokeNotifyPropertyChanged("ADDRESS_A3");
                }
            }
        }

        private string _OWNER_NAME_A3 = "";
        public string OWNER_NAME_A3
        {
            get { return _OWNER_NAME_A3; }
            set
            {
                if (value != _OWNER_NAME_A3)
                {
                    _OWNER_NAME_A3 = value;
                    InvokeNotifyPropertyChanged("OWNER_NAME_A3");
                }
            }
        }

        private string _CALL_SIGN_A3 = "";
        public string CALL_SIGN_A3
        {
            get { return _CALL_SIGN_A3; }
            set
            {
                if (value != _CALL_SIGN_A3)
                {
                    _CALL_SIGN_A3 = value;
                    InvokeNotifyPropertyChanged("CALL_SIGN_A3");
                }
            }
        }

        private string _CATEG_OPER_A3 = "";
        public string CATEG_OPER_A3
        {
            get { return _CATEG_OPER_A3; }
            set
            {
                if (value != _CATEG_OPER_A3)
                {
                    _CATEG_OPER_A3 = value;
                    InvokeNotifyPropertyChanged("CATEG_OPER_A3");
                }
            }
        }

        private string _APC_A3 = "";
        public string APC_A3
        {
            get { return _APC_A3; }
            set
            {
                if (value != _APC_A3)
                {
                    _APC_A3 = value;
                    InvokeNotifyPropertyChanged("APC_A3");
                }
            }
        }

        private string _DOZV_NUM_A3 = "";
        public string DOZV_NUM_A3
        {
            get { return _DOZV_NUM_A3; }
            set
            {
                if (value != _DOZV_NUM_A3)
                {
                    _DOZV_NUM_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_A3");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_A3 = IM.NullT;
        public DateTime DOZV_DATE_FROM_A3
        {
            get { return _DOZV_DATE_FROM_A3; }
            set
            {
                if (value != _DOZV_DATE_FROM_A3)
                {
                    _DOZV_DATE_FROM_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_A3");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_A3 = IM.NullT;
        public DateTime DOZV_DATE_TO_A3
        {
            get { return _DOZV_DATE_TO_A3; }
            set
            {
                if (value != _DOZV_DATE_TO_A3)
                {
                    _DOZV_DATE_TO_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_A3");
                }
            }
        }

        private DateTime _DOZV_DATE_PRINT_A3 = IM.NullT;
        public DateTime DOZV_DATE_PRINT_A3
        {
            get { return _DOZV_DATE_PRINT_A3; }
            set
            {
                if (value != _DOZV_DATE_PRINT_A3)
                {
                    _DOZV_DATE_PRINT_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_PRINT_A3");
                }
            }
        }

        private DateTime _DOZV_DATE_CANCEL_A3 = IM.NullT;
        public DateTime DOZV_DATE_CANCEL_A3
        {
            get { return _DOZV_DATE_CANCEL_A3; }
            set
            {
                if (value != _DOZV_DATE_CANCEL_A3)
                {
                    _DOZV_DATE_CANCEL_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_CANCEL_A3");
                }
            }
        }


        private string _DOZV_CREATED_BY_A3 = "";
        public string DOZV_CREATED_BY_A3
        {
            get { return _DOZV_CREATED_BY_A3; }
            set
            {
                if (value != _DOZV_CREATED_BY_A3)
                {
                    _DOZV_CREATED_BY_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_CREATED_BY_A3");
                }
            }
        }

        private string _DOZV_FILE_A3 = "";
        public string DOZV_FILE_A3
        {
            get { return _DOZV_FILE_A3; }
            set
            {
                if (value != _DOZV_FILE_A3)
                {
                    _DOZV_FILE_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_FILE_A3");
                }
            }
        }

        private string _STATUS_A3 = "";
        public string STATUS_A3
        {
            get { return _STATUS_A3; }
            set
            {
                if (value != _STATUS_A3)
                {
                    _STATUS_A3 = value;
                    InvokeNotifyPropertyChanged("STATUS_A3");
                }
            }
        }

        private string _CREATED_BY_A3 = "";
        public string CREATED_BY_A3
        {
            get { return _CREATED_BY_A3; }
            set
            {
                if (value != _CREATED_BY_A3)
                {
                    _CREATED_BY_A3 = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_A3");
                }
            }
        }

        private int _REF_ID_A3 = IM.NullI;
        public int REF_ID_A3
        {
            get { return _REF_ID_A3; }
            set
            {
                if (value != _REF_ID_A3)
                {
                    _REF_ID_A3 = value;
                    InvokeNotifyPropertyChanged("REF_ID_A3");
                }
            }
        }

        private int _ID_A3 = IM.NullI;
        public int ID_A3
        {
            get { return _ID_A3; }
            set
            {
                if (value != _ID_A3)
                {
                    _ID_A3 = value;
                    InvokeNotifyPropertyChanged("ID_A3");
                }
            }
        }

        private int _OBJ_ID1_A3 = IM.NullI;
        public int OBJ_ID1_A3
        {
            get { return _OBJ_ID1_A3; }
            set
            {
                if (value != _OBJ_ID1_A3)
                {
                    _OBJ_ID1_A3 = value;
                    InvokeNotifyPropertyChanged("OBJ_ID1_A3");
                }
            }
        }

        private string _OBJ_TABLE_A3 = "";
        public string OBJ_TABLE_A3
        {
            get { return _OBJ_TABLE_A3; }
            set
            {
                if (value != _OBJ_TABLE_A3)
                {
                    _OBJ_TABLE_A3 = value;
                    InvokeNotifyPropertyChanged("OBJ_TABLE_A3");
                }
            }
        }

        private int _EMPLOYEE_ID_A3 = IM.NullI;
        public int EMPLOYEE_ID_A3
        {
            get { return _EMPLOYEE_ID_A3; }
            set
            {
                if (value != _EMPLOYEE_ID_A3)
                {
                    _EMPLOYEE_ID_A3 = value;
                    InvokeNotifyPropertyChanged("EMPLOYEE_ID_A3");
                }
            }
        }


        private DateTime _DATE_CREATED_A3 = IM.NullT;
        public DateTime DATE_CREATED_A3
        {
            get { return _DATE_CREATED_A3; }
            set
            {
                if (value != _DATE_CREATED_A3)
                {
                    _DATE_CREATED_A3 = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_A3");
                }
            }
        }


        private string _DOZV_NUM_NEW_A3 = "";
        public string DOZV_NUM_NEW_A3
        {
            get { return _DOZV_NUM_NEW_A3; }
            set
            {
                if (value != _DOZV_NUM_NEW_A3)
                {
                    _DOZV_NUM_NEW_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_NEW_A3");
                }
            }
        }


        private DateTime _DOZV_DATE_FROM_NEW_A3 = IM.NullT;
        public DateTime DOZV_DATE_FROM_NEW_A3
        {
            get { return _DOZV_DATE_FROM_NEW_A3; }
            set
            {
                if (value != _DOZV_DATE_FROM_NEW_A3)
                {
                    _DOZV_DATE_FROM_NEW_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_NEW_A3");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_NEW_A3 = IM.NullT;
        public DateTime DOZV_DATE_TO_NEW_A3
        {
            get { return _DOZV_DATE_TO_NEW_A3; }
            set
            {
                if (value != _DOZV_DATE_TO_NEW_A3)
                {
                    _DOZV_DATE_TO_NEW_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_NEW_A3");
                }
            }
        }


        private string _DOZV_CREATED_BY_NEW_A3 = "";
        public string DOZV_CREATED_BY_NEW_A3
        {
            get { return _DOZV_CREATED_BY_NEW_A3; }
            set
            {
                if (value != _DOZV_CREATED_BY_NEW_A3)
                {
                    _DOZV_CREATED_BY_NEW_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_CREATED_BY_NEW_A3");
                }
            }
        }

        private string _DOZV_FILE_NEW_A3 = "";
        public string DOZV_FILE_NEW_A3
        {
            get { return _DOZV_FILE_NEW_A3; }
            set
            {
                if (value != _DOZV_FILE_NEW_A3)
                {
                    _DOZV_FILE_NEW_A3 = value;
                    InvokeNotifyPropertyChanged("DOZV_FILE_NEW_A3");
                }
            }
        }

        private string _PROVINCE_A3 = "";
        public string PROVINCE_A3
        {
            get { return _PROVINCE_A3; }
            set
            {
                if (value != _PROVINCE_A3)
                {
                    _PROVINCE_A3 = value;
                    InvokeNotifyPropertyChanged("PROVINCE_A3");
                }
            }
        }
        #endregion A3
        
        #region BS

        private string _ADDRESS_BS = "";
        public string ADDRESS_BS
        {
            get { return _ADDRESS_BS; }
            set
            {
                if (value != _ADDRESS_BS)
                {
                    _ADDRESS_BS = value;
                    InvokeNotifyPropertyChanged("ADDRESS_BS");
                }
            }
        }

        private string _CREATED_BY_BS = "";
        public string CREATED_BY_BS
        {
            get { return _CREATED_BY_BS; }
            set
            {
                if (value != _CREATED_BY_BS)
                {
                    _CREATED_BY_BS = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_BS");
                }
            }
        }

        private DateTime _DATE_CREATED_BS = IM.NullT;
        public DateTime DATE_CREATED_BS
        {
            get { return _DATE_CREATED_BS; }
            set
            {
                if (value != _DATE_CREATED_BS)
                {
                    _DATE_CREATED_BS = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_BS");
                }
            }
        }

        private string _STANDARD_BS = "";
        public string STANDARD_BS
        {
            get { return _STANDARD_BS; }
            set
            {
                if (value != _STANDARD_BS)
                {
                    _STANDARD_BS = value;
                    InvokeNotifyPropertyChanged("STANDARD_BS");
                }
            }
        }

        private string _STATUS_BS = "";
        public string STATUS_BS
        {
            get { return _STATUS_BS; }
            set
            {
                if (value != _STATUS_BS)
                {
                    _STATUS_BS = value;
                    InvokeNotifyPropertyChanged("STATUS_BS");
                }
            }
        }

        private string _LICENCE_NUM_BS = "";
        public string LICENCE_NUM_BS
        {
            get { return _LICENCE_NUM_BS; }
            set
            {
                if (value != _LICENCE_NUM_BS)
                {
                    _LICENCE_NUM_BS = value;
                    InvokeNotifyPropertyChanged("LICENCE_NUM_BS");
                }
            }
        }


        private string _RX_FREQ_TXT_BS = "";
        public string RX_FREQ_TXT_BS
        {
            get { return _RX_FREQ_TXT_BS; }
            set
            {
                if (value != _RX_FREQ_TXT_BS)
                {
                    _RX_FREQ_TXT_BS = value;
                    InvokeNotifyPropertyChanged("RX_FREQ_TXT_BS");
                }
            }
        }

        private string _TX_FREQ_TXT_BS = "";
        public string TX_FREQ_TXT_BS
        {
            get { return _TX_FREQ_TXT_BS; }
            set
            {
                if (value != _TX_FREQ_TXT_BS)
                {
                    _TX_FREQ_TXT_BS = value;
                    InvokeNotifyPropertyChanged("TX_FREQ_TXT_BS");
                }
            }
        }
        
        private string _DESIG_EM_BS = "";
        public string DESIG_EM_BS
        {
            get { return _DESIG_EM_BS; }
            set
            {
                if (value != _DESIG_EM_BS)
                {
                    _DESIG_EM_BS = value;
                    InvokeNotifyPropertyChanged("DESIG_EM_BS");
                }
            }
        }

        private string _EQUIP_NAME_BS = "";
        public string EQUIP_NAME_BS
        {
            get { return _EQUIP_NAME_BS; }
            set
            {
                if (value != _EQUIP_NAME_BS)
                {
                    _EQUIP_NAME_BS = value;
                    InvokeNotifyPropertyChanged("EQUIP_NAME_BS");
                }
            }
        }

        private string _ANT_NAME_BS = "";
        public string ANT_NAME_BS
        {
            get { return _ANT_NAME_BS; }
            set
            {
                if (value != _ANT_NAME_BS)
                {
                    _ANT_NAME_BS = value;
                    InvokeNotifyPropertyChanged("ANT_NAME_BS");
                }
            }
        }

        private string _KOATUU_BS = "";
        public string KOATUU_BS
        {
            get { return _KOATUU_BS; }
            set
            {
                if (value != _KOATUU_BS)
                {
                    _KOATUU_BS = value;
                    InvokeNotifyPropertyChanged("KOATUU_BS");
                }
            }
        }

        private string _CONC_NUM_BS = "";
        public string CONC_NUM_BS
        {
            get { return _CONC_NUM_BS; }
            set
            {
                if (value != _CONC_NUM_BS)
                {
                    _CONC_NUM_BS = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_BS");
                }
            }
        }

        private DateTime _CONC_DATE_FROM_BS = IM.NullT;
        public DateTime CONC_DATE_FROM_BS
        {
            get { return _CONC_DATE_FROM_BS; }
            set
            {
                if (value != _CONC_DATE_FROM_BS)
                {
                    _CONC_DATE_FROM_BS = value;
                    InvokeNotifyPropertyChanged("CONC_DATE_FROM_BS");
                }
            }
        }
        

        private string _DOZV_NUM_BS = "";
        public string DOZV_NUM_BS
        {
            get { return _DOZV_NUM_BS; }
            set
            {
                if (value != _DOZV_NUM_BS)
                {
                    _DOZV_NUM_BS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_BS");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_BS = IM.NullT;
        public DateTime DOZV_DATE_FROM_BS
        {
            get { return _DOZV_DATE_FROM_BS; }
            set
            {
                if (value != _DOZV_DATE_FROM_BS)
                {
                    _DOZV_DATE_FROM_BS = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_BS");
                }
            }
        }

        private string _DOZV_NUM_NEW_BS = "";
        public string DOZV_NUM_NEW_BS
        {
            get { return _DOZV_NUM_NEW_BS; }
            set
            {
                if (value != _DOZV_NUM_NEW_BS)
                {
                    _DOZV_NUM_NEW_BS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_NEW_BS");
                }
            }
        }
        
        

        #endregion BS

        #region TR

        private string _STATUS_TR = "";
        public string STATUS_TR
        {
            get { return _STATUS_TR; }
            set
            {
                if (value != _STATUS_TR)
                {
                    _STATUS_TR = value;
                    InvokeNotifyPropertyChanged("STATUS_TR");
                }
            }
        }

        private string _ADDRESS_TR = "";
        public string ADDRESS_TR
        {
            get { return _ADDRESS_TR; }
            set
            {
                if (value != _ADDRESS_TR)
                {
                    _ADDRESS_TR = value;
                    InvokeNotifyPropertyChanged("ADDRESS_TR");
                }
            }
        }

        private string _STANDARD_TR = "";
        public string STANDARD_TR
        {
            get { return _STANDARD_TR; }
            set
            {
                if (value != _STANDARD_TR)
                {
                    _STANDARD_TR = value;
                    InvokeNotifyPropertyChanged("STANDARD_TR");
                }
            }
        }

        private string _CALL_SIGN_TR = "";
        public string CALL_SIGN_TR
        {
            get { return _CALL_SIGN_TR; }
            set
            {
                if (value != _CALL_SIGN_TR)
                {
                    _CALL_SIGN_TR = value;
                    InvokeNotifyPropertyChanged("CALL_SIGN_TR");
                }
            }
        }

        private string _DOZV_NUM_TR = "";
        public string DOZV_NUM_TR
        {
            get { return _DOZV_NUM_TR; }
            set
            {
                if (value != _DOZV_NUM_TR)
                {
                    _DOZV_NUM_TR = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_TR");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_TR = IM.NullT;
        public DateTime DOZV_DATE_TO_TR
        {
            get { return _DOZV_DATE_TO_TR; }
            set
            {
                if (value != _DOZV_DATE_TO_TR)
                {
                    _DOZV_DATE_TO_TR = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_TR");
                }
            }
        }

        private string _EQUIP_NAME_TR = "";
        public string EQUIP_NAME_TR
        {
            get { return _EQUIP_NAME_TR; }
            set
            {
                if (value != _EQUIP_NAME_TR)
                {
                    _EQUIP_NAME_TR = value;
                    InvokeNotifyPropertyChanged("EQUIP_NAME_TR");
                }
            }
        }

        private string _CLASS_TR = "";
        public string CLASS_TR
        {
            get { return _CLASS_TR; }
            set
            {
                if (value != _CLASS_TR)
                {
                    _CLASS_TR = value;
                    InvokeNotifyPropertyChanged("CLASS_TR");
                }
            }
        }

        private string _CONC_NUM_TR = "";
        public string CONC_NUM_TR
        {
            get { return _CONC_NUM_TR; }
            set
            {
                if (value != _CONC_NUM_TR)
                {
                    _CONC_NUM_TR = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_TR");
                }
            }
        }

        private string _RX_FREQ_TXT_TR = "";
        public string RX_FREQ_TXT_TR
        {
            get { return _RX_FREQ_TXT_TR; }
            set
            {
                if (value != _RX_FREQ_TXT_TR)
                {
                    _RX_FREQ_TXT_TR = value;
                    InvokeNotifyPropertyChanged("RX_FREQ_TXT_TR");
                }
            }
        }

        private string _TX_FREQ_TXT_TR = "";
        public string TX_FREQ_TXT_TR
        {
            get { return _TX_FREQ_TXT_TR; }
            set
            {
                if (value != _TX_FREQ_TXT_TR)
                {
                    _TX_FREQ_TXT_TR = value;
                    InvokeNotifyPropertyChanged("TX_FREQ_TXT_TR");
                }
            }
        }

        private int _DIST_BORDER_TR = IM.NullI;
        public int DIST_BORDER_TR
        {
            get { return _DIST_BORDER_TR; }
            set
            {
                if (value != _DIST_BORDER_TR)
                {
                    _DIST_BORDER_TR = value;
                    InvokeNotifyPropertyChanged("DIST_BORDER_TR");
                }
            }
        }


        private DateTime _DOZV_DATE_FROM_TR = IM.NullT;
        public DateTime DOZV_DATE_FROM_TR
        {
            get { return _DOZV_DATE_FROM_TR; }
            set
            {
                if (value != _DOZV_DATE_FROM_TR)
                {
                    _DOZV_DATE_FROM_TR = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_TR");
                }
            }
        }


        private int _TABLE_ID_TR = IM.NullI;
        public int TABLE_ID_TR
        {
            get { return _TABLE_ID_TR; }
            set
            {
                if (value != _TABLE_ID_TR)
                {
                    _TABLE_ID_TR = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_TR");
                }
            }
        }

        private int _ID_TR = IM.NullI;
        public int ID_TR
        {
            get { return _ID_TR; }
            set
            {
                if (value != _ID_TR)
                {
                    _ID_TR = value;
                    InvokeNotifyPropertyChanged("ID_TR");
                }
            }
        }
        

        #endregion TR

        #region RR

        private string _STATUS_RR = "";
        public string STATUS_RR
        {
            get { return _STATUS_RR; }
            set
            {
                if (value != _STATUS_RR)
                {
                    _STATUS_RR = value;
                    InvokeNotifyPropertyChanged("STATUS_RR");
                }
            }
        }

        private string _ADDRESS_RR = "";
        public string ADDRESS_RR
        {
            get { return _ADDRESS_RR; }
            set
            {
                if (value != _ADDRESS_RR)
                {
                    _ADDRESS_RR = value;
                    InvokeNotifyPropertyChanged("ADDRESS_RR");
                }
            }
        }

        private string _CID_RR = "";
        public string CID_RR
        {
            get { return _CID_RR; }
            set
            {
                if (value != _CID_RR)
                {
                    _CID_RR = value;
                    InvokeNotifyPropertyChanged("CID_RR");
                }
            }
        }

        private string _DOZV_NUM_RR = "";
        public string DOZV_NUM_RR
        {
            get { return _DOZV_NUM_RR; }
            set
            {
                if (value != _DOZV_NUM_RR)
                {
                    _DOZV_NUM_RR = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_RR");
                }
            }
        }



        private DateTime _DOZV_DATE_TO_RR = IM.NullT;
        public DateTime DOZV_DATE_TO_RR
        {
            get { return _DOZV_DATE_TO_RR; }
            set
            {
                if (value != _DOZV_DATE_TO_RR)
                {
                    _DOZV_DATE_TO_RR = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_RR");
                }
            }
        }

        private string _EQUIP_NAME_RR = "";
        public string EQUIP_NAME_RR
        {
            get { return _EQUIP_NAME_RR; }
            set
            {
                if (value != _EQUIP_NAME_RR)
                {
                    _EQUIP_NAME_RR = value;
                    InvokeNotifyPropertyChanged("EQUIP_NAME_RR");
                }
            }
        }

        private string _CONC_NUM_RR = "";
        public string CONC_NUM_RR
        {
            get { return _CONC_NUM_RR; }
            set
            {
                if (value != _CONC_NUM_RR)
                {
                    _CONC_NUM_RR = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_RR");
                }
            }
        }




        private DateTime _DOZV_DATE_FROM_RR = IM.NullT;
        public DateTime DOZV_DATE_FROM_RR
        {
            get { return _DOZV_DATE_FROM_RR; }
            set
            {
                if (value != _DOZV_DATE_FROM_RR)
                {
                    _DOZV_DATE_FROM_RR = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_RR");
                }
            }
        }


        private int _TABLE_ID_RR = IM.NullI;
        public int TABLE_ID_RR
        {
            get { return _TABLE_ID_RR; }
            set
            {
                if (value != _TABLE_ID_RR)
                {
                    _TABLE_ID_RR = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_RR");
                }
            }
        }

        private int _ID_RR = IM.NullI;
        public int ID_RR
        {
            get { return _ID_RR; }
            set
            {
                if (value != _ID_RR)
                {
                    _ID_RR = value;
                    InvokeNotifyPropertyChanged("ID_RR");
                }
            }
        }

        #endregion RR

        #region TV
        private string _STATUS_TV2 = "";
        public string STATUS_TV2
        {
            get { return _STATUS_TV2; }
            set
            {
                if (value != _STATUS_TV2)
                {
                    _STATUS_TV2 = value;
                    InvokeNotifyPropertyChanged("STATUS_TV2");
                }
            }
        }

        private string _ADDRESS_TV2 = "";
        public string ADDRESS_TV2
        {
            get { return _ADDRESS_TV2; }
            set
            {
                if (value != _ADDRESS_TV2)
                {
                    _ADDRESS_TV2 = value;
                    InvokeNotifyPropertyChanged("ADDRESS_TV2");
                }
            }
        }

        private string _CHANNEL_TV2 = "";
        public string CHANNEL_TV2
        {
            get { return _CHANNEL_TV2; }
            set
            {
                if (value != _CHANNEL_TV2)
                {
                    _CHANNEL_TV2 = value;
                    InvokeNotifyPropertyChanged("CHANNEL_TV2");
                }
            }
        }

        private string _DOZV_NUM_TV2 = "";
        public string DOZV_NUM_TV2
        {
            get { return _DOZV_NUM_TV2; }
            set
            {
                if (value != _DOZV_NUM_TV2)
                {
                    _DOZV_NUM_TV2 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_TV2");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_TV2 = IM.NullT;
        public DateTime DOZV_DATE_TO_TV2
        {
            get { return _DOZV_DATE_TO_TV2; }
            set
            {
                if (value != _DOZV_DATE_TO_TV2)
                {
                    _DOZV_DATE_TO_TV2 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_TV2");
                }
            }
        }

        private string _EQUIP_NAME_TV2 = "";
        public string EQUIP_NAME_TV2
        {
            get { return _EQUIP_NAME_TV2; }
            set
            {
                if (value != _EQUIP_NAME_TV2)
                {
                    _EQUIP_NAME_TV2 = value;
                    InvokeNotifyPropertyChanged("EQUIP_NAME_TV2");
                }
            }
        }

        private string _CONC_NUM_TV2 = "";
        public string CONC_NUM_TV2
        {
            get { return _CONC_NUM_TV2; }
            set
            {
                if (value != _CONC_NUM_TV2)
                {
                    _CONC_NUM_TV2 = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_TV2");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_TV2 = IM.NullT;
        public DateTime DOZV_DATE_FROM_TV2
        {
            get { return _DOZV_DATE_FROM_TV2; }
            set
            {
                if (value != _DOZV_DATE_FROM_TV2)
                {
                    _DOZV_DATE_FROM_TV2 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_TV2");
                }
            }
        }

        private string _CREATED_BY_TV2 = "";
        public string CREATED_BY_TV2
        {
            get { return _CREATED_BY_TV2; }
            set
            {
                if (value != _CREATED_BY_TV2)
                {
                    _CREATED_BY_TV2 = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_TV2");
                }
            }
        }

        private DateTime _DATE_CREATED_TV2 = IM.NullT;
        public DateTime DATE_CREATED_TV2
        {
            get { return _DATE_CREATED_TV2; }
            set
            {
                if (value != _DATE_CREATED_TV2)
                {
                    _DATE_CREATED_TV2 = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_TV2");
                }
            }
        }

        private string _CITY_TV2 = "";
        public string CITY_TV2
        {
            get { return _CITY_TV2; }
            set
            {
                if (value != _CITY_TV2)
                {
                    _CITY_TV2 = value;
                    InvokeNotifyPropertyChanged("CITY_TV2");
                }
            }
        }

        private int _TABLE_ID_TV2 = IM.NullI;
        public int TABLE_ID_TV2
        {
            get { return _TABLE_ID_TV2; }
            set
            {
                if (value != _TABLE_ID_TV2)
                {
                    _TABLE_ID_TV2 = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_TV2");
                }
            }
        }

        private int _ID_TV2 = IM.NullI;
        public int ID_TV2
        {
            get { return _ID_TV2; }
            set
            {
                if (value != _ID_TV2)
                {
                    _ID_TV2 = value;
                    InvokeNotifyPropertyChanged("ID_TV2");
                }
            }
        }

        #endregion TV
        
        #region ZRS
        //
        private string _STATUS_ZRS = "";
        public string STATUS_ZRS
        {
            get { return _STATUS_ZRS; }
            set
            {
                if (value != _STATUS_ZRS)
                {
                    _STATUS_ZRS = value;
                    InvokeNotifyPropertyChanged("STATUS_ZRS");
                }
            }
        }

        private string _REG_PORT_ZRS = "";
        public string REG_PORT_ZRS
        {
            get { return _REG_PORT_ZRS; }
            set
            {
                if (value != _REG_PORT_ZRS)
                {
                    _REG_PORT_ZRS = value;
                    InvokeNotifyPropertyChanged("REG_PORT_ZRS");
                }
            }
        }

        private string _CREATED_BY_ZRS = "";
        public string CREATED_BY_ZRS
        {
            get { return _CREATED_BY_ZRS; }
            set
            {
                if (value != _CREATED_BY_ZRS)
                {
                    _CREATED_BY_ZRS = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_ZRS");
                }
            }
        }


        private DateTime _DATE_CREATED_ZRS = IM.NullT;
        public DateTime DATE_CREATED_ZRS
        {
            get { return _DATE_CREATED_ZRS; }
            set
            {
                if (value != _DATE_CREATED_ZRS)
                {
                    _DATE_CREATED_ZRS = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_ZRS");
                }
            }
        }


        private string _STANDARD_ZRS = "";
        public string STANDARD_ZRS
        {
            get { return _STANDARD_ZRS; }
            set
            {
                if (value != _STANDARD_ZRS)
                {
                    _STANDARD_ZRS = value;
                    InvokeNotifyPropertyChanged("STANDARD_ZRS");
                }
            }
        }

        private string _DOZV_NUM_ZRS = "";
        public string DOZV_NUM_ZRS
        {
            get { return _DOZV_NUM_ZRS; }
            set
            {
                if (value != _DOZV_NUM_ZRS)
                {
                    _DOZV_NUM_ZRS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_ZRS");
                }
            }
        }


        private DateTime _DOZV_DATE_FROM_ZRS = IM.NullT;
        public DateTime DOZV_DATE_FROM_ZRS
        {
            get { return _DOZV_DATE_FROM_ZRS; }
            set
            {
                if (value != _DOZV_DATE_FROM_ZRS)
                {
                    _DOZV_DATE_FROM_ZRS = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_ZRS");
                }
            }
        }


        private DateTime _DOZV_DATE_TO_ZRS = IM.NullT;
        public DateTime DOZV_DATE_TO_ZRS
        {
            get { return _DOZV_DATE_TO_ZRS; }
            set
            {
                if (value != _DOZV_DATE_TO_ZRS)
                {
                    _DOZV_DATE_TO_ZRS = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_ZRS");
                }
            }
        }

        private string _SHIP_NAME_ZRS = "";
        public string SHIP_NAME_ZRS
        {
            get { return _SHIP_NAME_ZRS; }
            set
            {
                if (value != _SHIP_NAME_ZRS)
                {
                    _SHIP_NAME_ZRS = value;
                    InvokeNotifyPropertyChanged("SHIP_NAME_ZRS");
                }
            }
        }

        private string _MMSI_NUM_ZRS = "";
        public string MMSI_NUM_ZRS
        {
            get { return _MMSI_NUM_ZRS; }
            set
            {
                if (value != _MMSI_NUM_ZRS)
                {
                    _MMSI_NUM_ZRS = value;
                    InvokeNotifyPropertyChanged("MMSI_NUM_ZRS");
                }
            }
        }


        private string _ATIS_ZRS = "";
        public string ATIS_ZRS
        {
            get { return _ATIS_ZRS; }
            set
            {
                if (value != _ATIS_ZRS)
                {
                    _ATIS_ZRS = value;
                    InvokeNotifyPropertyChanged("ATIS_ZRS");
                }
            }
        }

        private string _CALL_SIGN_ZRS = "";
        public string CALL_SIGN_ZRS
        {
            get { return _CALL_SIGN_ZRS; }
            set
            {
                if (value != _CALL_SIGN_ZRS)
                {
                    _CALL_SIGN_ZRS = value;
                    InvokeNotifyPropertyChanged("CALL_SIGN_ZRS");
                }
            }
        }


        private int _TABLE_ID_ZRS = IM.NullI;
        public int TABLE_ID_ZRS
        {
            get { return _TABLE_ID_ZRS; }
            set
            {
                if (value != _TABLE_ID_ZRS)
                {
                    _TABLE_ID_ZRS = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_ZRS");
                }
            }
        }

        private int _ID_ZRS = IM.NullI;
        public int ID_ZRS
        {
            get { return _ID_ZRS; }
            set
            {
                if (value != _ID_ZRS)
                {
                    _ID_ZRS = value;
                    InvokeNotifyPropertyChanged("ID_ZRS");
                }
            }
        }

        #endregion

        #region VP
        //
        private string _STATUS_VP = "";
        public string STATUS_VP
        {
            get { return _STATUS_VP; }
            set
            {
                if (value != _STATUS_VP)
                {
                    _STATUS_VP = value;
                    InvokeNotifyPropertyChanged("STATUS_VP");
                }
            }
        }

        private string _ADDRESS_VP = "";
        public string ADDRESS_VP
        {
            get { return _ADDRESS_VP; }
            set
            {
                if (value != _ADDRESS_VP)
                {
                    _ADDRESS_VP = value;
                    InvokeNotifyPropertyChanged("ADDRESS_VP");
                }
            }
        }

        private string _CREATED_BY_VP = "";
        public string CREATED_BY_VP
        {
            get { return _CREATED_BY_VP; }
            set
            {
                if (value != _CREATED_BY_VP)
                {
                    _CREATED_BY_VP = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_VP");
                }
            }
        }

        private DateTime _DATE_CREATED_VP = IM.NullT;
        public DateTime DATE_CREATED_VP
        {
            get { return _DATE_CREATED_VP; }
            set
            {
                if (value != _DATE_CREATED_VP)
                {
                    _DATE_CREATED_VP = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_VP");
                }
            }
        }

        private string _STANDARD_VP = "";
        public string STANDARD_VP
        {
            get { return _STANDARD_VP; }
            set
            {
                if (value != _STANDARD_VP)
                {
                    _STANDARD_VP = value;
                    InvokeNotifyPropertyChanged("STANDARD_VP");
                }
            }
        }

        private string _DOZV_NUM_VP = "";
        public string DOZV_NUM_VP
        {
            get { return _DOZV_NUM_VP; }
            set
            {
                if (value != _DOZV_NUM_VP)
                {
                    _DOZV_NUM_VP = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_VP");
                }
            }
        }


        private DateTime _DOZV_DATE_FROM_VP = IM.NullT;
        public DateTime DOZV_DATE_FROM_VP
        {
            get { return _DOZV_DATE_FROM_VP; }
            set
            {
                if (value != _DOZV_DATE_FROM_VP)
                {
                    _DOZV_DATE_FROM_VP = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_VP");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_VP = IM.NullT;
        public DateTime DOZV_DATE_TO_VP
        {
            get { return _DOZV_DATE_TO_VP; }
            set
            {
                if (value != _DOZV_DATE_TO_VP)
                {
                    _DOZV_DATE_TO_VP = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_VP");
                }
            }
        }

        private int _TABLE_ID_VP = IM.NullI;
        public int TABLE_ID_VP
        {
            get { return _TABLE_ID_VP; }
            set
            {
                if (value != _TABLE_ID_VP)
                {
                    _TABLE_ID_VP = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_VP");
                }
            }

        }

        private int _ID_VP = IM.NullI;
        public int ID_VP
        {
            get { return _ID_VP; }
            set
            {
                if (value != _ID_VP)
                {
                    _ID_VP = value;
                    InvokeNotifyPropertyChanged("ID_VP");
                }
            }
        }


        #endregion

        #region TV2d
        //
        private string _STATUS_TV2d = "";
        public string STATUS_TV2d
        {
            get { return _STATUS_TV2d; }
            set
            {
                if (value != _STATUS_TV2d)
                {
                    _STATUS_TV2d = value;
                    InvokeNotifyPropertyChanged("STATUS_TV2d");
                }
            }
        }

        private string _ADDRESS_TV2d = "";
        public string ADDRESS_TV2d
        {
            get { return _ADDRESS_TV2d; }
            set
            {
                if (value != _ADDRESS_TV2d)
                {
                    _ADDRESS_TV2d = value;
                    InvokeNotifyPropertyChanged("ADDRESS_TV2d");
                }
            }
        }

        private string _CREATED_BY_TV2d = "";
        public string CREATED_BY_TV2d
        {
            get { return _CREATED_BY_TV2d; }
            set
            {
                if (value != _CREATED_BY_TV2d)
                {
                    _CREATED_BY_TV2d = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_TV2d");
                }
            }
        }


        private DateTime _DATE_CREATED_TV2d = IM.NullT;
        public DateTime DATE_CREATED_TV2d
        {
            get { return _DATE_CREATED_TV2d; }
            set
            {
                if (value != _DATE_CREATED_TV2d)
                {
                    _DATE_CREATED_TV2d = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_TV2d");
                }
            }
        }


        private string _CHANNEL_TV2d = "";
        public string CHANNEL_TV2d
        {
            get { return _CHANNEL_TV2d; }
            set
            {
                if (value != _CHANNEL_TV2d)
                {
                    _CHANNEL_TV2d = value;
                    InvokeNotifyPropertyChanged("CHANNEL_TV2d");
                }
            }
        }

        private string _PROGRAM_TV2d = "";
        public string PROGRAM_TV2d
        {
            get { return _PROGRAM_TV2d; }
            set
            {
                if (value != _PROGRAM_TV2d)
                {
                    _PROGRAM_TV2d = value;
                    InvokeNotifyPropertyChanged("PROGRAM_TV2d");
                }
            }
        }

        private string _DOZV_NUM_TV2d = "";
        public string DOZV_NUM_TV2d
        {
            get { return _DOZV_NUM_TV2d; }
            set
            {
                if (value != _DOZV_NUM_TV2d)
                {
                    _DOZV_NUM_TV2d = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_TV2d");
                }
            }
        }


        private DateTime _DOZV_DATE_TO_TV2d = IM.NullT;
        public DateTime DOZV_DATE_TO_TV2d
        {
            get { return _DOZV_DATE_TO_TV2d; }
            set
            {
                if (value != _DOZV_DATE_TO_TV2d)
                {
                    _DOZV_DATE_TO_TV2d = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_TV2d");
                }
            }
        }

        private string _CONC_NUM_TV2d = "";
        public string CONC_NUM_TV2d
        {
            get { return _CONC_NUM_TV2d; }
            set
            {
                if (value != _CONC_NUM_TV2d)
                {
                    _CONC_NUM_TV2d = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_TV2d");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_TV2d = IM.NullT;
        public DateTime DOZV_DATE_FROM_TV2d
        {
            get { return _DOZV_DATE_FROM_TV2d; }
            set
            {
                if (value != _DOZV_DATE_FROM_TV2d)
                {
                    _DOZV_DATE_FROM_TV2d = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_TV2d");
                }
            }
        }


        private string _CITY_TV2d = "";
        public string CITY_TV2d
        {
            get { return _CITY_TV2d; }
            set
            {
                if (value != _CITY_TV2d)
                {
                    _CITY_TV2d = value;
                    InvokeNotifyPropertyChanged("CITY_TV2d");
                }
            }
        }

        private int _TABLE_ID_TV2d = IM.NullI;
        public int TABLE_ID_TV2d
        {
            get { return _TABLE_ID_TV2d; }
            set
            {
                if (value != _TABLE_ID_TV2d)
                {
                    _TABLE_ID_TV2d = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_TV2d");
                }
            }
        }

        private int _ID_TV2d = IM.NullI;
        public int ID_TV2d
        {
            get { return _ID_TV2d; }
            set
            {
                if (value != _ID_TV2d)
                {
                    _ID_TV2d = value;
                    InvokeNotifyPropertyChanged("ID_TV2d");
                }
            }
        }

        #endregion

        #region R2d
        //
        private string _STATUS_R2d = "";
        public string STATUS_R2d
        {
            get { return _STATUS_R2d; }
            set
            {
                if (value != _STATUS_R2d)
                {
                    _STATUS_R2d = value;
                    InvokeNotifyPropertyChanged("STATUS_R2d");
                }
            }
        }

        private string _ADDRESS_R2d = "";
        public string ADDRESS_R2d
        {
            get { return _ADDRESS_R2d; }
            set
            {
                if (value != _ADDRESS_R2d)
                {
                    _ADDRESS_R2d = value;
                    InvokeNotifyPropertyChanged("ADDRESS_R2d");
                }
            }
        }

        private string _BLOCK_R2d = "";
        public string BLOCK_R2d
        {
            get { return _BLOCK_R2d; }
            set
            {
                if (value != _BLOCK_R2d)
                {
                    _BLOCK_R2d = value;
                    InvokeNotifyPropertyChanged("BLOCK_R2d");
                }
            }
        }

        private string _CREATED_BY_R2d = "";
        public string CREATED_BY_R2d
        {
            get { return _CREATED_BY_R2d; }
            set
            {
                if (value != _CREATED_BY_R2d)
                {
                    _CREATED_BY_R2d = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_R2d");
                }
            }
        }

        private DateTime _DATE_CREATED_R2d = IM.NullT;
        public DateTime DATE_CREATED_R2d
        {
            get { return _DATE_CREATED_R2d; }
            set
            {
                if (value != _DATE_CREATED_R2d)
                {
                    _DATE_CREATED_R2d = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_R2d");
                }
            }
        }

        private string _DOZV_NUM_R2d = "";
        public string DOZV_NUM_R2d
        {
            get { return _DOZV_NUM_R2d; }
            set
            {
                if (value != _DOZV_NUM_R2d)
                {
                    _DOZV_NUM_R2d = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_R2d");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_R2d = IM.NullT;
        public DateTime DOZV_DATE_FROM_R2d
        {
            get { return _DOZV_DATE_FROM_R2d; }
            set
            {
                if (value != _DOZV_DATE_FROM_R2d)
                {
                    _DOZV_DATE_FROM_R2d = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_R2d");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_R2d = IM.NullT;
        public DateTime DOZV_DATE_TO_R2d
        {
            get { return _DOZV_DATE_TO_R2d; }
            set
            {
                if (value != _DOZV_DATE_TO_R2d)
                {
                    _DOZV_DATE_TO_R2d = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_R2d");
                }
            }
        }

        private string _CONC_NUM_R2d = "";
        public string CONC_NUM_R2d
        {
            get { return _CONC_NUM_R2d; }
            set
            {
                if (value != _CONC_NUM_R2d)
                {
                    _CONC_NUM_R2d = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_R2d");
                }
            }
        }

        private DateTime _CONC_DATE_FROM_R2d = IM.NullT;
        public DateTime CONC_DATE_FROM_R2d
        {
            get { return _CONC_DATE_FROM_R2d; }
            set
            {
                if (value != _CONC_DATE_FROM_R2d)
                {
                    _CONC_DATE_FROM_R2d = value;
                    InvokeNotifyPropertyChanged("CONC_DATE_FROM_R2d");
                }
            }
        }

        private string _CITY_R2d = "";
        public string CITY_R2d
        {
            get { return _CITY_R2d; }
            set
            {
                if (value != _CITY_R2d)
                {
                    _CITY_R2d = value;
                    InvokeNotifyPropertyChanged("CITY_R2d");
                }
            }
        }

        private string _KOATUU_R2d = "";
        public string KOATUU_R2d
        {
            get { return _KOATUU_R2d; }
            set
            {
                if (value != _KOATUU_R2d)
                {
                    _KOATUU_R2d = value;
                    InvokeNotifyPropertyChanged("KOATUU_R2d");
                }
            }
        }


        private int _TABLE_ID_R2d = IM.NullI;
        public int TABLE_ID_R2d
        {
            get { return TABLE_ID_R2d; }
            set
            {
                if (value != TABLE_ID_R2d)
                {
                    TABLE_ID_R2d = value;
                    InvokeNotifyPropertyChanged("TABLE_ID_R2d");
                }
            }
        }

        private int _ID_R2d = IM.NullI;
        public int ID_R2d
        {
            get { return _ID_R2d; }
            set
            {
                if (value != _ID_R2d)
                {
                    _ID_R2d = value;
                    InvokeNotifyPropertyChanged("ID_R2d");
                }
            }
        }

        #endregion

        #region ZS
        private string _ADDRESS_ZS = "";
        public string ADDRESS_ZS
        {
            get { return _ADDRESS_ZS; }
            set
            {
                if (value != _ADDRESS_ZS)
                {
                    _ADDRESS_ZS = value;
                    InvokeNotifyPropertyChanged("ADDRESS_ZS");
                }
            }
        }

        private string _CREATED_BY_ZS = "";
        public string CREATED_BY_ZS
        {
            get { return _CREATED_BY_ZS; }
            set
            {
                if (value != _CREATED_BY_ZS)
                {
                    _CREATED_BY_ZS = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_ZS");
                }
            }
        }

        private DateTime _DATE_CREATED_ZS = IM.NullT;
        public DateTime DATE_CREATED_ZS
        {
            get { return _DATE_CREATED_ZS; }
            set
            {
                if (value != _DATE_CREATED_ZS)
                {
                    _DATE_CREATED_ZS = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_ZS");
                }
            }
        }

        private string _STANDARD_ZS = "";
        public string STANDARD_ZS
        {
            get { return _STANDARD_ZS; }
            set
            {
                if (value != _STANDARD_ZS)
                {
                    _STANDARD_ZS = value;
                    InvokeNotifyPropertyChanged("STANDARD_ZS");
                }
            }
        }

        private string _STATUS_ZS = "";
        public string STATUS_ZS
        {
            get { return _STATUS_ZS; }
            set
            {
                if (value != _STATUS_ZS)
                {
                    _STATUS_ZS = value;
                    InvokeNotifyPropertyChanged("STATUS_ZS");
                }
            }
        }

        private string _LICENCES_ZS = "";
        public string LICENCES_ZS
        {
            get { return _LICENCES_ZS; }
            set
            {
                if (value != _LICENCES_ZS)
                {
                    _LICENCES_ZS = value;
                    InvokeNotifyPropertyChanged("LICENCES_ZS");
                }
            }
        }


        private string _RX_FREQ_TXT_ZS = "";
        public string RX_FREQ_TXT_ZS
        {
            get { return _RX_FREQ_TXT_ZS; }
            set
            {
                if (value != _RX_FREQ_TXT_ZS)
                {
                    _RX_FREQ_TXT_ZS = value;
                    InvokeNotifyPropertyChanged("RX_FREQ_TXT_ZS");
                }
            }
        }

        private string _TX_FREQ_TXT_ZS = "";
        public string TX_FREQ_TXT_ZS
        {
            get { return _TX_FREQ_TXT_ZS; }
            set
            {
                if (value != _TX_FREQ_TXT_ZS)
                {
                    _TX_FREQ_TXT_ZS = value;
                    InvokeNotifyPropertyChanged("TX_FREQ_TXT_ZS");
                }
            }
        }

        private string _DESIG_EMISSION_ZS = "";
        public string DESIG_EMISSION_ZS
        {
            get { return _DESIG_EMISSION_ZS; }
            set
            {
                if (value != _DESIG_EMISSION_ZS)
                {
                    _DESIG_EMISSION_ZS = value;
                    InvokeNotifyPropertyChanged("DESIG_EMISSION_ZS");
                }
            }
        }

        private string _EQUIP_ZS = "";
        public string EQUIP_ZS
        {
            get { return _EQUIP_ZS; }
            set
            {
                if (value != _EQUIP_ZS)
                {
                    _EQUIP_ZS = value;
                    InvokeNotifyPropertyChanged("EQUIP_ZS");
                }
            }
        }

        private string _ANTENNA_ZS = "";
        public string ANTENNA_ZS
        {
            get { return _ANTENNA_ZS; }
            set
            {
                if (value != _ANTENNA_ZS)
                {
                    _ANTENNA_ZS = value;
                    InvokeNotifyPropertyChanged("ANTENNA_ZS");
                }
            }
        }

        private string _KOATUU_ZS = "";
        public string KOATUU_ZS
        {
            get { return _KOATUU_ZS; }
            set
            {
                if (value != _KOATUU_ZS)
                {
                    _KOATUU_ZS = value;
                    InvokeNotifyPropertyChanged("KOATUU_ZS");
                }
            }
        }


        private string _CONC_NUM_ZS = "";
        public string CONC_NUM_ZS
        {
            get { return _CONC_NUM_ZS; }
            set
            {
                if (value != _CONC_NUM_ZS)
                {
                    _CONC_NUM_ZS = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_ZS");
                }
            }
        }

        private DateTime _CONC_DATE_FROM_ZS = IM.NullT;
        public DateTime CONC_DATE_FROM_ZS
        {
            get { return _CONC_DATE_FROM_ZS; }
            set
            {
                if (value != _CONC_DATE_FROM_ZS)
                {
                    _CONC_DATE_FROM_ZS = value;
                    InvokeNotifyPropertyChanged("CONC_DATE_FROM_ZS");
                }
            }
        }


        private string _DOZV_NUM_ZS = "";
        public string DOZV_NUM_ZS
        {
            get { return _DOZV_NUM_ZS; }
            set
            {
                if (value != _DOZV_NUM_ZS)
                {
                    _DOZV_NUM_ZS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_ZS");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_ZS = IM.NullT;
        public DateTime DOZV_DATE_FROM_ZS
        {
            get { return _DOZV_DATE_FROM_ZS; }
            set
            {
                if (value != _DOZV_DATE_FROM_ZS)
                {
                    _DOZV_DATE_FROM_ZS = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_ZS");
                }
            }
        }

        private string _DOZV_NUM_NEW_ZS = "";
        public string DOZV_NUM_NEW_ZS
        {
            get { return _DOZV_NUM_NEW_ZS; }
            set
            {
                if (value != _DOZV_NUM_NEW_ZS)
                {
                    _DOZV_NUM_NEW_ZS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_NEW_ZS");
                }
            }
        }

        private string _SAT_NAME_ZS = "";
        public string SAT_NAME_ZS
        {
            get { return _SAT_NAME_ZS; }
            set
            {
                if (value != _SAT_NAME_ZS)
                {
                    _SAT_NAME_ZS = value;
                    InvokeNotifyPropertyChanged("SAT_NAME_ZS");
                }
            }
        }

        private double _LONG_NOM_ZS = IM.NullD;
        public double LONG_NOM_ZS
        {
            get { return _LONG_NOM_ZS; }
            set
            {
                if (value != _LONG_NOM_ZS)
                {
                    _LONG_NOM_ZS = value;
                    InvokeNotifyPropertyChanged("LONG_NOM_ZS");
                }
            }
        }

        private int _ID_ZS = IM.NullI;
        public int ID_ZS
        {
            get { return _ID_ZS; }
            set
            {
                if (value != _ID_ZS)
                {
                    _ID_ZS = value;
                    InvokeNotifyPropertyChanged("ID_ZS");
                }
            }
        }


        #endregion

       #region TR_3
        private string _STANDARD_TR_3 = "";
        public string STANDARD_TR_3
        {
            get { return _STANDARD_TR_3; }
            set
            {
                if (value != _STANDARD_TR_3)
                {
                    _STANDARD_TR_3 = value;
                    InvokeNotifyPropertyChanged("STANDARD_TR_3");
                }
            }
        }

        private string _BRANCH_OFFICE_CODE_TR_3 = "";
        public string BRANCH_OFFICE_CODE_TR_3
        {
            get { return _BRANCH_OFFICE_CODE_TR_3; }
            set
            {
                if (value != _BRANCH_OFFICE_CODE_TR_3)
                {
                    _BRANCH_OFFICE_CODE_TR_3 = value;
                    InvokeNotifyPropertyChanged("BRANCH_OFFICE_CODE_TR_3");
                }
            }
        }

        private string _ADDRESS_TR_3 = "";
        public string ADDRESS_TR_3
        {
            get { return _ADDRESS_TR_3; }
            set
            {
                if (value != _ADDRESS_TR_3)
                {
                    _ADDRESS_TR_3 = value;
                    InvokeNotifyPropertyChanged("ADDRESS_TR_3");
                }
            }
        }

        private string _CALL_TR_3 = "";
        public string CALL_TR_3
        {
            get { return _CALL_TR_3; }
            set
            {
                if (value != _CALL_TR_3)
                {
                    _CALL_TR_3 = value;
                    InvokeNotifyPropertyChanged("CALL_TR_3");
                }
            }
        }

       

        private string _STATUS_TR_3 = "";
        public string STATUS_TR_3
        {
            get { return _STATUS_TR_3; }
            set
            {
                if (value != _STATUS_TR_3)
                {
                    _STATUS_TR_3 = value;
                    InvokeNotifyPropertyChanged("STATUS_TR_3");
                }
            }
        }

        private string _CREATED_BY_TR_3 = "";
        public string CREATED_BY_TR_3
        {
            get { return _CREATED_BY_TR_3; }
            set
            {
                if (value != _CREATED_BY_TR_3)
                {
                    _CREATED_BY_TR_3 = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_TR_3");
                }
            }
        }

        private DateTime _DATE_CREATED_TR_3 = IM.NullT;
        public DateTime DATE_CREATED_TR_3
        {
            get { return _DATE_CREATED_TR_3; }
            set
            {
                if (value != _DATE_CREATED_TR_3)
                {
                    _DATE_CREATED_TR_3 = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_TR_3");
                }
            }
        }

        private string _DOZV_NUM_TR_3 = "";
        public string DOZV_NUM_TR_3
        {
            get { return _DOZV_NUM_TR_3; }
            set
            {
                if (value != _DOZV_NUM_TR_3)
                {
                    _DOZV_NUM_TR_3 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_TR_3");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_TR_3 = IM.NullT;
        public DateTime DOZV_DATE_TO_TR_3
        {
            get { return _DOZV_DATE_TO_TR_3; }
            set
            {
                if (value != _DOZV_DATE_TO_TR_3)
                {
                    _DOZV_DATE_TO_TR_3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_TR_3");
                }
            }
        }

        private int _REF_ID_TR_3 = IM.NullI;
        public int REF_ID_TR_3
        {
            get { return _REF_ID_TR_3; }
            set
            {
                if (value != _REF_ID_TR_3)
                {
                    _REF_ID_TR_3 = value;
                    InvokeNotifyPropertyChanged("REF_ID_TR_3");
                }
            }
        }

        private int _ID_TR_3 = IM.NullI;
        public int ID_TR_3
        {
            get { return _ID_TR_3; }
            set
            {
                if (value != _ID_TR_3)
                {
                    _ID_TR_3 = value;
                    InvokeNotifyPropertyChanged("ID_TR_3");
                }
            }
        }
        private int _OBJ_ID1_TR_3 = IM.NullI;
        public int OBJ_ID1_TR_3
        {
            get { return _OBJ_ID1_TR_3; }
            set
            {
                if (value != _OBJ_ID1_TR_3)
                {
                    _OBJ_ID1_TR_3 = value;
                    InvokeNotifyPropertyChanged("OBJ_ID1_TR_3");
                }
            }
        }

        private string _OBJ_TABLE_TR_3 = "";
        public string OBJ_TABLE_TR_3
        {
            get { return _OBJ_TABLE_TR_3; }
            set
            {
                if (value != _OBJ_TABLE_TR_3)
                {
                    _OBJ_TABLE_TR_3 = value;
                    InvokeNotifyPropertyChanged("OBJ_TABLE_TR_3");
                }
            }
        }
       #endregion

        #region AP_3
        private string _STANDARD_АР_З = "";
        public string STANDARD_АР_З
        {
            get { return _STANDARD_АР_З; }
            set
            {
                if (value != _STANDARD_АР_З)
                {
                    _STANDARD_АР_З = value;
                    InvokeNotifyPropertyChanged("STANDARD_АР_З");
                }
            }
        }

        private string _BRANCH_OFFICE_CODE_АР_З = "";
        public string BRANCH_OFFICE_CODE_АР_З
        {
            get { return _BRANCH_OFFICE_CODE_АР_З; }
            set
            {
                if (value != _BRANCH_OFFICE_CODE_АР_З)
                {
                    _BRANCH_OFFICE_CODE_АР_З = value;
                    InvokeNotifyPropertyChanged("BRANCH_OFFICE_CODE_АР_З");
                }
            }
        }

        private string _ADDRESS_АР_З = "";
        public string ADDRESS_АР_З
        {
            get { return _ADDRESS_АР_З; }
            set
            {
                if (value != _ADDRESS_АР_З)
                {
                    _ADDRESS_АР_З = value;
                    InvokeNotifyPropertyChanged("ADDRESS_АР_З");
                }
            }
        }

        private string _CALL_АР_З = "";
        public string CALL_АР_З
        {
            get { return _CALL_АР_З; }
            set
            {
                if (value != _CALL_АР_З)
                {
                    _CALL_АР_З = value;
                    InvokeNotifyPropertyChanged("CALL_АР_З");
                }
            }
        }

        private string _CALL_SIGN_АР_З = "";
        public string CALL_SIGN_АР_З
        {
            get { return _CALL_SIGN_АР_З; }
            set
            {
                if (value != _CALL_SIGN_АР_З)
                {
                    _CALL_SIGN_АР_З = value;
                    InvokeNotifyPropertyChanged("CALL_SIGN_АР_З");
                }
            }
        }

        private string _STATUS_АР_З = "";
        public string STATUS_АР_З
        {
            get { return _STATUS_АР_З; }
            set
            {
                if (value != _STATUS_АР_З)
                {
                    _STATUS_АР_З = value;
                    InvokeNotifyPropertyChanged("STATUS_АР_З");
                }
            }
        }

        private string _CREATED_BY_АР_З = "";
        public string CREATED_BY_АР_З
        {
            get { return _CREATED_BY_АР_З; }
            set
            {
                if (value != _CREATED_BY_АР_З)
                {
                    _CREATED_BY_АР_З = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_АР_З");
                }
            }
        }

        private DateTime _DATE_CREATED_АР_З = IM.NullT;
        public DateTime DATE_CREATED_АР_З
        {
            get { return _DATE_CREATED_АР_З; }
            set
            {
                if (value != _DATE_CREATED_АР_З)
                {
                    _DATE_CREATED_АР_З = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_АР_З");
                }
            }
        }

        private string _DOZV_NUM_АР_З = "";
        public string DOZV_NUM_АР_З
        {
            get { return _DOZV_NUM_АР_З; }
            set
            {
                if (value != _DOZV_NUM_АР_З)
                {
                    _DOZV_NUM_АР_З = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_АР_З");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_АР_З = IM.NullT;
        public DateTime DOZV_DATE_TO_АР_З
        {
            get { return _DOZV_DATE_TO_АР_З; }
            set
            {
                if (value != _DOZV_DATE_TO_АР_З)
                {
                    _DOZV_DATE_TO_АР_З = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_АР_З");
                }
            }
        }

        private string _DOZV_NUM_NEW_АР_З = "";
        public string DOZV_NUM_NEW_АР_З
        {
            get { return _DOZV_NUM_NEW_АР_З; }
            set
            {
                if (value != _DOZV_NUM_NEW_АР_З)
                {
                    _DOZV_NUM_NEW_АР_З = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_NEW_АР_З");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_NEW_АР_З = IM.NullT;
        public DateTime DOZV_DATE_TO_NEW_АР_З
        {
            get { return _DOZV_DATE_TO_NEW_АР_З; }
            set
            {
                if (value != _DOZV_DATE_TO_NEW_АР_З)
                {
                    _DOZV_DATE_TO_NEW_АР_З = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_NEW_АР_З");
                }
            }
        }

        private int _REF_ID_АР_З = IM.NullI;
        public int REF_ID_АР_З
        {
            get { return _REF_ID_АР_З; }
            set
            {
                if (value != _REF_ID_АР_З)
                {
                    _REF_ID_АР_З = value;
                    InvokeNotifyPropertyChanged("REF_ID_АР_З");
                }
            }
        }

        private int _ID_АР_З = IM.NullI;
        public int ID_АР_З
        {
            get { return _ID_АР_З; }
            set
            {
                if (value != _ID_АР_З)
                {
                    _ID_АР_З = value;
                    InvokeNotifyPropertyChanged("ID_АР_З");
                }
            }
        }
        private int _OBJ_ID1_АР_З = IM.NullI;
        public int OBJ_ID1_АР_З
        {
            get { return _OBJ_ID1_АР_З; }
            set
            {
                if (value != _OBJ_ID1_АР_З)
                {
                    _OBJ_ID1_АР_З = value;
                    InvokeNotifyPropertyChanged("OBJ_ID1_АР_З");
                }
            }
        }

        private string _OBJ_TABLE_АР_З = "";
        public string OBJ_TABLE_АР_З
        {
            get { return _OBJ_TABLE_АР_З; }
            set
            {
                if (value != _OBJ_TABLE_АР_З)
                {
                    _OBJ_TABLE_АР_З = value;
                    InvokeNotifyPropertyChanged("OBJ_TABLE_АР_З");
                }
            }
        }
        #endregion

        #region RS
        private string _ADDRESS_RS = "";
        public string ADDRESS_RS
        {
            get { return _ADDRESS_RS; }
            set
            {
                if (value != _ADDRESS_RS)
                {
                    _ADDRESS_RS = value;
                    InvokeNotifyPropertyChanged("ADDRESS_RS");
                }
            }
        }

        private string _CREATED_BY_RS = "";
        public string CREATED_BY_RS
        {
            get { return _CREATED_BY_RS; }
            set
            {
                if (value != _CREATED_BY_RS)
                {
                    _CREATED_BY_RS = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_RS");
                }
            }
        }

        private DateTime _DATE_CREATED_RS = IM.NullT;
        public DateTime DATE_CREATED_RS
        {
            get { return _DATE_CREATED_RS; }
            set
            {
                if (value != _DATE_CREATED_RS)
                {
                    _DATE_CREATED_RS = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_RS");
                }
            }
        }

        private string _STANDARD_RS = "";
        public string STANDARD_RS
        {
            get { return _STANDARD_RS; }
            set
            {
                if (value != _STANDARD_RS)
                {
                    _STANDARD_RS = value;
                    InvokeNotifyPropertyChanged("STANDARD_RS");
                }
            }
        }

        private string _STATUS_RS = "";
        public string STATUS_RS
        {
            get { return _STATUS_RS; }
            set
            {
                if (value != _STATUS_RS)
                {
                    _STATUS_RS = value;
                    InvokeNotifyPropertyChanged("STATUS_RS");
                }
            }
        }

        private string _STATUS_MK_RS = "";
        public string STATUS_MK_RS
        {
            get { return _STATUS_MK_RS; }
            set
            {
                if (value != _STATUS_MK_RS)
                {
                    _STATUS_MK_RS = value;
                    InvokeNotifyPropertyChanged("STATUS_MK_RS");
                }
            }
        }

        private string _LICENCES_RS = "";
        public string LICENCES_RS
        {
            get { return _LICENCES_RS; }
            set
            {
                if (value != _LICENCES_RS)
                {
                    _LICENCES_RS = value;
                    InvokeNotifyPropertyChanged("LICENCES_RS");
                }
            }
        }

        private string _DOZV_NUM_RS = "";
        public string DOZV_NUM_RS
        {
            get { return _DOZV_NUM_RS; }
            set
            {
                if (value != _DOZV_NUM_RS)
                {
                    _DOZV_NUM_RS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_RS");
                }
            }
        }

        private DateTime _DOZV_DATE_FROM_RS = IM.NullT;
        public DateTime DOZV_DATE_FROM_RS
        {
            get { return _DOZV_DATE_FROM_RS; }
            set
            {
                if (value != _DOZV_DATE_FROM_RS)
                {
                    _DOZV_DATE_FROM_RS = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_RS");
                }
            }
        }

        private string _DOZV_NUM_NEW_RS = "";
        public string DOZV_NUM_NEW_RS
        {
            get { return _DOZV_NUM_NEW_RS; }
            set
            {
                if (value != _DOZV_NUM_NEW_RS)
                {
                    _DOZV_NUM_NEW_RS = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_NEW_RS");
                }
            }
        }

        private string _CONC_NUM_RS = "";
        public string CONC_NUM_RS
        {
            get { return _CONC_NUM_RS; }
            set
            {
                if (value != _CONC_NUM_RS)
                {
                    _CONC_NUM_RS = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_RS");
                }
            }
        }

        private DateTime _CONC_DATE_FROM_RS = IM.NullT;
        public DateTime CONC_DATE_FROM_RS
        {
            get { return _CONC_DATE_FROM_RS; }
            set
            {
                if (value != _CONC_DATE_FROM_RS)
                {
                    _CONC_DATE_FROM_RS = value;
                    InvokeNotifyPropertyChanged("CONC_DATE_FROM_RS");
                }
            }
        }

        private string _RX_FREQ_TXT_RS = "";
        public string RX_FREQ_TXT_RS
        {
            get { return _RX_FREQ_TXT_RS; }
            set
            {
                if (value != _RX_FREQ_TXT_RS)
                {
                    _RX_FREQ_TXT_RS = value;
                    InvokeNotifyPropertyChanged("RX_FREQ_TXT_RS");
                }
            }
        }

        private string _TX_FREQ_TXT_RS = "";
        public string TX_FREQ_TXT_RS
        {
            get { return _TX_FREQ_TXT_RS; }
            set
            {
                if (value != _TX_FREQ_TXT_RS)
                {
                    _TX_FREQ_TXT_RS = value;
                    InvokeNotifyPropertyChanged("TX_FREQ_TXT_RS");
                }
            }
        }

        private int _ID_RS = IM.NullI;
        public int ID_RS
        {
            get { return _ID_RS; }
            set
            {
                if (value != _ID_RS)
                {
                    _ID_RS = value;
                    InvokeNotifyPropertyChanged("ID_RS");
                }
            }
        }
        #endregion

        #region R2

        private string _ADDRESS_R2 = "";
        public string ADDRESS_R2
        {
            get { return _ADDRESS_R2; }
            set
            {
                if (value != _ADDRESS_R2)
                {
                    _ADDRESS_R2 = value;
                    InvokeNotifyPropertyChanged("ADDRESS_R2");
                }
            }
        }

        private double _FREQ_R2 = IM.NullD;
        public double FREQ_R2
        {
            get { return _FREQ_R2; }
            set
            {
                if (value != _FREQ_R2)
                {
                    _FREQ_R2 = value;
                    InvokeNotifyPropertyChanged("FREQ_R2");
                }
            }
        }

        private string _CITY_R2 = "";
        public string CITY_R2
        {
            get { return _CITY_R2; }
            set
            {
                if (value != _CITY_R2)
                {
                    _CITY_R2 = value;
                    InvokeNotifyPropertyChanged("CITY_R2");
                }
            }
        }

        private string _DOZV_NUM_R2 = "";
        public string DOZV_NUM_R2
        {
            get { return _DOZV_NUM_R2; }
            set
            {
                if (value != _DOZV_NUM_R2)
                {
                    _DOZV_NUM_R2 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_R2");
                }
            }
        }


        private DateTime _DOZV_DATE_FROM_R2 = IM.NullT;
        public DateTime DOZV_DATE_FROM_R2
        {
            get { return _DOZV_DATE_FROM_R2; }
            set
            {
                if (value != _DOZV_DATE_FROM_R2)
                {
                    _DOZV_DATE_FROM_R2 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_FROM_R2");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_R2 = IM.NullT;
        public DateTime DOZV_DATE_TO_R2
        {
            get { return _DOZV_DATE_TO_R2; }
            set
            {
                if (value != _DOZV_DATE_TO_R2)
                {
                    _DOZV_DATE_TO_R2 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_R2");
                }
            }
        }

        private string _STATUS_R2 = "";
        public string STATUS_R2
        {
            get { return _STATUS_R2; }
            set
            {
                if (value != _STATUS_R2)
                {
                    _STATUS_R2 = value;
                    InvokeNotifyPropertyChanged("STATUS_R2");
                }
            }
        }

        private string _CONC_NUM_R2 = "";
        public string CONC_NUM_R2
        {
            get { return _CONC_NUM_R2; }
            set
            {
                if (value != _CONC_NUM_R2)
                {
                    _CONC_NUM_R2 = value;
                    InvokeNotifyPropertyChanged("CONC_NUM_R2");
                }
            }
        }

        private DateTime _CONC_DATE_FROM_R2 = IM.NullT;
        public DateTime CONC_DATE_FROM_R2
        {
            get { return _CONC_DATE_FROM_R2; }
            set
            {
                if (value != _CONC_DATE_FROM_R2)
                {
                    _CONC_DATE_FROM_R2 = value;
                    InvokeNotifyPropertyChanged("CONC_DATE_FROM_R2");
                }
            }
        }

        private string _KOATUU_R2 = "";
        public string KOATUU_R2
        {
            get { return _KOATUU_R2; }
            set
            {
                if (value != _KOATUU_R2)
                {
                    _KOATUU_R2 = value;
                    InvokeNotifyPropertyChanged("KOATUU_R2");
                }
            }
        }

        private int _REF_ID_R2 = IM.NullI;
        public int REF_ID_R2
        {
            get { return _REF_ID_R2; }
            set
            {
                if (value != _REF_ID_R2)
                {
                    _REF_ID_R2 = value;
                    InvokeNotifyPropertyChanged("REF_ID_R2");
                }
            }
        }

        private int _ID_R2 = IM.NullI;
        public int ID_R2
        {
            get { return _ID_R2; }
            set
            {
                if (value != _ID_R2)
                {
                    _ID_R2 = value;
                    InvokeNotifyPropertyChanged("ID_R2");
                }
            }
        }

        private int _OBJ_ID1_R2 = IM.NullI;
        public int OBJ_ID1_R2
        {
            get { return _OBJ_ID1_R2; }
            set
            {
                if (value != _OBJ_ID1_R2)
                {
                    _OBJ_ID1_R2 = value;
                    InvokeNotifyPropertyChanged("OBJ_ID1_R2");
                }
            }
        }

        private string _OBJ_TABLE_R2 = "";
        public string OBJ_TABLE_R2
        {
            get { return _OBJ_TABLE_R2; }
            set
            {
                if (value != _OBJ_TABLE_R2)
                {
                    _OBJ_TABLE_R2 = value;
                    InvokeNotifyPropertyChanged("OBJ_TABLE_R2");
                }
            }
        }

        #endregion

        #region AR_3

        private string _STANDARD_AR_3 = "";
        public string STANDARD_AR_3
        {
            get { return _STANDARD_AR_3; }
            set
            {
                if (value != _STANDARD_AR_3)
                {
                    _STANDARD_AR_3 = value;
                    InvokeNotifyPropertyChanged("STANDARD_AR_3");
                }
            }
        }

        private string _EXPL_TYPE_AR_3 = "";
        public string EXPL_TYPE_AR_3
        {
            get { return _EXPL_TYPE_AR_3; }
            set
            {
                if (value != _EXPL_TYPE_AR_3)
                {
                    _EXPL_TYPE_AR_3 = value;
                    InvokeNotifyPropertyChanged("EXPL_TYPE_AR_3");
                }
            }
        }

        private string _ADDRESS_AR_3 = "";
        public string ADDRESS_AR_3
        {
            get { return _ADDRESS_AR_3; }
            set
            {
                if (value != _ADDRESS_AR_3)
                {
                    _ADDRESS_AR_3 = value;
                    InvokeNotifyPropertyChanged("ADDRESS_AR_3");
                }
            }
        }

        private string _LOCATION_AR_3 = "";
        public string LOCATION_AR_3
        {
            get { return _LOCATION_AR_3; }
            set
            {
                if (value != _LOCATION_AR_3)
                {
                    _LOCATION_AR_3 = value;
                    InvokeNotifyPropertyChanged("LOCATION_AR_3");
                }
            }
        }

        private string _CALL_AR_3 = "";
        public string CALL_AR_3
        {
            get { return _CALL_AR_3; }
            set
            {
                if (value != _CALL_AR_3)
                {
                    _CALL_AR_3 = value;
                    InvokeNotifyPropertyChanged("CALL_AR_3");
                }
            }
        }

        private string _FACTORY_NUM_AR_3 = "";
        public string FACTORY_NUM_AR_3
        {
            get { return _FACTORY_NUM_AR_3; }
            set
            {
                if (value != _FACTORY_NUM_AR_3)
                {
                    _FACTORY_NUM_AR_3 = value;
                    InvokeNotifyPropertyChanged("FACTORY_NUM_AR_3");
                }
            }
        }

        private string _CALL_SIGN_AR_3 = "";
        public string CALL_SIGN_AR_3
        {
            get { return _CALL_SIGN_AR_3; }
            set
            {
                if (value != _CALL_SIGN_AR_3)
                {
                    _CALL_SIGN_AR_3 = value;
                    InvokeNotifyPropertyChanged("CALL_SIGN_AR_3");
                }
            }
        }


        private string _STATUS_AR_3 = "";
        public string STATUS_AR_3
        {
            get { return _STATUS_AR_3; }
            set
            {
                if (value != _STATUS_AR_3)
                {
                    _STATUS_AR_3 = value;
                    InvokeNotifyPropertyChanged("STATUS_AR_3");
                }
            }
        }

        private string _CREATED_BY_AR_3 = "";
        public string CREATED_BY_AR_3
        {
            get { return _CREATED_BY_AR_3; }
            set
            {
                if (value != _CREATED_BY_AR_3)
                {
                    _CREATED_BY_AR_3 = value;
                    InvokeNotifyPropertyChanged("CREATED_BY_AR_3");
                }
            }
        }

        private DateTime _DATE_CREATED_AR_3 = IM.NullT;
        public DateTime DATE_CREATED_AR_3
        {
            get { return _DATE_CREATED_AR_3; }
            set
            {
                if (value != _DATE_CREATED_AR_3)
                {
                    _DATE_CREATED_AR_3 = value;
                    InvokeNotifyPropertyChanged("DATE_CREATED_AR_3");
                }
            }
        }

        private string _DOZV_NUM_AR_3 = "";
        public string DOZV_NUM_AR_3
        {
            get { return _DOZV_NUM_AR_3; }
            set
            {
                if (value != _DOZV_NUM_AR_3)
                {
                    _DOZV_NUM_AR_3 = value;
                    InvokeNotifyPropertyChanged("DOZV_NUM_AR_3");
                }
            }
        }

        private DateTime _DOZV_DATE_TO_AR_3 = IM.NullT;
        public DateTime DOZV_DATE_TO_AR_3
        {
            get { return _DOZV_DATE_TO_AR_3; }
            set
            {
                if (value != _DOZV_DATE_TO_AR_3)
                {
                    _DOZV_DATE_TO_AR_3 = value;
                    InvokeNotifyPropertyChanged("DOZV_DATE_TO_AR_3");
                }
            }
        }


        private int _REF_ID_AR_3 = IM.NullI;
        public int REF_ID_AR_3
        {
            get { return _REF_ID_AR_3; }
            set
            {
                if (value != _REF_ID_AR_3)
                {
                    _REF_ID_AR_3 = value;
                    InvokeNotifyPropertyChanged("REF_ID_AR_3");
                }
            }
        }

        private int _ID_AR_3 = IM.NullI;
        public int ID_AR_3
        {
            get { return _ID_AR_3; }
            set
            {
                if (value != _ID_AR_3)
                {
                    _ID_AR_3 = value;
                    InvokeNotifyPropertyChanged("ID_AR_3");
                }
            }
        }

        private int _OBJ_ID1_AR_3 = IM.NullI;
        public int OBJ_ID1_AR_3
        {
            get { return _OBJ_ID1_AR_3; }
            set
            {
                if (value != _OBJ_ID1_AR_3)
                {
                    _OBJ_ID1_AR_3 = value;
                    InvokeNotifyPropertyChanged("OBJ_ID1_AR_3");
                }
            }
        }

        private string _OBJ_TABLE_AR_3 = "";
        public string OBJ_TABLE_AR_3
        {
            get { return _OBJ_TABLE_AR_3; }
            set
            {
                if (value != _OBJ_TABLE_AR_3)
                {
                    _OBJ_TABLE_AR_3 = value;
                    InvokeNotifyPropertyChanged("OBJ_TABLE_AR_3");
                }
            }
        }


        #endregion
    }
}
