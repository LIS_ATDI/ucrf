﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.ComponentModel;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.AppPacket;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Extensions;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.UtilityClass;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.AppPacket.Forms;
using XICSM.UcrfRfaNET.HelpClasses.Classes;
using OrmCs;

namespace XICSM.UcrfRfaNET
{
    //======================================================
    /// <summary>
    /// Класс, который управляет бизнес логикой пакета документов
    /// </summary>
    public class CPacket : NotifyPropertyChanged
    {
        //===================================================
        // Поля
        protected bool isNewRecord = true;  //Новая запись?
        public bool NewRecord { get { return isNewRecord; } }
        protected int idPacket = 0;         //ID пакета
        protected int idOwner = 0;          //ID контрагента
        public int OwnerId { get { return idOwner; } }
        public List<ExtendedColumnsProperty> ListAllColumns { get; set; }
        public List<ExtendedColumnsProperty> ListSelectedColumns { get; set; }
        public Dictionary<string, AppType> ListAppType { get; set; }
        protected List<EPacketType> ListPacketType;
        protected EPacketType packetType = EPacketType.PckUnknown;
        protected AppType applType = AppType.AppUnknown;
        public DataGridView ApplGrid = null;
        protected string radioTech = CRadioTech.UNKNOWN;   //Радиотехнология - стандарт
        public string NumberIn { get; set; }
        public string NumberOut { get; set; }
       
        public DateTime DateIn { get; set; }
        protected DateTime dateOut = DateTime.Now;
        public UcrfDepartment RoleCreatedUnder { get; set; }
        public string CreatedBy { get; set; }
        public EPacketType PacketType { get { return packetType; } }
        public AppType ApplType { get { return applType; } }
        private StatePacket packetState = StatePacket.UNKNOWN;
        public bool ReadOnly { get; set; }
        public ComboBoxDictionaryList<string, string> standList = new ComboBoxDictionaryList<string, string>();
        public bool isDefaultListColumns = true; //признак, что нужно загружать список полей по умолчанию
        public bool SpecialStatus { get; set; }
        public int PacketId { get { return idPacket; } }
        

        //----
        public int TotalTrsmCnt // Загальна кылькість передавачів
        {
            get
            {
                int retVal = 0;
                foreach (PacketRowBase rowUrzp in _applListRows)
                    retVal += rowUrzp.TransmCnt;
                return retVal;
            }
            set
            {
                if (value > -1)
                    InvokeNotifyPropertyChanged("TotalTrsmCnt");
            }

        } // Загальна кылькість передавачів
        
        /// <summary>
        /// Кол-во заявок в пакете
        /// </summary>
        public int CountAppl
        {
            get { return _applListRows.Count; }
            set 
            { 
                if(_applListRows.Count != value)
                    InvokeNotifyPropertyChanged("CountAppl");
            }
        }

        private string _foreighnStatus;
        public string ForeighnStatus
        {
            get
            {
                return _foreighnStatus;
            }
            set
            {
                if (value != _foreighnStatus)
                    _foreighnStatus = value;
                InvokeNotifyPropertyChanged("ForeighnStatus");
            }
        }

        //--------
        protected SortableBindingList<PacketRowBase> _applListRows = new SortableBindingList<PacketRowBase>();
        public SortableBindingList<PacketRowBase> Applications { get { return _applListRows; } }
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public CPacket()
        {
            ReadOnly = CUsers.CanUpdatePacket();
            CreatedBy = IM.ConnectedUser();
        }
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        public CPacket(int PacketId)
            : this()
        {
            RoleCreatedUnder = CUsers.GetCurDepartment();
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Loading packet")))
            {
                IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
                rsPacket.Select("ID,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD,CONTENT,CREATED_BY,STATUS");
                rsPacket.Select("STATUS_COORD");
                rsPacket.Select("CREATED_MANAGEMENT");
                rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, PacketId);
                try
                {
                    rsPacket.Open();
                    if (!rsPacket.IsEOF())
                    {// Пакет существует
                        isNewRecord = false;
                        idPacket = rsPacket.GetI("ID");
                        idOwner = rsPacket.GetI("OWNER_ID");
                        NumberIn = rsPacket.GetS("NUMBER_IN");
                        NumberOut = rsPacket.GetS("NUMBER_OUT");
                        DateIn = rsPacket.GetT("DATE_IN");
                        DateOut = rsPacket.GetT("DATE_OUT");
                        ForeighnStatus = rsPacket.GetS("STATUS_COORD");
                        radioTech = CRadioTech.ToStandard(rsPacket.GetS("STANDARD"));
                        applType = appTypeConvert.StringToAppType(rsPacket.GetS("TYPE"));
                        RoleCreatedUnder = rsPacket.GetS("CREATED_MANAGEMENT").ToUcrfDepartment();
                        CreatedBy = rsPacket.GetS("CREATED_BY");
                        if (RoleCreatedUnder == UcrfDepartment.UNKNOWN)
                        {
                            // Не смогли определить управления в котором создан пакет
                            // Работаем по старому алгоритму
                            MessageBox.Show("Не в состоянии определить подразделение создания пакета.", CLocaliz.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            RoleCreatedUnder = CUsers.GetAllUserDepartments(CreatedBy);
                        }
                        try
                        {
                            packetType = (EPacketType)Enum.Parse(typeof(EPacketType), rsPacket.GetS("CONTENT"));
                        }
                        catch
                        {
                            MessageBox.Show("Не в состоянии определить тип содержимого пакета.", CLocaliz.Warning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            packetType = EPacketType.PckUnknown;
                        }
                        try
                        {
                            packetState = (StatePacket)Enum.Parse(typeof(StatePacket), rsPacket.GetS("STATUS"));
                        }
                        catch
                        {
                            packetState = StatePacket.UNKNOWN;
                        }
                    }
                    else
                    {// Новый пакет
                        idPacket = IM.AllocID(PlugTbl.itblXnrfaPacket, 1, -1);
                        idOwner = 0;
                        NumberIn = "";
                        NumberOut = "";
                        radioTech = CRadioTech.UNKNOWN;
                        DateIn = DateTime.Now;
                        DateOut = DateTime.Now;
                        packetState = StatePacket.UNKNOWN;
                    }
                }
                finally
                {
                    rsPacket.Close();
                    rsPacket.Destroy();
                }

                ListAllColumns = new List<ExtendedColumnsProperty>();


                //Mandatory fields
                    ListAllColumns.Add(new ExtendedColumnsProperty("FullPosition", CLocaliz.TxT("Address"), "gridPacketAddress", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("CallSign", CLocaliz.TxT("CallSign"), "gridPacketCallSign", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocNumConclusion", CLocaliz.TxT("Number conclusion"), "gridNumConclusion", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocNumPerm", CLocaliz.TxT("# permission"), "gridPacketDocNumPerm", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("Fio", CLocaliz.TxT("APPL CREATED BY"), "gridPacketCreatedBy", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("CreatedDate", CLocaliz.TxT("Created date"), "gridPacketCreatedDate", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("Sid", CLocaliz.TxT("SID"), "gridPacketSid", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("Standard", CLocaliz.TxT("APPL STANDARD"), "gridPacketStandard", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("PtkStatus", CLocaliz.TxT("State"), "gridPacketState", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("TransmCnt", CLocaliz.TxT("Transmitter count"), "gridPacketTrCount", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocNumTv", CLocaliz.TxT("Document number TV"), "gridDocNumTv", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocDate", CLocaliz.TxT("Document date"), "gridDocDate", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocEndDate", CLocaliz.TxT("Document end date"), "gridDocEndDate", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocNumUrcm", CLocaliz.TxT("Document number URCM"), "gridDocNumUrcm", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("DocUrcmDate", CLocaliz.TxT("Document URCM date"), "gridDocUrcmDate", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("PtkDateFrom", CLocaliz.TxT("PTK date from"), "gridPtkDateFrom", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("PtkDateTo", CLocaliz.TxT("PTK date to"), "gridPtkDateTo", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("IsTv", CLocaliz.TxT("Is TV"), "gridIsTv", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("IsNv", CLocaliz.TxT("Is NV"), "gridIsNv", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("Remark", CLocaliz.TxT("Remark"), "gridRemark", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("Status", CLocaliz.TxT("Status"), "gridApplStatus", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("ForeighnStatus", CLocaliz.TxT("Status coord"), "gridApplStatusCoord", AppType.AppUnknown, true, 1));
                    ListAllColumns.Add(new ExtendedColumnsProperty("ApplId", CLocaliz.TxT("ID"), "gridPacketID", AppType.AppUnknown, true, 1));

                //for FIndStation
                //ListAllColumns.Add(new ExtendedColumnsProperty("chId", CLocaliz.TxT("ID"), "chId", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chNumberDOZV", CLocaliz.TxT("Number DOZV"), "chNumberDOZV", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chNumberVISN", CLocaliz.TxT("Number VISN"), "chNumberVISN", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chStandard", CLocaliz.TxT("Standard"), "chStandard", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chOwnerName", CLocaliz.TxT("Owner name"), "chOwnerName", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chPosition", CLocaliz.TxT("Position"), "chPosition", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chTXFreq", CLocaliz.TxT("TX freq"), "chTXFreq", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chRXFreq", CLocaliz.TxT("Rx freq"), "chRXFreq", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chNameRES", CLocaliz.TxT("RES name"), "chNameRES", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chDesigEmission", CLocaliz.TxT("Desig emission"), "chDesigEmission", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chAntName", CLocaliz.TxT("Antenna name"), "chAntName", AppType.AppUnknown, true, 2));
                ListAllColumns.Add(new ExtendedColumnsProperty("chKOATUU", CLocaliz.TxT("KOATUU"), "chKOATUU", AppType.AppUnknown, true, 2));
                


                // BS
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_BS",CLocaliz.TxT("ADDRESS_BS"),"ADDRESS",AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_BS",CLocaliz.TxT("CREATED_BY_BS"),"CREATED_BY",AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_BS", CLocaliz.TxT("DATE_CREATED_BS"), "DATE_CREATED", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_BS", CLocaliz.TxT("STANDARD_BS"), "STANDARD", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_BS", CLocaliz.TxT("STATUS_BS"), "STATUS", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("LICENCE_NUM_BS", CLocaliz.TxT("LICENCE_NUM_BS"), "LICENCE_NUM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("RX_FREQ_TXT_BS", CLocaliz.TxT("RX_FREQ_TXT_BS"), "RX_FREQ_TXT", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("TX_FREQ_TXT_BS", CLocaliz.TxT("TX_FREQ_TXT_BS"), "TX_FREQ_TXT", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DESIG_EM_BS", CLocaliz.TxT("DESIG_EM_BS"), "DESIG_EM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("EQUIP_NAME_BS", CLocaliz.TxT("EQUIP_NAME_BS"), "EQUIP_NAME", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ANT_NAME_BS", CLocaliz.TxT("ANT_NAME_BS"), "ANT_NAME", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("KOATUU_BS", CLocaliz.TxT("KOATUU_BS"), "KOATUU", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_BS", CLocaliz.TxT("CONC_NUM_BS"), "CONC_NUM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_DATE_FROM_BS", CLocaliz.TxT("CONC_DATE_FROM_BS"), "CONC_DATE_FROM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_BS", CLocaliz.TxT("DOZV_NUM_BS"), "DOZV_NUM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_BS", CLocaliz.TxT("DOZV_DATE_FROM_BS"), "DOZV_DATE_FROM", AppType.AppBS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_NEW_BS", CLocaliz.TxT("DOZV_NUM_NEW_BS"), "DOZV_NUM_NEW", AppType.AppBS));


                // A3
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_A3", CLocaliz.TxT("STANDARD_A3"), "STANDARD", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("TYPE_STAT_A3", CLocaliz.TxT("TYPE_STAT_A3"), "TYPE_STAT", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_A3", CLocaliz.TxT("ADDRESS_A3"), "ADDRESS", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OWNER_NAME_A3", CLocaliz.TxT("OWNER_NAME_A3"), "OWNER_NAME", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_A3", CLocaliz.TxT("CALL_SIGN_A3"), "CALL_SIGN", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CATEG_OPER_A3", CLocaliz.TxT("CATEG_OPER_A3"), "CATEG_OPER", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("APC_A3", CLocaliz.TxT("APC_A3"), "APC", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_A3", CLocaliz.TxT("DOZV_NUM_A3"), "DOZV_NUM", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_A3", CLocaliz.TxT("DOZV_DATE_FROM_A3"), "DOZV_DATE_FROM", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_A3", CLocaliz.TxT("DOZV_DATE_TO_A3"), "DOZV_DATE_TO", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_PRINT_A3", CLocaliz.TxT("DOZV_DATE_PRINT_A3"), "DOZV_DATE_PRINT", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_CANCEL_A3", CLocaliz.TxT("DOZV_DATE_CANCEL_A3"), "DOZV_DATE_CANCEL", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_CREATED_BY_A3", CLocaliz.TxT("DOZV_CREATED_BY_A3"), "DOZV_CREATED_BY", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_FILE_A3", CLocaliz.TxT("DOZV_FILE_A3"), "DOZV_FILE", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_A3", CLocaliz.TxT("STATUS_A3"), "STATUS", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_A3", CLocaliz.TxT("CREATED_BY_A3"), "CREATED_BY", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("REF_ID_A3", CLocaliz.TxT("REF_ID_A3"), "REF_ID", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_A3", CLocaliz.TxT("ID_A3"), "ID", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_ID1_A3", CLocaliz.TxT("OBJ_ID1_A3"), "OBJ_ID1", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_TABLE_A3", CLocaliz.TxT("OBJ_TABLE_A3"), "OBJ_TABLE", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("EMPLOYEE_ID_A3", CLocaliz.TxT("EMPLOYEE_ID_A3"), "EMPLOYEE_ID", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_A3", CLocaliz.TxT("DATE_CREATED_A3"), "DATE_CREATED", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_NEW_A3", CLocaliz.TxT("DOZV_NUM_NEW_A3"), "DOZV_NUM_NEW", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_NEW_A3", CLocaliz.TxT("DOZV_DATE_FROM_NEW_A3"), "DOZV_DATE_FROM_NEW", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_NEW_A3", CLocaliz.TxT("DOZV_DATE_TO_NEW_A3"), "DOZV_DATE_TO_NEW", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_CREATED_BY_NEW_A3", CLocaliz.TxT("DOZV_CREATED_BY_NEW_A3"), "DOZV_CREATED_BY_NEW", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_FILE_NEW_A3", CLocaliz.TxT("DOZV_FILE_NEW_A3"), "DOZV_FILE_NEW", AppType.AppA3));
                ListAllColumns.Add(new ExtendedColumnsProperty("PROVINCE_A3", CLocaliz.TxT("PROVINCE_A3"), "PROVINCE", AppType.AppA3));

                // TR
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_TR", CLocaliz.TxT("STATUS_TR"), "STATUS", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_TR", CLocaliz.TxT("ADDRESS_TR"), "ADDRESS", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_TR", CLocaliz.TxT("STANDARD_TR"), "STANDARD", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_TR", CLocaliz.TxT("CALL_SIGN_TR"), "CALL_SIGN", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_TR", CLocaliz.TxT("DOZV_NUM_TR"), "DOZV_NUM", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_TR", CLocaliz.TxT("DOZV_DATE_TO_TR"), "DOZV_DATE_TO", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("EQUIP_NAME_TR", CLocaliz.TxT("EQUIP_NAME_TR"), "EQUIP_NAME", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("CLASS_TR", CLocaliz.TxT("CLASS_TR"), "CLASS", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_TR", CLocaliz.TxT("CONC_NUM_TR"), "CONC_NUM", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("RX_FREQ_TXT_TR", CLocaliz.TxT("RX_FREQ_TXT_TR"), "RX_FREQ_TXT", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("TX_FREQ_TXT_TR", CLocaliz.TxT("TX_FREQ_TXT_TR"), "TX_FREQ_TXT", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DIST_BORDER_TR", CLocaliz.TxT("DIST_BORDER_TR"), "DIST_BORDER", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_TR", CLocaliz.TxT("DOZV_DATE_FROM_TR"), "DOZV_DATE_FROM", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_TR", CLocaliz.TxT("TABLE_ID_TR"), "TABLE_ID", AppType.AppTR));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_TR", CLocaliz.TxT("ID_TR"), "ID", AppType.AppTR));
                
                // RR
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_RR", CLocaliz.TxT("STATUS_RR"), "STATUS", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_RR", CLocaliz.TxT("ADDRESS_RR"), "ADDRESS", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("CID_RR", CLocaliz.TxT("CID_RR"), "CID", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_RR", CLocaliz.TxT("DOZV_NUM_RR"), "DOZV_NUM", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_RR", CLocaliz.TxT("DOZV_DATE_TO_RR"), "DOZV_DATE_TO", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("EQUIP_NAME_RR", CLocaliz.TxT("EQUIP_NAME_RR"), "EQUIP_NAME", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_RR", CLocaliz.TxT("CONC_NUM_RR"), "CONC_NUM", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_RR", CLocaliz.TxT("DOZV_DATE_FROM_RR"), "DOZV_DATE_FROM", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_RR", CLocaliz.TxT("TABLE_ID_RR"), "TABLE_ID", AppType.AppRR));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_RR", CLocaliz.TxT("ID_RR"), "ID", AppType.AppRR));


                // TV
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_TV2", CLocaliz.TxT("STATUS_TV2"), "STATUS", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_TV2", CLocaliz.TxT("ADDRESS_TV2"), "ADDRESS", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CHANNEL_TV2", CLocaliz.TxT("CHANNEL_TV2"), "CHANNEL", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_TV2", CLocaliz.TxT("DOZV_NUM_TV2"), "DOZV_NUM", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_TV2", CLocaliz.TxT("DOZV_DATE_TO_TV2"), "DOZV_DATE_TO", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("EQUIP_NAME_TV2", CLocaliz.TxT("EQUIP_NAME_TV2"), "EQUIP_NAME", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_TV2", CLocaliz.TxT("CONC_NUM_TV2"), "CONC_NUM", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_TV2", CLocaliz.TxT("DOZV_DATE_FROM_TV2"), "DOZV_DATE_FROM", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_B_TV2", CLocaliz.TxT("CREATED_B_TV2"), "CREATED_BY", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_TV2", CLocaliz.TxT("CREATED_B_TV2"), "DATE_CREATED", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CITY_TV2", CLocaliz.TxT("CITY_TV2"), "CITY", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_TV2", CLocaliz.TxT("TABLE_ID_TV2"), "TABLE_ID", AppType.AppTV2));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_TV2", CLocaliz.TxT("ID_TV2"), "ID", AppType.AppTV2));


                // ZRS
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_ZRS", CLocaliz.TxT("STATUS_ZRS"), "STATUS", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("REG_PORT_ZRS", CLocaliz.TxT("REG_PORT_ZRS"), "REG_PORT", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_ZRS", CLocaliz.TxT("CREATED_BY_ZRS"), "CREATED_BY", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_ZRS", CLocaliz.TxT("DATE_CREATED_ZRS"), "DATE_CREATED", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_ZRS", CLocaliz.TxT("STANDARD_ZRS"), "STANDARD", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_ZRS", CLocaliz.TxT("DOZV_NUM_ZRS"), "DOZV_NUM", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_ZRS", CLocaliz.TxT("DOZV_DATE_FROM_ZRS"), "DOZV_DATE_FROM", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_ZRS", CLocaliz.TxT("DOZV_DATE_TO_ZRS"), "DOZV_DATE_TO", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("SHIP_NAME_ZRS", CLocaliz.TxT("SHIP_NAME_ZRS"), "SHIP_NAME", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("MMSI_NUM_ZRS", CLocaliz.TxT("MMSI_NUM_ZRS"), "MMSI_NUM", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ATIS_ZRS", CLocaliz.TxT("ATIS_ZRS"), "ATIS", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_ZRS", CLocaliz.TxT("CALL_SIGN_ZRS"), "CALL_SIGN", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_ZRS", CLocaliz.TxT("TABLE_ID_ZRS"), "TABLE_ID", AppType.AppZRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_ZRS", CLocaliz.TxT("ID_ZRS"), "ID", AppType.AppZRS));
                

                // VP
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_VP", CLocaliz.TxT("STATUS_VP"), "STATUS", AppType.AppVP ));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_VP", CLocaliz.TxT("ADDRESS_VP"), "ADDRESS", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_VP", CLocaliz.TxT("CREATED_BY_VP"), "CREATED_BY", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_VP", CLocaliz.TxT("DATE_CREATED_VP"), "DATE_CREATED", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_VP", CLocaliz.TxT("STANDARD_VP"), "STANDARD", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_VP", CLocaliz.TxT("DOZV_NUM_VP"), "DOZV_NUM", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_VP", CLocaliz.TxT("DOZV_DATE_FROM_VP"), "DOZV_DATE_FROM", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_VP", CLocaliz.TxT("DOZV_DATE_TO_VP"), "DOZV_DATE_TO", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_VP", CLocaliz.TxT("TABLE_ID_VP"), "TABLE_ID", AppType.AppVP));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_VP", CLocaliz.TxT("ID_VP"), "ID", AppType.AppVP));

                //TV2d
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_TV2d", CLocaliz.TxT("STATUS_TV2d"), "STATUS", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_TV2d", CLocaliz.TxT("ADDRESS_TV2d"), "ADDRESS", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_TV2d", CLocaliz.TxT("CREATED_BY_TV2d"), "CREATED_BY", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_TV2d", CLocaliz.TxT("DATE_CREATED_TV2d"), "DATE_CREATED", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CHANNEL_TV2d", CLocaliz.TxT("CHANNEL_TV2d"), "CHANNEL", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("PROGRAM_TV2d", CLocaliz.TxT("PROGRAM_TV2d"), "PROGRAM", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_TV2d", CLocaliz.TxT("DOZV_NUM_TV2d"), "DOZV_NUM", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_TV2d", CLocaliz.TxT("DOZV_DATE_TO_TV2d"), "DOZV_DATE_TO", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_TV2d", CLocaliz.TxT("CONC_NUM_TV2d"), "CONC_NUM", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_TV2d", CLocaliz.TxT("DOZV_DATE_FROM_TV2d"), "DOZV_DATE_FROM", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CITY_TV2d", CLocaliz.TxT("CITY_TV2d"), "CITY", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_TV2d", CLocaliz.TxT("TABLE_ID_TV2d"), "TABLE_ID", AppType.AppTV2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_TV2d", CLocaliz.TxT("ID_TV2d"), "ID", AppType.AppTV2d));

                //R2d
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_R2d", CLocaliz.TxT("STATUS_R2d"), "STATUS", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_R2d", CLocaliz.TxT("ADDRESS_R2d"), "ADDRESS", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("BLOCK_R2d", CLocaliz.TxT("BLOCK_R2d"), "BLOCK", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_R2d", CLocaliz.TxT("CREATED_BY_R2d"), "CREATED_BY", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_R2d", CLocaliz.TxT("DATE_CREATED_R2d"), "DATE_CREATED", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_R2d", CLocaliz.TxT("DOZV_NUM_R2d"), "DOZV_NUM", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_R2d", CLocaliz.TxT("DOZV_DATE_FROM_R2d"), "DOZV_DATE_FROM", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_R2d", CLocaliz.TxT("DOZV_DATE_TO_R2d"), "DOZV_DATE_TO", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_R2d", CLocaliz.TxT("CONC_NUM_R2d"), "CONC_NUM", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_DATE_FROM_R2d", CLocaliz.TxT("CONC_DATE_FROM_R2d"), "CONC_DATE_FROM", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("CITY_R2d", CLocaliz.TxT("CITY_R2d"), "CITY", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("KOATUU_R2d", CLocaliz.TxT("KOATUU_R2d"), "KOATUU", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("TABLE_ID_R2d", CLocaliz.TxT("TABLE_ID_R2d"), "TABLE_ID", AppType.AppR2d));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_R2d", CLocaliz.TxT("ID_R2d"), "ID", AppType.AppR2d));
                

                //ZS
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_ZS", CLocaliz.TxT("ADDRESS_ZS"), "ADDRESS", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_ZS", CLocaliz.TxT("CREATED_BY_ZS"), "CREATED_BY", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_ZS", CLocaliz.TxT("DATE_CREATED_ZS"), "DATE_CREATED", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_ZS", CLocaliz.TxT("STANDARD_ZS"), "STANDARD", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_ZS", CLocaliz.TxT("STATUS_ZS"), "STATUS", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("LICENCES_ZS", CLocaliz.TxT("LICENCES_ZS"), "LICENCES", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("RX_FREQ_TXT_ZS", CLocaliz.TxT("RX_FREQ_TXT_ZS"), "RX_FREQ_TXT", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("TX_FREQ_TXT_ZS", CLocaliz.TxT("TX_FREQ_TXT_ZS"), "TX_FREQ_TXT", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DESIG_EMISSION_ZS", CLocaliz.TxT("DESIG_EMISSION_ZS"), "DESIG_EMISSION", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("EQUIP_ZS", CLocaliz.TxT("EQUIP_ZS"), "EQUIP", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ANTENNA_ZS", CLocaliz.TxT("ANTENNA_ZS"), "ANTENNA", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("KOATUU_ZS", CLocaliz.TxT("KOATUU_ZS"), "KOATUU", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_ZS", CLocaliz.TxT("CONC_NUM_ZS"), "CONC_NUM", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_DATE_FROM_ZS", CLocaliz.TxT("CONC_DATE_FROM_ZS"), "CONC_DATE_FROM", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_ZS", CLocaliz.TxT("DOZV_NUM_ZS"), "DOZV_NUM", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_ZS", CLocaliz.TxT("DOZV_DATE_FROM_ZS"), "DOZV_DATE_FROM", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_NEW_ZS", CLocaliz.TxT("DOZV_NUM_NEW_ZS"), "DOZV_NUM_NEW", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("SAT_NAME_ZS", CLocaliz.TxT("SAT_NAME_ZS"), "SAT_NAME", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("LONG_NOM_ZS", CLocaliz.TxT("LONG_NOM_ZS"), "LONG_NOM", AppType.AppZS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_ZS", CLocaliz.TxT("ID_ZS"), "ID", AppType.AppZS));

                //TR_3
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_TR_3", CLocaliz.TxT("STANDARD_TR_3"), "STANDARD", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("BRANCH_OFFICE_CODE_TR_3", CLocaliz.TxT("BRANCH_OFFICE_CODE_TR_3"), "BRANCH_OFFICE_CODE", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_TR_3", CLocaliz.TxT("ADDRESS_TR_3"), "ADDRESS", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_TR_3", CLocaliz.TxT("CALL_TR_3"), "CALL", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_TR_3", CLocaliz.TxT("STATUS_TR_3"), "STATUS", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_TR_3", CLocaliz.TxT("CREATED_BY_TR_3"), "CREATED_BY", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_TR_3", CLocaliz.TxT("DATE_CREATED_TR_3"), "DATE_CREATED", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_TR_3", CLocaliz.TxT("DOZV_NUM_TR_3"), "DOZV_NUM", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_TR_3", CLocaliz.TxT("DOZV_DATE_TO_TR_3"), "DOZV_DATE_TO", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("REF_ID_TR_3", CLocaliz.TxT("REF_ID_TR_3"), "REF_ID", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_TR_3", CLocaliz.TxT("ID_TR_3"), "ID", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_ID1_TR_3", CLocaliz.TxT("OBJ_ID1_TR_3"), "OBJ_ID1", AppType.AppTR_З));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_TABLE_TR_3", CLocaliz.TxT("OBJ_TABLE_TR_3"), "OBJ_TABLE", AppType.AppTR_З));

                //AppAP3
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_АР_З", CLocaliz.TxT("STANDARD_АР_З"), "STANDARD", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("BRANCH_OFFICE_CODE_АР_З", CLocaliz.TxT("BRANCH_OFFICE_CODE_АР_З"), "BRANCH_OFFICE_CODE", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_АР_З", CLocaliz.TxT("ADDRESS_АР_З"), "ADDRESS", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_АР_З", CLocaliz.TxT("CALL_SIGN_АР_З"), "CALL_SIGN", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_АР_З", CLocaliz.TxT("STATUS_АР_З"), "STATUS", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_АР_З", CLocaliz.TxT("CREATED_BY_АР_З"), "CREATED_BY", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_АР_З", CLocaliz.TxT("DATE_CREATED_АР_З"), "DATE_CREATED", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_АР_З", CLocaliz.TxT("DOZV_NUM_АР_З"), "DOZV_NUM", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_АР_З", CLocaliz.TxT("DOZV_DATE_TO_АР_З"), "DOZV_DATE_TO", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_NEW_АР_З", CLocaliz.TxT("DOZV_NUM_NEW_АР_З"), "DOZV_NUM_NEW", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_NEW_АР_З", CLocaliz.TxT("DOZV_DATE_TO_NEW_АР_З"), "DOZV_DATE_TO_NEW", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("REF_ID_АР_З", CLocaliz.TxT("REF_ID_АР_З"), "REF_ID", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_АР_З", CLocaliz.TxT("ID_АР_З"), "ID", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_ID1_АР_З", CLocaliz.TxT("OBJ_ID1_АР_З"), "OBJ_ID1", AppType.AppAP3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_TABLE_АР_З", CLocaliz.TxT("OBJ_TABLE_АР_З"), "OBJ_TABLE", AppType.AppAP3));
                

                //RS
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_RS", CLocaliz.TxT("ADDRESS_RS"), "ADDRESS", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_RS", CLocaliz.TxT("CREATED_BY_RS"), "CREATED_BY", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_RS", CLocaliz.TxT("DATE_CREATED_RS"), "DATE_CREATED", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_RS", CLocaliz.TxT("STANDARD_RS"), "STANDARD", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_RS", CLocaliz.TxT("STATUS_RS"), "STATUS", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_MK_RS", CLocaliz.TxT("STATUS_MK_RS"), "STATUS_MK", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("LICENCES_RS", CLocaliz.TxT("LICENCES_RS"), "LICENCES", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_RS", CLocaliz.TxT("DOZV_NUM_RS"), "DOZV_NUM", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_RS", CLocaliz.TxT("DOZV_DATE_FROM_RS"), "DOZV_DATE_FROM", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_NEW_RS", CLocaliz.TxT("DOZV_NUM_NEW_RS"), "DOZV_NUM_NEW", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_RS", CLocaliz.TxT("CONC_NUM_RS"), "CONC_NUM", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_DATE_FROM_RS", CLocaliz.TxT("CONC_DATE_FROM_RS"), "CONC_DATE_FROM", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("RX_FREQ_TXT_RS", CLocaliz.TxT("RX_FREQ_TXT_RS"), "RX_FREQ_TXT", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("TX_FREQ_TXT_RS", CLocaliz.TxT("TX_FREQ_TXT_RS"), "TX_FREQ_TXT", AppType.AppRS));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_RS", CLocaliz.TxT("ID_RS"), "ID", AppType.AppRS));

                // R2
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_R2", CLocaliz.TxT("ADDRESS_R2"), "ADDRESS", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("FREQ_R2", CLocaliz.TxT("FREQ_R2"), "FREQ", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CITY_R2", CLocaliz.TxT("CITY_R2"), "CITY", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_R2", CLocaliz.TxT("DOZV_NUM_R2"), "DOZV_NUM", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_FROM_R2", CLocaliz.TxT("DOZV_DATE_FROM_R2"), "DOZV_DATE_FROM", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_R2", CLocaliz.TxT("DOZV_DATE_TO_R2"), "DOZV_DATE_TO", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_R2", CLocaliz.TxT("STATUS_R2"), "STATUS", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_NUM_R2", CLocaliz.TxT("CONC_NUM_R2"), "CONC_NUM", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("CONC_DATE_FROM_R2", CLocaliz.TxT("CONC_DATE_FROM_R2"), "CONC_DATE_FROM", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("KOATUU_R2", CLocaliz.TxT("KOATUU_R2"), "KOATUU", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("REF_ID_R2", CLocaliz.TxT("REF_ID_R2"), "REF_ID", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_R2", CLocaliz.TxT("ID_R2"), "ID", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_ID1_R2", CLocaliz.TxT("OBJ_ID1_R2"), "OBJ_ID1", AppType.AppR2));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_TABLE_R2", CLocaliz.TxT("OBJ_TABLE_R2"), "OBJ_TABLE", AppType.AppR2));

                // AR_3
                ListAllColumns.Add(new ExtendedColumnsProperty("STANDARD_AR_3", CLocaliz.TxT("STANDARD_AR_3"), "STANDARD", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("EXPL_TYPE_AR_3", CLocaliz.TxT("EXPL_TYPE_AR_3"), "EXPL_TYPE", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ADDRESS_AR_3", CLocaliz.TxT("ADDRESS_AR_3"), "ADDRESS", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("LOCATION_AR_3", CLocaliz.TxT("LOCATION_AR_3"), "LOCATION", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_AR_3", CLocaliz.TxT("CALL_AR_3"), "CALL", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_АР_З", CLocaliz.TxT("CALL_SIGN_АР_З"), "CALL_SIGN", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("FACTORY_NUM_AR_3", CLocaliz.TxT("FACTORY_NUM_AR_3"), "FACTORY_NUM", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CALL_SIGN_AR_3", CLocaliz.TxT("CALL_SIGN_AR_3"), "CALL_SIGN", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("STATUS_AR_3", CLocaliz.TxT("STATUS_AR_3"), "STATUS", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("CREATED_BY_AR_3", CLocaliz.TxT("CREATED_BY_AR_3"), "CREATED_BY", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DATE_CREATED_AR_3", CLocaliz.TxT("DATE_CREATED_AR_3"), "DATE_CREATED", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_NUM_AR_3", CLocaliz.TxT("DOZV_NUM_AR_3"), "DOZV_NUM", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("DOZV_DATE_TO_AR_3", CLocaliz.TxT("DOZV_DATE_TO_AR_3"), "DOZV_DATE_TO", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("REF_ID_AR_3", CLocaliz.TxT("REF_ID_AR_3"), "REF_ID", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("ID_AR_3", CLocaliz.TxT("ID_AR_3"), "ID", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_ID1_AR_3", CLocaliz.TxT("OBJ_ID1_AR_3"), "OBJ_ID1", AppType.AppAR_3));
                ListAllColumns.Add(new ExtendedColumnsProperty("OBJ_TABLE_AR_3", CLocaliz.TxT("OBJ_TABLE_AR_3"), "OBJ_TABLE", AppType.AppAR_3));


                ListAppType = new Dictionary<string, AppType>();
                ListPacketType = new List<EPacketType>();

                if (RoleCreatedUnder == UcrfDepartment.URCP)
                {
                    // Загружаем список заявок
                    ListAppType.Add("ЗС", AppType.AppZS);
                    ListAppType.Add("БС", AppType.AppBS);
                    ListAppType.Add("РC", AppType.AppRS);
                    ListAppType.Add("PP", AppType.AppRR);
                    ListAppType.Add("TP", AppType.AppTR);
                    ListAppType.Add("TP_З", AppType.AppTR_З);
                    ListAppType.Add("A3", AppType.AppA3);
                    ListAppType.Add("P", AppType.AppR2);
                    ListAppType.Add("Pц", AppType.AppR2d);
                    ListAppType.Add("T", AppType.AppTV2);
                    ListAppType.Add("Tц", AppType.AppTV2d);
                    ListAppType.Add("АР", AppType.AppAR_3);
                    ListAppType.Add("АР_3", AppType.AppAP3);
                    ListAppType.Add("ЗРС", AppType.AppZRS);
                    ListAppType.Add("ВП", AppType.AppVP);

                    // Загружаем список пакетов
                    ListPacketType.Add(EPacketType.PckVISN);
                    ListPacketType.Add(EPacketType.PckDOZV);
                    ListPacketType.Add(EPacketType.PctREISSUE);
                    ListPacketType.Add(EPacketType.PctPROLONG);
                    ListPacketType.Add(EPacketType.PctANNUL);
                    ListPacketType.Add(EPacketType.PctLICENCE);
                    ListPacketType.Add(EPacketType.PckSetCallSign);
                    ListPacketType.Add(EPacketType.PckDuplicate);
                }
                else if (RoleCreatedUnder == UcrfDepartment.URZP)
                {
                    ListAppType.Add("ЗС", AppType.AppZS);
                    ListAppType.Add("БС", AppType.AppBS);
                    ListAppType.Add("РC", AppType.AppRS);
                    ListAppType.Add("PP/ТР", AppType.AppRRTR);
                    ListAppType.Add("P", AppType.AppR2R2d);
                    ListAppType.Add("T", AppType.AppTV2TV2d);
                    ListAppType.Add("ЗРС", AppType.AppZRS);
                    ListAppType.Add("AР?", AppType.AppA3);
                    ListAppType.Add("ВП?", AppType.AppVP);

                    // Загружаем список пакетов
                    ListPacketType.Add(EPacketType.PckPTKNVTV);
                    ListPacketType.Add(EPacketType.PckDuplicate);
                    //#5459
                    //ListPacketType.Add(EPacketType.PckDOZV);
                    //ListPacketType.Add(EPacketType.PctNVTVURZP);
                }
                else if (RoleCreatedUnder == UcrfDepartment.Branch)
                {
                    ListAppType.Add("ЗС", AppType.AppZS);
                    ListAppType.Add("БС", AppType.AppBS);
                    ListAppType.Add("РC", AppType.AppRS);
                    ListAppType.Add("PP/ТР", AppType.AppRRTR);
                    ListAppType.Add("P", AppType.AppR2R2d);
                    ListAppType.Add("T", AppType.AppTV2TV2d);
                    ListAppType.Add("TP_З", AppType.AppTR_З);
                    ListAppType.Add("A3", AppType.AppA3);
                    ListAppType.Add("АР", AppType.AppAR_3);
                    ListAppType.Add("АР_3", AppType.AppAP3);
                    ListAppType.Add("ЗРС", AppType.AppZRS);
                    ListAppType.Add("ВП", AppType.AppVP);

                    ListPacketType.Add(EPacketType.PckVISN);
                    ListPacketType.Add(EPacketType.PckPTKNVTV);
                    ListPacketType.Add(EPacketType.PckDOZV);
                    ListPacketType.Add(EPacketType.PctPROLONG);
                    ListPacketType.Add(EPacketType.PctANNUL);
                    ListPacketType.Add(EPacketType.PckDuplicate);
                }
            }
        }
        //===================================================
        public StatePacket PacketState
        {
            get { return packetState; }
            set
            {
                if (packetState == value)
                    return;
                packetState = value;
                IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
                rsPacket.Select("ID,STATUS");
                rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, idPacket);
                try
                {
                    rsPacket.Open();
                    if (!rsPacket.IsEOF())
                    {// Пакет существует
                        rsPacket.Edit();
                        rsPacket.Put("STATUS", packetState.ToString());
                        rsPacket.Update();
                    }
                }
                finally
                {
                    rsPacket.Destroy();
                }
            }
        }
        //===================================================
        /// <summary>
        /// Возавращает список типа пакета
        /// </summary>
        public List<string> PacketTypeList
        {
            get
            {
                List<string> retList = new List<string>();
                foreach (EPacketType elem in ListPacketType)
                    retList.Add(CLocaliz.TxT(elem.ToString()));
                return retList;
            }
        }
        //===================================================
        /// <summary>
        /// Возавращает индекс типа пакета
        /// </summary>
        public int PacketTypeIndex
        {
            get { return ListPacketType.IndexOf(packetType); }
            set
            {
                if ((value < 0) || (value >= ListPacketType.Count))
                    packetType = EPacketType.PckUnknown;
                else packetType = ListPacketType[value];
            }
        }
        //===================================================
        /// <summary>
        /// Сохраняем пакет
        /// </summary>
        public virtual void Save()
        {
            if (idOwner == 0)
            {
                MessageBox.Show(CLocaliz.TxT("Packet owner is not selected."), CLocaliz.TxT("Error"));
                return;
            }
            if (packetType != EPacketType.PctANNUL && applType == AppType.AppUnknown)
                throw new IMException(CLocaliz.TxT("The type of packet is not selected"));

            DateTime curDate;
            string curDateStr = "";
            IM.ExecuteScalar(ref curDateStr, "select to_char(trunc(sysdate)) from dual");
            try { curDate = Convert.ToDateTime(curDateStr); }
            catch { curDate = DateTime.Now.TruncateByDay(); }

            if (DateIn.TruncateByDay() > curDate)
            {
                if (MessageBox.Show("Вхідна дата пізніша за поточну. Продовжити?", "Уточнення", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    throw new IMException("Виправте вхідну дату");
            }

            if (DateOut.TruncateByDay() > DateIn.TruncateByDay())
            {
                if (MessageBox.Show("Вихідна дата пізніша за вхідну. Продовжити?", "Уточнення", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    throw new IMException("Виправте вхідну або вихідну дату");
            }


            YXnrfaPacket rsPacket_ = new YXnrfaPacket();
            rsPacket_.Table = "XNRFA_PACKET";
            rsPacket_.Format("ID,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD,CONTENT,TABLE_NAME,CREATED_MANAGEMENT");
            if (rsPacket_.Fetch(idPacket))
            {
                rsPacket_.m_standard = radioTech;
                rsPacket_.m_modified_by = IM.ConnectedUser();
                rsPacket_.m_modified_date = DateTime.Now;
                rsPacket_.Save();
            }
            else
            {
                rsPacket_.New();
                rsPacket_.m_id = idPacket;
                rsPacket_.m_created_by = IM.ConnectedUser();
                rsPacket_.m_created_date = DateTime.Now;
                rsPacket_.m_owner_id = idOwner;
                rsPacket_.m_type = appTypeConvert.AppTypeToString(applType);
                rsPacket_.m_content = packetType.ToString();
                rsPacket_.m_number_in = NumberIn;
                rsPacket_.m_number_out = NumberOut;
                rsPacket_.m_date_in = DateIn;
                rsPacket_.m_date_out = DateOut;
                rsPacket_.m_standard = radioTech;
                rsPacket_.m_created_management = RoleCreatedUnder.ToString();
                rsPacket_.m_table_name = "XNRFA_PACKET";
                rsPacket_.Save();
            }
            rsPacket_.Close();
            rsPacket_.Dispose();

            isNewRecord = false;
            

            /*
            IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
            rsPacket.Select("ID,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD,CONTENT,TABLE_NAME");
            rsPacket.Select("CREATED_MANAGEMENT");
            rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, idPacket);
            try
            {
                rsPacket.Open();
                if (!rsPacket.IsEOF())
                {// Редактируем данные
                    rsPacket.Edit();
                    rsPacket["STANDARD"] = radioTech;
                    rsPacket["MODIFIED_BY"] = IM.ConnectedUser();
                    rsPacket["MODIFIED_DATE"] = DateTime.Now;
                    rsPacket.Update();
                }
                else
                {// Создаем новую запись
                    rsPacket.AddNew();
                    rsPacket["ID"] = idPacket;
                    rsPacket["CREATED_BY"] = IM.ConnectedUser();
                    rsPacket["CREATED_DATE"] = DateTime.Now;
                    rsPacket["OWNER_ID"] = idOwner;
                    rsPacket["TYPE"] = appTypeConvert.AppTypeToString(applType);
                    rsPacket["CONTENT"] = packetType.ToString();
                    rsPacket["NUMBER_IN"] = NumberIn;
                    rsPacket["NUMBER_OUT"] = NumberOut;
                    rsPacket["DATE_IN"] = DateIn;
                    rsPacket["DATE_OUT"] = DateOut;
                    rsPacket["STANDARD"] = radioTech;
                    rsPacket.Put("CREATED_MANAGEMENT", RoleCreatedUnder.ToString());
                    rsPacket["TABLE_NAME"]= "XNRFA_PACKET";
                    rsPacket.Update();
                }
                isNewRecord = false;
            }
            finally
            {
                rsPacket.Close();
                rsPacket.Destroy();
            }
             */ 

            for (int indexRow = 0; indexRow < _applListRows.Count; indexRow++)
            {
                PacketRowBase rowUrzp = _applListRows[indexRow];
                {
                    //Сохраняем данные заявки в пакете

                    YXnrfaPacToAppl pac_appl = new YXnrfaPacToAppl();
                    pac_appl.Table = "XNRFA_PAC_TO_APPL";
                    pac_appl.Format("PACKET_ID,APPL_ID,PTK_STATUS,DOC_NUM_TV,DOC_DATE,DOC_END_DATE,PTK_DATE_FROM,PTK_DATE_TO,DOC_NUM_URCM,DOC_URCM_DATE,IS_TV,IS_NV,REMARK,CREATED_BY,CREATED_DATE");
                    if (pac_appl.Fetch(string.Format(" (([PACKET_ID] = {0}) AND ([APPL_ID]={1})) ", idPacket, rowUrzp.ApplId)))
                    {
                        pac_appl.m_ptk_status = rowUrzp.PtkStatusCode;
                        pac_appl.m_doc_num_tv = rowUrzp.DocNumTv;
                        pac_appl.m_doc_date = rowUrzp.DocDate;
                        pac_appl.m_doc_end_date = rowUrzp.DocEndDate;
                        pac_appl.m_doc_num_urcm = rowUrzp.DocNumUrcm;
                        pac_appl.m_doc_urcm_date = rowUrzp.DocUrcmDate;
                        pac_appl.m_ptk_date_from = rowUrzp.PtkDateFrom;
                        pac_appl.m_ptk_date_to = rowUrzp.PtkDateTo;
                        pac_appl.m_remark = rowUrzp.Remark;
                        pac_appl.m_is_tv = (rowUrzp.IsTv) ? 1 : 0;
                        pac_appl.m_is_nv = (rowUrzp.IsNv) ? 1 : 0;
                        pac_appl.Save();
                    }
                    pac_appl.Close();
                    pac_appl.Dispose();

                    /*
                    IMRecordset rsPacketToAppl = new IMRecordset(PlugTbl.PacketToAppl, IMRecordset.Mode.ReadWrite);
                    rsPacketToAppl.Select("PACKET_ID,APPL_ID");
                    rsPacketToAppl.Select("PTK_STATUS");
                    rsPacketToAppl.Select("DOC_NUM_TV");
                    rsPacketToAppl.Select("DOC_DATE");
                    rsPacketToAppl.Select("DOC_END_DATE");
                    rsPacketToAppl.Select("PTK_DATE_FROM");
                    rsPacketToAppl.Select("PTK_DATE_TO");
                    rsPacketToAppl.Select("DOC_NUM_URCM");
                    rsPacketToAppl.Select("DOC_URCM_DATE");
                    rsPacketToAppl.Select("IS_TV");
                    rsPacketToAppl.Select("IS_NV");
                    rsPacketToAppl.Select("REMARK");
                    rsPacketToAppl.Select("CREATED_BY");
                    rsPacketToAppl.Select("CREATED_DATE");
                    rsPacketToAppl.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, idPacket);
                    rsPacketToAppl.SetWhere("APPL_ID", IMRecordset.Operation.Eq, rowUrzp.ApplId);
                    rsPacketToAppl.Open();
                    if (!rsPacketToAppl.IsEOF())
                    {
                        rsPacketToAppl.Edit();
                        rsPacketToAppl.Put("PTK_STATUS", rowUrzp.PtkStatusCode);
                        rsPacketToAppl.Put("DOC_NUM_TV", rowUrzp.DocNumTv);
                        rsPacketToAppl.Put("DOC_DATE", rowUrzp.DocDate);
                        rsPacketToAppl.Put("DOC_END_DATE", rowUrzp.DocEndDate);
                        rsPacketToAppl.Put("DOC_NUM_URCM", rowUrzp.DocNumUrcm);
                        rsPacketToAppl.Put("DOC_URCM_DATE", rowUrzp.DocUrcmDate);
                        rsPacketToAppl.Put("PTK_DATE_FROM", rowUrzp.PtkDateFrom);
                        rsPacketToAppl.Put("PTK_DATE_TO", rowUrzp.PtkDateTo);
                        rsPacketToAppl.Put("REMARK", rowUrzp.Remark);
                        rsPacketToAppl.Put("IS_TV", (rowUrzp.IsTv) ? 1 : 0);
                        rsPacketToAppl.Put("IS_NV", (rowUrzp.IsNv) ? 1 : 0);
                        rsPacketToAppl.Update();
                    }
                    if (rsPacketToAppl.IsOpen())
                        rsPacketToAppl.Close();
                    rsPacketToAppl.Destroy();
                     * */
                }
                      
                {
                    //Сохраняем данные в заявки
                    YXnrfaAppl app_p = new YXnrfaAppl();
                    app_p.Table = "XNRFA_APPL";
                    app_p.Format("ID,TRSMTRS_COUNT");
                    if (app_p.Fetch(rowUrzp.ApplId))
                    {
                        app_p.m_trsmtrs_count = rowUrzp.TransmCnt;
                        app_p.Save();
                    }
                    app_p.Close();
                    app_p.Dispose();

                    /*
                    IMRecordset rsAppl = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadWrite);
                    rsAppl.Select("ID");
                    rsAppl.Select("TRSMTRS_COUNT");
                    rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, rowUrzp.ApplId);
                    rsAppl.Open();
                    if (!rsAppl.IsEOF())
                    {
                        rsAppl.Edit();
                        rsAppl.Put("TRSMTRS_COUNT", rowUrzp.TransmCnt);
                        rsAppl.Update();
                    }
                    if (rsAppl.IsOpen())
                        rsAppl.Close();
                    rsAppl.Destroy();
                     */ 
                }
            }
        }
        //===================================================
        /// <summary>
        /// Выбираем контрагента
        /// </summary>
        /// <param name="filter">фильтер</param>
        /// <returns>выбрали или нет</returns>
        public bool SelectOwner(string filter)
        {
            string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(filter) + "*\"}";
            RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Seaching of an owner"), ICSMTbl.itblUsers, param);
            if (user.Id != IM.NullI)
            {
                idOwner = user.Id;
                return true;
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Возвращает имя котрагента
        /// </summary>
        public string OwnerName
        {
            get
            {
                string name = "";
                IMRecordset r = new IMRecordset(ICSMTbl.itblUsers, IMRecordset.Mode.ReadOnly);
                r.Select("ID,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, idOwner);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        name = r.GetS("NAME");
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
                return name;
            }
        }
        //===================================================
        /// <summary>
        /// Возвращает список имён доступных типов заявок
        /// </summary>
        public List<string> GetListAppl()
        {
            List<string> retList = new List<string>();
            foreach (var pair in ListAppType)
            {
                retList.Add(pair.Key);
            }
            return retList;
        }
        //===================================================
        /// <summary>
        /// Установить тип заявки
        /// </summary>
        public void SetApplType(string appTypeString)
        {
            applType = AppType.AppUnknown;
            if (ListAppType.ContainsKey(appTypeString))
                applType = ListAppType[appTypeString];
        }
        //===================================================
        /// <summary>
        /// Созадем новую заявку
        /// </summary>
        public void CreateNewApp(IWin32Window ownerWind)
        {
            using (MainAppForm frm = BaseAppClass.CreateAppl(applType, idPacket, idOwner, radioTech))
            {
                 List<LinkDocFiles> ListDocAttachPachet = GetAllLinkDocsFromIDPacket(idPacket);
                 PrintDocs prntDoc = new PrintDocs();
                 foreach (LinkDocFiles item_ in ListDocAttachPachet)
                 {
                  //prntDoc.LoadDocLink(item_.DOC_TYPE, System.IO.Path.GetFileName(item_.PATH), item_.PATH, item_.DATE_CREATED, IM.NullT, new List<int> { MainAppForm.StatNumApplIdOpened });
                     prntDoc.LoadDocLink(item_.DOC_TYPE, item_.DOC_REF , item_.PATH, item_.DATE_CREATED, IM.NullT, new List<int> { MainAppForm.StatNumApplIdOpened }, false);
                 }
                frm.ShowDialog(ownerWind);
                if (ListSelectedColumns != null) {
                    if (ListSelectedColumns.Count > 0)
                        RefreshRows();
                }
            }
            RefreshRows();
        }
        //===================================================
        /// <summary>
        /// Отобразить заявку
        /// </summary>
        public void ShowApp(string tableName, IWin32Window ownerWind)
        {
            DataGridViewRow curRow = ApplGrid.CurrentRow;
            if(curRow == null)
                return;
            if ((curRow.Index >= 0) && (curRow.Index < _applListRows.Count))
            {
                BaseAppClass appl = BaseAppClass.GetAppl(_applListRows[curRow.Index].ApplId, false);
                if (appl != null)
                {
                    if (!string.IsNullOrEmpty(tableName))
                        appl.SetRight(tableName);
                    using (MainAppForm formAppl = new MainAppForm(appl))
                    {
                        formAppl.ShowDialog(ownerWind);
                        if (ListSelectedColumns != null) {
                            if (ListSelectedColumns.Count > 0)
                                RefreshRows();
                        }
                    }
                }
                else
                    MessageBox.Show(CLocaliz.TxT("Can't find the station"), CLocaliz.TxT("Error"));

                appl.Dispose();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="appId"></param>
        public void SpecialCalcAddFields(ref PacketRowBase row, int appId)
        {
            if (ListSelectedColumns != null)
            {
                string S_ = "";
                //List<ExtendedColumnsProperty> ext_col = ListSelectedColumns.FindAll(r => r.TypeRadiosys == applType);
                List<ExtendedColumnsProperty> ext_col = ListAllColumns.FindAll(r => r.TypeRadiosys == applType);
                foreach (ExtendedColumnsProperty item in ext_col)
                    S_ += item.PathToColumn + ",";
                if (S_.Length > 0) S_ = S_.Remove(S_.Length - 1, 1);
                if (applType == AppType.AppA3)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_A3
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_A3", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STANDARD":
                                        row.STANDARD_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TYPE_STAT_A3":
                                        row.TYPE_STAT_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_A3":
                                        row.ADDRESS_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "OWNER_NAME_A3":
                                        row.OWNER_NAME_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_SIGN_A3":
                                        row.CALL_SIGN_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CATEG_OPER_A3":
                                        row.CATEG_OPER_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "APC_A3":
                                        row.APC_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_A3":
                                        row.DOZV_NUM_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_A3":
                                        row.DOZV_DATE_FROM_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_A3":
                                        row.DOZV_DATE_TO_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_PRINT_A3":
                                        row.DOZV_DATE_PRINT_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_CANCEL_A3":
                                        row.DOZV_DATE_CANCEL_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_CREATED_BY_A3":
                                        row.DOZV_CREATED_BY_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_FILE_A3":
                                        row.DOZV_FILE_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_A3":
                                        row.STATUS_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_A3":
                                        row.CREATED_BY_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "REF_ID_A3":
                                        row.REF_ID_A3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_A3":
                                        row.ID_A3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_ID1_A3":
                                        row.OBJ_ID1_A3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_TABLE_A3":
                                        row.OBJ_TABLE_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "EMPLOYEE_ID_A3":
                                        row.EMPLOYEE_ID_A3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_A3":
                                        row.DATE_CREATED_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_NEW_A3":
                                        row.DOZV_NUM_NEW_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_NEW_A3":
                                        row.DOZV_DATE_FROM_NEW_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_NEW_A3":
                                        row.DOZV_DATE_TO_NEW_A3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_CREATED_BY_NEW_A3":
                                        row.DOZV_CREATED_BY_NEW_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_FILE_NEW_A3":
                                        row.DOZV_FILE_NEW_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "PROVINCE_A3":
                                        row.PROVINCE_A3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppTV2)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_TV
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_TV", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_TV2":
                                        row.STATUS_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_TV2":
                                        row.ADDRESS_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CHANNEL_TV2":
                                        row.CHANNEL_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_TV2":
                                        row.DOZV_NUM_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_TV2":
                                        row.DOZV_DATE_TO_TV2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "EQUIP_NAME_TV2":
                                        row.EQUIP_NAME_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_TV2":
                                        row.CONC_NUM_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_TV2":
                                        row.DOZV_DATE_FROM_TV2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_TV2":
                                        row.CREATED_BY_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_TV2":
                                        row.DATE_CREATED_TV2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CITY_TV2":
                                        row.CITY_TV2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_TV2":
                                        row.TABLE_ID_TV2 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_TV2":
                                        row.ID_TV2 = rs_xv.GetI(item.PathToColumn);
                                        break;

                                }
                            }
                        }

                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppBS)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_BS
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_BS", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "ADDRESS_BS":
                                        row.ADDRESS_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_BS":
                                        row.CREATED_BY_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_BS":
                                        row.DATE_CREATED_BS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STANDARD_BS":
                                        row.STANDARD_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_BS":
                                        row.STATUS_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "LICENCE_NUM_BS":
                                        row.LICENCE_NUM_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "RX_FREQ_TXT_BS":
                                        row.RX_FREQ_TXT_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TX_FREQ_TXT_BS":
                                        row.TX_FREQ_TXT_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DESIG_EM_BS":
                                        row.DESIG_EM_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "EQUIP_NAME_BS":
                                        row.EQUIP_NAME_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ANT_NAME_BS":
                                        row.ANT_NAME_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "KOATUU_BS":
                                        row.KOATUU_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_BS":
                                        row.CONC_NUM_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_DATE_FROM_BS":
                                        row.CONC_DATE_FROM_BS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_BS":
                                        row.DOZV_NUM_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_BS":
                                        row.DOZV_DATE_FROM_BS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_NEW_BS":
                                        row.DOZV_NUM_NEW_BS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }

                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppRR)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_RR
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_RR", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_RR":
                                        row.STATUS_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_RR":
                                        row.ADDRESS_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CID_RR":
                                        row.CID_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_RR":
                                        row.DOZV_NUM_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_RR":
                                        row.DOZV_DATE_TO_RR = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "EQUIP_NAME_RR":
                                        row.EQUIP_NAME_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_RR":
                                        row.CONC_NUM_RR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_RR":
                                        row.DOZV_DATE_FROM_RR = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_RR":
                                        row.TABLE_ID_RR = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_RR":
                                        row.ID_RR = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppTR)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_TR
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_TR", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_TR":
                                        row.STATUS_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_TR":
                                        row.ADDRESS_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STANDARD_TR":
                                        row.STANDARD_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_SIGN_TR":
                                        row.CALL_SIGN_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_TR":
                                        row.DOZV_NUM_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_TR":
                                        row.DOZV_DATE_TO_TR = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "EQUIP_NAME_TR":
                                        row.EQUIP_NAME_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CLASS_TR":
                                        row.CLASS_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_TR":
                                        row.CONC_NUM_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "RX_FREQ_TXT_TR":
                                        row.RX_FREQ_TXT_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TX_FREQ_TXT_TR":
                                        row.TX_FREQ_TXT_TR = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DIST_BORDER_TR":
                                        row.DIST_BORDER_TR = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_TR":
                                        row.DOZV_DATE_FROM_TR = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_TR":
                                        row.TABLE_ID_TR = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_TR":
                                        row.ID_TR = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppZRS)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_ZRS
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_ZRS", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_ZRS":
                                        row.STATUS_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "REG_PORT_ZRS":
                                        row.REG_PORT_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_ZRS":
                                        row.CREATED_BY_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_ZRS":
                                        row.DATE_CREATED_ZRS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STANDARD_ZRS":
                                        row.STANDARD_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_ZRS":
                                        row.DOZV_NUM_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_ZRS":
                                        row.DOZV_DATE_FROM_ZRS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_ZRS":
                                        row.DOZV_DATE_TO_ZRS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "SHIP_NAME_ZRS":
                                        row.SHIP_NAME_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "MMSI_NUM_ZRS":
                                        row.MMSI_NUM_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ATIS_ZRS":
                                        row.ATIS_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_SIGN_ZRS":
                                        row.CALL_SIGN_ZRS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_ZRS":
                                        row.TABLE_ID_ZRS = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_ZRS":
                                        row.ID_ZRS = rs_xv.GetI(item.PathToColumn);
                                        break;
                                   
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppVP)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_VP
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_VP", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_VP":
                                        row.STATUS_VP = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_VP":
                                        row.ADDRESS_VP = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_VP":
                                        row.CREATED_BY_VP = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_VP":
                                        row.DATE_CREATED_VP = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STANDARD_VP":
                                        row.STANDARD_VP = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_VP":
                                        row.DOZV_NUM_VP = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_VP":
                                        row.DOZV_DATE_FROM_VP = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_VP":
                                        row.DOZV_DATE_TO_VP = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_VP":
                                        row.TABLE_ID_VP = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_VP":
                                        row.ID_VP = rs_xv.GetI(item.PathToColumn);
                                        break;

                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppTV2d)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_TV2D
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_TV2D", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_TV2d":
                                        row.STATUS_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_TV2d":
                                        row.ADDRESS_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_TV2d":
                                        row.CREATED_BY_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_TV2d":
                                        row.DATE_CREATED_TV2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CHANNEL_TV2d":
                                        row.CHANNEL_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "PROGRAM_TV2d":
                                        row.PROGRAM_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_TV2d":
                                        row.DOZV_NUM_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_TV2d":
                                        row.DOZV_DATE_TO_TV2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_TV2d":
                                        row.CONC_NUM_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_TV2d":
                                        row.DOZV_DATE_FROM_TV2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CITY_TV2d":
                                        row.CITY_TV2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_TV2d":
                                        row.TABLE_ID_TV2d = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_TV2d":
                                        row.ID_TV2d = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppR2d)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_TV2D
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_R2D", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STATUS_R2d":
                                        row.STATUS_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_R2d":
                                        row.ADDRESS_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "BLOCK_R2d":
                                        row.BLOCK_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_R2d":
                                        row.CREATED_BY_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_R2d":
                                        row.DATE_CREATED_R2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_R2d":
                                        row.DOZV_NUM_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_R2d":
                                        row.DOZV_DATE_FROM_R2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_R2d":
                                        row.DOZV_DATE_TO_R2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_R2d":
                                        row.CONC_NUM_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_DATE_FROM_R2d":
                                        row.CONC_DATE_FROM_R2d = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "CITY_R2d":
                                        row.CITY_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "KOATUU_R2d":
                                        row.KOATUU_R2d = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TABLE_ID_R2d":
                                        row.TABLE_ID_R2d = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_R2d":
                                        row.ID_R2d = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppZS)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_ZS
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_ZS", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "ADDRESS_ZS":
                                        row.ADDRESS_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_ZS":
                                        row.CREATED_BY_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_ZS":
                                        row.DATE_CREATED_ZS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STANDARD_ZS":
                                        row.STANDARD_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_ZS":
                                        row.STATUS_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "LICENCES_ZS":
                                        row.LICENCES_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "RX_FREQ_TXT_ZS":
                                        row.RX_FREQ_TXT_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TX_FREQ_TXT_ZS":
                                        row.TX_FREQ_TXT_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DESIG_EMISSION_ZS":
                                        row.DESIG_EMISSION_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "EQUIP_ZS":
                                        row.EQUIP_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ANTENNA_ZS":
                                        row.ANTENNA_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "KOATUU_ZS":
                                        row.KOATUU_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_ZS":
                                        row.CONC_NUM_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_DATE_FROM_ZS":
                                        row.CONC_DATE_FROM_ZS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_ZS":
                                        row.DOZV_NUM_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_ZS":
                                        row.DOZV_DATE_FROM_ZS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_NEW_ZS":
                                        row.DOZV_NUM_NEW_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "SAT_NAME_ZS":
                                        row.SAT_NAME_ZS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "LONG_NOM_ZS":
                                        row.LONG_NOM_ZS = rs_xv.GetD(item.PathToColumn);
                                        break;
                                    case "ID_ZS":
                                        row.ID_ZS = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppTR_З)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_TR_3
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_TR_3", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STANDARD_TR_3":
                                        row.STANDARD_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "BRANCH_OFFICE_CODE_TR_3":
                                        row.BRANCH_OFFICE_CODE_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_TR_3":
                                        row.ADDRESS_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_TR_3":
                                        row.CALL_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_TR_3":
                                        row.STATUS_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_TR_3":
                                        row.CREATED_BY_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_TR_3":
                                        row.DATE_CREATED_TR_3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_TR_3":
                                        row.DOZV_NUM_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_TR_3":
                                        row.DOZV_DATE_TO_TR_3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "REF_ID_TR_3":
                                        row.REF_ID_TR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_TR_3":
                                        row.ID_TR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_ID1_TR_3":
                                        row.OBJ_ID1_TR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_TABLE_TR_3":
                                        row.OBJ_TABLE_TR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppAP3)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_AP3
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_AP3", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STANDARD_АР_З":
                                        row.STANDARD_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "BRANCH_OFFICE_CODE_АР_З":
                                        row.BRANCH_OFFICE_CODE_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_АР_З":
                                        row.ADDRESS_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_SIGN_АР_З":
                                        row.CALL_SIGN_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_АР_З":
                                        row.STATUS_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_АР_З":
                                        row.CREATED_BY_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_АР_З":
                                        row.DATE_CREATED_АР_З = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_АР_З":
                                        row.DOZV_NUM_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_АР_З":
                                        row.DOZV_DATE_TO_АР_З = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_NEW_АР_З":
                                        row.DOZV_NUM_NEW_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_NEW_АР_З":
                                        row.DOZV_DATE_TO_NEW_АР_З = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "REF_ID_АР_З":
                                        row.REF_ID_АР_З = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_АР_З":
                                        row.ID_АР_З = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_ID1_АР_З":
                                        row.OBJ_ID1_АР_З = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_TABLE_АР_З":
                                        row.OBJ_TABLE_АР_З = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppRS)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_RS
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_RS", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "ADDRESS_RS":
                                        row.ADDRESS_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_RS":
                                        row.CREATED_BY_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_RS":
                                        row.DATE_CREATED_RS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STANDARD_RS":
                                        row.STANDARD_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_RS":
                                        row.STATUS_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_MK_RS":
                                        row.STATUS_MK_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "LICENCES_RS":
                                        row.LICENCES_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_RS":
                                        row.DOZV_NUM_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_RS":
                                        row.DOZV_DATE_FROM_RS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_NEW_RS":
                                        row.DOZV_NUM_NEW_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_RS":
                                        row.CONC_NUM_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_DATE_FROM_RS":
                                        row.CONC_DATE_FROM_RS = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "RX_FREQ_TXT_RS":
                                        row.RX_FREQ_TXT_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "TX_FREQ_TXT_RS":
                                        row.TX_FREQ_TXT_RS = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ID_RS":
                                        row.ID_RS = rs_xv.GetI(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppR2)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_RS
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_R2", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "ADDRESS_R2":
                                        row.ADDRESS_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "FREQ_R2":
                                        row.FREQ_R2 = rs_xv.GetD(item.PathToColumn);
                                        break;
                                    case "CITY_R2":
                                        row.CITY_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_R2":
                                        row.DOZV_NUM_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_FROM_R2":
                                        row.DOZV_DATE_FROM_R2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_R2":
                                        row.DOZV_DATE_TO_R2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "STATUS_R2":
                                        row.STATUS_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_NUM_R2":
                                        row.CONC_NUM_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CONC_DATE_FROM_R2":
                                        row.CONC_DATE_FROM_R2 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "KOATUU_R2":
                                        row.KOATUU_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "REF_ID_R2":
                                        row.REF_ID_R2 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_R2":
                                        row.ID_R2 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_ID1_R2":
                                        row.OBJ_ID1_R2 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_TABLE_R2":
                                        row.OBJ_TABLE_R2 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
                else if (applType == AppType.AppAR_3)
                {
                    if (!string.IsNullOrEmpty(S_))
                    {
                        //XV_APPL_AR_3
                        IMRecordset rs_xv = new IMRecordset("XV_APPL_AR_3", IMRecordset.Mode.ReadOnly);
                        rs_xv.Select("ID");
                        rs_xv.Select(S_);
                        rs_xv.SetWhere("ID", IMRecordset.Operation.Eq, appId);
                        rs_xv.Open();
                        if (!rs_xv.IsEOF())
                        {
                            foreach (ExtendedColumnsProperty item in ext_col)
                            {
                                switch (item.NameProperty)
                                {
                                    case "STANDARD_AR_3":
                                        row.STANDARD_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "EXPL_TYPE_AR_3":
                                        row.EXPL_TYPE_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "ADDRESS_AR_3":
                                        row.ADDRESS_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "LOCATION_AR_3":
                                        row.LOCATION_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_AR_3":
                                        row.CALL_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "FACTORY_NUM_AR_3":
                                        row.FACTORY_NUM_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CALL_SIGN_AR_3":
                                        row.CALL_SIGN_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "STATUS_AR_3":
                                        row.STATUS_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "CREATED_BY_AR_3":
                                        row.CREATED_BY_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DATE_CREATED_AR_3":
                                        row.DATE_CREATED_AR_3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "DOZV_NUM_AR_3":
                                        row.DOZV_NUM_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                    case "DOZV_DATE_TO_AR_3":
                                        row.DOZV_DATE_TO_AR_3 = rs_xv.GetT(item.PathToColumn);
                                        break;
                                    case "REF_ID_AR_3":
                                        row.REF_ID_AR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "ID_AR_3":
                                        row.ID_AR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_ID1_AR_3":
                                        row.OBJ_ID1_AR_3 = rs_xv.GetI(item.PathToColumn);
                                        break;
                                    case "OBJ_TABLE_AR_3":
                                        row.OBJ_TABLE_AR_3 = rs_xv.GetS(item.PathToColumn);
                                        break;
                                }
                            }
                        }
                        rs_xv.Close();
                        rs_xv.Destroy();
                    }
                }
            }
        }


        public void RefreshRowsSpecial(AppType tp)
        {
            IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
            rs.Select("Application.ID,Application.EMPLOYEE_ID,CREATED_DATE,Application.STANDARD");
            rs.Select("Application.OBJ_TABLE");
            rs.Select("Application.EarthStation.STATUS");
            rs.Select("Application.Microwave.STATUS");
            rs.Select("Application.TvStation.STATUS");
            rs.Select("Application.FmStation.STATUS");
            rs.Select("Application.DvbtStation.STATUS");
            rs.Select("Application.TdabStation.STATUS");
            rs.Select("Application.BaseSector1.STATUS");
            rs.Select("Application.MobileSector1.STATUS");
            rs.Select("Application.GSM.STATUS");
            rs.Select("Application.Abonent.STATUS");
            rs.Select("Application.Ship.STATUS");
            rs.Select("Application.AmateurStation.STATUS");
            rs.Select("Application.Railway1.STATUS");
            rs.Select("Application.STATUS_COORD");
            rs.Select("Application.TRSMTRS_COUNT");
            rs.Select("Application.CONC_NUM");
            rs.Select("PTK_STATUS");
            rs.Select("DOC_NUM_TV");
            rs.Select("DOC_DATE");
            rs.Select("DOC_END_DATE");
            rs.Select("PTK_DATE_FROM");
            rs.Select("PTK_DATE_TO");
            rs.Select("DOC_NUM_URCM");
            rs.Select("DOC_URCM_DATE");
            rs.Select("IS_TV");
            rs.Select("IS_NV");
            rs.Select("REMARK");
            rs.Select("CREATED_BY");
            rs.Select("CREATED_DATE");
            rs.Select("Packet.STATUS_COORD");
            rs.Select("Application.Abonent.CALL_SIGN");
            if (tp == AppType.AppRR)
                rs.Select("Application.MobileSector1.NAME");
            rs.SetWhere("Packet.ID", IMRecordset.Operation.Eq, idPacket);
            rs.OrderBy("CREATED_DATE", OrderDirection.Ascending);
            try
            {
                bool isFirstRecord = true;
                List<int> lstApplId = new List<int>();
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    if (isFirstRecord)
                    {
                        isFirstRecord = false;
                        ForeighnStatus = rs.GetS("Packet.STATUS_COORD");
                    }
                    PacketRowBase row = null;
                    int appId = rs.GetI("Application.ID");
                    lstApplId.Add(appId);
                    //Ищем в списке строк эту заявку
                    foreach (PacketRowBase rowUrcp in _applListRows)
                    {
                        if (rowUrcp.ApplId == appId)
                        {
                            row = rowUrcp; //Заявка уже добавлена
                            break;
                        }
                    }
                    int cnt = rs.GetI("Application.TRSMTRS_COUNT");
                    if ((cnt == IM.NullI) && (row == null))
                    {
                        //Подсчет кол-ва передатчиков
                        BaseAppClass app = BaseAppClass.GetBaseAppl(rs.GetI("Application.ID"));
                        cnt = app != null ? app.GetTxCount() : 1;
                        app.Dispose();
                    }
                    if (row == null)
                    {
                        row = new PacketRowBase();
                        _applListRows.Add(row);
                        row.IsSelected = false;
                        row.ApplId = rs.GetI("Application.ID");
                        row.ForeighnStatus = rs.GetS("Application.STATUS_COORD");
                        row.Fio = HelpFunction.getUserFIOByID(rs.GetI("Application.EMPLOYEE_ID"));
                        row.CreatedDate = rs.GetT("CREATED_DATE");
                        row.Standard = rs.GetS("Application.STANDARD");
                        row.PtkStatusCode = rs.GetS("PTK_STATUS");
                        row.TransmCnt = cnt;
                        row.DocNumTv = rs.GetS("DOC_NUM_TV");
                        row.DocDate = rs.GetT("DOC_DATE");
                        row.DocEndDate = rs.GetT("DOC_END_DATE");
                        row.DocNumUrcm = rs.GetS("DOC_NUM_URCM");
                        row.DocUrcmDate = rs.GetT("DOC_URCM_DATE");
                        row.PtkDateFrom = rs.GetT("PTK_DATE_FROM");
                        row.PtkDateTo = rs.GetT("PTK_DATE_TO");
                        row.Remark = rs.GetS("REMARK");
                        row.IsTv = ((rs.GetI("IS_TV") == 0) || (rs.GetI("IS_TV") == IM.NullI)) ? false : true;
                        row.IsNv = ((rs.GetI("IS_NV") == 0) || (rs.GetI("IS_NV") == IM.NullI)) ? false : true;
                        if (packetType == EPacketType.PctANNUL) //Для ускорения загрузки
                            row.DocNumPerm = ApplDocuments.GetPermNum(row.ApplId);
                    }
                    else
                    {
                        if (packetType == EPacketType.PctANNUL) //Для ускорения загрузки
                            row.DocNumPerm = ApplDocuments.GetPermNum(row.ApplId);
                    }
                    Dictionary<int, PositionState2> positions = BaseAppClass.GetPositions(PlugTbl.itblXnrfaAppl, row.ApplId);
                    string fullAddr = "";
                    bool posDiffersFromAdmSite = false;
                    foreach (KeyValuePair<int, PositionState2> position in positions)
                    {
                        string addr = "";
                        PositionState2 pos = position.Value;
                        if (position.Value != null)
                        {
                            addr = pos.FullAddressAuto;
                            pos.GetAdminSiteInfo(pos.AdminSiteId);
                            posDiffersFromAdmSite |= (pos.LatDiffersFromAdm || pos.LonDiffersFromAdm || pos.AddrDiffersFromAdm);
                        }
                        else
                            addr = PositionState2.NoPosition;
                        fullAddr += (fullAddr.Length > 0 ? " / " : "") + addr;
                    }
                    positions = null;
                    row.FullPosition = fullAddr;
                    row.PosDiffersFromAdmSite = posDiffersFromAdmSite;
                    row.DocNumConclusion = rs.GetS("Application.CONC_NUM");
                    string status;
                    switch (rs.GetS("Application.OBJ_TABLE"))
                    {
                        case "EARTH_STATION": status = rs.GetS("Application.EarthStation.STATUS"); break;
                        case "MICROWA": status = rs.GetS("Application.Microwave.STATUS"); break;
                        case "TV_STATION": status = rs.GetS("Application.TvStation.STATUS"); break;
                        case "FM_STATION": status = rs.GetS("Application.FmStation.STATUS"); break;
                        case "DVBT_STATION": status = rs.GetS("Application.DvbtStation.STATUS"); break;
                        case "TDAB_STATION": status = rs.GetS("Application.TdabStation.STATUS"); break;
                        case "MOB_STATION2": status = rs.GetS("Application.BaseSector1.STATUS"); break;
                        case "MOB_STATION": status = rs.GetS("Application.MobileSector1.STATUS"); break;
                        case "GSM": status = rs.GetS("Application.GSM.STATUS"); break;
                        case PlugTbl.XfaAbonentStation: status = rs.GetS("Application.Abonent.STATUS"); break;
                        case ICSMTbl.itblShip: status = rs.GetS("Application.Ship.STATUS"); break;
                        case PlugTbl.XfaAmateur: status = rs.GetS("Application.AmateurStation.STATUS"); break;
                        default: status = ""; break;
                    }
                    row.Status = status;
                    if (tp == AppType.AppRR)
                        row.Sid = rs.GetS("Application.MobileSector1.NAME");
                    row.CallSign = rs.GetS("Application.Abonent.CALL_SIGN");

                    //////////////////
                    // Доработка от 04.12.2016
                    //////////////////
                    SpecialCalcAddFields(ref row, appId);
                }
                // Удаляем ненужные строки
                int i = 0;
                while (i < _applListRows.Count)
                {
                    bool isDelete = true;
                    PacketRowBase rowUrсp = _applListRows[i];
                    foreach (int applId in lstApplId)
                    {
                        if (applId == rowUrсp.ApplId)
                        {
                            isDelete = false;
                            break;
                        }
                    }
                    if (isDelete)
                        _applListRows.RemoveAt(i);
                    else
                        i++;
                }
            }
            finally
            {
                rs.Destroy();
            }
            int cntTrnsm = 0;
            for (int i = 0; i < _applListRows.Count; i++)
            {
                _applListRows[i].NumRow = i + 1;
                cntTrnsm += _applListRows[i].TransmCnt;
            }
            TotalTrsmCnt = cntTrnsm;
            CountAppl = 0; //Обновим кол-во заявок
        }

        public void RefreshRowsFromIds(int ID_Station, string Table)
        {
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,TRSMTRS_COUNT,CREATE_DATE");
            rs.SetAdditional(string.Format(" (ID={0}) ", ID_Station));
            rs.OrderBy("CREATE_DATE", OrderDirection.Ascending);
            try
            {
                bool isFirstRecord = true;
                List<int> lstApplId = new List<int>();
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    if (isFirstRecord)
                    {
                        isFirstRecord = false;
                    }
                    PacketRowBase row = null;
                    int appId = rs.GetI("ID");
                    lstApplId.Add(appId);
                    //Ищем в списке строк эту заявку
                    foreach (PacketRowBase rowUrcp in _applListRows)
                    {
                        if (rowUrcp.ApplId == appId)
                        {
                            row = rowUrcp; //Заявка уже добавлена
                            break;
                        }
                    }
                    int cnt = rs.GetI("TRSMTRS_COUNT");
                    if ((cnt == IM.NullI) && (row == null))
                    {
                        //Подсчет кол-ва передатчиков
                        BaseAppClass app = BaseAppClass.GetBaseAppl(rs.GetI("ID"));
                        cnt = app != null ? app.GetTxCount() : 1;
                        app.Dispose();
                    }
                    if (row == null)
                    {
                        row = new PacketRowBase();
                        _applListRows.Add(row);
                        row.IsSelected = false;
                        row.ApplId = rs.GetI("ID");
                    }
                    //////////////////
                    // Доработка от 04.12.2016
                    //////////////////
                    SpecialCalcAddFields(ref row, appId);
                }
                // Удаляем ненужные строки
                int i = 0;
                while (i < _applListRows.Count)
                {
                    bool isDelete = true;
                    PacketRowBase rowUrсp = _applListRows[i];
                    foreach (int applId in lstApplId)
                    {
                        if (applId == rowUrсp.ApplId)
                        {
                            isDelete = false;
                            break;
                        }
                    }
                    if (isDelete)
                        _applListRows.RemoveAt(i);
                    else
                        i++;
                }
            }
            finally
            {
                rs.Destroy();
            }
            int cntTrnsm = 0;
            for (int i = 0; i < _applListRows.Count; i++)
            {
                _applListRows[i].NumRow = i + 1;
                cntTrnsm += _applListRows[i].TransmCnt;
            }
            TotalTrsmCnt = cntTrnsm;
            CountAppl = 0; //Обновим кол-во заявок
        }

        //===================================================
        public virtual void RefreshRows()
        {
            IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
            rs.Select("Application.ID,Application.EMPLOYEE_ID,CREATED_DATE,Application.STANDARD");
            rs.Select("Application.OBJ_TABLE");
            rs.Select("Application.EarthStation.STATUS");
            rs.Select("Application.Microwave.STATUS");
            rs.Select("Application.TvStation.STATUS");
            rs.Select("Application.FmStation.STATUS");
            rs.Select("Application.DvbtStation.STATUS");
            rs.Select("Application.TdabStation.STATUS");
            rs.Select("Application.BaseSector1.STATUS");
            rs.Select("Application.MobileSector1.STATUS");
            rs.Select("Application.GSM.STATUS");
            rs.Select("Application.Abonent.STATUS");
            rs.Select("Application.Ship.STATUS");
            rs.Select("Application.AmateurStation.STATUS");
            rs.Select("Application.Railway1.STATUS");
            rs.Select("Application.STATUS_COORD");
            rs.Select("Application.TRSMTRS_COUNT");
            rs.Select("Application.CONC_NUM");
            rs.Select("PTK_STATUS");
            rs.Select("DOC_NUM_TV");
            rs.Select("DOC_DATE");
            rs.Select("DOC_END_DATE");
            rs.Select("PTK_DATE_FROM");
            rs.Select("PTK_DATE_TO");
            rs.Select("DOC_NUM_URCM");
            rs.Select("DOC_URCM_DATE");
            rs.Select("IS_TV");
            rs.Select("IS_NV");
            rs.Select("REMARK");
            rs.Select("CREATED_BY");
            rs.Select("CREATED_DATE");
            rs.Select("Packet.STATUS_COORD");
            rs.Select("Application.Abonent.CALL_SIGN");            
            if (ApplType == AppType.AppRR)
                rs.Select("Application.MobileSector1.NAME");
            rs.SetWhere("Packet.ID", IMRecordset.Operation.Eq, idPacket);
            rs.OrderBy("CREATED_DATE", OrderDirection.Ascending);
            try
            {
                bool isFirstRecord = true;
                List<int> lstApplId = new List<int>();
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    if(isFirstRecord)
                    {
                        isFirstRecord = false;
                        ForeighnStatus = rs.GetS("Packet.STATUS_COORD");
                    }
                    PacketRowBase row = null;
                    int appId = rs.GetI("Application.ID");
                    lstApplId.Add(appId);
                    //Ищем в списке строк эту заявку
                    foreach (PacketRowBase rowUrcp in _applListRows)
                    {
                        if (rowUrcp.ApplId == appId)
                        {
                            row = rowUrcp; //Заявка уже добавлена
                            break;
                        }
                    }
                    int cnt = rs.GetI("Application.TRSMTRS_COUNT");
                    if ((cnt == IM.NullI) && (row == null))
                    {
                        //Подсчет кол-ва передатчиков
                        BaseAppClass app = BaseAppClass.GetBaseAppl(rs.GetI("Application.ID"));
                        cnt = app != null ? app.GetTxCount() : 1;
                        app.Dispose();
                    }
                    if (row == null)
                    {
                        row = new PacketRowBase();
                        _applListRows.Add(row);
                        row.IsSelected = false;
                        row.ApplId = rs.GetI("Application.ID");
                        row.ForeighnStatus = rs.GetS("Application.STATUS_COORD");
                        row.Fio = HelpFunction.getUserFIOByID(rs.GetI("Application.EMPLOYEE_ID"));
                        row.CreatedDate = rs.GetT("CREATED_DATE");
                        row.Standard = rs.GetS("Application.STANDARD");
                        row.PtkStatusCode = rs.GetS("PTK_STATUS");
                        row.TransmCnt = cnt;
                        row.DocNumTv = rs.GetS("DOC_NUM_TV");
                        row.DocDate = rs.GetT("DOC_DATE");
                        row.DocEndDate = rs.GetT("DOC_END_DATE");
                        row.DocNumUrcm = rs.GetS("DOC_NUM_URCM");
                        row.DocUrcmDate = rs.GetT("DOC_URCM_DATE");
                        row.PtkDateFrom = rs.GetT("PTK_DATE_FROM");
                        row.PtkDateTo = rs.GetT("PTK_DATE_TO");
                        row.Remark = rs.GetS("REMARK");
                        row.IsTv = ((rs.GetI("IS_TV") == 0) || (rs.GetI("IS_TV") == IM.NullI)) ? false : true;
                        row.IsNv = ((rs.GetI("IS_NV") == 0) || (rs.GetI("IS_NV") == IM.NullI)) ? false : true;
                        if (packetType == EPacketType.PctANNUL) //Для ускорения загрузки
                            row.DocNumPerm = ApplDocuments.GetPermNum(row.ApplId);
                    }
                    else
                    {
                        if (packetType == EPacketType.PctANNUL) //Для ускорения загрузки
                            row.DocNumPerm = ApplDocuments.GetPermNum(row.ApplId);
                    }
                    Dictionary<int, PositionState2> positions = BaseAppClass.GetPositions(PlugTbl.itblXnrfaAppl, row.ApplId);
                    string fullAddr = "";
                    bool posDiffersFromAdmSite = false;
                    foreach (KeyValuePair<int, PositionState2> position in positions)
                    {
                        string addr = "";
                        PositionState2 pos = position.Value;
                        if (position.Value != null)
                        {
                            addr = pos.FullAddressAuto;
                            pos.GetAdminSiteInfo(pos.AdminSiteId);
                            posDiffersFromAdmSite |= (pos.LatDiffersFromAdm || pos.LonDiffersFromAdm || pos.AddrDiffersFromAdm);
                        } 
                        else
                            addr = PositionState2.NoPosition;
                        fullAddr += (fullAddr.Length > 0 ? " / " : "") + addr;
                    }
                    positions = null;
                    row.FullPosition = fullAddr;
                    row.PosDiffersFromAdmSite = posDiffersFromAdmSite;
                    row.DocNumConclusion = rs.GetS("Application.CONC_NUM");
                    string status;
                    switch (rs.GetS("Application.OBJ_TABLE"))
                    {
                        case "EARTH_STATION": status = rs.GetS("Application.EarthStation.STATUS"); break;
                        case "MICROWA": status = rs.GetS("Application.Microwave.STATUS"); break;
                        case "TV_STATION": status = rs.GetS("Application.TvStation.STATUS"); break;
                        case "FM_STATION": status = rs.GetS("Application.FmStation.STATUS"); break;
                        case "DVBT_STATION": status = rs.GetS("Application.DvbtStation.STATUS"); break;
                        case "TDAB_STATION": status = rs.GetS("Application.TdabStation.STATUS"); break;
                        case "MOB_STATION2": status = rs.GetS("Application.BaseSector1.STATUS"); break;
                        case "MOB_STATION": status = rs.GetS("Application.MobileSector1.STATUS"); break;
                        case "GSM": status = rs.GetS("Application.GSM.STATUS"); break;
                        case PlugTbl.XfaAbonentStation: status = rs.GetS("Application.Abonent.STATUS"); break;
                        case ICSMTbl.itblShip: status = rs.GetS("Application.Ship.STATUS"); break;
                        case PlugTbl.XfaAmateur: status = rs.GetS("Application.AmateurStation.STATUS"); break;
                        default: status = ""; break;
                    }
                    row.Status = status;
                    if (ApplType == AppType.AppRR)
                        row.Sid = rs.GetS("Application.MobileSector1.NAME");
                    row.CallSign = rs.GetS("Application.Abonent.CALL_SIGN");

                    //////////////////
                    // Доработка от 04.12.2016
                    //////////////////
                    SpecialCalcAddFields(ref row, appId);
                }
                // Удаляем ненужные строки
                int i = 0;
                while (i < _applListRows.Count)
                {
                    bool isDelete = true;
                    PacketRowBase rowUrсp = _applListRows[i];
                    foreach (int applId in lstApplId)
                    {
                        if (applId == rowUrсp.ApplId)
                        {
                            isDelete = false;
                            break;
                        }
                    }
                    if (isDelete)
                        _applListRows.RemoveAt(i);
                    else
                        i++;
                }
            }
            finally
            {
                rs.Destroy();
            }
            int cntTrnsm = 0;
            for (int i = 0; i < _applListRows.Count; i++)
            {
                _applListRows[i].NumRow = i + 1;
                cntTrnsm += _applListRows[i].TransmCnt;
            }
            TotalTrsmCnt = cntTrnsm;
            CountAppl = 0; //Обновим кол-во заявок
        }
        //===================================================
        /// <summary>
        /// Получить все прикрепленные к пакету докуметы
        /// </summary>
        /// <param name="PacketId"></param>
        /// <returns></returns>
        public static List<LinkDocFiles> GetAllLinkDocsFromIDPacket(int PacketId)
        {
            
            List<LinkDocFiles> lstDocLink = new List<LinkDocFiles>();
            /*IMRecordset rsAps = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
            rsAps.Select("ID,MEMO,PATH,DOC_TYPE,CREATED_BY, DATE_CREATED");
            rsAps.SetWhere("MEMO", IMRecordset.Operation.Eq, PacketId);
            try
            {
                

                Documents.Documents Doc = new Documents.Documents();
                rsAps.Open();
                for (; !rsAps.IsEOF(); rsAps.MoveNext())
                {
                    LinkDocFiles LnkDoc_ = new LinkDocFiles();
                    LnkDoc_.ID = rsAps.GetI("ID");
                    if (rsAps.GetS("DOC_TYPE") != "") { LnkDoc_.DOC_TYPE = Doc.GetHumanNameOfDoc(rsAps.GetS("DOC_TYPE")); } else { LnkDoc_.DOC_TYPE = ""; };
                    LnkDoc_.CREATED_BY = rsAps.GetS("CREATED_BY");
                    LnkDoc_.DATE_CREATED = rsAps.GetT("DATE_CREATED");
                    LnkDoc_.PATH = rsAps.GetS("PATH");
                    lstDocLink.Add(LnkDoc_);
                }
            }
            finally
            {
                rsAps.Close();
                rsAps.Dispose();
            }
             */


            lstDocLink.Clear();
            IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
            rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
            rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, PacketId);
            rsDocLink.SetWhere("REC_TAB", IMRecordset.Operation.Eq, "XNRFA_PACKET");
            //rsDocLink.SetAdditional("[REC_TAB]='XNRFA_PACKET'");
            try
            {

                rsDocLink.Open();
                for (; !rsDocLink.IsEOF(); rsDocLink.MoveNext())
                {

                    int DOC_ID = rsDocLink.GetI("DOC_ID");


                    ////////////////
                    IMRecordset rsAps = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                    rsAps.Select("ID,MEMO,DOC_REF, DOC_DATE, PATH,DOC_TYPE,CREATED_BY, EXP_DATE, DATE_CREATED");
                    rsAps.SetWhere("ID", IMRecordset.Operation.Eq, DOC_ID);
                    //rsAps.SetWhere("MEMO", IMRecordset.Operation.Eq, packet.PacketId);



                    try
                    {
                        Documents.Documents Doc = new Documents.Documents();
                        
                        rsAps.Open();
                        for (; !rsAps.IsEOF(); rsAps.MoveNext())
                        {
                            LinkDocFiles LnkDoc_ = new LinkDocFiles();
                            LnkDoc_.ID = rsAps.GetI("ID");
                            if (rsAps.GetS("DOC_TYPE") != "") { LnkDoc_.DOC_TYPE = rsAps.GetS("DOC_TYPE"); } else { LnkDoc_.DOC_TYPE = ""; };
                            LnkDoc_.CREATED_BY = rsAps.GetS("CREATED_BY");
                            LnkDoc_.DATE_CREATED = rsAps.GetT("DATE_CREATED");
                            LnkDoc_.EXP_DATE = rsAps.GetT("EXP_DATE");


                            string networkLocation = PrintDocs.getPath(rsAps.GetS("DOC_TYPE"));
                            if (string.IsNullOrEmpty(networkLocation))
                            {
                                IMRecordset rsNetPath = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
                                rsNetPath.Select("ITEM,WHAT");
                                rsNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-DOC");
                                rsNetPath.Open();
                                if (!rsNetPath.IsEOF())
                                    networkLocation = rsNetPath.GetS("WHAT");
                                rsNetPath.Destroy();

                                if (string.IsNullOrEmpty(networkLocation))
                                    throw new Exception("Файл потрібно записати в мережеве сховище, але папку для збереження вкладень на сервері не задано.\nЗверніться до адміністратора.");

                            }
                            if (networkLocation[networkLocation.Length - 1] != '\\')
                                networkLocation += '\\';
                            string ext = System.IO.Path.GetExtension(rsAps.GetS("PATH"));
                            if (ext.Length > 0 && ext[0] == '.')
                                ext = ext.Substring(1);
                            string fn = System.IO.Path.GetFileNameWithoutExtension(rsAps.GetS("PATH"));
                            string newFileName = networkLocation + fn + "." + ext;



                            LnkDoc_.PATH = newFileName; // rsAps.GetS("PATH");
                            LnkDoc_.DOC_REF = rsAps.GetS("DOC_REF");
                            lstDocLink.Add(LnkDoc_);

                        }


                    }
                    finally
                    {
                        rsAps.Close();
                        rsAps.Dispose();
                    }


                    /////////////////

                }


            }
            finally
            {
                rsDocLink.Close();
                rsDocLink.Destroy();
            }




            return lstDocLink;
        }


        //===================================================
        /// <summary>
        /// Выбераем существующую хаявку
        /// </summary>
        /// <returns>true - выбрали</returns>
        public bool SelectNewAppl()
        {
            bool retVal = false;
            List<int> listId = null;
            FFindStation frmFindAppl = new FFindStation(PacketType, ApplType, radioTech, idOwner, idPacket, (packetType == EPacketType.PckDuplicate ? true : false));
            if (frmFindAppl.ShowDialog() == DialogResult.OK)
                listId = frmFindAppl.GetSelectedAppl();
            if (listId != null)
            {
                List<LinkDocFiles> ListDocAttachPachet = GetAllLinkDocsFromIDPacket(idPacket);
                StationStatus wf = new StationStatus();
                foreach (int applId in listId)
                {
                    if ((applId > 0) && (applId != IM.NullI))
                    {// Добавляем заявку в пакет
                        {
                            IMRecordset rPacket = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadWrite);
                            rPacket.Select("APPL_ID,PACKET_ID,CREATED_DATE");
                            rPacket.SetWhere("APPL_ID", IMRecordset.Operation.Eq, -1);
                            try
                            {
                                rPacket.Open();
                                rPacket.AddNew();
                                rPacket.Put("APPL_ID", applId);
                                rPacket.Put("PACKET_ID", idPacket);
                                rPacket.Put("CREATED_DATE", DateTime.Now);
                                rPacket.Update();
                                BaseAppClass.AddPacketEvent(applId, DateIn, DateOut, NumberIn, NumberOut);
                            }
                            finally
                            {
                                rPacket.Destroy();
                            }
                            using (BaseAppClass appl = BaseAppClass.GetBaseAppl(applId))
                            {
                                appl.ChangeOwner(idOwner);
                                appl.SaveApplType(applId, ApplType.ToString());
                            }
                        }
                        StatusType curStatus = BaseAppClass.ReadStatus(applId);
                        StatusType newStatus = wf.GetNewStatus(curStatus, packetType);
                        if (newStatus != curStatus)
                            BaseAppClass.UpdateStatus(newStatus, applId);


                        PrintDocs prntDoc = new PrintDocs();
                        foreach (LinkDocFiles item_ in ListDocAttachPachet)
                        {
                         //   prntDoc.LoadDocLink(item_.DOC_TYPE, System.IO.Path.GetFileName(item_.PATH),item_.PATH, item_.DATE_CREATED, IM.NullT, new List<int> { applId});
                            prntDoc.LoadDocLink(item_.DOC_TYPE, item_.DOC_REF, item_.PATH, item_.DATE_CREATED, IM.NullT, new List<int> { applId }, false);
                        }

                        retVal = true;
                    }
                }
                if (frmFindAppl.R_docs_dupl.Count > 0) {
                    foreach (KeyValuePair<int, string> x in frmFindAppl.R_docs_dupl) {
                        PrintDocs prn = new PrintDocs();
                        prn.CreateDuplicateDoc(x.Key, x.Value,applType,radioTech);
                    }
                }
            }
            if (retVal == true) {
                if (ListSelectedColumns != null) {
                    if (ListSelectedColumns.Count > 0)
                        RefreshRows();
                }
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Обновляет список стандартов
        /// </summary>
        public void UpdateStandard()
        {
            standList.Clear();
            List<string> list = CRadioTech.getListRadioTech(applType);
            foreach (string item in list)
            {
                string descr = item;
                IMRecordset rec = new IMRecordset("RADIO_SYSTEMS", IMRecordset.Mode.ReadOnly);
                try
                {
                    rec.Select("NAME,ID,DESCRIPTION");
                    rec.SetWhere("NAME", IMRecordset.Operation.Eq, item);
                    rec.Open();
                    if (!rec.IsEOF())
                    {
                        descr += " - " + rec.GetS("DESCRIPTION");
                    }
                }
                finally
                {
                    rec.Destroy();
                }
                standList.Add(new ComboBoxDictionary<string, string>(item, descr));
            }
            if (standList.Count > 0)
                radioTech = standList[0].Key;
            else
                radioTech = CRadioTech.UNKNOWN;
        }
        //===================================================
        /// <summary>
        /// Пакетная печать
        /// </summary>
        public void PrintPacket()
        {
            List<int> listApplId = new List<int>(GelListIdSelectedAppl());
            if (listApplId.Count != -1)
            {
                FormSelectOutDoc frmReport = new FormSelectOutDoc(new RecordPtr("", 0), true, 0, ApplType, true, listApplId.ToArray());
                
                if(ApplType == AppType.AppAR_3)
                    frmReport.IsEnableDateEnd = false;

                if (frmReport.ShowDialog() == DialogResult.OK)
                {
                    DateTime startDate = frmReport.getStartDate();
                    DateTime endDate = frmReport.getEndDate();
                    string docType = frmReport.getDocType();
                    if (docType == DocType.DOZV_CANCEL)
                        endDate = IM.NullT;
                    int index = 0;
                    List<string> fileList = new List<string>();
                    //if ((docType == DocType.VISN) || (docType == DocType.DOZV) || (docType == DocType.DOZV_FOREIGHT) || (docType == DocType.DOZV_TRAIN) || (docType == DocType.LYST_TR_CALLSIGN) || (docType == DocType.LYST_ZRS_CALLSIGN) || (docType == DocType.DOZV_AR3_SPEC) || (docType == DocType.DOZV_TR_SPEC))
                    if ((docType == DocType.VISN) || (docType == DocType.DOZV) || (docType == DocType.DOZV_FOREIGHT) || (docType == DocType.DOZV_TRAIN) || (docType == DocType.LYST_TR_CALLSIGN) || (docType == DocType.DOZV_AR3_SPEC) || (docType == DocType.DOZV_TR_SPEC))
                    {
                        string[] number = null;

                        if ((ApplType == AppType.AppAR_3) ||
                           (ApplType == AppType.AppZRS) ||
                           (ApplType == AppType.AppA3) ||
                           (ApplType == AppType.AppTR_З) ||
                            (ApplType == AppType.AppTR))
                        {

                            number = PrintDocs.GetListDocNumberForDozvWma(listApplId.Count);
                            if (docType == DocType.DOZV_TR_SPEC)
                            {
                                number = PrintDocs.getListDocNumber(docType, 1);
                            }

                        }
                        else
                            number = PrintDocs.getListDocNumber(docType, listApplId.Count);

                        int numberFile = frmReport.FileNumber.ToInt32(0);
                        foreach (int applId in listApplId)
                        {
                            if (ApplType == AppType.AppAR_3)
                            {
                                AbonentStation station = BaseStation.GetStationFromAppl(applId) as AbonentStation;
                                if (station != null)
                                {
                                    endDate = station.PermEndDate;
                                    station.NumBlank = numberFile.ToString();
                                    station.Save();
                                    numberFile++;
                                }
                            }
                            using (BaseAppClass appl = BaseAppClass.GetBaseAppl(applId))
                            {
                                using (MainAppForm formAppl = new MainAppForm(appl)) //Это необходимо для загрузки позиций
                                {
                                    appl.WritePrintDate();
                                    string fileName = appl.PacketPrint(startDate, endDate, docType, number[index]);
                                    index++;
                                    fileList.Add(fileName);
                                }
                            }
                        }
                    }
                    else if ((docType == DocType.DOZV_CANCEL) || (docType == DocType._0159))
                    {
                        if((docType == DocType.DOZV_CANCEL) && (ApplType == AppType.AppAR_3))
                        {
                            foreach (int applId in listApplId)
                                ApplDocuments.CancelPermision(applId, startDate);
                        }
                        List<int> lstPacketId = new List<int>();
                        lstPacketId.Add(idPacket);
                        PrintDocs newDoc = new PrintDocs();
                        string fileName = newDoc.PrintManyReports(docType, startDate, endDate, ManagemenUDCR.URCP, PlugTbl.itblXnrfaPacket, lstPacketId, applType, null, listApplId);
                        fileList.Add(fileName);
                    }
                    else if ((docType == DocType.REESTR_VID) || (docType == DocType.LYST_ZRS_CALLSIGN))
                    {
                        List<int> lstPacketId = new List<int>();
                        lstPacketId.Add(idPacket);
                        PrintDocs newDoc = new PrintDocs(); string fileName = "";
                        if (docType == DocType.REESTR_VID)
                        {
                        string fileNameDoc = GetNameReestrVid(idPacket);
                        if (string.IsNullOrEmpty(fileNameDoc) == false)
                            fileNameDoc += ".rtf";
                        fileName = newDoc.PrintManyReports(docType, startDate, endDate, ManagemenUDCR.URCP, PlugTbl.itblXnrfaPacket, lstPacketId, applType, null, listApplId);
                        }
                        else fileName = newDoc.PrintManyReports(docType, startDate, endDate, ManagemenUDCR.URCP, PlugTbl.itblXnrfaPacket, lstPacketId, applType, null, listApplId);
                      
                        fileList.Add(fileName);
                    }
                    else
                    {
                        
                        PrintDocs newDoc = new PrintDocs();
                        string fileName = "";
                        if (docType != DocType.LYST_ZRS_CALLSIGN)
                        {
                            fileName = newDoc.PrintManyReports(docType, startDate, endDate, ManagemenUDCR.URCP, PlugTbl.itblXnrfaAppl, listApplId, applType, null, listApplId);
                        }
                        else
                        {
                            fileName = newDoc.PrintManyReports(docType, startDate, endDate, ManagemenUDCR.URCP, PlugTbl.itblXnrfaPacket, listApplId, applType, null, listApplId);
                        }
                        
                        fileList.Add(fileName);
                         
                    }

                    //Изменяем статусы станций, если необходимо
                    StationStatus wf = new StationStatus();
                    foreach (int applId in listApplId)
                    {// Меняем статус если необходимо
                        StatusType curStatus = BaseAppClass.ReadStatus(applId);
                        StatusType newStatus = wf.GetNewStatus(curStatus, docType, StationStatus.ETypeEvent.PacketPrintDoc);
                        if (curStatus != newStatus)
                            BaseAppClass.UpdateStatus(newStatus, applId);

                        //-----
                        // Присоединяем к пакету
                        List<LinkDocFiles> ListDocAttachPachet = GetAllLinkDocsFromIDPacket(idPacket);
                        PrintDocs prntDoc = new PrintDocs();
                        foreach (LinkDocFiles item_ in ListDocAttachPachet)
                        {
                            if (!prntDoc.isDublicateRec(item_.ID))
                                prntDoc.LoadDocLink(item_.DOC_TYPE, System.IO.Path.GetFileName(item_.PATH), item_.PATH, item_.DATE_CREATED, IM.NullT, new List<int> { applId },false);
                        }
                        //-----

                    }

                    if (fileList.Count > 0) {
                        int indexName = fileList[0].IndexOf(System.IO.Path.GetFileName(fileList[0]));
                        string path = fileList[0].Remove(indexName, fileList[0].Length - indexName);
                        if (ListSelectedColumns != null) {
                            if (ListSelectedColumns.Count > 0)
                                RefreshRows();
                        }
                        MessageBox.Show("Файли документів розміщено за адресою: " + path, "Документи згенеровано");
                    }
                }
            }
        }
        /// <summary>
        /// Формирует имя файла для "Реестраційна відомість"
        /// </summary>
        /// <param name="packetId">Id пакета</param>
        /// <returns></returns>
        private static string GetNameReestrVid(int packetId)
        {
            string retVal = "";
            IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite);
            rsPacket.Select("ID,NUMBER_IN,NUM_REESTR_VID");
            rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packetId);
            try
            {
                rsPacket.Open();
                if(!rsPacket.IsEOF())
                {
                    int curIndex = rsPacket.GetI("NUM_REESTR_VID");
                    retVal = string.Format("{0}-{1}", packetId, curIndex);
                    curIndex++;
                    rsPacket.Edit();
                    rsPacket.Put("NUM_REESTR_VID", curIndex);
                    rsPacket.Update();
                }
            }
            finally
            {
                if (rsPacket.IsOpen())
                    rsPacket.Close();
                rsPacket.Destroy();
            }
            return retVal;
        }

        //===================================================
        /// <summary>
        /// свойство стандарта
        /// </summary>
        public string Standart
        {
            get { return radioTech; }
            set { radioTech = CRadioTech.ToStandard(value); }
        }
        //===================================================
        /// <summary>
        /// Свойство выходной даты
        /// </summary>
        public DateTime DateOut
        {
            get
            {
                return dateOut;
            }
            set
            {
                dateOut = value;
            }
        }
        //===================================================
        /// <summary>
        /// Выдалення завки з пакету
        /// </summary>
        public void DeleteAppl()
        {
            List<int> lstDeleteApplId = new List<int>(GelListIdSelectedAppl());

            if (lstDeleteApplId.Count == 0)
            {
                MessageBox.Show(CLocaliz.TxT("There are no selected applications"), "",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;  //Выходим
            }
            //Подтверждение удаления
            if (MessageBox.Show(string.Format(CLocaliz.TxT("Do you want to delete {0} selected application(s)?"), lstDeleteApplId.Count),
                                CLocaliz.TxT("Warning"),
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Warning) != DialogResult.Yes)
                return;  //Выходим
            //Проверка удаления некоторых заявок целиком
            List<int> lstApplWillDeletePermanently = new List<int>();
            foreach (int id in lstDeleteApplId)
            {
                int count = 0;
                if (IM.ExecuteScalar(ref count, string.Format("SELECT COUNT(1) FROM %{0} WHERE APPL_ID={1}", PlugTbl.itblPacketToAppl, id)) == false)
                    count = 0;
                if (count == 0)
                    lstApplWillDeletePermanently.Add(id);
            }
            if ((lstApplWillDeletePermanently.Count > 0) &&
                (MessageBox.Show(CLocaliz.TxT("Some applications will be deleted permanently!!! Do you want to continue?"),
                     CLocaliz.TxT("Warning"),
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Warning) != DialogResult.Yes))
                return;
            //Удаление
            StationStatus wf = new StationStatus();
            foreach (int applId in lstDeleteApplId)
            {
                if (lstApplWillDeletePermanently.Contains(applId) == false)
                {
                    IMRecordset applPack = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadWrite);
                    applPack.Select("PACKET_ID,APPL_ID");
                    applPack.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, idPacket);
                    applPack.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applId);
                    try
                    {
                        applPack.Open();
                        if (!applPack.IsEOF())
                            applPack.Delete();
                    }
                    finally
                    {
                        if (applPack.IsOpen())
                            applPack.Close();
                        applPack.Destroy();
                    }
                    //Обновление статуса при удалении заявки из пакета
                    StatusType statusOld = BaseAppClass.ReadStatus(applId);
                    StatusType statusNew = wf.GetNewStatus(statusOld, applType, StationStatus.ETypeEvent.DeleteAppl);
                    if(statusNew.StatusAsString != statusOld.StatusAsString)
                        BaseAppClass.UpdateStatus(statusNew, applId);
                }
                else
                {
                    IMRecordset applFrom = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
                    applFrom.Select("ID");
                    applFrom.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                    try
                    {
                        applFrom.Open();
                        if (!applFrom.IsEOF())
                        {
                            int curId = applFrom.GetI("ID");
                            CLogs.WriteInfo(ELogsWhat.Delete, string.Format("{0} ({1})", PlugTbl.itblXnrfaAppl, curId));
                            applFrom.Delete();
                        }
                    }
                    finally
                    {
                        if (applFrom.IsOpen())
                            applFrom.Close();
                        applFrom.Destroy();
                    }
                }
            }
            IM.RefreshQueries(PlugTbl.itblXnrfaAppl);
            if (ListSelectedColumns != null) {
                if (ListSelectedColumns.Count > 0)
                    RefreshRows();
            }
        }
        //===================================================
        /// <summary>
        /// Attaches of a document to applications
        /// </summary>
        public void AttachDoc()
        {
            bool Status_Selected_Items = false;
            List<int> listApplId = new List<int>(GelListIdSelectedAppl());
            FAttachDoc.CurrTypeDoc = false;
            if (listApplId.Count == 0)
            {
                Status_Selected_Items = true;
            }

            if (Status_Selected_Items)
            {

                if (MessageBox.Show(CLocaliz.TxT("Attached document in the packet?"), CLocaliz.TxT("Message"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                else
                {
                    SetAllApplCheckBox(true);
                    listApplId = new List<int>(GelListIdSelectedAppl());
                    FAttachDoc.CurrTypeDoc = Status_Selected_Items;
                }
            }
                    //--------------
                    using (FAttachDoc frm = new FAttachDoc(PacketId, listApplId.ToArray()))
                    {
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            //PrintDocs prntDoc = new PrintDocs();
                            //prntDoc.LoadDocLink(frm.DocTypeCode, frm.docRef, frm.fileName, frm.docDate, frm.expDate, listApplId);
                            //frm.NewIdLinkDoc = prntDoc.NewIdLinkDoc;

                            //Изменяем статусы станций, если необходимо
                            StationStatus wf = new StationStatus();
                            foreach (int applId in listApplId)
                            {// Меняем статус если необходимо
                                if (frm.DocTypeCode == DocType.ATTACH_DOZV_BY_NKRZ)
                                {
                                    ApplDocuments.CancelPermision(applId, frm.docDate);
                                }
                                StatusType curStatus = BaseAppClass.ReadStatus(applId);
                                StatusType newStatus = wf.GetNewStatus(curStatus, frm.DocTypeCode, StationStatus.ETypeEvent.AttachDoc);
                                if (curStatus != newStatus)
                                    BaseAppClass.UpdateStatus(newStatus, applId);
                            }
                            if (ListSelectedColumns != null) {
                                if (ListSelectedColumns.Count > 0)
                                    RefreshRows();
                            }
                            MessageBox.Show(CLocaliz.TxT(string.Format("The document \"{0}\" is attached.", frm.fileName)), CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    if (Status_Selected_Items) SetAllApplCheckBox(false);

        }
        /// <summary>
        /// Изменить статус на отправить в УРЗП
        /// </summary>
        public virtual void SendToUrcp()
        {
            //Статус не меняем
        }
        /// <summary>
        /// Возвращает список ID выбраных заявок
        /// </summary>
        /// <returns>список ID выбраных заявок</returns>
        public int[] GelListIdSelectedAppl()
        {
            List<int> listApplId = new List<int>();
            foreach (PacketRowBase rowAppl in _applListRows)
            {
                if (rowAppl.IsSelected)
                    listApplId.Add(rowAppl.ApplId);
            }
            return listApplId.ToArray();
        }
        /// <summary>
        /// Получить все ApplID 
        /// </summary>
        /// <returns></returns>
        public int[] GetListIdAllAppl()
        {
            List<int> listApplId = new List<int>();
            foreach (PacketRowBase rowAppl in _applListRows)
            {
                    listApplId.Add(rowAppl.ApplId);
            }
            return listApplId.ToArray();
        }
        /// <summary>
        /// Устанавливает CheckBox для всех заявок
        /// </summary>
        /// <param name="value">Значение CheckBox-а</param>
        public void SetAllApplCheckBox(bool value)
        {
            foreach (PacketRowBase rowAppl in _applListRows)
                rowAppl.IsSelected = value;
        }
        /// <summary>
        /// Установить флажок для заданного РЧП
        /// </summary>
        /// <param name="IdAppl"></param>
        public void SetAllApplCheckBoxID(int IdAppl)
        {
            foreach (PacketRowBase rowAppl in _applListRows)
            {
                if (rowAppl.ApplId == IdAppl)
                {
                    rowAppl.IsSelected = true;
                    break;
                }
            }
        }


        public void PrintOneByOne(bool showGui, Form parentForm)
        {            
            AppPacket.Forms.PacketDocumentPrintForm printForm;// = null;// = new AppPacket.Forms.PacketDocumentPrintForm();
            
            int currentDocNum = 0;

            foreach (int applId in GelListIdSelectedAppl())
            {
                IMRecordset rs = new IMRecordset("XV_PERM_ALL", IMRecordset.Mode.ReadOnly);
                rs.Select("APPL_ID,PERM_NUM,FILE_PATH");
                rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applId);
                rs.Open();

                if (!rs.IsEOF())
                {
                    string permNum = rs.GetS("PERM_NUM");
                    string filePath = rs.GetS("FILE_PATH");

                    if (showGui)
                    {
                        printForm = new AppPacket.Forms.PacketDocumentPrintForm();
                        printForm.Owner = parentForm;
                        printForm.SetInfo(permNum, filePath);
                        if (printForm.ShowDialog() == DialogResult.Cancel)
                            break;

                        string blankNumber = printForm.BlankNumber;
                    }

                    ApplDocuments.SendToPrinter(filePath, false);

                rs.Close();
                    currentDocNum++;
                }
                if (rs.IsOpen())
                    rs.Destroy();
                rs.Destroy();
            }            
        }

        public void PrintContinous(int blankNumber, bool showGui, Form parentForm)
        {
            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Packet printing of permissions")))
            {                
                int currentDocNum = 0;

                if (showGui)
                {
                    parentForm.Enabled = false;
                }

                foreach (int applId in GelListIdSelectedAppl())
                {
                    IMRecordset rs = new IMRecordset("XV_PERM_ALL", IMRecordset.Mode.ReadOnly);
                    rs.Select("APPL_ID,PERM_NUM,FILE_PATH");
                    rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applId);
                    rs.Open();

                    if (!rs.IsEOF())
                    {
                        string permNum = rs.GetS("PERM_NUM");
                        string filePath = rs.GetS("FILE_PATH");

                        if (showGui)
                        {
                            //processForm.SetInfo(permNum, currentDocNum, GelListIdSelectedAppl().Length);
                            //Thread.Sleep(2000);

                            pb.SetBig(permNum);
                            pb.SetSmall(currentDocNum +" / " + GelListIdSelectedAppl().Length);
                            pb.SetProgress(currentDocNum, GelListIdSelectedAppl().Length);
                            Application.DoEvents();

                            ApplDocuments.SendToPrinter(filePath, false);
                            
                            Application.DoEvents();

                            currentDocNum++;
                            pb.SetSmall(currentDocNum + " / " + GelListIdSelectedAppl().Length);
                            pb.SetProgress(currentDocNum, GelListIdSelectedAppl().Length);

                            if (pb.UserCanceled())
                                break;
                        }

                        rs.Close();
                        currentDocNum++;
                    }
                    if (rs.IsOpen())
                        rs.Destroy();
                    rs.Destroy();
                }

                if (showGui)
                {                
                    parentForm.Enabled = true;
                }
            }
        }


        public List<string> Print(string blankNumber, bool showGui, Form parentForm, bool bContinuous, string docType)
        {
            List<string> _errorList = new List<string>();

            string docNumFld = "";
            string docFileFld = "";

            if (docType == "DOZV")
            {
                docNumFld = "DOZV_NUM_NEW";
                docFileFld = "DOZV_FILE_NEW";
            } 
            else if (docType == "VISN")
            {
                docNumFld = "CONC_NUM_NEW";
                docFileFld = "CONC_FILE_NEW";
            }
            else
                throw new Exception ("Not allowed Doc Type for bacth print: '" + docType + "'");

            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Packet printing of permissions")))
            {
                int currentDocNum = 0;
                string currentBlankNumber = blankNumber;

                parentForm.Enabled = false;

                Dictionary<int, string[]> docData = new Dictionary<int, string[]>();

                foreach (int applId in GelListIdSelectedAppl())
                {
                    IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                    rs.Select("ID," + docNumFld + "," + docFileFld);
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        string permNum = rs.GetS(docNumFld);
                        string filePath = rs.GetS(docFileFld);

                        if (string.IsNullOrEmpty(permNum))
                        {
                            _errorList.Add("Відсутній номер документу для заявки " + applId);
                        }
                        else
                        {
                            if (!File.Exists(filePath))
                            {
                                _errorList.Add("Не існує файл '" + filePath + "' для заявки " + applId);
                            }
                        else
                                docData.Add(rs.GetI("ID"), new string[] {permNum, filePath});
                        }
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }

                if (_errorList.Count > 0)
                {
                    return _errorList;
                }

                foreach (KeyValuePair<int, string[]> kvp in docData)
                {
                    int applId = kvp.Key;
                    string permNum = kvp.Value[0];
                    string filePath = kvp.Value[1];

                    pb.SetBig(permNum);
                    pb.SetSmall(currentDocNum + " / " + GelListIdSelectedAppl().Length);
                    pb.SetProgress(currentDocNum, GelListIdSelectedAppl().Length);

                    if (!bContinuous)
                    {
                        AppPacket.Forms.PacketDocumentPrintForm printForm = new AppPacket.Forms.PacketDocumentPrintForm();
                        printForm.Owner = parentForm;
                        printForm.SetInfo(permNum, filePath);
                        if (printForm.ShowDialog() == DialogResult.Cancel)
                            break;

                        currentBlankNumber = printForm.BlankNumber;
                    }

                    try
                    {
                        ApplDocuments.SendToPrinter(filePath, false);

                        bool saveResults = false;
                        EDocEvent evt = EDocEvent.evDRVPrintDozv;

                        switch (ApplType)
                        {
                            default:
                                saveResults = false;
                                break;
                            case AppType.AppZRS:
                                {
                                    IMRecordset r = null;
                                    r = IMRecordset.ForRead(new RecordPtr(PlugTbl.APPL, applId), "OBJ_ID1");
                                    int staId = r.GetI("OBJ_ID1");
                                    r.Destroy();
                                    r = IMRecordset.ForWrite(new RecordPtr(PlugTbl.Ship, staId), "CUST_TXT4");
                                    r.Edit();
                                    r.Put("CUST_TXT4", currentBlankNumber.ToString());
                                    r.Update();
                                    r.Destroy();
                                    r = null;
                                    saveResults = true;
                                }
                                break;
                            case AppType.AppAR_3:
                            case AppType.AppAP3:
                                {
                                    AbonentStation station = BaseStation.GetStationFromAppl(applId) as AbonentStation;
                                    if (station != null)
                                    {
                                        if (docType == "DOZV")  //#7260 - Возможны и Заключения, но записывать ли поле Номер Бланка в этом случае - непонятно
                                        {
                                            station.NumBlank = currentBlankNumber.ToString();
                                            station.Save();
                                        }
 
                                        saveResults = true;
                                    }
                                }
                                break;
                            case AppType.AppTR_З:
                                {
                                    RailwayApplStation station = BaseStation.GetStationFromAppl(applId) as RailwayApplStation;
                                    if (station != null)
                                    {
                                        if (docType == "DOZV")  //#7260 - Возможны и Заключения, но записывать ли поле Номер Бланка в этом случае - непонятно
                                        {
                                            station.NumBlank = currentBlankNumber.ToString();
                                            station.Save();
                                        }
                                        saveResults = true;
                                    }
                                }
                                break;
                            case AppType.AppA3:
                                {
                                    //если нужно фиксировать номер бланка для MOVE - см. AmateurAppl.PrintOnBlank для MOVE. 
                                    IMRecordset r = null;
                                    r = IMRecordset.ForRead(new RecordPtr(PlugTbl.APPL, applId), "OBJ_ID1");
                                    int staId = r.GetI("OBJ_ID1");
                                    r.Destroy();
                                    r = IMRecordset.ForWrite(new RecordPtr(PlugTbl.XfaAmateur, staId), "NUMB_BLANK");
                                    r.Edit();
                                    r.Put("NUMB_BLANK", currentBlankNumber.ToString());
                                    r.Update();
                                    r.Destroy();
                                    r = null;

                                    saveResults = true;
                                }
                                break;
                        }

                        if (saveResults)
                        {
                            if (docType == "DOZV")
                            {
                                ApplDocuments.PrintPermision(applId, permNum, DateTime.Now, currentBlankNumber.ToString());
                            }
                            else if ((docType == "CONC") || (docType == "VISN"))
                            {
                                ApplDocuments.PrintConclusion(applId, permNum, DateTime.Now, currentBlankNumber.ToString());
                                evt = EDocEvent.evDRVPrintVisn;


                            }
                            //currentBlankNumber++;
                            int cnt_zero = 0;
                            string TXT_ZERO = "";
                            int blank = Convert.ToInt32(currentBlankNumber);
                            blank++;

                            for (int i = 0; i < currentBlankNumber.Length; i++)
                            {
                                if ((currentBlankNumber[i] == '0') && ((cnt_zero) == i))
                                {
                                    ++cnt_zero;
                                }
                                else
                                {
                                    break;
                                }
                            }

                            for (int i = 0; i < cnt_zero; i++)
                            {
                                TXT_ZERO = TXT_ZERO + "0";
                            }

                            currentBlankNumber = TXT_ZERO + blank.ToString();

                            //CEventLog.AddApplEvent(applId, evt, DateTime.Now, permNum, IM.NullT, filePath);
                            CEventLog.AddApplEventWithoutChangeStatus(applId, evt, DateTime.Now, permNum, IM.NullT, filePath);
                        }

                    }
                    catch (Exception e)
                    {
                        string errMsg = "Помилка друку документу " + currentDocNum.ToString() + ": " + e.Message;
                        while ((e = e.InnerException) != null)
                            errMsg += (": " + e.Message);
                        _errorList.Add(errMsg);
                    }

                    currentDocNum++;
                    pb.SetSmall(currentDocNum + " / " + GelListIdSelectedAppl().Length);
                    pb.SetProgress(currentDocNum, GelListIdSelectedAppl().Length);

                    if (pb.UserCanceled())
                        break;

                }                
                
                parentForm.Enabled = true;
            }
            return _errorList;
        }

        /// <summary>
        /// Внесення дати друку для кожного вибраного РЧП ЕФ
        /// </summary>
        /// <param name="issue"></param>
        public void ValidFrom(DateTime issue)
        {
            int[] arrIdAppl = GelListIdSelectedAppl();
            for (int i = 0; i < arrIdAppl.Length; i++)
            {
                string tblName = "";
                switch(ApplType)
                {
                    case AppType.AppAP3:
                    case AppType.AppAR_3:
                    case AppType.AppTR_З:
                        tblName = PlugTbl.XfaAbonentStation; break;
                    case AppType.AppZRS:
                        tblName = PlugTbl.Ship; break;
                    case AppType.AppA3:
                        tblName = PlugTbl.XfaAmateur; break;
                }
                if (tblName.Length > 0)
                    FillIssueDate(tblName, arrIdAppl[i], issue);
            }
            if (ListSelectedColumns != null) {
                if (ListSelectedColumns.Count > 0)
                    RefreshRows();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="issue"></param>
        /// <param name="tp_"></param>
        public void ValidFrom_XFA_ABONNET_STATION(DateTime issue, string tp_)
        {
            int[] arrIdAppl = GelListIdSelectedAppl();
            for (int i = 0; i < arrIdAppl.Length; i++)
            {
                string tblName = "";
                switch (ApplType)
                {
                    case AppType.AppAR_3:
                    case AppType.AppTR_З:
                        tblName = PlugTbl.XfaAbonentStation; break;
                    case AppType.AppZRS:
                        tblName = PlugTbl.Ship; break;
                    case AppType.AppA3:
                        tblName = PlugTbl.XfaAmateur; break;
                }
                if (tblName.Length > 0)
                {
                    IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadOnly);
                    rsEvent.Select("CREATED_DATE");
                    rsEvent.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, arrIdAppl[i]);
                    rsEvent.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.itblXnrfaAppl);
                    if (tp_ == DocType.DOZV)
                        rsEvent.SetWhere("EVENT", IMRecordset.Operation.Like, EDocEvent.evDOZV.ToString());
                    if (tp_ == DocType.VISN)
                        rsEvent.SetWhere("EVENT", IMRecordset.Operation.Like, EDocEvent.evVISN.ToString());

                    rsEvent.OrderBy("CREATED_DATE", OrderDirection.Descending);
                    try
                    {
                        rsEvent.Open();
                        if (rsEvent.IsEOF())
                        {
                            if (tp_ == DocType.DOZV)
                            {
                                MessageBox.Show("Для вибраного РЧП відсутній сформований проект документу 'Дозвіл на експлуатацію'");
                            }
                            else if (tp_ == DocType.VISN)
                            {
                                MessageBox.Show("Для вибраного РЧП відсутній сформований проект документу 'Висновок щодо ЕМС'");
                            }
                        }
                        else
                        {
                            
                            SetIssueDate(tblName, arrIdAppl[i], issue, tp_);
                            if (tp_ == DocType.DOZV)
                            {
                                string newDozvNum = "";
                                DateTime dateFrom;
                                DateTime dateTo;
                                string pathFile = "";
                                ApplDocuments.GetNewPermData(arrIdAppl[i], out newDozvNum, out dateFrom, out dateTo, out pathFile);
                                if (newDozvNum != "")
                                {
                                    ApplDocuments.CreateNewPermission(arrIdAppl[i], newDozvNum.ToFileNameWithoutExtension(), issue, issue.AddMonths(6), System.IO.Path.GetDirectoryName(newDozvNum));
                                    CEventLog.AddApplEventWithoutChangeStatus(arrIdAppl[i], EDocEvent.evDOZV, issue, newDozvNum, IM.NullT, pathFile);
                                }
                                
                            }
                            if (tp_ == DocType.VISN)
                            {
                                string newDozvNum = "";
                                DateTime dateFrom;
                                DateTime dateTo;
                                string pathFile = "";
                                ApplDocuments.GetConcData(arrIdAppl[i], out newDozvNum, out dateFrom, out dateTo, out pathFile);
                                if (newDozvNum != "")
                                {
                                    ApplDocuments.CreateNewConclusion_XFA_ABONENT_STATION(arrIdAppl[i], newDozvNum.ToFileNameWithoutExtension(), issue, issue, System.IO.Path.GetDirectoryName(newDozvNum));
                                    CEventLog.AddApplEventWithoutChangeStatus(arrIdAppl[i], EDocEvent.evVISN, issue, newDozvNum, issue.AddMonths(6), pathFile);
                                }
                            }
                                                    
                        }
                    }
                    finally
                    {
                        rsEvent.Destroy();
                    }
                }
            }
            if (ListSelectedColumns != null) {
                if (ListSelectedColumns.Count > 0)
                    RefreshRows();
            }
        }

        internal class DocItem
        {
            public string docType { get; set; }
            public string docName { get; set; }
            public string blankNum { get; set; }

            public DocItem(string dt, string dn, string bn)
            {
                docType = dt; docName = dn; blankNum = bn;
            }

            public override string ToString()
            {
                return (docType == DocType.DOZV || docType == DocType.DOZV_APC || docType == DocType.DOZV_APC50 || docType == DocType.DOZV_RETR ? "Осн. дозв." :
                    docType == DocType.DOZV_SPS ? "СПС/УПС" : docType == DocType.DOZV_MOVE ? "Дозв. Рух. Ст." : docType 
                    ) + " " + docName + ": номер бланку " + blankNum;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="station"></param>
        /// <param name="appl"></param>
        /// <param name="issue"></param>
        /// <param name="tp_"></param>
        private void SetIssueDate(string station, int appl, DateTime issue, string tp_)
        {
            string blankNumb = "";
            //string oldDozvNum = ApplDocuments.GetNewPermNum(appl);
            string ownerName = "";
            int User_ID=IM.NullI;
            int staId = IM.NullI;
            switch (station)
            {
                case PlugTbl.XfaAbonentStation:
                    {
                        IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,OBJ_ID1,Abonent.BLANK_NUM,Abonent.USER_ID,AmateurStation.NUMB_BLANK,AmateurStation.Owner.NAME");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, appl);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                            {
                                staId = r.GetI("OBJ_ID1");
                                switch (station)
                                {
                                    case PlugTbl.XfaAbonentStation:
                                        blankNumb = r.GetS("Abonent.BLANK_NUM");
                                        //User_ID = r.GetI("Abonent.USER_ID");
                                        break;
                                }
                            }
                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                    break;
            }

            List<DocItem> dl = new List<DocItem>();
            dl.Add(new DocItem(tp_, "", blankNumb));
            //dl.Add(new DocItem(DocType.DOZV, "", blankNumb));
             


            //
            string permNum = "";
            if (tp_ == DocType.DOZV)
            {
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID,DOZV_NUM_NEW");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, appl);
                rs.Open();
                if (!rs.IsEOF())
                {
                    permNum = rs.GetS("DOZV_NUM_NEW");
                }
                rs.Close();
            }
            else if (tp_ == DocType.VISN )
            {
                IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                rs.Select("ID,CONC_NUM_NEW");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, appl);
                rs.Open();
                if (!rs.IsEOF())
                {
                    permNum = rs.GetS("CONC_NUM_NEW");
                }
                rs.Close();
            }


            //
            foreach (DocItem item in dl)
            {

                switch (item.docType)
                {
                    case DocType.DOZV:
                    case DocType.DOZV_APC:
                    case DocType.DOZV_APC50:
                    case DocType.DOZV_CANCEL:
                    case DocType.DOZV_CANCEL_ANNUL:
                    case DocType.DOZV_MOVE:
                    case DocType.DOZV_OPER:
                    case DocType.DOZV_RETR:
                    case DocType.DOZV_SPS:
                    case DocType.MESS_DOZV_CANCEL:
                        //Запис в журнал подій   

                        CEventLog.AddEvent(PlugTbl.APPL, appl, EDocEvent.evIssuedDoc, issue, item.blankNum, issue, "",
                                     HelpFunction.getUserIDByName(IM.ConnectedUser()));
                        ApplDocuments.IssuedDocPermision_XFA_ABONENT_STATION(appl, permNum, issue, item.blankNum);
                        break;
                    case DocType.VISN:
                    case DocType.VISN_NR:

                        //Запис в журнал подій   
                        CEventLog.AddEvent(PlugTbl.APPL, appl, EDocEvent.evIssuedVisn, issue, item.blankNum, issue, "",
                                     HelpFunction.getUserIDByName(IM.ConnectedUser()));

                        ApplDocuments.IssuedDocConclusion_XFA_ABONENT_STATION(appl, permNum, issue, item.blankNum);
                        break;
                }


                HashSet<string> mainTypes = new HashSet<string> { DocType.DOZV, DocType.DOZV_APC, DocType.DOZV_APC50, DocType.DOZV_RETR };
                if (mainTypes.Contains(item.docType))
                {
                    StationStatus wf = new StationStatus();
                    StatusType curStatus = BaseAppClass.ReadStatus(appl);
                    StatusType newStatus = wf.GetNewStatus(curStatus, EDocEvent.evDRVPrintDozv);
                    if (curStatus != newStatus)
                        BaseAppClass.UpdateStatus(newStatus, appl);
                }
            }


            List<int> idList = new List<int>();
            //Проставляння статусів
            {
                IMRecordset rsChosenAppl = new IMRecordset("XNRFA_CHOSENAPPLS", IMRecordset.Mode.ReadOnly);
                rsChosenAppl.Select("ID,APPLPAY_ID,APPL_ID,ApplicationPay.ID,Application.ID");
                rsChosenAppl.SetWhere("APPL_ID", IMRecordset.Operation.Eq, appl);
                try
                {
                    for (rsChosenAppl.Open(); !rsChosenAppl.IsEOF(); rsChosenAppl.MoveNext())
                        idList.Add(rsChosenAppl.GetI("APPLPAY_ID"));
                }
                finally
                {
                    rsChosenAppl.Final();
                }
            }
            for (int i = 0; i < idList.Count; i++)
            {
                IMRecordset r = new IMRecordset("XNRFA_APPLPAY", IMRecordset.Mode.ReadWrite);
                r.Select("ID,PACKET_ID,STATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, idList[i]);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        r.Edit();
                        int tmpInt = r.GetI("STATE");
                        if (tmpInt == 4)
                            r.Put("STATE", 5);
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }
            }
            EDocEvent evt = EDocEvent.evDRVPrintDozv;
            CEventLog.AddApplEvent(appl, evt, issue, IM.NullT);
            Get_EOUSE_DATE(appl);
        }

        /// <summary>
        /// Заповнити дату дійсний до для абонентів і кораблів
        /// </summary>
        /// <param name="station"></param>
        /// <param name="appl"></param>
        /// <param name="issue"></param>
        private void FillIssueDate(string station, int appl, DateTime issue)
        {
            string blankNumb = "";
            //string oldDozvNum = ApplDocuments.GetNewPermNum(appl);
            string ownerName = "";
            int staId = IM.NullI;
            switch (station)
            {
                case PlugTbl.XfaAbonentStation:
                case PlugTbl.XfaAmateur:
                    {
                        IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,OBJ_ID1,Abonent.BLANK_NUM,AmateurStation.NUMB_BLANK,AmateurStation.Owner.NAME");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, appl);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                            {
                                staId = r.GetI("OBJ_ID1");
                                switch (station)
                                {
                                    case PlugTbl.XfaAbonentStation:
                                        blankNumb = r.GetS("Abonent.BLANK_NUM");
                                        break;
                                    case PlugTbl.XfaAmateur:
                                        blankNumb = r.GetS("AmateurStation.NUMB_BLANK");
                                        ownerName = r.GetS("AmateurStation.Owner.NAME");
                                        break;
                                }                               
                            }
                        }
                        finally
                        {
                            r.Final();
                        }
                    }
                    break;
            }

            List<DocItem> dl = new List<DocItem>();

            if (station == PlugTbl.XfaAmateur)
            {
                if (blankNumb.Length > 0)
                    dl.Add(new DocItem(DocType.DOZV, "", blankNumb));

                AmateurStation sta = new AmateurStation();
                sta.Id = staId;
                sta.Load();
                //СПС/УПС
                foreach (AmateurCalls spscall in sta.AmCallsSps)
                    if (spscall.NumberBlank.Length > 0)
                        dl.Add(new DocItem(DocType.DOZV_SPS, spscall.NumberDocument, spscall.NumberBlank));
                foreach (AmateurCalls upscall in sta.AmCallsUps)
                    if (upscall.NumberBlank.Length > 0)
                        dl.Add(new DocItem(DocType.DOZV_SPS, upscall.NumberDocument, upscall.NumberBlank));
                //Рух.Ст.Дозв
                IMRecordset r = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly);
                r.Select("ID,STA_ID,NUMB");
                r.Select("DATE_EXPIR_DOZV,NUMB_DOZV,PATH,NUM_BLANK");
                r.SetWhere("STA_ID", IMRecordset.Operation.Eq, staId);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        string bn = r.GetS("NUM_BLANK");
                        if (bn.Length > 0)
                            dl.Add(new DocItem(DocType.DOZV_MOVE, r.GetI("NUMB").ToString(), bn));
                    }
                }
                finally
                {
                    r.Destroy();
                }

                FChckListBoxToSelect dlg = new FChckListBoxToSelect();
                dlg.Text = "Оберіть документ(и)";
                dlg.lblText.Text = "Оберіть документ(и) аматорського РЧП " + ownerName + " для внесення дати видачі:";

                dlg.chLbAdditionalDocs.DataSource = dl;

                if (dlg.ShowDialog() == DialogResult.OK && dlg.chLbAdditionalDocs.CheckedItems.Count > 0)
                {
                    List<DocItem> newDl = new List<DocItem>();
                    foreach (DocItem item in dlg.chLbAdditionalDocs.CheckedItems)
                        newDl.Add(new DocItem(item.docType, item.docName, item.blankNum));
                    dl = newDl;
                }
                else
                    return;
            }
            else
                dl.Add(new DocItem(DocType.DOZV, "", blankNumb));


            //
            string permNum = "";
            IMRecordset rs = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,DOZV_NUM_NEW");
            rs.SetWhere("ID", IMRecordset.Operation.Eq, appl);
            rs.Open();
            if (!rs.IsEOF())
            {
                permNum = rs.GetS("DOZV_NUM_NEW");
            }
            rs.Close();

            //
            foreach (DocItem item in dl)
            {

                switch (item.docType)
                {
                    case DocType.DOZV:
                    case DocType.DOZV_APC:
                    case DocType.DOZV_APC50:
                    case DocType.DOZV_CANCEL:
                    case DocType.DOZV_CANCEL_ANNUL:
                    case DocType.DOZV_MOVE:
                    case DocType.DOZV_OPER:
                    case DocType.DOZV_RETR:
                    case DocType.DOZV_SPS:
                    case DocType.MESS_DOZV_CANCEL:
                        //Запис в журнал подій   

                        CEventLog.AddEvent(PlugTbl.APPL, appl, EDocEvent.evIssuedDozv, issue, item.blankNum, null, "",
                                     HelpFunction.getUserIDByName(IM.ConnectedUser()));
                        ApplDocuments.IssuedDocPermision(appl, permNum, issue, item.blankNum);
                        break;
                    case DocType.VISN:
                    case DocType.VISN_NR:

                        //Запис в журнал подій   
                        CEventLog.AddEvent(PlugTbl.APPL, appl, EDocEvent.evIssuedVisn, issue, item.blankNum, null, "",
                                     HelpFunction.getUserIDByName(IM.ConnectedUser()));

                        ApplDocuments.IssuedDocConclusion(appl, permNum, issue, item.blankNum);
                        break;
                }
                

                HashSet<string> mainTypes = new HashSet<string> { DocType.DOZV, DocType.DOZV_APC, DocType.DOZV_APC50, DocType.DOZV_RETR };
                if (mainTypes.Contains(item.docType))
                {
                    StationStatus wf = new StationStatus();
                    StatusType curStatus = BaseAppClass.ReadStatus(appl);
                    StatusType newStatus = wf.GetNewStatus(curStatus, EDocEvent.evDRVPrintDozv);
                    if (curStatus != newStatus)
                        BaseAppClass.UpdateStatus(newStatus, appl);
                }
            }
        

            List<int> idList = new List<int>();
            //Проставляння статусів
            {
                IMRecordset rsChosenAppl = new IMRecordset("XNRFA_CHOSENAPPLS", IMRecordset.Mode.ReadOnly);
                rsChosenAppl.Select("ID,APPLPAY_ID,APPL_ID,ApplicationPay.ID,Application.ID");
                rsChosenAppl.SetWhere("APPL_ID", IMRecordset.Operation.Eq, appl);
                try
                {
                    for (rsChosenAppl.Open(); !rsChosenAppl.IsEOF(); rsChosenAppl.MoveNext())
                        idList.Add(rsChosenAppl.GetI("APPLPAY_ID"));
                }
                finally
                {
                    rsChosenAppl.Final();
                }
            }
            for (int i = 0; i < idList.Count; i++)
            {
                IMRecordset r = new IMRecordset("XNRFA_APPLPAY", IMRecordset.Mode.ReadWrite);
                r.Select("ID,PACKET_ID,STATE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, idList[i]);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        r.Edit();
                        int tmpInt = r.GetI("STATE");
                        if (tmpInt == 4)
                            r.Put("STATE", 5);
                        r.Update();
                    }
                }
                finally
                {
                    r.Final();
                }
            } 
            EDocEvent evt = EDocEvent.evDRVPrintDozv;
            CEventLog.AddApplEvent(appl, evt, issue, IM.NullT);
            Get_EOUSE_DATE(appl);

        }
        /// <summary>
        /// Повертає значення терміну дії з XNRFA_APPL
        /// </summary>
        /// <param name="idAppl"></param>
        /// <returns></returns>
        public void  Get_EOUSE_DATE(int idAppl)
        {
            DateTime PermEndDate = IM.NullT;

            IMRecordset r = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ID,OBJ_ID1,OBJ_TABLE,DOZV_NUM_NEW,DOZV_DATE_TO_NEW,DOZV_DATE_TO,DOZV_NUM");
                r.SetWhere("ID", IMRecordset.Operation.Eq, idAppl);
                r.Open();
                if (!r.IsEOF())
                {
                    if (r.GetS("OBJ_TABLE") == PlugTbl.XfaAbonentStation)
                    {
                        if (!string.IsNullOrEmpty(r.GetS("DOZV_NUM_NEW")))
                        {
                            PermEndDate = r.GetT("DOZV_DATE_TO_NEW");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(r.GetS("DOZV_NUM")))
                            {
                                PermEndDate = r.GetT("DOZV_DATE_TO");
                            }
                        }



                        if (r.GetI("OBJ_ID1") != null)
                        {
                            if (r.GetI("OBJ_ID1") >= 0)
                            {
                                IMRecordset rx = new IMRecordset(PlugTbl.XfaAbonentStation, IMRecordset.Mode.ReadWrite);
                                try
                                {
                                    rx.Select("ID,EOUSE_DATE");


                                    rx.SetWhere("ID", IMRecordset.Operation.Eq, r.GetI("OBJ_ID1"));
                                    rx.Open();

                                    if (!rx.IsEOF())
                                    {
                                        rx.Edit();
                                        rx.Put("EOUSE_DATE", PermEndDate);
                                        rx.Update();
                                    }
                                }
                                finally
                                {
                                    rx.Final();
                                }
                            }
                        }

                    }
                }
            }
            finally
            {
                r.Final();
            }
        }    

        //===================================================
        /// <summary>
        /// Отобразить заявку
        /// </summary>
        public void UCRFEMS(string tableName)
        {
            // DataGridViewRow curRow = ApplGrid.CurrentRow;
            List<int> listApplId = new List<int>(GelListIdSelectedAppl());
            BaseAppClass appl_base = null;
            if ((listApplId != null) && (listApplId.Count > 0)) { appl_base = BaseAppClass.GetAppl(listApplId[0], false); }
            if (appl_base != null)
            {
                FSelectSCPattern frm = new FSelectSCPattern(appl_base.department, appl_base.departmentSector, DocType.VISN);
                if (frm.ShowDialog() == DialogResult.OK)
                {

                    for (int indexRow = 0; indexRow < _applListRows.Count; indexRow++)
                    {
                        PacketRowBase rowUrzp = _applListRows[indexRow];
                        {
                            if (rowUrzp.IsSelected)
                            {
                                BaseAppClass appl = BaseAppClass.GetAppl(_applListRows[indexRow].ApplId, false);
                                if (appl != null)
                                {
                                    if (!string.IsNullOrEmpty(tableName))
                                        appl.SetRight(tableName);
                                    using (MainAppForm formAppl = new MainAppForm(appl))
                                    {

                                        //string oldText = Environment.NewLine + formAppl.textBoxVisnovok.Text;
                                        //if ((BaseAppClass.AllTrim(frm.GetPatternText()).Trim().IndexOf(BaseAppClass.AllTrim(formAppl.textBoxDozvil.Text).Trim()) == -1) || (string.IsNullOrEmpty(BaseAppClass.AllTrim(formAppl.textBoxDozvil.Text).Trim())))
                                        //{

                                            //if (oldText.Trim().Length > 0) { formAppl.textBoxVisnovok.Text = frm.GetPatternText() + Environment.NewLine + oldText; }
                                            //else { formAppl.textBoxVisnovok.Text = frm.GetPatternText(); }
                                            formAppl.textBoxVisnovok.Text = frm.GetPatternText(); 
                                        //}

                                        formAppl.SaveToBase();
                                    }
                                    
                                }
                                appl.Dispose();

                            }
                        }
                    }
                }
                frm.Dispose();
            }

            appl_base.Dispose();


        }
        //===================================================
        /// <summary>
        /// Отобразить заявку
        /// </summary>
        public void eMSPermissionTool(string tableName)
        {
            List<int> listApplId = new List<int>(GelListIdSelectedAppl());
            BaseAppClass appl_base = null;
            if ((listApplId != null) && (listApplId.Count > 0)) { appl_base = BaseAppClass.GetAppl(listApplId[0], false); }
            if (appl_base != null)
            {
                FSelectSCPattern frm = new FSelectSCPattern(appl_base.department, appl_base.departmentSector, DocType.DOZV);
                if (frm.ShowDialog() == DialogResult.OK)
                {

                    for (int indexRow = 0; indexRow < _applListRows.Count; indexRow++)
                    {
                        PacketRowBase rowUrzp = _applListRows[indexRow];
                        {
                            if (rowUrzp.IsSelected)
                            {
                                BaseAppClass appl = BaseAppClass.GetAppl(_applListRows[indexRow].ApplId, false);
                                if (appl != null)
                                {
                                    if (!string.IsNullOrEmpty(tableName))
                                        appl.SetRight(tableName);
                                    using (MainAppForm formAppl = new MainAppForm(appl))
                                    {
                                        //string oldText = Environment.NewLine + formAppl.textBoxDozvil.Text;
                                        //if ((BaseAppClass.AllTrim(frm.GetPatternText()).Trim().IndexOf(BaseAppClass.AllTrim(formAppl.textBoxDozvil.Text).Trim()) == -1) || (string.IsNullOrEmpty(BaseAppClass.AllTrim(formAppl.textBoxDozvil.Text).Trim())))
                                        //{
                                        //if (oldText.Trim().Length > 0) { formAppl.textBoxDozvil.Text = frm.GetPatternText() + Environment.NewLine + oldText; }
                                        //else { formAppl.textBoxDozvil.Text = frm.GetPatternText(); }
                                            formAppl.textBoxDozvil.Text = frm.GetPatternText();
                                        //}

                                        formAppl.SaveToBase();
                                    }
                                    //appl.SaveApplication();

                                }
                                appl.Dispose();

                            }
                        }
                    }
                }
                frm.Dispose();
            }

            appl_base.Dispose();

        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ExtendedColumnsProperty
    {
        public string NameProperty { get; set;}
        public string CaptionProperty { get; set; }
        public string PathToColumn { get; set; }
        public AppType TypeRadiosys { get; set; }
        public bool isSpecialStatus { get; set; } // Признак, что поле со спец статусом (true - t статус)
        public bool isInvisible { get; set; } // Признак, что поле не отображается (true - не отображается)
        public bool isMandatory { get; set; } // Признак, что поле является обязательным (true - обязательное поле)
        public bool StatusExclude { get; set; } // Признак исключения столбца из общего перечня (если true - столбец исключен)
        public int Number_List { get; set; } // номер набора стандартаных полей (1 - для формы пакета заявок, 2- для формы поиска РЧП, 3 - для формы пакета электронных заявок, 4 - вспомогательные наборы полей)

        public ExtendedColumnsProperty(string NameProperty_, string CaptionProperty_, string PathToColumn_, AppType TypeRadiosys_)
        {
            NameProperty = NameProperty_;
            CaptionProperty = CaptionProperty_;
            PathToColumn = PathToColumn_;
            TypeRadiosys = TypeRadiosys_;
        }


        public ExtendedColumnsProperty(string NameProperty_, string CaptionProperty_, string PathToColumn_, AppType TypeRadiosys_, bool isMandatory_)
            : this(NameProperty_, CaptionProperty_, PathToColumn_, TypeRadiosys_)
        {
            isMandatory = isMandatory_;
        }

        public ExtendedColumnsProperty(string NameProperty_, string CaptionProperty_, string PathToColumn_, AppType TypeRadiosys_, bool isMandatory_, int Number_List_)
            : this(NameProperty_, CaptionProperty_, PathToColumn_, TypeRadiosys_, isMandatory_)
        {
            Number_List = Number_List_;
        }
        

        public override string ToString()
        {
            return CaptionProperty;
        }
    }


}