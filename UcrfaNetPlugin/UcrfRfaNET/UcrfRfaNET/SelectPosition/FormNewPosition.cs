﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ICSM;
using IdwmNET;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;

namespace XICSM.UcrfRfaNET
{
    public partial class FormNewPosition : FBaseForm
    {
        /// <summary>
        /// Долгота
        /// </summary>
        private double _longDms = IM.NullD;
        /// <summary>
        /// Широта
        /// </summary>
        private double _latDms = IM.NullD;
        /// <summary>
        /// Список прикрепленных файлов
        /// </summary>
        private string[] _listFiles;
        /// <summary>
        /// Список прикрепленных файлов
        /// </summary>
        public string[] ListAttachFile { get { return _listFiles; } }
        /// <summary>
        /// Отображать контролы для выбора фотографий
        /// </summary>
        public bool CanAttachPhoto { get; set; }
        /// <summary>
        /// Дать возможность выбора городов
        /// </summary>
        public bool CanEditPosition { get; set; }
        /// <summary>
        /// Дать возможность редактировать улицу
        /// </summary>
        public bool CanEditStreet { get; set; }
        /// <summary>
        /// Старая позиция (отсюда берется только улица)
        /// </summary>
        public PositionState OldPosition { get; set; }
       
        //===================================================

        private SelectAddressControlFast _addressCtrl;
        
        //===================================================
        public FormNewPosition(int _idCities)
        {
            InitializeComponent();
            CLocaliz.TxT(this);
            OldPosition = null;
            //------
            CanAttachPhoto = false;
            CanEditPosition = true;
            CanEditStreet = true;
            //------    

            _addressCtrl = new SelectAddressControlFast();
            _addressCtrl.PropertyChanged += Address_PropertyChanged;
            panelAddress.Controls.Add(_addressCtrl);
            _addressCtrl.Dock = DockStyle.Fill;
            _addressCtrl.InitializeByCityId(_idCities);
            //================================================
            //
            //int a = 0;            
        }

        void Address_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsValid")
            {
                buttonOk.Enabled = _addressCtrl.IsValid;            
            }
        }
       
        /// <summary>
        /// Возвращает обьект Position
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="id">Айди записи</param>
        /// <returns></returns>
        public PositionState LoadNewPosition(string tableName, double X, double Y)
        {
            PositionState positionState = new PositionState();
            positionState.Id = IM.NullI;
            positionState.TableName = tableName;
            positionState.LongDms = X;//.DmsToDec();
            positionState.LatDms = Y;//.DmsToDec();
            positionState.X = X;
            positionState.Y = Y;
            positionState.Csys = "4DMS";

            if (_addressCtrl.CurrentCity != null)
                positionState.CityId = _addressCtrl.CurrentCity.Id;
            else
                positionState.CityId = IM.NullI;

            positionState.CountryId = "UKR";
            positionState.ASl = 0;
            //-----------------------------
            //Созадем адрес
            string adr = GetShortAddress();
            //-----------------------------
            // Создаем полный адрес
            string fullAdr = GetFullAddress();

            // positionState.Name= fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length) + ID_pos.ToString();
            positionState.Address = adr;

            if (_addressCtrl.CurrentCity != null)
            {
                positionState.Province = _addressCtrl.CurrentCity.prov;
                positionState.Subprovince = _addressCtrl.CurrentCity.subprov;
                positionState.City = _addressCtrl.CurrentCity.cityName;
                positionState.TypeCity = _addressCtrl.CurrentCity.type;
            }
            else
            {
                positionState.Province = _addressCtrl.GetProvinceText();
                positionState.Subprovince = _addressCtrl.GetSubprovinceText();
                positionState.City = "";
                positionState.TypeCity = "";
            }
            positionState.FullAddress = fullAdr;
            positionState.StreetType =  cbTypeStreet.Text;
            positionState.StreetName = tbStreet.Text;
            positionState.BuildingNumber = tbHouse.Text;
            positionState.Remark = tbNote.Text;
            return positionState;
        }

        //===================================================
        public IMObject getNewPosition(string tableName, double X, double Y)
        {
            IMObject newPos = IMObject.New(tableName);
            int ID_pos = IM.AllocID(tableName, 1, -1);
            newPos["ID"] = ID_pos;
            newPos["CODE"] = newPos.GetI("ID").ToString();
            newPos["TABLE_NAME"] = tableName;
            newPos["CSYS"] = "4DMS";
            newPos["LONGITUDE"] = X.DmsToDec();
            newPos["X"] = X;
            newPos["LATITUDE"] = Y.DmsToDec();
            newPos["Y"] = Y;                     
            newPos["DATUM"] = 4;
            newPos["CREATED_BY"] = IM.ConnectedUser();
            newPos["DATE_CREATED"] = DateTime.Now;
            newPos["CITY_ID"] = _addressCtrl.CurrentCity.Id;
            newPos["COUNTRY_ID"] = "UKR";
            newPos["ASL"] = 0;
            //-----------------------------
            //Созадем адрес
            string adr = GetShortAddress();
            //-----------------------------
            // Создаем полный адрес
            string fullAdr = GetFullAddress();

            newPos["NAME"] = fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length) + ID_pos.ToString();
            newPos["ADDRESS"] = adr;
            newPos["PROVINCE"] = _addressCtrl.CurrentCity.prov;
            newPos["SUBPROVINCE"] = _addressCtrl.CurrentCity.subprov;
            newPos["CITY"] = _addressCtrl.CurrentCity.cityName;
            newPos["CUST_TXT1"] = _addressCtrl.CurrentCity.type;
            newPos["REMARK"] = fullAdr;
            newPos.Put("CUST_TXT4", cbTypeStreet.Text);
            newPos.Put("CUST_TXT3", tbStreet.Text);
            newPos.Put("CUST_TXT8", tbHouse.Text);
            newPos.Put("CUST_TXT7", tbNote.Text);

            int id = CopyToAdmSite(newPos);
            newPos.Put("ADMS_ID", id);
            
            Idwm idwmVal = new Idwm();
            bool isNotInit = !idwmVal.Init(11);
            if (isNotInit)
                return newPos;
            string[] exclude = { "UKR" };
            NearestCountryItem[] nearCountry = idwmVal.GetNearestCountries(Idwm.DecToRadian((float)(X.DmsToDec())), Idwm.DecToRadian((float)(Y.DmsToDec())), 500, exclude, 1);
            if (nearCountry.Count() > 0)
                if (nearCountry[0].distance != IM.NullI) newPos.Put("DIST_BORDER", nearCountry[0].distance);
            return newPos;
        }

        /// <summary>
        /// Копіювати сайт в адмін сайт 
        /// </summary>
        /// <param name="pos"></param>
        private int CopyToAdmSite(IMObject pos)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.SITES, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CSYS,LONGITUDE,X,LATITUDE,Y,DATUM,CREATED_BY");
            r.Select("DATE_CREATED,CITY_ID,COUNTRY_ID,ASL,STATUS");
            r.Select("NAME,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK,CUST_TXT4");
            r.Select("CUST_TXT3,CUST_TXT8,CUST_TXT7");
            int id;
            try
            {
                r.Open();
                r.AddNew();
                id = IM.AllocID(ICSMTbl.SITES, 1, -1);
                r.Put("ID", id);
                r.Put("CSYS", "4DMS");

                r.Put("LONGITUDE", pos.GetD("X").DmsToDec());
                r.Put("X", pos.GetD("X"));
                r.Put("LATITUDE", pos.GetD("Y").DmsToDec());
                r.Put("Y", pos.GetD("Y"));
                r.Put("DATUM", 4);
                r.Put("CREATED_BY", IM.ConnectedUser());
                r.Put("DATE_CREATED", DateTime.Now);
                r.Put("CITY_ID", _addressCtrl.CurrentCity.Id);
                r.Put("COUNTRY_ID", "UKR");
                r.Put("ASL", 0);
                //-----------------------------
                //Созадем адрес
                string adr = GetShortAddress();
                //-----------------------------
                // Создаем полный адрес
                string fullAdr = GetFullAddress();

                r.Put("NAME", fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length) + id);
                r.Put("ADDRESS", adr);
                r.Put("PROVINCE", _addressCtrl.CurrentCity.prov);
                //r.Put("SUBPROVINCE", _addressCtrl.CurrentCity.subprov);
                r.Put("CITY", _addressCtrl.CurrentCity.cityName);
                r.Put("CUST_TXT1", _addressCtrl.CurrentCity.type);
                r.Put("REMARK", fullAdr);
                r.Put("CUST_TXT4", cbTypeStreet.Text);
                r.Put("CUST_TXT3", tbStreet.Text);
                r.Put("CUST_TXT8", tbHouse.Text);
                r.Put("CUST_TXT7", tbNote.Text);
                r.Put("STATUS", "Requ");
                r.Update();
            }
            finally
            {
                r.Final();
            }
            return id;
        }

        //===================================================
        public PositionState getNewPositionState(string tableName, double X, double Y)
        {
            PositionState newPos = new PositionState();            
            int ID_pos = IM.AllocID(tableName, 1, -1);
            newPos.Id= ID_pos;            
            newPos.TableName = tableName;
            newPos.Csys = "4DMS";
            newPos.LonDms = X.DmsToDec();
            newPos.X = X;
            newPos.LatDms = Y.DmsToDec();
            newPos.Y = Y;                        
            newPos.CityId = _addressCtrl.CurrentCity.Id;
            newPos.CountryId = "UKR";
            newPos.ASl= 0;
            //-----------------------------
            //Созадем адрес
            string adr = GetShortAddress();
            //-----------------------------
            // Создаем полный адрес
            string fullAdr = GetFullAddress();

            newPos.Name = fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length) + ID_pos.ToString();
            newPos.Address = adr;
            newPos.Province = _addressCtrl.CurrentCity.prov;
            newPos.Subprovince = _addressCtrl.CurrentCity.subprov;
            newPos.City = _addressCtrl.CurrentCity.cityName;
            newPos.TypeCity = _addressCtrl.CurrentCity.type;
            newPos.Remark= fullAdr;
            newPos.StreetType = cbTypeStreet.Text;
            newPos.StreetName = cbTypeStreet.Text; 
            return newPos;
        }

        //===================================================
        public IMObject getNewXnrfaPosition(double X, double Y)
        {
            IMObject objPosition = null;
            try
            {
                objPosition = IMObject.New(ICSMTbl.itblPositionBro); //TODO Якась ХЕРНЯ
                objPosition.Put("LONGITUDE", X);
                objPosition.Put("LATITUDE", Y);
                objPosition.Put("CREATED_BY", IM.ConnectedUser());
                objPosition.Put("DATE_CREATED", DateTime.Now);
                objPosition.Put("CITY_ID", _addressCtrl.CurrentCity.Id);
                //-----------------------------
                //Создаем адресс
                string adr = GetShortAddress();
                //-----------------------------
                // Создаем полный адрес
                string fullAdr = GetFullAddress();
                objPosition.Put("NAME", fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length));
                objPosition.Put("ADDRESS", adr);
                objPosition.Put("PROVINCE", _addressCtrl.CurrentCity.prov);
                objPosition.Put("SUBPROVINCE", _addressCtrl.CurrentCity.subprov);
                objPosition.Put("CITY", _addressCtrl.CurrentCity.cityName);
                objPosition.Put("CUST_TXT1", _addressCtrl.CurrentCity.type);
                objPosition.Put("REMARK", fullAdr);
                objPosition.Put("CUST_TXT4", cbTypeStreet.Text);
                objPosition.Put("CUST_TXT3", tbStreet.Text);
                objPosition.Put("CUST_TXT8", tbHouse.Text);
                objPosition.Put("CUST_TXT7", tbNote.Text);
            }
            catch (IMException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return objPosition;
        }


        //===================================================
        /// <summary>
        /// Созадаем полный адрес
        /// </summary>
        /// <returns>полный адрес</returns>
        public string GetFullAddress()
        {
            string fullAdr = "";
            string addr = GetShortAddress();

            fullAdr = _addressCtrl.GetFullAddress();
            
            if (fullAdr != "" && addr != "")
                fullAdr += ", ";
            fullAdr += addr;
            return fullAdr;
        }
        //===================================================
        /// <summary>
        /// Созадем короткий адрес
        /// </summary>
        /// <returns>короткий адрес</returns>
        public string GetShortAddress()
        {
            //Созадем адрес
            string adr = "";
            if (cbTypeStreet.Text != "")
                adr += cbTypeStreet.Text + " " + tbStreet.Text;
            if (tbHouse.Text != "")
            {
                if (adr != "")
                    adr += ", ";
                adr += tbHouse.Text;
            }
            if (tbNote.Text != "")
            {
                if (adr != "")
                    adr += ", ";
                adr += tbNote.Text;
            }
            return adr;
        }       
        
        //===================================================
        public string getProvince()
        {
            return _addressCtrl.GetProvinceText();
        }
        //===================================================
        public string getCity()
        {
            return _addressCtrl.GetCity();
        }

        private void FormNewPosition_Load(object sender, EventArgs e)
        {
            //-----
            
            tbLatitude.Text = _latDms.LatDMSToString();
            tbLongitude.Text = _longDms.LongDMSToString();
            //-----
            lblAttachPhoto.DataBindings.Add("Enabled", this, "CanAttachPhoto");
            lblAttachPhoto.DataBindings.Add("Visible", this, "CanAttachPhoto");
            tbAttachPhoto.DataBindings.Add("Enabled", this, "CanAttachPhoto");
            tbAttachPhoto.DataBindings.Add("Visible", this, "CanAttachPhoto");
            btnFind.DataBindings.Add("Enabled", this, "CanAttachPhoto");
            btnFind.DataBindings.Add("Visible", this, "CanAttachPhoto");


            _addressCtrl.DataBindings.Add("CanEditPosition", this, "CanEditPosition");
            buttonOk.DataBindings.Add("Enabled", _addressCtrl, "IsValid");
            //-----
            //cbProvince.DataBindings.Add("Enabled", this, "CanEditPosition");
            //cbSubprovince.DataBindings.Add("Enabled", this, "CanEditPosition");
            //cbTypeCity.DataBindings.Add("Enabled", this, "CanEditPosition");
            //cbCity.DataBindings.Add("Enabled", this, "CanEditPosition");
            cbTypeStreet.DataBindings.Add("Enabled", this, "CanEditStreet");
            tbStreet.DataBindings.Add("Enabled", this, "CanEditStreet");
            tbHouse.DataBindings.Add("Enabled", this, "CanEditStreet");
            tbNote.DataBindings.Add("Enabled", this, "CanEditStreet");                        
            //-----
            if (CanAttachPhoto == true)
                buttonOk.Enabled = true;
            //-----
            if(OldPosition != null)
            {
                cbTypeStreet.Text = OldPosition.StreetType;
                tbStreet.Text = OldPosition.StreetName;
                tbHouse.Text = OldPosition.BuildingNumber;
                tbNote.Text = OldPosition.Remark;

                Text += "(" + OldPosition.TableName + "," + OldPosition.Id + ")";
            }
        }
        /// <summary>
        /// Функция копирования файлов в каталог, который указан в Plugin's settings
        /// </summary>
        /// <param name="listDst">список результирующих путей для файлов</param>
        /// <param name="listSrc">список входных путей для файлов</param>
        public void GetFotos(out List<string> listDst, out List<string> listSrc)
        {
            //Считывание названия каталога из БД куда необходимо занести информацию о фото
            //string fotoDstFolder = HelpFunction.ReadDataFromSysConfig("PLG_FOLDER_SELECTION_FOTO");
            string fotoDstFolder = HelpFunction.ReadDataFromSysConfig("SHDIR-DOC");
            FileInfo fi;

            List<string> listTempDst = new List<string>();
            if (ListAttachFile != null)
                foreach (string fotos in ListAttachFile)
                {
                    fi = new FileInfo(fotos);
                    if (fi.Exists)
                        listTempDst.Add(fotoDstFolder + @"\" + fi.Name);
                }
            listDst = listTempDst;
            listSrc=new List<string>();
            if (ListAttachFile != null)
                listSrc = ListAttachFile.ToList();
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            //_addressCtrl.OnSelectCity();
        }

        /// <summary>
        /// Устанавливает долготу
        /// </summary>
        /// <param name="longDms">координата в формате DMS</param>
        public void SetLongitudeAsDms(double longDms)
        {
            _longDms = longDms;
        }
        /// <summary>
        /// Устанавливает широту
        /// </summary>
        /// <param name="latDms">координата в формате DMS</param>
        public void SetLatitudeAsDms(double latDms)
        {
            _latDms = latDms;
        }
        /// <summary>
        /// Устанавливает долготу
        /// </summary>
        /// <param name="longDec">координата в формате DEC</param>
        public void SetLongitudeAsDec(double longDec)
        {
            SetLongitudeAsDms(longDec.DecToDms());
        }
        /// <summary>
        /// Устанавливает широту
        /// </summary>
        /// <param name="latDec">координата в формате DEC</param>
        public void SetLatitudeAsDec(double latDec)
        {
            SetLatitudeAsDms(latDec.DecToDms());
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            bool isRepeat = false;
            do
            {
                isRepeat = false;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.SafeFileNames.Length > 5)
                    {
                        MessageBox.Show(CLocaliz.TxT("You can't select more than 5 files."), "", MessageBoxButtons.OK,
                                        MessageBoxIcon.Stop);
                        isRepeat = true;
                    }
                    else
                    {
                        _listFiles = openFileDialog.FileNames;
                        string strFhoto = "";
                        foreach (string s in openFileDialog.SafeFileNames)
                        {
                            if (strFhoto != "")
                                strFhoto += "; ";
                            strFhoto += s;
                        }
                        tbAttachPhoto.Text = strFhoto;
                    }
                }
            }
            while (isRepeat);
        }
    }
}