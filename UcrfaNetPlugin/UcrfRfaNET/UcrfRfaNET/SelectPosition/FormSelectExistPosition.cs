﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ICSM;
using NSPosition;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.UtilityClass;
using XICSM.UcrfRfaNET.Icsm;
using XICSM.UcrfRfaNET.HelpClasses.Forms;


namespace XICSM.UcrfRfaNET
{
    internal partial class FormSelectExistPosition : FBaseForm
    {
        public class PositionRowBase : NotifyPropertyChanged
        {
            //----
            private int _id = 0;
            private int _city_id = 0;
            private bool _is_check = false;
            private string _streetName = "";
            private string _streetType = "";
            private string _codeKoatuu = "";
            /// <summary>
            /// Id
            /// </summary>
            public int Id
            {
                get { return _id; }
                set
                {
                    if (value != _id)
                    {
                        _id = value;
                        InvokeNotifyPropertyChanged("Id");
                    }
                }
            }

            /// <summary>
            /// Is check
            /// </summary>
            public bool IsCheck
            {
                get { return _is_check; }
                set
                {
                    if (value != _is_check)
                    {
                        _is_check = value;
                        InvokeNotifyPropertyChanged("IsCheck");
                    }
                }
            }
            /// <summary>
            ///  Street Name
            /// </summary>
            public string StreetName
            {
                get { return _streetName; }
                set
                {
                    if (value != _streetName)
                    {
                        _streetName = value;
                        InvokeNotifyPropertyChanged("StreetName");
                    }
                }
            }

            /// <summary>
            ///  Street Type
            /// </summary>
            public string StreetType
            {
                get { return _streetType; }
                set
                {
                    if (value != _streetType)
                    {
                        _streetType = value;
                        InvokeNotifyPropertyChanged("StreetType");
                    }
                }
            }


            /// <summary>
            ///  Code Koatuu
            /// </summary>
            public string CodeKoatuu
            {
                get { return _codeKoatuu; }
                set
                {
                    if (value != _codeKoatuu)
                    {
                        _codeKoatuu = value;
                        InvokeNotifyPropertyChanged("CodeKoatuu");
                    }
                }
            }
            /// <summary>
            /// City id
            /// </summary>
            public int City_Id
            {
                get { return _city_id; }
                set
                {
                    if (value != _city_id)
                    {
                        _city_id = value;
                        InvokeNotifyPropertyChanged("City_Id");
                    }
                }
            }


            //----
            private string _tableName = "";
            /// <summary>
            /// Название таблицы
            /// </summary>
            public string TableName
            {
                get { return _tableName; }
                set
                {
                    if (value != _tableName)
                    {
                        _tableName = value;
                        InvokeNotifyPropertyChanged("TableName");
                        InvokeNotifyPropertyChanged("LocalizTableName");
                    }
                }
            }
            /// <summary>
            /// Локализированое Название таблицы
            /// </summary>
            public string LocalizTableName
            {
                get { return CLocaliz.TxT(TableName); }
            }
            //----
            private double _distance = 0.0;
            /// <summary>
            /// Растояние
            /// </summary>
            public double Distance
            {
                get { return _distance.Round(4); }
                set
                {
                    if (value != _distance)
                    {
                        _distance = value;
                        InvokeNotifyPropertyChanged("Distance");
                    }
                }
            }
            //----
            private double _longitudeDms = 0.0;
            /// <summary>
            /// Долгота
            /// </summary>
            public double LongitudeDms
            {
                get { return _longitudeDms; }
                set
                {
                    if (value != _longitudeDms)
                    {
                        _longitudeDms = value;
                        InvokeNotifyPropertyChanged("LongitudeDms");
                        InvokeNotifyPropertyChanged("LongitudeDmsText");
                    }
                }
            }
            /// <summary>
            /// Долгота текст
            /// </summary>
            public string LongitudeDmsText
            {
                get { return HelpFunction.DmsToString(LongitudeDms, EnumCoordLine.Lon); }
            }
            //----
            private double _latitudeDms = 0.0;
            /// <summary>
            /// Долгота
            /// </summary>
            public double LatitudeDms
            {
                get { return _latitudeDms; }
                set
                {
                    if (value != _latitudeDms)
                    {
                        _latitudeDms = value;
                        InvokeNotifyPropertyChanged("LatitudeDms");
                        InvokeNotifyPropertyChanged("LatitudeDmsText");
                    }
                }
            }
            /// <summary>
            /// Долгота текст
            /// </summary>
            public string LatitudeDmsText
            {
                get { return HelpFunction.DmsToString(LatitudeDms, EnumCoordLine.Lat); }
            }
            //----
            private bool _isKoatuu = false;
            /// <summary>
            /// Долгота
            /// </summary>
            public bool IsKoatuu
            {
                get { return _isKoatuu; }
                set
                {
                    if (value != _isKoatuu)
                    {
                        _isKoatuu = value;
                        InvokeNotifyPropertyChanged("IsKoatuu");
                    }
                }
            }
            //----
            private string _remark = "";
            /// <summary>
            /// Долгота
            /// </summary>
            public string Remark
            {
                get { return _remark; }
                set
                {
                    if (value != _remark)
                    {
                        _remark = value;
                        InvokeNotifyPropertyChanged("Remark");
                    }
                }
            }

            //----
            private string _ownername = "";
            /// <summary>
            /// Долгота
            /// </summary>
            public string OwnerName
            {
                get { return _ownername; }
                set
                {
                    if (value != _ownername)
                    {
                        _ownername = value;
                        InvokeNotifyPropertyChanged("OwnerName");
                    }
                }
            }
            //----
            private string _status = "";
            /// <summary>
            /// Долгота
            /// </summary>
            public string Status
            {
                get { return _status; }
                set
                {
                    if (value != _status)
                    {
                        _status = value;
                        InvokeNotifyPropertyChanged("Status");
                    }
                }
            }

            public string StatusReadable { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public DateTime ModifiedDate { get; set; }

            //----
            private Color _backGroundColor = Color.White;
            /// <summary>
            /// Долгота
            /// </summary>
            public Color BackGroundColor
            {
                get { return _backGroundColor; }
                set
                {
                    if (value != _backGroundColor)
                    {
                        _backGroundColor = value;
                        InvokeNotifyPropertyChanged("BackGroundColor");
                    }
                }
            }

        }
        //===================================================
        /// <summary>
        /// Отображает окно для выбора сайта для таблиц XFA_...
        /// </summary>
        /// <param name="ownerWind">Владелец окна</param>
        /// <param name="table">Имя таблицы, которая должна ити после SITE</param>
        /// <param name="radius">радиус поиска</param>
        /// <param name="lon">долгота</param>
        /// <param name="lat">широта</param>
        public static PositionState2 SelectPositionState(IWin32Window ownerWind, string table, double radius, double lon, double lat)
        {
            using (FormSelectExistPosition frm = new FormSelectExistPosition(table, radius, lon, lat))
            {
                frm.ShowDialog(ownerWind);
                if (IsPressOk)
                {
                    PositionState2 retObj = frm._posState;
                    return retObj;
                }
            }
            return null;
        }

        //===================================================
        //===================================================
        //===================================================
        //===================================================
        private string _table = "";
        private double _lonDms;
        private double _latDms;
        public static bool IsPressOk;
        private PositionState2 _posState;
        private static double _radius = IM.NullD;
        protected HelpClasses.Classes.SortableBindingList<PositionRowBase> PosListRows = new HelpClasses.Classes.SortableBindingList<PositionRowBase>();
        //-----
        /// <summary>
        /// Долгота DEC
        /// </summary>
        private double LongDec
        {
            get { return _lonDms.DmsToDec(); }
        }
        /// <summary>
        /// Широта DEC
        /// </summary>
        private double LatDec
        {
            get { return _latDms.DmsToDec(); }
        }
        /// <summary>
        /// Долгота DMS
        /// </summary>
        private double LongDms
        {
            get { return _lonDms; }
            set { _lonDms = value; }
        }
        /// <summary>
        /// Широта DMS
        /// </summary>
        private double LatDms
        {
            get { return _latDms; }
            set { _latDms = value; }
        }


        protected void ScanCity()
        {
            foreach (PositionRowBase bs in PosListRows)
            {
                IMObject rsSite = null;

                try
                {
                    rsSite = IMObject.LoadFromDB(bs.TableName, bs.Id);
                }
                catch
                {
                    rsSite = null;
                }

                if (rsSite != null)
                {
                    bs.StreetName = rsSite.GetS("CUST_TXT3");
                    bs.StreetType = rsSite.GetS("CUST_TXT4");
                }

                IMObject rsCity = null;
                try
                {
                    rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, bs.City_Id);
                }
                catch
                {
                    rsCity = null;
                }
                if (rsCity != null)
                {
                      
                    bs.CodeKoatuu = rsCity.GetS("CODE");  
                    if (rsCity.GetS("ADM_CODE").Contains("_"))
                    {
                        bs.IsCheck = true;
                    }
                }
            }
        }

        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        protected FormSelectExistPosition(string table, double radius, double lon, double lat)
        {
            if ((table == ICSMTbl.itblAdmSite) || (table == ICSMTbl.Site))
                throw new ArgumentException(string.Format("Argument 'table' is not validtable: {0}", table));
            InitializeComponent();
            dataGridView.AutoGenerateColumns = false;
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            // DataGridView

            CLocaliz.TxT(this);
            this._table = table;
            if (_radius == IM.NullD)
                _radius = radius;
            tbRadius_TextChanged(null, null);
            this._lonDms = lon; tbLon_TextChanged(null, null);
            this._latDms = lat; tbLat_TextChanged(null, null);
            // Столбцы грида
            // ID
            // DISTANCE
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "lisDISTANCE";
            column.HeaderText = CLocaliz.TxT("DISTANCE");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "Distance";
            dataGridView.Columns.Add(column);
            // TABLE_NAME
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisTABLE_NAME";
            column.HeaderText = CLocaliz.TxT("TABLE_NAME");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.Visible = false;
            column.DataPropertyName = "TableName";
            dataGridView.Columns.Add(column);

            // TABLE_NAME_LOCALIZ
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisTABLE_NAME_LOCALIZ";
            column.HeaderText = CLocaliz.TxT("TABLE_NAME");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "LocalizTableName";
            dataGridView.Columns.Add(column);

            // LONGITUDE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisLONGITUDE";
            column.HeaderText = CLocaliz.TxT("LONGITUDE");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "LongitudeDmsText";
            dataGridView.Columns.Add(column);
            // LATITUDE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisLATITUDE";
            column.HeaderText = CLocaliz.TxT("LATITUDE");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            column.DataPropertyName = "LatitudeDmsText";
            dataGridView.Columns.Add(column);
            // REMARK
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisREMARK";
            column.HeaderText = CLocaliz.TxT("REMARK");
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "Remark";
            dataGridView.Columns.Add(column);

            // REMARK
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisOWNERNAME";
            column.HeaderText = CLocaliz.TxT("Owner");
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "OwnerName";
            dataGridView.Columns.Add(column);

            // Status
            column = new DataGridViewTextBoxColumn();
            column.Name = "Status";
            column.HeaderText = "Статус";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "Status";
            dataGridView.Columns.Add(column);

            // Status readable
            /*
            column = new DataGridViewTextBoxColumn();
            column.Name = "Status";
            column.HeaderText = CLocaliz.TxT("Status");
            column.Width = 80;
            column.DataPropertyName = "Status";
            dataGridView.Columns.Add(column);
            */

            // ModifiedDate
            column = new DataGridViewTextBoxColumn();
            column.Name = "ModifiedDate";
            column.HeaderText = "Дата ост. зміни";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "ModifiedDate";
            dataGridView.Columns.Add(column);

            // ModifiedBy
            column = new DataGridViewTextBoxColumn();
            column.Name = "ModifiedBy";
            column.HeaderText = "Останнім змінив";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "ModifiedBy";
            dataGridView.Columns.Add(column);

            // CreatedDate
            column = new DataGridViewTextBoxColumn();
            column.Name = "CreatedDate";
            column.HeaderText = "Дата створення";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "CreatedDate";
            dataGridView.Columns.Add(column);

            // Created By
            column = new DataGridViewTextBoxColumn();
            column.Name = "CreatedBy";
            column.HeaderText = "Створив";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "CreatedBy";
            dataGridView.Columns.Add(column);



            // ID_KOATUU
            {
                DataGridViewCheckBoxColumn columnCb = new DataGridViewCheckBoxColumn();
                columnCb.Name = "lisID_KOATUU";
                columnCb.HeaderText = "КОАТУУ";
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                columnCb.ReadOnly = true;
                columnCb.DataPropertyName = "IsKoatuu";
                dataGridView.Columns.Add(columnCb);
            }

            // ID
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisID";
            column.HeaderText = "ID";
            column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            column.DataPropertyName = "Id";
            dataGridView.Columns.Add(column);



            dataGridView.DataSource = null;
            dataGridView.DataSource = PosListRows;
            // Обновляем запрос
            UpdateRow();

        }
        //===================================================
        /// <summary>
        /// Изменилась долгота
        /// </summary>
        private void tbLon_TextChanged(object sender, EventArgs e)
        {
            double tmpLon = LongDms;
            LongDms = ConvertType.ToDouble(tbLon.Text, tmpLon);
            tbLon.Text = LongDms.ToString();
            labelDECLon.Text = HelpFunction.DmsToString(LongDms, EnumCoordLine.Lon);
            if (!HelpFunction.ValidateCoords(LongDms, EnumCoordLine.Lon))
                tbLon.BackColor = Color.Red;
            else
                tbLon.BackColor = SystemColors.Window;
        }
        //===================================================
        /// <summary>
        /// Изменилась широта
        /// </summary>
        private void tbLat_TextChanged(object sender, EventArgs e)
        {
            double tmpLat = LatDms;
            LatDms = ConvertType.ToDouble(tbLat.Text, tmpLat);
            tbLat.Text = LatDms.ToString();
            labelDECLat.Text = HelpFunction.DmsToString(LatDms, EnumCoordLine.Lat);
            if (!HelpFunction.ValidateCoords(LatDms, EnumCoordLine.Lat))
                tbLat.BackColor = Color.Red;
            else
                tbLat.BackColor = SystemColors.Window;
        }
        //===================================================
        /// <summary>
        /// Изменился радиус
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbRadius_TextChanged(object sender, EventArgs e)
        {
            double tmpRadius = _radius;
            _radius = ConvertType.ToDouble(tbRadius.Text, tmpRadius);
            tbRadius.Text = _radius.ToString();
        }
        //==================================================
        /// <summary>
        /// Обновить запрос
        /// </summary>
        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateRow();
        }
        //==================================================
        /// <summary>
        /// Обновляет данные в таблице
        /// </summary>
        private void UpdateRow()
        {
            string tableFirst = _table;
            PosListRows.Clear();
            // Выбираем записи из таблицы SITES
            GetPositionRows(ICSMTbl.itblAdmSite);
            tbRadius.ReadOnly = true;
            if (tableFirst != "SITES")
            {
                tbRadius.ReadOnly = false;
                // Выбираем записи из таблицы SITE
                GetPositionRows(ICSMTbl.itblSite);
                // Добавляем все остальные Position
                string[] listPosTable = new string[] { };
                if (tableFirst == ICSMTbl.itblPositionEs)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionBro)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionFmn)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionHf)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionMob2)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionMw)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionWim)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionWim,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw
                                       };
                else if (tableFirst == PlugTbl.XfaPosition)
                    listPosTable = new[]
                                       {
                                           PlugTbl.XfaPosition,
                                           ICSMTbl.itblPositionWim,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw
                                       };

                // Добавляем новые Position
                foreach (string tableName in listPosTable)
                    GetPositionRows(tableName);
            }
        }
        //==================================================
        /// <summary>
        /// Возвразает список строк для грида в уже нормальном виде
        /// </summary>
        /// <param name="tablePosition">имя таблицы (POSITION_XX)</param>
        private void GetPositionRows(string tablePosition)
        {
            RecordPos positionRadius = Position.GetRangeCoordinates(LongDec, LatDec, _radius, "", "");
            using (LisProgressBar pb = new LisProgressBar(tablePosition))
            {
                pb.SetBig(tablePosition);
                pb.SetProgress(0, 500);
                PositionState2 pos = new PositionState2();
                using (Icsm.LisRecordSet rsPosition = new Icsm.LisRecordSet(tablePosition, IMRecordset.Mode.ReadOnly))
                {
                    string selectParamPostitions = "ID,LONGITUDE,LATITUDE,REMARK,CITY_ID,X,Y,CREATED_BY,DATE_CREATED,MODIFIED_BY,DATE_MODIFIED";
                    selectParamPostitions += tablePosition == ICSMTbl.SITES ? ",STATUS" : ",ADMS_ID";
                    if (tablePosition == ICSMTbl.itblSite)
                        selectParamPostitions += ",TOWER";
                    if ((tablePosition == ICSMTbl.SITES) || (tablePosition == ICSMTbl.Site) || (tablePosition == ICSMTbl.itblPositionEs) || (tablePosition == ICSMTbl.itblPositionBro) || (tablePosition == ICSMTbl.itblPositionFmn) || (tablePosition == ICSMTbl.itblPositionHf) || (tablePosition == ICSMTbl.itblPositionMob2) || (tablePosition == ICSMTbl.itblPositionMw) || (tablePosition == ICSMTbl.itblPositionWim))
                        selectParamPostitions += ",CUST_TXT6";
                    rsPosition.Select(selectParamPostitions);
                    rsPosition.SetWhere("LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                    rsPosition.SetWhere("LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                    rsPosition.SetWhere("LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                    rsPosition.SetWhere("LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);

                    rsPosition.OrderBy("LONGITUDE", OrderDirection.Ascending);
                    rsPosition.OrderBy("LATITUDE", OrderDirection.Ascending);
                    if (tablePosition == ICSMTbl.SITES)
                        rsPosition.OrderBy("STATUS", OrderDirection.Ascending);
                    List<PositionRowBase> tmpList = new List<PositionRowBase>();
                    for (rsPosition.Open(); !rsPosition.IsEOF(); rsPosition.MoveNext())
                    {
                        if (pb.UserCanceled())
                            continue;
                        double findStationLon = rsPosition.GetD("LONGITUDE");
                        double findStationLat = rsPosition.GetD("LATITUDE");

                        bool isKoatuu = false;
                        if (rsPosition.GetI("CITY_ID") != IM.NullI)
                            isKoatuu = true;
                        Color colorSite = Color.White;

                        double distance = IMPosition.Distance(new IMPosition(findStationLon, findStationLat, "4DEC"), new IMPosition(LongDms, LatDms, "4DMS"));

                        if (distance <= _radius)
                        {
                            pb.Increment(true);
                            //Перевірка на станцію моніторинга
                            if (tablePosition == ICSMTbl.itblSite)
                            {
                                string findStationType = rsPosition.GetS("TOWER");
                                if (findStationType == "РКП/АСРМ" && distance <= 0.5)
                                    colorSite = Color.Pink;
                            }
                            if (tablePosition == ICSMTbl.SITES)
                            {
                                string status = rsPosition.GetS("STATUS");
                                if (status == "CHKD")
                                    colorSite = Color.LightGreen;
                            }
                            else //if (_tableName!=table)
                            {
                                if (rsPosition.GetI("ADMS_ID") != IM.NullI)
                                    continue;
                            }

                            string statusGrid = "";
                            string statusReadable = "";



                            if (tablePosition == ICSMTbl.SITES)
                            {
                                statusGrid = rsPosition.GetS("STATUS");
                                //statusReadable = 
                            }

                            int idPos = rsPosition.GetI("ID");



                            string NAME_OWN = "";
                            NAME_OWN = (tablePosition == PlugTbl.XfaPosition ? "" : rsPosition.GetS("CUST_TXT6"));



                            pos.LoadStatePosition(idPos, tablePosition);

                            tmpList.Add(new PositionRowBase
                            {
                                Id = idPos,
                                City_Id = rsPosition.GetI("CITY_ID"),
                                TableName = tablePosition,
                                Distance = distance,
                                IsKoatuu = isKoatuu,
                                LongitudeDms = findStationLon.DecToDms(),
                                LatitudeDms = findStationLat.DecToDms(),
                                Remark = pos.FullAddressAuto,
                                //OwnerName = ((tablePosition == PlugTbl.XfaPosition) ? pos.NAME_OWNER : rsPosition.GetS("CUST_TXT6")),
                                OwnerName = NAME_OWN,
                                Status = statusGrid,
                                BackGroundColor = colorSite,
                                StatusReadable = statusReadable,
                                CreatedBy = rsPosition.GetS("CREATED_BY"),
                                CreatedDate = rsPosition.GetT("DATE_CREATED"),
                                ModifiedBy = rsPosition.GetS("MODIFIED_BY"),
                                ModifiedDate = rsPosition.GetT("DATE_MODIFIED")
                            });
                        }
                    }
                    tmpList.Sort(CompareRowByDistance);
                    foreach (PositionRowBase rowBase in tmpList)
                        PosListRows.Add(rowBase);
                }
                ScanCity();
            }
            
        }
        /// <summary>
        /// Сортировка по дистанции
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private static int CompareRowByDistance(PositionRowBase x, PositionRowBase y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're
                    // equal. 
                    return 0;
                }
                else
                {
                    // If x is null and y is not null, y
                    // is greater. 
                    return -1;
                }
            }
            else
            {
                // If x is not null...
                //
                if (y == null)
                // ...and y is null, x is greater.
                {
                    return 1;
                }
                else
                {
                    if (x.Distance < y.Distance)
                        return -1;
                    return 1;
                }
            }
        }
        //==================================================
        /// <summary>
        /// Ищет сайты, которые ссылаються на андминистратывные сайты
        /// </summary>
        /// <param name="table">в какой таблице искать</param>
        /// <param name="admId">ID административного сайта</param>
        /// <returns>id сайта</returns>
        private int FindSiteToAdmSite(string table, int admId)
        {
            int retId = IM.NullI;
            if (table != ICSMTbl.SITES)
            {
                using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(table, IMRecordset.Mode.ReadOnly))
                {
                    rs.Select("ID,ADMS_ID");
                    rs.SetWhere("ADMS_ID", IMRecordset.Operation.Eq, admId);
                    rs.Open();
                    if (!rs.IsEOF())
                        retId = rs.GetI("ID");
                }
            }
            return retId;
        }
        //===================================================
        /// <summary>
        /// Выбрали станцию
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            IsPressOk = true;
            GetNewPosition();
        }
        //===================================================
        /// <summary>
        /// Отменить выбор
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            IsPressOk = false;
            _posState = null;
            Close();
        }
        //===================================================
        /// <summary>
        /// Отобразим сайт
        /// </summary>
        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            IsPressOk = true;
            GetNewPosition();
        }
        //===================================================
        /// <summary>
        /// Вытаскавает выбраную запись и создает новую позицию, если необходимо.
        /// Закрывает форму, если все ОК
        /// </summary>
        private void GetNewPosition()
        {
            if (dataGridView.CurrentCell == null)
                return;
            int rowIndex = dataGridView.CurrentCell.RowIndex;
            PositionRowBase row = null;

            if ((rowIndex < PosListRows.Count) && (rowIndex > -1))
                row = PosListRows[rowIndex];

            if (row == null)
            {
                MessageBox.Show("Can't find selected position", "Error");
                return;
            }
            if (row.IsKoatuu == false)
            {
                if (MessageBox.Show(CLocaliz.TxT("Selected station isn't connected to KOATUU. Continue?"), CLocaliz.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
            }

            string tableName = row.TableName;
            int id = row.Id;

            if ((id == 0) || (tableName == ""))
                return;

            if (tableName == ICSMTbl.itblAdmSite)
            {
                //Выбор админ сайта
                //Поиск сайта в необходимой радиослужбе
                int foundId = FindSiteToAdmSite(_table, id);
                if (foundId != IM.NullI)
                    id = foundId;
                else
                {
                    //Сайта в службе нет
                    //Создадим его
                    PositionState2 posTmp = new PositionState2();
                    posTmp.LoadStatePosition(id, tableName); //Загружаем выбраный сайт
                    posTmp.CopyToTable(_table);
                    posTmp.AdminSiteId = id; //Устанавливает ID админ сайта
                    posTmp.Save(0); //Сохраняем сайт
                    id = posTmp.Id;
                }
            }
            else
            {
                //Выбрали сайт
                PositionState2 posTmp = new PositionState2();
                posTmp.LoadStatePosition(id, tableName); //Загружаем выбраный сайт
                if (tableName == _table)
                {
                    //выбрали сайт текущей службы
                    //if(posTmp.CreateAdminSite())  //25-02-2013 - Мырзаков - выключить
                    //    posTmp.Save();  //Админ сайт был создан
                    //id - остаеться прежним
                }
                else
                {
                    posTmp.CopyToTable(_table);
                    //posTmp.CreateAdminSite();  //Создаем админ сайт, если необходимо   //25-02-2013 - Мырзаков - выключить
                    posTmp.Save(0);         //Записаваем сайт в БД
                    id = posTmp.Id;
                }
            }
            _posState = null;
            if (PositionState2.IsRecordExist(id, _table))
            {
                _posState = new PositionState2();
                _posState.LoadStatePosition(id, _table);
            }
            if (_posState == null)
                MessageBox.Show(CLocaliz.TxT("Can't find selected position"), CLocaliz.Error);
            else
                Close();
        }

        //===================================================
        /// <summary>
        /// Нажатие кнопки на сриде
        /// </summary>
        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (dataGridView.CurrentCell != null))
            {
                int rowIndex = dataGridView.CurrentCell.RowIndex;
                PositionRowBase row = null;

                if ((rowIndex < PosListRows.Count) && (rowIndex > -1))
                    row = PosListRows[rowIndex];

                try
                {
                    if (row != null)
                    {
                        ApplSource.Forms.AdminSiteAllTech2.Show(row.TableName, row.Id, (IM.TableRight(row.TableName) & IMTableRight.Update) != IMTableRight.Update, this);
                        //position may appear being changed
                        PositionState2 pos = new PositionState2();
                        pos.LoadStatePosition(row.Id, row.TableName);
                        row.Remark = pos.FullAddressAuto;
                        row.LongitudeDms = pos.LongDms;
                        row.LatitudeDms = pos.LatDms;
                        dataGridView.Refresh();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace, CLocaliz.Error);
                }
            }
        }

        private void dataGridView_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if ((-1 < e.RowIndex) && (e.RowIndex < PosListRows.Count))
            {
                dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = PosListRows[e.RowIndex].BackGroundColor;
                if (StreetChangeForm.Load(PosListRows[e.RowIndex].StreetType, PosListRows[e.RowIndex].StreetName, PosListRows[e.RowIndex].CodeKoatuu))
                {
                    dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(236, 183, 0);
                }
                if (PosListRows[e.RowIndex].IsCheck) dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Aqua;//System.Drawing.Color.FromArgb(236, 183, 0);
                
            }
        }

        private void Custom_Resize()
        {
            try
            {
                // DataGridView
                double[] WeightKoef = new double[14];
               /* WeightKoef[0] = 4.62F;
                WeightKoef[1] = 6.87F;
                WeightKoef[2] = 6.87F;
                WeightKoef[3] = 5.48F;
                WeightKoef[4] = 5.48F;
                WeightKoef[5] = 28.4F;
                WeightKoef[6] = 7.36F;
                WeightKoef[7] = 3.7F;
                WeightKoef[8] = 7.33F;
                WeightKoef[9] = 5.55F;
                WeightKoef[10] = 6.47F;
                WeightKoef[11] = 6.47F;
                WeightKoef[12] = 5.09F;
                WeightKoef[13] = 3.7F;
                */
                WeightKoef[6] = 94;
                WeightKoef[7] = 56;
                WeightKoef[8] = 84;
                WeightKoef[9] = 80;
                WeightKoef[10] = 77;
                WeightKoef[11] = 81;
                WeightKoef[12] = 56;
                WeightKoef[13] = 56;
                


                dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

                for (int i = 0; i < 6; i++)
                {
                    dataGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    WeightKoef[i]=dataGridView.Columns[i].Width;
                }

                for (int i = 6; i < dataGridView.Columns.Count; i++)
                {
                    dataGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                }


                for (int i = 0; i < dataGridView.Columns.Count; i++)
                {
                   
                    dataGridView.Columns[i].Width = (int)(WeightKoef[i]);
                }

                /*
                for (int i = 0; i < dataGridView.Columns.Count; i++)
                {
                    int colw = (int)(((double)WeightKoef[i] * dataGridView.Width) / 100);
                    dataGridView.Columns[i].Width = colw;
                }
                 */ 

                dataGridView.AllowUserToOrderColumns = true;
                dataGridView.AllowUserToResizeColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка в обработчике OnResize формы: " + ex.Message);
            }
        }

        private void FormSelectExistPosition_Resize(object sender, EventArgs e)
        {
            Custom_Resize();

        }

        private void FormSelectExistPosition_Shown(object sender, EventArgs e)
        {
            Custom_Resize();
        }
    }
}
