﻿namespace XICSM.UcrfRfaNET
{
   partial class FormNewPosition
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.cbTypeStreet = new System.Windows.Forms.ComboBox();
          this.label5 = new System.Windows.Forms.Label();
          this.tbStreet = new System.Windows.Forms.TextBox();
          this.label6 = new System.Windows.Forms.Label();
          this.tbHouse = new System.Windows.Forms.TextBox();
          this.label7 = new System.Windows.Forms.Label();
          this.tbNote = new System.Windows.Forms.TextBox();
          this.label8 = new System.Windows.Forms.Label();
          this.buttonOk = new System.Windows.Forms.Button();
          this.buttonCancel = new System.Windows.Forms.Button();
          this.label9 = new System.Windows.Forms.Label();
          this.tbLatitude = new System.Windows.Forms.TextBox();
          this.tbLongitude = new System.Windows.Forms.TextBox();
          this.label10 = new System.Windows.Forms.Label();
          this.tbAttachPhoto = new System.Windows.Forms.TextBox();
          this.lblAttachPhoto = new System.Windows.Forms.Label();
          this.btnFind = new System.Windows.Forms.Button();
          this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
          this.panelAddress = new System.Windows.Forms.Panel();
          this.SuspendLayout();
          // 
          // cbTypeStreet
          // 
          this.cbTypeStreet.FormattingEnabled = true;
          this.cbTypeStreet.Items.AddRange(new object[] {
            "",
            "алея",
            "б-р",
            "в'їзд",
            "вул.",
            "дорога",
            "ж/м",
            "заїзд",
            "кв-л",
            "лінія",
            "майдан",
            "мкр-н",
            "наб.",
            "пл.",
            "пров.",
            "проїзд",
            "просіка",
            "пр-т",
            "роз'їзд",
            "ст.м",
            "тупик",
            "узвіз",
            "шосе"
          });
          this.cbTypeStreet.Location = new System.Drawing.Point(12, 201);
          this.cbTypeStreet.Name = "cbTypeStreet";
          this.cbTypeStreet.Size = new System.Drawing.Size(83, 21);
          this.cbTypeStreet.TabIndex = 1;
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(9, 186);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(58, 13);
          this.label5.TabIndex = 13;
          this.label5.Text = "Street type";
          // 
          // tbStreet
          // 
          this.tbStreet.Location = new System.Drawing.Point(106, 201);
          this.tbStreet.Name = "tbStreet";
          this.tbStreet.Size = new System.Drawing.Size(235, 20);
          this.tbStreet.TabIndex = 2;
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(105, 186);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(35, 13);
          this.label6.TabIndex = 14;
          this.label6.Text = "Street";
          // 
          // tbHouse
          // 
          this.tbHouse.Location = new System.Drawing.Point(347, 202);
          this.tbHouse.Name = "tbHouse";
          this.tbHouse.Size = new System.Drawing.Size(76, 20);
          this.tbHouse.TabIndex = 3;
          // 
          // label7
          // 
          this.label7.AutoSize = true;
          this.label7.Location = new System.Drawing.Point(344, 186);
          this.label7.Name = "label7";
          this.label7.Size = new System.Drawing.Size(38, 13);
          this.label7.TabIndex = 15;
          this.label7.Text = "House";
          // 
          // tbNote
          // 
          this.tbNote.Location = new System.Drawing.Point(12, 246);
          this.tbNote.Multiline = true;
          this.tbNote.Name = "tbNote";
          this.tbNote.Size = new System.Drawing.Size(411, 74);
          this.tbNote.TabIndex = 4;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(9, 231);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(30, 13);
          this.label8.TabIndex = 16;
          this.label8.Text = "Note";
          // 
          // buttonOk
          // 
          this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.buttonOk.Location = new System.Drawing.Point(267, 389);
          this.buttonOk.Name = "buttonOk";
          this.buttonOk.Size = new System.Drawing.Size(75, 23);
          this.buttonOk.TabIndex = 7;
          this.buttonOk.Text = "OK";
          this.buttonOk.UseVisualStyleBackColor = true;
          this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
          // 
          // buttonCancel
          // 
          this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.buttonCancel.Location = new System.Drawing.Point(348, 389);
          this.buttonCancel.Name = "buttonCancel";
          this.buttonCancel.Size = new System.Drawing.Size(75, 23);
          this.buttonCancel.TabIndex = 8;
          this.buttonCancel.Text = "Cancel";
          this.buttonCancel.UseVisualStyleBackColor = true;
          // 
          // label9
          // 
          this.label9.AutoSize = true;
          this.label9.Location = new System.Drawing.Point(9, 13);
          this.label9.Name = "label9";
          this.label9.Size = new System.Drawing.Size(45, 13);
          this.label9.TabIndex = 9;
          this.label9.Text = "Latitude";
          // 
          // tbLatitude
          // 
          this.tbLatitude.Enabled = false;
          this.tbLatitude.Location = new System.Drawing.Point(77, 10);
          this.tbLatitude.Name = "tbLatitude";
          this.tbLatitude.Size = new System.Drawing.Size(99, 20);
          this.tbLatitude.TabIndex = 10;
          // 
          // tbLongitude
          // 
          this.tbLongitude.Enabled = false;
          this.tbLongitude.Location = new System.Drawing.Point(324, 10);
          this.tbLongitude.Name = "tbLongitude";
          this.tbLongitude.Size = new System.Drawing.Size(99, 20);
          this.tbLongitude.TabIndex = 12;
          // 
          // label10
          // 
          this.label10.AutoSize = true;
          this.label10.Location = new System.Drawing.Point(249, 13);
          this.label10.Name = "label10";
          this.label10.Size = new System.Drawing.Size(54, 13);
          this.label10.TabIndex = 11;
          this.label10.Text = "Longitude";
          // 
          // tbAttachPhoto
          // 
          this.tbAttachPhoto.Location = new System.Drawing.Point(100, 342);
          this.tbAttachPhoto.Name = "tbAttachPhoto";
          this.tbAttachPhoto.Size = new System.Drawing.Size(241, 20);
          this.tbAttachPhoto.TabIndex = 5;
          // 
          // lblAttachPhoto
          // 
          this.lblAttachPhoto.AutoSize = true;
          this.lblAttachPhoto.Location = new System.Drawing.Point(9, 345);
          this.lblAttachPhoto.Name = "lblAttachPhoto";
          this.lblAttachPhoto.Size = new System.Drawing.Size(73, 13);
          this.lblAttachPhoto.TabIndex = 17;
          this.lblAttachPhoto.Text = "Attach photos";
          // 
          // btnFind
          // 
          this.btnFind.Location = new System.Drawing.Point(348, 340);
          this.btnFind.Name = "btnFind";
          this.btnFind.Size = new System.Drawing.Size(75, 23);
          this.btnFind.TabIndex = 6;
          this.btnFind.Text = "Find";
          this.btnFind.UseVisualStyleBackColor = true;
          this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
          // 
          // openFileDialog
          // 
          this.openFileDialog.Multiselect = true;
          // 
          // panelAddress
          // 
          this.panelAddress.Location = new System.Drawing.Point(12, 36);
          this.panelAddress.Name = "panelAddress";
          this.panelAddress.Size = new System.Drawing.Size(411, 147);
          this.panelAddress.TabIndex = 0;
          // 
          // FormNewPosition
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(435, 424);
          this.Controls.Add(this.panelAddress);
          this.Controls.Add(this.btnFind);
          this.Controls.Add(this.lblAttachPhoto);
          this.Controls.Add(this.tbAttachPhoto);
          this.Controls.Add(this.tbLongitude);
          this.Controls.Add(this.label10);
          this.Controls.Add(this.tbLatitude);
          this.Controls.Add(this.label9);
          this.Controls.Add(this.buttonCancel);
          this.Controls.Add(this.buttonOk);
          this.Controls.Add(this.tbNote);
          this.Controls.Add(this.tbHouse);
          this.Controls.Add(this.tbStreet);
          this.Controls.Add(this.cbTypeStreet);
          this.Controls.Add(this.label6);
          this.Controls.Add(this.label5);
          this.Controls.Add(this.label8);
          this.Controls.Add(this.label7);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.Name = "FormNewPosition";
          this.Text = "Create a new position";
          this.Load += new System.EventHandler(this.FormNewPosition_Load);
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.ComboBox cbTypeStreet;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox tbStreet;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox tbHouse;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox tbNote;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Button buttonOk;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox tbLatitude;
      private System.Windows.Forms.TextBox tbLongitude;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.TextBox tbAttachPhoto;
      private System.Windows.Forms.Label lblAttachPhoto;
      private System.Windows.Forms.Button btnFind;
      private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.Panel panelAddress;
   }
}