﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ICSM;
using NSPosition;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.UtilityClass;


namespace XICSM.UcrfRfaNET
{
    internal partial class frmSelectPosition : FBaseForm
    {
        public class PositionRowBase : NotifyPropertyChanged
        {
            //----
            private int _id = 0;
            /// <summary>
            /// Id
            /// </summary>
            public int Id
            {
                get { return _id; }
                set
                {
                    if (value != _id)
                    {
                        _id = value;
                        InvokeNotifyPropertyChanged("Id");
                    }
                }
            }
            //----
            private string _tableName = "";
            /// <summary>
            /// Название таблицы
            /// </summary>
            public string TableName
            {
                get { return _tableName; }
                set
                {
                    if (value != _tableName)
                    {
                        _tableName = value;
                        InvokeNotifyPropertyChanged("TableName");
                        InvokeNotifyPropertyChanged("LocalizTableName");
                    }
                }
            }
            /// <summary>
            /// Локализированое Название таблицы
            /// </summary>
            public string LocalizTableName
            {
                get { return CLocaliz.TxT(TableName); }
            }
            //----
            private double _distance = 0.0;
            /// <summary>
            /// Растояние
            /// </summary>
            public double Distance
            {
                get { return _distance.Round(4); }
                set
                {
                    if (value != _distance)
                    {
                        _distance = value;
                        InvokeNotifyPropertyChanged("Distance");
                    }
                }
            }
            //----
            private double _longitudeDms = 0.0;
            /// <summary>
            /// Долгота
            /// </summary>
            public double LongitudeDms
            {
                get { return _longitudeDms; }
                set
                {
                    if (value != _longitudeDms)
                    {
                        _longitudeDms = value;
                        InvokeNotifyPropertyChanged("LongitudeDms");
                        InvokeNotifyPropertyChanged("LongitudeDmsText");
                    }
                }
            }
            /// <summary>
            /// Долгота текст
            /// </summary>
            public string LongitudeDmsText
            {
                get { return HelpFunction.DmsToString(LongitudeDms, EnumCoordLine.Lon); }
            }
            //----
            private double _latitudeDms = 0.0;
            /// <summary>
            /// Долгота
            /// </summary>
            public double LatitudeDms
            {
                get { return _latitudeDms; }
                set
                {
                    if (value != _latitudeDms)
                    {
                        _latitudeDms = value;
                        InvokeNotifyPropertyChanged("LatitudeDms");
                        InvokeNotifyPropertyChanged("LatitudeDmsText");
                    }
                }
            }
            /// <summary>
            /// Долгота текст
            /// </summary>
            public string LatitudeDmsText
            {
                get { return HelpFunction.DmsToString(LatitudeDms, EnumCoordLine.Lat); }
            }
            //----
            private bool _isKoatuu = false;
            /// <summary>
            /// Долгота
            /// </summary>
            public bool IsKoatuu
            {
                get { return _isKoatuu; }
                set
                {
                    if (value != _isKoatuu)
                    {
                        _isKoatuu = value;
                        InvokeNotifyPropertyChanged("IsKoatuu");
                    }
                }
            }
            //----
            private string _remark = "";
            /// <summary>
            /// Долгота
            /// </summary>
            public string Remark
            {
                get { return _remark; }
                set
                {
                    if (value != _remark)
                    {
                        _remark = value;
                        InvokeNotifyPropertyChanged("Remark");
                    }
                }
            }
            //----
            private string _status = "";
            /// <summary>
            /// Долгота
            /// </summary>
            public string Status
            {
                get { return _status; }
                set
                {
                    if (value != _status)
                    {
                        _status = value;
                        InvokeNotifyPropertyChanged("Status");
                    }
                }
            }
            //----
            private Color _backGroundColor = Color.White;
            /// <summary>
            /// Долгота
            /// </summary>
            public Color BackGroundColor
            {
                get { return _backGroundColor; }
                set
                {
                    if (value != _backGroundColor)
                    {
                        _backGroundColor = value;
                        InvokeNotifyPropertyChanged("BackGroundColor");
                    }
                }
            }
        }
        //===================================================
        /// <summary>
        /// Отображает окно для выбора сайта 
        /// </summary>
        /// <param name="table">Имя таблицы, которая должна ити после SITE</param>
        /// <param name="radius">радиус поиска</param>
        /// <param name="lon">долгота</param>
        /// <param name="lat">широта</param>
        public static IMObject SelectPosition(string table, double radius, double lon, double lat)
        {
            frmSelectPosition frm = new frmSelectPosition(table, radius, lon, lat);
            frm.IsPositionState = false;            
            frm.ShowDialog();
            if (IsPressOk)
            {                
                IMObject retObj = frm.objPos;
                frm.Dispose();
                return retObj;
            }
            return null;
        }
        

        //===================================================
        /// <summary>
        /// Отображает окно для выбора сайта для таблиц XFA_...
        /// </summary>
        /// <param name="table">Имя таблицы, которая должна ити после SITE</param>
        /// <param name="radius">радиус поиска</param>
        /// <param name="lon">долгота</param>
        /// <param name="lat">широта</param>
        public static PositionState SelectPositionState(string table, double radius, double lon, double lat)
        {
            frmSelectPosition frm = new frmSelectPosition(table, radius, lon, lat);
            frm.IsPositionState = true;
            frm.ShowDialog();
            if (IsPressOk)
            {
                PositionState retObj = frm.posState;
                frm.Dispose();
                return retObj;
            }
            return null;
        }
        
        //===================================================
        //===================================================
        //===================================================
        //===================================================
        private string table = "";
        private double lon;
        private double lat;
        public static bool IsPressOk;
        public bool IsPositionState;
        public PositionState posState;
        public IMObject objPos;
        private static double _radius = IM.NullD;
        protected HelpClasses.Classes.SortableBindingList<PositionRowBase> PosListRows = new HelpClasses.Classes.SortableBindingList<PositionRowBase>();
        //===================================================
        /// <summary>
        /// Конструктор
        /// </summary>
        protected frmSelectPosition(string table, double radius, double lon, double lat)
        {
            InitializeComponent();
            dataGridView.AutoGenerateColumns = false;
            CLocaliz.TxT(this);
            this.table = table;
            if (frmSelectPosition._radius == IM.NullD)
                frmSelectPosition._radius = radius;
            tbRadius_TextChanged(null, null);
            this.lon = lon; tbLon_TextChanged(null, null);
            this.lat = lat; tbLat_TextChanged(null, null);
            // Столбцы грида
            // ID
            // DISTANCE
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "lisDISTANCE";
            column.HeaderText = CLocaliz.TxT("DISTANCE");
            column.Width = 100;
            column.DataPropertyName = "Distance";
            dataGridView.Columns.Add(column);
            // ID
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisID";
            column.HeaderText = CLocaliz.TxT("ID");
            column.Width = 50;
            column.Visible = ApplSetting.IsDebug;
            column.DataPropertyName = "Id";
            dataGridView.Columns.Add(column);
            // TABLE_NAME
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisTABLE_NAME";
            column.HeaderText = CLocaliz.TxT("TABLE_NAME");
            column.Width = 100;
            column.Visible = false;
            column.DataPropertyName = "TableName";
            dataGridView.Columns.Add(column);
            // TABLE_NAME_LOCALIZ
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisTABLE_NAME_LOCALIZ";
            column.HeaderText = CLocaliz.TxT("TABLE_NAME");
            column.Width = 100;
            column.DataPropertyName = "LocalizTableName";
            dataGridView.Columns.Add(column);
            // LONGITUDE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisLONGITUDE";
            column.HeaderText = CLocaliz.TxT("LONGITUDE");
            column.Width = 80;
            column.DataPropertyName = "LongitudeDmsText";
            dataGridView.Columns.Add(column);
            // LATITUDE
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisLATITUDE";
            column.HeaderText = CLocaliz.TxT("LATITUDE");
            column.Width = 80;
            column.DataPropertyName = "LatitudeDmsText";
            dataGridView.Columns.Add(column);
            // ID_KOATUU
            {
                DataGridViewCheckBoxColumn columnCb = new DataGridViewCheckBoxColumn();
                columnCb.Name = "lisID_KOATUU";
                columnCb.HeaderText = CLocaliz.TxT("ID_KOATUU");
                columnCb.Width = 100;
                columnCb.ReadOnly = true;
                columnCb.DataPropertyName = "IsKoatuu";
                dataGridView.Columns.Add(columnCb);
            }
            // REMARK
            column = new DataGridViewTextBoxColumn();
            column.Name = "lisREMARK";
            column.HeaderText = CLocaliz.TxT("REMARK");
            column.Width = 250;
            column.DataPropertyName = "Remark";
            dataGridView.Columns.Add(column);
            // Status
            column = new DataGridViewTextBoxColumn();
            column.Name = "Status";
            column.HeaderText = CLocaliz.TxT("Status");
            column.Width = 80;
            column.DataPropertyName = "Status";
            dataGridView.Columns.Add(column);

            dataGridView.DataSource = null;
            dataGridView.DataSource = PosListRows;
            // Обновляем запрос
            UpdateRow();
        }
        //===================================================
        /// <summary>
        /// Изменилась долгота
        /// </summary>
        private void tbLon_TextChanged(object sender, EventArgs e)
        {
            double tmpLon = lon;
            lon = ConvertType.ToDouble(tbLon.Text, tmpLon);
            tbLon.Text = lon.ToString();
            labelDECLon.Text = HelpFunction.DmsToString(lon, EnumCoordLine.Lon);
            if (!HelpFunction.ValidateCoords(lon, EnumCoordLine.Lon))
                tbLon.BackColor = Color.Red;
            else
                tbLon.BackColor = SystemColors.Window;
        }
        //===================================================
        /// <summary>
        /// Изменилась широта
        /// </summary>
        private void tbLat_TextChanged(object sender, EventArgs e)
        {
            double tmpLat = lat;
            lat = ConvertType.ToDouble(tbLat.Text, tmpLat);
            tbLat.Text = lat.ToString();
            labelDECLat.Text = HelpFunction.DmsToString(lat, EnumCoordLine.Lat);
            if (!HelpFunction.ValidateCoords(lat, EnumCoordLine.Lat))
                tbLat.BackColor = Color.Red;
            else
                tbLat.BackColor = SystemColors.Window;
        }
        //===================================================
        /// <summary>
        /// Изменился радиус
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbRadius_TextChanged(object sender, EventArgs e)
        {
            double tmpRadius = _radius;
            _radius = ConvertType.ToDouble(tbRadius.Text, tmpRadius);
            tbRadius.Text = _radius.ToString();
        }
        //==================================================
        /// <summary>
        /// Обновить запрос
        /// </summary>
        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateRow();
        }
        //==================================================
        /// <summary>
        /// Обновляет данные в таблице
        /// </summary>
        private void UpdateRow()
        {
            string tableFirst = table;
            PosListRows.Clear();
            RecordXY decPosition = Position.convertPosition(lon, lat, "DMS", "DEC");            
            IMPosition imPos = IMPosition.Convert(new IMPosition(lon, lat, "4DMS"), "4DEC");
            decPosition.Latitude = decPosition.Y = imPos.Lat;
            decPosition.Longitude = decPosition.X = imPos.Lon;
            // Выбираем записи из таблицы SITES
            GetPositionRows(decPosition.Longitude, decPosition.Latitude, _radius, ICSMTbl.itblAdmSite);
            tbRadius.ReadOnly = true;
            if (tableFirst != "SITES")
            {
                tbRadius.ReadOnly = false;
                // Выбираем записи из таблицы SITE
                GetPositionRows(decPosition.Longitude, decPosition.Latitude, _radius, ICSMTbl.itblSite);
                // Добавляем все остальные Position
                string[] listPosTable = new string[] {};
                if (tableFirst == ICSMTbl.itblPositionEs)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionBro)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionFmn)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionHf)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionMob2)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionMw)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionMw,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionWim
                                       };
                else if (tableFirst == ICSMTbl.itblPositionWim)
                    listPosTable = new[]
                                       {
                                           ICSMTbl.itblPositionWim,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw
                                       };
                else if (tableFirst == PlugTbl.XfaPosition)
                    listPosTable = new[]
                                       {
                                           PlugTbl.XfaPosition,
                                           ICSMTbl.itblPositionWim,
                                           ICSMTbl.itblPositionEs,
                                           ICSMTbl.itblPositionBro,
                                           ICSMTbl.itblPositionFmn,
                                           ICSMTbl.itblPositionHf,
                                           ICSMTbl.itblPositionMob2,
                                           ICSMTbl.itblPositionMw
                                       };

                // Добавляем новые Position
                foreach (string tableName in listPosTable)
                    GetPositionRows(decPosition.Longitude, decPosition.Latitude, _radius, tableName);
            }
        }
        //==================================================
        /// <summary>
        /// Возвразает список строк для грида в уже нормальном виде
        /// </summary>
        /// <param name="lon">Долгота в DEC</param>
        /// <param name="lat">Широта в DEC</param>
        /// <param name="_radius">Радиус в км</param>
        /// <param name="_tableName">имя таблицы (POSITION_XX)</param>
        private void GetPositionRows(double lon, double lat, double _radius, string _tableName)
        {
            RecordPos positionRadius = Position.GetRangeCoordinates(lon, lat, _radius, "", "");
            IMRecordset rsPosition = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);
            string selectParamPostitions = "ID,LONGITUDE,LATITUDE,REMARK,CITY_ID,X,Y";
            selectParamPostitions += _tableName == ICSMTbl.SITES ? ",STATUS" : ",ADMS_ID";
            if (_tableName == ICSMTbl.itblSite)
                selectParamPostitions += ",TOWER";
            rsPosition.Select(selectParamPostitions);
            rsPosition.SetWhere("LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
            rsPosition.SetWhere("LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
            rsPosition.SetWhere("LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
            rsPosition.SetWhere("LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);            
           
            rsPosition.OrderBy("LONGITUDE", OrderDirection.Ascending);
            rsPosition.OrderBy("LATITUDE", OrderDirection.Ascending);
            if (_tableName == ICSMTbl.SITES)
                rsPosition.OrderBy("STATUS", OrderDirection.Ascending);
            
            try
            {
                List<PositionRowBase> tmpList = new List<PositionRowBase>();
                for (rsPosition.Open(); !rsPosition.IsEOF(); rsPosition.MoveNext())
                {
                    double findStationLon = rsPosition.GetD("LONGITUDE");
                    double findStationLat = rsPosition.GetD("LATITUDE");

                    bool isKoatuu = false;
                    if (rsPosition.GetI("CITY_ID") != IM.NullI)
                        isKoatuu = true;
                    Color colorSite = Color.White;
                  
                    double distance = IMPosition.Distance(new IMPosition(findStationLon, findStationLat, "4DEC"), new IMPosition(lon, lat, "4DEC"));                  

                    if (distance <= _radius)
                    {
                        //Перевірка на станцію моніторинга
                        if (_tableName == ICSMTbl.itblSite)
                        {                            
                            string findStationType = rsPosition.GetS("TOWER");
                            if (findStationType == "РКП/АСРМ" && distance <= 0.5)
                                colorSite = Color.Pink;
                           
                        }
                        if (_tableName == ICSMTbl.SITES)
                        {
                            string status = rsPosition.GetS("STATUS");
                            if (status == "CHKD")
                                colorSite = Color.LightGreen;
                        }                        
                        else //if (_tableName!=table)
                        {
                            if (rsPosition.GetI("ADMS_ID") != IM.NullI)
                                continue;
                        }

                        string statusGrid = "";
                        if (_tableName == ICSMTbl.SITES)
                            statusGrid = rsPosition.GetS("STATUS");

                        RecordXY dmsPosition = Position.convertPosition(findStationLon, findStationLat, "DEC", "DMS");

                        tmpList.Add(new PositionRowBase
                                        {
                                            Id = rsPosition.GetI("ID"),
                                            TableName = _tableName,
                                            Distance = distance,
                                            IsKoatuu = isKoatuu,
                                            LongitudeDms = dmsPosition.Longitude,
                                            LatitudeDms = dmsPosition.Latitude,
                                            Remark = rsPosition.GetS("REMARK"),
                                            Status = statusGrid,
                                            BackGroundColor = colorSite,
                                        });
                    }
                }
                tmpList.Sort(CompareRowByDistance);
                foreach (PositionRowBase rowBase in tmpList)
                    PosListRows.Add(rowBase);
            }
            finally
            {
                rsPosition.Final();
            }
        }
        /// <summary>
        /// Сортировка по дистанции
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private static int CompareRowByDistance(PositionRowBase x, PositionRowBase y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're
                    // equal. 
                    return 0;
                }
                else
                {
                    // If x is null and y is not null, y
                    // is greater. 
                    return -1;
                }
            }
            else
            {
                // If x is not null...
                //
                if (y == null)
                    // ...and y is null, x is greater.
                {
                    return 1;
                }
                else
                {
                    if (x.Distance < y.Distance)
                        return -1;
                    return 1;
                }
            }
        }
        //==================================================
        /// <summary>
        /// Копирует сайт, если необходимо и возвращает RecordPtr
        /// </summary>
        /// <param name="tableSites">имя таблицы, в котором выбрали сайт</param>
        /// <param name="id">ID сайта</param>
        /// <returns>объект необходимого сайта</returns>
        private IMObject ClonePosition(string tableSites, int id)
        {
            int sitesId = id;
            IMObject retPosition = null;

            //Проверка на выбор административного сайта
            if (tableSites == ICSMTbl.itblAdmSite)
            {
                // поиск сайта, который ссылаеться на административный
                string[] masPositionTables = new[] { ICSMTbl.itblPositionEs, ICSMTbl.itblPositionBro, ICSMTbl.itblPositionFmn, ICSMTbl.itblPositionHf, ICSMTbl.itblPositionMob2, ICSMTbl.itblPositionMw, ICSMTbl.itblPositionWim };
                // Ищем провереный сайт для нашей службы
                tableSites = table;
                int idAdmSite = FindIdAdmSite(tableSites, id);
                int i = 0;
                while ((idAdmSite == 0) && (i < masPositionTables.Length))
                {// Ищем любой сайт
                    tableSites = masPositionTables[i++];
                    idAdmSite = FindIdAdmSite(tableSites, id);
                }
                if (idAdmSite == 0)
                {// Сайт не нашли
                    tableSites = "";
                    id = 0;
                }
                else
                {
                    id = idAdmSite;
                }
            }
            if (tableSites == table)
            {// Выбрали имено тот майт, который необходимо для данной службы
                retPosition = IMObject.LoadFromDB(tableSites, id);
            }
            else if (id != 0)
            {// Необходимо копировать сайт
                if (table != ICSMTbl.SITES)
                    retPosition = CloneSite(tableSites, id, table, true);               
            }
            if (retPosition == null && table == ICSMTbl.SITES)
                retPosition = IMObject.LoadFromDB(table, sitesId);
            return retPosition;
        }

        /// <summary>
        /// Создает новый сайт, копируя значения из таблицы fromTable с Id=fromId
        /// </summary>
        /// <param name="fromTable">из какой таблицы копируются данные</param>
        /// <param name="fromId">из какой записи таблицы fromId копируются данные</param>
        /// <param name="toTable">в какую таблицу копируются данные</param>
        /// <returns> запись</returns>
        public static PositionState CloneSiteToPosState(string fromTable, int fromId, string toTable)
        {                     
            PositionState pos = new PositionState();
            pos.LoadStatePosition(fromId, fromTable);
            if (!(fromTable == PlugTbl.XfaPosition && toTable == PlugTbl.XfaPosition))
            {
                pos.Id = IM.AllocID(toTable, 1, -1);
                if (toTable != PlugTbl.XfaPosition)
                    pos.TableName = toTable;
            }
            return pos;
        }

        //==================================================
        /// <summary>
        /// Создает новый сайт, копирую данные из исходного
        /// </summary>
        /// <param name="fromTable">Из какой таблицы копировать</param>
        /// <param name="fromId">ID записи для копирования</param>
        /// <param name="toTable">в какую таблицу копировать</param>
        /// <param name="copyAdmSite">копировать ссылку на административный сайт</param>
        /// <returns>новый объект сайта</returns>
        public static IMObject CloneSite(string fromTable, int fromId, string toTable, bool copyAdmSite)
        {
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }
            IMObject srcPosition = IMObject.LoadFromDB(fromTable, fromId);
            IMObject retPosition = IMObject.New(toTable);
            int newId = retPosition.GetI("ID");
            foreach (OrmField fld in fldMas)
            {
                if (fld == null)
                    continue;
                if (fld.Name == "ID")
                    continue;
                if ((fld.Name == "ADMS_ID") && (copyAdmSite != true))
                    continue;  //Не копируем ссылку на админ сайт
                if ((fld.Name == "ADMS_ID") && (toTable==ICSMTbl.SITES))
                    continue;
                if (fld.Name == "COUNTRY_ID")
                    if (string.IsNullOrEmpty(srcPosition.GetS(fld.Name)))
                        srcPosition.Put(fld.Name, "UKR");
                if (fld.Name == "CODE")
                {
                    string code = fromTable.Substring(fromTable.Length - 1, 1) + "_" + newId;
                    if (toTable != ICSMTbl.SITES)
                        retPosition[fld.Name] = code;
                    continue;
                }
                if (fld.DDesc != null)
                {
                    try
                    {
                        retPosition[fld.Name] = srcPosition.Get(fld.Name);
                    }
                    catch { }
                }
            }

            retPosition["DATE_CREATED"] = DateTime.Now;
            retPosition["CREATED_BY"] = IM.ConnectedUser();

            bool copyNotSaved = true;
            if (MessageBox.Show(CLocaliz.TxT("A copy of selected position created. Save it to database?"), CLocaliz.TxT("Question"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                retPosition.SaveToDB();
                copyNotSaved = false;
            }

            //copy attachments
            IMRecordset srcAtt = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadOnly);
            IMRecordset dstAtt = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
            try
            {
                //OrmTable ormDocLink = OrmSchema.Table("DOC_LINK");
                OrmField[] ormDocLinkFields = OrmSchema.Table("DOCLINK").Fields;
                foreach (OrmField fld in ormDocLinkFields)
                    if (fld.Nature == OrmFieldNature.Column)
                    {
                        srcAtt.Select(fld.Name);
                        dstAtt.Select(fld.Name);
                    }
                srcAtt.SetWhere("REC_TAB", IMRecordset.Operation.Eq, fromTable);
                srcAtt.SetWhere("REC_ID", IMRecordset.Operation.Eq, fromId);
                dstAtt.SetWhere("REC_TAB", IMRecordset.Operation.Eq, toTable);
                dstAtt.SetWhere("REC_ID", IMRecordset.Operation.Eq, newId);

                for (srcAtt.Open(), dstAtt.Open(); !srcAtt.IsEOF(); srcAtt.MoveNext())
                {
                    if (copyNotSaved)
                    {
                        if (MessageBox.Show(CLocaliz.TxT("Position being copied has an attachments. In order to copy attachments, we have to save position copy immediately. Save the copy?"), CLocaliz.TxT("Question"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            retPosition.SaveToDB();
                            copyNotSaved = false;
                        }
                        else
                        {
                            break;
                        }
                    }

                    try
                    {
                        dstAtt.AddNew();
                        foreach (OrmField fld in ormDocLinkFields)
                        {
                            if (fld.Name == "REC_TAB")
                                dstAtt.Put(fld.Name, toTable);
                            else if (fld.Name == "REC_ID")
                                dstAtt.Put(fld.Name, newId);
                            else if (fld.Nature == OrmFieldNature.Column)
                                dstAtt.Put(fld.Name, srcAtt.Get(fld.Name));
                        }
                        dstAtt.Update();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Error copying an attachment: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading attachment(s): " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                srcAtt.Close();
                srcAtt.Dispose();
            }
            return retPosition;
        }
        //==================================================
        /// <summary>
        /// Ищет сайты, которые ссылаються на андминистратывные сайты
        /// </summary>
        /// <param name="table">в какой таблице искать</param>
        /// <param name="admId">ID административного сайта</param>
        /// <returns>id сайта</returns>
        private int FindIdAdmSite(string table, int admId)
        {
            int retId = 0;
            if (table != ICSMTbl.SITES)
            {
                IMRecordset r = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
                r.Select("ID,ADMS_ID");
                r.SetWhere("ADMS_ID", IMRecordset.Operation.Eq, admId);
                r.Open();
                if (!r.IsEOF())
                    retId = r.GetI("ID");
                r.Final();
            }
            return retId;
        }
        //===================================================
        /// <summary>
        /// Выбрали станцию
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            IsPressOk = true;
            GetNewPosition();         
        }
        //===================================================
        /// <summary>
        /// Отменить выбор
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            IsPressOk = false;
            posState = null;
            objPos = null;
            Close();
        }
        //===================================================
        /// <summary>
        /// Отобразим сайт
        /// </summary>
        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            IsPressOk = true;
            GetNewPosition();
        }
        //===================================================
        /// <summary>
        /// Вытаскавает выбраную запись и создает новую позицию, если необходимо.
        /// Закрывает форму, если все ОК
        /// </summary>
        private void GetNewPosition()
        {
            if (dataGridView.CurrentCell == null)
                return;
            int rowIndex = dataGridView.CurrentCell.RowIndex;
            PositionRowBase row = null;

            if ((rowIndex < PosListRows.Count) && (rowIndex > -1))
                row = PosListRows[rowIndex];

            if(row == null)
            {
                MessageBox.Show("Can't find selected position", "Error");
                return;
            }
            if (row.IsKoatuu == false)
            {
                if (MessageBox.Show(CLocaliz.TxT("Selected station isn't connected to KOATUU. Continue?"), CLocaliz.TxT("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
            }

            string tableName = row.TableName;
            int id = row.Id;

            if ((id == 0) || (tableName == ""))
                return;

            if (IsPositionState)
                posState = CloneSiteToPosState(tableName, id, table);
            else
            {
                objPos = ClonePosition(tableName, id);
                //TODO Это уже не нужно, так как здесь всегда будет корректный сайт (смотри выше)
                ////Перевірка на доступність запису позиції в адм. сайті     
                //if (tableName == ICSMTbl.SITES)
                //{//адмін сайти
                //    bool isCopy = false;
                //    IMRecordset r = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
                //    r.Select("ID,ADMS_ID");
                //    r.SetWhere("ADMS_ID", IMRecordset.Operation.Eq, id);
                //    try
                //    {
                //        r.Open();
                //        if (r.IsEOF())
                //            isCopy = true;
                //    }
                //    finally
                //    {
                //       r.Final(); 
                //    }
                //    if (isCopy)
                //    {
                //        objPos = IMObject.LoadFromDB(table, PositionState.ClonePositions(IMObject.LoadFromDB(tableName, id), table));                       
                //    }
                //}
                //else
                //{//сайт служби
                //    if (objPos != null)
                //    {
                //        //objPos.Put("ADMS_ID", PositionState.ClonePositions(objPos, ICSMTbl.SITES)); //Будет выполненно выше
                //        objPos.SaveToDB();
                //    }
                //    else
                //    {
                //        PositionState.ClonePositions(IMObject.LoadFromDB(tableName, id), ICSMTbl.SITES);                        
                //    }
                //}         
            }

            if ((objPos == null && !IsPositionState) || (IsPositionState && posState == null))
                MessageBox.Show("Can't find selected position", "Error");
            else
                Close();
        }      

        //===================================================
        /// <summary>
        /// Нажатие кнопки на сриде
        /// </summary>
        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (dataGridView.CurrentCell != null))
            {
                int rowIndex = dataGridView.CurrentCell.RowIndex;
                PositionRowBase row = null;

                if ((rowIndex < PosListRows.Count) && (rowIndex > -1))
                    row = PosListRows[rowIndex];

                try
                {
                    if (row != null)
                    {
                        int id = row.Id;
                        string tableName = row.TableName;
                        RecordPtr ptr = new RecordPtr(tableName, id);
                        if (ptr.UserEdit())
                            UpdateRow();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace, "Error");
                }
            }
        }

        private void dataGridView_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if((-1 < e.RowIndex) && (e.RowIndex < PosListRows.Count))
            {
                dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = PosListRows[e.RowIndex].BackGroundColor;
            }
        }
    }
}
