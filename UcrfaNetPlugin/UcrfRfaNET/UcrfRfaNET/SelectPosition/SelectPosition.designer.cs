﻿namespace XICSM.UcrfRfaNET
{
   partial class frmSelectPosition
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.OkButton = new System.Windows.Forms.Button();
          this.CnclButton = new System.Windows.Forms.Button();
          this.gbParam = new System.Windows.Forms.GroupBox();
          this.labelDECLat = new System.Windows.Forms.Label();
          this.labelDECLon = new System.Windows.Forms.Label();
          this.UpdateButton = new System.Windows.Forms.Button();
          this.label4 = new System.Windows.Forms.Label();
          this.tbRadius = new System.Windows.Forms.TextBox();
          this.label3 = new System.Windows.Forms.Label();
          this.tbLat = new System.Windows.Forms.TextBox();
          this.tbLon = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.dataGridView = new System.Windows.Forms.DataGridView();
          this.gbTable = new System.Windows.Forms.GroupBox();
          this.gbParam.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
          this.gbTable.SuspendLayout();
          this.SuspendLayout();
          // 
          // OkButton
          // 
          this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.OkButton.Location = new System.Drawing.Point(803, 651);
          this.OkButton.Name = "OkButton";
          this.OkButton.Size = new System.Drawing.Size(75, 23);
          this.OkButton.TabIndex = 1;
          this.OkButton.Text = "OK";
          this.OkButton.UseVisualStyleBackColor = true;
          this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
          // 
          // CnclButton
          // 
          this.CnclButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.CnclButton.Location = new System.Drawing.Point(919, 651);
          this.CnclButton.Name = "CnclButton";
          this.CnclButton.Size = new System.Drawing.Size(75, 23);
          this.CnclButton.TabIndex = 2;
          this.CnclButton.Text = "Cancel";
          this.CnclButton.UseVisualStyleBackColor = true;
          this.CnclButton.Click += new System.EventHandler(this.CancelButton_Click);
          // 
          // gbParam
          // 
          this.gbParam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.gbParam.Controls.Add(this.labelDECLat);
          this.gbParam.Controls.Add(this.labelDECLon);
          this.gbParam.Controls.Add(this.UpdateButton);
          this.gbParam.Controls.Add(this.label4);
          this.gbParam.Controls.Add(this.tbRadius);
          this.gbParam.Controls.Add(this.label3);
          this.gbParam.Controls.Add(this.tbLat);
          this.gbParam.Controls.Add(this.tbLon);
          this.gbParam.Controls.Add(this.label2);
          this.gbParam.Controls.Add(this.label1);
          this.gbParam.Location = new System.Drawing.Point(12, 582);
          this.gbParam.Name = "gbParam";
          this.gbParam.Size = new System.Drawing.Size(443, 92);
          this.gbParam.TabIndex = 3;
          this.gbParam.TabStop = false;
          // 
          // labelDECLat
          // 
          this.labelDECLat.AutoSize = true;
          this.labelDECLat.Location = new System.Drawing.Point(174, 42);
          this.labelDECLat.Name = "labelDECLat";
          this.labelDECLat.Size = new System.Drawing.Size(35, 13);
          this.labelDECLat.TabIndex = 9;
          this.labelDECLat.Text = "label6";
          // 
          // labelDECLon
          // 
          this.labelDECLon.AutoSize = true;
          this.labelDECLon.Location = new System.Drawing.Point(174, 19);
          this.labelDECLon.Name = "labelDECLon";
          this.labelDECLon.Size = new System.Drawing.Size(35, 13);
          this.labelDECLon.TabIndex = 8;
          this.labelDECLon.Text = "label5";
          // 
          // UpdateButton
          // 
          this.UpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.UpdateButton.Location = new System.Drawing.Point(353, 53);
          this.UpdateButton.Name = "UpdateButton";
          this.UpdateButton.Size = new System.Drawing.Size(75, 23);
          this.UpdateButton.TabIndex = 7;
          this.UpdateButton.Text = "Update";
          this.UpdateButton.UseVisualStyleBackColor = true;
          this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
          // 
          // label4
          // 
          this.label4.AutoSize = true;
          this.label4.Location = new System.Drawing.Point(406, 20);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(22, 13);
          this.label4.TabIndex = 6;
          this.label4.Text = "Km";
          // 
          // tbRadius
          // 
          this.tbRadius.Location = new System.Drawing.Point(346, 16);
          this.tbRadius.MaxLength = 3;
          this.tbRadius.Name = "tbRadius";
          this.tbRadius.Size = new System.Drawing.Size(54, 20);
          this.tbRadius.TabIndex = 5;
          this.tbRadius.Validated += new System.EventHandler(this.tbRadius_TextChanged);
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(291, 19);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(40, 13);
          this.label3.TabIndex = 4;
          this.label3.Text = "Radius";
          // 
          // tbLat
          // 
          this.tbLat.Location = new System.Drawing.Point(90, 39);
          this.tbLat.MaxLength = 8;
          this.tbLat.Name = "tbLat";
          this.tbLat.Size = new System.Drawing.Size(78, 20);
          this.tbLat.TabIndex = 3;
          this.tbLat.Validated += new System.EventHandler(this.tbLat_TextChanged);
          // 
          // tbLon
          // 
          this.tbLon.Location = new System.Drawing.Point(90, 13);
          this.tbLon.MaxLength = 8;
          this.tbLon.Name = "tbLon";
          this.tbLon.Size = new System.Drawing.Size(78, 20);
          this.tbLon.TabIndex = 2;
          this.tbLon.Validated += new System.EventHandler(this.tbLon_TextChanged);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(6, 42);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(45, 13);
          this.label2.TabIndex = 1;
          this.label2.Text = "Latitude";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(6, 19);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(54, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Longitude";
          // 
          // dataGridView
          // 
          this.dataGridView.AllowUserToAddRows = false;
          this.dataGridView.AllowUserToDeleteRows = false;
          this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.dataGridView.BackgroundColor = System.Drawing.Color.White;
          this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridView.Location = new System.Drawing.Point(6, 19);
          this.dataGridView.MultiSelect = false;
          this.dataGridView.Name = "dataGridView";
          this.dataGridView.ReadOnly = true;
          this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
          this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridView.Size = new System.Drawing.Size(976, 539);
          this.dataGridView.TabIndex = 0;
          this.dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellDoubleClick);
          this.dataGridView.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dataGridView_RowPrePaint);
          this.dataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView_KeyDown);
          // 
          // gbTable
          // 
          this.gbTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.gbTable.Controls.Add(this.dataGridView);
          this.gbTable.Location = new System.Drawing.Point(12, 12);
          this.gbTable.Name = "gbTable";
          this.gbTable.Size = new System.Drawing.Size(988, 564);
          this.gbTable.TabIndex = 0;
          this.gbTable.TabStop = false;
          // 
          // frmSelectPosition
          // 
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.ClientSize = new System.Drawing.Size(1012, 686);
          this.Controls.Add(this.gbParam);
          this.Controls.Add(this.CnclButton);
          this.Controls.Add(this.OkButton);
          this.Controls.Add(this.gbTable);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
          this.MaximizeBox = true;
          this.MinimumSize = new System.Drawing.Size(750, 450);
          this.Name = "frmSelectPosition";
          this.Text = "Select position";
          this.gbParam.ResumeLayout(false);
          this.gbParam.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
          this.gbTable.ResumeLayout(false);
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button OkButton;
      private System.Windows.Forms.Button CnclButton;
      private System.Windows.Forms.GroupBox gbParam;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox tbRadius;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbLat;
      private System.Windows.Forms.TextBox tbLon;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button UpdateButton;
      private System.Windows.Forms.Label labelDECLat;
      private System.Windows.Forms.Label labelDECLon;
      private System.Windows.Forms.DataGridView dataGridView;
      private System.Windows.Forms.GroupBox gbTable;
   }
}