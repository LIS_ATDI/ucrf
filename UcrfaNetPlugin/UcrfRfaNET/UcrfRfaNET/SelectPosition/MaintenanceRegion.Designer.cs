﻿namespace XICSM.UcrfRfaNET
{
   partial class MaintenanceRegion
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.SelectRegionBox = new System.Windows.Forms.GroupBox();
         this.RegionTextBox = new System.Windows.Forms.TextBox();
         this.RegionList = new System.Windows.Forms.ListView();
         this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
         this.btnSave = new System.Windows.Forms.Button();
         this.btnCancel = new System.Windows.Forms.Button();
         this.SelectRegionBox.SuspendLayout();
         this.SuspendLayout();
         // 
         // SelectRegionBox
         // 
         this.SelectRegionBox.Controls.Add(this.RegionTextBox);
         this.SelectRegionBox.Controls.Add(this.RegionList);
         this.SelectRegionBox.Location = new System.Drawing.Point(12, 12);
         this.SelectRegionBox.Name = "SelectRegionBox";
         this.SelectRegionBox.Size = new System.Drawing.Size(236, 378);
         this.SelectRegionBox.TabIndex = 0;
         this.SelectRegionBox.TabStop = false;
         this.SelectRegionBox.Text = "Select Region:";
         // 
         // RegionTextBox
         // 
         this.RegionTextBox.BackColor = System.Drawing.SystemColors.Window;
         this.RegionTextBox.Location = new System.Drawing.Point(13, 21);
         this.RegionTextBox.Multiline = true;
         this.RegionTextBox.Name = "RegionTextBox";
         this.RegionTextBox.ReadOnly = true;
         this.RegionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.RegionTextBox.Size = new System.Drawing.Size(211, 88);
         this.RegionTextBox.TabIndex = 1;
         // 
         // RegionList
         // 
         this.RegionList.CheckBoxes = true;
         this.RegionList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
         this.RegionList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
         this.RegionList.Location = new System.Drawing.Point(13, 130);
         this.RegionList.Name = "RegionList";
         this.RegionList.Size = new System.Drawing.Size(211, 242);
         this.RegionList.TabIndex = 0;
         this.RegionList.UseCompatibleStateImageBehavior = false;
         this.RegionList.View = System.Windows.Forms.View.Details;
         this.RegionList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.RegionList_ItemCheck);
         // 
         // columnHeader1
         // 
         this.columnHeader1.Width = 180;
         // 
         // btnSave
         // 
         this.btnSave.Location = new System.Drawing.Point(47, 396);
         this.btnSave.Name = "btnSave";
         this.btnSave.Size = new System.Drawing.Size(75, 23);
         this.btnSave.TabIndex = 2;
         this.btnSave.Text = "Save";
         this.btnSave.UseVisualStyleBackColor = true;
         this.btnSave.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // btnCancel
         // 
         this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.btnCancel.Location = new System.Drawing.Point(141, 396);
         this.btnCancel.Name = "btnCancel";
         this.btnCancel.Size = new System.Drawing.Size(75, 23);
         this.btnCancel.TabIndex = 3;
         this.btnCancel.Text = "Cancel";
         this.btnCancel.UseVisualStyleBackColor = true;
         // 
         // MaintenanceRegion
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(263, 441);
         this.Controls.Add(this.btnCancel);
         this.Controls.Add(this.btnSave);
         this.Controls.Add(this.SelectRegionBox);
         this.Name = "MaintenanceRegion";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "Maintenance Region";
         this.Load += new System.EventHandler(this.MaintenanceRegion_Load);
         this.SelectRegionBox.ResumeLayout(false);
         this.SelectRegionBox.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox SelectRegionBox;
      private System.Windows.Forms.Button btnSave;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.ListView RegionList;
      private System.Windows.Forms.ColumnHeader columnHeader1;
      public System.Windows.Forms.TextBox RegionTextBox;
   }
}