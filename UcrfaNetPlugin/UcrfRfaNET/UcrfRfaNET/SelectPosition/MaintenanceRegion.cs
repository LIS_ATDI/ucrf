﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   public partial class MaintenanceRegion : FBaseForm
   {
      HashSet<string> _regionsHash;
      string _regionValue;

      public MaintenanceRegion(string region)
      {
         _regionValue = region;
         _regionsHash = new HashSet<string>();
         InitializeComponent();
      }

      private void LoadRegions()
      {
         _regionsHash.Clear();
         _regionsHash.Add(HelpClasses.PositionState.AllUkr);
         //_regionsHash.Add("Київ");   //Сергей. Удалил так как эти поля уже занесены в таблицу CITIES
         //_regionsHash.Add("Севастопіль");
         using (CProgressBar bar = new CProgressBar(CLocaliz.TxT("Loading province...")))
         {
            int index = 0;
            IMRecordset r = new IMRecordset(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME,SUBPROVINCE,PROVINCE,CUST_TXT1,CUST_TXT3,CUST_TXT2");
            r.OrderBy("PROVINCE", OrderDirection.Ascending);
            try
            {
               for (r.Open(); !r.IsEOF(); r.MoveNext())
               {
                  bar.ShowSmall(index++);
                  string province = r.GetS("PROVINCE");
                  if (!string.IsNullOrEmpty(province))
                  {
                     if (!_regionsHash.Contains(province))
                     {
                        _regionsHash.Add(province);
                        bar.ShowBig(province);
                     }
                  }
               }
            }
            finally
            {
               r.Close();
               r.Destroy();
            }
         }
      }

      private void MaintenanceRegion_Load(object sender, EventArgs e)
      {        
         LoadRegions();
         foreach(string region in _regionsHash)
         {
           ListViewItem item = RegionList.Items.Add(region);
           if (_regionValue.Contains(region))
              item.Checked = true;
         }
         Text = CLocaliz.TxT("Maintenance Region");
         SelectRegionBox.Text = CLocaliz.TxT("Select Region:");
         btnSave.Text = CLocaliz.TxT("Save ");
         btnCancel.Text = CLocaliz.TxT("Cancel ");
      }

      private void RegionList_ItemCheck(object sender, ItemCheckEventArgs e)
      {
         string regionString = "";
         foreach(ListViewItem item in RegionList.Items)
         {
            if (e.Index == 0 && e.NewValue == CheckState.Checked)
            {
               regionString += item.Text;
               break;
            } else               
               if (item.Checked && (!(item.Index == e.Index && e.NewValue==CheckState.Unchecked)) || 
                     ((item.Index == e.Index && e.NewValue==CheckState.Checked)))
               {
                  if (item.Index == 0 && (!(0 == e.Index && e.NewValue == CheckState.Unchecked)))
                  {
                     regionString += item.Text;
                     break;
                  } else {
                     if (!string.IsNullOrEmpty(regionString))
                        regionString += "; ";
                     regionString += item.Text;
                  }
               }

         }
         RegionTextBox.Text = regionString;         
      }

      private void SaveButton_Click(object sender, EventArgs e)
      {
         DialogResult = DialogResult.OK;
         Close();
      }
   }
}
