﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
   public partial class FormVRS : FBaseForm
   {
      #region MEMBERS

      private int _tableID;
      private string _tableName = string.Empty;
      private Station _currStat;
      private NumberFormatInfo provider;

      public FormVRS(int tablID, string tablName)
      {         
         InitializeComponent();
         _tableID = tablID;
         _tableName = tablName;
         provider = new NumberFormatInfo();
         FillsData();
      }

      #endregion

      #region EVENTS

      private void FillsData()
      {
         _currStat = new Station(_tableName, _tableID);
      }

      private void Tbx_KeyPressOnDigit(object sender, KeyPressEventArgs e)
      {
         // Если это не цифра.
         if ((!Char.IsDigit(e.KeyChar)) && (e.KeyChar != 45) && (e.KeyChar != '\b'))
         {
            // Запрет на ввод более одной десятичной точки.
            TextBox tb = (TextBox)sender;
            char separator = provider.CurrencyGroupSeparator[0];
            if (e.KeyChar != separator || tb.Text.IndexOf(separator) != -1)
            {
               e.Handled = true;
            }
         }
      }

      private void BtnCalculation_Click(object sender, EventArgs e)
      {
         if (TbxRadius.Text.Length > 0)
         {
             double radius = ConvertType.ToDouble(TbxRadius.Text, 0);

             if (ApplSetting.IsDebugCalculation)
             {
                 ViborkaVRS_1546 viborka = new ViborkaVRS_1546(_currStat, radius, ChbTV.Checked,
                                                               ChbFM.Checked, ChbDVBT.Checked, ChbTDAB.Checked, checkBox5.Checked);

                 viborka.Calculate();
                 viborka.ShowResultForm();
             }
             else
             {
                 ViborkaVRS viborka = new ViborkaVRS(_currStat, radius, ChbTV.Checked,
                                                               ChbFM.Checked, ChbDVBT.Checked, ChbTDAB.Checked);

                 viborka.Calculate();
                 viborka.ShowResult();
             }
         }
      }

      #endregion

      private void BtnExit_Click(object sender, EventArgs e)
      {
         this.Close();
      }
   }
}
