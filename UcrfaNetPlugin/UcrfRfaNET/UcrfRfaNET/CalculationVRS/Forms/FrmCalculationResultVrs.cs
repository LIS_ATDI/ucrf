﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.Calculation;
using XICSM.UcrfRfaNET.Calculation.ResultForm;
using ComponentsLib;
using PointConverter=System.Drawing.PointConverter;
using ICSM;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    public partial class FrmCalculationResultVrs : Form
    {
        public class StationDetailsRow : TreeColumnRowBase
        {            
            /// <summary>
            /// Точка
            /// </summary>            
            [TreeColumn("Точка")]
            public StringElemCell Point { get; set; }

            /// <summary>
            /// Потужнiсть завали, "фактор часу" - 50%
            /// </summary>
            [TreeColumn("E зав 50%")]
            public DoubleElemCell Eintf50 { get; set; }

            /// <summary>
            /// Потужнiсть завали, "фактор часу" - 10%
            /// </summary>
            [TreeColumn("E зав 10%")]
            public DoubleElemCell Eintf10 { get; set; }

            /// <summary>
            /// Потужнiсть завали, "фактор часу" - 1%
            /// </summary>
            [TreeColumn("E зав 1%")]
            public DoubleElemCell Eintf1 { get; set; }

            /// <summary>
            /// Захисне відношення, постійне
            /// </summary>
            [TreeColumn("ЗВ(П)")]
            public DoubleElemCell PCp { get; set; }

            /// <summary>
            /// Захисне відношення, тропосферне
            /// </summary>
            [TreeColumn("ЗВ(T)")]
            public DoubleElemCell PCt { get; set; }

            /// <summary>
            /// "Диференціал" антен
            /// </summary>
            [TreeColumn("D(ант)")]
            public DoubleElemCell D { get; set; }

            /// <summary>
            /// Використана потужність, "Фактор часу" - 50%
            /// </summary>            
            [TreeColumn("E вик 50%")]
            public DoubleElemCell Eused50 { get; set; }

            /// <summary>
            /// Використана потужність, "Фактор часу" - 10%
            /// </summary>            
            [TreeColumn("E вик 10%")]
            public DoubleElemCell Eused10 { get; set; }

            /// <summary>
            /// Використана потужність, "Фактор часу" - 10%
            /// </summary>            
            [TreeColumn("E вик 1%")]
            public DoubleElemCell Eused1 { get; set; }

            /// <summary>
            /// Мінімальна потужність
            /// </summary>            
            [TreeColumn("E min")]
            public DoubleElemCell Emin { get; set; }

            /// <summary>
            ///  Тип завали
            /// </summary>            
            [TreeColumn("Тип зав")]
            public StringElemCell InterferenceType { get; set; }

            public StationDetailsRow()
            {                
                Point = new StringElemCell();                 
                Eintf50 = new DoubleElemCell(3);
                Eintf10 = new DoubleElemCell(3);
                Eintf1 = new DoubleElemCell(3);
                PCp = new DoubleElemCell(2);
                PCt = new DoubleElemCell(2);
                D = new DoubleElemCell(2);
                Eused50 = new DoubleElemCell(3);
                Eused10 = new DoubleElemCell(3);
                Eused1 = new DoubleElemCell(3);
                Emin = new DoubleElemCell(3);
                InterferenceType = new StringElemCell();             
            }
        }

        public class StationViewRow : TreeColumnRowBase
        {
            private Station _station;

            /// <summary>
            /// Номер
            /// </summary>
            [TreeColumn("№")]
            public StringElemCell Number { get; set; }

            /// <summary>
            /// Перекриття зони обслуговування
            /// </summary>
            [TreeColumn("ПЗ")]
            public StringElemCell Overlap { get; set; }

            /// <summary>
            /// Назва
            /// </summary>
            [TreeColumn("Назва")]
            public StringElemCell Name { get; set; }

            /// <summary>
            /// Телевізайний канал
            /// </summary>
            [TreeColumn("ТВК")]
            public StringElemCell Channel { get; set; }
            ///--
            /// <summary>
            /// Частота
            /// </summary>
            [TreeColumn("F, МГц")]
            public DoubleElemCell Frequency { get; set; }

            /// <summary>
            /// Відстань
            /// </summary>
            [TreeColumn("Відст.")]
            public DoubleElemCell Distance { get; set; }

            /// <summary>
            /// Азтимут
            /// </summary>
            [TreeColumn("Aз.")]
            public DoubleElemCell Azimuth { get; set; }

            /// <summary>
            /// ЕВП
            /// </summary>
            [TreeColumn("ЕВП")]
            public DoubleElemCell ERP { get; set; }

            /// <summary>
            /// Ефективна висота
            /// </summary>
            [TreeColumn("Н(еф)")]
            public DoubleElemCell HEff { get; set; }

            /// <summary>
            /// Поляризація
            /// </summary>
            [TreeColumn("Пол.")]
            public StringElemCell Polarization { get; set; }

            /// <summary>
            /// Зміщення несучої частоти
            /// </summary>
            [TreeColumn("ЗНЧ")]
            public DoubleElemCell Offset { get; set; }

            /// <summary>
            /// Мультиплексор
            /// </summary>
            [TreeColumn("Мx")]
            public StringElemCell Mx { get; set; }

            /// <summary>
            /// Стан
            /// </summary>
            [TreeColumn("Стан")]
            public StringElemCell StateType { get; set; }

            /// <summary>
            /// Завада нам
            /// </summary>
            [TreeColumn("Е зав \"нам\"")]
            public DoubleElemCell EOf { get; set; }

            /// <summary>
            /// Завада від нас
            /// </summary>
            [TreeColumn("Е зав \"від нас\"")]
            public DoubleElemCell EFrom { get; set; }

            /// <summary>
            /// Область
            /// </summary>
            [TreeColumn("Рег")]
            public StringElemCell Region { get; set; }

            /// <summary>
            /// ID
            /// </summary>
            [TreeColumn("ID")]
            public IntElemCell Id { get; set; }

            /// <summary>
            /// Широта
            /// </summary>
            [TreeColumn("Широта")]
            public StringElemCell Latitude { get; set; }

            /// <summary>
            /// Довгота
            /// </summary>
            [TreeColumn("Довгота")]
            public StringElemCell Longitude { get; set; }

            /// <summary>
            /// Стандарт
            /// </summary>
            [TreeColumn("Станд.")]
            public StringElemCell Standard { get; set; }

            /// <summary>
            /// Система
            /// </summary>
            [TreeColumn("Сист.")]
            public StringElemCell System { get; set; }

            /// <summary>
            /// Напр?
            /// </summary>
            [TreeColumn("Напр.")]
            public StringElemCell PalSecam { get; set; }

            /// <summary>
            /// Звуження зони обслуговування
            /// </summary>
            [TreeColumn("R(%)")]
            public DoubleElemCell RDecrease { get; set; }
            
            public StationViewRow(Station st)
            {
                Number = new StringElemCell();
                Overlap  = new StringElemCell(); 
                Name = new StringElemCell();
                Channel = new StringElemCell();
                Frequency = new DoubleElemCell(3);            
                Distance = new DoubleElemCell(3);            
                Azimuth = new DoubleElemCell(2);
                ERP = new DoubleElemCell(3);            
                HEff = new DoubleElemCell(3);
                Polarization = new StringElemCell();
                Offset = new DoubleElemCell(3);
                Mx = new StringElemCell();
                StateType = new StringElemCell();
                EOf = new DoubleElemCell(3);
                EFrom = new DoubleElemCell(3);
                Region = new StringElemCell();
                Id = new IntElemCell();
                Latitude = new StringElemCell();
                Longitude = new StringElemCell();
                Standard = new StringElemCell();
                System = new StringElemCell();
                PalSecam = new StringElemCell();
                RDecrease = new DoubleElemCell(2);
                       
                Number.Value = st._number.ToString(st.NumberFormat);
                Overlap.Value = st._Overlap == 1 ? "+" : "-";
                Azimuth.Value = st.Azimuth1;
                Frequency.Value = st._freq;
                Name.Value = st.Name;
                Channel.Value = st.Channel;
                Distance.Value = st.DISTANCE;
                ERP.Value = st._EVP2_for_R; // _EVP_for_R
                HEff.Value = Station.GetHeigh(st._TMP_EFHGT, st.Azimuth2);// st._EFHGT;                
                Polarization.Value = st.Polarization;
                Offset.Value = st._SNCH;
                Mx.Value = "??";
                StateType.Value = st.StatusType;
                //EOf.Value = 0.0;
                //EFrom.Value = 0.0;
                Region.Value = st.Region;
                Id.Value = st._tableID;
                Latitude.Value = HelpFunction.DmsToString(st._latitude, EnumCoordLine.Lat);
                Longitude.Value = HelpFunction.DmsToString(st._longitude, EnumCoordLine.Lon);
                Standard.Value = st.Standard;
                System.Value = st._dvbsys.ToString();

                EOf.Value = st.GetE_of();                    
                EFrom.Value = st.GetE_from();

                
                switch (st._std)
                {
                    case LISBCTxServer.TBCTvStandards.csPAL:
                        PalSecam.Value = "PAL";
                        break;

                    case LISBCTxServer.TBCTvStandards.csSECAM:
                        PalSecam.Value = "SECAM";
                        break;

                    case LISBCTxServer.TBCTvStandards.csNTSC:
                        PalSecam.Value = "NTSC";
                        break;

                    case LISBCTxServer.TBCTvStandards.csUNKNOWN:
                        PalSecam.Value = "?";
                        break;
                }

                /*if (st._tableID==128)
                {
                    int xx = 1;
                }*/

                RDecrease.Value = 100.0 * (1.0 - (st.R1n + st.R1f) / (st.Rn + st.Rf));                
                /*IsSelected = false;
                
                Distance = st.DISTANCE.Round(1);
                Azimuth = st.Azimuth1.Round(2);

                ERP = st._EVP_for_R.Round(3);
                EffectiveHeight = st._EFHGT.Round(0);
                Polarization = st.Polarization;
                Offset = st._SNCH;
                Multiplexor = "";
                Status = st.StatusType;
                Efor = st._E1.Round(3);
                Efrom = st._E2.Round(3);

                Id = st._tableID;
                Region = st.Region;
                Longitude = HelpFunction.DmsToString(st._longitude, EnumCoordLine.Lon);
                Latitude = HelpFunction.DmsToString(st._latitude, EnumCoordLine.Lat);
                System = st._dvbsys.ToString();
                Standard = st.Standard;

                //PalSecam = 
                
                _station = st;

                RDecrease = (100.0*(1.0 - (st.R1n + st.R1f)/(st.Rn + st.Rf))).Round(3);


                for (int i = 0; i < 4; i++)
                {
                    Details.Add(new StationDetails());
                    Details[i].Eintf50 = st.Points[i].Eintf50.Round(3);
                    Details[i].Eintf10 = st.Points[i].Eintf10.Round(3);
                    Details[i].Eintf1 = st.Points[i].Eintf1.Round(3);
                    Details[i].D = st.Points[i].D.Round(3);
                    Details[i].Emin = st.Points[i].Emin.Round(3);
                    Details[i].Eused1 = st.Points[i].Eused1.Round(3);
                    Details[i].Eused10 = st.Points[i].Eused10.Round(3);
                    Details[i].Eused50 = st.Points[i].Eused50.Round(3);
                    Details[i].InterferenceType = st.Points[i].InterferenceType;
                    Details[i].PCp = st.Points[i].PCp.Round(3);
                    Details[i].PCt = st.Points[i].PCt.Round(3);
                }

                Details[0].Point = "0-1";
                Details[1].Point = "0-2";
                Details[2].Point = st._number.ToString() + "-3";
                Details[3].Point = st._number.ToString() + "-4";                

                if (!string.IsNullOrEmpty(st.ADM_KEY))
                {
                    int x = 1;
                }*/
                _station = st;
            }

            public Station GetStation() 
            {
                return _station;
            }
        }

        public List<Station> StationList = new List<Station>();
        //public BindingList<StationView> StationViewList = new BindingList<StationView>();

        public Station CurrentStation;

        private TabsTreeColumn _topTable;
        public TabsTreeColumn TopTable { get { return _topTable; } }

        private TabsTreeColumn _bottomTable;
        public TabsTreeColumn BottomTable { get { return _bottomTable; } }

        public FrmCalculationResultVrs()
        {
            InitializeComponent();

            _topTable = new TabsTreeColumn();
            topPanel.Controls.Add(TopTable);
            TopTable.Dock = DockStyle.Fill;

            _bottomTable = new TabsTreeColumn();
            bottomPanel.Controls.Add(BottomTable);
            BottomTable.Dock = DockStyle.Fill;
                        
            /*
            DataGridViewCheckBoxColumn columnCb = new DataGridViewCheckBoxColumn();
            columnCb.HeaderText = "";
            columnCb.Name = "gridCB";
            columnCb.FlatStyle = FlatStyle.Standard;
            columnCb.ThreeState = true;
            columnCb.CellTemplate = new DataGridViewCheckBoxCell();
            columnCb.Width = 30;
            columnCb.TrueValue = true;
            columnCb.DataPropertyName = "IsSelected";
            stationGridView.Columns.Add(columnCb);

            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "Number";
            column.HeaderText = "№";
            column.Width = 35;
            column.ReadOnly = true;
            column.DataPropertyName = "Number";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Overlap";
            column.HeaderText = "ПЗ";
            column.Width = 35;
            column.ReadOnly = true;
            column.DataPropertyName = "Overlap";
            stationGridView.Columns.Add(column);
           
            column = new DataGridViewTextBoxColumn();
            column.Name = "Name";
            column.HeaderText = "Назва";
            column.Width = 55;
            column.ReadOnly = true;
            column.DataPropertyName = "Name";
            stationGridView.Columns.Add(column);

           
            column = new DataGridViewTextBoxColumn();
            column.Name = "Channel";
            column.HeaderText = "ТВК";
            column.Width = 35;
            column.ReadOnly = true;
            column.DataPropertyName = "Channel";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Frequency";
            column.HeaderText = "F, МГц";
            column.Width = 65;
            column.ReadOnly = true;
            column.DataPropertyName = "Frequency";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Distance";
            column.HeaderText = "Відст";
            column.Width = 65;
            column.ReadOnly = true;
            column.DataPropertyName = "Distance";
            stationGridView.Columns.Add(column);
                        
            column = new DataGridViewTextBoxColumn();
            column.Name = "Azimuth";
            column.HeaderText = "Аз.";
            column.Width = 35;
            column.ReadOnly = true;
            column.DataPropertyName = "Azimuth";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "ERP";
            column.HeaderText = "ЕВП";
            column.Width = 65;
            column.ReadOnly = true;
            column.DataPropertyName = "ERP";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "EffectiveHeight";
            column.HeaderText = "H(еф)";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = "EffectiveHeight";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Polarization";
            column.HeaderText = "Пол.";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Polarization";
            stationGridView.Columns.Add(column);
          
            column = new DataGridViewTextBoxColumn();
            column.Name = "Offset";
            column.HeaderText = "ЗНЧ";
            column.Width = 36;
            column.ReadOnly = true;
            column.DataPropertyName = "Offset";
            stationGridView.Columns.Add(column);
                        
            column = new DataGridViewTextBoxColumn();
            column.Name = "Multiplexor";
            column.HeaderText = "МХ";
            column.Width = 35;
            column.ReadOnly = true;
            column.DataPropertyName = "Multiplexor";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Status";
            column.HeaderText = "Стан";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Status";
            stationGridView.Columns.Add(column);
          
            column = new DataGridViewTextBoxColumn();
            column.Name = "Efor";
            column.HeaderText = "E зав \"нам\"";
            column.Width = 70;
            column.ReadOnly = true;
            column.DataPropertyName = "Efor";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Efrom";
            column.HeaderText = "E зав \"от нас\"";
            column.Width = 70;
            column.ReadOnly = true;
            column.DataPropertyName = "Efrom";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Region";
            column.HeaderText = "Рег";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Region";
            stationGridView.Columns.Add(column);
           
            column = new DataGridViewTextBoxColumn();
            column.Name = "ID";
            column.HeaderText = "ID";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "ID";
            stationGridView.Columns.Add(column);
           
            column = new DataGridViewTextBoxColumn();
            column.Name = "Latitude";
            column.HeaderText = "Шир.";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Latitude";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Longitude";
            column.HeaderText = "Довг.";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Longitude";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Standard";
            column.HeaderText = "Стд.";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Standard";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "System";
            column.HeaderText = "Сист.";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = "System";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "PalSecam";
            column.HeaderText = "Напр.";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "PalSecam";
            stationGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "RDecrease";
            column.HeaderText = "R%";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "RDecrease";
            stationGridView.Columns.Add(column);       
    
            //---------
            column = new DataGridViewTextBoxColumn();
            column.Name = "Point";
            column.HeaderText = "Точка";
            column.Width = 40;
            column.ReadOnly = true;
            column.DataPropertyName = "Point";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eintf50";
            column.HeaderText = "E зав 50%";
            column.Width = 95;
            column.ReadOnly = true;
            column.DataPropertyName = "Eintf50";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eintf10";
            column.HeaderText = "E зав 10%";
            column.Width = 95;
            column.ReadOnly = true;
            column.DataPropertyName = "Eintf10";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eintf1";
            column.HeaderText = "E зав 1%";
            column.Width = 90;
            column.ReadOnly = true;
            column.DataPropertyName = "Eintf1";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "PCp";
            column.HeaderText = "ЗВ(П))";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = "PCp";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "PCt";
            column.HeaderText = "ЗВ(Т))";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = "PCt";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "D";
            column.HeaderText = "D(ant)";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = "D";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eused50";
            column.HeaderText = "E вик 50%";
            column.Width = 95;
            column.ReadOnly = true;
            column.DataPropertyName = "Eused50";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eused10";
            column.HeaderText = "E вик 10%";
            column.Width = 95;
            column.ReadOnly = true;
            column.DataPropertyName = "Eused10";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Eused1";
            column.HeaderText = "E вик 1%";
            column.Width = 95;
            column.ReadOnly = true;
            column.DataPropertyName = "Eused1";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "Emin";
            column.HeaderText = "E min";
            column.Width = 80;
            column.ReadOnly = true;
            column.DataPropertyName = "Emin";
            detailGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.Name = "InterferenceType";
            column.HeaderText = "Тип зав";
            column.Width = 70;
            column.ReadOnly = true;
            column.DataPropertyName = "InterferenceType";
            detailGridView.Columns.Add(column);*/
            
        }

        private static int CompareStations(Station x, Station y)
        {
            if (x.GetE_of() == y.GetE_of())
            {
                return 0;   
            }

            if (x.GetE_of() < y.GetE_of())
            {                
                return 1;
            }
            
            return -1;           
        }

        private static int CompareStationLists(List<Station> x, List<Station> y)
        {
            if (x[0].GetE_of() == y[0].GetE_of())
            {
                return 0;                
            }

            if (x[0].GetE_of() < y[0].GetE_of())
            {
                return 1;
            }
            
            return -1;                                     
        }

        private void FrmCalculationResultVrs_Shown(object sender, EventArgs e)
        {
            //StationList.Sort(CompareStations);

            // Цель всего этогого - одна
            // отстортировать список
            //   группы по ADM_KEY при этом не разбивать
            //   сортировать по излучаемой помехе

            Dictionary<string, List<Station>> subDic = new Dictionary<string, List<Station>>();
            
            foreach (Station st in StationList)
            {
                if (!subDic.ContainsKey(st.ADM_KEY))
                {
                    List<Station> subList = new List<Station>();
                    subList.Add(st);
                    subDic.Add(st.ADM_KEY, subList);
                } else
                {
                    List<Station> subList = subDic[st.ADM_KEY];
                    subList.Add(st);                    
                }
            }

            List<List<Station>> superList = new List<List<Station>>();
            foreach (KeyValuePair<string, List<Station>> kvp1 in subDic)
            {
                if (string.IsNullOrEmpty(kvp1.Key))
                {
                    foreach(Station st in  kvp1.Value)
                    {
                        superList.Add(new List<Station>{st});
                    }
                }
                else
                {
                    kvp1.Value.Sort(CompareStations);
                    superList.Add(kvp1.Value);                    
                }
            }
            subDic.Clear();

            superList.Sort(CompareStationLists);

            List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();

            int _number = 1;
            foreach(List<Station> subList in superList)
            {
                if (subList.Count == 1)
                {
                    subList[0].NumberFormat = "N0";
                    subList[0]._number = _number;
                    _number++;

                    lstShowingData.Add(new StationViewRow(subList[0]));
                }
                else
                {
                    double subNumber = 1.0;

                    double E = Math.Pow(10, (Math.Log10(subList.Count)+0.5).Round(0));

                    foreach (Station st in subList)
                    {
                        st.NumberFormat = "N"+(E.ToString().Length-1);
                        st._number = _number + subNumber/E;
                        lstShowingData.Add(new StationViewRow(st));
                        subNumber = subNumber + 1.0;
                    }
                    _number++;
                }
            }
         
            //ПОБУДОВА ВИБІРКИ
            TreeColumnWinForms newTab = TopTable.AddNewTab(CLocaliz.TxT("SAMPLE BUILDING RESULTS"), lstShowingData, "CalculationVRS_TopGrid");
            newTab.TreeColumn.OnSelectedItemChanged += TopGridSelectedItemChanged;            
            newTab.TreeColumn.OnSelectedItemDoubleClick += TopGridMouseDoubleClickRow;
            //foreach (Station st in StationList)
            //{
            //    StationViewList.Add(new StationView(st));
            //}            

            /* stationGridView.DataSource = StationViewList;

            foreach (DataGridViewRow row in stationGridView.Rows)
            {
                row.Height = 19;
            }

            txtStandard.Text = CurrentStation.Standard;
            txtName.Text = CurrentStation.Name;txtReg.Text = CurrentStation.Region;
            txtTVK.Text = CurrentStation.Channel;
            txtFreq.Text = CurrentStation._freq.Round(3).ToString();
            txtERP.Text = CurrentStation._EVP.Round(3).ToString();

            checkDuel_CheckedChanged(sender, e);
            SetDetails(0);
            */

            txtStandard.Text = CurrentStation.Standard;
            txtName.Text = CurrentStation.Name; txtReg.Text = CurrentStation.Region;
            txtTVK.Text = CurrentStation.Channel;
            txtFreq.Text = CurrentStation._freq.Round(3).ToString();
            txtERP.Text = CurrentStation._EVP.Round(3).ToString();
        }

        public void TopGridSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            StationViewRow newRow = e.NewValue as StationViewRow;
            if (newRow!=null)
            {
                txtChannel1.Text = CurrentStation.Channel;
                txtChannel2.Text = newRow.GetStation().Channel;

                txtHeff1.Text = Station.GetHeigh(CurrentStation._TMP_EFHGT, newRow.GetStation().Azimuth1).ToString();
                txtHeff2.Text = Station.GetHeigh(newRow.GetStation()._TMP_EFHGT, newRow.GetStation().Azimuth2).ToString();

                txtOffset1.Text = CurrentStation._SNCH.ToStringNullD();
                    // "0";//CurrentStation_ CurrentStation._EFHGT.ToString();
                txtOffset2.Text = newRow.GetStation()._SNCH.ToStringNullD();

                txtPeff1.Text = newRow.GetStation()._EVP2_for_R.Round(3).ToString();
                txtPeff2.Text = newRow.GetStation()._EVP_for_R.Round(3).ToString();

                txtAzimuth1.Text = newRow.GetStation().Azimuth1.Round(2).ToString();
                txtAzimuth2.Text = newRow.GetStation().Azimuth2.Round(2).ToString();

                txtRadius1.Text = newRow.GetStation().Rn.Round(2).ToString() + " / " + newRow.GetStation().R1n.Round(2).ToString();
                txtRadius2.Text = newRow.GetStation().CRn.Round(2).ToString() + " / " + newRow.GetStation().CR1n.Round(2).ToString();                

                lblOther.Text = newRow.GetStation()._number.ToString() + ":";

                newRow.GetStation().Points[0].Name = "0-1";
                newRow.GetStation().Points[1].Name = "0-2";
                newRow.GetStation().Points[2].Name = newRow.GetStation()._number.ToString()+"-3";
                newRow.GetStation().Points[3].Name = newRow.GetStation()._number.ToString()+"-4";

                SetDetails(newRow.GetStation());            
            }            
        }

        public void TopGridMouseDoubleClickRow(object sender, System.Windows.Input.MouseButtonEventArgs e, TreeColumnRowBase row)
        {
            StationViewRow newRow = row as StationViewRow;

            BaseAppClass obj = BaseAppClass.GetStation(newRow.GetStation()._tableID, newRow.GetStation()._table, 0, false);
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }

        private void checkDuel_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDuel.Checked)
            {
                //Width = 1108;
                Height = 722;
            } 
            else
            {
                //Width = 950;
                Height = 540;
            }
        }

        private void stationGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //SetDetails(e.RowIndex);            
        }

        private void SetDetails(Station station)
        {
            BottomTable.Clear();

            List<TreeColumnRowBase> lstShowingData = new List<TreeColumnRowBase>();

            foreach (Station.PointInfo pi in station.Points)
            {
                StationDetailsRow newDetailRow = new StationDetailsRow();
                newDetailRow.IsCheckBoxVisible = false;
                newDetailRow.Point.Value = pi.Name;
                newDetailRow.Eintf50.Value = pi.Eintf50;
                newDetailRow.Eintf10.Value = pi.Eintf10;
                newDetailRow.Eintf1.Value = pi.Eintf1;
                newDetailRow.Emin.Value = pi.Emin;
                newDetailRow.D.Value = pi.D;
                newDetailRow.PCp.Value = pi.PCp;
                newDetailRow.PCt.Value = pi.PCt;
                newDetailRow.Eused50.Value = pi.Eused50;
                newDetailRow.Eused10.Value = pi.Eused10;
                newDetailRow.Eused1.Value = pi.Eused1;
                newDetailRow.InterferenceType.Value = pi.InterferenceType;
                lstShowingData.Add(newDetailRow);
            }
            TreeColumnWinForms newTab = BottomTable.AddNewTab(CLocaliz.TxT("Duel"), lstShowingData, "CalculationVRS_BottomGrid");                            
        }

        private void FrmCalculationResultVrs_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnStation_Click(object sender, EventArgs e)
        {
            BaseAppClass obj = BaseAppClass.GetStation(CurrentStation._tableID, CurrentStation._table, 0, false);            
            using (MainAppForm formAppl = new MainAppForm(obj))
            {
                formAppl.ShowDialog();
            }
        }

        /// <summary>
        /// Получить список выбраных строчек
        /// </summary>
        /// <returns></returns>
        private List<StationViewRow> GetSelectedRows()
        {
            List<StationViewRow> lstRow = new List<StationViewRow>();

            int indexTab = TopTable.tabControl.SelectedIndex;
            if ((indexTab > -1) && (indexTab < TopTable.ListOfItemSources.Count))
            {
                IEnumerable curItemSource = TopTable.ListOfItemSources[indexTab];
                foreach (object source in curItemSource)
                {
                    StationViewRow tmpSource = source as StationViewRow;
                    if ((tmpSource != null) && (tmpSource.IsChecked == true))
                    {
                        lstRow.Add(tmpSource);
                    }
                }
            }

            return lstRow;            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<StationViewRow> lstRow = GetSelectedRows();
                                
            if (lstRow.Count > 0)            
            {
                List<RecordPtr> lstBag = new List<RecordPtr>();
                List<string> lstCaption = new List<string>();


                List<string> tableString = new List<string>{ICSMTbl.itblFM, ICSMTbl.itblFM, ICSMTbl.itblTDAB, ICSMTbl.itblDVBT };

                foreach(string tableName in tableString)
                {
                    RecordPtr bag = IMBag.CreateTemporary(tableName);
                    lstBag.Add(bag);
                    lstCaption.Add(CLocaliz.TxT(tableName));

                    foreach (StationViewRow row in lstRow)
                    {
                        if (row.GetStation()._table == tableName)
                        {
                            IMRecordset r = new IMRecordset("BG_" + tableName, IMRecordset.Mode.ReadWrite);
                            r.Select("BAG_ID,OBJ_ID");
                            r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                            r.Open();
                            r.AddNew();
                            r.Put("BAG_ID", bag.Id);
                            r.Put("OBJ_ID", row.GetStation()._tableID);
                            r.Update();
                            if (r.IsOpen())
                                r.Close();
                            r.Destroy();
                        }
                    }                    
                }

                RecordPtr bag2 = IMBag.CreateTemporary(CurrentStation._table);
                lstBag.Add(bag2);
                lstCaption.Add(CLocaliz.TxT("Current station"));
                {
                    IMRecordset r = new IMRecordset("BG_" + CurrentStation._table, IMRecordset.Mode.ReadWrite);
                    r.Select("BAG_ID,OBJ_ID");
                    r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                    r.Open();
                    r.AddNew();
                    r.Put("BAG_ID", bag2.Id);
                    r.Put("OBJ_ID", CurrentStation._tableID);
                    r.Update();
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }                
            }
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            List<StationViewRow> lstRow = GetSelectedRows();

            if (lstRow.Count > 0)
            {                
                if (exportToCsvFileDialog.ShowDialog()==DialogResult.OK)
                {
                    StreamWriter sw = new StreamWriter(exportToCsvFileDialog.FileName, true, Encoding.Default);
                    foreach (StationViewRow row in lstRow)
                    {                                                
                        StringBuilder CsvLine = new StringBuilder();

                        CsvLine.Append(row.GetStation()._table);
                        CsvLine.Append(";");
                        CsvLine.Append(row.GetStation()._tableID);
                        CsvLine.Append(";;");
                        CsvLine.Append(row.Number.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Overlap.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Name.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Channel.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Frequency.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Distance.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Azimuth.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.ERP.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.HEff.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Polarization.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Offset.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Mx.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.StateType.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.EOf.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.EFrom.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Region.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Latitude.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Longitude.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.Standard.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.System.Value);
                        CsvLine.Append(";");
                        CsvLine.Append(row.PalSecam.Value);
                        CsvLine.Append(";");                        
                        CsvLine.Append(row.RDecrease.Value);
                        
                        sw.WriteLine(CsvLine.ToString());
                    }                    
                    sw.Close();
                }
            }
            else
                System.Windows.Forms.MessageBox.Show("Please, select the stations");

        }
    }
}
