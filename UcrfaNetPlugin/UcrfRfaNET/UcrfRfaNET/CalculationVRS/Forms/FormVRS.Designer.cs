﻿namespace XICSM.UcrfRfaNET.CalculationVRS
{
   partial class FormVRS
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.BtnCalculation = new System.Windows.Forms.Button();
          this.TbxRadius = new System.Windows.Forms.TextBox();
          this.ChbFM = new System.Windows.Forms.CheckBox();
          this.ChbTV = new System.Windows.Forms.CheckBox();
          this.ChbDVBT = new System.Windows.Forms.CheckBox();
          this.ChbTDAB = new System.Windows.Forms.CheckBox();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.checkBox4 = new System.Windows.Forms.CheckBox();
          this.checkBox3 = new System.Windows.Forms.CheckBox();
          this.checkBox2 = new System.Windows.Forms.CheckBox();
          this.checkBox1 = new System.Windows.Forms.CheckBox();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.label1 = new System.Windows.Forms.Label();
          this.BtnExit = new System.Windows.Forms.Button();
          this.checkBox5 = new System.Windows.Forms.CheckBox();
          this.groupBox1.SuspendLayout();
          this.groupBox2.SuspendLayout();
          this.SuspendLayout();
          // 
          // BtnCalculation
          // 
          this.BtnCalculation.Location = new System.Drawing.Point(39, 318);
          this.BtnCalculation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.BtnCalculation.Name = "BtnCalculation";
          this.BtnCalculation.Size = new System.Drawing.Size(86, 23);
          this.BtnCalculation.TabIndex = 0;
          this.BtnCalculation.Text = "Розрахунок";
          this.BtnCalculation.UseVisualStyleBackColor = true;
          this.BtnCalculation.Click += new System.EventHandler(this.BtnCalculation_Click);
          // 
          // TbxRadius
          // 
          this.TbxRadius.Location = new System.Drawing.Point(148, 21);
          this.TbxRadius.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.TbxRadius.Name = "TbxRadius";
          this.TbxRadius.Size = new System.Drawing.Size(76, 20);
          this.TbxRadius.TabIndex = 1;
          this.TbxRadius.Text = "500";
          this.TbxRadius.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tbx_KeyPressOnDigit);
          // 
          // ChbFM
          // 
          this.ChbFM.AutoSize = true;
          this.ChbFM.Checked = true;
          this.ChbFM.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbFM.Location = new System.Drawing.Point(13, 44);
          this.ChbFM.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.ChbFM.Name = "ChbFM";
          this.ChbFM.Size = new System.Drawing.Size(41, 17);
          this.ChbFM.TabIndex = 4;
          this.ChbFM.Text = "FM";
          this.ChbFM.UseVisualStyleBackColor = true;
          // 
          // ChbTV
          // 
          this.ChbTV.AutoSize = true;
          this.ChbTV.Checked = true;
          this.ChbTV.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbTV.Location = new System.Drawing.Point(13, 66);
          this.ChbTV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.ChbTV.Name = "ChbTV";
          this.ChbTV.Size = new System.Drawing.Size(96, 17);
          this.ChbTV.TabIndex = 5;
          this.ChbTV.Text = "Аналогові TV ";
          this.ChbTV.UseVisualStyleBackColor = true;
          // 
          // ChbDVBT
          // 
          this.ChbDVBT.AutoSize = true;
          this.ChbDVBT.Checked = true;
          this.ChbDVBT.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbDVBT.Location = new System.Drawing.Point(13, 110);
          this.ChbDVBT.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.ChbDVBT.Name = "ChbDVBT";
          this.ChbDVBT.Size = new System.Drawing.Size(58, 17);
          this.ChbDVBT.TabIndex = 6;
          this.ChbDVBT.Text = "DVB-T";
          this.ChbDVBT.UseVisualStyleBackColor = true;
          // 
          // ChbTDAB
          // 
          this.ChbTDAB.AutoSize = true;
          this.ChbTDAB.Checked = true;
          this.ChbTDAB.CheckState = System.Windows.Forms.CheckState.Checked;
          this.ChbTDAB.Location = new System.Drawing.Point(13, 88);
          this.ChbTDAB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.ChbTDAB.Name = "ChbTDAB";
          this.ChbTDAB.Size = new System.Drawing.Size(58, 17);
          this.ChbTDAB.TabIndex = 7;
          this.ChbTDAB.Text = "T-DAB";
          this.ChbTDAB.UseVisualStyleBackColor = true;
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.checkBox4);
          this.groupBox1.Controls.Add(this.checkBox3);
          this.groupBox1.Controls.Add(this.checkBox2);
          this.groupBox1.Controls.Add(this.checkBox1);
          this.groupBox1.Controls.Add(this.ChbTDAB);
          this.groupBox1.Controls.Add(this.ChbFM);
          this.groupBox1.Controls.Add(this.ChbDVBT);
          this.groupBox1.Controls.Add(this.ChbTV);
          this.groupBox1.Location = new System.Drawing.Point(9, 10);
          this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.groupBox1.Size = new System.Drawing.Size(236, 198);
          this.groupBox1.TabIndex = 8;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Вибір категорій станцій які залучаються до розрахунків";
          // 
          // checkBox4
          // 
          this.checkBox4.AutoSize = true;
          this.checkBox4.Location = new System.Drawing.Point(134, 132);
          this.checkBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.checkBox4.Name = "checkBox4";
          this.checkBox4.Size = new System.Drawing.Size(63, 17);
          this.checkBox4.TabIndex = 11;
          this.checkBox4.Text = "BR IFIC";
          this.checkBox4.UseVisualStyleBackColor = true;
          // 
          // checkBox3
          // 
          this.checkBox3.AutoSize = true;
          this.checkBox3.Location = new System.Drawing.Point(13, 176);
          this.checkBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.checkBox3.Name = "checkBox3";
          this.checkBox3.Size = new System.Drawing.Size(121, 17);
          this.checkBox3.TabIndex = 10;
          this.checkBox3.Text = "Еталонна ситуацію";
          this.checkBox3.UseVisualStyleBackColor = true;
          // 
          // checkBox2
          // 
          this.checkBox2.AutoSize = true;
          this.checkBox2.Checked = true;
          this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
          this.checkBox2.Location = new System.Drawing.Point(13, 154);
          this.checkBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.checkBox2.Name = "checkBox2";
          this.checkBox2.Size = new System.Drawing.Size(133, 17);
          this.checkBox2.TabIndex = 9;
          this.checkBox2.Text = "Частотні присвоєння";
          this.checkBox2.UseVisualStyleBackColor = true;
          // 
          // checkBox1
          // 
          this.checkBox1.AutoSize = true;
          this.checkBox1.Checked = true;
          this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
          this.checkBox1.Location = new System.Drawing.Point(13, 132);
          this.checkBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.checkBox1.Name = "checkBox1";
          this.checkBox1.Size = new System.Drawing.Size(77, 17);
          this.checkBox1.TabIndex = 8;
          this.checkBox1.Text = "Виділення";
          this.checkBox1.UseVisualStyleBackColor = true;
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.checkBox5);
          this.groupBox2.Controls.Add(this.label1);
          this.groupBox2.Controls.Add(this.TbxRadius);
          this.groupBox2.Location = new System.Drawing.Point(9, 212);
          this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.groupBox2.Size = new System.Drawing.Size(236, 99);
          this.groupBox2.TabIndex = 9;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "Завдання параметрів розрахунку";
          // 
          // label1
          // 
          this.label1.Location = new System.Drawing.Point(4, 21);
          this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(123, 30);
          this.label1.TabIndex = 2;
          this.label1.Text = "Максимальна відстань пошуку завад [км]";
          // 
          // BtnExit
          // 
          this.BtnExit.Location = new System.Drawing.Point(129, 318);
          this.BtnExit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.BtnExit.Name = "BtnExit";
          this.BtnExit.Size = new System.Drawing.Size(86, 23);
          this.BtnExit.TabIndex = 10;
          this.BtnExit.Text = "Вийти";
          this.BtnExit.UseVisualStyleBackColor = true;
          this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
          // 
          // checkBox5
          // 
          this.checkBox5.AutoSize = true;
          this.checkBox5.Location = new System.Drawing.Point(7, 67);
          this.checkBox5.Name = "checkBox5";
          this.checkBox5.Size = new System.Drawing.Size(116, 17);
          this.checkBox5.TabIndex = 3;
          this.checkBox5.Text = "Враховувати воду";
          this.checkBox5.UseVisualStyleBackColor = true;
          // 
          // FormVRS
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(254, 352);
          this.Controls.Add(this.BtnExit);
          this.Controls.Add(this.groupBox2);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.BtnCalculation);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
          this.Name = "FormVRS";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "FormVRS";
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button BtnCalculation;
      private System.Windows.Forms.TextBox TbxRadius;
      private System.Windows.Forms.CheckBox ChbFM;
      private System.Windows.Forms.CheckBox ChbTV;
      private System.Windows.Forms.CheckBox ChbDVBT;
      private System.Windows.Forms.CheckBox ChbTDAB;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.CheckBox checkBox1;
      private System.Windows.Forms.CheckBox checkBox2;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button BtnExit;
      private System.Windows.Forms.CheckBox checkBox4;
      private System.Windows.Forms.CheckBox checkBox3;
      private System.Windows.Forms.CheckBox checkBox5;
   }
}