﻿namespace XICSM.UcrfRfaNET.CalculationVRS
{
    partial class FrmCalculationResultVrs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnToIcaTelecom = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.checkDuel = new System.Windows.Forms.CheckBox();
            this.btnStation = new System.Windows.Forms.Button();
            this.lblStandard = new System.Windows.Forms.Label();
            this.txtStandard = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtReg = new System.Windows.Forms.TextBox();
            this.lblReg = new System.Windows.Forms.Label();
            this.txtTVK = new System.Windows.Forms.TextBox();
            this.lblTVK = new System.Windows.Forms.Label();
            this.txtFreq = new System.Windows.Forms.TextBox();
            this.lblFreq = new System.Windows.Forms.Label();
            this.txtERP = new System.Windows.Forms.TextBox();
            this.lblERP = new System.Windows.Forms.Label();
            this.txtChannel1 = new System.Windows.Forms.TextBox();
            this.lblTVK1 = new System.Windows.Forms.Label();
            this.txtChannel2 = new System.Windows.Forms.TextBox();
            this.lblTVK2 = new System.Windows.Forms.Label();
            this.lblZero = new System.Windows.Forms.Label();
            this.lblOther = new System.Windows.Forms.Label();
            this.txtHeff2 = new System.Windows.Forms.TextBox();
            this.lblHEff2 = new System.Windows.Forms.Label();
            this.txtHeff1 = new System.Windows.Forms.TextBox();
            this.lblHEff1 = new System.Windows.Forms.Label();
            this.txtRadius2 = new System.Windows.Forms.TextBox();
            this.lblRadius2 = new System.Windows.Forms.Label();
            this.txtRadius1 = new System.Windows.Forms.TextBox();
            this.lblRadius1 = new System.Windows.Forms.Label();
            this.txtOffset2 = new System.Windows.Forms.TextBox();
            this.lbOffset2 = new System.Windows.Forms.Label();
            this.txtOffset1 = new System.Windows.Forms.TextBox();
            this.lbOffset1 = new System.Windows.Forms.Label();
            this.txtPeff2 = new System.Windows.Forms.TextBox();
            this.lblPeff2 = new System.Windows.Forms.Label();
            this.txtPeff1 = new System.Windows.Forms.TextBox();
            this.lblPeff1 = new System.Windows.Forms.Label();
            this.txtAzimuth2 = new System.Windows.Forms.TextBox();
            this.lblAzimuth2 = new System.Windows.Forms.Label();
            this.txtAzimuth1 = new System.Windows.Forms.TextBox();
            this.lblAzimuth1 = new System.Windows.Forms.Label();
            this.topPanel = new System.Windows.Forms.Panel();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.exportToCsvFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(178, 475);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(118, 23);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Export to CSV";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnToIcaTelecom
            // 
            this.btnToIcaTelecom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnToIcaTelecom.Location = new System.Drawing.Point(-641, 311);
            this.btnToIcaTelecom.Name = "btnToIcaTelecom";
            this.btnToIcaTelecom.Size = new System.Drawing.Size(96, 23);
            this.btnToIcaTelecom.TabIndex = 4;
            this.btnToIcaTelecom.Text = "To ICSTelecom";
            this.btnToIcaTelecom.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(76, 475);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(96, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "To ICSTelecom";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkDuel
            // 
            this.checkDuel.AutoSize = true;
            this.checkDuel.Location = new System.Drawing.Point(12, 479);
            this.checkDuel.Name = "checkDuel";
            this.checkDuel.Size = new System.Drawing.Size(58, 17);
            this.checkDuel.TabIndex = 7;
            this.checkDuel.Text = "Дуель";
            this.checkDuel.UseVisualStyleBackColor = true;
            this.checkDuel.CheckedChanged += new System.EventHandler(this.checkDuel_CheckedChanged);
            // 
            // btnStation
            // 
            this.btnStation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStation.Location = new System.Drawing.Point(1015, 12);
            this.btnStation.Name = "btnStation";
            this.btnStation.Size = new System.Drawing.Size(75, 23);
            this.btnStation.TabIndex = 9;
            this.btnStation.Text = "Станція";
            this.btnStation.UseVisualStyleBackColor = true;
            this.btnStation.Click += new System.EventHandler(this.btnStation_Click);
            // 
            // lblStandard
            // 
            this.lblStandard.AutoSize = true;
            this.lblStandard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStandard.Location = new System.Drawing.Point(9, 17);
            this.lblStandard.Name = "lblStandard";
            this.lblStandard.Size = new System.Drawing.Size(59, 13);
            this.lblStandard.TabIndex = 10;
            this.lblStandard.Text = "Радіотех.: ";
            // 
            // txtStandard
            // 
            this.txtStandard.Location = new System.Drawing.Point(85, 14);
            this.txtStandard.Name = "txtStandard";
            this.txtStandard.ReadOnly = true;
            this.txtStandard.Size = new System.Drawing.Size(67, 20);
            this.txtStandard.TabIndex = 11;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblName.Location = new System.Drawing.Point(159, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(42, 13);
            this.lblName.TabIndex = 12;
            this.lblName.Text = "Назва:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(213, 14);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(52, 20);
            this.txtName.TabIndex = 13;
            // 
            // txtReg
            // 
            this.txtReg.Location = new System.Drawing.Point(310, 14);
            this.txtReg.Name = "txtReg";
            this.txtReg.ReadOnly = true;
            this.txtReg.Size = new System.Drawing.Size(52, 20);
            this.txtReg.TabIndex = 15;
            // 
            // lblReg
            // 
            this.lblReg.AutoSize = true;
            this.lblReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblReg.Location = new System.Drawing.Point(270, 18);
            this.lblReg.Name = "lblReg";
            this.lblReg.Size = new System.Drawing.Size(30, 13);
            this.lblReg.TabIndex = 14;
            this.lblReg.Text = "РЕГ:";
            // 
            // txtTVK
            // 
            this.txtTVK.Location = new System.Drawing.Point(407, 14);
            this.txtTVK.Name = "txtTVK";
            this.txtTVK.ReadOnly = true;
            this.txtTVK.Size = new System.Drawing.Size(52, 20);
            this.txtTVK.TabIndex = 17;
            // 
            // lblTVK
            // 
            this.lblTVK.AutoSize = true;
            this.lblTVK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTVK.Location = new System.Drawing.Point(367, 18);
            this.lblTVK.Name = "lblTVK";
            this.lblTVK.Size = new System.Drawing.Size(31, 13);
            this.lblTVK.TabIndex = 16;
            this.lblTVK.Text = "ТВК:";
            // 
            // txtFreq
            // 
            this.txtFreq.Location = new System.Drawing.Point(563, 14);
            this.txtFreq.Name = "txtFreq";
            this.txtFreq.ReadOnly = true;
            this.txtFreq.Size = new System.Drawing.Size(52, 20);
            this.txtFreq.TabIndex = 19;
            // 
            // lblFreq
            // 
            this.lblFreq.AutoSize = true;
            this.lblFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFreq.Location = new System.Drawing.Point(465, 18);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(79, 13);
            this.lblFreq.TabIndex = 18;
            this.lblFreq.Text = "Частота, МГц:";
            // 
            // txtERP
            // 
            this.txtERP.Location = new System.Drawing.Point(703, 14);
            this.txtERP.Name = "txtERP";
            this.txtERP.ReadOnly = true;
            this.txtERP.Size = new System.Drawing.Size(52, 20);
            this.txtERP.TabIndex = 21;
            // 
            // lblERP
            // 
            this.lblERP.AutoSize = true;
            this.lblERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblERP.Location = new System.Drawing.Point(623, 18);
            this.lblERP.Name = "lblERP";
            this.lblERP.Size = new System.Drawing.Size(64, 13);
            this.lblERP.TabIndex = 20;
            this.lblERP.Text = "ЕВП, макс:";
            // 
            // txtChannel1
            // 
            this.txtChannel1.Location = new System.Drawing.Point(104, 521);
            this.txtChannel1.Name = "txtChannel1";
            this.txtChannel1.ReadOnly = true;
            this.txtChannel1.Size = new System.Drawing.Size(46, 20);
            this.txtChannel1.TabIndex = 23;
            // 
            // lblTVK1
            // 
            this.lblTVK1.AutoSize = true;
            this.lblTVK1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTVK1.Location = new System.Drawing.Point(64, 524);
            this.lblTVK1.Name = "lblTVK1";
            this.lblTVK1.Size = new System.Drawing.Size(34, 13);
            this.lblTVK1.TabIndex = 22;
            this.lblTVK1.Text = "ТВК: ";
            // 
            // txtChannel2
            // 
            this.txtChannel2.Location = new System.Drawing.Point(104, 547);
            this.txtChannel2.Name = "txtChannel2";
            this.txtChannel2.ReadOnly = true;
            this.txtChannel2.Size = new System.Drawing.Size(46, 20);
            this.txtChannel2.TabIndex = 25;
            this.txtChannel2.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // lblTVK2
            // 
            this.lblTVK2.AutoSize = true;
            this.lblTVK2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTVK2.Location = new System.Drawing.Point(64, 550);
            this.lblTVK2.Name = "lblTVK2";
            this.lblTVK2.Size = new System.Drawing.Size(34, 13);
            this.lblTVK2.TabIndex = 24;
            this.lblTVK2.Text = "ТВК: ";
            // 
            // lblZero
            // 
            this.lblZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZero.Location = new System.Drawing.Point(12, 522);
            this.lblZero.Name = "lblZero";
            this.lblZero.Size = new System.Drawing.Size(46, 16);
            this.lblZero.TabIndex = 26;
            this.lblZero.Text = "0:";
            this.lblZero.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOther
            // 
            this.lblOther.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOther.Location = new System.Drawing.Point(12, 547);
            this.lblOther.Name = "lblOther";
            this.lblOther.Size = new System.Drawing.Size(46, 16);
            this.lblOther.TabIndex = 27;
            this.lblOther.Text = "0:";
            this.lblOther.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtHeff2
            // 
            this.txtHeff2.Location = new System.Drawing.Point(203, 547);
            this.txtHeff2.Name = "txtHeff2";
            this.txtHeff2.ReadOnly = true;
            this.txtHeff2.Size = new System.Drawing.Size(46, 20);
            this.txtHeff2.TabIndex = 31;
            // 
            // lblHEff2
            // 
            this.lblHEff2.AutoSize = true;
            this.lblHEff2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHEff2.Location = new System.Drawing.Point(156, 550);
            this.lblHEff2.Name = "lblHEff2";
            this.lblHEff2.Size = new System.Drawing.Size(41, 13);
            this.lblHEff2.TabIndex = 30;
            this.lblHEff2.Text = "Н(еф): ";
            // 
            // txtHeff1
            // 
            this.txtHeff1.Location = new System.Drawing.Point(203, 521);
            this.txtHeff1.Name = "txtHeff1";
            this.txtHeff1.ReadOnly = true;
            this.txtHeff1.Size = new System.Drawing.Size(46, 20);
            this.txtHeff1.TabIndex = 29;
            // 
            // lblHEff1
            // 
            this.lblHEff1.AutoSize = true;
            this.lblHEff1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHEff1.Location = new System.Drawing.Point(156, 524);
            this.lblHEff1.Name = "lblHEff1";
            this.lblHEff1.Size = new System.Drawing.Size(41, 13);
            this.lblHEff1.TabIndex = 28;
            this.lblHEff1.Text = "Н(еф): ";
            // 
            // txtRadius2
            // 
            this.txtRadius2.Location = new System.Drawing.Point(672, 547);
            this.txtRadius2.Name = "txtRadius2";
            this.txtRadius2.ReadOnly = true;
            this.txtRadius2.Size = new System.Drawing.Size(137, 20);
            this.txtRadius2.TabIndex = 35;
            this.txtRadius2.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // lblRadius2
            // 
            this.lblRadius2.AutoSize = true;
            this.lblRadius2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRadius2.Location = new System.Drawing.Point(574, 550);
            this.lblRadius2.Name = "lblRadius2";
            this.lblRadius2.Size = new System.Drawing.Size(92, 13);
            this.lblRadius2.TabIndex = 34;
            this.lblRadius2.Text = "Радіус зони, км: ";
            // 
            // txtRadius1
            // 
            this.txtRadius1.Location = new System.Drawing.Point(672, 521);
            this.txtRadius1.Name = "txtRadius1";
            this.txtRadius1.ReadOnly = true;
            this.txtRadius1.Size = new System.Drawing.Size(137, 20);
            this.txtRadius1.TabIndex = 33;
            // 
            // lblRadius1
            // 
            this.lblRadius1.AutoSize = true;
            this.lblRadius1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRadius1.Location = new System.Drawing.Point(574, 524);
            this.lblRadius1.Name = "lblRadius1";
            this.lblRadius1.Size = new System.Drawing.Size(92, 13);
            this.lblRadius1.TabIndex = 32;
            this.lblRadius1.Text = "Радіус зони, км: ";
            this.lblRadius1.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtOffset2
            // 
            this.txtOffset2.Location = new System.Drawing.Point(299, 547);
            this.txtOffset2.Name = "txtOffset2";
            this.txtOffset2.ReadOnly = true;
            this.txtOffset2.Size = new System.Drawing.Size(46, 20);
            this.txtOffset2.TabIndex = 39;
            // 
            // lbOffset2
            // 
            this.lbOffset2.AutoSize = true;
            this.lbOffset2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbOffset2.Location = new System.Drawing.Point(257, 550);
            this.lbOffset2.Name = "lbOffset2";
            this.lbOffset2.Size = new System.Drawing.Size(36, 13);
            this.lbOffset2.TabIndex = 38;
            this.lbOffset2.Text = "ЗНЧ: ";
            // 
            // txtOffset1
            // 
            this.txtOffset1.Location = new System.Drawing.Point(299, 521);
            this.txtOffset1.Name = "txtOffset1";
            this.txtOffset1.ReadOnly = true;
            this.txtOffset1.Size = new System.Drawing.Size(46, 20);
            this.txtOffset1.TabIndex = 37;
            // 
            // lbOffset1
            // 
            this.lbOffset1.AutoSize = true;
            this.lbOffset1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbOffset1.Location = new System.Drawing.Point(257, 524);
            this.lbOffset1.Name = "lbOffset1";
            this.lbOffset1.Size = new System.Drawing.Size(36, 13);
            this.lbOffset1.TabIndex = 36;
            this.lbOffset1.Text = "ЗНЧ: ";
            // 
            // txtPeff2
            // 
            this.txtPeff2.Location = new System.Drawing.Point(397, 547);
            this.txtPeff2.Name = "txtPeff2";
            this.txtPeff2.ReadOnly = true;
            this.txtPeff2.Size = new System.Drawing.Size(46, 20);
            this.txtPeff2.TabIndex = 43;
            // 
            // lblPeff2
            // 
            this.lblPeff2.AutoSize = true;
            this.lblPeff2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPeff2.Location = new System.Drawing.Point(351, 550);
            this.lblPeff2.Name = "lblPeff2";
            this.lblPeff2.Size = new System.Drawing.Size(40, 13);
            this.lblPeff2.TabIndex = 42;
            this.lblPeff2.Text = "P(еф): ";
            // 
            // txtPeff1
            // 
            this.txtPeff1.Location = new System.Drawing.Point(397, 521);
            this.txtPeff1.Name = "txtPeff1";
            this.txtPeff1.ReadOnly = true;
            this.txtPeff1.Size = new System.Drawing.Size(46, 20);
            this.txtPeff1.TabIndex = 41;
            // 
            // lblPeff1
            // 
            this.lblPeff1.AutoSize = true;
            this.lblPeff1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPeff1.Location = new System.Drawing.Point(351, 524);
            this.lblPeff1.Name = "lblPeff1";
            this.lblPeff1.Size = new System.Drawing.Size(40, 13);
            this.lblPeff1.TabIndex = 40;
            this.lblPeff1.Text = "P(еф): ";
            // 
            // txtAzimuth2
            // 
            this.txtAzimuth2.Location = new System.Drawing.Point(519, 547);
            this.txtAzimuth2.Name = "txtAzimuth2";
            this.txtAzimuth2.ReadOnly = true;
            this.txtAzimuth2.Size = new System.Drawing.Size(46, 20);
            this.txtAzimuth2.TabIndex = 47;
            this.txtAzimuth2.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // lblAzimuth2
            // 
            this.lblAzimuth2.AutoSize = true;
            this.lblAzimuth2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAzimuth2.Location = new System.Drawing.Point(453, 550);
            this.lblAzimuth2.Name = "lblAzimuth2";
            this.lblAzimuth2.Size = new System.Drawing.Size(61, 13);
            this.lblAzimuth2.TabIndex = 46;
            this.lblAzimuth2.Text = "Г, азимут: ";
            this.lblAzimuth2.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtAzimuth1
            // 
            this.txtAzimuth1.Location = new System.Drawing.Point(519, 521);
            this.txtAzimuth1.Name = "txtAzimuth1";
            this.txtAzimuth1.ReadOnly = true;
            this.txtAzimuth1.Size = new System.Drawing.Size(46, 20);
            this.txtAzimuth1.TabIndex = 45;
            this.txtAzimuth1.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // lblAzimuth1
            // 
            this.lblAzimuth1.AutoSize = true;
            this.lblAzimuth1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAzimuth1.Location = new System.Drawing.Point(453, 524);
            this.lblAzimuth1.Name = "lblAzimuth1";
            this.lblAzimuth1.Size = new System.Drawing.Size(61, 13);
            this.lblAzimuth1.TabIndex = 44;
            this.lblAzimuth1.Text = "Г, азимут: ";
            this.lblAzimuth1.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // topPanel
            // 
            this.topPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.topPanel.Location = new System.Drawing.Point(9, 40);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1081, 429);
            this.topPanel.TabIndex = 48;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.bottomPanel.Location = new System.Drawing.Point(9, 573);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(1081, 104);
            this.bottomPanel.TabIndex = 49;
            // 
            // exportToCsvFileDialog
            // 
            this.exportToCsvFileDialog.Filter = "СSV файли|*.csv|Будь-які файли|*.*||";
            this.exportToCsvFileDialog.Title = "Експортувати";
            // 
            // FrmCalculationResultVrs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 689);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.txtAzimuth2);
            this.Controls.Add(this.lblAzimuth2);
            this.Controls.Add(this.txtAzimuth1);
            this.Controls.Add(this.lblAzimuth1);
            this.Controls.Add(this.txtPeff2);
            this.Controls.Add(this.lblPeff2);
            this.Controls.Add(this.txtPeff1);
            this.Controls.Add(this.lblPeff1);
            this.Controls.Add(this.txtOffset2);
            this.Controls.Add(this.lbOffset2);
            this.Controls.Add(this.txtOffset1);
            this.Controls.Add(this.lbOffset1);
            this.Controls.Add(this.txtRadius2);
            this.Controls.Add(this.lblRadius2);
            this.Controls.Add(this.txtRadius1);
            this.Controls.Add(this.lblRadius1);
            this.Controls.Add(this.txtHeff2);
            this.Controls.Add(this.lblHEff2);
            this.Controls.Add(this.txtHeff1);
            this.Controls.Add(this.lblHEff1);
            this.Controls.Add(this.lblOther);
            this.Controls.Add(this.lblZero);
            this.Controls.Add(this.txtChannel2);
            this.Controls.Add(this.lblTVK2);
            this.Controls.Add(this.txtChannel1);
            this.Controls.Add(this.lblTVK1);
            this.Controls.Add(this.txtERP);
            this.Controls.Add(this.lblERP);
            this.Controls.Add(this.txtFreq);
            this.Controls.Add(this.lblFreq);
            this.Controls.Add(this.txtTVK);
            this.Controls.Add(this.lblTVK);
            this.Controls.Add(this.txtReg);
            this.Controls.Add(this.lblReg);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtStandard);
            this.Controls.Add(this.lblStandard);
            this.Controls.Add(this.btnStation);
            this.Controls.Add(this.checkDuel);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnToIcaTelecom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCalculationResultVrs";
            this.Text = "Calculation result";
            this.Load += new System.EventHandler(this.FrmCalculationResultVrs_Load);
            this.Shown += new System.EventHandler(this.FrmCalculationResultVrs_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnToIcaTelecom;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.CheckBox checkDuel;
        private System.Windows.Forms.Button btnStation;
        private System.Windows.Forms.Label lblStandard;
        private System.Windows.Forms.TextBox txtStandard;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtReg;
        private System.Windows.Forms.Label lblReg;
        private System.Windows.Forms.TextBox txtTVK;
        private System.Windows.Forms.Label lblTVK;
        private System.Windows.Forms.TextBox txtFreq;
        private System.Windows.Forms.Label lblFreq;
        private System.Windows.Forms.TextBox txtERP;
        private System.Windows.Forms.Label lblERP;
        private System.Windows.Forms.TextBox txtChannel1;
        private System.Windows.Forms.Label lblTVK1;
        private System.Windows.Forms.TextBox txtChannel2;
        private System.Windows.Forms.Label lblTVK2;
        private System.Windows.Forms.Label lblZero;
        private System.Windows.Forms.Label lblOther;
        private System.Windows.Forms.TextBox txtHeff2;
        private System.Windows.Forms.Label lblHEff2;
        private System.Windows.Forms.TextBox txtHeff1;
        private System.Windows.Forms.Label lblHEff1;
        private System.Windows.Forms.TextBox txtRadius2;
        private System.Windows.Forms.Label lblRadius2;
        private System.Windows.Forms.TextBox txtRadius1;
        private System.Windows.Forms.Label lblRadius1;
        private System.Windows.Forms.TextBox txtOffset2;
        private System.Windows.Forms.Label lbOffset2;
        private System.Windows.Forms.TextBox txtOffset1;
        private System.Windows.Forms.Label lbOffset1;
        private System.Windows.Forms.TextBox txtPeff2;
        private System.Windows.Forms.Label lblPeff2;
        private System.Windows.Forms.TextBox txtPeff1;
        private System.Windows.Forms.Label lblPeff1;
        private System.Windows.Forms.TextBox txtAzimuth2;
        private System.Windows.Forms.Label lblAzimuth2;
        private System.Windows.Forms.TextBox txtAzimuth1;
        private System.Windows.Forms.Label lblAzimuth1;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.SaveFileDialog exportToCsvFileDialog;
    }
}