﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;
using RSAGeography;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    partial class ViborkaVRS_1546
    {
        private RSARSTerrainInfoClass terrainInfo;
        private RSAGeoPath geoPath;

        private void InitRSGeography()
        {
            terrainInfo = new RSARSTerrainInfoClass();
            string[] initParams = new string[2];
            initParams[0] = HelpClasses.PluginSetting.PluginFolderSetting.RsaGeographyMapsIni;
            initParams[1] = HelpClasses.PluginSetting.PluginFolderSetting.RsaGeographyRelief;
            terrainInfo.Init(initParams);

            geoPath = new RSAGeoPath();
            geoPath.Init(terrainInfo);

            TRSAPathParams pathParams = new TRSAPathParams();
            pathParams.CalcHEff = 1;
            pathParams.CalcRxClearance = 1;
            pathParams.CalcTxClearance = 1;
            pathParams.CalcSeaPercent = 1;
            pathParams.Step = 10;

            geoPath.Set_Params(pathParams);
        }

        public void ShowResult()
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("FillingStations")))
            {
                pBar.UserCanceled();

                List<RecordPtr> listData = new List<RecordPtr>();
                List<string> listCaption = new List<string>();

                if (_needTV)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of TV"));
                    listData.Add(FillBagStations(_stListZhertvaTV, Station.TypeOfStation.TV, TypeOfFrequency.Zhertva));
                    listCaption.Add("Analog TV");
                }
                if (_needFM)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of FM"));
                    listData.Add(FillBagStations(_stListZhertvaFM, Station.TypeOfStation.FM, TypeOfFrequency.Zhertva));
                    listCaption.Add("FM");
                }
                if (_needDVBT)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of DVBT"));
                    listData.Add(FillBagStations(_stListZhertvaDVBT, Station.TypeOfStation.DVBT, TypeOfFrequency.Zhertva));
                    listCaption.Add("DVB-T");
                }
                if (_needTDAB)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of TDAB"));
                    listData.Add(FillBagStations(_stListZhertvaTDAB, Station.TypeOfStation.TDAB, TypeOfFrequency.Zhertva));
                    listCaption.Add("T-DAB");
                }

                //------------------
                //Добавляем ислед. станцию
                listData.Add(FillBagStations(new List<Station>() { _curStat }, _curStat._typeOfStation, TypeOfFrequency.Pomeha));
                listCaption.Add("Текущая станция");

                string position = string.Empty;
                string numberChan = string.Empty;
                string standart = string.Empty;
                double height = 0;
                string type = string.Empty;

                if (_curStat._typeOfStation == Station.TypeOfStation.TV)
                    type = ICSMTbl.itblTV;
                if (_curStat._typeOfStation == Station.TypeOfStation.TDAB)
                    type = ICSMTbl.itblTDAB;
                if (_curStat._typeOfStation == Station.TypeOfStation.FM)
                    type = ICSMTbl.itblFM;
                if (_curStat._typeOfStation == Station.TypeOfStation.DVBT)
                    type = ICSMTbl.itblDVBT;

                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(type, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,Position.NAME,Channel.CHANNEL,STANDARD,AGL");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _curStat._tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        position = r.GetS("Position.NAME");
                        numberChan = r.GetS("Channel.CHANNEL");
                        standart = r.GetS("STANDARD");
                        height = r.GetD("AGL");
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }

                StringBuilder str = new StringBuilder();
                str.Append(position);
                str.Append(";  ");

                str.Append(numberChan);
                str.Append(" (");
                str.Append(_curStat._freq);
                str.Append(" MHz");
                str.Append(")");
                str.Append("\r\n");

                str.Append(standart);
                str.Append("; ");
                str.Append("\r\n");

                str.Append("Longitude ");
                str.Append(_curStat._longitude);
                str.Append("; ");

                str.Append("Latitude ");
                str.Append(_curStat._latitude);
                str.Append("; ");
                str.Append("\r\n");

                str.Append("Power ");
                str.Append(_curStat._power);
                str.Append(" (dBW); ");

                str.Append("Height ");
                str.Append(height);
                str.Append("; ");

                str.Append("Gain ");
                str.Append(_curStat._gMax);
                str.Append("; ");

                IMBag.Display2(listData.ToArray(), listCaption.ToArray(), str.ToString(), "Calculation VRS");
                foreach (RecordPtr h in listData)
                {
                    IMBag.DeleteBag(h);
                }
            }
        }

        public RecordPtr FillBagStations(List<Station> stList, Station.TypeOfStation typeOfStation, TypeOfFrequency typeOfFrequency)
        {
            IMRecordset r = null;
            RecordPtr b1 = new RecordPtr();
            try
            {
                switch (typeOfStation)
                {
                    case Station.TypeOfStation.TV:
                        b1 = IMBag.CreateTemporary(ICSMTbl.itblTV);
                        r = new IMRecordset(ICSMTbl.itblBGTV, IMRecordset.Mode.ReadWrite);
                        break;

                    case Station.TypeOfStation.FM:
                        b1 = IMBag.CreateTemporary(ICSMTbl.itblFM);
                        r = new IMRecordset(ICSMTbl.itblBGFM, IMRecordset.Mode.ReadWrite);
                        break;

                    case Station.TypeOfStation.TDAB:
                        b1 = IMBag.CreateTemporary(ICSMTbl.itblTDAB);
                        r = new IMRecordset(ICSMTbl.itblBGTDAB, IMRecordset.Mode.ReadWrite);
                        break;

                    case Station.TypeOfStation.DVBT:
                        b1 = IMBag.CreateTemporary(ICSMTbl.itblDVBT);
                        r = new IMRecordset(ICSMTbl.itblBGDVBT, IMRecordset.Mode.ReadWrite);
                        break;

                    default:
                        throw new Exception("invalid type of station");
                }

                r.Select("BAG_ID,OBJ_ID,DISTANCE,H_EFFECTIVE,AZIMUTH,INTERF_FROM_US,INTERF_FOR_US,ERP,PROT_RATES_FOR_US,PROT_RATES_FROM_US,STRENGTH_FOR_US,STRENGTH_FROM_US,OVERLAP_AREA,NUMBER,CUST_TXT1,CUST_TXT2");
                r.SetWhere("BAG_ID", IMRecordset.Operation.Eq, 0);
                r.Open();

                IComparer<Station> stComparer = new ComparerStation();
                stList.Sort(stComparer);

                for (int i = 0; i < stList.Count; i++)
                {
                    Station st = stList[i];
                    st._number = i + 1;
                }

                int povtorniNumber = 0;
                int stCounter2 = 0;
                for (int stCounter = 0; stCounter < stList.Count; stCounter++)
                {
                    Station st = stList[stCounter];

                    if (!string.IsNullOrEmpty(st.ADM_KEY))
                    {
                        string adm_key = st.ADM_KEY;
                        double ostatok = st._number - (int)st._number;
                        if (ostatok == 0)
                        {
                            st._number = stCounter + 1 + 0.01 - povtorniNumber;

                            stCounter2 = 1;

                            for (int stCounter1 = stCounter + 1; stCounter1 < stList.Count; stCounter1++)
                            {
                                st = stList[stCounter1];

                                if (st.ADM_KEY == adm_key)
                                {
                                    stCounter2 = stCounter2 + 1;
                                    st._number = stCounter + 1 - povtorniNumber + (stCounter2) * 0.01;
                                }
                            }
                        }
                        else
                        {
                            povtorniNumber = povtorniNumber + 1;
                        }
                    }
                    else
                    {
                        st._number = stCounter - povtorniNumber + 1;
                    }
                }
                for (int i = 0; i < stList.Count; i++)
                {
                    Station st = stList[i];

                    r.AddNew();
                    r.Put("BAG_ID", b1.Id);
                    r.Put("OBJ_ID", st._tableID);

                    r.Put("DISTANCE", st.DISTANCE);
                    r.Put("H_EFFECTIVE", st._EFHGT);
                    r.Put("AZIMUTH", st.A1);

                    r.Put("INTERF_FROM_US", st._E2);
                    r.Put("INTERF_FOR_US", st._E1);

                    r.Put("NUMBER", i + 1);
                    r.Put("PROT_RATES_FOR_US", st._PC1);
                    r.Put("PROT_RATES_FROM_US", st._PC2);

                    r.Put("STRENGTH_FOR_US", st._E1_PC1);
                    r.Put("STRENGTH_FROM_US", st._E2_PC2);

                    string numb = string.Empty;
                    if (st._number < 10)
                    {
                        numb = "000" + st._number.ToString();
                    }
                    else if (st._number < 100)
                    {
                        numb = "00" + st._number.ToString();
                    }
                    else if (st._number < 1000)
                    {
                        numb = "0" + st._number.ToString();
                    }
                    else
                    {
                        numb = st._number.ToString();
                    }

                    r.Put("CUST_TXT1", numb);

                    r.Put("CUST_TXT2", Math.Abs(_curStat._freq - st._freq).ToString());

                    r.Put("OVERLAP_AREA", st._Overlap); // пересичение зон обслуживания

                    r.Put("ERP", st._EVP);

                    r.Update();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                }
            }

            return b1;

            //foreach (Station st in stList)
            //{
            //   //System.Diagnostics.Debug.WriteLine("Станция");

            //System.Diagnostics.Debug.WriteLine("E1");
            //System.Diagnostics.Debug.WriteLine(st._E1);

            //System.Diagnostics.Debug.WriteLine("E2");
            //System.Diagnostics.Debug.WriteLine(st._E2);

            //System.Diagnostics.Debug.WriteLine("R");
            //System.Diagnostics.Debug.WriteLine(st._R);          


            //}
        }

        public void ShowResultForm()
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("FillingStations")))
            {
                pBar.UserCanceled();

                List<RecordPtr> listData = new List<RecordPtr>();
                List<string> listCaption = new List<string>();

                if (_needTV)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of TV"));
                    listData.Add(FillBagStations(_stListZhertvaTV, Station.TypeOfStation.TV, TypeOfFrequency.Zhertva));
                    listCaption.Add("Analog TV");
                }
                if (_needFM)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of FM"));
                    listData.Add(FillBagStations(_stListZhertvaFM, Station.TypeOfStation.FM, TypeOfFrequency.Zhertva));
                    listCaption.Add("FM");
                }
                if (_needDVBT)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of DVBT"));
                    listData.Add(FillBagStations(_stListZhertvaDVBT, Station.TypeOfStation.DVBT, TypeOfFrequency.Zhertva));
                    listCaption.Add("DVB-T");
                }
                if (_needTDAB)
                {
                    pBar.ShowBig(CLocaliz.TxT("Filling stations of TDAB"));
                    listData.Add(FillBagStations(_stListZhertvaTDAB, Station.TypeOfStation.TDAB, TypeOfFrequency.Zhertva));
                    listCaption.Add("T-DAB");
                }

                //------------------
                //Добавляем ислед. станцию
                listData.Add(FillBagStations(new List<Station>() { _curStat }, _curStat._typeOfStation, TypeOfFrequency.Pomeha));
                listCaption.Add("Текущая станция");

                string position = string.Empty;
                string numberChan = string.Empty;
                string standart = string.Empty;
                double height = 0;
                string type = string.Empty;

                if (_curStat._typeOfStation == Station.TypeOfStation.TV)
                    type = ICSMTbl.itblTV;
                if (_curStat._typeOfStation == Station.TypeOfStation.TDAB)
                    type = ICSMTbl.itblTDAB;
                if (_curStat._typeOfStation == Station.TypeOfStation.FM)
                    type = ICSMTbl.itblFM;
                if (_curStat._typeOfStation == Station.TypeOfStation.DVBT)
                    type = ICSMTbl.itblDVBT;

                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(type, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,Position.NAME,Channel.CHANNEL,STANDARD,AGL");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _curStat._tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        position = r.GetS("Position.NAME");
                        numberChan = r.GetS("Channel.CHANNEL");
                        standart = r.GetS("STANDARD");
                        height = r.GetD("AGL");
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }

                StringBuilder str = new StringBuilder();
                str.Append(position);
                str.Append(";  ");

                str.Append(numberChan);
                str.Append(" (");
                str.Append(_curStat._freq);
                str.Append(" MHz");
                str.Append(")");
                str.Append("\r\n");

                str.Append(standart);
                str.Append("; ");
                str.Append("\r\n");

                str.Append("Longitude ");
                str.Append(_curStat._longitude);
                str.Append("; ");

                str.Append("Latitude ");
                str.Append(_curStat._latitude);
                str.Append("; ");
                str.Append("\r\n");

                str.Append("Power ");
                str.Append(_curStat._power);
                str.Append(" (dBW); ");

                str.Append("Height ");
                str.Append(height);
                str.Append("; ");

                str.Append("Gain ");
                str.Append(_curStat._gMax);
                str.Append("; ");

                FrmCalculationResultVrs resultForm = new FrmCalculationResultVrs();

                foreach (Station st in _stListZhertvaTV)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListZhertvaFM)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListZhertvaDVBT)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListZhertvaTDAB)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListPomehaTV)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListPomehaFM)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListPomehaDVBT)
                    resultForm.StationList.Add(st);

                foreach (Station st in _stListPomehaTDAB)
                    resultForm.StationList.Add(st);

                resultForm.CurrentStation = _curStat;
                resultForm.ShowDialog();
                
                IMBag.Display2(listData.ToArray(), listCaption.ToArray(), str.ToString(), "Calculation VRS");
                foreach (RecordPtr h in listData)
                {
                    IMBag.DeleteBag(h);
                }
            }
        }
    }
}
