﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using LISBCProtect;
using LISBCTxServer;
using Diagramm;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    partial class Station
    {
        private double? GetPower(IMRecordset r)
        {
            double _power = r.GetD("PWR_ANT");
            if (_power == IM.NullD)
            {
                double gain = r.GetD("GAIN");
                if (gain == IM.NullD)
                    gain = 0.0;

                double txLosses = r.GetD("TX_LOSSES");
                if (txLosses == IM.NullD)
                    txLosses = 0.0;

                string polarization = r.GetS("POLARIZATION");
                if (string.IsNullOrEmpty(polarization))
                {
                    polarization = "H";
                }

                double erpH = r.GetD("ERP_H");
                double erpV = r.GetD("ERP_V");

                if (polarization == "H")
                {
                    if (erpH == IM.NullD)
                        return null;

                    _power = erpH - gain + txLosses;
                }
                else if (polarization == "V")
                {
                    if (erpV == IM.NullD)
                        return null;

                    _power = erpV - gain + txLosses;
                }
                else
                {
                    _power = Math.Max(erpH, erpV) - gain + txLosses;
                }
            }

            return _power;
        }

        private TBCSound CheckOnSND2_SYSTEM(string str)
        {
            if (str.Equals("F"))
                return TBCSound.sndFM;
            if (str.Equals("N"))
                return TBCSound.sndAM;
            else
                return TBCSound.sndFM;
        }

        private TBCTvStandards CheckOnCOLOUR_SYSTEM(string str)
        {
            if (str.Equals("S"))
                return TBCTvStandards.csSECAM;
            if (str.Equals("P"))
                return TBCTvStandards.csPAL;
            if (str.Equals("N"))
                return TBCTvStandards.csNTSC;

            return TBCTvStandards.csUNKNOWN;
        }

        private TBCOffsetType CheckOnOFFSET_TYPE(string str)
        {
            if (str.Equals("P"))
                return TBCOffsetType.otPrecision;
            else
                return TBCOffsetType.otNonPrecision;
        }

        private TBCTvSystems CheckOnTVSYS_CODE(string str)
        {
            if (str.Equals("B"))
                return TBCTvSystems.tvB;
            if (str.Equals("B1"))
                return TBCTvSystems.tvB1;
            if (str.Equals("D"))
                return TBCTvSystems.tvD;
            if (str.Equals("G"))
                return TBCTvSystems.tvG;
            if (str.Equals("H"))
                return TBCTvSystems.tvH;
            if (str.Equals("I"))
                return TBCTvSystems.tvI;
            if (str.Equals("K"))
                return TBCTvSystems.tvK;
            if (str.Equals("K1"))
                return TBCTvSystems.tvK1;
            if (str.Equals("L"))
                return TBCTvSystems.tvL;

            if (str.Equals("L1"))
                return TBCTvSystems.tvK1;
            if (str.Equals("M"))
                return TBCTvSystems.tvK1;
            if (str.Equals("N"))
                return TBCTvSystems.tvK1;
            if (str.Equals("T1"))
                return TBCTvSystems.tvK1;

            return TBCTvSystems.tvK1;
        }

        private TBCFMSystem CheckOnTRANSMISSION_SYS(string sys)
        {
            if (sys.Equals("1"))
                return TBCFMSystem.fm1;
            if (sys.Equals("2"))
                return TBCFMSystem.fm2;
            if (sys.Equals("3"))
                return TBCFMSystem.fm3;
            if (sys.Equals("4"))
                return TBCFMSystem.fm4;
            if (sys.Equals("5"))
                return TBCFMSystem.fm5;

            return TBCFMSystem.fm5;
        }

        private TBCDVBSystem CheckOnDVBT_SYS_CODE(string str)
        {
            switch (str)
            {
                case "A1":
                    return TBCDVBSystem.dsA1;
                case "A2":
                    return TBCDVBSystem.dsA2;
                case "A3":
                    return TBCDVBSystem.dsA3;
                case "A5":
                    return TBCDVBSystem.dsA5;
                case "A7":
                    return TBCDVBSystem.dsA7;

                case "B1":
                    return TBCDVBSystem.dsB1;
                case "B2":
                    return TBCDVBSystem.dsB2;
                case "B3":
                    return TBCDVBSystem.dsB3;
                case "B5":
                    return TBCDVBSystem.dsB5;
                case "B7":
                    return TBCDVBSystem.dsB7;

                case "C1":
                    return TBCDVBSystem.dsC1;
                case "C2":
                    return TBCDVBSystem.dsC2;
                case "C3":
                    return TBCDVBSystem.dsC3;
                case "C5":
                    return TBCDVBSystem.dsC5;
                case "C7":
                    return TBCDVBSystem.dsC7;

                case "D1":
                    return TBCDVBSystem.dsD1;
                case "D2":
                    return TBCDVBSystem.dsD2;
                case "D3":
                    return TBCDVBSystem.dsD3;
                case "D5":
                    return TBCDVBSystem.dsD5;
                case "D7":
                    return TBCDVBSystem.dsD7;

                case "E1":
                    return TBCDVBSystem.dsE1;
                case "E2":
                    return TBCDVBSystem.dsE2;
                case "E3":
                    return TBCDVBSystem.dsE3;
                case "E5":
                    return TBCDVBSystem.dsE5;
                case "E7":
                    return TBCDVBSystem.dsE7;

                case "F1":
                    return TBCDVBSystem.dsF1;
                case "F2":
                    return TBCDVBSystem.dsF2;
                case "F3":
                    return TBCDVBSystem.dsF3;
                case "F5":
                    return TBCDVBSystem.dsF5;
                case "F7":
                    return TBCDVBSystem.dsF7;
            }

            return TBCDVBSystem.dsA1;
        }

        private TBcRxMode CheckOnRX_MODE(string str)
        {
            if (str.Equals("PI"))
                return TBcRxMode.rmPi;
            if (str.Equals("PO"))
                return TBcRxMode.rmPo;
            if (str.Equals("FX"))
                return TBcRxMode.rmFx;
            if (str.Equals("MO"))
                return TBcRxMode.rmMo;

            return TBcRxMode.rmFx;
        }

        public static double GetG(double gMax, string diagH, double a1)
        {
            if (string.IsNullOrEmpty(diagH) || diagH == "OMNI")
                return gMax;

            if (diagH.Contains("POINTS"))
                return gMax; //TODO Для POINTS не реализовано

            int index = diagH.IndexOf("10") + 2;
            diagH = diagH.Substring(index);

            List<double> allValues = new List<double>();
            string[] splits = diagH.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // заполняем лист полученными значениями высоты антенны. 
            foreach (string str in splits)
            {
                allValues.Add(ConvertType.ToDouble(str, 0.0));
            }

            // определяем индекс масива
            double tmpIndex = (a1 / 10);

            int tmp = (int)tmpIndex;
            int nextVal = 0;

            tmp = tmp % 36; // индекс первого элемента в листе       
            nextVal = (tmp + 1) % 36; // индекс второго элемента во втором листе

            double minScale = tmp * 10; // ближайшое минимальное значение шкалы
            double maxScale = nextVal * 10; // ближайшое максимальное значение шкалы        

            double valMinScal1 = allValues[tmp]; // ближайшое минимальное значение с листа
            double valMaxScal2 = allValues[nextVal]; // ближайшое максимальное значение с листа

            double deltaValScal = valMaxScal2 - valMinScal1; // разница между ближайшими значениями с листа         

            double raznica = a1 - minScale;
            double proc = raznica / 10.0; // 10.0 - default value of degree;
            double valOfProc = deltaValScal * proc;
            double result = valMinScal1 + valOfProc;

            return result;

        }

        /// <summary>
        /// Перенести в Lis.CommonLib
        /// </summary>
        /// <param name="azimuth"></param>
        /// <returns></returns>
        private static double NormalizeAzimuth(double azimuth)
        {
            while (azimuth < 0.0)
                azimuth += 360.0;

            while (azimuth > 360.0)
                azimuth -= 360.0;

            return azimuth;
        }

        /// <summary>
        /// that interpolation height of antenna
        /// </summary>
        /// <param name="height">effective height for azimuth (for interpolation)</param>
        /// <param name="azimuth">EVP of azimuth</param>
        /// <returns></returns>
        public static double GetHeigh(string height, double azimuth)
        {
            if (string.IsNullOrEmpty(height))
                return 0.0;

            azimuth = NormalizeAzimuth(azimuth);
            
            List<int> allValues = new List<int>();
            string[] splits = height.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // заполняем лист полученными значениями высоты антенны. 
            foreach (string str in splits)
            {
                allValues.Add(ConvertType.ToInt32(str, 0));
            }

            // определяем индекс масива
            double tmpIndex = azimuth / 10;

            int tmp = (int)tmpIndex;
            int nextVal = 0;

            tmp = tmp % 36; // индекс первого элемента в листе       
            nextVal = (tmp + 1) % 36; // индекс второго элемента во втором листе

            double minScale = tmp * 10; // ближайшое минимальное значение шкалы
            double maxScale = nextVal * 10; // ближайшое максимальное значение шкалы        

            double valMinScal1 = allValues[tmp]; // ближайшое минимальное значение с листа
            double valMaxScal2 = allValues[nextVal]; // ближайшое максимальное значение с листа

            double deltaValScal = valMaxScal2 - valMinScal1; // разница между ближайшими значениями с листа         

            double raznica = azimuth - minScale;
            double proc = raznica / 10.0; // 10.0 - default value of degree;
            double valOfProc = deltaValScal * proc;
            double result = valMinScal1 + valOfProc;

            return result;
        }

        private bool GetEVP_HV(string diag, double gMax, double longitude, double latitude)
        {
            if (string.IsNullOrEmpty(diag))
                return false;
            else
                return true;
        }
    }
}
