﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using LisUtility;
using NSPosition;
using Protect;
using LISBCProtect;
using LISBCTxServer;
using Distribution;
using Diagramm;
using Lis.CommonLib;
using RSAGeography;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    partial class ViborkaVRS_1546
   {        
        #region MEMBERS

        bool _bWater = false;

        public class LandSea
        {
            public List<land_sea> LengthsList { get; set; }

            public double Distance
            {
                get
                {
                    double sum = 0;
                    foreach (land_sea ls in LengthsList)
                        sum += ls.land + ls.sea;

                    return sum;
                }
            }

            public LandSea()
            {
                LengthsList = new List<land_sea>();
            }

            public void Add(double length, int sea)
            {
                if (sea==0)
                {
                    land_sea nls = new land_sea();
                    nls.sea = 0;
                    nls.land = length;
                    LengthsList.Add(nls);
                } else
                {
                    land_sea nls = new land_sea();
                    nls.sea = length;
                    nls.land = 0;
                    LengthsList.Add(nls);
                }          
            }

            public land_sea[] Get(double startPos, double stopPos)
            {
                List<land_sea> returned = new List<land_sea>();

                double sum = 0;

                foreach (land_sea ls in LengthsList)
                {
                    double pre_sum = sum;
                    sum += ls.land + ls.sea;

                    if (sum>startPos)
                    {
                        if (returned.Count == 0)
                        {
                            land_sea nls = new land_sea();
                            if (sum < stopPos)
                            {
                                nls.land = ls.land > 0 ? sum - startPos : 0;
                                nls.sea = ls.sea > 0 ? sum - startPos : 0;
                            } else
                            {
                                nls.land = ls.land > 0 ? stopPos - startPos : 0;
                                nls.sea = ls.sea > 0 ? stopPos - startPos : 0;                                
                            }
                            returned.Add(nls);
                        }
                        else
                        {
                            if (sum > stopPos)
                            {
                                land_sea nls = new land_sea();
                                nls.land = ls.land > 0 ? stopPos - pre_sum : 0;
                                nls.sea = ls.sea > 0 ? stopPos - pre_sum : 0;
                                returned.Add(nls);
                            } else
                            {
                                land_sea nls = new land_sea();
                                nls.land = ls.land;
                                nls.sea = ls.sea;
                                returned.Add(nls);
                            }
                        }
                    }                    
                }

                return returned.ToArray();
            }

            /// <summary>
            ///   
            /// </summary>
            /// <param name="distance1">Расстояние от начала отрезка где размещена первая станция</param>
            /// <param name="distance2">Расстояние от начала отрезка где размещена вторая станция</param>
            public void Normalize(double distance1, double distance2)
            {
                double accumulatedDistance = 0;
                double margin = 0; // Сколько расстояние окрестности от станции, которое ненало оптимизировать
                double MinDistance = 10; // минимальный размер участка для оптимизации.

                bool bMerged = false;

                do
                {
                    bMerged = false;
                    int index = 0;
                    foreach (land_sea ls in LengthsList)
                    {
                        if (!((accumulatedDistance < distance1 - margin && accumulatedDistance + ls.Length > distance1 + margin) ||
                            (accumulatedDistance < distance2 - margin && accumulatedDistance + ls.Length > distance2 + margin)))
                        {
                            if (ls.Length <= MinDistance)
                            {
                                bMerged = true;

                                if (index == 0)
                                {
                                    land_sea ls0 = LengthsList[0];
                                    land_sea ls1 = LengthsList[1];

                                    if (ls0.sea == 0)
                                    {
                                        ls1.sea = ls1.sea + ls0.land;
                                    }
                                    else
                                    {
                                        ls1.land = ls1.land + ls0.sea;
                                    }
                                    LengthsList.RemoveAt(0);
                                    break;
                                }
                                if (index == LengthsList.Count - 1)
                                {
                                    land_sea ls0 = LengthsList[index];
                                    land_sea ls1 = LengthsList[index - 1];

                                    if (ls0.sea == 0)
                                    {
                                        ls1.sea = ls1.sea + ls0.land;
                                    }
                                    else
                                    {
                                        ls1.land = ls1.land + ls0.sea;
                                    }
                                    LengthsList.RemoveAt(index);
                                    break;
                                }

                                land_sea als0 = LengthsList[index - 1];
                                land_sea als = LengthsList[index];
                                land_sea als1 = LengthsList[index + 1];

                                if (als.sea == 0)
                                {
                                    als.sea = als.land + als0.sea + als1.sea;
                                    als.land = 0;
                                }
                                else
                                {
                                    als.land = als.sea + als0.land + als1.land;
                                    als.sea = 0;
                                }
                                LengthsList.RemoveAt(index + 1);
                                LengthsList.RemoveAt(index - 1);
                                break;
                            }
                        }
                        accumulatedDistance += ls.Length;
                        index++;
                    }
                } while (bMerged);
            }
            //void Invert()
            //{            
            //}            
        };

        public enum TypeOfFrequency // тип розрахунку по частоті
        {
            Zhertva, // жертва
            Pomeha // помеха
        }

        TxParsClass _tx0;

        private List<Station> _stListZhertvaTV = new List<Station>();
        private List<Station> _stListPomehaTV = new List<Station>();
        private List<Station> _stListZhertvaFM = new List<Station>();
        private List<Station> _stListPomehaFM = new List<Station>();
        private List<Station> _stListZhertvaDVBT = new List<Station>();
        private List<Station> _stListPomehaDVBT = new List<Station>();
        private List<Station> _stListZhertvaTDAB = new List<Station>();
        private List<Station> _stListPomehaTDAB = new List<Station>();

        private Station _curStat;
        private double _radius;

        private bool _needTV;
        private bool _needFM;
        private bool _needDVBT;
        private bool _needTDAB;

        /// <summary>
        /// Fills the current station special parameters.
        /// </summary>
        private void FillDataCurStat()
        {
            _tx0 = new TxParsClass();

            _tx0.id = 1;
            _tx0.bw = _curStat._bw;
            _tx0.f = _curStat._freq; // предположительно частота станцыи 1 МГц       
            _tx0.std = (int)_curStat._std;// (int)TBCTvStandards.csSECAM; // стандарт аналового телебачення
            _tx0.snd = (int)_curStat._sndSound;//TBCSound.sndFM; // предположительно либо телевидение либо радио аналоговое
            _tx0.offset_type = (int)_curStat._offset_type;//TBCOffsetType.otNonPrecision; // предположительно типа оффсетного здвига
            _tx0.v_carrier = _curStat._v_carrier;//304; // неизвестно какая хрень аналового телевиденья
            _tx0.s_carrier = _curStat._s_carrier;//100; // неизвестно какая хрень аналового телевиденья
            _tx0.v_offset_line = _curStat._v_offset_line; // неизвестно какая хрень аналового телевиденья
            _tx0.s_offset_line = _curStat._s_offset_line; // неизвестно какая хрень аналового телевиденья

            _tx0.ms = 0; // предположительно временная задержка в цыфровому телевиденьи 
            //if (_curStat._type == TBCTxType.ttFM)           

            _tx0.tx_type = (int)_curStat._type; //TBCTxType.ttTV;////TBCTxType.ttTV; // тип станции
            _tx0.rxmode = (int)_curStat._rmFx; // тип приема
            _tx0.sfn_id = 0;
            _tx0.Fxm_system = 0;//8;
            _tx0.tvsys = (int)_curStat._tvsys;//LISBCTxServer.TBCTvSystems.tvK1; // ти телевиазионной системе
            _tx0.fmsys = (int)_curStat._fmsys;//LISBCTxServer.TBCFMSystem.fm4;
            _tx0.dvbsys = (int)_curStat._dvbsys;//LISBCTxServer.TBCDVBSystem.dsF3;
        }

        ////////////////////////////////////////////////////////////////////////////
        // CONSTRUCTOR
        ////////////////////////////////////////////////////////////////////////////
        public ViborkaVRS_1546(Station currentStation, double radius,
           bool needTV, bool needFM, bool needDVBT, bool needTDAB, bool bWater)
        {
            _curStat = currentStation;
            _radius = radius;
            _needTV = needTV;
            _needFM = needFM;
            _needDVBT = needDVBT;
            _needTDAB = needTDAB;
            _bWater = bWater;

            FillDataCurStat();
        }

        #endregion

        #region CALCUALTION

        /// <summary>
        /// Main calculation method
        /// </summary>
        public void Calculate()
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Intermodulation")))
            {
                pBar.UserCanceled();

                InitRSGeography();

                if (_needTV)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.TV, _stListZhertvaTV);
                }
                if (_needFM)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.FM, _stListZhertvaFM);
                }
                if (_needDVBT)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.DVBT, _stListZhertvaDVBT);
                }
                if (_needTDAB)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.TDAB, _stListZhertvaTDAB);
                }
            }
            //this.FillingStations();
        }

        /// <summary>
        /// Считает ближнюю зону обслуживания для curStat без помех. (По модели ITU R P 1546-4)
        /// </summary>
        /// <param name="curStat">Станция которая получит помеху)</param>
        /// <param name="st">Станция которая создаст помеху</param>        
        /// <returns></returns>
        private double CalculateServiceNearZoneWithoutIntfr(Station curStat, Station st, LandSea landSea, double radius)
        {
            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            // a1 - азимут от _сurStation в направлении от st

            double EVPH = 0, EVPV = 0;
            GetEvp(curStat, st, 0, ref EVPH, ref EVPV, true); 
            
            if (string.IsNullOrEmpty(curStat._TMP_EFHGT))
                curStat._EFHGT = curStat.AGL;
            else
                curStat._EFHGT = Station.GetHeigh(curStat._TMP_EFHGT, a1);

            int h2 = 10;
            if (curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;

            double RH = 250.0;
            double RV = 250.0;
            double dl = 125.0;


            //double R = 500.0;
            //double d1 = 250.0;
            double eMin = GetEMinForStation(curStat);

            double EV, EH;
            do            
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RV);

                EV = EVPV - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RV, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);
                

                if (EV > eMin)
                    RV += dl;
                else
                    RV -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            dl = 125.0;
            
            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RH);

                EH = EVPH - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RH, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);
                
                if (EH > eMin)
                    RH += dl;
                else
                    RH -= dl;

                dl /= 2;

            } while (dl >= 0.001);
                        
            return Math.Max(RH, RV);
        }

        /// <summary>
        /// Считает дальнюю зону обслуживания для curStat без помех. (По модели ITU R P 1546-4)
        /// </summary>
        /// <param name="curStat">Станция которая получит помеху)</param>
        /// <param name="st">Станция которая создаст помеху</param>        
        /// <returns></returns>
        private double CalculateServiceFarZoneWithoutIntfr(Station curStat, Station st, LandSea landSea, double radius)
        {
            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            // a1 - азимут от _сurStation в направлении от st

            double EVPH = 0, EVPV = 0;
            GetEvp(curStat, st, 0.0, ref EVPH, ref EVPV, false);          

            if (string.IsNullOrEmpty(curStat._TMP_EFHGT))
                curStat._EFHGT = curStat.AGL;
            else
                curStat._EFHGT = Station.GetHeigh(curStat._TMP_EFHGT, a2);

            int h2 = 10;
            if (_curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;

            double RH = 250.0;
            double RV = 250.0;
            double dl = 125.0;


            //double R = 500.0;
            //double d1 = 250.0;
            double eMin = GetEMinForStation(curStat);

            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RV);

                double EV = EVPV - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RV, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);


                if (EV > eMin)
                    RV += dl;
                else
                    RV -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            dl = 125.0;

            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RH);

                double EH = EVPH - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RH, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);

                if (EH > eMin)
                    RH += dl;
                else
                    RH -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            return Math.Max(RH, RV);
        }

        /// <summary>
        /// Считает зону ближнюю обслуживания для curStat с помехой от st. (По модели ITU R P 1546-4)
        /// </summary>
        /// <param name="curStat">Станция которая получит помеху)</param>
        /// <param name="st">Станция которая создаст помеху</param>        
        /// <returns></returns>
        private double CalculateServiceNearZoneWithIntfr(Station curStat, Station st, double PC, double PT, LandSea landSea, double radius)
        {
            double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude, st._longitude, st._latitude);

            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            // a1 - азимут от _сurStation в направлении от st

            double EVPH = 0, EVPV = 0;
            double EVPHint = 0, EVPVint = 0;

            GetEvp(curStat, st, d, ref EVPH, ref EVPV, true);
            GetEvpIntf(curStat, st, d, ref EVPHint, ref EVPVint);

                                    
            if (string.IsNullOrEmpty(curStat._TMP_EFHGT))
                curStat._EFHGT = curStat.AGL;
            else
                curStat._EFHGT = Station.GetHeigh(curStat._TMP_EFHGT, a1);

            if (string.IsNullOrEmpty(st._TMP_EFHGT))
                st._EFHGT = st.AGL;
            else
                st._EFHGT = Station.GetHeigh(st._TMP_EFHGT, a2);

            int h2 = 10;
            if (_curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;

            double RH = d/2.0;
            double RV = d/2.0;
            double dl = d/4.0;


            //double R = 500.0;
            //double d1 = 250.0;
            double eMin = GetEMinForStation(curStat);

            //расчет для вертикальной поляризации
            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RV);

                double EV = EVPV - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RV, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);

                land_sea[] landSea2 = landSea.Get(radius, radius + d - RV);

                double ENV50 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RV, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENV1 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RV, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENV = Math.Max(ENV50+PC, ENV1 + PT);

                double ENH50 = EVPHint - 30 +
                             _1546_4.Get_E(st.AGL, st._EFHGT, d - RV, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENH1 = EVPHint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RV, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENH = Math.Max(ENH50 + PC, ENH1 + PT);

                if (curStat._rmFx != TBcRxMode.rmFx)
                {
                    ENH -= 16.0;
                }
                else
                {
                    ENH -= 16.0;
                    ENV -= 16.0;
                }

                double EN = MathLib.SummLog10(ENV, ENH);
                double esumm = MathLib.SummLog10(EN, eMin);

                if (EV > esumm)
                    RV += dl;
                else
                    RV -= dl;

                dl /= 2;

                //eeen = EN;

            } while (dl >= 0.001);

            dl = d/4.0;

            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RH);

                double EH = EVPH - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RH, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);

                land_sea[] landSea2 = landSea.Get(radius, radius + d - RH);

                double ENV50 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RH, st._freq, 50, curStat.AGL + curStat.ASL, h2, landSea2);

                double ENV1 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RH, st._freq, 1, curStat.AGL + curStat.ASL, h2, landSea2);

                double ENV = Math.Max(ENV50 + PC, ENV1 + PT);

                double ENH50 = EVPHint - 30 +
                             _1546_4.Get_E(st.AGL, st._EFHGT, d - RH, st._freq, 50, curStat.AGL + curStat.ASL, h2, landSea2);

                double ENH1 = EVPHint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d - RH, st._freq, 1, curStat.AGL + curStat.ASL, h2, landSea2);

                double ENH = Math.Max(ENH50 + PC, ENH1 + PT);

                if (_curStat._rmFx != TBcRxMode.rmFx)
                {
                    // Для нефиксированного приема
                    ENV -= 16.0;                    
                }
                else
                {
                    // Для фиксированного приема
                    ENH -= 16.0;
                    ENV -= 16.0;
                }

                double EN = MathLib.SummLog10(ENV,ENH);
                double esumm = MathLib.SummLog10(EN, eMin);
                
                if (EH > esumm)
                    RH += dl;
                else
                    RH -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            return Math.Max(RH, RV);
        }

        private static double NormalizeAzimuth(double azimuth)
        {
            while (azimuth < 0.0)
                azimuth += 360.0;

            while (azimuth > 360.0)
                azimuth -= 360.0;

            return azimuth;
        }

        private static void GetEvp(Station curStat, Station st, double d, ref double evpH, ref double evpV, bool bNear)
        {
            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
                                          st._longitude, st._latitude, ref a1, ref a2);

            evpH = -9999;
            evpV = -9999;

            double azimuth = bNear ? a1 : a2; 
                        
            if (!string.IsNullOrEmpty(curStat._aDiagH))
            {
                double deltaA1 = Math.Abs(NormalizeAzimuth(azimuth - curStat._azimuth));
                double GH = Station.GetG(curStat._gMax, curStat._aDiagH, deltaA1);
                evpH = curStat._power + curStat._gMax - GH - curStat._lossesTX; 
            }

            if (!string.IsNullOrEmpty(curStat._bDiagV))
            {
                double deltaA1 = Math.Abs(NormalizeAzimuth(azimuth - curStat._azimuth));
                double GV = Station.GetG(curStat._gMax, curStat._bDiagV, deltaA1);
                evpV = curStat._power + curStat._gMax - GV - curStat._lossesTX;
            }            
        }
        /// <summary>
        /// Проводить расчет ЕВП st в направлении curStat
        /// </summary>
        /// <param name="curStat"></param>
        /// <param name="st"></param>
        /// <param name="d"></param>
        /// <param name="evpIntfH"></param>
        /// <param name="evpIntfV"></param>
        private static void GetEvpIntf(Station curStat, Station st, double d, ref double evpIntfH, ref double evpIntfV)
        {
            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            // a1 - азимут от _сurStation в направлении от st
            if (d<0)
            {
                double t = a1;
                a1 = a2;
                a2 = t;
                d = Math.Abs(d);
            }
            
            if (!string.IsNullOrEmpty(st._aDiagH))
            {
                //stH = true;
                double deltaA1 = NormalizeAzimuth(a2 - st._azimuth);
                double GH = Station.GetG(st._gMax, st._aDiagH, deltaA1);
                evpIntfH = st._power + st._gMax - GH - st._lossesTX;
            }
            else
            {
                evpIntfH = -9999;
            }

            if (!string.IsNullOrEmpty(st._bDiagV))
            {
                //stV = true;
                double deltaA1 = NormalizeAzimuth(a2 - st._azimuth);
                double GV = Station.GetG(st._gMax, st._bDiagV, deltaA1);
                evpIntfV = st._power + st._gMax - GV - st._lossesTX;
            }
            else
            {
                evpIntfV = -9999;
            }            
        }

        public bool IsHorizontal(double evph, double evpv)
        {
            if (evph != -9999)
                return true;

            return false;
        }

        public bool IsVertical(double evph, double evpv)
        {
            if (evpv != -9999)
                return true;

            return false;
        }

        /// <summary>
        /// Считает зону дальнюю обслуживания для curStat с помехой от st. (По модели ITU R P 1546-4)
        /// </summary>
        /// <param name="curStat">Станция которая получит помеху)</param>
        /// <param name="st">Станция которая создаст помеху</param>        
        /// <returns></returns>
        private double CalculateServiceFarZoneWithIntfr(Station curStat, Station st, double PC, double PT, LandSea landSea, double radius)
        {
            double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude, st._longitude, st._latitude);

            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            double EVPV = 0, EVPH = 0;
            double EVPVint = 0, EVPHint = 0;

            GetEvp(curStat, st, d, ref EVPH, ref EVPV, false);
            GetEvpIntf(curStat, st, d, ref EVPHint, ref EVPVint);

            if (string.IsNullOrEmpty(curStat._TMP_EFHGT))
                curStat._EFHGT = curStat.AGL;
            else
                curStat._EFHGT = Station.GetHeigh(curStat._TMP_EFHGT, a2);

            if (string.IsNullOrEmpty(st._TMP_EFHGT))
                st._EFHGT = st.AGL;
            else
                st._EFHGT = Station.GetHeigh(st._TMP_EFHGT, a2);

            int h2 = 10;
            if (_curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;

            double RH = 500;// d / 2.0;
            double RV = 500;// d / 2.0;
            double dl = 250;// d / 4.0;


            //double R = 500.0;
            //double d1 = 250.0;
            double eMin = GetEMinForStation(curStat);

            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RV);

                double EV = EVPV - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RV, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);

                land_sea[] landSea2 = landSea.Get(radius, radius + d + RV);

                double ENV50 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RV, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENV1 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RV, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENV = Math.Max(ENV50 + PC, ENV1 + PT);

                double ENH50 = EVPHint - 30 +
                             _1546_4.Get_E(st.AGL, st._EFHGT, d + RV, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENH1 = EVPHint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RV, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENH = Math.Max(ENH50 + PC, ENH1 + PT);

                
                ENH -= 16.0;

                double EN = MathLib.SummLog10(ENV, ENH);
                double esumm = MathLib.SummLog10(EN, eMin);

                if (EV > esumm)
                    RV += dl;
                else
                    RV -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            dl = 250;

            do
            {
                land_sea[] landSea1 = landSea.Get(radius, radius + RH);

                double EH = EVPH - 30 +
                        _1546_4.Get_E(curStat.AGL, curStat._EFHGT, RH, curStat._freq, 50, curStat.AGL + curStat.ASL,
                                      h2, landSea1);

                land_sea[] landSea2 = landSea.Get(radius, radius + d + RH);

                double ENV50 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RH, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENV1 = EVPVint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RH, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENV = Math.Max(ENV50 + PC, ENV1 + PT);

                double ENH50 = EVPHint - 30 +
                             _1546_4.Get_E(st.AGL, st._EFHGT, d + RH, st._freq, 50, st.AGL + st.ASL, h2, landSea2);

                double ENH1 = EVPHint - 30 +
                               _1546_4.Get_E(st.AGL, st._EFHGT, d + RH, st._freq, 1, st.AGL + st.ASL, h2, landSea2);

                double ENH = Math.Max(ENH50 + PC, ENH1 + PT);

                
                ENV -= 16.0;
                
                double EN = MathLib.SummLog10(ENV, ENH);
                double esumm = MathLib.SummLog10(EN, eMin);

                if (EH > esumm)
                    RH += dl;
                else
                    RH -= dl;

                dl /= 2;

            } while (dl >= 0.001);

            return Math.Max(RH, RV);
        }

        private bool AreSamePolarizations(Station st, Station curStat)
        {
            //if (string.IsNullOrEmpty(st._aDiagH) && string.IsNullOrEmpty(curStat._aDiagH))
            //    return true;

            if (!string.IsNullOrEmpty(st._aDiagH) && !string.IsNullOrEmpty(curStat._aDiagH))
                return true;

            //if (string.IsNullOrEmpty(st._bDiagV) && string.IsNullOrEmpty(curStat._bDiagV))
            //    return true;

            if (!string.IsNullOrEmpty(st._bDiagV) && !string.IsNullOrEmpty(curStat._bDiagV))
                return true;

            return false;            
        }

        LandSea GetLandSea(double longitude1, double latitude1, double longitude2, double latitude2, double radius)
        {            
            TRSAMorpho morpo = new TRSAMorpho();            
            
            TRSAGeoPoint _current_point = new TRSAGeoPoint();
            
            RSASphericsClass sphericsClass = new RSASphericsClass();

            double azimuth;
            TRSAGeoPoint pa = new TRSAGeoPoint();
            TRSAGeoPoint pb = new TRSAGeoPoint();
            pa.H = latitude1;
            pb.H = latitude2;
            pa.L = longitude1;
            pb.L = longitude2;
            
            sphericsClass.Azimuth(pa, pb, out azimuth);
            double goAzimuth = azimuth;

            azimuth += 180;
            sphericsClass.ValidateAzimuth(ref azimuth);
            //p.H = latitude;
            //p.L = longitude;
            double distance;
            sphericsClass.Distance(pa, pb, out distance);

            LandSea _land_sea = new LandSea();
            if (!_bWater)
            {
                _land_sea.Add(distance+2*radius, 0);
                return _land_sea;
            }

            TRSAGeoPoint _end_point = new TRSAGeoPoint();
            TRSAGeoPoint _begin_point = new TRSAGeoPoint();            
            sphericsClass.PolarToGeo(radius, azimuth, pa, out _begin_point);
            sphericsClass.PolarToGeo(radius+distance, goAzimuth, pa, out _end_point);

            TRSAGeoPoint rp = new TRSAGeoPoint();

            TRSAGeoPathData data = new TRSAGeoPathData();
            TRSAGeoPathResults result = new TRSAGeoPathResults();
            geoPath.RunPointToPoint(_begin_point, _end_point, data, out result);

            _current_point.H = _begin_point.H;
            _current_point.L = _begin_point.L;

            double step_amount = 0.1;
            long steps = Convert.ToInt64((radius*2+distance)/step_amount);

            double accumulated_length = 0.0;
            int sea = -1;
            int oldsea = sea;
            do
            {
                oldsea = sea;
                short alt = -1;                

                terrainInfo.Get_Morpho(_current_point, out morpo);
                terrainInfo.Get_Altitude(_current_point, out alt);
                sphericsClass.PolarToGeo(step_amount, goAzimuth, _current_point, out rp);

                if (morpo.Kind == 64 || morpo.Kind == 192)
                    sea = 1;
                else
                    sea = 0;

                if (sea != oldsea && accumulated_length>0)
                {
                    if (oldsea>=0)
                    {
                        _land_sea.Add(accumulated_length, oldsea);
                        accumulated_length = 0;
                    }
                }

                _current_point.H = rp.H;
                _current_point.L = rp.L;
                accumulated_length += step_amount;
                steps--;
            } while (steps>0);

            _land_sea.Add(accumulated_length, sea);

            _land_sea.Normalize(radius, radius+distance);

            //TRSAGeoPathResults result = new TRSAGeoPathResults();
            //geoPath.RunPointToPoint(p, rp, data, out result);            
            
            /*double p = 300;
            int ii = 0;
            land_sea _ls;            
            while (_land_sea.Distance + p < radius*2)
            {
                _ls = new land_sea();
                if ((ii & 1) == 0)
                {
                    _ls.land = p;
                    _ls.sea = 0;
                }
                else
                {
                    _ls.land = 0;
                    _ls.sea = p;
                }
                _land_sea.LengthsList.Add(_ls);

                p += 250;
                ii++;
            }
            p = radius * 2 - _land_sea.Distance;

            _ls = new land_sea();
            if ((ii & 1) == 0)
            {
                _ls.land = p;
                _ls.sea = 0;
            }
            else
            {
                _ls.land = 0;
                _ls.sea = p;
            }
            _land_sea.LengthsList.Add(_ls);*/
            return _land_sea;
        }
        
        private void CalculateOnRadius(Station curStat, Station.TypeOfStation typeOfSearch, List<Station> Zhertva)
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate")))
            {
                int countRecord = 0;
                RecordPos positionRadius = Position.GetRangeCoordinates(curStat._longitude,
                   curStat._latitude, _radius, "Position.X", "Position.Y");

                IMRecordset r = null;
                try
                {
                    string tableName = string.Empty;

                    switch (typeOfSearch)
                    {
                        case Station.TypeOfStation.TV:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type TV ..."));
                            r = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblTV.ToString();
                            break;

                        case Station.TypeOfStation.FM:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type FM ..."));
                            r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblFM.ToString();
                            break;

                        case Station.TypeOfStation.TDAB:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type TDAB ..."));
                            r = new IMRecordset(ICSMTbl.itblTDAB, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE");
                            tableName = ICSMTbl.itblTDAB.ToString();
                            break;

                        case Station.TypeOfStation.DVBT:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type DVBT ..."));
                            r = new IMRecordset(ICSMTbl.itblDVBT, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblDVBT.ToString();
                            break;
                    }
                    r.Select("STATUS");
                    r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                    r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                    r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                    r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                    r.OrderBy("ID", OrderDirection.Ascending);

                    //int lastTablID = 0;

                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                            continue; //Отбрасываем ненужные станции

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();

                        int tableID = r.GetI("ID");                                                

                        if (curStat._tableID == tableID)
                            continue;

                        //if (_needOwner)
                        //   if (curStat._ownerID == ownerID)
                        //      continue;

                        Station st = new Station(tableName, tableID); // create a station with sampling
                        if (st._valid == false)
                            continue;

                        if (!CalculateValue(st))
                            continue;
                        try
                        {                            
                            double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude, st._longitude,
                                                                   st._latitude);                            

                            /*if ((curStat._tableID == 487111 && st._tableID == 667296)
                                || (st._tableID == 487111 && curStat._tableID == 667296))
                            {
                                int xx = 1;
                            }*/

                            LandSea _land_sea = GetLandSea(
                                curStat._longitude, curStat._latitude,
                                st._longitude, st._latitude,
                                500);

                            bool b = CalculateValue(curStat);

                            st._E1 = CalculationEVP(curStat, st, d, st._PC2, st._PT2, _land_sea, 500); // calculates field strange помеха нам            
                            CalculationEVPExtParams(curStat, st, d, st._PC2, st._PT2, _land_sea, 500);
                            //st.Points[1].Eused50 = LogSumm(st.Points[1].Emin, st.Points[1].Eintf50 - st.Points[1].D + 30.0);
                            //st.Points[2].Eused50 = LogSumm(st.Points[2].Emin, st.Points[2].Eintf50 - st.Points[2].D + 30.0);
                            //st.Points[3].Eused50 = LogSumm(st.Points[3].Emin, st.Points[3].Eintf50 - st.Points[3].D + 30.0);                            
                            _land_sea.LengthsList.Reverse();

                            st._E2 = CalculationEVP(st, curStat, d, st._PC1, st._PT1, _land_sea, 500); // calculates field strange помеха от нас (запис ЕВП в BAG)  
                            CalculationEVPExtParams(st, curStat, d, st._PC1, st._PT1, _land_sea, 500);
                            //DeterminationZone(curStat);

                            st._EVP2_for_R = curStat._EVP_for_R;

                            st.CRn = curStat.Rn;
                            st.CR1n = curStat.R1n;

                            //Structures....
                            st.Points[2] = curStat.Points[1];
                            st.Points[3] = curStat.Points[0];
                            curStat.Points[2] = st.Points[1];
                            curStat.Points[3] = st.Points[0];

                            //double R2n = CalculateServiceNearZoneWithoutIntfr(st, curStat);
                            //double R2f = CalculateServiceFarZoneWithoutIntfr(st, curStat);
                            //double R2in = CalculateServiceNearZoneWithIntfr(st, curStat, st._PC1, st._PT1);
                            //double R2if = CalculateServiceFarZoneWithIntfr(st, curStat, st._PC1, st._PT1);                            

                            if (st.DISTANCE >= st.Rn + curStat.Rn)
                                st._Overlap = -1;
                            else
                                st._Overlap = 1;

                            st._E1_PC1 = st._E1 - st._PC1;
                            st._E2_PC2 = st._E2 - st._PC2;

                            Zhertva.Add(st);
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.Message);
                        }
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
            }
        }

        /// <summary>
        /// Расчет защитных отношений
        /// PC1, PC2 - для постоянной помехи
        /// PT1, PT2 - для тропосферной помехи
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        private bool CalculateValue(Station st)
        {
            ProtectRelClass prc = new ProtectRelClass();
            Protect.TxParsClass tx1 = new Protect.TxParsClass();

            tx1.bw = st._bw;
            tx1.id = 1;
            tx1.f = st._freq;
            tx1.std = (int)st._std;//(int)TBCTvStandards.csSECAM;
            tx1.snd = (int)st._sndSound;//TBCSound.sndFM;//sndAM;
            tx1.offset_type = (int)st._offset_type;//(int)TBCOffsetType.otNonPrecision;
            tx1.v_carrier = st._v_carrier;//300;
            tx1.s_carrier = st._s_carrier;//100;
            tx1.v_offset_line = st._v_offset_line;//40;
            tx1.s_offset_line = st._s_offset_line;//50;
            tx1.ms = 0;
            tx1.tx_type = (int)st._type; // TBCTxType.ttTV;
            tx1.rxmode = (int)st._rmFx; //(int)TBcRxMode.rmFx;
            tx1.sfn_id = 0;
            tx1.Fxm_system = 0;//8;
            tx1.tvsys = (int)st._tvsys;//LISBCTxServer.TBCTvSystems.tvK1;
            tx1.fmsys = (int)st._fmsys;//LISBCTxServer.TBCFMSystem.fm4;
            tx1.dvbsys = (int)LISBCTxServer.TBCDVBSystem.dsD1;

            double pt, pc;

            try
            {
                prc.GetPr(_tx0, tx1, out pc, out pt);                
                st._PC1 = pc;
                st._PT1 = pt;

                prc.GetPr(tx1, _tx0, out pc, out pt);
                st._PC2 = pc;
                st._PT2 = pt;
            }
            catch
            {
                st._PC1 = -999;
                st._PC2 = -999;
                st._PT1 = -999;
                st._PT2 = -999;
            }

            if (st._PC1 > -999 || st._PC2 > -999 ||
                st._PT1 > -999 || st._PT2 > -999)
            {
                return true;
            }

            return false;
        }

        #endregion





        /// <summary>
        /// calculates field strange
        /// Раасчитывает, какую помеху st делает на curStat
        /// 
        /// Расчет производится для расстояния d, без учета напреленности антенны абонента.
        /// Рекомендуется для рассчетов напряженности поля в месте установки станции.
        /// </summary>
        /// <param name="curStation">the current(selected) station</param>
        /// <param name="st">the station with sampling</param>       ///                
        /// <param name="d">расстояние в км (=IM.NullD если на считать)</param>
        /// <param name="pc">Защитное отношение (постоянное)</param>
        /// <param name="pt">Защитное отношение (тропосферное)</param>
        /// <returns></returns>
        private double CalculationEVP(Station curStat, Station st, double d, double pc, double pt, LandSea landSea, double radius)
        {            
            if (d==IM.NullD)
                d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude, st._longitude, st._latitude);

            double a1 = 0, a2 = 180; // azimuths
            // a1 - азимут на st 
            // a2 - азимут на сurStat
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            st.Azimuth1 = a1;
            st.Azimuth2 = a2;

            /////////////////////////////////////////////////////////
            // эффективная высота с станции curStat в направлении st.
            curStat._EFHGT = Station.GetHeigh(curStat._TMP_EFHGT, a1);

            // эффективная высота с станции st в направлении curStat.            
            st._EFHGT = Station.GetHeigh(st._TMP_EFHGT, a2);            
            ///////////////////////////////////////////////////////            
            // определение высоты абонента (жертвы = сurStat)
            int h2 = 10;
            if (_curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;
            

            ////////////////////////////////////////////////////////////////////////////////
            bool stH = false, stV = false, curH = false, curV = false;
            double EVPH = -9999, EVPV = -9999, Ev = -9999, Eh = -9999, E = -9999;

            /// Расчет ЕВП в горизонтальной поляризации
            if (!string.IsNullOrEmpty(st._aDiagH))
            {
                stH = true;
                double deltaA1 = Math.Abs(st._azimuth - a2);
                double GH = Station.GetG(st._gMax, st._aDiagH, deltaA1);
                EVPH = st._power + st._gMax - GH - st._lossesTX; 
            }

            /// Расчет ЕВП в вертикальной поляризации
            if (!string.IsNullOrEmpty(st._bDiagV))
            {
                stV = true;
                double deltaA2 = Math.Abs(st._azimuth - a2);                
                double GV = Station.GetG(st._gMax, st._bDiagV, deltaA2);
                EVPV = st._power + st._gMax - GV - st._lossesTX;
            }            

            if (!string.IsNullOrEmpty(curStat._aDiagH))
                curH = true;

            if (!string.IsNullOrEmpty(curStat._bDiagV))
                curV = true;

            land_sea[] ls = landSea.Get(radius, radius + d);
            double Eh1 = EVPH - 30.0 + Distribution._1546_4.Get_E(st.AGL, st._EFHGT, d, st._freq, 1, st.AGL+st.ASL, h2, ls);
            double Eh50 = EVPH - 30.0 + Distribution._1546_4.Get_E(st.AGL, st._EFHGT, d, st._freq, 50, st.AGL + st.ASL, h2, ls);

            double Ev1 = EVPV - 30.0 +
                         Distribution._1546_4.Get_E(st.AGL, st._EFHGT, d, st._freq, 1, st.AGL + st.ASL, h2, ls);
            double Ev50 = EVPV - 30.0 + Distribution._1546_4.Get_E(st.AGL, st._EFHGT, d, st._freq, 50, st.AGL + st.ASL, h2, ls);


            if (stH != curH)
            {
                Eh1 -= 16;
                Eh50 -= 16;
            }

            if (stV != curV)
            {
                Ev1 -= 16;
                Ev50 -= 16;
            }

            Eh = Math.Max(Eh1 + pt, Eh50 + pc);
            Ev = Math.Max(Ev1 + pt, Ev50 + pc);

            E = MathLib.SummLog10(Eh, Ev);

            st.A1 = a2;
            st.DISTANCE = d;

            curStat._EVP = Math.Max(EVPH, EVPV);
            st._EVP_for_R = curStat._EVP;
            
            return E;
        }

        /// <summary>
        /// Рассчитывает "дополнительные" параметры ЕВП
        /// st мешает curStat.
        /// </summary>
        /// <param name="curStat"></param>
        /// <param name="st"></param>
        /// <param name="d"></param>
        /// <param name="pc"></param>
        /// <param name="pt"></param>
        private void CalculationEVPExtParams(Station curStat, Station st, double d, double pc, double pt, LandSea landSea, double radius)
        {
            // Расчет ближней зоны без помех для curStat в направлении st
            st.Rn = CalculateServiceNearZoneWithoutIntfr(curStat, st, landSea, radius);
            // Расчет дальней зоны без помех для curStat в направлении от st
            st.Rf = CalculateServiceFarZoneWithoutIntfr(curStat, st, landSea, radius);
            // Расчет ближней зоны с помехой для curStat в направлении st
            st.R1n = CalculateServiceNearZoneWithIntfr(curStat, st, st._PC2, st._PT2, landSea, radius);
            // Расчет дальней зоны с помехой для curStat в направлении от st
            st.R1f = CalculateServiceFarZoneWithIntfr(curStat, st, st._PC2, st._PT2, landSea, radius);

            //st._R = st.R1n;// ХЗ?

            int h2 = 10;
            if (curStat._rmFx != TBcRxMode.rmFx)
                h2 = 2;

            //--
            double EVPHint = 0, EVPVint = 0;

            /// Вероятно, расчитываем ЕВП от помехи(st) для дальней зоны(curStat).
            GetEvpIntf(curStat, st, d + st.Rf, ref EVPHint, ref EVPVint);

            double ENV50 = EVPVint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 50, st.AGL + st.ASL, h2);
            double ENH50 = EVPHint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 50, st.AGL + st.ASL, h2);
            st.Points[0].Eintf50 = Math.Max(ENV50, ENH50);

            double ENV10 = EVPVint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 10, st.AGL + st.ASL, h2);
            double ENH10 = EVPHint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 10, st.AGL + st.ASL, h2);
            st.Points[0].Eintf10 = Math.Max(ENV10, ENH10);

            double ENV1 = EVPVint - 30 +
                          _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 1, st.AGL + st.ASL, h2);
            double ENH1 = EVPHint - 30 +
                          _1546_4.Get_E(st.AGL, st._EFHGT, d + st.Rf, st._freq, 1, st.AGL + st.ASL, h2);
            st.Points[0].Eintf1 = Math.Max(ENV1, ENH1);


            /// Вероятно, расчитываем ЕВП от помехи(st) для ближней зоны(curStat).
            GetEvpIntf(curStat, st, d - st.Rn, ref EVPHint, ref EVPVint);

            ENV50 = EVPVint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 50, st.AGL + st.ASL, h2);
            ENH50 = EVPHint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 50, st.AGL + st.ASL, h2);
            st.Points[1].Eintf50 = Math.Max(ENV50, ENH50);

            ENV10 = EVPVint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 10, st.AGL + st.ASL, h2);
            ENH10 = EVPHint - 30 +
                           _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 10, st.AGL + st.ASL, h2);
            st.Points[1].Eintf10 = Math.Max(ENV10, ENH10);

            ENV1 = EVPVint - 30 +
                          _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 1, st.AGL + st.ASL, h2);
            ENH1 = EVPHint - 30 +
                          _1546_4.Get_E(st.AGL, st._EFHGT, Math.Abs(d - st.Rn), st._freq, 1, st.AGL + st.ASL, h2);
            st.Points[1].Eintf1 = Math.Max(ENV1, ENH1);

            /*int dnear = d - R1in;
            GetEvpIntf(curStat, st, dnear, ref EVPHint, ref EVPVint);

            double ENV50 = EVPVint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 50, curStat.AGL + curStat.ASL, h2);
            double ENH50 = EVPHint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 50, curStat.AGL + curStat.ASL, h2);
            st.Points[0].Eintf50 = Math.Max(ENV50, ENH50);

            double ENV10 = EVPVint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 10, curStat.AGL + curStat.ASL, h2);
            double ENH10 = EVPHint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 10, curStat.AGL + curStat.ASL, h2);
            st.Points[0].Eintf10 = Math.Max(ENV10, ENH10);

            double ENV1 = EVPVint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 1, curStat.AGL + curStat.ASL, h2);
            double ENH1 = EVPHint - 30 + _1546_4.Get_E(st.AGL, st._EFHGT, d + R1if, st._freq, 1, curStat.AGL + curStat.ASL, h2);
            st.Points[0].Eintf1 = Math.Max(ENV1, ENH1);*/

            st.Points[0].PCp = st._PC2;
            st.Points[0].PCt = st._PT2;
            st.Points[1].PCp = st._PC2;
            st.Points[1].PCt = st._PT2;
            //st.Points[2].PCp = st._PC1;
            //st.Points[2].PCt = st._PT1;
            //st.Points[3].PCp = st._PC1;
            //st.Points[3].PCt = st._PT1;

            if (AreSamePolarizations(st, curStat))
                st.Points[0].D = 0;
            else
                st.Points[0].D = 16;

            if (curStat._rmFx == TBcRxMode.rmFx && (d - st.Rn) > 0)
            {
                st.Points[1].D = 16;                
            }
            else
            {
                if (AreSamePolarizations(st, curStat))
                    st.Points[1].D = 0;
                else
                    st.Points[1].D = 16;
            }

            /*if (st._rmFx == TBcRxMode.rmFx)
            {
                st.Points[2].D = 16;
            }
            else
            {
                if (AreSamePolarizations(st, curStat))
                    st.Points[2].D = 0;
                else
                    st.Points[2].D = 16;
            }

            if (AreSamePolarizations(st, curStat))
                st.Points[3].D = 0;
            else
                st.Points[3].D = 16;*/

            st.Points[0].Emin = GetEMinForStation(curStat);
            st.Points[1].Emin = GetEMinForStation(curStat);
            //st.Points[2].Emin = GetEMinForStation(st);
            //st.Points[3].Emin = GetEMinForStation(st);

            st.Points[0].Eused50 = MathLib.SummLog10(st.Points[0].Emin, st.Points[0].Eintf50 - st.Points[0].D + st.Points[0].PCp);
            st.Points[0].Eused10 = MathLib.SummLog10(st.Points[0].Emin, st.Points[0].Eintf10 - st.Points[0].D + st.Points[0].PCt);
            st.Points[0].Eused1 = MathLib.SummLog10(st.Points[0].Emin, st.Points[0].Eintf1 - st.Points[0].D + st.Points[0].PCt);

            st.Points[1].Eused50 = MathLib.SummLog10(st.Points[1].Emin, st.Points[1].Eintf50 - st.Points[1].D + st.Points[1].PCp);
            st.Points[1].Eused10 = MathLib.SummLog10(st.Points[1].Emin, st.Points[1].Eintf10 - st.Points[1].D + st.Points[1].PCt);
            st.Points[1].Eused1 = MathLib.SummLog10(st.Points[1].Emin, st.Points[1].Eintf1 - st.Points[1].D + st.Points[1].PCt);

            if (st.Points[0].Eused50 > st.Points[0].Eused10 && st.Points[0].Eused50 > st.Points[0].Eused1)
                st.Points[0].InterferenceType = "п";
            else
                st.Points[0].InterferenceType = "т";

            if (st.Points[1].Eused50 > st.Points[1].Eused10 && st.Points[1].Eused50 > st.Points[1].Eused1)
                st.Points[1].InterferenceType = "п";
            else
                st.Points[1].InterferenceType = "т";
        }
    
        private double GetEMinForStation(Station st)
        {
            double eMin = 0;
            EminClass e = new EminClass();

            if (st._typeOfStation == Station.TypeOfStation.TV)
                eMin = e.GetEminTV(st._freq, (int)st._type);

            else if (st._typeOfStation == Station.TypeOfStation.DVBT)
                eMin = e.GetEminDVB((int)st._dvbsys, (int)st._rmFx, st._freq, 1);

            else if (st._typeOfStation == Station.TypeOfStation.FM)
                eMin = e.GetEminFM((int)st._fmsys);

            else if (st._typeOfStation == Station.TypeOfStation.TDAB)
                eMin = e.GetEminDAB((int)st._rmFx, st._freq);

            return eMin;
        }

        /// <summary>
        /// Возможно устаревшая.
        /// </summary>
        /// <param name="st"></param>
        private void DeterminationZone(Station st)
        {
            /*double R = 500.0;
            double d1 = 250.0;
            double eMin = GetEMinForStation(st);
            
            do
            {
                double l1546 = HATA.GetL(st._freq, R, st._EFHGT, 10); // Посмотреть с максимом эту функцыю...
                double E = st._EVP_for_R - 30.0 + 139.0 + 20 * Math.Log10(st._freq) - l1546;

                if (E > eMin)
                    R += d1;
                else
                    R -= d1;

                d1 /= 2;

            } while (d1 >= 0.05);

            st._R = R;*/

        }
    }
}
