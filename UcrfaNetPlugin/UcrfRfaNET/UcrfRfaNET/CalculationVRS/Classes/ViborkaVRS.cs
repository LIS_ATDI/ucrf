﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using LisUtility;
using NSPosition;
using Protect;
using LISBCProtect;
using LISBCTxServer;
using Distribution;
using Diagramm;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    partial class ViborkaVRS
    {
        #region MEMBERS

        public enum TypeOfFrequency // тип розрахунку по частоті
        {
            Zhertva, // жертва
            Pomeha // помеха
        }

        TxParsClass _tx0;

        private List<Station> _stListZhertvaTV = new List<Station>();
        private List<Station> _stListPomehaTV = new List<Station>();
        private List<Station> _stListZhertvaFM = new List<Station>();
        private List<Station> _stListPomehaFM = new List<Station>();
        private List<Station> _stListZhertvaDVBT = new List<Station>();
        private List<Station> _stListPomehaDVBT = new List<Station>();
        private List<Station> _stListZhertvaTDAB = new List<Station>();
        private List<Station> _stListPomehaTDAB = new List<Station>();

        private Station _curStat;
        private double _radius;

        private bool _needTV;
        private bool _needFM;
        private bool _needDVBT;
        private bool _needTDAB;

        /// <summary>
        /// Fills the current station special parameters.
        /// </summary>
        private void FillDataCurStat()
        {
            _tx0 = new TxParsClass();

            _tx0.id = 1;
            _tx0.bw = _curStat._bw;
            _tx0.f = _curStat._freq; // предположительно частота станцыи 1 МГц       
            _tx0.std = (int)_curStat._std;// (int)TBCTvStandards.csSECAM; // стандарт аналового телебачення
            _tx0.snd = (int)_curStat._sndSound;//TBCSound.sndFM; // предположительно либо телевидение либо радио аналоговое
            _tx0.offset_type = (int)_curStat._offset_type;//TBCOffsetType.otNonPrecision; // предположительно типа оффсетного здвига
            _tx0.v_carrier = _curStat._v_carrier;//304; // неизвестно какая хрень аналового телевиденья
            _tx0.s_carrier = _curStat._s_carrier;//100; // неизвестно какая хрень аналового телевиденья
            _tx0.v_offset_line = _curStat._v_offset_line; // неизвестно какая хрень аналового телевиденья
            _tx0.s_offset_line = _curStat._s_offset_line; // неизвестно какая хрень аналового телевиденья

            _tx0.ms = 0; // предположительно временная задержка в цыфровому телевиденьи 
            //if (_curStat._type == TBCTxType.ttFM)           

            _tx0.tx_type = (int)_curStat._type; //TBCTxType.ttTV;////TBCTxType.ttTV; // тип станции
            _tx0.rxmode = (int)_curStat._rmFx; // тип приема
            _tx0.sfn_id = 0;
            _tx0.Fxm_system = 0;//8;
            _tx0.tvsys = (int)_curStat._tvsys;//LISBCTxServer.TBCTvSystems.tvK1; // ти телевиазионной системе
            _tx0.fmsys = (int)_curStat._fmsys;//LISBCTxServer.TBCFMSystem.fm4;
            _tx0.dvbsys = (int)_curStat._dvbsys;//LISBCTxServer.TBCDVBSystem.dsF3;
        }

        ////////////////////////////////////////////////////////////////////////////
        // CONSTRUCTOR
        ////////////////////////////////////////////////////////////////////////////
        public ViborkaVRS(Station currentStation, double radius,
           bool needTV, bool needFM, bool needDVBT, bool needTDAB)
        {
            _curStat = currentStation;
            _radius = radius;
            _needTV = needTV;
            _needFM = needFM;
            _needDVBT = needDVBT;
            _needTDAB = needTDAB;

            FillDataCurStat();
        }

        #endregion

        #region CALCUALTION

        /// <summary>
        /// Main calculation method
        /// </summary>
        public void Calculate()
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Intermodulation")))
            {
                pBar.UserCanceled();

                if (_needTV)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.TV, _stListZhertvaTV);
                }
                if (_needFM)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.FM, _stListZhertvaFM);
                }
                if (_needDVBT)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.DVBT, _stListZhertvaDVBT);
                }
                if (_needTDAB)
                {
                    this.CalculateOnRadius(_curStat, Station.TypeOfStation.TDAB, _stListZhertvaTDAB);
                }
            }
            //this.FillingStations();
        }

        private void CalculateOnRadius(Station curStat, Station.TypeOfStation typeOfSearch, List<Station> Zhertva)
        {
            using (CProgressBar pBar = new CProgressBar(CLocaliz.TxT("Calculate")))
            {
                int countRecord = 0;
                RecordPos positionRadius = Position.GetRangeCoordinates(curStat._longitude,
                   curStat._latitude, _radius, "Position.X", "Position.Y");

                IMRecordset r = null;
                try
                {
                    string tableName = string.Empty;

                    switch (typeOfSearch)
                    {
                        case Station.TypeOfStation.TV:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type TV ..."));
                            r = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblTV.ToString();
                            break;

                        case Station.TypeOfStation.FM:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type FM ..."));
                            r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblFM.ToString();
                            break;

                        case Station.TypeOfStation.TDAB:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type TDAB ..."));
                            r = new IMRecordset(ICSMTbl.itblTDAB, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE");
                            tableName = ICSMTbl.itblTDAB.ToString();
                            break;

                        case Station.TypeOfStation.DVBT:
                            pBar.ShowBig(CLocaliz.TxT("Searching and calculating stations type DVBT ..."));
                            r = new IMRecordset(ICSMTbl.itblDVBT, IMRecordset.Mode.ReadOnly);
                            r.Select("ID,TX_LOSSES,Position.LONGITUDE,Position.LATITUDE,BW");
                            tableName = ICSMTbl.itblDVBT.ToString();
                            break;
                    }
                    r.Select("STATUS");
                    r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
                    r.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
                    r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
                    r.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);
                    r.OrderBy("ID", OrderDirection.Ascending);

                    //int lastTablID = 0;

                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        if (StationsStatus.StatusDivision.CanUseForCalculationEms(r.GetS("STATUS")) == false)
                            continue; //Отбрасываем ненужные станции

                        pBar.ShowSmall(countRecord++);
                        pBar.UserCanceled();

                        int tableID = r.GetI("ID");

                        //if (tableID == 1222 || tableID == 1237)
                        //{

                        //}

                        if (curStat._tableID == tableID)
                            continue;

                        //if (_needOwner)
                        //   if (curStat._ownerID == ownerID)
                        //      continue;

                        Station st = new Station(tableName, tableID); // create a station with sampling
                        if (st._valid == false)
                            continue;

                        if (!CalculateValue(st))
                            continue;
                        try
                        {
                            st._E1 = CalculationEVP(curStat, st, st._PC2); // calculates field strange помеха нам
                            DeterminationZone(st);
                            st._E2 = CalculationEVP(st, curStat, st._PC1); // calculates field strange помеха от нас (запис ЕВП в Баг)                                    
                            DeterminationZone(curStat);

                            if (st.DISTANCE > st._R + curStat._R)
                                st._Overlap = -1;
                            else
                                st._Overlap = 1;

                            st._E1_PC1 = st._E1 - st._PC1;
                            st._E2_PC2 = st._E2 - st._PC2;

                            Zhertva.Add(st);
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.Message);
                        }
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
            }
        }

        private bool CalculateValue(Station st)
        {
            ProtectRelClass prc = new ProtectRelClass();
            Protect.TxParsClass tx1 = new Protect.TxParsClass();

            tx1.bw = st._bw;
            tx1.id = 1;
            tx1.f = st._freq;
            tx1.std = (int)st._std;//(int)TBCTvStandards.csSECAM;
            tx1.snd = (int)st._sndSound;//TBCSound.sndFM;//sndAM;
            tx1.offset_type = (int)st._offset_type;//(int)TBCOffsetType.otNonPrecision;
            tx1.v_carrier = st._v_carrier;//300;
            tx1.s_carrier = st._s_carrier;//100;
            tx1.v_offset_line = st._v_offset_line;//40;
            tx1.s_offset_line = st._s_offset_line;//50;
            tx1.ms = 0;
            tx1.tx_type = (int)st._type; // TBCTxType.ttTV;
            tx1.rxmode = (int)st._rmFx; //(int)TBcRxMode.rmFx;
            tx1.sfn_id = 0;
            tx1.Fxm_system = 0;//8;
            tx1.tvsys = (int)st._tvsys;//LISBCTxServer.TBCTvSystems.tvK1;
            tx1.fmsys = (int)st._fmsys;//LISBCTxServer.TBCFMSystem.fm4;
            tx1.dvbsys = (int)LISBCTxServer.TBCDVBSystem.dsD1;

            double pt, pc;

            try
            {
                prc.GetPr(_tx0, tx1, out pc, out pt);
                st._PC1 = pc;

                prc.GetPr(tx1, _tx0, out pc, out pt);
                st._PC2 = pc;
            }
            catch
            {
                st._PC1 = -999;
                st._PC2 = -999;
            }

            if (st._PC1 > -999 || st._PC2 > -999)
            {
                return true;
            }

            return false;
        }

        #endregion

        /// <summary>
        /// calculates field strange
        /// </summary>
        /// <param name="curStation">the current(selected) station</param>
        /// <param name="st">the station with sampling</param>
        /// <returns></returns>
        private double CalculationEVP(Station curStat, Station st, double pc)
        {
            double d = StationUtility.AntennaLength(curStat._longitude, curStat._latitude,
            st._longitude, st._latitude);

            double a1 = 0, a2 = 180; // azimuths
            StationUtility.AntennaAzimuth(curStat._longitude, curStat._latitude,
               st._longitude, st._latitude, ref a1, ref a2);

            /////////////////////////////////////////////////////////
            curStat._EFHGT = Station.GetHeigh(st._TMP_EFHGT, a1);
            ///////////////////////////////////////////////////////

            double losses = 0;

            if (d != 0)
            {
                if (d > 5)
                {
                    int hA = 10;
                    if (st._rmFx != TBcRxMode.rmFx)
                        hA = 2;

                    losses = HATA.GetL(_curStat._freq, d, curStat._EFHGT, hA); // Посмотреть с максимом эту функцыю... может быть неправильные параметры подаю 
                }
                else
                {
                    double TXggc = st._freq / 1000; // переводим в ГГц
                    losses = L525.GetL525(TXggc, d);
                }
            }

            ////////////////////////////////////////////////////////////////////////////////
            bool stH = false, stV = false, curH = false, curV = false;
            double EVPH = 0, EVPV = 0, Ev = 0, Eh = 0, E = 0;

            if (!string.IsNullOrEmpty(st._aDiagH))
            {
                stH = true;
                double deltaA1 = Math.Abs(st._azimuth - a1);
                //AntennaDiagramm diagH = new AntennaDiagramm();
                //diagH.SetMaximalGain(st._gMax);
                //diagH.Build(st._aDiagH);
                double GH = Station.GetG(st._gMax, st._aDiagH, deltaA1); //diagH.GetLossesAmount(deltaA1);
                EVPH = st._power + st._gMax - GH - st._lossesTX; //+ l525; витягнуть послаблення в фідері або в фільтрі
            }
            else
            {
                EVPH = -9999;
            }

            if (!string.IsNullOrEmpty(st._bDiagV))
            {
                stV = true;
                double deltaA2 = Math.Abs(st._azimuth - a1);
                //AntennaDiagramm diagV = new AntennaDiagramm();
                //diagV.SetMaximalGain(st._gMax);
                //diagV.Build(st._bDiagV);
                double GV = Station.GetG(st._gMax, st._bDiagV, deltaA2);//diagV.GetLossesAmount(deltaA2);
                EVPV = st._power + GV;// +витягнуть послаблення в фідері або в фільтрі
            }
            else
            {
                EVPV = -9999;
            }

            if (!string.IsNullOrEmpty(curStat._aDiagH))
                curH = true;

            if (!string.IsNullOrEmpty(curStat._bDiagV))
                curV = true;

            Eh = EVPH - 30.0 + 139.0 + 20.0 * Math.Log10(st._freq) -
                  losses + pc; // проверить формулу с максимом

            Ev = EVPV - 30.0 + 139.0 + 20.0 * Math.Log10(st._freq) -
                  losses + pc; // проверить формулу с максимом

            if (stH != curH)
                Eh -= 16;

            if (stV != curV)
                Ev -= 16;

            E = 10 * Math.Log10(Math.Pow(10, Ev / 10) + Math.Pow(10, Eh / 10));

            st.A1 = a2;
            //st.A2 = a2;
            st.DISTANCE = d;

            curStat._EVP = Math.Max(EVPH, EVPV);
            st._EVP_for_R = curStat._EVP;

            return E;
        }

        private void DeterminationZone(Station st)
        {
            double R = 500.0;
            double d1 = 250.0;
            EminClass e = new EminClass();
            double eMin = 0;

            if (st._typeOfStation == Station.TypeOfStation.TV)
                eMin = e.GetEminTV(st._freq, (int)st._type);

            else if (st._typeOfStation == Station.TypeOfStation.DVBT)
                eMin = e.GetEminDVB((int)st._dvbsys, (int)st._rmFx, st._freq, 1);

            else if (st._typeOfStation == Station.TypeOfStation.FM)
                eMin = e.GetEminFM((int)st._fmsys);

            else if (st._typeOfStation == Station.TypeOfStation.TDAB)
                eMin = e.GetEminDAB((int)st._rmFx, st._freq);

            do
            {
                double l1546 = HATA.GetL(st._freq, R, st._EFHGT, 10); // Посмотреть с максимом эту функцыю...
                double E = st._EVP_for_R - 30.0 + 139.0 + 20 * Math.Log10(st._freq) - l1546;

                if (E > eMin)
                    R += d1;
                else
                    R -= d1;

                d1 /= 2;

            } while (d1 >= 0.05);

            st._R = R;
        }
    }
}
