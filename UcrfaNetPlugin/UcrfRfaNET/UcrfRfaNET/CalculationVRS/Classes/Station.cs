﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using LISBCProtect;
using LISBCTxServer;
using LisUtility;
using Diagramm;

namespace XICSM.UcrfRfaNET.CalculationVRS
{
    public partial class Station
    {        
        public struct PointInfo
        {
            public string Name;
            public double Eintf50;
            public double Eintf10;
            public double Eintf1;
            public double PCp;
            public double PCt;
            public double D;
            public double Eused50;
            public double Eused10;
            public double Eused1;
            public double Emin;
            public string InterferenceType;
        };


        #region MEMBERS

        public enum TypeOfStation
        {
            TV,
            FM,
            DVBT,
            TDAB
        }

        public int _tableID;
        public double _bw;
        public double _power; // потужність
        public double _latitude; // широта
        public double _longitude; // довгота
        public string _aDiagH = string.Empty; // diagram horizontally
        public string _bDiagV = string.Empty; // diagram vertically
        public double _EVPH; // EVP of azimuth in horizontal polarization
        public double _EVPV; // EVP of azimuth in vertical polarization
        public double _lossesTX; // ослабление в фидере передатчика
        public double _feederTX; // "дополнительное" ослабление в фидере передатчика

        public double _E1; // помеха нам
        public double _E2; // помеха от нас
        public double _EVP; // EVP от азимута

        public double _R;// радиус зона обслуживания
        public double _PC1 = IM.NullD; // защитное отношение (постоянное) нам
        public double _PC2 = IM.NullD; // защитное отношение (постоянное) от нас

        public double _PT1 = IM.NullD; // защитное отношение (тропосферное) нам
        public double _PT2 = IM.NullD; // защитное отношение (тропосферное) от нас

        public double _E1_PC1; // напряженность поля нам
        public double _E2_PC2; // напряженность поля от нас
        public int _Overlap { get; set; } // пересичение зон обслуживания      

        public double _EFHGT; // effective height for azimuth
        public string _TMP_EFHGT = string.Empty;// temporary storege of EFHGT
        public string _RPC = string.Empty; // Configuration of System only for DVB-T or T-DAB
        public double _SNCH; // Frequency offset only for analog type
        public double _freq;
        public double _gMax;

        public double _EVP_for_R; // ЕВП для розщета зоны обслужывания
        public double _EVP2_for_R; // ЕВП для розщета зоны обслужывания

        public double _modulation; // modulation, speed of code, protect interval
        public string _identificator = string.Empty; // identification of network
        public string _connection = string.Empty; // connection    
        public bool _valid = true;
        public double _number{ get; set; } // номер выборки (по уменьшению уровня помехи от нас)
        public string NumberFormat { get; set; } // Формат для номера 
        public string ADM_KEY = string.Empty;
        public string SFN_IDENT = string.Empty;

        public double _s_carrier; // неизвестно какая хрень аналового телевиденья
        public double _v_carrier; // неизвестно какая хрень аналового телевиденья

        public TBCTvStandards _std; // стандарт аналового телебачення
        public TBCOffsetType _offset_type; // предположительно типа оффсетного здвига
        public TBCTvSystems _tvsys; // тип телевиазионной системе
        public TBCDVBSystem _dvbsys; // тип(модуляция кодирования) системы DVB 
        public int _v_offset_line; // неизвестно какая хрень аналового телевиденья змінюється від 0 до 12
        public int _s_offset_line; // неизвестно какая хрень аналового телевиденья
        public TBCTxType _type; // тип станции      
        public TBCFMSystem _fmsys; // тип системы который зависит от стерео или моно и частоты дивиации.
        public TBcRxMode _rmFx; // тип приема
        public TBCSound _sndSound; // предположительно либо телевидение либо радио аналоговое
        public string _table = string.Empty;
        public double _azimuth; // азимут станции

        public double A1 { get; set; } // azimuth
        public double A2 { get; set; } // azimuth
        public double DISTANCE { get; set; } // distance between  

        public double Azimuth1 { get; set; } // Азимут от St на cur_stat
        public double Azimuth2 { get; set; } // Азимут от cur_stat на st

        public double AGL { get; set; }
        public double ASL { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public string Channel { get; set; }
        public string Polarization { get; set; }
        public string Region { get; set; }
        public string Standard { get; set; }
        
        public TypeOfStation _typeOfStation;

        public string StatusType { get; set; }

        public double Rn { get; set; }
        public double Rf { get; set; }
        public double R1n { get; set; }
        public double R1f { get; set; }

        // от CurrentStation
        public double CRn { get; set; }        
        public double CR1n { get; set; }

        public PointInfo[] Points = new PointInfo[4];


        public double GetE_of()
        {
            return (new List<double>
            {
                Points[0].Eintf1 + Points[0].PCt - Points[0].D,
                Points[0].Eintf10 + Points[0].PCt - Points[0].D,
                Points[0].Eintf50 + Points[0].PCp - Points[0].D,
                Points[1].Eintf1 + Points[1].PCt - Points[1].D,
                Points[1].Eintf10 + Points[1].PCt - Points[1].D,
                Points[1].Eintf50 + Points[1].PCp - Points[1].D
            }).Max().Round(3);
        }

        public double GetE_from()
        {
            return (new List<double>
            {
                Points[2].Eintf1 + Points[2].PCt - Points[2].D,
                Points[2].Eintf10 + Points[2].PCt - Points[2].D,
                Points[2].Eintf50 + Points[2].PCp - Points[2].D,
                Points[3].Eintf1 + Points[3].PCt - Points[3].D,
                Points[3].Eintf10 + Points[3].PCt - Points[3].D,
                Points[3].Eintf50 + Points[3].PCp - Points[3].D
            }).Max().Round(3);
        }

        /////////////////////////////////////////////////////////////////////         
        // CONSTRUCTOR         
        /////////////////////////////////////////////////////////////////////               
        public Station(string tableName, int id)
        {
            _table = tableName;
            _tableID = id;
            CreateStation(tableName);
        }

        #endregion

        private void CreateStation(string tableName)
        {
            if (tableName.Equals(ICSMTbl.itblTV))
            {
                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(ICSMTbl.itblTV, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,Position.LONGITUDE,Position.LATITUDE,Position.PROVINCE,PWR_ANT,DIAGV,DIAGH,ERP_V,ERP_H,EFHGT,Assignment.REF_PLAN_CFG,OFFSET_V_KHZ,OFFSET_S1_KHZ,FREQ_V_CARR,Assignment.RX_MODE,Assignment.GUARD_INTERVAL,Assignment.SFN_IDENT,Assignment.ALLOTM_ADM_KEY,TVSYS_CODE,COLOUR_SYSTEM,OFFSET_TYPE,FREQ,PWR_RATIO_1,DELTA_F_SND1,OFFSET_V_12,Assignment.IS_ALLOTM,Assignment.DIGITAL,Assignment.CLASS,Assignment.FREQ,Assignment.DESIG_EMISSION,BW,Assignment.DVBT_SYS_CODE,Assignment.RX_MODE,Assignment.TYP_REF_NTWK,Assignment.SFN_IDENT,Assignment.REF_PLAN_CFG,GAIN,DESIG_EM,SND2_SYSTEM,AZIMUTH,GAIN,TX_LOSSES,ERP_H,ERP_V,POLARIZATION,AGL,Position.ASL,NAME,Channel.CHANNEL,STATUS,TYPE,STANDARD,CUST_NBR1,CUST_NBR2");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        _longitude = r.GetD("Position.LONGITUDE");
                        _latitude = r.GetD("Position.LATITUDE");

                        _bDiagV = r.GetS("DIAGV");
                        _aDiagH = r.GetS("DIAGH");
                        _EVPH = r.GetD("ERP_H");
                        _EVPV = r.GetD("ERP_V"); // максимальне EVP в поляризац H
                        _gMax = r.GetD("GAIN");
                        Name = r.GetS("NAME");
                        Channel = r.GetS("Channel.CHANNEL");
                        Polarization = r.GetS("POLARIZATION");
                        //FreqOffset = r.GetD("");
                        StatusType = r.GetS("STATUS")+"/"+r.GetS("TYPE");

                        AGL = r.GetD("AGL");
                        ASL = r.GetD("Position.ASL");
                        Region = r.GetS("Position.PROVINCE");
                                                    
                        _TMP_EFHGT = r.GetS("EFHGT");

                        _s_offset_line = r.GetI("OFFSET_V_12");
                        _v_carrier = r.GetD("FREQ_V_CARR");
                        _s_carrier = r.GetD("DELTA_F_SND1") + _v_carrier;

                        _freq = r.GetD("FREQ");
                        if (_freq < 1)
                        {
                            _valid = false;
                            return;
                        }

                        string str = r.GetS("OFFSET_TYPE");
                        _offset_type = CheckOnOFFSET_TYPE(str);

                        str = r.GetS("COLOUR_SYSTEM");
                        _std = CheckOnCOLOUR_SYSTEM(str);
                        Standard = r.GetS("STANDARD");

                        str = r.GetS("TVSYS_CODE");
                        _tvsys = CheckOnTVSYS_CODE(str);

                        _bw = r.GetD("BW");
                        if (_bw == IM.NullD || _bw == 0.0)
                        {
                            string tmp = r.GetS("DESIG_EM");
                            _bw = XICSM.UcrfRfaNET.Station.ParserForSRTBW(tmp);
                            if (_bw <= 0.0)
                                _bw = 8;
                        }
                        else
                        {
                            _bw /= 1000;
                        }

                        _v_offset_line = Convert.ToInt32(r.GetD("OFFSET_V_KHZ"));
                        _s_offset_line = Convert.ToInt32(r.GetD("OFFSET_S1_KHZ"));

                        string tmpSndSound = r.GetS("SND2_SYSTEM");
                        _sndSound = CheckOnSND2_SYSTEM(tmpSndSound);

                        _azimuth = r.GetD("AZIMUTH");
                        if (_azimuth == IM.NullD)
                            _azimuth = 0.0;

                        double? power = GetPower(r);
                        if (power == null)
                            return;
                        else
                            _power = (double)power;

                        _lossesTX = r.GetD("TX_LOSSES");
                        if (_lossesTX == IM.NullD)
                            _lossesTX = 0.0;

                        _feederTX = r.GetD("CUST_NBR1")*r.GetD("CUST_NBR2");
                        if (_feederTX <= IM.NullD)
                            _feederTX = 0;
                        
                        //str = r.GetS("Assignment.IS_ALLOTM");
                        //int rddd = r.GetI("Assignment.DIGITAL");

                        //str = r.GetS("Assignment.CLASS");
                        //double fddd = r.GetD("Assignment.FREQ");

                        //str = r.GetS("Assignment.DESIG_EMISSION");  // нада буде узнать шо це


                        //double bddd = r.GetD("Assignment.BW");

                        //str = r.GetS("Assignment.DVBT_SYS_CODE");
                        //_dvbsys = CheckOnDVBT_SYS_CODE(str);               


                        _typeOfStation = TypeOfStation.TV;
                        _type = TBCTxType.ttTV;
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
                return;
            }

            if (tableName.Equals(ICSMTbl.itblFM))
            {
                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(ICSMTbl.itblFM, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,Position.LONGITUDE,Position.LATITUDE,Position.PROVINCE,PWR_ANT,DIAGV,DIAGH,ERP_V,ERP_H,EFHGT,Assignment.REF_PLAN_CFG,FREQ_OFFSET,FREQ,Assignment.RX_MODE,Assignment.GUARD_INTERVAL,Assignment.SFN_IDENT,Assignment.ALLOTM_ADM_KEY,BW,TRANSMISSION_SYS,Assignment.IS_ALLOTM,Assignment.DIGITAL,Assignment.CLASS,Assignment.FREQ,Assignment.DESIG_EMISSION,Assignment.BW,Assignment.DVBT_SYS_CODE,Assignment.RX_MODE,Assignment.TYP_REF_NTWK,Assignment.SFN_IDENT,Assignment.REF_PLAN_CFG,GAIN,TX_LOSSES,POLARIZATION,DESIG_EM,AGL,Position.ASL,NAME,Channel.CHANNEL,AZIMUTH,STATUS,TYPE,STANDARD,CUST_NBR1,CUST_NBR2");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        _longitude = r.GetD("Position.LONGITUDE");
                        _latitude = r.GetD("Position.LATITUDE");
                        Region = r.GetS("Position.PROVINCE");

                        _bDiagV = r.GetS("DIAGV");
                        _aDiagH = r.GetS("DIAGH");
                        _EVPH = r.GetD("ERP_H");
                        _EVPV = r.GetD("ERP_V"); // максимальне EVP в поляризац H
                        Name = r.GetS("NAME");
                        Channel = r.GetS("Channel.CHANNEL");
                        Polarization = r.GetS("POLARIZATION");
                        StatusType = r.GetS("STATUS") + "/" + r.GetS("TYPE");

                        _gMax = r.GetD("GAIN");

                        _TMP_EFHGT = r.GetS("EFHGT");
                        
                        AGL = r.GetD("AGL");
                        ASL = r.GetD("Position.ASL");
                        _azimuth = r.GetD("AZIMUTH");
                        if (_azimuth == IM.NullD)
                            _azimuth = 0.0;

                        _lossesTX = r.GetD("TX_LOSSES");
                        if (_lossesTX == IM.NullD)
                            _lossesTX = 0.0;

                        _feederTX = r.GetD("CUST_NBR1")*r.GetD("CUST_NBR2");
                        if (_feederTX <= IM.NullD)
                            _feederTX = 0;

                        _freq = r.GetD("FREQ");
                        if (_freq < 1)
                        {
                            _valid = false;
                            return;
                        }

                        _RPC = r.GetS("Assignment.REF_PLAN_CFG");

                        _SNCH = r.GetD("FREQ_OFFSET");
                        Standard = r.GetS("STANDARD");

                        string str = r.GetS("Assignment.RX_MODE");
                        _rmFx = CheckOnRX_MODE(str);

                        _modulation = r.GetD("Assignment.GUARD_INTERVAL");
                        _identificator = r.GetS("Assignment.SFN_IDENT");
                        _connection = r.GetS("Assignment.ALLOTM_ADM_KEY");

                        str = r.GetS("TRANSMISSION_SYS");
                        _fmsys = CheckOnTRANSMISSION_SYS(str);

                        _bw = r.GetD("BW");
                        if (_bw == IM.NullD || _bw == 0.0)
                        {
                            string tmp = r.GetS("DESIG_EM");
                            _bw = XICSM.UcrfRfaNET.Station.ParserForSRTBW(tmp);
                            if (_bw <= 0.0)
                                _bw = 8;
                        }
                        else
                        {
                            _bw /= 1000;
                        }

                        double? power = GetPower(r);
                        if (power == null)
                            return;
                        else
                            _power = (double)power;

                        _v_carrier = _freq;
                        _s_carrier = _freq;

                        _typeOfStation = TypeOfStation.FM;
                        _type = TBCTxType.ttFM;
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }
                return;
            }

            if (tableName.Equals(ICSMTbl.itblDVBT))
            {
                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(ICSMTbl.itblDVBT, IMRecordset.Mode.ReadOnly);
                    r.Select(
                        "ID,Position.LONGITUDE,Position.LATITUDE,Position.PROVINCE,PWR_ANT,DIAGV,DIAGH,ERP_V,ERP_H,EFHGT,Assignment.REF_PLAN_CFG,Assignment.FREQ_OFFSET,FREQ,Assignment.RX_MODE,Assignment.GUARD_INTERVAL,Assignment.SFN_IDENT,Assignment.ALLOTM_ADM_KEY,DESIG_EM,DVBT_SYS_CODE,BW,Assignment.IS_ALLOTM,Assignment.DIGITAL,Assignment.CLASS,Assignment.FREQ,Assignment.DESIG_EMISSION,Assignment.BW,Assignment.DVBT_SYS_CODE,Assignment.RX_MODE,Assignment.TYP_REF_NTWK,Assignment.SFN_IDENT,Assignment.REF_PLAN_CFG,GAIN,TX_LOSSES,POLARIZATION,ADM_KEY,AGL,AZIMUTH,Position.ASL,NAME,Channel.CHANNEL,STATUS,TYPE,STANDARD,CUST_NBR1,CUST_NBR2");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        _longitude = r.GetD("Position.LONGITUDE");
                        _latitude = r.GetD("Position.LATITUDE");
                        Region = r.GetS("Position.PROVINCE");

                        _bDiagV = r.GetS("DIAGV");
                        _aDiagH = r.GetS("DIAGH");
                        _EVPH = r.GetD("ERP_H");
                        _EVPV = r.GetD("ERP_V");
                        _TMP_EFHGT = r.GetS("EFHGT");
                        _gMax = r.GetD("GAIN");
                        Name = r.GetS("NAME");
                        Channel = r.GetS("Channel.CHANNEL");
                        Polarization = r.GetS("POLARIZATION");
                        StatusType = r.GetS("STATUS") + "/" + r.GetS("TYPE");

                        _azimuth = r.GetD("AZIMUTH");
                        if (_azimuth == IM.NullD)
                            _azimuth = 0.0;

                        AGL = r.GetD("AGL");
                        ASL = r.GetD("Position.ASL");
                        Standard = r.GetS("STANDARD");

                        string str = r.GetS("DVBT_SYS_CODE");
                        _dvbsys = CheckOnDVBT_SYS_CODE(str);

                        _freq = r.GetD("FREQ");
                        if (_freq < 1)
                        {
                            _valid = false;
                            return;
                        }

                        _bw = r.GetD("BW");
                        if (_bw == IM.NullD || _bw == 0.0)
                        {
                            string tmp = r.GetS("DESIG_EM");
                            _bw = XICSM.UcrfRfaNET.Station.ParserForSRTBW(tmp);
                            if (_bw <= 0.0)
                                _bw = 8;
                        }
                        else
                        {
                            _bw /= 1000;
                        }

                        double? power = GetPower(r);
                        if (power == null)
                            return;
                        else
                            _power = (double)power;

                        _lossesTX = r.GetD("TX_LOSSES");
                        if (_lossesTX == IM.NullD)
                            _lossesTX = 0.0;

                        _feederTX = r.GetD("CUST_NBR1")*r.GetD("CUST_NBR2");
                        if (_feederTX <= IM.NullD)
                            _feederTX = 0;

                        _typeOfStation = TypeOfStation.DVBT;
                        _type = TBCTxType.ttDVB;

                        ADM_KEY = r.GetS("ADM_KEY");
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }

                return;
            }

            if (tableName.Equals(ICSMTbl.itblTDAB))
            {
                IMRecordset r = null;
                try
                {
                    r = new IMRecordset(ICSMTbl.itblTDAB, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,Position.LONGITUDE,Position.LATITUDE,Position.PROVINCE,PWR_ANT,DIAGV,DIAGH,ERP_V,ERP_H,EFHGT,Assignment.REF_PLAN_CFG,Assignment.FREQ_OFFSET,FREQ,Assignment.RX_MODE,Assignment.GUARD_INTERVAL,Assignment.SFN_IDENT,Assignment.ALLOTM_ADM_KEY,Assignment.IS_ALLOTM,Assignment.DIGITAL,Assignment.CLASS,Assignment.FREQ,Assignment.DESIG_EMISSION,Assignment.BW,Assignment.DVBT_SYS_CODE,Assignment.RX_MODE,Assignment.TYP_REF_NTWK,Assignment.SFN_IDENT,Assignment.REF_PLAN_CFG,GAIN,TX_LOSSES,POLARIZATION,AZIMUTH,AGL,Position.ASL,NAME,Channel.CHANNEL,STATUS,TYPE,STANDARD,CUST_NBR1,CUST_NBR2");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        _longitude = r.GetD("Position.LONGITUDE");
                        _latitude = r.GetD("Position.LATITUDE");
                        Region = r.GetS("Position.PROVINCE");

                        _bDiagV = r.GetS("DIAGV");
                        _aDiagH = r.GetS("DIAGH");
                        _EVPH = r.GetD("ERP_H");
                        _EVPV = r.GetD("ERP_V");
                        string EFHGT = r.GetS("EFHGT");
                        _EFHGT = GetHeigh(EFHGT, _EVPH);
                        Name = r.GetS("NAME");
                        Channel = r.GetS("Channel.CHANNEL");
                        Polarization = r.GetS("POLARIZATION");
                        StatusType = r.GetS("STATUS") + "/" + r.GetS("TYPE");

                        AGL = r.GetD("AGL");
                        ASL = r.GetD("Position.ASL");

                        _RPC = r.GetS("Assignment.REF_PLAN_CFG");
                        _SNCH = r.GetD("Assignment.FREQ_OFFSET");
                        _azimuth = r.GetD("AZIMUTH");
                        if (_azimuth == IM.NullD)
                            _azimuth = 0.0;

                        _modulation = r.GetD("Assignment.GUARD_INTERVAL");
                        _identificator = r.GetS("Assignment.SFN_IDENT");
                        _connection = r.GetS("Assignment.ALLOTM_ADM_KEY");
                        Standard = r.GetS("STANDARD");

                        _freq = r.GetD("FREQ");
                        if (_freq < 1)
                        {
                            _valid = false;
                            return;
                        }

                        double? power = GetPower(r);
                        if (power == null)
                            return;
                        else
                            _power = (double)power;

                        _lossesTX = r.GetD("TX_LOSSES");
                        if (_lossesTX == IM.NullD)
                            _lossesTX = 0.0;

                        _feederTX = r.GetD("CUST_NBR1")*r.GetD("CUST_NBR2");
                        if (_feederTX <= IM.NullD)
                            _feederTX = 0;

                        _gMax = r.GetD("GAIN");

                        _typeOfStation = TypeOfStation.DVBT;
                        _type = TBCTxType.ttDAB;

                        _v_carrier = _freq;
                        _s_carrier = _freq;
                    }
                }
                finally
                {
                    if (r != null)
                    {
                        r.Close();
                        r.Destroy();
                    }
                }

                return;
            }
        }
    }

    public class ComparerStation : IComparer<Station>
    {
        public int Compare(Station x, Station y)
        {
            int retval = x._E2.CompareTo(y._E2);

            if (retval == 1)
                return -1;

            if (retval == -1)
                return 1;

            return retval;
        }
    }
}
