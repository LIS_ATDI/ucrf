﻿using System;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.HCM
{
    internal class HcmMobStation
    {
        /// <summary>
        /// Добавляет станциию в координационную таблицу
        /// </summary>
        /// <param name="stationId">ID мобильной станции</param>
        public static void AddMobStationToWien(int stationId)
        {
            AddMobStationToWien(stationId, ICSMTbl.itblMobStaFreqs);
        }
        public static int AddMobStationToWien(int stationId, string tableName)
        {
            return AddMobStationToWien(stationId, ICSMTbl.itblMobStaFreqs, IM.NullI);
        }
        public static int AddMobStationToWien(int stationId, string tableName, int freqId)
        {
            int id = IM.NullI;
            IMRecordset rsWien = new IMRecordset(ICSMTbl.WienCoordMob, IMRecordset.Mode.ReadWrite);
            try
            {
                rsWien.Select("ID");
                rsWien.Select("CSYS");
                rsWien.Select("X");
                rsWien.Select("Y");
                rsWien.Select("CLASS");
                rsWien.Select("SERVICE");
                rsWien.Select("CAT_USE");
                rsWien.Select("BIUSE_DATE");
                rsWien.Select("TX_FREQ");
                rsWien.Select("RX_FREQ");
                rsWien.Select("BW");
                rsWien.Select("DESIG_EMISSION");
                rsWien.Select("CHAN_OCC");
                rsWien.Select("FREQ_TYPE");
                rsWien.Select("NAME");
                rsWien.Select("COUNTRY_ID");
                rsWien.Select("ASL");
                rsWien.Select("PWR_ANT");
                rsWien.Select("POWER");
                rsWien.Select("ANT_TYPE");
                rsWien.Select("AZIMUTH");
                rsWien.Select("ELEVATION"); 
                rsWien.Select("POLAR");
                rsWien.Select("GAIN");
                rsWien.Select("TX_LOSSES");
                rsWien.Select("AGL");
                rsWien.Select("DIAGA");
                rsWien.Select("DIAGH");
                rsWien.Select("DIAGV");
                rsWien.Select("DATE_CREATED");
                rsWien.Select("CREATED_BY");
                rsWien.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                rsWien.Open();
                {
                    IMRecordset rsMobFreqs = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                    rsMobFreqs.Select("Station.Position.CSYS");
                    rsMobFreqs.Select("Station.Position.X");
                    rsMobFreqs.Select("Station.Position.Y");
                    rsMobFreqs.Select("Station.CLASS");
                    rsMobFreqs.Select("Station.SERVICE");
                    rsMobFreqs.Select("Station.CAT_USE");
                    rsMobFreqs.Select("BIUSE_DATE");
                    rsMobFreqs.Select("Station.BIUSE_DATE");
                    rsMobFreqs.Select("TX_FREQ");
                    rsMobFreqs.Select("RX_FREQ");
                    rsMobFreqs.Select("Station.DESIG_EMISSION");
                    rsMobFreqs.Select("Station.Position.NAME");
                    rsMobFreqs.Select("Station.Position.CITY");
                    rsMobFreqs.Select("Station.Position.ASL");
                    rsMobFreqs.Select("Station.PWR_ANT");
                    rsMobFreqs.Select("Station.POWER");
                    rsMobFreqs.Select("Station.ANT_TYPE");
                    rsMobFreqs.Select("Station.AZIMUTH");
                    rsMobFreqs.Select("Station.ELEVATION");
                    rsMobFreqs.Select("Station.POLAR");
                    rsMobFreqs.Select("Station.GAIN");
                    rsMobFreqs.Select("Station.TX_LOSSES");
                    rsMobFreqs.Select("Station.AGL");
                    rsMobFreqs.Select("Station.CHAN_OCC");
                    rsMobFreqs.Select("Station.Antenna.DIAGA");
                    rsMobFreqs.Select("Station.Antenna.DIAGH");
                    rsMobFreqs.Select("Station.Antenna.DIAGV");
                    rsMobFreqs.SetWhere("Station.ID", IMRecordset.Operation.Eq, stationId);
                    if (freqId!=IM.NullI)
                        rsMobFreqs.SetWhere("ID", IMRecordset.Operation.Eq, freqId);
                    try
                    {
                        for (rsMobFreqs.Open(); !rsMobFreqs.IsEOF(); rsMobFreqs.MoveNext())
                        {
                            rsWien.AddNew();
                            id = IM.AllocID(ICSMTbl.WienCoordMob, 1, -1);
                            rsWien.Put("ID", id);
                            rsWien.Put("DATE_CREATED", DateTime.Now);
                            rsWien.Put("CREATED_BY", IM.ConnectedUser());
                            //---
                            rsWien.Put("CSYS", rsMobFreqs.Get("Station.Position.CSYS"));
                            rsWien.Put("X", rsMobFreqs.Get("Station.Position.X"));
                            rsWien.Put("Y", rsMobFreqs.Get("Station.Position.Y"));
                            rsWien.Put("CLASS", rsMobFreqs.Get("Station.CLASS"));
                            rsWien.Put("SERVICE", rsMobFreqs.Get("Station.SERVICE"));
                            rsWien.Put("CAT_USE", rsMobFreqs.Get("Station.CAT_USE"));
                            rsWien.Put("TX_FREQ", rsMobFreqs.Get("TX_FREQ"));
                            rsWien.Put("RX_FREQ", rsMobFreqs.Get("RX_FREQ"));
                            //rsWien.Put("CHAN_OCC", rsMobFreqs.Get("Station.CHAN_OCC"));
                            rsWien.Put("CHAN_OCC", "1");
                            rsWien.Put("FREQ_TYPE", "2");
                            string city = rsMobFreqs.Get("Station.Position.CITY").ToString();
                            rsWien.Put("NAME", LatinPlugin.Transliteration.Translite(city, false));
                            rsWien.Put("COUNTRY_ID", "UKR");
                            rsWien.Put("ASL", rsMobFreqs.Get("Station.Position.ASL"));
                            rsWien.Put("PWR_ANT", rsMobFreqs.Get("Station.PWR_ANT"));
                            rsWien.Put("POWER", rsMobFreqs.Get("Station.POWER"));
                            rsWien.Put("ANT_TYPE", rsMobFreqs.Get("Station.ANT_TYPE"));
                            rsWien.Put("AZIMUTH", rsMobFreqs.Get("Station.AZIMUTH"));
                            rsWien.Put("ELEVATION", rsMobFreqs.Get("Station.ELEVATION"));
                            rsWien.Put("POLAR", rsMobFreqs.Get("Station.POLAR"));
                            rsWien.Put("GAIN", rsMobFreqs.Get("Station.GAIN"));
                            rsWien.Put("TX_LOSSES", rsMobFreqs.Get("Station.TX_LOSSES"));
                            rsWien.Put("AGL", rsMobFreqs.Get("Station.AGL"));
                            rsWien.Put("DIAGA", rsMobFreqs.Get("Station.Antenna.DIAGA"));
                            rsWien.Put("DIAGH", rsMobFreqs.Get("Station.Antenna.DIAGH"));
                            rsWien.Put("DIAGV", rsMobFreqs.Get("Station.Antenna.DIAGV"));
                            {
                                DateTime biuseDate = rsMobFreqs.GetT("BIUSE_DATE");
                                if(biuseDate == IM.NullT)
                                    biuseDate = rsMobFreqs.GetT("Station.BIUSE_DATE");
                                rsWien.Put("BIUSE_DATE", biuseDate);
                            }
                            //----
                            string desEmi = rsMobFreqs.GetS("Station.DESIG_EMISSION");
                            rsWien.Put("DESIG_EMISSION", desEmi);
                            if (!string.IsNullOrEmpty(desEmi))
                            {
                                double bw = 0.0;
                                double minF = 0.0;
                                double maxF = 0.0;
                                if (IM.DesigEmissionToBW(desEmi, ref bw, ref minF, ref maxF))
                                    rsWien.Put("BW", bw);
                            }
                            rsWien.Update();
                        }
                    }
                    finally
                    {
                        rsMobFreqs.Final();
                    }
                    // Створити запис у XFA_WIEN_CRD
                    IMRecordset rXfaWien=new IMRecordset(PlugTbl.XfaWienCrd, IMRecordset.Mode.ReadWrite);
                    rXfaWien.Select("ID,MOBSTA_FREQ_ID,MOBSTA_FREQ_TN");
                    try
                    {
                        rXfaWien.Open();
                        rXfaWien.AddNew();
                        rXfaWien.Put("ID",id);
                        rXfaWien.Update();
                    }
                    finally
                    {
                        rXfaWien.Final();
                    }
                }
            }
            finally
            {
                rsWien.Final();
            }
            return id;
        }
    }
}
