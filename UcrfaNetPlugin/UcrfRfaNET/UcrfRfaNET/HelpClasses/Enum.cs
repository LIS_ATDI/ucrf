﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using ICSM;

namespace XICSM.UcrfRfaNET
{
    static public class EnumLoader
    {
        static public Dictionary<string, string> LoadEnum(string EnumName)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            Dictionary<string, string> langs = new Dictionary<string, string>();

            IMRecordset rs = new IMRecordset("COMBO", IMRecordset.Mode.ReadOnly);
            try
            {
                rs.Select("DOMAIN,ITEM,LANG,LEGEN");
                rs.SetWhere("DOMAIN", IMRecordset.Operation.Eq, EnumName);
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    string Code = rs.GetS("ITEM");
                    string Description = rs.GetS("LEGEN").Trim();
                    string Lang = rs.GetS("LANG");

                    if (retval.ContainsKey(Code))
                    {
                        int curLangPriority = LangList.IndexOf(langs[Code]);
                        int newLangPriority = LangList.IndexOf(Lang);
                        if (newLangPriority != -1 && curLangPriority != -1 && newLangPriority < curLangPriority)
                        {
                            retval[Code] = Description;
                            langs[Code] = Lang;
                        }
                    }
                    else
                    {
                        retval.Add(Code, Description);
                        langs.Add(Code, Lang);
                    }
                }
            }
            finally
            {
                rs.Destroy();
            }
            
            return retval;
        }

        static public string GetItemDescription (this Dictionary<string, string> dict, string code)
        {
            return (dict.ContainsKey(code) && dict[code].Length > 0) ? dict[code] : code;
        }

        static private List<string> langList = null;

        static public List<string> LangList
        {
            get
            {
                if (langList == null)
                {
                    langList = new List<string>();

                    RegistryKey readKey = Registry.CurrentUser.OpenSubKey("Software\\ATDI\\ICSM\\LANGUAGES");
                    if (readKey != null)
                    {
                        try
                        {
                            // Загружаем список языков, поддерживаемых ICSM
                            string loadLanguage = (string)readKey.GetValue("order");
                            if (loadLanguage != null)
                            {
                                string[] splitLanguage = loadLanguage.Split(new Char[] { '.' });
                                foreach (string lng in splitLanguage)
                                    if (lng != "")
                                        langList.Add(lng);
                            }
                        }
                        finally
                        {
                            readKey.Close();
                        }
                    }
                    if (!langList.Contains("ENG"))
                        langList.Add("ENG");
                }
                
                return langList;
            }
        }
    }

    //======================================================
    /// <summary>
    /// Типы пакетов
    /// </summary>
    public enum EPacketType
    {
        PckUnknown = 0,  //Неизвестний
        PckDOZV = (1 << 0),   //Заява про видачу Дозволу на експлуатацію РЕЗ
        PckVISN = (1 << 1),   //Заява про видачу Висновку щодо ЕМС
        PctREISSUE = (1 << 2),   //заяви на Переоформлення
        PctPROLONG = (1 << 3),   //заяви на Продовження
        PctANNUL = (1 << 4),   //заяви на Анулювання
        PctLICENCE = (1 << 5),   //заяви на Ліцензування
        PctNVTVURZP = (1 << 6),   //заяви на проведення НВ/ТВ
        PckDOZVURZP = (1 << 7),   //Заява про видачу Дозволу на експлуатацію РЕЗ (УРЗП)
        PckPTKNVTV = (1 << 8),   //заяви на проведення ПТК/НВ/ТВ
        PckSetCallSign = (1 << 9),   //заяви на призначення позивних
        PckDuplicate = (1 << 10),   //заяви-дублікати
    }
    //======================================================
    /// <summary>
    /// Класс конвертирует тип ApplType
    /// </summary>
    public static class ConvertPacketType
    {
        //===================================================
        /// <summary>
        /// Возвращает человеческое название типа пакета
        /// </summary>
        /// <param name="packetType">Тп заявки</param>
        /// <returns>Название пакета</returns>
        public static string GetHumanNameOfPacketType(this EPacketType packetType)
        {
            Dictionary<EPacketType, string> packetNames = new Dictionary<EPacketType, string>();
            packetNames.Add(EPacketType.PckUnknown, "Неизвестний");
            packetNames.Add(EPacketType.PckDOZV, "Заява про видачу Дозволу на експлуатацію РЕЗ");
            packetNames.Add(EPacketType.PckVISN, "Заява про видачу Висновку щодо ЕМС");
            packetNames.Add(EPacketType.PctREISSUE, "заяви на Переоформлення");
            packetNames.Add(EPacketType.PctPROLONG, "заяви на Продовження");
            packetNames.Add(EPacketType.PctANNUL, "заяви на Анулювання");
            packetNames.Add(EPacketType.PctLICENCE, "заяви на Ліцензування");
            packetNames.Add(EPacketType.PctNVTVURZP, "заяви на проведення НВ/ТВ");
            packetNames.Add(EPacketType.PckDOZVURZP, "Заява про видачу Дозволу на експлуатацію РЕЗ (УРЗП)");
            packetNames.Add(EPacketType.PckSetCallSign, "заяви на призначення позивних");

            if (packetNames.ContainsKey(packetType) == true)
                return CLocaliz.TxT(packetNames[packetType]);
            return "";
        }

        //===================================================
        /// <summary>
        /// Конвертирует тип EPacketType в строку
        /// </summary>
        /// <param name="packetType">что конвертировать</param>
        /// <returns>сроковое представление типа</returns>
        public static string ToStr(this EPacketType packetType)
        {
            string retString = "";
            try
            {
                retString = Enum.Format(typeof(EPacketType), packetType, "F");
            }
            catch { }
            return retString;
        }
        //===================================================
        public static EPacketType ToPacketType(this string packetType)
        {
            EPacketType retType = EPacketType.PckUnknown;
            try
            {
                retType = (EPacketType)Enum.Parse(typeof(EPacketType), packetType);
            }
            catch { }
            return retType;
        }
    }
    //======================================================
    /// <summary>
    /// Типы заявок
    /// </summary>
    public enum AppType
    {
        AppUnknown = 0,  //Неизвестная
        AppZS = (1 << 0),  //БС
        AppBS = (1 << 1),  //ЗС
        AppRS = (1 << 2),  //PС
        AppRR = (1 << 3),  //PP
        AppTR = (1 << 4),  //ТP
        AppR2 = (1 << 5),  //P2 (аналогове радио)
        AppTV2 = (1 << 6),  //TV (аналогове телевидение)
        AppR2d = (1 << 7),  //ТPd (цифровое радио)
        AppTV2d = (1 << 8),  //TVd (цифровое телевидение)
        AppAR_3 = (1 << 9),  //ВМА
        AppRRTR = (1 << 10),  //PP TP
        AppR2R2d = (1 << 11),  //P2 (аналогове радио) ТPd (цифровое радио)
        AppTV2TV2d = (1 << 12),//TVd (цифровое телевидение)  TV (аналогове телевидение)
        AppZRS = (1 << 13),  //ЗPС
        AppVP = (1 << 14), //ВП  випромінювальні пристрої
        AppA3 = (1 << 15), //A3  аматорські пристрої
        AppTR_З = (1 << 16), //Тр3 - железная дорога
        AppAP3 = (1 << 17), //AP_3 - абонентський РЕЗ з другим сектором
        AppAR3 = (1 << 18)
    }
    //======================================================
    /// <summary>
    /// Класс конвертирует тип ApplType
    /// </summary>
    public static class appTypeConvert
    {
        //===================================================
        /// <summary>
        /// Возвращает человеческое название типа заявки
        /// </summary>
        /// <param name="applType">Тп заявки</param>
        /// <returns>Название документа</returns>
        public static string GetHumanNameOfAppType(AppType applType)
        {
            Dictionary<AppType, string> applNames = new Dictionary<AppType, string>();
            applNames.Add(AppType.AppUnknown, "");
            applNames.Add(AppType.AppZS, "ЗС");
            applNames.Add(AppType.AppBS, "БС");
            applNames.Add(AppType.AppRS, "PС");
            applNames.Add(AppType.AppRR, "PP");
            applNames.Add(AppType.AppTR, "ТP");
            applNames.Add(AppType.AppTR_З, "ТP_З");
            applNames.Add(AppType.AppA3, "Аматоры");
            applNames.Add(AppType.AppR2, "P2 (аналогове радио)");
            applNames.Add(AppType.AppTV2, "TV (аналогове телевидение)");
            applNames.Add(AppType.AppR2d, "ТPd (цифровое радио)");
            applNames.Add(AppType.AppTV2d, "TVd (цифровое телевидение)");
            applNames.Add(AppType.AppAR_3, "ВМА");
            applNames.Add(AppType.AppRRTR, "PP TP");
            applNames.Add(AppType.AppR2R2d, "P2 (аналогове радио) ТPd (цифровое радио)");
            applNames.Add(AppType.AppTV2TV2d, "TVd (цифровое телевидение)  TV (аналогове телевидение)");
            applNames.Add(AppType.AppZRS, "ЗРС");
            applNames.Add(AppType.AppVP, "Излучающие устройства");
            applNames.Add(AppType.AppAP3 , "Абонентські РЕЗ залізниці");
            if (applNames.ContainsKey(applType) == true)
                return CLocaliz.TxT(applNames[applType]);
            return "";
        }
        //===================================================
        /// <summary>
        /// Конвертирует тип AppType в строку
        /// </summary>
        /// <param name="applType">что конвертировать</param>
        /// <returns>сроковое представление типа</returns>
        public static string AppTypeToString(AppType applType)
        {
            string retString = "";
            try
            {
                retString = Enum.Format(typeof(AppType), applType, "F");
            }
            catch { }
            return retString;
        }
        //===================================================
        public static AppType StringToAppType(string applType)
        {
            AppType retType = AppType.AppUnknown;
            try
            {
                retType = (AppType)Enum.Parse(typeof(AppType), applType);
            }
            catch { }
            return retType;
        }
    }
    ////======================================================
    ///// <summary>
    ///// Типы документов
    ///// </summary>
    //public enum DocType
    //{
    //   DocUnknown = 0,  //Неизвестный
    //   VISN,     //Висновок
    //   DOZV,     //Дозвіл
    //   PCR,      //Повідомлення про скасування рішення про видачу висновків
    //   sentPCR,  //Відправленно документ "Повідомлення про скасування рішення про видачу висновків"
    //   VVP,      //Відмова у наданні дозволу
    //   sentVVP,  //Відправленно документ "Відмова у наданні дозволу"
    //   DRV,      //Службова записка до ДРВ на рахунок
    //   VVE,      //Відмова у наданні висновку щодо ЕМС
    //   sentVVE,  //Відправленно повідомлення про "Відмова у наданні висновку щодо ЕМС"
    //   LYST,     //Повідомлення про необхідність узгодження МК
    //   _0159,    //На узгодження з ГШ ЗС України
    //   FYLIA,    //Запит до регіональної філії УДЦР або УРЧМ (коорд.+адреса)
    //   PCR2,     //Залишення без розгляду (форма 2)
    //   LYST_N,   //Повідомлення про необхідність узгодження МК (негативна)
    //   _0159_N,  //На узгодження з ГШ ЗС України (негативна)
    //   VISN_NR,  //Висновок в НР
    //   DOZV_OPER,//Дозвіл оператора
    //   TEST_TV,  //Погодження тестового включення (ТВ)
    //   TEST_NV,  //Погодження тестового включення (НВ)
    //   TEST_PTK, //Погодження тестового включення (ПТК)
    //   TEST_PTK_NV,//Погодження тестового включення (ПТК + НВ)
    //   TEST_PCR2, //Погодження тестового включення + залишення без розгляду
    //   LIST_FYLIA,//Лист до філії (перенаправлення заявочних документів)
    //   NOTICE_PTK,//Попередження щодо визначення заявником строків ПТК
    //   URCM_PTK,    //Службова записка до УРЧМ щодо їх участі в ПТК
    //   URCM_DOC,    //Службова записка до УРЧП про передачу заявочних документів
    //   AKT_PTK,     //АКТ ПТК
    //   URCM_NOTICE, //Службова записка до УРЧМ
    //   DOZV_CANCEL, //Анулювання дозволу
    //   REESTR_VID,  //Реєстраційну відомість
    //   DOZV_CANCEL_ANNUL, //Скасування анулювання дозволу
    //   GIVE_LICENCE, //Разрешение на выдачу лицензии
    //}
    //======================================================
    /// <summary>
    /// Типы департаментов
    /// </summary>
    public enum DepartmentType
    {
        Unknown = 0,      //Неизвестный
        VRR = (1 << 0),   //ВРР
        VFSR = (1 << 1),  //ВФСР
        VRS = (1 << 2),   //ВРС
        VMA = (1 << 3),   //ВМА
        FILIA = (1 << 4), //Филия
        URCM = (1 << 5),  //УРЧМ
        URZP = (1 << 6),  //УРЗП
        VRZ = (1 << 7),   //ВРЗ
    }
    public static class DepartmentTypeExtension
    {
        private static readonly Dictionary<DepartmentType, string> DepList = new Dictionary<DepartmentType, string>();

        static DepartmentTypeExtension()
        {
            DepList.Add(DepartmentType.Unknown, "");
            DepList.Add(DepartmentType.VRR, "ВРР");
            DepList.Add(DepartmentType.VFSR, "ВФСР");
            DepList.Add(DepartmentType.VRS, "ВРС");
            DepList.Add(DepartmentType.VMA, "ВМА");
            DepList.Add(DepartmentType.FILIA, "Филия");
            DepList.Add(DepartmentType.URCM, "УРЧМ");
            DepList.Add(DepartmentType.URZP, "УРЗП");
            DepList.Add(DepartmentType.VRZ, "ВРЗ");
        }

        public static DepartmentType ToDepartmentType(this string val)
        {
            DepartmentType retVal;
            try
            {
                retVal = (DepartmentType)Enum.Parse(typeof(DepartmentType), val);
            }
            catch (Exception)
            {
                retVal = DepartmentType.Unknown;
            }
            return retVal;
        }

        public static string ToHumanString(this DepartmentType val)
        {
            if (DepList.ContainsKey(val))
                return DepList[val];
            return "";
        }
    }
    //======================================================
    /// <summary>
    /// Типы секторов в департамете
    /// </summary>
    public enum DepartmentSectorType
    {
        VFSR_SRZ = (1 << 0),
        VFSR_SSZ = (1 << 1),
        VFSR_SSRT = (1 << 2),
        VRR_STZ = (1 << 3),
        VRR_SSZ = (1 << 4),
        VRS_ST = (1 << 5),
        VRS_SR = (1 << 6),
        VMA_NULL = (1 << 7),
        VZR_VZR = (1 << 8),
        VMA_TR3 = (1 << 9),
    }
    //======================================================
    /// <summary>
    /// Цифровой номер управления УДЦР (использовать в очень редких случаях)
    /// Первоисточником являеться тип "ManagementUdcr"
    /// Я.К.(22.11.12): похоже, используется только в маркировке заявок на счета
    /// </summary>
    public enum ManagemenUDCR
    {
        URCP = 09,   //УРЧП
        URZP = 11,   //УРЗП
        URCM = 12,   //УРЧМ
        UAMV = 10//,   //УАМВ
       // URCP_VMA = 100, //УРЧП-ВМА
    }
    //======================================================
    /// <summary>
    /// Типы работ
    /// </summary>
    public enum WorkType
    {
        work1 = 0, //Розгляд заяви про видачу висновку що до ЕМС
        work2,     //Розгляд заявочн. докум. про надання дозв. на експл. РЕЗ (без розрахунку ЕМС)
        work3,     //Розрахунок електромагнітної сумісності РЕЗ
        work4,     //Підбір частот з розрахунком ЕМС
        work5,     //Міжнародна координація параметрів одного РЧП з однією адм. зв'язку
        work6,     //Оформлення висновків щодо ЕМС РЕЗ та ВП
        work7,     //Оформл., переоформл. та подовження дії дозволу на експл. РЕЗ, ВП
        work8,     //Оформлення та подача документів на реестрацію (нотифікацію) одного радіочастотного присвоення або судана на Міжнародному союзі електрозязку
        work9,     //Оформлення дозволу (ліцензії) на експлуатацію радіообладнання судна
        work10,    //Призначення сигналів розпізнавання береговим та судновим радіотехнологіям
        work11,    //Призначення вкороченного позивного сигналу служби аматорського радіозвязку, аматорского супутникового радіозвязку, та оформлення відповідного дозволу
        work12,    //Оформлення дозволу на ввезення з-за кордону в Україну РЕЗ та ВП (крім терміналів)
        work13,    //Оформлення дозволу на ввезення з-за кордону в Україну терміналів
        work14,    //Провеедння первинного технічного контролю РЕЗ та ВП на місці експлуатації
        work15,    //Проведення вимірювань параметрів РЕЗ та ВП на місці їх єксплуатації
        work16,    //Розгляд технологічних документів на розробку РЕЗ та іншого обладанання
        work17,    //Визначення можливомті або неможливості застосування заявленого типу РЕЗ та ВП на території України
        work18,    //Видача дозволу на реалізацію в Україні РЕЗ, ВП
        work19,    //Надання консультацій з підготовки та оформлень заявочних матеріалів, участь у роботі комісії та інше
        work20,    //Надання довідкових матеріалів
        work21,    //Радіозвязок берегових та суднових станцій
        work22,    //Ультракороткохвильовий звязок
        work23,    //Аналоговий короткохвильовий звязок
        work24,    //Пейджинговий звязок
        work25,    //Цифровий та аналоговий транкінговий радіозязок
        work26,    //Цифровий стільниковий звязок
        work27,    //Цифрова безпровідна телефонія меред радіозязку в системі з фіксованим обоненським радіодоступом стандарту DECT
        work28,    //Радіозязок передавання данних
        work29,    //Широкосмуговий радіодоступ
        work30,    //Радіорелейний звязок
        work31,    //Радіолокація, радіонавігація
        work32,    //Багатоканальне наземне телебачення
        work33,    //Мультимедійний, мультисервісний радіодоступ
        work34,    //Супутниковий радіозвязок, телеметрія та телеуправління супутникових мереж
        work35,    //Звукове мовлення
        work36,    //Телевізійне мовлення
        work37,    //Радіоподовжувачі абоненських телефонних ліній
        work38,    //Радіотелеметрія та рвдіодистанціцне курування, радіотелеметрія охоронних та пожежних систем
        work39,    //Промислові, наукові, медичні випромінювальні пристрої
        work40,    //Оформлення дозволу на експлуатацію РЕЗ на строк до 3 місяців
        work41,    //Оформлення дозволу на ввезення в Україну радіоелектронних засобів для випробувань, виставок, ярмарків та ін., без права на експлуатацію
        work42,    //Підготовка висновку щодо виділення номерного ресурсу
        work43,    //Підготовка висновку щодо переоформлення аналогового номерного ресрсу на цифровий
        work44,    //Підготовка висновку щодо виділення коду пункту сигналізації СКС-7, коду мережі сигналізації у складі нумерації мережі СКС-7
        work45,    //Підготовка висновку щодо виділення скороченого номера
        work46,    //Підготовка висновку щодо виділення коду послуг
        work47,    //Підготовка висновку щодо виділення коду MNC/NDC
        work48,    //Підготовка висновку щодо виділення вивільненого номерного ресурсу 

        work52,    //інше (УРЗП)
        work50,    //Пробіг автотранспорту

        work51,    //Работа от УРЧМ

        other = 999,     //інше
        notype = 1000,    //Невідомий тип
    }
    //======================================================
    /// <summary>
    /// Статусы заявок в ДРВ
    /// </summary>
    public enum DRVStatus
    {
        saved = 99999, //напечатан документ и сохранена заявка
        sent = 0,     //Заявка отправлена в ДРВ
        cancelled = -2,     //Заявка аннулирована
        newapl = 88888, //Создается новая заявка (не сохранена в базе)
        notoneappl = 77777, //Выбранные заявки сохранены в разных записках в ДРВ
        deleted = 66666, //Заявка удалена\
        generated = 7,   //Згенерирована в ДРВ
        printed = 100,   //Распечатана
        notprinted = 101,//Не распечатана
        auto = 102,      //Заявка отправлена в ДРВ в автоматическом режиме
        auto_gener = 103,//Сгенерировано в автоматическом режиме
        auto_added = 104,//Обработано в автоматическом режиме
        error = 99998,   //Ошибка
    }
    public static class DrvStatusExtension
    {
        public static string FromEri(this DRVStatus status)
        {
            return HelpClasses.EriFiles.GetEriDescription(status.ToString(), HelpClasses.EriFiles.StatusApplUrcmEri);
        }
    }

    //======================================================
    /// <summary>
    /// Статусы счета заявки в ДРВ
    /// </summary>
    public enum InvoiceStatus
    {
        Unknown = -100,   //Неизвестный
        Generated = 2,    //Сформирован счет
        Invoiced = 3,     //Выставлен счет
        Paid = 4,         //Оплачен счет
    }
    public static class InvoiceStatusExtension
    {
        public static string FromEri(this InvoiceStatus status)
        {
            return HelpClasses.EriFiles.GetEriDescription(status.ToString(), HelpClasses.EriFiles.InvoiceStatusEri);
        }
        public static InvoiceStatus ToInvoiceStatus(this string val)
        {
            InvoiceStatus retVal;
            try
            {
                retVal = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), val);
            }
            catch(Exception)
            {
                retVal = InvoiceStatus.Unknown;
            }
            return retVal;
        }
    }
    //======================================================
    /// <summary>
    /// Коды ошибок при обработке ответов из ДРВ
    /// </summary>
    public enum DRVError
    {
        de_1 = 1, //	значення поля ID заявки не задано 
        de_2, //	тип події не задано
        de_3, //	ID заявки не число
        de_4, //	Тип події не число
        de_5, //	значення поля Datetime не задано
        de_6, //	значення поля Accagent не задано
        de_7, //	значення поля Datetime не конвертується в дату
        de_8, //	значення поля Accagent не число
        de_9, //	поле UserGUID - значення відповідальної особи в ЦОБ не знайдено (Employee)
        de_10, //	значення поля Accnumb не задано
        de_11, //	значення поля Accdate не задано
        de_12, //	значення поля Accdate не конвертується в дату
        de_13, //	значення поля MSG пустое не задано
        de_14, //	значення поля PayDate не задано
        de_15, //	значення поля PayDate не дата не задано
        de_16, //	значення поля PayNUMB не задано
        de_17, //	значення поля FormNUMB не задано
        de_18, //	значення поля IssueDat не задано
        de_19, //	відсутня в ЦОБ заявка з відповідним ID
        de_20, //	ХМL файл не пройшов валідацію, не відповідає ХSD схемі
        de_21, // Помилка під час імпорту XML файла
        de_22, // Помилка під час імпорту данних до БД ICSM
        de_23,  // Спроба занести зміни у відсутній запис
        de_24,  // Помилка в значенні <TYPEOPER> має бути 1 а не 0
        de_25,  // значення поля NUMBER не задано
        de_26,  // значення поля DOCTYPE не задано
        de_27,  // значення поля DOCTYPE не відповідає дозволу або висновку
        de_28,  // значення поля FULL_PATCH пусте
        de_29,  // значення поля TYPE_OPER помилкове
        de_30,  // значення поля STATUS_CONTRACTS помилкове
        de_31,  // значення поля CODE_JURPERS помилкове
        de_32,  // значення поля TYPE_CONTRACTS помилкове
        de_33,  // значення поля EMPLOYEE_ID помилкове
        de_34,  // значення поля  EMPLOYEE_ID не число
        
    }
    //======================================================
    /// <summary>
    /// Тип умолчания для позиции тарифов - обычный, обл.центр, другие города, другие Прд
    /// </summary>
    public enum PriceDefaultType
    {
        pdtUsual, //usual default
        pdtRegCenter, //default for region centers
        pdtOthrCities, //default for other cities
        pdtOthrTrans //default for other transmitters
    }
    //======================================================
    /// <summary>
    /// Типы ограничений
    /// </summary>
    public enum LimitType
    {
        noLimit, //no limit 
        powerLimit, // limit for REZ power
        countLimit, //limit for REZ count 
        freqLimit, //limit for REZ frequency
        RRtrCountLimit, //limit for GSM REZ frequency
        RRaccToBSLimit, //limit for GSM REZ frequency
        RRnoFreqHop, //limit for GSM without fh
        coastStaLimit, //limit for RBSS coast stations
        seaStaLimit, //limit for RBSS sea stations
        AglLimit //limit for AGL stations
    }
}
