﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public enum PowerUnits
   {
      [Description("дБВт")] dBW = 1,
      [Description("дБm")] dBm = 2,
      [Description("Вт")] W = 3,
      [Description("кВт")] kW = 4
   };

   public class Power : ICloneable
   {
      double _power = IM.NullD;

      public double this[PowerUnits unit]
      {
         get
         {
            double result = IM.NullD;
            switch(unit)
            {
                case PowerUnits.W:
                    if (IsValid)
                    {
                        result =  _power;
                    }
                    break;                                
                case PowerUnits.kW:
                    if (IsValid)
                    {
                        result = (_power/1000.0);
                    }
                    break;
               case PowerUnits.dBm:
                    if (IsValid)
                    {
                        result = (10.0*Math.Log10(_power) + 30.0);
                    }
                    break;
               case PowerUnits.dBW:
                    if (IsValid)
                    {
                        result = (10.0*Math.Log10(_power));
                    }
                    break;
               default:
                    result = IM.NullD;
                    break;                    
            }
            return result;
         }

         set
         {
            switch (unit)
            {
                case PowerUnits.W:
                    if (value != IM.NullD)
                    {
                        _power = value;
                    }
                    else
                    {
                        _power = IM.NullD;
                    }
                    break;
                case PowerUnits.kW:
                    if (value != IM.NullD)
                    {
                        _power = value*1000.0;
                    }
                    else
                    {
                        _power = IM.NullD;
                    }
                    break;
                case PowerUnits.dBm:
                    if (value != IM.NullD)
                    {
                        _power = Math.Pow(10.0, (value - 30.0)/10.0);
                    }
                    else
                    {
                        _power = IM.NullD;
                    }
                    break;
                case PowerUnits.dBW:
                    if (value != IM.NullD)
                    {
                        _power = Math.Pow(10.0, value/10.0);
                    } else
                    {
                        _power = IM.NullD;                        
                    }
                    break;
               default:
                  break;
            }            
         }
      }     
 
      public bool IsValid
      {
         get
         {
             return _power != IM.NullD;
         }         
      }

      public object Clone()
      {
         Power clone = new Power();
         clone._power = _power;
         return clone;         
      }
   }

   public class Certificate
   {
      public DateTime Date { get; set; }
      public string Symbol { get; set; }

      public Certificate()
      {
         Date = IM.NullT;
         Symbol = "";
      }
   }

   public abstract class Equipment
   {
      protected int ID = IM.NullI;
      protected string _tableName = "";
      protected string _equipmentName = "";
      protected string _equipmentType = "";
      protected Power _maxPower = new Power();
      protected Power _maxPowerAudio = new Power();
      protected Power _maxPowerVideo = new Power();
      protected string _desigEmission = "";
      protected Certificate _certificate = new Certificate();
      
      public int Id
      {
         get { return ID; }
         set { ID = value; }
      }

      public string TableName
      {
         get { return _tableName; }
         set { _tableName = value; }
      }
      
      public string Name
      {
         get { return _equipmentName; }
         set { _equipmentName = value; }
      }

      public string EqTypes
      {
          get { return _equipmentType; }
          set { _equipmentType = value; }
      }

      public string DesigEmission
      {
         get { return _desigEmission; }
         set { _desigEmission = value; }
      }

      public Power MaxPower
      {
         get { return _maxPower; }         
      }
            
      public Certificate Certificate
      {
         get { return _certificate; }
      }

      public double BandWidth { get; set; }

      public abstract void Load();

      public virtual void Save()
      {
      }
   }
}
