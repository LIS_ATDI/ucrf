﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using Lis.CommonLib.Extensions;
using ICSM;
using System.Text;
using System.IO;
using System.Windows;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    //======================================================
    /// <summary>
    /// Перечисление широты/долготы
    /// </summary>
    enum EnumCoordLine
    {
        Lon = 1,   //долгота
        Lat = 2    //широта
    };
    //======================================================
    /// <summary>
    /// Класс с вспомагательными функциями
    /// </summary>
    class HelpFunction
    {
        //===================================================
        // Коды областных центров
        private static Dictionary<string, int> ProvinceCityCode = null;
        //===================================================
        /// <summary>
        /// Загружаем значение ячейки из БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromDbToCell(Cell cell, IMObject obj)
        {
            if ((obj == null) || (cell == null))
                return;

            if (!string.IsNullOrEmpty(cell.DBField))
            {
                if (cell.Type == "double")
                {
                    double tmpVal = obj.GetD(cell.DBField);
                    if (tmpVal != IM.NullD)
                        cell.Value = tmpVal.ToString();
                    else
                        cell.Value = "";
                }
                else if (cell.Type == "string")
                {
                    cell.Value = obj.GetS(cell.DBField);
                }
            }
        }
        /// <summary>
        /// Загружаем значение ячейки из БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromDbToCell2(Cell cell, string obj)
        {
            if ((obj == null) || (cell == null))
                return;

            if (!string.IsNullOrEmpty(cell.DBField))
                if (cell.Type == "string")
                    cell.Value = obj;
        }
        /// <summary>
        /// Загружаем значение ячейки из БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromDbToCell2(Cell cell, double obj)
        {
            if (cell == null)
                return;

            if (!string.IsNullOrEmpty(cell.DBField))
                if (cell.Type == "double")
                {
                    double tmpVal = obj;
                    if (tmpVal != IM.NullD)
                        cell.Value = tmpVal.ToStringNullD();
                    else
                        cell.Value = "";
                }
        }
        //===================================================
        /// <summary>
        /// Загружаем значение ячейки в БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromCellToDb(Cell cell, IMObject obj)
        {
            if ((obj == null) || (cell == null))
                return;

            if (cell.Type == "double")
            {
                double tmpVal = ConvertType.ToDouble(cell, IM.NullD);
                if (tmpVal != IM.NullD)
                    obj[cell.DBField] = tmpVal;
            }
            else if (cell.Type == "string")
            {
                if (!string.IsNullOrEmpty(cell.DBField))
                    obj[cell.DBField] = cell.Value;
            }
        }

        /// <summary>
        /// Загружаем значение ячейки в БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromCellToDb2(Cell cell, ref string obj)
        {
            if ((obj == null) || (cell == null))
                return;

            if (cell.Type == "string")
            {
                if (!string.IsNullOrEmpty(cell.DBField))
                    obj = cell.Value;
            }
        }
        /// <summary>
        /// Загружаем значение ячейки в БД
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="obj">ORM БД</param>
        public static void LoadFromCellToDb2(Cell cell, ref double obj)
        {
            if (cell == null)
                return;

            if (cell.Type == "double")
            {
                double tmpVal = ConvertType.ToDouble(cell, IM.NullD);
                if (tmpVal != IM.NullD)
                    obj = tmpVal;
            }
        }
        //===================================================
        /// <summary>
        /// проверяет адекватность координат
        /// </summary>
        /// <param name="coord">координата в формате DMS</param>
        /// <param name="line">широта/долгота</param>
        /// <returns>адекватна или нет</returns>
        public static bool ValidateCoords(double coord, EnumCoordLine line)
        {
            Int64 coordInt = (Int64)((coord + 0.000005) * 100000.0);
            if (coordInt < 0)
            {// Меняем знак
                coordInt = -coordInt;
            }
            // Секунды
            Int64 sec = coordInt % 1000;
            double tmp = 0;
            if ((sec % 10) != 0)
            {
                tmp = (double)sec / 10.0;
            }
            else
            {
                tmp = (double)sec / 10.0;
            }
            if (tmp >= 60)
                return false;

            // Минуты
            coordInt = coordInt / 1000;
            if (coordInt % 100 > 60)
                return false;
            // Градусы
            coordInt = coordInt / 100;
            if (line == EnumCoordLine.Lon)
            {
                if (coordInt % 100 > 180)
                    return false;
            }
            else
            {
                if (coordInt % 100 > 90)
                    return false;
            }
            return true;
        }
        //===================================================
        /// <summary>
        /// Конвертирует DMS в строку
        /// </summary>
        /// <param name="coord">координата в формате DMS</param>
        /// <param name="line">широта/долгота</param>
        /// <returns>строку в формате ГГ ММ' СС"</returns>
        public static string DmsToString(double coord, EnumCoordLine line)
        {
            if (coord == IM.NullD)
                return "";

            Int64 coordInt = (Int64)((coord + 0.000005) * 100000.0);
            Char[] symbol = new Char[] { '\xB0', '\x27' };
            bool isNegative = false;
            if (coordInt < 0)
            {// Меняем знак
                isNegative = true;
                coordInt = -coordInt;
            }
            // Секунды
            Int64 sec = coordInt % 1000;
            string seconds = "";
            if ((sec % 10) != 0)
            {
                double tmp = (double)sec / 10.0;
                seconds = tmp.ToString(" 00.0");
            }
            else
            {
                double tmp = (double)sec / 10.0;
                seconds = tmp.ToString(" 00");
            }
            seconds += Convert.ToString(symbol[1]) + Convert.ToString(symbol[1]);
            // Минуты
            coordInt = coordInt / 1000;
            string minutes = (coordInt % 100).ToString(" 00") + Convert.ToString(symbol[1]);
            // Градусы
            coordInt = coordInt / 100;
            string degree = (coordInt % 100).ToString("00") + Convert.ToString(symbol[0]);
            if (line == EnumCoordLine.Lon)
            {
                degree += (isNegative) ? "W" : "E";
            }
            else
            {
                degree += (isNegative) ? "S" : "N";
            }
            return degree + minutes + seconds;
        }
        //===================================================
        /// <summary>
        /// Конвертирует список в строку
        /// </summary>
        /// <param name="list">список</param>
        /// <returns>созданая строка</returns>
        public static string ToString<T>(List<T> list)
        {
            string retStr = "";
            foreach (T value in list)
            {
                if (retStr.Length > 0)
                    retStr += "; ";

                retStr += value.ToString();
            }
            return retStr;
        }
        //===================================================
        /// <summary>
        /// Конвертирует Double в Строку с разделителем "."
        /// </summary>
        /// <param name="value">число</param>
        /// <returns>созданая строка</returns>
        public static string DoubleToStringPoint(double value)
        {
            string retStr = value.ToString();
            return retStr.Replace(',', '.');
        }
        //===================================================
        /// <summary>
        /// Заполняет ячейку списком чисел
        /// </summary>
        /// <param name="cell">ячейка</param>
        /// <param name="list">список чисел</param>
        public static void ToString<T>(Cell cell, List<T> list)
        {
            if (cell != null)
            {
                cell.Value = ToString<T>(list);
            }
        }

        //===================================================
        /// <summary>
        /// Replaces quita symbols
        /// заменяет символы с кавычками
        /// </summary>
        public static string ReplaceQuotaSumbols(string oldValue)
        {
            //TODO Если все будет отлично работать, то удалить комментарии
            // Проблема была в том, что при передаче строки с символом '*'
            // ICSM выдавал ошибку.

            // Сейчас просто подмениваю симол '*' на '?'. Это почти не влияет на результат запроса
            //string retVal = oldValue.Replace('*', '?').Replace('"', '?');

            // убрал подмену звездочки (со звездочкой работает нормально),
            // оставил символ '"' (двойная кавычка)
            string retVal = oldValue.Replace('"', '?');
            retVal = retVal.Replace('\'', '?');
            if (retVal.Length > 48)
                retVal = retVal.Remove(48);
            return retVal;

        }
        //===================================================
        /// <summary>
        /// Возвращает идентификатор подключенного пользователя
        /// </summary>
        /// <returns>идентификатор подключенного пользователя</returns>
        public static string getUserNumber()
        {
            string userName = IM.ConnectedUser();
            string retVal = "";
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            r.Select("APP_USER,IDENT");
            r.SetWhere("APP_USER", IMRecordset.Operation.Like, userName);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetS("IDENT");
                while (retVal.Length < 2)
                    retVal = "0" + retVal;
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Возвращает Имя пользователя по ID
        /// </summary>
        /// <returns>ФИО пользователя</returns>
        public static string getUserFIOByID(int id)
        {
            string userName = IM.ConnectedUser();
            string retVal = "";
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            r.Select("ID,LASTNAME");
            r.SetWhere("ID", IMRecordset.Operation.Eq, id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetS("LASTNAME");
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Возвращает код региону по КОАТУУ
        /// </summary>
        /// <returns>код региону по КОАТУУ</returns>
        public static string getAreaCode(string province, string city)
        {
            string retVal = "";
            IMRecordset r = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
            r.Select("ID,CODE,NAME");
            r.SetWhere("NAME", IMRecordset.Operation.Like, province);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    retVal = r.GetS("CODE");
                    while (retVal.Length < 2)
                        retVal = "0" + retVal;
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            //----------------------------------
            if (string.IsNullOrEmpty(retVal))
            {// Проверка на областные центры
                if (ProvinceCityCode == null)
                {// Инициализируем словарь
                    ProvinceCityCode = new Dictionary<string, int>();
                    ProvinceCityCode.Add("Вінниця", 5);
                    ProvinceCityCode.Add("Дніпропетровськ", 12);
                    ProvinceCityCode.Add("Донецьк", 14);
                    ProvinceCityCode.Add("Житомир", 18);
                    ProvinceCityCode.Add("Запоріжжя", 23);
                    ProvinceCityCode.Add("Івано-Франківськ", 26);
                    ProvinceCityCode.Add("Київ", 80);
                    ProvinceCityCode.Add("Кіровоград", 35);
                    ProvinceCityCode.Add("Луганськ", 9);
                    ProvinceCityCode.Add("Луцьк", 7);
                    ProvinceCityCode.Add("Львів", 46);
                    ProvinceCityCode.Add("Миколаїв", 48);
                    ProvinceCityCode.Add("Одеса", 51);
                    ProvinceCityCode.Add("Полтава", 53);
                    ProvinceCityCode.Add("Рівне", 56);
                    ProvinceCityCode.Add("Севастополь", 85);
                    ProvinceCityCode.Add("Сімферополь", 43);
                    ProvinceCityCode.Add("Суми", 59);
                    ProvinceCityCode.Add("Тернопіль", 61);
                    ProvinceCityCode.Add("Ужгород", 21);
                    ProvinceCityCode.Add("Харків", 63);
                    ProvinceCityCode.Add("Херсон", 65);
                    ProvinceCityCode.Add("Хмельницький", 68);
                    ProvinceCityCode.Add("Черкаси", 71);
                    ProvinceCityCode.Add("Чернівці", 73);
                    ProvinceCityCode.Add("Чернігів", 74);
                }

                if (ProvinceCityCode.ContainsKey(city))
                {
                    retVal = ProvinceCityCode[city].ToString();
                    while (retVal.Length < 2)
                        retVal = "0" + retVal;
                }
            }
            //---------------------------
            if (string.IsNullOrEmpty(retVal))
                throw new IMException("Can't find the province code \"" + province + "\" or city code \"" + city + "\"");
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Возвращает Имя пользователя по ID
        /// </summary>
        /// <returns>ФИО пользователя</returns>
        public static int getUserIDByName(string userName)
        {
            int retVal = 0;
            IMRecordset r = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.SetWhere("APP_USER", IMRecordset.Operation.Like, userName);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetI("ID");
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static string DiagToString(List<double> values, short step)
        {
            StringBuilder sBuilder = new StringBuilder();

            sBuilder.Append("VECTOR ");
            sBuilder.Append(step);

            if (values.Count == 0)
            {
                for (int i = 0; i < 36; i++)
                    values.Add(0);
            }

            foreach (double val in values)
            {
                sBuilder.Append(" ");
                sBuilder.Append(val);
            }

            return sBuilder.ToString();
        }

        public static string GainedDiagToString(List<double> values, short step, double Gain)
        {
            StringBuilder sBuilder = new StringBuilder();

            sBuilder.Append("VECTOR ");
            sBuilder.Append(step);

            if (values.Count == 0)
            {
                for (int i = 0; i < 36; i++)
                    values.Add(0);
            }

            foreach (double val in values)
            {
                sBuilder.Append(" ");
                NumberFormatInfo customProvider = new NumberFormatInfo();
                IFormatProvider provider = customProvider;
                customProvider.NumberDecimalSeparator = ".";
                sBuilder.Append((Gain - val).ToString(provider));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="diagram"></param>
        /// <returns></returns>
        public static List<double> StringToDiag(string diagram)
        {
            List<double> result = new List<double>();
            if (diagram.Contains("VECTOR"))
            {
                string[] data = diagram.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (data[1] == "5")
                {
                    bool skip = false;
                    for (short i = 2; i < data.Count(); i++)
                    {
                        if (!skip)
                        {
                            result.Add(ConvertType.ToDouble(data[i], 0.0));
                        }

                        skip = !skip;
                    }
                }
                else
                {
                    for (short i = 2; i < data.Count(); i++)
                    {
                        result.Add(ConvertType.ToDouble(data[i], 0.0));
                    }
                }
            }
            return result;
        }

        public static List<double> StringToGainedDiagEx(string diagram, double Gain)
        {
            if (diagram.Equals("OMNI"))
            {
                List<double> result = new List<double>();
                for (int i = 0; i < 36; i++)
                    result.Add(Gain);

                return result;
            }
            else
                return StringToGainedDiag(diagram, Gain);

        }

        public static List<double> StringToGainedDiag(string diagram, double Gain)
        {
            List<double> result = new List<double>();
            if (diagram.Contains("VECTOR"))
            {
                string[] data = diagram.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (data[1] == "5")
                {
                    bool skip = false;
                    for (short i = 2; i < data.Count(); i++)
                    {
                        if (!skip)
                        {
                            result.Add(ConvertType.ToDouble(data[i], 0.0));
                        }

                        skip = !skip;
                    }
                }
                else
                {
                    for (short i = 2; i < data.Count(); i++)
                    {
                        result.Add(Gain - ConvertType.ToDouble(data[i], 0.0));
                    }
                }
            }
            return result;
        }

        public static double GetBandwidthFromDesigEmission(string DesigEmission)
        {
            if (DesigEmission.Length >= 4)
            {
                string BandWidthSign = DesigEmission.Substring(0, 4);

                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InstalledUICulture;
                System.Globalization.NumberFormatInfo ni = (System.Globalization.NumberFormatInfo)ci.NumberFormat.Clone();
                ni.NumberDecimalSeparator = ".";

                double Multiplier = 1.0;
                if (BandWidthSign.Contains("K"))
                {
                    BandWidthSign = BandWidthSign.Replace('K', '.');
                    Multiplier = 1000.0;
                }

                if (BandWidthSign.Contains("M"))
                {
                    BandWidthSign = BandWidthSign.Replace('M', '.');
                    Multiplier = 1000000.0;
                }
                double BW = double.Parse(BandWidthSign, ni);
                return BW * Multiplier;
            }
            else
            {
                return 0;
            }
        }
        //===================================================
        /// <summary>
        /// Считает данные из таблицы SYS_CONFIG
        /// </summary>
        /// <param name="filter">фильтр записи</param>
        /// <returns>данные из поля WHAT (INTEGER)</returns>
        public static int ReadIntFromSysConfig(string filter)
        {
            return ReadDataFromSysConfig(filter).ToInt32(IM.NullI);
        }
        //===================================================
        /// <summary>
        /// Считает данные из таблицы SYS_CONFIG
        /// </summary>
        /// <param name="filter">фильтр записи</param>
        /// <returns>данные из поля WHAT (INTEGER)</returns>
        public static bool ReadBoolFromSysConfig(string filter)
        {
            string res = ReadDataFromSysConfig(filter).ToLower();
            return (res == "1") || (res == "true");
        }
        //===================================================
        /// <summary>
        /// Считает данные из таблицы SYS_CONFIG
        /// </summary>
        /// <param name="filter">фильтр записи</param>
        /// <returns>данные из поля WHAT</returns>
        public static string ReadDataFromSysConfig(string filter)
        {
            string retVal = string.Empty;
            IMRecordset r = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
            r.Select("WHAT");
            r.SetWhere("ITEM", IMRecordset.Operation.Like, filter);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetS("WHAT");
            }
            finally
            {
                r.Destroy();
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Сохраняет/Обновляет данные в таблице SYS_CONFIG
        /// </summary>
        /// <param name="filter">фильтр записи</param>
        /// <param name="data">данные</param>
        public static void WriteDataToSysConfig(string filter, int data)
        {
            WriteDataToSysConfig(filter, data.ToString());
        }
        /// <summary>
        /// Сохраняет/Обновляет данные в таблице SYS_CONFIG
        /// </summary>
        /// <param name="filter">параметр</param>
        /// <param name="data">значение</param>
        public static void WriteDataToSysConfig(string filter, bool data)
        {
            WriteDataToSysConfig(filter, data ? "true" : "false");
        }
        //===================================================
        /// <summary>
        /// Сохраняет/Обновляет данные в таблице SYS_CONFIG
        /// </summary>
        /// <param name="filter">фильтр записи</param>
        /// <param name="data">данные</param>
        public static void WriteDataToSysConfig(string filter, string data)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadWrite);
            r.Select("WHAT,ITEM");
            r.SetWhere("ITEM", IMRecordset.Operation.Like, filter);
            try
            {
                r.Open();
                if (r.IsEOF())
                {
                    r.AddNew();
                    r.Put("ITEM", filter);
                }
                else
                {
                    r.Edit();
                }
                r.Put("WHAT", data);
                r.Update();
            }
            finally
            {
                r.Destroy();
            }
        }

        public static bool AreSameDoubleLists(List<double> rxFreqList, List<double> txFreqList)
        {
            if (rxFreqList.Count != txFreqList.Count)
                return false;

            foreach (double d in rxFreqList)
            {
                if (!txFreqList.Contains(d))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Передвинуть аттучмент с одной записи на другую
        /// </summary>
        /// <param name="tableNameFrom"></param>
        /// <param name="idFrom"></param>
        /// <param name="tableNameTo"></param>
        /// <param name="idTo"></param>
        public static void MoveAttachment(string tableNameFrom, int idFrom, string tableNameTo, int idTo)
        {
            IMRecordset fotoRec;

            fotoRec = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
            fotoRec.Select("REC_TAB,REC_ID,PATH");
            fotoRec.SetWhere("REC_TAB", IMRecordset.Operation.Eq, tableNameFrom);
            fotoRec.SetWhere("REC_ID", IMRecordset.Operation.Eq, idFrom);

            //fotoRec.SetWhere("PATH", IMRecordset.Operation.Eq, fileName);
            for (fotoRec.Open(); !fotoRec.IsEOF(); fotoRec.MoveNext())
            {
                fotoRec.Edit();
                fotoRec.Put("REC_TAB", tableNameTo);
                fotoRec.Put("REC_ID", idTo);
                fotoRec.Update();
            }
            //fotoRec.Delete();
            //if (fotoRec.IsOpen())*/
            fotoRec.Close();
            fotoRec.Destroy();
        }

        /// <summary>
        /// Вертає всі Id станцій для даної заявки з полів OBJ_IDx(1..6)
        /// </summary>
        public static List<int> GetListIdAppl(int _id)
        {
            List<int> objIdLst = new List<int>();
            IMRecordset rStat = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
            rStat.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
            rStat.SetWhere("ID", IMRecordset.Operation.Eq, _id);
            rStat.Open();
            if (!rStat.IsEOF())
                for (int i = 1; i <= 6; i++)
                {
                    int objInt = rStat.GetI("OBJ_ID" + i);
                    if (objInt != IM.NullI)
                        objIdLst.Add(rStat.GetI("OBJ_ID" + i));
                }
            rStat.Final();
            return objIdLst;
        }

        /// <summary>
        /// Функция корректировки файла *.LIS
        /// </summary>
        /// <param name="NameFile"></param>
        public static void CorrectFile_LIS(string NameFile, string Net_, string Additional_path, int _ID)
        {
            try
            {
                if (!System.IO.Path.GetExtension(NameFile).ToLower().Contains("xml"))
                {
                    const int MaxL = 218;
                    int Curr_Max = 0;
                    int idx_mul_max = 1;
                    using (System.IO.StreamReader read = new System.IO.StreamReader(NameFile, System.Text.Encoding.Default))
                    {
                        string s = "";
                        string repl_text = "";
                        while ((s = read.ReadLine()) != null)
                        {
                            repl_text = s;
                            double MX = s.Length / 219;
                            idx_mul_max = (int)Math.Round(MX);
                            for (int idx_mul = 1; idx_mul < idx_mul_max; idx_mul++)
                            {
                                if (s.Length > 219)
                                {
                                    if (idx_mul>1) Curr_Max = Curr_Max + 219;


                                    string key_="";
                                    string class_="";
                                    string radius_="";
                                    string asl_ = "";

                                    string freq1_11_corr = "";
                                    string freq1_11 = s.Substring(MaxL + (Curr_Max+1) , 11); double freq1_11_;
                                    freq1_11_corr = freq1_11; int freq1_11__v1 = 0; int freq1_11__v2 = 0; string freq1_11__v11=""; string freq1_11__v22="";
                                    if (double.TryParse(freq1_11.ToString().Trim().Replace(".", ","), out freq1_11_)) { freq1_11__v1 = (int)freq1_11_; freq1_11__v11 = freq1_11__v1.ToString().PadLeft(5, ' '); freq1_11__v22 = (freq1_11_ - freq1_11__v1).ToString("F5"); if (freq1_11__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_11__v22.Replace(",", ".").IndexOf('.') + 1; freq1_11__v22 = freq1_11__v22.Replace(",", ".").Substring(start, freq1_11__v22.Replace(",", ".").Length - start).PadRight(5, '0'); freq1_11_corr = freq1_11__v11 + "." + freq1_11__v22; } }  // freq1_11_corr = freq1_11_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq1_11_.ToString().Length <= 9 ? freq1_11_.ToString().Insert(freq1_11_.ToString().Length, ".").PadRight(11, '0') : freq1_11_.ToString().PadLeft(11, '0')) : freq1_11_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 1) , 11).Insert(MaxL + (Curr_Max + 1) , freq1_11_corr);


                                    string freq72_75_corr = "";
                                    string freq72_75 = s.Substring(MaxL + (Curr_Max + 72) , 4); double freq72_75_;
                                    freq72_75_corr = freq72_75;
                                    if (double.TryParse(freq72_75.ToString().Trim().Replace(".", ","), out freq72_75_)) { freq72_75_corr = freq72_75_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq72_75_.ToString().Length <= 2 ? freq72_75_.ToString().Insert(freq72_75_.ToString().Length, ".").PadRight(4, '0') : freq72_75_.ToString().PadLeft(4, ' ')) : freq72_75_.ToString().Trim().Replace(",", ".").PadLeft(4, ' '); }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 72) , 4).Insert(MaxL + (Curr_Max + 72) , freq72_75_corr);

                                    //string freq85_90_corr = "";
                                    //string freq85_90 = s.Substring(MaxL + (Curr_Max + 85) , 6); double freq85_90_;
                                    //freq85_90_corr = freq85_90;
                                    //if (double.TryParse(freq85_90.ToString().Trim().Replace(".", ","), out freq85_90_)) { freq85_90_corr = freq85_90_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq85_90_.ToString().Length <= 4 ? freq85_90_.ToString().Insert(freq85_90_.ToString().Length, ".").PadRight(6, '0') : freq85_90_.ToString().PadLeft(6, '0')) : freq85_90_.ToString().Trim().Replace(",", ".").PadLeft(6, ' '); }
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 85) , 6).Insert(MaxL + (Curr_Max + 85) , freq85_90_corr);

                                    string freq85_90_corr = "";
                                    string freq85_90 = s.Substring(MaxL + (Curr_Max + 85), 6); double freq85_90_;
                                    freq85_90_corr = freq85_90; int freq1_85__v1 = 0; int freq1_85__v2 = 0; string freq1_85__v11 = ""; string freq1_85__v22 = "";
                                    if (double.TryParse(freq85_90.ToString().Trim().Replace(".", ","), out freq85_90_)) { freq1_85__v1 = (int)freq85_90_; freq1_85__v11 = freq1_85__v1.ToString().PadLeft(4, ' '); freq1_85__v22 = (freq85_90_ - freq1_85__v1).ToString("F1"); if (freq1_85__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_85__v22.Replace(",", ".").IndexOf('.') + 1; freq1_85__v22 = freq1_85__v22.Replace(",", ".").Substring(start, freq1_85__v22.Replace(",", ".").Length - start).PadRight(1, '0'); freq85_90_corr = freq1_85__v11 + "." + freq1_85__v22; } }  // freq1_11_corr = freq1_11_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq1_11_.ToString().Length <= 9 ? freq1_11_.ToString().Insert(freq1_11_.ToString().Length, ".").PadRight(11, '0') : freq1_11_.ToString().PadLeft(11, '0')) : freq1_11_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 85), 6).Insert(MaxL + (Curr_Max + 85), freq85_90_corr);


                                    

                                    //string freq92_96_corr = "";
                                    //string freq92_96 = s.Substring(MaxL + (Curr_Max + 92), 5); double freq92_96_;
                                    //freq92_96_corr = freq92_96; int freq1_92__v1 = 0; int freq1_92__v2 = 0; string freq1_92__v11 = ""; string freq1_92__v22 = "";
                                    //if (double.TryParse(freq92_96.ToString().Trim().Replace(".", ","), out freq92_96_)) { freq1_92__v1 = (int)freq92_96_; freq1_92__v11 = freq1_92__v1.ToString().PadLeft(3, ' '); freq1_92__v22 = (freq85_90_ - freq1_92__v1).ToString("F1"); if (freq1_92__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_92__v22.Replace(",", ".").IndexOf('.') + 1; freq1_92__v22 = freq1_92__v22.Replace(",", ".").Substring(start, freq1_92__v22.Replace(",", ".").Length - start).PadRight(1, '0'); freq92_96_corr = freq1_92__v11 + "." + freq1_92__v22; } }  // freq1_11_corr = freq1_11_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq1_11_.ToString().Length <= 9 ? freq1_11_.ToString().Insert(freq1_11_.ToString().Length, ".").PadRight(11, '0') : freq1_11_.ToString().PadLeft(11, '0')) : freq1_11_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    //if ((freq1_92__v1 == 0) && (freq1_92__v2 == 0)) freq92_96_corr = "".PadRight(5, ' ');
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 92), 5).Insert(MaxL + (Curr_Max + 92), freq92_96_corr);


                                    //string freq104_107_corr = "";
                                    //string freq104_107 = s.Substring(MaxL + (Curr_Max + 104) , 4); double freq104_107_;
                                    //freq104_107_corr = freq104_107;
                                    //if (double.TryParse(freq104_107.ToString().Trim().Replace(".", ","), out freq104_107_)) { freq104_107_corr = freq104_107_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq104_107_.ToString().Length <= 2 ? freq104_107_.ToString().Insert(freq104_107_.ToString().Length, ".").PadRight(4, '0') : freq104_107_.ToString().PadLeft(4, '0')) : freq104_107_.ToString().Trim().Replace(",", ".").PadLeft(4, ' '); }
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 104) , 4).Insert(MaxL + (Curr_Max + 104) , freq104_107_corr);


                                    //string freq97_101_corr = "";
                                    //string freq97_101 = s.Substring(MaxL + (Curr_Max + 97), 5); double freq97_101_;
                                    //freq97_101_corr = freq97_101;
                                    //if (double.TryParse(freq97_101.ToString().Trim().Replace(".", ","), out freq97_101_)) { freq97_101_corr = freq97_101_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq97_101_.ToString().Length <= 2 ? freq97_101_.ToString().Insert(freq97_101_.ToString().Length, ".").PadRight(4, '0') : freq97_101_.ToString().PadLeft(4, ' ')) : freq97_101_.ToString().Trim().Replace(",", ".").PadLeft(4, ' '); }
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 97), 5).Insert(MaxL + (Curr_Max + 97), freq97_101_corr);

                                    string freq97_101_corr = "";
                                    string freq97_101 = s.Substring(MaxL + (Curr_Max + 97), 5); double freq97_101_;
                                    freq97_101_corr = freq97_101; int freq1_97_101__v1 = 0; int freq1_97_101__v2 = 0; string freq1_97_101__v11 = ""; string freq1_97_101__v22 = "";
                                    if (double.TryParse(freq97_101.ToString().Trim().Replace(".", ","), out freq97_101_)) { freq1_97_101__v1 = (int)freq97_101_; freq1_97_101__v11 = (freq1_97_101__v1 >= 0 ? freq1_97_101__v1.ToString().PadLeft(3, ' ') : "-"+Math.Abs(freq1_97_101__v1).ToString().PadLeft(2, ' ')); freq1_97_101__v22 = (freq97_101_ - freq1_97_101__v1).ToString("F1"); if (freq1_97_101__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_97_101__v22.Replace(",", ".").IndexOf('.') + 1; freq1_97_101__v22 = freq1_97_101__v22.Replace(",", ".").Substring(start, freq1_97_101__v22.Replace(",", ".").Length - start).PadRight(1, '0'); freq97_101_corr = freq1_97_101__v11 + "." + freq1_97_101__v22; } }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 97), 5).Insert(MaxL + (Curr_Max + 97), freq97_101_corr);


                                    string freq104_107_corr = "";
                                    string freq104_107 = s.Substring(MaxL + (Curr_Max + 104), 4); double freq104_107_;
                                    freq104_107_corr = freq104_107; int freq1_107__v1 = 0; int freq1_107__v2 = 0; string freq1_107__v11 = ""; string freq1_107__v22 = "";
                                    if (double.TryParse(freq104_107.ToString().Trim().Replace(".", ","), out freq104_107_)) { freq1_107__v1 = (int)freq104_107_; freq1_107__v11 = freq1_107__v1.ToString().PadLeft(2, ' '); freq1_107__v22 = (freq104_107_ - freq1_107__v1).ToString("F1"); if (freq1_107__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_107__v22.Replace(",", ".").IndexOf('.') + 1; freq1_107__v22 = freq1_107__v22.Replace(",", ".").Substring(start, freq1_107__v22.Replace(",", ".").Length - start).PadRight(1, '0'); freq104_107_corr = freq1_107__v11 + "." + freq1_107__v22; } }  // freq1_11_corr = freq1_11_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq1_11_.ToString().Length <= 9 ? freq1_11_.ToString().Insert(freq1_11_.ToString().Length, ".").PadRight(11, '0') : freq1_11_.ToString().PadLeft(11, '0')) : freq1_11_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 104), 4).Insert(MaxL + (Curr_Max + 104), freq104_107_corr);


                                    //string freq108_111_corr = "";
                                    //string freq108_111 = s.Substring(MaxL + (Curr_Max + 108) , 4); double freq108_111_;
                                    //freq108_111_corr = freq108_111;
                                    //if (double.TryParse(freq108_111.ToString().Trim().Replace(".", ","), out freq108_111_)) { freq108_111_corr = freq108_111_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq108_111_.ToString().Length <= 2 ? freq108_111_.ToString().Insert(freq108_111_.ToString().Length, ".").PadRight(4, '0') : freq108_111_.ToString().PadLeft(4, '0')) : freq108_111_.ToString().Trim().Replace(",", ".").PadLeft(4, ' '); }
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 108) , 4).Insert(MaxL + (Curr_Max + 108) , freq108_111_corr);

                                    string freq108_111_corr = "";
                                    string freq108_111 = s.Substring(MaxL + (Curr_Max + 108), 4); double freq108_111_;
                                    freq108_111_corr = freq108_111; int freq1_108__v1 = 0; int freq1_108__v2 = 0; string freq1_108__v11 = ""; string freq1_108__v22 = "";
                                    if (double.TryParse(freq108_111.ToString().Trim().Replace(".", ","), out freq108_111_)) { freq1_108__v1 = (int)freq108_111_; freq1_108__v11 = freq1_108__v1.ToString().PadLeft(4, ' '); freq108_111_corr = freq1_108__v11; }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 108), 4).Insert(MaxL + (Curr_Max + 108), freq108_111_corr);


                                    //string freq126_136_corr = "";
                                    //string freq126_136 = s.Substring(MaxL + (Curr_Max + 126) , 11); double freq126_136_;
                                    //freq126_136_corr = freq126_136;
                                    //if (double.TryParse(freq126_136.ToString().Trim().Replace(".", ","), out freq126_136_)) { freq126_136_corr = freq126_136_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq126_136_.ToString().Length <= 9 ? freq126_136_.ToString().Insert(freq126_136_.ToString().Length, ".").PadRight(11, '0') : freq126_136_.ToString().PadLeft(11, '0')) : freq126_136_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    //repl_text = repl_text.Remove(MaxL + (Curr_Max + 126) , 11).Insert(MaxL + (Curr_Max + 126) , freq126_136_corr);
                                    /////
                                    string freq126_136_corr = "";
                                    string freq126_136 = s.Substring(MaxL + (Curr_Max + 126), 11); double freq126_136_;
                                    freq126_136_corr = freq126_136; int freq1_126__v1 = 0; int freq1_126__v2 = 0; string freq1_126__v11 = ""; string freq1_126__v22 = "";
                                    if (double.TryParse(freq126_136.ToString().Trim().Replace(".", ","), out freq126_136_)) { freq1_126__v1 = (int)freq126_136_; freq1_126__v11 = freq1_126__v1.ToString().PadLeft(5, ' '); freq1_126__v22 = (freq126_136_ - freq1_126__v1).ToString("F5"); if (freq1_126__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_126__v22.Replace(",", ".").IndexOf('.') + 1; freq1_126__v22 = freq1_126__v22.Replace(",", ".").Substring(start, freq1_126__v22.Replace(",", ".").Length - start).PadRight(5, '0'); freq126_136_corr = freq1_126__v11 + "." + freq1_126__v22; } }  // freq1_11_corr = freq1_11_.ToString().Replace(",", ".").IndexOf('.') == -1 ? (freq1_11_.ToString().Length <= 9 ? freq1_11_.ToString().Insert(freq1_11_.ToString().Length, ".").PadRight(11, '0') : freq1_11_.ToString().PadLeft(11, '0')) : freq1_11_.ToString().Trim().Replace(",", ".").PadLeft(11, ' '); }
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 126), 11).Insert(MaxL + (Curr_Max + 126), freq126_136_corr);
                                    ////

                                    /// получить ключ записи
                                    /// 
                                    string azim = "";
                                    string key_corr = "";
                                    key_corr = s.Substring(MaxL + (Curr_Max + 210), 10);
                                    IMRecordset r_wien_ans_mob = new IMRecordset("WIEN_ANS_MOB", IMRecordset.Mode.ReadOnly);
                                    r_wien_ans_mob.Select("HEADREQ_ID,CO_LINK_ID");
                                    r_wien_ans_mob.SetWhere("HEADREQ_ID", IMRecordset.Operation.Eq, _ID);
                                    for (r_wien_ans_mob.Open(); !r_wien_ans_mob.IsEOF(); r_wien_ans_mob.MoveNext())
                                    {
                                        bool isFnd = false;
                                        IMRecordset r_wien_coord_mob = new IMRecordset("WIEN_COORD_MOB", IMRecordset.Mode.ReadOnly);
                                        r_wien_coord_mob.Select("ID,P6,F2,R1,O1,CLASS,RADIUS,ASL,AZIMUTH");
                                        r_wien_coord_mob.SetWhere("ID", IMRecordset.Operation.Eq, r_wien_ans_mob.GetI("CO_LINK_ID"));
                                        for (r_wien_coord_mob.Open(); !r_wien_coord_mob.IsEOF(); r_wien_coord_mob.MoveNext())
                                        {
                                            //210-219 (10)
                                            string Key = r_wien_coord_mob.GetS("P6").PadLeft(6, '0') + (r_wien_coord_mob.GetI("F2") != IM.NullI ? r_wien_coord_mob.GetI("F2").ToString().PadLeft(2, '0') : "00") + (r_wien_coord_mob.GetI("R1") != IM.NullI ? r_wien_coord_mob.GetI("R1").ToString().PadLeft(1, '0') : "0") + (r_wien_coord_mob.GetI("O1") != IM.NullI ? r_wien_coord_mob.GetI("O1").ToString().PadLeft(1, '0') : "0");
                                            if (Key == key_corr)
                                            {
                                                key_ = r_wien_coord_mob.GetS("P6").PadLeft(6, '0') + (r_wien_coord_mob.GetI("F2") != IM.NullI ? r_wien_coord_mob.GetI("F2").ToString().PadLeft(2, ' ') : "00") + (r_wien_coord_mob.GetI("R1") != IM.NullI ? r_wien_coord_mob.GetI("R1").ToString().PadLeft(1, ' ') : "0") + (r_wien_coord_mob.GetI("O1") != IM.NullI ? r_wien_coord_mob.GetI("O1").ToString().PadLeft(1, ' ') : "0");
                                                //14-15 (2)
                                                class_ = r_wien_coord_mob.GetS("CLASS").PadLeft(2, ' ');
                                                //67-71 (5)
                                                double radius = r_wien_coord_mob.GetD("RADIUS"); radius_ = (((radius != IM.NullD) && (radius != IM.NullI)) ? ((int)Math.Round(radius)).ToString().PadLeft(5, ' ') : "     ");
                                                //72-75 (4)
                                                double ASL = r_wien_coord_mob.GetD("ASL"); asl_ = (ASL!=IM.NullD ? ((int)Math.Round(ASL)).ToString().PadLeft(4, ' ') : "    ");
                                                //92-96

                                                string freq92_96_corr = ""; double freq92_96_;
                                                freq92_96_ =  r_wien_coord_mob.GetD("AZIMUTH");
                                                int freq1_92__v1 = 0; int freq1_92__v2 = 0; string freq1_92__v11 = ""; string freq1_92__v22 = "";
                                                if (double.TryParse(freq92_96_.ToString().Trim().Replace(".", ","), out freq92_96_)) { freq1_92__v1 = (int)freq92_96_; freq1_92__v11 = freq1_92__v1.ToString().PadLeft(3, ' '); freq1_92__v22 = (freq92_96_ - freq1_92__v1).ToString("F1"); if (freq1_92__v22.Replace(",", ".").IndexOf('.') != -1) { int start = freq1_92__v22.Replace(",", ".").IndexOf('.') + 1; freq1_92__v22 = freq1_92__v22.Replace(",", ".").Substring(start, freq1_92__v22.Replace(",", ".").Length - start).PadRight(1, '0'); freq92_96_corr = freq1_92__v11 + "." + freq1_92__v22; } }
                                                repl_text = repl_text.Remove(MaxL + (Curr_Max + 92), 5).Insert(MaxL + (Curr_Max + 92), freq92_96_corr);

                                                isFnd = true;
                                                break;
                                            }
                                        }
                                        r_wien_coord_mob.Close();
                                        r_wien_coord_mob.Destroy();
                                        if (isFnd) break;
                                    }
                                        
                                    r_wien_ans_mob.Close();
                                    r_wien_ans_mob.Destroy();               


                                    //14-15
                                    string freq14_15_corr = "";
                                    string freq14_15 = s.Substring(MaxL + (Curr_Max + 14) , 2);
                                    if (string.IsNullOrEmpty(freq14_15.Trim()))
                                    {
                                        repl_text = repl_text.Remove(MaxL + (Curr_Max + 14) , 2).Insert(MaxL + (Curr_Max + 14) , class_);
                                    }

                                    //string freq210 = s.Substring((MaxL + 210) , 10);
                                    repl_text = repl_text.Remove(MaxL + (Curr_Max + 210) , 10);
                                    repl_text = repl_text.Insert(MaxL + (Curr_Max + 210), key_corr);

                                    if (class_.Length > 0)
                                    {
                                        if (class_[0] == 'M')
                                        {
                                            repl_text = repl_text.Remove(MaxL + (Curr_Max + 67), 5).Insert(MaxL + (Curr_Max + 67) , radius_);
                                            repl_text = repl_text.Remove(MaxL + (Curr_Max + 92), 5).Insert(MaxL + (Curr_Max + 92), "     ");
                                        }
                                        else if (class_[0] == 'F')
                                        {
                                            //repl_text = repl_text.Remove(MaxL + (Curr_Max + 67), 5).Insert(MaxL + (Curr_Max + 67), "    0");
                                            repl_text = repl_text.Remove(MaxL + (Curr_Max + 67), 5).Insert(MaxL + (Curr_Max + 67), "     ");
                                            repl_text = repl_text.Remove(MaxL + (Curr_Max + 72) , 4).Insert(MaxL + (Curr_Max + 72) , asl_);
                                        }
                                    }


                                    

                                }

                            }
                        }


                        if (!string.IsNullOrEmpty(repl_text))
                        {
                            if (System.IO.File.Exists(NameFile))
                            {
                                string NM_FILE = Additional_path.Replace("/", "_");
                                string Net_Out = Net_ + @"\" + NM_FILE;
                              
                                using (StreamWriter fs = new StreamWriter((System.IO.Path.GetDirectoryName(Net_Out) + @"\" + System.IO.Path.GetFileNameWithoutExtension(Net_Out) + "_Correct.txt"), false))
                                {
                                    fs.Write(repl_text);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
