﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET
{
   static class CDoubleExtension
   {
      //===================================================
      /// <summary>
      /// Converts coordinate from DMS to String
      /// </summary>
      /// <param name="value">DMS coordinate</param>
      /// <returns>Coordinate string</returns>
      public static string LongDMSToString(this double value)
      {
         string retVal = HelpFunction.DmsToString(value, EnumCoordLine.Lon);
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Converts coordinate from DEC to String
      /// </summary>
      /// <param name="value">DEC coordinate</param>
      /// <returns>Coordinate string</returns>
      public static string LongDECToString(this double value)
      {
         return value.DecToDms().LongDMSToString();
      }
      //===================================================
      /// <summary>
      /// Converts coordinate from DMS to String
      /// </summary>
      /// <param name="value">DMS coordinate</param>
      /// <returns>Coordinate string</returns>
      public static string LatDMSToString(this double value)
      {
         string retVal = HelpFunction.DmsToString(value, EnumCoordLine.Lat);
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Converts coordinate from DEC to String
      /// </summary>
      /// <param name="value">DEC coordinate</param>
      /// <returns>Coordinate string</returns>
      public static string LatDECToString(this double value)
      {
         return value.DecToDms().LatDMSToString();
      }
      //===================================================
      /// <summary>
      /// Converts coordinate from DEC to DMS
      /// </summary>
      /// <param name="value">DEC value</param>
      /// <returns>DMS value</returns>
      public static double DecToDms(this double value)
      {
         double retVal = IMPosition.Dec2Dms(value);
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Converts coordinate from DMS to DEC
      /// </summary>
      /// <param name="value">DMS value</param>
      /// <returns>DEC value</returns>
      public static double DmsToDec(this double value)
      {
         double retVal = IMPosition.Dms2Dec(value);
         return retVal;
      }
      //===================================================
      /// <summary>
      /// rounds double
      /// </summary>
      /// <param name="value">value</param>
      /// <param name="Digits">numbers of digits to round</param>
      /// <returns>rounded value</returns>
      public static double Round(this double value,int Digits)
      {
         return IM.RoundDeci(value, Digits);
      }
      //===================================================
      /// <summary>
      /// Converts a double to string
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>string</returns>
      public static string ToStringNullD(this double value)
      {
         return (value == ICSM.IM.NullD) ? "" : value.ToString();
      }

      public static double DivNullD(this double value, double divisor)
      {
          return (value == ICSM.IM.NullD) ? IM.NullD : value / divisor;
      }

      public static double MulNullD(this double value, double multiplicator)
      {
          return (value == ICSM.IM.NullD) ? IM.NullD : value * multiplicator;
      }

      
      //===================================================
      /// <summary>
      /// Converts a double to string
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>string</returns>
      public static string ToStringNullD(this double value, string nullValue)
      {
         return (value == ICSM.IM.NullD) ? nullValue : value.ToString();
      }
      //===================================================
      /// <summary>
      /// Checks for NULLD
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>true if the value == IM.NULLD</returns>
      public static bool IsNullD(this double value)
      {
         return (value == IM.NullD);
      }
      //===================================================
      /// <summary>
      /// Checks for NOT NULLD
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>true if the value != IM.NULLD</returns>
      public static bool IsNotNullD(this double value)
      {
         return !value.IsNullD();
      }
      //===================================================
      /// <summary>
      /// W -> dBW
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>dBW</returns>
      public static double W_To_dBW(this double value)
      {
          if (value == IM.NullD)
              return value;
          return (10.0 * Math.Log10(value)).Round(8);
      }
      //===================================================
      /// <summary>
      /// dBw -> W
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>W</returns>
      public static double dBw_To_W(this double value)
      {
          if (value == IM.NullD)
              return value;
          return Math.Pow(10.0, value / 10.0);
      }
      //===================================================
      /// <summary>
      /// dBm -> dBW
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>dBW</returns>
      public static double dBm2dBW(this double value)
      {
          if (value == IM.NullD)
              return value;
          return value - 30.0;
      }
      //===================================================
      /// <summary>
      /// dBm -> W
      /// </summary>
      /// <param name="value">a double value</param>
      /// <returns>W</returns>
      public static double dBm2W(this double value)
      {
          if (value == IM.NullD)
              return value;
          return value.dBm2dBW().dBw_To_W();
      }
      /// <summary>
      /// Compares the current double with another double.
      /// </summary>
      /// <returns>
      /// A 32-bit signed integer that indicates the relative order of the objects being compared.
      /// The return value has these meanings: Value Meaning Less than zero This instance is less than obj. Zero This instance is equal to obj. Greater than zero This instance is greater than obj. 
      /// </returns>
      public static int CompareWith(this double val1, double val2)
      {
          if (Math.Abs(val1 - val1) < 0.00000001)
              return 0;
          if (val1 < val2)
              return -1;
          return 1;
      }

   }
}
