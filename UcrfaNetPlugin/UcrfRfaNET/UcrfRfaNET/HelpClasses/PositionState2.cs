﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.UtilityClass;
using OrmCs;



namespace XICSM.UcrfRfaNET.HelpClasses
{

    public class Positions
    {

        private readonly Dictionary<int, PositionState2> _positionStates;
        private const int PositionAKey = 1;
        private const int PositionBKey = 2;

        public Positions(Dictionary<int, PositionState2> positionStates)
        {
            if (positionStates == null)
                throw new ArgumentNullException("positionStates");
            _positionStates = positionStates;
        }

        public bool HasPositionA
        {
            get
            {
                return _positionStates.ContainsKey(PositionAKey);
            }
        }

        public bool HasPositionB
        {
            get
            {
                return _positionStates.ContainsKey(PositionBKey);
            }
        }

        public PositionState2 PositionA
        {
            get
            {
                if (HasPositionA)
                {
                    return _positionStates[PositionAKey];
                }
                return null;
            }
        }

        public PositionState2 PositionB
        {
            get
            {
                if (HasPositionB)
                {
                    return _positionStates[PositionBKey];
                }
                return null;
            }
        }

        public string FullAddressPositionA
        {
            get
            {
                var positionA = PositionA;
                if(positionA != null)
                {
                    return positionA.FullAddressAuto;
                }
                return PositionState2.NoPosition;
            }
        }

        public string FullAddressPositionB
        {
            get
            {
                var positionB = PositionB;
                if (positionB != null)
                {
                    return positionB.FullAddressAuto;
                }
                return PositionState2.NoPosition;
            }
        }

    }

    public class PositionState2 : NotifyPropertyChanged
    {
        public double AdminSiteY { get; set; }
        public double AdminSiteX { get; set; }
        public string AdminSiteCsys { get; set; }
        public string AdminSiteAddress { get; set; }
        public static string NoPosition { get { return "<" + CLocaliz.TxT("no position") + ">"; } }
        
        public void GetAdminSiteInfo(int id)
        {
            if (id != IM.NullI)
            {
                PositionState2 pos = new PositionState2();
                pos.LoadStatePosition(id, ICSMTbl.SITES);
                AdminSiteId = pos.Id;
                AdminSiteCsys = pos.Csys;
                AdminSiteX = pos.X;
                AdminSiteY = pos.Y;
                AdminSiteAddress = pos.FullAddressAuto;
            }
            else
            {
                AdminSiteId = IM.NullI;
                AdminSiteCsys = "";
                AdminSiteX = IM.NullD;
                AdminSiteY = IM.NullD;
                AdminSiteAddress = "";
            }
        }

        public bool LonDiffersFromAdm { get { return (TableName != ICSMTbl.SITES) && (AdminSiteId == IM.NullI || (IM.RoundDeci(this.X, 5) != IM.RoundDeci(AdminSiteX, 5) || this.Csys != AdminSiteCsys)); } }
        public bool LatDiffersFromAdm { get { return (TableName != ICSMTbl.SITES) && (AdminSiteId == IM.NullI || (IM.RoundDeci(this.Y, 5) != IM.RoundDeci(AdminSiteY, 5) || this.Csys != AdminSiteCsys)); } }
        public bool AddrDiffersFromAdm { get { return (TableName != ICSMTbl.SITES) && (AdminSiteId == IM.NullI || this.FullAddressAuto != AdminSiteAddress); } }

        /// <summary>
        /// Проверка существования записи в БД
        /// </summary>
        /// <param name="id">ID записи</param>
        /// <param name="table">Название таблицы</param>
        /// <returns>TRUE - Запись есть в БД, иначе FALSE</returns>
        public static bool IsRecordExist(int id, string table)
        {
            bool retval = false;
            using(Icsm.LisRecordSet rs = new Icsm.LisRecordSet(table, IMRecordset.Mode.ReadOnly))
            {
                rs.Select("ID");
                rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                rs.Open();
                if (!rs.IsEOF())
                    retval = true;
            }
            return retval;
        }
        /// <summary>
        /// Формирует полный адрес
        /// </summary>
        /// <param name="koatyy">строка КОАТУУ</param>
        /// <param name="address">адрес</param>
        /// <returns></returns>
        public static string CreateFullAddress(string koatyy, string address,string tower,bool Status_)
        {
            string retVal = koatyy;
            if (string.IsNullOrEmpty(retVal))
                retVal = "";
            retVal += ((string.IsNullOrEmpty(address) || string.IsNullOrEmpty(retVal)) ? "" : ", ") + address;
            if (Status_)
            {
                if ((tower != "будівля") && (!string.IsNullOrEmpty(tower))) retVal += ", " + tower;
            }
            return retVal;
        }

        /// <summary>
        /// Возвращает короткий адрес
        /// </summary>
        /// <returns>короткий адрес</returns>
        public static string GenerateShortAddress(string streetType, string street, string houseNumber, string note)
        {
            System.Text.StringBuilder adr = new System.Text.StringBuilder("");
            if ((string.IsNullOrEmpty(streetType) == false) && (string.IsNullOrEmpty(street) == false))
                adr.Append(streetType + " " + street);
            if (string.IsNullOrEmpty(houseNumber) == false)
            {
                if (adr.Length > 0)
                    adr.Append(", ");
                adr.Append(houseNumber);
            }
            if (string.IsNullOrEmpty(note) == false)
            {
                if (adr.Length > 0)
                    adr.Append(", ");
                adr.Append(note);
            }
            return adr.ToString();
        }

        /// <summary>
        /// Возвращает короткий адрес
        /// </summary>
        /// <returns>короткий адрес</returns>
        public static string GenerateShortAddress(string streetType, string street, string houseNumber, string note,string tower)
        {
            System.Text.StringBuilder adr = new System.Text.StringBuilder("");
            adr.Append(GenerateShortAddress(streetType, street, houseNumber, note));
            if (!string.IsNullOrEmpty(tower))
            {
                if ((tower != "будівля") && (!string.IsNullOrEmpty(tower)))
                {
                    if (adr.Length > 0)
                        adr.Append(", ");
                    adr.Append(tower);
                }
            }
            return adr.ToString();
        }


        public const string AllUkr = "Вся Україна";

        private Component.SelectAddressControlFast2.City _cityKoatu = null;
        private bool is_changed = false;
        private int _id = IM.NullI;
        private string _tableName = "";
        private double _x;
        private double _y;
        private string _csys = "4DMS";
        private int _cityId;
        private string _countryId = "UKR";
        private double _asl = IM.NullD;
        private string _name = "";
        private string _province = "";
        private string _subprovince = "";
        private string _city = "";
        private string _address = "";
        private string _typeCity = "";
        private string _remark = "";
        private double _distBorder;

        // объявление новых переменных
        private string _tower = "";
        private int _cust_nbr1;
        private int _cust_nbr2;

        private string _data_owner = "";
        private string _data_number_building = "";

               



        public bool WithoutCity { get; set; }
        public string CitiesCode { get; protected set; }
        public string CitiesName { get; protected set; }
        public string CitiesProvince { get; protected set; }
        public string CitiesSubprovince { get; protected set; }

        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    InvokeNotifyPropertyChanged("Id");
                }
            }
        }

        /*
        private int CopyToAdmSite(IMObject pos)
        {
            IMRecordset r = new IMRecordset(ICSMTbl.SITES, IMRecordset.Mode.ReadWrite);
            r.Select("ID,CSYS,LONGITUDE,X,LATITUDE,Y,DATUM,CREATED_BY");
            r.Select("DATE_CREATED,CITY_ID,COUNTRY_ID,ASL,STATUS");
            r.Select("NAME,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK,CUST_TXT4");
            r.Select("CUST_TXT3,CUST_TXT8,CUST_TXT7");
            int id;
            try
            {
                r.Open();
                r.AddNew();
                id = IM.AllocID(ICSMTbl.SITES, 1, -1);
                r.Put("ID", id);
                r.Put("CSYS", "4DMS");

                r.Put("LONGITUDE", pos.GetD("X").DmsToDec());
                r.Put("X", pos.GetD("X"));
                r.Put("LATITUDE", pos.GetD("Y").DmsToDec());
                r.Put("Y", pos.GetD("Y"));
                r.Put("DATUM", 4);
                r.Put("CREATED_BY", IM.ConnectedUser());
                r.Put("DATE_CREATED", DateTime.Now);
                r.Put("CITY_ID",_cityId);// _addressCtrl.CurrentCity.Id);
                r.Put("COUNTRY_ID", "UKR");
                r.Put("ASL", 0);
                //-----------------------------
                //Созадем адрес
                string adr = AddressAuto;// GetShortAddress();
                //-----------------------------
                // Создаем полный адрес
                string fullAdr = FullAddressAuto;

                r.Put("NAME", fullAdr.Substring(0, (fullAdr.Length > 40) ? 40 : fullAdr.Length) + id);
                r.Put("ADDRESS", adr);
                r.Put("PROVINCE",Subprovince);// _addressCtrl.CurrentCity.prov);
                //r.Put("SUBPROVINCE", _addressCtrl.CurrentCity.subprov);
                r.Put("CITY",City);// _addressCtrl.CurrentCity.cityName);
                r.Put("CUST_TXT1", TypeCity);//_addressCtrl.CurrentCity.type);
                r.Put("REMARK", fullAdr);
                r.Put("CUST_TXT4", StreetType);//cbTypeStreet.Text);
                r.Put("CUST_TXT3", StreetName);//tbStreet.Text);
                r.Put("CUST_TXT8",_buildingNumber);// tbHouse.Text);
                r.Put("CUST_TXT7", Remark);// tbNote.Text);
                r.Put("STATUS", "NEED");
                r.Update();
            }
            finally
            {
                r.Final();
            }
            return id;
        }
        */

        /// <summary>
        /// Клонировать данные
        /// </summary>
        /// <param name="pos">Класс позиции, с которой скрпировать</param>
        private void ClonePositionFrom(PositionState2 pos)
        {
            if (pos == null)
                return;
            Id = pos.Id;
            TableName = pos.TableName;
            X = pos.X;
            Y = pos.Y;
            Csys = pos.Csys;
            CityId = pos.CityId;
            CountryId = pos.CountryId;
            Asl = pos.Asl;
            Name = pos.Name;
            Province = pos.Province;
            Subprovince = pos.Subprovince;
            City = pos.City;
            TypeCity = pos.TypeCity;
            Remark = pos.Remark;
            StreetName = pos.StreetName;
            StreetType = pos.StreetType;
            BuildingNumber = pos.BuildingNumber;
            AdminSiteId = pos.AdminSiteId;
            WithoutCity = pos.WithoutCity;
            Tower = pos.Tower;
            Cust_Nbr1 = pos.Cust_Nbr1;
            Cust_Nbr2 = pos.Cust_Nbr2;
            NAME_OWNER = pos.NAME_OWNER;
            NUMBER_BUILDING = pos.NUMBER_BUILDING;

                      
        }

        public string TableName
        {
            get { return _tableName; }
            set
            {
                if (_tableName != value)
                {
                    _tableName = value;
                    InvokeNotifyPropertyChanged("TableName");
                }
            }
        }

        private void SetLongitude(string format, double val)
        {
            IMPosition pos1 = new IMPosition(val, Y, format);
            X = IMPosition.Convert(pos1, Csys).Lon;           
        }
        private void SetLatitude(string format, double val)
        {
            IMPosition pos1 = new IMPosition(X, val, format);
            Y = IMPosition.Convert(pos1, Csys).Lat;            
        }
        private double GetLongitude(string format)
        {
            IMPosition pos1 = new IMPosition(X, Y, Csys);
            return IMPosition.Convert(pos1, format).Lon;
        }
        private double GetLatitude(string format)
        {
            IMPosition pos1 = new IMPosition(X, Y, Csys);
            return IMPosition.Convert(pos1, format).Lat;
        }

        public const string FieldLongDms = "LongDms";
        public double LongDms
        {
            get
            {
                return GetLongitude("4DMS");
            }
            set
            {
                SetLongitude("4DMS", value);
                InvokeNotifyPropertyChanged(FieldLongDms);
            }
        }

        public const string FieldLatDms = "LatDms";
        public double LatDms
        {
            get
            {
                return GetLatitude("4DMS");
            }
            set
            {
                SetLatitude("4DMS", value);
                InvokeNotifyPropertyChanged(FieldLatDms);
            }
        }

        public double LatDec
        {
            get { return GetLatitude("4DEC"); }
        }

        public double LonDec
        {
            get { return GetLongitude("4DEC"); }
        }

        public int CityId
        {
            get { return _cityId; }
            set
            {
                if (_cityId != value)
                {
                    _cityId = value;
                    InvokeNotifyPropertyChanged("CityId");
                    _cityKoatu = new Component.SelectAddressControlFast2.City(_cityId);
                }
            }
        }

        public string CountryId
        {
            get { return _countryId; }
            set
            {
                if (_countryId != value)
                {
                    _countryId = value;
                    InvokeNotifyPropertyChanged("CountryId");
                }
            }
        }

        public const string FieldFullAddressAuto = "FullAddressAuto";
        /// <summary>
        /// Полный адрес (генерируется автоматически)
        /// </summary>
        public string FullAddressAuto
        {
            get
            {
                return CreateFullAddress((_cityKoatu != null) ? _cityKoatu.Address : "", AddressAuto, Tower,false);
            }
        }
        
        /// <summary>
        /// Полный адрес по Таблице и Ид 
        /// </summary>
        public static string GetFullAddress(string tablename, int id)
        {
            PositionState2 pos = new PositionState2();
            pos.LoadStatePosition(id, tablename);
            return pos.FullAddressAuto;
        }

        // вствка дополнительных параметров
        // CUST_NBR1
        public int Cust_Nbr1
        {
            get { return _cust_nbr1; }
            set
            {
                if (_cust_nbr1 != value)
                {
                    _cust_nbr1 = value;
                    InvokeNotifyPropertyChanged("CUSTNBR1");
                }
            }
        }

        // вствка дополнительных параметров
        // CUST_NBR2
        public int Cust_Nbr2
        {
            get { return _cust_nbr2; }
            set
            {
                if (_cust_nbr2 != value)
                {
                    _cust_nbr2 = value;
                    InvokeNotifyPropertyChanged("CUSTNBR2");
                }
            }
        }


        // вствка дополнительных параметров
        // NAME
        public string NAME_OWNER
        {
            get { return _data_owner; }
            set
            {
                if (_data_owner != value)
                {
                    _data_owner = value;
                    InvokeNotifyPropertyChanged("NAME");
                }
            }
        }

        // вствка дополнительных параметров
        // NAME
        public string NUMBER_BUILDING
        {
            get { return _data_number_building; }
            set
            {
                if (_data_number_building != value)
                {
                    _data_number_building = value;
                    InvokeNotifyPropertyChanged("CUST_TXT6");
                }
            }
        }

        
        private int _datum;
        public int Datum
        {
            get { return _datum; }
            set
            {
                if (_datum != value)
                {
                    _datum = value;
                    InvokeNotifyPropertyChanged("Datum");
                }
            }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    InvokeNotifyPropertyChanged("Status");
                }
            }
        }

        // вставка дополнительных параметров
        
        public string Tower
        {
            get { return _tower; }
            set
            {
                if (_tower != value)
                {
                    _tower = value;
                    InvokeNotifyPropertyChanged("TOWER");
                }
            }
        }

        public const string FieldAsl = "Asl";
        /// <summary>
        /// Ввысота над уровнем моря
        /// </summary>
        //public double ASl
        public double Asl
        {
            get { return _asl; }
            set
            {
                if (_asl != value)
                {
                    _asl = value;
                    InvokeNotifyPropertyChanged(FieldAsl);
                }
            }
        }

        public double DistBorder
        {
            get { return _distBorder; }
            set
            {
                if (_distBorder != value)
                {
                    _distBorder = value;
                    InvokeNotifyPropertyChanged("DistBorder");
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    InvokeNotifyPropertyChanged("Name");
                }
            }
        }
        /// <summary>
        /// Адрес (Генерируется автоматически)
        /// </summary>
        public string AddressAuto
        {
            get { return (_cityKoatu != null && _cityKoatu.type == "район") ? Remark : GenerateShortAddress(StreetType, StreetName, BuildingNumber, Remark,Tower); }
        }

        public string Province
        {
            get { return _province; }
            set
            {
                if (_province != value)
                {
                    _province = value;
                    InvokeNotifyPropertyChanged("Province");
                }
            }
        }

        public string Subprovince
        {
            get { return _subprovince; }
            set
            {
                if (_subprovince != value)
                {
                    _subprovince = value;
                    InvokeNotifyPropertyChanged("Subprovince");
                }
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                if (_city != value)
                {
                    _city = value;
                    InvokeNotifyPropertyChanged("City");
                }
            }
        }

        public string TypeCity
        {
            get { return _typeCity; }
            set
            {
                if (_typeCity != value)
                {
                    _typeCity = value;
                    InvokeNotifyPropertyChanged("TypeCity");
                }
            }
        }

        string _foa;
        public string FullOldAddress { get { return _foa; } set { if (_foa != value) { _foa = value; IsChanged = true; } } }

        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    InvokeNotifyPropertyChanged("Remark");
                }
            }
        }

        public double X
        {
            get { return _x; }
            set
            {
                if (_x != value)
                {
                    _x = value;
                    InvokeNotifyPropertyChanged("X");
                    //we have following, so have to take care
                    InvokeNotifyPropertyChanged("LongDms");
                }
            }
        }

        public double Y
        {
            get { return _y; }
            set
            {
                if (_y != value)
                {
                    _y = value;
                    InvokeNotifyPropertyChanged("Y");
                    //we have following, so have to take care
                    InvokeNotifyPropertyChanged("LatDms");
                }
            }
        }

        public string Csys
        {
            get { return _csys; }
            set
            {
                if (_csys != value)
                {
                    _csys = value;
                    InvokeNotifyPropertyChanged("Csys");
                }
            }
        }

        private int _adminSiteId;
        public int AdminSiteId
        {
            get { return _adminSiteId; }
            set
            {
                if (_adminSiteId != value)
                {
                    _adminSiteId = value;
                    InvokeNotifyPropertyChanged("AdminSitesId");
                }
            }
        }

    
        private string _streetType;
        public string StreetType
        {
            get { return _streetType; }
            set
            {
                if (_streetType != value)
                {
                    _streetType = value;
                    InvokeNotifyPropertyChanged("StreetType");
                }
            }
        }

        private string _streetName;
        public string StreetName
        {
            get { return _streetName; }
            set
            {
                if (_streetName != value)
                {
                    _streetName = value;
                    InvokeNotifyPropertyChanged("StreetName");
                }
            }
        }

        private string _buildingNumber;
        public string BuildingNumber
        {
            get { return _buildingNumber; }
            set
            {
                if (_buildingNumber != value)
                {
                    _buildingNumber = value;
                    InvokeNotifyPropertyChanged("BuildingNumber");
                }
            }
        }

        public string CreatedBy { get; protected set; }
        public DateTime DateCreated { get; protected set; }
        public string ModifiedBy { get; protected set; }
        public DateTime DateModified { get; protected set; }

        //=================================================

        /// <summary>
        /// конструктор
        /// </summary>
        public PositionState2()
        {
            AdminSiteId = IM.NullI;
            DateCreated = IM.NullT;
            DateModified = IM.NullT;
        }
        /// <summary>
        /// Заполняем обьект данными
        /// </summary>
        public void LoadStatePosition(PositionState2 pos)
        {
            ClonePositionFrom(pos);
        }

        /// <summary>
        /// извлекаем информацию о Высоте вышки и макс.высоте из таблицы XNRFA_LINKS_SITES
        /// </summary>
        public void GetParamsLinksSites(int EXT_ID_SITES)
        {

            using (IMRecordset r1 = new IMRecordset(PlugTbl.LinkSites, IMRecordset.Mode.ReadOnly))
            {
                r1.Select("ID,EXT_ID_SITES,CUST_NBR1,CUST_NBR2,CUST_NBR3,CUST_NBR4,CUST_NBR5,CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5");
                r1.SetWhere("EXT_ID_SITES", IMRecordset.Operation.Eq, EXT_ID_SITES);
                r1.Open();
                if (!r1.IsEOF())
                {
                    if (r1.GetI("CUST_NBR1").ToString().Trim().Length < 9)
                    {
                        Cust_Nbr1 = r1.GetI("CUST_NBR1");
                    }
                    else
                    {
                        Cust_Nbr1 = 0;
                    }

                    if (r1.GetI("CUST_NBR2").ToString().Trim().Length < 9)
                    {
                        Cust_Nbr2 = r1.GetI("CUST_NBR2");
                    }
                    else
                    {
                        Cust_Nbr2 = 0;
                    }


                }
            }



        }
        /// <summary>
        /// вставляем информацию о Высоте вышки и макс.высоте в таблицу XNRFA_LINKS_SITES
        /// </summary>
        public void SetParamsLinksSites(int EXT_ID_SITES)
        {

           using (Icsm.LisTransaction trx = new Icsm.LisTransaction())
            {
                using (Icsm.LisRecordSet r1 = new Icsm.LisRecordSet(PlugTbl.LinkSites, IMRecordset.Mode.ReadWrite))
                {

                    r1.Select("ID,EXT_ID_SITES,CUST_NBR1,CUST_NBR2,CUST_NBR3,CUST_NBR4,CUST_NBR5,CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5");
                    r1.SetWhere("EXT_ID_SITES", IMRecordset.Operation.Eq, EXT_ID_SITES);

                    r1.Open();

                    if (r1.IsEOF()) // , ...
                    {
                        Id = IM.AllocID(PlugTbl.LinkSites, 1, -1);
                        r1.AddNew();
                        r1.Put("ID", Id);
                        r1.Put("EXT_ID_SITES", EXT_ID_SITES);
                        r1.Put("CUST_NBR1", Cust_Nbr1);
                        r1.Put("CUST_NBR2", Cust_Nbr2);

                    }


                    if (!r1.IsEOF())
                    {
                        r1.Edit();
                        r1.Put("EXT_ID_SITES", EXT_ID_SITES);
                        r1.Put("CUST_NBR1", Cust_Nbr1);
                        r1.Put("CUST_NBR2", Cust_Nbr2);
                    }
                    r1.Update();
                }
                trx.Commit();
            }




        }



        /// <summary>
        /// Заполняем обьект данными
        /// </summary>
        public virtual void LoadStatePosition(int id, string table)
        {
                TableName = table;
                bool isAdmSites = (table == ICSMTbl.SITES);
                using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(table, IMRecordset.Mode.ReadOnly))
                {
                    r.Select("ID,ASL,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID");
                    r.Select("NAME,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK");
                    r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7");
                    r.Select("CREATED_BY,DATE_CREATED,MODIFIED_BY,DATE_MODIFIED");
                    r.Select("City.CUST_CHB5");


                       
                    

                    if ((table == ICSMTbl.SITES) || (table == ICSMTbl.Site) || (table == ICSMTbl.itblPositionEs) || (table == ICSMTbl.itblPositionBro)|| (table == ICSMTbl.itblPositionFmn)|| (table == ICSMTbl.itblPositionHf) || (table == ICSMTbl.itblPositionMob2)|| (table == ICSMTbl.itblPositionMw)|| (table == ICSMTbl.itblPositionWim) || (table == PlugTbl.XfaPosition))
                    {
                        r.Select("TOWER");
                        if (table != PlugTbl.XfaPosition)
                        {
                            r.Select("CUST_TXT6");
                        }
                    }
                    if ((table == ICSMTbl.lnkSites) || (table == ICSMTbl.Site) || (table == ICSMTbl.itblPositionEs) || (table == ICSMTbl.itblPositionBro) || (table == ICSMTbl.itblPositionFmn) || (table == ICSMTbl.itblPositionHf) || (table == ICSMTbl.itblPositionMob2) || (table == ICSMTbl.itblPositionMw) || (table == ICSMTbl.itblPositionWim))
                    {
                        r.Select("CUST_NBR1,CUST_NBR2");
                    }

                      


                    if (!isAdmSites)
                        r.Select("SUBPROVINCE,TABLE_NAME,ADMS_ID");
                    else
                        r.Select("DATUM,COUNTRY_ID,STATUS");

                   
                    if (isAdmSites) { GetParamsLinksSites(id); }
                   
                    r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    r.Open();
                    if (!r.IsEOF())
                    {

                        if ((table == ICSMTbl.SITES) || (table == ICSMTbl.Site) || (table == ICSMTbl.itblPositionEs) || (table == ICSMTbl.itblPositionBro) || (table == ICSMTbl.itblPositionFmn) || (table == ICSMTbl.itblPositionHf) || (table == ICSMTbl.itblPositionMob2) || (table == ICSMTbl.itblPositionMw) || (table == ICSMTbl.itblPositionWim) || (table == PlugTbl.XfaPosition))
                        {
                            Tower = r.GetS("TOWER");
                            if (table != PlugTbl.XfaPosition)
                            {
                                NAME_OWNER = r.GetS("NAME");
                                NUMBER_BUILDING = r.GetS("CUST_TXT6");
                            }
                        }






                        if ((table == ICSMTbl.lnkSites) || (table == ICSMTbl.Site) || (table == ICSMTbl.itblPositionEs) || (table == ICSMTbl.itblPositionBro) || (table == ICSMTbl.itblPositionFmn) || (table == ICSMTbl.itblPositionHf) || (table == ICSMTbl.itblPositionMob2) || (table == ICSMTbl.itblPositionMw) || (table == ICSMTbl.itblPositionWim) )
                        
                        {

                            if (r.GetI("CUST_NBR1").ToString().Trim().Length < 9)
                            {
                                Cust_Nbr1 = r.GetI("CUST_NBR1");
                            }
                            else
                            {
                                Cust_Nbr1 = 0;
                            }

                            if (r.GetI("CUST_NBR2").ToString().Trim().Length < 9)
                            {
                                Cust_Nbr2 = r.GetI("CUST_NBR2");
                            }
                            else
                            {
                                Cust_Nbr2 = 0;
                            }
                        }

                        if (!isAdmSites)
                        {
                            Subprovince = r.GetS("SUBPROVINCE");
                            AdminSiteId = r.GetI("ADMS_ID");

                        }
                        else
                        {
                            CountryId = r.GetS("COUNTRY_ID");
                            Status = r.GetS("STATUS");
                            Datum = r.GetI("DATUM");
                        }

                        Id = r.GetI("ID");
                        Csys = r.GetS("CSYS");
                        CityId = r.GetI("CITY_ID");
                        CountryId = "UKR";
                        Asl = r.GetD("ASL");
                        X = r.GetD("X");
                        Y = r.GetD("Y");
                        Name = r.GetS("NAME");
                        Province = r.GetS("PROVINCE");
                        City = r.GetS("CITY");
                        _address = r.GetS("ADDRESS");
                        TypeCity = r.GetS("CUST_TXT1");
                        FullOldAddress = r.GetS("REMARK");
                        Remark = r.GetS("CUST_TXT7");
                        StreetType = r.GetS("CUST_TXT4");
                        StreetName = r.GetS("CUST_TXT3");
                        BuildingNumber = r.GetS("CUST_TXT8");
                        WithoutCity = (r.GetI("City.CUST_CHB5") == 1);
                        
                        CreatedBy = r.GetS("CREATED_BY");
                        DateCreated = r.GetT("DATE_CREATED");
                        ModifiedBy = r.GetS("MODIFIED_BY");
                        DateModified = r.GetT("DATE_MODIFIED");

                       
                    }
                    else
                        MessageBox.Show(CLocaliz.TxT("No such site in database (perhabs, it is not saved after copying)") + string.Format(" ID: {0}, TABLE: {1}", id, table), CLocaliz.TxT("Warning"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                DistBorder = IM.NullI;
                IsChanged = false;
        }
        /// <summary>
        /// Сохранение фотографий на диске и в БД их путей c помощью станции
        /// </summary>
        /// <param name="filesDst">список входящих путей</param>
        /// <param name="filesSrc">список исходящих путей</param>
        //?? Проверить
        [Obsolete("Проверить")]
        public void SaveFoto(List<string> filesDst, List<string> filesSrc)
        {
            AttachmentControl ac = new AttachmentControl();
            ac.AttachFiles(filesDst, filesSrc, TableName, Id);
        }

        /// <summary>
        /// Сохранение фотографий на диске и в БД их путей c помощью станции
        /// </summary>
        /// <param name="filesDst">список входящих путей</param>
        /// <param name="filesSrc">список исходящих путей</param>
        /// <param name="tableName"></param>
        /// <param name="id"></param>
        //?? Проверить
        [Obsolete("Проверить")]
        public void SaveFoto(List<string> filesDst, List<string> filesSrc, string tableName, int id)
        {
            AttachmentControl ac = new AttachmentControl();
            ac.AttachFiles(filesDst, filesSrc, tableName, id);            
        }



        /// <summary>
        /// Проверка на наличие дубликатов номеров сооружений в базе (если запись уже есть  возвращает false)
        /// <params> NUM_BUILDING (Номер сооружения)
        /// <params> TABLE_NAME (Название таблицы)
        /// <params> status_include (признак исключения из рассмотрения текущей открытой для редактирования записи)
        /// </summary>
        public bool GetStatus_Dublicate_NUM(string TABLE_NAME,string NUM_BUILDING , int ID, bool status_include)
        {
            bool res = true;
            IMRecordset rx = new IMRecordset(TABLE_NAME, IMRecordset.Mode.ReadOnly);
            try
            {
                rx.Select("ID,NAME");
                rx.SetWhere("NAME", IMRecordset.Operation.Eq, NUM_BUILDING.ToString().TrimEnd().TrimStart());
                if (status_include) { rx.SetWhere("ID", IMRecordset.Operation.Neq, Id); }
                rx.Open();


                if (rx.IsEOF())
                {
                    res = true;
                }
                else
                {
                    res = false;
                }
                

            }
            finally
            {
                rx.Final();
            }

            return res;
        }



        /// <summary>
        /// Cохраняем в Базу
        /// </summary>
        public void Save(int Status_Nic)
        {
            if (!is_changed)
            {
                if (string.IsNullOrEmpty(TableName))
                    return;
                using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                {
                    using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(TableName, IMRecordset.Mode.ReadWrite))
                    {
                        if ((TableName == ICSMTbl.lnkSites) || (TableName == ICSMTbl.Site) || (TableName == ICSMTbl.itblPositionEs) || (TableName == ICSMTbl.itblPositionBro) || (TableName == ICSMTbl.itblPositionFmn) || (TableName == ICSMTbl.itblPositionHf) || (TableName == ICSMTbl.itblPositionMob2) || (TableName == ICSMTbl.itblPositionMw) || (TableName == ICSMTbl.itblPositionWim))
                        {
                            r.Select("CUST_NBR1,CUST_NBR2");
                            r.Select("TABLE_NAME,SUBPROVINCE,ADMS_ID,CODE");
                        }

                        if ((TableName == ICSMTbl.Site) || (TableName == ICSMTbl.SITES) || (TableName == ICSMTbl.itblPositionEs) || (TableName == ICSMTbl.itblPositionBro) || (TableName == ICSMTbl.itblPositionFmn) || (TableName == ICSMTbl.itblPositionHf) || (TableName == ICSMTbl.itblPositionMob2) || (TableName == ICSMTbl.itblPositionMw) || (TableName == ICSMTbl.itblPositionWim) || (TableName == PlugTbl.XfaPosition))
                        {
                            r.Select("DIST_BORDER");
                        }
                        if (TableName == PlugTbl.XfaPosition)
                        {
                            r.Select("TABLE_NAME,SUBPROVINCE,ADMS_ID,CODE");
                        }



                        r.Select("ID,ASL,LONGITUDE,COUNTRY_ID,X,Y,CSYS,LATITUDE");
                        r.Select("CITY_ID,NAME,ASL,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK");
                        r.Select("DATE_CREATED,CREATED_BY");
                        r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7");
                        r.Select("CREATED_BY,DATE_CREATED,MODIFIED_BY,DATE_MODIFIED");

                        if (TableName == PlugTbl.XfaPosition)
                        {
                            r.Select("ADM_COD");
                        }

                        if (TableName == ICSMTbl.SITES) r.Select("STATUS");
                        if ((TableName == ICSMTbl.SITES) || (TableName == ICSMTbl.Site) || (TableName == ICSMTbl.itblPositionEs) || (TableName == ICSMTbl.itblPositionBro) || (TableName == ICSMTbl.itblPositionFmn) || (TableName == ICSMTbl.itblPositionHf) || (TableName == ICSMTbl.itblPositionMob2) || (TableName == ICSMTbl.itblPositionMw) || (TableName == ICSMTbl.itblPositionWim) || (TableName == PlugTbl.XfaPosition))
                        {
                            r.Select("TOWER");
                            if (TableName != PlugTbl.XfaPosition)
                            {
                                r.Select("CUST_TXT6");
                            }

                            if ((TableName != PlugTbl.XfaPosition) && (TableName != ICSMTbl.SITES))
                            {
                                r.Select("ADM_CODE");
                            }

                        }

                        r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                        r.Open();

                        if (r.IsEOF()) // , ...
                        {


                            if (GetStatus_Dublicate_NUM(TableName, NAME_OWNER, Id, false) == false)
                            {

                                if (MessageBox.Show(CLocaliz.TxT("This number of buildings already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    return;
                                }


                            }



                            r.AddNew();
                            Id = IM.AllocID(TableName, 1, -1);
                            CreatedBy = IM.ConnectedUser();
                            DateCreated = DateTime.Now;
                        }
                        else
                        {

                            if (GetStatus_Dublicate_NUM(TableName, NAME_OWNER, Id, true) == false)
                            {

                                if (MessageBox.Show(CLocaliz.TxT("This number of buildings already exists in the database! Insert a record into the database (Yes/No)"), "", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    return;
                                }


                            }

                            r.Edit();
                            ModifiedBy = IM.ConnectedUser();
                            DateModified = DateTime.Now;
                        }
                        r.Put("ID", Id);
                        r.Put("CREATED_BY", CreatedBy);
                        r.Put("DATE_CREATED", DateCreated);
                        r.Put("MODIFIED_BY", ModifiedBy);
                        r.Put("DATE_MODIFIED", DateModified);

                        if (TableName != ICSMTbl.SITES)
                        {
                            r.Put("TABLE_NAME", TableName);
                            r.Put("SUBPROVINCE", Subprovince);
                            r.Put("ADMS_ID", AdminSiteId);
                            string code = r.GetS("CODE");
                            if (string.IsNullOrEmpty(code))
                                r.Put("CODE", Id.ToString());
                        }
                        //Maybe converted;
                        r.Put("LONGITUDE", LonDec);
                        r.Put("LATITUDE", LatDec);
                        r.Put("ASL", Asl);
                        r.Put("CITY_ID", (CityId == 0) ? IM.NullI : CityId);
                        r.Put("COUNTRY_ID", CountryId);
                        //string name = r.GetS("NAME");
                        //if (string.IsNullOrEmpty(name))
                        r.Put("NAME", LatinPlugin.Transliteration.Translite(City, false) + " " + Id);
                        r.Put("ADDRESS", AddressAuto);
                        r.Put("PROVINCE", Province);
                        r.Put("CITY", City);
                        r.Put("CUST_TXT1", TypeCity);
                        r.Put("REMARK", string.IsNullOrEmpty(FullOldAddress) ? FullAddressAuto : FullOldAddress); // Запишем, если пусто.
                        r.Put("X", X);
                        r.Put("Y", Y);
                        r.Put("CSYS", Csys);
                        r.Put("CUST_TXT3", StreetName);
                        r.Put("CUST_TXT4", StreetType);
                        r.Put("CUST_TXT7", Remark);
                        r.Put("CUST_TXT8", BuildingNumber);
                        if (TableName != ICSMTbl.lnkSites)
                        {
                          if (DistBorder!=IM.NullI) r.Put("DIST_BORDER", DistBorder);
                        }


                        if (TableName == PlugTbl.XfaPosition)
                        {
                            

                            ///
                            IMObject rsCity = null;
                            try
                            {
                                rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, CityId);
                            }
                            catch
                            {
                                
                                rsCity = null;
                            }
                            if (rsCity != null)
                            {
                                r.Put("ADM_COD", rsCity.GetS("ADM_CODE"));
                            }
                            //



                            r.Put("TOWER", Tower);
                            r.Update();
                        }


                        if ((TableName == ICSMTbl.Site) || (TableName == ICSMTbl.itblPositionEs) || (TableName == ICSMTbl.itblPositionBro) || (TableName == ICSMTbl.itblPositionFmn) || (TableName == ICSMTbl.itblPositionHf) || (TableName == ICSMTbl.itblPositionMob2) || (TableName == ICSMTbl.itblPositionMw) || (TableName == ICSMTbl.itblPositionWim))
                        {
                            //r.Put("ADM_CODE", r.GetS("City.ADM_CODE"));
                            ///
                            IMObject rsCity = null;
                            try
                            {
                                rsCity = IMObject.LoadFromDB(ICSMTbl.itblCities, CityId);
                            }
                            catch
                            {

                                rsCity = null;
                            }
                            if (rsCity != null)
                            {
                                r.Put("ADM_CODE", rsCity.GetS("ADM_CODE"));
                            }
                            //



                            r.Put("TOWER", Tower);
                            if (!string.IsNullOrEmpty(NAME_OWNER)) { r.Put("NAME", NAME_OWNER); } else { if (!string.IsNullOrEmpty(City)) { r.Put("NAME", LatinPlugin.Transliteration.Translite(City, false) + " " + Id); NAME_OWNER = LatinPlugin.Transliteration.Translite(City, false) + " " + Id; } else { r.Put("NAME", " "); } }
                            if (!string.IsNullOrEmpty(NUMBER_BUILDING)) { r.Put("CUST_TXT6", NUMBER_BUILDING); } else { r.Put("CUST_TXT6", " "); }

                            r.Put("CUST_NBR1", Cust_Nbr1);
                            r.Put("CUST_NBR2", Cust_Nbr2);
                            r.Update();

                        }
                        if ((TableName == ICSMTbl.SITES))
                        {
                            r.Put("TOWER", Tower);
                            if (!string.IsNullOrEmpty(NAME_OWNER)) { r.Put("NAME", NAME_OWNER); } else { if (!string.IsNullOrEmpty(City)) { r.Put("NAME", LatinPlugin.Transliteration.Translite(City, false) + " " + Id); NAME_OWNER = LatinPlugin.Transliteration.Translite(City, false) + " " + Id; } else { r.Put("NAME", " "); } }
                            if (!string.IsNullOrEmpty(NUMBER_BUILDING)) { r.Put("CUST_TXT6", NUMBER_BUILDING); } else { r.Put("CUST_TXT6", " "); }
                            if (!string.IsNullOrEmpty(Status)) { r.Put("STATUS", Status); }
                            r.Update();
                            SetParamsLinksSites(Id);


                        }
                        if ((TableName != ICSMTbl.Site) && (TableName != ICSMTbl.SITES) && (TableName != ICSMTbl.itblPositionEs) && (TableName != ICSMTbl.itblPositionBro) && (TableName != ICSMTbl.itblPositionFmn) && (TableName != ICSMTbl.itblPositionHf) && (TableName != ICSMTbl.itblPositionMob2) && (TableName != ICSMTbl.itblPositionMw) && (TableName != ICSMTbl.itblPositionWim) && (TableName != PlugTbl.XfaPosition))
                        {
                            r.Update();
                        }

                    }
                    tr.Commit();
                }
                using (YXfaSiteNic nic_s = new YXfaSiteNic()) {
                    nic_s.Format("*");
                    if (!nic_s.Fetch(string.Format("([OBJ_ID]={0}) AND ([OBJ_TABLE]={1})", Id, TableName.ToSql()))) {
                        nic_s.New();
                        nic_s.AllocID();
                        nic_s.m_obj_id = Id;
                        nic_s.m_obj_table = TableName;
                        nic_s.m_nic = Status_Nic;
                        nic_s.Save();
                    }
                    else{
                        nic_s.m_nic = Status_Nic;
                        nic_s.Save();
                    }
                }
                is_changed = true;
            }   
        }
        /// <summary>
        /// Создает админ сайт, если необходимо
        /// </summary>
        /// <returns>TRUE - админ сайт создан, иначе FALSE</returns>
        public bool CreateAdminSite()
        {
            if ((this.AdminSiteId == IM.NullI) && (this.TableName != ICSMTbl.SITES))
            {
                PositionState2 admPos = new PositionState2();
                admPos.LoadStatePosition(this);
                admPos.CopyToTable(ICSMTbl.SITES);
                admPos.Save(0);
                this.AdminSiteId = admPos.Id;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Переопределяет позицию на другую таблицу (Требует вызова функции SaveToDB)
        /// </summary>
        /// <param name="table">Название таблицы, на поторую переопределяется</param>
        public void CopyToTable(string table)
        {
            if(this.TableName != table)
            {
                this.Id = IM.NullI;
                this.TableName = table;
            }
        }

        /// <summary>
        /// Заповнює властивості нас.пункту з довідника КОАТУУ
        /// </summary>
        /// <param name="id">id нас.пункту</param>
        public void LoadCitiesId(int id)
        {
            CitiesName = "";
            CitiesCode = "";
            CitiesProvince = "";
            CitiesSubprovince = "";
            using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly))
            {
                r.Select("ID,CODE,NAME,PROVINCE,SUBPROVINCE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                r.Open();
                if (!r.IsEOF())
                {
                    CitiesName = r.GetS("NAME");
                    CitiesCode = r.GetS("CODE");
                    CitiesProvince = r.GetS("PROVINCE");
                    CitiesSubprovince = r.GetS("SUBPROVINCE");
                }
            }
        }
    }
}
