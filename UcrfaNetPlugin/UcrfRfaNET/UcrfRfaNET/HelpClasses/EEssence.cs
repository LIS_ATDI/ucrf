﻿namespace XICSM.UcrfRfaNET
{
   //==============================================
   /// <summary>
   /// Список сущностей для помежуточной БД
   /// </summary>
   class EEssence
   {
      public static string APPL_PAY_ICSM = "APPL_PAY_ICSM";    //Заявка на выставление счета из ICSM
      public static string STATE_APPL_PAY_PARUS = "STATE_APPL_PAY_PARUS";  // Ответ Паруса по заявке
      public static string EMPLOYEE_PARUS = "EMPLOYEE_PARUS";  // Контрагент паруса
      public static string StationIcsm = "STATION_ICSM";  // Станции ICSM
      public static string PriceIcsm = "PRICE_ICSM";    // Прайсы ICSM
      public static string TestEssence = "ICSM_TEST_ESS";    // Тестовая сущность
   }
}
