﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   static class CArticleUpdate
   {
      public static ArticlesManager artManager = new ArticlesManager();

      public static int UpdateArticles(bool isAuto, string tableNameExternal)
      {
          int retVal = 0;


          using (LisProgressBar pb = new LisProgressBar("Net updating"))
          {
              int count = 0;
              IM.ExecuteScalar(ref count, "SELECT COUNT(1) FROM %XFA_NET WHERE ((PRICE_ID is null) OR (((OBJ_ID2 is not null) AND (PRICE_ID2 is null)) AND (OBJ_TABLE='XFA_ABONENT_STATION') )) and (AUTO_SET_ARTICLE = 1) and (IS_CONFIRMED = 0)");
              pb.SetProgress(0, count);
              pb.SetBig("Net updating");
              pb.SetSmall(0, count);
              using (Icsm.LisRecordSet rsNet = new Icsm.LisRecordSet(PlugTbl.XfaNet, IMRecordset.Mode.ReadOnly))
              {
                  rsNet.Select("ID");
                  rsNet.SetWhere("PRICE_ID", IMRecordset.Operation.Null, IM.NullI);
                  rsNet.SetWhere("AUTO_SET_ARTICLE", IMRecordset.Operation.Eq, 1);
                  rsNet.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
                  //rsNet.SetWhere("ID", IMRecordset.Operation.Eq, 117214);
                  for (rsNet.Open(); !rsNet.IsEOF(); rsNet.MoveNext())
                  {
                      int idNet = rsNet.GetI("ID");
                      Net.NetObject net = new Net.NetObject();
                      net.Load(idNet);
                      int idArticle = net.ArticleIdSuggest;
                      if (idArticle != IM.NullI)
                      {
                          net.ArticleId = idArticle;
                          net.Save();
                          retVal++;
                      }
                      pb.Increment(true);
                      if (pb.UserCanceled())
                          break;
                  }
              }
          }


          string AllExcludeField = " [ID] not in ({0}) ";

          using (LisProgressBar pb2 = new LisProgressBar("Articles updating"))
          {
              string Excludefields = "";
              int maxCount = 50000;
              int count = 0;
              int countSetted = 0;
              pb2.SetBig("Artiles updating");

              IM.ExecuteScalar(ref maxCount, "SELECT COUNT(1) FROM %XNRFA_APPL WHERE ((PRICE_ID is null) OR (((OBJ_ID2 is not null) AND (PRICE_ID2 is null)) AND (OBJ_TABLE='XFA_ABONENT_STATION') )) and (WAS_USED = 1) and (IS_CONFIRMED = 0)");
              IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
              rsAppl.Select("ID,OBJ_TABLE");
              //rsAppl.SetWhere("PRICE_ID", IMRecordset.Operation.Null, IM.NullI);
              rsAppl.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
              rsAppl.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
              rsAppl.SetAdditional(" ((PRICE_ID is null) OR (((OBJ_ID2 is not null) AND (PRICE_ID2 is null)) AND (OBJ_TABLE='XFA_ABONENT_STATION'))) ");
              //rsAppl.SetAdditional("[ID] in ('10087712','450978160','451013628','451013651','451013655','451013658','451014658','451801029')");



              try
              {
                  for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                  {
                      if (pb2.UserCanceled())
                          break;
                      string tableName = rsAppl.GetS("OBJ_TABLE");
                      int id = rsAppl.GetI("ID");
                      pb2.SetBig(string.Format("{0}:{1}", tableName, id));
                      pb2.SetSmall(count++, maxCount);
                      pb2.SetProgress(count, maxCount);
                      int[] StationIds = ArticleLib.ArticleUpdate.GetArticle(id);
                      if (ArticleLib.ArticleUpdate.UpdateArticle(id, StationIds, tableName))
                      {
                          Excludefields += string.Format("'{0}',", rsAppl.GetI("ID"));
                          countSetted++;
                          retVal++;
                      }
                  }
                  if (Excludefields.Length > 0)
                  {
                      Excludefields = Excludefields.Remove(Excludefields.Length - 1, 1);
                      AllExcludeField = string.Format(AllExcludeField, Excludefields);
                  }
              }
              finally
              {
                  rsAppl.Final();
              }
          }
        

          string[] updatedTable = new string[]
                                      {
                                          ICSMTbl.itblGSM,
                                          PlugTbl.XfaAbonentStation,
                                          PlugTbl.XfaEmitterStation,
                                          ICSMTbl.itblEarthStation,
                                          ICSMTbl.itblShip,
                                      };
          artManager.InitArticles();

          string usingTable = "";
          IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
          r.Select("ID,STANDARD,PRICE_ID,PRICE_ID2,Price.ARTICLE,OBJ_TABLE");
          //r.SetWhere("PRICE_ID", IMRecordset.Operation.Null, IM.NullI);
          r.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
          r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
          r.SetAdditional(" ((PRICE_ID is null) OR (((OBJ_ID2 is not null) AND (PRICE_ID2 is null)) AND (OBJ_TABLE='XFA_ABONENT_STATION'))) ");
          //if (AllExcludeField.Length>0) r.SetAdditional(AllExcludeField);
          //r.SetAdditional("[ID] in ('10087712','450978160','451013628','451013651','451013655','451013658','451014658','451801029')");
          //r.SetWhere("ID", IMRecordset.Operation.Eq, 451614098);
          //r.SetAdditional("[ID] in ('451013658')");

          string nbQry = "SELECT COUNT(1) FROM %XNRFA_APPL WHERE ((PRICE_ID is null) OR (((OBJ_ID2 is not null) AND (PRICE_ID2 is null)) AND (OBJ_TABLE='XFA_ABONENT_STATION'))) and (WAS_USED = 1) and (IS_CONFIRMED = 0)";

          if (string.IsNullOrEmpty(ApplSetting.TableForSetArticle) == false)
          {
              //r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ApplSetting.TableForSetArticle);
              usingTable = ApplSetting.TableForSetArticle;
              nbQry += string.Format(" and (OBJ_TABLE like '{0}')", usingTable);
          }
          else if (string.IsNullOrEmpty(tableNameExternal) == false)
          {
              r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, tableNameExternal);
              usingTable = tableNameExternal;
              nbQry += string.Format(" and (OBJ_TABLE like '{0}')", usingTable);
          }
          else
          {
              foreach (string table in updatedTable)
              {
                  r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Neq, table);
                  nbQry += string.Format(" and (OBJ_TABLE <> '{0}')", table);
              }
          }

          using (LisProgressBar pb = new LisProgressBar("Articles updating" + usingTable))
          {
              pb.SetBig("Artiles updating");
              try
              {
                  int recCount = 0;
                  int updtdRecCnt = 0;

                  int maxCount = 0;
                  IM.ExecuteScalar(ref maxCount, nbQry);

                  bool abReZ = true;
                  if (!isAuto)
                      if (MessageBox.Show("Проставляти статті для абонентських РЕЗ?", "Налаштування", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                          abReZ = false;

                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                      if (pb.UserCanceled())
                          continue;
                      pb.SetSmall(++recCount, maxCount);
                      int appId = r.GetI("ID");
                      try
                      {
                          using (BaseAppClass app = BaseAppClass.GetBaseAppl(appId))
                          {
                              if (app != null)
                              {
                                  if ((app.radioTech == CRadioTech.YKX) || (app.radioTech == CRadioTech.ЦУКХ))
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                                          double value = IM.NullD;
                                          LimitType type = mobApp.GetAppTypeAndLimitValue(out value);
                                          if (type == LimitType.noLimit)
                                              app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          else
                                          {
                                              if (!abReZ && type == LimitType.countLimit)
                                                  continue;
                                              else
                                                  app.SaveArticle(artManager.GetArticle(app.radioTech, type, value));
                                          }
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.RBSS)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                                          string cl = mobApp.GetClass();
                                          if (cl == "FC")
                                              app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.coastStaLimit, IM.NullD));
                                          else if (cl == "MS")
                                          {
                                              app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.seaStaLimit, IM.NullD));
                                          }
                                          updtdRecCnt++;
                                      }
                                      else if (app != null && app.appType == AppType.AppAR_3)
                                      {
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.coastStaLimit, IM.NullD));
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.KX)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          MobStaTechnologicalApp mobApp = (MobStaTechnologicalApp)app;
                                          double pow = mobApp.GetPower();
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.PADG)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.TETRA || app.radioTech == CRadioTech.TRUNK)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }

                                  else if (app.radioTech == CRadioTech.GSM_900 || app.radioTech == CRadioTech.GSM_1800)
                                  {
                                      if (app != null && app.appType == AppType.AppRR)
                                      {
                                          MobStaApp mobApp = (MobStaApp)app;
                                          LimitType type = mobApp.GetAppTypeAndLimitValueGSM();
                                          if (type != LimitType.noLimit)
                                              app.SaveArticle(artManager.GetArticle(app.radioTech, type, IM.NullD));
                                          else
                                              continue;
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.UMTS)
                                  {
                                      if (app != null && app.appType == AppType.AppRR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.CDMA_450 || app.radioTech == CRadioTech.CDMA_800)
                                  {
                                      if (app != null && app.appType == AppType.AppRR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.DAMPS)
                                  {
                                      if (app != null && app.appType == AppType.AppRR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.DECT)
                                  {
                                      if (app != null && app.appType == AppType.AppRR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.PD)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.SHR || app.radioTech == CRadioTech.MsR || app.radioTech == CRadioTech.MmR)
                                  {
                                      if (app != null && app.appType == AppType.AppBS)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  // Земные станции обрабатываются в классе "ArticleLib.ArticleUpdate"
                                  //else if (app.radioTech == CRadioTech.ZS)
                                  //{
                                  //   if (app != null && app.appType == AppType.AppZS)
                                  //   {
                                  //      app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                  //      updtdRecCnt++;
                                  //   }
                                  //}
                                  else if (app.radioTech == CRadioTech.RS)
                                  {
                                      if (app != null && app.appType == AppType.AppRS)
                                      {
                                          MicrowaveApp mobApp = (MicrowaveApp)app;
                                          double fr = mobApp.st1[0].NomFreq;
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.freqLimit, fr));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.AAB)
                                  {
                                      if (app != null && app.appType == AppType.AppR2)
                                      {
                                          FmAnalogApp mobApp = (FmAnalogApp)app;
                                          double pow = mobApp.GetPower();
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.TDAB)
                                  {
                                      if (app != null && app.appType == AppType.AppR2d)
                                      {
                                          FmDigitalApp mobApp = (FmDigitalApp)app;
                                          double pow = mobApp.GetPower();
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.ATM)
                                  {
                                      if (app != null && app.appType == AppType.AppTV2)
                                      {
                                          CTvAnalog mobApp = (CTvAnalog)app;
                                          double pow = mobApp.GetPower();
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if ((app.radioTech == CRadioTech.DVBT) || (app.radioTech == CRadioTech.DVBT2))
                                  {
                                      if (app != null && app.appType == AppType.AppTV2d)
                                      {
                                          CTvDigital mobApp = (CTvDigital)app;
                                          double pow = mobApp.GetPower();
                                          app.SaveArticle(artManager.GetArticle(app.radioTech, LimitType.powerLimit, pow));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if ((app.radioTech == CRadioTech.RPATL) || (app.radioTech == CRadioTech.RPS))
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.ROPS)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else if (app.radioTech == CRadioTech.RRK)
                                  {
                                      if (app != null && app.appType == AppType.AppTR)
                                      {
                                          app.SaveArticle(artManager.GetArticleByRadioTech(app.radioTech));
                                          updtdRecCnt++;
                                      }
                                  }
                                  else
                                  {
                                      CLogs.WriteWarning(ELogsWhat.AutoSetArticle, string.Format("Can't set article for standard {0} APPL.ID = {1}", app.radioTech, app.ApplID));
                                  }
                              }
                          }
                      }
                      catch (Exception e)
                      {
                          CLogs.WriteError(ELogsWhat.Appl, e);
                      }
                  }
                  retVal += updtdRecCnt;
              }
              finally
              {
                  r.Close();
                  r.Destroy();
              }
          }

          return retVal;
      }
       

      /// <summary>
      /// Обновление кол-ва работ
      /// </summary>
      /// <returns>кол-во обновлений</returns>
      public static int UpdateWorksCount()
      {
          int retVal = 0;
          using (LisProgressBar pb = new LisProgressBar("Net count updating"))
          {
              pb.SetProgress(0, 1000);
              pb.SetBig("Net count updating");
              using (Icsm.LisRecordSet rsNet = new Icsm.LisRecordSet(PlugTbl.XfaNet, IMRecordset.Mode.ReadOnly))
              {
                  rsNet.Select("ID");
                  rsNet.SetWhere("PRICE_ID", IMRecordset.Operation.NotNull, IM.NullI);
                  rsNet.SetWhere("WORKS_COUNT", IMRecordset.Operation.Null, IM.NullI);
                  rsNet.SetWhere("AUTO_SET_ARTICLE", IMRecordset.Operation.Eq, 1);
                  rsNet.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
                  for (rsNet.Open(); !rsNet.IsEOF(); rsNet.MoveNext())
                  {
                      int idNet = rsNet.GetI("ID");
                      Net.NetObject net = new Net.NetObject();
                      net.Load(idNet);
                      int workCount = net.WorkCountSuggest;
                      if (workCount != IM.NullI)
                      {
                          net.WorkCount = workCount;
                          net.Save();
                          retVal++;
                      }
                  }
              }
          }

          using (LisProgressBar pb = new LisProgressBar("Count updating 1"))
          {
              IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
              r.Select("ID,OBJ_ID1,OBJ_TABLE");
              //r.SetWhere("WORKS_COUNT", IMRecordset.Operation.Null, IM.NullI);
              //r.SetWhere("PRICE_ID", IMRecordset.Operation.NotNull, IM.NullI);
              r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
              r.SetAdditional(string.Format(" (((PRICE_ID is not null) AND (WORKS_COUNT is null)) OR ((PRICE_ID2 is not null) AND (WORKS_COUNT2 is null)  AND (OBJ_TABLE='{0}'))) ", "XFA_ABONENT_STATION"));
              //r.SetAdditional("[ID] in ('10087712','450978160','451013628','451013651','451013655','451013658','451014658','451801029')");
              //r.SetAdditional("[ID] in ('451013658')");

              pb.SetBig("Works' count updating");
              try
              {
                  const int maxCounter = 50000;
                  int recCount = 0;
                  int recId = 0;
                  string tableName = "";
                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                      if (pb.UserCanceled())
                          break;
                      recId = r.GetI("OBJ_ID1");
                      tableName = r.GetS("OBJ_TABLE");

                      pb.SetBig(string.Format("{0}:{1}", tableName, recId));
                      pb.SetSmall(recCount++);
                      pb.SetProgress(recCount, maxCounter);
                      int appId = r.GetI("ID");
                      if (ArticleLib.CountWork.SetWorkCountExt(appId))
                          retVal++;
                  }
              }
              finally
              {
                  if (r.IsOpen())
                      r.Close();
                  r.Destroy();
              }
          }

          /*
          using (LisProgressBar pb = new LisProgressBar("Count updating 1"))
          {
              IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
              r.Select("ID,OBJ_ID1,OBJ_TABLE");
              r.SetWhere("WORKS_COUNT2", IMRecordset.Operation.Null, IM.NullI);
              r.SetWhere("PRICE_ID2", IMRecordset.Operation.NotNull, IM.NullI);
              r.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "XFA_ABONENT_STATION");
              r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
              r.SetAdditional("[ID] in ('10087712','450978160','451013628','451013651','451013655','451013658','451014658','451801029')");

              pb.SetBig("Works' count updating");
              try
              {
                  const int maxCounter = 50000;
                  int recCount = 0;
                  int recId = 0;
                  string tableName = "";
                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                      if (pb.UserCanceled())
                          break;
                      recId = r.GetI("OBJ_ID1");
                      tableName = r.GetS("OBJ_TABLE");

                      pb.SetBig(string.Format("{0}:{1}", tableName, recId));
                      pb.SetSmall(recCount++);
                      pb.SetProgress(recCount, maxCounter);
                      int appId = r.GetI("ID");
                      if (ArticleLib.CountWork.SetWorkCountExt(appId))
                          retVal++;
                  }
              }
              finally
              {
                  if (r.IsOpen())
                      r.Close();
                  r.Destroy();
              }
          }
           */ 
         

          using (LisProgressBar pb = new LisProgressBar("Count updating 2"))
          {
              IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadWrite);
              r.Select("ID,WORKS_COUNT,OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
              r.SetWhere("WORKS_COUNT", IMRecordset.Operation.Null, IM.NullI);
              r.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
              r.SetWhere("PRICE_ID", IMRecordset.Operation.NotNull, 0);
              r.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 0);//#6842 Если установлен флажок XNRFA_APPL.IS_CONFIRMED, запись должна игнорироваться
              //r.SetAdditional("[ID] in ('10087712','450978160','451013628','451013651','451013655','451013658','451014658','451801029')");

              pb.SetBig("Works' count updating");
              try
              {
                  int recCount = 0;
                  for (r.Open(); !r.IsEOF(); r.MoveNext())
                  {
                      if (pb.UserCanceled())
                          continue;
                      pb.SetSmall(++recCount);
                      int appId = r.GetI("ID");
                      DateTime s1 = IM.NullT;
                      List<int> ID = new List<int>();
                      for (int i = 1; i <= 6; ++i)
                      {
                          if (r.GetI("OBJ_ID" + i.ToString()) != IM.NullI)
                              ID.Add(r.GetI("OBJ_ID" + i.ToString()));
                      }
                      string s2 = "";
                      string s3 = "";
                      try
                      {
                          int wrkCount = BaseAppClass.GetDataForURCM(r.GetS("OBJ_TABLE"), ID, null, ref s2, ref s3);
                          if ((wrkCount != 0) && (wrkCount != IM.NullI))
                          {
                              r.Edit();
                              r.Put("WORKS_COUNT", wrkCount);
                              r.Update();
                              retVal++;
                          }
                      }
                      catch (Exception e)
                      {
                          CLogs.WriteError(ELogsWhat.Appl, e);
                      }
                  }
              }
              finally
              {
                  r.Close();
                  r.Destroy();
              }
          }
          return retVal;
      }
   }

   public class ArticlesManager
   {

      public struct techArticle
      {
         public string radioTech;
         public string article;
         public LimitType lType;
         public double min;
         public double max;
      }

      public List<techArticle> articles = new List<techArticle>();

      public void AddArticle(string article, string radiotech, LimitType lType, double min, double max)
      {
         techArticle t = new techArticle();
         t.article = article;
         t.max = max;
         t.min = min;
         t.radioTech = radiotech;
         t.lType = lType;
         articles.Add(t);
      }

      public void InitArticles()
      {
          articles = new List<techArticle>();

         IMRecordset a = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
         a.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,STANDARD,MIN,MAX,LIMITTYPE,STATUS,DATEOUTACTION,DATESTARTACTION");
        // a.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, Enum.GetName(typeof(WorkType), WorkType.work51));
         string department = Enum.GetName(typeof(ManagemenUDCR), ManagemenUDCR.URCM);
         a.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
         a.SetWhere("STATUS", IMRecordset.Operation.Eq, 1);
         a.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
         a.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

         try
         {
            a.Open();
            for (; !a.IsEOF(); a.MoveNext())
            {
               AddArticle(a.GetS("ARTICLE"), a.GetS("STANDARD"), (LimitType)Enum.Parse(typeof(LimitType), a.GetS("LIMITTYPE") == "" ? "noLimit" : a.GetS("LIMITTYPE")), a.GetD("MIN"), a.GetD("MAX"));
            }           
         }
         finally
         {
            a.Close();
            a.Destroy();
         }
      }

      public string GetArticleByRadioTech(string radTech)
      {
         foreach (techArticle t in articles)
         {
            if (t.radioTech == radTech && t.lType == LimitType.noLimit)
               return t.article;
         }
         return "";
      }

      public string GetArticle(string radTech, LimitType lType, double value)
      {
         foreach (techArticle t in articles)
         {
            if (t.radioTech == radTech && t.lType == lType && (value == IM.NullD || (t.max == IM.NullD || value <= t.max) && (value >= t.min || t.min == IM.NullD)))
               return t.article;
         }
         return "";
      }
   }
}
