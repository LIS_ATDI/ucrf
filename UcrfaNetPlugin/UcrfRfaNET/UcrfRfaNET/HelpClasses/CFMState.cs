﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class CFMState
   {
      private static List<string> InternalStateList = new List<string>();
      private static List<string> ForeignStateList = new List<string>();
         
      static CFMState()
      {
         // Внутреній стан
         InternalStateList.Add("1 - Рассматриваемый");
         InternalStateList.Add("2 - Разрешенный");
         InternalStateList.Add("3 - Действующий");
         InternalStateList.Add("4 - Новое сост.8");
         InternalStateList.Add("5 - Новое сост.2");
         InternalStateList.Add("6 - Новое сост.3");
         InternalStateList.Add("7 - Столб");
         InternalStateList.Add("8 - Рекоммендация");
         InternalStateList.Add("9 - Уточнение хар-к");
         InternalStateList.Add("10 - Планируемый");
         InternalStateList.Add("33 - BR IFIC ADD");
         InternalStateList.Add("34 - BR IFIC REC");
         InternalStateList.Add("35 - BR IFIC SUP");
         InternalStateList.Add("36 - BR IFIC MOD");
         InternalStateList.Add("38 - Geneva 06");

         // Міжнародний стан           
         ForeignStateList.Add("A - Скоорд.");
         ForeignStateList.Add("B - Берлин 59");
         ForeignStateList.Add("C - Чужой скоорд.");
         ForeignStateList.Add("D - Удаленный, был.");
         ForeignStateList.Add("E - Цифра, планируем.");
         ForeignStateList.Add("F - Прошел коорд.");
         ForeignStateList.Add("G - Женева 84");
         ForeignStateList.Add("G06 - GENEVA 2006");
         ForeignStateList.Add("H - Цифра, план Честер");
         ForeignStateList.Add("I - Честер");
         ForeignStateList.Add("K - На коорд.");
         ForeignStateList.Add("L - Цифра, удаленный");
         ForeignStateList.Add("M - Цифра, не надо коорд.");
         ForeignStateList.Add("N - Не надо коорд.");
         ForeignStateList.Add("O - Планируемый");
         ForeignStateList.Add("P - Под крышей");
         ForeignStateList.Add("Q - Цифра, регистр.");
         ForeignStateList.Add("R - Регистр");
         ForeignStateList.Add("S - Стокгольм");
         ForeignStateList.Add("T - Цифра, скоорд.");
         ForeignStateList.Add("TP - Цифра, скоорд. на пп");
         ForeignStateList.Add("U - Цифра на коорд.");
         ForeignStateList.Add("Y - Цифра, нескоорд.");
         ForeignStateList.Add("Z - Не скоорд.");
      }

      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getInternalStateList()
      {
         return new List<string>(InternalStateList);
      }

      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getForeignStateList()
      {
         return new List<string>(ForeignStateList);
      }

      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение поляризации
      /// </summary>
      /// <param name="longPolarization">полная строка поляризации</param>
      /// <returns>сокращенное значение поляризации</returns>
      public static string getShortState(string longState)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longState.Length; i++)
         {
            if (longState[i] == ' ')
               break;
            else
               sb.Append(longState[i]);
         }
         return sb.ToString();
      }
   }
}
