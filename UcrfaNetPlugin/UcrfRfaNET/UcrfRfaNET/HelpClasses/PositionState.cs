﻿using System.Windows;
using System;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.UtilityClass;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    public class PositionState : NotifyPropertyChanged, ICloneable
    {
        private Component.SelectAddressControlFast2.City _cityKoatu = null;
        public const string AllUkr = "Вся Україна";

        private int _id = IM.NullI;
        private string _tableName = "";
        //private double _longDms;
        //private double _latDms;
        private double _x;
        private double _y;
        private string _csys = "4DMS";
        private int _cityId;
        private string _countryId = "UKR";
        private double _aSl;
        private string _name = "";
        private string _address = "";
        private string _province = "";
        private string _subprovince = "";
        private string _city = "";
        private string _typeCity = "";
        private string _fullAddress = "";
        private string _remark = "";
        private double _distBorder;
        //private string _tower = "";
        
        public string CitiesCode { get; set; }
        public string CitiesName { get; set; }
        public string CitiesProvince { get; set; }
        public string CitiesSubprovince { get; set; }

        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    InvokeNotifyPropertyChanged("Id");
                }
            }
        }

        public object Clone()
        {
            PositionState clone = new PositionState();

            clone._id = _id;
            clone._tableName = _tableName;
            clone._x = _x;
            clone._y = _y;
            clone._csys = _csys;
            clone._cityId = _cityId;
            clone._countryId = _countryId;
            clone._aSl = _aSl;
            clone._name = _name;
            clone._address = _address;
            clone._province = _province;
            clone._subprovince = _subprovince;
            clone._city = _city;
            clone._typeCity = _typeCity;
            clone._fullAddress = _fullAddress;
            clone._remark = _remark;

            clone.StreetName = StreetName;
            clone.StreetType = StreetType;        
            clone.BuildingNumber = BuildingNumber;
        
            clone.AdminSitesId = AdminSitesId;
            clone.NeedAddAdminSite = NeedAddAdminSite;
            return clone;
        }

        public string TableName
        {
            get { return _tableName; }
            set
            {
                if (_tableName != value)
                {
                    _tableName = value;
                    InvokeNotifyPropertyChanged("TableName");
                }
            }
        }

        private void SetLongitude(string format, double val)
        {
            IMPosition pos1 = new IMPosition(val, Y, format);
            X = IMPosition.Convert(pos1, Csys).Lon;           
        }
        private void SetLatitude(string format, double val)
        {
            IMPosition pos1 = new IMPosition(X, val, format);
            Y = IMPosition.Convert(pos1, Csys).Lat;            
        }
        private double GetLongitude(string format)
        {
            IMPosition pos1 = new IMPosition(X, Y, Csys);
            return IMPosition.Convert(pos1, format).Lon;
        }
        private double GetLatitude(string format)
        {
            IMPosition pos1 = new IMPosition(X, Y, Csys);
            return IMPosition.Convert(pos1, format).Lat;
        }

        public double LongDms
        {
            get
            {
                return GetLongitude("4DMS");
            }
            set
            {
                SetLongitude("4DMS", value);
                InvokeNotifyPropertyChanged("LongDms");
            }
        }

        public const string FieldLonDms = "LonDms";
        public double LonDms
        {
            get
            {
                return LongDms;
            }
            set
            {
                LongDms = value;
                InvokeNotifyPropertyChanged(FieldLonDms);
            }
        }

        public const string FieldLatDms = "LatDms";
        public double LatDms
        {
            get
            {
                return GetLatitude("4DMS");
            }
            set
            {
                SetLatitude("4DMS", value);
                InvokeNotifyPropertyChanged(FieldLatDms);
            }
        }

        public double LatDec
        {
            get { return GetLatitude("4DEC"); }
        }

        public double LonDec
        {
            get { return GetLongitude("4DEC"); }
        }

        public int CityId
        {
            get { return _cityId; }
            set
            {
                if (_cityId != value)
                {
                    _cityId = value;
                    InvokeNotifyPropertyChanged("CityId");
                    _cityKoatu = new Component.SelectAddressControlFast2.City(_cityId);
                }
            }
        }

        public string CountryId
        {
            get { return _countryId; }
            set
            {
                if (_countryId != value)
                {
                    _countryId = value;
                    InvokeNotifyPropertyChanged("CountryId");
                }
            }
        }


        // вставка дополнительных параметров

        /*
        public string Tower
        {
            get { return _tower; }
            set
            {
                if (_tower != value)
                {
                    _tower = value;
                    InvokeNotifyPropertyChanged("TOWER");
                }
            }
        }
         */ 

        private int _datum;
        public int Datum
        {
            get { return _datum; }
            set
            {
                if (_datum != value)
                {
                    _datum = value;
                    InvokeNotifyPropertyChanged("Datum");
                }
            }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    InvokeNotifyPropertyChanged("Status");
                }
            }
        }

        public double ASl
        {
            get { return _aSl; }
            set
            {
                if (_aSl != value)
                {
                    _aSl = value;
                    InvokeNotifyPropertyChanged("ASl");
                }
            }
        }

        public double DistBorder
        {
            get { return _distBorder; }
            set
            {
                if (_distBorder != value)
                {
                    _distBorder = value;
                    InvokeNotifyPropertyChanged("DistBorder");
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    InvokeNotifyPropertyChanged("Name");
                }
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (_address != value)
                {
                    _address = value;
                    InvokeNotifyPropertyChanged("Address");
                }
            }
        }

        public string Province
        {
            get { return _province; }
            set
            {
                if (_province != value)
                {
                    _province = value;
                    InvokeNotifyPropertyChanged("Province");
                }
            }
        }

        public string Subprovince
        {
            get { return _subprovince; }
            set
            {
                if (_subprovince != value)
                {
                    _subprovince = value;
                    InvokeNotifyPropertyChanged("Subprovince");
                }
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                if (_city != value)
                {
                    _city = value;
                    InvokeNotifyPropertyChanged("City");
                }
            }
        }

        public string TypeCity
        {
            get { return _typeCity; }
            set
            {
                if (_typeCity != value)
                {
                    _typeCity = value;
                    InvokeNotifyPropertyChanged("TypeCity");
                }
            }
        }


        public const string FieldFullAddressAuto = "FullAddress";
        public string FullAddress
        {


            //get { return CreateFullAddress((_cityKoatu != null) ? _cityKoatu.Address : "", AddressAutoform,Tower);}
            get { return CreateFullAddress((_cityKoatu != null) ? _cityKoatu.Address : "", AddressAutoform); }
            set
            {
                if (_fullAddress != value)
                {
                    _fullAddress = value;
                    InvokeNotifyPropertyChanged("FullAddress");
                }
            }
        }

        public string AddressAutoform
        {

            get { return (_cityKoatu != null && _cityKoatu.type == "район") ? Remark : GenerateShortAddress(StreetType, StreetName, BuildingNumber, Remark); }
        }

        string _foa;
        public string FullOldAddress { get { return _foa; } set { if (_foa != value) { _foa = value; IsChanged = true; } } }

        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    InvokeNotifyPropertyChanged("Remark");
                }
            }
        }

        public double X
        {
            get { return _x; }
            set
            {
                if (_x != value)
                {
                    _x = value;
                    InvokeNotifyPropertyChanged("X");
                }
            }
        }

        public double Y
        {
            get { return _y; }
            set
            {
                if (_y != value)
                {
                    _y = value;
                    InvokeNotifyPropertyChanged("Y");
                }
            }
        }

        public string Csys
        {
            get { return _csys; }
            set
            {
                if (_csys != value)
                {
                    _csys = value;
                    InvokeNotifyPropertyChanged("Csys");
                }
            }
        }

        private int adminSitesID;
        public int AdminSitesId
        {
            get { return adminSitesID; }
            set
            {
                if (adminSitesID != value)
                {
                    adminSitesID = value;
                    InvokeNotifyPropertyChanged("AdminSitesId");
                }
            }
        }

        private bool _needAddAdminSite;
        public bool NeedAddAdminSite
        {
            get { return _needAddAdminSite; }
            set
            {
                if (_needAddAdminSite != value)
                {
                    _needAddAdminSite = value;
                    InvokeNotifyPropertyChanged("NeedAddAdminSite");
                }
            }
        }

        private string streetType;
        public string StreetType
        {
            get { return streetType; }
            set
            {
                if (streetType != value)
                {
                    streetType = value;
                    InvokeNotifyPropertyChanged("StreetType");
                }
            }
        }

        private string streetName;
        public string StreetName
        {
            get { return streetName; }
            set
            {
                if (streetName != value)
                {
                    streetName = value;
                    InvokeNotifyPropertyChanged("StreetName");
                }
            }
        }

        private string buildingNumber;
        public string BuildingNumber
        {
            get { return buildingNumber; }
            set
            {
                if (buildingNumber != value)
                {
                    buildingNumber = value;
                    InvokeNotifyPropertyChanged("BuildingNumber");
                }
            }
        }

        private object mapAltitude;
       

        public object MapAltitude
        {
            get { return mapAltitude; }
            set
            {
                if (mapAltitude != value)
                {
                    mapAltitude = value;
                    InvokeNotifyPropertyChanged("MapAltitude");
                }
            }
        }

        public PositionState()
        {
            AdminSitesId = IM.NullI;
            NeedAddAdminSite = false;
        }



        /// <summary>
        /// Формирует полный адрес
        /// </summary>
        /// <param name="koatyy">строка КОАТУУ</param>
        /// <param name="address">адрес</param>
        /// <returns></returns>
        //public static string CreateFullAddress(string koatyy, string address, string tower)
        public static string CreateFullAddress(string koatyy, string address)
        {
            string retVal = koatyy;
            if (string.IsNullOrEmpty(retVal))
                retVal = "";
            retVal += ((string.IsNullOrEmpty(address) || string.IsNullOrEmpty(retVal)) ? "" : ", ") + address;
            //if ((tower != "будівля") && (!string.IsNullOrEmpty(tower))) retVal += ", " + tower;
            return retVal;
        }

        /// <summary>
        /// Возвращает короткий адрес
        /// </summary>
        /// <returns>короткий адрес</returns>
        public static string GenerateShortAddress(string streetType, string street, string houseNumber, string note)
        {
            System.Text.StringBuilder adr = new System.Text.StringBuilder("");
            if ((string.IsNullOrEmpty(streetType) == false) && (string.IsNullOrEmpty(street) == false))
                adr.Append(streetType + " " + street);
            if (string.IsNullOrEmpty(houseNumber) == false)
            {
                if (adr.Length > 0)
                    adr.Append(", ");
                adr.Append(houseNumber);
            }
            if (string.IsNullOrEmpty(note) == false)
            {
                if (adr.Length > 0)
                    adr.Append(", ");
                adr.Append(note);
            }
            return adr.ToString();
        }

        /// <summary>
        /// Заполняем обьект данными
        /// </summary>
        public virtual void LoadStatePosition(int id, string table)
        {
            bool isAdmSites = (table == ICSMTbl.SITES);
            IMRecordset r = null;
            try
            {
                r = new IMRecordset(table, IMRecordset.Mode.ReadOnly);
                r.Select("ID,ASL,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID");
                r.Select("NAME,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK");
                r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7");
                if (!isAdmSites)
                    r.Select("SUBPROVINCE,TABLE_NAME,ADMS_ID");
                else
                    r.Select("DATUM,COUNTRY_ID,STATUS");
                r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                r.Open();
                if (!r.IsEOF())
                {
                    if (!isAdmSites)
                    {
                        TableName = r.GetS("TABLE_NAME");
                        Subprovince = r.GetS("SUBPROVINCE");
                        AdminSitesId = r.GetI("ADMS_ID");
                    }
                    else
                    {
                        TableName = table;
                        CountryId = r.GetS("COUNTRY_ID");
                        Status = r.GetS("STATUS");
                        Datum = r.GetI("DATUM");
                    }

                    Id = r.GetI("ID");                    
                    Csys = r.GetS("CSYS");
                    CityId = r.GetI("CITY_ID");
                    CountryId = "UKR";
                    ASl = r.GetD("ASL");
                    X = r.GetD("X");
                    Y = r.GetD("Y");
                    Name = r.GetS("NAME");
                    Address = r.GetS("ADDRESS");
                    Province = r.GetS("PROVINCE");
                    
                    City = r.GetS("CITY");
                    TypeCity = r.GetS("CUST_TXT1");
                    FullOldAddress = r.GetS("REMARK");
                    //FullAddress = r.GetS("REMARK");
                    Remark = r.GetS("CUST_TXT7");
                    //Tower = r.GetS("TOWER");
                    
                    StreetType = r.GetS("CUST_TXT4");
                    StreetName = r.GetS("CUST_TXT3");
                    BuildingNumber = r.GetS("CUST_TXT8");                  
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Dispose();
            }
            DistBorder = IM.NullI;
        }
        /// <summary>
        /// Заполняем обьект данными
        /// </summary>
        public void LoadStatePosition(IMObject obj)
        {
            if (obj == null)
                return;

            Id = obj.GetI("ID");
            bool isAdmSites = (obj.GetS("TABLE_NAME") == ICSMTbl.SITES);
            if (!isAdmSites)
            {
                TableName = obj.GetS("TABLE_NAME");
                Subprovince = obj.GetS("SUBPROVINCE");
                AdminSitesId = obj.GetI("ADMS_ID");
            }
            else
            {
                CountryId = obj.GetS("COUNTRY_ID");
                Status = obj.GetS("STATUS");
                Datum = obj.GetI("DATUM");
            }
            ASl = obj.GetD("ASL");
            CityId = obj.GetI("CITY_ID");
            CountryId = "UKR";
            ASl = obj.GetD("ASL");
            X = obj.GetD("X");
            Y = obj.GetD("Y");
            Csys = obj.GetS("CSYS");
            Name = obj.GetS("NAME");
            _address = obj.GetS("ADDRESS");
            Province = obj.GetS("PROVINCE");
            //Tower = obj.GetS("TOWER");
            City = obj.GetS("CITY");
            TypeCity = obj.GetS("CUST_TXT1");
            FullOldAddress = obj.GetS("REMARK");
            //FullAddress = obj.GetS("REMARK");
            Remark = obj.GetS("CUST_TXT7");
           
            StreetType = obj.GetS("CUST_TXT4");
            StreetName = obj.GetS("CUST_TXT3");
            BuildingNumber = obj.GetS("CUST_TXT8");
         //   DistBorder = obj.GetD("DIST_BORDER");
        }

        /// <summary>
        /// Копирует значения Sites->Position_x || Position_x->Sites 
        /// </summary>
        /// <param name="pos">об'єкт позиції</param>
        /// <param name="tblName">таблиця в якій зберігаємо</param>
        /// <returns>ID нового запису</returns>
        public static int ClonePositions(PositionState pos, string tblName)
        {
            int id = IM.NullI;
            if (pos == null)
                return IM.NullI;        
            if (tblName==ICSMTbl.SITES)
            {
                IMRecordset rSites = new IMRecordset(tblName, IMRecordset.Mode.ReadWrite);
                rSites.Select("ID,ASL,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID,NAME");
                rSites.Select("ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK,STATUS");
                rSites.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,DATUM,COUNTRY_ID");
                rSites.Select("DATE_CREATED,CREATED_BY");
                try
                {
                    id = IM.AllocID(tblName, 1, -1);
                    rSites.Open();
                    rSites.AddNew();
                    rSites.Put("ID", id);
                    rSites.Put("NAME",pos.Name);
                    rSites.Put("STATUS", CAndE.Need);
                    rSites.Put("X",pos.X);
                    rSites.Put("Y",pos.Y);
                    rSites.Put("CSYS", pos.Csys);
                    rSites.Put("LONGITUDE",pos.LonDec);
                    rSites.Put("LATITUDE", pos.LatDec);
                    rSites.Put("DATUM",4);
                    rSites.Put("ASL",pos.ASl);
                    rSites.Put("COUNTRY_ID","UKR");
                    rSites.Put("ADDRESS", pos.Address);
                    rSites.Put("CITY",pos.City);
                    rSites.Put("CITY_ID",pos.CityId);
                    rSites.Put("PROVINCE",pos.Province);
                    rSites.Put("CUST_TXT1",pos.TypeCity);
                    rSites.Put("CUST_TXT3", pos.StreetName);
                    rSites.Put("CUST_TXT4", pos.StreetType);
                    rSites.Put("CUST_TXT7", pos.Remark);
                    rSites.Put("CUST_TXT8", pos.BuildingNumber);
                    rSites.Put("DATE_CREATED", DateTime.Now);                    
                    rSites.Put("CREATED_BY", IM.ConnectedUser());
                    rSites.Update();                 
                }
                finally 
                {
                    rSites.Final();                    
                }
                AttachmentControl.CopyAttachment(pos.TableName, pos.Id, tblName, id);
                return id;
            }

            IMRecordset r = new IMRecordset(tblName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,ASL,TABLE_NAME,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID,NAME,ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK,ADMS_ID");
            r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7");
            try
            {
                id = IM.AllocID(tblName, 1, -1);
                r.Open();
                r.AddNew();
                r.Put("ID", id);
                r.Put("TABLE_NAME", tblName);
                r.Put("LONGITUDE",pos.LonDec);
                r.Put("LATITUDE", pos.LatDec);
                r.Put("ASL",pos.ASl);
                r.Put("CITY_ID",pos.CityId);
                
                r.Put("ASL",pos.ASl);
                r.Put("X",pos.X);
                r.Put("Y",pos.Y);
                r.Put("CSYS",pos.Csys);
                r.Put("NAME",pos.Name);
                r.Put("ADDRESS",pos.Address);
                r.Put("PROVINCE",pos.Province);
                r.Put("SUBPROVINCE",pos.Subprovince);
                r.Put("CITY",pos.City);
                r.Put("CUST_TXT1",pos.TypeCity);
                r.Put("REMARK", string.IsNullOrEmpty(pos.FullOldAddress) ? pos.FullAddress : pos.FullOldAddress); // Запишем, если пусто.
                r.Put("CUST_TXT7",pos.Remark);
                r.Put("ADMS_ID",pos.AdminSitesId);
                r.Put("CUST_TXT4",pos.StreetType);
                r.Put("CUST_TXT3", pos.StreetName);
                r.Put("CUST_TXT8",pos.BuildingNumber);

                r.Update();

            }
            finally
            {
                r.Final();
            }
            return id;
                       
        }

      

        /// <summary>
        /// Копирует значения Sites->Position_x || Position_x->Sites 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="tblName"></param>
        /// <returns></returns>
        public static int ClonePositions(IMObject obj, string tblName)
        {
            int id;
            if (obj == null)
                return IM.NullI;
            if (tblName == ICSMTbl.SITES)
            {
                IMRecordset rSites = new IMRecordset(tblName, IMRecordset.Mode.ReadWrite);
                rSites.Select("ID,ASL,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID,NAME");
                rSites.Select("ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK,STATUS");
                rSites.Select("CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,DATUM,COUNTRY_ID");
                rSites.Select("DATE_CREATED,CREATED_BY,TOWER");
                try
                {
                    id = IM.AllocID(tblName, 1, -1);
                    rSites.Open();
                    rSites.AddNew();
                    rSites.Put("ID", id);
                    rSites.Put("NAME", obj.GetS("NAME"));
                    rSites.Put("STATUS", CAndE.Need);
                    rSites.Put("X", obj.GetD("X"));
                    rSites.Put("Y", obj.GetD("Y"));
                    rSites.Put("CSYS", obj.GetS("CSYS"));
                    rSites.Put("LONGITUDE", obj.GetD("LONGITUDE"));
                    rSites.Put("LATITUDE", obj.GetD("LATITUDE"));
                    rSites.Put("DATUM", 4);
                    rSites.Put("ASL", obj.GetD("ASL"));
                    rSites.Put("COUNTRY_ID", "UKR");
                    rSites.Put("ADDRESS", obj.GetS("ADDRESS"));
                    rSites.Put("REMARK", obj.GetS("REMARK"));
                    rSites.Put("CITY", obj.GetS("CITY"));
                    int cityId = obj.GetI("CITY_ID");
                    rSites.Put("CITY_ID", cityId);
                    rSites.Put("PROVINCE", obj.GetS("PROVINCE"));
                    rSites.Put("REMARK", obj.GetS("REMARK"));
                    rSites.Put("CUST_TXT1", obj.GetS("CUST_TXT1"));
                    rSites.Put("CUST_TXT2", obj.GetS("CUST_TXT2"));
                    rSites.Put("CUST_TXT3", obj.GetS("CUST_TXT3"));
                    rSites.Put("CUST_TXT4", obj.GetS("CUST_TXT4"));
                    rSites.Put("CUST_TXT5", obj.GetS("CUST_TXT5"));
                    rSites.Put("CUST_TXT6", obj.GetS("CUST_TXT6"));
                    rSites.Put("CUST_TXT7", obj.GetS("CUST_TXT7"));
                    rSites.Put("CUST_TXT8", obj.GetS("CUST_TXT8"));
                    rSites.Put("CUST_TXT9", obj.GetS("CUST_TXT9"));
                    rSites.Put("DATE_CREATED", DateTime.Now);
                    rSites.Put("CREATED_BY", IM.ConnectedUser());
                
                    rSites.Put("TOWER", obj.GetS("TOWER"));
                    rSites.Update();
                    ////////////////////////////////////////////////
                    // Вставка значений двух полей в дочернюю таблицу XNRFA_LINKS_SITES
                    ////////////////////////////////////////////////
                    using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
                    {
                        using (Icsm.LisRecordSet r1 = new Icsm.LisRecordSet(PlugTbl.LinkSites, IMRecordset.Mode.ReadWrite))
                        {
                            int IDS=-1;

                            r1.Select("ID,EXT_ID_SITES,CUST_NBR1,CUST_NBR2,CUST_NBR3,CUST_NBR4,CUST_NBR5,CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5");
                            r1.SetWhere("EXT_ID_SITES", IMRecordset.Operation.Eq, id);

                            IDS = IM.AllocID(PlugTbl.LinkSites, 1, -1);
                            r1.Open();
                            r1.AddNew();
                                                                                  
                                                        
                            r1.Put("ID", IDS);
                            r1.Put("EXT_ID_SITES", id);
                            r1.Put("CUST_NBR1", obj.GetI("CUST_NBR1"));
                            r1.Put("CUST_NBR2", obj.GetI("CUST_NBR2"));

                           
                            r1.Update();
                        }
                        tr.Commit();
                    }
                    ////////////////////////////////////////////////

                 
                }
                finally
                {
                    rSites.Final();
                }
                return id;
            }

            IMRecordset r = new IMRecordset(tblName, IMRecordset.Mode.ReadWrite);
            r.Select("ID,ASL,TABLE_NAME,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID,NAME");
            r.Select("ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK,ADMS_ID");
            r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7,CODE,TOWER");

            if (tblName == ICSMTbl.Site)
            {
                r.Select("TOWER");
            }

            try
            {
                id = IM.AllocID(tblName, 1, -1);
                r.Open();
                r.AddNew();
                r.Put("ID", id);
                r.Put("TABLE_NAME", tblName);
                r.Put("CODE", id.ToString());
                r.Put("LONGITUDE", obj.GetD("LONGITUDE"));
                r.Put("LATITUDE", obj.GetD("LATITUDE"));
                
                r.Put("CITY_ID", obj.GetI("CITY_ID"));

                r.Put("ASL", obj.GetD("ASL"));
                r.Put("X", obj.GetD("X"));
                r.Put("Y", obj.GetD("Y"));
                r.Put("CSYS", obj.GetS("CSYS"));
                r.Put("NAME", obj.GetS("NAME"));
                r.Put("ADDRESS", obj.GetS("ADDRESS"));
                r.Put("PROVINCE", obj.GetS("PROVINCE"));
             
                r.Put("CITY", obj.GetS("CITY"));
                r.Put("CUST_TXT1", obj.GetS("CUST_TXT1"));
                r.Put("REMARK", obj.GetS("REMARK"));
                int admId = obj.GetI("ID");
                r.Put("ADMS_ID", admId);                
                r.Put("CUST_TXT4", obj.GetS("CUST_TXT4"));
                r.Put("CUST_TXT3", obj.GetS("CUST_TXT3"));
                r.Put("CUST_TXT8", obj.GetS("CUST_TXT8"));
                r.Put("CUST_TXT7", obj.GetS("CUST_TXT7"));

                if (tblName == ICSMTbl.Site)
                {
                    r.Put("TOWER", obj.GetS("TOWER"));
                }
               

                r.Update();

            }
            finally
            {
                r.Final();
            }
            return id;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Params">
        /// 0 - координаты как ДMC</param>
        public void SaveToBD(int Params)
        {
            if (Params == 0)
                Csys = "4DMS";
            SaveToBD();
        }

        /// <summary>
        /// Сохранение фотографий на диске и в БД их путей c помощью станции
        /// </summary>
        /// <param name="filesDst">список входящих путей</param>
        /// <param name="filesSrc">список исходящих путей</param>
        /// <param name="TableName"></param>
        public void SaveFoto(List<string> filesDst, List<string> filesSrc)
        {
            AttachmentControl ac = new AttachmentControl();
            ac.AttachFiles(filesDst, filesSrc, TableName, Id);
        }

        /// <summary>
        /// Сохранение фотографий на диске и в БД их путей c помощью станции
        /// </summary>
        /// <param name="filesDst">список входящих путей</param>
        /// <param name="filesSrc">список исходящих путей</param>
        /// <param name="TableName"></param>
        public void SaveFoto(List<string> filesDst, List<string> filesSrc, string tableName, int id)
        {
            AttachmentControl ac = new AttachmentControl();
            ac.AttachFiles(filesDst, filesSrc, tableName, id);            
        }

        /// <summary>
        /// Cохраняем в Базу
        /// </summary>
        public void SaveToBD()
        {
            if (string.IsNullOrEmpty(TableName))
                return;           
            if(NeedAddAdminSite)
                CreateAdminSite();
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadWrite);
            try
            {
                if (TableName != ICSMTbl.SITES)
                {
                    r.Select("TABLE_NAME,SUBPROVINCE,ADMS_ID,CODE");
                }
                r.Select("ID,ASL,LONGITUDE,COUNTRY_ID,X,Y,CSYS,LATITUDE");
                r.Select("CITY_ID,NAME,ASL,ADDRESS,PROVINCE,CITY,CUST_TXT1,REMARK");
                r.Select("DATE_CREATED,CREATED_BY");
                r.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
              
                if (r.IsEOF()) // , ...
                {
                    r.AddNew();
                    Id = IM.AllocID(TableName, 1, -1);
                    r.Put("CREATED_BY", IM.ConnectedUser());
                    r.Put("DATE_CREATED", DateTime.Now);
                }
                else // 
                    r.Edit();
                r.Put("ID", Id);
                if (TableName != ICSMTbl.SITES)
                {
                    r.Put("TABLE_NAME", TableName);
                    r.Put("SUBPROVINCE", Subprovince);
                    r.Put("ADMS_ID", AdminSitesId);
                    string code = r.GetS("CODE");
                    if (string.IsNullOrEmpty(code))
                        r.Put("CODE", Id.ToString());
                }
                //Maybe converted;
                r.Put("LONGITUDE", LonDec);
                r.Put("LATITUDE", LatDec);
                r.Put("ASL", ASl);
                r.Put("CITY_ID", CityId);
                r.Put("COUNTRY_ID", CountryId);
                string name = r.GetS("NAME");
                if (string.IsNullOrEmpty(name))
                    r.Put("NAME", FullAddress.Substring(0, (FullAddress.Length > 40) ? 40 : FullAddress.Length) + Id.ToString());
                r.Put("ADDRESS", Address);
                r.Put("PROVINCE", Province);
                
                r.Put("CITY", City);
                r.Put("CUST_TXT1", TypeCity);
                r.Put("REMARK", string.IsNullOrEmpty(FullOldAddress) ? FullAddress : FullOldAddress); // Запишем, если пусто.
           
                r.Put("X", X);
                r.Put("Y", Y);
                r.Put("CSYS", Csys);
                
                r.Put("CUST_TXT3", StreetName);
                r.Put("CUST_TXT4", StreetType);
                r.Put("CUST_TXT7", Remark);
                r.Put("CUST_TXT8", BuildingNumber);
                
              //  r.Put("DIST_BORDER",DistBorder);
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }
        /// <summary>
        /// Создает админ сайт, если необходимо
        /// </summary>
        /// <returns>TRUE - админ сайт создан, иначе FALSE</returns>
        public bool CreateAdminSite()
        {
            if((this.AdminSitesId == IM.NullI) && (this.TableName != ICSMTbl.SITES))
            {
                int idAdmSite = ClonePositions(this, ICSMTbl.SITES);
                this.AdminSitesId = idAdmSite;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Переопределяет позицию на другую таблицу (Требует вызова функции SaveToDB)
        /// </summary>
        /// <param name="table">Название таблицы, на поторую переопределяется</param>
        public void CopyToTable(string table)
        {
            if(this.TableName != table)
            {
                this.Id = IM.NullI;
                this.TableName = table;
            }
        }

        /// <summary>
        /// Заповнює властивості нас.пункту з довідника КОАТУУ
        /// </summary>
        /// <param name="id">id нас.пункту</param>
        public void LoadCitiesId(int id)
        {
            IMRecordset r=new IMRecordset(ICSMTbl.itblCities,IMRecordset.Mode.ReadOnly);
            r.Select("ID,CODE,NAME,PROVINCE,SUBPROVINCE");
            r.SetWhere("ID",IMRecordset.Operation.Eq,id);
            try
            {
                r.Open();
                if (!r.IsEOF())
                {
                    CitiesName = r.GetS("NAME");
                    CitiesCode = r.GetS("CODE");
                    CitiesProvince = r.GetS("PROVINCE");
                    CitiesSubprovince = r.GetS("SUBPROVINCE");
                }
            }
            finally 
            {
                r.Final();                
            }
        }

        /// <summary>
        /// Об'єднання комірок
        /// </summary>
        /// <param name="context">запис, з яким зливається інший(запис остається)</param>
        /// <param name="recAnt">запис, який зливається з context(запис видаляється)</param>        
        /// <param name="changeFields">Чи потрібно копіювати дані при об'єднанні</param>
        public static void ChangeSites(RecordPtr context, RecordPtr recAnt, bool changeFields)
        {
            if (context.Table != ICSMTbl.SITES || recAnt.Table != ICSMTbl.SITES)
                return;

            bool isNew = (context.Id == IM.NullI);

            bool isExists;
            // 1.Копіювати всі дані з context            
            IMObject obj = null;
            IMRecordset rTmp= new IMRecordset(context.Table,IMRecordset.Mode.ReadOnly);
            rTmp.Select("ID");
            rTmp.SetWhere("ID",IMRecordset.Operation.Eq,context.Id);
            try
            {
                rTmp.Open();
                isExists = !rTmp.IsEOF();
            }
            finally
            {
                rTmp.Final();
            }
            if (!isNew && isExists)
                obj = IMObject.LoadFromDB(context);
            // 2.1 Визначити всі позиції,які зсилаються на recAnt
            List<RecordPtr> recPosList = new List<RecordPtr>();
            string[] Positions = new[]
                                     {
                                         ICSMTbl.itblPositionBro,
                                         ICSMTbl.itblPositionEs, ICSMTbl.itblPositionFmn, ICSMTbl.itblPositionHf,
                                         ICSMTbl.itblPositionMob2, ICSMTbl.itblPositionMw, ICSMTbl.itblPositionWim
                                     };
            for (int i = 0; i < Positions.Length; i++)
            {
                IMRecordset rPos = new IMRecordset(Positions[i], IMRecordset.Mode.ReadOnly);
                rPos.Select("ID,ADMS_ID");
                rPos.SetWhere("ADMS_ID", IMRecordset.Operation.Eq, recAnt.Id);
                try
                {
                    for (rPos.Open(); !rPos.IsEOF(); rPos.MoveNext())
                        recPosList.Add(new RecordPtr(Positions[i], rPos.GetI("ID")));
                }
                finally
                {
                    rPos.Final();
                }
            }
            if (obj != null)
            {
                // 2.2.Внести у всі POS2 інформацію з obj 
                for (int i = 0; i < recPosList.Count; i++)
                {
                    IMRecordset rRecPos = new IMRecordset(recPosList[i].Table, IMRecordset.Mode.ReadWrite);
                    rRecPos.Select("ID,ASL,TABLE_NAME,LONGITUDE,X,Y,CSYS,LATITUDE,CITY_ID,NAME");
                    rRecPos.Select("ADDRESS,PROVINCE,SUBPROVINCE,CITY,CUST_TXT1,REMARK,ADMS_ID");
                    rRecPos.Select("CUST_TXT4,CUST_TXT3,CUST_TXT8,CUST_TXT7,DATUM,COUNTRY_ID");
                    rRecPos.SetWhere("ID", IMRecordset.Operation.Eq, recPosList[i].Id);
                    try
                    {
                        rRecPos.Open();
                        rRecPos.Edit();
                        rRecPos.Put("ADMS_ID", obj.GetI("ID"));
                        if (changeFields)
                        {
                            rRecPos.Put("LONGITUDE", obj.GetD("LONGITUDE"));
                            rRecPos.Put("LATITUDE", obj.GetD("LATITUDE"));
                            rRecPos.Put("X", obj.GetD("X"));
                            rRecPos.Put("Y", obj.GetD("Y"));
                            rRecPos.Put("CSYS", obj.GetS("CSYS"));
                            rRecPos.Put("CITY_ID", obj.GetI("CITY_ID"));
                            rRecPos.Put("DATUM", obj.GetI("DATUM"));
                            rRecPos.Put("ASL", obj.GetD("ASL"));
                            rRecPos.Put("COUNTRY_ID", obj.GetS("COUNTRY_ID"));
                            rRecPos.Put("ADDRESS", obj.GetS("ADDRESS"));
                            rRecPos.Put("PROVINCE", obj.GetS("PROVINCE"));
                            rRecPos.Put("CITY", obj.GetS("CITY"));
                            rRecPos.Put("NAME", obj.GetS("NAME"));
                            rRecPos.Put("REMARK", obj.GetS("REMARK"));
                        }
                        rRecPos.Update();
                    }
                    finally
                    {
                        rRecPos.Final();
                    }
                }
            }
        }
    }

}
