﻿namespace XICSM.UcrfRfaNET
{
   partial class FAttachDoc
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.label1 = new System.Windows.Forms.Label();
            this.cbDocName = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpDateIn = new System.Windows.Forms.DateTimePicker();
            this.lbDateIn = new System.Windows.Forms.Label();
            this.tbRefIn = new System.Windows.Forms.TextBox();
            this.lbRefIn = new System.Windows.Forms.Label();
            this.chbUseExpDate = new System.Windows.Forms.CheckBox();
            this.dtExpDate = new System.Windows.Forms.DateTimePicker();
            this.dtDocDate = new System.Windows.Forms.DateTimePicker();
            this.labelInDate = new System.Windows.Forms.Label();
            this.tbDocRef = new System.Windows.Forms.TextBox();
            this.labelInNumber = new System.Windows.Forms.Label();
            this.tbFilePath = new System.Windows.Forms.TextBox();
            this.buttonAttach = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnAdditionalData = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Тип документу";
            // 
            // cbDocName
            // 
            this.cbDocName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocName.FormattingEnabled = true;
            this.cbDocName.Location = new System.Drawing.Point(122, 6);
            this.cbDocName.Name = "cbDocName";
            this.cbDocName.Size = new System.Drawing.Size(347, 21);
            this.cbDocName.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpDateIn);
            this.groupBox1.Controls.Add(this.lbDateIn);
            this.groupBox1.Controls.Add(this.tbRefIn);
            this.groupBox1.Controls.Add(this.lbRefIn);
            this.groupBox1.Controls.Add(this.chbUseExpDate);
            this.groupBox1.Controls.Add(this.dtExpDate);
            this.groupBox1.Controls.Add(this.dtDocDate);
            this.groupBox1.Controls.Add(this.labelInDate);
            this.groupBox1.Controls.Add(this.tbDocRef);
            this.groupBox1.Controls.Add(this.labelInNumber);
            this.groupBox1.Location = new System.Drawing.Point(15, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 113);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registration data";
            // 
            // dtpDateIn
            // 
            this.dtpDateIn.CustomFormat = " ";
            this.dtpDateIn.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpDateIn.Enabled = false;
            this.dtpDateIn.Location = new System.Drawing.Point(297, 19);
            this.dtpDateIn.Name = "dtpDateIn";
            this.dtpDateIn.Size = new System.Drawing.Size(143, 20);
            this.dtpDateIn.TabIndex = 12;
            // 
            // lbDateIn
            // 
            this.lbDateIn.AutoSize = true;
            this.lbDateIn.Enabled = false;
            this.lbDateIn.Location = new System.Drawing.Point(243, 23);
            this.lbDateIn.Name = "lbDateIn";
            this.lbDateIn.Size = new System.Drawing.Size(48, 13);
            this.lbDateIn.TabIndex = 11;
            this.lbDateIn.Text = "Вх. дата";
            // 
            // tbRefIn
            // 
            this.tbRefIn.Enabled = false;
            this.tbRefIn.Location = new System.Drawing.Point(50, 19);
            this.tbRefIn.Name = "tbRefIn";
            this.tbRefIn.Size = new System.Drawing.Size(143, 20);
            this.tbRefIn.TabIndex = 10;
            // 
            // lbRefIn
            // 
            this.lbRefIn.AutoSize = true;
            this.lbRefIn.Enabled = false;
            this.lbRefIn.Location = new System.Drawing.Point(8, 22);
            this.lbRefIn.Name = "lbRefIn";
            this.lbRefIn.Size = new System.Drawing.Size(36, 13);
            this.lbRefIn.TabIndex = 9;
            this.lbRefIn.Text = "Вх. №";
            // 
            // chbUseExpDate
            // 
            this.chbUseExpDate.AutoSize = true;
            this.chbUseExpDate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbUseExpDate.Location = new System.Drawing.Point(216, 85);
            this.chbUseExpDate.Name = "chbUseExpDate";
            this.chbUseExpDate.Size = new System.Drawing.Size(75, 17);
            this.chbUseExpDate.TabIndex = 8;
            this.chbUseExpDate.Text = "Термін дії";
            this.chbUseExpDate.UseVisualStyleBackColor = true;
            // 
            // dtExpDate
            // 
            this.dtExpDate.Checked = false;
            this.dtExpDate.CustomFormat = " ";
            this.dtExpDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExpDate.Location = new System.Drawing.Point(297, 82);
            this.dtExpDate.Name = "dtExpDate";
            this.dtExpDate.Size = new System.Drawing.Size(143, 20);
            this.dtExpDate.TabIndex = 7;
            this.dtExpDate.ValueChanged += new System.EventHandler(this.dtOutDate_ValueChanged);
            // 
            // dtDocDate
            // 
            this.dtDocDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtDocDate.Location = new System.Drawing.Point(297, 51);
            this.dtDocDate.Name = "dtDocDate";
            this.dtDocDate.Size = new System.Drawing.Size(143, 20);
            this.dtDocDate.TabIndex = 6;
            // 
            // labelInDate
            // 
            this.labelInDate.AutoSize = true;
            this.labelInDate.Location = new System.Drawing.Point(255, 55);
            this.labelInDate.Name = "labelInDate";
            this.labelInDate.Size = new System.Drawing.Size(33, 13);
            this.labelInDate.TabIndex = 4;
            this.labelInDate.Text = "Дата";
            // 
            // tbDocRef
            // 
            this.tbDocRef.Location = new System.Drawing.Point(50, 52);
            this.tbDocRef.Name = "tbDocRef";
            this.tbDocRef.Size = new System.Drawing.Size(143, 20);
            this.tbDocRef.TabIndex = 2;
            // 
            // labelInNumber
            // 
            this.labelInNumber.AutoSize = true;
            this.labelInNumber.Location = new System.Drawing.Point(23, 55);
            this.labelInNumber.Name = "labelInNumber";
            this.labelInNumber.Size = new System.Drawing.Size(21, 13);
            this.labelInNumber.TabIndex = 0;
            this.labelInNumber.Text = "№ ";
            // 
            // tbFilePath
            // 
            this.tbFilePath.Location = new System.Drawing.Point(15, 161);
            this.tbFilePath.Name = "tbFilePath";
            this.tbFilePath.ReadOnly = true;
            this.tbFilePath.Size = new System.Drawing.Size(335, 20);
            this.tbFilePath.TabIndex = 3;
            // 
            // buttonAttach
            // 
            this.buttonAttach.Location = new System.Drawing.Point(356, 159);
            this.buttonAttach.Name = "buttonAttach";
            this.buttonAttach.Size = new System.Drawing.Size(113, 23);
            this.buttonAttach.TabIndex = 4;
            this.buttonAttach.Text = "Приєднати док";
            this.buttonAttach.UseVisualStyleBackColor = true;
            this.buttonAttach.Click += new System.EventHandler(this.buttonAttach_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(273, 196);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 5;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(364, 196);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // btnAdditionalData
            // 
            this.btnAdditionalData.Location = new System.Drawing.Point(15, 196);
            this.btnAdditionalData.Name = "btnAdditionalData";
            this.btnAdditionalData.Size = new System.Drawing.Size(211, 23);
            this.btnAdditionalData.TabIndex = 7;
            this.btnAdditionalData.Text = "Additional Data";
            this.btnAdditionalData.UseVisualStyleBackColor = true;
            this.btnAdditionalData.Click += new System.EventHandler(this.btnAdditionalData_Click);
            // 
            // FAttachDoc
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(486, 233);
            this.Controls.Add(this.btnAdditionalData);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonAttach);
            this.Controls.Add(this.tbFilePath);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbDocName);
            this.Controls.Add(this.label1);
            this.Name = "FAttachDoc";
            this.Text = "FAttachDoc";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.ComboBox cbDocName;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label labelInNumber;
      private System.Windows.Forms.TextBox tbDocRef;
      private System.Windows.Forms.DateTimePicker dtExpDate;
      private System.Windows.Forms.DateTimePicker dtDocDate;
      private System.Windows.Forms.Label labelInDate;
      private System.Windows.Forms.TextBox tbFilePath;
      private System.Windows.Forms.Button buttonAttach;
      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.CheckBox chbUseExpDate;
      private System.Windows.Forms.DateTimePicker dtpDateIn;
      private System.Windows.Forms.Label lbDateIn;
      private System.Windows.Forms.TextBox tbRefIn;
      private System.Windows.Forms.Label lbRefIn;
      private System.Windows.Forms.Button btnAdditionalData;
   }
}