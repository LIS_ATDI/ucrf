﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    internal class T1nToClink
    {
        // списки НД новых линков и соотв. станций. 
        List<IMRecordset> listLinks = new List<IMRecordset>();
        List<IMRecordset> listStationsA = new List<IMRecordset>();
        List<IMRecordset> listStationsB = new List<IMRecordset>();            
            
        internal void Import(IMQueryMenuNode.Context context)
        {
            IMRecordset rsAntennas = null;
            IMRecordset rsRxs = null;

            using (LisProgressBar pb = new LisProgressBar(CLocaliz.TxT("Обробка повідомлень")))
            {
                //pb.

                IMRecordset rsNotices = new IMRecordset(ICSMTbl.FixMobNotice, IMRecordset.Mode.ReadOnly);
                try
                {
                    rsNotices.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.FixMobNotice));
                    rsNotices.Select("Position.NAME");
                    rsNotices.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                    for (rsNotices.Open(); !rsNotices.IsEOF(); rsNotices.MoveNext())
                    {
                        pb.SetBig(CLocaliz.TxT(rsNotices.GetS("NAME")));
                        pb.Increment(true);

                        // игнорируем все действия, кроме "ADD"
                        if (rsNotices.GetS("T_NOTICE_TYPE") != "T11")
                            continue;
                        if ((rsNotices.GetS("T_ACTION") != "ADD") && (rsNotices.GetS("T_ACTION") != "MODIFY"))
                            continue;

                        // выгребаем все антенны, а у тех - все приёмники
                        if (rsAntennas != null) { rsAntennas.Destroy(); rsAntennas = null; }
                        rsAntennas = new IMRecordset(ICSMTbl.T1nAntennas, IMRecordset.Mode.ReadOnly);
                        rsAntennas.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.T1nAntennas));
                        rsAntennas.SetWhere("NOT_ID", IMRecordset.Operation.Eq, rsNotices.GetI("ID"));
                        rsAntennas.Open();
                        if (rsAntennas.IsEOF())
                            ProcessLink(rsNotices, null, null);
                        else
                            for (; !rsAntennas.IsEOF(); rsAntennas.MoveNext())
                            {
                                if (rsRxs != null) { rsRxs.Destroy(); rsRxs = null; }
                                rsRxs = new IMRecordset(ICSMTbl.T1nRxTxStations, IMRecordset.Mode.ReadOnly);
                                rsRxs.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.T1nRxTxStations));
                                rsRxs.SetWhere("ANT_ID", IMRecordset.Operation.Eq, rsAntennas.GetI("ID"));
                                rsRxs.Open();
                                if (rsRxs.IsEOF())
                                    ProcessLink(rsNotices, rsAntennas, null);
                                else
                                    for (; !rsRxs.IsEOF(); rsRxs.MoveNext())
                                    {
                                        ProcessLink(rsNotices, rsAntennas, rsRxs);
                                    }
                            }
                    }

                    // кэш координационных запросов
                    Dictionary<string, Dictionary<DateTime, int>> reqCache = new Dictionary<string, Dictionary<DateTime, int>>();
                    
                    //записываем!
                    for (int i = 0; i < listLinks.Count; i++)
                    {
                        IMRecordset curLink = listLinks[i];
                        if (curLink == null) 
                            continue;

                        int staId = curLink.GetI("ID");
                        string adm = curLink.GetS("ADM");
                        DateTime reqDate = curLink.GetT("CREQUEST_DATE");
                        string reqRef = "---";

                        curLink.Update();

                        if (i < listStationsA.Count && listStationsA[i] != null) listStationsA[i].Update();
                        if (i < listStationsB.Count && listStationsB[i] != null) listStationsB[i].Update();

                        //provision
                        IMRecordset rsProv = new IMRecordset(ICSMTbl.FxLkClinkPrv, IMRecordset.Mode.ReadWrite);
                        try
                        {
                            rsProv.Select("ID,STA_ID,ADM,REQ_ID,REQ_REF,REQ_DATE");
                            rsProv.SetWhere("ID", IMRecordset.Operation.Eq, 0);
                            rsProv.Open();
                            rsProv.AddNew();
                            rsProv.Put("ID", IM.AllocID(ICSMTbl.FxLkClinkPrv, 1, -1));
                            rsProv.Put("ADM", "UKR");
                            rsProv.Put("STA_ID", staId);

                            int reqId = IM.NullI;
                            
                            if (reqCache.ContainsKey(adm))
                            {
                                Dictionary<DateTime, int> dateCache = reqCache[adm];
                                if (dateCache != null && dateCache.ContainsKey(reqDate))
                                    reqId = dateCache[reqDate];
                            }
                            
                            if (reqId == IM.NullI)
                            {
                                // new request
                                IMRecordset rsReq = new IMRecordset(ICSMTbl.FxLkCoordReg, IMRecordset.Mode.ReadWrite);
                                try 
                                {
                                    rsReq.Select("ID,REQ_ADM,SERVICE,REQ_DATE,REQ_REF,REQ_STATUS,TRG_ADM");
                                    rsReq.SetWhere("ID", IMRecordset.Operation.Eq, 0);
                                    rsReq.Open();
                                    rsReq.AddNew();
                                    rsReq.Put("ID", IM.AllocID(ICSMTbl.FxLkCoordReg, 1, -1));
                                    rsReq.Put("REQ_ADM", adm);
                                    rsReq.Put("SERVICE", "FIX");
                                    rsReq.Put("REQ_DATE", reqDate);
                                    rsReq.Put("REQ_REF", reqRef);
                                    rsReq.Put("TRG_ADM", "UKR");
                                    rsReq.Update();
                                    
                                    reqId = rsReq.GetI("ID");
                                    Dictionary<DateTime, int> dateCache = null;
                                    if (!reqCache.ContainsKey(adm))
                                        reqCache.Add(adm, new Dictionary<DateTime, int>());
                                    dateCache = reqCache[adm];
                                    dateCache.Add(reqDate, reqId);
                                }
                                finally 
                                {
                                    rsReq.Destroy();
                                }
                            }

                            rsProv.Put("REQ_ID", reqId);
                            rsProv.Put("REQ_DATE", reqDate);
                            rsProv.Put("REQ_REF", reqRef);
                            rsProv.Update();
                        } finally {
                            rsProv.Destroy();
                        }
                    }

                }
                finally
                {
                    rsNotices.Destroy();
                    rsNotices = null;
                    if (rsAntennas != null) { rsAntennas.Destroy(); rsAntennas = null; }
                    if (rsRxs != null) { rsRxs.Destroy(); rsRxs = null; }

                    for (int i = 0; i < listLinks.Count; i++)
                        if (listLinks[i] != null) listLinks[i].Destroy();
                    listLinks.Clear();
                    for (int i = 0; i < listStationsA.Count; i++)
                        if (listStationsA[i] != null) listStationsA[i].Destroy();
                    listStationsA.Clear();
                    for (int i = 0; i < listStationsB.Count; i++)
                        if (listStationsB[i] != null) listStationsB[i].Destroy();
                    listStationsB.Clear();
                }
            }
        }

        private void ProcessLink(IMRecordset rsNotices, IMRecordset rsAntennas, IMRecordset rsRxs)
        {
            // для уведомления смотрим, где есть сайт этой станции (название и координаты) в приёмниках-заглушках, 
            // да чтоб сайт того передатчика совпадал с сайтом одного из этих приёмников (название и координаты).
            // и кроме того, классы излучения и вся поебень, в том числе некоторые параметры антенн, должны совпадать. 
            // тогда оформляем как это релейку, обновляем этими данными станцию Б, она больше не заглушка
            // иначе - новая станция, с заглушкой вместо станции Б
            bool isNew = true;
            if (rsAntennas != null && rsRxs != null)
            {
                for (int i = 0; i < listLinks.Count; i++)
                    if (listStationsB[i].GetD("TX_FREQ") == IM.NullD &&

                        rsNotices.GetS("NAME") == listStationsB[i].GetS("NAME") &&
                        rsNotices.GetS("COUNTRY_ID") == listStationsB[i].GetS("COUNTRY_ID") &&
                        rsNotices.GetD("LONGITUDE") == listStationsB[i].GetD("LONGITUDE") &&
                        rsNotices.GetD("LATITUDE") == listStationsB[i].GetD("LATITUDE") &&

                        rsRxs.GetS("NAME") == listStationsA[i].GetS("NAME") &&
                        rsRxs.GetS("COUNTRY_ID") == listStationsA[i].GetS("COUNTRY_ID") &&
                        rsRxs.GetD("LONGITUDE") == listStationsA[i].GetD("LONGITUDE") &&
                        rsRxs.GetD("LATITUDE") == listStationsA[i].GetD("LATITUDE") &&

                        rsAntennas.GetS("T_PWR_XYZ") == listLinks[i].GetS("PWR_XYZ") &&
                        //rsAntennas.GetS("T_PWR_EIV") == listLinks[i].GetS("PWR_EIV") &&

                        rsNotices.GetS("T_STN_CLS") == listLinks[i].GetS("CLASS") &&
                        rsNotices.GetS("NAT_SRV") == listLinks[i].GetS("NAT_SRV") &&
                        rsNotices.GetS("DESIG_EMISSION") == listLinks[i].GetS("DESIG_EM")                        
                        )
                    {
                        //update station B parameters with current values
                        UpdateStationParameters(listStationsB[i], rsNotices, rsAntennas);
                        isNew = false;
                        break;
                    }
            }
            if (isNew)
            {
                //новый линк
                IMRecordset rsLink = new IMRecordset(ICSMTbl.FxLkClink, IMRecordset.Mode.ReadWrite);
                listLinks.Add(rsLink);
                rsLink.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.FxLkClink));
                rsLink.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                rsLink.Open();
                rsLink.AddNew();
                int idLink = IM.AllocID(ICSMTbl.FxLkClink, 1, -1);
                rsLink.Put("ID", idLink);
                rsLink.Put("A", "A");
                rsLink.Put("B", "B");

                rsLink.Put("ADM", rsNotices.GetS("ADM"));
                rsLink.Put("CREQUEST_DATE", rsNotices.GetT("T_D_ADM_NTC"));
                rsLink.Put("LASTK_DATE", rsNotices.GetT("T_D_ADM_NTC"));
                rsLink.Put("C_SOUGHT", 1);
                rsLink.Put("C_ADM_COMP", 1);
                rsLink.Put("C_STATUS", "1"); // "Waiting for reply"
                
                rsLink.Put("PWR_XYZ", rsAntennas.GetS("T_PWR_XYZ"));
                rsLink.Put("PWR_EIV", rsAntennas.GetS("T_PWR_EIV"));

                rsLink.Put("CLASS", rsNotices.GetS("T_STN_CLS"));
                rsLink.Put("NAT_SRV", rsNotices.GetS("NAT_SRV"));
                rsLink.Put("DESIG_EM", rsNotices.GetS("DESIG_EMISSION"));
                rsLink.Put("BIUSE_DATE", rsNotices.GetT("T_D_INUSE"));
                rsLink.Put("ADM_REMARKS", rsNotices.GetS("T_REMARKS"));
                rsLink.Put("OP_HH_FR", rsNotices.GetD("T_OP_HH_FR"));
                rsLink.Put("OP_HH_TO", rsNotices.GetD("T_OP_HH_TO"));

                rsLink.Put("CREATED_BY", IM.ConnectedUser());
                rsLink.Put("DATE_CREATED", DateTime.Now);
                
                //новая станция А
                IMRecordset rsStationA = new IMRecordset(ICSMTbl.FxLkClinkSta, IMRecordset.Mode.ReadWrite);
                listStationsA.Add(rsStationA);
                rsStationA.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.FxLkClinkSta));
                rsStationA.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                rsStationA.Open();
                rsStationA.AddNew();
                int idA = IM.AllocID(ICSMTbl.FxLkClinkSta, 1, -1);
                rsStationA.Put("ID", idA);
                rsStationA.Put("CLINK_ID", idLink);
                rsStationA.Put("ROLE", "A");
                rsStationA.Put("END_ROLE", "B");

                UpdateStationParameters(rsStationA, rsNotices, rsAntennas);

                //новая станция Б
                //заглушка
                IMRecordset rsStationB = new IMRecordset(ICSMTbl.FxLkClinkSta, IMRecordset.Mode.ReadWrite);
                listStationsB.Add(rsStationB);
                rsStationB.Select(OrmHelp.GetAllNamesFromTable(ICSMTbl.FxLkClinkSta));
                rsStationB.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                rsStationB.Open();
                rsStationB.AddNew();
                int idB = IM.AllocID(ICSMTbl.FxLkClinkSta, 1, -1);
                rsStationB.Put("ID", idB);
                rsStationB.Put("CLINK_ID", idLink);
                rsStationB.Put("ROLE", "B");
                rsStationB.Put("END_ROLE", "A");

                rsStationB.Put("NAME", rsRxs.GetS("NAME"));
                rsStationB.Put("COUNTRY_ID", rsRxs.GetS("COUNTRY_ID"));
                rsStationB.Put("LONGITUDE", rsRxs.GetD("LONGITUDE"));
                rsStationB.Put("LATITUDE", rsRxs.GetD("LATITUDE"));
                
            }
        }

        private void UpdateStationParameters(IMRecordset rsStation, IMRecordset rsNotices, IMRecordset rsAntennas)
        {
            rsStation.Put("TX_FREQ", rsNotices.GetD("T_FREQ_ASSGN"));
            rsStation.Put("NAME", rsNotices.GetS("NAME"));
            //rsStation.Put("NAME", rsNotices.GetS("T_STATION_ID"));
            rsStation.Put("COUNTRY_ID", rsNotices.GetS("COUNTRY_ID"));
            rsStation.Put("LONGITUDE", rsNotices.GetD("LONGITUDE"));
            rsStation.Put("LATITUDE", rsNotices.GetD("LATITUDE"));
            rsStation.Put("ASL", rsNotices.GetD("ASL"));

            rsStation.Put("PWR_ANT", rsAntennas.GetD("T_PWR_ANT"));
            rsStation.Put("ERP_DBW", rsAntennas.GetD("T_PWR_DBW"));
            //rsStation.Put("", rsAntennas.GetD("T_PWR_DENS"));
            //rsStation.Put("", rsAntennas.GetS("T_ANT_DIR"));
            rsStation.Put("AZIMUTH", rsAntennas.GetD("T_AZM_MAX_E"));
            rsStation.Put("H_BEAMWIDTH", rsAntennas.GetD("T_BMWDTH"));
            rsStation.Put("GAIN", rsAntennas.GetD("T_GAIN_MAX"));
            rsStation.Put("RX_GAIN", rsAntennas.GetD("T_GAIN_MAX"));
            //rsStation.Put("", rsAntennas.GetS("T_GAIN_TYPE"));
            rsStation.Put("POLARIZATION", rsAntennas.GetS("T_POLAR"));
            rsStation.Put("ELEVATION", rsAntennas.GetD("T_ELEV"));
            rsStation.Put("AGL", rsAntennas.GetD("T_HGT_AGL"));                       
                    
        }
    }
}
