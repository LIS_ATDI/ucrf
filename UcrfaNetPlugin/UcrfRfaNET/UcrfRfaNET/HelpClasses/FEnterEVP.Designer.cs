﻿using GridCtrl;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FEnterEVP
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.grid = new GridCtrl.Grid();
         this.BtnOk = new System.Windows.Forms.Button();
         this.BtnExit = new System.Windows.Forms.Button();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.grid);
         this.groupBox1.Location = new System.Drawing.Point(12, 12);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(789, 154);
         this.groupBox1.TabIndex = 1;
         this.groupBox1.TabStop = false;
         // 
         // grid
         // 
         this.grid.AllowDrop = true;
         this.grid.BackColor = System.Drawing.SystemColors.Window;
         this.grid.EditMode = true;
         this.grid.FirstColumnWidth = 225;
         this.grid.GridLine = GridCtrl.Grid.GridLineStyle.Both;
         this.grid.GridLineColor = System.Drawing.Color.Black;
         this.grid.GridOpacity = 100;
         this.grid.Image = null;
         this.grid.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;
         this.grid.Location = new System.Drawing.Point(9, 15);
         this.grid.LockUpdates = false;
         this.grid.Name = "grid";
         this.grid.OtherColumnsWidth = 544;
         this.grid.ReadOnly = false;
         this.grid.SelectedCell = null;
         this.grid.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;
         this.grid.Size = new System.Drawing.Size(770, 127);
         this.grid.TabIndex = 0;
         this.grid.ToolTips = true;
         this.grid.OnDataValidate += new GridCtrl.Grid.DataValidateDelegate(this.grid_OnDataValidate);
         // 
         // BtnOk
         // 
         this.BtnOk.Location = new System.Drawing.Point(645, 173);
         this.BtnOk.Name = "BtnOk";
         this.BtnOk.Size = new System.Drawing.Size(75, 23);
         this.BtnOk.TabIndex = 1;
         this.BtnOk.Text = "ОK";
         this.BtnOk.UseVisualStyleBackColor = true;
         this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
         // 
         // BtnExit
         // 
         this.BtnExit.Location = new System.Drawing.Point(726, 173);
         this.BtnExit.Name = "BtnExit";
         this.BtnExit.Size = new System.Drawing.Size(75, 23);
         this.BtnExit.TabIndex = 0;
         this.BtnExit.Text = "Cancel";
         this.BtnExit.UseVisualStyleBackColor = true;
         this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
         // 
         // FEnterEVP
         // 
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
         this.ClientSize = new System.Drawing.Size(807, 205);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.BtnOk);
         this.Controls.Add(this.BtnExit);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
         this.Name = "FEnterEVP";
         this.ShowInTaskbar = false;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "Ввести коефіцієнт підсилення антенни";
         this.Shown += new System.EventHandler(this.FEnterEVP_Shown);
         this.groupBox1.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private Grid grid;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button BtnOk;
      private System.Windows.Forms.Button BtnExit;

   }
}