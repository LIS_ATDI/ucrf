﻿using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    internal class TableRights
    {
        private static readonly Dictionary<string, IMTableRight> TableRight;
        private static bool _isInit;
        /// <summary>
        /// Static constructor
        /// </summary>
        static TableRights()
        {
            TableRight = new Dictionary<string, IMTableRight>();
            _isInit = false;
        }
        /// <summary>
        /// Initialization
        /// </summary>
        public static void Init()
        {
            if (_isInit == false)
            {
                _isInit = true;
                IM.AdminDisconnect();
                string[] listTable = new[]
                                         {
                                             ICSMTbl.LICENCE,
                                             PlugTbl.APPL,
                                             PlugTbl.XvTaxPayerForm1
                                         };
                foreach (string table in listTable)
                    TableRight.Add(table, IM.TableRight(table));
            }
        }
        /// <summary>
        /// Return Right of table
        /// </summary>
        /// <param name="tableName">Name of table</param>
        /// <returns>Right of table</returns>
        public static IMTableRight GetRight(string tableName)
        {
            if (TableRight.ContainsKey(tableName))
                return TableRight[tableName];
            return new IMTableRight();
        }
        /// <summary>
        /// Check right
        /// </summary>
        /// <param name="tableName">Name of table</param>
        /// <param name="right">Right</param>
        /// <returns></returns>
        public static bool CheckRight(string tableName, IMTableRight right)
        {
            IMTableRight r = GetRight(tableName);
            return ((r & right) == right);
        }
    }
}
