﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   /// <summary>
   /// Базовая форма для всех форм
   /// </summary>
   public partial class FBaseForm : Form, INotifyPropertyChanged
   {
        protected const string FieldIsChanged = "IsChanged";
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get { return _isChanged;}
            set
            {
                if(_isChanged != value)
                {
                    _isChanged = value;
                    InvokeNotifyPropertyChanged(FieldIsChanged);
                }
            }
        }

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != FieldIsChanged)
                    IsChanged = true;
            }
        }
        #endregion
   
      private int _localId = 0;
      public int FormId { get { return _localId; } set { _localId = value; } }
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="id">ID of the record</param>
      public FBaseForm(int id)
      {
         InitializeComponent();
         _localId = id;
      }
      //===================================================
      /// <summary>
      /// Base constructor
      /// </summary>
      public FBaseForm() : this(-1){}

      protected override void OnKeyDown(KeyEventArgs e)
      {
          base.OnKeyDown(e);
          if (e.KeyCode == Keys.Escape)
          {// Выход
              Close();
          }
      }

      protected override void OnClosing(CancelEventArgs e)
      {
          e.Cancel = !ConfirmClose();
          base.OnClosing(e);
      }
      /// <summary>
      /// Подтвердить закрытие формы
      /// </summary>
      /// <returns>TRUE - Можна закрыть форму</returns>
      protected virtual bool ConfirmClose()
      {
          return true;
      }
      //===================================================
      /// <summary>
      /// Sets the title of the form
      /// </summary>
      /// <param name="title"></param>
      protected void SetTitle(string title)
      {
         string ttl = title;
         if ((_localId > 0) && (_localId != IM.NullI))
            ttl += string.Format(" ID:{0}", _localId);
         this.Text = ttl;
      }
   }
}
