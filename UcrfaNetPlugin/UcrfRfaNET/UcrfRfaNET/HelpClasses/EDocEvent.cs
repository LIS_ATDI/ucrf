﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   /// <summary>
   /// Типы событий
   /// </summary>
   public enum EDocEvent 
   {
      evUnknown,            //Неизвестное событие
      evCreatePacket, //Создание пакета
      evVISN,   //Висновок
      evDOZV,   //Дозвіл
      evPCR,   //Повідомлення про скасування рішення про видачу висновків
      evVVP,   //Відмова у наданні дозволу
      evDRV_DontUse,  //Не использовать //#4867 //Службова записка до ДРВ на рахунок  
      evVVE,   //Відмова у наданні висновку щодо ЕМС
      evLYST,   //Повідомлення про необхідність узгодження МК
      ev_0159,   //На узгодження з ГШ ЗС України
      evFYLIA,   //Запит до регіональної філії УДЦР або УРЧМ (коорд.+адреса)
      evPCR2,   //Залишення без розгляду (форма 2)
      evDRVVidh,   //Повідомлення про відхилення даних заявки (службової записки) на виставлення рахунку: тип події «–1»
      evDRVAccForm,   //Повідомлення про формування рахунку: тип події «2»
      evDRVVyst,   //Повідомлення про виставлення рахунку: тип події «3»
      evDRVAccPay4,   //Повідомлення про оплату рахунку: тип події «4»
      evDRVDocVyd,   //Повідомлення про видачу дозвільного документу: тип події «5»
      evDRVAppAnnul,   //Повідомлення про анулювання заявки на виставлення рахунку: тип події «–5»
      evDRVAccAnnul41,   //Повідомлення про анулювання рахунку (якщо не було руху по рахунку): тип події «–41»
      evDRVAccAnul42,   //Повідомлення про анулювання рахунку через несплату: тип події «–42»
      evDRVAddApp,   //Повідомлення про додавання заявки (службової записки) на виставлення рахунку: тип події «1»
      evSendToDRV,   //Отправлено в ДРВ
      evCreatePacketURCP, //Создание пакета отделом УРЧП
      evCreatePacketURZP, //Создание пакета отделом УРЗП
      evTEST_TV, //Погодження тестового включення (ТВ)
      evTEST_NV, //Погодження тестового включення (НВ)
      evTEST_PTK, //Погодження тестового включення (ПТК)
      evTEST_PTK_NV, //Погодження тестового включення (ПТК + НВ)
      evTEST_PCR2, //Погодження тестового включення + залишення без розгляду
      evLIST_FYLIA,//Лист до філії (перенаправлення заявочних документів)
      evNOTICE_PTK, //Попередження щодо визначення заявником строків ПТК
      evURCM_PTK,    //Службова записка до УРЧМ щодо їх участі в ПТК
      evURCM_DOC,    //Службова записка до УРЧП про передачу заявочних документів
      evAKT_PTK,     //АКТ ПТК
      evURCM_NOTICE, //Службова записка до УРЧМ
      evREESTR_VID, //Реестраційна відомість
      evDRVPrintDozv, //Печать разрешения в ДРВ
      evDRVPrintVisn, //Печать заключения в ДРВ
      evChangedState, //Изменился статус
      evSelectionFreqDoc,//Подбор частот
      evDrvUrzp,   //Службова записка до ДРВ на рахунок (УРЗП)
      evDrvUrcp,   //Службова записка до ДРВ на рахунок (УРЧП)
      evDocAnnulByNkrz,   //Анулювання дозволу рішенням НКРЗ
      evAKT_TE_PTK,   //АКТ ПТК інструментальної оцінки      
      evChangeProvinceMoni,// Изменение области мониторинга
      evCalculationEms,       // Расчет ЕМС
      evPARAM_REZ,     //АКТ ПТК вимірювання параметрів РЕЗ
      evDocCancellation, // Аннулювання дозволу     
      evSPS, // СПС
      evMOVE, // Дозвіл на рухому станцію
      evSERT,  // Гармонізований екзаменаційний сертифікат
      evAttachScan,  // Прикрипление скан-копии
      evFreqFitting, // Підбір частот (СРТ, РРЛ)
      evPrintCertificate, // Печать сертификата аматеров
      evBranchInvoice,  // заявка на выставление счёта из филиала
      evBranchPacket,  // заявка на выставление счёта из филиала
      evAttachDoc,     // вложение документа
      evIssuedDozv,    // видано Дозвіл (для группировки событий "Дозвіл на експлуатацію evDozv та Видано документ evDRVDocVyd")
      evIssuedVisn,     // видано Висновок (для группировки событий "Дозвіл на експлуатацію evVISN та Видано документ evDRVDocVyd")
      evIssuedDoc,     // видано документ
      evApplCallSign, //Лист-повідомлення про призначення позивних сигналів
      evSpecialDozv // Спеціальний дозвіл
   }
   //======================================================
   /// <summary>
   /// Класс конвертирует тип ApplType
   /// </summary>
   public static class EventConvert
   {
      //===================================================
      /// <summary>
      /// Возвращает человеческое название собитий
      /// </summary>
      /// <param name="eventType">Тип собития</param>
      /// <returns>Название собития</returns>
      public static string GetHumanNameOfEvent(EDocEvent eventType)
      {
         IMEriLOV eriData = new IMEriLOV("EventLog");
         List<string> freDescrArr = new List<string>();
         string freDescr = null;
         string freBmp = null;
         eriData.GetEntry(EDocEvent.evUnknown.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evCreatePacket.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evVISN.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDOZV.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evPCR.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evVVP.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDrvUrcp.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDrvUrzp.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evVVE.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evLYST.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.ev_0159.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evFYLIA.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evPCR2.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVVidh.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAccForm.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVVyst.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAccPay4.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVDocVyd.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAppAnnul.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAccAnnul41.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAccAnul42.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVAddApp.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evSendToDRV.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evCreatePacketURCP.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evCreatePacketURZP.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evTEST_TV.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evTEST_NV.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evTEST_PTK.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evTEST_PTK_NV.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evTEST_PCR2.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evLIST_FYLIA.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evNOTICE_PTK.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evURCM_PTK.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evURCM_DOC.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evAKT_PTK.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evURCM_NOTICE.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evREESTR_VID.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVPrintDozv.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDRVPrintVisn.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evChangedState.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evSelectionFreqDoc.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evDocAnnulByNkrz.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evAKT_TE_PTK.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evChangeProvinceMoni.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evCalculationEms.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evPARAM_REZ.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evSPS.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evMOVE.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evSERT.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evAttachScan.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evFreqFitting.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evPrintCertificate.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evBranchInvoice.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evBranchPacket.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evAttachDoc.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evIssuedDozv.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evIssuedVisn.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr=null; freBmp=null;
         eriData.GetEntry(EDocEvent.evIssuedDoc.ToString(), ref freDescr, ref freBmp); freDescrArr.Add(freDescr); freDescr = null; freBmp = null;
          

         Dictionary<EDocEvent, string> eventNames = new Dictionary<EDocEvent, string>();
         eventNames.Add(EDocEvent.evUnknown, freDescrArr[0]);
         eventNames.Add(EDocEvent.evCreatePacket, freDescrArr[1]);
         eventNames.Add(EDocEvent.evVISN, freDescrArr[2]);
         eventNames.Add(EDocEvent.evDOZV, freDescrArr[3]);
         eventNames.Add(EDocEvent.evPCR, freDescrArr[4]);
         eventNames.Add(EDocEvent.evVVP, freDescrArr[5]);
         eventNames.Add(EDocEvent.evDrvUrcp, freDescrArr[6]);
         eventNames.Add(EDocEvent.evDrvUrzp, freDescrArr[7]);
         eventNames.Add(EDocEvent.evVVE, freDescrArr[8]);
         eventNames.Add(EDocEvent.evLYST, freDescrArr[9]);
         eventNames.Add(EDocEvent.ev_0159, freDescrArr[10]);
         eventNames.Add(EDocEvent.evFYLIA, freDescrArr[11]);
         eventNames.Add(EDocEvent.evPCR2, freDescrArr[12]);
         eventNames.Add(EDocEvent.evDRVVidh, freDescrArr[13]);
         eventNames.Add(EDocEvent.evDRVAccForm, freDescrArr[14]);
         eventNames.Add(EDocEvent.evDRVVyst, freDescrArr[15]);
         eventNames.Add(EDocEvent.evDRVAccPay4, freDescrArr[16]);
         eventNames.Add(EDocEvent.evDRVDocVyd, freDescrArr[17]);
         eventNames.Add(EDocEvent.evDRVAppAnnul, freDescrArr[18]);
         eventNames.Add(EDocEvent.evDRVAccAnnul41, freDescrArr[19]);
         eventNames.Add(EDocEvent.evDRVAccAnul42, freDescrArr[20]);
         eventNames.Add(EDocEvent.evDRVAddApp, freDescrArr[21]);
         eventNames.Add(EDocEvent.evSendToDRV, freDescrArr[22]);
         eventNames.Add(EDocEvent.evCreatePacketURCP, freDescrArr[23]);
         eventNames.Add(EDocEvent.evCreatePacketURZP, freDescrArr[24]);
         eventNames.Add(EDocEvent.evTEST_TV,freDescrArr[25]);
         eventNames.Add(EDocEvent.evTEST_NV, freDescrArr[26]);
         eventNames.Add(EDocEvent.evTEST_PTK, freDescrArr[27]);
         eventNames.Add(EDocEvent.evTEST_PTK_NV, freDescrArr[28]);
         eventNames.Add(EDocEvent.evTEST_PCR2, freDescrArr[29]);
         eventNames.Add(EDocEvent.evLIST_FYLIA, freDescrArr[30]);
         eventNames.Add(EDocEvent.evNOTICE_PTK, freDescrArr[31]);
         eventNames.Add(EDocEvent.evURCM_PTK, freDescrArr[32]);
         eventNames.Add(EDocEvent.evURCM_DOC, freDescrArr[33]);
         eventNames.Add(EDocEvent.evAKT_PTK, freDescrArr[34]);
         eventNames.Add(EDocEvent.evURCM_NOTICE, freDescrArr[35]);
         eventNames.Add(EDocEvent.evREESTR_VID, freDescrArr[36]);
         eventNames.Add(EDocEvent.evDRVPrintDozv, freDescrArr[37]);
         eventNames.Add(EDocEvent.evDRVPrintVisn, freDescrArr[38]);
         eventNames.Add(EDocEvent.evChangedState, freDescrArr[39]);
         eventNames.Add(EDocEvent.evSelectionFreqDoc, freDescrArr[40]);
         eventNames.Add(EDocEvent.evDocAnnulByNkrz, freDescrArr[41]);
         eventNames.Add(EDocEvent.evAKT_TE_PTK, freDescrArr[42]);
         eventNames.Add(EDocEvent.evChangeProvinceMoni, freDescrArr[43]);
         eventNames.Add(EDocEvent.evCalculationEms, freDescrArr[44]);
         eventNames.Add(EDocEvent.evPARAM_REZ, freDescrArr[45]);
         eventNames.Add(EDocEvent.evSPS, freDescrArr[46]);
         eventNames.Add(EDocEvent.evMOVE, freDescrArr[47]);
         eventNames.Add(EDocEvent.evSERT, freDescrArr[48]);
         eventNames.Add(EDocEvent.evAttachScan, freDescrArr[49]);
         eventNames.Add(EDocEvent.evFreqFitting, freDescrArr[50]);
         eventNames.Add(EDocEvent.evPrintCertificate, freDescrArr[51]);
         eventNames.Add(EDocEvent.evBranchInvoice, freDescrArr[52]);
         eventNames.Add(EDocEvent.evBranchPacket, freDescrArr[53]);
         eventNames.Add(EDocEvent.evAttachDoc, freDescrArr[54]);
         eventNames.Add(EDocEvent.evIssuedDozv, freDescrArr[55]);
         eventNames.Add(EDocEvent.evIssuedVisn, freDescrArr[56]);
         eventNames.Add(EDocEvent.evIssuedDoc, freDescrArr[57]);
          
/*
         eventNames.Add(EDocEvent.evUnknown, CLocaliz.TxT("evUnknown"));
         eventNames.Add(EDocEvent.evCreatePacket, CLocaliz.TxT("evCreatePacket"));
         eventNames.Add(EDocEvent.evVISN, CLocaliz.TxT("evVISN"));
         eventNames.Add(EDocEvent.evDOZV, CLocaliz.TxT("evDOZV"));
         eventNames.Add(EDocEvent.evPCR, CLocaliz.TxT("evPCR"));
         eventNames.Add(EDocEvent.evVVP, CLocaliz.TxT("evVVP"));
         eventNames.Add(EDocEvent.evDrvUrcp, CLocaliz.TxT("evDrvUrcp"));
         eventNames.Add(EDocEvent.evDrvUrzp, CLocaliz.TxT("evDrvUrzp"));
         eventNames.Add(EDocEvent.evVVE, CLocaliz.TxT("evVVE"));
         eventNames.Add(EDocEvent.evLYST, CLocaliz.TxT("evLYST"));
         eventNames.Add(EDocEvent.ev_0159, CLocaliz.TxT("ev_0159"));
         eventNames.Add(EDocEvent.evFYLIA, CLocaliz.TxT("evFYLIA"));
         eventNames.Add(EDocEvent.evPCR2, CLocaliz.TxT("evPCR2"));
         eventNames.Add(EDocEvent.evDRVVidh, CLocaliz.TxT("evDRVVidh"));
         eventNames.Add(EDocEvent.evDRVAccForm, CLocaliz.TxT("evDRVAccForm"));
         eventNames.Add(EDocEvent.evDRVVyst, CLocaliz.TxT("evDRVVyst"));
         eventNames.Add(EDocEvent.evDRVAccPay4, CLocaliz.TxT("evDRVAccPay4"));
         eventNames.Add(EDocEvent.evDRVDocVyd, CLocaliz.TxT("evDRVDocVyd"));
         eventNames.Add(EDocEvent.evDRVAppAnnul, CLocaliz.TxT("evDRVAppAnnul"));
         eventNames.Add(EDocEvent.evDRVAccAnnul41, CLocaliz.TxT("evDRVAccAnnul41"));
         eventNames.Add(EDocEvent.evDRVAccAnul42, CLocaliz.TxT("evDRVAccAnul42"));
         eventNames.Add(EDocEvent.evDRVAddApp, CLocaliz.TxT("evDRVAddApp"));
         eventNames.Add(EDocEvent.evSendToDRV, CLocaliz.TxT("evSendToDRV"));
         eventNames.Add(EDocEvent.evCreatePacketURCP, CLocaliz.TxT("evCreatePacketURCP"));
         eventNames.Add(EDocEvent.evCreatePacketURZP, CLocaliz.TxT("evCreatePacketURZP"));
         eventNames.Add(EDocEvent.evTEST_TV, CLocaliz.TxT("evTEST_TV"));
         eventNames.Add(EDocEvent.evTEST_NV, CLocaliz.TxT("evTEST_NV"));
         eventNames.Add(EDocEvent.evTEST_PTK, CLocaliz.TxT("evTEST_PTK"));
         eventNames.Add(EDocEvent.evTEST_PTK_NV, CLocaliz.TxT("evTEST_PTK_NV"));
         eventNames.Add(EDocEvent.evTEST_PCR2, CLocaliz.TxT("evTEST_PCR2"));
         eventNames.Add(EDocEvent.evLIST_FYLIA, CLocaliz.TxT("evLIST_FYLIA"));
         eventNames.Add(EDocEvent.evNOTICE_PTK, CLocaliz.TxT("evNOTICE_PTK"));
         eventNames.Add(EDocEvent.evURCM_PTK, CLocaliz.TxT("evURCM_PTK"));
         eventNames.Add(EDocEvent.evURCM_DOC, CLocaliz.TxT("evURCM_DOC"));
         eventNames.Add(EDocEvent.evAKT_PTK, CLocaliz.TxT("evAKT_PTK"));
         eventNames.Add(EDocEvent.evURCM_NOTICE, CLocaliz.TxT("evURCM_NOTICE"));
         eventNames.Add(EDocEvent.evREESTR_VID, CLocaliz.TxT("evREESTR_VID"));
         eventNames.Add(EDocEvent.evDRVPrintDozv, CLocaliz.TxT("evDRVPrintDozv"));
         eventNames.Add(EDocEvent.evDRVPrintVisn, CLocaliz.TxT("evDRVPrintVisn"));
         eventNames.Add(EDocEvent.evChangedState, CLocaliz.TxT("evChangedState"));
         eventNames.Add(EDocEvent.evSelectionFreqDoc, CLocaliz.TxT("evSelectionFreqDoc"));
         eventNames.Add(EDocEvent.evDocAnnulByNkrz, CLocaliz.TxT("evDocAnnulByNkrz"));
         eventNames.Add(EDocEvent.evAKT_TE_PTK, CLocaliz.TxT("evAKT_TE_PTK"));
         eventNames.Add(EDocEvent.evChangeProvinceMonitor, CLocaliz.TxT("evChangeProvinceMonitor"));
         eventNames.Add(EDocEvent.evCalculationEms, CLocaliz.TxT("evCalculationEms"));
         eventNames.Add(EDocEvent.evPARAM_REZ, CLocaliz.TxT("evPARAM_REZ"));
         eventNames.Add(EDocEvent.evSPS, CLocaliz.TxT("evSPS"));
         eventNames.Add(EDocEvent.evMOVE, CLocaliz.TxT("evMOVE"));
         eventNames.Add(EDocEvent.evSERT, CLocaliz.TxT("evSERT"));
         eventNames.Add(EDocEvent.evAttachScan, CLocaliz.TxT("evAttachScan"));
         eventNames.Add(EDocEvent.evFreqFitting, CLocaliz.TxT("evFreqFitting"));
         eventNames.Add(EDocEvent.evPrintCertificate, CLocaliz.TxT("evPrintCertificate"));
         eventNames.Add(EDocEvent.evBranchInvoice, CLocaliz.TxT("evBranchInvoice"));
         eventNames.Add(EDocEvent.evBranchPacket, CLocaliz.TxT("evBranchPacket"));
         eventNames.Add(EDocEvent.evAttachDoc, CLocaliz.TxT("evAttachDoc"));
         eventNames.Add(EDocEvent.evIssuedDozv, CLocaliz.TxT("evIssuedDozv"));
         eventNames.Add(EDocEvent.evIssuedVisn, CLocaliz.TxT("evIssuedVisn"));
*/

         if (eventNames.ContainsKey(eventType))
            return CLocaliz.TxT(eventNames[eventType]);
         return "";
      }

      //===================================================
      /// <summary>
      /// Конвертирует тип EDocEvent в строку
      /// </summary>
      /// <param name="eventType">что конвертировать</param>
      /// <returns>сроковое представление типа</returns>
      public static string ToStr(EDocEvent eventType)
      {
         string retString = "";
         try
         {
            retString = Enum.Format(typeof(EDocEvent), eventType, "F");
         }
         catch { }
         return retString;
      }
      //===================================================
      public static EDocEvent ToEvent(string eventType)
      {
         EDocEvent retType = EDocEvent.evUnknown;
         try
         {
            retType = (EDocEvent)Enum.Parse(typeof(EDocEvent), eventType);
         }
         catch { }
         return retType;
      }
   }
}
