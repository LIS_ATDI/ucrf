﻿namespace XICSM.UcrfRfaNET
{
    partial class FormEditedPacket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxFormEditPacket = new System.Windows.Forms.GroupBox();
            this.chbPermitsIsEnabled = new System.Windows.Forms.CheckBox();
            this.chbConclusionIsEnabled = new System.Windows.Forms.CheckBox();
            this.DE_COUNT = new System.Windows.Forms.TextBox();
            this.EMC_COUNT = new System.Windows.Forms.TextBox();
            this.RFA_APPL_IN_PACKET = new System.Windows.Forms.TextBox();
            this.APPL_IN_PACKET = new System.Windows.Forms.TextBox();
            this.Region = new System.Windows.Forms.ComboBox();
            this.DRV_EMC_DATE = new System.Windows.Forms.DateTimePicker();
            this.DRV_DE_DATE = new System.Windows.Forms.DateTimePicker();
            this.lbl_DRV_DE_DATE = new System.Windows.Forms.Label();
            this.lbl_DE_COUNT = new System.Windows.Forms.Label();
            this.lbl_DRV_EMC_DATE = new System.Windows.Forms.Label();
            this.lbl_EMC_COUNT = new System.Windows.Forms.Label();
            this.lbl_RFA_APPL_IN_PACKET = new System.Windows.Forms.Label();
            this.lbl_APPL_IN_PACKET = new System.Windows.Forms.Label();
            this.LblRegion = new System.Windows.Forms.Label();
            this.groupBoxControl = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.Note = new System.Windows.Forms.Label();
            this.groupBoxFormEditPacket.SuspendLayout();
            this.groupBoxControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFormEditPacket
            // 
            this.groupBoxFormEditPacket.Controls.Add(this.Note);
            this.groupBoxFormEditPacket.Controls.Add(this.textBoxNote);
            this.groupBoxFormEditPacket.Controls.Add(this.chbPermitsIsEnabled);
            this.groupBoxFormEditPacket.Controls.Add(this.chbConclusionIsEnabled);
            this.groupBoxFormEditPacket.Controls.Add(this.DE_COUNT);
            this.groupBoxFormEditPacket.Controls.Add(this.EMC_COUNT);
            this.groupBoxFormEditPacket.Controls.Add(this.RFA_APPL_IN_PACKET);
            this.groupBoxFormEditPacket.Controls.Add(this.APPL_IN_PACKET);
            this.groupBoxFormEditPacket.Controls.Add(this.Region);
            this.groupBoxFormEditPacket.Controls.Add(this.DRV_EMC_DATE);
            this.groupBoxFormEditPacket.Controls.Add(this.DRV_DE_DATE);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_DRV_DE_DATE);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_DE_COUNT);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_DRV_EMC_DATE);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_EMC_COUNT);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_RFA_APPL_IN_PACKET);
            this.groupBoxFormEditPacket.Controls.Add(this.lbl_APPL_IN_PACKET);
            this.groupBoxFormEditPacket.Controls.Add(this.LblRegion);
            this.groupBoxFormEditPacket.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxFormEditPacket.Location = new System.Drawing.Point(0, 0);
            this.groupBoxFormEditPacket.Name = "groupBoxFormEditPacket";
            this.groupBoxFormEditPacket.Size = new System.Drawing.Size(474, 453);
            this.groupBoxFormEditPacket.TabIndex = 0;
            this.groupBoxFormEditPacket.TabStop = false;
            // 
            // chbPermitsIsEnabled
            // 
            this.chbPermitsIsEnabled.AutoSize = true;
            this.chbPermitsIsEnabled.Location = new System.Drawing.Point(25, 193);
            this.chbPermitsIsEnabled.Name = "chbPermitsIsEnabled";
            this.chbPermitsIsEnabled.Size = new System.Drawing.Size(111, 17);
            this.chbPermitsIsEnabled.TabIndex = 15;
            this.chbPermitsIsEnabled.Text = "Permits is enabled";
            this.chbPermitsIsEnabled.UseVisualStyleBackColor = true;
            this.chbPermitsIsEnabled.CheckedChanged += new System.EventHandler(this.chbPermitsIsEnabled_CheckedChanged);
            // 
            // chbConclusionIsEnabled
            // 
            this.chbConclusionIsEnabled.AutoSize = true;
            this.chbConclusionIsEnabled.Location = new System.Drawing.Point(25, 114);
            this.chbConclusionIsEnabled.Name = "chbConclusionIsEnabled";
            this.chbConclusionIsEnabled.Size = new System.Drawing.Size(129, 17);
            this.chbConclusionIsEnabled.TabIndex = 14;
            this.chbConclusionIsEnabled.Text = "Conclusion is enabled";
            this.chbConclusionIsEnabled.UseVisualStyleBackColor = true;
            this.chbConclusionIsEnabled.CheckedChanged += new System.EventHandler(this.chbConclusionIsEnabled_CheckedChanged);
            // 
            // DE_COUNT
            // 
            this.DE_COUNT.Location = new System.Drawing.Point(239, 219);
            this.DE_COUNT.Name = "DE_COUNT";
            this.DE_COUNT.Size = new System.Drawing.Size(217, 20);
            this.DE_COUNT.TabIndex = 13;
            // 
            // EMC_COUNT
            // 
            this.EMC_COUNT.Location = new System.Drawing.Point(242, 131);
            this.EMC_COUNT.Name = "EMC_COUNT";
            this.EMC_COUNT.Size = new System.Drawing.Size(217, 20);
            this.EMC_COUNT.TabIndex = 12;
            // 
            // RFA_APPL_IN_PACKET
            // 
            this.RFA_APPL_IN_PACKET.Location = new System.Drawing.Point(242, 87);
            this.RFA_APPL_IN_PACKET.Name = "RFA_APPL_IN_PACKET";
            this.RFA_APPL_IN_PACKET.Size = new System.Drawing.Size(217, 20);
            this.RFA_APPL_IN_PACKET.TabIndex = 11;
            // 
            // APPL_IN_PACKET
            // 
            this.APPL_IN_PACKET.Location = new System.Drawing.Point(242, 56);
            this.APPL_IN_PACKET.Name = "APPL_IN_PACKET";
            this.APPL_IN_PACKET.Size = new System.Drawing.Size(217, 20);
            this.APPL_IN_PACKET.TabIndex = 10;
            // 
            // Region
            // 
            this.Region.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Region.FormattingEnabled = true;
            this.Region.Location = new System.Drawing.Point(242, 25);
            this.Region.Name = "Region";
            this.Region.Size = new System.Drawing.Size(217, 21);
            this.Region.TabIndex = 9;
            // 
            // DRV_EMC_DATE
            // 
            this.DRV_EMC_DATE.Location = new System.Drawing.Point(242, 168);
            this.DRV_EMC_DATE.Name = "DRV_EMC_DATE";
            this.DRV_EMC_DATE.Size = new System.Drawing.Size(217, 20);
            this.DRV_EMC_DATE.TabIndex = 8;
            this.DRV_EMC_DATE.Enter += new System.EventHandler(this.DRV_EMC_DATE_Enter);
            // 
            // DRV_DE_DATE
            // 
            this.DRV_DE_DATE.Location = new System.Drawing.Point(239, 252);
            this.DRV_DE_DATE.Name = "DRV_DE_DATE";
            this.DRV_DE_DATE.Size = new System.Drawing.Size(217, 20);
            this.DRV_DE_DATE.TabIndex = 7;
            this.DRV_DE_DATE.Enter += new System.EventHandler(this.DRV_DE_DATE_Enter);
            // 
            // lbl_DRV_DE_DATE
            // 
            this.lbl_DRV_DE_DATE.AutoSize = true;
            this.lbl_DRV_DE_DATE.Location = new System.Drawing.Point(22, 258);
            this.lbl_DRV_DE_DATE.Name = "lbl_DRV_DE_DATE";
            this.lbl_DRV_DE_DATE.Size = new System.Drawing.Size(66, 13);
            this.lbl_DRV_DE_DATE.TabIndex = 6;
            this.lbl_DRV_DE_DATE.Text = "Date permits";
            // 
            // lbl_DE_COUNT
            // 
            this.lbl_DE_COUNT.AutoSize = true;
            this.lbl_DE_COUNT.Location = new System.Drawing.Point(22, 226);
            this.lbl_DE_COUNT.Name = "lbl_DE_COUNT";
            this.lbl_DE_COUNT.Size = new System.Drawing.Size(147, 13);
            this.lbl_DE_COUNT.TabIndex = 5;
            this.lbl_DE_COUNT.Text = "Number of permits for projects";
            // 
            // lbl_DRV_EMC_DATE
            // 
            this.lbl_DRV_EMC_DATE.AutoSize = true;
            this.lbl_DRV_EMC_DATE.Location = new System.Drawing.Point(22, 168);
            this.lbl_DRV_EMC_DATE.Name = "lbl_DRV_EMC_DATE";
            this.lbl_DRV_EMC_DATE.Size = new System.Drawing.Size(127, 13);
            this.lbl_DRV_EMC_DATE.TabIndex = 4;
            this.lbl_DRV_EMC_DATE.Text = "Date of conclusions EMC";
            // 
            // lbl_EMC_COUNT
            // 
            this.lbl_EMC_COUNT.AutoSize = true;
            this.lbl_EMC_COUNT.Location = new System.Drawing.Point(22, 138);
            this.lbl_EMC_COUNT.Name = "lbl_EMC_COUNT";
            this.lbl_EMC_COUNT.Size = new System.Drawing.Size(135, 13);
            this.lbl_EMC_COUNT.TabIndex = 3;
            this.lbl_EMC_COUNT.Text = "The number of conclusions";
            // 
            // lbl_RFA_APPL_IN_PACKET
            // 
            this.lbl_RFA_APPL_IN_PACKET.AutoSize = true;
            this.lbl_RFA_APPL_IN_PACKET.Location = new System.Drawing.Point(22, 87);
            this.lbl_RFA_APPL_IN_PACKET.Name = "lbl_RFA_APPL_IN_PACKET";
            this.lbl_RFA_APPL_IN_PACKET.Size = new System.Drawing.Size(138, 13);
            this.lbl_RFA_APPL_IN_PACKET.TabIndex = 2;
            this.lbl_RFA_APPL_IN_PACKET.Text = "Number RFI in the package";
            // 
            // lbl_APPL_IN_PACKET
            // 
            this.lbl_APPL_IN_PACKET.AutoSize = true;
            this.lbl_APPL_IN_PACKET.Location = new System.Drawing.Point(22, 56);
            this.lbl_APPL_IN_PACKET.Name = "lbl_APPL_IN_PACKET";
            this.lbl_APPL_IN_PACKET.Size = new System.Drawing.Size(209, 13);
            this.lbl_APPL_IN_PACKET.TabIndex = 1;
            this.lbl_APPL_IN_PACKET.Text = "The number of applications in the package";
            // 
            // LblRegion
            // 
            this.LblRegion.AutoSize = true;
            this.LblRegion.Location = new System.Drawing.Point(22, 28);
            this.LblRegion.Name = "LblRegion";
            this.LblRegion.Size = new System.Drawing.Size(41, 13);
            this.LblRegion.TabIndex = 0;
            this.LblRegion.Text = "Region";
            // 
            // groupBoxControl
            // 
            this.groupBoxControl.Controls.Add(this.btnCancel);
            this.groupBoxControl.Controls.Add(this.btnOk);
            this.groupBoxControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxControl.Location = new System.Drawing.Point(0, 453);
            this.groupBoxControl.Name = "groupBoxControl";
            this.groupBoxControl.Size = new System.Drawing.Size(474, 62);
            this.groupBoxControl.TabIndex = 1;
            this.groupBoxControl.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(355, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 19);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(118, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // textBoxNote
            // 
            this.textBoxNote.Location = new System.Drawing.Point(25, 314);
            this.textBoxNote.Multiline = true;
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.Size = new System.Drawing.Size(431, 133);
            this.textBoxNote.TabIndex = 16;
            // 
            // Note
            // 
            this.Note.AutoSize = true;
            this.Note.Location = new System.Drawing.Point(22, 298);
            this.Note.Name = "Note";
            this.Note.Size = new System.Drawing.Size(30, 13);
            this.Note.TabIndex = 17;
            this.Note.Text = "Note";
            // 
            // FormEditedPacket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(474, 535);
            this.Controls.Add(this.groupBoxControl);
            this.Controls.Add(this.groupBoxFormEditPacket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormEditedPacket";
            this.Text = "FormEditedPacket";
            this.groupBoxFormEditPacket.ResumeLayout(false);
            this.groupBoxFormEditPacket.PerformLayout();
            this.groupBoxControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFormEditPacket;
        private System.Windows.Forms.Label lbl_DRV_DE_DATE;
        private System.Windows.Forms.Label lbl_DE_COUNT;
        private System.Windows.Forms.Label lbl_DRV_EMC_DATE;
        private System.Windows.Forms.Label lbl_EMC_COUNT;
        private System.Windows.Forms.Label lbl_RFA_APPL_IN_PACKET;
        private System.Windows.Forms.Label lbl_APPL_IN_PACKET;
        private System.Windows.Forms.Label LblRegion;
        private System.Windows.Forms.TextBox DE_COUNT;
        private System.Windows.Forms.TextBox EMC_COUNT;
        private System.Windows.Forms.TextBox RFA_APPL_IN_PACKET;
        private System.Windows.Forms.TextBox APPL_IN_PACKET;
        private System.Windows.Forms.ComboBox Region;
        private System.Windows.Forms.DateTimePicker DRV_EMC_DATE;
        private System.Windows.Forms.DateTimePicker DRV_DE_DATE;
        private System.Windows.Forms.GroupBox groupBoxControl;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox chbPermitsIsEnabled;
        private System.Windows.Forms.CheckBox chbConclusionIsEnabled;
        private System.Windows.Forms.Label Note;
        private System.Windows.Forms.TextBox textBoxNote;
    }
}