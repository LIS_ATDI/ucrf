﻿namespace XICSM.UcrfRfaNET
{
   partial class FFindStation
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.components = new System.ComponentModel.Container();
            Lis.CheckBoxComboBox.CheckBoxProperties checkBoxProperties1 = new Lis.CheckBoxComboBox.CheckBoxProperties();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FFindStation));
            this.label1 = new System.Windows.Forms.Label();
            this.tbDozvil = new System.Windows.Forms.TextBox();
            this.tbVisn = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFreqFrom = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFreqTo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridAppl = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip_Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStrip_OpenDesigner = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.tbOwner = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chComboBox = new Lis.CheckBoxComboBox.CheckBoxComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAppl)).BeginInit();
            this.contextMenuStrip_Grid.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Дозвіл на експлуатацію";
            // 
            // tbDozvil
            // 
            this.tbDozvil.Location = new System.Drawing.Point(168, 30);
            this.tbDozvil.Name = "tbDozvil";
            this.tbDozvil.Size = new System.Drawing.Size(242, 20);
            this.tbDozvil.TabIndex = 1;
            // 
            // tbVisn
            // 
            this.tbVisn.Location = new System.Drawing.Point(168, 56);
            this.tbVisn.Name = "tbVisn";
            this.tbVisn.Size = new System.Drawing.Size(242, 20);
            this.tbVisn.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Висновок щодо ЕМС";
            // 
            // tbFreqFrom
            // 
            this.tbFreqFrom.Location = new System.Drawing.Point(168, 82);
            this.tbFreqFrom.Name = "tbFreqFrom";
            this.tbFreqFrom.Size = new System.Drawing.Size(110, 20);
            this.tbFreqFrom.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Частотний діапазон, МГц";
            // 
            // tbFreqTo
            // 
            this.tbFreqTo.Location = new System.Drawing.Point(297, 82);
            this.tbFreqTo.Name = "tbFreqTo";
            this.tbFreqTo.Size = new System.Drawing.Size(112, 20);
            this.tbFreqTo.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(459, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "-";
            // 
            // dataGridAppl
            // 
            this.dataGridAppl.AllowUserToAddRows = false;
            this.dataGridAppl.AllowUserToDeleteRows = false;
            this.dataGridAppl.AllowUserToOrderColumns = true;
            this.dataGridAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridAppl.BackgroundColor = System.Drawing.Color.White;
            this.dataGridAppl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAppl.ContextMenuStrip = this.contextMenuStrip_Grid;
            this.dataGridAppl.Location = new System.Drawing.Point(15, 186);
            this.dataGridAppl.Name = "dataGridAppl";
            this.dataGridAppl.ReadOnly = true;
            this.dataGridAppl.RowHeadersWidth = 4;
            this.dataGridAppl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridAppl.Size = new System.Drawing.Size(572, 219);
            this.dataGridAppl.TabIndex = 8;
            // 
            // contextMenuStrip_Grid
            // 
            this.contextMenuStrip_Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrip_OpenDesigner,
            this.resetSettingsToolStripMenuItem});
            this.contextMenuStrip_Grid.Name = "contextMenuStrip_Grid";
            this.contextMenuStrip_Grid.Size = new System.Drawing.Size(275, 70);
            this.contextMenuStrip_Grid.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Grid_Opening);
            // 
            // toolStrip_OpenDesigner
            // 
            this.toolStrip_OpenDesigner.Name = "toolStrip_OpenDesigner";
            this.toolStrip_OpenDesigner.Size = new System.Drawing.Size(274, 22);
            this.toolStrip_OpenDesigner.Text = "Панель управління переліком полів";
            this.toolStrip_OpenDesigner.Click += new System.EventHandler(this.toolStrip_OpenDesigner_Click);
            // 
            // resetSettingsToolStripMenuItem
            // 
            this.resetSettingsToolStripMenuItem.Name = "resetSettingsToolStripMenuItem";
            this.resetSettingsToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.resetSettingsToolStripMenuItem.Text = "Перелік полів за замовченням";
            this.resetSettingsToolStripMenuItem.Click += new System.EventHandler(this.resetSettingsToolStripMenuItem_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(512, 419);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(405, 419);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 10;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(96, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Область";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(168, 145);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(110, 23);
            this.buttonUpdate.TabIndex = 13;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // tbOwner
            // 
            this.tbOwner.Location = new System.Drawing.Point(168, 4);
            this.tbOwner.Name = "tbOwner";
            this.tbOwner.Size = new System.Drawing.Size(392, 20);
            this.tbOwner.TabIndex = 16;
            this.tbOwner.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbOwner_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(81, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Контрагент";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chComboBox);
            this.panel1.Controls.Add(this.tbOwner);
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tbFreqTo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tbFreqFrom);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tbVisn);
            this.panel1.Controls.Add(this.tbDozvil);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(603, 168);
            this.panel1.TabIndex = 18;
            // 
            // chComboBox
            // 
            checkBoxProperties1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chComboBox.CheckBoxProperties = checkBoxProperties1;
            this.chComboBox.DisplayMemberSingleItem = "";
            this.chComboBox.FormattingEnabled = true;
            this.chComboBox.Location = new System.Drawing.Point(168, 108);
            this.chComboBox.Name = "chComboBox";
            this.chComboBox.Size = new System.Drawing.Size(392, 21);
            this.chComboBox.TabIndex = 17;
            // 
            // FFindStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 453);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.dataGridAppl);
            this.Controls.Add(this.label4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 370);
            this.Name = "FFindStation";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FFindStation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FFindStation_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAppl)).EndInit();
            this.contextMenuStrip_Grid.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbDozvil;
      private System.Windows.Forms.TextBox tbVisn;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox tbFreqFrom;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbFreqTo;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.DataGridView dataGridAppl;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Button buttonUpdate;
      private System.Windows.Forms.TextBox tbOwner;
      private System.Windows.Forms.Label label6;
      private Lis.CheckBoxComboBox.CheckBoxComboBox chComboBox;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.ContextMenuStrip contextMenuStrip_Grid;
      private System.Windows.Forms.ToolStripMenuItem toolStrip_OpenDesigner;
      private System.Windows.Forms.ToolStripMenuItem resetSettingsToolStripMenuItem;
 


   }
}