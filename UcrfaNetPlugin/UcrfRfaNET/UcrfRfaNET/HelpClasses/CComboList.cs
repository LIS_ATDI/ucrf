﻿using System.Collections.Generic;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    internal class CComboList
    {
        private List<string> list = new List<string>();

        /// <summary>
        /// Adds value to the list. (prefix L = hint will be localized)
        /// </summary>
        /// <param name="shortName">short name (abriviatura)</param>
        /// <param name="hint">hint will be Localized</param>
        public void AddValueL(string shortName, string hint)
        {
            list.Add(shortName + " - " + CLocaliz.TxT(hint));
        }


        /// <summary>
        /// Adds range value to the list from ERI  will be Localized
        /// </summary>
        /// <param name="eriName">filename</param>        
        public void AddRangeValueDictL(string eriName)
        {
            list.Clear();
            Dictionary<string, string> tempDict = GetDictValuesFromEri(eriName);
            foreach (KeyValuePair<string, string> dict in tempDict)
                list.Add(dict.Key + " - " + CLocaliz.TxT(dict.Value));
        }


        /// <summary>
        /// Adds value to the list.
        /// </summary>
        /// <param name="shortName">short name (abriviatura)</param>
        /// <param name="hint">hint not will be Localized</param>
        public void AddValue(string shortName, string hint)
        {
            list.Add(shortName + " - " + hint);
        }

        /// <summary>
        /// Adds range value to the list from ERI not will be Localized
        /// </summary>
        /// <param name="eriName">filename</param>        
        public void AddRangeValueDict(string eriName)
        {
            list.Clear();
            Dictionary<string, string> tempDict = GetDictValuesFromEri(eriName);
            foreach (KeyValuePair<string, string> dict in tempDict)
                list.Add(dict.Key + " - " + dict.Value);
        }


        /// <summary>
        /// Adds value to the list.
        /// </summary>
        /// <param name="shortName">short name (abriviatura)</param>      
        public void AddValue(string shortName)
        {
            list.Add(shortName);
        }

        /// <summary>
        /// Adds value to the list from ERI.
        /// </summary>
        /// <param name="eriName">file name</param>      
        public void AddRangeValue(string eriName)
        {
            list.Clear();
            list = GetListValuesFromEri(eriName);
        }

        /// <summary>
        /// Gets list data
        /// </summary>
        /// <returns></returns>
        public List<string> GetList()
        {
            return list;
        }
        /// <summary>
        /// returns short value from fullValue
        /// </summary>
        /// <param name="fullValue">full text value of list</param>
        /// <returns>returns short value</returns>
        public string GetShortValue(string fullValue)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < fullValue.Length; i++)
            {
                if (fullValue[i] == ' ')
                    break;
                sb.Append(fullValue[i]);
            }

            return sb.ToString();
        }
        /// <summary>
        /// Return Dictionary code-description from Eri file
        /// </summary>
        /// <param name="eriFile">eri file</param>
        /// <returns>dictionary code-description</returns>
        public static Dictionary<string, string> GetDictValuesFromEri(string eriFile)
        {
            return EriFiles.GetEriCodeAndDescr(eriFile);
        }

        /// <summary>
        /// Return List code from Eri file
        /// </summary>
        /// <param name="eriFile">eri file</param>
        /// <returns>list code</returns>
        public static List<string> GetListValuesFromEri(string eriFile)
        {
            return EriFiles.GetEriCodeList(eriFile);
        }

    }
}
