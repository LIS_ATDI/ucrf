﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
	partial class FComments
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btOk = new System.Windows.Forms.Button();
			this.tbComments = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btOk
			// 
			this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btOk.Location = new System.Drawing.Point(125, 224);
			this.btOk.Name = "btOk";
			this.btOk.Size = new System.Drawing.Size(137, 32);
			this.btOk.TabIndex = 0;
			this.btOk.Text = "Додати коментар";
			this.btOk.UseVisualStyleBackColor = true;
			// 
			// tbComments
			// 
			this.tbComments.Location = new System.Drawing.Point(12, 12);
			this.tbComments.Multiline = true;
			this.tbComments.Name = "tbComments";
			this.tbComments.Size = new System.Drawing.Size(359, 206);
			this.tbComments.TabIndex = 1;
			// 
			// FComments
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(383, 261);
			this.Controls.Add(this.tbComments);
			this.Controls.Add(this.btOk);
			this.Name = "FComments";
			this.Text = "Введіть коментар";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btOk;
		private System.Windows.Forms.TextBox tbComments;
	}
}