﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    public static class PluginSetting
    {
        public enum TypeSeparator
        {
            OsSeparator,
            TabSeparator,
            UserSeparator
        }

        public static string ToDelimitedString(this List<string> value, string delimiter = ",")
        {
            string ExcludedUserTypeCodesVal = "";
            foreach (string val in value) { ExcludedUserTypeCodesVal += ((ExcludedUserTypeCodesVal.Length > 0 ? delimiter : "") + val); };
            return ExcludedUserTypeCodesVal;
        }

        public class PluginFolderSetting
        {
            //====================================================
            // Описание констант для данных
            private const string CstFolderSelectFreq = "PLG_FOLDER_SELECTION_FREQ";
            private const string CstFolderSelectFoto = "PLG_FOLDER_SELECTION_FOTO";
            private const string CstFolderCalculatedEmc = "PLG_FOLDER_CALCULATED_EMC";
            private const string CstFolderAktPtk = "PLG_FOLDER_AKT_PTK";
            private const string CstFolderFreqFitting = "PLG_FOLDER_FREQ_FITTING";
            private const string CstSqlSetArticle = "PLG_SQL_SET_ARTICLE";
            private const string CstMapsIniPath = "PLG_RSAGEO_MAPS_INI_PATH";
            private const string CstReliefPath = "PLG_RSAGEO_RELIEF_PATH";
            private const string CstMaxDaysDocumentRecognize = "PLG_MAX_DAYS_DOCUMENT_REC";
            private const string CstFolderScanDocument = "PLG_FOLDER_SCAN_DOCUMENT";
            private const string CstUserByDefault = "PLG_USER_BY_DEFAULT";
            private const string CstSqlCalcWorksAppl = "PLG_SQL_CALC_WORK_APPL";
            private const string CstSqlCalcWorksApplAndArticle = "PLG_SQL_CALC_WORK_APPL_ARTICLE";
            private const string CstTypeCsvSeparator = "PLG_TYPE_CSV_SEPARATOR";
            private const string CstUserSeparatorChar = "PLG_USER_CSV_SEPARATOR_CHAR";
            private const string CstBranchUserTypeCode = "XUCRF_BRANCH_USER_TYPE_CODE";
            private const string CstUseBranchSubfolders = "XUCRF_USE_BRANCH_SUBFOLDERS";
            private const string CstUseBranchCodesAsSubfolders = "XUCRF_USE_BRANCH_CODES_AS_SUBFOLDERS";
            private const string CstUserTypesToIgnore = "XUCRF_USER_TYPES_TO_IGNORE_IN_DOCS";
            private const string CstUsePtkFeaturesInUrcpDrvAppl = "XUCRF_USE_PTK_IN_URCP_APPL";
            private const string CstUseRegionFieldAsBranchAssign = "XUCRF_USE_REGION_AS_BRANCH_ASSIGN";
            
            /// <summary>
            /// Папка для файлов с подбором частот
            /// </summary>
            public static string FolderSelectionFreq { get; set; }
            /// <summary>
            /// Папка для файлов с подбором фото
            /// </summary>
            public static string FolderSelectionFoto { get; set; }
            /// <summary>
            /// Папка для файлов подсчета ЕМС
            /// </summary>
            public static string FolderCalculatedEmc { get; set; }
            /// <summary>
            /// Папка для файлов подсчета ЕМС
            /// </summary>
            public static string FolderAktPtk { get; set; }

            /// <summary>
            /// Папка для файлов подбора частот
            /// </summary>
            public static string FolderFreqFitting { get; set; }

            /// <summary>
            /// SQL запрос для установки статей
            /// </summary>
            public static string SqlSetArticle { get; set; }
            /// <summary>
            /// SQL запрос для подсчета кол-ва работ по заявке
            /// </summary>
            public static string SqlSetCalcWorksAppl { get; set; }
            /// <summary>
            /// SQL запрос для подсчета кол-ва работ по заявке и по статье
            /// </summary>
            public static string SqlSetCalcWorksApplAndArticle { get; set; }

            /// <summary>
            /// RSA Geography путь для maps.ini
            /// </summary>
            public static string RsaGeographyMapsIni { get; set; }

            /// <summary>
            /// RSA Geography путь для рельефа
            /// </summary>
            public static string RsaGeographyRelief { get; set; }

            /// <summary>
            /// Максимальное кол-во дней для выборки документов
            /// </summary>
            public static int MaxDaysDocumentRecognizer { get; set; }
            /// <summary>
            /// Папка для скан копий
            /// </summary>
            public static string FolderDocumentRecognizer { get; set; }

            /// <summary>
            /// Пользователь по умолчанию (если не можем определить текущего пользователя)
            /// </summary>
            public static string UserByDefault { get; set; }
            /// <summary>
            /// Тип разделителя CSV файлов
            /// </summary>
            public static TypeSeparator TypeSeparator { get; set; }
            /// <summary>
            /// Символ пользовательского разделителя CSV файлов
            /// </summary>
            public static string UserSeparator { get; set; }
            /// <summary>
            /// Pазделитель CSV файлов
            /// </summary>
            public static string Separator { get; protected set; }
            /// <summary>
            /// Используем поле 'Область' записи пользователя как признак того, что пользователь таки в соответствующем филиале
            /// </summary>
            public static bool UseRegionFieldAsBranchAssign { get; set; }
            /// <summary>
            /// Код "Тип сотрудника", используемый для обозначения принадлежности к филиалу
            /// </summary>
            public static string BranchUserTypeCode { get; set; }
            /// <summary>
            /// Использовать отдельные подпапки для филиалов
            /// </summary>
            public static bool UseBranchSubfolders { get; set; }
            /// <summary>
            /// Использовать полные названия регионов в филиальных подпапках 
            /// </summary>
            public static bool UseBranchCodesAsSubfolders { get; set; }
            /// <summary>
            /// Коды "Типа содтрудника", не используемые в конфигурировании путей сохранения документов
            /// </summary>
            public static List<string> UserTypeCodesToIgnore { get; set; }
           

            /// <summary>
            /// Использовать виды работ ПТК в общих заявках на счета за работы
            /// </summary>
            public static bool UsePtkFeaturesInUrcpDrvAppl { get; set; }

            static PluginFolderSetting()
            {
                Load();
            }
            /// <summary>
            /// Загружаем данные
            /// </summary>
            public static void Load()
            {
                FolderDocumentRecognizer = HelpFunction.ReadDataFromSysConfig(CstFolderScanDocument);
                FolderSelectionFreq = HelpFunction.ReadDataFromSysConfig(CstFolderSelectFreq);
                FolderSelectionFoto = HelpFunction.ReadDataFromSysConfig(CstFolderSelectFoto);
                FolderCalculatedEmc = HelpFunction.ReadDataFromSysConfig(CstFolderCalculatedEmc);
                FolderAktPtk = HelpFunction.ReadDataFromSysConfig(CstFolderAktPtk);
                FolderFreqFitting = HelpFunction.ReadDataFromSysConfig(CstFolderFreqFitting);
                SqlSetArticle = HelpFunction.ReadDataFromSysConfig(CstSqlSetArticle);
                SqlSetCalcWorksAppl = HelpFunction.ReadDataFromSysConfig(CstSqlCalcWorksAppl);
                SqlSetCalcWorksApplAndArticle = HelpFunction.ReadDataFromSysConfig(CstSqlCalcWorksApplAndArticle);                
                RsaGeographyMapsIni = HelpFunction.ReadDataFromSysConfig(CstMapsIniPath);
                RsaGeographyRelief = HelpFunction.ReadDataFromSysConfig(CstReliefPath);
                UserByDefault = HelpFunction.ReadDataFromSysConfig(CstUserByDefault);
                MaxDaysDocumentRecognizer = HelpFunction.ReadIntFromSysConfig(CstMaxDaysDocumentRecognize);
                if (MaxDaysDocumentRecognizer == ICSM.IM.NullI)
                    MaxDaysDocumentRecognizer = 30;
                if (MaxDaysDocumentRecognizer < 1)
                    MaxDaysDocumentRecognizer = 1;
                //Разделитель
                string tmp = HelpFunction.ReadDataFromSysConfig(CstTypeCsvSeparator);
                if (string.IsNullOrEmpty(tmp))
                    TypeSeparator = TypeSeparator.OsSeparator;
                else
                {
                    try
                    {
                        TypeSeparator = (TypeSeparator)Enum.Parse(typeof(TypeSeparator), tmp);
                    }
                    catch (Exception)
                    {
                        TypeSeparator = TypeSeparator.OsSeparator;
                    }
                }
                UserSeparator = HelpFunction.ReadDataFromSysConfig(CstUserSeparatorChar);
                UpdateSeparator();

                UseRegionFieldAsBranchAssign = HelpFunction.ReadBoolFromSysConfig(CstUseRegionFieldAsBranchAssign);
                BranchUserTypeCode = HelpFunction.ReadDataFromSysConfig(CstBranchUserTypeCode);
                UseBranchSubfolders = HelpFunction.ReadBoolFromSysConfig(CstUseBranchSubfolders);
                UseBranchCodesAsSubfolders = HelpFunction.ReadBoolFromSysConfig(CstUseBranchCodesAsSubfolders);
                UserTypeCodesToIgnore = new List<string>(HelpFunction.ReadDataFromSysConfig(CstUserTypesToIgnore).Split(new Char[] { ',', ';', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries));

                UsePtkFeaturesInUrcpDrvAppl = HelpFunction.ReadBoolFromSysConfig(CstUsePtkFeaturesInUrcpDrvAppl);
            }

            /// <summary>
            /// Сохраняем данные
            /// </summary>
            public static void Save()
            {
                HelpFunction.WriteDataToSysConfig(CstFolderScanDocument, FolderDocumentRecognizer);
                HelpFunction.WriteDataToSysConfig(CstFolderSelectFreq, FolderSelectionFreq);
                HelpFunction.WriteDataToSysConfig(CstFolderSelectFoto, FolderSelectionFoto);
                HelpFunction.WriteDataToSysConfig(CstFolderCalculatedEmc, FolderCalculatedEmc);
                HelpFunction.WriteDataToSysConfig(CstFolderAktPtk, FolderAktPtk);
                HelpFunction.WriteDataToSysConfig(CstFolderFreqFitting, FolderFreqFitting);                
                HelpFunction.WriteDataToSysConfig(CstSqlSetArticle, SqlSetArticle);
                HelpFunction.WriteDataToSysConfig(CstSqlCalcWorksAppl, SqlSetCalcWorksAppl);
                HelpFunction.WriteDataToSysConfig(CstSqlCalcWorksApplAndArticle, SqlSetCalcWorksApplAndArticle);
                HelpFunction.WriteDataToSysConfig(CstMapsIniPath, RsaGeographyMapsIni);
                HelpFunction.WriteDataToSysConfig(CstReliefPath, RsaGeographyRelief);
                HelpFunction.WriteDataToSysConfig(CstMaxDaysDocumentRecognize, MaxDaysDocumentRecognizer);
                HelpFunction.WriteDataToSysConfig(CstUserByDefault, UserByDefault);
                HelpFunction.WriteDataToSysConfig(CstTypeCsvSeparator, TypeSeparator.ToString());
                HelpFunction.WriteDataToSysConfig(CstUserSeparatorChar, UserSeparator);

                HelpFunction.WriteDataToSysConfig(CstUseRegionFieldAsBranchAssign, UseRegionFieldAsBranchAssign);
                HelpFunction.WriteDataToSysConfig(CstBranchUserTypeCode, BranchUserTypeCode);
                HelpFunction.WriteDataToSysConfig(CstUseBranchSubfolders, UseBranchSubfolders);
                HelpFunction.WriteDataToSysConfig(CstUseBranchCodesAsSubfolders, UseBranchCodesAsSubfolders);
                HelpFunction.WriteDataToSysConfig(CstUserTypesToIgnore, UserTypeCodesToIgnore.ToDelimitedString(","));
                
                HelpFunction.WriteDataToSysConfig(CstUsePtkFeaturesInUrcpDrvAppl, UsePtkFeaturesInUrcpDrvAppl);
                UpdateSeparator();
            }
            /// <summary>
            /// Обновление разделителя
            /// </summary>
            private static void UpdateSeparator()
            {
                switch (TypeSeparator)
                {
                    case TypeSeparator.OsSeparator:
                        Separator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                        break;
                    case TypeSeparator.TabSeparator:
                        Separator = "\t";
                        break;
                    case TypeSeparator.UserSeparator:
                        Separator = UserSeparator;
                        break;
                    default:
                        throw new Exception("Unknown TypeSeparator");
                }
                if (string.IsNullOrEmpty(Separator))
                    Separator = ";";
            }
        }
    }
}
