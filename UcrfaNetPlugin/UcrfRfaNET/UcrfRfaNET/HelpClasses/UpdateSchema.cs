﻿using ICSM;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET
{
    public class UpdateSchema
    {
        //=============================================================
        // hotfix 12.12.2013 
        // имя поля DIAGА таблицы XFA_ANTEN  содержит русскую букву А - исправлено
        // имя поля DIAGА представления XV_ANTEN содержит русскую букву А -исправлено
        //  размер поля ADDRESS представления ALLSTATIONS не соответствует размер поля в базе (4000) - исправлено
        //=============================================================
        // Описание типов ICSM
        private const string Standard = "Telsys";
        private const string FreqMHz = "F/MHz";
        private const string FreqHz = "F/Hz";
        private const string BwKHz = "BW/kHz";
        private const string DesigEm = "DesigEm";
        private const string MbPerSec = "MBitps";
        private const string dBm = "dBm";
        private const string dB = "dB";
        private const string dBW = "dBW";
        private const string W = "W";
        private const string dBWpHz = "dBW/Hz";
        private const string Number = "Number";
        private const string Unsigned = "Unsigned";
        private const string Longitude = "Longitude";
        private const string Latitude = "Latitude";
        private const string Date = "Date";
        private const string Distance = "Dist";
        private const string Metre = "m";
        private const string Degrees = "deg";
        private const string Asl = "ASML";
        private const string Agl = "AGL";
        private const string Hour = "Hour";
        private const string YesNo = "eri_YesNo";
        //=============================================================
        private const string plugin1 = "PLUGIN_1,97";
        private const string plugin2 = "PLUGIN_2,98";
        private const string plugin3 = "PLUGIN_3,99";
        private const string plugin3plus = "PLUGIN_3,99";
        private const string plugin4 = "PLUGIN_4,100";
        //=============================================================
        private const string RefAccessDbms = "DBMS";
        //=============================================================
        /// <summary>
        /// Регистрирует плагиновские таблицы в ICSM
        /// </summary>
        /// <param name="s">Схема ICSM</param>
        public static void RegisterSchema(IMSchema s)
        {
            //===============================================
            // TABLES
            //===============================================

            s.DeclareTable("XFA_BAND_EMI", CLocaliz.TxT("XFA_BAND_EMI"), plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("BANDMIN", "NUMBER(22,8)", null, null, null);
                s.DeclareField("BANDMAX", "NUMBER(22,8)", null, null, null);
                s.DeclareField("FREQ", "NUMBER(22,8)", null, null, null);
                s.DeclareField("OFFSET_PLUS", "NUMBER(22,8)", null, null, null);
                s.DeclareField("OFFSET_MINUS", "NUMBER(22,8)", null, null, null);
                s.DeclareField("EMI_TYPE", "VARCHAR(20)", null, null, null);
            }

            s.DeclareTable("XFA_EMI_TO_BAND", CLocaliz.TxT("XFA_EMI_TO_BAND"), plugin3);
            {
                s.DeclareField("EMI_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("BAND_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Link_to_band", "XFA_BAND_EMI", "", "BAND_ID", "ID");
            }

            s.DeclareTable("XFA_SITE_NIC", CLocaliz.TxT("XFA_SITE_NIC"), plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(25)", null, null, null);
                s.DeclareField("NIC", "NUMBER(1,0)", null, null, null);
            }


            // Вспомогательная таблица XNRFA_MICROWA_MON для зберігання та відображення результатів технічного радіоконтролю РЕЗ радіорелейного зв*язку
            s.DeclareTable("XNRFA_MICROWA_MON", "XNRFA_MICROWA_MON", plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("ALLSTAT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(25)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(1)", "eri_MicrowaMonStatus", null, null);
                s.DeclareField("PLAN", "VARCHAR(2)", "eri_Months", null, null);
                s.DeclareField("END_DATE", "DATE", null, null, null);
                s.DeclareField("NOTES", "VARCHAR(300)", null, null, null);
                s.DeclareField("LAST_EDIT_NAME", "VARCHAR(25)", null, null, null);
                s.DeclareField("LAST_EDIT_DATE", "DATE", null, null, null);
                s.DeclareField("APP_URCM_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PLAN_NEW", "DATE", null, null, null);
                s.DeclareField("PLAN_PAY", "DATE", null, null, null);
            }
            //
            s.DeclareTable("XNRFA_STREETS", "Зміна назви вулиць", plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_STREETS", "PRIMARY", "ID");
                s.DeclareField("STREET_TYPE", "VARCHAR(50)", null, null, null);
                s.DeclareField("STREET_NAME", "VARCHAR(150)", null, null, null);
                s.DeclareField("STREET_NAME_NEW", "VARCHAR(150)", null, null, null);
                s.DeclareField("TERRAIN_CODE", "VARCHAR(15)", null, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", null, null, null);
                s.DeclareField("DOC_REF", "VARCHAR(1000)", null, null, null);
                s.DeclareJoin("FG_XNRFA_STREETS", "CITIES", "DBMS", "TERRAIN_CODE", "CODE");
            }


            // Вспомогательная таблица XNRFA_DOCS для генерации формы №3 для налоговой
            s.DeclareTable("XNRFA_DOCS", "To create a report form №3 for the tax ", plugin3);
            {

                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_DOCS", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OWNER_NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("OWNER_CODE", "VARCHAR(20)", null, null, null);
                s.DeclareField("OWNER_ADDRESS", "VARCHAR(500)", null, null, null);
                s.DeclareField("OWNERPROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("LICENCE_ID", "VARCHAR(100)", null, null, null);
                s.DeclareField("LIC_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("TX_FREQ", "VARCHAR(500)", null, null, null);
                s.DeclareField("CHANEL", "VARCHAR(100)", null, null, null);
                s.DeclareField("LIC_DATE_FROM", "VARCHAR(100)", null, null, null);
                s.DeclareField("LIC_DATE_TO", "VARCHAR(100)", null, null, null);
                s.DeclareField("DOC_DATE_FROM", "DATE", null, null, null);
                s.DeclareField("DOC_DATE_TO", "DATE", null, null, null);
                s.DeclareField("DOC_DATE_CANCEL", "DATE", null, null, null);
                s.DeclareField("DOC_DATE_STOP", "DATE", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(500)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("FEES_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("FEES_TXT", "VARCHAR(500)", null, null, null);
                s.DeclareField("BW", "NUMBER(16,6)", null, null, null);
                s.DeclareField("POWER", "NUMBER(16,6)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(1000)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("CUSTOM_TXT1", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUSTOM_TXT2", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUSTOM_TXT3", "VARCHAR(500)", null, null, null);
                s.DeclareField("CUSTOM_DATE1", "DATE", null, null, null);
                s.DeclareField("CUSTOM_DATE2", "DATE", null, null, null);
                s.DeclareField("CUSTOM_DATE3", "DATE", null, null, null);
                s.DeclareField("LIC_COUNT", "NUMBER(12,0)", null, null, null);

                ////

                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareJoin("Licence", "LICENCE", "", "LICENCE_ID", "ID");
                s.DeclareJoin("Fees", "XNRFA_FEES", "", "FEES_ID", "ID");
                s.DeclareJoin("Appl", "XNRFA_APPL", null, "APPL_ID", "ID");
            }

            // Вспомогательная таблица XNRFA_LINKS_SITES для задания нескольких дополнительных параметров
            s.DeclareTable("XNRFA_LINKS_SITES", "New Params for table SITES", "SITES");
            {

                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_LINKS_SITES", "PRIMARY", "ID");
                s.DeclareField("EXT_ID_SITES", "NUMBER(9,0)", null, null, null);

                s.DeclareField("CUST_NBR1", "NUMBER(8,0)", null, null, null);
                s.DeclareField("CUST_NBR2", "NUMBER(8,0)", null, null, null);
                s.DeclareField("CUST_NBR3", "NUMBER(8,0)", null, null, null);
                s.DeclareField("CUST_NBR4", "NUMBER(8,0)", null, null, null);
                s.DeclareField("CUST_NBR5", "NUMBER(8,0)", null, null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT2", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT4", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT5", "VARCHAR(100)", null, null, null);
                s.DeclareJoin("FG_XNRFA_LINKS_SITE", "SITES", "DBMS", "EXT_ID_SITES", "ID");
            }
            // Вспомогательная таблица XNRFA_MON_CONTRACTS для задания нескольких дополнительных параметров
            s.DeclareTable("XNRFA_MON_CONTRACTS", "XNRFA Monitoring contracts", ICSMTbl.USERS);
            {

                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_MON_CONTRACTS", "PRIMARY", "ID");
                s.DeclareField("RN_AGENT", "VARCHAR(20)", null, null, null);
                s.DeclareField("RN_CONTRACTS", "NUMBER(17,0)", null, null, null);
                s.DeclareField("NUMB_CONTRACTS", "VARCHAR(40)", null, null, null);
                s.DeclareField("STATUS_CONTRACTS", "VARCHAR(1)", "eri_StatusMonContract", null, null);
                s.DeclareField("TYPE_CONTRACTS", "VARCHAR(1)", "eri_TypeContractsNew", null, null);
                s.DeclareField("DATE_FROM", "DATE", null, null, null);
                s.DeclareField("DATE_TO", "DATE", null, null, null);
                s.DeclareField("CODE_JURPERS", "VARCHAR(2)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("CREATED_DATE", "DATE", null, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", null, null, null);
                s.DeclareField("CLOSED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("CLOSED_DATE", "DATE", null, null, null);
                s.DeclareJoin("FG_XNRFA_LINKS_USERS", "USERS", null, "RN_AGENT", "CODE");
            }


            s.DeclareTable("XNRFA_IMPORT_ORDER", "XNRFA import orders", plugin4);
            {
                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("XNRFA_IMPORT_ORDER_PK", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PERM_NUM_ORDER", "VARCHAR(200)", null, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DESCRIPTION", "VARCHAR(20)", "eri_TImportMonOrder", null, null);
                s.DeclareField("PERM_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DATE_FROM", "DATE", null, null, null);
                s.DeclareField("DATE_TO", "DATE", null, null, null);
                s.DeclareField("DATE_CANCEL", "DATE", null, null, null);
                s.DeclareField("WORKS_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("WORKS_COUNT_DIFF", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
            }

            // Вспомогательная таблица XNRFA_MON_CONTRACTS для задания нескольких дополнительных параметров
            s.DeclareTable("XNRFA_DOCFILES", "XNRFA additional data attached doc", plugin3);
            {
                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("XNRFA_DOCFILES_PK", "PRIMARY", "ID");
                s.DeclareField("REF_ID", "NUMBER(12,0)", null, null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_IN_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN_VALUE", "DATE", null, null, null);
                s.DeclareField("ARMY_REZULT", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_REZULT_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT_VALUE", "VARCHAR(500)", null, null, null);
            }




            // Порты приписки
            s.DeclareTable("XFA_REGISTRY_PORT", "Port of registry", "EMPLOYEE");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_REGISTRY_PORT", "PRIMARY", "ID");
                s.DeclareField("NAME_UKR", "VARCHAR(100)", null, null, null);
                s.DeclareField("NAME_ENG", "VARCHAR(100)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(100)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(50)", null, "NOTNULL", null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
            }
            // Таблица тарифов
            s.DeclareTable("XNRFA_PRICE", "Prices", plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_PRICE", "PRIMARY", "ID");
                s.DeclareField("DEPARTMENT", "VARCHAR(5)", null, "NOTNULL", null);
                s.DeclareField("ARTICLE", "VARCHAR(40)", null, "NOTNULL", null);
                s.DeclareField("DESCR_WORK", "VARCHAR(200)", null, "NOTNULL", null);
                s.DeclareField("STANDARD", "VARCHAR(50)", null, null, null);
                s.DeclareJoin("RadioSystem", "RADIO_SYSTEMS", null, "STANDARD", "NAME");
                s.DeclareField("MEASURE", "VARCHAR(100)", null, null, null);
                s.DeclareField("PRICE", "NUMBER(9,3)", null, null, null);
                s.DeclareField("DESCRIPTION", "VARCHAR(1000)", null, null, null);
                s.DeclareField("STATUS", "NUMBER(1,0)", null, "NOTNULL", null);
                s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);
                s.DeclareField("DEFAULT", "NUMBER(1,0)", null, null, null);
                s.DeclareField("DEFAULTTYPE", "VARCHAR(15)", null, null, null);
                s.DeclareField("LIMITTYPE", "VARCHAR(15)", null, null, null);
                s.DeclareField("MIN", "NUMBER(9,3)", null, null, null);
                s.DeclareField("MAX", "NUMBER(9,3)", null, null, null);
                //----
                s.DeclareField("CREATED_BY", "VARCHAR(50)", null, "NOTNULL", null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);

                s.DeclareField("DATEOUTACTION", "DATE", "Date", null, null);
                s.DeclareField("DATESTARTACTION", "DATE", "Date", null, null);
            }
            // Параметры внешних фильтров
            s.DeclareTable("XNRFA_FILTER_PARAM", "Params of an external filter", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_XNRFA_FILTER_PARAM", "PRIMARY", "ID");
            s.DeclareField("EXT_FILT_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("DELTA_FREQ", "NUMBER(9,3)", null, "NOTNULL", null);
            s.DeclareJoin("ExternalFilters", "XNRFA_EXTERN_FILTERS", "CASCDEL", "EXT_FILT_ID", "ID");
            s.DeclareField("LOSS", "NUMBER(9,3)", null, "NOTNULL", null);

            // Внешний фильтр
            s.DeclareTable("XNRFA_EXTERN_FILTERS", "External filters", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_XNRFA_EXTERN_FILTERS", "PRIMARY", "ID");
            s.DeclareField("TYPE", "VARCHAR(50)", null, null, null);
            s.DeclareField("NAME", "VARCHAR(50)", null, null, null);
            s.DeclareField("MAX_POWER", "NUMBER(9,3)", null, null, null);
            s.DeclareField("FREQ", "NUMBER(9,3)", null, null, null);
            s.DeclareField("MAX_FREQ", "NUMBER(9,3)", null, null, null);
            s.DeclareField("MIN_FREQ", "NUMBER(9,3)", null, null, null);
            s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);
            s.DeclareField("CREATED_BY", "VARCHAR(50)", null, "NOTNULL", null);
            s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
            s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
            s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);

            // Table for writing change log
            //
            //    (УДАЛЕНА)
            //
            //s.DeclareTable("XRFA_CHANGE_LOG", "Table for writing change log", );
            //   s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("TABLE_NAME_ID", "VARCHAR(20)", null, "NOTNULL", null);            
            //   s.DeclareIndex("PK_XRFA_CHANGE_LOG", "PRIMARY", "TABLE_NAME_ID,ID");
            //   s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("CREATED_DATE", "DATE", null, "NOTNULL", null);
            //   s.DeclareField("FILE_LINK", "VARCHAR(20)", null, "NOTNULL", null);
            //   s.DeclareJoin("MobStation2", "MOB_STATION2", "", "ID,TABLE_NAME_ID", "ID,TABLE_NAME");
            //   s.DeclareJoin("EarthStation", "EARTH_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("Microwa", "MICROWA", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("MobStation", "MOB_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("TvStation", "TV_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("FmStation", "FM_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("DvbtStation", "DVBT_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");
            //   s.DeclareJoin("TdabStation", "TDAB_STATION", "", "ID,TABLE_NAME_ID", "TABLE_NAME,ID");

            // Таблица связи станции с внешними фильтрами
            s.DeclareTable("XNRFA_STAT_EXFLTR", "Connects the station with external filter ", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_STATION_EX_FILTER", "PRIMARY", "ID");
            s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
            s.DeclareField("EX_FILTER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("IS_TX", "NUMBER(1,0)", null, "NOTNULL", null);
            s.DeclareJoin("MobStation2", "MOB_STATION2", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("EarthStation", "EARTH_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("Microwa", "MICROWA", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("MobStation", "MOB_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("TvStation", "TV_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("FmStation", "FM_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("DvbtStation", "DVBT_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("TdabStation", "TDAB_STATION", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareJoin("ExternFilters", "XNRFA_EXTERN_FILTERS", "", "EX_FILTER_ID", "ID");
            //=========================================================
            //Таблицы для филий
            //=========================================================
            //Антена
            s.DeclareTable("XFA_ANTEN", "Antenns", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_ANTEN", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_ANTEN");
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("GAIN", "NUMBER(22,8)", null, null, null);
                s.DeclareField("H_BEAMWIDTH", "NUMBER(22,8)", null, null, null);
                s.DeclareField("V_BEAMWIDTH", "NUMBER(22,8)", null, null, null);
                //    s.DeclareField("DIAGА", "VARCHAR(200)", null, null, null); - hotfix 12.12.2013 - имя поля DIAGА содержит русскую букву А  
                s.DeclareField("DIAGA", "VARCHAR(200)", null, null, null);  // hotfix 12.12.2013 
                s.DeclareField("DIAGH", "VARCHAR(200)", null, null, null);
                s.DeclareField("DIAGV", "VARCHAR(200)", null, null, null);
            }

            //+ EQUIP
            s.DeclareTable("XFA_EQUIP", "Equipments", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_EQUIP", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_EQUIP");
                s.DeclareField("CODE", "VARCHAR(50)", null, null, null);
                s.DeclareField("MANUFACTURER", "VARCHAR(50)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("FAMILY", "VARCHAR(50)", null, null, null);
                s.DeclareField("TECH_IDNO", "VARCHAR(50)", null, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                s.DeclareField("MAX_POWER", "NUMBER(14,10)", dBm, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", null, null, null);
                s.DeclareField("RX_LOWER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("RX_UPPER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("LOWER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("UPPER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("CERTIFICATE_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERTIFICATE_DATE", "DATE", Date, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //=========================================================
            // POSITION
            s.DeclareTable("XFA_POSITION", "Decentralized Positions", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_POS", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_POSITION");
                s.DeclareField("ADMS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("AdmSite", "SITES", "DBMS", "ADMS_ID", "ID");
                s.DeclareField("NAME", "VARCHAR(50)", null, "NOTNULL", null);
                s.DeclareField("ADM_COD", "VARCHAR(10)", "lov_POS_ADMCOD", null, null);
                s.DeclareField("CODE", "VARCHAR(9)", null, null, null);
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", Longitude, null, null);
                s.DeclareField("LATITUDE", "NUMBER(10,6)", Latitude, null, null);
                s.DeclareField("X", "NUMBER(22,8)", null, null, null);
                s.DeclareField("Y", "NUMBER(22,8)", null, null, null);
                s.DeclareField("CSYS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ASL", "NUMBER(7,2)", Asl, null, null);
                s.DeclareField("COUNTRY_ID", "VARCHAR(3)", "eri_COUNTRY", null, null);
                s.DeclareField("DIST_BORDER", "NUMBER(9,3)", Distance, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(100)", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("City", "CITIES", "DBMS", "CITY_ID", "ID");
                s.DeclareField("POSTCODE", "VARCHAR(10)", null, null, null);
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", "lov_SUBPROVINCES", null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", "lov_PROVINCES", null, null);
                s.DeclareField("TEL", "VARCHAR(20)", null, null, null);
                s.DeclareField("FAX", "VARCHAR(20)", null, null, null);
                s.DeclareField("EMAIL", "VARCHAR(50)", null, null, null);
                s.DeclareField("REMARK", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TOWER", "VARCHAR(32)", "lov_STATION_TOWER", null, null);
                s.DeclareField("PLACE", "VARCHAR(300)", null, null, null);
                s.DeclareField("REF_CADASTRE", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT4", "VARCHAR(10)", null, null, null);
                s.DeclareField("CUST_TXT7", "VARCHAR(1000)", null, null, null);
                s.DeclareField("CUST_TXT8", "VARCHAR(50)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //=========================================================
            //+ NET
            s.DeclareTable("XFA_NET", "Net", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_NET", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_NET");
                s.DeclareField("NAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                s.DeclareField("NOTE", "VARCHAR(500)", null, null, null);
                s.DeclareField("AUTO_SET_ARTICLE", "NUMBER(9,0)", null, null, "1");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", "DBMS", "OWNER_ID", "ID");
                s.DeclareField("PRICE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Price", "XNRFA_PRICE", "DBMS", "PRICE_ID", "ID");
                s.DeclareField("WORKS_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("BaseStation", "XFA_REF_BASE_STA", "", "ID", "NET_ID");
                s.DeclareJoin("Abonent", "XFA_ABONENT_STATION", "", "ID", "NET_ID");
                s.DeclareField("IS_CONFIRMED", "NUMBER(1,0)", null, "NOTNULL", "0");
                s.DeclareField("CONFIRMED_BY", "VARCHAR(20)", null, null, null);
                s.DeclareJoin("ConfirmedUser", "EMPLOYEE", null, "CONFIRMED_BY", "APP_USER");
                s.DeclareField("CONFIRMED_DATE", "DATE", Date, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //=========================================================
            //+ REF_BASE_STATION
            s.DeclareTable("XFA_REF_BASE_STA", "Reference to base stations", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_REF_BASE_STA", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_REF_BASE_STA");
                s.DeclareField("NET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Net", "XFA_NET", "CASCDEL", "NET_ID", "ID");
                s.DeclareJoin("Abonent", "XFA_ABONENT_STATION", null, "NET_ID", "NET_ID");
                //s.DeclareField("STA_ID", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("STA_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
                s.DeclareField("STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATION_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
                s.DeclareJoin("BaseMobStation", "MOB_STATION", null, "STATION_ID,STATION_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseMobStation2", "MOB_STATION2", null, "STATION_ID,STATION_TABLE", "ID,TABLE_NAME");
            }
            //=========================================================
            //+ XFA_ABONENT_STATION
            s.DeclareTable("XFA_ABONENT_STATION", "Abonent station", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_ABONENT_STATION", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XFA_ABONENT_STATION");
                s.DeclareField("STANDARD", "VARCHAR(10)", "eri_TRFAApplStandard", null, null);
                //s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", "eri_TRFAStatus", null, null);
                s.DeclareField("ADM", "VARCHAR(3)", "eri_Adm", null, null);
                s.DeclareField("LOCATION", "VARCHAR(400)", "eri_XfaLocation", null, null);
                s.DeclareField("FACTORY_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", dBW, null, null);
                s.DeclareField("EQP_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Equipment", "XFA_EQUIP", "DBMS", "EQP_ID", "ID");
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XFA_POSITION", "DBMS", "POS_ID", "ID");
                s.DeclareField("NET_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Net", "XFA_NET", "DBMS", "NET_ID", "ID");
                s.DeclareField("ABONENT_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Abonent", "XNRFA_ABONENT_OWNER", "DBMS", "ABONENT_ID", "ID");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "DBMS", "USER_ID", "ID");

                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Antenna", "XFA_ANTEN", null, "ANT_ID", "ID");
                s.DeclareJoin("Freqs", "XFA_FREQ", null, "ID", "STA_ID");

                //s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                //s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");

                /*  s.DeclareField("PLAN_ID", "NUMBER(9,0)", null, null, null);
                  s.DeclareJoin("plan", "FREQ_PLAN", null, "PLAN_ID", "ID");

               */

                s.DeclareField("BIUSE_DATE", "DATE", Date, null, null);
                s.DeclareField("EOUSE_DATE", "DATE", Date, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", BwKHz, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(100)", null, null, null);
                s.DeclareField("USE_REGION", "VARCHAR(1000)", null, null, null);
                s.DeclareField("PERM_NUM", "VARCHAR(1000)", null, null, null);
                s.DeclareField("PERM_DATE_PRINT", "DATE", Date, null, null);
                s.DeclareField("BLANK_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("REGION_CODE", "NUMBER(5,0)", null, null, null);
                s.DeclareField("EXPL_TYPE", "VARCHAR(100)", "eri_TRFAExplType", null, null);
                s.DeclareField("FREQ_TX_TEXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("FREQ_RX_TEXT", "VARCHAR(2000)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);

                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("CALL", "VARCHAR(50)", null, null, null);
                s.DeclareField("TX_LOSSES", "NUMBER(6,2)", dB, null, null);
                s.DeclareField("AZIMUTH", "NUMBER(5,2)", Degrees, null, null);

                s.DeclareField("NUMB_EXIST_VISN", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_PRINT_DRV", "DATE", Date, null, null);
                s.DeclareField("DATE_PRINT_EXIST", "DATE", Date, null, null);
                s.DeclareField("NUMB_OLD_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_OLD_PRINT_DRV", "DATE", Date, null, null);
                s.DeclareField("DATE_OLD_PRINT_EX", "DATE", Date, null, null);
                s.DeclareField("SPEC_COND_DOZV", "VARCHAR(1000)", null, null, null); //Спец. условия дозвола
                s.DeclareField("SPEC_COND_VISN", "VARCHAR(1000)", null, null, null); //Спец. условия висновка
                s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);           //Примечания
                s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);
            }
            //=========================================================
            //+ FREQ
            s.DeclareTable("XFA_FREQ", "Frequencies", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_FREQ", "PRIMARY", "ID");
                s.DeclareField("STA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Station", "XFA_ABONENT_STATION", "CASCDEL", "STA_ID", "ID");
                s.DeclareField("TX_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("RX_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("TX_CHANNEL", "VARCHAR(100)", null, null, null);
                s.DeclareField("RX_CHANNEL", "VARCHAR(100)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            // Связь MOB_STATION2 в сектора
            //
            //     (УДАЛЕНA)
            //
            //s.DeclareTable("XRFA_MOB2_SECTOR", "Sectors for MOB_STATION2", );
            //   s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("MOB_STATION2_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("NUM", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareIndex("PK_XRFA_MOB2_SECTOR", "PRIMARY", "ID,MOB_STATION2_ID");
            //   s.DeclareJoin("MobStation2", "MOB_STATION2", "", "MOB_STATION2_ID", "ID");

            // Связь MOB_STATION2 в сектора
            //
            //     (УДАЛЕНA)
            //
            //s.DeclareTable("XRFA_MOB_SECTOR", "Sectors for MOB_STATION", );
            //   s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("MOB_STATION_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("NUM", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareIndex("PK_XRFA_MOB_SECTOR", "PRIMARY", "ID,MOB_STATION_ID");
            //   s.DeclareJoin("MobStation", "MOB_STATION", "", "MOB_STATION_ID", "ID");

            // Связь лицензий с станциями
            //
            //     (УДАЛЕНA) Создали новую таблицу XNRFA_APPL_LIC
            //
            //s.DeclareTable("XNRFA_STATION_LIC", "Licence for STATION", "PLUGIN_1,97");
            //   s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareIndex("PK_STATION_LIC", "PRIMARY", "ID");
            //   s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
            //   s.DeclareField("LICENCE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            //   s.DeclareJoin("MobStation2", "MOB_STATION2", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            //   s.DeclareJoin("EarthStation", "EARTH_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("Microwa", "MICROWA", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("MobStation", "MOB_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("TvStation", "TV_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("FmStation", "FM_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("DvbtStation", "DVBT_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("TdabStation", "TDAB_STATION", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("Application", "XNRFA_APPL", "", "OBJ_ID,OBJ_TABLE", "TABLE_NAME,ID");
            //   s.DeclareJoin("Licence", "LICENCE", "", "LICENCE_ID", "ID");

            // Связь лицензий с станциями
            s.DeclareTable("XNRFA_PACKET", "Applications packet", "PLUGIN_1,97");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_PACKET", "PRIMARY", "ID");
            s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
            s.DeclareField("TYPE", "VARCHAR(20)", "eri_TRFAApplType", "NOTNULL", null);
            s.DeclareField("CONTENT", "VARCHAR(20)", "eri_TRFAPackContent", "NOTNULL", null);
            s.DeclareField("NUMBER_IN", "VARCHAR(100)", null, "NOTNULL", null);
            s.DeclareField("NUMBER_OUT", "VARCHAR(100)", null, "NOTNULL", null);
            s.DeclareField("DATE_IN", "DATE", "Date", "NOTNULL", null);
            s.DeclareField("DATE_OUT", "DATE", "Date", "NOTNULL", null);
            s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", "NOTNULL", null);
            s.DeclareField("STATUS", "VARCHAR(20)", null, null, null);
            s.DeclareField("NUM_REESTR_VID", "NUMBER(9,0)", null, "NOTNULL", "1");
            s.DeclareField("STATUS_COORD", "VARCHAR(2)", null, null, null);
            s.DeclareField("CREATED_MANAGEMENT", "VARCHAR(50)", null, null, null);
            s.DeclareField("CREATED_BY", "VARCHAR(50)", null, "NOTNULL", null);
            s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
            s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
            s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
            //
            s.DeclareField("EMC_COUNT", "NUMBER(9,0)", null, null, null);
            s.DeclareField("DRV_EMC_DATE", "DATE", "Date", null, null);
            s.DeclareField("DE_COUNT", "NUMBER(9,0)", null, null, null);
            s.DeclareField("DRV_DE_DATE", "DATE", "Date", null, null);
            s.DeclareField("APPL_IN_PACKET", "NUMBER(9,0)", null, null, null);
            s.DeclareField("RFA_APPL_IN_PACKET", "NUMBER(9,0)", null, null, null);
            s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, null);

            s.DeclareField("REGION", "VARCHAR(50)", "eri_Area", null, null);

            //

            s.DeclareJoin("DRV_Join", "XNRFA_APPLPAY", "", "ID", "PACKET_ID");
            s.DeclareFieldExpr("STATE_EXPR", "(case when [DRV_Join.STATE]=66666 then 'Видалена' when [DRV_Join.STATE]=99999 then 'Збережена' when [DRV_Join.STATE]=0 then 'Відправлена в ДРВ' when [DRV_Join.STATE]=1 then 'Додана в ДРВ' when [DRV_Join.STATE]=2 then 'Сформовано рахунок' when [DRV_Join.STATE]=3 then 'Виставлено рахунок' when [DRV_Join.STATE]=4 then 'Сплачено рахунок' when [DRV_Join.STATE]=5 then 'Видано документ' when [DRV_Join.STATE]=-1 then 'Відхилення даних' when [DRV_Join.STATE]=-42 then 'Анульвано рахунок' else ' ' end)", "VARCHAR(50)", "String");


            ///XV_XNRFA_MICROWA_MON
            s.DeclareView("XV_XNRFA_MICROWA_MON", "View XNRFA_MICROWA_MON", "USERS");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("MICROWS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATUS_MON", "VARCHAR(1)", null, null, null);     
                s.DeclareField("PLAN_NEW", "DATE", "Date", null, null);
                s.DeclareField("END_DATE", "DATE", "Date", null, null);
                s.DeclareField("NOTES", "VARCHAR(300)", null, null, null);
                s.DeclareField("PLAN_PAY", "DATE", "Date", null, null);
                s.DeclareField("APP_URCM_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MICROWA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("WAS_USED", "NUMBER(1,0)", null, null, null);
                s.DeclareField("IS_CONFIRMED", "NUMBER(1,0)", null, null, null);
                s.DeclareField("PAY_OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", null, null, null);
            }
            /// XV_APPL_AR_3
            ///
            s.DeclareView("XV_APPL_AR_3", "View application AR_3", "USERS");
            {
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("LOCATION", "VARCHAR(400)", null, null, null);
                s.DeclareField("CALL", "VARCHAR(50)", null, null, null);
                s.DeclareField("FACTORY_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(100)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
            }

            /// XV_APPL_RS
            ///
            s.DeclareView("XV_APPL_R2", "View application R2", "USERS");
            {
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("FREQ", "NUMBER(10,5)", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("KOATUU", "VARCHAR(50)", null, null, null);
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
            }

            /// XV_APPL_RS
            ///
            s.DeclareView("XV_APPL_RS", "View application RS", "USERS");
            {

                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("STATUS_MK", "VARCHAR(4)", null, null, null);
                s.DeclareField("LICENCES", "VARCHAR(4000)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_TR_3
            ///
            s.DeclareView("XV_APPL_TR_3", "View application TR_3", "USERS");
            {
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CALL", "VARCHAR(50)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
            }

            /// XV_APPL_AP3
            ///
            s.DeclareView("XV_APPL_AP3", "View application AP3", "USERS");
            {
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(100)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO_NEW", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
            }

            /// XV_APPL_ZS
            ///
            s.DeclareView("XV_APPL_ZS", "View application ZS", "USERS");
            {
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("LICENCES", "VARCHAR(4000)", null, null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", null, null, null);
                s.DeclareField("EQUIP", "VARCHAR(50)", null, null, null);
                s.DeclareField("ANTENNA", "VARCHAR(12)", null, null, null);
                s.DeclareField("KOATUU", "VARCHAR(50)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);
                s.DeclareField("SAT_NAME", "VARCHAR(20)", null, null, null);
                s.DeclareField("LONG_NOM", "NUMBER(10,6)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_R2d
            ///
            s.DeclareView("XV_APPL_R2D", "View application R2d", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("BLOCK", "VARCHAR(3)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("KOATUU", "VARCHAR(50)", null, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }


            /// XV_APPL_TV2d
            ///
            s.DeclareView("XV_APPL_TV2D", "View application TV2d", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("CHANNEL", "VARCHAR(5)", null, null, null);
                s.DeclareField("PROGRAM", "VARCHAR(8)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_VP
            ///
            s.DeclareView("XV_APPL_VP", "View application VP", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_ZRS
            ///
            s.DeclareView("XV_APPL_ZRS", "View application ZRS", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("REG_PORT", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("SHIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("MMSI_NUM", "VARCHAR(9)", null, null, null);
                s.DeclareField("ATIS", "VARCHAR(10)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(20)", null, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_RR
            ///
            s.DeclareView("XV_APPL_RR", "View application RR", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CID", "VARCHAR(100)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_TR
            ///
            s.DeclareView("XV_APPL_TR", "View application TR", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("CLASS", "VARCHAR(2)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("DIST_BORDER", "NUMBER(9,3)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }


            /// XV_APPL_BS
            ///
            s.DeclareView("XV_APPL_BS", "View application BS", "USERS");
            {
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("LICENCE_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("DESIG_EM", "VARCHAR(9)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("ANT_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("KOATUU", "VARCHAR(50)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }


            /// XV_APPL_TV
            ///
            s.DeclareView("XV_APPL_TV", "View application TV", "USERS");
            {
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CHANNEL", "VARCHAR(5)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            }

            /// XV_APPL_A3
            ///
            s.DeclareView("XV_APPL_A3", "View application amateur", "USERS");
            {
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("TYPE_STAT", "VARCHAR(4)", "eri_XFA_TYPE_STAT", null, null);
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null);
                s.DeclareField("OWNER_NAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                s.DeclareField("CATEG_OPER", "VARCHAR(4)", null, null, null);
                s.DeclareField("APC", "VARCHAR(4)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("DOZV_DATE_PRINT", "DATE", Date, null, null);     //Дата печати
                s.DeclareField("DOZV_DATE_CANCEL", "DATE", Date, null, null);    //Дата анулирования
                s.DeclareField("DOZV_CREATED_BY", "VARCHAR(50)", null, null, null);//Пользователь создавший разрешение
                s.DeclareField("DOZV_FILE", "VARCHAR(3000)", null, null, null);  //Полный путь
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);    //
                s.DeclareField("DOZV_DATE_FROM_NEW", "DATE", Date, null, null);      //
                s.DeclareField("DOZV_DATE_TO_NEW", "DATE", Date, null, null);        //
                s.DeclareField("DOZV_CREATED_BY_NEW", "VARCHAR(50)", null, null, null);//
                s.DeclareField("DOZV_FILE_NEW", "VARCHAR(3000)", null, null, null);  //
                s.DeclareField("PROVINCE", "VARCHAR(50)", null, null, null);  //
                s.DeclareField("STATUS_COORD", "VARCHAR(2)", null, null, null);
            }

            // Таблица заявок
            s.DeclareTable("XNRFA_APPL", "Application", "PLUGIN_1,97");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_APPL", "PRIMARY", "ID");
                // Поле удалено
                //s.DeclareField("PACKET_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                //s.DeclareJoin("Packet", "XNRFA_PACKET", "CASCDEL", "PACKET_ID", "ID");
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
                s.DeclareField("PAY_OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("PayOwner", "USERS", "", "PAY_OWNER_ID", "ID");
                s.DeclareField("CREATE_DATE", "DATE", null, "NOTNULL", null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT2", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT4", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT5", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT6", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT7", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT8", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT9", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT10", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT11", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT12", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT13", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT14", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT15", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT16", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT17", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT18", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT19", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT20", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CUST_TXT21", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT22", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT23", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_DAT1", "DATE", null, null, null);
                s.DeclareField("CUST_DAT2", "DATE", null, null, null);
                s.DeclareField("CUST_DAT3", "DATE", null, null, null);
                s.DeclareField("CUST_DAT4", "DATE", null, null, null);
                s.DeclareField("CUST_DAT5", "DATE", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID3", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID4", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID5", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID6", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATE_DOC", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
                s.DeclareJoin("AmateurStation", "XFA_AMATEUR", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("EarthStation", "EARTH_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave", "MICROWA", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave2", "MICROWA", "", "OBJ_ID2,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave3", "MICROWA", "", "OBJ_ID3,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave4", "MICROWA", "", "OBJ_ID4,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave5", "MICROWA", "", "OBJ_ID5,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Microwave6", "MICROWA", "", "OBJ_ID6,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("TvStation", "TV_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("FmStation", "FM_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("DvbtStation", "DVBT_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("TdabStation", "TDAB_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector1", "MOB_STATION2", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector2", "MOB_STATION2", "", "OBJ_ID2,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector3", "MOB_STATION2", "", "OBJ_ID3,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector4", "MOB_STATION2", "", "OBJ_ID4,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector5", "MOB_STATION2", "", "OBJ_ID5,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("BaseSector6", "MOB_STATION2", "", "OBJ_ID6,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector1", "MOB_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector2", "MOB_STATION", "", "OBJ_ID2,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector3", "MOB_STATION", "", "OBJ_ID3,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector4", "MOB_STATION", "", "OBJ_ID4,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector5", "MOB_STATION", "", "OBJ_ID5,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MobileSector6", "MOB_STATION", "", "OBJ_ID6,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("NetSector1", "XFA_REF_BASE_STA", "", "OBJ_ID1,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("NetSector2", "XFA_REF_BASE_STA", "", "OBJ_ID2,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("NetSector3", "XFA_REF_BASE_STA", "", "OBJ_ID3,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("NetSector4", "XFA_REF_BASE_STA", "", "OBJ_ID4,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("NetSector5", "XFA_REF_BASE_STA", "", "OBJ_ID5,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("NetSector6", "XFA_REF_BASE_STA", "", "OBJ_ID6,OBJ_TABLE", "STATION_ID,STATION_TABLE");
                s.DeclareJoin("GSM", "GSM", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Abonent", "XFA_ABONENT_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Railway1", "XFA_ABONENT_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Railway2", "XFA_ABONENT_STATION", "", "OBJ_ID2,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Ship", "SHIP", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Emitter", "XFA_EMI_STATION", "", "OBJ_ID1,OBJ_TABLE", "ID,TABLE_NAME");
                s.DeclareField("PRICE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Price", "XNRFA_PRICE", "", "PRICE_ID", "ID");
                s.DeclareField("PRICE_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Price2", "XNRFA_PRICE", "", "PRICE_ID2", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XNRFA_APPL");
                s.DeclareField("TRSMTRS_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATUS_COORD", "VARCHAR(2)", null, null, null);
                //------
                // Сергей: Удалены
                //s.DeclareField("AGREEMENT", "VARCHAR(100)", null, null, null);
                //s.DeclareField("MEMOTODRV", "VARCHAR(100)", null, null, null);
                //s.DeclareField("AGREEMENT_PTK", "VARCHAR(100)", null, null, null);
                //s.DeclareField("MEMOTODRV_PTK", "VARCHAR(100)", null, null, null);
                //s.DeclareField("STATE_DOC_PTK", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("PTKDATE", "DATE", null, null, null);
                //------
                s.DeclareField("WAS_USED", "NUMBER(1,0)", null, null, "0");
                s.DeclareField("WORKS_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("WORKS_COUNT2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("IS_CONFIRMED", "NUMBER(1,0)", null, "NOTNULL", "0");
                s.DeclareField("CONFIRMED_BY", "VARCHAR(20)", null, null, null);
                s.DeclareField("CONFIRMED_DATE", "DATE", Date, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);
                // Для изменения области мониторинга
                s.DeclareField("CHANGE_FILE_POS", "VARCHAR(2000)", null, null, null);
                s.DeclareField("CHANGE_DOC_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("CHANGE_PROVINCE", "VARCHAR(100)", null, null, null);
                s.DeclareField("CHANGE_DOC_DATE", "DATE", Date, null, null);
                // Поля для заключения (высновков)
                s.DeclareField("CONC_NUM_NEW", "VARCHAR(200)", null, null, null);    //
                s.DeclareField("CONC_DATE_FROM_NEW", "DATE", Date, null, null);      //
                s.DeclareField("CONC_DATE_TO_NEW", "DATE", Date, null, null);        //
                s.DeclareField("CONC_FILE_NEW", "VARCHAR(3000)", null, null, null);  //
                s.DeclareField("CONC_CREATED_BY_NEW", "VARCHAR(50)", null, null, null);//
                //----
                s.DeclareField("CONC_NUM", "VARCHAR(200)", null, null, null);    //Номер заключения
                s.DeclareField("CONC_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("CONC_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("CONC_DATE_PRINT", "DATE", Date, null, null);     //Дата печати
                s.DeclareField("CONC_FILE", "VARCHAR(3000)", null, null, null);  //Полный путь
                s.DeclareField("CONC_CREATED_BY", "VARCHAR(50)", null, null, null);//Пользователь создавший заключение
                //----
                s.DeclareField("CONC_NUM_OLD", "VARCHAR(200)", null, null, null);    //
                s.DeclareField("CONC_DATE_FROM_OLD", "DATE", Date, null, null);      //
                s.DeclareField("CONC_DATE_TO_OLD", "DATE", Date, null, null);        //
                s.DeclareField("CONC_DATE_PRINT_OLD", "DATE", Date, null, null);     //
                s.DeclareField("CONC_CREATED_BY_OLD", "VARCHAR(50)", null, null, null);//
                s.DeclareField("CONC_FILE_OLD", "VARCHAR(3000)", null, null, null);  //
                // Поля для разрешений
                s.DeclareField("DOZV_NUM_NEW", "VARCHAR(200)", null, null, null);    //
                s.DeclareField("DOZV_DATE_FROM_NEW", "DATE", Date, null, null);      //
                s.DeclareField("DOZV_DATE_TO_NEW", "DATE", Date, null, null);        //
                s.DeclareField("DOZV_CREATED_BY_NEW", "VARCHAR(50)", null, null, null);//
                s.DeclareField("DOZV_FILE_NEW", "VARCHAR(3000)", null, null, null);  //
                //----
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("DOZV_DATE_PRINT", "DATE", Date, null, null);     //Дата печати
                s.DeclareField("DOZV_DATE_CANCEL", "DATE", Date, null, null);    //Дата анулирования
                s.DeclareField("DOZV_CREATED_BY", "VARCHAR(50)", null, null, null);//Пользователь создавший разрешение
                s.DeclareField("DOZV_FILE", "VARCHAR(3000)", null, null, null);  //Полный путь
                //----
                s.DeclareField("DOZV_NUM_OLD", "VARCHAR(200)", null, null, null);    //
                s.DeclareField("DOZV_DATE_FROM_OLD", "DATE", Date, null, null);      //
                s.DeclareField("DOZV_DATE_TO_OLD", "DATE", Date, null, null);        //
                s.DeclareField("DOZV_DATE_PRINT_OLD", "DATE", Date, null, null);     //
                s.DeclareField("DOZV_DATE_CANCEL_OLD", "DATE", Date, null, null);    //
                s.DeclareField("DOZV_CREATED_BY_OLD", "VARCHAR(50)", null, null, null);//
                s.DeclareField("DOZV_FILE_OLD", "VARCHAR(3000)", null, null, null);  //
                //===========
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", "OLD_USER");
                s.DeclareField("APPL_TYPE", "VARCHAR(20)", "eri_TRFAApplType", "NOTNULL", null);
            }

            //Table for connecting XNRFA_PACKET with XNRFA_APPL
            s.DeclareTable("XNRFA_PAC_TO_APPL", "Connecting PACKET with APPL", "PLUGIN_2,98");
            {
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("PACKET_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_PACTOAPPL", "PRIMARY", "APPL_ID,PACKET_ID");
                s.DeclareJoin("Application", "XNRFA_APPL", "CASCDEL", "APPL_ID", "ID");
                s.DeclareJoin("Packet", "XNRFA_PACKET", "CASCDEL", "PACKET_ID", "ID");
                //
                s.DeclareField("PTK_STATUS", "VARCHAR(4)", "eri_TRFAPtkStatus", null, null);
                s.DeclareField("DOC_NUM_TV", "VARCHAR(100)", null, null, null);
                s.DeclareField("DOC_DATE", "DATE", null, null, null);
                s.DeclareField("DOC_END_DATE", "DATE", null, null, null);
                s.DeclareField("PTK_DATE_FROM", "DATE", null, null, null);
                s.DeclareField("PTK_DATE_TO", "DATE", null, null, null);
                s.DeclareField("DOC_NUM_URCM", "VARCHAR(100)", null, null, null);
                s.DeclareField("DOC_URCM_DATE", "DATE", null, null, null);
                s.DeclareField("IS_TV", "NUMBER(1,0)", null, null, "0");
                s.DeclareField("IS_NV", "NUMBER(1,0)", null, null, "0");
                s.DeclareField("REMARK", "VARCHAR(300)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                //
                s.DeclareField("CREATED_DATE", "DATE", null, null, null);
            }

            // Table for writing change log
            s.DeclareTable("XNRFA_CHANGE_LOG", "Table for writing change log", "PLUGIN_1,97");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_CHANGE_LOG", "PRIMARY", "ID");
            s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XNRFA_CHANGE_LOG");
            s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
            s.DeclareJoin("Application", "XNRFA_APPL", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("PATH", "VARCHAR(500)", null, "NOTNULL", null);
            s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
            s.DeclareField("CREATED_DATE", "DATE", null, "NOTNULL", null);

            // Table for patterns of special condition
            s.DeclareTable("XNRFA_SCPATTERN", "Patterns of special condition", "PLUGIN_3,99");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_SCPATTERN", "PRIMARY", "ID");
            s.DeclareField("DEPARTMENT", "VARCHAR(5)", null, "NOTNULL", null);
            s.DeclareField("SECTOR", "VARCHAR(10)", null, "NOTNULL", null);
            s.DeclareField("TYPE", "VARCHAR(10)", null, "NOTNULL", null);
            s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
            s.DeclareField("TEXT", "VARCHAR(3000)", null, null, null);

            // Table for events log
            s.DeclareTable("XNRFA_EVENT_LOG", "Application event log", "PLUGIN_1,97");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_EVENT_LOG", "PRIMARY", "ID");
            s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
            s.DeclareJoin("Application", "XNRFA_APPL", "", "OBJ_ID,OBJ_TABLE", "ID,TABLE_NAME");
            s.DeclareField("EVENT", "VARCHAR(20)", "eri_EventLog", null, null);
            s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
            s.DeclareField("DOC_DATE", "DATE", null, null, null);
            s.DeclareField("DOC_NUMBER", "VARCHAR(100)", null, null, null);
            s.DeclareField("DOC_END_DATE", "DATE", null, null, null);
            s.DeclareField("PATH", "VARCHAR(500)", null, null, null);
            s.DeclareField("VISIBLE", "NUMBER(1,0)", null, "NOTNULL", "1");
            s.DeclareField("CREATED_DATE", "DATE", null, "NOTNULL", null);

            // Table of the global GUIDs for the global DB
            s.DeclareTable("XNRFA_GUID", "GUID for the global DB", "PLUGIN_3,99");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_GUID", "PRIMARY", "ID");
            s.DeclareField("GUID_ID", "NUMBER(15,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);

            // Связь лицензий с заявкой
            s.DeclareTable("XNRFA_APPL_LIC", "Licence for STATION", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_APPL_LIC", "PRIMARY", "ID");
            s.DeclareField("APPL_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Application", "XNRFA_APPL", "CASCDEL", "APPL_ID", "ID");
            s.DeclareField("LIC_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Licence", "LICENCE", "", "LIC_ID", "ID");

            // Таблица заявок в ДРВ
            s.DeclareTable("XNRFA_APPLPAY", "Applications for DRV", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_APPLPAY", "PRIMARY", "ID");
            s.DeclareField("PACKET_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Packet", "XNRFA_PACKET", "CASCDEL", "PACKET_ID", "ID");
            //s.DeclareField("STATE", "NUMBER(9,0)", "eri_ApplDrvState", "NOTNULL", null);
            s.DeclareField("STATE", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("CUST_TXT1", "VARCHAR(1000)", null, null, null);
            s.DeclareField("CUST_TXT2", "VARCHAR(1000)", null, null, null);
            s.DeclareField("CUST_TXT3", "VARCHAR(1000)", null, null, null);
            s.DeclareField("DOC_DATE", "DATE", null, null, null);
            s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
            s.DeclareField("DEPARTMENT", "VARCHAR(5)", null, null, null);
            s.DeclareField("INVOICE", "VARCHAR(50)", null, null, null);
            s.DeclareField("INVOICE_FROM", "DATE", null, null, null);
            s.DeclareField("INVOICE_TO", "DATE", null, null, null);
            s.DeclareField("INV_EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);

            // Таблица работ в заявках в ДРВ
            s.DeclareTable("XNRFA_APPLWORKS", "Works in applications for DRV", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_APPLWORKS", "PRIMARY", "ID");
            s.DeclareField("APPLPAY_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("ApplicationPay", "XNRFA_APPLPAY", "CASCDEL", "APPLPAY_ID", "ID");
            s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
            s.DeclareField("WORKTYPE", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("ARTICLE", "VARCHAR(20)", null, null, null);
            s.DeclareField("COUNT", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("NOTES", "VARCHAR(500)", null, null, null);
            s.DeclareField("COEFFICIENT", "NUMBER(9,2)", null, null, null);
            s.DeclareField("LINKNUM", "NUMBER(1,0)", null, null, null);
            s.DeclareField("ADDITIONAL_NOTES", "VARCHAR(500)", null, null, null);

            // Список выбраных заявок станций для заявки в ДРВ
            s.DeclareTable("XNRFA_CHOSENAPPLS", "Chosen applications for DRV application", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_CHOSENAPPLS", "PRIMARY", "ID");
            s.DeclareField("APPLPAY_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("ApplicationPay", "XNRFA_APPLPAY", "CASCDEL", "APPLPAY_ID", "ID");
            s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");

            //TODO После добавления абонентов, необходимо удалить
            // Таблица связи абонентских РЕЗ с базовыми
            s.DeclareTable("XNRFA_ABONENT_LINKS", "Abonents Links", plugin2);
            s.DeclareField("BASE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("BASE_TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", null);
            s.DeclareField("ABONENT_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_ABONENT_LINKS", "PRIMARY", "BASE_ID,ABONENT_ID,BASE_TABLE_NAME");
            s.DeclareJoin("BaseMobStation", "MOB_STATION", null, "BASE_ID,BASE_TABLE_NAME", "ID,TABLE_NAME");
            s.DeclareJoin("BaseMobStation2", "MOB_STATION2", null, "BASE_ID,BASE_TABLE_NAME", "ID,TABLE_NAME");
            s.DeclareJoin("AbonentStation", "GSM", null, "ABONENT_ID", "ID");

            // Таблица владельцев абонентских РЕЗ
            s.DeclareTable("XNRFA_ABONENT_OWNER", "Abonent owners", plugin2);
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_ABONENT_OWNER", "PRIMARY", "ID");
            s.DeclareField("NAME", "VARCHAR(300)", null, "NOTNULL", null);
            s.DeclareField("SHORTNAME", "VARCHAR(300)", null, null, null);
            s.DeclareField("NAME_LATIN", "VARCHAR(300)", null, null, null);
            s.DeclareField("ADDRESS", "VARCHAR(300)", null, null, null);
            s.DeclareField("TEL", "VARCHAR(100)", null, null, null);
            s.DeclareField("REMARK", "VARCHAR(2000)", null, null, null);
            s.DeclareField("OKPO", "VARCHAR(50)", null, null, null);
            s.DeclareField("PASSPORT", "VARCHAR(500)", null, null, null);
            s.DeclareField("FILE_NUM", "VARCHAR(500)", null, null, null);
            s.DeclareField("DATE_BORN", "DATE", "Date", null, null);
            //----
            s.DeclareField("NAME1", "VARCHAR(300)", null, null, null);
            s.DeclareField("NAME_LATIN1", "VARCHAR(300)", null, null, null);
            s.DeclareField("NAME2", "VARCHAR(300)", null, null, null);
            s.DeclareField("SURNAME", "VARCHAR(300)", null, null, null);
            s.DeclareField("SURNAME_LATIN", "VARCHAR(300)", null, null, null);
            // s.DeclareField("INDEX", "NUMBER(7,0)", null, null, null);
            s.DeclareField("INDEX", "VARCHAR(10)", null, null, null);
            s.DeclareField("OBL", "VARCHAR(300)", null, null, null);
            s.DeclareField("DISTR", "VARCHAR(300)", null, null, null);
            s.DeclareField("LOCAL", "VARCHAR(300)", null, null, null);
            s.DeclareField("FAX", "VARCHAR(100)", null, null, null);
            s.DeclareField("SER_PASSP", "VARCHAR(2)", null, null, null);
            s.DeclareField("NUM_PASSP", "VARCHAR(6)", null, null, null);
            s.DeclareField("DATE_PASSP", "DATE", "Date", null, null);
            s.DeclareField("PUB_PASSP", "VARCHAR(400)", null, null, null);
            //----
            s.DeclareField("CREATED_BY", "VARCHAR(50)", null, null, null);
            s.DeclareField("CREATED_DATE", "DATE", "Date", null, null);
            s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
            s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
            s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);



            //TODO После добавления абонентов, необходимо удалить
            // Таблица связи владельца абонентской РЕЗ с РЕЗ
            s.DeclareTable("XNRFA_OWNER_LINK", "Owner link", plugin2);
            s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("ABONENT_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_OWNER_LINK", "PRIMARY", "OWNER_ID,ABONENT_ID");
            s.DeclareJoin("AbonentStation", "GSM", null, "ABONENT_ID", "ID");

            // Таблица содерхит счетчики для нумерации документов
            s.DeclareTable("XNRFA_LOCKS", "Plugins locks", "PLUGIN_2,98");
            s.DeclareField("WHAT", "VARCHAR(255)", null, null, null);
            s.DeclareField("WHO", "VARCHAR(255)", null, null, null);
            s.DeclareField("WHEN", "DATE", "Date", null, null);

            // Таблица для временного хранениия 
            s.DeclareTable("XNRFA_SYS_CONFIG", "Plugins system config", "PLUGIN_2,98");
            s.DeclareField("ITEM", "VARCHAR(100)", null, null, null);
            s.DeclareField("WHAT", "VARCHAR(100)", null, null, null);

            // Таблица для хранения переходов статусов станций
            s.DeclareTable("XNRFA_STATUS_WF", "Workflow of station's status", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_XNRFA_STATUS_WF", "PRIMARY", "ID");
            s.DeclareField("CODE_WF", "VARCHAR(4000)", null, null, null);
            s.DeclareField("IS_AUTO", "NUMBER(1,0)", null, null, "0");

            s.DeclareTable("XNRFA_POSITION", "Positions for tables of Differences famuly", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_POSITIONS", "PRIMARY", "ID");
                s.DeclareField("NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("LONGITUDE", "NUMBER(22,8)", "Longitude", null, null);
                s.DeclareField("LATITUDE", "NUMBER(22,8)", "Latitude", null, null);
                s.DeclareField("REMARK", "VARCHAR(2000)", null, null, null);
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("City", "CITIES", null, "CITY_ID", "ID");
                s.DeclareField("ADDRESS", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT4", "VARCHAR(10)", null, null, null);
                s.DeclareField("CUST_TXT7", "VARCHAR(1000)", null, null, null);
                s.DeclareField("CUST_TXT8", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("COUNTRY_ID", "VARCHAR(4)", "eri_COUNTRY", null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("X", "NUMBER(22,8)", null, null, null);
                s.DeclareField("Y", "NUMBER(22,8)", null, null, null);
                s.DeclareField("CSYS", "VARCHAR(4)", null, null, null);
                s.DeclareField("CODE", "VARCHAR(13)", null, null, null);
                s.DeclareField("ASL", "NUMBER(22,8)", null, null, null);
                s.DeclareField("ADMS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("AdmSite", "SITES", null, "ADMS_ID", "ID");
            }

            // Сертификаты для измерительного оборудования.
            s.DeclareTable("XNRFA_MSREQ_CERT", "Certificates for measure equipment", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_MSREQ_CERT", "PRIMARY", "ID");

                s.DeclareField("CERT_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("CERTIFIED_BY", "VARCHAR(100)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(35)", null, null, null);

                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", "OLD_USER");
            }

            // Таблица справочник измерительного оборудования
            s.DeclareTable("XNRFA_MEASURE_EQ", "Measure equipment", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_MEASURE_EQ", "PRIMARY", "ID");

                s.DeclareField("NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DEV_TYPE", "VARCHAR(5)", "eri_MeasureEquipmentType", null, null);
                s.DeclareField("SERIAL", "VARCHAR(40)", null, null, null);
                s.DeclareField("NOTES", "VARCHAR(200)", null, null, null);
                s.DeclareField("CHECKDATE", "DATE", "Date", null, null);

                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "CERT_ID", "ID");
            }

            // Таблица справочник дополнительного оборудования
            s.DeclareTable("XNRFA_AUXILIARY_EQ", "Auxiliary equipment", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_AUXILIARY_EQ", "PRIMARY", "ID");
                s.DeclareField("NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("DEV_TYPE", "VARCHAR(5)", "eri_AuxiliaryEquipmentType", null, null);
                s.DeclareField("NOTES", "VARCHAR(200)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(35)", null, null, null);
            }

            // Связи - измерительное оборудование к протоколу
            s.DeclareTable("XNRFA_MSREQ_LIST", "Selected measure equipment", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("MOBSTA2_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MW_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TV_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("FM_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDVB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDAB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, "NOTNULL", null);

                s.DeclareIndex("PK_XNRFA_MSREQ_LIST", "PRIMARY", "ID");

                s.DeclareJoin("MeasureEquipment", "XNRFA_MEASURE_EQ", "", "MEASURE_EQ_ID", "ID");

                s.DeclareJoin("MobStation2", "XNRFA_DIFF_MOBSTA2", "CASCDEL", "MOBSTA2_ID", "ID");
                s.DeclareJoin("EarthStation", "XNRFA_DIFF_ET_STAT", "CASCDEL", "ET_ID", "ID");
                s.DeclareJoin("Microwa", "XNRFA_DIFF_MW_STAT", "CASCDEL", "MW_ID", "ID");
                s.DeclareJoin("MobStation", "XNRFA_DIFF_MOBSTA", "CASCDEL", "MOBSTA_ID", "ID");
                s.DeclareJoin("TvStation", "XNRFA_DIFF_TV_STAT", "CASCDEL", "TV_ID", "ID");
                s.DeclareJoin("FmStation", "XNRFA_DIFF_FM_STAT", "CASCDEL", "FM_ID", "ID");
                s.DeclareJoin("DvbtStation", "XNRFA_DIFF_DVB_STAT", "CASCDEL", "TDVB_ID", "ID");
                s.DeclareJoin("TdabStation", "XNRFA_DIFF_FMDIG", "CASCDEL", "TDAB_ID", "ID");
            }

            // Связи - дополнительное оборудование к протоколу
            s.DeclareTable("XNRFA_AUXEQ_LIST", "Selected auxiliary equipment", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("MOBSTA2_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MW_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TV_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("FM_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDVB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDAB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("AUXILIARY_EQ_ID", "NUMBER(9,0)", null, "NOTNULL", null);

                s.DeclareIndex("PK_XNRFA_AUXEQ_LIST", "PRIMARY", "ID");

                s.DeclareJoin("AuxiliaryEquipment", "XNRFA_AUXILIARY_EQ", "", "AUXILIARY_EQ_ID", "ID");

                s.DeclareJoin("MobStation2", "XNRFA_DIFF_MOBSTA2", "CASCDEL", "MOBSTA2_ID", "ID");
                s.DeclareJoin("EarthStation", "XNRFA_DIFF_ET_STAT", "CASCDEL", "ET_ID", "ID");
                s.DeclareJoin("Microwa", "XNRFA_DIFF_MW_STAT", "CASCDEL", "MW_ID", "ID");
                s.DeclareJoin("MobStation", "XNRFA_DIFF_MOBSTA", "CASCDEL", "MOBSTA_ID", "ID");
                s.DeclareJoin("TvStation", "XNRFA_DIFF_TV_STAT", "CASCDEL", "TV_ID", "ID");
                s.DeclareJoin("FmStation", "XNRFA_DIFF_FM_STAT", "CASCDEL", "FM_ID", "ID");
                s.DeclareJoin("DvbtStation", "XNRFA_DIFF_DVB_STAT", "CASCDEL", "TDVB_ID", "ID");
                s.DeclareJoin("TdabStation", "XNRFA_DIFF_FMDIG", "CASCDEL", "TDAB_ID", "ID");
            }

            // Связи - дополнительное оборудование к протоколу
            s.DeclareTable("XNRFA_PARM_REZ_NOTE", "Notes for (\"PARAM REZ\") protocol", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("MOBSTA2_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MW_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TV_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("FM_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDVB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TDAB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("SC_ID", "NUMBER(9,0)", null, "NOTNULL", null);

                s.DeclareIndex("PK_XNRFA_PR_NOTE_LIST", "PRIMARY", "ID");

                s.DeclareJoin("SpecialCondition", "XNRFA_SCPATTERN", "", "SC_ID", "ID");

                s.DeclareJoin("MobStation2", "XNRFA_DIFF_MOBSTA2", "CASCDEL", "MOBSTA2_ID", "ID");
                s.DeclareJoin("EarthStation", "XNRFA_DIFF_ET_STAT", "CASCDEL", "ET_ID", "ID");
                s.DeclareJoin("Microwa", "XNRFA_DIFF_MW_STAT", "CASCDEL", "MW_ID", "ID");
                s.DeclareJoin("MobStation", "XNRFA_DIFF_MOBSTA", "CASCDEL", "MOBSTA_ID", "ID");
                s.DeclareJoin("TvStation", "XNRFA_DIFF_TV_STAT", "CASCDEL", "TV_ID", "ID");
                s.DeclareJoin("FmStation", "XNRFA_DIFF_FM_STAT", "CASCDEL", "FM_ID", "ID");
                s.DeclareJoin("DvbtStation", "XNRFA_DIFF_DVB_STAT", "CASCDEL", "TDVB_ID", "ID");
                s.DeclareJoin("TdabStation", "XNRFA_DIFF_FMDIG", "CASCDEL", "TDAB_ID", "ID");
            }

            s.DeclareTable("XNRFA_DIFF_FMDIG", "Differences for FM_DIGITALL", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_FMDIG");
                s.DeclareIndex("PK_DIFF_FMDIGITAL", "PRIMARY", "ID");
                s.DeclareField("LONGITUDE", "NUMBER(22,8)", null, null, null);
                s.DeclareField("LATITUDE", "NUMBER(22,8)", null, null, null);
                s.DeclareField("REMARK", "VARCHAR(100)", null, null, null);
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("POLAR", "VARCHAR(20)", null, null, null);
                s.DeclareField("OFFSET", "NUMBER(22,8)", null, null, null);
                s.DeclareField("TIP_REZ", "VARCHAR(50)", null, null, null);
                s.DeclareField("BLOCK_STR", "VARCHAR(50)", null, null, null);
                s.DeclareField("POWER", "NUMBER(22,8)", dBm, null, null);
                s.DeclareField("ANT_DIR", "VARCHAR(6)", null, null, null);
                s.DeclareField("ANT_POLARH", "VARCHAR(3)", null, null, null);
                s.DeclareField("ANT_POLARV", "VARCHAR(3)", null, null, null);
                s.DeclareField("ANT_HEIGHT", "NUMBER(22,8)", null, null, null);
                s.DeclareField("FREQS_BLOCK", "NUMBER(22,8)", null, null, null);
                s.DeclareField("FREQS_CENTRAL", "NUMBER(22,8)", null, null, null);
                s.DeclareField("FM_DIGITAL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("BW", "NUMBER(15,5)", null, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareJoin("FmDigital", ICSMTbl.itblTDAB, null, "FM_DIGITAL_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionBro, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");
                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");

                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            s.DeclareTable("XNRFA_DIFF_MOBSTA2", "Differences for MOB_STATION2", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_DIFF_MOBSTA2", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_MOBSTA2");
                s.DeclareField("MOBSTA2_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", dBm, null, null);
                s.DeclareField("AZIMUTH", "NUMBER(5,2)", null, null, null);
                s.DeclareField("ELEVATION", "NUMBER(5,2)", null, null, null);
                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ASL", "NUMBER(7,2)", null, null, null);
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("POLAR", "VARCHAR(3)", null, null, null);
                s.DeclareField("MODULATION", "VARCHAR(3)", null, null, null);
                s.DeclareField("ONOFF", "NUMBER(1,0)", null, null, null);

                s.DeclareJoin("Antenna", "ANTENNA_MOB2", null, "ANT_ID", "ID");
                s.DeclareJoin("Equipment", "EQUIP_MOB2", null, "EQUIP_ID", "ID");
                s.DeclareJoin("MobStation2", ICSMTbl.itblMobStation2, null, "MOBSTA2_ID", "ID");

                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionMob2, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("DESIG_EM", "VARCHAR(10)", DesigEm, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", BwKHz, null, null);
                s.DeclareField("WIDTH_ANT", "NUMBER(7,2)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("ANTEN_TYPE", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("GAIN", "NUMBER(6,2)", null, null, null);
                s.DeclareField("T_BMWDTH", "NUMBER(7,2)", null, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            s.DeclareTable("XNRFA_DIFF_MOBSTA", "Differences for MOB_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_DIFF_MOBSTA", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_MOBSTA");
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(40)", null, null, null);
                s.DeclareField("EQUIP_NBR", "NUMBER(6,0)", null, null, null);
                s.DeclareField("EX_FILTER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("IS_TX", "NUMBER(1,0)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", dBm, null, null);
                s.DeclareField("AZIMUTH", "NUMBER(5,2)", null, null, null); //азимут 
                s.DeclareField("ELEVATION", "NUMBER(5,2)", null, null, null);
                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ASL", "NUMBER(7,2)", null, null, null); //высота
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null); //позиция
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("TX_LOSSES", "NUMBER(6,2)", null, null, null); //затухания
                s.DeclareField("GAIN", "NUMBER(6,2)", null, null, null);
                s.DeclareField("STATION_CLASS", "VARCHAR(100)", null, null, null);
                //поле для тех диф класа класс станции
                s.DeclareField("CALL_SIGN", "VARCHAR(100)", null, null, null); //поле для тех диф класа позывной станции
                /*s.DeclareField("POLAR", "VARCHAR(3)", null, null, null);*/
                s.DeclareField("ONOFF", "NUMBER(1,0)", null, null, null);
                s.DeclareField("NETWORK_IDENT", "VARCHAR(100)", null, null, null);
                s.DeclareField("TYPE", "VARCHAR(100)", null, null, null); //???
                s.DeclareField("CLASS", "VARCHAR(100)", null, null, null); //???
                // s.DeclareField("STANDART", "VARCHAR(100)", null, null, null);
                //s.DeclareJoin("Antenna", "ANTENNA_MOB", null, "ANT_ID", "ID");
                //s.DeclareJoin("Equipment", "EQUIP_MOB", null, "EQUIP_ID", "ID");
                s.DeclareJoin("MobStation", ICSMTbl.itblMobStation, null, "MOBSTA_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionWim, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("DESIG_EM", "VARCHAR(10)", DesigEm, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("AGL", "NUMBER(7,2)", null, null, null); //высота
                s.DeclareField("EQUIP_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("ANTENNA_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("ANT_WIDTH", "NUMBER(9,2)", null, null, null); //ширина ДС   
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            s.DeclareTable("XNRFA_MOBSTA_FREQ", "Differences frequencies for MOB_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_DIFF_MOBSTA_FREQ", "PRIMARY", "ID");
                s.DeclareField("FREQUENCY", "NUMBER(22,8)", null, null, null);
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TR", "VARCHAR(3)", null, null, null);
                s.DeclareJoin("MobStation", "MOB_STATION", null, "MOBSTA_ID", "ID");
            }

            s.DeclareTable("XNRFA_CHAN_MOBSTA", "Differences channels for MOB_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_CHANNELS_MOBSTA", "PRIMARY", "ID");
                s.DeclareField("MOBSTA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("CHANNEL1", "NUMBER(16,0)", null, null, null);
                s.DeclareField("CHANNEL2", "NUMBER(16,0)", null, null, null);
            }

            s.DeclareTable("XNRFA_MOBSTA2_FREQ", "Differences frequencies for MOB_STATION2", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_DIFF_MOBSTA2_FREQ", "PRIMARY", "ID");
                s.DeclareField("FREQUENCY", "NUMBER(22,8)", null, null, null);
                s.DeclareField("MOBSTA2_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TR", "VARCHAR(3)", null, null, null);
                s.DeclareJoin("MobStation2", "MOB_STATION2", null, "MOBSTA2_ID", "ID");
            }

            s.DeclareTable("XNRFA_DIFF_TV_STAT", "Differences for TV_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_TV_STAT");
                s.DeclareIndex("PK_DIFF_TV_STAT", "PRIMARY", "ID");
                //s.DeclareField("LONGITUDE", "NUMBER(10,6)", null, null, null);
                //s.DeclareField("LATITUDE", "NUMBER(10,6)", null, null, null);
                s.DeclareField("OFFSET_V_KHZ", "NUMBER(8,3)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", null, null, null);
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("DELTA_F_SND1", "NUMBER(8,3)", null, null, null); // steep
                s.DeclareField("FREQ", "NUMBER(10,5)", null, null, null);
                s.DeclareField("FREQ_V_CARR", "NUMBER(10,5)", null, null, null);
                s.DeclareField("CHANNEL", "VARCHAR(5)", null, null, null);
                //s.DeclareField("PWR_RATIO_1", "NUMBER(5,2)", null, null, null);
                s.DeclareField("AGL", "NUMBER(7,2)", null, null, null);
                s.DeclareField("POLAR", "VARCHAR(3)", null, null, null);
                s.DeclareField("DIRECTION", "VARCHAR(3)", null, null, null);
                s.DeclareField("PLAN_ID", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TV_STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);            
                //s.DeclareJoin("Position", "POSITION_BRO", null, "POS_ID", "ID");
                //s.DeclareJoin("Antenna", "ANTENNA_BRO", null, "ANT_ID", "ID");
                s.DeclareJoin("Equipment", "EQUIP_BRO", null, "EQUIP_ID", "ID");
                s.DeclareJoin("TvStation", ICSMTbl.itblTV, null, "TV_STATION_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionBro, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);

                //s.DeclareField("equip", "VARCHAR(255)", null, null, null);
                //s.DeclareField("powVideo", "NUMBER(10,5)", null, null, null);
                //s.DeclareField("powAudio", "NUMBER(10,5)", null, null, null);
                //s.DeclareField("rxBand", "NUMBER(10,5)", null, null, null);
                //s.DeclareField("emiVideo", "VARCHAR(255)", null, null, null);
                //s.DeclareField("emiAudio", "VARCHAR(255)", null, null, null);
                //s.DeclareField("certNo", "VARCHAR(255)", null, null, null);
                //s.DeclareField("certDa", "DATE", "Date", null, null);

                s.DeclareField("EQUIP", "VARCHAR(255)", null, null, null);
                s.DeclareField("POWVIDEO", "NUMBER(10,5)", null, null, null);
                s.DeclareField("POWAUDIO", "NUMBER(10,5)", null, null, null);
                s.DeclareField("RXBAND", "NUMBER(10,5)", null, null, null);
                s.DeclareField("EMIVIDEO", "VARCHAR(255)", null, null, null);
                s.DeclareField("EMIAUDIO", "VARCHAR(255)", null, null, null);
                s.DeclareField("CERTNO", "VARCHAR(255)", null, null, null);
                s.DeclareField("CERTDA", "DATE", Date, null, null);

                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //////////////////////////////////////////////////////////////////////////
            s.DeclareTable("XNRFA_DIFF_FM_STAT", "Differences for FM_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_FM_STAT");
                s.DeclareIndex("PK_DIFF_FM_STAT", "PRIMARY", "ID");
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", null, null, null);
                s.DeclareField("AGL", "NUMBER(7,2)", null, null, null);
                s.DeclareField("BW", "NUMBER(7,2)", null, null, null);
                s.DeclareField("FREQ", "NUMBER(10,5)", null, null, null);
                s.DeclareField("POLAR", "VARCHAR(3)", null, null, null);
                s.DeclareField("DIRECTION", "VARCHAR(3)", null, null, null);
                s.DeclareField("FM_STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);

                s.DeclareJoin("Equipment", "EQUIP_BRO", null, "EQUIP_ID", "ID");
                s.DeclareJoin("FmStation", ICSMTbl.itblFM, null, "FM_STATION_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionBro, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("MAX_KOEF_G", "VARCHAR(30)", null, null, null);
                s.DeclareField("MAX_KOEF_V", "VARCHAR(30)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(30)", null, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //////////////////////////////////////////////////////////////////////////
            s.DeclareTable("XNRFA_DIFF_MW_STAT", "Differences for MICROWA", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_MW_STAT");
                s.DeclareIndex("PK_DIFF_MW_STAT", "PRIMARY", "ID");
                s.DeclareField("POS_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE1", "VARCHAR(20)", null, null, null);
                s.DeclareField("POS_TABLE2", "VARCHAR(20)", null, null, null);
                s.DeclareField("POWER1", "NUMBER(15,6)", null, null, null);
                s.DeclareField("POWER2", "NUMBER(15,6)", null, null, null);
                s.DeclareField("AGL1", "NUMBER(7,2)", null, null, null);
                s.DeclareField("AGL2", "NUMBER(7,2)", null, null, null);
                s.DeclareField("TX_FREQ1", "NUMBER(22,8)", null, null, null);
                s.DeclareField("TX_FREQ2", "NUMBER(22,8)", null, null, null);
                s.DeclareField("POLAR1", "VARCHAR(3)", null, null, null);
                s.DeclareField("POLAR2", "VARCHAR(3)", null, null, null);
                s.DeclareField("MW_STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ANTEN_ID1", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ANTEN_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Equipment1", "EQUIP_MW", null, "EQUIP_ID1", "ID");
                s.DeclareJoin("Equipment2", "EQUIP_MW", null, "EQUIP_ID2", "ID");
                s.DeclareJoin("Ant1", "ANTENNA_MW", null, "ANTEN_ID1", "ID");
                s.DeclareJoin("Ant2", "ANTENNA_MW", null, "ANTEN_ID2", "ID");

                s.DeclareJoin("Microwa", ICSMTbl.itblMicrowa, null, "MW_STATION_ID", "ID");
                s.DeclareJoin("NativePosition1", ICSMTbl.itblPositionMw, null, "POS_ID1,POS_TABLE1", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition1", PlugTbl.itblXnrfaPositions, null, "POS_ID1,POS_TABLE1", "ID,TABLE_NAME");
                s.DeclareJoin("NativePosition2", ICSMTbl.itblPositionMw, null, "POS_ID2,POS_TABLE2", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition2", PlugTbl.itblXnrfaPositions, null, "POS_ID2,POS_TABLE2", "ID,TABLE_NAME");

                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("AZIMUTH_1", "NUMBER(7,2)", null, null, null);
                s.DeclareField("AZIMUTH_2", "NUMBER(7,2)", null, null, null);
                s.DeclareField("BW", "NUMBER(22,8)", null, null, null);
                s.DeclareField("DIAMETER_1", "NUMBER(6,2)", null, null, null);
                s.DeclareField("DIAMETER_2", "NUMBER(6,2)", null, null, null);
                s.DeclareField("ANTENNA1_NAME", "VARCHAR(125)", null, null, null);
                s.DeclareField("ANTENNA2_NAME", "VARCHAR(125)", null, null, null);
                s.DeclareField("EQUIPMENT_NAME", "VARCHAR(125)", null, null, null);
                s.DeclareField("GAIN1", "NUMBER(7,2)", null, null, null);
                s.DeclareField("GAIN2", "NUMBER(7,2)", null, null, null);
                s.DeclareField("MODULATION", "VARCHAR(3)", null, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //////////////////////////////////////////////////////////////////////////
            s.DeclareTable("XNRFA_DIFF_ET_STAT", "Differences for EARTH_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_ET_STAT");
                s.DeclareIndex("PK_DIFF_ET_STAT", "PRIMARY", "ID");
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", null, null, null);
                s.DeclareField("AGL", "NUMBER(7,2)", null, null, null);
                s.DeclareField("POLAR_TX", "VARCHAR(3)", null, null, null);
                s.DeclareField("POLAR_RX", "VARCHAR(3)", null, null, null);
                s.DeclareField("ET_STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);

                s.DeclareJoin("EarthStation", ICSMTbl.itblEarthStation, null, "ET_STATION_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionEs, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("AZM_TO", "NUMBER(4,1)", null, null, null);
                s.DeclareField("EVEL_MIN", "NUMBER(4,1)", null, null, null);
                s.DeclareField("RX_DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("TX_DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("TX_BW", "NUMBER(22,8)", null, null, null);
                s.DeclareField("RX_BW", "NUMBER(22,8)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("DIAMETER", "NUMBER(6,2)", null, null, null);
                s.DeclareField("SATTELITE", "VARCHAR(100)", null, null, null);
                s.DeclareField("ANTENNA_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("RXMODULATION", "VARCHAR(4)", null, null, null);
                s.DeclareField("TXMODULATION", "VARCHAR(4)", null, null, null);
                s.DeclareField("RXGAIN", "NUMBER(6,2)", null, null, null);
                s.DeclareField("TXGAIN", "NUMBER(6,2)", null, null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //////////////////////////////////////////////////////////////////////////
            s.DeclareTable("XNRFA_DIFF_ET_FREQ", "Differences frequencies for EARTH_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_DIFF_ET_FREQ", "PRIMARY", "ID");
                s.DeclareField("ET_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("FREQ", "NUMBER(22,8)", null, null, null);
                s.DeclareField("TX", "VARCHAR(3)", null, null, null);
                s.DeclareJoin("XnrfEarthStation", "XNRFA_DIFF_ET_STAT", null, "ET_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //////////////////////////////////////////////////////////////////////////
            s.DeclareTable("XNRFA_DIFF_DVB_STAT", "Differences for DVB_STATION", "PLUGIN_2,98");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XNRFA_DIFF_DVB_STAT");
                s.DeclareIndex("PK_DIFF_DVB_STAT", "PRIMARY", "ID");
                s.DeclareField("OFFSET_V_KHZ", "NUMBER(8,3)", null, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", null, null, null);
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("FREQ", "NUMBER(10,5)", null, null, null);
                s.DeclareField("CHANNEL", "VARCHAR(5)", null, null, null);
                s.DeclareField("AGL", "NUMBER(7,2)", null, null, null);
                s.DeclareField("POLAR", "VARCHAR(3)", null, null, null);
                s.DeclareField("DIRECTION", "VARCHAR(3)", null, null, null);
                s.DeclareField("PLAN_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DVB_STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);

                s.DeclareJoin("Equipment", "EQUIP_BRO", null, "EQUIP_ID", "ID");
                s.DeclareJoin("DvbStation", ICSMTbl.itblDVBT, null, "DVB_STATION_ID", "ID");
                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionBro, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");

                s.DeclareField("BW", "NUMBER(7,2)", null, null, null);
                s.DeclareField("MAX_KOEF_G", "NUMBER(7,2)", null, null, null);
                s.DeclareField("MAX_KOEF_V", "NUMBER(7,2)", null, null, null);
                s.DeclareField("DESIG_EM", "VARCHAR(10)", null, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("COMMENTS", "VARCHAR(2000)", null, null, null);

                s.DeclareField("TOTAL", "VARCHAR(2000)", null, null, null); // Особливі умови Висновку щодо ЕМС
                s.DeclareField("RNV", "VARCHAR(2000)", null, null, null); // Результати НВ
                s.DeclareField("RTV", "VARCHAR(2000)", null, null, null); // Результати ТВ
                s.DeclareField("RMP", "VARCHAR(2000)", null, null, null); // Результати вимірювання паремтрів
                s.DeclareField("RITMP", "VARCHAR(2000)", null, null, null); // Результати інстр. оцінки парам. випр.
                s.DeclareField("GENERAL", "VARCHAR(2000)", null, null, null); // Загальні висновки акту ПТК
                s.DeclareField("PATH1", "VARCHAR(261)", null, null, null); // Протокол інстр. оцінки парам. випр.
                s.DeclareField("PATH2", "VARCHAR(261)", null, null, null); // Протокол вимірювання парамертрів РЕЗ
                s.DeclareField("LASTPATH", "VARCHAR(261)", null, null, null); // Останній варінт акту ПТК
                s.DeclareField("CHIEF_ID", "NUMBER(9,0)", null, null, null); // Голова комісії
                s.DeclareField("MEMBER1_ID", "NUMBER(9,0)", null, null, null); // Член комісії 1
                s.DeclareField("MEMBER2_ID", "NUMBER(9,0)", null, null, null); // Член комісії 2
                s.DeclareField("MEMBER3_ID", "NUMBER(9,0)", null, null, null); // Член комісії 3
                // Інструментальна оцінка
                s.DeclareField("MEASURE_EQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("MeasureEquip", PlugTbl.MeasureEquipment, null, "MEASURE_EQ_ID", "ID");
                s.DeclareField("MEASURE_DATE1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO1", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_DATE2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_FROM2", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO2", "DATE", "Date", null, null);
                s.DeclareField("VISA_USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ViseUser", "EMPLOYEE", null, "VISA_USER_ID", "ID");
                s.DeclareField("EXECUTOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Executor", "EMPLOYEE", null, "EXECUTOR_ID", "ID");
                s.DeclareField("LINK_IE", "VARCHAR(200)", null, null, ""); // Останній варінт інструментальної оцінки
                s.DeclareField("ENABLE_DATE1", "NUMBER(1,0)", null, null, "1");
                s.DeclareField("ENABLE_DATE2", "NUMBER(1,0)", null, null, "0");

                //Для протоколу вимірювання параметрів РЕЗ
                s.DeclareField("MSREQ_CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURE_TIME_FROM3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_TIME_TO3", "DATE", "Date", null, null);
                s.DeclareField("MEASURE_APPROVED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEASURED_BY", "NUMBER(9,0)", null, null, null);
                s.DeclareField("LINK_IE3", "VARCHAR(200)", null, null, "");
                // Останній варінт протоколу вимірювання параметрів РЕЗ
                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "MSREQ_CERT_ID", "ID");
                s.DeclareField("INACTIVE", "NUMBER(1,0)", null, null, "0");
            }

            //===============================================
            s.DeclareTable("XNRFA_ALL_STAT", "Plugin's all stations", plugin2);
            s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("OBJ_NAME", "VARCHAR(20)", null, "NOTNULL", null);
            s.DeclareIndex("PK_ALL_STAT", "PRIMARY", "OBJ_ID,OBJ_NAME");
            s.DeclareJoin("AllStations", "ALL_STATIONS", null, "OBJ_ID,OBJ_NAME", "TABLE_ID,TABLE_NAME");
            s.DeclareField("CUST_TXT1", "VARCHAR(50)", null, null, null);
            s.DeclareField("CUST_TXT2", "VARCHAR(50)", null, null, null);
            s.DeclareField("CUST_TXT3", "VARCHAR(2000)", null, null, null);
            s.DeclareField("CUST_TXT4", "VARCHAR(2000)", null, null, null);


            // Таблица заявок в ДРВ от УРЧМ
            s.DeclareTable("XNRFA_APPLPAYURCM", "Applications for DRV from URCM", "PLUGIN_2,98");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_APPLPAYURCM", "PRIMARY", "ID");
            s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
            s.DeclareField("STATUS", "VARCHAR(15)", "eri_StatusApplUrcm", "NOTNULL", null);
            s.DeclareField("INVOICE_STATUS", "VARCHAR(15)", "eri_InvoiceStatus", null, null);
            s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
            s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
            s.DeclareField("DOC_PATH", "VARCHAR(500)", null, null, null);
            s.DeclareField("MEMO_NUMBER", "VARCHAR(100)", null, null, null);
            s.DeclareField("HASDIFF", "NUMBER(1,0)", null, null, null);
            s.DeclareField("ADD_DOC_PATH", "VARCHAR(500)", null, null, null);
            s.DeclareField("ADD_CREATED_DATE", "DATE", null, null, null);
            s.DeclareField("FULL_PATH", "VARCHAR(500)", null, null, null);
            s.DeclareField("EMPLOYEE_DRV_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("EmployeeDRV", "EMPLOYEE", "", "EMPLOYEE_DRV_ID", "ID");
            s.DeclareField("RESPONSE_DATE", "DATE", null, null, null);
            s.DeclareField("CONTRACTS_ID", "NUMBER(12,0)", null, null, null);
            s.DeclareField("IS_ORDER", "NUMBER(1,0)", null, null, null);
            s.DeclareJoin("ConnectMonitorContract", "XNRFA_MON_CONTRACTS", "", "CONTRACTS_ID", "ID");
            s.DeclareField("INVOICE", "VARCHAR(50)", null, null, null);
            s.DeclareField("INVOICE_FROM", "DATE", null, null, null);
            s.DeclareField("INVOICE_TO", "DATE", null, null, null);
            s.DeclareField("INV_EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareField("TYPE", "NUMBER(1,0)", null, null, null);


            //XNRFA_CHOSENAPPLS_URCM
            // Список выбраных заявок станций для заявки в ДРВ
            s.DeclareTable("XNRFA_CHAPPURCM", "Chosen applications for DRV application URCM", plugin2);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_CHAPSURCM", "PRIMARY", "ID");
                s.DeclareField("APP_URCM_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("ApplicationPay", "XNRFA_APPLPAYURCM", null, "APP_URCM_ID", "ID");
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
                s.DeclareField("NET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Net", "XFA_NET", null, "NET_ID", "ID");
                s.DeclareField("COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ADRES", "VARCHAR(500)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", "eri_Area", null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("PRICE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Price", "XNRFA_PRICE", RefAccessDbms, "PRICE_ID", "ID");
            }

            // Сведения о составе сетей УКХ (лицензиатов), которые содержаться в договоре 
            s.DeclareTable("XNRFA_CHAPPNET", "Data on the composition of networks VHF", plugin2);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("CHAPPNET_ID", "PRIMARY", "ID");
                s.DeclareField("NET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("APPL_URCM_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, "NOTNULL", null);

                s.DeclareJoin("AppPayUrcm", "XNRFA_APPLPAYURCM", null, "APPL_URCM_ID", "ID");
                s.DeclareJoin("Appl_ID", "XNRFA_APPL", null, "APPL_ID", "ID");
            }


            // Таблица для хранения глобальных идентификаторов мониторинга
            s.DeclareTable("XNRFA_MONITORING", "Monitoring global ID", "PLUGIN_3,99");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("ELEM", "VARCHAR(255)", null, null, null);

            // Лог таблица
            s.DeclareTable("XNRFA_LOGS", "Plugins log", "REC_HISTORY");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareField("TYPE", "VARCHAR(50)", "eri_TRFATypeLogs", null, null);
            s.DeclareField("WHAT", "VARCHAR(100)", "eri_TRFAWhatLogs", null, null);
            //s.DeclareField("WHEN", "DATE", "Date", null, null);
            s.DeclareField("MESSAGE", "VARCHAR(6000)", null, null, null);
            s.DeclareField("DATE_CREATED", "DATE", null, null, null);
            s.DeclareField("CREATED_BY", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Employee", "EMPLOYEE", null, "CREATED_BY", "ID");

            // Таблица хранит данные об статусах отправленных XML в Р135
            s.DeclareTable("XNRFA_STATE_XML135", "State of R135 XML", "PLUGIN_3,99");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Application", "XNRFA_APPL", "CASCDEL", "APPL_ID", "ID");
                s.DeclareField("FILE_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("DOCUMENT", "VARCHAR(100)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(10)", "eri_TRFAStatusXml135", null, null);
                s.DeclareField("MESSAGE", "VARCHAR(4000)", null, null, null);
                s.DeclareField("DATE_SENT", "DATE", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(100)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", null, null, null);
                s.DeclareField("DATE_UPDATED", "DATE", null, null, null);
            }

            // Таблица работ в заявках в ДРВ от УРЧМ
            s.DeclareTable("XNRFA_WORKS_URCM", "Works in applications for DRV from URCM", "PLUGIN_3,99");
            s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareIndex("PK_APPLWORKSURCM", "PRIMARY", "ID");
            s.DeclareField("APPLPAY_ID", "NUMBER(9,0)", null, "NOTNULL", null);
            s.DeclareJoin("ApplicationPay", "XNRFA_APPLPAYURCM", "CASCDEL", "APPLPAY_ID", "ID");
            s.DeclareField("PRICE_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Price", "XNRFA_PRICE", "", "PRICE_ID", "ID");
            s.DeclareField("COUNT", "NUMBER(9,0)", null, "NOTNULL", null);
            //=========================================================
            // XNRFA_STATUS
            s.DeclareTable("XNRFA_STATUS", "Station's status", plugin2);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_STATUS", "PRIMARY", "ID");
                s.DeclareField("STATUS", "VARCHAR(4)", "eri_TRFAStatus", "NOTNULL", null);
                s.DeclareField("WHERE_IS_USED", "VARCHAR(100)", "list_eri_StatusWhereIsUsed", null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, "NOTNULL", null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }

            /*s.DeclareTable("XNRFA_ATTACHMENT", "Not Yet Committed Attachments", "PLUGIN_1,101");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_ATTACHMENT", "PRIMARY", "ID");
                s.DeclareField("STATION_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("STATION_TABLE", "VARCHAR(45)", null, "NOTNULL", null);
                s.DeclareField("SRC_FILE", "VARCHAR(262)", null, null, null);
                s.DeclareField("DST_FILE", "VARCHAR(262)", null, null, null);
            }*/

            //=========================================================
            // XNRFA_CHANGE_MSG
            s.DeclareTable("XN_CHANGE_MSG", "Message of change", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_CHANGEMSG", "PRIMARY", "ID");
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", "eri_ALL_STATIONS", null, null);
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
                s.DeclareField("MESSAGE", "VARCHAR(6000)", null, null, null);
            }
            //=========================================================
            // XNRFA_CHANGE_LOG
            s.DeclareTable("XN_CHANGE_LOG", "Log of change", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_CHANGELOG", "PRIMARY", "ID");
                s.DeclareField("MSG_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Message", "XN_CHANGE_MSG", "", "MSG_ID", "ID");
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATION_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STATION_TBL", "VARCHAR(20)", "eri_ALL_STATIONS", null, null);
                s.DeclareField("RECORD_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("RECORD_TBL", "VARCHAR(20)", "eri_ALL_STATIONS", "NOTNULL", null);
                s.DeclareField("FIELD_NAME", "VARCHAR(50)", "eri_ALL_FIELDS", "NOTNULL", null);
                s.DeclareField("VAL_NEW", "VARCHAR(6000)", null, null, null);
                s.DeclareField("VAL_TYPE", "VARCHAR(100)", null, null, null);
                s.DeclareField("VAL_OLD", "VARCHAR(6000)", null, null, null);
                s.DeclareField("OPER_TYPE", "VARCHAR(100)", "eri_OperationType", null, null);
                s.DeclareField("IP_ADDRESS", "VARCHAR(20)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, "NOTNULL", null);
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
            }
            //=========================================================
            // XNRFA_SPEC_COND
            s.DeclareTable("XNRFA_SPEC_COND", "Special condition for R135", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_SPEC_COND", "PRIMARY", "ID");
                s.DeclareField("DOC_TYPE", "VARCHAR(20)", null, "NOTNULL", null);
                s.DeclareField("SQL", "VARCHAR(6000)", null, "NOTNULL", null);
                s.DeclareField("TEXT_CONDITION", "VARCHAR(6000)", null, null, null);
            }

            s.DeclareTable("XFA_EMI_STATION", "Emitters stations", "XFA_ABONENT_STATION");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_EMI_STAT", "PRIMARY", "ID");
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XFA_POSITION", "DBMS", "POS_ID", "ID");
                s.DeclareField("FREQPLANCHAN_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareField("FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("FREQ_MIN", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("FREQ_MAX", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("BW", "NUMBER(22,8)", null, null, null);
                s.DeclareJoin("Frequency", "FREQ_PLAN_CHAN", null, "FREQPLANCHAN_ID,FREQ", "PLAN_ID,FREQ");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "DBMS", "USER_ID", "ID");
                s.DeclareField("STANDARD", "VARCHAR(10)", Standard, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Equipment", "EQUIP_BRO", null, "EQUIP_ID", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XFA_EMI_STATION");
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);

                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("EMI_TYPE", "VARCHAR(20)", "eri_EmitterType", null, null);
                s.DeclareField("DOP_DEVICE", "VARCHAR(1000)", null, null, null);
                s.DeclareField("POWER", "NUMBER(22,8)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);

                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", "OLD_USER");
                s.DeclareJoin("Emit_link_band", "XFA_EMI_TO_BAND", "", "ID", "EMI_ID");
            }

            s.DeclareTable("XNRFA_SHIP_EXT", "Ship Extended information", "SHIP");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("XNRFA_SHIP_EXT", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("SHIPOWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("CHARTER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("REGISTRATOR_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("REGISTRY_PORT_ID", "NUMBER(9,0)", null, null, null);

                s.DeclareField("COSPAS_SARSAT_IDENT_1", "VARCHAR(30)", null, null, null);
                s.DeclareField("COSPAS_SARSAT_IDENT_2", "VARCHAR(30)", null, null, null);
                s.DeclareField("COSPAS_SARSAT_IDENT_3", "VARCHAR(30)", null, null, null);

                s.DeclareField("BANDWIDTH", "NUMBER(13,6)", null, null, null);

                s.DeclareJoin("Ship", "SHIP", null, "ID", "ID");
                s.DeclareJoin("Owner", "XNRFA_ABONENT_OWNER", null, "OWNER_ID", "ID");
                s.DeclareJoin("ShipOwner", "XNRFA_ABONENT_OWNER", null, "SHIPOWNER_ID", "ID");
                s.DeclareJoin("Charter", "XNRFA_ABONENT_OWNER", null, "CHARTER_ID", "ID");
                s.DeclareJoin("Registrator", "XNRFA_ABONENT_OWNER", null, "REGISTRATOR_ID", "ID");
                s.DeclareJoin("Port", "XFA_REGISTRY_PORT", null, "REGISTRY_PORT_ID", "ID");
            }

            s.DeclareTable("XFA_AMATEUR", "Amateurs", "XFA_ABONENT_STATION");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_AMATEUR_STAT", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_AMATEUR");
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XFA_POSITION", "DBMS", "POS_ID", "ID");

                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "XNRFA_ABONENT_OWNER", null, "OWNER_ID", "ID");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "DBMS", "USER_ID", "ID");

                s.DeclareField("STATUS", "VARCHAR(4)", "eri_TRFAStatus", null, null);
                s.DeclareField("TYPE_STAT", "VARCHAR(4)", "eri_XFA_TYPE_STAT", null, null);
                s.DeclareField("CATEG_OPER", "VARCHAR(4)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                s.DeclareField("APC", "VARCHAR(4)", "eri_XFA_APC", null, null);
                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("CERT", "VARCHAR(4)", null, null, null);
                /////////////////////
                s.DeclareField("NUMB_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("NUMB_PROJ_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_IN_DOZV", "DATE", Date, null, null);
                s.DeclareField("DATE_PERIOD_DOZV", "DATE", Date, null, null);
                s.DeclareField("DATE_PRINT_DOZV", "DATE", Date, null, null);
                s.DeclareField("NUMB_OLD_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_IN_OLD_DOZV", "DATE", Date, null, null);
                /////////////////////
                s.DeclareField("NUMB_BLANK", "VARCHAR(50)", null, null, null);
                //CERTIFICATE
                s.DeclareField("CERTIF_NUMB", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERTIF_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DATE_PRINT", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DELIVERY", "DATE", Date, null, null);
                s.DeclareField("CERTIF_PATH", "VARCHAR(500)", null, null, null);
                //Spec. condition
                s.DeclareField("SPEC_COND_DOZV", "VARCHAR(1000)", null, null, null); //Спец. условия дозвола
                s.DeclareField("SPEC_COND_VISN", "VARCHAR(1000)", null, null, null); //Спец. условия висновка
                s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);           //Примечания
                //Службові поля
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }

            s.DeclareTable("XFA_EQUIP_AMATEUR", "EquipmentsAmateur", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_EQUIP_AMAT", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_EQUIP_AMATEUR");
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }

            //   XFA_AMATEUR  <-----------XFA_ES_AMATEUR ----------->XFA_EQUIP_AMATEUR
            s.DeclareTable("XFA_ES_AMATEUR", "EquipStatAmateur", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_ES_AMAT", "PRIMARY", "ID");
                s.DeclareField("STA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Station", "XFA_AMATEUR", "CASCDEL", "STA_ID", "ID");
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Equipment", "XFA_EQUIP_AMATEUR", null, "EQUIP_ID", "ID");
                s.DeclareField("NUMB", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PLANT_NUMB", "VARCHAR(100)", null, null, null);
                s.DeclareField("POWER", "NUMBER(10,7)", null, null, null);
                s.DeclareField("DEVI", "VARCHAR(30)", null, null, null);
                s.DeclareField("PASSING", "VARCHAR(1)", null, null, null);
                s.DeclareField("NUMB_DOZV", "VARCHAR(100)", null, null, null);
                s.DeclareField("DATE_PRINT_DOZV", "DATE", Date, null, null);
                s.DeclareField("DATE_EXPIR_DOZV", "DATE", Date, null, null);
                s.DeclareField("PATH", "VARCHAR(1000)", null, null, null);
                s.DeclareField("NUM_BLANK", "VARCHAR(200)", null, null, null);
            }
            //   XFA_FREQ_AMATEUR --> XFA_ES_AMATEUR
            s.DeclareTable("XFA_FREQ_AMATEUR", "FreqAmateur", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_FREQ_AMAT", "PRIMARY", "ID");
                s.DeclareField("FREQ_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Frequency", "XFA_ES_AMATEUR", null, "FREQ_ID", "ID");

                s.DeclareField("CHANNEL", "VARCHAR(100)", null, null, null);
                s.DeclareField("TX_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("RX_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                //Службові
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //   XFA_BAND_AMATEUR 
            s.DeclareTable("XFA_BAND_AMATEUR", "BandAmateur", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_BAND_AMAT", "PRIMARY", "ID");
                s.DeclareField("BANDMIN", "NUMBER(22,8)", null, null, null);
                s.DeclareField("BANDMAX", "NUMBER(22,8)", null, null, null);

                s.DeclareField("POWER", "NUMBER(22,8)", null, null, null);
                s.DeclareField("CAT_OPER", "VARCHAR(30)", null, null, null);
                s.DeclareField("TYPE_STAT", "VARCHAR(30)", null, null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(200)", null, null, null);
                //Службові
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }


            //   XFA_ES_AMATEUR  <-----------XFA_FE_AMATEUR ----------->XFA_FREQ_AMATEUR
            s.DeclareTable("XFA_FE_AMATEUR", "FreqEquipAmateur", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_FE_AMAT", "PRIMARY", "ID");
                s.DeclareField("SEL_EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("EquipStation", "XFA_ES_AMATEUR", null, "SEL_EQUIP_ID", "ID");
                s.DeclareField("BAND_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Bandwidth", "XFA_BAND_AMATEUR", null, "BAND_ID", "ID");
            }

            // XFA_CALL_AMATEURS --> XFA_AMATEUR
            s.DeclareTable("XFA_CALL_AMATEUR", "CallAmateurs", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XFA_CALL_AMAT", "PRIMARY", "ID");
                s.DeclareField("STA_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Station", "XFA_AMATEUR", null, "STA_ID", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_CALL_AMATEUR");
                s.DeclareField("TYPE_CALL", "VARCHAR(10)", null, null, null);
                s.DeclareField("CALL", "VARCHAR(200)", null, null, null);
                s.DeclareField("GOAL_USED", "VARCHAR(200)", null, null, null);
                s.DeclareField("DATE_IN", "DATE", Date, null, null);
                s.DeclareField("DATE_OUT", "DATE", Date, null, null);
                s.DeclareField("NUMBER_DOZV", "VARCHAR(200)", null, null, null);
                s.DeclareField("PATH", "VARCHAR(1000)", null, null, null);
                s.DeclareField("NUMB", "VARCHAR(200)", null, null, null);
            }

            s.DeclareTable("XNRFA_FEES", "Fees", "RADIO_SYSTEMS");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_FEES", "PRIMARY", "ID");

                s.DeclareField("ORDER_SYMBOL", "VARCHAR(8)", null, null, null);
                s.DeclareField("RADIO_SYSTEM_NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("MIN_FREQUENCY", "NUMBER(22,9)", "F/MHz", null, null);
                s.DeclareField("MAX_FREQUENCY", "NUMBER(22,9)", "F/MHz", null, null);
                s.DeclareField("MIN_POWER", "NUMBER(13,6)", "W", null, null);
                s.DeclareField("MAX_POWER", "NUMBER(13,6)", "W", null, null);
                s.DeclareField("COST", "NUMBER(10,3)", "Currency", null, null);
            }

            s.DeclareTable("XNRFA_FEES_RS", "Fees for RadioSystems", "RADIO_SYSTEMS");
            {
                s.DeclareField("RADIO_SYSTEM_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("FEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_FEES_RS", "PRIMARY", "RADIO_SYSTEM_ID,FEE_ID");

                s.DeclareJoin("Radiosystem", "RADIO_SYSTEMS", null, "RADIO_SYSTEM_ID", "ID");
                s.DeclareJoin("Fee", "XNRFA_FEES", null, "FEE_ID", "ID");
            }

            s.DeclareTable("XFA_EXPORT_RCHP", "Exported nabor TSOB", "XNRFA_FEES");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("XFA_EXPORT_RCHP", "PRIMARY", "ID");
                s.DeclareField("VALUES", "VARCHAR(200)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_EXPORT_RCHP");
            }

            s.DeclareTable("XFA_NAB_ENT", "Nabory TSOB", "XNRFA_FEES");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("XFA_NAB_ENT", "PRIMARY", "ID");
                s.DeclareField("EXPORT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ExportFields", "XFA_EXPORT_RCHP", null, "EXPORT_ID", "ID");
                s.DeclareField("NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("ENTITY", "VARCHAR(200)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_NAB_ENT");
            }

            s.DeclareTable("XFA_POSITION_EXT", "Extended tables of positions", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_POSITION_EXT", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_POSITION_EXT");
                s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POS_TABLE", "VARCHAR(20)", null, null, null);
                s.DeclareField("SEND_USER", "VARCHAR(50)", null, null, null);
                s.DeclareField("SEND_DATE", "DATE", Date, null, null);
                s.DeclareField("CHECK_USER", "VARCHAR(50)", null, null, null);
                s.DeclareField("CHECK_DATE", "DATE", Date, null, null);

                s.DeclareJoin("NativePosition", ICSMTbl.itblPositionBro, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("CustomPosition", PlugTbl.itblXnrfaPositions, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("EarthPosition", ICSMTbl.itblPositionEs, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("FmnPosition", ICSMTbl.itblPositionFmn, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("HfPosition", ICSMTbl.itblPositionHf, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("Mob2Position", ICSMTbl.itblPositionMob2, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("MwPosition", ICSMTbl.itblPositionMw, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("WimPosition", ICSMTbl.itblPositionWim, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
                s.DeclareJoin("XfaPosition", PlugTbl.XfaPosition, null, "POS_ID,POS_TABLE", "ID,TABLE_NAME");
            }

            s.DeclareTable("XADM_COORD_AGRE", "ADM Technology", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_ADM_COORD", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XADM_COORD_AGRE");
                s.DeclareField("ADM", "VARCHAR(50)", null, null, null);
                s.DeclareField("TECHNOLOGY", "VARCHAR(50)", null, null, null);
            }

            s.DeclareTable("XFREC_DIST_ADM_AGRE", "Freq dist ADM Technology", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_DIST_ADM_AGRE", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFREC_DIST_ADM_AGRE");
                s.DeclareField("FREQ_MIN", "NUMBER(22,9)", null, null, null);
                s.DeclareField("FREQ_MAX", "NUMBER(22,9)", null, null, null);
                s.DeclareField("D", "NUMBER(22,9)", null, null, null);
                s.DeclareField("ID_ADM_AGRE", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ADMCoordination", "XADM_COORD_AGRE", null, "ID_ADM_AGRE", "ID");
            }

            s.DeclareTable("XFA_WIEN_CRD_EXT", "Wien coord Extended information", "WIEN_COORD_MOB");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("XNRFA_WIEN_EXT", "PRIMARY", "ID");
                s.DeclareJoin("Wiencoord", "WIEN_COORD_MOB", "CASCDEL", "ID", "ID");
                s.DeclareField("MOBSTA_FREQ_ID", "NUMBER(12,0)", null, null, null);
                s.DeclareField("MOBSTA_FREQ_TN", "VARCHAR(50)", null, null, null);
            }

            s.DeclareTable("XNRFA_REMINDER", "UCRF Reminder Messages", "REMINDER");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XNRFA_REMINDER", "PRIMARY", "ID");
                //s.DeclareField("NOTIFICATION", "NUMBER(10,0)", "eri_REMIND_NOTIFICATION", null, null); Удаленно
                s.DeclareField("NOTIFY_TYPE", "VARCHAR(20)", "eri_REMIND_NOTIFY_TYPE", null, null);
                s.DeclareField("TYPE", "VARCHAR(1)", "eri_REMIND_TYPE", null, null);
                s.DeclareField("TITLE", "VARCHAR(80)", null, null, null);
                s.DeclareField("DESCRIPTION", "VARCHAR(2000)", null, null, null);
                s.DeclareField("PRIORITY", "VARCHAR(2)", "eri_REMIND_PRIORITY", null, null);
                s.DeclareField("STATUS", "VARCHAR(1)", "eri_REMIND_STATUS", null, null);
                s.DeclareField("DELAY", "NUMBER(4,0)", null, null, null);
                s.DeclareField("ACTIVE", "NUMBER(1,0)", null, null, null);
            }

            s.DeclareTable("XNRFA_REMINDER_USER", "UCRF Reminder User Recipients", "REMINDER");
            {
                s.DeclareField("REMINDER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("USER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_REMINDER_USER", "PRIMARY", "REMINDER_ID,USER_ID");
                s.DeclareJoin("Reminder", "XNRFA_REMINDER", null, "REMINDER_ID", "ID");
                s.DeclareJoin("User", "EMPLOYEE", null, "USER_ID", "ID");
            }

            s.DeclareTable("XNRFA_REPORT", "XNRFA Report Form", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_REPORT2", "PRIMARY", "ID");
                s.DeclareField("START_DATE", "DATE", Date, "NOTNULL", null);
                s.DeclareField("DUE_DATE", "DATE", Date, "NOTNULL", null);
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("PATH", "VARCHAR(300)", null, null, null);
                //s.DeclareJoin("SpecReport", "XNRFA_SPEC_REPORT", null, "ID", "REPORT_ID");
            }

            s.DeclareTable("XNRFA_SPEC_REPORT", "XNRFA Report Details", "REC_HISTORY");
            {
                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_SPEC_REPORT2", "PRIMARY", "ID");
                s.DeclareField("DATE_IN", "DATE", Date, "NOTNULL", null);
                s.DeclareField("REPORT_ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareJoin("Report", "XNRFA_REPORT", "CASCDEL", "REPORT_ID", "ID");
                s.DeclareField("PTK", "NUMBER(6,0)", null, "NOTNULL", "0");
                s.DeclareField("TV", "NUMBER(6,0)", null, "NOTNULL", "0");
                s.DeclareField("NV", "NUMBER(6,0)", null, "NOTNULL", "0");
                s.DeclareField("PIO", "NUMBER(6,0)", null, "NOTNULL", "0");
                s.DeclareField("PVP", "NUMBER(6,0)", null, "NOTNULL", "0");
            }

            //s.DeclareTable("XNRFA_MONITOR_CONTR", "Table XNRFA_MONITOR_CONTR contracts", plugin3);
            s.DeclareTable("XNRFA_MONITOR_CONTR", "", plugin3);
            {
                s.DeclareField("ID", "NUMBER(12,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_MONITOR_CONTR", "PRIMARY", "ID");
                s.DeclareField("USERS_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Owner", "USERS", null, "USERS_ID", "ID");
                s.DeclareField("NUM_CONTRACT", "VARCHAR(40)", null, null, null);
                s.DeclareField("STATUS_CONTRACT", "VARCHAR(40)", "eri_StatusContract", null, null);
                s.DeclareField("DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DATE_TO", "DATE", Date, null, null);
                s.DeclareField("CODE_BRANCH_OFFICE", "VARCHAR(2)", "eri_CodeBranchOffice", null, null);
                s.DeclareField("OPERATION_DATE", "DATE", Date, null, null);
                s.DeclareField("CREATED_DATE", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", Date, null, null);
            }

            //===============================================
            // VIEWS
            CreateViews(s);
        }
        /// <summary>
        /// Сохдает виды
        /// </summary>
        /// <param name="s"></param>
        private static void CreateViews(IMSchema s)
        {
            //===============================================
            // ALLSTATIONS
            s.DeclareView("ALLSTATIONS", "Centralized assignments", plugin1);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_ALLSTATIONS_VIEW", "PRIMARY", "ID");
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", "eri_ALL_STATIONS", null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", "eri_TRFAStatus", null, null);
                s.DeclareField("OWNER_NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareField("PAY_OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("PayOwner", "USERS", null, "PAY_OWNER_ID", "ID");
                s.DeclareField("CODE_EDRPU", "VARCHAR(50)", null, null, null);
                //    s.DeclareField("ADDRESS", "VARCHAR(2000)", null, null, null); - hotfix 12.12.2013 - не соответствует размер поля, в базе 4000
                s.DeclareField("ADDRESS", "VARCHAR(4000)", null, null, null); //  - hotfix 12.12.2013 
                s.DeclareField("CITY", "VARCHAR(50)", "lov_CITIES", null, null);
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", "lov_SUBPROVINCES", null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", "lov_PROVINCES", null, null);
                s.DeclareField("LATITUDE", "NUMBER(10,6)", "Latitude", null, null);
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", "Longitude", null, null);
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("POWER", "NUMBER(14,10)", dBm, null, null);
                s.DeclareField("DES_EM", "VARCHAR(9)", null, null, null);
                s.DeclareField("DES_EM_SOUND", "VARCHAR(9)", null, null, null);
                s.DeclareField("ANT_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("AZIMUTH_TXT", "VARCHAR(4000)", null, null, null);
                s.DeclareField("ALTITUDE", "NUMBER(5,1)", null, null, null);
                s.DeclareField("POLARIZATION", "VARCHAR(4)", "eri_ANT_POLAR", null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(4000)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(4000)", null, null, null);
                s.DeclareField("TX_LOW_FREQ", "NUMBER(10,6)", null, null, null);
                s.DeclareField("TX_HIGH_FREQ", "NUMBER(10,6)", null, null, null);
                s.DeclareField("RX_LOW_FREQ", "NUMBER(10,6)", null, null, null);
                s.DeclareField("RX_HIGH_FREQ", "NUMBER(10,6)", null, null, null);
                s.DeclareField("CONCLUS_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("CONCLUS_DATE", "DATE", "Date", null, null);
                s.DeclareField("CONCLUS_DATE_STOP", "DATE", "Date", null, null);
                s.DeclareField("PERM_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("PERM_DATE", "DATE", "Date", null, null);
                s.DeclareField("PERM_DATE_STOP", "DATE", "Date", null, null);
                s.DeclareField("PERM_NUM_OLD", "VARCHAR(200)", null, null, null);
                s.DeclareField("PERM_DATE_OLD", "DATE", "Date", null, null);
                s.DeclareField("PERM_DATE_STOP_OLD", "DATE", "Date", null, null);
                s.DeclareField("LICENCE_NUM", "VARCHAR(200)", null, null, null);
                s.DeclareField("SAT_NAME", "VARCHAR(20)", null, null, null);
                s.DeclareField("ABONENT_COUNT", "NUMBER(10,2)", null, null, null);
                s.DeclareField("SID", "VARCHAR(100)", null, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
                s.DeclareJoin("JoinMicroMon", "XNRFA_MICROWA_MON", null, "ID", "ALLSTAT_ID");
            }

            s.DeclareView("P_CHAPPNET", "View P_CHAPPNET", plugin2);
            {
                s.DeclareField("APP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("NET_ID", "NUMBER(9,0)", null, null, null);

            }

            
			//===============================================
            // XV_MONITOR_USERS
            s.DeclareView("XV_UPDATE_POSITION", "View update position", "USERS");
            {
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(300)", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("POSTCODE", "VARCHAR(10)", null, null, null);
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareJoin("CitiesJoin", "CITIES", null, "CITY_ID", "ID");
            }
			

            
            //===============================================
            // XV_URCHMPAY
            s.DeclareView("XV_URCHMPAY", "Optimize view for generate report", "USERS");
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OBJ_ID1", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("OBJ_TABLE", "VARCHAR(20)", null, "NOTNULL", null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("PAY_OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(1000)", "lov_PROVINCES", null, null);
                s.DeclareField("PROVINCE2", "VARCHAR(50)", "lov_PROVINCES", null, null);
                s.DeclareField("ARTICLE", "VARCHAR(40)", null, null, null);
                s.DeclareField("ARTICLE2", "VARCHAR(40)", null, null, null);
                s.DeclareField("PRICE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("PRICE_ID2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("WORKS_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("WORKS_COUNT2", "NUMBER(9,0)", null, null, null);
                s.DeclareField("CHANGE_PROVINCE", "VARCHAR(100)", null, null, null);
                s.DeclareField("CHANGE_DOC_DATE", "DATE", Date, null, null);
                s.DeclareField("IS_CONFIRMED", "NUMBER(1,0)", null, "NOTNULL", "0");
                s.DeclareField("ADRESS", "VARCHAR(4000)", null, null, null); 
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    //Номер разрешения
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      //Дата ОТ
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        //Дата ДО
                s.DeclareField("DOZV_DATE_CANCEL", "DATE", Date, null, null);    //Дата анулирования
                s.DeclareField("NAME_SHIP", "VARCHAR(151)", null, null, null);
                s.DeclareField("NET_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareField("DATEOUTACTION", "DATE", "Date", null, null);
                s.DeclareField("DATESTARTACTION", "DATE", "Date", null, null);
                s.DeclareField("PRICE_STATUS", "NUMBER(9,0)", null, null, null);
            }
             

            //===============================================
            // XV_MONITOR_USERS
            s.DeclareView("XV_MONITOR_USERS", "Monitored users", "USERS");
            {

                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_MONITOR_USERS", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareField("APP_URCM_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("ApplicationPay", "XV_APPLPAYURCHM", null, "APP_URCM_ID", "ID");

                s.DeclareField("OWNER_REGIST_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("OWNER_NAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("CONTRACT_NUMB", "VARCHAR(50)", null, null, null);
                s.DeclareField("CONTRACT_STATUS", "VARCHAR(1)", "eri_StatusMonContract", null, null);
                s.DeclareField("CONTRACT_TYPE", "VARCHAR(1)", "eri_TypeContractsNew", null, null);
                s.DeclareField("APP_URCM_STATUS", "VARCHAR(50)", "eri_StatusApplUrcm", null, null);
                s.DeclareField("APP_URCM_INVOICE_STATUS", "VARCHAR(50)", "eri_InvoiceStatus", null, null);
                s.DeclareField("APP_URCM_DOC_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("APP_URCM_CREATED_DATE", "DATE", "Date", null, null);
                s.DeclareField("APP_URCM_FULL_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("APP_URCM_ADD_DOC_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("APP_URCM_ADD_CREATED_DATE", "DATE", "Date", null, null);
                s.DeclareField("APP_URCM_HASDIFF", "NUMBER(1,0)", null, null, null);
                s.DeclareField("APP_URCM_EMPLOYEE", "VARCHAR(160)", null, null, null);
                s.DeclareField("BELONG_APP_URCM_EMPLOYEE", "VARCHAR(160)", null, null, null);
                s.DeclareField("CONTRACT_DATE_FROM", "DATE", "Date", null, null);
                s.DeclareField("CONTRACT_DATE_TO", "DATE", "Date", null, null);
                s.DeclareField("CONTRACT_REGION_CODE", "VARCHAR(2)", null, null, null);
                s.DeclareField("CONTRACT_CREATED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", null, null);
                s.DeclareField("CONTRACT_MODIFIED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
                s.DeclareField("CONTRACT_CLOSED_BY", "VARCHAR(300)", null, null, null);
                s.DeclareField("CLOSED_DATE", "DATE", "Date", null, null);



            }

            //===============================================
            // XV_MONITOR_USERS
            s.DeclareView("XV_ARMY", "XV_ARMY", plugin3);
            {
                s.DeclareField("PACKET_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OWNER_NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("OWNER_NUMBER_OUT", "VARCHAR(100)", null, null, null);
                s.DeclareField("OWNER_DATE_OUT", "DATE", "Date", null, null);
                s.DeclareField("UCRF_NUMBER_IN", "VARCHAR(100)", null, null, null);
                s.DeclareField("UCRF_DATE_IN", "DATE", "Date", null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("CONTENT", "VARCHAR(20)", null, null, null);
                s.DeclareField("REGION", "VARCHAR(50)", "eri_Area", null, null);
                s.DeclareField("APPL_IN_PACKET", "NUMBER(9,0)", null, null, null);
                s.DeclareField("RFA_APPL_IN_PACKET", "NUMBER(9,0)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT", "VARCHAR(50)", null, null, null);
                s.DeclareField("UCRF_ARMY_DATE_OUT", "DATE", "Date", null, null);
                s.DeclareField("DAY_CHECK_50", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_OUT", "VARCHAR(50)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_OUT", "DATE", "Date", null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_DATE_IN", "DATE", "Date",  null, null);
                s.DeclareField("ARMY_REZULT", "VARCHAR(500)",null, null, null);
                s.DeclareField("EMC_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DRV_EMC_DATE", "DATE", "Date", null, null);
                s.DeclareField("DE_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DRV_DE_DATE", "DATE", "Date", null, null);
                s.DeclareField("DAY_CHEK_5", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DAY_CHEK_10", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DAY_CHEK_80", "NUMBER(9,0)", null, null, null);
                s.DeclareField("MEMO", "VARCHAR(4000)", null, null, null);
            }

            //===============================================
            // XV_ARMY_DOCFILES
            s.DeclareView("XV_ARMY_DOCFILES", "XV_ARMY_DOCFILES", plugin3);
            {

                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(128)", null, null, null);
                s.DeclareField("PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("DOC_TYPE", "VARCHAR(4)", null, null, null);
                s.DeclareField("DOC_REF", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_DATE", "DATE", "Date", null, null);
                s.DeclareField("EXP_DATE", "DATE", "Date", null, null);
                s.DeclareField("REC_TAB", "VARCHAR(32)", null, null, null);
                s.DeclareField("REC_ID", "NUMBER(9,0)", null, null, null);

                s.DeclareField("ARMY_UCRF_NUMBER_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_IN_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN_VALUE", "DATE", null, null, null);
                s.DeclareField("ARMY_REZULT", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_REZULT_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT_VALUE", "VARCHAR(500)", null, null, null);

            }

            //===============================================
            // XV_UCRF_DOCFILES
            s.DeclareView("XV_UCRF_DOCFILES", "XV_UCRF_DOCFILES", plugin3);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(128)", null, null, null);
                s.DeclareField("PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("DOC_TYPE", "VARCHAR(4)", null, null, null);
                s.DeclareField("DOC_REF", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_DATE", "DATE", "Date", null, null);
                s.DeclareField("EXP_DATE", "DATE", "Date", null, null);
                s.DeclareField("REC_TAB", "VARCHAR(32)", null, null, null);
                s.DeclareField("REC_ID", "NUMBER(9,0)", null, null, null);

                s.DeclareField("ARMY_UCRF_NUMBER_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_NUMBER_IN_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_UCRF_DATE_IN_VALUE", "DATE", null, null, null);
                s.DeclareField("ARMY_REZULT", "VARCHAR(500)", null, null, null);
                s.DeclareField("ARMY_REZULT_VALUE", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT", "VARCHAR(500)", null, null, null);
                s.DeclareField("UCRF_ARMY_NUMBER_OUT_VALUE", "VARCHAR(500)", null, null, null);

            }


            //===============================================
            // XV_PERM_MOB
            s.DeclareView("XV_PERM_MOB", "View of permissions MOB_STATION", "MOB_STATION");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_MOB2
            s.DeclareView("XV_PERM_MOB2", "View of permissions MOB_STATION2", "MOB_STATION2");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_MW
            s.DeclareView("XV_PERM_MW", "View of permissions MICROWA", "MICROWA");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_TV
            s.DeclareView("XV_PERM_TV", "View of permissions TV_STATION", "TV_STATION");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_FM
            s.DeclareView("XV_PERM_FM", "View of permissions FM_STATION", "FM_STATION");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_DVBT
            s.DeclareView("XV_PERM_DVBT", "View of permissions DVBT_STATION", "DVBT_STATION");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_TDAB
            s.DeclareView("XV_PERM_TDAB", "View of permissions TDAB_STATION", "TDAB_STATION");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_AS
            s.DeclareView("XV_PERM_AS", "View of permissions ABONENT_STATION", PlugTbl.XfaAbonentStation);
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_SHIP
            s.DeclareView("XV_PERM_SHIP", "View of permissions SHIP_STATION", "SHIP");
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_AM
            s.DeclareView("XV_PERM_AM", "View of permissions AMATEUR", PlugTbl.XfaAmateur);
            CreateViewForPermission(s);
            //===============================================
            // XV_PERM_ALL
            s.DeclareView("XV_PERM_ALL", "View of all permissions", plugin3);
            CreateViewForPermission(s);

            //TODO Не удалять
            ////===============================================
            //// CHANGELOG_VIEW
            //s.DeclareView("CHANGELOG_VIEW", "Change log view", "REC_HISTORY");
            //{
            //    s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
            //    s.DeclareField("CREATED_BY", "VARCHAR(200)", null, null, null);
            //}

            //TODO Не удалять
            ////===============================================
            //// XFV_POSITION
            //s.DeclareView("XFV_POSITION", "Posirion view", plugin3plus);
            //{
            //    s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareIndex("PK_XFV_POSITION", "PRIMARY", "ID");
            //    s.DeclareField("POS_ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareJoin("Position", "XFA_POSITION", null, "POS_ID", "ID");
            //}

            //TODO Не удалять
            ////===============================================
            //// XFV_EQUIP
            //s.DeclareView("XFV_EQUIP", "Equip view", plugin3plus);
            //{
            //    s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareIndex("PK_XFV_EQUIP", "PRIMARY", "ID");
            //    s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareJoin("Equipment", "XFA_EQUIP", null, "EQUIP_ID", "ID");
            //}

            //TODO Не удалять
            ////===============================================
            //// XFV_STATION
            //s.DeclareView("XFV_STATION", "Station view", plugin3plus);
            //{
            //    s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareIndex("PK_XFV_STATION", "PRIMARY", "ID");
            //    s.DeclareField("STATION_ID", "NUMBER(9,0)", null, null, null);
            //    s.DeclareJoin("Station", "XFA_ABONENT_STATION", null, "STATION_ID", "ID");
            //    s.DeclareField("FREQ_TX", "VARCHAR(1000)", null, null, null);
            //    s.DeclareField("FREQ_RX", "VARCHAR(1000)", null, null, null);
            //}

            //===============================================
            // XV_ABONENT_STATION
            CreateViewForAccess(s, "XV_ABONENT_STATION", "Branch office Abonent Stations", plugin4, PlugTbl.XfaAbonentStation, "Station");
            {
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XFA_ABONENT_STATION");
                s.DeclareField("STANDARD", "VARCHAR(10)", Standard, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ADM", "VARCHAR(3)", "eri_Adm", null, null);
                s.DeclareField("LOCATION", "VARCHAR(400)", "eri_XfaLocation", null, null);
                s.DeclareField("FACTORY_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,7)", dBW, null, null);
                s.DeclareField("EQP_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Equipment", "XFA_EQUIP", "", "EQP_ID", "ID");
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XV_FILIAL_SITE", "", "POS_ID", "ID");
                s.DeclareField("NET_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Net", "XV_NET", "DBMS", "NET_ID", "ID");
                s.DeclareField("ABONENT_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Abonent", "XV_ABONENT_OWNER", "", "ABONENT_ID", "ID");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "DBMS", "USER_ID", "ID");

                s.DeclareField("ANT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Antenna", "XV_ANTEN", null, "ANT_ID", "ID");
                s.DeclareJoin("Freqs", "XFA_FREQ", null, "ID", "STA_ID");

                s.DeclareField("BIUSE_DATE", "DATE", Date, null, null);
                s.DeclareField("EOUSE_DATE", "DATE", Date, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", BwKHz, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(100)", null, null, null);
                s.DeclareField("USE_REGION", "VARCHAR(1000)", null, null, null);
                s.DeclareField("PERM_NUM", "VARCHAR(1000)", null, null, null);
                s.DeclareField("PERM_DATE_PRINT", "DATE", Date, null, null);
                s.DeclareField("BLANK_NUM", "VARCHAR(100)", null, null, null);
                s.DeclareField("REGION_CODE", "NUMBER(5,0)", null, null, null);
                s.DeclareField("EXPL_TYPE", "VARCHAR(100)", "eri_TRFAExplType", null, null);
                s.DeclareField("FREQ_TX_TEXT", "VARCHAR(2000)", null, null, null);
                s.DeclareField("FREQ_RX_TEXT", "VARCHAR(2000)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);

                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("CALL", "VARCHAR(50)", null, null, null);
                s.DeclareField("TX_LOSSES", "NUMBER(6,2)", dB, null, null);
                s.DeclareField("AZIMUTH", "NUMBER(5,2)", Degrees, null, null);

                s.DeclareField("NUMB_EXIST_VISN", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_PRINT_DRV", "DATE", Date, null, null);
                s.DeclareField("DATE_PRINT_EXIST", "DATE", Date, null, null);
                s.DeclareField("NUMB_OLD_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_OLD_PRINT_DRV", "DATE", Date, null, null);
                s.DeclareField("DATE_OLD_PRINT_EX", "DATE", Date, null, null);
                s.DeclareField("SPEC_COND_DOZV", "VARCHAR(1000)", null, null, null); //Спец. условия дозвола
                s.DeclareField("SPEC_COND_VISN", "VARCHAR(1000)", null, null, null); //Спец. условия висновка
                s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);           //Примечания
                s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);           


            }//===============================================

            // XV_AMATEUR
            CreateViewForAccess(s, "XV_AMATEUR", "Branch office Amateurs", plugin4, PlugTbl.XfaAmateur, "Amateur");
            {
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_AMATEUR");
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XV_FILIAL_SITE", "", "POS_ID", "ID");

                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "XNRFA_ABONENT_OWNER", null, "OWNER_ID", "ID");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "", "USER_ID", "ID");


                s.DeclareField("STATUS", "VARCHAR(4)", "eri_TRFAStatus", null, null);
                s.DeclareField("TYPE_STAT", "VARCHAR(4)", "eri_XFA_TYPE_STAT", null, null);
                s.DeclareField("CATEG_OPER", "VARCHAR(4)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                s.DeclareField("APC", "VARCHAR(4)", "eri_XFA_APC", null, null);
                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("CERT", "VARCHAR(4)", null, null, null);
                /////////////////////
                s.DeclareField("NUMB_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("NUMB_PROJ_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_IN_DOZV", "DATE", Date, null, null);
                s.DeclareField("DATE_PERIOD_DOZV", "DATE", Date, null, null);
                s.DeclareField("DATE_PRINT_DOZV", "DATE", Date, null, null);
                s.DeclareField("NUMB_OLD_DOZV", "VARCHAR(50)", null, null, null);
                s.DeclareField("DATE_IN_OLD_DOZV", "DATE", Date, null, null);
                /////////////////////
                s.DeclareField("NUMB_BLANK", "VARCHAR(50)", null, null, null);
                //CERTIFICATE
                s.DeclareField("CERTIF_NUMB", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERTIF_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DATE_PRINT", "DATE", Date, null, null);
                s.DeclareField("CERTIF_DELIVERY", "DATE", Date, null, null);
                s.DeclareField("CERTIF_PATH", "VARCHAR(500)", null, null, null);
                //Spec. condition
                s.DeclareField("SPEC_COND_DOZV", "VARCHAR(1000)", null, null, null); //Спец. условия дозвола
                s.DeclareField("SPEC_COND_VISN", "VARCHAR(1000)", null, null, null); //Спец. условия висновка
                s.DeclareField("NOTE", "VARCHAR(1000)", null, null, null);           //Примечания
                //Службові поля
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }

            //===============================================
            // XV_SHIP
            CreateViewForAccess(s, "XV_SHIP", "Branch office Ships", plugin4, PlugTbl.Ship, "Ship");
            //===============================================
            // XV_EMI_STATION
            CreateViewForAccess(s, "XV_EMI_STATION", "Branch office Emitting Devices", plugin4, PlugTbl.XfaEmitterStation, "Emitter");
            {
                s.DeclareField("POS_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("Position", "XFA_POSITION", "DBMS", "POS_ID", "ID");
                s.DeclareField("FREQPLANCHAN_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareField("FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("FREQ_MIN", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("FREQ_MAX", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("BW", "NUMBER(22,8)", null, null, null);
                s.DeclareJoin("Frequency", "FREQ_PLAN_CHAN", null, "FREQPLANCHAN_ID,FREQ", "PLAN_ID,FREQ");
                s.DeclareField("USER_ID", "NUMBER(9,0)", Unsigned, null, null);
                s.DeclareJoin("User", "USERS", "DBMS", "USER_ID", "ID");
                s.DeclareField("STANDARD", "VARCHAR(10)", Standard, null, null);
                s.DeclareField("EQUIP_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Equipment", "EQUIP_BRO", null, "EQUIP_ID", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, "XFA_EMI_STATION");
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("AGL", "NUMBER(5,1)", Agl, null, null);
                s.DeclareField("CERT_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("EMI_TYPE", "VARCHAR(20)", "eri_EmitterType", null, null);
                s.DeclareField("DOP_DEVICE", "VARCHAR(1000)", null, null, null);
                s.DeclareField("POWER", "NUMBER(10,7)", null, null, null);
                s.DeclareField("STATUS", "VARCHAR(4)", null, null, null);
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("DOZV_NUM", "VARCHAR(200)", null, null, null);    
                s.DeclareField("DOZV_DATE_FROM", "DATE", Date, null, null);      
                s.DeclareField("DOZV_DATE_TO", "DATE", Date, null, null);        
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", "OLD_USER");
            }
            //===============================================
            // XV_MSREQ_CERT
            CreateViewForAccess(s, "XV_MSREQ_CERT", "Branch office Measure Equipment CERT", plugin4, PlugTbl.itblXnrfaMeasureEquipmentCertificates, "Certif");
            {
                s.DeclareField("CERT_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("CERTIFIED_BY", "VARCHAR(100)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(35)", null, null, null);

                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, "NOTNULL", "OLD_USER");
            }
            //===============================================
            //XV_ANTEN
            CreateViewForAccess(s, "XV_ANTEN", "Branch office Antennas", plugin4, PlugTbl.XfaAnten, "Anten");
            {
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_ANTEN");
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("GAIN", "NUMBER(22,8)", null, null, null);
                s.DeclareField("H_BEAMWIDTH", "NUMBER(22,8)", null, null, null);
                s.DeclareField("V_BEAMWIDTH", "NUMBER(22,8)", null, null, null);
                //   s.DeclareField("DIAGА", "VARCHAR(200)", null, null, null); - hotfix 12.12.2013 - имя поля DIAGА содержит русскую букву А 
                s.DeclareField("DIAGA", "VARCHAR(200)", null, null, null);  // hotfix 12.12.2013
                s.DeclareField("DIAGH", "VARCHAR(200)", null, null, null);
                s.DeclareField("DIAGV", "VARCHAR(200)", null, null, null);
            }
            //XV_EQUIP
            CreateViewForAccess(s, "XV_EQUIP", "Branch office Equipment", plugin4, PlugTbl.XfaEquip, "Equip");
            {
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_EQUIP");
                s.DeclareField("CODE", "VARCHAR(50)", null, null, null);
                s.DeclareField("MANUFACTURER", "VARCHAR(50)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("FAMILY", "VARCHAR(50)", null, null, null);
                s.DeclareField("TECH_IDNO", "VARCHAR(50)", null, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                s.DeclareField("MAX_POWER", "NUMBER(14,10)", dBm, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", null, null, null);
                s.DeclareField("RX_LOWER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("RX_UPPER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("LOWER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("UPPER_FREQ", "NUMBER(22,8)", FreqMHz, null, null);
                s.DeclareField("CERTIFICATE_NUM", "VARCHAR(30)", null, null, null);
                s.DeclareField("CERTIFICATE_DATE", "DATE", Date, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //XV_EQUIP_AMATEUR
            CreateViewForAccess(s, "XV_EQUIP_AMATEUR", "Branch office Amateur Equipment", plugin4, PlugTbl.XfaEquipAmateur, "Equip");
            {
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_EQUIP_AMATEUR");
                s.DeclareField("NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("DESIG_EMISSION", "VARCHAR(9)", DesigEm, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //XV_AUXILARY_EQ, 
            CreateViewForAccess(s, "XV_AUXILARY_EQ", "Branch office AUXILARY EQ", plugin4, PlugTbl.AuxiliaryEquipment, "Equip");
            {
                s.DeclareField("NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("DEV_TYPE", "VARCHAR(5)", "eri_AuxiliaryEquipmentType", null, null);
                s.DeclareField("NOTES", "VARCHAR(200)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(35)", null, null, null);
            }
            //XV_MEASURE_EQ
            CreateViewForAccess(s, "XV_MEASURE_EQ", "Branch office MEASURE EQ", plugin4, PlugTbl.MeasureEquipment, "Equip");
            {
                s.DeclareField("NAME", "VARCHAR(200)", null, null, null);
                s.DeclareField("CERT_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("DEV_TYPE", "VARCHAR(5)", "eri_MeasureEquipmentType", null, null);
                s.DeclareField("SERIAL", "VARCHAR(40)", null, null, null);
                s.DeclareField("NOTES", "VARCHAR(200)", null, null, null);
                s.DeclareField("CHECKDATE", "DATE", "Date", null, null);

                s.DeclareJoin("Certificate", "XNRFA_MSREQ_CERT", null, "CERT_ID", "ID");
            }
            //===============================================
            // ALLDECENTRALIZED
            s.DeclareView("ALLDECENTRALIZED", CLocaliz.TxT("Decentralized assignments"), plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("XV_ALLDECENTRALIZED", "PRIMARY", "ID");
                s.DeclareJoin("Application", "XNRFA_APPL", null, "ID", "ID");
                s.DeclareField("STATUS", "VARCHAR(20)", "eri_TRFAStatus", null, null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", null, null);
                s.DeclareField("USER", "VARCHAR(2000)", null, null, null);
                s.DeclareField("REGIST_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("OWNER", "VARCHAR(300)", null, null, null);
                s.DeclareField("USE_REGION", "VARCHAR(1000)", "lov_PROVINCES", null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", "lov_PROVINCES", null, null);
                s.DeclareField("ADRESS", "VARCHAR(2000)", null, null, null);
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", Longitude, null, null);
                s.DeclareField("LATITUDE", "NUMBER(10,6)", Latitude, null, null);
                s.DeclareField("TYP_RZ", "VARCHAR(4000)", null, null, null);
                s.DeclareField("DEM", "VARCHAR(4000)", null, null, null);
                s.DeclareField("POWER", "NUMBER(14,10)", W, null, null);
                s.DeclareField("SN", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(4000)", null, null, null);
                s.DeclareField("CALL_TLGR", "VARCHAR(100)", null, null, null);
                s.DeclareField("NUM_VBR", "VARCHAR(100)", null, null, null); //номер избирательного візова
                s.DeclareField("NUM_IND_SS", "VARCHAR(100)", null, null, null); //идентификатор морской подвижной службі
                s.DeclareField("NUM_INM", "VARCHAR(300)", null, null, null); //инмарсат
                s.DeclareField("NUM_COSPASS", "VARCHAR(300)", null, null, null); //коспас-сарсат
                s.DeclareField("FREQ_RX", "VARCHAR(4000)", null, null, null);
                s.DeclareField("FREQ_TX", "VARCHAR(4000)", null, null, null);
                s.DeclareField("BW", "NUMBER(15,5)", "BW/kHz", null, null);
                s.DeclareField("PERM_NUM", "VARCHAR(1000)", null, null, null);
                s.DeclareField("BEG_DATE", "DATE", Date, null, null);
                s.DeclareField("END_DATE", "DATE", Date, null, null);
                s.DeclareField("ANUL_DAT", "DATE", Date, null, null);
                s.DeclareField("BLANK_NUM", "VARCHAR(500)", null, null, null);
                s.DeclareField("BASEDOZV", "VARCHAR(4000)", null, null, null);
                s.DeclareField("SAT_NET", "VARCHAR(20)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", "eri_ALL_DECENTRSTATIONS", null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("USER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareField("PAY_OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("PayOwner", "USERS", null, "PAY_OWNER_ID", "ID");
                s.DeclareField("NAME_SHIP", "VARCHAR(151)", null, null, null);
            }
            // XV_PACKET
            CreateViewForAccess(s, "XV_PACKET", CLocaliz.TxT("Branch office Packets"), plugin4, PlugTbl.itblXnrfaPacket, "Packet");
            {
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
                s.DeclareField("TYPE", "VARCHAR(20)", "eri_TRFAApplType", "NOTNULL", null);
                s.DeclareField("CONTENT", "VARCHAR(20)", "eri_TRFAPackContent", "NOTNULL", null);
                s.DeclareField("NUMBER_IN", "VARCHAR(100)", null, "NOTNULL", null);
                s.DeclareField("NUMBER_OUT", "VARCHAR(100)", null, "NOTNULL", null);
                s.DeclareField("DATE_IN", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("DATE_OUT", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("STANDARD", "VARCHAR(50)", "eri_TRFAApplStandard", "NOTNULL", null);
                s.DeclareField("STATUS", "VARCHAR(20)", null, null, null);
                s.DeclareField("NUM_REESTR_VID", "NUMBER(9,0)", null, "NOTNULL", "1");
                s.DeclareField("STATUS_COORD", "VARCHAR(2)", null, null, null);
                s.DeclareField("CREATED_MANAGEMENT", "VARCHAR(50)", null, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(50)", null, "NOTNULL", null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
                s.DeclareJoin("DRV_Join_XV", "XNRFA_APPLPAY", "", "ID", "PACKET_ID");
                s.DeclareFieldExpr("STATE_EXPR_XV", "(case when [DRV_Join_XV.STATE]=66666 then 'Видалена' when [DRV_Join_XV.STATE]=99999 then 'Збережена' when [DRV_Join_XV.STATE]=0 then 'Відправлена в ДРВ' when [DRV_Join_XV.STATE]=1 then 'Додана в ДРВ' when [DRV_Join_XV.STATE]=2 then 'Сформовано рахунок' when [DRV_Join_XV.STATE]=3 then 'Виставлено рахунок' when [DRV_Join_XV.STATE]=4 then 'Сплачено рахунок' when [DRV_Join_XV.STATE]=5 then 'Видано документ' when [DRV_Join_XV.STATE]=-1 then 'Відхилення даних' when [DRV_Join_XV.STATE]=-42 then 'Анульвано рахунок' else ' ' end)", "VARCHAR(50)", "");
            }
            //===============================================
            // XV_APPL
            CreateViewForAccess(s, "XV_APPL", CLocaliz.TxT("Branch office Applications"), plugin4, PlugTbl.APPL, "Rfa");
            //===============================================
            //XV_ABONENT_OWNER,
            CreateViewForAccess(s, "XV_ABONENT_OWNER", CLocaliz.TxT("Branch office ABONENT_OWNER"), plugin4, PlugTbl.ABONENT_OWNER, "Owner");
            {
                s.DeclareField("NAME", "VARCHAR(300)", null, "NOTNULL", null);
                s.DeclareField("SHORTNAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("NAME_LATIN", "VARCHAR(300)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(300)", null, null, null);
                s.DeclareField("TEL", "VARCHAR(100)", null, null, null);
                s.DeclareField("REMARK", "VARCHAR(2000)", null, null, null);
                s.DeclareField("OKPO", "VARCHAR(50)", null, null, null);
                s.DeclareField("PASSPORT", "VARCHAR(500)", null, null, null);
                s.DeclareField("FILE_NUM", "VARCHAR(500)", null, null, null);
                s.DeclareField("DATE_BORN", "DATE", "Date", null, null);
                //----
                s.DeclareField("NAME1", "VARCHAR(300)", null, null, null);
                s.DeclareField("NAME_LATIN1", "VARCHAR(300)", null, null, null);
                s.DeclareField("NAME2", "VARCHAR(300)", null, null, null);
                s.DeclareField("SURNAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("SURNAME_LATIN", "VARCHAR(300)", null, null, null);
                // s.DeclareField("INDEX", "NUMBER(7,0)", null, null, null);
                s.DeclareField("INDEX", "VARCHAR(10)", null, null, null);
                s.DeclareField("OBL", "VARCHAR(300)", null, null, null);
                s.DeclareField("DISTR", "VARCHAR(300)", null, null, null);
                s.DeclareField("LOCAL", "VARCHAR(300)", null, null, null);
                s.DeclareField("FAX", "VARCHAR(100)", null, null, null);
                s.DeclareField("SER_PASSP", "VARCHAR(2)", null, null, null);
                s.DeclareField("NUM_PASSP", "VARCHAR(6)", null, null, null);
                s.DeclareField("DATE_PASSP", "DATE", "Date", null, null);
                s.DeclareField("PUB_PASSP", "VARCHAR(400)", null, null, null);
                //----
                s.DeclareField("CREATED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(50)", null, null, null);
                s.DeclareField("MODIFIED_DATE", "DATE", "Date", null, null);
                s.DeclareField("BRANCH_OFFICE_CODE", "VARCHAR(50)", null, null, null);           
            }

            //XNRFA_APPLPAY
            CreateViewForAccess(s, "XV_APPLPAY", CLocaliz.TxT("Branch office APPLPAY"), plugin4, PlugTbl.itblXnrfaDrvAppl, "Appl");
            {
                s.DeclareField("PACKET_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Packet", "XNRFA_PACKET", "CASCDEL", "PACKET_ID", "ID");
                //s.DeclareField("STATE", "NUMBER(9,0)", "eri_ApplDrvState", "NOTNULL", null);
                s.DeclareField("STATE", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("CUST_TXT1", "VARCHAR(1000)", null, null, null);
                s.DeclareField("CUST_TXT2", "VARCHAR(1000)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(1000)", null, null, null);
                s.DeclareField("DOC_DATE", "DATE", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
                s.DeclareField("DEPARTMENT", "VARCHAR(5)", null, null, null);
            }
            //XV_APPLPAYURCHM
            CreateViewForAccess(s, "XV_APPLPAYURCHM", CLocaliz.TxT("Branch office APPLPAYURCHM"), plugin4, PlugTbl.itblXnrfaDrvApplURCM, "Appl");
           //CreateViewForAccess(s, "XV_APPLPAYURCHM", CLocaliz.TxT("Branch office APPLPAYURCHM"), plugin4, PlugTbl.itblXnrfaDrvApplURCM, null);
            //s.DeclareView("XV_APPLPAYURCHM", CLocaliz.TxT("Branch office APPLPAYURCHM"), plugin4);
            {
                s.DeclareField("CONTRACTS_ID", "NUMBER(12,0)", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Owner", "USERS", "", "OWNER_ID", "ID");
                s.DeclareField("STATUS", "VARCHAR(15)", "eri_StatusApplUrcm", "NOTNULL", null);
                s.DeclareField("INVOICE_STATUS", "VARCHAR(15)", "eri_InvoiceStatus", null, null);
                s.DeclareField("CREATED_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareField("EMPLOYEE_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareJoin("Employee", "EMPLOYEE", "", "EMPLOYEE_ID", "ID");
                s.DeclareField("DOC_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("MEMO_NUMBER", "VARCHAR(100)", null, null, null);
                s.DeclareField("HASDIFF", "NUMBER(1,0)", null, null, null);
                s.DeclareField("ADD_DOC_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("ADD_CREATED_DATE", "DATE", null, null, null);
                s.DeclareField("FULL_PATH", "VARCHAR(500)", null, null, null);
                s.DeclareField("EMPLOYEE_DRV_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TYPE", "NUMBER(1,0)", null, null, null);
                s.DeclareJoin("EmployeeDRV", "EMPLOYEE", "", "EMPLOYEE_DRV_ID", "ID");
                s.DeclareField("RESPONSE_DATE", "DATE", null, null, null);
                s.DeclareJoin("ConnMonitorContract", "XNRFA_MON_CONTRACTS", "", "CONTRACTS_ID", "ID");
            }
            //XV_WORKS_URCHM
            CreateViewForAccess(s, "XV_WORKSURCHM", CLocaliz.TxT("Branch office WORKS URCHM"), plugin4, PlugTbl.itblXnrfaDrvApplWorksURCM, "ApplItem");
            //===============================================
            //XV_SITES
            s.DeclareView(PlugTbl.XvSites, CLocaliz.TxT("Branch office Sites"), plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_" + PlugTbl.XvSites, "PRIMARY", "ID");
                s.DeclareField("NAME", "VARCHAR(50)", "", "", "");
                s.DeclareField("STATUS", "VARCHAR(4)", "stat_SITES", "", "");
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", "Longitude", "", "");
                s.DeclareField("LATITUDE", "NUMBER(10,6)", "Latitude", "", "");
                s.DeclareField("FULL_ADDRESS", "VARCHAR(200)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("City", ICSMTbl.itblCities, "", "CITY_ID", "ID");
                s.DeclareField("CITY", "VARCHAR(50)", "", "", "");
                s.DeclareField("PROVINCE", "VARCHAR(50)", "", "", "");
                s.DeclareField("ADDRESS", "VARCHAR(300)", "", "", "");
                s.DeclareField("ASL", "NUMBER(9,0)", "m", null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(32)", "", "", "");
                s.DeclareJoin("DocAttachLink", "DOCLINK", "DBMS", "ID,TABLE_NAME", "REC_ID,REC_TAB");
            }
            //===============================================
            // XV_FILIAL_SITE
            s.DeclareView(PlugTbl.XvFilialSite, CLocaliz.TxT("Decentralized sites"), plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareIndex("PK_" + PlugTbl.XvFilialSite, "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XFA_POSITION");
                s.DeclareField("ADMS_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("AdmSite", "XV_SITES", "", "ADMS_ID", "ID");
                s.DeclareField("NAME", "VARCHAR(50)", null, "NOTNULL", null);
                s.DeclareField("ADM_COD", "VARCHAR(10)", "lov_POS_ADMCOD", null, null);
                s.DeclareField("CODE", "VARCHAR(9)", null, null, null);
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", Longitude, null, null);
                s.DeclareField("LATITUDE", "NUMBER(10,6)", Latitude, null, null);
                s.DeclareField("X", "NUMBER(22,8)", null, null, null);
                s.DeclareField("Y", "NUMBER(22,8)", null, null, null);
                s.DeclareField("CSYS", "VARCHAR(4)", null, null, null);
                s.DeclareField("ASL", "NUMBER(7,2)", Asl, null, null);
                s.DeclareField("COUNTRY_ID", "VARCHAR(3)", "eri_COUNTRY", null, null);
                s.DeclareField("DIST_BORDER", "NUMBER(9,3)", Distance, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(100)", null, null, null);
                s.DeclareField("CITY", "VARCHAR(50)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("City", "CITIES", "DBMS", "CITY_ID", "ID");
                s.DeclareField("POSTCODE", "VARCHAR(10)", null, null, null);
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", "lov_SUBPROVINCES", null, null);
                s.DeclareField("PROVINCE", "VARCHAR(50)", "lov_PROVINCES", null, null);
                s.DeclareField("TEL", "VARCHAR(20)", null, null, null);
                s.DeclareField("FAX", "VARCHAR(20)", null, null, null);
                s.DeclareField("EMAIL", "VARCHAR(50)", null, null, null);
                s.DeclareField("REMARK", "VARCHAR(2000)", null, null, null);
                s.DeclareField("TOWER", "VARCHAR(32)", "lov_STATION_TOWER", null, null);
                s.DeclareField("PLACE", "VARCHAR(300)", null, null, null);
                s.DeclareField("REF_CADASTRE", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUST_TXT1", "VARCHAR(50)", null, null, null);
                s.DeclareField("CUST_TXT3", "VARCHAR(100)", null, null, null);
                s.DeclareField("CUST_TXT4", "VARCHAR(10)", null, null, null);
                s.DeclareField("CUST_TXT7", "VARCHAR(1000)", null, null, null);
                s.DeclareField("CUST_TXT8", "VARCHAR(50)", null, null, null);
                //-----
                s.DeclareField("DATE_CREATED", "DATE", Date, null, null);
                s.DeclareField("CREATED_BY", "VARCHAR(30)", null, null, null);
                s.DeclareField("DATE_MODIFIED", "DATE", Date, null, null);
                s.DeclareField("MODIFIED_BY", "VARCHAR(30)", null, null, null);
            }
            //===============================================
            //XV_ALL_POSITIONS
            s.DeclareView(PlugTbl.XvAllPosition, CLocaliz.TxT("Branch office All Positions"), plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_" + PlugTbl.XvAllPosition, "PRIMARY", "ID");
                s.DeclareField("REF_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(50)", "", "", "");
                s.DeclareField("LONGITUDE", "NUMBER(10,6)", "Longitude", "", "");
                s.DeclareField("LATITUDE", "NUMBER(10,6)", "Latitude", "", "");
                s.DeclareField("FULL_ADDRESS", "VARCHAR(200)", null, null, null);
                s.DeclareField("CITY_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("City", ICSMTbl.itblCities, "", "CITY_ID", "ID");
                s.DeclareField("CITY", "VARCHAR(50)", "", "", "");
                s.DeclareField("SUBPROVINCE", "VARCHAR(50)", "", "", "");
                s.DeclareField("PROVINCE", "VARCHAR(50)", "", "", "");
                s.DeclareField("ADDRESS", "VARCHAR(300)", "", "", "");
                s.DeclareField("ASL", "NUMBER(9,0)", "m", null, null);
            }
            //===============================================
            // XV_FILIAL_EQUIP
            /* 21/11/2012 - think it's useless - SK
            s.DeclareView("XV_FILIAL_EQUIP", CLocaliz.TxT("Decentralized equipment"), plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_FILIAL_EQUIP", "PRIMARY", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, "NOTNULL", "XV_FILIAL_EQUIP");
                s.DeclareField("EQUIP_NAME", "VARCHAR(50)", null, null, null);
                s.DeclareField("DESIG_EM", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_NUMB", "VARCHAR(50)", null, null, null);
                s.DeclareField("CERT_DATE", "DATE", "Date", null, null);
                s.DeclareField("POWER", "NUMBER(10,6)", dBW, null, null);
                s.DeclareField("BW", "NUMBER(10,6)", BwKHz, null, null);
            }
            */

            //===============================================
            // XV_NET
            s.DeclareView("XV_NET", "Net view", plugin4);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_NET", "PRIMARY", "ID");
                s.DeclareJoin("Net", "XFA_NET", null, "ID", "ID");
                s.DeclareJoin("BaseStation", "XFA_REF_BASE_STA", null, "ID", "NET_ID");
                s.DeclareJoin("Abonent", "XFA_ABONENT_STATION", null, "ID", "NET_ID");
                s.DeclareField("NAME", "VARCHAR(300)", null, null, null);
                s.DeclareField("CALL_SIGN", "VARCHAR(200)", null, null, null);
                //s.DeclareField("ABONENT_COUNT", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("BASE_COUNT", "NUMBER(9,0)", null, null, null);
                //s.DeclareField("ALL_COUNT", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ABONENT_COUNT_USED", "NUMBER(9,0)", null, null, null);
                s.DeclareField("BASE_COUNT_USED", "NUMBER(9,0)", null, null, null);
                s.DeclareField("ALL_COUNT_USED", "NUMBER(9,0)", null, null, null);
            }
            //===============================================
            // XV_TAX_PAYER_FORM1
            s.DeclareView("XV_TAX_PAYER_FORM1", "Tax payer (Form 1)", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_TAX_PAYER_FORM1", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareField("LICENCE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Licence", "LICENCE", "", "LICENCE_ID", "ID");
                s.DeclareField("LIC_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("LIC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("LIC_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("FEES_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Fees", "XNRFA_FEES", "", "FEES_ID", "ID");
                s.DeclareField("PROVINCE", "VARCHAR(500)", "lov_PROVINCES", null, null);
                s.DeclareField("BW", "NUMBER(10,6)", FreqMHz, null, null);
                s.DeclareField("CHAL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("ChAllot", "CH_ALLOTMENTS", null, "CHAL_ID", "ID");
            }
            //===============================================
            // XV_TAX_PAYER_FORM2
            s.DeclareView("XV_TAX_PAYER_FORM2", "Tax payer (Form 2)", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_TAX_PAYER_FORM1", "PRIMARY", "ID");
                s.DeclareField("OWNER_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("OWNER_CODE", "VARCHAR(20)", null, null, null);
                s.DeclareField("OWNER_ADDRESS", "VARCHAR(100)", null, null, null);
                s.DeclareField("OWNER_PROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOC_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("FREQ", "NUMBER(22,8)", null, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(1000)", null, null, null);
                s.DeclareField("RX_FREQ_TXT", "VARCHAR(1000)", null, null, null);
                s.DeclareField("ADDRESS", "VARCHAR(500)", null, null, null);
                s.DeclareField("PROVINCE", "VARCHAR(1000)", "lov_PROVINCES", null, null);
                s.DeclareField("BW", "NUMBER(10,6)", FreqMHz, null, null);
                s.DeclareField("FEES_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Fees", "XNRFA_FEES", "", "FEES_ID", "ID");
                s.DeclareField("TABLE_NAME", "VARCHAR(20)", null, null, null);
                s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("STANDARD", "VARCHAR(20)", null, null, null);
                s.DeclareField("CLASS", "VARCHAR(20)", null, null, null);
                s.DeclareField("USER_AB_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareField("EXPL_TYPE", "VARCHAR(100)", "eri_TRFAExplType", null, null);
                s.DeclareField("DOZV_DATE_CANCEL", "DATE", Date, null, null);
                s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
            }

            //===============================================
            // XV_TAX_PAYER_FORM3
            s.DeclareView("XV_TAX_PAYER_FORM3", "Tax payer (Form 3)", plugin3plus);
            {
                s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
                s.DeclareIndex("PK_XV_TAX_PAYER_FORM1", "PRIMARY", "ID");
                s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Owner", "USERS", null, "OWNER_ID", "ID");
                s.DeclareField("OWNER_NAME", "VARCHAR(100)", null, null, null);
                s.DeclareField("OWNER_CODE", "VARCHAR(20)", null, null, null);
                s.DeclareField("OWNER_ADDRESS", "VARCHAR(100)", null, null, null);
                s.DeclareField("OWNER_PROVINCE", "VARCHAR(50)", null, null, null);
                s.DeclareField("LICENCE_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Licence", "LICENCE", "", "LICENCE_ID", "ID");
                s.DeclareField("LIC_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("LIC_DATE_FROM", "DATE", null, null, null);
                s.DeclareField("DOC_NUM", "VARCHAR(50)", null, null, null);
                s.DeclareField("DOC_DATE_FROM", "DATE", Date, null, null);
                s.DeclareField("DOC_DATE_TO", "DATE", Date, null, null);
                s.DeclareField("PWR_ANT", "NUMBER(10,6)", "dBW", null, null);
                s.DeclareField("PWR_W", "NUMBER(10,6)", "W", null, null);
                s.DeclareField("ADDRESS", "VARCHAR(500)", null, null, null);
                s.DeclareField("FEES_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Fees", "XNRFA_FEES", "", "FEES_ID", "ID");
                s.DeclareField("PROVINCE", "VARCHAR(500)", "lov_PROVINCES", null, null);
                s.DeclareField("BW", "NUMBER(10,6)", FreqMHz, null, null);
                s.DeclareField("TX_FREQ_TXT", "VARCHAR(500)", null, null, null);
                s.DeclareField("DOZV_DATE_CANCEL", "DATE", Date, null, null);
                s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
                s.DeclareJoin("Appl", "XNRFA_APPL", null, "APPL_ID", "ID");
            }
        }

        /// <summary>
        /// Сохдает схему вида для разрешенией
        /// </summary>
        /// <param name="s"></param>
        private static void CreateViewForPermission(IMSchema s)
        {
            s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            s.DeclareField("TABLE_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareField("TABLE_NAME", "VARCHAR(30)", null, null, null);
            s.DeclareField("STANDARD", "VARCHAR(50)", null, null, null);
            s.DeclareField("OWNER_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareField("APPL_ID", "NUMBER(9,0)", null, null, null);
            s.DeclareJoin("Application", "XNRFA_APPL", null, "APPL_ID", "ID");
            s.DeclareField("PERM_NUM", "VARCHAR(100)", null, null, null);
            s.DeclareField("PERM_DATE", "DATE", Date, null, null);
            s.DeclareField("PERM_DATE_STOP", "DATE", Date, null, null);
            s.DeclareField("PERM_NUM_OLD", "VARCHAR(100)", null, null, null);
            s.DeclareField("PERM_DATE_OLD", "DATE", Date, null, null);
            s.DeclareField("PERM_DATE_STOP_OLD", "DATE", Date, null, null);
            s.DeclareField("FILE_PATH", "VARCHAR(2000)", null, null, null);
        }

        /// <summary>
        /// Создает схему вида для распределения доступа
        /// </summary>
        /// <param name="s"></param>
        private static void CreateViewForAccess(IMSchema s, string viewName, string viewDecs, string tableGroup, string tableName, string joinName)
        {
            s.DeclareView(viewName, viewDecs, tableGroup);
            s.DeclareField("ID", "NUMBER(9,0)", null, null, null);
            s.DeclareIndex("PK_" + viewName, "PRIMARY", "ID");
            s.DeclareJoin(joinName, tableName, null, "ID", "ID");
        }
        //=============================================================
        /// <summary>
        /// Обновление таблиц
        /// </summary>
        /// <param name="s">Схема ICSM</param>
        /// <param name="dbCurVersion">Текущая версия БД</param>
        /// <returns>true</returns>
        public static bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            if (dbCurVersion < 20140409.2248)
            {
                s.CreateTableFields("XNRFA_APPLPAYURCM", "CONTRACTS_ID");
                s.SetDatabaseVersion(20140409.2248);
            }

            if (dbCurVersion < 20140407.1000)
            {
                s.CreateTables("XNRFA_MON_CONTRACTS");
                s.SetDatabaseVersion(20140430.1200);
            }


            if (dbCurVersion < 20140403.1200)
            {
                s.CreateTables("XNRFA_LINKS_SITES");
                s.SetDatabaseVersion(20140430.1200);
            }

            if (dbCurVersion < 20140614.1200)
            {
                s.CreateTables("XNRFA_DOCS");
                s.SetDatabaseVersion(20140614.1200);
            }

            if (dbCurVersion < 20140407.1000)
            {
                s.CreateTables("XNRFA_DOCFILES");
                s.SetDatabaseVersion(20140430.1200);
            }



            if (dbCurVersion < 20100426.1019)
            {
                s.CreateTables("XNRFA_EXTERN_FILTERS,XNRFA_FILTER_PARAM");
                s.SetDatabaseVersion(20100426.1019);
            }

            if (dbCurVersion < 20100706.0927)
            {
                s.CreateTables("XRFA_CHANGE_LOG");
                s.SetDatabaseVersion(20100706.0927);
            }

            if (dbCurVersion < 20100713.1444)
            {
                s.CreateTables("XNRFA_STAT_EXFLTR");
                s.SetDatabaseVersion(20100713.1444);
            }

            if (dbCurVersion < 20100719.1200)
            {
                // initial creation of tables
                s.CreateTables("XRFA_MOB2_SECTOR");
                s.SetDatabaseVersion(20100719.1200);
            }

            if (dbCurVersion < 20100720.1300)
            {
                // Обновляю поля таблицы XRFA_MOB2_SECTOR
                s.UpdateTableFields("XRFA_MOB2_SECTOR", "MOB_STATION2_ID,NUM");
                s.CreateTables("XRFA_MOB_SECTOR");
                s.SetDatabaseVersion(20100720.1300);
            }

            if (dbCurVersion < 20100721.1411)
            {
                // Обновляю поля таблицы XRFA_MOB2_SECTOR
                s.UpdateTableFields(PlugTbl.itblXnrfaMob2Sector, "TYPE,NAME,CREATED_BY,MODIFIED_BY");
                s.SetDatabaseVersion(20100721.1411);
            }

            // Удалена
            //if (dbCurVersion < 20100722.1448)
            //{// Создаем таблицу "XNRFA_STATION_LIC"
            //   s.CreateTables("XNRFA_STATION_LIC");
            //   s.SetDatabaseVersion(20100722.1448);
            //}

            if (dbCurVersion < 20100723.1229)
            {
                // Удадаляю таблицы XRFA_MOB_SECTOR и XRFA_MOB2_SECTOR - они уже не нужны
                s.DeleteTables("XRFA_MOB_SECTOR,XRFA_MOB2_SECTOR", false);
                // Создаем таблицу Пакета ("XNRFA_PACKET") и заявки ("XNRFA_APPL")
                s.CreateTables("XNRFA_PACKET,XNRFA_APPL");
                s.SetDatabaseVersion(20100723.1229);
            }

            if (dbCurVersion < 20100723.1649)
            {
                // Удадаляю таблицы XRFA_CHANGE_LOG - немношко не та структура
                s.DeleteTables("XRFA_CHANGE_LOG", false);
                // Создаем таблицу XNRFA_CHANGE_LOG - новая структура
                s.CreateTables("XNRFA_CHANGE_LOG");
                s.SetDatabaseVersion(20100723.1649);
            }

            if (dbCurVersion < 20100723.1744)
            {
                // Обновляю поля, чтоб они могли быть NULL
                s.UpdateTableFields(PlugTbl.itblXnrfaAppl, "OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                s.SetDatabaseVersion(20100723.1744);
            }

            if (dbCurVersion < 20100724.1138)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaChangeLog, "PATH"); //Добавил поле PATH
                s.CreateTableFields(PlugTbl.itblXnrfaPacket,
                                    "OWNER_ID,TYPE,NUMBER_IN,NUMBER_OUT,DATE_IN,DATE_OUT,STANDARD");
                //Добавил поле в пакет
                s.SetDatabaseVersion(20100724.1138);
            }

            if (dbCurVersion < 20100727.1350)
            {
                s.CreateTables(PlugTbl.itblXnrfaSCPattern); //Создаем таблицу "шаблонів особливих умов" 
                s.SetDatabaseVersion(20100727.1350);
            }

            if (dbCurVersion < 20100730.1000)
            {
                // Создаем таблицу событий
                s.CreateTables(PlugTbl.itblXnrfaEventLog);
                s.SetDatabaseVersion(20100730.1000);
            }

            if (dbCurVersion < 20100730.1419)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaPacket, "CONTENT");
                s.CreateTableFields(PlugTbl.itblXnrfaAppl, "EMPLOYEE_ID,CREATE_DATE,STANDARD");
                s.SetDatabaseVersion(20100730.1419);
            }

            if (dbCurVersion < 20100809.1601)
            {
                // Добавил вспомагательные поля а таблицу заявок
                s.CreateTableFields(PlugTbl.itblXnrfaAppl,
                                    "CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_TXT10");
                s.CreateTableFields(PlugTbl.itblXnrfaAppl,
                                    "CUST_TXT11,CUST_TXT12,CUST_TXT13,CUST_TXT14,CUST_TXT15,CUST_TXT16,CUST_TXT17,CUST_TXT18,CUST_TXT19,CUST_TXT20");
                s.CreateTableFields(PlugTbl.itblXnrfaAppl, "TABLE_NAME");
                // s.UpdateTableFields("XNRFA_STATION_LIC", "OBJ_ID,OBJ_TABLE"); (Удалена)
                s.SetDatabaseVersion(20100809.1601);
            }

            if (dbCurVersion < 20100819.1105)
            {
                // Добавляем таблицу для глобавльных идентификаторов
                s.CreateTables(PlugTbl.itblXnrfaGUID);
                s.SetDatabaseVersion(20100819.1105);
            }

            if (dbCurVersion < 20100820.1318)
            {
                s.DeleteTables("XNRFA_STATION_LIC", false);
                s.CreateTables("XNRFA_APPL_LIC");
                s.SetDatabaseVersion(20100820.1318);
            }

            if (dbCurVersion < 20100901.1329)
            {
                s.CreateTables(PlugTbl.itblXnrfaDrvAppl);
                s.CreateTables(PlugTbl.itblXnrfaDrvApplApps);
                s.CreateTables(PlugTbl.itblXnrfaDrvApplWorks);
                s.SetDatabaseVersion(20100901.1329);
            }

            if (dbCurVersion < 20100908.1245)
            {
                s.UpdateTableFields(PlugTbl.itblXnrfaDrvApplWorks, "NOTES");
                s.SetDatabaseVersion(20100908.1245);
            }

            if (dbCurVersion < 20100910.0900)
            {
                s.CreateTables("XNRFA_PRICE");
                s.SetDatabaseVersion(20100910.0900);
            }

            if (dbCurVersion < 20100914.1149)
            {
                s.CreateTables("XNRFA_ABONENT_LINKS");
                s.SetDatabaseVersion(20100914.1149);
            }

            if (dbCurVersion < 20100923.1644)
            {
                s.CreateTableFields("XNRFA_PRICE", "DEFAULT");
                s.SetDatabaseVersion(20100923.1644);
            }

            if (dbCurVersion < 20100930.0830)
            {
                s.UpdateTableFields("XNRFA_GUID", "GUID_ID");
                s.SetDatabaseVersion(20100930.0830);
            }

            if (dbCurVersion < 20101004.0930)
            {
                s.CreateTableFields("XNRFA_APPLPAY", "CUST_TXT1,CUST_TXT2,CUST_TXT3");
                s.SetDatabaseVersion(20101004.0930);
            }

            if (dbCurVersion < 20101008.0830)
            {
                s.CreateTables("XNRFA_ABONENT_OWNER,XNRFA_OWNER_LINK");
                s.SetDatabaseVersion(20101008.0830);
            }

            if (dbCurVersion < 20101008.0835)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE");
                s.SetDatabaseVersion(20101008.0835);
            }

            if (dbCurVersion < 20101008.1515)
            {
                s.UpdateTableFields("XNRFA_APPL", "OBJ_ID1,OBJ_TABLE");
                s.SetDatabaseVersion(20101008.1515);
            }

            if (dbCurVersion < 20101018.0845)
            {
                s.CreateTableFields("XNRFA_EVENT_LOG", "VISIBLE");
                s.SetDatabaseVersion(20101018.0845);
            }

            if (dbCurVersion < 20101018.1544)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "ADDRESS,TEL,REMARK,OKPO,PASSPORT");
                s.SetDatabaseVersion(20101018.1544);
            }

            if (dbCurVersion < 20101020.1352)
            {
                s.CreateTables(
                    "XNRFA_POSITION,XNRFA_DIFF_MOBSTA2,XNRFA_DIFF_TV_STAT,XNRFA_DIFF_FM_STAT,XNRFA_MOBSTA2_FREQ,XNRFA_DIFF_MOBSTA,XNRFA_MOBSTA_FREQ");
                s.SetDatabaseVersion(20101020.1352);
            }

            if (dbCurVersion < 20101104.1604)
            {
                s.CreateTables("XNRFA_DIFF_ET_STAT,XNRFA_DIFF_ET_FREQ,XNRFA_DIFF_MW_STAT");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "POS_ID,POS_TABLE");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "POS_ID,POS_TABLE");
                s.SetDatabaseVersion(20101104.1604);
            }

            if (dbCurVersion < 20101116.1325)
            {
                s.CreateTables("XNRFA_DIFF_DVB_STAT");
                s.SetDatabaseVersion(20101116.1325);
            }

            if (dbCurVersion < 20101117.0001)
            {
                s.CreateTableFields("XNRFA_APPLPAY", "DEPARTMENT");
                s.CreateTableFields("XNRFA_APPL", "TRSMTRS_COUNT");
                s.SetDatabaseVersion(20101117.0001);
            }
            if (dbCurVersion < 20101122.1439)
            {
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "NETWORK_IDENT,TYPE,CLASS");
                s.SetDatabaseVersion(20101122.1439);
            }
            if (dbCurVersion < 20101118.0900)
            {
                s.CreateTables("XNRFA_LOCKS,XNRFA_SYS_CONFIG");
                s.SetDatabaseVersion(20101118.0900);
            }
            if (dbCurVersion < 20101123.1101)
            {
                s.UpdateTableFields("XNRFA_APPLWORKS", "ARTICLE");
                s.SetDatabaseVersion(20101123.1101);
            }

            if (dbCurVersion < 20101123.1214)
            {
                s.DeleteTableFields("XNRFA_APPL", "Agreement,MemoToDRV,PTKDate");
                //Поля удалены
                //s.CreateTableFields("XNRFA_APPL", "AGREEMENT,MEMOTODRV,PTKDATE");
                s.CreateTables("XNRFA_ALL_STAT");

                s.SetDatabaseVersion(20101123.1214);
            }

            if (dbCurVersion < 20101124.1944)
            {
                s.CreateTables("XNRFA_DIFF_FMDIG");
                s.SetDatabaseVersion(20101124.1944);
            }

            if (dbCurVersion < 20101124.1946)
            {
                s.DeleteTableFields("XNRFA_APPLWORKS", "INDEX");
                s.CreateTableFields("XNRFA_APPLWORKS", "COEFFICIENT");
                s.SetDatabaseVersion(20101124.1946);
            }
            if (dbCurVersion < 20101202.1731)
            {
                s.CreateTableFields("XNRFA_APPL", "STATE_DOC");
                s.CreateTableFields("XNRFA_PRICE", "DEFAULTTYPE");
                s.SetDatabaseVersion(20101202.1731);
            }

            if (dbCurVersion < 20101206.1550)
            {
                s.CreateTableFields("XNRFA_APPL",
                                    "CUST_TXT21,CUST_TXT22,CUST_TXT23,CUST_DAT1,CUST_DAT2,CUST_DAT3,CUST_DAT4,CUST_DAT5,PRICE_ID");
                s.SetDatabaseVersion(20101206.1550);
            }

            if (dbCurVersion < 20101209.1337)
            {
                s.CreateTables("XNRFA_APPLPAYURCM");
                s.CreateTables("XNRFA_CHAPPURCM");
                s.SetDatabaseVersion(20101209.1337);
            }

            if (dbCurVersion < 20140528.1337)
            {
                s.CreateTables("XNRFA_CHAPPNET");
                s.SetDatabaseVersion(20140528.1337);

            }


            if (dbCurVersion < 20101210.1017)
            {
                s.CreateTables("XNRFA_PAC_TO_APPL");
                s.SetDatabaseVersion(20101210.1017);
            }

            if (dbCurVersion < 20101210.1018)
            {
                s.DeleteTableFields("XNRFA_APPL", "PACKET_ID");
                s.SetDatabaseVersion(20101210.1018);
            }

            if (dbCurVersion < 20101213.1449)
            {
                s.UpdateTableFields("XNRFA_APPLPAYURCM", "MEMO_NUMBER");
                s.SetDatabaseVersion(20101213.1449);
            }

            if (dbCurVersion < 20101217.0939)
            {
                s.UpdateTableFields("XNRFA_DIFF_MOBSTA,XNRFA_PACKET,XNRFA_APPL,XNRFA_PRICE", "STANDARD");
                s.SetDatabaseVersion(20101217.0939);
            }

            if (dbCurVersion < 20101220.1300)
            {
                s.CreateTableFields("XNRFA_PRICE", "LIMITTYPE,MIN,MAX");
                s.SetDatabaseVersion(20101220.1300);
            }

            if (dbCurVersion < 20101222.1701)
            {
                s.CreateTableFields("XNRFA_APPLPAYURCM", "HASDIFF,ADD_DOC_PATH,ADD_CREATED_DATE");
                s.SetDatabaseVersion(20101222.1701);
            }

            if (dbCurVersion < 20101223.1038)
            {
                s.CreateTableFields("XNRFA_APPLWORKS", "LINKNUM");
                s.SetDatabaseVersion(20101223.1038);
            }

            if (dbCurVersion < 20110106.1404)
            {
                s.CreateTableFields("XNRFA_APPLPAY", "DOC_DATE");
                s.SetDatabaseVersion(20110106.1404);
            }

            if (dbCurVersion < 20110106.1500)
            {
                s.CreateTables("XNRFA_MONITORING,XNRFA_LOGS");
                s.SetDatabaseVersion(20110106.1500);
            }

            if (dbCurVersion < 20110111.1700)
            {
                s.DeleteTableFields("XNRFA_LOGS", "WHEN,WHO");
                s.CreateTableFields("XNRFA_LOGS", "DATE_CREATED,CREATED_BY,WHAT");
                s.SetDatabaseVersion(20110111.1700);
            }

            if (dbCurVersion < 20110114.1024)
            {
                s.CreateTableFields("XNRFA_APPLPAY", "OWNER_ID");
                s.SetDatabaseVersion(20110114.1024);
            }

            if (dbCurVersion < 20110124.1444)
            {
                s.CreateTableFields("XNRFA_APPLPAYURCM", "FULL_PATH");
                s.SetDatabaseVersion(20110124.1444);
            }

            if (dbCurVersion < 20110131.1048)
            {
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "DESIG_EM");
                s.SetDatabaseVersion(20110131.1048);
            }

            if (dbCurVersion < 20110208.1227)
            {
                s.CreateTableFields("XNRFA_APPLWORKS", "ADDITIONAL_NOTES");
                s.CreateTables("XNRFA_STATE_XML135");
                s.SetDatabaseVersion(20110208.1227);
            }

            if (dbCurVersion < 20110211.1643)
            {
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "BW,CERT_NUM,CERT_DATE,GAIN,T_BMWDTH");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT",
                                    "AZM_TO,EVEL_MIN,RX_DESIG_EM,TX_DESIG_EM,TX_BW,RX_BW,DIAMETER,CERT_NUM,CERT_DATE");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "CERT_NUM,CERT_DATE");
                s.CreateTables("XNRFA_CHAN_MOBSTA");

                s.SetDatabaseVersion(20110211.1643);
            }

            if (dbCurVersion < 20110222.1609)
            {
                //s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "BW,CERT_NUM,CERT_DATE,GAIN,T_BMWDTH");
                //s.CreateTableFields("XNRFA_DIFF_ET_STAT", "AZM_TO,EVEL_MIN,RX_DESIG_EM,TX_DESIG_EM,TX_BW,RX_BW,DIAMETER,CERT_NUM,CERT_DATE");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "CERT_NUM,CERT_DATE");
                //s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "CERT_NUM,CERT_DATE");

                s.CreateTableFields("XNRFA_DIFF_FMDIG", "CERT_NUM,CERT_DATE");
                //s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "DESIG_EM");
                //s.CreateTableFields("XNRFA_DIFF_MOBSTA", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "CERT_NUM,CERT_DATE");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "CERT_NUM,CERT_DATE");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "CERT_NUM,CERT_DATE");
                //s.CreateTableFields("XNRFA_DIFF_ET_STAT", "DESIG_EM");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "CERT_NUM,CERT_DATE");

                //s.CreateTables("XNRFA_CHAN_MOBSTA");

                s.SetDatabaseVersion(20110222.1609);
            }

            if (dbCurVersion < 20110225.1029)
            {
                s.CreateTableFields("XNRFA_CHAPPURCM", "COUNT");
                s.SetDatabaseVersion(20110225.1029);
            }

            if (dbCurVersion < 20110228.1033)
            {
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "AZIMUTH_1,AZIMUTH_2,BW,DIAMETER_1,DIAMETER_2");
                s.SetDatabaseVersion(20110228.1033);
            }

            if (dbCurVersion < 20110302.1410)
            {
                s.CreateTables("XNRFA_WORKS_URCM");
                s.SetDatabaseVersion(20110302.1410);
            }

            if (dbCurVersion < 20110302.1411)
            {
                s.CreateTableFields("XNRFA_PACKET", "STATUS");
                s.SetDatabaseVersion(20110302.1411);
            }

            if (dbCurVersion < 20110303.1306)
            {
                s.CreateTableFields("XNRFA_CHAPPURCM", "ADRES");
                s.SetDatabaseVersion(20110303.1306);
            }

            if (dbCurVersion < 20110309.1323)
            {
                s.CreateTableFields("XNRFA_STATE_XML135", "MESSAGE");
                s.SetDatabaseVersion(20110309.1323);
            }

            //Удаленны
            //if (dbCurVersion < 20110318.1305)
            //{
            //   s.CreateTables("XF_ANTENNA");
            //   s.CreateTables("XF_EQUIP");
            //   s.CreateTables("XF_POSITION");
            //   s.CreateTables("XF_MOB_STATION");
            //   s.CreateTables("XF_FREQ");
            //   s.SetDatabaseVersion(20110318.1305);
            //}

            if (dbCurVersion < 20110324.1537)
            {
                s.CreateTables("XNRFA_STATUS_WF");
                s.SetDatabaseVersion(20110324.1537);
            }

            if (dbCurVersion < 20110328.1612)
            {
                // Поля удалены
                //s.CreateTableFields("XNRFA_APPL", "AGREEMENT_PTK,MEMOTODRV_PTK,STATE_DOC_PTK");
                s.SetDatabaseVersion(20110328.1612);
            }

            if (dbCurVersion < 20110401.1201)
            {
                s.CreateTables("XNRFA_STATUS,XN_CHANGE_LOG");
                s.SetDatabaseVersion(20110401.1201);
            }

            if (dbCurVersion < 20110405.0825)
            {
                s.CreateTableFields("XNRFA_APPL", "WAS_USED");
                s.SetDatabaseVersion(20110405.0825);
            }

            if (dbCurVersion < 20110414.1241)
            {
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "SATTELITE,ANTENNA_NAME,EQUIP_NAME");
                s.SetDatabaseVersion(20110414.1241);
            }

            // Поля удалены
            //if (dbCurVersion < 20110420.1132)
            //{
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "equip");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "powVideo");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "powAudio");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "rxBand");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "emiVideo");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "emiAudio");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "certNo");
            //    s.CreateTableFields("XNRFA_DIFF_TV_STAT", "certDa");
            //    s.SetDatabaseVersion(20110420.1132);
            //}

            if (dbCurVersion < 20110427.1531)
            {
                s.CreateTableFields("XNRFA_STATE_XML135", "DOCUMENT");
                s.SetDatabaseVersion(20110427.1531);
            }

            if (dbCurVersion < 20110428.0909)
            {
                s.CreateTables("XNRFA_SPEC_COND");
                s.SetDatabaseVersion(20110428.0909);
            }

            if (dbCurVersion < 20110428.1154)
            {
                s.CreateTableFields("XNRFA_SPEC_COND", "TEXT_CONDITION");
                s.SetDatabaseVersion(20110428.1154);
            }

            if (dbCurVersion < 20110510.0944)
            {
                s.CreateTables("XFA_EQUIP");
                s.CreateTables("XFA_POSITION");
                s.CreateTables("XFA_NET");
                s.CreateTables("XFA_REF_BASE_STA");
                s.CreateTables("XFA_ABONENT_STATION");
                s.CreateTables("XFA_FREQ");
                s.SetDatabaseVersion(20110510.0944);
            }

            if (dbCurVersion < 20110510.1341)
            {
                s.CreateTableFields("XNRFA_APPL", "PAY_OWNER_ID");
                s.CreateTableFields("XNRFA_PAC_TO_APPL", "CREATED_DATE");
                s.SetDatabaseVersion(20110510.1341);
            }

            if (dbCurVersion < 20110510.1342)
            {
                s.CreateTableFields("XNRFA_APPL", "WORKS_COUNT");
                s.SetDatabaseVersion(20110510.1342);
            }

            if (dbCurVersion < 20110530.1418)
            {
                s.CreateTableFields("XNRFA_POSITION", "COUNTRY_ID,TABLE_NAME,X,Y,ASL,CSYS,CODE");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "ANTENNA1_NAME,ANTENNA2_NAME,EQUIPMENT_NAME");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "GAIN1,GAIN2,MODULATION");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "BW");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MODULATION");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "RXMODULATION,TXMODULATION,TXGAIN,RXGAIN");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "BW,MAX_KOEF_G,MAX_KOEF_V");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MAX_KOEF_G,MAX_KOEF_V");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "FREQ_RECEIVE,FREQ_SEND,WIDTH_ANT");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "AGL,EQUIP_NAME,ANTENNA_NAME");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "CERT_NUM,CERT_DATE,BW");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "ANT_WIDTH");
                s.CreateTableFields("XFA_ABONENT_STATION", "REGION_CODE");
                s.CreateTableFields("XFA_ABONENT_STATION", "EXPL_TYPE");
                s.CreateTableFields("XFA_POSITION", "TABLE_NAME,X,Y,CSYS,CITY,CUST_TXT1");
                s.CreateTableFields("XFA_EQUIP", "BW");
                s.CreateTableFields("XFA_ABONENT_STATION", "FREQ_TX_TEXT,FREQ_RX_TEXT");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "ANTEN_TYPE");
                s.CreateTableFields("XFA_REF_BASE_STA", "STA_TABLE");
                s.CreateTableFields("XFA_ABONENT_STATION", "STATUS");
                s.UpdateTableFields("XNRFA_APPL", "OBJ_ID1,OBJ_TABLE");
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "FILE_NUM");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "ANTEN_TYPE,EQUIP_NAME");
                s.CreateTableFields("XFA_ABONENT_STATION", "PERM_DATE_PRINT");
                s.SetDatabaseVersion(20110530.1418);
            }

            if (dbCurVersion < 20110606.1654)
            {
                s.DeleteTableFields("XFA_REF_BASE_STA", "STA_ID,STA_TABLE");
                s.CreateTableFields("XFA_REF_BASE_STA", "STATION_ID,STATION_TABLE");
                s.SetDatabaseVersion(20110606.1654);
            }

            if (dbCurVersion < 20110610.1444)
            {
                s.CreateTableFields("XNRFA_POSITION", "ADMS_ID");
                s.SetDatabaseVersion(20110610.1444);
            }

            if (dbCurVersion < 20110610.1445)
            {
                s.CreateTableFields("XNRFA_POSITION", "CUST_TXT3,CUST_TXT4,CUST_TXT8");
                s.SetDatabaseVersion(20110610.1445);
            }

            if (dbCurVersion < 20110615.1445)
            {
                s.CreateTableFields("XFA_POSITION", "CUST_TXT3,CUST_TXT4,CUST_TXT8");
                s.SetDatabaseVersion(20110615.1445);
            }

            if (dbCurVersion < 20110616.1516)
            {
                s.CreateTableFields("XFA_POSITION", "CUST_TXT7");
                s.CreateTableFields("XNRFA_POSITION", "CUST_TXT7");
                s.SetDatabaseVersion(20110616.1516);
            }

            if (dbCurVersion < 20110617.1216)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffDvbSta, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffEtSta, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffMwSta, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffTvSta, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffFmDigital, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffMobSta, "COMMENTS");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffMobSta2, "COMMENTS");

                s.CreateTableFields(PlugTbl.APPL, "COMMENTS");
                s.SetDatabaseVersion(20110617.1216);
            }

            if (dbCurVersion < 20110620.0821)
            {
                s.UpdateTableFields("XFA_ABONENT_STATION", "CALL_SIGN");
                s.SetDatabaseVersion(20110620.0821);
            }

            if (dbCurVersion < 20110620.1111)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffFmSta, "CALL_SIGN");
                s.SetDatabaseVersion(20110620.1111);
            }

            if (dbCurVersion < 20110621.1220)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffDvbSta, "EQUIP_NAME");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffFmSta, "EQUIP_NAME");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffFmDigital, "EQUIP_NAME");
                s.SetDatabaseVersion(20110621.1220);
            }


            if (dbCurVersion < 20110621.1452)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffFmSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffMwSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffFmDigital,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffFmSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffTvSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffDvbSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffMobSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.CreateTableFields(PlugTbl.itblXnrfaDiffMobSta2,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");

                s.SetDatabaseVersion(20110621.1452);
            }

            if (dbCurVersion < 20110622.1600)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffEtSta,
                                    "TOTAL,CHIEF_ID,MEMBER1_ID,MEMBER2_ID,MEMBER3_ID,RNV,RTV,RMP,RITMP,GENERAL,PATH1,PATH2,LASTPATH");
                s.SetDatabaseVersion(20110622.1600);
            }

            if (dbCurVersion < 20110623.0916)
            {
                s.UpdateTableFields(PlugTbl.itblXnrfaDeiffDvbSta, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDeiffEtSta, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDeiffMwSta, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDeiffTvSta, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDiffFmDigital, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDiffMobSta, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDiffMobSta2, "EQUIP_NAME");
                s.UpdateTableFields(PlugTbl.itblXnrfaDeiffFmSta, "EQUIP_NAME");
                s.SetDatabaseVersion(20110623.0916);
            }

            if (dbCurVersion < 20110623.0917)
            {
                s.CreateTableFields(PlugTbl.PacketToAppl,
                                    "PTK_STATUS,DOC_NUM_TV,DOC_DATE,DOC_END_DATE,PTK_DATE_FROM,PTK_DATE_TO");
                s.CreateTableFields(PlugTbl.PacketToAppl, "DOC_NUM_URCM,DOC_URCM_DATE,IS_TV,REMARK,CREATED_BY");
                s.SetDatabaseVersion(20110623.0917);
            }


            if (dbCurVersion < 20110623.0944)
            {
                s.CreateTableFields(PlugTbl.itblXnrfaDeiffFmSta, "COMMENTS");
                s.SetDatabaseVersion(20110623.0944);
            }

            if (dbCurVersion < 20110624.1046)
            {
                s.DeleteTableFields(PlugTbl.APPL,
                                    "AGREEMENT,MEMOTODRV,AGREEMENT_PTK,MEMOTODRV_PTK,STATE_DOC_PTK,PTKDATE");
                s.SetDatabaseVersion(20110624.1046);
            }

            if (dbCurVersion < 20110630.1049)
            {
                s.CreateTables("XFA_EMI_STATION");
                s.CreateTableFields("XFA_EMI_STATION", "ID,POS_ID,AGL,CERT_NUM,CERT_DATE,EMI_TYPE,DOP_DEVICE,POWER");
                s.SetDatabaseVersion(20110630.1049);
            }
            if (dbCurVersion < 20110630.1201)
            {
                s.CreateTableFields("XFA_EMI_STATION", "STATUS,TABLE_NAME");
                s.SetDatabaseVersion(20110630.1201);
            }

            if (dbCurVersion < 20110630.1501)
            {
                s.UpdateTableFields("XFA_EMI_STATION", "EMI_TYPE");
                s.SetDatabaseVersion(20110630.1501);
            }

            if (dbCurVersion < 20110701.1637)
            {
                s.CreateTableFields("XFA_EMI_STATION", "STANDART");
                s.CreateTableFields("XFA_EMI_STATION", "FREQPLANCHAN_ID,FREQ");
                s.SetDatabaseVersion(20110701.1637);
            }

            if (dbCurVersion < 20110704.1157)
            {
                s.UpdateTableFields("XNRFA_ABONENT_OWNER", "NAME");
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "NAME_LATIN");
                s.SetDatabaseVersion(20110704.1157);
            }

            if (dbCurVersion < 20110705.1257)
            {
                s.CreateTableFields("XNRFA_PAC_TO_APPL", "IS_NV");
                s.UpdateTableFields("XNRFA_PAC_TO_APPL", "IS_TV");
                s.SetDatabaseVersion(20110705.1257);
            }

            if (dbCurVersion < 20110705.1449)
            {
                s.DeleteTableFields("XNRFA_DIFF_TV_STAT",
                                    "equip,powVideo,powAudio,rxBand,emiVideo,emiAudio,certDa,certNo");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT",
                                    "EQUIP,POWVIDEO,POWAUDIO,RXBAND,EMIVIDEO,EMIAUDIO,CERTDA,CERTNO");
                s.UpdateTableFields("XNRFA_DIFF_TV_STAT",
                                    "EQUIP,POWVIDEO,POWAUDIO,RXBAND,EMIVIDEO,EMIAUDIO,CERTDA,CERTNO");
                s.SetDatabaseVersion(20110705.1449);
            }

            if (dbCurVersion < 20110714.0948)
            {
                s.CreateTables("XFA_ANTEN");
                s.CreateTableFields("XFA_ABONENT_STATION", "NAME,CALL,TX_LOSSES,AZIMUTH,ANT_ID");
                s.SetDatabaseVersion(20110714.0948);
            }

            if (dbCurVersion < 20110714.0949)
            {
                s.CreateTables("XNRFA_SHIP_EXT");
                s.SetDatabaseVersion(20110714.0949);
            }

            if (dbCurVersion < 20110714.0950)
            {
                s.CreateTableFields("XNRFA_SHIP_EXT",
                                    "COSPAS_SARSAT_IDENT_1,COSPAS_SARSAT_IDENT_2,COSPAS_SARSAT_IDENT_3");
                s.SetDatabaseVersion(20110714.0951);
            }

            if (dbCurVersion < 20110714.0952)
            {
                s.CreateTables(PlugTbl.MeasureEquipment);
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "ENABLE_DATE2");

                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_EQ_ID");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_TIME_FROM1");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_TIME_TO1");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_DATE2");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_TIME_FROM2");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "MEASURE_TIME_TO2");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "VISA_USER_ID");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "EXECUTOR_ID");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "LINK_IE");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "ENABLE_DATE1");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "ENABLE_DATE2");

                s.SetDatabaseVersion(20110714.0952);
            }

            if (dbCurVersion < 20110714.0953)
            {
                s.UpdateTableFields("XFA_EQUIP", "NAME");
                s.SetDatabaseVersion(20110714.0953);
            }

            if (dbCurVersion < 20110714.0955)
            {
                s.CreateTableFields("XNRFA_APPL", "CHANGE_FILE_POS,CHANGE_DOC_NUM,CHANGE_PROVINCE,CHANGE_DOC_DATE");
                s.SetDatabaseVersion(20110714.0955);
            }


            if (dbCurVersion < 20110714.1113)
            {
                s.CreateTables("XFA_AMATEUR");
                s.CreateTables("XFA_EQUIP_AMATEUR");
                s.CreateTables("XFA_ES_AMATEUR");
                s.CreateTables("XFA_FREQ_AMATEUR");
                s.CreateTables("XFA_FE_AMATEUR");
                s.CreateTables("XFA_BAND_AMATEUR");

                s.SetDatabaseVersion(20110714.1113);
            }

            if (dbCurVersion < 20110714.1119)
            {
                s.CreateTables("XFA_CALL_AMATEUR");
                s.SetDatabaseVersion(20110714.1119);
            }

            if (dbCurVersion < 20110719.1147)
            {
                s.CreateTableFields("XNRFA_PACKET", "NUM_REESTR_VID");
                s.SetDatabaseVersion(20110719.1147);
            }

            if (dbCurVersion < 20110719.1642)
            {
                s.CreateTableFields("XNRFA_STATE_XML135", "CREATED_BY");
                s.UpdateTableFields("XFA_ABONENT_STATION", "FREQ_TX_TEXT,FREQ_RX_TEXT");
                s.SetDatabaseVersion(20110719.1642);
            }

            if (dbCurVersion < 20110719.1741)
            {
                s.CreateTables("XNRFA_MSREQ_CERT");

                s.CreateTableFields("XNRFA_MEASURE_EQ", "CERT_ID");

                s.CreateTableFields("XNRFA_DIFF_FMDIG", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "TABLE_NAME,MSREQ_CERT_ID,MEASURE_TIME_FROM3,MEASURE_TIME_TO3,MEASURE_APPROVED_BY,MEASURED_BY,LINK_IE3");

                s.CreateTables("XNRFA_AUXILIARY_EQ");
                s.CreateTables("XNRFA_MSREQ_LIST");
                s.CreateTables("XNRFA_AUXEQ_LIST");
                s.CreateTables("XNRFA_PARM_REZ_NOTE");

                s.SetDatabaseVersion(20110719.1741);
            }

            if (dbCurVersion < 20110720.1009)
            {
                s.CreateTableFields("XFA_AMATEUR", "NUMB_DOZV,NUMB_PROJ_DOZV,DATE_IN_DOZV,DATE_PERIOD_DOZV");
                s.CreateTableFields("XFA_AMATEUR", "DATE_PRINT_DOZV,NUMB_OLD_DOZV,DATE_IN_OLD_DOZV");
                s.SetDatabaseVersion(20110720.1009);
            }

            if (dbCurVersion < 20110720.1659)
            {
                s.CreateTableFields("XNRFA_MEASURE_EQ", "DEV_TYPE,SERIAL,NOTES");
                s.CreateTableFields("XNRFA_AUXILIARY_EQ", "DEV_TYPE,NOTES");
                s.SetDatabaseVersion(20110720.1659);
            }

            if (dbCurVersion < 20110721.1652)
            {
                s.CreateTableFields("XFA_CALL_AMATEUR", "NUMB");
                s.SetDatabaseVersion(20110721.1652);
            }

            if (dbCurVersion < 20110722.0934)
            {
                s.UpdateTableFields("XFA_CALL_AMATEUR", "NUMB");
                s.SetDatabaseVersion(20110722.0934);
            }

            if (dbCurVersion < 20110722.1634)
            {
                s.DeleteTableFields("XFA_CALL_AMATEUR", "NUMBER");
                s.CreateTableFields("XFA_CALL_AMATEUR", "NUMB");
                s.SetDatabaseVersion(20110722.1634);
            }

            if (dbCurVersion < 20110725.1529)
            {
                s.CreateTables("XNRFA_FEES");
                s.SetDatabaseVersion(20110725.1529);
            }

            if (dbCurVersion < 20110725.1746)
            {
                s.DeleteTables("XNRFA_FEES", false);
                s.CreateTables("XNRFA_FEES");
                s.CreateTables("XNRFA_FEES_RS");

                s.SetDatabaseVersion(20110725.1746);
            }

            if (dbCurVersion < 20110726.0942)
            {
                s.DeleteTables("XNRFA_FEES_RS", false);
                s.CreateTables("XNRFA_FEES_RS");
                s.SetDatabaseVersion(20110725.0942);
            }

            if (dbCurVersion < 20110726.0957)
            {
                s.DeleteTableFields("XFA_BAND_AMATEUR", "BAND");
                s.CreateTableFields("XFA_BAND_AMATEUR", "BANDMIN,BANDMAX");
                s.SetDatabaseVersion(20110726.0957);
            }

            if (dbCurVersion < 20110726.1611)
            {
                s.CreateTableFields("XNRFA_MEASURE_EQ", "CHECKDATE");
                s.SetDatabaseVersion(20110726.1611);
            }

            if (dbCurVersion < 20110726.1750)
            {
                //     s.CreateTableFields("XFA_ANTEN", "GAIN,H_BEAMWIDTH,V_BEAMWIDTH,DIAGА,DIAGH,DIAGV"); - hotfix 12.12.2013 - имя поля DIAGА содержит русскую букву А
                s.CreateTableFields("XFA_ANTEN", "GAIN,H_BEAMWIDTH,V_BEAMWIDTH,DIAGA,DIAGH,DIAGV"); // hotfix 12.12.2013
                s.SetDatabaseVersion(20110726.1750);
            }

            if (dbCurVersion < 20110727.0851)
            {
                s.CreateTableFields("XFA_ABONENT_STATION",
                                    "NUMB_EXIST_VISN,DATE_PRINT_DRV,DATE_PRINT_EXIST,NUMB_OLD_VISN,DATE_OLD_PRINT_DRV,DATE_OLD_PRINT_EX");
                s.SetDatabaseVersion(20110727.0851);
            }

            if (dbCurVersion < 20110727.1235)
            {
                s.CreateTables("XFA_EXPORT_RCHP,XFA_NAB_ENT");
                s.SetDatabaseVersion(20110727.1235);
            }
            if (dbCurVersion < 20110727.1451)
            {
                s.CreateTableFields("XFA_BAND_AMATEUR", "CUST_TXT1");
                s.SetDatabaseVersion(20110727.1451);
            }

            if (dbCurVersion < 20110727.1546)
            {
                s.CreateTableFields("XNRFA_AUXILIARY_EQ", "PROVINCE");
                s.SetDatabaseVersion(20110727.1546);
            }

            if (dbCurVersion < 20110727.1600)
            {
                s.DeleteTableFields("XFA_ABONENT_STATION", "NUMB_OLD_VISN");
                s.CreateTableFields("XFA_ABONENT_STATION", "NUMB_OLD_DOZV");
                s.SetDatabaseVersion(20110727.1600);
            }

            if (dbCurVersion < 20110728.1515)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "DATE_BORN");
                s.SetDatabaseVersion(20110728.1515);
            }

            if (dbCurVersion < 20110729.1222)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER",
                                    "SURNAME,SURNAME_LATIN,INDEX,OBL,DISTR,LOCAL,FAX,SER_PASSP,NUM_PASSP,DATE_PASSP,PUB_PASSP");
                s.SetDatabaseVersion(20110729.1222);
            }

            if (dbCurVersion < 20110729.1715)
            {
                s.CreateTableFields("XFA_ES_AMATEUR", "NUMB_DOZV,DATE_PRINT_DOZV,DATE_EXPIR_DOZV");
                s.SetDatabaseVersion(20110729.1715);
            }

            if (dbCurVersion < 20110729.1745)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "NAME2");
                s.SetDatabaseVersion(20110729.1745);
            }

            if (dbCurVersion < 20110801.1334)
            {
                s.CreateTableFields("XFA_EMI_STATION", "USER_ID");
                s.SetDatabaseVersion(20110801.1334);
            }

            if (dbCurVersion < 20110801.1456)
            {
                s.CreateTableFields("XFA_EMI_STATION", "EQUIP_ID,EQUIP_NAME");
                s.CreateTableFields("XFA_ABONENT_STATION", "SPEC_COND_DOZV");
                s.SetDatabaseVersion(20110801.1456);
            }

            if (dbCurVersion < 20110801.1840)
            {
                s.CreateTableFields("XNRFA_SHIP_EXT", "BANDWIDTH");
                s.SetDatabaseVersion(20110801.1840);
            }

            if (dbCurVersion < 20110803.1315)
            {
                s.CreateTableFields("XFA_EMI_STATION", "BW");
                s.SetDatabaseVersion(20110803.1315);
            }

            if (dbCurVersion < 20110804.1226)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "NAME1,NAME_LATIN1");
                s.SetDatabaseVersion(20110804.1226);
            }
            if (dbCurVersion < 20110810.1658)
            {
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "SHORTNAME");
                s.SetDatabaseVersion(20110810.1658);
            }

            if (dbCurVersion < 20110812.1432)
            {
                s.CreateTableFields("XNRFA_MSREQ_CERT", "DATE_CREATED,CREATED_BY");
                s.CreateTableFields("XFA_EMI_STATION", "DATE_CREATED,CREATED_BY");
                s.CreateTableFields("XNRFA_APPL", "DATE_CREATED,CREATED_BY");
                s.SetDatabaseVersion(20110812.1432);
            }

            if (dbCurVersion < 20110815.1709)
            {
                //помінянно тип int на string, так як необхідно зберігати числа типу 00xxx
                s.DeleteTableFields("XNRFA_ABONENT_OWNER", "INDEX");
                s.CreateTableFields("XNRFA_ABONENT_OWNER", "INDEX");
                s.SetDatabaseVersion(20110815.1709);
            }

            if (dbCurVersion < 20110817.0939)
            {
                s.CreateTableFields("XNRFA_CHAPPURCM", "PROVINCE");
                s.SetDatabaseVersion(20110817.0939);
            }

            if (dbCurVersion < 20110819.1036)
            {
                s.CreateTableFields("XFA_EMI_STATION", "FREQ_MIN,FREQ_MAX");
                s.SetDatabaseVersion(20110819.1036);
            }

            if (dbCurVersion < 20110819.1451)
            {
                s.DeleteTables("XFA_POSITION_EXT", false);
                s.CreateTables("XFA_POSITION_EXT");
                s.SetDatabaseVersion(20110819.1451);
            }

            if (dbCurVersion < 20110822.1546)
            {
                s.CreateTableFields("XFA_POSITION", "ADM_COD");
                s.SetDatabaseVersion(20110822.1546);
            }

            if (dbCurVersion < 20110906.1540)
            {
                s.CreateTables("XN_CHANGE_MSG");
                s.SetDatabaseVersion(20110906.1540);
            }

            if (dbCurVersion < 20110907.1446)
            {
                s.CreateTableFields("XNRFA_APPL", "CONC_NUM_NEW,CONC_DATE_FROM_NEW,CONC_DATE_TO_NEW,CONC_FILE_NEW,CONC_CREATED_BY_NEW");
                s.CreateTableFields("XNRFA_APPL", "CONC_NUM,CONC_DATE_FROM,CONC_DATE_TO,CONC_DATE_PRINT,CONC_FILE,CONC_CREATED_BY");
                s.CreateTableFields("XNRFA_APPL", "CONC_NUM_OLD,CONC_DATE_FROM_OLD,CONC_DATE_TO_OLD,CONC_DATE_PRINT_OLD,CONC_CREATED_BY_OLD,CONC_FILE_OLD");
                s.CreateTableFields("XNRFA_APPL", "DOZV_NUM_NEW,DOZV_DATE_FROM_NEW,DOZV_DATE_TO_NEW,DOZV_CREATED_BY_NEW,DOZV_FILE_NEW");
                s.CreateTableFields("XNRFA_APPL", "DOZV_NUM,DOZV_DATE_FROM,DOZV_DATE_TO,DOZV_DATE_PRINT,DOZV_DATE_CANCEL,DOZV_CREATED_BY,DOZV_FILE");
                s.CreateTableFields("XNRFA_APPL", "DOZV_NUM_OLD,DOZV_DATE_FROM_OLD,DOZV_DATE_TO_OLD,DOZV_DATE_PRINT_OLD,DOZV_DATE_CANCEL_OLD,DOZV_CREATED_BY_OLD,DOZV_FILE_OLD");
                s.SetDatabaseVersion(20110907.1446);
            }

            if (dbCurVersion < 20110913.1626)
            {
                s.CreateTables("XADM_COORD_AGRE,XFREC_DIST_ADM_AGRE");
                s.CreateTableFields("XNRFA_APPL", "STATUS_COORD");
                s.CreateTableFields("XNRFA_PACKET", "STATUS_COORD");
                s.SetDatabaseVersion(20110913.1626);
            }

            if (dbCurVersion < 20110913.1627)
            {
                s.CreateTableFields("XN_CHANGE_LOG", "MSG_ID");
                s.SetDatabaseVersion(20110913.1627);
            }

            if (dbCurVersion < 20110923.1231)
            {
                s.CreateTableFields("XNRFA_APPLPAYURCM", "EMPLOYEE_DRV_ID,RESPONSE_DATE");
                s.SetDatabaseVersion(20110923.1231);
            }

            if (dbCurVersion < 20110927.1058)
            {
                s.CreateTables("XFA_WIEN_CRD_EXT");
                s.SetDatabaseVersion(20110927.1058);
            }

            if (dbCurVersion < 20111005.1700)
            {
                s.CreateTableFields("XN_CHANGE_LOG", "IP_ADDRESS");
                s.SetDatabaseVersion(20111005.1700);
            }

            if (dbCurVersion < 20111024.1321)
            {
                s.CreateTableFields("XFA_CALL_AMATEUR", "PATH");
                s.CreateTableFields("XFA_ES_AMATEUR", "PATH,NUM_BLANK");
                s.SetDatabaseVersion(20111024.1321);
            }

            if (dbCurVersion < 20111108.1156)
            {
                s.CreateTableFields("XNRFA_CHAPPURCM", "NET_ID");
                s.SetDatabaseVersion(20111108.1156);
            }

            if (dbCurVersion < 20111118.1527)
            {
                s.CreateTables("XFA_REGISTRY_PORT");
                s.CreateTableFields("XNRFA_SHIP_EXT", "REGISTRY_PORT_ID");
                s.SetDatabaseVersion(20111118.1527);
            }

            if (dbCurVersion < 20111129.1139)
            {
                s.CreateTableFields("XNRFA_APPL", "PRICE_ID2,WORKS_COUNT2");
                s.SetDatabaseVersion(20111129.1139);
            }

            if (dbCurVersion < 20111208.1110)
            {
                s.CreateTableFields("XFA_NET", "OWNER_ID,PRICE_ID,WORKS_COUNT,NOTE");
                s.SetDatabaseVersion(20111208.1110);
            }

            if (dbCurVersion < 20111212.1600)
            {
                s.CreateTableFields("XNRFA_CHAPPURCM", "STANDARD,DOZV_NUM,DOZV_DATE_FROM,DOZV_DATE_TO,PRICE_ID");
                s.SetDatabaseVersion(20111212.1600);
            }

            if (dbCurVersion < 20120104.1122)
            {
                s.CreateTableFields("XNRFA_PACKET", "CREATED_MANAGEMENT");
                s.CreateTableFields("XFA_NET", "AUTO_SET_ARTICLE");
                s.SetDatabaseVersion(20120104.1122);
            }

            if (dbCurVersion < 20120111.1024)
            {
                //Добавление видов: XV_TAX_PAYER_FORM2, XV_TAX_PAYER_FORM3
                s.SetDatabaseVersion(20120111.1024);
            }

            if (dbCurVersion < 20120116.1654)
            {
                s.CreateTableFields("XFA_ABONENT_STATION", "SPEC_COND_VISN,NOTE");
                s.SetDatabaseVersion(20120116.1654);
            }

            if (dbCurVersion < 20120119.1245)
            {
                //Пересоздание видов: XV_TAX_PAYER_FORM1, XV_TAX_PAYER_FORM2, XV_TAX_PAYER_FORM3
                s.SetDatabaseVersion(20120119.1245);
            }

            if (dbCurVersion < 20120124.1037)
            {
                s.CreateTables("XNRFA_REMINDER,XNRFA_REMINDER_USER");
                s.SetDatabaseVersion(20120124.1037);
            }

            if (dbCurVersion < 20120125.1211)
            {
                s.CreateTableFields("XNRFA_REMINDER", "DELAY,ACTIVE");
                s.SetDatabaseVersion(20120125.1211);
            }

            if (dbCurVersion < 20120130.0921)
            {
                s.CreateTableFields("XFA_NET", "IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                s.CreateTableFields("XNRFA_APPL", "IS_CONFIRMED,CONFIRMED_BY,CONFIRMED_DATE");
                s.SetDatabaseVersion(20120130.0921);
            }

            if (dbCurVersion < 20120131.1500)
            {
                s.DeleteTableFields("XNRFA_REMINDER", "NOTIFICATION");
                s.CreateTableFields("XNRFA_REMINDER", "NOTIFY_TYPE");
                s.SetDatabaseVersion(20120131.1500);
            }

            if (dbCurVersion < 20120202.1502)
            {
                s.CreateTableFields("XNRFA_APPLPAYURCM", "INVOICE_STATUS");
                s.SetDatabaseVersion(20120202.1502);
            }

            if (dbCurVersion < 20120209.1543)
            {
                s.UpdateTableFields("XNRFA_PRICE", "ARTICLE");
                s.SetDatabaseVersion(20120209.1543);
            }

            if (dbCurVersion < 20120216.1312)
            {
                s.CreateTables("XNRFA_REPORT");
                s.CreateTables("XNRFA_SPEC_REPORT");
                s.SetDatabaseVersion(20120216.1312);
            }

            if (dbCurVersion < 20120216.1605)
            {
                s.CreateTableFields("XNRFA_DIFF_MOBSTA", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_MOBSTA2", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_ET_STAT", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_MW_STAT", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_FM_STAT", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_DVB_STAT", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_FMDIG", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_TV_STAT", "INACTIVE");
                s.CreateTableFields("XNRFA_DIFF_ET_FREQ", "INACTIVE");

                s.SetDatabaseVersion(20120216.1605);
            }

            if (dbCurVersion < 20120217.1200)
            {
                s.CreateTables("XNRFA_MONITOR_CONTR");
                s.SetDatabaseVersion(20120217.1200);
            }

            if (dbCurVersion < 20120217.1521)
            {
                s.CreateTableFields("XNRFA_REPORT", "PATH");
                s.SetDatabaseVersion(20120217.1521);
            }

            if (dbCurVersion < 20120223.0901)
            {
                s.CreateTableFields("XFA_AMATEUR", "CERTIF_NUMB,CERTIF_DATE_FROM,CERTIF_DATE_TO,CERTIF_DATE_PRINT");
                s.CreateTableFields("XFA_AMATEUR", "CERTIF_DELIVERY,CERTIF_PATH");
                s.SetDatabaseVersion(20120223.0901);
            }

            if (dbCurVersion < 20120316.0909)
            {
                s.CreateTableFields("XFA_AMATEUR", "SPEC_COND_DOZV,SPEC_COND_VISN,NOTE");
                s.SetDatabaseVersion(20120316.0909);
            }

            if (dbCurVersion < 20120316.0909)
            {
                //s.CreateTableFields("XNRFA_ABONENT_OWNER", "BRANCH_OFFICE_CODE");
                //s.CreateTableFields("XFA_ABONENT_STATION", "BRANCH_OFFICE_CODE");
                //s.SetDatabaseVersion(20120317.0909);
            }

            if (dbCurVersion < 20160721.0909)
            {
             //   s.CreateTables("XFA_BAND_EMI,XFA_EMI_TO_BAND");
              //  s.CreateTableFields("XFA_BAND_EMI", "ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
              //  s.CreateTables("XFA_EMI_TO_BAND");
              //  s.CreateTableFields("XFA_EMI_TO_BAND", "ID,BAND_ID");
              //  s.SetDatabaseVersion(20160721.0909);
            }

            

            return true;
        }

        //=============================================================
        /// <summary>
        /// Текущая версия БД плагина
        /// </summary>
        public static readonly double schemaVersion = 20120316.0909;
    }
}
