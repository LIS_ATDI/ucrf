﻿using System;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   internal partial class FrmSpecConditionR135 : Form
   {
      /// <summary>
      /// ID записи
      /// </summary>
      protected int RecordId { get; set; }
      /// <summary>
      /// Название таблицы
      /// </summary>
      public string DocTypeName { get; set; }
      /// <summary>
      /// Sql запрос
      /// </summary>
      public string Sql { get; set; }
      /// <summary>
      /// Текст условий
      /// </summary>
      public string TextCondition { get; set; }

      private ComboBoxDictionaryList<string, string> _docTypeList = new ComboBoxDictionaryList<string, string>();
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="id">Id записи</param>
      public FrmSpecConditionR135(int id)
      {
         InitializeComponent();
         //-----
         RecordId = id;
         DocTypeName = "";
         Sql = "";
         _docTypeList.Add(new ComboBoxDictionary<string, string>("PERM", "Дозвіл"));
         _docTypeList.Add(new ComboBoxDictionary<string, string>("CONC", "Висновок"));
         //-----
         IMRecordset rs = new IMRecordset(PlugTbl.SpecCondition, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,DOC_TYPE,SQL,TEXT_CONDITION");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, RecordId);
         rs.Open();
         if(!rs.IsEOF())
         {
            DocTypeName = rs.GetS("DOC_TYPE");
            Sql = rs.GetS("SQL");
            TextCondition = rs.GetS("TEXT_CONDITION");
         }
         if (rs.IsOpen())
            rs.Close();
         rs.Destroy();
         //------
         txtBoxSql.DataBindings.Add("Text", this, "Sql");
         txtBoxCondition.DataBindings.Add("Text", this, "TextCondition");
         _docTypeList.InitComboBox(cmbBoxDocType);
         cmbBoxDocType.DataBindings.Add("SelectedValue", this, "DocTypeName", true);
      }
      /// <summary>
      /// Сохраняем результаты
      /// </summary>
      private void btnOk_Click(object sender, EventArgs e)
      {
         IMRecordset rs = new IMRecordset(PlugTbl.SpecCondition, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,DOC_TYPE,SQL,TEXT_CONDITION");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, RecordId);
         rs.Open();
         if (rs.IsEOF())
         {
            rs.AddNew();
            RecordId = IM.AllocID(PlugTbl.SpecCondition, 1, -1);
            rs.Put("ID", RecordId);
         }
         else
         {
            rs.Edit();
         }
         rs.Put("DOC_TYPE", DocTypeName);
         rs.Put("SQL", Sql);
         rs.Put("TEXT_CONDITION", TextCondition);
         rs.Update();
         if (rs.IsOpen())
            rs.Close();
         rs.Destroy();
      }
      /// <summary>
      /// Тестируем SQL запрос
      /// </summary>
      private void btnTestSql_Click(object sender, EventArgs e)
      {
         int resultSql = -1;
         string localSql = string.Format(Sql, nmrcUpDownRecordId.Value);
         if (IM.ExecuteScalar(ref resultSql, localSql) == true)
         {
            MessageBox.Show(resultSql.ToString(), "Result of SQL query");
         }
         else
         {
            MessageBox.Show("Error", "Error");
         }
      }
   }
}
