﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    /// <summary>
    /// Класс "EriFiles" вытаскивает из eri файла описание кода
    /// </summary>
    public static class EriFiles
    {
        public const string ReminderTypeEri = "REMIND_TYPE";
        public const string ReminderPriorityEri = "REMIND_PRIORITY";
        public const string ReminderStatusEri = "REMIND_STATUS";
        public const string ReminderNotifyTypeEri = "REMIND_NOTIFY_TYPE";
        public const string StatusApplUrcmEri = "StatusApplUrcm";
        public const string InvoiceStatusEri = "InvoiceStatus";
        /// <summary>
        /// Возвращает описание из eri файла по коду
        /// </summary>
        /// <param name="code">кодовое значение</param>
        /// <param name="eriName">название eri файла</param>
        /// <returns>Возвращает описание из eri файла по коду</returns>
        public static string GetEriDescription(string code, string eriName)
        {
            string retVal = code;
            IMEriLOV eriData = new IMEriLOV(eriName);
            string freDescr = null, freBmp = null;
            eriData.GetEntry(code, ref freDescr, ref freBmp);
            if (!string.IsNullOrEmpty(freDescr))
                retVal = freDescr;
            return retVal;
        }
        /// <summary>
        /// Возвращает код из eri файла по описанию
        /// </summary>
        /// <param name="code">кодовое значение</param>
        /// <param name="eriName">название eri файла</param>
        /// <returns>Возвращает описание из eri файла по коду</returns>
        public static string GetEriCode(string descr, string eriName)
        {            
            IMEriLOV eriData = new IMEriLOV(eriName);
            IMEriLOVEntry[] eriEntries = eriData.GetallEntries();

            if (eriEntries != null)            
                foreach (IMEriLOVEntry entry in eriEntries)
                    if (entry.Description == descr)
                        return entry.Code;                    
            
            return null;
        }

        /// <summary>
        /// Возвращает описание статуса из eri файла
        /// </summary>
        /// <param name="status">код статуса</param>
        /// <returns>Описание статуса из eri файла</returns>
        public static string ToEriStationStatus(this string status)
        {
            return GetEriDescription(status, "TRFAStatus");
        }

        /// <summary>
        /// Возвращает словарь "код-описание" для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>словарь "код-описание"</returns>
        public static Dictionary<string, string> GetEriCodeAndDescr(string eriName)
        {
            Dictionary<string, string> eriDict = new Dictionary<string, string>();
            IMEriLOV eriData = new IMEriLOV(eriName);
            IMEriLOVEntry[] eriEntries = eriData.GetallEntries();

            if (eriEntries!=null)
            {
                foreach (IMEriLOVEntry entry in eriEntries)
                    if (!eriDict.ContainsKey(entry.Code))
                        eriDict.Add(entry.Code, entry.Description);
            }
            return eriDict;
        }

        /// <summary>
        /// Возвращает список кодов для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>список кодов</returns>
        public static List<string> GetEriCodeList(string eriName)
        {
            List<string> eriList = new List<string>();
            IMEriLOV eriData = new IMEriLOV(eriName);
            IMEriLOVEntry[] eriEntries = eriData.GetallEntries();

            if (eriEntries != null)
            {
                foreach (IMEriLOVEntry entry in eriEntries)
                    if (!eriList.Contains(entry.Code))
                        eriList.Add(entry.Code);
            }
            return eriList;
        }
        /// <summary>
        /// Возвращает список описаний для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>список описаний</returns>
        public static List<string> GetEriDescrList(string eriName)
        {
            List<string> eriList = new List<string>();
            IMEriLOV eriData = new IMEriLOV(eriName);
            IMEriLOVEntry[] eriEntries = eriData.GetallEntries();

            if (eriEntries != null)
            {
                foreach (IMEriLOVEntry entry in eriEntries)
                    if (!eriList.Contains(entry.Description))
                        eriList.Add(entry.Description);
            }
            return eriList;
        }

    }
}
