﻿using GridCtrl;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    internal class CertificateHelper
    {
        public static bool CheckCertificate(CellStringProperty symbolProperty, CellDateNullableProperty dateProperty, Certificate certificate)
        {
            
            if (!string.IsNullOrEmpty(symbolProperty.Value2.Trim()) && string.IsNullOrEmpty(symbolProperty.Value.Trim()))
                symbolProperty.Value = symbolProperty.Value2.Trim();
            if (!string.IsNullOrEmpty(dateProperty.Value2.Trim()) && string.IsNullOrEmpty(dateProperty.Value.Trim()))
                dateProperty.Value = dateProperty.Value2.Trim();
            if ((symbolProperty.Value == "-" && dateProperty.Value != "-") ||
                (symbolProperty.Value != "-" && dateProperty.Value == "-") ||
                string.IsNullOrEmpty(symbolProperty.Value.Trim()) ||
                string.IsNullOrEmpty(dateProperty.Value.Trim()))
                return false;

            if (symbolProperty.Value == "-" && dateProperty.Value == "-")
            {
                certificate.Date = IM.NullT;
                certificate.Symbol = "-";
            }
            else
            {
                certificate.Date = dateProperty.DateValue;
                certificate.Symbol = symbolProperty.Value.Trim();
            }
            return true;
        }

        public static void InitializeCertificate(CellStringProperty symbolProperty, CellDateNullableProperty dateProperty, Certificate certificate)
        {
            if (certificate.Symbol == "-")
            {
                symbolProperty.Value = "-";
                symbolProperty.SetRedValue();
                dateProperty.Value = "-";
                dateProperty.SetRedValue();
            }
            else
            {
                symbolProperty.Value = certificate.Symbol;
                dateProperty.DateValue = certificate.Date;
            }
        }
    }
}
