﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.HelpClasses.Forms;

namespace XICSM.UcrfRfaNET
{
   internal partial class FAttachDoc : FBaseForm
   {
      //===================================================
      private Documents.Documents docum;
      private ComboBoxDictionaryList<string, string> _docList = new ComboBoxDictionaryList<string, string>();
      public int NewIdLinkDoc { get; set; }
      public int IDPacket { get; set; }
      private List<int> listApplId_ = new List<int>();
      public static bool CurrTypeDoc = false;


      //===================================================
      public string fileName { get { return tbFilePath.Text; } }
      public DateTime docDate { get { return dtDocDate.Value; } }
      public DateTime? expDate { get { return chbUseExpDate.Checked ? (DateTime?)dtExpDate.Value : null; } }
      public string docRef { get { return tbDocRef.Text; } }
      //public string outNumber { get { return tbOutNumber.Text; } }
      private string _docType;
      public string DocTypeCode
       {
           get { return _docType; }
           set
           {
               if(value != _docType)
               {
                   _docType = value;
                   UpdateForm();
               }
           }
       }
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      //public FAttachDoc(int packetID, int[] idAppls,int[] idApplsAll)
      public FAttachDoc(int packetID, int[] idAppls)
      {
         InitializeComponent();

         btnAdditionalData.Text = CLocaliz.TxT("Additional Data");
         this.NewIdLinkDoc = -1;
         this.IDPacket = packetID;
         listApplId_.AddRange(idAppls.ToList());
         //listApplId_All_.AddRange(idApplsAll.ToList());
         dtExpDate.DataBindings.Add("Format", chbUseExpDate, "Checked", true);
         dtExpDate.DataBindings[0].Format += (o, ea) => { ea.Value = (bool)ea.Value ? DateTimePickerFormat.Long : DateTimePickerFormat.Custom; };

         CLocaliz.TxT(this);

         EPacketType packetType = EPacketType.PckUnknown;

         IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
         try
         {
             rsPacket.Select("ID,CONTENT,NUMBER_IN,DATE_IN");
             rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, packetID);
             rsPacket.Open();
             if (!rsPacket.IsEOF())
             {
                 try { packetType = (EPacketType)Enum.Parse(typeof(EPacketType), rsPacket.GetS("CONTENT")); }
                 catch {  };
                 tbRefIn.Text = rsPacket.GetS("NUMBER_IN");
                 dtpDateIn.Value = rsPacket.GetT("DATE_IN");
                 dtpDateIn.Format = dtpDateIn.Value == IM.NullT ? DateTimePickerFormat.Custom : DateTimePickerFormat.Long;
             }
         }
         finally
         {
             rsPacket.Destroy();
         }
         
         docum = new Documents.Documents();
         StationStatus wf = new StationStatus();
         List<string> accesedDocs = wf.AccessedDocuments(StationStatus.ETypeEvent.AttachDoc, idAppls);
         //List<string> accesedDocs = wf.AccessedDocuments(StationStatus.ETypeEvent.AttachDoc, idApplsAll);
          
         UcrfDepartment curDept = CUsers.GetCurDepartment();
         Documents.Documents docTypes = new Documents.Documents();
         docTypes.LoadWfConfiguration();
         foreach (DocType dt in docTypes._docTypes.Values)
         {
             // Для обычного случая
             if (!CurrTypeDoc)
             {
                 if ((curDept == UcrfDepartment.URZP && dt.urzpAccessible
                  || curDept == UcrfDepartment.URCP && dt.urcpAccessible
                  || curDept == UcrfDepartment.Branch && dt.branchAccessible)
                  &&
                  ( // специальные условия для некоторых типов документов в зависимости от пакета
                  dt.code == DocType.AKT_PTK && packetType == EPacketType.PckDOZV
                  || (dt.code == DocType.TEST_TV || dt.code == DocType.TEST_NV) && packetType != EPacketType.PckDOZV
                  || dt.code == DocType.URCM_NOTICE && packetType == EPacketType.PctNVTVURZP
                  || dt.code != DocType.AKT_PTK && dt.code != DocType.TEST_TV && dt.code != DocType.TEST_NV && dt.code != DocType.URCM_NOTICE //&&  dt.code != DocType.ARMY
                  ))
                 {
                     AddNewDoc(dt.code, accesedDocs);
                 }

             }
             else if (CurrTypeDoc)
             {
                 //MessageBox.Show(dt.packetAccessible+" "+dt.code);
                 if (((curDept == UcrfDepartment.URZP && dt.urzpAccessible && dt.packetAccessible)
                                     || (curDept == UcrfDepartment.URCP && dt.urcpAccessible && dt.packetAccessible)
                                     || (curDept == UcrfDepartment.Branch && dt.branchAccessible && dt.packetAccessible)
                                     )
                     ||
                 (!dt.urzpAccessible && !dt.urcpAccessible && !dt.branchAccessible && dt.packetAccessible)) // || (dt.urzpAccessible && dt.packetAccessible) || (dt.urcpAccessible && dt.packetAccessible) || (dt.branchAccessible && dt.packetAccessible))
                 {
                     AddNewDoc(dt.code, accesedDocs);
                 }
             }

             
         }

         // Инициализируем ComboBox
         _docList.InitComboBox(cbDocName);
         cbDocName.DataBindings.Add("SelectedValue", this, "DocTypeCode", true);
         buttonOK.Enabled = (_docList.Count > 0);
         SetTitle(CLocaliz.TxT("Attaching of the document"));
         //-----
         //labelOutDate.DataBindings.Add("Enabled", dtOutDate, "Enabled");
         labelInNumber.DataBindings.Add("Enabled", tbDocRef, "Enabled");
         //labelOutNumber.DataBindings.Add("Enabled", tbOutNumber, "Enabled");
       //  CurrTypeDoc = false;
      }
      //==================================================
      /// <summary>
      /// Добавляет в список документ если он доступен основываясь на статусе текущих станций
      /// </summary>
      /// <param name="typeOfDoc">Тип документа, который необходимо добавит</param>
      /// <param name="accesedDocs">Типы разрешенных документов</param>
      private void AddNewDoc(string typeOfDoc, List<string> accesedDocs)
      {
         if(accesedDocs.Contains(typeOfDoc) == true)
         {
            string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc);
            _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
         }
      }
      /// <summary>
      /// Обновляет компоненты формы
      /// </summary>
      private void UpdateForm()
      {
          dtExpDate.Enabled = true;
          tbDocRef.Enabled = true;
          //tbOutNumber.Enabled = true;
          if (DocTypeCode == DocType.ATTACH_DOZV_BY_NKRZ)
          {
              //dtOutDate.Enabled = false;
              //tbInNumber.Enabled = false;
              //tbOutNumber.Enabled = false;
          }
      }
      //==================================================
      /// <summary>
      /// Selects of a document
      /// </summary>
      private void buttonAttach_Click(object sender, EventArgs e)
      {
          if (string.IsNullOrEmpty(DocTypeCode))
          {
              MessageBox.Show(CLocaliz.TxT("Please input document type!"));
              return;
          }
          if (string.IsNullOrEmpty(docRef))
          {
              if (MessageBox.Show(CLocaliz.TxT("Number and date are not filled. Continue?"), CLocaliz.TxT("Message"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
              {
                  return;
              }
          }

          if (openFileDialog.ShowDialog() == DialogResult.OK)
          {
              string fileName = openFileDialog.FileName;
              if (fileName.IndexOf("\\\\") != 0)
              {
                  string networkLocation = PrintDocs.getPath(DocTypeCode);
                  if (string.IsNullOrEmpty(networkLocation))
                  {
                      IMRecordset rsNetPath = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
                      rsNetPath.Select("ITEM,WHAT");
                      rsNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-DOC");
                      rsNetPath.Open();
                      if (!rsNetPath.IsEOF())
                          networkLocation = rsNetPath.GetS("WHAT");
                      rsNetPath.Destroy();

                      if (string.IsNullOrEmpty(networkLocation))
                          throw new Exception("Файл потрібно записати в мережеве сховище, але папку для збереження вкладень на сервері не задано.\nЗверніться до адміністратора.");

                  }
                  if (networkLocation[networkLocation.Length - 1] != '\\')
                      networkLocation += '\\';
                  string ext = System.IO.Path.GetExtension(fileName);
                  if (ext.Length > 0 && ext[0] == '.')
                      ext = ext.Substring(1);
                  string fn = System.IO.Path.GetFileNameWithoutExtension(fileName);
                  string newFileName = networkLocation + fn + "." + ext;

                  if (MessageBox.Show("Файл буде записаний в мережеве сховиище як "+
                      "\n'" + newFileName + "'.\n"
                      +" Продовжити?", CLocaliz.Information, MessageBoxButtons.YesNo, MessageBoxIcon.Information) != DialogResult.Yes)
                      return;
                  //take care of potential existence of target file;
                  int fSuffix = 1;
                  if (System.IO.File.Exists(newFileName))
                  {
                      DialogResult dr = MessageBox.Show("Файл з таким ім'ям ('"+newFileName+"') вже існує. Згенерувати нове ім'я?\nТак \t- згенеруати нове ім'я\nНі \t- я виберу інший файл або зміню ім'я.",
                          CLocaliz.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                      if (dr == DialogResult.Yes)
                        while (System.IO.File.Exists(newFileName))
                            newFileName = networkLocation + fn + (fSuffix++).ToString() + "." + ext;
                      else if (dr == DialogResult.No)
                          newFileName = "";
                  }
                  //OK
                  if (!string.IsNullOrEmpty(newFileName))
                  {
                      System.IO.File.Copy(fileName, newFileName);
                      if (CurrTypeDoc)
                      {
                          PrintDocs prntDoc = new PrintDocs();
                          prntDoc.LoadDocLink(DocTypeCode, docRef, newFileName, docDate, expDate, listApplId_, IDPacket);
                          NewIdLinkDoc = prntDoc.NewIdLinkDoc;
                      }
                      else
                      {
                          PrintDocs prntDoc = new PrintDocs();
                          prntDoc.LoadDocLink(DocTypeCode, docRef, newFileName, docDate, expDate, listApplId_,false);
                          NewIdLinkDoc = prntDoc.NewIdLinkDoc;
                      }
                  }
                      fileName = newFileName;
                      CurrTypeDoc = false;
                  
              }
              if (!string.IsNullOrEmpty(fileName))
                this.tbFilePath.Text = fileName;
          }
      }
      //==================================================
      /// <summary>
      /// Press OK
      /// </summary>
      private void buttonOK_Click(object sender, EventArgs e)
      {
         if(ParamIsOk() == false)
            DialogResult = DialogResult.None;
      }
      //===================================================
      /// <summary>
      /// Checks of the parameters
      /// </summary>
      /// <returns>true - OK, false - error</returns>
      private bool ParamIsOk()
      {
         bool retVal = true;
         string errorMessage = "";
         if (cbDocName.SelectedIndex < 0)
            errorMessage = CLocaliz.TxT("Type of the document is not selected.");
         else if (string.IsNullOrEmpty(tbFilePath.Text))
            errorMessage = CLocaliz.TxT("Document is not selected.");

         if (!string.IsNullOrEmpty(errorMessage))
         {
            MessageBox.Show(errorMessage, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            retVal = false;
         }
         return retVal;
      }

      private void dtOutDate_ValueChanged(object sender, EventArgs e)
      {
          chbUseExpDate.Checked = true;
      }

      private void buttonCancel_Click(object sender, EventArgs e)
      {

      }

      private void btnAdditionalData_Click(object sender, EventArgs e)
      {
          if (NewIdLinkDoc != -1)
          {
              AdditionalDataForm dataFormAdditional = new AdditionalDataForm(NewIdLinkDoc, NewIdLinkDoc, false);
              dataFormAdditional.ShowDialog();
          }
          else
          {
              MessageBox.Show(CLocaliz.TxT("Please attached document, after input additional data!"));
          }
      }
   }
}
