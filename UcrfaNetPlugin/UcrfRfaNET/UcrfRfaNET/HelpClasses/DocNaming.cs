﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using ICSM;
using XICSM.Lock;

namespace XICSM.NSDoc
{
   class DocNaming
   {
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Дозвіл АР-3"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetNumDozvAr3(int countResult)
      {
         const string itemName = "XRFA_DOZV_AR_3";
         StringCollection results = new StringCollection();
         int lastId = 0;
         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable(itemName))
         {
            int id = GetID(itemName, countResult, ref lastId);
            do
            {
               lastId++;
               results.Add(lastId.ToString("0000000"));
            }
            while (lastId < id);
            lockTable.UnlockTable(itemName);
         }
         return results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Анулювання дозволу"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetNumDozvAnn(int countResult)
      {
         const string itemName = "XRFA_DOZV_ANN";
         StringCollection results = new StringCollection();
         int lastId = 0;
         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable(itemName))
         {
            int id = GetID(itemName, countResult, ref lastId);
            do
            {
               lastId++;
               results.Add(lastId.ToString("0000000"));
            }
            while (lastId < id);
            lockTable.UnlockTable(itemName);
         }
         return results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Вісновок ЕМС"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetVisnovokEMS(int countResult)
      {
         StringCollection Results = new StringCollection();
         int lastID = 0;
         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable("XRFA_VISNOVOKEMS"))
         {
            int id = GetID("XRFA_VISNOVOKEMS", countResult, ref lastID);
            do
            {
               lastID++;
               Results.Add(lastID.ToString("0000000"));
            }
            while (lastID < id);
            lockTable.UnlockTable("XRFA_VISNOVOKEMS");
         }
         return Results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Дозвіл на експлуатацію"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetNumberOnExpluat(int countResult)
      {
         StringCollection Results = new StringCollection();
         int lastID = 0;

         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable("XRFA_NUMEXPLUAT"))
         {
            int id = GetID("XRFA_NUMEXPLUAT", countResult, ref lastID);
            do
            {
               lastID++;
               Results.Add(lastID.ToString("0000000"));
            }
            while (lastID < id);
            lockTable.UnlockTable("XRFA_NUMEXPLUAT");
         }
         return Results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Службова записка"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetSlujbovaZapis(int countResult)
      {
         StringCollection Results = new StringCollection();
         int lastID = 0;
         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable("XRFA_SLUJBOVAZAPIS"))
         {
            int id = GetID("XRFA_SLUJBOVAZAPIS", countResult, ref lastID);
            do
            {
               lastID++;
               Results.Add(lastID.ToString("0000"));
            }
            while (lastID < id);
            lockTable.UnlockTable("XRFA_SLUJBOVAZAPIS");
         }
         return Results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Службова записка" для филиалов
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetBranchMemoNumbers(int countResult)
      {
         const string fieldName = "XRFA_BRANCHMEMO";
         var results = new StringCollection();
         int lastId = 0;
         var lockTable = new LockTable();
         if(lockTable.TryLockTable(fieldName)) {
            int id = GetID(fieldName, countResult, ref lastId);
            do {
               lastId++;
               results.Add(lastId.ToString("0000"));
            }
            while(lastId < id);
            lockTable.UnlockTable(fieldName);
         }
         return results;
      }
      //===================================================
      /// <summary>
      /// Генерация новеров документа "Вихідний лист"
      /// </summary>
      /// <param name="countResult">кол-во документов</param>
      /// <returns>список номеров</returns>
      public StringCollection GetVihidnyList(int countResult)
      {
         StringCollection Results = new StringCollection();
         int lastID = 0;
         LockTable lockTable = new LockTable();
         if (lockTable.TryLockTable("XRFA_VIHIDNYLIST"))
         {
            int id = GetID("XRFA_VIHIDNYLIST", countResult, ref lastID);
            do
            {
               lastID++;
               Results.Add(lastID.ToString("0000"));
            }
            while (lastID < id);
            lockTable.UnlockTable("XRFA_VIHIDNYLIST");
         }
         return Results;
      }
      //===================================================
      private int GetID(string fieldName, int countRes, ref int lastID)
      {
         IMRecordset r = new IMRecordset(XICSM.UcrfRfaNET.PlugTbl.SYS_CONFIG, IMRecordset.Mode.ReadWrite);
         r.Select("ITEM,WHAT");
         r.SetWhere("ITEM", IMRecordset.Operation.Eq, fieldName);
         r.Open();
         int id = 0;

         if (!r.IsEOF())
         {
            string resID = r.GetS("WHAT");
            id = Convert.ToInt32(resID);
            lastID = id;
            id += countRes;
            r.Edit();
            r.Put("WHAT", id.ToString());
            r.Update();
         }
         else
         {
            lastID = id;
            id += countRes;
            r.AddNew();
            r.Put("WHAT", id.ToString());
            r.Put("ITEM", fieldName);
            r.Update();
         }

         r.Close();
         r.Destroy();
         return id;
      }
   }
}
