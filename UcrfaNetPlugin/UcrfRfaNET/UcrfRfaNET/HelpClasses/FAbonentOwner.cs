﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET
{
   public partial class FAbonentOwner : FBaseForm
   {
      internal CAbonentOwner dAbonentOwner;       

      //======================================
      public FAbonentOwner(int _id) : base(_id)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         SetTitle(CLocaliz.TxT("Abonent Owner"));
         dAbonentOwner = new CAbonentOwner();
         dAbonentOwner.Read(_id);
         //-------
         this.tbName.DataBindings.Add("Text", dAbonentOwner, "zsNAME1");
         this.tbNameLatin.DataBindings.Add("Text", dAbonentOwner, "zsNAME_LATIN1");
         this.tbAddress.DataBindings.Add("Text", dAbonentOwner, "zsADDRESS");
         this.tbTelephone.DataBindings.Add("Text", dAbonentOwner, "zsTEL");
         this.tbOKPO.DataBindings.Add("Text", dAbonentOwner, "zsOKPO");        
         this.tbRematrk.DataBindings.Add("Text", dAbonentOwner, "zsREMARK");
         this.tbNumFile.DataBindings.Add("Text", dAbonentOwner, "zsNumFile");
         



         this.label1.Text = CLocaliz.TxT("Name/Partronymic (UKR)");//локализация метки "Имя, Отчество"
         this.label2.Text = CLocaliz.TxT("Address");//локализация метки "Адресс"
         this.label3.Text = CLocaliz.TxT("Telephone");//локализация метки "Телефон"
         this.lblPassp.Text = CLocaliz.TxT("Pasport:series / № / date"); //локализация метки "Серія / № / Дата видачі паспорту"
         this.label5.Text = CLocaliz.TxT("OKPO / IPN/ BRANCH CODE"); //локализация метки "код ЕДРПОУ / ІПН";
         this.label6.Text = CLocaliz.TxT("Remark");//локализация метки "Примітка"
         this.label7.Text = CLocaliz.TxT("Case"); //локализация метки"Справа";
         this.label8.Text = CLocaliz.TxT("Name (LAT)");//локализация метки "Имя (лат.)"
         this.label9.Text = CLocaliz.TxT("Birthday");//локализация метки "День народження"
         this.lblSurnameLat.Text = CLocaliz.TxT("Surname (LAT)");//локализация метки "Прізвище / Назва (лат)"
         this.lblSurname.Text = CLocaliz.TxT("Surname (UKR)");//локализация метки "Прізвище / Назва (укр)"
         this.lblIndex.Text = CLocaliz.TxT("Index / Region"); //локализация метки "Індекс / Область"
         this.lblDistr.Text = CLocaliz.TxT("District / City"); //локализация метки "Район / Нас.пункт"
         this.lblFax.Text = CLocaliz.TxT("Fax");//локализация метки "Факс"
         this.lblPubl.Text = CLocaliz.TxT("Passport issued"); //локализация "Ким видано паспорт";
         this.lblFullName.Text = CLocaliz.TxT("Full Name"); //локализация метки "Повне ім\'я";
         this.lblShortName.Text = CLocaliz.TxT("Short Name"); //локалізация метки"Коротке ім\'я";




         // Загрузка списка областей         
         IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
         rsArea.Select("ID,NAME");
         rsArea.OrderBy("NAME", OrderDirection.Ascending);
         cmbBoxObl.Items.Clear();
         cmbBoxDistr.Items.Clear();
         cmbBoxLoc.Items.Clear();                          
          
         this.txtBoxFullName.DataBindings.Add("Text", dAbonentOwner, "zsNAME");
         this.txtBoxName2.DataBindings.Add("Text", dAbonentOwner, "zsNAME2");
         this.txtBoxSurname.DataBindings.Add("Text", dAbonentOwner, "zsSURNAME");
         this.txtBoxSurnameLat.DataBindings.Add("Text", dAbonentOwner, "zsSURNAMELATIN");
         this.txtBoxIndex.DataBindings.Add("Text", dAbonentOwner, "zsINDEX");
         this.cmbBoxObl.DataBindings.Add("SelectedItem", dAbonentOwner, "zsOBL");
         this.cmbBoxDistr.DataBindings.Add("SelectedItem", dAbonentOwner, "zsDISTR");
         this.cmbBoxLoc.DataBindings.Add("SelectedItem", dAbonentOwner, "zsLOCAL");
         this.txtBoxFax.DataBindings.Add("Text", dAbonentOwner, "zsFAX");
         this.txtBoxSerPassp.DataBindings.Add("Text", dAbonentOwner, "zsSERPASSP");
         this.txtBoxNumbPassp.DataBindings.Add("Text", dAbonentOwner, "zsNUMPASSP");
         this.txtBoxDateBorn.DataBindings.Add("Text", dAbonentOwner, "zsDateBorn", true);
         this.txtBoxDatePassp.DataBindings.Add("Text", dAbonentOwner, "zsDATEPASSP", true);
         this.txtBoxShortName.DataBindings.Add("Text", dAbonentOwner, "zsSHORTNAME");
         this.txtBoxBranchCode.DataBindings.Add("Text", dAbonentOwner, "zsBranchCode");
 
         this.tbPassport.DataBindings.Add("Text", dAbonentOwner, "zsPUBPASSP");        
          
          if (cmbBoxDistr.Items.Count == 0)
         {
             cmbBoxDistr.Items.Add(dAbonentOwner.zsDISTR);
             cmbBoxDistr.SelectedIndex = 0;
         }
         if (cmbBoxLoc.Items.Count == 0)
         {
             cmbBoxLoc.Items.Add(dAbonentOwner.zsLOCAL);
             cmbBoxLoc.SelectedIndex = 0;
         }
         cmbBoxObl.DataSource = GetObl();
         cmbBoxDistr.DataSource = GetDistr(dAbonentOwner.zsOBL);
         cmbBoxLoc.DataSource = GetLocal(dAbonentOwner.zsOBL, dAbonentOwner.zsDISTR);

         cmbBoxObl.Text = dAbonentOwner.zsOBL;
         cmbBoxDistr.Text= dAbonentOwner.zsDISTR;
         cmbBoxLoc.Text = dAbonentOwner.zsLOCAL;
         buttonOK.Enabled = ((IM.TableRight(PlugTbl.ABONENT_OWNER) & IMTableRight.Update) == IMTableRight.Update);
      }
       /// <summary>
       /// Отримати список поселень для даної області і району
       /// </summary>
       /// <param name="obl">область</param>
       /// <param name="distr">район</param>
       /// <returns></returns>
       private List<object> GetLocal(object obl, object distr)
       {
           List<object> tmpObl = new List<object>();
           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly);
           rsArea.Select("ID,PROVINCE,SUBPROVINCE,NAME");
           if (obl != null && !string.IsNullOrEmpty(obl.ToString()))
               rsArea.SetWhere("PROVINCE", IMRecordset.Operation.Eq, obl.ToString());
           if (distr != null && !string.IsNullOrEmpty(distr.ToString()))
               rsArea.SetWhere("SUBPROVINCE", IMRecordset.Operation.Eq, distr.ToString());
           rsArea.OrderBy("NAME", OrderDirection.Ascending);
           try
           {
               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
               {
                   string province = rsArea.GetS("NAME");
                   if (!tmpObl.Contains(province))
                       tmpObl.Add(province);
               }
           }
           finally
           {
               rsArea.Final();
           }
           
           if (tmpObl.Count == 0)
               tmpObl.Add(" ");  

           return tmpObl;
       }
       /// <summary>
       /// Отримати список районів для даної області 
       /// </summary>
       /// <param name="obl"></param>
       /// <returns></returns>
       private List<object> GetDistr(object obl)
       {
           List<object> tmpObl = new List<object>();
           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly);
           rsArea.Select("ID,PROVINCE,SUBPROVINCE");
           if (obl != null && !string.IsNullOrEmpty(obl.ToString()))
               rsArea.SetWhere("PROVINCE", IMRecordset.Operation.Eq, obl.ToString());
           rsArea.OrderBy("SUBPROVINCE", OrderDirection.Ascending);
           try
           {
               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
               {
                   string province = rsArea.GetS("SUBPROVINCE");
                   if (!tmpObl.Contains(province))
                       tmpObl.Add(province);
               }
           }
           finally
           {
               rsArea.Final();
           }

           if (tmpObl.Count == 0)
               tmpObl.Add(" ");  


           return tmpObl;
       }

       /// <summary>
       /// Отримати список областей
       /// </summary>
       /// <returns></returns>
       private List<object> GetObl()
       {
           List<object> tmpObl=new List<object>();
           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly);
           rsArea.Select("ID,PROVINCE");
           rsArea.OrderBy("PROVINCE", OrderDirection.Ascending);
           try
           {
               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
               {
                   string province = rsArea.GetS("PROVINCE");
                   if (!tmpObl.Contains(province))
                       tmpObl.Add(province);
               }
           }
           finally
           {
               rsArea.Final();
           }

           if (tmpObl.Count==0)
               tmpObl.Add(" ");  

           return tmpObl;
       }

       //======================================
      /// <summary>
      /// Сохранить изменения
      /// </summary>
      private void buttonOK_Click(object sender, EventArgs e)
      {
         // обновляем поля         
         foreach (Binding b in this.tbName.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbNameLatin.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbAddress.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbTelephone.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbOKPO.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbPassport.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbRematrk.DataBindings) b.WriteValue();
         foreach (Binding b in this.tbNumFile.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxDateBorn.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxDatePassp.DataBindings) b.WriteValue();

         foreach (Binding b in this.txtBoxShortName.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxFullName.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxName2.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxFax.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxIndex.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxNumbPassp.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxSerPassp.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxSurname.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxSurnameLat.DataBindings) b.WriteValue();
         foreach (Binding b in this.cmbBoxDistr.DataBindings) b.WriteValue();
         foreach (Binding b in this.cmbBoxLoc.DataBindings) b.WriteValue();
         foreach (Binding b in this.cmbBoxObl.DataBindings) b.WriteValue();
         foreach (Binding b in this.txtBoxBranchCode.DataBindings) b.WriteValue();
          
         //---
         if (CheckData() == false)
         {
            DialogResult = DialogResult.None;
            return;
         }
         if (!string.IsNullOrEmpty(cmbBoxObl.SelectedItem.ToString()))
             dAbonentOwner.zsOBL = cmbBoxObl.SelectedItem.ToString();
         else
             dAbonentOwner.zsOBL = "";
         if (!string.IsNullOrEmpty(cmbBoxDistr.SelectedItem.ToString()))
             dAbonentOwner.zsDISTR = cmbBoxDistr.SelectedItem.ToString();
         else
             dAbonentOwner.zsDISTR = "";
         if (!string.IsNullOrEmpty(cmbBoxLoc.SelectedItem.ToString()))
             dAbonentOwner.zsLOCAL = cmbBoxLoc.SelectedItem.ToString();
         else
             dAbonentOwner.zsLOCAL = "";
         dAbonentOwner.Write();
      }
      //======================================================
      /// <summary>
      /// Проверка данных
      /// </summary>
      /// <returns>TRUE - все ОК, иначе FALSE</returns>
      private bool CheckData()
      {
         bool retVal = true;
         string errorMessage = "";

         if (string.IsNullOrEmpty(txtBoxSurname.Text))
            errorMessage = CLocaliz.TxT("Surname is empty.");

         if (!string.IsNullOrEmpty(errorMessage))
         {
            MessageBox.Show(errorMessage, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            retVal = false;
         }
         return retVal;
      }      

      private void cmbBoxDistr_MouseClick(object sender, MouseEventArgs e)
      {
          cmbBoxDistr.DataSource = GetDistr(cmbBoxObl.SelectedItem);
          cmbBoxLoc.DataSource = GetLocal(cmbBoxObl.SelectedItem, cmbBoxDistr.SelectedItem);             
      }     

      private void cmbBoxDistr_KeyDown(object sender, KeyEventArgs e)
      {
          cmbBoxLoc.DataSource = GetLocal(cmbBoxObl.SelectedItem, cmbBoxDistr.SelectedItem);                
      }      

      private void cmbBoxObl_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
      {          
          cmbBoxDistr.DataSource = GetDistr(cmbBoxObl.SelectedItem);          
      }

      private void txtBoxDateBorn_Leave(object sender, EventArgs e)
      {
          cmbBoxObl.DataSource = GetObl();
      }

      private void cmbBoxObl_MouseClick(object sender, MouseEventArgs e)
      {
          cmbBoxDistr.DataSource = GetDistr(cmbBoxObl.SelectedItem);    
      }

      private void cmbBoxLoc_MouseClick(object sender, MouseEventArgs e)
      {
          cmbBoxLoc.DataSource = GetLocal(cmbBoxObl.SelectedItem, cmbBoxDistr.SelectedItem);
      }

      private void FAbonentOwner_Load(object sender, EventArgs e)
      {
        
      }                       
   }
}
