﻿using ArticleLib;
using ICSM;
namespace XICSM.UcrfRfaNET
{
    internal static class SqlDatabase
    {
        /// <summary>
        /// Подсчитывает кол-во работ по заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Кол-во работ, или NULLI при ошибке</returns>
        public static int CalculateWorks(int applId)
        {
            return CountWork.CountWorkAppl(applId);
        }
        /// <summary>
        /// Подсчитsdftn кол-во работ по заявке и по статье
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="article">статья</param>
        /// <returns>Кол-во работ, или NULLI при ошибке</returns>
        public static int CalculateWorks(int applId, string article)
        {
            return CountWork.CountWorkByArticle(applId, IM.NullI, article);
        }
    }
}
