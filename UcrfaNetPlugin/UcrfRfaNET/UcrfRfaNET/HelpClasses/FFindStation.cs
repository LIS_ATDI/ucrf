﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CheckBoxComboBox;
using System.Reflection;
using OrmCs;

namespace XICSM.UcrfRfaNET
{

    public partial class FFindStation : Form, IDisposable
    {
        //===================================================
        EPacketType packetType;
        AppType appType;
        string radioTech;
        private int IDOwner = 0;
        protected CPacket packet = null;
        private SettingsGrid _settings;
        private int indexKOATUU = 0;
        private const string KOATUU_YES = "YES";
        private const string KOATUU_NO = "NO";
        private int excludeIdPacket = IM.NullI;
        private List<string> listProvince;
        private List<Status> StatusListGl;
        private ListSelectionWrapper<Status> StatusSelections;
        private List<int> listID_Global;
        private List<DataGridViewColumn> ColumnsLstMain = new List<DataGridViewColumn>();
        private bool isWorkStatus = false;
        public List<KeyValuePair<int, string>> R_docs_dupl = new List<KeyValuePair<int, string>>();
        //===================================================
        /// <summary>
        /// Конструктор формы
        /// </summary>
        /// <param name="packetType">Тип пакета</param>
        /// <param name="_appType">Тип заявки</param>
        public FFindStation(EPacketType _packetType, AppType _appType, string _radioTech, int _idOwner, int _idPacket, bool _isWorkStatus)
            : this(_packetType, _appType, _radioTech, _idOwner, _idPacket)
        {
            isWorkStatus = _isWorkStatus;
        }
        //===================================================
        /// <summary>
        /// Конструктор формы
        /// </summary>
        /// <param name="packetType">Тип пакета</param>
        /// <param name="_appType">Тип заявки</param>
        public FFindStation(EPacketType _packetType, AppType _appType, string _radioTech, int _idOwner, int _idPacket)
        {
            InitializeComponent();
            _settings = new SettingsGrid(Name);
            listID_Global = new List<int>();
            packet = new CPacket(_idPacket);
            CLocaliz.TxT(this);
            if (_packetType != EPacketType.PctANNUL) {
                //--------------------------
                //Проверка на заявку
                switch (_appType)
                {
                    case AppType.AppZS:
                    case AppType.AppRS:
                    case AppType.AppBS:
                    case AppType.AppRR:
                    case AppType.AppTR:
                    case AppType.AppTV2:
                    case AppType.AppTV2d:
                    case AppType.AppR2:
                    case AppType.AppR2d:
                    case AppType.AppVP:
                    case AppType.AppZRS:
                        break;
                    case AppType.AppRRTR:
                        {
                            List<string> list1 = CRadioTech.getListRadioTech(AppType.AppRR);
                            List<string> list2 = CRadioTech.getListRadioTech(AppType.AppTR);
                            if (list1.Contains(_radioTech))
                                _appType = AppType.AppRR;
                            else if (list2.Contains(_radioTech))
                                _appType = AppType.AppTR;
                            else
                                throw new Exception("No such radiotechnology for appl " + _appType.ToString());
                            break;
                        }
                    case AppType.AppR2R2d:
                        {
                            List<string> list1 = CRadioTech.getListRadioTech(AppType.AppR2);
                            List<string> list2 = CRadioTech.getListRadioTech(AppType.AppR2d);
                            if (list1.Contains(_radioTech))
                                _appType = AppType.AppR2;
                            else if (list2.Contains(_radioTech))
                                _appType = AppType.AppR2d;
                            else
                                throw new Exception("No such radiotechnology for appl " + _appType.ToString());
                            break;
                        }
                    case AppType.AppTV2TV2d:
                        {
                            List<string> list1 = CRadioTech.getListRadioTech(AppType.AppTV2);
                            List<string> list2 = CRadioTech.getListRadioTech(AppType.AppTV2d);
                            if (list1.Contains(_radioTech))
                                _appType = AppType.AppTV2;
                            else if (list2.Contains(_radioTech))
                                _appType = AppType.AppTV2d;
                            else
                                throw new Exception("No such radiotechnology for appl " + _appType.ToString());
                            break;
                        }
                    case AppType.AppAR_3:
                    case AppType.AppAP3:
                        {
                            tbVisn.Enabled = false;
                            tbFreqFrom.Enabled = false;
                            tbFreqTo.Enabled = false;
                            chComboBox.Enabled = false;
                        }
                        break;
                    case AppType.AppTR_З:
                        {
                            tbFreqFrom.Enabled = false;
                            tbFreqTo.Enabled = false;
                            chComboBox.Enabled = false;
                        }
                        break;
                    case AppType.AppA3:
                        {
                            tbVisn.Enabled = false;
                            tbFreqFrom.Enabled = false;
                            tbFreqTo.Enabled = false;
                        }
                        break;
                    default:
                        throw new Exception("Not implemented for appl " + _appType.ToString());
                }
            }
            //--------------------------
            packetType = _packetType;
            appType = _appType;
            radioTech = _radioTech;
            IDOwner = _idOwner;
            excludeIdPacket = _idPacket;
            //--------------------------
            InitGrid();
            tbOwner.Text = OwnerName;
            //--------------------------
            //
            this.Text = string.Format(CLocaliz.TxT("Пошук РЧП ({0})"), radioTech);
            //----

            
            string userProvince = CUsers.GetUserProvince();
            listProvince = new List<string>();
            
            List<string> Lst_Prov = new List<string>();
            Lst_Prov = CUsers.GetSubProvinceUser();
            if (Lst_Prov != null) { if (Lst_Prov.Count > 0) listProvince.AddRange(Lst_Prov); }
            Lst_Prov = null;

            

            // Инициализация компонента  ComboBoxCheckBox      
            StatusList _StatusList = new StatusList();
            StatusListGl = new List<Status>();
            for (int i = 0; i < listProvince.Count(); i++)
            {
                StatusListGl.Add(new Status(i, listProvince[i]));
            }
            _StatusList.AddRange(StatusListGl);
            StatusSelections = new ListSelectionWrapper<Status>(_StatusList, "Name");
            chComboBox.DataSource = StatusSelections;
            chComboBox.DisplayMemberSingleItem = "Name";
            chComboBox.DisplayMember = "NameConcatenated";
            chComboBox.ValueMember = "Selected";
            chComboBox.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            chComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            foreach (CheckBoxComboBoxItem Item in chComboBox.CheckBoxItems)
            {
                Item.CheckedChanged += new EventHandler(item_CheckedChanged);
            }

            //если в списке нет строк
            if (listProvince.Count == 0)
            {
                chComboBox.Enabled = false;
            }
            //если в списке одна строка(область)
            else if (listProvince.Count == 1)
            {
                chComboBox.Text = userProvince;
                if (!string.IsNullOrEmpty(userProvince)) chComboBox.CheckBoxItems[userProvince].Checked = true;
                chComboBoxAllSelected();
                chComboBox.Enabled = false;
            }
            //если в списке больше одной области
            else if (listProvince.Count > 1)
            {
                chComboBox.Text = userProvince;
                if (!string.IsNullOrEmpty(userProvince)) chComboBox.CheckBoxItems[userProvince].Checked = true;
                chComboBoxAllSelected();
                chComboBox.Enabled = true;
            }


        }
        //==================================================
        /// <summary>
        /// Инициализация грида
        /// </summary>
        private void InitGrid()
        {
            // ID

            ColumnsLstMain.Clear();
            dataGridAppl.Columns.Clear();
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "chId";
            column.HeaderText = CLocaliz.TxT("ID");
            column.Width = 50;
            if (ApplSetting.IsDebug)
                column.Visible = true; // В DEBUG версии отображаем ID
            else
                column.Visible = false;// В RELEASE версии не отображаем ID
            //dataGridAppl.Columns.Add(column);
            //ColumnsLstMain.Add(AddGridColumnMain(column, "chId", column.HeaderText, 50, true, "chId"));
            dataGridAppl.Columns.Add(AddGridColumnMain(column, "chId", column.HeaderText, 50, true, "chId"));

            // NumberDOZV
            column = new DataGridViewTextBoxColumn();
            column.Name = "chNumberDOZV";
            column.HeaderText = CLocaliz.TxT("Number DOZV");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chNumberDOZV", column.HeaderText, 50, true, "chNumberDOZV"));
            // NumberVISN
            column = new DataGridViewTextBoxColumn();
            column.Name = "chNumberVISN";
            column.HeaderText = CLocaliz.TxT("Number VISN");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chNumberVISN", column.HeaderText, 50, true, "chNumberVISN"));
            // Standard
            column = new DataGridViewTextBoxColumn();
            column.Name = "chStandard";
            column.HeaderText = CLocaliz.TxT("Standard");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chStandard", column.HeaderText, 50, true, "chStandard"));
            // OwnerName
            column = new DataGridViewTextBoxColumn();
            column.Name = "chOwnerName";
            column.HeaderText = CLocaliz.TxT("Owner name");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chOwnerName", column.HeaderText, 50, true, "chOwnerName"));
            // Position
            column = new DataGridViewTextBoxColumn();
            column.Name = "chPosition";
            column.HeaderText = CLocaliz.TxT("Position");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chPosition", column.HeaderText, 50, true, "chPosition"));
            // TXFreq
            column = new DataGridViewTextBoxColumn();
            column.Name = "chTXFreq";
            column.HeaderText = CLocaliz.TxT("TX freq");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chTXFreq", column.HeaderText, 50, true, "chTXFreq"));
            // RXFreq
            column = new DataGridViewTextBoxColumn();
            column.Name = "chRXFreq";
            column.HeaderText = CLocaliz.TxT("Rx freq");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chRXFreq", column.HeaderText, 50, true, "chRXFreq"));
            // NameRES
            column = new DataGridViewTextBoxColumn();
            column.Name = "chNameRES";
            column.HeaderText = CLocaliz.TxT("RES name");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chNameRES", column.HeaderText, 50, true, "chNameRES"));
            // DESIG_EMISSION
            column = new DataGridViewTextBoxColumn();
            column.Name = "chDesigEmission";
            column.HeaderText = CLocaliz.TxT("Desig emission");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chDesigEmission", column.HeaderText, 50, true, "chDesigEmission"));
            // AntName
            column = new DataGridViewTextBoxColumn();
            column.Name = "chAntName";
            column.HeaderText = CLocaliz.TxT("Antenna name");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chAntName", column.HeaderText, 50, true, "chAntName"));
            // KOATUU
            column = new DataGridViewTextBoxColumn();
            column.Name = "chKOATUU";
            column.HeaderText = CLocaliz.TxT("KOATUU");
            column.Width = 50;
            column.Visible = true;
            //dataGridAppl.Columns.Add(column);
            ColumnsLstMain.Add(AddGridColumnMain(column, "chKOATUU", column.HeaderText, 50, true, "chKOATUU"));
            indexKOATUU = 11;


            if ((packet.ListSelectedColumns != null) && (packet.isDefaultListColumns == false))
            {
                foreach (ExtendedColumnsProperty item in packet.ListSelectedColumns)
                    if (item.TypeRadiosys != AppType.AppUnknown)
                        AddGridColumn(null, item.NameProperty, CLocaliz.TxT(item.NameProperty), 100, true, item.NameProperty);
                    else
                    {
                        DataGridViewColumn fnd = ColumnsLstMain.Find(r => r.DataPropertyName == item.NameProperty && item.Number_List == 2);
                        if (fnd != null)
                        {
                            dataGridAppl.Columns.Add(fnd);
                        }
                    }
            }

            if (packet.isDefaultListColumns)
            {
                List<ColumnInfoGridView> L_FND_MAIN = _settings.ColumnInfo.FindAll(r => r.appType == AppType.AppUnknown);
                ////
                if (L_FND_MAIN != null)
                {
                    L_FND_MAIN.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                    foreach (ColumnInfoGridView colInfo in L_FND_MAIN)
                    {
                        DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                        if (col != null)
                        {
                            col.Width = colInfo.Width;
                            if (!dataGridAppl.Columns.Contains(col)) { dataGridAppl.Columns.Add(col); }
                        }
                        else
                        {
                            if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                            ExtendedColumnsProperty Fnd_ = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 2);
                            if (Fnd_ != null)
                            {
                                if (!packet.ListSelectedColumns.Contains(Fnd_))
                                {
                                    packet.ListSelectedColumns.Add(Fnd_);
                                    DataGridViewColumn col_x = ColumnsLstMain.Find(r => r.Name == Fnd_.PathToColumn);
                                    if (col_x != null)
                                    {
                                        if (!dataGridAppl.Columns.Contains(col_x)) { dataGridAppl.Columns.Add(col_x); }
                                    }
                                }
                            }
                        }
                    }
                }
                ////
                if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                if (packet.ListSelectedColumns.Count == 0)
                {
                    foreach (DataGridViewColumn item in ColumnsLstMain)
                    {
                        if (!dataGridAppl.Columns.Contains(item))
                        {
                            dataGridAppl.Columns.Add(item);
                            ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.PathToColumn == item.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 2);
                            if (Fnd != null)
                            {
                                if (packet.ListSelectedColumns != null)
                                {
                                    if (!packet.ListSelectedColumns.Contains(Fnd))
                                        packet.ListSelectedColumns.Add(Fnd);
                                }
                            }
                        }
                    }
                }
            }
            packet.isDefaultListColumns = false;
            LoadComponentSettings();
            

        }


        protected DataGridViewColumn AddGridColumnMain(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName)
        {
            if (column == null)
                column = new DataGridViewTextBoxColumn();
            if (!string.IsNullOrEmpty(name))
                column.Name = name;
            column.HeaderText = headerText;
            column.Width = width;
            column.ReadOnly = readOnly;
            column.DataPropertyName = dataPropertyName;
            return column;
        }

        protected DataGridViewColumn AddGridColumn(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName, int DisplayIndex)
        {
            if (column == null)
                column = new DataGridViewTextBoxColumn();
            if (!string.IsNullOrEmpty(name))
                column.Name = name;
            column.HeaderText = headerText;
            column.Width = width;
            column.ReadOnly = readOnly;
            column.DataPropertyName = dataPropertyName;
            column.DisplayIndex = DisplayIndex;
            dataGridAppl.Columns.Add(column);
            return column;
        }
        protected DataGridViewColumn AddGridColumn(DataGridViewColumn column, string name, string headerText, int width, bool readOnly, string dataPropertyName)
        {
            if (column == null)
                column = new DataGridViewTextBoxColumn();
            if (!string.IsNullOrEmpty(name))
                column.Name = name;
            column.HeaderText = headerText;
            column.Width = width;
            column.ReadOnly = readOnly;
            column.DataPropertyName = dataPropertyName;
            dataGridAppl.Columns.Add(column);
            return column;
        }
        //==================================================
        /// <summary>
        /// Ищем заявку
        /// </summary>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            dataGridAppl.Rows.Clear();
            buttonOK.Enabled = false;
            List<RecordPtr> listID = new List<RecordPtr>();
            if (packetType == EPacketType.PctANNUL)
            {
                if (appType == AppType.AppA3) listID = FindStations(appType);
                else
                {
                    foreach (AppType type in Enum.GetValues(typeof(AppType)))
                    {
                        if (type != AppType.AppTV2TV2d  && type != AppType.AppAR3 && type != AppType.AppUnknown && type != AppType.AppRRTR && type != AppType.AppR2R2d)
                        {
                            radioTech = ""; //Ищем по всем радиотехнологиям
                            List<RecordPtr> temp = type == AppType.AppUnknown ? new List<RecordPtr>() : FindStations(type);
                            foreach (RecordPtr id in temp)
                                listID.Add(id);
                        }
                    }
                }
            }
            else
                listID = FindStations(appType);
            //------------------------------------
            // Выбераем заявки текущего пакета, что б в одном пакете не повторялись заявки
            List<int> listIDApplInPacket = new List<int>();
            {
                IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                rs.Select("APPL_ID");
                rs.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, excludeIdPacket);
                try
                {
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        listIDApplInPacket.Add(rs.GetI("APPL_ID"));
                }
                finally
                {
                    rs.Destroy();
                }
            }

            foreach (RecordPtr ptr in listID)
                listID_Global.Add(ptr.Id);
            

            

            //------------------------------------
            // Обновляем список грида
            string MessageProgBar = CLocaliz.TxT("Updating grid...");
            using (CProgressBar pb = new CProgressBar(MessageProgBar))
            {
                pb.ShowBig(MessageProgBar);
                HashSet<int> dublicate = new HashSet<int>();
                for (int i = 0; i < listID.Count; i++)
                {
                    pb.ShowProgress(i, listID.Count);
                    if (pb.UserCanceled())
                        break;
                    RecordPtr id = listID[i];
                    if (listIDApplInPacket.Contains(id.Id) == true)
                        continue; //Пропускаем заявки, которые уже есть в текущем пакете
                    if (dublicate.Contains(id.Id))
                        continue;
                    dublicate.Add(id.Id);
                    packet.RefreshRowsFromIds(id.Id, id.Table);
                    List<string> AdditionalData = new List<string>();
                    if (packet.ListSelectedColumns != null) {
                        List<ExtendedColumnsProperty> porp_column_all = packet.ListSelectedColumns.FindAll(r => r.TypeRadiosys == packet.ApplType);
                        foreach (AppPacket.PacketRowBase bs in packet.Applications) {
                            if (porp_column_all != null) {
                                Type fieldsType = typeof(AppPacket.PacketRowBase);
                                PropertyInfo[] fields = fieldsType.GetProperties();
                                foreach (ExtendedColumnsProperty pr in porp_column_all) {
                                    foreach (PropertyInfo f in fields){
                                        if (f.Name == pr.NameProperty) {
                                            object obj = f.GetValue(bs, null);
                                            if (obj != null) {
                                                AdditionalData.Add(obj.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    List<string> Values = new List<string>();
                    string[] row = getRow(id.Id, id.Table, isWorkStatus);
                    if (row != null)
                    {
                        Values = row.ToList();
                        Values.AddRange(AdditionalData);
                        row = Values.ToArray();
                        dataGridAppl.Rows.Add(row);
                        buttonOK.Enabled = true;
                    }
                }
            }

           
        }
        /// <summary>
        /// ПОиск РЧП по заданным параметрам
        /// </summary>
        /// <param name="recset_">Заданный источник данных</param>
        /// <param name="tableName"> таблица</param>
        /// <param name="type">тип</param>
        /// <param name="Status">Статус 
        ///                     (false - если необходимо выполнять поиск по Position.Province)
        ///                     (true -  если необходимо выполнять поиск по регионам пользователя)</param>
        private void ExecTemplateFind(ref IMRecordset recset_, ref string tableName , AppType type, bool Status)
        {

            string province = "";
            string DozvNumber = tbDozvil.Text;
            string VisnNumber = tbVisn.Text;

          

            // По таблице
            switch (type)
            {
                //ЗС
                case AppType.AppZS:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblEarthStation);
                    recset_.SetWhere("EarthStation.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblEarthStation;
                    break;
                //РС
                case AppType.AppRS:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblMicrowa);
                    recset_.SetWhere("Microwave.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblMicrowa;
                    break;
                //БС
                case AppType.AppBS:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblMobStation2);
                    {
                        string query = string.Format("(([BaseSector1.STANDARD] LIKE '{0}') OR ([BaseSector1.STANDARD] LIKE '{1}') OR ([BaseSector1.STANDARD] LIKE '{2}')  OR ([BaseSector1.STANDARD] LIKE '{3}'))", CRadioTech.MmR, CRadioTech.MsR, CRadioTech.SHR, CRadioTech.BNT);
                        recset_.SetAdditional(query);
                    }
                    tableName = ICSMTbl.itblMobStation2;
                    break;
                //РР,ТР
                case AppType.AppRR:
                case AppType.AppTR:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblMobStation);
                    recset_.SetWhere("MobileSector1.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblMobStation;
                    break;
                //T
                case AppType.AppTV2:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblTV);
                    recset_.SetWhere("TvStation.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblTV;
                    break;
                //Tц
                case AppType.AppTV2d:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblDVBT);
                    recset_.SetWhere("DvbtStation.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblDVBT;
                    break;
                //Р
                case AppType.AppR2:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblFM);
                    recset_.SetWhere("FmStation.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblFM;
                    break;
                //Рц
                case AppType.AppR2d:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblTDAB);
                    recset_.SetWhere("TdabStation.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblTDAB;
                    break;
                //АР,ТР-З
                case AppType.AppAR_3:
                case AppType.AppAP3:
                case AppType.AppTR_З:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.XfaAbonentStation);
                    recset_.SetWhere("Abonent.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = PlugTbl.XfaAbonentStation;
                    break;
                //ВП  випромінювальні пристрої
                case AppType.AppVP:
                    //findAppl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSM);
                    //findAppl.SetWhere("GSM.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    //tableName = ICSMTbl.itblGSM;
                    break;
                //ЗРС
                case AppType.AppZRS:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, ICSMTbl.itblShip);
                    //findAppl.SetWhere("Ship.STANDARD", IMRecordset.Operation.Like, "*" + radioTech + "*");
                    tableName = ICSMTbl.itblShip;
                    break;
                //A3  аматорські пристрої
                case AppType.AppA3:
                    recset_.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.XfaAmateur);
                    tableName = PlugTbl.XfaAmateur;
                    break;
            }

            // По контрагенту
            if (IDOwner > 0)
            {
                switch (type)
                {
                    //ЗС
                    case AppType.AppZS:
                        recset_.SetWhere("EarthStation.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //РС
                    case AppType.AppRS:
                        recset_.SetWhere("Microwave.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //БС
                    case AppType.AppBS:
                        recset_.SetWhere("BaseSector1.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //РР,ТР
                    case AppType.AppRR:
                    case AppType.AppTR:
                        recset_.SetWhere("MobileSector1.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //T
                    case AppType.AppTV2:
                        recset_.SetWhere("TvStation.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //Tц
                    case AppType.AppTV2d:
                        recset_.SetWhere("DvbtStation.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //Р
                    case AppType.AppR2:
                        recset_.SetWhere("FmStation.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //Рц
                    case AppType.AppR2d:
                        recset_.SetWhere("TdabStation.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //АР,ТР-З
                    case AppType.AppAR_3:
                    case AppType.AppAP3:
                    case AppType.AppTR_З:
                        recset_.SetWhere("Abonent.User.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //ЗРС
                    case AppType.AppZRS:
                        recset_.SetWhere("Ship.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //ВП  випромінювальні пристрої
                    case AppType.AppVP:
                        recset_.SetWhere("GSM.Owner.ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                    //A3  аматорські пристрої
                    case AppType.AppA3:
                        recset_.SetWhere("AmateurStation.USER_ID", IMRecordset.Operation.Eq, IDOwner);
                        break;
                }
            }
            // По дозволу
            if (!string.IsNullOrEmpty(DozvNumber))
            {
                recset_.SetWhere("DOZV_NUM", IMRecordset.Operation.Like, DozvNumber);
            }
            // По Висновку
            if (!string.IsNullOrEmpty(VisnNumber))
            {
                recset_.SetWhere("CONC_NUM", IMRecordset.Operation.Like, VisnNumber);
            }

            string AdditionalQuery = "";
            // Выборка по выбранным областям

            if (!Status)
            {
                switch (type)
                {
                    //БС
                    case AppType.AppBS:
                        AdditionalQuery = GetAdditionalQuery("BaseSector1.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //РС
                    case AppType.AppRS:
                        {
                            if (!string.IsNullOrEmpty(GetAdditionalQuery("Microwave.StationA.Position.PROVINCE", false))) AdditionalQuery = GetAdditionalQuery("Microwave.StationA.Position.PROVINCE", false);
                            if (!string.IsNullOrEmpty(GetAdditionalQuery("Microwave.StationB.Position.PROVINCE", false))) AdditionalQuery += " OR " + GetAdditionalQuery("Microwave.StationB.Position.PROVINCE", false);
                            if ((!string.IsNullOrEmpty(AdditionalQuery)) && (AdditionalQuery.Trim() != "OR")) recset_.SetAdditional(AdditionalQuery);
                        }
                        break;
                    //ЗС
                    case AppType.AppZS:
                        AdditionalQuery = GetAdditionalQuery("EarthStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //РР,ТР
                    case AppType.AppRR:
                    case AppType.AppTR:
                        AdditionalQuery = GetAdditionalQuery("MobileSector1.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //АР,ТР-З
                    case AppType.AppAR_3:
                    case AppType.AppAP3:
                    case AppType.AppTR_З:
                        AdditionalQuery = GetAdditionalQuery("Abonent.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //T
                    case AppType.AppTV2:
                        AdditionalQuery = GetAdditionalQuery("TvStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //Tц
                    case AppType.AppTV2d:
                        AdditionalQuery = GetAdditionalQuery("DvbtStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //Р
                    case AppType.AppR2:
                        AdditionalQuery = GetAdditionalQuery("FmStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //Рц
                    case AppType.AppR2d:
                        AdditionalQuery = GetAdditionalQuery("TdabStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //A3  аматорські пристрої
                    case AppType.AppA3:
                        AdditionalQuery = GetAdditionalQuery("AmateurStation.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                    //VP
                    case AppType.AppVP:
                        AdditionalQuery = GetAdditionalQuery("GSM.Position.PROVINCE", false);
                        if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
                        break;
                }
            }
            else
            {
                     AdditionalQuery = GetAdditionalQuery("Employee.PROVINCE", true);
                     if (!string.IsNullOrEmpty(AdditionalQuery)) recset_.SetAdditional(AdditionalQuery);
            }

        }

        /// <summary>
        /// Метод, реализующий функционал поиска станций по заданным параметрам
        /// </summary>
        /// <param name="pb">Панель отображения вспомогательных данных(ProgressBar)</param>
        /// <param name="type">тип</param>
        /// <param name="listID">Список найденных id</param>
        private void ExecuteAlgFindStation(CProgressBar pb, AppType type, List<RecordPtr> listID)
        {
            string tableName = "";

            //Открытие таблицы XNRFA_APPL и выборка перечня полей
            IMRecordset findAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            string SQL = "ID,OBJ_ID1,OBJ_TABLE,Microwave.StationA.TX_FREQ,Microwave.StationB.TX_FREQ,FmStation.FREQ,TdabStation.FREQ,TvStation.FREQ,DvbtStation.FREQ,EarthStation.STANDARD,Microwave.STANDARD,BaseSector1.STANDARD,MobileSector1.STANDARD,TvStation.STANDARD,DvbtStation.STANDARD,Employee.PROVINCE,FmStation.STANDARD,Abonent.STANDARD";
            findAppl.Select(SQL);
        

            // Если текущее подразделение - филиал
            if (UcrfDepartment.Branch != CUsers.GetCurDepartment())
            {
                ExecTemplateFind(ref findAppl,ref tableName, type,false);
            }
            else
            {
                findAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                findAppl.Select(SQL);
                ExecTemplateFind(ref findAppl, ref tableName, type, false);

                findAppl.Open();
                pb.ShowBig("Prepary executing a query...");
                // Если ничего не нашло - то включить вспомогательную проверку 
                bool Stat = false;
                if (findAppl.IsEOF())
                {
                    Stat = true;
                }
                findAppl.Close();

                if (Stat)
                {
                    //Открытие таблицы XNRFA_APPL и выборка перечня полей
                    findAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                    findAppl.Select(SQL);
                    ExecTemplateFind(ref findAppl, ref tableName, type, true);
                }
                else
                {
                    findAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                    findAppl.Select(SQL);
                    ExecTemplateFind(ref findAppl, ref tableName, type, false);
                }
               

            }


            try
            {
                int counter = 0;
                HashSet<int> duplicateHash = new HashSet<int>();
                //получить данные о частотах для поиска с пользовательской формы
                double freqFrom = ConvertType.ToDouble(tbFreqFrom.Text, IM.NullD);
                double freqTo = ConvertType.ToDouble(tbFreqTo.Text, IM.NullD);
                findAppl.OrderBy("CREATE_DATE", OrderDirection.Descending);
                pb.ShowBig("Executing a query...");
                //открытие набора данных с учетом предыдущих условий
                for (findAppl.Open(); !findAppl.IsEOF(); findAppl.MoveNext())
                {
                    pb.ShowBig("Loading stations...");
                    pb.ShowSmall(counter++);
                    if (pb.UserCanceled())
                        break;
                    int hashTable = (findAppl.GetI("OBJ_ID1").ToString() + findAppl.GetS("OBJ_TABLE")).GetHashCode();
                    if (duplicateHash.Contains(hashTable))
                        continue;

                    duplicateHash.Add(hashTable);

                    //если частоты для поиска введены корректно
                    if ((freqFrom != IM.NullD) && (freqTo != IM.NullD))
                    {
                        //добавление в список ID станций, частоты которых находятся в указанном пользователем диапазоне
                        switch (type)
                        {
                            //БС
                            case AppType.AppBS:
                                {
                                    IMRecordset rsFreq = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
                                    rsFreq.Select("TX_FREQ");
                                    rsFreq.SetWhere("STA_ID", IMRecordset.Operation.Eq, findAppl.GetI("OBJ_ID1"));
                                    try
                                    {
                                        for (rsFreq.Open(); !rsFreq.IsEOF(); rsFreq.MoveNext())
                                            if ((freqFrom <= rsFreq.GetD("TX_FREQ")) && (rsFreq.GetD("TX_FREQ") <= freqTo))
                                                listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                    }
                                    finally
                                    {
                                        rsFreq.Close();
                                        rsFreq.Destroy();
                                    }
                                }
                                break;
                            //РС
                            case AppType.AppRS:
                                if (((freqFrom <= findAppl.GetD("Microwave.StationA.TX_FREQ")) && (findAppl.GetD("Microwave.StationA.TX_FREQ") <= freqTo)) ||
                                   ((freqFrom <= findAppl.GetD("Microwave.StationB.TX_FREQ")) && (findAppl.GetD("Microwave.StationB.TX_FREQ") <= freqTo)))
                                    listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                break;
                            //ЗС
                            case AppType.AppZS:
                                {
                                    IMRecordset rsFreq = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                                    rsFreq.Select("FREQ");
                                    rsFreq.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, findAppl.GetI("OBJ_ID1"));
                                    try
                                    {
                                        for (rsFreq.Open(); !rsFreq.IsEOF(); rsFreq.MoveNext())
                                            if ((freqFrom <= rsFreq.GetD("FREQ")) && (rsFreq.GetD("FREQ") <= freqTo))
                                                listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                    }
                                    finally
                                    {
                                        rsFreq.Close();
                                        rsFreq.Destroy();
                                    }
                                }
                                break;
                            //РР,ТР,АР,ТР-З
                            case AppType.AppRR:
                            case AppType.AppTR:
                            case AppType.AppAR_3:
                            case AppType.AppAP3:
                            case AppType.AppTR_З:
                                {
                                    IMRecordset rsFreq = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                                    rsFreq.Select("TX_FREQ");
                                    rsFreq.SetWhere("STA_ID", IMRecordset.Operation.Eq, findAppl.GetI("OBJ_ID1"));
                                    try
                                    {
                                        for (rsFreq.Open(); !rsFreq.IsEOF(); rsFreq.MoveNext())
                                            if ((freqFrom <= rsFreq.GetD("TX_FREQ")) && (rsFreq.GetD("TX_FREQ") <= freqTo))
                                                listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                    }
                                    finally
                                    {
                                        rsFreq.Close();
                                        rsFreq.Destroy();
                                    }
                                }
                                break;
                            //T
                            case AppType.AppTV2:
                                if ((freqFrom <= findAppl.GetD("TvStation.FREQ")) && (findAppl.GetD("TvStation.FREQ") <= freqTo))
                                    listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                break;
                            //Tц
                            case AppType.AppTV2d:
                                if ((freqFrom <= findAppl.GetD("DvbtStation.FREQ")) && (findAppl.GetD("DvbtStation.FREQ") <= freqTo))
                                    listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                break;
                            //Р
                            case AppType.AppR2:
                                if ((freqFrom <= findAppl.GetD("FmStation.FREQ")) && (findAppl.GetD("FmStation.FREQ") <= freqTo))
                                    listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                break;
                            //Рц
                            case AppType.AppR2d:
                                if ((freqFrom <= findAppl.GetD("TdabStation.FREQ")) && (findAppl.GetD("TdabStation.FREQ") <= freqTo))
                                    listID.Add(new RecordPtr(tableName, findAppl.GetI("ID")));
                                break;
                        }
                    }
                    else
                        listID.Add(new RecordPtr(findAppl.GetS("OBJ_TABLE"), findAppl.GetI("ID")));
                }
            }
            finally
            {
                //освобождение ресурсов
                findAppl.Close();
                findAppl.Destroy();
            }

        }

        /// <summary>
        /// Вспомогательный метод, который выполняет конкатенацию перечня условий ИЛИ. В качестве 
        /// аргумента (string NameFld) используется наименование столбца, которое используется для постороения
        /// условия вида ([NameFld]=Province1) or ([NameFld]=Province2) or ([NameFld]=Province3) ...
        /// </summary>
        /// <param name="NameFld"></param>
        /// <returns></returns>
        private string GetAdditionalQuery(string NameFld, bool status)
        {
            bool StatusCheck=false;
            string query = "";
            string province="";
            query += "(";
            int cnt = 0; int index = 0;
            //Подсчет количества выбранных пользователем областей для поиска
             for (int i = 0; i < StatusListGl.Count(); i++)  {  if (chComboBox.CheckBoxItems[listProvince[i]].Checked)   {   ++cnt;  } }
            //Цикл выполняющий конкатенацию перечня условий OR (ИЛИ)
             for (int i = 0; i < StatusListGl.Count(); i++)
             {
                 //Получить очередное наименование области из списка выбранных
                 province = StatusSelections.FindObjectWithItem(StatusListGl[i]).Item.Name;
                 // если данная область точно выбрана, то сформировать условие фильтра
                 if (chComboBox.CheckBoxItems[listProvince[i]].Checked)
                 {
                     //признак, указывающий на то, что пользователем выбрана хотя-бы одна область из списка доступных
                     StatusCheck = true;
                     // формирование условия
                     query += "([" + NameFld + "] = '" + province + "')";
                     // OR вставляется только между двумя сформированными условиями и не добавляется в конец
                     if ((cnt >= 2) && (index < cnt - 1)) query += " OR ";
                     //увеличить на 1 количество обработанных выбранных областей
                     ++index;
                 }
             }
             // Если выбраны все галочки для Областей, то кроме всего прочего выполняется поиск возмжных NULL - значений для
             // конкретных полей таблиц
             if (isAllChecked()) { query += " OR ([" + NameFld + "]  IS NULL)"; }
             query += ")";

             //если не выбраны области результат работы метода - пустое строковое значение (отсутствие фильтра отбора)
             if ((!StatusCheck) && (!status))
             {
                 query = "";
             }
             else if (status)
             {
                 cnt = 0; index = 0;
                 List<string> Lst = new List<string>();
                 Lst.Clear();
                 Lst = CUsers.GetSubProvinceOnUser(CUsers.GetUserProvince());

                 query = "";
                 query += "(";
                 for (int i = 0; i < Lst.Count(); i++)
                 {
                         //Получить очередное наименование области из списка выбранных
                         province = Lst[i];
                         // формирование условия
                         query += "([" + NameFld + "] = '" + province + "')";
                         // OR вставляется только между двумя сформированными условиями и не добавляется в конец
                         if ((Lst.Count() >= 2) && (index < Lst.Count() - 1)) query += " OR ";
                         //увеличить на 1 количество обработанных выбранных областей
                         ++index;
                 }
                 query += ")";
                
             }
             return query;
        }

        /// <summary>
        /// Метод,выполняющий поиск станций по различным критериям
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<RecordPtr> FindStations(AppType type)
        {
            List<RecordPtr> listID = new List<RecordPtr>();
            using (CProgressBar pb = new CProgressBar(string.Format("Finding stations of type {0}", type.ToString())))
            {
                        // Функция, реализующая алгоритмы поиска станций
                        ExecuteAlgFindStation(pb, type,listID);
            }

            return listID;
        }
        //=================================================
        /// <summary>
        /// Возвращает строку для грида
        /// </summary>
        /// <param name="_applID">ID заявки</param>
        /// <returns>строку для грида</returns>
        private string[] getRow(int _applID, string tableName, bool OnlyWorkStatus)
        {
            string[] retVal = null;
            bool isWorkStatus = false;
            string numberDOZV = "";
            string numberVISN = "";
            string OwnerName = "";
            string position = "";
            string standard = "";
            string nameRES = "";
            string DesigEmission = "";
            string nameAnt = "";
            string TXFreq = "";
            string RXFreq = "";
            string KOATUU = "";

            IMRecordset applFrom = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
            applFrom.Select("ID,OBJ_TABLE");
            applFrom.Select("DOZV_NUM");
            applFrom.Select("CONC_NUM");

            if (tableName == PlugTbl.XfaAbonentStation)
            {
                applFrom.Select("Abonent.ID");
                applFrom.Select("Abonent.PERM_NUM");
                applFrom.Select("Abonent.User.NAME");
                applFrom.Select("Abonent.DESIG_EMISSION");
                applFrom.Select("Abonent.STANDARD");
                applFrom.Select("Abonent.Equipment.NAME");
                applFrom.Select("Abonent.USE_REGION");
                applFrom.Select("Abonent.STATUS");
            }
            else if (tableName == PlugTbl.XfaAmateur)
            {
                applFrom.Select("AmateurStation.ID");
                applFrom.Select("AmateurStation.NUMB_DOZV");
                applFrom.Select("AmateurStation.User.NAME");
                applFrom.Select("AmateurStation.Position.PROVINCE");
                applFrom.Select("AmateurStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblEarthStation)
            {
                applFrom.Select("EarthStation.ID");
                applFrom.Select("EarthStation.MODIF_DECIS_NUM");
                applFrom.Select("EarthStation.SUPPR_DECIS_NUM");
                applFrom.Select("EarthStation.Owner.NAME");
                applFrom.Select("EarthStation.Position.REMARK");
                applFrom.Select("EarthStation.STANDARD");
                applFrom.Select("EarthStation.Position.CITY_ID");
                applFrom.Select("EarthStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblMicrowa)
            {
                applFrom.Select("Microwave.ID");
                applFrom.Select("Microwave.MODIF_DECIS_NUM");
                applFrom.Select("Microwave.SUPPR_DECIS_NUM");
                applFrom.Select("Microwave.Owner.NAME");
                applFrom.Select("Microwave.StationA.Position.REMARK");
                applFrom.Select("Microwave.StationB.Position.REMARK");
                applFrom.Select("Microwave.STANDARD");
                applFrom.Select("Microwave.Equipment.NAME");
                applFrom.Select("Microwave.DESIG_EM");
                applFrom.Select("Microwave.StationA.Ant1.NAME");
                applFrom.Select("Microwave.StationB.Ant1.NAME");
                applFrom.Select("Microwave.StationA.TX_FREQ");
                applFrom.Select("Microwave.StationB.TX_FREQ");
                applFrom.Select("Microwave.StationA.Position.CITY_ID");
                applFrom.Select("Microwave.StationB.Position.CITY_ID");
                applFrom.Select("Microwave.STATUS");
            }
            else if (tableName == ICSMTbl.itblMobStation2)
            {
                applFrom.Select("BaseSector1.ID");
                applFrom.Select("BaseSector1.CUST_TXT3");
                applFrom.Select("BaseSector1.CUST_TXT5");
                applFrom.Select("BaseSector1.Owner.NAME");
                applFrom.Select("BaseSector1.Position.REMARK");
                applFrom.Select("BaseSector1.STANDARD");
                applFrom.Select("BaseSector1.Equipment.NAME");
                applFrom.Select("BaseSector1.DESIG_EMISSION");
                applFrom.Select("BaseSector1.Antenna.NAME");
                applFrom.Select("BaseSector1.Position.CITY_ID");
                applFrom.Select("BaseSector1.STATUS");
            }
            else if (tableName == ICSMTbl.itblMobStation)
            {
                applFrom.Select("MobileSector1.ID");
                applFrom.Select("MobileSector1.CUST_TXT3");
                applFrom.Select("MobileSector1.CUST_TXT5");
                applFrom.Select("MobileSector1.Owner.NAME");
                applFrom.Select("MobileSector1.Position.REMARK");
                applFrom.Select("MobileSector1.STANDARD");
                applFrom.Select("MobileSector1.Equipment.NAME");
                applFrom.Select("MobileSector1.DESIG_EMISSION");
                applFrom.Select("MobileSector1.Position.CITY_ID");
                applFrom.Select("MobileSector1.Antenna.NAME");
                applFrom.Select("MobileSector1.STATUS");
            }
            else if (tableName == ICSMTbl.itblTV)
            {
                applFrom.Select("TvStation.ID");
                applFrom.Select("TvStation.CUST_TXT2"); //V
                applFrom.Select("TvStation.CUST_TXT3"); //D
                applFrom.Select("TvStation.Owner.NAME");
                applFrom.Select("TvStation.Position.REMARK");
                applFrom.Select("TvStation.STANDARD");
                applFrom.Select("TvStation.Equipment.NAME");
                applFrom.Select("TvStation.DESIG_EM");
                applFrom.Select("TvStation.Antenna.NAME");
                applFrom.Select("TvStation.FREQ");
                applFrom.Select("TvStation.Position.CITY_ID");
                applFrom.Select("TvStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblDVBT)
            {
                applFrom.Select("DvbtStation.ID");
                applFrom.Select("DvbtStation.CUST_TXT2"); //V
                applFrom.Select("DvbtStation.CUST_TXT3"); //D
                applFrom.Select("DvbtStation.Owner.NAME");
                applFrom.Select("DvbtStation.Position.REMARK");
                applFrom.Select("DvbtStation.STANDARD");
                applFrom.Select("DvbtStation.Equipment.NAME");
                applFrom.Select("DvbtStation.DESIG_EM");
                applFrom.Select("DvbtStation.Antenna.NAME");
                applFrom.Select("DvbtStation.FREQ");
                applFrom.Select("DvbtStation.Position.CITY_ID");
                applFrom.Select("DvbtStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblFM)
            {
                applFrom.Select("FmStation.ID");
                applFrom.Select("FmStation.CUST_TXT2"); //V
                applFrom.Select("FmStation.CUST_TXT3"); //D
                applFrom.Select("FmStation.Owner.NAME");
                applFrom.Select("FmStation.Position.REMARK");
                applFrom.Select("FmStation.STANDARD");
                applFrom.Select("FmStation.Equipment.NAME");
                applFrom.Select("FmStation.DESIG_EM");
                applFrom.Select("FmStation.Antenna.NAME");
                applFrom.Select("FmStation.FREQ");
                applFrom.Select("FmStation.Position.CITY_ID");
                applFrom.Select("FmStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblTDAB)
            {
                applFrom.Select("TdabStation.ID");
                applFrom.Select("TdabStation.CUST_TXT2"); //V
                applFrom.Select("TdabStation.CUST_TXT3"); //D
                applFrom.Select("TdabStation.Owner.NAME");
                applFrom.Select("TdabStation.Position.REMARK");
                applFrom.Select("TdabStation.STANDARD");
                applFrom.Select("TdabStation.Equipment.NAME");
                applFrom.Select("TdabStation.DESIG_EM");
                applFrom.Select("TdabStation.FREQ");
                applFrom.Select("TdabStation.Position.CITY_ID");
                applFrom.Select("TdabStation.STATUS");
            }
            else if (tableName == ICSMTbl.itblShip)
            {
                applFrom.Select("Ship.ID");
                applFrom.Select("Ship.CUST_TXT1");   // Висновок
                //applFrom.Select("Ship.CUST_TXT4");   // Дозвід
                applFrom.Select("Ship.Owner.NAME");  // Власник
                //applFrom.Select("Ship.CUST_TXT4");   // Адреса
                //applFrom.Select("Ship.CUST_TXT4");   // Стандарт(радіотехнологія)
                //applFrom.Select("Ship.CUST_TXT4");   // Назва обладнання
                //applFrom.Select("Ship.CUST_TXT4");   // Клас випромінювання
                //applFrom.Select("Ship.CUST_TXT4");   // Частота
                //applFrom.Select("Ship.CUST_TXT4");   // Місто
                applFrom.Select("Ship.STATUS");
            }


            applFrom.SetWhere("ID", IMRecordset.Operation.Eq, _applID);
            applFrom.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, tableName);
            try
            {
                applFrom.Open();
                if (!applFrom.IsEOF())
                {
                    if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblEarthStation)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("EarthStation.STATUS") == "P") || (applFrom.GetS("EarthStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("EarthStation.MODIF_DECIS_NUM");
                        numberDOZV = applFrom.GetS("EarthStation.SUPPR_DECIS_NUM");
                        OwnerName = applFrom.GetS("EarthStation.Owner.NAME");
                        position = applFrom.GetS("EarthStation.Position.REMARK");
                        standard = applFrom.GetS("EarthStation.STANDARD");
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("EarthStation.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                        {
                            IMRecordset rsEstaAssgn = new IMRecordset(ICSMTbl.itblEstaAssgn, IMRecordset.Mode.ReadOnly);
                            rsEstaAssgn.Select("Group.Antenna.EMI_RCP,FREQ");
                            rsEstaAssgn.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq,
                                                 applFrom.GetI("EarthStation.ID"));
                            try
                            {
                                for (rsEstaAssgn.Open(); !rsEstaAssgn.IsEOF(); rsEstaAssgn.MoveNext())
                                {
                                    string direction = rsEstaAssgn.GetS("Group.Antenna.EMI_RCP");
                                    if (direction == "R")
                                    {
                                        if (!string.IsNullOrEmpty(RXFreq))
                                            RXFreq += "; ";
                                        RXFreq += rsEstaAssgn.GetD("FREQ").ToString("F2");
                                    }
                                    else if (direction == "E")
                                    {
                                        if (!string.IsNullOrEmpty(TXFreq))
                                            TXFreq += "; ";
                                        TXFreq += rsEstaAssgn.GetD("FREQ").ToString("F2");
                                    }
                                }
                            }
                            finally
                            {
                                rsEstaAssgn.Destroy();
                            }
                        }
                        {
                            IMRecordset rsEstaEmiss = new IMRecordset(ICSMTbl.itblEstaEmiss, IMRecordset.Mode.ReadOnly);
                            rsEstaEmiss.Select("DESIG_EMISSION,Group.Antenna.Antenna.NAME,EquipConfig.NAME");
                            rsEstaEmiss.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq,
                                                 applFrom.GetI("EarthStation.ID"));
                            try
                            {
                                for (rsEstaEmiss.Open(); !rsEstaEmiss.IsEOF(); rsEstaEmiss.MoveNext())
                                {
                                    DesigEmission = rsEstaEmiss.GetS("DESIG_EMISSION");
                                    nameAnt = rsEstaEmiss.GetS("Group.Antenna.Antenna.NAME");
                                    nameRES = rsEstaEmiss.GetS("EquipConfig.NAME");
                                }
                            }
                            finally
                            {
                                rsEstaEmiss.Destroy();
                            }
                        }
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMicrowa)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("Microwave.STATUS") == "P") || (applFrom.GetS("Microwave.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("Microwave.MODIF_DECIS_NUM");
                        numberDOZV = applFrom.GetS("Microwave.SUPPR_DECIS_NUM");
                        OwnerName = applFrom.GetS("Microwave.Owner.NAME");
                        position = applFrom.GetS("Microwave.StationA.Position.REMARK") + " / " +
                                   applFrom.GetS("Microwave.StationB.Position.REMARK");
                        standard = applFrom.GetS("Microwave.STANDARD");
                        nameRES = applFrom.GetS("Microwave.Equipment.NAME");
                        DesigEmission = applFrom.GetS("Microwave.DESIG_EM");
                        nameAnt = applFrom.GetS("Microwave.StationA.Ant1.NAME") + " / " +
                                  applFrom.GetS("Microwave.StationB.Ant1.NAME");
                        TXFreq = applFrom.GetD("Microwave.StationA.TX_FREQ").ToString("F2") + " / " +
                                 applFrom.GetD("Microwave.StationB.TX_FREQ").ToString("F2");
                        RXFreq = applFrom.GetD("Microwave.StationB.TX_FREQ").ToString("F2") + " / " +
                                 applFrom.GetD("Microwave.StationA.TX_FREQ").ToString("F2");
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if ((applFrom.GetI("Microwave.StationA.Position.CITY_ID") != IM.NullI) &&
                            (applFrom.GetI("Microwave.StationB.Position.CITY_ID") != IM.NullI))
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMobStation2)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("BaseSector1.STATUS") == "P") || (applFrom.GetS("BaseSector1.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("BaseSector1.CUST_TXT3");
                        numberDOZV = applFrom.GetS("BaseSector1.CUST_TXT5");
                        OwnerName = applFrom.GetS("BaseSector1.Owner.NAME");
                        position = applFrom.GetS("BaseSector1.Position.REMARK");
                        standard = applFrom.GetS("BaseSector1.STANDARD");
                        nameRES = applFrom.GetS("BaseSector1.Equipment.NAME");
                        DesigEmission = applFrom.GetS("BaseSector1.DESIG_EMISSION");
                        nameAnt = applFrom.GetS("BaseSector1.Antenna.NAME");
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("BaseSector1.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                        {
                            IMRecordset freq = new IMRecordset(ICSMTbl.itblMobStaFreqs2, IMRecordset.Mode.ReadOnly);
                            freq.Select("ID,RX_FREQ,TX_FREQ");
                            freq.SetWhere("Station.ID", IMRecordset.Operation.Eq, applFrom.GetI("BaseSector1.ID"));
                            try
                            {
                                for (freq.Open(); !freq.IsEOF(); freq.MoveNext())
                                {
                                    if (!string.IsNullOrEmpty(TXFreq))
                                        TXFreq += "; ";
                                    TXFreq += freq.GetD("TX_FREQ").ToString("F2");

                                    if (!string.IsNullOrEmpty(RXFreq))
                                        RXFreq += "; ";
                                    RXFreq += freq.GetD("RX_FREQ").ToString("F2");
                                }
                            }
                            finally
                            {
                                freq.Destroy();
                            }
                        }
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblMobStation)
                    {
                        int ID = applFrom.GetI("MobileSector1.ID");
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("MobileSector1.STATUS") == "P") || (applFrom.GetS("MobileSector1.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("MobileSector1.CUST_TXT3");
                        numberDOZV = applFrom.GetS("MobileSector1.CUST_TXT5");
                        OwnerName = applFrom.GetS("MobileSector1.Owner.NAME");
                        position = applFrom.GetS("MobileSector1.Position.REMARK");
                        standard = applFrom.GetS("MobileSector1.STANDARD");
                        nameRES = applFrom.GetS("MobileSector1.Equipment.NAME");
                        DesigEmission = applFrom.GetS("MobileSector1.DESIG_EMISSION");
                        nameAnt = applFrom.GetS("MobileSector1.Antenna.NAME");
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("MobileSector1.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                        {
                            IMRecordset freq = new IMRecordset(ICSMTbl.itblMobStaFreqs, IMRecordset.Mode.ReadOnly);
                            freq.Select("ID,RX_FREQ,TX_FREQ");
                            freq.SetWhere("Station.ID", IMRecordset.Operation.Eq, applFrom.GetI("MobileSector1.ID"));
                            try
                            {
                                for (freq.Open(); !freq.IsEOF(); freq.MoveNext())
                                {
                                    if (!string.IsNullOrEmpty(TXFreq))
                                        TXFreq += "; ";
                                    TXFreq += freq.GetD("TX_FREQ").ToString("F2");

                                    if (!string.IsNullOrEmpty(RXFreq))
                                        RXFreq += "; ";
                                    RXFreq += freq.GetD("RX_FREQ").ToString("F2");
                                }
                            }
                            finally
                            {
                                freq.Destroy();
                            }
                        }
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblTV)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("TvStation.STATUS") == "P") || (applFrom.GetS("TvStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("TvStation.CUST_TXT2");
                        numberDOZV = applFrom.GetS("TvStation.CUST_TXT3");
                        OwnerName = applFrom.GetS("TvStation.Owner.NAME");
                        position = applFrom.GetS("TvStation.Position.REMARK");
                        standard = applFrom.GetS("TvStation.STANDARD");
                        nameRES = applFrom.GetS("TvStation.Equipment.NAME");
                        DesigEmission = applFrom.GetS("TvStation.DESIG_EM");
                        nameAnt = applFrom.GetS("TvStation.Antenna.NAME");
                        TXFreq = applFrom.GetD("TvStation.FREQ").ToString("F2");
                        RXFreq = "-";
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("TvStation.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblDVBT)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("DvbtStation.STATUS") == "P") || (applFrom.GetS("DvbtStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("DvbtStation.CUST_TXT2");
                        numberDOZV = applFrom.GetS("DvbtStation.CUST_TXT3");
                        OwnerName = applFrom.GetS("DvbtStation.Owner.NAME");
                        position = applFrom.GetS("DvbtStation.Position.REMARK");
                        standard = applFrom.GetS("DvbtStation.STANDARD");
                        nameRES = applFrom.GetS("DvbtStation.Equipment.NAME");
                        DesigEmission = applFrom.GetS("DvbtStation.DESIG_EM");
                        nameAnt = applFrom.GetS("DvbtStation.Antenna.NAME");
                        TXFreq = applFrom.GetD("DvbtStation.FREQ").ToString("F2");
                        RXFreq = "-";
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("DvbtStation.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblFM)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("FmStation.STATUS") == "P") || (applFrom.GetS("FmStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("FmStation.CUST_TXT2");
                        numberDOZV = applFrom.GetS("FmStation.CUST_TXT3");
                        OwnerName = applFrom.GetS("FmStation.Owner.NAME");
                        position = applFrom.GetS("FmStation.Position.REMARK");
                        standard = applFrom.GetS("FmStation.STANDARD");
                        nameRES = applFrom.GetS("FmStation.Equipment.NAME");
                        DesigEmission = applFrom.GetS("FmStation.DESIG_EM");
                        nameAnt = applFrom.GetS("FmStation.Antenna.NAME");
                        TXFreq = applFrom.GetD("FmStation.FREQ").ToString("F2");
                        RXFreq = "-";
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("FmStation.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblTDAB)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("TdabStation.STATUS") == "P") || (applFrom.GetS("TdabStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = applFrom.GetS("TdabStation.CUST_TXT2");
                        numberDOZV = applFrom.GetS("TdabStation.CUST_TXT3");
                        OwnerName = applFrom.GetS("TdabStation.Owner.NAME");
                        position = applFrom.GetS("TdabStation.Position.REMARK");
                        standard = applFrom.GetS("TdabStation.STANDARD");
                        nameRES = applFrom.GetS("TdabStation.Equipment.NAME");
                        DesigEmission = applFrom.GetS("TdabStation.DESIG_EM");
                        nameAnt = "-";
                        TXFreq = applFrom.GetD("TdabStation.FREQ").ToString("F2");
                        RXFreq = "-";
                        KOATUU = CLocaliz.TxT(KOATUU_NO);
                        if (applFrom.GetI("TdabStation.Position.CITY_ID") != IM.NullI)
                            KOATUU = CLocaliz.TxT(KOATUU_YES);
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == PlugTbl.XfaAbonentStation)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("Abonent.STATUS") == "P") || (applFrom.GetS("Abonent.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = "-";
                        numberDOZV = applFrom.GetS("Abonent.PERM_NUM");
                        OwnerName = applFrom.GetS("Abonent.User.NAME");
                        position = applFrom.GetS("Abonent.USE_REGION");
                        standard = applFrom.GetS("Abonent.STANDARD");
                        nameRES = applFrom.GetS("Abonent.Equipment.NAME");
                        DesigEmission = applFrom.GetS("Abonent.DESIG_EMISSION");
                        nameAnt = "-";
                        TXFreq = "-";
                        RXFreq = "-";
                        KOATUU = "-";
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == ICSMTbl.itblShip)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("Ship.STATUS") == "P") || (applFrom.GetS("Ship.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = "-";
                        numberDOZV = applFrom.GetS("Ship.CUST_TXT1");
                        OwnerName = applFrom.GetS("Ship.Owner.NAME");
                        position = "-";
                        standard = "-";
                        nameRES = "-";
                        DesigEmission = "-";
                        nameAnt = "-";
                        TXFreq = "-";
                        RXFreq = "-";
                        KOATUU = "-";
                    }
                    else if (applFrom.GetS("OBJ_TABLE") == PlugTbl.XfaAmateur)
                    {
                        if (OnlyWorkStatus) { isWorkStatus = ((applFrom.GetS("AmateurStation.STATUS") == "P") || (applFrom.GetS("AmateurStation.STATUS") == "PP")) ? true : false; if (isWorkStatus == false) return null; }
                        numberVISN = "-";
                        numberDOZV = applFrom.GetS("AmateurStation.NUMB_DOZV");
                        OwnerName = applFrom.GetS("AmateurStation.User.NAME");
                        position = applFrom.GetS("AmateurStation.Position.PROVINCE");
                        standard = "";
                        nameRES = "";
                        DesigEmission = "";
                        nameAnt = "-";
                        TXFreq = "-";
                        RXFreq = "-";
                        KOATUU = "-";
                    }
                    numberDOZV = applFrom.GetS("DOZV_NUM");
                    numberVISN = applFrom.GetS("CONC_NUM");

                    retVal = new string[] {
                  _applID.ToString(),
                  numberDOZV,
                  numberVISN,
                  standard,
                  OwnerName,
                  position,
                  TXFreq,
                  RXFreq,
                  nameRES,
                  DesigEmission,
                  nameAnt,
                  KOATUU
               };
                }
            }
            finally
            {
                applFrom.Close();
                applFrom.Destroy();
            }
            return retVal;
        }

        //=================================================
        /// <summary>
        /// Выбрали запись
        /// </summary>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            bool tmpBool = false;
            try
            {
                string str_yes = CLocaliz.TxT(KOATUU_YES);
                for (int row = 0; row < dataGridAppl.RowCount; row++)
                {
                    if (dataGridAppl.Rows[row].Selected == true)
                    {
                        if (appType != AppType.AppAR_3 && appType != AppType.AppTR_З &&
                            str_yes != dataGridAppl.Rows[row].Cells[indexKOATUU].Value.ToString())
                        {
                            tmpBool = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
                DialogResult = DialogResult.None;
                return;
            }
            if ((appType != AppType.AppZRS) && (tmpBool == true) &&
                (MessageBox.Show(CLocaliz.TxT("Some selected Appl don't have code KOATUU. Continue?"),
                                 CLocaliz.TxT("Warning"), MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question) != DialogResult.Yes))
            {
                DialogResult = DialogResult.None;
                return;
            }
            // Проверка привязки к пакету ПТК
            List<string> messageList = new List<string>();
            try
            {
                for (int row = 0; row < dataGridAppl.RowCount; row++)
                {
                    if (dataGridAppl.Rows[row].Selected == true)
                    {
                        int tmpId = dataGridAppl.Rows[row].Cells[0].Value.ToString().ToInt32(IM.NullI);
                        if (tmpId != IM.NullI)
                        {
                            string DOZV = "";
                            YXnrfaAppl applx = new YXnrfaAppl();
                            applx.Format("ID,DOZV_NUM,DOZV_NUM_NEW");
                            if (applx.Fetch(tmpId)) {
                                DOZV = applx.m_dozv_num_new == "" ? (applx.m_dozv_num != "" ? applx.m_dozv_num : applx.m_dozv_num_new) : applx.m_dozv_num_new;
                                if (packetType== EPacketType.PckDuplicate)
                                    R_docs_dupl.Add(new KeyValuePair<int, string>(tmpId, DOZV));
                            }

                            IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                            rs.Select("PACKET_ID");
                            rs.Select("APPL_ID");
                            rs.Select("Packet.NUMBER_IN");
                            rs.Select("Packet.NUMBER_OUT");
                            rs.Select("Packet.STATUS");
                            rs.SetWhere("Packet.CONTENT", IMRecordset.Operation.Like, EPacketType.PckPTKNVTV.ToString());
                            rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, tmpId);
                            try
                            {
                                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                                {
                                    string status = rs.GetS("Packet.STATUS");
                                    if (status == StatePacket.SentToUrcp.ToString())
                                        status = "Передано до УРЧП";
                                    else
                                        status = "";
                                    string tmpStr = string.Format("Вх.номер: {0}; Вих.номер: {1}; Статус:{2}",
                                                                  rs.GetS("Packet.NUMBER_IN"),
                                                                  rs.GetS("Packet.NUMBER_OUT"),
                                                                  status);
                                    messageList.Add(tmpStr);
                                }
                            }
                            finally
                            {
                                rs.Final();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
                DialogResult = DialogResult.None;
                return;
            }
            if (messageList.Count > 0)
            {
                StringBuilder strBuild = new StringBuilder(100);
                foreach (string val in messageList)
                    strBuild.Append(string.Format("{0}{1}", val, Environment.NewLine));
                strBuild.Append("Приєднати заявку?");
                if (MessageBox.Show(strBuild.ToString(),
                                    "Заявка вже приєднана до пакету ПТК/ТВ/НВ", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    DialogResult = DialogResult.None;
                    return;
                }
            }
        }
        //===================================================
        /// <summary>
        /// Обновляет поле контрагента
        /// </summary>
        private bool SelectOwner()
        {
            string param = "{NAME=\"*" + XICSM.UcrfRfaNET.HelpClasses.HelpFunction.ReplaceQuotaSumbols(tbOwner.Text) + "*\"}";
            RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Seaching of an owner"), ICSMTbl.itblUsers, param);
            if (user.Id != IM.NullI)
            {
                IDOwner = user.Id;
                return true;
            }
            return false;
        }
        //===================================================
        /// <summary>
        /// Возвращает имя котрагента
        /// </summary>
        public string OwnerName
        {
            get
            {
                string name = "";
                IMRecordset r = new IMRecordset(ICSMTbl.itblUsers, IMRecordset.Mode.ReadOnly);
                r.Select("ID,NAME");
                r.SetWhere("ID", IMRecordset.Operation.Eq, IDOwner);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        name = r.GetS("NAME");
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
                return name;
            }
        }
        //===================================================
        /// <summary>
        /// Поиск нового контрагента
        /// </summary>
        private void tbOwner_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Enter)
            {
                if (SelectOwner())
                    tbOwner.Text = OwnerName;
            }
        }
        //===================================================
        /// <summary>
        /// Возвращает список выбраных заяв
        /// </summary>
        /// <returns>Список выбраных заяв</returns>
        public List<int> GetSelectedAppl()
        {
            List<int> retVal = new List<int>();
            try
            {
                for (int row = 0; row < dataGridAppl.RowCount; row++)
                {
                    if (dataGridAppl.Rows[row].Selected == true)
                    {
                        int tmpID = Convert.ToInt32(dataGridAppl.Rows[row].Cells[0].Value.ToString());
                        retVal.Add(tmpID);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error");
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Выбирает все пункты выпадающего списка
        /// </summary>
        /// <returns>Список выбраных заяв</returns>
        private void chComboBoxAllSelected()
        {

            for (int k = 0; k < chComboBox.CheckBoxItems.Count(); k++)
            {
                chComboBox.CheckBoxItems[k].Checked = true;
            }
        }

        //===================================================
        /// <summary>
        /// Выбирает все пункты выпадающего списка если нажат пункт Вся Україна
        /// </summary>
        /// <returns>Список выбраных областей</returns>
        private void item_CheckedChanged(object sender, EventArgs e)
        {
            const string AllUkr = "Вся Україна";
            // Если НЕ выбран пункт "Вся Україна"
            if ((((CheckBoxComboBoxItem)(sender)).Text == AllUkr) && !(((CheckBoxComboBoxItem)(sender)).Checked))
            {
                for (int k = 0; k < chComboBox.CheckBoxItems.Count(); k++)
                {
                    chComboBox.CheckBoxItems[StatusListGl[k].Name].Checked = false;
                }
            }
            // Если  выбран пункт "Вся Україна"
            if ((((CheckBoxComboBoxItem)(sender)).Text == AllUkr) && (((CheckBoxComboBoxItem)(sender)).Checked))
            {
                for (int k = 0; k < chComboBox.CheckBoxItems.Count(); k++)
                {
                    chComboBox.CheckBoxItems[StatusListGl[k].Name].Checked = true;
                }
            }

        }


        //===================================================
        /// <summary>
        /// Выполняет проверкую - если нажат пункт "Вся Україна", то возвращает ИСТИНА (true)
        /// </summary>
        /// <returns>Список выбраных заяв</returns>
        private bool isAllChecked()
      {
          bool ret = false;
          const string AllUkr = "Вся Україна";
          
              for (int k = 0; k < chComboBox.CheckBoxItems.Count(); k++)
              {
                  if ((StatusListGl[k].Name==AllUkr) && (chComboBox.CheckBoxItems[StatusListGl[k].Name].Checked))
                  {
                    ret=true;
                    break;
                  }
             }
          return ret;
        
   }

        private void resetSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult DA = MessageBox.Show("Очистити попередні налагодження відображення пекетних даних? (Так/Ні)", "", MessageBoxButtons.YesNo);
            if (DA == System.Windows.Forms.DialogResult.Yes)
            {
                ResetComponentSettings();
                if (packet.ListSelectedColumns != null)
                    packet.ListSelectedColumns.Clear();
                packet.isDefaultListColumns = true;
                InitGrid();
            }
        }

        /// <summary>
        /// Загрузка структуры
        /// </summary>
        public void LoadComponentSettings()
        {
            try
            {
                List<ColumnInfoGridView> L_FND_MAIN = _settings.ColumnInfo.FindAll(r => r.appType == AppType.AppUnknown);
                if (L_FND_MAIN != null)
                {
                    L_FND_MAIN.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                    foreach (ColumnInfoGridView colInfo in L_FND_MAIN)
                    {
                        DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                        if (col != null) { col.Width = colInfo.Width; }
                        else
                        {
                            if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                            ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 2);
                            if (Fnd != null)
                            {
                                if (!packet.ListSelectedColumns.Contains(Fnd))
                                    packet.ListSelectedColumns.Add(Fnd);
                            }
                        }
                    }
                }

                List<ColumnInfoGridView> L_FND = _settings.ColumnInfo.FindAll(r => r.appType == packet.ApplType);
                if (L_FND != null)
                {
                    L_FND.Sort((emp1, emp2) => emp1.DisplayIndex.CompareTo(emp2.DisplayIndex));
                    foreach (ColumnInfoGridView colInfo in L_FND)
                    {
                        DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                        if (col != null) { col.Width = colInfo.Width; }
                        else
                        {
                            if (packet.ListSelectedColumns == null) packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                            ExtendedColumnsProperty Fnd = packet.ListAllColumns.Find(r => r.NameProperty == colInfo.Name.Replace("grid", "") && r.TypeRadiosys == packet.ApplType);
                            if (Fnd != null)
                            {
                                if (!packet.ListSelectedColumns.Contains(Fnd))
                                    packet.ListSelectedColumns.Add(Fnd);
                            }
                            ExtendedColumnsProperty Fnd_unknown = packet.ListAllColumns.Find(r => r.PathToColumn == colInfo.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 2);
                            if (Fnd_unknown == null)
                                AddGridColumn(null, colInfo.Name, CLocaliz.TxT(colInfo.Name), colInfo.Width, true, colInfo.Name);
                        }
                    }

                    int idx = 0;
                    foreach (ColumnInfoGridView colInfo in L_FND)
                    {
                        DataGridViewColumn col = dataGridAppl.Columns[colInfo.Name];
                        if (col != null)
                        {
                            col.Width = colInfo.Width;
                            if (colInfo.DisplayIndex <= (dataGridAppl.Columns.Count - 1))
                                col.DisplayIndex = colInfo.DisplayIndex;
                            else col.DisplayIndex = idx;
                        }
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                ResetComponentSettings();
            }
        }

        public void ResetComponentSettings()
        {
            //_settings.Reset();
            ClearComponentSetting();
        }

        public void ClearComponentSetting()
        {
            List<ColumnInfoGridView> L_Del = _settings.ColumnInfo.FindAll(r => r.appType == packet.ApplType || (r.appType == AppType.AppUnknown));
            foreach (ColumnInfoGridView del in L_Del)
                _settings.ColumnInfo.Remove(del);
        }

        //Сохранение структуры
        public void SaveComponentSettings()
        {
            //_settings.ColumnInfo.Clear();
            ClearComponentSetting();
            if (packet.ListSelectedColumns != null)
            {
                if (packet.ListSelectedColumns.Count() == 0)
                {
                    InitGrid();
                }
            }

            foreach (DataGridViewColumn col in dataGridAppl.Columns)
            {
                if (packet.ListSelectedColumns != null)
                {
                    if ((packet.ListSelectedColumns.Find(r => r.NameProperty == col.Name && r.TypeRadiosys == packet.ApplType) != null))
                    {
                        if (_settings.ColumnInfo.Find(r => r.Name == col.Name) != null)
                        {
                            continue;
                        }
                    }
                }
                ColumnInfoGridView colInfo = new ColumnInfoGridView();
                colInfo.Name = col.Name;
                colInfo.Width = col.Width;
                colInfo.appType = packet.ApplType;
                colInfo.DisplayIndex = col.DisplayIndex;
                if (packet.ListSelectedColumns.Find(r => r.PathToColumn == col.Name && r.TypeRadiosys == AppType.AppUnknown && r.Number_List == 2) != null)
                    colInfo.appType = AppType.AppUnknown;
                _settings.ColumnInfo.Add(colInfo);
            }
            List<string> str_rem = new List<string>();
            foreach (ColumnInfoGridView v in _settings.ColumnInfo)
            {
                ExtendedColumnsProperty pr_ = packet.ListAllColumns.Find(r => r.NameProperty == v.Name && r.TypeRadiosys == packet.ApplType);
                if ((pr_ != null) && (packet.ListSelectedColumns.Find(r => r.NameProperty == v.Name && r.TypeRadiosys == packet.ApplType) == null))
                {
                    str_rem.Add(v.Name);
                }
            }
            foreach (string s in str_rem)
                _settings.ColumnInfo.RemoveAll(r => r.Name == s && r.appType == packet.ApplType);

            _settings.Save();
        }

        private void FFindStation_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveComponentSettings();
        }

        private void contextMenuStrip_Grid_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStrip_OpenDesigner_Click(object sender, EventArgs e)
        {
            using (FChannels fChannel = new FChannels("Вибір стовпця", "Перелік стовпців"))
            {
                List<ExtendedColumnsProperty> porp_column_all = packet.ListAllColumns.FindAll(r => r.TypeRadiosys == packet.ApplType || ((r.TypeRadiosys == AppType.AppUnknown) && (r.Number_List == 2)));
                if (porp_column_all != null)
                {
                    foreach (ExtendedColumnsProperty item in porp_column_all)
                    {
                        fChannel.CLBAllChannel.Items.Add(item);
                    }
                }

                if (packet.ListSelectedColumns != null)
                {
                    foreach (ExtendedColumnsProperty item in packet.ListSelectedColumns)
                    {
                        fChannel.CLBSelectChannel.Items.Add(item);
                    }
                }

                if (fChannel.CLBSelectChannel.Items.Count > 0)
                {
                    foreach (ExtendedColumnsProperty item in fChannel.CLBSelectChannel.Items)
                    {
                        if (porp_column_all.Find(r => r.TypeRadiosys == item.TypeRadiosys && r.CaptionProperty == item.CaptionProperty && r.NameProperty == item.NameProperty && r.PathToColumn == item.PathToColumn) != null)
                        {
                            ExtendedColumnsProperty bnd = porp_column_all.Find(r => r.TypeRadiosys == item.TypeRadiosys && r.CaptionProperty == item.CaptionProperty && r.NameProperty == item.NameProperty && r.PathToColumn == item.PathToColumn);
                            fChannel.CLBAllChannel.Items.Remove(bnd);
                        }
                    }
                }

                if (fChannel.ShowDialog() == DialogResult.OK)
                {
                    packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                    if (fChannel.CLBSelectChannel.Items.Count > 0)
                    {
                        for (int i = 0; i < fChannel.CLBSelectChannel.Items.Count; i++)
                        {
                            packet.ListSelectedColumns.Add(((ExtendedColumnsProperty)(fChannel.CLBSelectChannel.Items[i])));
                        }
                    }
                    else
                    {
                        packet.ListSelectedColumns = new List<ExtendedColumnsProperty>();
                    }
                }
            }

            SaveComponentSettings();
            InitGrid();
            //dataGridAppl.Rows.Clear();
            //buttonUpdate_Click(this, e);
            //dataGridAppl.Refresh();
            //dataGridAppl.Update();
        }
    }


    /// <summary>
    /// Отдельный класс для хранения занчений номера и названия области в пунктах компонента ComboBoxCheckBox
    /// </summary>
    public class Status
    {
        public Status(int id, string name) { _Id = id; _Name = name; }

        private int _Id;
        private string _Name;

        public int Id { get { return _Id; } set { _Id = value; } }
        public string Name { get { return _Name; } set { _Name = value; } }


        public override string ToString() { return Name; }
    }

    public class StatusList : List<Status>
    {
    }
}
