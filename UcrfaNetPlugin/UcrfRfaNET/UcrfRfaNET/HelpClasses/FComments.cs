﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses
{
	public partial class FComments : Form
	{	
		public FComments()
		{
			InitializeComponent();
		}

		public string GetComment()
		{
			return this.tbComments.Text;
		}
	}
}
