﻿namespace XICSM.UcrfRfaNET
{
   partial class FAbonentOwner
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTelephone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPassport = new System.Windows.Forms.TextBox();
            this.lblPassp = new System.Windows.Forms.Label();
            this.tbOKPO = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbRematrk = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNumFile = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNameLatin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBoxSurnameLat = new System.Windows.Forms.TextBox();
            this.lblSurnameLat = new System.Windows.Forms.Label();
            this.txtBoxSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtBoxIndex = new System.Windows.Forms.TextBox();
            this.lblIndex = new System.Windows.Forms.Label();
            this.cmbBoxObl = new System.Windows.Forms.ComboBox();
            this.cmbBoxDistr = new System.Windows.Forms.ComboBox();
            this.cmbBoxLoc = new System.Windows.Forms.ComboBox();
            this.lblDistr = new System.Windows.Forms.Label();
            this.txtBoxFax = new System.Windows.Forms.TextBox();
            this.lblFax = new System.Windows.Forms.Label();
            this.txtBoxSerPassp = new System.Windows.Forms.TextBox();
            this.txtBoxNumbPassp = new System.Windows.Forms.TextBox();
            this.lblPubl = new System.Windows.Forms.Label();
            this.txtBoxName2 = new System.Windows.Forms.TextBox();
            this.txtBoxFullName = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.txtBoxDateBorn = new System.Windows.Forms.TextBox();
            this.txtBoxDatePassp = new System.Windows.Forms.TextBox();
            this.txtBoxShortName = new System.Windows.Forms.TextBox();
            this.lblShortName = new System.Windows.Forms.Label();
            this.txtBoxBranchCode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name/Partronymic (UKR)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbName
            // 
            this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbName.Location = new System.Drawing.Point(188, 56);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(167, 20);
            this.tbName.TabIndex = 2;
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(334, 485);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 21;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(428, 485);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 22;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // tbAddress
            // 
            this.tbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddress.Location = new System.Drawing.Point(188, 241);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(348, 20);
            this.tbAddress.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 244);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Address";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbTelephone
            // 
            this.tbTelephone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTelephone.Location = new System.Drawing.Point(188, 267);
            this.tbTelephone.Name = "tbTelephone";
            this.tbTelephone.Size = new System.Drawing.Size(348, 20);
            this.tbTelephone.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 270);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Telephone";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbPassport
            // 
            this.tbPassport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassport.Location = new System.Drawing.Point(188, 344);
            this.tbPassport.Name = "tbPassport";
            this.tbPassport.Size = new System.Drawing.Size(348, 20);
            this.tbPassport.TabIndex = 17;
            // 
            // lblPassp
            // 
            this.lblPassp.Location = new System.Drawing.Point(3, 324);
            this.lblPassp.Name = "lblPassp";
            this.lblPassp.Size = new System.Drawing.Size(178, 13);
            this.lblPassp.TabIndex = 0;
            this.lblPassp.Text = "Pasport:series / № / date";
            this.lblPassp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbOKPO
            // 
            this.tbOKPO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOKPO.Location = new System.Drawing.Point(188, 370);
            this.tbOKPO.Name = "tbOKPO";
            this.tbOKPO.Size = new System.Drawing.Size(167, 20);
            this.tbOKPO.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 373);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "OKPO / IPN/ BRANCH CODE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbRematrk
            // 
            this.tbRematrk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRematrk.Location = new System.Drawing.Point(188, 422);
            this.tbRematrk.Multiline = true;
            this.tbRematrk.Name = "tbRematrk";
            this.tbRematrk.Size = new System.Drawing.Size(348, 52);
            this.tbRematrk.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 422);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Remark";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNumFile
            // 
            this.tbNumFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNumFile.Location = new System.Drawing.Point(188, 396);
            this.tbNumFile.Name = "tbNumFile";
            this.tbNumFile.Size = new System.Drawing.Size(348, 20);
            this.tbNumFile.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 396);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(178, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Case";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNameLatin
            // 
            this.tbNameLatin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNameLatin.Location = new System.Drawing.Point(188, 135);
            this.tbNameLatin.Name = "tbNameLatin";
            this.tbNameLatin.Size = new System.Drawing.Size(348, 20);
            this.tbNameLatin.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(3, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Name (LAT)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(178, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Birthday";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxSurnameLat
            // 
            this.txtBoxSurnameLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSurnameLat.Location = new System.Drawing.Point(188, 109);
            this.txtBoxSurnameLat.Name = "txtBoxSurnameLat";
            this.txtBoxSurnameLat.Size = new System.Drawing.Size(348, 20);
            this.txtBoxSurnameLat.TabIndex = 4;
            // 
            // lblSurnameLat
            // 
            this.lblSurnameLat.Location = new System.Drawing.Point(3, 112);
            this.lblSurnameLat.Name = "lblSurnameLat";
            this.lblSurnameLat.Size = new System.Drawing.Size(178, 13);
            this.lblSurnameLat.TabIndex = 0;
            this.lblSurnameLat.Text = "Surname (LAT)";
            this.lblSurnameLat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxSurname
            // 
            this.txtBoxSurname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSurname.Location = new System.Drawing.Point(188, 32);
            this.txtBoxSurname.Name = "txtBoxSurname";
            this.txtBoxSurname.Size = new System.Drawing.Size(348, 20);
            this.txtBoxSurname.TabIndex = 1;
            // 
            // lblSurname
            // 
            this.lblSurname.Location = new System.Drawing.Point(3, 35);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(178, 13);
            this.lblSurname.TabIndex = 0;
            this.lblSurname.Text = "Surname (UKR)";
            this.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxIndex
            // 
            this.txtBoxIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxIndex.Location = new System.Drawing.Point(188, 187);
            this.txtBoxIndex.Name = "txtBoxIndex";
            this.txtBoxIndex.Size = new System.Drawing.Size(88, 20);
            this.txtBoxIndex.TabIndex = 7;
            // 
            // lblIndex
            // 
            this.lblIndex.Location = new System.Drawing.Point(3, 190);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(178, 13);
            this.lblIndex.TabIndex = 0;
            this.lblIndex.Text = "Index / Region";
            this.lblIndex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbBoxObl
            // 
            this.cmbBoxObl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxObl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxObl.FormattingEnabled = true;
            this.cmbBoxObl.Location = new System.Drawing.Point(282, 187);
            this.cmbBoxObl.Name = "cmbBoxObl";
            this.cmbBoxObl.Size = new System.Drawing.Size(254, 21);
            this.cmbBoxObl.TabIndex = 8;
            this.cmbBoxObl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbBoxObl_MouseClick);
            this.cmbBoxObl.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.cmbBoxObl_PreviewKeyDown);
            // 
            // cmbBoxDistr
            // 
            this.cmbBoxDistr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxDistr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxDistr.FormattingEnabled = true;
            this.cmbBoxDistr.Location = new System.Drawing.Point(188, 214);
            this.cmbBoxDistr.Name = "cmbBoxDistr";
            this.cmbBoxDistr.Size = new System.Drawing.Size(167, 21);
            this.cmbBoxDistr.TabIndex = 9;
            this.cmbBoxDistr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBoxDistr_KeyDown);
            this.cmbBoxDistr.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbBoxDistr_MouseClick);
            // 
            // cmbBoxLoc
            // 
            this.cmbBoxLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBoxLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxLoc.FormattingEnabled = true;
            this.cmbBoxLoc.Location = new System.Drawing.Point(361, 214);
            this.cmbBoxLoc.Name = "cmbBoxLoc";
            this.cmbBoxLoc.Size = new System.Drawing.Size(175, 21);
            this.cmbBoxLoc.TabIndex = 10;
            this.cmbBoxLoc.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbBoxLoc_MouseClick);
            // 
            // lblDistr
            // 
            this.lblDistr.Location = new System.Drawing.Point(3, 217);
            this.lblDistr.Name = "lblDistr";
            this.lblDistr.Size = new System.Drawing.Size(178, 13);
            this.lblDistr.TabIndex = 0;
            this.lblDistr.Text = "District / City";
            this.lblDistr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxFax
            // 
            this.txtBoxFax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxFax.Location = new System.Drawing.Point(188, 293);
            this.txtBoxFax.Name = "txtBoxFax";
            this.txtBoxFax.Size = new System.Drawing.Size(348, 20);
            this.txtBoxFax.TabIndex = 13;
            // 
            // lblFax
            // 
            this.lblFax.Location = new System.Drawing.Point(3, 296);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(178, 13);
            this.lblFax.TabIndex = 0;
            this.lblFax.Text = "Fax";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxSerPassp
            // 
            this.txtBoxSerPassp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSerPassp.Location = new System.Drawing.Point(188, 320);
            this.txtBoxSerPassp.Name = "txtBoxSerPassp";
            this.txtBoxSerPassp.Size = new System.Drawing.Size(44, 20);
            this.txtBoxSerPassp.TabIndex = 14;
            // 
            // txtBoxNumbPassp
            // 
            this.txtBoxNumbPassp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxNumbPassp.Location = new System.Drawing.Point(239, 320);
            this.txtBoxNumbPassp.Name = "txtBoxNumbPassp";
            this.txtBoxNumbPassp.Size = new System.Drawing.Size(117, 20);
            this.txtBoxNumbPassp.TabIndex = 15;
            // 
            // lblPubl
            // 
            this.lblPubl.Location = new System.Drawing.Point(3, 347);
            this.lblPubl.Name = "lblPubl";
            this.lblPubl.Size = new System.Drawing.Size(178, 13);
            this.lblPubl.TabIndex = 0;
            this.lblPubl.Text = "Passport issued";
            this.lblPubl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxName2
            // 
            this.txtBoxName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxName2.Location = new System.Drawing.Point(369, 56);
            this.txtBoxName2.Name = "txtBoxName2";
            this.txtBoxName2.Size = new System.Drawing.Size(167, 20);
            this.txtBoxName2.TabIndex = 3;
            // 
            // txtBoxFullName
            // 
            this.txtBoxFullName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxFullName.Location = new System.Drawing.Point(188, 6);
            this.txtBoxFullName.Name = "txtBoxFullName";
            this.txtBoxFullName.ReadOnly = true;
            this.txtBoxFullName.Size = new System.Drawing.Size(348, 20);
            this.txtBoxFullName.TabIndex = 39;
            // 
            // lblFullName
            // 
            this.lblFullName.Location = new System.Drawing.Point(3, 9);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(178, 13);
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "Full Name";
            this.lblFullName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxDateBorn
            // 
            this.txtBoxDateBorn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxDateBorn.Location = new System.Drawing.Point(188, 161);
            this.txtBoxDateBorn.Name = "txtBoxDateBorn";
            this.txtBoxDateBorn.Size = new System.Drawing.Size(348, 20);
            this.txtBoxDateBorn.TabIndex = 6;
            this.txtBoxDateBorn.Leave += new System.EventHandler(this.txtBoxDateBorn_Leave);
            // 
            // txtBoxDatePassp
            // 
            this.txtBoxDatePassp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxDatePassp.Location = new System.Drawing.Point(361, 320);
            this.txtBoxDatePassp.Name = "txtBoxDatePassp";
            this.txtBoxDatePassp.Size = new System.Drawing.Size(175, 20);
            this.txtBoxDatePassp.TabIndex = 16;
            // 
            // txtBoxShortName
            // 
            this.txtBoxShortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxShortName.Location = new System.Drawing.Point(188, 82);
            this.txtBoxShortName.Name = "txtBoxShortName";
            this.txtBoxShortName.Size = new System.Drawing.Size(348, 20);
            this.txtBoxShortName.TabIndex = 4;
            // 
            // lblShortName
            // 
            this.lblShortName.Location = new System.Drawing.Point(3, 85);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(178, 13);
            this.lblShortName.TabIndex = 40;
            this.lblShortName.Text = "Short Name";
            this.lblShortName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBoxBranchCode
            // 
            this.txtBoxBranchCode.Location = new System.Drawing.Point(361, 370);
            this.txtBoxBranchCode.Name = "txtBoxBranchCode";
            this.txtBoxBranchCode.Size = new System.Drawing.Size(175, 20);
            this.txtBoxBranchCode.TabIndex = 41;
            // 
            // FAbonentOwner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 519);
            this.Controls.Add(this.txtBoxBranchCode);
            this.Controls.Add(this.txtBoxShortName);
            this.Controls.Add(this.lblShortName);
            this.Controls.Add(this.txtBoxDatePassp);
            this.Controls.Add(this.txtBoxDateBorn);
            this.Controls.Add(this.txtBoxFullName);
            this.Controls.Add(this.lblFullName);
            this.Controls.Add(this.txtBoxName2);
            this.Controls.Add(this.lblPubl);
            this.Controls.Add(this.txtBoxNumbPassp);
            this.Controls.Add(this.txtBoxSerPassp);
            this.Controls.Add(this.txtBoxFax);
            this.Controls.Add(this.lblFax);
            this.Controls.Add(this.lblDistr);
            this.Controls.Add(this.cmbBoxLoc);
            this.Controls.Add(this.cmbBoxDistr);
            this.Controls.Add(this.cmbBoxObl);
            this.Controls.Add(this.txtBoxIndex);
            this.Controls.Add(this.lblIndex);
            this.Controls.Add(this.txtBoxSurnameLat);
            this.Controls.Add(this.lblSurnameLat);
            this.Controls.Add(this.txtBoxSurname);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbNameLatin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbNumFile);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbRematrk);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbOKPO);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPassport);
            this.Controls.Add(this.lblPassp);
            this.Controls.Add(this.tbTelephone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label1);
            this.Name = "FAbonentOwner";
            this.Text = "Abonent Owner";
            this.Load += new System.EventHandler(this.FAbonentOwner_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbName;
      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.TextBox tbAddress;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox tbTelephone;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbPassport;
      private System.Windows.Forms.Label lblPassp;
      private System.Windows.Forms.TextBox tbOKPO;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox tbRematrk;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox tbNumFile;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox tbNameLatin;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox txtBoxSurnameLat;
      private System.Windows.Forms.Label lblSurnameLat;
      private System.Windows.Forms.TextBox txtBoxSurname;
      private System.Windows.Forms.Label lblSurname;
      private System.Windows.Forms.TextBox txtBoxIndex;
      private System.Windows.Forms.Label lblIndex;
      private System.Windows.Forms.ComboBox cmbBoxObl;
      private System.Windows.Forms.ComboBox cmbBoxDistr;
      private System.Windows.Forms.ComboBox cmbBoxLoc;
      private System.Windows.Forms.Label lblDistr;
      private System.Windows.Forms.TextBox txtBoxFax;
      private System.Windows.Forms.Label lblFax;
      private System.Windows.Forms.TextBox txtBoxSerPassp;
      private System.Windows.Forms.TextBox txtBoxNumbPassp;
      private System.Windows.Forms.Label lblPubl;
      private System.Windows.Forms.TextBox txtBoxName2;
      private System.Windows.Forms.TextBox txtBoxFullName;
      private System.Windows.Forms.Label lblFullName;
      private System.Windows.Forms.TextBox txtBoxDateBorn;
      private System.Windows.Forms.TextBox txtBoxDatePassp;
      private System.Windows.Forms.TextBox txtBoxShortName;
      private System.Windows.Forms.Label lblShortName;
      private System.Windows.Forms.TextBox txtBoxBranchCode;
   }
}