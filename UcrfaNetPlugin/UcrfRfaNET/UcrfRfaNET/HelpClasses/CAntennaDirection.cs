﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class CAntennaDirection
   {
      private static List<string> AntennaDirection = new List<string>();

      static CAntennaDirection()
      {
         AntennaDirection.Add("D - Спрямована");
         AntennaDirection.Add("ND - Неспрямована");         
      }

      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getAntennaDirectionList()
      {
         return new List<string>(AntennaDirection);
      }

      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение направления
      /// </summary>
      /// <param name="longDirection">полная строка направления</param>
      /// <returns>сокращенное значение направления</returns>
      public static string getShortDirection(string longDirection)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longDirection.Length; i++)
         {
            if (longDirection[i] == ' ')
               break;
            else
               sb.Append(longDirection[i]);
         }
         return sb.ToString();
      }

   }
}
