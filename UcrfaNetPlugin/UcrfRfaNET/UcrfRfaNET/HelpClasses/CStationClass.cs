﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class CStationClass
   {
      //===================================================
      private static List<string> lstStationClass = new List<string>();
      //===================================================
      /// <summary>
      /// Статический конструктор
      /// </summary>
      static CStationClass()
      {            
         lstStationClass.AddRange(new string[] {
            "FB - " + CLocaliz.TxT("Базова"),
            "FL - " + CLocaliz.TxT("Стаціонарна абонентська"),
            "FC - " + CLocaliz.TxT("Берегова"),
            "NL - " + CLocaliz.TxT("Морська радіонавігаційна сухопутна"),
            "FR - " + CLocaliz.TxT("Повторювач (ретранслятор)")
         });
      }
      //===================================================
      /// <summary>
      /// Station classs list
      /// </summary>
      /// <returns>station class list</returns>
      public static List<string> getlstStationClass()
      {
         return new List<string>(lstStationClass);
      }
      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение поляризации
      /// </summary>
      /// <param name="longStationClass">full station class name</param>
      /// <returns>short station class name</returns>
      public static string getShortStationClass(string longStationClass)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longStationClass.Length; i++)
         {
            if (longStationClass[i] == ' ')
               break;
            else
               sb.Append(longStationClass[i]);
         }
         return sb.ToString();
      }
   }
}
