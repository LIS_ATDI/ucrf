﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   /// <summary>
   /// Форма для создания / редактирования "Особливих умов"
   /// </summary>
   public partial class FSCPattern : Form
   {
      //===================================================
      List<string> lstDocType = new List<string>();
      int recID = 0;
      string departType = "VFSR";
      DepartmentSectorType departSectorType = DepartmentSectorType.VFSR_SSRT;
      string docType = DocType.VISN;
      //===================================================
      private class ComboItem
      {
          public string Code { get; set; }
          public string Desc { get; set; }

          public ComboItem(string strCode, string strDesc)
          {

              this.Code = strCode;
              this.Desc = strDesc;
          }
      }

      public FSCPattern(int id)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         //-----------------------------------------------
         // Типы документов
         lstDocType.Add(DocType.VISN);
         lstDocType.Add(DocType.DOZV);
         lstDocType.Add(DocType.NV);
         lstDocType.Add(DocType.TV);
         lstDocType.Add(DocType.MP);
         lstDocType.Add(DocType.ITMP);
         lstDocType.Add(DocType.GENERAL);
         lstDocType.Add(DocType.PARAM_REZ);
         // 
         Dictionary<string, string> DepartmentList = EnumLoader.LoadEnum("EMPLOYEE_TYPE");
         List<ComboItem> comboList = new List<ComboItem>();
         //---------------------------
         foreach (var tmpVal in DepartmentList)
             if (!(tmpVal.Key == "" || PluginSetting.PluginFolderSetting.UserTypeCodesToIgnore.Contains(tmpVal.Key)))
                 comboList.Add(new ComboItem(tmpVal.Key, string.IsNullOrEmpty(tmpVal.Value) ? tmpVal.Key : tmpVal.Value));
         cbDepartment.ValueMember = "Code";
         cbDepartment.DisplayMember = "Desc";
         cbDepartment.DataSource = comboList;
         //---------------------------
         List<string> tmpStrList = new List<string>();
         tmpStrList = new List<string>();
         foreach (DepartmentSectorType tmpSector in Enum.GetValues(typeof(DepartmentSectorType)))
            tmpStrList.Add(CLocaliz.TxT(tmpSector.ToString()));
         cbSector.DataSource = tmpStrList;
         //---------------------------
         comboList = new List<ComboItem>();
         foreach (string iDocType in lstDocType)
             comboList.Add(new ComboItem(iDocType.ToString(),  CLocaliz.TxT(iDocType.ToString())));
         cbType.ValueMember = "Code";
         cbType.DisplayMember = "Desc";
         cbType.DataSource = comboList;
         //---------------------------
         recID = id;
         //---------------------------
         IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaSCPattern, IMRecordset.Mode.ReadOnly);
         r.Select("ID,DEPARTMENT,SECTOR,TYPE,NAME,TEXT");
         r.SetWhere("ID", IMRecordset.Operation.Eq, recID);
         try
         {
            r.Open();
            if (!r.IsEOF())
            {// Вытаскиваем данные из записи
               departType = r.GetS("DEPARTMENT");
               departSectorType = (DepartmentSectorType)Enum.Parse(typeof(DepartmentSectorType), r.GetS("SECTOR"));
               docType = r.GetS("TYPE");
               tbName.Text = r.GetS("NAME");
               tbText.Text = r.GetS("TEXT");

               cbDepartment.Enabled = false;
               cbSector.Enabled = false;
               cbType.Enabled = false;
            }
            cbDepartment.SelectedValue = departType;
            //cbDepartment.SelectedIndex = (cbDepartment.DataSource as List<ComboItem>).FindIndex(delegate(ComboItem ci) { return ci.Code == departType; });
            cbSector.Text = CLocaliz.TxT(departSectorType.ToString());
            cbType.SelectedValue = docType;
         }
         finally
         {
            r.Close();
            r.Destroy();
         }

         buttonOk.Enabled = ((IM.TableRight(PlugTbl.itblXnrfaSCPattern) & IMTableRight.Update) == IMTableRight.Update);
      }
      //===================================================
      /// <summary>
      /// Изменили департамерт
      /// </summary>
      private void cbDepartment_SelectedValueChanged(object sender, EventArgs e)
      {
         departType = cbDepartment.SelectedValue != null ? cbDepartment.SelectedValue.ToString() : "";
         System.Diagnostics.Debug.WriteLine("Department [" + departType.ToString() + "]");
      }
      //===================================================
      /// <summary>
      /// Сохраняем помеху
      /// </summary>
      private void buttonOk_Click(object sender, EventArgs e)
      {
         IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaSCPattern, IMRecordset.Mode.ReadWrite);
         r.Select("ID,DEPARTMENT,SECTOR,TYPE,NAME,TEXT");
         r.SetWhere("ID", IMRecordset.Operation.Eq, recID);
         try
         {
            r.Open();
            if (r.IsEOF())
            {// Добавляем новую запись
               recID = IM.AllocID(PlugTbl.itblXnrfaSCPattern, 1, -1);
               r.AddNew();
               r["ID"] = recID;
               r["DEPARTMENT"] = departType.ToString();
               r["SECTOR"] = departSectorType.ToString();
               r["TYPE"] = docType.ToString();
            }
            else
            {// Редактируем
               r.Edit();
            }
            r["NAME"] = tbName.Text;
            r["TEXT"] = tbText.Text;
            r.Update();
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Изменили сектор
      /// </summary>
      private void cbSector_TextChanged(object sender, EventArgs e)
      {
         foreach(DepartmentSectorType iSector in Enum.GetValues(typeof(DepartmentSectorType)))
               {
                  if(CLocaliz.TxT(iSector.ToString()) == cbSector.Text)
                  {
                     departSectorType = iSector;
                     System.Diagnostics.Debug.WriteLine("Sector [" + departSectorType.ToString() + "]");
                     return;
                  }
               }
      }
      //===================================================
      /// <summary>
      /// Изменили тип документа
      /// </summary>
      private void cbType_TextChanged(object sender, EventArgs e)
      {
         //if ((cbType.SelectedIndex >= 0) && (cbType.SelectedIndex < lstDocType.Count))
         {
            docType = cbType.SelectedValue != null ? cbType.SelectedValue.ToString() : "";
            System.Diagnostics.Debug.WriteLine("Type [" + docType.ToString() + "]");
         }
      }

   }
}
