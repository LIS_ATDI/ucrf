﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   internal partial class FPrice : FBaseForm
   {
      //===================================================
      CPrice dPrice;
      //===================================================
      public FPrice(int _id)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         dPrice = new CPrice();
         dPrice.Read(_id);
         this.Text = CLocaliz.TxT("Price ID:")+ dPrice.ziID;

         this.lblValidFrom.Text = CLocaliz.TxT("Valid date from");
         this.lblValidTo.Text = CLocaliz.TxT("Valid date to");


         //---
         this.cbStandart.DataSource = GetStandard();
         this.cbDepartment.DataSource = dPrice.departm.userList;
         this.cbWorkType.DataSource = dPrice.works.userList;
         this.cbDefault.DataSource = dPrice.defaultTypes.userList;
         this.cbLimitType.DataSource = dPrice.limitTypes.userList;

         this.tbArticle.DataBindings.Add("Text", dPrice, "zsARTICLE");
         this.tbDescription.DataBindings.Add("Text", dPrice, "zsDESCRIPTION");
         this.tbMeasure.DataBindings.Add("Text", dPrice, "zsMEASURE");
         this.tbNote.DataBindings.Add("Text", dPrice, "zsNOTE");

         Binding bind = new Binding("Text", dPrice, "zsPRICE");
         bind.FormatString = "F2";
         bind.FormattingEnabled = true;
         bind.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
         this.tbPrice.DataBindings.Add(bind);
         Binding bind2 = new Binding("Text", dPrice, "zsMAX");
         bind2.FormatString = "F3";
         bind2.FormattingEnabled = true;
         bind2.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
         this.tbMax.DataBindings.Add(bind2);
         Binding bind3 = new Binding("Text", dPrice, "zsMIN");
         bind3.FormatString = "F3";
         bind3.FormattingEnabled = true;
         bind3.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
         this.tbMin.DataBindings.Add(bind3);
         //-----
         this.cbStandart.DataBindings.Add("Text", dPrice, "zsSTANDARD", true, DataSourceUpdateMode.OnPropertyChanged);
         this.cbDepartment.DataBindings.Add("SelectedIndex", dPrice, "ind_DEPARTMENT", true, DataSourceUpdateMode.OnPropertyChanged);
         this.cbWorkType.DataBindings.Add("SelectedIndex", dPrice, "ind_WORK", true, DataSourceUpdateMode.OnPropertyChanged);
         this.cbDefault.DataBindings.Add("SelectedIndex", dPrice, "ind_DEFAULT", true, DataSourceUpdateMode.OnPropertyChanged);
         this.cbLimitType.DataBindings.Add("SelectedIndex", dPrice, "ind_LIMIT", true, DataSourceUpdateMode.OnPropertyChanged);
         this.checkBoxActive.DataBindings.Add("Checked", dPrice, "ziSTATUS");
         this.checkBoxDefault.DataBindings.Add("Checked", dPrice, "ziDEFAULT");
         this.dateTimePicker1.DataBindings.Add("Text", dPrice, "ztDATEStart", true, DataSourceUpdateMode.OnPropertyChanged);
         this.dateTimePicker2.DataBindings.Add("Text", dPrice, "ztDATEOut", true, DataSourceUpdateMode.OnPropertyChanged);
         if (this.checkBoxDefault.Checked)
            cbDefault.Enabled = true;
         else
            cbDefault.Enabled = false;

         buttonOK.Enabled = ((IM.TableRight(PlugTbl.itblXnrfaPrice) & IMTableRight.Update) == IMTableRight.Update);

         gbLimits.Text = CLocaliz.TxT(gbLimits.Text);
         label8.Text = CLocaliz.TxT(label8.Text);
         label9.Text = CLocaliz.TxT(label9.Text);
         label10.Text = CLocaliz.TxT(label10.Text);

      }
      //===================================================
      /// <summary>
      /// Save and Exit
      /// </summary>
      private void buttonOK_Click(object sender, EventArgs e)
      {
         
         foreach (Binding b in this.dateTimePicker1.DataBindings) b.WriteValue();
         foreach (Binding b in this.dateTimePicker2.DataBindings) b.WriteValue();

         foreach (Binding b in this.cbDepartment.DataBindings) b.WriteValue();
         foreach (Binding b in this.cbStandart.DataBindings) b.WriteValue();
         foreach (Binding b in this.cbWorkType.DataBindings) b.WriteValue();
         foreach (Binding b in this.cbDefault.DataBindings) b.WriteValue();
         foreach (Binding b in this.cbLimitType.DataBindings) b.WriteValue();
         foreach (Binding b in this.checkBoxActive.DataBindings) b.WriteValue();
         if (CheckData() == false)
         {
            DialogResult = DialogResult.None;
            return;
         }
         dPrice.Write();
      }
      //===================================================
      /// <summary>
      /// Возвращает все стандарты
      /// </summary>
      /// <returns></returns>
      private List<string> GetStandard()
      {
         List<string> retVal = CRadioTech.AllStandard();
         retVal.Insert(0, "");/* new List<string>();
         retVal.Add("");  //Нет стандарта
         IMRecordset rs = new IMRecordset(ICSMTbl.itblRadioSystem, IMRecordset.Mode.ReadOnly);
         rs.Select("NAME");
         try
         {
            for(rs.Open(); !rs.IsEOF(); rs.MoveNext())
               retVal.Add(rs.GetS("NAME"));
         }
         finally
         {
            rs.Destroy();
         }*/
         return retVal;
      }
      //======================================================
      private bool CheckData()
      {
         bool retVal = true;
         string errorMessage = "";

         

         if (cbDepartment.SelectedIndex < 0)
            errorMessage = CLocaliz.TxT("Department is not selected.");
         else if(cbStandart.SelectedIndex < 0)
            errorMessage = CLocaliz.TxT("Standard is not selected.");
         else if (cbWorkType.SelectedIndex < 0)
            errorMessage = CLocaliz.TxT("Work type is not selected.");
         else if (cbDefault.Enabled && cbDefault.SelectedIndex < 0)
            errorMessage = CLocaliz.TxT("Default type is not selected.");
         else if (string.IsNullOrEmpty(tbArticle.Text))
            errorMessage = CLocaliz.TxT("Article is empty.");
         else if (string.IsNullOrEmpty(tbPrice.Text))
            errorMessage = CLocaliz.TxT("Price is empty.");
         else if (dPrice.zsPRICE < 0.01)
            errorMessage = CLocaliz.TxT("Price can't be 0.00.");
         else if (string.IsNullOrEmpty(dateTimePicker1.Text))
             errorMessage = CLocaliz.TxT("Valid date from.");
         else if (string.IsNullOrEmpty(dateTimePicker2.Text))
             errorMessage = CLocaliz.TxT("Valid date from.");
         if (!string.IsNullOrEmpty(errorMessage))
         {
            MessageBox.Show(errorMessage, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            retVal = false;
         }
         return retVal;
      }

      private void checkBoxDefault_CheckedChanged(object sender, EventArgs e)
      {
         if (this.checkBoxDefault.Checked)
            cbDefault.Enabled = true;
         else
            cbDefault.Enabled = false;
      }

      private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
      {
          if (this.dateTimePicker1.Value > this.dateTimePicker2.Value)
          {
              MessageBox.Show(CLocaliz.TxT("Start date can not be greater than the end date!"));
              this.dateTimePicker2.Value = this.dateTimePicker1.Value;
          }
      }

      private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
      {
          if (this.dateTimePicker1.Value > this.dateTimePicker2.Value)
          {
              MessageBox.Show(CLocaliz.TxT("Start date can not be greater than the end date!"));
              this.dateTimePicker2.Value = this.dateTimePicker1.Value;
          }
      }

   }
}
