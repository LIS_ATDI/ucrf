﻿namespace XICSM.UcrfRfaNET
{
   partial class FSelectSCPattern
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.ToolTip1 = new System.Windows.Forms.ToolTip();
          this.dgPattern = new System.Windows.Forms.DataGridView();
          this.PatternID = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.PatternName = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.PatternText = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.label1 = new System.Windows.Forms.Label();
          this.tbFind = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.buttonFind = new System.Windows.Forms.Button();
          this.buttonAccept = new System.Windows.Forms.Button();
          this.buttonCancel = new System.Windows.Forms.Button();
          ((System.ComponentModel.ISupportInitialize)(this.dgPattern)).BeginInit();
          this.SuspendLayout();
          // 
          // dgPattern
          // 
          this.dgPattern.AllowUserToAddRows = false;
          this.dgPattern.AllowUserToDeleteRows = false;
          this.dgPattern.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgPattern.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatternID,
            this.PatternName,
            this.PatternText});
          this.dgPattern.Location = new System.Drawing.Point(12, 81);
          this.dgPattern.Name = "dgPattern";
          this.dgPattern.ReadOnly = true;
          this.dgPattern.RowHeadersWidth = 4;
          this.dgPattern.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dgPattern.Size = new System.Drawing.Size(725, 201);
          this.dgPattern.TabIndex = 0;
          this.dgPattern.ShowCellToolTips = false;
          this.dgPattern.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgPattern_MouseDoubleClick);
          this.dgPattern.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPattern_CellMouseEnter);
         

          // 
          // PatternID
          // 
          this.PatternID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
          this.PatternID.HeaderText = "ID";
          this.PatternID.Name = "PatternID";
          this.PatternID.ReadOnly = true;
          this.PatternID.Visible = false;
          // 
          // PatternName
          // 
          this.PatternName.FillWeight = 200F;
          this.PatternName.HeaderText = "Name";
          this.PatternName.Name = "PatternName";
          this.PatternName.ReadOnly = true;
          this.PatternName.Width = 200;
          // 
          // PatternText
          // 
          this.PatternText.FillWeight = 1000F;
          this.PatternText.HeaderText = "Text";
          this.PatternText.Name = "PatternText";
          this.PatternText.ReadOnly = true;
          this.PatternText.Width = 1000;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(13, 11);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(89, 13);
          this.label1.TabIndex = 1;
          this.label1.Text = "Enter filter phrase";
          // 
          // tbFind
          // 
          this.tbFind.Location = new System.Drawing.Point(16, 29);
          this.tbFind.Name = "tbFind";
          this.tbFind.Size = new System.Drawing.Size(640, 20);
          this.tbFind.TabIndex = 2;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(13, 61);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(37, 13);
          this.label2.TabIndex = 3;
          this.label2.Text = "Result";
          // 
          // buttonFind
          // 
          this.buttonFind.Location = new System.Drawing.Point(662, 26);
          this.buttonFind.Name = "buttonFind";
          this.buttonFind.Size = new System.Drawing.Size(75, 23);
          this.buttonFind.TabIndex = 4;
          this.buttonFind.Text = "Find";
          this.buttonFind.UseVisualStyleBackColor = true;
          this.buttonFind.Click += new System.EventHandler(this.buttonFind_Click);
          // 
          // buttonAccept
          // 
          this.buttonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.buttonAccept.Location = new System.Drawing.Point(12, 288);
          this.buttonAccept.Name = "buttonAccept";
          this.buttonAccept.Size = new System.Drawing.Size(75, 23);
          this.buttonAccept.TabIndex = 5;
          this.buttonAccept.Text = "Accept";
          this.buttonAccept.UseVisualStyleBackColor = true;
          this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
          // 
          // buttonCancel
          // 
          this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.buttonCancel.Location = new System.Drawing.Point(662, 288);
          this.buttonCancel.Name = "buttonCancel";
          this.buttonCancel.Size = new System.Drawing.Size(75, 23);
          this.buttonCancel.TabIndex = 6;
          this.buttonCancel.Text = "Cancel";
          this.buttonCancel.UseVisualStyleBackColor = true;
          // 
          // FSelectSCPattern
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(749, 319);
          this.Controls.Add(this.buttonCancel);
          this.Controls.Add(this.buttonAccept);
          this.Controls.Add(this.buttonFind);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.tbFind);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.dgPattern);
          this.Name = "FSelectSCPattern";
          this.Text = "FSelectSCPattern";
          ((System.ComponentModel.ISupportInitialize)(this.dgPattern)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.DataGridView dgPattern;
      private System.Windows.Forms.DataGridViewTextBoxColumn PatternID;
      private System.Windows.Forms.DataGridViewTextBoxColumn PatternName;
      private System.Windows.Forms.DataGridViewTextBoxColumn PatternText;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbFind;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button buttonFind;
      private System.Windows.Forms.Button buttonAccept;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.ToolTip ToolTip1;
   }
}