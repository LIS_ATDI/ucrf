﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   enum PolarizarionType
   {
      Small,
      Standard
   };

   class CPolarization
   {
      //===================================================
      private static List<string> lstPolarizationStd = new List<string>();
      private static List<string> lstPolarizationSmall = new List<string>();
      //===================================================
      /// <summary>
      /// Статический конструктор
      /// </summary>
      static CPolarization()
      {
         lstPolarizationStd.AddRange(new string[] {
            "V - " + CLocaliz.TxT("Вертикальна"),
            "H - " + CLocaliz.TxT("Горизонтальна"),
            "CL - " + CLocaliz.TxT("Лівокругова"),
            "CR - " + CLocaliz.TxT("Правокругова"),
            "D - " + CLocaliz.TxT("Дуальна (Вертикальна або Горизонтальна)"),
            "L - " + CLocaliz.TxT("Лінійна"),
            "M - " + CLocaliz.TxT("Змішана"),
            "SL - " + CLocaliz.TxT("Лівостороння"),
            "SR - " + CLocaliz.TxT("Правостороння")});

         lstPolarizationSmall.AddRange(new string[] {
            "V - " + CLocaliz.TxT("Вертикальна"),
            "H - " + CLocaliz.TxT("Горизонтальна"),            
            "M - " + CLocaliz.TxT("Змішана")});
      }
      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getPolarizationList(PolarizarionType polarizartionType)
      {
         switch(polarizartionType)
         {
            case PolarizarionType.Small:
               return new List<string>(lstPolarizationSmall);
            case PolarizarionType.Standard:
               return new List<string>(lstPolarizationStd);
            default:
               return null;
         }               
      }
      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение поляризации
      /// </summary>
      /// <param name="longPolarization">полная строка поляризации</param>
      /// <returns>сокращенное значение поляризации</returns>
      public static string getShortPolarization(string longPolarization)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longPolarization.Length; i++)
         {
            if (longPolarization[i] == ' ')
               break;
            else
               sb.Append(longPolarization[i]);
         }
         return sb.ToString();
      }

      public static string GetLongPolarization(string shortPolarization)
      {
         foreach(string pl in lstPolarizationStd)
         {
            if (pl.StartsWith(shortPolarization+" "))
               return pl;            
         }
         return shortPolarization+ " - (неправильне значення)";
      }
   }
}
