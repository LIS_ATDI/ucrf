﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class CFmTransmissionSystem
   {
      private static List<string> TransmissionSystem = new List<string>();

      static CFmTransmissionSystem()
      {
         TransmissionSystem.Add("Моно, 75 кГц");
         TransmissionSystem.Add("Моно, 50 кГц");
         TransmissionSystem.Add("Стерео, 50 кГц");
         TransmissionSystem.Add("Стерео, пилот-тон, 75 кГц");
         TransmissionSystem.Add("Стерео, пилот-тон, 50 кГц");
      }

      //===================================================
      /// <summary>
      /// Возвращает список передач
      /// </summary>
      /// <returns>список передач</returns>
      public static List<string> getTransmissionSystemList()
      {
         return new List<string>(TransmissionSystem);
      }

   }
}
