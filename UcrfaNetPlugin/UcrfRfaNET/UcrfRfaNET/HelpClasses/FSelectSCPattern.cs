﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.Documents;

namespace XICSM.UcrfRfaNET
{
   public partial class FSelectSCPattern : FBaseForm
   {
      DepartmentType department;
      DepartmentSectorType depSector;
      string docType;
      int patternID = 0;
      string patternText = "";
      ToolTip toolTip1 = new ToolTip();
   
      //===================================================
      public FSelectSCPattern(DepartmentType _department, DepartmentSectorType _depSector, string _docType)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         department = _department;
         depSector = _depSector;
         docType = _docType;
         UpdateGrid();
         if ((department == DepartmentType.Unknown) ||
            (docType == DocType.DocUnknown))
            MessageBox.Show("Не все параметры установлены!", "Для программиста");
      }
      //===================================================
      /// <summary>
      /// Обновить грид
      /// </summary>
      private void UpdateGrid()
      {
         dgPattern.Rows.Clear();
         string findText = tbFind.Text;
         IMRecordset r = new IMRecordset(PlugTbl.itblXnrfaSCPattern, IMRecordset.Mode.ReadOnly);
         r.Select("ID,NAME,TEXT");
         r.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department.ToString());
         r.SetWhere("SECTOR", IMRecordset.Operation.Like, depSector.ToString());
         r.SetWhere("TYPE", IMRecordset.Operation.Like, docType.ToString());
         try
         {
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               string name = r.GetS("NAME");
               string text = r.GetS("TEXT");
               // !! сделал так, потому что запросом не получилось
               if ((findText == "") || name.Contains(findText) || text.Contains(findText))
               {
                  string[] row = new string[] { r.GetI("ID").ToString(), 
                                             r.GetS("NAME"),
                                             r.GetS("TEXT")
                                           };
                  dgPattern.Rows.Add(row);
               }
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
         buttonAccept.Enabled = (dgPattern.Rows.Count > 0) ? true : false;
      }
      //===================================================
      /// <summary>
      /// Необходимо обновить поиск
      /// </summary>
      private void buttonFind_Click(object sender, EventArgs e)
      {
         UpdateGrid();
      }
      //===================================================
      /// <summary>
      /// Возвращает ID шаблока
      /// </summary>
      /// <returns>Возвращает ID шаблока</returns>
      public int GetIDPattern()
      {
         return patternID;
      }
      //===================================================
      /// <summary>
      /// Возвращает текст шаблона
      /// </summary>
      /// <returns>Возвращает текст шаблона</returns>
      public string GetPatternText()
      {
         return patternText;
      }
      //===================================================
      /// <summary>
      /// Обновляет ID патерна
      /// </summary>
      private void UpdatePatternID()
      {
         try
         {
            patternID = Convert.ToInt32(dgPattern.Rows[dgPattern.CurrentCell.RowIndex].Cells[0].Value.ToString());
            patternText = dgPattern.Rows[dgPattern.CurrentCell.RowIndex].Cells[2].Value.ToString();
         }
         catch
         {
            patternID = 0;
            patternText = "";
         }
      }
      //===================================================
      /// <summary>
      /// Выбрали станцию
      /// </summary>
      private void buttonAccept_Click(object sender, EventArgs e)
      {
         UpdatePatternID();
      }
      //===================================================
      /// <summary>
      /// Выбрали станцию
      /// </summary>
      private void dgPattern_MouseDoubleClick(object sender, MouseEventArgs e)
      {
         UpdatePatternID();
         if (buttonAccept.Enabled)
            this.DialogResult = DialogResult.OK;
      }



      private void dgPattern_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
      {
          if ((e.ColumnIndex >= 0) && (e.RowIndex >= 0))
          {
              CreateToolTipForControl(dgPattern, dgPattern.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
          }
      }


      private void CreateToolTipForControl(Control ctl, String msg)
      {
          String[] lines = msg.Split(new char[] { '\u000D' });
          StringBuilder sb = new StringBuilder();

          int lineCount = lines.Length;
          for (int i = 0; i < lineCount; i++)
          {
              char tab = '\u0009';
              char lf = '\u000A';
              String line = lines[i].Replace(tab.ToString(), "    ");
              line = line.Replace(lf.ToString(), "");
              sb.Append(line + "\n");
          }

          toolTip1.AutoPopDelay = 10000;
          toolTip1.InitialDelay = 1000;
          toolTip1.ReshowDelay = 500;
          toolTip1.ShowAlways = true;
          toolTip1.SetToolTip(ctl, sb.ToString());
      } 
         
      
   }
}
