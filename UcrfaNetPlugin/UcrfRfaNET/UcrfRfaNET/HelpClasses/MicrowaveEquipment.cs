﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class MicrowaveEquipment : Equipment, ICloneable
   {
      protected Power _minPower = new Power();
    
      public double Bandwidth {get; set; }       
      public string Modulation {get; set; }
                  
      public MicrowaveEquipment()
      {
          TableName = ICSMTbl.itblEquipMicrow;          
      }
         
      public override void Load()
      {
         IMRecordset r = new IMRecordset(_tableName, IMRecordset.Mode.ReadOnly);

         if (ID == IM.NullI)
         {
            throw new Exception("Microwave equipment ID is nit initialized");
         }

         try
         {
            r.Select("ID,MAX_POWER,MIN_POWER, NAME,BW,DESIG_EMISSION,CUST_TXT1,CUST_DAT1,MODULATION");
            r.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            r.Open();

            if (!r.IsEOF())
            {
               _maxPower[PowerUnits.dBm] = r.GetD("MAX_POWER");
               _minPower[PowerUnits.dBm] = r.GetD("MIN_POWER");
               _equipmentName = r.GetS("NAME");
               _desigEmission = r.GetS("DESIG_EMISSION");
               Bandwidth = r.GetD("BW");
               Certificate.Date = r.GetT("CUST_DAT1");
               Certificate.Symbol = r.GetS("CUST_TXT1");
               Modulation = r.GetS("MODULATION");
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }
       
      public override void Save()
      {
      }

      public object Clone()
      {
         MicrowaveEquipment clone = new MicrowaveEquipment();

         clone._maxPower = _maxPower.Clone() as Power;
         clone._minPower = _minPower.Clone() as Power;

         clone._equipmentName = _equipmentName;
         clone._desigEmission = _desigEmission;
         clone.Bandwidth = Bandwidth;
         clone.Certificate.Date = Certificate.Date;
         clone.Certificate.Symbol = Certificate.Symbol;
         clone.Modulation = Modulation;
         clone.Id = Id;
         clone._tableName = _tableName;
         
         return clone;
      }

      public Power MinPower
      {
          get { return _minPower; }
      }
   }
}
