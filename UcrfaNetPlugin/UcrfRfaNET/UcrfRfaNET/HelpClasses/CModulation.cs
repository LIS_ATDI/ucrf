﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   internal static class CModulation
   {
      //===================================================
      private static List<string> lstModulation = new List<string>();
      //===================================================
      /// <summary>
      /// Статический конструктор
      /// </summary>
      static CModulation()
      {
         lstModulation.AddRange(new string[]{
            CLocaliz.TxT("none"),
            CLocaliz.TxT("AM compr normal"),
            CLocaliz.TxT("FM"),
            CLocaliz.TxT("QPSK"),
            CLocaliz.TxT("4-QAM"),
            CLocaliz.TxT("8-QAM"),
            CLocaliz.TxT("16-QAM"),
            CLocaliz.TxT("32-QAM"),
            CLocaliz.TxT("64-QAM"),
            CLocaliz.TxT("128-QAM"),
            CLocaliz.TxT("256-QAM"),
            CLocaliz.TxT("512-QAM"),
            CLocaliz.TxT("DQPSK"),
            CLocaliz.TxT("QPSK 1/2"),
            CLocaliz.TxT("QPSK 2/3"),
            CLocaliz.TxT("QPSK 3/4"),
            CLocaliz.TxT("QPSK 5/6"),
            CLocaliz.TxT("QPSK 7/8"),
            CLocaliz.TxT("16-QAM 1/2 M1"),
            CLocaliz.TxT("16-QAM 2/3"),
            CLocaliz.TxT("16-QAM 3/4"),
            CLocaliz.TxT("16-QAM 5/6"),
            CLocaliz.TxT("16-QAM 7/8"),
            CLocaliz.TxT("64-QAM 1/2M2"),
            CLocaliz.TxT("64-QAM 2/3M3"),
            CLocaliz.TxT("64-QAM 3/4"),
            CLocaliz.TxT("64-QAM 5/6"),
            CLocaliz.TxT("64-QAM 7/8"),
            CLocaliz.TxT("PDM-8"),
            CLocaliz.TxT("PDM-4 1/2"),
            CLocaliz.TxT("Analog"),
            CLocaliz.TxT("TPRS"),
            CLocaliz.TxT("AM compr high"),
            CLocaliz.TxT("BFSK 1/2"),
            CLocaliz.TxT("BFSK 3/4"),
            CLocaliz.TxT("PDM-4 2/3"),
            CLocaliz.TxT("PDM-4 3/4"),
            CLocaliz.TxT("PDM-4 5/6"),
            CLocaliz.TxT("PDM-4 7/8"),
            CLocaliz.TxT("BPSK"),
            CLocaliz.TxT("GMSK"),
            CLocaliz.TxT("8PSK"),
            CLocaliz.TxT("OFDM"),
            CLocaliz.TxT("FFSK"),
            CLocaliz.TxT("FFSK2"),
            CLocaliz.TxT("FFSK4"),
            CLocaliz.TxT("FFSK4L"),
            CLocaliz.TxT("1024-QAM"),
            CLocaliz.TxT("2048-QAM"),
            CLocaliz.TxT("4096-QAM")

         });
      }
      //===================================================
      /// <summary>
      /// Возвращает числовой индекс по значению строки
      /// </summary>
      /// <param name="modString">строковое значение модуляции</param>
      /// <returns>-1 если индекс не найдено, иначе индекс начиная с "0"</returns>
      public static int getModulationIndex(string modString)
      {
         return lstModulation.IndexOf(modString);
      }
      //===================================================
      /// <summary>
      /// Возвращает строковое представление модуляции по индексу
      /// </summary>
      /// <param name="index">индекс модуляции</param>
      /// <returns>если индекс корректен, то возвращает строковое представление модуляции, иначе пустую строку</returns>
      public static string getModulationString(int index)
      {
         if ((0 <= index) && (index < lstModulation.Count))
            return lstModulation[index];
         return "";
      }
      //===================================================
      /// <summary>
      /// Возвращает строковое представление модуляции по строковому индексу
      /// </summary>
      /// <param name="indexStr">строковый индекс модуляции</param>
      /// <returns>если индекс корректен, то возвращает строковое представление модуляции, иначе пустую строку</returns>
      public static string getModulationString(string indexStr)
      {
         if(indexStr == null)
            return "";
         int index = ConvertType.ToInt32(indexStr, IM.NullI);
         if ((0 <= index) && (index < lstModulation.Count))
            return lstModulation[index];
         return "";
      }
   }
}
