﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   internal class CPrice
   {
      //===================================================
      private int _ziID = IM.NullI;
      private string _zsDEPARTMENT = "";
      private string _zsARTICLE = "";
      private string _zsDESCR_WORK = "";
      private string _zsSTANDARD = "";
      private string _zsMEASURE = "";
      private double _zsPRICE = IM.NullD;
      private string _zsDESCRIPTION = "";
      private int _ziSTATUS = 1;
      private int _ziDEFAULT = 0;
      private string _ziDEFAULTTYPE = "";
      private string _ziLIMITTYPE = "";
      private double _zsMIN = IM.NullD;
      private double _zsMAX = IM.NullD;
      private string _zsNOTE = "";
      //----
      private string _zsCREATED_BY = "";
      private DateTime _zdtCREATED_DATE = IM.NullT;
      private string _zsMODIFIED_BY = "";
      private DateTime _zdtMODIFIED_DATE = IM.NullT;
      // Properties
      public int ziID {get{return _ziID;} set{ _ziID=value;}}
      public DateTime _ztDateOutAction;
      public DateTime _ztDateStartAction;
      

     
      
     /// <summary>
     /// Дата припинення дії
     /// </summary>
      public DateTime ztDATEOut { get { return _ztDateOutAction; } set { _ztDateOutAction = value; } }
     /// <summary>
     ///  Дата початку дії
     /// </summary>
      public DateTime ztDATEStart { get { return _ztDateStartAction; } set { _ztDateStartAction = value; } }

      public int ind_DEPARTMENT
      {
         get
         {
            return departm.getIndex(_zsDEPARTMENT);
         } 
         set
         {
            _zsDEPARTMENT = departm.getType(value).ToString();
         }
      }

      public string zsARTICLE {get{return _zsARTICLE;} set{ _zsARTICLE=value;}}
      public int ind_WORK 
      {
         get
         {
            return works.getIndex(_zsDESCR_WORK);
         } 
         set
         {
            _zsDESCR_WORK = works.getType(value).ToString();
         }
      }
      public int ind_DEFAULT
      {
         get
         {
            return defaultTypes.getIndex(_ziDEFAULTTYPE);
         }
         set
         {
            _ziDEFAULTTYPE = defaultTypes.getType(value).ToString();
         }
      }
      public int ind_LIMIT
      {
         get
         {
            return limitTypes.getIndex(_ziLIMITTYPE);
         }
         set
         {
            _ziLIMITTYPE = limitTypes.getType(value).ToString();
         }
      }
      public string zsSTANDARD {get{return _zsSTANDARD;} set{ _zsSTANDARD=value;}}
      public string zsMEASURE {get{return _zsMEASURE;} set{ _zsMEASURE=value;}}
      public double zsPRICE {get{return _zsPRICE;} set{ _zsPRICE=value;}}
      public double zsMIN { get { return _zsMIN; } set { _zsMIN = value; } }
      public double zsMAX { get { return _zsMAX; } set { _zsMAX = value; } }
      public string zsDESCRIPTION {get{return _zsDESCRIPTION;} set{ _zsDESCRIPTION=value;}}
      public int ziSTATUS {get{return _ziSTATUS;} set{ _ziSTATUS=value;}}
      public int ziDEFAULT { get { return _ziDEFAULT; } set { _ziDEFAULT = value; } }
      public string ziDEFAULTTYPE { get { return _ziDEFAULTTYPE; } set { _ziDEFAULTTYPE = value; } }
      public string zsNOTE {get{ return _zsNOTE;} set{ _zsNOTE = value;}}
      //----
      public string zsCREATED_BY {get{return _zsCREATED_BY;} set{ _zsCREATED_BY=value;}}
      public DateTime zdtCREATED_DATE {get{return _zdtCREATED_DATE;} set{ _zdtCREATED_DATE=value;}}
      public string zsMODIFIED_BY {get{return _zsMODIFIED_BY;} set{ _zsMODIFIED_BY=value;}}
      public DateTime zdtMODIFIED_DATE {get{return _zdtMODIFIED_DATE;} set{ _zdtMODIFIED_DATE=value;}}

      //---------------------------------------------------
      public CComboBoxContainer<WorkType> works = new CComboBoxContainer<WorkType>();
      public CComboBoxContainer<ManagemenUDCR> departm = new CComboBoxContainer<ManagemenUDCR>();
      public CComboBoxContainer<PriceDefaultType> defaultTypes = new CComboBoxContainer<PriceDefaultType>();
      public CComboBoxContainer<LimitType> limitTypes = new CComboBoxContainer<LimitType>();
      
      //===================================================
      /// <summary>
      /// Loads data from DB
      /// </summary>
      /// <param name="id">record ID</param>
      public void Read(int id)
      {
         IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,DEPARTMENT,ARTICLE,DESCR_WORK,STANDARD,MEASURE,PRICE,DESCRIPTION,STATUS,NOTE,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,DEFAULT,DEFAULTTYPE,MIN,MAX,LIMITTYPE,DATEOUTACTION,DATESTARTACTION");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
            {
               _ziID = rs.GetI("ID");
               _zsDEPARTMENT = rs.GetS("DEPARTMENT");
               _zsARTICLE = rs.GetS("ARTICLE");
               _zsDESCR_WORK = rs.GetS("DESCR_WORK");
               _zsSTANDARD = rs.GetS("STANDARD");
               _zsMEASURE = rs.GetS("MEASURE");
               _zsPRICE = rs.GetD("PRICE");
               _zsMAX = rs.GetD("MAX");
               _zsMIN = rs.GetD("MIN");
               _zsDESCRIPTION = rs.GetS("DESCRIPTION");
               _ziSTATUS = rs.GetI("STATUS");
               _ziDEFAULT = rs.GetI("DEFAULT");
               _ziDEFAULTTYPE = rs.GetS("DEFAULTTYPE");
               _ziLIMITTYPE = rs.GetS("LIMITTYPE");
               if (_ziDEFAULT == IM.NullI)
                  _ziDEFAULT = 0;
               _zsNOTE = rs.GetS("NOTE");
               //----
               _zsCREATED_BY = rs.GetS("CREATED_BY");
               _zdtCREATED_DATE = rs.GetT("CREATED_DATE");
               _zsMODIFIED_BY = rs.GetS("MODIFIED_BY");
               _zdtMODIFIED_DATE = rs.GetT("MODIFIED_DATE");

               _ztDateOutAction = rs.GetT("DATEOUTACTION");
               _ztDateStartAction = rs.GetT("DATESTARTACTION");
            }
            else
            {
               _ziID = IM.AllocID(PlugTbl.itblXnrfaPrice, 1, -1);
            }
         }
         finally
         {
            rs.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Saves
      /// </summary>
      public void Write()
      {
         if ((_ziID == IM.NullI) || (_ziID == 0))
            throw new Exception("ID is incorrect");
         
         IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,DEPARTMENT,ARTICLE,DESCR_WORK,STANDARD,MEASURE,PRICE,DESCRIPTION,STATUS,NOTE,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,DEFAULT,DEFAULTTYPE,MIN,MAX,LIMITTYPE,DATEOUTACTION,DATESTARTACTION");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, _ziID);
         try
         {
            rs.Open();
            if (rs.IsEOF())
            {
               rs.AddNew();
               rs.Put("CREATED_BY", IM.ConnectedUser());
               rs.Put("CREATED_DATE", DateTime.Now);
            }
            else
            {
               rs.Edit();
               rs.Put("MODIFIED_BY", IM.ConnectedUser());
               rs.Put("MODIFIED_DATE", DateTime.Now);
            }
            //------
            rs.Put("ID", _ziID);
            rs.Put("DEPARTMENT", _zsDEPARTMENT);
            rs.Put("ARTICLE", _zsARTICLE);
            rs.Put("DESCR_WORK", _zsDESCR_WORK);
            rs.Put("STANDARD", _zsSTANDARD);
            rs.Put("MEASURE", _zsMEASURE);
            rs.Put("PRICE", _zsPRICE);
            rs.Put("MIN", _zsMIN);
            rs.Put("MAX", _zsMAX);
            rs.Put("LIMITTYPE", _ziLIMITTYPE);
            rs.Put("DESCRIPTION", _zsDESCRIPTION);
            rs.Put("STATUS", _ziSTATUS);
            rs.Put("DEFAULT", _ziDEFAULT);
            rs.Put("DEFAULTTYPE", _ziDEFAULTTYPE);
            rs.Put("NOTE", _zsNOTE);

            rs.Put("DATEOUTACTION", _ztDateOutAction);
            rs.Put("DATESTARTACTION", _ztDateStartAction);
            rs.Update();
         }
         finally
         {
            rs.Destroy();
         }
      }
   }
}
