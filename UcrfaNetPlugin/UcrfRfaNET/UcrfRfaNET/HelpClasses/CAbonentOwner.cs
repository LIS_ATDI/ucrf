﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   internal class CAbonentOwner
   {
      private bool _createNew = true;
      //===================================================
      private int _ziID = IM.NullI;
      private string _zsNAME = "";
      private string _zsSHORTNAME = "";
      private string _zsNAME1 = "";
      private string _zsNAME2 = "";
      private string _zsNameLatin = "";
      private string _zsNameLatin1 = "";
      private string _zsADDRESS = "";
      private string _zsTEL = "";
      private string _zsREMARK = "";
      private string _zsOKPO = "";
      private string _zsPASSPORT = "";
      private string _zsNumFile = "";
      private DateTime _zsDateBorn = IM.NullT;
      //----
      private string _zsCREATED_BY = "";
      private DateTime _zdtCREATED_DATE = IM.NullT;
      private string _zsMODIFIED_BY = "";
      private DateTime _zdtMODIFIED_DATE = IM.NullT;

      private string _zsSurNAME = "";
      private string _zsSurNameLatin = "";
      private string _zsIndex = "";
      private string _zsObl = "";
      private string _zsDistr = "";
      private string _zsLocal = "";
      private string _zsFax = "";
      private string _zsSerPassp = "";
      private string _zsNumPassp = "";
      private DateTime _zsDatePassp = IM.NullT;
      private string _zsPubPassp = "";
      private string _zsBranchCode = "";

      // Properties
      public int ziID { get { return _ziID; } set { _ziID = value; } }
      public string zsNAME { get { return _zsNAME; } set { _zsNAME = value; } }
      public string zsSHORTNAME { get { return _zsSHORTNAME; } set { _zsSHORTNAME = value; } }
      public string zsNAME1 { get { return _zsNAME1; } set { _zsNAME1 = value; } }
      public string zsNAME_LATIN { get { return _zsNameLatin; } set { _zsNameLatin = value; } }
      public string zsNAME_LATIN1 { get { return _zsNameLatin1; } set { _zsNameLatin1 = value; } }
      public string zsADDRESS { get { return _zsADDRESS; } set { _zsADDRESS = value; } }
      public string zsTEL { get { return _zsTEL; } set { _zsTEL = value; } }
      public string zsREMARK { get { return _zsREMARK; } set { _zsREMARK = value; } }
      public string zsOKPO { get { return _zsOKPO; } set { _zsOKPO = value; } }
      public string zsPASSPORT { get { return _zsPASSPORT; } set { _zsPASSPORT = value; } }
      public string zsNumFile { get { return _zsNumFile; } set { _zsNumFile = value; } }
      public string zsBranchCode { get { return _zsBranchCode; } set { _zsBranchCode = value; } }
      public DateTime zsDateBorn { get { return _zsDateBorn; } set { _zsDateBorn = value; } }
      //----
      public string zsCREATED_BY { get { return _zsCREATED_BY; } set { _zsCREATED_BY = value; } }
      public DateTime zdtCREATED_DATE { get { return _zdtCREATED_DATE; } set { _zdtCREATED_DATE = value; } }
      public string zsMODIFIED_BY { get { return _zsMODIFIED_BY; } set { _zsMODIFIED_BY = value; } }
      public DateTime zdtMODIFIED_DATE { get { return _zdtMODIFIED_DATE; } set { _zdtMODIFIED_DATE = value; } }

      public string zsNAME2 { get { return _zsNAME2; } set { _zsNAME2 = value; } }
      public string zsSURNAME { get { return _zsSurNAME; } set { _zsSurNAME = value; } }
      public string zsSURNAMELATIN { get { return _zsSurNameLatin; } set { _zsSurNameLatin = value; } }
      public string zsINDEX
      {
          get { return _zsIndex; }
          set
          {
              int i;
              if (Int32.TryParse(value, out i))
                  _zsIndex = value;
              else if (string.IsNullOrEmpty(value))
                  _zsIndex = "";              
          }
      }
      public string zsOBL { get { return _zsObl; } set { _zsObl = value; } }
      public string zsDISTR { get { return _zsDistr; } set { _zsDistr = value; } }
      public string zsLOCAL { get { return _zsLocal; } set { _zsLocal = value; } }
      public string zsFAX { get { return _zsFax; } set { _zsFax = value; } }
      public string zsSERPASSP { get { return _zsSerPassp; } set { _zsSerPassp = value; } }
      public string zsNUMPASSP { get { return _zsNumPassp; } set { _zsNumPassp = value; } }
      public DateTime zsDATEPASSP { get { return _zsDatePassp; } set { _zsDatePassp = value; } }
      public string zsPUBPASSP { get { return _zsPubPassp; } set { _zsPubPassp = value; } }

      public bool CreateNew { get { return _createNew; } set { _createNew = value; } }

      //===================================================
      /// <summary>
      /// Loads data from DB
      /// </summary>
      /// <param name="id">record ID</param>
      public void Read(int id)
      {
         IMRecordset rs = new IMRecordset(PlugTbl.ABONENT_OWNER, IMRecordset.Mode.ReadOnly);
         rs.Select("ID,NAME,NAME_LATIN,ADDRESS,TEL,DATE_BORN,REMARK,OKPO,PASSPORT,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,FILE_NUM");
         rs.Select("SURNAME,SURNAME_LATIN,INDEX,OBL,DISTR,LOCAL,FAX,SER_PASSP,NUM_PASSP,DATE_PASSP,PUB_PASSP");
         rs.Select("NAME2,NAME1,NAME_LATIN1,SHORTNAME,BRANCH_OFFICE_CODE");
         

         rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
         try
         {
            rs.Open();
            if (!rs.IsEOF())
            {
               _ziID = rs.GetI("ID");
               _zsNAME = rs.GetS("NAME").Trim();

               _zsNAME1 = rs.GetS("NAME1").Trim();
               _zsNameLatin = rs.GetS("NAME_LATIN").Trim();
               _zsNameLatin1 = rs.GetS("NAME_LATIN1").Trim();
               _zsADDRESS = rs.GetS("ADDRESS");
               _zsTEL = rs.GetS("TEL");
               _zsREMARK = rs.GetS("REMARK");
               _zsOKPO = rs.GetS("OKPO");
               _zsPASSPORT = rs.GetS("PASSPORT");
               _zsNumFile = rs.GetS("FILE_NUM");
               _zsBranchCode = rs.GetS("BRANCH_OFFICE_CODE");
               //----
               _zsCREATED_BY = rs.GetS("CREATED_BY");
               _zdtCREATED_DATE = rs.GetT("CREATED_DATE");
               _zsMODIFIED_BY = rs.GetS("MODIFIED_BY");
               _zdtMODIFIED_DATE = rs.GetT("MODIFIED_DATE");
               _zsDateBorn = rs.GetT("DATE_BORN");

                _zsSHORTNAME = rs.GetS("SHORTNAME").Trim();
               _zsNAME2 = rs.GetS("NAME2").Trim();
               _zsSurNAME = rs.GetS("SURNAME").Trim();
               _zsSurNameLatin = rs.GetS("SURNAME_LATIN").Trim();
               _zsIndex = rs.GetS("INDEX");
               _zsObl = rs.GetS("OBL");
               _zsDistr = rs.GetS("DISTR");
               _zsLocal = rs.GetS("LOCAL");
               _zsFax = rs.GetS("FAX");
               _zsSerPassp = rs.GetS("SER_PASSP");
               _zsNumPassp = rs.GetS("NUM_PASSP");
               _zsDatePassp = rs.GetT("DATE_PASSP");
               _zsPubPassp = rs.GetS("PUB_PASSP");
            }
            else
            {
                if (CreateNew)
                {
                    _ziID = IM.AllocID(PlugTbl.ABONENT_OWNER, 1, -1);
                    _zsNAME = "";
                    _zsNameLatin = "";
                    _zsADDRESS = "";
                    _zsTEL = "";
                    _zsREMARK = "";
                    _zsOKPO = "";
                    _zsPASSPORT = "";
                    //----
                    _zsCREATED_BY = "";
                    _zdtCREATED_DATE = IM.NullT;
                    _zsMODIFIED_BY = "";
                    _zdtMODIFIED_DATE = IM.NullT;
                    _zsDateBorn = IM.NullT;
                    _zsSurNAME = "";
                    _zsSurNameLatin = "";
                    _zsIndex = "";
                    _zsObl = "";
                    _zsDistr = "";
                    _zsLocal = "";
                    _zsFax = "";
                    _zsSerPassp = "";
                    _zsNumPassp = "";
                    _zsDatePassp = IM.NullT;
                    _zsPubPassp = "";
                    _zsBranchCode = "";
                }
            }
         }
         finally
         {
            rs.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Saves
      /// </summary>
      public void Write()
      {
         if ((_ziID == IM.NullI) || (_ziID == 0))
            throw new Exception("ID is incorrect");

         IMRecordset rs = new IMRecordset(PlugTbl.ABONENT_OWNER, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,NAME,NAME_LATIN,ADDRESS,TEL,DATE_BORN,REMARK,OKPO,PASSPORT,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,FILE_NUM");
         rs.Select("SURNAME,SURNAME_LATIN,INDEX,OBL,DISTR,LOCAL,FAX,SER_PASSP,NUM_PASSP,DATE_PASSP,PUB_PASSP");
         rs.Select("NAME2,NAME1,NAME_LATIN1,SHORTNAME,BRANCH_OFFICE_CODE");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, _ziID);
         try
         {
            rs.Open();
            if (rs.IsEOF())
            {
               rs.AddNew();
               rs.Put("CREATED_BY", IM.ConnectedUser());
               rs.Put("CREATED_DATE", DateTime.Now);
            }
            else
            {
               rs.Edit();
               rs.Put("MODIFIED_BY", IM.ConnectedUser());
               rs.Put("MODIFIED_DATE", DateTime.Now);
            }
            //------            
            _zsNAME = _zsSurNAME + " " + _zsNAME1 + " " + _zsNAME2;
            _zsNAME = _zsNAME.Replace("  ", " ").Trim();             
            _zsNameLatin = _zsSurNameLatin + " " + _zsNameLatin1;
            _zsNameLatin = _zsNameLatin.Replace("  ", " ").Trim();   

            rs.Put("ID", _ziID);
            rs.Put("NAME", _zsNAME.Trim());
            rs.Put("NAME_LATIN", _zsNameLatin.Trim());
            rs.Put("ADDRESS", _zsADDRESS);
            rs.Put("TEL", _zsTEL);
            rs.Put("REMARK", _zsREMARK);
            rs.Put("OKPO", _zsOKPO);
            rs.Put("PASSPORT", _zsPASSPORT);
            rs.Put("FILE_NUM", _zsNumFile);
            rs.Put("DATE_BORN", _zsDateBorn);
            rs.Put("SHORTNAME", _zsSHORTNAME.Trim());
            rs.Put("NAME1", _zsNAME1.Trim());
            rs.Put("NAME2", _zsNAME2.Trim());
            rs.Put("SURNAME", _zsSurNAME.Trim());
            rs.Put("SURNAME_LATIN", _zsSurNameLatin.Trim());
            rs.Put("NAME_LATIN1", _zsNameLatin1.Trim());
            rs.Put("INDEX", _zsIndex);
            rs.Put("OBL", _zsObl);
            rs.Put("DISTR", _zsDistr);
            rs.Put("LOCAL", _zsLocal);
            rs.Put("FAX", _zsFax);
            rs.Put("SER_PASSP", _zsSerPassp);
            rs.Put("NUM_PASSP", _zsNumPassp);
            rs.Put("DATE_PASSP", _zsDatePassp);
            rs.Put("PUB_PASSP", _zsPubPassp);
            rs.Put("BRANCH_OFFICE_CODE", _zsBranchCode);


            rs.Update();
         }
         finally
         {
            rs.Destroy();
         }
      }
   }
}
