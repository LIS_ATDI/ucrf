﻿namespace XICSM.UcrfRfaNET
{
    //=========================================================
    /// <summary>
    /// Название таблиц ICSM
    /// </summary>
    public static class ICSMTbl
    {

        
        public const string lnkSites = "XNRFA_LINKS_SITES";
        public const string itblFmtvAssign = "FMTV_ASSIGN";
        public const string itblSatellites = "SATELLITES";
        public const string EQUIP_BRO = "EQUIP_BRO";
        public const string itblReminder = "REMINDER";
        public const string itblReminderTrg = "REMINDER_TRG";
        public const string itblEquipBro = EQUIP_BRO;
        public const string itblEquipMob2 = "EQUIP_MOB2";
        public const string itblEquipEsta = "EQUIP_ESTA";
        public const string itblAntenna = "ANTENNA";
        public const string ANTENNA_BRO = "ANTENNA_BRO";
        public const string itblAntennaBro = ANTENNA_BRO;
        public const string AntennaMW = "ANTENNA_MW";
        public const string itblEstaAntenna = "ESTA_ANTENNA";
        public const string itblEstaGroup = "ESTA_GROUP";
        public const string itblEstaAssgn = "ESTA_ASSGN";
        public const string itblEstaEmiss = "ESTA_EMISS";
        public const string itblAntennaMob2 = "ANTENNA_MOB2";
        public const string itblAntennaMob = "ANTENNA_MOB";
        public const string USERS = "USERS";
        public const string itblUsers = USERS;
        public const string itblEarthStation = "EARTH_STATION";
        public const string MobStation2 = "MOB_STATION2";
        public const string itblMobStation2 = MobStation2;
        public const string itblMobStation = "MOB_STATION";
        public const string itblBgMobStation = "BG_MOB_STATION";
        public const string itblEquipPmrMpt = "EQUIP_PMR_MPT";
        public const string itblEquipPmr = "EQUIP_PMR";
        public const string POSITION_ES = "POSITION_ES";
        public const string itblPositionEs = POSITION_ES;
        public const string itblPositionBro = "POSITION_BRO";
        public const string itblPositionFmn = "POSITION_FMN";
        public const string itblPositionHf = "POSITION_HF";
        public const string itblPositionMob2 = "POSITION_MOB2";
        public const string itblPositionMw = "POSITION_MW";
        public const string itblPositionWim = "POSITION_WIM";
        public const string itblAdmSite = "SITES";
        public const string Site = "SITE";
        public const string itblSite = Site;        
        public const string itblSites = "SITES";
        public const string SITES = itblSites;
        public const string itblMicrows = "MICROWS";
        public const string itblMicrowa = "MICROWA";
        public const string EQUIP_MW = "EQUIP_MW";
        public const string itblEquipMicrow = EQUIP_MW;
        public const string itblMicrowAntenna = "ANTENNA_MW";
        public const string DocFiles = "DOCFILES";
        public const string itblDocLink = "DOCLINK";
        public const string itblSysConfig = "SYS_CONFIG";
        public const string itblEmployee = "EMPLOYEE";
        public const string itblArea = "AREA";
        public const string itblCities = "CITIES";
        public const string itblCombo = "COMBO";
        public const string itblAhAllotments = "CH_ALLOTMENTS";
        public const string itblChAllotCh = "CH_ALLOTED_CH";
        public const string ChAllotments = itblAhAllotments;
        public const string LICENCE = "LICENCE";
        public const string itblLicence = LICENCE;
        public const string itblMobStaFreqs2 = "MOBSTA_FREQS2";
        public const string itblMobStaFreqs = "MOBSTA_FREQS";
        public const string itblTV = "TV_STATION";
        public const string itblBGTV = "BG_TV_STATION";
        public const string itblFM = "FM_STATION";
        public const string itblBGFM = "BG_FM_STATION";
        public const string itblDVBT = "DVBT_STATION";
        public const string itblBGDVBT = "BG_DVBT_STATION";
        public const string itblTDAB = "TDAB_STATION";
        public const string itblBGTDAB = "BG_TDAB_STATION";
        public const string itblFreqPlan = "FREQ_PLAN";
        public const string itblFreqPlanChan = "FREQ_PLAN_CHAN";
        public const string itblGSM = "GSM";
        public const string itblShip = "SHIP";
        public const string itblShipEqp = "SHIP_EQP";
        public const string itblRadioSystem = "RADIO_SYSTEMS";
        public const string itblItuContour = "ITU_CONTOUR";
        public const string itblTskfMember = "TSKF_MEMBER";
        
        public const string FxLkClink = "FXLK_CLINK";
        public const string FxLkClinkSta = "FXLK_CLINKSTA";
        public const string FxLkClinkPrv = "FXLK_CLINKPRV";
        public const string FxLkCoordReg = "FXLK_COORD_REQ";
        
        public const string WienCoordMob = "WIEN_COORD_MOB";
        public const string WienAnsMob = "WIEN_ANS_MOB";
        public const string FixMobNotice = "FIXMOBNOTICE";
        public const string T1nAntennas = "T1N_ANTENNAS";
        public const string T1nRxTxStations = "T1N_RXTX_STATIONS";
        public const string T1nRotational = "T1N_ROTATIONAL";
        //--
        public const string AllPositions = "ALL_POSITIONS";
    }
    //=========================================================
    /// <summary>
    /// Название таблиц плагина
    /// </summary>
    public static class PlugTbl
    {

        public const string itblXnrfaWorksUrcm = "XNRFA_WORKS_URCM";
        public const string xnrfa_urchmpay = "XV_URCHMPAY";
        public const string xnrfa_docfiles = "XNRFA_DOCFILES";
        public const string xnrfa_monitor_contracts = "XNRFA_MON_CONTRACTS";
        public const string LinkSites = "XNRFA_LINKS_SITES";
        public const string RegistryPort = "XFA_REGISTRY_PORT";
        public const string XfaPositionExt = "XFA_POSITION_EXT";
        public const string _itblXnrfaStatExfltr = "XNRFA_STAT_EXFLTR";
        public const string _itblXnrfaExternFilter = "XNRFA_EXTERN_FILTERS";
        public const string _itblXnrfaFilterParam = "XNRFA_FILTER_PARAM";
        public const string itblXnrfaMob2Sector = "XRFA_MOB2_SECTOR";
        public const string APPL_LIC = "XNRFA_APPL_LIC";
        public const string itblXnrfaApplLic = APPL_LIC;
        public const string itblXnrfaPacket = "XNRFA_PACKET";
        public const string APPL = "XNRFA_APPL";
        public const string itblXnrfaAppl = APPL;
        public const string PacketToAppl = "XNRFA_PAC_TO_APPL";
        public const string itblPacketToAppl = PacketToAppl;
        public const string itblXnrfaChangeLog = "XNRFA_CHANGE_LOG";
        public const string itblXnrfaSCPattern = "XNRFA_SCPATTERN";
        public const string EventLog = "XNRFA_EVENT_LOG";
        public const string itblXnrfaEventLog = EventLog;
        public const string itblXnrfaGUID = "XNRFA_GUID";
        public const string itblXnrfaDrvAppl = "XNRFA_APPLPAY";
        public const string itblXnrfaDrvApplWorks = "XNRFA_APPLWORKS";
        public const string itblXnrfaDrvApplApps = "XNRFA_CHOSENAPPLS";
        public const string itblXnrfaPrice = "XNRFA_PRICE";
        public const string itblXnrfaAbonentLinks = "XNRFA_ABONENT_LINKS";
        public const string itblXnrfaDiffMobSta = "XNRFA_DIFF_MOBSTA";
        public const string itblXnrfaDiffMobSta2 = "XNRFA_DIFF_MOBSTA2";
        public const string itblXnrfaMobSta2Freq = "XNRFA_MOBSTA2_FREQ";
        public const string itblXnrfaMobStaFreq = "XNRFA_MOBSTA_FREQ";
        public const string itblXnrfaPositions = "XNRFA_POSITION";
        public const string itblXnrfaEtFreq = "XNRFA_DIFF_ET_FREQ";
        public const string itblXnrfaDeiffTvSta = "XNRFA_DIFF_TV_STAT";
        public const string itblXnrfaDeiffDvbSta = "XNRFA_DIFF_DVB_STAT";
        public const string DifffmStat = "XNRFA_DIFF_FM_STAT";
        public const string itblXnrfaDeiffFmSta = DifffmStat;
        public const string itblXnrfaDeiffMwSta = "XNRFA_DIFF_MW_STAT";
        public const string itblXnrfaDeiffEtSta = "XNRFA_DIFF_ET_STAT";
        public const string itblXnrfaDiffFmDigital = "XNRFA_DIFF_FMDIG";
        public const string itblXnrfaSpecReport = "XNRFA_SPEC_REPORT";
        public const string itblXnrfaReport = "XNRFA_REPORT";
        public const string itblXnrfaShipExt = "XNRFA_SHIP_EXT";
        public const string OWNER_LINK = "XNRFA_OWNER_LINK";
        public const string ABONENT_OWNER = "XNRFA_ABONENT_OWNER";
        public const string LOCKS = "XNRFA_LOCKS";
        public const string SYS_CONFIG = "XNRFA_SYS_CONFIG";
        public const string itblXnrfaDrvApplURCM = "XNRFA_APPLPAYURCM";
        public const string itblXnrfaDrvApplAppsURCM = "XNRFA_CHAPPURCM";
        public const string itblXnrfaCHAPPNET = "XNRFA_CHAPPNET";
        

        public const string MONITORING = "XNRFA_MONITORING";
        public const string LOGS = "XNRFA_LOGS";
        public const string STATE_XML135 = "XNRFA_STATE_XML135";
        public const string itblXnrfaDrvApplWorksURCM = "XNRFA_WORKS_URCM";
        public const string STATUS_WF = "XNRFA_STATUS_WF";
        public const string Status = "XNRFA_STATUS";
        public const string Reminder = "XNRFA_REMINDER";
        public const string ReminderUsers = "XNRFA_REMINDER_USER";
        public const string Ship = "SHIP";
        public const string SpecCondition = "XNRFA_SPEC_COND";
        public const string XfaPosition = "XFA_POSITION";
        public const string XfaAbonentStation = "XFA_ABONENT_STATION";
        public const string XfaAmateur = "XFA_AMATEUR";
        public const string XfaEmitterStation = "XFA_EMI_STATION";
        public const string XfaAnten = "XFA_ANTEN";
        public const string XfaEquip = "XFA_EQUIP";
        public const string XfaNet = "XFA_NET";
        public const string XfaFreq = "XFA_FREQ";
        public const string XfaBandAmateur = "XFA_BAND_AMATEUR";
        public const string RefBaseStation = "XFA_REF_BASE_STA";
        public const string XfaRefBaseStation = RefBaseStation;
        public const string MeasureEquipment = "XNRFA_MEASURE_EQ";
        public const string AuxiliaryEquipment = "XNRFA_AUXILIARY_EQ";
        public const string itblXnrfaMeasureEquipmentCertificates = "XNRFA_MSREQ_CERT";
        public const string itblXnrfaMeasureEquipmentList = "XNRFA_MSREQ_LIST";
        public const string itblXnrfaMeasureEq = "XNRFA_MSREQ_EQ";
        public const string XnrfaMeasureEq = itblXnrfaMeasureEq;
        public const string itblXnrfaAuxiliaryEquipmentList = "XNRFA_AUXEQ_LIST";
        public const string itblXnrfaParamRezNote = "XNRFA_PARM_REZ_NOTE";
        public const string itblXnrfaFees = "XNRFA_FEES";
        public const string itblXnrfaFeesRadioSystems = "XNRFA_FEES_RS";
        public const string XfaExportRchp = "XFA_EXPORT_RCHP";
        public const string XfaNabEnt = "XFA_NAB_ENT";
        public const string XAdmCoordAgre = "XADM_COORD_AGRE";
        public const string XFrecDistAdmAgre = "XFREC_DIST_ADM_AGRE";
        public const string MonitoringContract = "XNRFA_MONITOR_CONTR";
        public const string XnrfaDocs = "XNRFA_DOCS";

        public const string TableNamePosition = "XFA_POSITION";
        public const string XfaEsAmateur = "XFA_ES_AMATEUR";
        public const string XfaEquipAmateur = "XFA_EQUIP_AMATEUR";
        public const string XfaFeAmateur = "XFA_FE_AMATEUR";
        public const string XfaFreqAmateur = "XFA_FREQ_AMATEUR";
        public const string XfaCallAmateur = "XFA_CALL_AMATEUR";
        public const string XnChangeMsg = "XN_CHANGE_MSG";
        public const string XnChangeLog = "XN_CHANGE_LOG";
        public const string XfaWienCrd = "XFA_WIEN_CRD_EXT";

        // View
        public const string AllStations = "ALLSTATIONS";
        public const string AllDecentral = "ALLDECENTRALIZED";
        public const string XvPermAll = "XV_PERM_ALL";
        public const string XvNet = "XV_NET";
        public const string XvMonitorUsers = "XV_MONITOR_USERS";
        public const string XvPCHAPPNET = "P_CHAPPNET";

        // View access
        public const string XvAllPosition = "XV_ALL_POSITIONS";
        public const string XvAbonentStation = "XV_ABONENT_STATION";
        public const string XvAmateur = "XV_AMATEUR";
        public const string XvShip = "XV_SHIP";
        public const string XvEmiStation = "XV_EMI_STATION";
        public const string XvPacket = "XV_PACKET";
        public const string XvAppl = "XV_APPL";
        public const string XvMsreqCert = "XV_MSREQ_CERT";
        public const string XvFilialSite = "XV_FILIAL_SITE";
        public const string XvFilEquip = "XV_FILIAL_EQUIP";
        public const string XvAnten = "XV_ANTEN";
        public const string XvEquip = "XV_EQUIP";
        public const string XvEquipAmateur = "XV_EQUIP_AMATEUR";
        public const string XvSites = "XV_SITES";
        public const string XvAbonentOwner = "XV_ABONENT_OWNER";
        public const string XvApplPay = "XV_APPLPAY";
        public const string XvApplPayUrchm = "XV_APPLPAYURCHM";
        public const string XvWorksUrchm = "XV_WORKSURCHM";
        public const string XvAuxEq = "XV_AUXILARY_EQ";
        public const string XvMeasureEq = "XV_MEASURE_EQ";
        public const string XvTaxPayerForm1 = "XV_TAX_PAYER_FORM1";
        public const string XvTaxPayerForm2 = "XV_TAX_PAYER_FORM2";
        public const string XvTaxPaeyrForm3 = "XV_TAX_PAYER_FORM3";
    }
}