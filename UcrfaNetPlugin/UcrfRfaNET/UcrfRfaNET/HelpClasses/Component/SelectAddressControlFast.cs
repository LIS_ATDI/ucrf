﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses.Component
{
    public partial class SelectAddressControlFast : UserControl, INotifyPropertyChanged
    {
        public class City : ICloneable
        {
            //==========================================
            private string _cityName;
            public string cityName
            {
                get { return _cityName; }
                set
                {
                    _cityName = value;
                    if (_cityName == null)
                        _cityName = "";
                }
            }
            //=========================================
            private string _subprov;
            public string subprov
            {
                get { return _subprov; }
                set
                {
                    _subprov = value;
                    if (_subprov == null)
                        _subprov = "";
                }
            }
            //========================================
            private string _prov;
            public string prov
            {
                get { return _prov; }
                set
                {
                    _prov = value;
                    if (_prov == null)
                        _prov = "";
                }
            }
            //=======================================
            private string _type;
            public string type
            {
                get { return _type; }
                set
                {
                    _type = value;
                    if (_type == null)
                        _type = "";
                }
            }
            //=======================================
            private string shortTypeCity;
            public string ShortTypeCity
            {
                get { return shortTypeCity; }
                set
                {
                    shortTypeCity = value;
                    if (shortTypeCity == null)
                        shortTypeCity = "";
                }
            }
            //=======================================
            private string provType;
            public string ProvType
            {
                get { return provType; }
                set
                {
                    provType = value;
                    if (provType == null)
                        provType = "";
                }
            }
            //=======================================
            private string _shortProvType;
            public string ShortProvType
            {
                get { return _shortProvType; }
                set
                {
                    _shortProvType = value;
                    if (_shortProvType == null)
                        _shortProvType = "";
                }
            }
            //=======================================
            private string _shortSubprovType;
            public string ShortSubprovType
            {
                get { return _shortSubprovType; }
                set
                {
                    _shortSubprovType = value;
                    if (_shortSubprovType == null)
                        _shortSubprovType = "";
                }
            }
            //=======================================
            protected int _cityId = IM.NullI;
            public int Id { 
                get
                {
                    return _cityId;
                }
                set
                {
                    if (_cityId != value)
                    {
                        _cityId = value;                        
                    }
                }
            }
            
            //=======================================
            public City()
            {
                cityName = "";
                subprov = "";
                prov = "";
                ProvType = "";
                shortTypeCity = "";
                _shortProvType = "";
                _shortSubprovType = "";
                Id = IM.NullI;
            }            

            public object Clone()
            {
                City newCity = new City();
                newCity.cityName = cityName;
                newCity.subprov = subprov;
                newCity.prov = prov;
                newCity.ProvType = ProvType;
                newCity.shortTypeCity = shortTypeCity;
                newCity._shortProvType = _shortProvType;
                newCity._shortSubprovType = _shortSubprovType;
                newCity._type = _type;
                newCity.Id = Id;

                return newCity;
            }
        }
        
        private bool _isValid;
        /// <summary>
        /// Выбран корректная адрес из KOATУУ
        /// </summary>
        public bool IsValid
        {
            get { return _isValid; }

            set
            {
                if (_isValid != value)
                {
                    _isValid = value;
                    NotifyPropertyChanged("IsValid");
                }
            }
        }
        private string _tmpFullAddress = "";
        public string LableFullAddress
        {
            get { return _tmpFullAddress; }
            set
            {
                if (_tmpFullAddress != value)
                {
                    _tmpFullAddress = value;
                    NotifyPropertyChanged("LableFullAddress");
                }
            }
        }
        private string _lableMessage = "";
        public string LableMessage
        {
            get { return _lableMessage; }
            set
            {
                if (_lableMessage != value)
                {
                    _lableMessage = value;
                    NotifyPropertyChanged("LableMessage");
                    btnClearLists.Refresh();
                }
            }
        }
        /// <summary>
        ///  чтобы забисаблить
        /// </summary>
        public bool CanEditPosition { get; set; }

        private const string DefaultMessage = "F12 - Clear all lists";
        //------
        private List<City> _listForComboBox = new List<City>();
        private List<City> _listUnvisibleData = new List<City>();
        private static List<City> _listCities = null;
        private ComboBox _lastComboBox = null;
        //------
        // Функции
        /// <summary>
        /// Конструктор
        /// </summary>
        public SelectAddressControlFast()
        {
            InitializeComponent();
            InitCityList(); //Инициализация списка городов
            // ----
            IsValid = false;
            // Привязки
            cbProvince.DataBindings.Add("Enabled", this, "CanEditPosition");
            cbSubprovince.DataBindings.Add("Enabled", this, "CanEditPosition");
            cbTypeCity.DataBindings.Add("Enabled", this, "CanEditPosition");
            cbCity.DataBindings.Add("Enabled", this, "CanEditPosition");
            cbCheckKoatuu.DataBindings.Add("Text", this, "LableFullAddress");
            cbCheckKoatuu.DataBindings.Add("Checked", this, "IsValid", true, DataSourceUpdateMode.OnPropertyChanged);
            btnClearLists.DataBindings.Add("Text", this, "LableMessage");
            _listForComboBox.AddRange(_listCities);
        }
        /// <summary>
        /// Отображение компоненты
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAddressControlFast_Load(object sender, EventArgs e)
        {
            UpdateFilter(true);
        }

        /// <summary>
        /// инициализация списка городов
        /// </summary>
        private void InitCityList()
        {
            if (_listCities == null)
            {
                int index = 0;
                using (CProgressBar bar = new CProgressBar(CLocaliz.TxT("Loading cities...")))
                {
                    _listCities = new List<City>();
                    //==============================
                    //первоначальная загрузка списков
                    IMRecordset r = new IMRecordset(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly);
                    r.Select("ID,NAME,SUBPROVINCE,PROVINCE,CUST_TXT1,CUST_TXT3,CUST_TXT2,CUST_TXT4,CUST_TXT5,CUST_TXT8");
                    r.OrderBy("PROVINCE", OrderDirection.Ascending);
                    try
                    {
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            City c = new City();
                            c.Id = r.GetI("ID");
                            c.cityName = ChangeSumbol(r.GetS("NAME"));
                            c.subprov = ChangeSumbol(r.GetS("SUBPROVINCE"));
                            c.prov = ChangeSumbol(r.GetS("PROVINCE"));
                            c.type = ChangeSumbol(r.GetS("CUST_TXT1"));
                            c.ProvType = ChangeSumbol(r.GetS("CUST_TXT3"));
                            c.ShortTypeCity = ChangeSumbol(r.GetS("CUST_TXT4"));
                            c.ShortSubprovType = ChangeSumbol(r.GetS("CUST_TXT5"));
                            c.ShortProvType = ChangeSumbol(r.GetS("CUST_TXT8"));
                            _listCities.Add(c);
                            bar.ShowSmall(++index);
                            bar.ShowBig(c.cityName);
                        }
                    }
                    finally
                    {
                        r.Final();
                    }
                }
            }            
        }

        bool _isUpdatingFilter = false;
        private void UpdateFilter(bool update)
        {
            if (_isUpdatingFilter == false)
            {
                int indexForPercent = 0;
                string mess = CLocaliz.TxT("Updating lists. Please wait.");
                LableMessage = mess;
                Cursor lastCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;
                _isUpdatingFilter = true;
                try
                {
                    bool isChenged = false;
                    string tmpProvinceOld = (string.IsNullOrEmpty(cbProvince.Text) == false) ? cbProvince.Text : "";
                    string tmpSubProvinceOld = (string.IsNullOrEmpty(cbSubprovince.Text) == false) ? cbSubprovince.Text : "";
                    string tmpCityTypeOld = (string.IsNullOrEmpty(cbTypeCity.Text) == false) ? cbTypeCity.Text : "";
                    string tmpCityOld = (string.IsNullOrEmpty(cbCity.Text) == false) ? cbCity.Text : "";
                    string tmpProvince = tmpProvinceOld.ToLower();
                    string tmpSubProvince = tmpSubProvinceOld.ToLower();
                    string tmpCityType = tmpCityTypeOld.ToLower();
                    string tmpCity = tmpCityOld.ToLower();

                    //Удаляем не нужные записи
                    int index = 0;
                    while (index < _listForComboBox.Count)
                    {
                        City city = _listForComboBox[index];

                        bool canDelete = false;
                        if ((string.IsNullOrEmpty(tmpProvince) == false) && (city.prov.ToLower().Equals(tmpProvince) == false))
                            canDelete = true;
                        if ((string.IsNullOrEmpty(tmpSubProvince) == false) && (city.subprov.ToLower().Equals(tmpSubProvince) == false))
                            canDelete = true;
                        if ((string.IsNullOrEmpty(tmpCityType) == false) && (city.type.ToLower().Equals(tmpCityType) == false))
                            canDelete = true;
                        if ((string.IsNullOrEmpty(tmpCity) == false) && (city.cityName.ToLower().Equals(tmpCity) == false))
                            canDelete = true;

                        if (canDelete)
                        {
                            _listForComboBox.Remove(city);
                            _listUnvisibleData.Add(city);
                            isChenged = true;
                        }
                        else
                        {
                            index++;
                        }
                        indexForPercent++;
                        if (_listCities.Count != 0)
                        {
                            int percent = 100 * indexForPercent / _listCities.Count;
                            if (percent > 100)
                                percent = 100;
                            LableMessage = string.Format("{0} {1}%", mess, percent);
                        }
                    }

                    index = 0;
                    while (index < _listUnvisibleData.Count)
                    {
                        City city = _listUnvisibleData[index];

                        bool canDelete = false;
                        //if (hash.Contains(city) == false)
                        {
                            bool canAdd = true;
                            if ((string.IsNullOrEmpty(tmpProvince) == false) &&
                                (city.prov.ToLower().Equals(tmpProvince) == false))
                                canAdd = false;
                            if ((string.IsNullOrEmpty(tmpSubProvince) == false) &&
                                (city.subprov.ToLower().Equals(tmpSubProvince) == false))
                                canAdd = false;
                            if ((string.IsNullOrEmpty(tmpCityType) == false) &&
                                (city.type.ToLower().Equals(tmpCityType) == false))
                                canAdd = false;
                            if ((string.IsNullOrEmpty(tmpCity) == false) &&
                                (city.cityName.ToLower().Equals(tmpCity) == false))
                                canAdd = false;
                            if (canAdd)
                            {
                                _listForComboBox.Add(city);
                                canDelete = true;
                                isChenged = true;
                            }
                        }
                        if (canDelete)
                            _listUnvisibleData.Remove(city);
                        else
                            index++;
                        indexForPercent++;
                        if (_listCities.Count != 0)
                        {
                            int percent = 100*indexForPercent/_listCities.Count;
                            if (percent > 100)
                                percent = 100;
                            LableMessage = string.Format("{0} {1}%", mess, percent);
                        }
                    }
                    if (isChenged || update)
                    {
                        //
                        {
                            var arrayCity = (from n in _listForComboBox
                                             orderby n.cityName
                                             select n.cityName).Distinct().ToArray();
                            cbCity.DataSource = arrayCity;
                            if (arrayCity.Length == 1)
                                cbCity.Text = arrayCity[0];
                            else
                                cbCity.Text = tmpCityOld;
                        }
                        //
                        {
                            var arrayTypeCity = (from n in _listForComboBox
                                                 orderby n.type
                                                 select n.type).Distinct().ToArray();
                            cbTypeCity.DataSource = arrayTypeCity;
                            if (arrayTypeCity.Length == 1)
                                cbTypeCity.Text = arrayTypeCity[0];
                            else
                                cbTypeCity.Text = tmpCityTypeOld;
                        }
                        //
                        {
                            var arraySubProv = (from n in _listForComboBox
                                                orderby n.subprov
                                                select n.subprov).Distinct().ToArray();
                            cbSubprovince.DataSource = arraySubProv;
                            if (arraySubProv.Length == 1)
                                cbSubprovince.Text = arraySubProv[0];
                            else
                                cbSubprovince.Text = tmpSubProvinceOld;
                        }
                        //
                        {
                            var arrayProv = (from n in _listForComboBox
                                             orderby n.prov
                                             select n.prov).Distinct().ToArray();
                            cbProvince.DataSource = arrayProv;
                            if (arrayProv.Length == 1)
                                cbProvince.Text = arrayProv[0];
                            else
                                cbProvince.Text = tmpProvinceOld;
                        }
                        if (_listForComboBox.Count == 1)
                        {
                            SetCurCity(_listForComboBox[0]);
                            LableFullAddress = GetFullAddress();
                        }
                        else
                        {
                            LableFullAddress = "Без КОАТУУ";
                            SetCurCity(null);
                        }
                    }
                }
                finally
                {
                    _isUpdatingFilter = false;
                    this.Cursor = lastCursor;
                    LableMessage = CLocaliz.TxT(DefaultMessage);
                }
            }
        }

        private City _curCity = null;
        private void SetCurCity(City city)
        {
            if(_curCity != city)
            {
                _curCity = city;
                City tmpCity = _curCity;
                if (tmpCity != null)
                {
                    _isUpdatingFilter = true;
                    cbProvince.Text = tmpCity.prov;
                    cbSubprovince.Text = tmpCity.subprov;
                    cbTypeCity.Text = tmpCity.type;
                    cbCity.Text = tmpCity.cityName;
                    _isUpdatingFilter = false;
                    UpdateFilter(true);
                }
                IsValid = (_curCity != null);
            }
        }
       
        public City CurrentCity
        { 
            get { return (_curCity == null) ? new City() : _curCity; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void InitializeByCityId(int idCities)
        {
            FindCityById(idCities);
        }
        /// <summary>
        /// Функция заменяет символ '!' (знач восклицания) на ' ' (пробел)
        /// </summary>
        /// <param name="inString">Входящая строка</param>
        /// <returns>Возвращает измененую строку</returns>
        private string ChangeSumbol(string inString)
        {
            if (string.IsNullOrEmpty(inString))
                return "";
            return inString.Replace('!', ' ');
        }

        private void FindCityById(int cityId)
        {
            City tmpCity = null;
            foreach (City c in _listCities)
            {
                if (c.Id == cityId)
                    tmpCity = c;
            }
            SetCurCity(tmpCity);
        }

        public string GetFullAddress()
        {
            string fullAdr = "";

            City referenceCity = _curCity;
            if (referenceCity == null)
                return fullAdr;

            if (referenceCity.prov.Trim() != "" &&
                string.IsNullOrEmpty(referenceCity.ShortProvType) == false)
            {
                fullAdr = referenceCity.prov + " " + referenceCity.ShortProvType.Trim();
            }

            if (referenceCity.subprov.Trim() != "" &&
                string.IsNullOrEmpty(referenceCity.ShortSubprovType) == false)
            {
                if (fullAdr != "")
                    fullAdr += ", ";
                fullAdr += referenceCity.subprov + " " + referenceCity.ShortSubprovType;
            }

            if (referenceCity.cityName.Trim() != "")
            {
                if (fullAdr != "")
                    fullAdr += ", ";
                fullAdr += referenceCity.ShortTypeCity + " " + referenceCity.cityName;
            }
            return fullAdr;
        }

        public string GetProvinceText()
        {
            return cbProvince.Text;            
        }

        public string GetSubprovinceText()
        {
            return cbSubprovince.Text;
        }

        public string GetCity()
        {
            return cbCity.Text;
        }

        private void SelectAddressControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                e.Handled = true;
                ClearLists();
            }
        }

        private void ClearLists()
        {
            _isUpdatingFilter = true;
            cbProvince.Text = "";
            cbSubprovince.Text = "";
            cbTypeCity.Text = "";
            cbCity.Text = "";
            _isUpdatingFilter = false;
            UpdateFilter(false);
        }

        private void cbProvince_Validated(object sender, EventArgs e)
        {
            UpdateFilter(false);
            if(IsValid == false)
            {
                if(_lastComboBox == cbCity)
                {
                    cbCity.TabIndex = 0;
                    cbTypeCity.TabIndex = 1;
                    cbSubprovince.TabIndex = 2;
                    cbProvince.TabIndex = 3;
                    if (sender == cbCity)
                        this.ActiveControl = cbTypeCity;
                }
                else if (_lastComboBox == cbProvince)
                {
                    cbCity.TabIndex = 3;
                    cbTypeCity.TabIndex = 2;
                    cbSubprovince.TabIndex = 1;
                    cbProvince.TabIndex = 0;
                    if (sender == cbProvince)
                        this.ActiveControl = cbSubprovince;
                }
            }
        }

        private void btnClearLists_Click(object sender, EventArgs e)
        {
            ClearLists();
        }

        private void cbProvince_Enter(object sender, EventArgs e)
        {
            if (sender == cbCity)
                _lastComboBox = cbCity;
            else if (sender == cbProvince)
                _lastComboBox = cbProvince;
        }
    }
}
