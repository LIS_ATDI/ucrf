﻿namespace XICSM.UcrfRfaNET.HelpClasses.Component
{
    partial class SelectAddressControlFast2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCity = new System.Windows.Forms.Label();
            this.lblTypeCity = new System.Windows.Forms.Label();
            this.cbCity = new System.Windows.Forms.ComboBox();
            this.cbTypeCity = new System.Windows.Forms.ComboBox();
            this.lblDistr = new System.Windows.Forms.Label();
            this.lblObl = new System.Windows.Forms.Label();
            this.cbSubprovince = new System.Windows.Forms.ComboBox();
            this.cbProvince = new System.Windows.Forms.ComboBox();
            this.btnClearLists = new System.Windows.Forms.Button();
            this.cbCheckKoatuu = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(156, 67);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(97, 13);
            this.lblCity.TabIndex = 8;
            this.lblCity.Text = "Населений пункт:";
            // 
            // lblTypeCity
            // 
            this.lblTypeCity.AutoSize = true;
            this.lblTypeCity.Location = new System.Drawing.Point(3, 67);
            this.lblTypeCity.Name = "lblTypeCity";
            this.lblTypeCity.Size = new System.Drawing.Size(127, 13);
            this.lblTypeCity.TabIndex = 7;
            this.lblTypeCity.Text = "Тип населеного пункту:";
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(159, 84);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(211, 21);
            this.cbCity.TabIndex = 0;
            this.cbCity.SelectedIndexChanged += new System.EventHandler(this.cbCity_SelectedIndexChanged);
            this.cbCity.SelectionChangeCommitted += new System.EventHandler(this.cbCity_SelectionChangeCommitted);
            this.cbCity.Enter += new System.EventHandler(this.cbProvince_Enter);
            this.cbCity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectAddressControl_KeyDown);
            this.cbCity.Validated += new System.EventHandler(this.cbProvince_Validated);
            // 
            // cbTypeCity
            // 
            this.cbTypeCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbTypeCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTypeCity.FormattingEnabled = true;
            this.cbTypeCity.Location = new System.Drawing.Point(3, 84);
            this.cbTypeCity.Name = "cbTypeCity";
            this.cbTypeCity.Size = new System.Drawing.Size(146, 21);
            this.cbTypeCity.TabIndex = 1;
            this.cbTypeCity.Enter += new System.EventHandler(this.cbProvince_Enter);
            this.cbTypeCity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectAddressControl_KeyDown);
            this.cbTypeCity.Validated += new System.EventHandler(this.cbProvince_Validated);
            // 
            // lblDistr
            // 
            this.lblDistr.AutoSize = true;
            this.lblDistr.Location = new System.Drawing.Point(206, 26);
            this.lblDistr.Name = "lblDistr";
            this.lblDistr.Size = new System.Drawing.Size(41, 13);
            this.lblDistr.TabIndex = 6;
            this.lblDistr.Text = "Район:";
            // 
            // lblObl
            // 
            this.lblObl.AutoSize = true;
            this.lblObl.Location = new System.Drawing.Point(3, 26);
            this.lblObl.Name = "lblObl";
            this.lblObl.Size = new System.Drawing.Size(53, 13);
            this.lblObl.TabIndex = 5;
            this.lblObl.Text = "Область:";
            // 
            // cbSubprovince
            // 
            this.cbSubprovince.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSubprovince.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbSubprovince.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSubprovince.FormattingEnabled = true;
            this.cbSubprovince.Location = new System.Drawing.Point(209, 43);
            this.cbSubprovince.Name = "cbSubprovince";
            this.cbSubprovince.Size = new System.Drawing.Size(161, 21);
            this.cbSubprovince.TabIndex = 2;
            this.cbSubprovince.Enter += new System.EventHandler(this.cbProvince_Enter);
            this.cbSubprovince.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectAddressControl_KeyDown);
            this.cbSubprovince.Validated += new System.EventHandler(this.cbProvince_Validated);
            // 
            // cbProvince
            // 
            this.cbProvince.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbProvince.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbProvince.FormattingEnabled = true;
            this.cbProvince.Location = new System.Drawing.Point(3, 43);
            this.cbProvince.Name = "cbProvince";
            this.cbProvince.Size = new System.Drawing.Size(196, 21);
            this.cbProvince.TabIndex = 3;
            this.cbProvince.Enter += new System.EventHandler(this.cbProvince_Enter);
            this.cbProvince.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectAddressControl_KeyDown);
            this.cbProvince.Validated += new System.EventHandler(this.cbProvince_Validated);
            // 
            // btnClearLists
            // 
            this.btnClearLists.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearLists.Location = new System.Drawing.Point(159, 0);
            this.btnClearLists.Name = "btnClearLists";
            this.btnClearLists.Size = new System.Drawing.Size(211, 20);
            this.btnClearLists.TabIndex = 11;
            this.btnClearLists.Text = "button1";
            this.btnClearLists.UseVisualStyleBackColor = true;
            this.btnClearLists.Click += new System.EventHandler(this.btnClearLists_Click);
            // 
            // cbCheckKoatuu
            // 
            this.cbCheckKoatuu.AutoSize = true;
            this.cbCheckKoatuu.Location = new System.Drawing.Point(3, 3);
            this.cbCheckKoatuu.Name = "cbCheckKoatuu";
            this.cbCheckKoatuu.Size = new System.Drawing.Size(137, 17);
            this.cbCheckKoatuu.TabIndex = 12;
            this.cbCheckKoatuu.Text = "З населеним пунктом";
            this.cbCheckKoatuu.UseVisualStyleBackColor = true;
            this.cbCheckKoatuu.Visible = false;
            // 
            // SelectAddressControlFast2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.cbCheckKoatuu);
            this.Controls.Add(this.btnClearLists);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lblTypeCity);
            this.Controls.Add(this.cbCity);
            this.Controls.Add(this.cbTypeCity);
            this.Controls.Add(this.lblDistr);
            this.Controls.Add(this.lblObl);
            this.Controls.Add(this.cbSubprovince);
            this.Controls.Add(this.cbProvince);
            this.MinimumSize = new System.Drawing.Size(375, 115);
            this.Name = "SelectAddressControlFast2";
            this.Size = new System.Drawing.Size(373, 113);
            this.Load += new System.EventHandler(this.SelectAddressControlFast_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectAddressControl_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblTypeCity;
        private System.Windows.Forms.ComboBox cbCity;
        private System.Windows.Forms.ComboBox cbTypeCity;
        private System.Windows.Forms.Label lblDistr;
        private System.Windows.Forms.Label lblObl;
        private System.Windows.Forms.ComboBox cbSubprovince;
        private System.Windows.Forms.ComboBox cbProvince;
        private System.Windows.Forms.Button btnClearLists;
        public System.Windows.Forms.CheckBox cbCheckKoatuu;
    }
}
