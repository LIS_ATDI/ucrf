﻿namespace XICSM.UcrfRfaNET.HelpClasses.Component
{
    partial class ShortAddressControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbHouse = new System.Windows.Forms.TextBox();
            this.tbStreet = new System.Windows.Forms.TextBox();
            this.cbTypeStreet = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbOther = new System.Windows.Forms.TextBox();
            this.button_change = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbHouse
            // 
            this.tbHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHouse.Location = new System.Drawing.Point(329, 20);
            this.tbHouse.Name = "tbHouse";
            this.tbHouse.Size = new System.Drawing.Size(66, 20);
            this.tbHouse.TabIndex = 18;
            // 
            // tbStreet
            // 
            this.tbStreet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStreet.Location = new System.Drawing.Point(100, 20);
            this.tbStreet.Name = "tbStreet";
            this.tbStreet.Size = new System.Drawing.Size(150, 20);
            this.tbStreet.TabIndex = 17;
            // 
            // cbTypeStreet
            // 
            this.cbTypeStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeStreet.FormattingEnabled = true;
            this.cbTypeStreet.Location = new System.Drawing.Point(6, 20);
            this.cbTypeStreet.Name = "cbTypeStreet";
            this.cbTypeStreet.Size = new System.Drawing.Size(83, 21);
            this.cbTypeStreet.TabIndex = 16;
            this.cbTypeStreet.DropDown += new System.EventHandler(this.cbTypeStreet_DropDown);
            this.cbTypeStreet.SelectionChangeCommitted += new System.EventHandler(this.cbTypeStreet_SelectionChangeCommitted);
            this.cbTypeStreet.DropDownClosed += new System.EventHandler(this.cbTypeStreet_DropDownClosed);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(97, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Street";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Street type";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(326, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "House";
            // 
            // tbOther
            // 
            this.tbOther.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOther.Location = new System.Drawing.Point(6, 47);
            this.tbOther.Multiline = true;
            this.tbOther.Name = "tbOther";
            this.tbOther.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOther.Size = new System.Drawing.Size(389, 50);
            this.tbOther.TabIndex = 22;
            // 
            // button_change
            // 
            this.button_change.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_change.Location = new System.Drawing.Point(267, 18);
            this.button_change.Name = "button_change";
            this.button_change.Size = new System.Drawing.Size(56, 23);
            this.button_change.TabIndex = 23;
            this.button_change.Text = "Change";
            this.button_change.UseVisualStyleBackColor = true;
            this.button_change.Click += new System.EventHandler(this.button_change_Click);
            // 
            // ShortAddressControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.button_change);
            this.Controls.Add(this.tbOther);
            this.Controls.Add(this.tbHouse);
            this.Controls.Add(this.tbStreet);
            this.Controls.Add(this.cbTypeStreet);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Name = "ShortAddressControl";
            this.Size = new System.Drawing.Size(400, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbHouse;
        private System.Windows.Forms.TextBox tbStreet;
        private System.Windows.Forms.ComboBox cbTypeStreet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbOther;
        private System.Windows.Forms.Button button_change;
    }
}
