﻿using System;
using System.Windows.Forms;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.UtilityClass;
using XICSM.UcrfRfaNET.HelpClasses.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Component
{
    public partial class ShortAddressControl : UserControlNotifyPropertyChanged
    {
        /// <summary>
        /// Список типов улиц
        /// </summary>
        /// 
        private int CurrIndex = -1;
        private ComboBoxDictionaryList<string, string> _streetTypeDictionary;

        public const string FieldStreetType = "StreetType";
        private string _streetType = "";
        /// <summary>
        /// Тип улицы
        /// </summary>
        public string StreetType
        {
            get { return _streetType; }
            set
            {
                if (_streetType != value)
                {
                    _streetType = value;
                    InvokeNotifyPropertyChanged(FieldStreetType);
                }
            }
        }

        public const string FieldStreetName = "StreetName";
        private string _streetName = "";
        /// <summary>
        /// Название улицы
        /// </summary>
        public string StreetName
        {
            get { return _streetName; }
            set
            {
                if (_streetName != value)
                {
                    _streetName = value;
                    InvokeNotifyPropertyChanged(FieldStreetName);
                }
            }
        }

        public const string FieldHouseNumber = "HouseNumber";
        private string _houseNumber = "";
        /// <summary>
        /// Номер дома
        /// </summary>
        public string HouseNumber
        {
            get { return _houseNumber; }
            set
            {
                if (_houseNumber != value)
                {
                    _houseNumber = value;
                    InvokeNotifyPropertyChanged(FieldHouseNumber);
                }
            }
        }

        public const string FieldOther = "Other";
        private string _other = "";
        /// <summary>
        /// Номер дома
        /// </summary>
        public string Other
        {
            get { return _other; }
            set
            {
                if (_other != value)
                {
                    _other = value;
                    InvokeNotifyPropertyChanged(FieldOther);
                }
            }
        }

        public const string FieldCanEdit = "CanEdit";
        private bool _canEdit = true;
        /// <summary>
        /// Можна изменять
        /// </summary>
        public bool CanEdit
        {
            get { return _canEdit; }
            set
            {
                if (_canEdit != value)
                {
                    _canEdit = value;
                    InvokeNotifyPropertyChanged(FieldCanEdit);
                }
            }
        }



        public const string FieldKoatuu = "Koatuu";
        private string _koatuu = "";
        /// <summary>
        /// Номер КОАТУУ
        /// </summary>
        public string Koatuu
        {
            get { return _koatuu; }
            set
            {
                if (_koatuu != value)
                {
                    _koatuu = value;
                    InvokeNotifyPropertyChanged(FieldKoatuu);
                }
            }
        }

        public const string FieldNasPunct = "NasPunct";
        private string _naspunct = "";
        /// <summary>
        /// Населенный пункт
        /// </summary>
        public string NasPunct
        {
            get { return _naspunct; }
            set
            {
                if (_naspunct != value)
                {
                    _naspunct = value;
                    InvokeNotifyPropertyChanged(FieldNasPunct);
                }
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ShortAddressControl()
        {
            InitializeComponent();
            //---
            label5.Text = CLocaliz.TxT(label5.Text);
            label6.Text = CLocaliz.TxT(label6.Text);
            label7.Text = CLocaliz.TxT(label7.Text);
            button_change.Text = CLocaliz.TxT(button_change.Text);
            //---
   
            _streetTypeDictionary = new ComboBoxDictionaryList<string, string>();
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("", ""));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("алея", "алея"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("б-р", "бульвар"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("в'їзд", "в'їзд"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("вул.", "вулиця"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("дорога", "дорога"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("ж/м", "житловий масив"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("заїзд", "заїзд"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("кв-л", "квартал"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("лінія", "лінія"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("майдан", "майдан"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("мкр-н", "мікрорайон"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("наб.", "набережна"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("пл.", "площа"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("пров.", "провулок"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("проїзд", "проїзд"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("просіка", "просіка"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("пр-т", "проспект"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("роз'їзд", "роз'їзд"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("ст.м.", "станція метро"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("тупик", "тупик"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("узвіз", "узвіз"));
            _streetTypeDictionary.Add(new ComboBoxDictionary<string, string>("шосе", "шосе"));

            //---
            _streetTypeDictionary.InitComboBox(cbTypeStreet, true);
            cbTypeStreet.DataBindings.Add("SelectedValue", this, FieldStreetType);
            tbStreet.DataBindings.Add("Text", this, FieldStreetName, true, DataSourceUpdateMode.OnPropertyChanged);
            tbHouse.DataBindings.Add("Text", this, FieldHouseNumber, true, DataSourceUpdateMode.OnPropertyChanged);
            tbOther.DataBindings.Add("Text", this, FieldOther, true, DataSourceUpdateMode.OnPropertyChanged);
            //---
            cbTypeStreet.DataBindings.Add("Enabled", this, FieldCanEdit);
            tbStreet.DataBindings.Add("Enabled", this, FieldCanEdit);
            tbHouse.DataBindings.Add("Enabled", this, FieldCanEdit);
            tbOther.DataBindings.Add("Enabled", this, FieldCanEdit);
        }
        /// <summary>
        /// Инициализация компоненты
        /// </summary>
        /// <param name="panel">Панель, в которой отображаеться компонента</param>
        public void Init(Panel panel)
        {
            panel.Controls.Add(this);
            this.Dock = DockStyle.Fill;
        }
        /// <summary>
        /// Возвращает короткий адрес
        /// </summary>
        /// <returns>короткий адрес</returns>
        public string GetAddress()
        {
            return PositionState2.GenerateShortAddress(StreetType, StreetName, HouseNumber, Other);
        }


        private void cbTypeStreet_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
             CurrIndex = cbTypeStreet.SelectedIndex;
            _streetTypeDictionary.InitComboBox(cbTypeStreet, true);
            cbTypeStreet.Refresh();
            cbTypeStreet.SelectedIndex = CurrIndex;
           
        }


        private void cbTypeStreet_DropDown(object sender, System.EventArgs e)
        {
            _streetTypeDictionary.InitComboBox(cbTypeStreet, false);
            CurrIndex = cbTypeStreet.SelectedIndex;
        }

        private void cbTypeStreet_DropDownClosed(object sender, System.EventArgs e)
        {
            cbTypeStreet_SelectionChangeCommitted(sender,e);
        }

        private void button_change_Click(object sender, System.EventArgs e)
        {
            if (StreetChangeForm.Load(((ComboBoxDictionary<string, string>)cbTypeStreet.SelectedItem).Key, tbStreet.Text, Koatuu))
            {
                MessageBox.Show(string.Format("Увага! {0} Назву вулиці змінено з {1} {2} на {3}", Environment.NewLine, tbStreet.Text, Environment.NewLine, StreetChangeForm.GetStreeNameNew(((ComboBoxDictionary<string, string>)cbTypeStreet.SelectedItem).Key, tbStreet.Text, Koatuu)));
            }

            StreetChangeForm street_frm = new StreetChangeForm(((ComboBoxDictionary<string,string>)cbTypeStreet.SelectedItem).Key, tbStreet.Text, Koatuu, NasPunct);
            street_frm.ShowDialog();

        }       
    }
}
