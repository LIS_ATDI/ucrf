﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses.Component
{
    public partial class SelectAddressControlFast2 : UserControl, INotifyPropertyChanged
    {
        public class City : ICloneable
        {
            //==========================================
            private string _admCode;
            public string AdmCode
            {
                get { return _admCode; }
                set
                {
                    _admCode = value;
                    if (_admCode == null)
                        _admCode = "";
                }
            }
            //==========================================
            private string _cityName;
            public string cityName
            {
                get { return _cityName; }
                set
                {
                    _cityName = value;
                    if (_cityName == null)
                        _cityName = "";
                }
            }
            //=========================================
            private string _subprov;
            public string subprov
            {
                get { return _subprov; }
                set
                {
                    _subprov = value;
                    if (_subprov == null)
                        _subprov = "";
                }
            }
            //========================================
            private string _prov;
            public string prov
            {
                get { return _prov; }
                set
                {
                    _prov = value;
                    if (_prov == null)
                        _prov = "";
                }
            }
            //=======================================
            private string _type;
            public string type
            {
                get { return _type; }
                set
                {
                    _type = value;
                    if (_type == null)
                        _type = "";
                }
            }
            //=======================================
            private string _shortTypeCity;
            public string ShortTypeCity
            {
                get { return _shortTypeCity; }
                set
                {
                    _shortTypeCity = value;
                    if (_shortTypeCity == null)
                        _shortTypeCity = "";
                }
            }
            //=======================================
            private string _provType;
            public string ProvType
            {
                get { return _provType; }
                set
                {
                    _provType = value;
                    if (_provType == null)
                        _provType = "";
                }
            }
            //=======================================
            private string _shortProvType;
            public string ShortProvType
            {
                get { return _shortProvType; }
                set
                {
                    _shortProvType = value;
                    if (_shortProvType == null)
                        _shortProvType = "";
                }
            }
            //=======================================
            private string _shortSubprovType;
            public string ShortSubprovType
            {
                get { return _shortSubprovType; }
                set
                {
                    _shortSubprovType = value;
                    if (_shortSubprovType == null)
                        _shortSubprovType = "";
                }
            }
            //=======================================
            private int _cityId = IM.NullI;
            public int Id { 
                get
                {
                    return _cityId;
                }
                set
                {
                    if (_cityId != value)
                    {
                        _cityId = value;                        
                    }
                }
            }
            //=======================================
            public bool WithoutCity { get; set; }
            public string Code { get; set; }
            /// <summary>
            /// Формирование адреса
            /// </summary>
            public string Address
            {
                get
                {
                    if (ShortTypeCity.ToLower() == "обл.")
                        return cityName + " " + ShortTypeCity;

                    string fullAdr = "";
                    if (prov.Trim() != "" &&
                        string.IsNullOrEmpty(ShortProvType) == false)
                    {
                        fullAdr = prov + " " + ShortProvType.Trim();
                    }

                    if (subprov.Trim() != "" &&
                        string.IsNullOrEmpty(ShortSubprovType) == false)
                    {
                        if (fullAdr != "")
                            fullAdr += ", ";
                        fullAdr += subprov + " " + ShortSubprovType;
                    }

                    if (cityName.Trim() != "")
                    {
                        if (fullAdr != "")
                            fullAdr += ", ";
                        fullAdr += ShortTypeCity.ToLower() == "р-н" ? cityName + " " + ShortTypeCity : ShortTypeCity + " " + cityName;
                    }
                    return fullAdr;
                }
            }
            //=======================================
            public City()
            {
                Load(IM.NullI, "", "", "", "", "", "", "", "", 0, "","");
            }
            //=======================================
            public City(int id) : this()
            {
                using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly))
                {
                    r.Select("ID,NAME,SUBPROVINCE,PROVINCE,CUST_TXT1,CUST_TXT3,CUST_TXT2,CUST_TXT4,CUST_TXT5,CUST_TXT8,CUST_CHB5");
                    r.Select("CODE,ADM_CODE");
                    r.OrderBy("PROVINCE", OrderDirection.Ascending);
                    r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        Load(r.GetI("ID"), r.GetS("NAME"), r.GetS("SUBPROVINCE"),
                            r.GetS("PROVINCE"), r.GetS("CUST_TXT1"), r.GetS("CUST_TXT3"),
                            r.GetS("CUST_TXT4"), r.GetS("CUST_TXT5"), r.GetS("CUST_TXT8"),
                            r.GetI("CUST_CHB5"), r.GetS("CODE"), r.GetS("ADM_CODE"));
                    }
                }
            }
            /// <summary>
            /// Загружает данные
            /// </summary>
            /// <param name="id"></param>
            /// <param name="cityNameLocal"></param>
            /// <param name="subprovLocal"></param>
            /// <param name="provLocal"></param>
            /// <param name="typeLocal"></param>
            /// <param name="provTypeLocal"></param>
            /// <param name="shortTypeCityLocal"></param>
            /// <param name="shortSubprovType"></param>
            /// <param name="shortProvType"></param>
            /// <param name="isNoCoatyy"></param>
            /// <param name="code"></param>
            private void Load(int id,
                              string cityNameLocal,
                              string subprovLocal,
                              string provLocal,
                              string typeLocal,
                              string provTypeLocal,
                              string shortTypeCityLocal,
                              string shortSubprovType,
                              string shortProvType,
                              int isNoCoatyy,
                              string code,
                              string adm_code)
            {
                this.Id = id;
                this.cityName = ChangeSumbol(cityNameLocal);
                this.subprov = ChangeSumbol(subprovLocal);
                this.prov = ChangeSumbol(provLocal);
                this.type = ChangeSumbol(typeLocal);
                this.ProvType = ChangeSumbol(provTypeLocal);
                this.ShortTypeCity = ChangeSumbol(shortTypeCityLocal);
                this.ShortSubprovType = ChangeSumbol(shortSubprovType);
                this.ShortProvType = ChangeSumbol(shortProvType);
                this.WithoutCity = (isNoCoatyy == 1);
                this.Code = code;
                this.AdmCode = adm_code;
            }

            public object Clone()
            {
                City newCity = new City();
                newCity.cityName = cityName;
                newCity.subprov = subprov;
                newCity.prov = prov;
                newCity.ProvType = ProvType;
                newCity.ShortTypeCity = ShortTypeCity;
                newCity.ShortProvType = ShortProvType;
                newCity.ShortSubprovType = ShortSubprovType;
                newCity.type = type;
                newCity.Id = Id;

                return newCity;
            }
            /// <summary>
            /// Функция заменяет символ '!' (знач восклицания) на ' ' (пробел)
            /// </summary>
            /// <param name="inString">Входящая строка</param>
            /// <returns>Возвращает измененую строку</returns>
            private static string ChangeSumbol(string inString)
            {
                if (string.IsNullOrEmpty(inString))
                    return "";
                return inString.Replace('!', ' ');
            }
            /// <summary>
            /// Загрузить все записи
            /// </summary>
            /// <returns></returns>
            public static City[] LoadAllCities()
            {
                List<City> listCities = new List<City>();
                using (LisProgressBar bar = new LisProgressBar(CLocaliz.TxT("Loading cities...")))
                {
                    double citiesNb = 0.0;
                    IM.ExecuteScalar(ref citiesNb, "select count(*) from %CITIES");
                    bar.SetProgress(0, (int)citiesNb);
                    //первоначальная загрузка списков
                    int cnt = 0;
                    
                    bar.SetSmall(string.Format("{0} / {1}", cnt, (int)citiesNb));
                    bar.SetBig("");
                    
                    using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblCities, IMRecordset.Mode.ReadOnly))
                    {
                        r.Select("ID,NAME,SUBPROVINCE,PROVINCE,CUST_TXT1,CUST_TXT3,CUST_TXT2,CUST_TXT4,CUST_TXT5,CUST_TXT8,CUST_CHB5");
                        r.Select("CODE,ADM_CODE");
                        r.OrderBy("PROVINCE", OrderDirection.Ascending);
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            City c = new City();
                            c.Load(r.GetI("ID"), r.GetS("NAME"), r.GetS("SUBPROVINCE"),
                                   r.GetS("PROVINCE"), r.GetS("CUST_TXT1"), r.GetS("CUST_TXT3"),
                                   r.GetS("CUST_TXT4"), r.GetS("CUST_TXT5"), r.GetS("CUST_TXT8"),
                                   r.GetI("CUST_CHB5"), r.GetS("CODE"), r.GetS("ADM_CODE"));
                            listCities.Add(c);
                            
                            if ((++cnt) % 100 == 0 || cnt == 1)
                            {
                                //bar.Increment(true);
                                bar.SetProgress(cnt, (int)citiesNb);
                                bar.SetSmall(string.Format("{0} / {1}", cnt, (int)citiesNb)); 
                                bar.SetBig(c.cityName);
                            }
                            
                        }
                    }
                }                
                return listCities.ToArray();
            }
        }
        /// <summary>
        /// Класс для собития изменения КОАТУУ
        /// </summary>
        public class CityEventArgs
        {
            public CityEventArgs(City city) { City = city; }
            public City City { get; private set; } // readonly
        }

        // 
        public delegate void CityChangedEventHandler(object sender, CityEventArgs e);
        public delegate void TypeCityChangedEventHandler(object sender, CityEventArgs e);

        // Declare the event.
        public event CityChangedEventHandler CityChangedEvent;
        public event TypeCityChangedEventHandler TypeCityChangedEvent;
        private bool _isValid;
        /// <summary>
        /// Выбран корректная адрес из KOATУУ
        /// </summary>
        public bool IsValid
        {
            get { return _isValid; }

            set
            {
                if (_isValid != value)
                {
                    _isValid = value;
                    NotifyPropertyChanged("IsValid");
                }
            }
        }

        private string _lableMessage = "";
        public string LableMessage
        {
            get { return _lableMessage; }
            set
            {
                if (_lableMessage != value)
                {
                    _lableMessage = value;
                    NotifyPropertyChanged("LableMessage");
                    btnClearLists.Refresh();
                }
            }
        }

        private const string FieldCanEditPosition = "CanEditPosition";
        private bool _canEditPosition;
        /// <summary>
        ///  чтобы забисаблить
        /// </summary>
        public bool CanEditPosition
        {
            get { return _canEditPosition; }
            set
            {
                if (_canEditPosition != value)
                {
                    _canEditPosition = value;
                    NotifyPropertyChanged(FieldCanEditPosition);
                    cbTypeCity.Enabled = CanEditPosition;
                    cbCity.Enabled = CanEditPosition;
                }
            }
        }

        public bool ReadOnly { get { return !_canEditPosition; } }

        private string _defaultMessage = CLocaliz.TxT("F12 - Clear all lists");
        //------
        private List<City> _listForComboBox = new List<City>();
        private List<City> _listInvisibleData = new List<City>();
        private static City[] _listCities = null;
        private ComboBox _lastComboBox = null;
        //------
        // Функции
        /// <summary>
        /// Конструктор
        /// </summary>
        public SelectAddressControlFast2()
        {
            InitializeComponent();
            InitCityList(); //Инициализация списка городов
            // ----
            IsValid = false;
            // Привязки
            cbProvince.DataBindings.Add("Enabled", this, FieldCanEditPosition);
            cbSubprovince.DataBindings.Add("Enabled", this, FieldCanEditPosition);
            cbCheckKoatuu.DataBindings.Add("Enabled", this, FieldCanEditPosition);
            btnClearLists.DataBindings.Add("Text", this, "LableMessage");
            btnClearLists.DataBindings.Add("Enabled", this, FieldCanEditPosition);
            _listForComboBox.AddRange(_listCities);
        }


        private void SelectAddressControlFast_Load(object sender, EventArgs e)
        {
            UpdateFilter(true);
        }

        /// <summary>
        /// инициализация списка городов
        /// </summary>
        private void InitCityList()
        {
            if (_listCities == null)
            {
                _listCities = City.LoadAllCities();
            }            
        }

        static public void DropCityList()
        {
            _listCities = null;
        }

        bool _isUpdatingFilter = false;
        public void UpdateFilter(bool forceCombos)
        {
            if (_isUpdatingFilter == false)
            {
                int indexForPercent = 0;
                string mess = CLocaliz.TxT("Updating lists. Please wait.");
                LableMessage = mess;
                Cursor lastCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;
                _isUpdatingFilter = true;
                try
                {
                    bool isChanged = false;
                    string tmpProvinceOld = (string.IsNullOrEmpty(cbProvince.Text) == false) ? cbProvince.Text : "";
                    string tmpSubProvinceOld = (string.IsNullOrEmpty(cbSubprovince.Text) == false) ? cbSubprovince.Text : "";
                    string tmpCityTypeOld = (string.IsNullOrEmpty(cbTypeCity.Text) == false) ? cbTypeCity.Text : "";
                    string tmpCityOld = (string.IsNullOrEmpty(cbCity.Text) == false) ? cbCity.Text : "";
                    string tmpProvince = tmpProvinceOld.ToLower();
                    string tmpSubProvince = tmpSubProvinceOld.ToLower();
                    string tmpCityType = tmpCityTypeOld.ToLower();
                    string tmpCity = tmpCityOld.ToLower();

                    //Удаляем не нужные записи
                    int index = 0;
                    while (index < _listForComboBox.Count)
                    {
                        City city = _listForComboBox[index];

                        bool canDelete = false;
                        if ((string.IsNullOrEmpty(tmpProvince) == false) && (city.prov.ToLower().Equals(tmpProvince) == false))
                            canDelete = true;
                        if ((string.IsNullOrEmpty(tmpSubProvince) == false) && (city.subprov.ToLower().Equals(tmpSubProvince) == false))
                            canDelete = true;
                        
                        if ((string.IsNullOrEmpty(tmpCityType) == false) && (city.type.ToLower().Equals(tmpCityType) == false))
                            canDelete = true;
                        if ((string.IsNullOrEmpty(tmpCity) == false) && (city.cityName.ToLower().Equals(tmpCity) == false))
                            canDelete = true;
                    
                        if (canDelete)
                        {
                            _listForComboBox.Remove(city);
                            _listInvisibleData.Add(city);
                            isChanged = true;
                        }
                        else
                        {
                            index++;
                        }
                        indexForPercent++;
                        if (_listCities.Length != 0)
                        {
                            int percent = 100 * indexForPercent / _listCities.Length;
                            if (percent > 100)
                                percent = 100;
                            LableMessage = string.Format("{0} {1}%", mess, percent);
                        }
                    }

                    index = 0;
                    while (index < _listInvisibleData.Count)
                    {
                        City city = _listInvisibleData[index];

                        bool canDelete = false;
                        //if (hash.Contains(city) == false)
                        {
                            bool canAdd = true;
                            if ((string.IsNullOrEmpty(tmpProvince) == false) &&
                                (city.prov.ToLower().Equals(tmpProvince) == false))
                                canAdd = false;
                            if ((string.IsNullOrEmpty(tmpSubProvince) == false) &&
                                (city.subprov.ToLower().Equals(tmpSubProvince) == false))
                                canAdd = false;
                            
                            if ((string.IsNullOrEmpty(tmpCityType) == false) &&
                                (city.type.ToLower().Equals(tmpCityType) == false))
                                canAdd = false;
                            if ((string.IsNullOrEmpty(tmpCity) == false) &&
                                (city.cityName.ToLower().Equals(tmpCity) == false))
                                canAdd = false;
                        
                            if (canAdd)
                            {
                                _listForComboBox.Add(city);
                                canDelete = true;
                                isChanged = true;
                            }
                        }
                        if (canDelete)
                            _listInvisibleData.Remove(city);
                        else
                            index++;
                        indexForPercent++;
                        if (_listCities.Length != 0)
                        {
                            int percent = 100*indexForPercent/_listCities.Length;
                            if (percent > 100)
                                percent = 100;
                            LableMessage = string.Format("{0} {1}%", mess, percent);
                        }
                    }
                    if (isChanged || forceCombos)
                    {
                        //
                        int oldCityIdx = isChanged ? -1 : cbCity.SelectedIndex;
                        {
                            var arrayCity = (from n in _listForComboBox
                                             orderby n.cityName
                                             select n.cityName).ToArray();
                            // apply Distinct() if city list is not filtered by (sub)region - total list from DB
                            if (string.IsNullOrEmpty(tmpProvince) && string.IsNullOrEmpty(tmpSubProvince))
                                arrayCity = arrayCity.Distinct().ToArray();
                            cbCity.DataSource = arrayCity;
                            cbCity.SelectedIndex = -1;
                            if (arrayCity.Length == 1)
                                cbCity.Text = arrayCity[0];
                            else
                                cbCity.Text = tmpCityOld;
                        }
                        //
                        {
                            var arrayTypeCity = (from n in _listForComboBox
                                                 orderby n.type
                                                 select n.type).Distinct().ToArray();
                            cbTypeCity.DataSource = arrayTypeCity;
                            cbTypeCity.SelectedIndex = -1;
                            if (arrayTypeCity.Length == 1)
                                cbTypeCity.Text = arrayTypeCity[0];
                            else
                                cbTypeCity.Text = tmpCityTypeOld;
                        }
                        //
                        {
                            var arraySubProv = (from n in _listForComboBox
                                                orderby n.subprov
                                                select n.subprov).Distinct().ToArray();
                            cbSubprovince.DataSource = arraySubProv;
                            cbSubprovince.SelectedIndex = -1;
                            if (arraySubProv.Length == 1)
                                cbSubprovince.Text = arraySubProv[0];
                            else
                                cbSubprovince.Text = tmpSubProvinceOld;
                        }
                        //
                        {
                            var arrayProv = (from n in _listForComboBox
                                             orderby n.prov
                                             select n.prov).Distinct().ToArray();
                            cbProvince.DataSource = arrayProv;
                            cbProvince.SelectedIndex = -1;
                            if (arrayProv.Length == 1)
                                cbProvince.Text = arrayProv[0];
                            else
                                cbProvince.Text = tmpProvinceOld;
                        }

                        if (_listForComboBox.Count == 1)
                            SetCurCity(_listForComboBox[0]);
                        else if ((string.IsNullOrEmpty(tmpProvince) && string.IsNullOrEmpty(tmpSubProvince)) || !_listForComboBox.Contains(_curCity))
                            SetCurCity(null);

                        // rayon?
                        if (!string.IsNullOrEmpty(tmpProvince) && !string.IsNullOrEmpty(tmpSubProvince) && string.IsNullOrEmpty(tmpCityType) && string.IsNullOrEmpty(tmpCity))
                        {
                            City [] arrRayon = (from n in _listCities
                                                where (n.prov == cbProvince.Text && n.cityName == cbSubprovince.Text && n.ShortTypeCity == "р-н")
                                                select n).ToArray();
                            if (arrRayon.Count() == 1)
                                SetCurCity(arrRayon[0]);
                            else if (arrRayon.Count() > 1)
                                ; // two rayons with the same name in one region?
                        }
                        
                        // область?
                        if (!string.IsNullOrEmpty(tmpProvince) && string.IsNullOrEmpty(tmpSubProvince) && string.IsNullOrEmpty(tmpCityType) && string.IsNullOrEmpty(tmpCity))
                        {
                            City [] arrRegion = (from n in _listCities
                                                 where (n.cityName == cbProvince.Text && n.ShortTypeCity == "обл.")
                                                 select n).ToArray();
                            if (arrRegion.Count() == 1)
                                SetCurCity(arrRegion[0]);
                            else if (arrRegion.Count() > 1)
                                ; // two regions with the same name?
                        }
                    }
                }
                finally
                {
                    _isUpdatingFilter = false;
                    this.Cursor = lastCursor;
                    LableMessage = CLocaliz.TxT(_defaultMessage);
                }
            }
        }

        private City _curCity = null;
        private void SetCurCity(City city)
        {
            if (_curCity != city)
            {
                _curCity = city;
                IsValid = (_curCity != null);
                if (CityChangedEvent != null)
                    CityChangedEvent(this, new CityEventArgs(CurrentCity));
            }
        }
       
        public City CurrentCity
        { 
            get { return (_curCity == null) ? new City() : _curCity; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void InitializeByCityId(int idCities)
        {
            FindCityById(idCities);
        }

        private void FindCityById(int cityId)
        {
            City tmpCity = null;
            foreach (City c in _listCities)
            {
                if (c.Id == cityId)
                    tmpCity = c;
            }
            _isUpdatingFilter = true;
            if (tmpCity == null)
            {
                cbProvince.Text = "";
                cbSubprovince.Text = "";
                cbTypeCity.Text = "";
                cbCity.Text = "";
            }
            else if (tmpCity.type == "район")
            {
                cbProvince.Text = tmpCity.prov;
                cbSubprovince.Text = tmpCity.subprov;
                cbTypeCity.Text = tmpCity.type;
                cbCity.Text = tmpCity.cityName;
            }
            else if (tmpCity.type == "область")
            {
                cbProvince.Text = tmpCity.prov;
                cbSubprovince.Text = tmpCity.subprov;
                cbTypeCity.Text = tmpCity.type;
                cbCity.Text = tmpCity.cityName;
            }
            else
            {
                cbProvince.Text = tmpCity.prov;
                cbSubprovince.Text = tmpCity.subprov;
                cbTypeCity.Text = tmpCity.type;
                cbCity.Text = tmpCity.cityName;
            }
            _isUpdatingFilter = false;
            UpdateFilter(true);
            SetCurCity(tmpCity);
        }

        public string GetFullAddress()
        {
            City referenceCity = _curCity;
            if (referenceCity == null)
                return "";
            return referenceCity.Address;
        }

        public string GetProvinceText()
        {
            return cbProvince.Text;            
        }

        public string GetSubprovinceText()
        {
            return cbSubprovince.Text;
        }

        public string GetCity()
        {
            return cbCity.Text;
        }

        private void SelectAddressControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                e.Handled = true;
                ClearLists();
            }
        }

        private void ClearLists()
        {
            _isUpdatingFilter = true;
            cbProvince.Text = "";
            cbSubprovince.Text = "";
            cbTypeCity.Text = "";
            cbCity.Text = "";
            _isUpdatingFilter = false;
            UpdateFilter(false);
            SetCurCity(null);
        }

        private void cbProvince_Validated(object sender, EventArgs e)
        {
            UpdateFilter(false);
            if(IsValid == false)
            {
                if(_lastComboBox == cbCity)
                {
                    cbCity.TabIndex = 0;
                    cbTypeCity.TabIndex = 1;
                    cbSubprovince.TabIndex = 2;
                    cbProvince.TabIndex = 3;
                    if (sender == cbCity)
                        this.ActiveControl = cbTypeCity;
                }
                else if (_lastComboBox == cbProvince)
                {
                    cbCity.TabIndex = 3;
                    cbTypeCity.TabIndex = 2;
                    cbSubprovince.TabIndex = 1;
                    cbProvince.TabIndex = 0;
                    if (sender == cbProvince)
                        this.ActiveControl = cbSubprovince;
                }
            }
        }

        private void btnClearLists_Click(object sender, EventArgs e)
        {
            ClearLists();
        }

        private void cbProvince_Enter(object sender, EventArgs e)
        {
            if (sender == cbCity)
                _lastComboBox = cbCity;
            else if (sender == cbProvince)
                _lastComboBox = cbProvince;
        }

        private void cbCity_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
            if ((sender == cbCity) && !_isUpdatingFilter 
                && (_listForComboBox != null) && (_listForComboBox.Count > 0) && (cbCity.SelectedIndex < _listForComboBox.Count))
            {
                City c = null;
                if (cbCity.SelectedIndex == -1)
                {
                    SetCurCity(null);
                }
                else
                {
                    c = _listForComboBox[cbCity.SelectedIndex];
                    if (c.cityName == cbCity.Items[cbCity.SelectedIndex].ToString())
                        SetCurCity(c);
                    else
                        SetCurCity(null);
                }
            }
        }

        private void cbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender == cbCity) 
               && (_listForComboBox != null) && (_listForComboBox.Count > 0))
            {
                City c = null;
                if (cbCity.SelectedIndex == -1)
                {
                    SetCurCity(null);
                }
                else
                {
                    c = _listForComboBox.Find(r => r.cityName == cbCity.Items[cbCity.SelectedIndex].ToString());
                    SetCurCity(c);
                }
            }
        }
    }
}
