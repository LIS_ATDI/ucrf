﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{

    public partial class FPutCountRecord : Form
    {

        decimal _ValueRecordCount;
        

        public  static DialogResultMessageBoxButtons CurrentPress = DialogResultMessageBoxButtons.None;


        public FPutCountRecord(decimal Value, string Caption, decimal Min, decimal Max)
        {
          
            InitializeComponent();
            _ValueRecordCount = Value;
            Show(Caption, Min, Max);
        }

        public decimal GetValueRecordCount()
        {
            return _ValueRecordCount;
        }


        public void Show(string caption, decimal Min, decimal Max)
        {
            label1.Text = CLocaliz.TxT("Count records");
            button1.Text = CLocaliz.TxT("OK");
            numericUpDown1.Minimum = Min;
            numericUpDown1.Maximum = Max;
            numericUpDown1.Value = GetValueRecordCount();
            this.Text = caption;
            CurrentPress = DialogResultMessageBoxButtons.None;
            this.ShowDialog();
        }

        public DialogResultMessageBoxButtons GetStatusPress()
        {
            return CurrentPress;
        }

        private   void button1_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.OK;
            Close();
        }


        private void CustomDialogBox_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            _ValueRecordCount = numericUpDown1.Value;
        }


    }
}
