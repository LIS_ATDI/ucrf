﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.OfficeBridge;
using System.Collections;
using XICSM.UcrfRfaNET.Reports;
using XICSM.UcrfRfaNET.ApplSource;
using OrmCs;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormAnaliticsImport : Form
    {
       public int ID_Table { get; set; }
       public string TableName { get; set; }
       public int ID_OWNER { get; set; }
       public FormAnaliticsImport(int owner_ID, string Table,int Id_Table_)
        {
            InitializeComponent();
            ID_OWNER = owner_ID;
            TableName = Table;
            ID_Table = Id_Table_;
            InitListView();
            ViewData();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitListView()
        {
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.Items.Clear();
            listView1.Columns.Clear();

            listView1.Columns.Add(new ColHeader("№", 70, HorizontalAlignment.Left, true));
            //listView1.Columns.Add(new ColHeader("ID запису", 110, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("ID власника", 150, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Номер дозволу на експлуатацію", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Appl_id", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Опис", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Діючий дозвіл", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Термін дії з", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Термін дії до", 70, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Дата анулювання", 100, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("Кількість робіт", 100, HorizontalAlignment.Left, true));
            listView1.Columns.Add(new ColHeader("WORKS_COUNT_DIFF", 100, HorizontalAlignment.Left, true));
            this.listView1.ColumnClick += new ColumnClickEventHandler(listView1_ColumnClick);
        }

        private void ViewData()
        {
            listView1.Items.Clear();
            string[] arr = new string[11];
            int Num = 1; if (listView1.Items.Count > 0) Num = listView1.Items.Count + 1;
            IMRecordset rs_imp_x = new IMRecordset("XNRFA_IMPORT_ORDER", IMRecordset.Mode.ReadWrite);
            rs_imp_x.Select("ID,DESCRIPTION,APPL_ID,PERM_NUM_ORDER,OWNER_ID,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF,EMPLOYEE_ID");
            rs_imp_x.SetWhere("ID", IMRecordset.Operation.Neq, -1);
            rs_imp_x.SetWhere("EMPLOYEE_ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
            for (rs_imp_x.Open(); !rs_imp_x.IsEOF(); rs_imp_x.MoveNext())
            {
                ListViewItem itm;
                arr[0] = Num.ToString();
                //arr[1] = rs_imp_x.GetI("ID").ToString();
                arr[1] = rs_imp_x.GetI("OWNER_ID") != IM.NullI ? rs_imp_x.GetI("OWNER_ID").ToString() : "";
                arr[2] = rs_imp_x.GetS("PERM_NUM_ORDER");
                arr[3] = rs_imp_x.GetI("APPL_ID") != IM.NullI ? rs_imp_x.GetI("APPL_ID").ToString() : "";
                arr[4] = rs_imp_x.GetS("DESCRIPTION");
                arr[5] = rs_imp_x.GetS("PERM_NUM");
                arr[6] = rs_imp_x.GetT("DATE_FROM") != IM.NullT ? rs_imp_x.GetT("DATE_FROM").ToString() : "";
                arr[7] = rs_imp_x.GetT("DATE_TO") != IM.NullT ? rs_imp_x.GetT("DATE_TO").ToString() : "";
                arr[8] = rs_imp_x.GetT("DATE_CANCEL") != IM.NullT ? rs_imp_x.GetT("DATE_CANCEL").ToString() : "";
                arr[9] = rs_imp_x.GetI("WORKS_COUNT") != IM.NullI ? rs_imp_x.GetI("WORKS_COUNT").ToString() : "";
                arr[10] = rs_imp_x.GetI("WORKS_COUNT_DIFF") != IM.NullI ? rs_imp_x.GetI("WORKS_COUNT_DIFF").ToString() : "";
                itm = new ListViewItem(arr); listView1.Items.Add(itm);
                listView1.FocusedItem = itm;
                Num++;
            }
            if (rs_imp_x.IsOpen())
                rs_imp_x.Close();
            rs_imp_x.Destroy();
        }

        private void SaveCsv(string FileName)
        {
            IMProgress.Create("Generate reports ...");
            try
            {
                string[] ListHead = {   "№",
                                            "ID власника",
                                            "Номер дозволу на експлуатацію",
                                            "Appl_id",
                                            "Опис",
                                            "Діючий дозвіл",
                                            "Термін дії з",
                                            "Термін дії до",
                                            "Дата анулювання",
                                            "Кількість робіт",
                                            "WORKS_COUNT_DIFF"
                                            };
                using (IReport rep_csv = Report.CreateReport(ReportType.Csv))
                {
                    string path_csv = FileName;
                    rep_csv.Init(path_csv,"",";");
                    List<string> line1 = new List<string>();
                    for (int i = 0; i < ListHead.Length; i++)
                    {
                        line1.Add(ListHead[i]);
                    }
                    rep_csv.WriteLine(line1.ToArray());

                    int cb_ = 0;
                    for (int k = 0; k < listView1.Items.Count; k++)
                    {
                        line1.Clear();
                        for (int j = 0; j < listView1.Items[k].SubItems.Count; j++)
                        {
                            line1.Add(listView1.Items[k].SubItems[j].Text);
                        }
                        IMProgress.ShowProgress(cb_++, listView1.Items.Count);
                        rep_csv.WriteLine(line1.ToArray());
                    }

                    rep_csv.Save();

                }
                MessageBox.Show(string.Format(" Csv файл '{0}' вивантажено!", FileName));
            }
            catch (Exception ex)
            { }
            IMProgress.Destroy();
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ColHeader clickedCol = (ColHeader)this.listView1.Columns[e.Column];
            clickedCol.ascending = !clickedCol.ascending;
            int numItems = this.listView1.Items.Count;
            this.listView1.BeginUpdate();
            ArrayList SortArray = new ArrayList();
            for (int i = 0; i < numItems; i++) {
                SortArray.Add(new SortWrapper(this.listView1.Items[i], e.Column));
            }
            SortArray.Sort(0, SortArray.Count, new SortWrapper.SortComparer(clickedCol.ascending));
            this.listView1.Items.Clear();
            for (int i = 0; i < numItems; i++)
                this.listView1.Items.Add(((SortWrapper)SortArray[i]).sortItem);
            this.listView1.EndUpdate();
        }


        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            IM.Execute(string.Format(" DELETE %XNRFA_IMPORT_ORDER WHERE (ID > 0) AND (EMPLOYEE_ID={0}) ",CUsers.GetCurUserID()));
            Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Attach Files(*.CSV)|*.CSV";
            DialogResult DA = saveFileDialog1.ShowDialog();
            if (DA == System.Windows.Forms.DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
                    SaveCsv(saveFileDialog1.FileName);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID_OWNER != IM.NullI)
                {
                    int cnt = 0;
                    IM.ExecuteScalar(ref cnt, string.Format(" SELECT COUNT(*) FROM %XNRFA_IMPORT_ORDER WHERE (APPL_ID = {0}) AND (EMPLOYEE_ID={1}) ", IM.NullI, CUsers.GetCurUserID()));
                    if (cnt > 0)
                    {
                        if (MessageBox.Show("Є записи, для яких не знайдено діючий дозвіл в БД. Продовжити формування службової записки?", "Увага!", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                    }
                    //CDRVMemoURCM(idApplPay, IM.NullI, PlugTbl.XvMonitorUsers, recPtr.Id);
                    if ((TableName == PlugTbl.xnrfa_monitor_contracts) || (TableName == PlugTbl.XvMonitorUsers)) {
                        CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ID_OWNER, TableName, ID_Table, true, false,false,IM.NullI);
                         drvMemo.ShowForm();
                    }
                    else {
                        CDRVMemoURCM drvMemo = new CDRVMemoURCM(IM.NullI, ID_OWNER, TableName, 0, true, false, false, IM.NullI);
                        drvMemo.ShowForm();
                    }
                }
            }
            catch (Exception ex)
            { }

        }

        private void FormAnaliticsImport_FormClosing(object sender, FormClosingEventArgs e)
        {
            IM.Execute(string.Format(" DELETE %XNRFA_IMPORT_ORDER WHERE (ID > 0) AND (EMPLOYEE_ID={0}) ", CUsers.GetCurUserID()));
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    int ID_;
                    if (int.TryParse(listView1.SelectedItems[i].SubItems[3].Text, out ID_))
                    {

                        using (YXnrfaAppl rs_imp = new YXnrfaAppl())
                        {
                            rs_imp.Table = "XNRFA_APPL";
                            rs_imp.Format("*");
                            if (rs_imp.Fetch(ID_))
                            {
                                if (rs_imp.m_id.IsNotNull())
                                {
                                    BaseAppClass obj = BaseAppClass.GetAppl(ID_, false);
                                    obj.SetRight("XNRFA_APPL");
                                    using (MainAppForm formAppl = new MainAppForm(obj))
                                    {
                                        formAppl.ShowDialog();
                                    }
                                }
                            }
                            else MessageBox.Show(string.Format("Вказаного ідентифікатору - {0} не існує в таблиці XNRFA_APPL", ID_));
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

     

    }

    public class SortWrapper
    {
        internal ListViewItem sortItem;
        internal int sortColumn;


        // A SortWrapper requires the item and the index of the clicked column.
        public SortWrapper(ListViewItem Item, int iColumn)
        {
            sortItem = Item;
            sortColumn = iColumn;
        }

        // Text property for getting the text of an item.
        public string Text
        {
            get
            {
                return sortItem.SubItems[sortColumn].Text;
            }
        }

        // Implementation of the IComparer
        // interface for sorting ArrayList items.
        public class SortComparer : IComparer
        {
            bool ascending;

            // Constructor requires the sort order;
            // true if ascending, otherwise descending.
            public SortComparer(bool asc)
            {
                this.ascending = asc;
            }

            // Implemnentation of the IComparer:Compare
            // method for comparing two objects.
            public int Compare(object x, object y)
            {
                SortWrapper xItem = (SortWrapper)x;
                SortWrapper yItem = (SortWrapper)y;
                string xText = xItem.sortItem.SubItems[xItem.sortColumn].Text;
                string yText = yItem.sortItem.SubItems[yItem.sortColumn].Text;
                return xText.CompareTo(yText) * (this.ascending ? 1 : -1);
            }
        }
    }

    public class ColHeader : ColumnHeader
    {
        public bool ascending;
        public ColHeader(string text, int width, HorizontalAlignment align, bool asc)
        {
            this.Text = text;
            this.Width = width;
            this.TextAlign = align;
            this.ascending = asc;
        }
    }

}
