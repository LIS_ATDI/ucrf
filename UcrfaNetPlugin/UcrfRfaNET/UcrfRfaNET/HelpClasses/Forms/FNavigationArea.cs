﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FNavigationArea : Form
    {
        public bool A1 { get { return chbA1.Checked;} set { chbA1.Checked = value; }}
        public bool A2 { get { return chbA2.Checked;} set { chbA2.Checked = value; }}
        public bool A3 { get { return chbA3.Checked;} set { chbA3.Checked = value; }}
        public bool DomescticWaterWays { get { return chbDomestic.Checked;} set { chbDomestic.Checked = value; }}
        public string NonConvention { get { return txtNonConvention.Text; } set { txtNonConvention.Text = value; }}

        public FNavigationArea()
        {
            InitializeComponent();
        }
    }
}
