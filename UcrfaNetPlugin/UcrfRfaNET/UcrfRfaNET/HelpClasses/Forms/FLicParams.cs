﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FLicParams : Form
    {
        protected FLicParams()
        {
            InitializeComponent();
            dtpActualDate.DataBindings["Enabled"].Format += new ConvertEventHandler((s, e) => { e.Value = !(bool)e.Value; });
            CLocaliz.TxT(this);
        }

        void FLicParams_Format(object sender, ConvertEventArgs e)
        {
            throw new NotImplementedException();
        }
        static public DialogResult Config()
        {
            FLicParams f = new FLicParams();
            f.licParamsBindingSource.Add(LicParams.GetLicParams());
            return f.ShowDialog();
        }        
    }
}
