﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FProvincesList
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FProvincesList));
          this.chlbProvs = new System.Windows.Forms.CheckedListBox();
          this.btOk = new System.Windows.Forms.Button();
          this.chb1 = new System.Windows.Forms.CheckBox();
          this.lbCaption = new System.Windows.Forms.Label();
          this.tbComment = new System.Windows.Forms.TextBox();
          this.btCancel = new System.Windows.Forms.Button();
          this.SuspendLayout();
          // 
          // chlbProvs
          // 
          this.chlbProvs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.chlbProvs.FormattingEnabled = true;
          this.chlbProvs.Location = new System.Drawing.Point(12, 27);
          this.chlbProvs.Name = "chlbProvs";
          this.chlbProvs.Size = new System.Drawing.Size(320, 334);
          this.chlbProvs.TabIndex = 0;
          // 
          // btOk
          // 
          this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.btOk.Location = new System.Drawing.Point(135, 401);
          this.btOk.Name = "btOk";
          this.btOk.Size = new System.Drawing.Size(80, 25);
          this.btOk.TabIndex = 3;
          this.btOk.Text = "OK";
          this.btOk.UseVisualStyleBackColor = true;
          // 
          // chb1
          // 
          this.chb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.chb1.AutoSize = true;
          this.chb1.Location = new System.Drawing.Point(12, 397);
          this.chb1.Name = "chb1";
          this.chb1.Size = new System.Drawing.Size(85, 17);
          this.chb1.TabIndex = 2;
          this.chb1.Text = "Вибрати всі";
          this.chb1.UseVisualStyleBackColor = true;
          this.chb1.CheckedChanged += new System.EventHandler(this.chb1_CheckedChanged);
          // 
          // lbCaption
          // 
          this.lbCaption.AutoSize = true;
          this.lbCaption.Location = new System.Drawing.Point(14, 9);
          this.lbCaption.Name = "lbCaption";
          this.lbCaption.Size = new System.Drawing.Size(0, 13);
          this.lbCaption.TabIndex = 3;
          // 
          // tbComment
          // 
          this.tbComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbComment.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.tbComment.Location = new System.Drawing.Point(12, 372);
          this.tbComment.Multiline = true;
          this.tbComment.Name = "tbComment";
          this.tbComment.ReadOnly = true;
          this.tbComment.Size = new System.Drawing.Size(320, 19);
          this.tbComment.TabIndex = 1;
          // 
          // btCancel
          // 
          this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btCancel.Location = new System.Drawing.Point(234, 401);
          this.btCancel.Name = "btCancel";
          this.btCancel.Size = new System.Drawing.Size(80, 25);
          this.btCancel.TabIndex = 4;
          this.btCancel.Text = "Cancel";
          this.btCancel.UseVisualStyleBackColor = true;
          // 
          // FProvincesList
          // 
          this.AcceptButton = this.btOk;
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.CancelButton = this.btCancel;
          this.ClientSize = new System.Drawing.Size(344, 438);
          this.Controls.Add(this.btCancel);
          this.Controls.Add(this.tbComment);
          this.Controls.Add(this.lbCaption);
          this.Controls.Add(this.chb1);
          this.Controls.Add(this.btOk);
          this.Controls.Add(this.chlbProvs);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.MinimumSize = new System.Drawing.Size(300, 400);
          this.Name = "FProvincesList";
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "Вибір областей для пошуку станцій";
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      public System.Windows.Forms.CheckedListBox chlbProvs;
      private System.Windows.Forms.Button btOk;
      private System.Windows.Forms.CheckBox chb1;
      private System.Windows.Forms.Button btCancel;
      public System.Windows.Forms.Label lbCaption;
      public System.Windows.Forms.TextBox tbComment;
   }
}