﻿namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    partial class FNavigationArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblArea;
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.chbA1 = new System.Windows.Forms.CheckBox();
            this.chbA2 = new System.Windows.Forms.CheckBox();
            this.chbA3 = new System.Windows.Forms.CheckBox();
            this.txtNonConvention = new System.Windows.Forms.TextBox();
            this.chbDomestic = new System.Windows.Forms.CheckBox();
            lblArea = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblArea
            // 
            lblArea.AutoSize = true;
            lblArea.Location = new System.Drawing.Point(144, 9);
            lblArea.Name = "lblArea";
            lblArea.Size = new System.Drawing.Size(172, 13);
            lblArea.TabIndex = 2;
            lblArea.Text = "Неконвенційні райони плавання:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(309, 63);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Відхилити";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(228, 63);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Вибрати";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // chbA1
            // 
            this.chbA1.AutoSize = true;
            this.chbA1.Location = new System.Drawing.Point(12, 29);
            this.chbA1.Name = "chbA1";
            this.chbA1.Size = new System.Drawing.Size(39, 17);
            this.chbA1.TabIndex = 3;
            this.chbA1.Text = "A1";
            this.chbA1.UseVisualStyleBackColor = true;
            // 
            // chbA2
            // 
            this.chbA2.AutoSize = true;
            this.chbA2.Location = new System.Drawing.Point(57, 29);
            this.chbA2.Name = "chbA2";
            this.chbA2.Size = new System.Drawing.Size(39, 17);
            this.chbA2.TabIndex = 4;
            this.chbA2.Text = "A2";
            this.chbA2.UseVisualStyleBackColor = true;
            // 
            // chbA3
            // 
            this.chbA3.AutoSize = true;
            this.chbA3.Location = new System.Drawing.Point(102, 29);
            this.chbA3.Name = "chbA3";
            this.chbA3.Size = new System.Drawing.Size(39, 17);
            this.chbA3.TabIndex = 5;
            this.chbA3.Text = "A3";
            this.chbA3.UseVisualStyleBackColor = true;
            // 
            // txtNonConvention
            // 
            this.txtNonConvention.Location = new System.Drawing.Point(147, 27);
            this.txtNonConvention.Name = "txtNonConvention";
            this.txtNonConvention.Size = new System.Drawing.Size(237, 20);
            this.txtNonConvention.TabIndex = 6;
            // 
            // chbDomestic
            // 
            this.chbDomestic.AutoSize = true;
            this.chbDomestic.Location = new System.Drawing.Point(12, 52);
            this.chbDomestic.Name = "chbDomestic";
            this.chbDomestic.Size = new System.Drawing.Size(180, 17);
            this.chbDomestic.TabIndex = 7;
            this.chbDomestic.Text = "Внутрішні водні шляхи України";
            this.chbDomestic.UseVisualStyleBackColor = true;
            // 
            // FNavigationArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 98);
            this.Controls.Add(this.chbDomestic);
            this.Controls.Add(this.txtNonConvention);
            this.Controls.Add(this.chbA3);
            this.Controls.Add(this.chbA2);
            this.Controls.Add(this.chbA1);
            this.Controls.Add(lblArea);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FNavigationArea";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Вибір району плавання";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chbA1;
        private System.Windows.Forms.CheckBox chbA2;
        private System.Windows.Forms.CheckBox chbA3;
        private System.Windows.Forms.TextBox txtNonConvention;
        private System.Windows.Forms.CheckBox chbDomestic;
    }
}