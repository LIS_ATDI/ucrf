﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FWorksEMS : Form
   {
       // Названия колонок
       private const string CnApplId = "id";
       private const string CnArticle = "stat";
       private const string CnCount = "count";


      public FWorksEMS(List<Work> wrks, WorkType type, List<string> arts, AppType appType)
      {
         InitializeComponent();
         CLocaliz.TxT(this);


         DataGridViewComboBoxColumn columnArt = new DataGridViewComboBoxColumn();
         {
            columnArt.HeaderText = "Стаття";
            columnArt.Name = CnArticle;
            columnArt.Width = 120;
            if(arts != null)
               columnArt.DataSource = arts;
         }
         grWorks.Columns.Add(columnArt);

         DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
         column.Name = CnCount;
         column.HeaderText = CLocaliz.TxT("Кількість");
         column.Width = 120;
         grWorks.Columns.Add(column);

         column = new DataGridViewTextBoxColumn();
         column.Name = "notes";
         column.HeaderText = CLocaliz.TxT("Адреса");
         column.Width =  type != WorkType.work14 ? 240 : 360;
         column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
         grWorks.Columns.Add(column);
         
         if (type != WorkType.work14)
         {
            column = new DataGridViewTextBoxColumn();
            column.Name = "koef";
            column.HeaderText = CLocaliz.TxT("Коефіцієнт");
            column.Width = 120;
            grWorks.Columns.Add(column);

            if (type == WorkType.work15)
               this.Text = "Проведення вимірювань параметрів РЕЗ та ВП";
            else if (type == WorkType.work3 && appType == AppType.AppRR)
            {
               column.Visible = false;
            }
         }
         else
         {
            this.Text = "Проведення ПТК";
         }


         column = new DataGridViewTextBoxColumn();
         column.Name = CnApplId;
         column.Visible = false;
         grWorks.Columns.Add(column);

         column = new DataGridViewTextBoxColumn();
         column.Name = "linkNum";
         column.Visible = false;
         grWorks.Columns.Add(column);

         column = new DataGridViewTextBoxColumn();
         column.Name = "addNotes";
         column.Visible = false;
         grWorks.Columns.Add(column);

         Dictionary<int, string> cid = new Dictionary<int, string>();

         if (type == WorkType.work3 && appType == AppType.AppRR)
         {
            column = new DataGridViewTextBoxColumn();
            column.Name = "CID";
            column.HeaderText = CLocaliz.TxT("CID");
            column.Width = 120;
            column.ReadOnly = true;
            grWorks.Columns.Add(column);

            List<int> ids = new List<int>();
            foreach (Work w in wrks)
            {
               ids.Add(w.appId);
            }
            cid = GetSids(ids);
         }

         grWorks.Rows.Clear();
         foreach (Work w in wrks)
         {

            if (type == WorkType.work3 && appType == AppType.AppRR)
            {
               string[] row = new string[] { w.article, w.count.ToString(), w.notes, w.index == IM.NullD ? "" : w.index.ToString(), w.appId.ToString(), (w.linkNum == IM.NullI) ? "1" : w.linkNum.ToString(), w.addNotes, cid[w.appId]};
               grWorks.Rows.Add(row);
            }
            else if (type != WorkType.work14)
            {
               string[] row = new string[] { w.article, w.count.ToString(), w.notes, w.index == IM.NullD ? "" : w.index.ToString(), w.appId.ToString(), (w.linkNum == IM.NullI) ? "1" : w.linkNum.ToString(), w.addNotes};
               grWorks.Rows.Add(row);
            }
            else
            {
               string[] row = new string[] { w.article, w.count.ToString(), w.notes, w.appId.ToString(), (w.linkNum == IM.NullI) ? "1" : w.linkNum.ToString(), w.addNotes };
               grWorks.Rows.Add(row);
            }
         }
      }

      private void FWorksEMS_FormClosed(object sender, FormClosedEventArgs e)
      {
         this.grWorks.EndEdit();
      }
      //===========================================================
      /// <summary>
      /// Возвращает SID заявки
      /// </summary>
      public static Dictionary<int, string> GetSids(List<int> Ids)
      {
         Dictionary<int, string> Sid = new Dictionary<int, string>();
         IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
         try
         {
            rs.Select("ID,MobileSector1.NAME,MobileSector1.STANDARD");
            rs.SetWhere("ID", IMRecordset.Operation.Gt, 0);
            string ids = "";
            foreach (int i in Ids)
            {
               ids += i.ToString() + ",";
            }
            ids = ids.Remove(ids.Length - 1, 1);
            rs.SetAdditional(string.Format("[ID] in ({0})", ids));
            for (rs.Open(); !rs.IsEOF();rs.MoveNext())
            {
               Sid.Add(rs.GetI("ID"), rs.GetS("MobileSector1.NAME"));
            }
         }
         finally
         {
            rs.Close();
            rs.Destroy();
         }
         return Sid;
      }
      /// <summary>
      /// Пересчитать кол-во работ
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnCalculateWork_Click(object sender, EventArgs e)
      {
          DialogResult dlgRez = MessageBox.Show(CLocaliz.TxT("Do you want to recalculate work count?"), "",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (dlgRez == DialogResult.Yes)
              RecalculateWorkCount();
          
      }
      /// <summary>
      /// Пересчитыват кол-во работ
      /// </summary>
      private void RecalculateWorkCount()
      {
          using (CProgressBar pb = new CProgressBar("Recalculation count of work..."))
          {
              pb.ShowBig("Please wait...");
              for (int i = 0; i < grWorks.Rows.Count; i++)
              {
                  DataGridViewRow row = grWorks.Rows[i];
                  int applId = row.Cells[CnApplId].Value.ToString().ToInt32(IM.NullI);
                  pb.ShowSmall(applId);
                  string article = row.Cells[CnArticle].Value.ToString();
                  int count = SqlDatabase.CalculateWorks(applId, article);
                  if (count != IM.NullI)
                      row.Cells[CnCount].Value = count;
                  pb.ShowProgress(i, grWorks.Rows.Count);
              }
          }
      }
   }
}
