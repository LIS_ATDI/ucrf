﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.OfficeBridge;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormLoadFile : Form
    {
        public bool StatusCheckPress { get; set; }
        string NameF { get; set; }
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }
        public TypeFormatFile ChbType { get; set; }

        public FormLoadFile(IMQueryMenuNode.Context context)
        {
            InitializeComponent();
            lstContext = context.DataList;
            NameTable = context.TableName;
            Id_Table = context.TableId;
            openFileDialog1.Filter = "Attach Files(*.CSV;*.XLSX)|*.CSV;*XLSX";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string AllErrors = ""; int CountSucessNew = 0; int CountSucessUpdate = 0; int CountFail = 0;
            DA = System.Windows.Forms.DialogResult.OK;
            try
            {
                if (string.IsNullOrEmpty(NameF)) { MessageBox.Show("Файл не обрано!"); return; }
                if (System.IO.File.Exists(NameF))
                {

                    if (ChbType == TypeFormatFile.CSV)
                    {

                        using (System.IO.StreamReader read = new System.IO.StreamReader(NameF, System.Text.Encoding.Default))
                        {
                            int limit_start = 0; int MaxCountCh=0;
                            int iu_c = 0; if (checkBox1.Checked) limit_start = 1;
                            while (read.Peek() >= limit_start)
                            {

                                string[] str_word = read.ReadLine().Split(new char[] { ';' });

                                if (str_word.Length > 0)
                                {
                                    string decimal_sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;

                                    int ID = IM.NullI;
                                    if (int.TryParse(str_word[0].Trim().ToString(), out ID))
                                    {
                                        MaxCountCh = 0;
                                        if ((ID > 22999999) || (ID < 22000000))
                                        {
                                            AllErrors += string.Format("- ID {0} не належить до технології РРЗ", str_word[0].Trim()) + Environment.NewLine;
                                            iu_c++;
                                            CountFail++;
                                            continue;
                                        }
                                        

                                        int Month_plan = IM.NullI;
                                        if (!string.IsNullOrEmpty(str_word[1].Trim()))
                                        {
                                            if (!int.TryParse(str_word[1].Trim().ToString(), out Month_plan))
                                            {
                                                AllErrors += string.Format("- Номер місяця(PLAN) {0} не коректний [{1}]", str_word[1].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                iu_c++;
                                                CountFail++;
                                                continue;
                                            }
                                            else
                                            {
                                                if ((Month_plan < 1) || (Month_plan > 12))
                                                {
                                                    AllErrors += string.Format("- Номер місяця(PLAN) {0} не коректний [{1}]", str_word[1].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                    iu_c++;
                                                    CountFail++;
                                                    continue;
                                                }
                                            }
                                        }

                                         int Year_plan = IM.NullI;
                                         if (!string.IsNullOrEmpty(str_word[2].Trim()))
                                         {
                                             if (!int.TryParse(str_word[2].Trim().ToString(), out Year_plan))
                                             {
                                                 AllErrors += string.Format("- Номер року(PLAN) {0} не коректний [{1}]", str_word[2].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                 iu_c++;
                                                 CountFail++;
                                                 continue;
                                             }
                                             else
                                             {
                                                 if (Year_plan.ToString().Length < 4)
                                                 {
                                                     AllErrors += string.Format("- Номер року(PLAN) {0} не коректний [{1}]", str_word[2].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                     iu_c++;
                                                     CountFail++;
                                                     continue;
                                                 }
                                             }
                                         }

                                        ////////
                                         int Month_pay= IM.NullI;
                                         if (!string.IsNullOrEmpty(str_word[3].Trim()))
                                         {
                                             if (!int.TryParse(str_word[3].Trim().ToString(), out Month_pay))
                                             {
                                                 AllErrors += string.Format("- Номер місяця(PAY) {0} не коректний [{1}]", str_word[3].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                 iu_c++;
                                                 CountFail++;
                                                 continue;
                                             }
                                             else
                                             {
                                                 if ((Month_pay < 1) || (Month_pay > 12))
                                                 {
                                                     AllErrors += string.Format("- Номер місяця(PAY) {0} не коректний [{1}]", str_word[3].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                     iu_c++;
                                                     CountFail++;
                                                     continue;
                                                 }
                                             }
                                         }

                                         int Year_pay = IM.NullI;
                                         if (!string.IsNullOrEmpty(str_word[4].Trim()))
                                         {
                                             if (!int.TryParse(str_word[4].Trim().ToString(), out Year_pay))
                                             {
                                                 AllErrors += string.Format("- Номер року(PAY) {0} не коректний [{1}]", str_word[4].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                 iu_c++;
                                                 CountFail++;
                                                 continue;
                                             }
                                             else
                                             {
                                                 if (Year_pay.ToString().Length < 4)
                                                 {
                                                     AllErrors += string.Format("- Номер року(PAY) {0} не коректний [{1}]", str_word[4].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                     iu_c++;
                                                     CountFail++;
                                                     continue;
                                                 }
                                             }
                                         }

                                         if ((Year_pay != IM.NullI) && (Month_pay != IM.NullI) && (Year_plan != IM.NullI) && (Month_plan != IM.NullI)) {
                                             DateTime DA_pay = (new DateTime(Year_pay, Month_pay, 1));
                                             DateTime DA_plan = (new DateTime(Year_plan, Month_plan, 1));
                                             if (DA_pay < DA_plan)
                                             {
                                                 AllErrors += string.Format("- Дата(PAY) {0} не може бути менша за дату(PLAN) {1}  [{2}]", DA_pay.ToShortDateString(), DA_plan.ToShortDateString(), str_word[0].Trim()) + Environment.NewLine;
                                                 iu_c++;
                                                 CountFail++;
                                                 continue;
                                             }
                                         }
                                        

                                        ////////
                                        DateTime DT = IM.NullT;
                                        if (!string.IsNullOrEmpty(str_word[5].Trim()))
                                        {
                                            if (!DateTime.TryParse(str_word[5].Trim().ToString(), out DT))
                                            {
                                                AllErrors += string.Format("- Дата проведення ТРК {0} не коректна [{1}]", str_word[5].Trim(), str_word[0].Trim()) + Environment.NewLine;
                                                iu_c++;
                                                continue;
                                                CountFail++;
                                            }
                                        }
                                        string Notes = str_word[6];



                                        IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                                        rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                                        rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID - 22000000);
                                        rs.Open();
                                        if (!rs.IsEOF())
                                        {
                                            if (rs.GetS("STATUS") != "3")
                                            {
                                                if (rs.GetS("STATUS") != "2")
                                                {
                                                    rs.Edit();
                                                    rs.Put("ALLSTAT_ID", ID);
                                                    rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                    rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                    rs.Put("OBJ_ID", ID - 22000000);
                                                    rs.Put("OBJ_TABLE", "MICROWS");

                                                    if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                    if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                                    if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                                    if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }

                                                    if (DT != IM.NullT) rs.Put("STATUS", "2");
                                                    if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                                    if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                                    if (rs.GetT("END_DATE") != IM.NullT)
                                                    {
                                                        AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Record (s) containing information about the TRK result! Change of plan is not possible!") + Environment.NewLine;
                                                        CountFail++;
                                                    }
                                                    if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                                    {
                                                        AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                                        CountFail++;
                                                    }
                                                    if (!(((rs.GetT("END_DATE") != IM.NullT)) && ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))))
                                                        rs.Update();
                                                    else  {
                                                        if ((((rs.GetT("END_DATE") != IM.NullT)) && (DT != IM.NullT)))  {
                                                            AllErrors += "[" + ID.ToString() + "] - " + "Присутні результати ТРК РЕЗ. Операція не можлива!" + Environment.NewLine;
                                                            CountFail++;
                                                        }
                                                    }
                                                }
                                                else if (rs.GetS("STATUS") == "2")
                                                {
                                                    rs.Edit();
                                                    rs.Put("ALLSTAT_ID", ID);
                                                    rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                    rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                    rs.Put("OBJ_ID", ID - 22000000);
                                                    rs.Put("OBJ_TABLE", "MICROWS");
                                                    rs.Put("NOTES", Notes);
                                                    if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                    if (!(((rs.GetT("END_DATE") != IM.NullT)) && (DT != IM.NullT)))
                                                           rs.Update();
                                                    else  {
                                                        AllErrors += "[" + ID.ToString() + "] - " + "Присутні результати ТРК РЕЗ. Операція не можлива!" + Environment.NewLine;
                                                        CountFail++;
                                                    }

                                                }
                                            }
                                            else {
                                                bool isCreateNewp = false;
                                                if (Year_pay!=IM.NullI) {
                                                    if (Year_pay>DateTime.Now.Year) {
                                                        bool isEmpty = true;
                                                        IMRecordset rs_check = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                                                        rs_check.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                                                        rs_check.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID - 22000000);
                                                        rs_check.SetWhere("PLAN_PAY", IMRecordset.Operation.Eq, new DateTime(Year_pay, Month_pay, 1));
                                                        rs_check.Open();
                                                        if (!rs_check.IsEOF())
                                                        {
                                                            isEmpty = false;
                                                        }
                                                        rs_check.Close();
                                                        rs_check.Destroy();

                                                        if (isEmpty)
                                                        {
                                                            rs.AddNew();
                                                            int ID_ = IM.AllocID("XNRFA_MICROWA_MON", 1, -1);
                                                            rs.Put("ID", ID_);
                                                            rs.Put("ALLSTAT_ID", ID);
                                                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                            rs.Put("OBJ_ID", ID - 22000000);
                                                            rs.Put("OBJ_TABLE", "MICROWS");

                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }

                                                            if (DT != IM.NullT) rs.Put("STATUS", "2");
                                                            if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                                            if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                                            if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                                            {
                                                                AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                                                CountFail++;
                                                            }
                                                            else
                                                            {
                                                                rs.Update();
                                                                CountSucessNew++;
                                                                isCreateNewp = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!isCreateNewp) {
                                                    AllErrors += "[" + ID.ToString() + "] - " + " Для даного РЕЗ РРЗ STATUS = '3' операція не може бути виконана!" + Environment.NewLine;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            rs.AddNew();
                                            int ID_ = IM.AllocID("XNRFA_MICROWA_MON", 1, -1);
                                            rs.Put("ID", ID_);
                                            rs.Put("ALLSTAT_ID", ID);
                                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                            rs.Put("OBJ_ID", ID - 22000000);
                                            rs.Put("OBJ_TABLE", "MICROWS");

                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }

                                            if (DT != IM.NullT) rs.Put("STATUS", "2");
                                            if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                            if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                            if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                            {
                                                AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                                CountFail++;
                                            }
                                            else
                                            {
                                                rs.Update();
                                                CountSucessNew++;
                                            }
                                        }
                                        if (rs.IsOpen())
                                            rs.Close();
                                        rs.Destroy();

                                        CountSucessUpdate++;
                                    }
                                }
                                else if (MaxCountCh <= 8) { MaxCountCh++; iu_c++; continue; }
                                else if (MaxCountCh > 8) break;

                                iu_c++;
                            }

                        }
                    }
                    else if (ChbType == TypeFormatFile.XLSX)
                    {
                        ExcelFastNPOI excel = new ExcelFastNPOI();
                        excel.GetWorkBook(NameF);
                        int row_val = 0; int MaxCountCh=0;
                        if (checkBox1.Checked) { row_val = 1; }
                        for (; ; )
                        {
                          
                            string m_ID = excel.GetCellValue(0, row_val).ToString();
                            if (m_ID.Trim().Length > 0)
                            {
                                string decimal_sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                                int ID = IM.NullI;
                                if (int.TryParse(excel.GetCellValue(0, row_val).ToString().Trim(), out ID))
                                {
                                    MaxCountCh = 0;

                                    if ((ID > 22999999) || (ID < 22000000))
                                    {
                                        AllErrors += string.Format("- ID {0} не належить до технології РРЗ", ID.ToString()) + Environment.NewLine;
                                        row_val++;
                                        CountFail++;
                                        continue;
                                    }

                                    int Month_plan = IM.NullI;
                                    if (!string.IsNullOrEmpty(excel.GetCellValue(1, row_val).ToString().Trim()))
                                    {
                                        if (!int.TryParse(excel.GetCellValue(1, row_val).ToString().Trim(), out Month_plan))
                                        {
                                            AllErrors += string.Format("- Номер місяця(PLAN) {0} не коректний [{1}]", excel.GetCellValue(1, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                        else
                                        {
                                            if ((Month_plan < 1) || (Month_plan > 12))
                                            {
                                                AllErrors += string.Format("- Номер місяця(PLAN) {0} не коректний [{1}]", excel.GetCellValue(1, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                                row_val++;
                                                CountFail++;
                                                continue;
                                            }
                                        }
                                    }

                                    int Year_plan = IM.NullI;
                                    if (!string.IsNullOrEmpty(excel.GetCellValue(2, row_val).ToString().Trim()))
                                    {
                                        if (!int.TryParse(excel.GetCellValue(2, row_val).ToString().Trim(), out Year_plan))
                                        {
                                            AllErrors += string.Format("- Номер року(PLAN) {0} не коректний [{1}]", excel.GetCellValue(2, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                        else
                                        {
                                            if (Year_plan.ToString().Length < 4)
                                            {
                                                AllErrors += string.Format("- Номер року(PLAN) {0} не коректний [{1}]", excel.GetCellValue(2, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                                row_val++;
                                                CountFail++;
                                                continue;
                                            }
                                        }
                                    }

                                    //////////////
                                    int Month_pay = IM.NullI;
                                    if (!string.IsNullOrEmpty(excel.GetCellValue(3, row_val).ToString().Trim()))
                                    {
                                        if (!int.TryParse(excel.GetCellValue(3, row_val).ToString().Trim(), out Month_pay))
                                        {
                                            AllErrors += string.Format("- Номер місяця(PAY) {0} не коректний [{1}]", excel.GetCellValue(3, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                        else
                                        {
                                            if ((Month_pay < 1) || (Month_pay > 12))
                                            {
                                                AllErrors += string.Format("- Номер місяця(PAY) {0} не коректний [{1}]", excel.GetCellValue(3, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                                row_val++;
                                                CountFail++;
                                                continue;
                                            }
                                        }
                                    }

                                    int Year_pay = IM.NullI;
                                    if (!string.IsNullOrEmpty(excel.GetCellValue(4, row_val).ToString().Trim()))
                                    {
                                        if (!int.TryParse(excel.GetCellValue(4, row_val).ToString().Trim(), out Year_pay))
                                        {
                                            AllErrors += string.Format("- Номер року(PAY) {0} не коректний [{1}]", excel.GetCellValue(4, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                        else
                                        {
                                            if (Year_pay.ToString().Length < 4)
                                            {
                                                AllErrors += string.Format("- Номер року(PAY) {0} не коректний [{1}]", excel.GetCellValue(4, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                                row_val++;
                                                CountFail++;
                                                continue;
                                            }
                                        }
                                    }


                                    if ((Year_pay != IM.NullI) && (Month_pay != IM.NullI) && (Year_plan != IM.NullI) && (Month_plan != IM.NullI)) {
                                        DateTime DA_pay = (new DateTime(Year_pay, Month_pay, 1));
                                        DateTime DA_plan = (new DateTime(Year_plan, Month_plan, 1));
                                        if (DA_pay < DA_plan)
                                        {
                                            AllErrors += string.Format("- Дата(PAY) {0} не може бути менша за дату(PLAN) {1}  [{2}]", DA_pay.ToShortDateString(), DA_plan.ToShortDateString(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                    }

                                    ////////////

                                    DateTime DT = IM.NullT;
                                    if (!string.IsNullOrEmpty(excel.GetCellValue(5, row_val).ToString().Trim()))
                                    {
                                        if (!DateTime.TryParse(excel.GetCellValue(5, row_val).ToString().Trim(), out DT))
                                        {
                                            AllErrors += string.Format("- Дата проведення ТРК {0} не коректна [{1}]", excel.GetCellValue(5, row_val).ToString().Trim(), excel.GetCellValue(0, row_val).ToString().Trim()) + Environment.NewLine;
                                            row_val++;
                                            CountFail++;
                                            continue;
                                        }
                                    }

                                    string Notes = excel.GetCellValue(6, row_val).ToString();


                                    
                                    IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                                    rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                                    rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID - 22000000);
                                    rs.Open();
                                    if (!rs.IsEOF())
                                    {
                                        if (rs.GetS("STATUS") != "3") {
                                            if (rs.GetS("STATUS") != "2")
                                            {
                                                rs.Edit();
                                                rs.Put("ALLSTAT_ID", ID);
                                                rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                rs.Put("OBJ_ID", ID - 22000000);
                                                rs.Put("OBJ_TABLE", "MICROWS");

                                                if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                                if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                                if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }
                                                if (DT != IM.NullT) rs.Put("STATUS", "2");
                                                if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                                if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                                if (rs.GetT("END_DATE") != IM.NullT)
                                                {
                                                    AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Record (s) containing information about the TRK result! Change of plan is not possible!") + Environment.NewLine;
                                                    CountFail++;
                                                }
                                                if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                                {
                                                    AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                                    CountFail++;
                                                }
                                                if (!(((rs.GetT("END_DATE") != IM.NullT)) && ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))))
                                                    rs.Update();
                                                else  {
                                                    if ((((rs.GetT("END_DATE") != IM.NullT)) && (DT != IM.NullT)))  {
                                                        AllErrors += "[" + ID.ToString() + "] - " + "Присутні результати ТРК РЕЗ. Операція не можлива!" + Environment.NewLine;
                                                        CountFail++;
                                                    }
                                                }
                                            }
                                            else if (rs.GetS("STATUS") == "2")
                                            {
                                                rs.Edit();
                                                rs.Put("ALLSTAT_ID", ID);
                                                rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                rs.Put("OBJ_ID", ID - 22000000);
                                                rs.Put("OBJ_TABLE", "MICROWS");
                                                rs.Put("NOTES", Notes);
                                                if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                if (!(((rs.GetT("END_DATE") != IM.NullT)) && (DT != IM.NullT)))  
                                                    rs.Update();
                                                else  {
                                                    AllErrors += "[" + ID.ToString() + "] - " + "Присутні результати ТРК РЕЗ. Операція не можлива!" + Environment.NewLine;
                                                    CountFail++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            bool isCreateNewp = false;
                                            if (Year_pay != IM.NullI) {
                                                if (Year_pay > DateTime.Now.Year) {
                                                     bool isEmpty = true;
                                                        IMRecordset rs_check = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                                                        rs_check.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                                                        rs_check.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID - 22000000);
                                                        rs_check.SetWhere("PLAN_PAY", IMRecordset.Operation.Eq, new DateTime(Year_pay, Month_pay, 1));
                                                        rs_check.Open();
                                                        if (!rs_check.IsEOF())
                                                        {
                                                            isEmpty = false;
                                                        }
                                                        rs_check.Close();
                                                        rs_check.Destroy();

                                                        if (isEmpty)
                                                        {
                                                            rs.AddNew();
                                                            int ID_ = IM.AllocID("XNRFA_MICROWA_MON", 1, -1);
                                                            rs.Put("ID", ID_);
                                                            rs.Put("ALLSTAT_ID", ID);
                                                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                            rs.Put("OBJ_ID", ID - 22000000);
                                                            rs.Put("OBJ_TABLE", "MICROWS");

                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                                            if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }

                                                            if (DT != IM.NullT) rs.Put("STATUS", "2");
                                                            if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                                            if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                                            if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                                            {
                                                                AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                                                CountFail++;
                                                            }
                                                            else
                                                            {
                                                                rs.Update();
                                                                isCreateNewp = true;
                                                                CountSucessNew++;
                                                            }
                                                        }
                                                }
                                            }
                                            if (!isCreateNewp) {
                                                AllErrors += "[" + ID.ToString() + "] - " + " Для даного РЕЗ РРЗ STATUS = '3' операція не може бути виконана!" + Environment.NewLine;
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        rs.AddNew();
                                        int ID_ = IM.AllocID("XNRFA_MICROWA_MON", 1, -1);
                                        rs.Put("ID", ID_);
                                        rs.Put("ALLSTAT_ID", ID);
                                        rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                        rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                        rs.Put("OBJ_ID", ID - 22000000);
                                        rs.Put("OBJ_TABLE", "MICROWS");
                                        if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_pay != IM.NullI) && (Year_pay != IM.NullI))) { rs.Put("PLAN_PAY", new DateTime(Year_pay, Month_pay, 1)); }
                                        if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Month_plan != IM.NullI) && (Year_plan != IM.NullI))) { rs.Put("PLAN_NEW", new DateTime(Year_plan, Month_plan, 1)); }
                                        if ((!checkBox2.Checked) || ((checkBox2.Checked) && (DT != IM.NullT))) { rs.Put("END_DATE", DT); }
                                        if ((!checkBox2.Checked) || ((checkBox2.Checked) && (Notes != ""))) { rs.Put("NOTES", Notes); }
                                        if (DT != IM.NullT) rs.Put("STATUS", "2");
                                        if ((!checkBox2.Checked) && (DT == IM.NullT) && (Month_plan == IM.NullI)) rs.Put("STATUS", "");
                                        if ((Month_plan != IM.NullI) && (DT == IM.NullT)) rs.Put("STATUS", "1");

                                        if ((rs.GetT("PLAN_NEW") == IM.NullT) && (DT != IM.NullT))
                                        {
                                            AllErrors += "[" + ID.ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                                            CountFail++;
                                        }
                                        else
                                        {
                                            rs.Update();
                                            CountSucessNew++;
                                        }
                                    }
                                    if (rs.IsOpen())
                                        rs.Close();
                                    rs.Destroy();

                                    CountSucessUpdate++;
                                }

                            }
                            else if (MaxCountCh <= 8) { MaxCountCh++; row_val++; continue; }
                            else if (MaxCountCh > 8) break;
                            row_val++;
                          
                        }
                        excel.Close();
                        excel.Dispose();
                    }
                    
                }
                else { MessageBox.Show("Файл не існує!"); }

                if (AllErrors.Length > 0)
                {
                    using (IMLogFile log = new IMLogFile())
                    {
                        log.Create("ResultWorkImportData");
                        string StrWarning = "Під час імпорту даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrors;
                        log.Warning(StrWarning);
                        log.Display("Результати імпорту");
                    }

                }
                MessageBox.Show(string.Format("Додано новых записів - {0}.{1}Оновлено записів - {2}.{3}Кількість записів з помилками - {4}",CountSucessNew, Environment.NewLine, CountSucessUpdate,Environment.NewLine, CountFail));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Attach Files(*.CSV;*.XLSX)|*.CSV;*XLSX";
                    openFileDialog1.FileName = "";
                    DialogResult DA = openFileDialog1.ShowDialog();
                    if (DA == System.Windows.Forms.DialogResult.OK)
                    {
                        NameF = openFileDialog1.FileName;
                        textBox1.Text = NameF;
                        string ext = System.IO.Path.GetExtension(NameF);
                        if (ext.ToLower().Contains("csv")) ChbType = TypeFormatFile.CSV;
                        if (ext.ToLower().Contains("xlsx")) ChbType = TypeFormatFile.XLSX;

                    }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormLoadFile_Shown(object sender, EventArgs e)
        {
            CLocaliz.TxT(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if ((!checkBox2.Checked) && (StatusCheckPress)) StatusCheckPress = false;
            if (!StatusCheckPress) 
            {
                StatusCheckPress = true;
                if (MessageBox.Show("Увага!" + Environment.NewLine + "Це може призвести до видалення даних." + Environment.NewLine + "Ви бажаєте продовжити?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    checkBox2.Checked = false;
                }
                else { checkBox2.Checked = true; StatusCheckPress = false; }
                
            }
        }
    }


    public enum TypeFormatFile
    {
        CSV,
        XLSX
    }

}
