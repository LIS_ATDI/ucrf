﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormRRZ_Contain : Form
    {
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }

        public FormRRZ_Contain(IMQueryMenuNode.Context context, ComboBox cmb)
        {
                InitializeComponent();
                comboBox1.Items.Clear();
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                for (int i = 0; i < cmb.Items.Count;i++)
                    comboBox1.Items.Add(cmb.Items[i]);
                if (comboBox1.Items.Count > 0) {
                        comboBox1.SelectedIndex = 0;
                }
                lstContext = context.DataList;
                NameTable = context.TableName;
                Id_Table = context.TableId;
           
        }


    
        private DateTime ConcatDate()
        {
            DateTime DT = IM.NullT;
            if (((cbItemContain)comboBox1.Items[comboBox1.SelectedIndex]).Month != IM.NullI)
            {
                DT = new DateTime(((cbItemContain)comboBox1.Items[comboBox1.SelectedIndex]).Year, ((cbItemContain)comboBox1.Items[comboBox1.SelectedIndex]).Month, 1);
            }
            return DT;
        }
    

        private void button_OK_Click(object sender, EventArgs e)
        {
            int CountUpdated = 0;
            string AllErrors = "";
            bool isSuccess = false;
            DA = System.Windows.Forms.DialogResult.OK;
            try  {
                int Cnt = 0;
                IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID,TABLE_ID,STANDARD");
                rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);
                for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext()) {
                    IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext()) {
                        if (rs.GetS("STATUS") == "2") {
                            if (ConcatDate() != IM.NullT) {
                                DateTime DF_PLANNED = ConcatDate();
                                DateTime DF_FACT = rs.GetT("PLAN_PAY");
                                if (DF_FACT==IM.NullT) {
                                    rs.Edit();
                                    rs.Put("PLAN_PAY", ConcatDate());
                                    rs.Update();
                                    isSuccess = true;
                                }
                                else if (DF_FACT.Year == DF_PLANNED.Year) {
                                    rs.Edit();
                                    rs.Put("PLAN_PAY", ConcatDate());
                                    rs.Update();
                                    isSuccess = true;
                                }
                                
                            }
                        }
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                    CountUpdated++;
                }

              
                string Mess = "";
                if (isSuccess)  {
                    Mess = "Запис(и) оновлено успішно!";
                    if (CountUpdated > 0)
                        Mess += Environment.NewLine + string.Format("Кількість записів : {0}", CountUpdated);
                    MessageBox.Show(Mess);
                }
                else  {
                    MessageBox.Show("Запис не оновлено. Перевірте статус відповідного запису таблиці XNRFA_MICROWA_MON.");
                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void FormRRZ_Plan_Shown(object sender, EventArgs e)
        {
          
        }
    }

   
}
