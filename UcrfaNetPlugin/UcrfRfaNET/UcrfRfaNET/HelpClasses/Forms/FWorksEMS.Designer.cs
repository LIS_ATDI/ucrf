﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FWorksEMS
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FWorksEMS));
          this.grWorks = new System.Windows.Forms.DataGridView();
          this.btnCalculateWork = new System.Windows.Forms.Button();
          ((System.ComponentModel.ISupportInitialize)(this.grWorks)).BeginInit();
          this.SuspendLayout();
          // 
          // grWorks
          // 
          this.grWorks.AllowUserToAddRows = false;
          this.grWorks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.grWorks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.grWorks.Location = new System.Drawing.Point(12, 12);
          this.grWorks.Name = "grWorks";
          this.grWorks.RowHeadersVisible = false;
          this.grWorks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
          this.grWorks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.grWorks.Size = new System.Drawing.Size(584, 277);
          this.grWorks.TabIndex = 0;
          // 
          // btnCalculateWork
          // 
          this.btnCalculateWork.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnCalculateWork.Location = new System.Drawing.Point(427, 301);
          this.btnCalculateWork.Name = "btnCalculateWork";
          this.btnCalculateWork.Size = new System.Drawing.Size(169, 23);
          this.btnCalculateWork.TabIndex = 1;
          this.btnCalculateWork.Text = "Calculate works count";
          this.btnCalculateWork.UseVisualStyleBackColor = true;
          this.btnCalculateWork.Click += new System.EventHandler(this.btnCalculateWork_Click);
          // 
          // FWorksEMS
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(612, 336);
          this.Controls.Add(this.btnCalculateWork);
          this.Controls.Add(this.grWorks);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "FWorksEMS";
          this.Text = "Роботи (послуги) з розрахунку ЕМС";
          this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FWorksEMS_FormClosed);
          ((System.ComponentModel.ISupportInitialize)(this.grWorks)).EndInit();
          this.ResumeLayout(false);

      }

      #endregion

      public System.Windows.Forms.DataGridView grWorks;
      private System.Windows.Forms.Button btnCalculateWork;
   }
}