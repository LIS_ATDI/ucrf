﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FBillMemoURZP
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
          this.pnChbPtk = new System.Windows.Forms.Panel();
          this.chbPtk = new System.Windows.Forms.CheckBox();
          this.pnTblPtk = new System.Windows.Forms.Panel();
          this.btPtk = new System.Windows.Forms.Button();
          this.pnParamRez = new System.Windows.Forms.Panel();
          this.chbParamRez = new System.Windows.Forms.CheckBox();
          this.pnTblParamRez = new System.Windows.Forms.Panel();
          this.pnComiss = new System.Windows.Forms.Panel();
          this.chbComiss = new System.Windows.Forms.CheckBox();
          this.pnTblComiss = new System.Windows.Forms.Panel();
          this.pnOther = new System.Windows.Forms.Panel();
          this.chbOther = new System.Windows.Forms.CheckBox();
          this.pnTblOther = new System.Windows.Forms.Panel();
          this.pnOdo = new System.Windows.Forms.Panel();
          this.tbOdo = new System.Windows.Forms.TextBox();
          this.chbOdo = new System.Windows.Forms.CheckBox();
          this.label1 = new System.Windows.Forms.Label();
          this.btPrint = new System.Windows.Forms.Button();
          this.btDRV = new System.Windows.Forms.Button();
          this.btExit = new System.Windows.Forms.Button();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.panel9 = new System.Windows.Forms.Panel();
          this.btDelete = new System.Windows.Forms.Button();
          this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
          this.panel12 = new System.Windows.Forms.Panel();
          this.tbOwner = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.flowLayoutPanel1.SuspendLayout();
          this.pnChbPtk.SuspendLayout();
          this.pnTblPtk.SuspendLayout();
          this.pnParamRez.SuspendLayout();
          this.pnComiss.SuspendLayout();
          this.pnOther.SuspendLayout();
          this.pnOdo.SuspendLayout();
          this.groupBox1.SuspendLayout();
          this.panel9.SuspendLayout();
          this.flowLayoutPanel2.SuspendLayout();
          this.panel12.SuspendLayout();
          this.SuspendLayout();
          // 
          // flowLayoutPanel1
          // 
          this.flowLayoutPanel1.AutoSize = true;
          this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.flowLayoutPanel1.Controls.Add(this.pnChbPtk);
          this.flowLayoutPanel1.Controls.Add(this.pnTblPtk);
          this.flowLayoutPanel1.Controls.Add(this.pnParamRez);
          this.flowLayoutPanel1.Controls.Add(this.pnTblParamRez);
          this.flowLayoutPanel1.Controls.Add(this.pnComiss);
          this.flowLayoutPanel1.Controls.Add(this.pnTblComiss);
          this.flowLayoutPanel1.Controls.Add(this.pnOther);
          this.flowLayoutPanel1.Controls.Add(this.pnTblOther);
          this.flowLayoutPanel1.Controls.Add(this.pnOdo);
          this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
          this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
          this.flowLayoutPanel1.Name = "flowLayoutPanel1";
          this.flowLayoutPanel1.Size = new System.Drawing.Size(476, 305);
          this.flowLayoutPanel1.TabIndex = 0;
          // 
          // pnChbPtk
          // 
          this.pnChbPtk.Controls.Add(this.chbPtk);
          this.pnChbPtk.Location = new System.Drawing.Point(3, 3);
          this.pnChbPtk.Name = "pnChbPtk";
          this.pnChbPtk.Size = new System.Drawing.Size(470, 18);
          this.pnChbPtk.TabIndex = 15;
          // 
          // chbPtk
          // 
          this.chbPtk.AutoSize = true;
          this.chbPtk.Location = new System.Drawing.Point(3, 3);
          this.chbPtk.Name = "chbPtk";
          this.chbPtk.Size = new System.Drawing.Size(113, 17);
          this.chbPtk.TabIndex = 1;
          this.chbPtk.Text = "Проведення ПТК";
          this.chbPtk.UseVisualStyleBackColor = true;
          this.chbPtk.CheckedChanged += new System.EventHandler(this.chbPtk_CheckedChanged);
          // 
          // pnTblPtk
          // 
          this.pnTblPtk.Controls.Add(this.btPtk);
          this.pnTblPtk.Location = new System.Drawing.Point(3, 27);
          this.pnTblPtk.Name = "pnTblPtk";
          this.pnTblPtk.Size = new System.Drawing.Size(470, 32);
          this.pnTblPtk.TabIndex = 1;
          this.pnTblPtk.Visible = false;
          // 
          // btPtk
          // 
          this.btPtk.Location = new System.Drawing.Point(3, 3);
          this.btPtk.Name = "btPtk";
          this.btPtk.Size = new System.Drawing.Size(86, 25);
          this.btPtk.TabIndex = 0;
          this.btPtk.Text = "Таблиця";
          this.btPtk.UseVisualStyleBackColor = true;
          this.btPtk.Click += new System.EventHandler(this.btPTK_Click);
          // 
          // pnParamRez
          // 
          this.pnParamRez.Controls.Add(this.chbParamRez);
          this.pnParamRez.Location = new System.Drawing.Point(3, 65);
          this.pnParamRez.Name = "pnParamRez";
          this.pnParamRez.Size = new System.Drawing.Size(470, 19);
          this.pnParamRez.TabIndex = 14;
          // 
          // chbParamRez
          // 
          this.chbParamRez.AutoSize = true;
          this.chbParamRez.Location = new System.Drawing.Point(3, 3);
          this.chbParamRez.Name = "chbParamRez";
          this.chbParamRez.Size = new System.Drawing.Size(267, 17);
          this.chbParamRez.TabIndex = 4;
          this.chbParamRez.Text = "Проведення вимірювань параметрів РЕЗ та ВП";
          this.chbParamRez.UseVisualStyleBackColor = true;
          this.chbParamRez.CheckedChanged += new System.EventHandler(this.chbParamRez_CheckedChanged);
          // 
          // pnTblParamRez
          // 
          this.pnTblParamRez.Location = new System.Drawing.Point(3, 90);
          this.pnTblParamRez.Name = "pnTblParamRez";
          this.pnTblParamRez.Size = new System.Drawing.Size(470, 39);
          this.pnTblParamRez.TabIndex = 5;
          this.pnTblParamRez.Visible = false;
          // 
          // pnComiss
          // 
          this.pnComiss.Controls.Add(this.chbComiss);
          this.pnComiss.Location = new System.Drawing.Point(3, 135);
          this.pnComiss.Name = "pnComiss";
          this.pnComiss.Size = new System.Drawing.Size(470, 19);
          this.pnComiss.TabIndex = 13;
          // 
          // chbComiss
          // 
          this.chbComiss.AutoSize = true;
          this.chbComiss.Location = new System.Drawing.Point(3, 3);
          this.chbComiss.Name = "chbComiss";
          this.chbComiss.Size = new System.Drawing.Size(143, 17);
          this.chbComiss.TabIndex = 6;
          this.chbComiss.Text = "Участь у роботі комісій";
          this.chbComiss.UseVisualStyleBackColor = true;
          this.chbComiss.CheckedChanged += new System.EventHandler(this.chbComiss_CheckedChanged);
          // 
          // pnTblComiss
          // 
          this.pnTblComiss.Location = new System.Drawing.Point(3, 160);
          this.pnTblComiss.Name = "pnTblComiss";
          this.pnTblComiss.Size = new System.Drawing.Size(470, 39);
          this.pnTblComiss.TabIndex = 7;
          this.pnTblComiss.Visible = false;
          // 
          // pnOther
          // 
          this.pnOther.Controls.Add(this.chbOther);
          this.pnOther.Location = new System.Drawing.Point(3, 205);
          this.pnOther.Name = "pnOther";
          this.pnOther.Size = new System.Drawing.Size(470, 19);
          this.pnOther.TabIndex = 12;
          // 
          // chbOther
          // 
          this.chbOther.AutoSize = true;
          this.chbOther.Location = new System.Drawing.Point(3, 3);
          this.chbOther.Name = "chbOther";
          this.chbOther.Size = new System.Drawing.Size(49, 17);
          this.chbOther.TabIndex = 8;
          this.chbOther.Text = "Інше";
          this.chbOther.UseVisualStyleBackColor = true;
          this.chbOther.CheckedChanged += new System.EventHandler(this.chbOther_CheckedChanged);
          // 
          // pnTblOther
          // 
          this.pnTblOther.Location = new System.Drawing.Point(3, 230);
          this.pnTblOther.Name = "pnTblOther";
          this.pnTblOther.Size = new System.Drawing.Size(470, 39);
          this.pnTblOther.TabIndex = 9;
          this.pnTblOther.Visible = false;
          // 
          // pnOdo
          // 
          this.pnOdo.Controls.Add(this.tbOdo);
          this.pnOdo.Controls.Add(this.chbOdo);
          this.pnOdo.Location = new System.Drawing.Point(3, 275);
          this.pnOdo.Name = "pnOdo";
          this.pnOdo.Size = new System.Drawing.Size(470, 27);
          this.pnOdo.TabIndex = 11;
          // 
          // tbOdo
          // 
          this.tbOdo.Enabled = false;
          this.tbOdo.Location = new System.Drawing.Point(171, 3);
          this.tbOdo.Name = "tbOdo";
          this.tbOdo.Size = new System.Drawing.Size(97, 20);
          this.tbOdo.TabIndex = 11;
          // 
          // chbOdo
          // 
          this.chbOdo.AutoSize = true;
          this.chbOdo.Location = new System.Drawing.Point(3, 3);
          this.chbOdo.Name = "chbOdo";
          this.chbOdo.Size = new System.Drawing.Size(162, 17);
          this.chbOdo.TabIndex = 10;
          this.chbOdo.Text = "Пробіг автотранспорту, км";
          this.chbOdo.UseVisualStyleBackColor = true;
          this.chbOdo.CheckedChanged += new System.EventHandler(this.chbOdo_CheckedChanged);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Dock = System.Windows.Forms.DockStyle.Top;
          this.label1.Location = new System.Drawing.Point(3, 47);
          this.label1.Name = "label1";
          this.label1.Padding = new System.Windows.Forms.Padding(2, 10, 2, 10);
          this.label1.Size = new System.Drawing.Size(482, 33);
          this.label1.TabIndex = 0;
          this.label1.Text = "Спочатку виберіть заяви у формі Пакет заяв. Потім виберіть види робіт (послуг).";
          this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // btPrint
          // 
          this.btPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.btPrint.Location = new System.Drawing.Point(3, 8);
          this.btPrint.Name = "btPrint";
          this.btPrint.Size = new System.Drawing.Size(115, 26);
          this.btPrint.TabIndex = 0;
          this.btPrint.Text = "Друк";
          this.btPrint.UseVisualStyleBackColor = true;
          this.btPrint.Click += new System.EventHandler(this.Print_Click);
          // 
          // btDRV
          // 
          this.btDRV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btDRV.Location = new System.Drawing.Point(243, 8);
          this.btDRV.Name = "btDRV";
          this.btDRV.Size = new System.Drawing.Size(115, 26);
          this.btDRV.TabIndex = 1;
          this.btDRV.Text = "-> ДРВ";
          this.btDRV.UseVisualStyleBackColor = true;
          this.btDRV.Click += new System.EventHandler(this.ToDRV_Click);
          // 
          // btExit
          // 
          this.btExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btExit.Location = new System.Drawing.Point(364, 8);
          this.btExit.Name = "btExit";
          this.btExit.Size = new System.Drawing.Size(115, 26);
          this.btExit.TabIndex = 2;
          this.btExit.Text = "Вихід";
          this.btExit.UseVisualStyleBackColor = true;
          // 
          // groupBox1
          // 
          this.groupBox1.AutoSize = true;
          this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.groupBox1.Controls.Add(this.flowLayoutPanel1);
          this.groupBox1.Location = new System.Drawing.Point(3, 83);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(482, 324);
          this.groupBox1.TabIndex = 1;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Види робіт (послуг) УДЦР";
          // 
          // panel9
          // 
          this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.panel9.AutoSize = true;
          this.panel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.panel9.Controls.Add(this.btDelete);
          this.panel9.Controls.Add(this.btPrint);
          this.panel9.Controls.Add(this.btDRV);
          this.panel9.Controls.Add(this.btExit);
          this.panel9.Location = new System.Drawing.Point(3, 413);
          this.panel9.Name = "panel9";
          this.panel9.Size = new System.Drawing.Size(482, 43);
          this.panel9.TabIndex = 2;
          // 
          // btDelete
          // 
          this.btDelete.Location = new System.Drawing.Point(124, 8);
          this.btDelete.Name = "btDelete";
          this.btDelete.Size = new System.Drawing.Size(103, 26);
          this.btDelete.TabIndex = 4;
          this.btDelete.Text = "Видалити";
          this.btDelete.UseVisualStyleBackColor = true;
          this.btDelete.Click += new System.EventHandler(this.Delete_Click);
          // 
          // flowLayoutPanel2
          // 
          this.flowLayoutPanel2.AutoSize = true;
          this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.flowLayoutPanel2.Controls.Add(this.panel12);
          this.flowLayoutPanel2.Controls.Add(this.label1);
          this.flowLayoutPanel2.Controls.Add(this.groupBox1);
          this.flowLayoutPanel2.Controls.Add(this.panel9);
          this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
          this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
          this.flowLayoutPanel2.Name = "flowLayoutPanel2";
          this.flowLayoutPanel2.Size = new System.Drawing.Size(489, 460);
          this.flowLayoutPanel2.TabIndex = 0;
          // 
          // panel12
          // 
          this.panel12.Controls.Add(this.tbOwner);
          this.panel12.Controls.Add(this.label2);
          this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
          this.panel12.Location = new System.Drawing.Point(3, 3);
          this.panel12.Name = "panel12";
          this.panel12.Size = new System.Drawing.Size(482, 41);
          this.panel12.TabIndex = 5;
          // 
          // tbOwner
          // 
          this.tbOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbOwner.Location = new System.Drawing.Point(82, 10);
          this.tbOwner.Name = "tbOwner";
          this.tbOwner.Size = new System.Drawing.Size(388, 20);
          this.tbOwner.TabIndex = 5;
          this.tbOwner.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbOwner_KeyDown);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
          this.label2.Location = new System.Drawing.Point(11, 13);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(65, 13);
          this.label2.TabIndex = 4;
          this.label2.Text = "Контрагент";
          // 
          // FBillMemoURZP
          // 
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.AutoSize = true;
          this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.ClientSize = new System.Drawing.Size(489, 460);
          this.Controls.Add(this.flowLayoutPanel2);
          this.Name = "FBillMemoURZP";
          this.Text = "Створення службової записки на виставлення рахунку";
          this.flowLayoutPanel1.ResumeLayout(false);
          this.pnChbPtk.ResumeLayout(false);
          this.pnChbPtk.PerformLayout();
          this.pnTblPtk.ResumeLayout(false);
          this.pnParamRez.ResumeLayout(false);
          this.pnParamRez.PerformLayout();
          this.pnComiss.ResumeLayout(false);
          this.pnComiss.PerformLayout();
          this.pnOther.ResumeLayout(false);
          this.pnOther.PerformLayout();
          this.pnOdo.ResumeLayout(false);
          this.pnOdo.PerformLayout();
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.panel9.ResumeLayout(false);
          this.flowLayoutPanel2.ResumeLayout(false);
          this.flowLayoutPanel2.PerformLayout();
          this.panel12.ResumeLayout(false);
          this.panel12.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
      private System.Windows.Forms.Panel pnTblPtk;
      private System.Windows.Forms.CheckBox chbParamRez;
      private System.Windows.Forms.CheckBox chbComiss;
      private System.Windows.Forms.CheckBox chbOther;
      private System.Windows.Forms.CheckBox chbOdo;
      private System.Windows.Forms.Panel pnTblParamRez;
      private System.Windows.Forms.Panel pnTblComiss;
      private System.Windows.Forms.Panel pnTblOther;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button btPrint;
      private System.Windows.Forms.Button btDRV;
      private System.Windows.Forms.Button btExit;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Panel panel9;
      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
      private System.Windows.Forms.Button btDelete;
      private System.Windows.Forms.Button btPtk;
      private System.Windows.Forms.Panel pnOdo;
      private System.Windows.Forms.TextBox tbOdo;
      private System.Windows.Forms.Panel pnParamRez;
      private System.Windows.Forms.Panel pnComiss;
      private System.Windows.Forms.Panel pnOther;
      private System.Windows.Forms.Panel pnChbPtk;
      private System.Windows.Forms.CheckBox chbPtk;
      private System.Windows.Forms.Panel panel12;
      protected System.Windows.Forms.TextBox tbOwner;
      protected System.Windows.Forms.Label label2;
   }
}