﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FShowLogs
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.buttonOK = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.tbType = new System.Windows.Forms.TextBox();
         this.tbWhat = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.tbCreatedBy = new System.Windows.Forms.TextBox();
         this.tbMessage = new System.Windows.Forms.TextBox();
         this.Message = new System.Windows.Forms.GroupBox();
         this.Message.SuspendLayout();
         this.SuspendLayout();
         // 
         // buttonOK
         // 
         this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
         this.buttonOK.Location = new System.Drawing.Point(588, 357);
         this.buttonOK.Name = "buttonOK";
         this.buttonOK.Size = new System.Drawing.Size(75, 23);
         this.buttonOK.TabIndex = 0;
         this.buttonOK.Text = "OK";
         this.buttonOK.UseVisualStyleBackColor = true;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 15);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(31, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Type";
         // 
         // tbType
         // 
         this.tbType.Location = new System.Drawing.Point(99, 12);
         this.tbType.Name = "tbType";
         this.tbType.ReadOnly = true;
         this.tbType.ShortcutsEnabled = false;
         this.tbType.Size = new System.Drawing.Size(148, 20);
         this.tbType.TabIndex = 3;
         // 
         // tbWhat
         // 
         this.tbWhat.Location = new System.Drawing.Point(99, 38);
         this.tbWhat.Name = "tbWhat";
         this.tbWhat.ReadOnly = true;
         this.tbWhat.ShortcutsEnabled = false;
         this.tbWhat.Size = new System.Drawing.Size(148, 20);
         this.tbWhat.TabIndex = 5;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 41);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(33, 13);
         this.label2.TabIndex = 4;
         this.label2.Text = "What";
         // 
         // tbCreatedBy
         // 
         this.tbCreatedBy.Location = new System.Drawing.Point(275, 12);
         this.tbCreatedBy.Multiline = true;
         this.tbCreatedBy.Name = "tbCreatedBy";
         this.tbCreatedBy.ReadOnly = true;
         this.tbCreatedBy.ShortcutsEnabled = false;
         this.tbCreatedBy.Size = new System.Drawing.Size(388, 46);
         this.tbCreatedBy.TabIndex = 6;
         // 
         // tbMessage
         // 
         this.tbMessage.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tbMessage.Location = new System.Drawing.Point(3, 16);
         this.tbMessage.Multiline = true;
         this.tbMessage.Name = "tbMessage";
         this.tbMessage.ReadOnly = true;
         this.tbMessage.Size = new System.Drawing.Size(642, 238);
         this.tbMessage.TabIndex = 7;
         // 
         // Message
         // 
         this.Message.Controls.Add(this.tbMessage);
         this.Message.Location = new System.Drawing.Point(15, 73);
         this.Message.Name = "Message";
         this.Message.Size = new System.Drawing.Size(648, 257);
         this.Message.TabIndex = 8;
         this.Message.TabStop = false;
         this.Message.Text = "Message";
         // 
         // FShowLogs
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(675, 392);
         this.Controls.Add(this.Message);
         this.Controls.Add(this.tbCreatedBy);
         this.Controls.Add(this.tbWhat);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.tbType);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.buttonOK);
         this.Name = "FShowLogs";
         this.Text = "Log message";
         this.Message.ResumeLayout(false);
         this.Message.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbType;
      private System.Windows.Forms.TextBox tbWhat;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox tbCreatedBy;
      private System.Windows.Forms.TextBox tbMessage;
      private System.Windows.Forms.GroupBox Message;
   }
}