﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormRRZ_Plan : Form
    {
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }

        public FormRRZ_Plan(IMQueryMenuNode.Context context, ComboBox cmb)
        {
                InitializeComponent();
                comboBox1.Items.Clear();
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBox_Year.Items.Clear();
                for (int i = 0; i < 10; i++) 
                    comboBox_Year.Items.Add((DateTime.Now.Year+i).ToString());
                comboBox_Year.SelectedIndex = 0;
                for (int i = 0; i < cmb.Items.Count;i++)
                    comboBox1.Items.Add(cmb.Items[i]);
                int Month = DateTime.Now.Month;
                if (comboBox1.Items.Count > 0)
                {
                    if (Month < 12)
                    {
                        comboBox1.SelectedIndex = Month;
                    }
                    else if (Month == 12)
                    {
                        comboBox1.SelectedIndex = 0;
                    }
                }
                lstContext = context.DataList;
                NameTable = context.TableName;
                Id_Table = context.TableId;
           
        }


        public static bool FillStart(string NameTable, IMDBList lstContext, ref ComboBox comboBox1, int Year)
        {
            try
            {
                bool isViewMess = false;
                string AllErrors = "";
                bool isContainRecord = false;
                bool isContainRecordTRKRes = false;
                int CntAll = 0;
                IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadOnly);
                rs_allstations.Select("ID,TABLE_ID,STANDARD");
                rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);
                for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext())
                {

                    IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {

                        for(int r=0;r<comboBox1.Items.Count;r++)
                        {
                            if (rs.GetT("PLAN_NEW") != IM.NullT)
                            {
                                if (((ConcatDate(comboBox1.Items[r], Year) == rs.GetT("PLAN_NEW")) || (ConcatDate(comboBox1.Items[r], Year - 1) == rs.GetT("PLAN_NEW")) || (ConcatDate(comboBox1.Items[r], Year + 1) == rs.GetT("PLAN_NEW"))) && (rs.GetT("PLAN_NEW") != IM.NullT))
                                {
                                    isContainRecord = true;
                                    comboBox1.SelectedIndex = r;
                                }


                            if (((ConcatDate(comboBox1.Items[r], Year) == rs.GetT("PLAN_NEW")) || (ConcatDate(comboBox1.Items[r], Year - 1) == rs.GetT("PLAN_NEW")) || (ConcatDate(comboBox1.Items[r], Year + 1) == rs.GetT("PLAN_NEW"))) && (rs.GetT("END_DATE") != IM.NullT))
                            {
                                isContainRecordTRKRes = true;
                               // AllErrors += "[" + rs_allstations.GetI("ID").ToString() + "] - " + CLocaliz.TxT("Record (s) containing information about the TRK result! Change of plan is not possible!") + Environment.NewLine;
                                comboBox1.SelectedIndex = r;

                                if (!isViewMess) { MessageBox.Show(CLocaliz.TxT("Record (s) contain information about planning!")); isViewMess = true; }
                                /*
                                if (MessageBox.Show("Запис " + rs_allstations.GetI("ID").ToString() + " містить інформацію про результат ТРК!  " + Environment.NewLine + "Ви дійсно бажаєте змінити планування ТРК?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    rs.Edit();
                                    rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                    rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                    rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                    rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                    rs.Put("OBJ_TABLE", "MICROWS");
                                    // rs.Put("PLAN", ((cbItem)comboBox1.Items[comboBox1.SelectedIndex]).Key);
                                    if (((cbItem)comboBox1.Items[comboBox1.SelectedIndex]).Key != "") { rs.Put("STATUS", "1"); } else { rs.Put("STATUS", ""); }
                                    rs.Update();
                                }
                                 */ 

                            }
                            }
                            
                        }
                        CntAll++;
                        break;
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }
                rs_allstations.Close();
                rs_allstations.Destroy();
               

                if (!isContainRecordTRKRes) return false;
                /*
                if ((AllErrors.Length > 0) && (CntAll>1))
                {
                    using (IMLogFile log = new IMLogFile())
                    {
                        log.Create("ResultWorkImportData");
                        string StrWarning = "Під час внесення даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrors;
                        log.Warning(StrWarning);
                        log.Display("Результати імпорту");
                    }
                    return false;
                }
                

                if ((AllErrors.Length > 0) && (CntAll == 1)) {
                    MessageBox.Show(CLocaliz.TxT("Record (s) containing information about the TRK result! Change of plan is not possible!"));
                    return false;
                }
                 */ 
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            return true;
        }
     
        private DateTime ConcatDate()
        {
            DateTime DT = IM.NullT;
            int Year;
            if (int.TryParse(comboBox_Year.Text, out Year)) {
                if (((cbItem)comboBox1.Items[comboBox1.SelectedIndex]).Month != IM.NullI) {
                    DT = new DateTime(Year, ((cbItem)comboBox1.Items[comboBox1.SelectedIndex]).Month, 1);
                }
            }
            return DT;
        }

        public static DateTime ConcatDate(object obj, int Year)
        {
            DateTime DT = IM.NullT;
            if (Year!=IM.NullI) {
                if (((cbItem)obj).Month != IM.NullI) {
                    DT = new DateTime(Year, ((cbItem)obj).Month, 1);
                }
            }
            return DT;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            int CountUpdated = 0;
            string AllErrors = "";
            bool isSuccess = false;
            bool isShowConfirmStatus = false;
            bool isShowConfirmNewPeriod = false;
            DA = System.Windows.Forms.DialogResult.OK;
            try {
                int Cnt = 0;
                bool CreateRecordNewPeriod = false;
                IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID,TABLE_ID,STANDARD");
                rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);
                for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext()) {

                    List<int> L_DatePlan = new List<int>();
                    IMRecordset rs_fnd = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadOnly);
                    rs_fnd.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs_fnd.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                    for (rs_fnd.Open(); !rs_fnd.IsEOF(); rs_fnd.MoveNext())
                    {
                        if (rs_fnd.GetT("PLAN_NEW")!=IM.NullT)
                            L_DatePlan.Add(rs_fnd.GetT("PLAN_NEW").Year);
                    }
                    rs_fnd.Close();
                    rs_fnd.Destroy();



                    IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        //if (rs.GetT("END_DATE") != IM.NullT)
                        //{
                            //MessageBox.Show("Присутні результати ТРК РЕЗ. Операція не можлива!");
                            //isShowConfirmStatus = true;
                            //break;
                        //}
                        //else
                        //{

                            string Plan = "";
                            if (rs.GetT("PLAN_NEW") != IM.NullT)
                                Plan = rs.GetT("PLAN_NEW").Month.ToString().PadLeft(2, '0');

                            ////
                            if (rs.GetT("PLAN_NEW") != IM.NullT)
                            {
                                DateTime DF_PLANNED = ConcatDate();
                                if (DF_PLANNED != IM.NullT)
                                {
                                    if ((!L_DatePlan.Contains(DF_PLANNED.Year)) && (!CreateRecordNewPeriod))
                                    {
                                        IMRecordset rs_new_period = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                                        rs_new_period.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                                        rs_new_period.Open();
                                        rs_new_period.AddNew();
                                        rs_new_period.Put("ID", IM.AllocID("XNRFA_MICROWA_MON", 1, -1));
                                        rs_new_period.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                        rs_new_period.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                        rs_new_period.Put("LAST_EDIT_DATE", DateTime.Now);
                                        rs_new_period.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                        rs_new_period.Put("OBJ_TABLE", "MICROWS");
                                        rs_new_period.Put("PLAN_NEW", ConcatDate());
                                        rs_new_period.Put("PLAN_PAY", ConcatDate());
                                        if (ConcatDate() != IM.NullT) { rs_new_period.Put("STATUS", "1"); } else { rs_new_period.Put("STATUS", ""); }
                                        rs_new_period.Update();
                                        isSuccess = true;
                                        CreateRecordNewPeriod = true;

                                        if (rs_new_period.IsOpen())
                                            rs_new_period.Close();
                                        rs_new_period.Destroy();
                                    }
                                }
                            }
                            ////

                            if (!CreateRecordNewPeriod)
                            {
                                if (string.IsNullOrEmpty(Plan))
                                {
                                    DateTime DF_PLANNED = ConcatDate();
                                    DateTime DF_FACT = rs.GetT("PLAN_NEW");
                                    if (DF_PLANNED != IM.NullT)
                                    {
                                        if (DF_FACT.Year == DF_PLANNED.Year)
                                        {
                                            if (rs.GetT("END_DATE") != IM.NullT)
                                            {
                                                MessageBox.Show("Присутні результати ТРК РЕЗ. Операція не можлива!");
                                                isShowConfirmStatus = true;
                                                break;
                                            }
                                            else
                                            {
                                                if (rs.GetS("STATUS") != "3")
                                                {
                                                    if (rs.GetS("STATUS") != "2")
                                                    {
                                                        rs.Edit();
                                                        rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                        rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                        rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                        rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                        rs.Put("OBJ_TABLE", "MICROWS");
                                                        rs.Put("PLAN_NEW", ConcatDate());
                                                        rs.Put("PLAN_PAY", ConcatDate());
                                                        if (ConcatDate() != IM.NullT) { rs.Put("STATUS", "1"); } else { rs.Put("STATUS", ""); }
                                                        rs.Update();
                                                        isSuccess = true;
                                                    }
                                                    else
                                                    {
                                                        rs.Edit();
                                                        rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                        rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                        rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                        rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                        rs.Put("OBJ_TABLE", "MICROWS");
                                                        rs.Put("PLAN_PAY", ConcatDate());
                                                        rs.Update();
                                                        isSuccess = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (!isShowConfirmStatus)
                                                        MessageBox.Show("Для значення Status = '3' операція не може бути виконана!");
                                                    isShowConfirmStatus = true;
                                                }
                                            }

                                        }
                                    }

                                }
                                else
                                {
                                    DateTime DF_PLANNED = ConcatDate();
                                    DateTime DF_FACT = rs.GetT("PLAN_NEW");
                                    if (DF_PLANNED != IM.NullT)
                                    {
                                        if (DF_FACT.Year == DF_PLANNED.Year)
                                        {
                                            if (MessageBox.Show("Запис " + rs_allstations.GetI("ID").ToString() + " містить інформацію про результат ТРК!  " + Environment.NewLine + "Ви дійсно бажаєте змінити планування ТРК?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                if (rs.GetT("END_DATE") != IM.NullT)
                                                {
                                                    MessageBox.Show("Присутні результати ТРК РЕЗ. Операція не можлива!");
                                                    isShowConfirmStatus = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    if (rs.GetS("STATUS") != "3")
                                                    {
                                                        if (rs.GetS("STATUS") != "2")
                                                        {
                                                            rs.Edit();
                                                            rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                            rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                            rs.Put("OBJ_TABLE", "MICROWS");
                                                            rs.Put("PLAN_NEW", ConcatDate());
                                                            rs.Put("PLAN_PAY", ConcatDate());
                                                            if (ConcatDate() != IM.NullT) { rs.Put("STATUS", "1"); } else { rs.Put("STATUS", ""); }
                                                            rs.Update();
                                                            isSuccess = true;
                                                            isShowConfirmNewPeriod = true;
                                                        }
                                                        else
                                                        {
                                                            rs.Edit();
                                                            rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                                                            rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                            rs.Put("OBJ_TABLE", "MICROWS");
                                                            rs.Put("PLAN_PAY", ConcatDate());
                                                            rs.Update();
                                                            isSuccess = true;
                                                            isShowConfirmNewPeriod = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!isShowConfirmStatus)
                                                            MessageBox.Show("Для значення Status = '3' операція не може бути виконана!");
                                                        isShowConfirmStatus = true;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                Cnt++;
                            }

                       // }
                    }
                        if (rs.IsOpen())
                            rs.Close();
                        rs.Destroy();


                        if (!CreateRecordNewPeriod)
                        {
                            IMRecordset rs_ = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                            rs_.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                            rs_.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                            rs_.Open();
                            if (rs_.IsEOF())
                            {
                                rs_.AddNew();
                                rs_.Put("ID", IM.AllocID("XNRFA_MICROWA_MON", 1, -1));
                                rs_.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                rs_.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                rs_.Put("LAST_EDIT_DATE", DateTime.Now);
                                rs_.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                rs_.Put("OBJ_TABLE", "MICROWS");
                                rs_.Put("PLAN_NEW", ConcatDate());
                                rs_.Put("PLAN_PAY", ConcatDate());
                                if (ConcatDate() != IM.NullT) { rs_.Put("STATUS", "1"); } else { rs_.Put("STATUS", ""); }
                                rs_.Update();
                                isSuccess = true;
                            }
                            else
                            {
                                if (!isShowConfirmNewPeriod)
                                {
                                    if (rs_.GetT("END_DATE") != IM.NullT)
                                    {
                                        if (!isShowConfirmStatus)
                                            MessageBox.Show("Присутні результати ТРК РЕЗ. Операція не можлива!");
                                        break;
                                    }
                                    else
                                    {

                                        if (rs_.GetS("STATUS") != "3")
                                        {
                                            if (rs_.GetS("STATUS") != "2")
                                            {
                                                rs_.Edit();
                                                rs_.Put("ID", IM.AllocID("XNRFA_MICROWA_MON", 1, -1));
                                                rs_.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                rs_.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                rs_.Put("LAST_EDIT_DATE", DateTime.Now);
                                                rs_.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                rs_.Put("OBJ_TABLE", "MICROWS");
                                                rs_.Put("PLAN_NEW", ConcatDate());
                                                rs_.Put("PLAN_PAY", ConcatDate());
                                                if (ConcatDate() != IM.NullT) { rs_.Put("STATUS", "1"); } else { rs_.Put("STATUS", ""); }
                                                rs_.Update();
                                                isSuccess = true;
                                            }
                                            else
                                            {
                                                rs_.Edit();
                                                rs_.Put("ID", IM.AllocID("XNRFA_MICROWA_MON", 1, -1));
                                                rs_.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                                                rs_.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                                                rs_.Put("LAST_EDIT_DATE", DateTime.Now);
                                                rs_.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                                                rs_.Put("OBJ_TABLE", "MICROWS");
                                                rs_.Put("PLAN_PAY", ConcatDate());
                                                rs_.Update();
                                                isSuccess = true;
                                            }
                                        }
                                        else
                                        {
                                            if (!isShowConfirmStatus)
                                                MessageBox.Show("Для значення Status = '3' операція не може бути виконана!");
                                            isShowConfirmStatus = true;
                                        }
                                    }
                                }
                            }
                            if (rs_.IsOpen())
                                rs_.Close();
                            rs_.Destroy();
                        }

                        CountUpdated++;
                    //}
                }
                if (rs_allstations.IsOpen())
                    rs_allstations.Close();
                rs_allstations.Destroy();
              
                string Mess = "";
                if (isSuccess)
                {
                    Mess = "Запис(и) створено/оновлено успішно!";
                    if (CountUpdated > 0)
                        Mess += Environment.NewLine + string.Format("Кількість записів : {0}", CountUpdated);
                    MessageBox.Show(Mess);
                }

               /*
                if ((AllErrors.Length > 0) && (Cnt==1)) {
                    using (IMLogFile log = new IMLogFile())  {
                        log.Create("ResultWorkImportData");
                        string StrWarning = "Під час внесення даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrors;
                        log.Warning(StrWarning);
                        log.Display("Результати імпорту");
                    }
                }
                */ 
               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void FormRRZ_Plan_Shown(object sender, EventArgs e)
        {
          
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            int CntAll = 0;
            bool isSuccess = false;
            IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadOnly);
            rs_allstations.Select("ID,TABLE_ID,STANDARD");
            rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);
            for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext()) {
                IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,PLAN_PAY,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext()) {
                    if (rs.GetT("END_DATE") != IM.NullT) {
                        MessageBox.Show("Присутні результати ТРК РЕЗ. Операція не можлива!");
                    }
                    else {
                        if (MessageBox.Show("Ви дійсно бажаєте очистити планування? Так/Ні?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            rs.Edit();
                            rs.Put("ALLSTAT_ID", rs_allstations.GetI("ID"));
                            rs.Put("LAST_EDIT_NAME", IM.ConnectedUser());
                            rs.Put("LAST_EDIT_DATE", DateTime.Now);
                            rs.Put("OBJ_ID", rs_allstations.GetI("ID") - 22000000);
                            rs.Put("OBJ_TABLE", "MICROWS");
                            rs.Put("PLAN_NEW", IM.NullT);
                            rs.Put("PLAN_PAY", IM.NullT);
                            rs.Put("STATUS", "");
                            rs.Update();
                            CntAll++;
                            isSuccess = true;
                        }
                    }
                }
                if (rs.IsOpen())
                    rs.Close();
                rs.Destroy();
            }
            rs_allstations.Close();
            rs_allstations.Destroy();

            string Mess = "";
            if (isSuccess) {
                Mess = "Запис(и) очищено успішно!";
                if (CntAll > 0)
                    Mess += Environment.NewLine + string.Format("Кількість записів, які було очищено : {0}", CntAll);
                MessageBox.Show(Mess);
            }
            Close();
        }
    }

    public class cbItem
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string Value { get; set; }

        public cbItem()
        {
            Month = IM.NullI;
            Year = IM.NullI;
        }
        public override string ToString()
        {
            return Value.ToString();
        }

    }

    public class cbItemContain
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string Value { get; set; }

        public cbItemContain()
        {
            Month = IM.NullI;
            Year = IM.NullI;
        }
        public override string ToString()
        {
            return Year != IM.NullI ? (Value.ToString() + " " + Year.ToString()) : Value.ToString();
        }

    }
}
