﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using OrmCs;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormBandFreq : Form
    {
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }

        public FormBandFreq(int context_id)
        {
            InitializeComponent();
            Id_Table = context_id;
            FillStart();
        }

        /// <summary>
        /// 
        /// </summary>
        private void FillStart()
        {
            try
            {

                IMRecordset rs_allstations = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                    rs_allstations.Select("ID");
                    rs_allstations.SetWhere("ID", IMRecordset.Operation.Eq, Id_Table);
                    bool isContainRecord = false;
                    for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext())
                    {
                        IMRecordset rs = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadOnly);
                        rs.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID"));
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        {
                            isContainRecord = true;
                            BandMin.Value = rs.GetD("BANDMIN");
                            BandMax.Value = rs.GetD("BANDMAX");
                            CentralFreq.Value = rs.GetD("FREQ");
                            DeviationM.Value = rs.GetD("OFFSET_MINUS");
                            DeviationP.Value = rs.GetD("OFFSET_PLUS");
                            icsCombo_Type_VP.Value = rs.GetS("EMI_TYPE");
                            break;
                        }
                        if (rs.IsOpen())
                            rs.Close();
                        rs.Destroy();

                    }
              
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }
        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_OK_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.OK;
            try
            {
                IMRecordset rs_allstations = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID");
                rs_allstations.SetWhere("ID", IMRecordset.Operation.Eq, Id_Table);
                rs_allstations.Open(); 
                if (!rs_allstations.IsEOF())
                {
                    bool isContain = false;
                    IMRecordset rs_check = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadOnly);
                    rs_check.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                    rs_check.SetWhere("BANDMIN", IMRecordset.Operation.Eq, BandMin.Value);
                    rs_check.SetWhere("BANDMAX", IMRecordset.Operation.Eq, BandMax.Value);
                    rs_check.SetWhere("FREQ", IMRecordset.Operation.Eq, CentralFreq.Value);
                    rs_check.SetWhere("OFFSET_MINUS", IMRecordset.Operation.Eq, DeviationM.Value);
                    rs_check.SetWhere("OFFSET_PLUS", IMRecordset.Operation.Eq, DeviationP.Value);
                    rs_check.SetWhere("EMI_TYPE", IMRecordset.Operation.Eq, EriFiles.GetEriDescription(icsCombo_Type_VP.Value, "EmitterType"));
                    for (rs_check.Open(); !rs_check.IsEOF(); rs_check.MoveNext())
                    {
                        isContain = true;
                    }
                    if (rs_check.IsOpen())
                        rs_check.Close();
                    rs_check.Destroy();

                    if (!isContain)
                    {
                        IMRecordset rs = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                        rs.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID"));
                        rs.Open();
                        if (rs.IsEOF())
                        {
                            int ID = IM.AllocID("XFA_BAND_EMI", 1,- 1);
                            rs.AddNew();
                            rs.Put("ID", ID);
                            rs.Put("BANDMIN", BandMin.Value);
                            rs.Put("BANDMAX", BandMax.Value);
                            rs.Put("FREQ", CentralFreq.Value);
                            rs.Put("OFFSET_MINUS", DeviationM.Value);
                            rs.Put("OFFSET_PLUS", DeviationP.Value);
                            rs.Put("EMI_TYPE", EriFiles.GetEriDescription(icsCombo_Type_VP.Value, "EmitterType"));
                            rs.Update();
                        }
                        else
                        {
                            rs.Edit();
                            rs.Put("BANDMIN", BandMin.Value);
                            rs.Put("BANDMAX", BandMax.Value);
                            rs.Put("FREQ", CentralFreq.Value);
                            rs.Put("OFFSET_MINUS", DeviationM.Value);
                            rs.Put("OFFSET_PLUS", DeviationP.Value);
                            rs.Put("EMI_TYPE", EriFiles.GetEriDescription(icsCombo_Type_VP.Value, "EmitterType"));
                            rs.Update();
                        }
                        if (rs.IsOpen())
                            rs.Close();
                        rs.Destroy();
                    }
                    else MessageBox.Show("Запис зы вказаними параметрами вже існує в таблиці XFA_BAND_EMI");
                }
                else
                {
                    bool isContain = false;
                    IMRecordset rs_check = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadOnly);
                    rs_check.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                    rs_check.SetWhere("BANDMIN", IMRecordset.Operation.Eq, BandMin.Value);
                    rs_check.SetWhere("BANDMAX", IMRecordset.Operation.Eq, BandMax.Value);
                    rs_check.SetWhere("FREQ", IMRecordset.Operation.Eq, CentralFreq.Value);
                    rs_check.SetWhere("OFFSET_MINUS", IMRecordset.Operation.Eq, DeviationM.Value);
                    rs_check.SetWhere("OFFSET_PLUS", IMRecordset.Operation.Eq, DeviationP.Value);
                    rs_check.SetWhere("EMI_TYPE", IMRecordset.Operation.Eq, EriFiles.GetEriDescription(icsCombo_Type_VP.Value, "EmitterType"));
                    

                    for (rs_check.Open(); !rs_check.IsEOF(); rs_check.MoveNext())
                    {
                        isContain = true;
                    }
                    if (rs_check.IsOpen())
                        rs_check.Close();
                    rs_check.Destroy();

                    if (!isContain)
                    {
                        IMRecordset rs = new IMRecordset("XFA_BAND_EMI", IMRecordset.Mode.ReadWrite);
                        rs.Select("ID,BANDMIN,BANDMAX,FREQ,OFFSET_PLUS,OFFSET_MINUS,EMI_TYPE");
                        rs.Open();
                       
                            int ID = IM.AllocID("XFA_BAND_EMI", 1, -1);
                            rs.AddNew();
                            rs.Put("ID", ID);
                            rs.Put("BANDMIN", BandMin.Value);
                            rs.Put("BANDMAX", BandMax.Value);
                            rs.Put("FREQ", CentralFreq.Value);
                            rs.Put("OFFSET_MINUS", DeviationM.Value);
                            rs.Put("OFFSET_PLUS", DeviationP.Value);
                            rs.Put("EMI_TYPE", EriFiles.GetEriDescription(icsCombo_Type_VP.Value, "EmitterType"));
                            rs.Update();
                        if (rs.IsOpen())
                            rs.Close();
                        rs.Destroy();
                    }
                    else MessageBox.Show("Запис зы вказаними параметрами вже існує в таблиці XFA_BAND_EMI");
                }
                  

                MessageBox.Show("Запис створено успішно!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: "+ex.Message);
            }
            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
