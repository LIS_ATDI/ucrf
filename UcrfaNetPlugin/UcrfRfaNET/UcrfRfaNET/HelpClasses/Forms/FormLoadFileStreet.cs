﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.OfficeBridge;
using OrmCs;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormLoadFileStreet : Form
    {
        string NameF { get; set; }
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }
        public TypeFormatFile ChbType { get; set; }

        public FormLoadFileStreet(IMQueryMenuNode.Context context)
        {
            InitializeComponent();
            lstContext = context.DataList;
            NameTable = context.TableName;
            Id_Table = context.TableId;
            comboBox1.SelectedIndex = 0;
            openFileDialog1.Filter = "Attach Files(*.CSV;*.XLSX)|*.CSV;*XLSX";
            button2.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            int AllFailRec = 0; int AllAddedRec = 0;
            string AllErrors = ""; int CountSucessNew = 0; int CountSucessUpdate = 0; int CountFail = 0;
            DA = System.Windows.Forms.DialogResult.OK;
            try
            {
               int count_ = 0;
               int done_=0;
               IM.ExecuteScalar(ref count_, string.Format("SELECT COUNT(*)  FROM %CITIES"));
               IMProgress.Create("Please wait...");
               List<CitiesClass> L_CitiesClass = new List<CitiesClass>();
               IMRecordset rsStreet_ = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
               rsStreet_.Select("ID,CODE,NAME,SUBPROVINCE,PROVINCE,ADM_CODE");
               for (rsStreet_.Open(); !rsStreet_.IsEOF(); rsStreet_.MoveNext())
               {
                   IMProgress.ShowProgress(done_++, count_);
                   CitiesClass ct_ = new CitiesClass();
                   ct_.ID = rsStreet_.GetI("ID");
                   ct_.CODE = rsStreet_.GetS("CODE");
                   ct_.NAME = rsStreet_.GetS("NAME");
                   ct_.SUB_PROVINCE  = rsStreet_.GetS("SUBPROVINCE");
                   ct_.PROVINCE = rsStreet_.GetS("PROVINCE");
                   ct_.ADM_CODE = rsStreet_.GetS("ADM_CODE");
                   L_CitiesClass.Add(ct_);
               }
               rsStreet_.Close();
               rsStreet_.Destroy();
               IMProgress.Destroy();
               if (string.IsNullOrEmpty(NameF)) { MessageBox.Show("Файл не обрано!"); return; }
               if (System.IO.File.Exists(NameF))
                {
                    if (ChbType == TypeFormatFile.CSV)
                    {
                        string AllErrorsX = "";
                        using (System.IO.StreamReader read = new System.IO.StreamReader(NameF, System.Text.Encoding.Default))
                        {
                            int limit_start = 0; int MaxCountCh=0;
                            int iu_c = 0; if (checkBox1.Checked) limit_start = 1;
                            while (read.Peek() >= limit_start)
                            {
                                string[] str_word = read.ReadLine().Split(new char[] { ';' });

                                if (str_word.Length > 0)
                                {
                                    AllErrors = "";
                                    string decimal_sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                                    string KOATU = str_word[0].Trim().ToString();
                                    string OldStreet = str_word[1].Trim().ToString();
                                    string NewStreet = str_word[2].Trim().ToString();
                                    MaxCountCh = 0;
                                    if (KOATU.Trim() == "") { CountFail++; }
                                    if (OldStreet.Trim() == "") { CountFail++; }
                                    if (NewStreet.Trim() == "") { CountFail++; }
                                    if (CountFail > 0)
                                    {
                                        if (MaxCountCh > 8) break;
                                        if (MaxCountCh <= 8) { MaxCountCh++; }
                                    }
                                    if ((KOATU.Trim() == "") && (OldStreet.Trim() == "") && (NewStreet.Trim() == "")) continue;
                                    if (KOATU.Trim() == "") { AllErrors += string.Format("- КОАТУ не може бути порожнім (рядок {0})", iu_c) + Environment.NewLine; }
                                    if (OldStreet.Trim() == "") { AllErrors += string.Format("- Поле 'Назва' не може бути порожнім (Код КОАТУ {0}) ", KOATU.Trim()) + Environment.NewLine; }
                                    if (NewStreet.Trim() == "") { AllErrors += string.Format("- Поле 'Нова назва' не може бути порожнім (Код КОАТУ {0})", KOATU.Trim()) + Environment.NewLine; }
                                    bool isCodeSuccess = false;
                                    CitiesClass rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU);
                                    if (rsStreet_1 != null) { isCodeSuccess = true; }
                                    if (!isCodeSuccess) AllErrors += string.Format(" Запис з наступним значенням CITIES.CODE: {0} не знайдено ", KOATU) + Environment.NewLine;
                                    bool isCodeSuccess_ = true;
                                    rsStreet_1 = L_CitiesClass.Find(r => r.CODE.Contains(KOATU) && r.CODE.Contains("_"));
                                    if (rsStreet_1 != null) isCodeSuccess_ = false;
                                    if (!isCodeSuccess_) AllErrors += string.Format(" Запис з наступним значенням CITIES.CODE: {0} містить символ '_' ", KOATU) + Environment.NewLine;
                                    bool isCitiesName_ = true;
                                    if (comboBox1.SelectedIndex == 2)
                                    {
                                        rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && r.NAME == OldStreet);
                                        if (rsStreet_1 == null) isCitiesName_ = false;
                                    }
                                    if (!isCitiesName_) AllErrors += string.Format(" Запис з наступним значенням CITIES.NAME: {0} не знайдено (Код КОАТУ {1})", OldStreet, KOATU) + Environment.NewLine;
                                    bool isCitiesSubprovince_ = true;
                                    if (comboBox1.SelectedIndex == 1)
                                    {
                                        rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && r.SUB_PROVINCE == OldStreet);
                                        if (rsStreet_1 == null) isCitiesSubprovince_ = false;
                                    }
                                    if (!isCitiesSubprovince_) AllErrors += string.Format(" Запис з наступним значенням CITIES.SUBPROVINCE: {0} не знайдено (Код КОАТУ {1})", OldStreet, KOATU) + Environment.NewLine;

                                    if (string.IsNullOrEmpty(AllErrors))
                                    {
                                        //rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && (((r.NAME == OldStreet) && (comboBox1.SelectedIndex == 2)) || ((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                        List<CitiesClass> rsStreet_11 = new List<CitiesClass>();
                                        if (comboBox1.SelectedIndex == 2)
                                        {
                                            rsStreet_11 = L_CitiesClass.FindAll(r => r.CODE == KOATU && (((r.NAME == OldStreet) && (comboBox1.SelectedIndex == 2))));
                                        }
                                        else if (comboBox1.SelectedIndex == 1)
                                        {
                                            rsStreet_11 = L_CitiesClass.FindAll(r => (((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                        }
                                        //rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && (((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                        if (rsStreet_11 != null)
                                        {
                                            YXvUpdatePosition upd_pos = new YXvUpdatePosition();
                                            string AllReq = "";
                                            List<KeyValuePair<int, string>> LCodesAdm = new List<KeyValuePair<int, string>>();
                                            foreach (CitiesClass item in rsStreet_11)
                                            {
                                                if (item.ADM_CODE != "")
                                                {
                                                    LCodesAdm.Add(new KeyValuePair<int, string>(item.ID, item.ADM_CODE));
                                                    if (comboBox1.SelectedIndex == 2)
                                                    {
                                                        AllReq += string.Format(" ([CITY_ID]={0}) OR ", item.ID);
                                                    }
                                                    else if (comboBox1.SelectedIndex == 1)
                                                    {
                                                        AllReq += string.Format(" ([CITY_ID]={0}) OR ", item.ID);
                                                    }
                                                }
                                            }

                                            if (AllReq.Length > 0)
                                            {
                                                if (comboBox1.SelectedIndex == 2)
                                                {
                                                    AllReq = "(" + AllReq.Remove(AllReq.Length - 3, 3) + ")";
                                                    AllReq += string.Format(" AND ([CITY]='{0}')", OldStreet);
                                                }
                                                else if (comboBox1.SelectedIndex == 1)
                                                {
                                                    AllReq = "(" + AllReq.Remove(AllReq.Length - 3, 3) + ")";
                                                    AllReq += string.Format(" AND ([SUBPROVINCE]='{0}')", OldStreet);
                                                }

                                                if (comboBox1.SelectedIndex == 2)
                                                {
                                                    upd_pos.Table = "XV_UPDATE_POSITION";
                                                    upd_pos.Format("**");
                                                    upd_pos.Filter = AllReq;
                                                    for (upd_pos.OpenRs(); !upd_pos.IsEOF(); upd_pos.MoveNext())
                                                    {
                                                        string ADM_R = "";
                                                        switch (upd_pos.m_table_name)
                                                        {
                                                            case "POSITION_WIM":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_WIM WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_WIM SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_BRO":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_BRO WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_BRO SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_ES":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_ES WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_ES SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_MOB2":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MOB2 WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_MOB2 SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_MW":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MW WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_MW SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "XFA_POSITION":
                                                                IM.Execute(string.Format(" UPDATE %XFA_POSITION SET CITY='{0}' WHERE (ID={1}) ", NewStreet, upd_pos.m_table_id));
                                                                break;
                                                            case "SITE":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %SITE WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %SITE SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                        }
                                                    }

                                                }
                                                if (comboBox1.SelectedIndex == 1)
                                                {
                                                    upd_pos.Table = "XV_UPDATE_POSITION";
                                                    upd_pos.Format("**");
                                                    upd_pos.Filter = AllReq;
                                                    for (upd_pos.OpenRs(); !upd_pos.IsEOF(); upd_pos.MoveNext())
                                                    {
                                                        string ADM_R = "";
                                                        switch (upd_pos.m_table_name)
                                                        {
                                                            case "POSITION_WIM":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_WIM WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_WIM SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_BRO":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_BRO WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_BRO SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_ES":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_ES WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_ES SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_MOB2":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MOB2 WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_MOB2 SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "POSITION_MW":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MW WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %POSITION_MW SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                            case "XFA_POSITION":
                                                                IM.Execute(string.Format(" UPDATE %XFA_POSITION SET SUBPROVINCE='{0}' WHERE (ID={1}) ", NewStreet, upd_pos.m_table_id));
                                                                break;
                                                            case "SITE":
                                                                if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %SITE WHERE ID={0} ", upd_pos.m_table_id)))
                                                                {
                                                                    ADM_R += "_";
                                                                    if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                                    IM.Execute(string.Format(" UPDATE %SITE SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }

                                                upd_pos.Close();
                                                upd_pos.Dispose();
                                            }


                                            foreach (CitiesClass item in rsStreet_11)
                                            {
                                                using (YCities cities = new YCities())
                                                {
                                                    cities.Table = "CITIES";
                                                    cities.Filter = string.Format(" ([ID]={0}) ", item.ID);
                                                    for (cities.OpenRs(); !cities.IsEOF(); cities.MoveNext())
                                                    {
                                                        YCities cities_dupl = new YCities();
                                                        cities_dupl.Table = "CITIES";
                                                        cities_dupl.New();
                                                        cities_dupl.CopyDataFrom(cities);
                                                        cities_dupl.m_id = cities_dupl.AllocID();
                                                        cities.m_code += "_";
                                                        cities.m_adm_code += "_";
                                                        if (cities.m_code.Length > 10) cities.m_code = cities.m_code.Substring(0, 11);
                                                        if (cities.m_adm_code.Length > 10) cities.m_adm_code = cities.m_adm_code.Substring(0, 11);
                                                        cities.Save();
                                                        //cities_dupl.m_code += "_";
                                                        if (comboBox1.SelectedIndex == 2)
                                                        {
                                                            cities_dupl.m_name = NewStreet;

                                                        }
                                                        else if (comboBox1.SelectedIndex == 1)
                                                        {
                                                            cities_dupl.m_subprovince = NewStreet;

                                                            using (YCombo comboTable = new YCombo())
                                                            {
                                                                comboTable.Table = "COMBO";
                                                                if (comboTable.Fetch(string.Format(" (([DOMAIN] LIKE 'SUBPROVINCES')  AND ([ITEM] LIKE '{0}')) ", OldStreet)))
                                                                {
                                                                    comboTable.m_item = NewStreet;
                                                                    comboTable.Save();
                                                                }
                                                                else
                                                                {

                                                                    if (!comboTable.Fetch(string.Format(" (([DOMAIN] LIKE 'SUBPROVINCES')  AND ([ITEM] = '{0}')) ", NewStreet)))
                                                                    {
                                                                        comboTable.New();
                                                                        comboTable.m_domain = "SUBPROVINCES";
                                                                        comboTable.m_lang = "RUS";
                                                                        comboTable.m_item = NewStreet;
                                                                        comboTable.Save();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        cities_dupl.SaveWithComponents();
                                                        AllAddedRec++;
                                                        cities_dupl.Close();
                                                        cities_dupl.Dispose();


                                                    }
                                                    cities.Close();
                                                }
                                            }
                                        }
                                        else AllFailRec++;
                                    }
                                    else AllFailRec++;
                                }
                                AllErrorsX += AllErrors;
                                if (MaxCountCh > 8) break;
                                if (MaxCountCh <= 8) { MaxCountCh++; iu_c++; continue; }
                            }

                        }
                        if (AllErrorsX.Length > 0)
                        {
                            using (IMLogFile log = new IMLogFile())
                            {
                                log.Create("ResultWorkImportData");
                                string StrWarning = "Під час внесення даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrorsX;
                                log.Warning(StrWarning);
                                log.Display("Результати імпорту");
                            }
                        }
                    }
                    else if (ChbType == TypeFormatFile.XLSX)
                    {
                        string AllErrorsX = "";
                        ExcelFastNPOI excel = new ExcelFastNPOI();
                        excel.GetWorkBook(NameF);
                        int row_val = 0; int MaxCountCh=0;
                        if (checkBox1.Checked) { row_val = 1; }
                        for (; ; )
                        {
                            AllErrors = "";
                            string decimal_sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                            string KOATU = excel.GetCellValue(0, row_val).ToString();
                            string OldStreet = excel.GetCellValue(1, row_val).ToString();
                            string NewStreet = excel.GetCellValueTxt(2, row_val).ToString();
                            if (KOATU.Trim() == "")  {  CountFail++;  }
                            if (OldStreet.Trim() == "")  {  CountFail++; }
                            if (NewStreet.Trim() == "")  {  CountFail++; }
                            if (CountFail > 0)
                            {
                                if (MaxCountCh > 8) break;
                                if (MaxCountCh <= 8) { MaxCountCh++; }
                            }
                            if ((KOATU.Trim() == "") && (OldStreet.Trim() == "") && (NewStreet.Trim() == "")) continue;
                            if (KOATU.Trim() == "")   { AllErrors += string.Format("- КОАТУ не може бути порожнім (рядок {0})", row_val) + Environment.NewLine;   }
                            if (OldStreet.Trim() == "") { AllErrors += string.Format("- Поле 'Назва' не може бути порожнім (Код КОАТУ {0})", KOATU) + Environment.NewLine; }
                            if (NewStreet.Trim() == "") { AllErrors += string.Format("- Поле 'Нова назва' не може бути порожнім (Код КОАТУ {0})", KOATU) + Environment.NewLine; }
                            bool isCodeSuccess = false;
                            CitiesClass rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU);
                            if (rsStreet_1 != null) { isCodeSuccess = true; }
                            if (!isCodeSuccess) AllErrors += string.Format(" Запис з наступним значенням CITIES.CODE: {0} не знайдено", KOATU) + Environment.NewLine;
                            bool isCodeSuccess_ = true;
                            rsStreet_1 = L_CitiesClass.Find(r => r.CODE.Contains(KOATU) && r.CODE.Contains("_"));
                            if (rsStreet_1 != null) isCodeSuccess_ = false;
                            if (!isCodeSuccess_) AllErrors += string.Format(" Запис з наступним значенням CITIES.CODE: {0}  містить символ '_'", KOATU) + Environment.NewLine;
                            bool isCitiesName_ = true;
                            if (comboBox1.SelectedIndex == 2)
                            {
                                rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && r.NAME == OldStreet);
                                if (rsStreet_1 == null) isCitiesName_ = false;
                            }
                            if (!isCitiesName_) AllErrors += string.Format(" Запис з наступним значенням CITIES.NAME: {0} не знайдено (Код КОАТУ {1})", OldStreet, KOATU) + Environment.NewLine;
                            bool isCitiesSubprovince_ = true;
                            if (comboBox1.SelectedIndex == 1)
                            {
                                rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && r.SUB_PROVINCE == OldStreet);
                                if (rsStreet_1 == null) isCitiesSubprovince_ = false;
                            }
                            if (!isCitiesSubprovince_) AllErrors += string.Format(" Запис з наступним значенням CITIES.SUBPROVINCE: {0} не знайдено (Код КОАТУ {1})", OldStreet, KOATU) + Environment.NewLine;
                            if (string.IsNullOrEmpty(AllErrors))  {
                                //rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && (((r.NAME == OldStreet) && (comboBox1.SelectedIndex == 2)) || ((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                List<CitiesClass> rsStreet_11 = new List<CitiesClass>();
                                if (comboBox1.SelectedIndex == 2)
                                {
                                    rsStreet_11 = L_CitiesClass.FindAll(r => r.CODE == KOATU && (((r.NAME == OldStreet) && (comboBox1.SelectedIndex == 2))));
                                }
                                else if (comboBox1.SelectedIndex == 1)
                                {
                                    rsStreet_11 = L_CitiesClass.FindAll(r => (((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                }
                                    //rsStreet_1 = L_CitiesClass.Find(r => r.CODE == KOATU && (((r.SUB_PROVINCE == OldStreet) && (comboBox1.SelectedIndex == 1))));
                                if (rsStreet_11 != null)
                                {
                                    YXvUpdatePosition upd_pos = new YXvUpdatePosition();
                                    string AllReq = "";
                                    List<KeyValuePair<int, string>> LCodesAdm = new List<KeyValuePair<int, string>>();
                                    foreach (CitiesClass item in rsStreet_11)
                                    {
                                        if (item.ADM_CODE != "")
                                        {
                                            LCodesAdm.Add(new KeyValuePair<int, string>(item.ID, item.ADM_CODE));
                                            if (comboBox1.SelectedIndex == 2)
                                            {
                                                AllReq += string.Format(" ([CITY_ID]={0}) OR ", item.ID);
                                            }
                                            else if (comboBox1.SelectedIndex == 1)
                                            {
                                                AllReq += string.Format(" ([CITY_ID]={0}) OR ", item.ID);
                                            }
                                        }
                                    }

                                    if (AllReq.Length > 0)
                                    {
                                        if (comboBox1.SelectedIndex == 2)
                                        {
                                            AllReq = "("+AllReq.Remove(AllReq.Length - 3, 3)+")";
                                            AllReq += string.Format(" AND ([CITY]='{0}')", OldStreet);
                                        }
                                        else if (comboBox1.SelectedIndex == 1)
                                        {
                                            AllReq = "(" + AllReq.Remove(AllReq.Length - 3, 3) + ")"; 
                                            AllReq += string.Format(" AND ([SUBPROVINCE]='{0}')", OldStreet);
                                        }

                                        if (comboBox1.SelectedIndex == 2)
                                        {
                                            upd_pos.Table = "XV_UPDATE_POSITION";
                                            upd_pos.Format("**");
                                            upd_pos.Filter = AllReq;
                                            for (upd_pos.OpenRs(); !upd_pos.IsEOF(); upd_pos.MoveNext())
                                            {
                                                string ADM_R = "";
                                                switch (upd_pos.m_table_name)
                                                {
                                                    case "POSITION_WIM":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_WIM WHERE ID={0} ", upd_pos.m_table_id)))  {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_WIM SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_BRO":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_BRO WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_BRO SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_ES":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_ES WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_ES SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_MOB2":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MOB2 WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_MOB2 SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_MW":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MW WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_MW SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "XFA_POSITION":
                                                        IM.Execute(string.Format(" UPDATE %XFA_POSITION SET CITY='{0}' WHERE (ID={1}) ", NewStreet, upd_pos.m_table_id));
                                                        break;
                                                    case "SITE":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %SITE WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %SITE SET CITY='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                }
                                            }
                                            
                                        }
                                        if (comboBox1.SelectedIndex == 1)
                                        {
                                            upd_pos.Table = "XV_UPDATE_POSITION";
                                            upd_pos.Format("**");
                                            upd_pos.Filter = AllReq;
                                            for (upd_pos.OpenRs(); !upd_pos.IsEOF(); upd_pos.MoveNext())
                                            {
                                                string ADM_R = "";
                                                switch (upd_pos.m_table_name)
                                                {
                                                    case "POSITION_WIM":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_WIM WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_WIM SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_BRO":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_BRO WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_BRO SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_ES":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_ES WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_ES SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_MOB2":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MOB2 WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_MOB2 SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "POSITION_MW":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %POSITION_MW WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %POSITION_MW SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                    case "XFA_POSITION":
                                                        IM.Execute(string.Format(" UPDATE %XFA_POSITION SET SUBPROVINCE='{0}' WHERE (ID={1}) ", NewStreet, upd_pos.m_table_id));
                                                        break;
                                                    case "SITE":
                                                        if (IM.ExecuteScalar(ref ADM_R, string.Format(" SELECT ADM_CODE FROM %SITE WHERE ID={0} ", upd_pos.m_table_id)))
                                                        {
                                                            ADM_R += "_";
                                                            if (ADM_R.Length > 10) ADM_R = ADM_R.Substring(0, 11);
                                                            IM.Execute(string.Format(" UPDATE %SITE SET SUBPROVINCE='{0}', ADM_CODE='{1}' WHERE (ID={2}) ", NewStreet, ADM_R, upd_pos.m_table_id));
                                                        }
                                                        break;
                                                }
                                            }
                                        }

                                        upd_pos.Close();
                                        upd_pos.Dispose();
                                    }


                                    foreach (CitiesClass item in rsStreet_11)
                                    {
                                        using (YCities cities = new YCities())
                                        {
                                            cities.Table = "CITIES";
                                            cities.Filter = string.Format(" ([ID]={0}) ", item.ID);
                                            for (cities.OpenRs(); !cities.IsEOF(); cities.MoveNext())
                                            {
                                                YCities cities_dupl = new YCities();
                                                cities_dupl.Table = "CITIES";
                                                cities_dupl.New();
                                                cities_dupl.CopyDataFrom(cities);
                                                cities_dupl.m_id = cities_dupl.AllocID();
                                                cities.m_code += "_";
                                                cities.m_adm_code += "_";
                                                if (cities.m_code.Length>10) cities.m_code = cities.m_code.Substring(0, 11);
                                                if (cities.m_adm_code.Length> 10) cities.m_adm_code = cities.m_adm_code.Substring(0, 11);
                                                cities.Save();
                                                //cities_dupl.m_code += "_";
                                                if (comboBox1.SelectedIndex == 2)
                                                {
                                                    cities_dupl.m_name = NewStreet;
                                                    
                                                }
                                                else if (comboBox1.SelectedIndex == 1)
                                                {
                                                    cities_dupl.m_subprovince = NewStreet;

                                                    using (YCombo comboTable = new YCombo())
                                                    {
                                                        comboTable.Table = "COMBO";
                                                        if (comboTable.Fetch(string.Format(" (([DOMAIN] LIKE 'SUBPROVINCES')  AND ([ITEM] LIKE '{0}')) ", OldStreet)))
                                                        {
                                                            comboTable.m_item = NewStreet;
                                                            comboTable.Save();
                                                        }
                                                        else
                                                        {

                                                            if (!comboTable.Fetch(string.Format(" (([DOMAIN] LIKE 'SUBPROVINCES')  AND ([ITEM] = '{0}')) ", NewStreet)))
                                                            {
                                                                comboTable.New();
                                                                comboTable.m_domain = "SUBPROVINCES";
                                                                comboTable.m_lang = "RUS";
                                                                comboTable.m_item = NewStreet;
                                                                comboTable.Save();
                                                            }
                                                        }
                                                    }
                                                }
                                                cities_dupl.SaveWithComponents();
                                                AllAddedRec++;
                                                cities_dupl.Close();
                                                cities_dupl.Dispose();


                                            }
                                            cities.Close();
                                        }
                                    }
                                }
                                else AllFailRec++;
                            }
                            else AllFailRec++;

                            AllErrorsX += AllErrors;
                            if (MaxCountCh > 8) break;
                            if (MaxCountCh <= 8) { MaxCountCh++; row_val++; continue; }
                        }

                        excel.Close();
                        excel.Dispose();

                        if (AllErrorsX.Length > 0)
                        {
                            using (IMLogFile log = new IMLogFile())
                            {
                                log.Create("ResultWorkImportData");
                                string StrWarning = "Під час внесення даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrorsX;
                                log.Warning(StrWarning);
                                log.Display("Результати імпорту");
                            }
                        }
                    }
                    
                }
                else { MessageBox.Show("Файл не існує!"); }

                MessageBox.Show(string.Format("Додано новых записів - {0}.{1}Кількість записів з помилками - {2}", AllAddedRec, Environment.NewLine, AllFailRec));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            Close();
             
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Attach Files(*.CSV;*.XLSX)|*.CSV;*XLSX";
                    openFileDialog1.FileName = "";
                    DialogResult DA = openFileDialog1.ShowDialog();
                    if (DA == System.Windows.Forms.DialogResult.OK)
                    {
                        NameF = openFileDialog1.FileName;
                        textBox1.Text = NameF;
                        string ext = System.IO.Path.GetExtension(NameF);
                        if (ext.ToLower().Contains("csv")) ChbType = TypeFormatFile.CSV;
                        if (ext.ToLower().Contains("xlsx")) ChbType = TypeFormatFile.XLSX;
                        if (comboBox1.SelectedIndex > 0)
                            button2.Enabled = true;
                    }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void FormLoadFile_Shown(object sender, EventArgs e)
        {
            CLocaliz.TxT(this);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 0)
                button2.Enabled = true;
            else
                button2.Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CitiesClass
    {
        public int ID { get; set; }
        public string CODE { get; set; }
        public string ADM_CODE { get; set; }
        public string NAME { get; set; }
        public string PROVINCE { get; set; }
        public string SUB_PROVINCE { get; set; }
    }


}
