﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    partial class FCorrespondencyCategory : FBaseForm
    {
        public string CategoriesAsString
        {
            get { return GetCategoryAsString(); }
            set { SetCategoryAsString(value); }
        }

        public FCorrespondencyCategory()
        {
            InitializeComponent();
        }        

        private string GetCategoryAsString()
        {
            string result = "";
            
            if (CLCategories.GetItemChecked(0))            
                result = result + "CO,";

            if (CLCategories.GetItemChecked(1))
                result = result + "CP,";

            if (CLCategories.GetItemChecked(2))
                result = result + "CR,";

            if (CLCategories.GetItemChecked(3))
                result = result + "CV,";

            if (CLCategories.GetItemChecked(4))
                result = result + "OT,";

            if (result.Length > 0)
                return result.Substring(0, result.Length - 1);

            return result;
        }

        private void SetCategoryAsString(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string[] splitted = value.Split(',');

                if (splitted.Contains("CO"))
                    CLCategories.SetItemChecked(0, true);

                if (splitted.Contains("CP"))
                    CLCategories.SetItemChecked(1, true);

                if (splitted.Contains("CR"))
                    CLCategories.SetItemChecked(2, true);

                if (splitted.Contains("CV"))
                    CLCategories.SetItemChecked(3, true);

                if (splitted.Contains("OT"))
                    CLCategories.SetItemChecked(4, true);
            }
        }
    }
}
