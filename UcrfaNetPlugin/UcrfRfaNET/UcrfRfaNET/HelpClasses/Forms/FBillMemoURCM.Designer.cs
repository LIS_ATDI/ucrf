﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FBillMemoURCM
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.btPrint = new System.Windows.Forms.Button();
            this.btDRV = new System.Windows.Forms.Button();
            this.btExit = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbKod = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dtApplCreated = new System.Windows.Forms.DateTimePicker();
            this.tbMemoDocLink = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAuto = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbInvoice = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbCurrentUser = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbEvent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbDateTime = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbAddDocLinkList = new System.Windows.Forms.ComboBox();
            this.btnChangeToPrinted = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tbAddCreateDate = new System.Windows.Forms.TextBox();
            this.btnNotPrinted = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbRemember = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btREZPrint = new System.Windows.Forms.Button();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btPrint
            // 
            this.btPrint.Location = new System.Drawing.Point(117, 5);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(93, 26);
            this.btPrint.TabIndex = 0;
            this.btPrint.Text = "Друк";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btDRV
            // 
            this.btDRV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDRV.Location = new System.Drawing.Point(333, 5);
            this.btDRV.Name = "btDRV";
            this.btDRV.Size = new System.Drawing.Size(93, 26);
            this.btDRV.TabIndex = 1;
            this.btDRV.Text = "-> ДРВ";
            this.btDRV.UseVisualStyleBackColor = true;
            this.btDRV.Click += new System.EventHandler(this.btDRV_Click);
            // 
            // btExit
            // 
            this.btExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btExit.Location = new System.Drawing.Point(424, 3);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(115, 26);
            this.btExit.TabIndex = 2;
            this.btExit.Text = "Вихід";
            this.btExit.UseVisualStyleBackColor = true;
            // 
            // btDelete
            // 
            this.btDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDelete.Location = new System.Drawing.Point(225, 5);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(93, 26);
            this.btDelete.TabIndex = 4;
            this.btDelete.Text = "Видалити";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.AutoSize = true;
            this.panel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel9.Location = new System.Drawing.Point(551, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(0, 0);
            this.panel9.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.groupBox3);
            this.flowLayoutPanel2.Controls.Add(this.groupBox4);
            this.flowLayoutPanel2.Controls.Add(this.groupBox2);
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.groupBox5);
            this.flowLayoutPanel2.Controls.Add(this.groupBox6);
            this.flowLayoutPanel2.Controls.Add(this.panel3);
            this.flowLayoutPanel2.Controls.Add(this.panel9);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(551, 699);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbKod);
            this.groupBox3.Controls.Add(this.tbName);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(542, 78);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Користувач радіочастотним ресурсом України";
            // 
            // tbKod
            // 
            this.tbKod.BackColor = System.Drawing.SystemColors.Window;
            this.tbKod.Location = new System.Drawing.Point(108, 45);
            this.tbKod.Name = "tbKod";
            this.tbKod.ReadOnly = true;
            this.tbKod.Size = new System.Drawing.Size(140, 20);
            this.tbKod.TabIndex = 3;
            // 
            // tbName
            // 
            this.tbName.BackColor = System.Drawing.SystemColors.Window;
            this.tbName.Location = new System.Drawing.Point(108, 19);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(428, 20);
            this.tbName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Код ЄДРПОУ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Назва";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dtApplCreated);
            this.groupBox4.Controls.Add(this.tbMemoDocLink);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.tbUser);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.tbNumber);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 87);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(542, 83);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Реквізити службової записки";
            // 
            // dtApplCreated
            // 
            this.dtApplCreated.Location = new System.Drawing.Point(108, 19);
            this.dtApplCreated.Name = "dtApplCreated";
            this.dtApplCreated.Size = new System.Drawing.Size(140, 20);
            this.dtApplCreated.TabIndex = 12;
            // 
            // tbMemoDocLink
            // 
            this.tbMemoDocLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMemoDocLink.BackColor = System.Drawing.SystemColors.Window;
            this.tbMemoDocLink.Location = new System.Drawing.Point(396, 45);
            this.tbMemoDocLink.Name = "tbMemoDocLink";
            this.tbMemoDocLink.ReadOnly = true;
            this.tbMemoDocLink.Size = new System.Drawing.Size(140, 20);
            this.tbMemoDocLink.TabIndex = 11;
            this.tbMemoDocLink.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tbMemoDocLink_MouseDoubleClick);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(295, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Лінк на документ";
            // 
            // tbUser
            // 
            this.tbUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUser.BackColor = System.Drawing.SystemColors.Window;
            this.tbUser.Location = new System.Drawing.Point(396, 19);
            this.tbUser.Name = "tbUser";
            this.tbUser.ReadOnly = true;
            this.tbUser.Size = new System.Drawing.Size(140, 20);
            this.tbUser.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Виконавець";
            // 
            // tbNumber
            // 
            this.tbNumber.BackColor = System.Drawing.SystemColors.Window;
            this.tbNumber.Location = new System.Drawing.Point(108, 45);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.ReadOnly = true;
            this.tbNumber.Size = new System.Drawing.Size(140, 20);
            this.tbNumber.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "№";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата створення";
            // 
            // groupBox2
            // 
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 176);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(542, 246);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Кількість робіт за статтями Тарифів";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAuto);
            this.panel1.Controls.Add(this.btSave);
            this.panel1.Controls.Add(this.btPrint);
            this.panel1.Controls.Add(this.btDRV);
            this.panel1.Controls.Add(this.btDelete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 428);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(542, 36);
            this.panel1.TabIndex = 7;
            // 
            // btnAuto
            // 
            this.btnAuto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuto.Location = new System.Drawing.Point(441, 5);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(93, 26);
            this.btnAuto.TabIndex = 6;
            this.btnAuto.Text = "Авто";
            this.btnAuto.UseVisualStyleBackColor = true;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(9, 5);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(93, 26);
            this.btSave.TabIndex = 5;
            this.btSave.Text = "Зберегти";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbInvoice);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.tbCurrentUser);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.tbEvent);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.tbDateTime);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(3, 470);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(542, 76);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Поточний стан опрацювання в ДРВ";
            // 
            // tbInvoice
            // 
            this.tbInvoice.BackColor = System.Drawing.SystemColors.Window;
            this.tbInvoice.Location = new System.Drawing.Point(421, 45);
            this.tbInvoice.Name = "tbInvoice";
            this.tbInvoice.ReadOnly = true;
            this.tbInvoice.Size = new System.Drawing.Size(113, 20);
            this.tbInvoice.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(359, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Рахунок";
            // 
            // tbCurrentUser
            // 
            this.tbCurrentUser.BackColor = System.Drawing.SystemColors.Window;
            this.tbCurrentUser.Location = new System.Drawing.Point(85, 19);
            this.tbCurrentUser.Name = "tbCurrentUser";
            this.tbCurrentUser.ReadOnly = true;
            this.tbCurrentUser.Size = new System.Drawing.Size(268, 20);
            this.tbCurrentUser.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Виконавець";
            // 
            // tbEvent
            // 
            this.tbEvent.BackColor = System.Drawing.SystemColors.Window;
            this.tbEvent.Location = new System.Drawing.Point(85, 45);
            this.tbEvent.Name = "tbEvent";
            this.tbEvent.ReadOnly = true;
            this.tbEvent.Size = new System.Drawing.Size(268, 20);
            this.tbEvent.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Подія";
            // 
            // tbDateTime
            // 
            this.tbDateTime.BackColor = System.Drawing.SystemColors.Window;
            this.tbDateTime.Location = new System.Drawing.Point(421, 19);
            this.tbDateTime.Name = "tbDateTime";
            this.tbDateTime.ReadOnly = true;
            this.tbDateTime.Size = new System.Drawing.Size(113, 20);
            this.tbDateTime.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(359, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Дата, час";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbAddDocLinkList);
            this.groupBox6.Controls.Add(this.btnChangeToPrinted);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.tbAddCreateDate);
            this.groupBox6.Controls.Add(this.btnNotPrinted);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.tbRemember);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 552);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(542, 101);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Генерація Додатку до Договору";
            // 
            // tbAddDocLinkList
            // 
            this.tbAddDocLinkList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbAddDocLinkList.FormattingEnabled = true;
            this.tbAddDocLinkList.Location = new System.Drawing.Point(114, 74);
            this.tbAddDocLinkList.Name = "tbAddDocLinkList";
            this.tbAddDocLinkList.Size = new System.Drawing.Size(171, 21);
            this.tbAddDocLinkList.TabIndex = 20;
            this.tbAddDocLinkList.SelectedIndexChanged += new System.EventHandler(this.tbAddDocLinkList_SelectedIndexChanged);
            // 
            // btnChangeToPrinted
            // 
            this.btnChangeToPrinted.Location = new System.Drawing.Point(291, 43);
            this.btnChangeToPrinted.Name = "btnChangeToPrinted";
            this.btnChangeToPrinted.Size = new System.Drawing.Size(245, 23);
            this.btnChangeToPrinted.TabIndex = 4;
            this.btnChangeToPrinted.Text = "Документ надруковано";
            this.btnChangeToPrinted.UseVisualStyleBackColor = true;
            this.btnChangeToPrinted.Click += new System.EventHandler(this.btnChangeToPrinted_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Лінк на документ";
            // 
            // tbAddCreateDate
            // 
            this.tbAddCreateDate.BackColor = System.Drawing.SystemColors.Window;
            this.tbAddCreateDate.Location = new System.Drawing.Point(114, 45);
            this.tbAddCreateDate.Name = "tbAddCreateDate";
            this.tbAddCreateDate.ReadOnly = true;
            this.tbAddCreateDate.Size = new System.Drawing.Size(171, 20);
            this.tbAddCreateDate.TabIndex = 7;
            // 
            // btnNotPrinted
            // 
            this.btnNotPrinted.Location = new System.Drawing.Point(291, 72);
            this.btnNotPrinted.Name = "btnNotPrinted";
            this.btnNotPrinted.Size = new System.Drawing.Size(245, 23);
            this.btnNotPrinted.TabIndex = 19;
            this.btnNotPrinted.Text = "Документ не надруковано";
            this.btnNotPrinted.UseVisualStyleBackColor = true;
            this.btnNotPrinted.Click += new System.EventHandler(this.btnNotPrinted_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Дата створення";
            // 
            // tbRemember
            // 
            this.tbRemember.BackColor = System.Drawing.SystemColors.Window;
            this.tbRemember.Location = new System.Drawing.Point(114, 19);
            this.tbRemember.Name = "tbRemember";
            this.tbRemember.ReadOnly = true;
            this.tbRemember.Size = new System.Drawing.Size(422, 20);
            this.tbRemember.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Нагадування";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btREZPrint);
            this.panel3.Controls.Add(this.btExit);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 659);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(542, 34);
            this.panel3.TabIndex = 3;
            // 
            // btREZPrint
            // 
            this.btREZPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btREZPrint.Location = new System.Drawing.Point(9, 5);
            this.btREZPrint.Name = "btREZPrint";
            this.btREZPrint.Size = new System.Drawing.Size(115, 26);
            this.btREZPrint.TabIndex = 3;
            this.btREZPrint.Text = "Друк переліку РЕЗ";
            this.btREZPrint.UseVisualStyleBackColor = true;
            this.btREZPrint.Visible = false;
            this.btREZPrint.Click += new System.EventHandler(this.btREZPrint_Click);
            // 
            // FBillMemoURCM
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(551, 699);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Name = "FBillMemoURCM";
            this.Text = "Службова  записка до ДРВ на виставлення рахунку за роботи РЧМ";
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btPrint;
      private System.Windows.Forms.Button btDRV;
      private System.Windows.Forms.Button btExit;
      private System.Windows.Forms.Button btDelete;
      private System.Windows.Forms.Panel panel9;
      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Button btSave;
      private System.Windows.Forms.Button btREZPrint;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbKod;
      private System.Windows.Forms.TextBox tbName;
      private System.Windows.Forms.TextBox tbMemoDocLink;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox tbUser;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox tbNumber;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbCurrentUser;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox tbEvent;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox tbDateTime;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.TextBox tbAddCreateDate;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.TextBox tbRemember;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Button btnChangeToPrinted;
      private System.Windows.Forms.Button btnNotPrinted;
      private System.Windows.Forms.Button btnAuto;
      private System.Windows.Forms.TextBox tbInvoice;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.DateTimePicker dtApplCreated;
      private System.Windows.Forms.ComboBox tbAddDocLinkList;
   }
}