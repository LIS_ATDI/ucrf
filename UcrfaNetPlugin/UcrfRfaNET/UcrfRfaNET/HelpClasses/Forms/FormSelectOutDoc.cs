﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.StationsStatus;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET
{
    public partial class FormSelectOutDoc : FBaseForm
    {
        private int __applID { get; set; }
        private int[] __idAddList { get; set; }
        
        private AppType __applType { get; set; }
        private bool _isEnableDateEnd = true;
        private List<string> str_human = new List<string>();
        //===================================================
        //private SelectDocs[] listTypeDoc;
        private Documents.Documents docum;
        public string TableName { get; set; }
        private ComboBoxDictionaryList<string, string> _docList = new ComboBoxDictionaryList<string, string>();
        private DateTime LastLicenceDate = DateTime.Now.AddYears(5);
        private AppType applType = AppType.AppUnknown;
        public bool IsEnableDateEnd
        {
            get
            {
                return _isEnableDateEnd;
            }
            set
            {
                if (_isEnableDateEnd != value)
                {
                    _isEnableDateEnd = value;
                    InvokeNotifyPropertyChanged("IsEnableDateEnd");
                }
            }
        }
        //---------------------------------------------------
        private DateTime prvFromDate = DateTime.Now;
        public DateTime pFromDate
        {
            get { return prvFromDate; }
            set
            {
                if (prvFromDate != value)
                {
                    string retVal = DocumentType;
                        if (retVal != DocType.DOZV_CANCEL && value > pToDate)
                            throw new Exception("The From Date can't be later then To Date");
                        else
                        {
                            prvFromDate = value;
                            InvokeNotifyPropertyChanged("pFromDate");
                        }
                }
            }
        }
        //---------------------------------------------------
        private DateTime prvToDate = DateTime.Now.AddDays(1);
        public DateTime pToDate
        {
            get { return prvToDate; }
            set
            {
                if (prvToDate != value)
                {
                    string retVal = DocumentType;
                    if (retVal != DocType.DOZV_CANCEL && value < pFromDate)
                        throw new Exception("The To Date can't be earlier then From Date");
                    else
                    {
                        prvToDate = value;
                        InvokeNotifyPropertyChanged("pToDate");
                    }
                }
            }
        }
        //---------------------------------------------------
        private string _fileNumber = "0";
        public string FileNumber
        {
            get { return _fileNumber; }
            set
            {
                if (_fileNumber != value)
                {
                    _fileNumber = value;
                    InvokeNotifyPropertyChanged("FileNumber");
                }
            }
        }
        //===================================================
        private string _documentType;
        public bool ShowBlankNumBox { get; set; }
        public string DocumentType
        {
            get { return _documentType; }
            set
            {
                if (_documentType != value)
                {
                    _documentType = value;
                    SetFirstDate();
                    SetEndDate(DateTime.Now);

                    ////////

                    if ((__applType == AppType.AppAR_3) && (_documentType == DocType.DOZV_AR3_SPEC))
                    {
                        List<int> applId = new List<int>();
                        if (__idAddList != null)
                            applId.AddRange(__idAddList);
                        applId.Add(__applID);

                        foreach (int id in applId)
                        {
                            IMRecordset rsStatLic = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                            rsStatLic.Select("ID,Abonent.ID");
                            rsStatLic.SetWhere("ID", IMRecordset.Operation.Eq, id);
                            try
                            {
                                DateTime lastDate = DateTime.Now.AddDays(-1).Date;
                                for (rsStatLic.Open(); !rsStatLic.IsEOF(); rsStatLic.MoveNext())
                                {
                                    int idStation = rsStatLic.GetI("Abonent.ID");
                                    if (idStation != IM.NullI)
                                    {
                                        ApplSource.AbonentStation station = new XICSM.UcrfRfaNET.ApplSource.AbonentStation();
                                        station.Load(idStation);
                                        SetEndDate(station.PermEndDate);

                                    }
                                }

                            }
                            finally
                            {
                                rsStatLic.Close();
                                rsStatLic.Destroy();
                            }
                        }
                    }
                    else if ((__applType == AppType.AppTR) && (_documentType == DocType.DOZV_TR_SPEC))
                    {
                        SetEndDate(pFromDate.AddMonths(36));
                    }

                    /////////////////////////////////


                    //-----
                    tbFileNumber.Enabled = false;
                    lblDoc.Enabled = false;                    
                    cmbBxDocument.Enabled = false;
                    cmbBxDocument.Visible = false;
                    checkAmateurDateEnd.Checked = false;
                    checkAmateurDateEnd.Visible = false;
                    dateEndComp.Visible = true;
                    dateEndComp.Enabled = true;
                    labelEndDate.Visible = true;
                    IsEnableDateEnd = true;


                    if ((applType == AppType.AppA3) && ((_documentType == DocType.GARM_CERT) || (_documentType == DocType.NOVIC_CERT)))
                    {
                        checkAmateurDateEnd.Visible = true;
                        checkAmateurDateEnd.Enabled = true;
                        dateEndComp.Visible = false;
                        dateEndComp.Enabled = false;
                        labelEndDate.Visible = false;
                        IsEnableDateEnd = false;
                    }

                    if ((applType == AppType.AppAR_3) && (_documentType == DocType.DOZV) || (_documentType == DocType.DOZV_TR_SPEC) || (_documentType == DocType.DOZV_AR3_SPEC))
                    {
                        tbFileNumber.Enabled = ShowBlankNumBox;
                    }
                    if ((applType == AppType.AppTR_З) && ((_documentType == DocType.DOZV) || (_documentType == DocType.VISN) || (_documentType == DocType.DOZV_TR_SPEC) || (_documentType == DocType.DOZV_AR3_SPEC)))
                    {
                        tbFileNumber.Enabled = ShowBlankNumBox;
                    }
                    if (applType == AppType.AppA3)
                    {
                        if ((_documentType == DocType.DOZV_SPS) || (_documentType == DocType.DOZV_MOVE))
                        {
                            tbFileNumber.Enabled = ShowBlankNumBox;
                            cmbBxDocument.Enabled = true;
                            cmbBxDocument.Items.Clear();
                            if ((_documentType == DocType.DOZV_SPS))
                            {
                                cmbBxDocument.Items.AddRange(AmateurAppl.ObjSpsList.ToArray());
                                if (cmbBxDocument.Items.Count > 0)
                                    cmbBxDocument.SelectedItem = cmbBxDocument.Items[0].ToString();
                            }

                            if ((_documentType == DocType.DOZV_MOVE))
                            {
                                cmbBxDocument.Items.AddRange(AmateurAppl.ObjRezList.ToArray());
                                if (cmbBxDocument.Items.Count > 0)
                                    cmbBxDocument.SelectedItem = cmbBxDocument.Items[0].ToString();
                            }
                        }
                        else if ((_documentType == DocType.DOZV_APC) || (_documentType == DocType.DOZV_APC50) || (_documentType == DocType.DOZV_RETR))
                        {
                            tbFileNumber.Enabled = ShowBlankNumBox;
                        }
                    }

                    cmbBxDocument.Visible = cmbBxDocument.Enabled;
                    lblDoc.Visible = cmbBxDocument.Enabled;
                    lblDoc.Enabled = cmbBxDocument.Enabled;

                    tbFileNumber.Visible = tbFileNumber.Enabled;
                    lblFileNum.Enabled = tbFileNumber.Enabled;
                    lblFileNum.Visible = tbFileNumber.Enabled;
                }
            }
        }      

        private string amatourCert;
        private string amatourTypeStat;
        /// <summary>
        /// Конструктор для аматоров
        /// </summary>               
        public FormSelectOutDoc(RecordPtr recordID, bool isTech, int _applID, AppType _applType, bool isPacketPrint, string cert, string typeStat,params int[] idAddList)
            : this(recordID, isTech, _applID, _applType, isPacketPrint, idAddList)
        {
            TableName = recordID.Table;
            __applID = _applID;
            __applType = _applType;
            __idAddList = idAddList;
            checkAmateurDateEnd.Text = CLocaliz.TxT("Turn right <Expiry date>");
            checkAmateurDateEnd.Checked = false;
            checkAmateurDateEnd.Visible = false;
            dateEndComp.Visible = true;
            dateEndComp.Enabled = true;
            labelEndDate.Visible = true;
            IsEnableDateEnd = true;
            ///

            StationStatus wf = new StationStatus();
            StationStatus.ETypeEvent typeEvent = isPacketPrint
                                                    ? StationStatus.ETypeEvent.PacketPrintDoc
                                                    : StationStatus.ETypeEvent.ApplPrintDoc;
            amatourTypeStat = typeStat;
            amatourCert = cert;
            ShowBlankNumBox = true;
            List<string> accesedDocs = idAddList.Count() == 0 ? wf.AccessedDocuments(typeEvent, _applID) : wf.AccessedDocuments(typeEvent, idAddList);            
            if (applType == AppType.AppA3)
            {
                
                ClearDoc();
                if (!accesedDocs.Contains(DocType.GARM_CERT))
                {
                    accesedDocs.Add(DocType.GARM_CERT);
                }
                if (!accesedDocs.Contains(DocType.NOVIC_CERT))
                    accesedDocs.Add(DocType.NOVIC_CERT);
                if (!accesedDocs.Contains(DocType.DOZV_RETR))
                    accesedDocs.Add(DocType.DOZV_RETR);
                if (!accesedDocs.Contains(DocType.DOZV_APC))
                    accesedDocs.Add(DocType.DOZV_APC);
                if (!accesedDocs.Contains(DocType.DOZV_APC50))
                    accesedDocs.Add(DocType.DOZV_APC50);
                if (!accesedDocs.Contains(DocType.DOZV_SPS))
                    accesedDocs.Add(DocType.DOZV_SPS);
                if (!accesedDocs.Contains(DocType.DOZV_MOVE))
                    accesedDocs.Add(DocType.DOZV_MOVE);
                Dictionary<string, string> typeDict = EriFiles.GetEriCodeAndDescr("AmateurType");
                int swich = typeDict.Values.ToList().IndexOf(amatourTypeStat);
                if (swich == 1)
                    AddNewDoc(DocType.DOZV_RETR, accesedDocs);
                else if (swich == 0)
                    AddNewDoc(DocType.DOZV_APC, accesedDocs);
                else if (swich == 3)
                    AddNewDoc(DocType.DOZV_APC50, accesedDocs);

                if (amatourCert.Trim() == "HAREC")
                    AddNewDoc(DocType.GARM_CERT, accesedDocs);
                else if (amatourCert.Trim() == "NOVICE")
                    AddNewDoc(DocType.NOVIC_CERT, accesedDocs);
                AddNewDoc(DocType.DOZV_SPS, accesedDocs);
                AddNewDoc(DocType.DOZV_MOVE, accesedDocs);                
            }
            buttonOK.Enabled = true;//(_docList.Count > 0); 
        }

        //===================================================
        /// <summary>
        /// Форма выбора документа
        /// </summary>
        public FormSelectOutDoc(RecordPtr recordID, bool isTech, int _applID, AppType _applType, bool isPacketPrint, params int[] idAddList)
        {
            InitializeComponent();
            TableName = recordID.Table;
            __applID = _applID;
            __applType = _applType;
            __idAddList = idAddList;
            ShowBlankNumBox = true; 

            applType = _applType;

            StationStatus wf = new StationStatus();
            docum = new Documents.Documents();
            StationStatus.ETypeEvent typeEvent = (isPacketPrint == true)
                                                    ? StationStatus.ETypeEvent.PacketPrintDoc
                                                    : StationStatus.ETypeEvent.ApplPrintDoc;
            List<string> accesedDocs = idAddList.Count() == 0 ? wf.AccessedDocuments(typeEvent, _applID) : wf.AccessedDocuments(typeEvent, idAddList);

            Binding bind = new Binding("Value", this, "pFromDate", true, DataSourceUpdateMode.OnValidation, "null");
            bind.BindingComplete += new BindingCompleteEventHandler(OnBindingComplete);
            dateStartComp.DataBindings.Add(bind);

            bind = new Binding("Value", this, "pToDate", true, DataSourceUpdateMode.OnValidation, "null");
            bind.BindingComplete += new BindingCompleteEventHandler(OnBindingComplete);
            dateEndComp.DataBindings.Add(bind);
            dateEndComp.DataBindings.Add("Enabled", this, "IsEnableDateEnd");
            
            cmbBxDocument.Visible = false;
            cmbBxDocument.Enabled = false;


            /*
            //===============
            //Временное разрешение на печать, когда не выбрано ни одного РЧП
            if (accesedDocs.Count==0)
            {
                //accesedDocs = new List<string>();
                accesedDocs = wf.AccessedDocuments(StationStatus.ETypeEvent.AttachDoc, idAddList);
                string strTypeEvent = StationStatus.ETypeEvent.AttachDoc.ToString();
                UcrfDepartment curDept = CUsers.GetCurDepartment();
                Documents.Documents docTypes = new Documents.Documents();
                docTypes.LoadWfConfiguration();
                foreach (DocType dt in docTypes._docTypes.Values)
                {
                    /*
                    if ((curDept == UcrfDepartment.URZP && dt.urzpAccessible
                        || curDept == UcrfDepartment.URCP && dt.urcpAccessible
                        || curDept == UcrfDepartment.Branch && dt.branchAccessible)
                        && (dt.packetAccessible))
                    {

                            if (!accesedDocs.Contains(dt.code))
                                {
                                    accesedDocs.Add(dt.code);
                                    
                                }
                    }
                     */


            /*

                        //MessageBox.Show(dt.packetAccessible+" "+dt.code);
                        //if (((curDept == UcrfDepartment.URZP && dt.urzpAccessible && dt.packetAccessible)
                         //                   || (curDept == UcrfDepartment.URCP && dt.urcpAccessible && dt.packetAccessible)
                         //                   || (curDept == UcrfDepartment.Branch && dt.branchAccessible && dt.packetAccessible)
                         //                   )
                         //   || (!dt.urzpAccessible && !dt.urcpAccessible && !dt.branchAccessible && dt.packetAccessible))
                        //{
                            if  (dt.packetAccessible)
                            {
                            AddNewDoc(dt.code, accesedDocs);
                        }

                }
            }
            //==============================
    */

            
            if ((applType == AppType.AppTR) && (FPacket._PacketType== EPacketType.PckSetCallSign))
            {
                if (_applID == 0)
                    AddNewDoc(DocType.LYST_TR_CALLSIGN, accesedDocs);
            }

            if ((applType == AppType.AppTR_З) && (FPacket._PacketType == EPacketType.PckVISN))
            {
                accesedDocs.Add("VISN");
                AddNewDoc(DocType.VISN, accesedDocs);
            }
            //if ((applType == AppType.AppTR_З) && (_documentType == DocType.VISN))
                //AddNewDoc(DocType.VISN, accesedDocs);
            //if ((applType == AppType.AppTR_З) && (_documentType == DocType.DOZV))
                //AddNewDoc(DocType.DOZV, accesedDocs);

            if ((applType == AppType.AppZRS)  && (FPacket._PacketType== EPacketType.PckSetCallSign))
            {
                if (_applID==0)
                    AddNewDoc(DocType.LYST_ZRS_CALLSIGN, accesedDocs);
            }
            

            


         

            if (applType == AppType.AppTR)
            {
                AddNewDoc(DocType.DOZV_TR_SPEC, accesedDocs);
            }

            //List<SelectDocs> typeDocsList = new List<SelectDocs>();
            if ((applType != AppType.AppAR_3) && (applType != AppType.AppAP3))
            {
                if (applType != AppType.AppVP)
                {
                    AddNewDoc(DocType.DOZV, accesedDocs);
                    AddNewDoc(DocType.VISN, accesedDocs);
                    AddNewDoc(DocType.VISN_NR, accesedDocs);
                    AddNewDoc(DocType.DOZV, accesedDocs);
                    AddNewDoc(DocType.DOZV_OPER, accesedDocs);
                    AddNewDoc(DocType.PCR, accesedDocs);
                    AddNewDoc(DocType.VVP, accesedDocs);
                    if ((_applID != 0) && (_applID != IM.NullI))
                        AddNewDoc(DocType.DRV, accesedDocs);
                    AddNewDoc(DocType.VVE, accesedDocs);
                    AddNewDoc(DocType.LYST, accesedDocs);
                    AddNewDoc(DocType._0159, accesedDocs);
                    AddNewDoc(DocType.FYLIA, accesedDocs);
                    if ((_applID != 0) && (_applID != IM.NullI))
                        AddNewDoc(DocType.PCR2, accesedDocs);
                }
                else if (applType == AppType.AppVP)
                {
                    AddNewDoc(DocType.DOZV, accesedDocs);
                }
            }
            else if (applType == AppType.AppAR_3)
            {
                AddNewDoc(DocType.DOZV_FOREIGHT, accesedDocs);
                AddNewDoc(DocType.DOZV_TRAIN, accesedDocs);
                AddNewDoc(DocType.DOZV, accesedDocs);
                AddNewDoc(DocType.REESTR_VID, accesedDocs);

                AddNewDoc(DocType.DOZV_AR3_SPEC, accesedDocs);
            }
            else if (applType == AppType.AppAP3)
            {
                AddNewDoc(DocType.DOZV_TRAIN, accesedDocs);
                AddNewDoc(DocType.REESTR_VID, accesedDocs);
            }

            if (applType == AppType.AppA3)
            {                
                ClearDoc();
                if (!accesedDocs.Contains(DocType.GARM_CERT))
                    accesedDocs.Add(DocType.GARM_CERT);
                if (!accesedDocs.Contains(DocType.NOVIC_CERT))
                    accesedDocs.Add(DocType.NOVIC_CERT);
                if (!accesedDocs.Contains(DocType.DOZV_RETR))
                    accesedDocs.Add(DocType.DOZV_RETR);
                if (!accesedDocs.Contains(DocType.DOZV_APC))
                    accesedDocs.Add(DocType.DOZV_APC);
                if (!accesedDocs.Contains(DocType.DOZV_APC50))
                    accesedDocs.Add(DocType.DOZV_APC50);
                Dictionary<string, string> typeDict = EriFiles.GetEriCodeAndDescr("AmateurType");
                int swich = typeDict.Values.ToList().IndexOf(amatourTypeStat);                
                if (swich == 1)
                    AddNewDoc(DocType.DOZV_RETR, accesedDocs);
                else if (swich == 0)
                    AddNewDoc(DocType.DOZV_APC, accesedDocs);
                else if (swich == 2)
                    AddNewDoc(DocType.DOZV_APC50, accesedDocs);
                
                if (amatourCert == "HAREC")
                    AddNewDoc(DocType.GARM_CERT, accesedDocs);
                else if (amatourCert == "NOVICE")
                    AddNewDoc(DocType.NOVIC_CERT, accesedDocs);
                AddNewDoc(DocType.DOZV_SPS, accesedDocs);
                AddNewDoc(DocType.DOZV_MOVE, accesedDocs);
            }
            AddNewDoc(DocType.DOZV_CANCEL, accesedDocs);
            AddNewDoc(DocType.GIVE_LICENCE, accesedDocs);
            AddNewDoc(DocType.MESS_DOZV_CANCEL, accesedDocs);

            // Инициализируем ComboBox
            _docList.InitComboBox(cbDocumentName);
            bind = new Binding("SelectedValue", this, "DocumentType", true, DataSourceUpdateMode.OnPropertyChanged);
            cbDocumentName.DataBindings.Add(bind);
            bind = new Binding("Text", this, "FileNumber", true, DataSourceUpdateMode.OnPropertyChanged);
            tbFileNumber.DataBindings.Add(bind);
            buttonOK.Enabled = (_docList.Count > 0);           
           
            pFromDate = DateTime.Now;
            SetEndDate(DateTime.Now);
            CLocaliz.TxT(this);  //Локализуем форму
            //============================================
            // Поиск минимальную дату лицензии
            bool bShowMessage = true;
            List<int> applId = new List<int>();
            if (idAddList != null)
                applId.AddRange(idAddList);
            applId.Add(_applID);
            if (_applType == AppType.AppAR_3)
            {

                foreach (int id in applId)
                {
                    IMRecordset rsStatLic = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                    rsStatLic.Select("ID,Abonent.ID");
                    rsStatLic.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    try
                    {
                        DateTime lastDate = DateTime.Now.AddDays(-1).Date;
                        for (rsStatLic.Open(); !rsStatLic.IsEOF(); rsStatLic.MoveNext())
                        {
                            int idStation = rsStatLic.GetI("Abonent.ID");
                            if(idStation != IM.NullI)
                            {
                                ApplSource.AbonentStation station = new XICSM.UcrfRfaNET.ApplSource.AbonentStation();
                                station.Load(idStation);
                                SetEndDate(station.PermEndDate);
                                DateTime[] lstEndPermDate = station.GetEndPermDateBaseStation();
                                if(lstEndPermDate.Length > 0)
                                {
                                    DateTime stopDate = lstEndPermDate.Min();
                                    if ((stopDate >= DateTime.Now) &&
                                        ((stopDate < lastDate) || (lastDate == DateTime.Now.AddDays(-1).Date)))
                                        lastDate = stopDate;
                                }

                            }
                        }
                        if (lastDate != DateTime.Now.AddDays(-1).Date)
                            LastLicenceDate = lastDate;
                    }
                    finally
                    {
                        rsStatLic.Close();
                        rsStatLic.Destroy();
                    }
                }
            }
            else
            {
                    foreach (int id in applId)
                    {
                        IMRecordset rsStatLic = new IMRecordset(PlugTbl.itblXnrfaApplLic, IMRecordset.Mode.ReadOnly);
                        rsStatLic.Select("ID,APPL_ID,LIC_ID");
                        rsStatLic.SetWhere("APPL_ID", IMRecordset.Operation.Eq, id);
                        try
                        {
                            DateTime currDate = DateTime.Now.AddDays(-1).Date;
                            DateTime lastDate = currDate;
                            for (rsStatLic.Open(); !rsStatLic.IsEOF(); rsStatLic.MoveNext())
                            {
                                int idLicence = rsStatLic.GetI("LIC_ID");
                                IMRecordset rsLic = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                                rsLic.Select("STOP_DATE,END_DATE");
                                rsLic.SetWhere("ID", IMRecordset.Operation.Eq, idLicence);
                                try
                                {
                                    rsLic.Open();
                                    if (!rsLic.IsEOF())
                                    {
                                        DateTime stopDate = rsLic.GetT("STOP_DATE");
                                        DateTime endDate = rsLic.GetT("END_DATE");
                                        if (endDate != IM.NullT && endDate < stopDate)
                                            stopDate = endDate;
                                        if ((stopDate >= DateTime.Now) &&
                                            ((stopDate < lastDate) || (lastDate == currDate)))
                                            lastDate = stopDate;
                                        bShowMessage = false;
                                    }
                                }
                                finally
                                {
                                    rsLic.Close();
                                    rsLic.Destroy();
                                }
                            }
                            if (lastDate != DateTime.Now.AddDays(-1).Date)
                                LastLicenceDate = lastDate;
                        }
                        finally
                        {
                            rsStatLic.Close();
                            rsStatLic.Destroy();
                        }
                    }
               
            }
            if (applType != AppType.AppVP)
            {
                if ((bShowMessage == true) && (recordID.Id != 0) && (isTech == false) && (recordID.Table != PlugTbl.XfaAbonentStation) && (recordID.Table != "XFA_AMATEUR"))
                    MessageBox.Show(CLocaliz.TxT("Licences are not found"), CLocaliz.TxT("Warning"));
            }
        }      
        //==================================================
        /// <summary>
        /// Добавляет в список документ если он доступен основываясь на статусе текущих станций
        /// </summary>
        /// <param name="typeOfDoc">Тип документа, который необходимо добавит</param>
        /// <param name="accesedDocs">Типы разрешенных документов</param>
        private void AddNewDoc(string typeOfDoc, List<string> accesedDocs)
        {
            if (accesedDocs.Contains(typeOfDoc) == true)
            {
                string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                if (!str_human.Contains(humanDocName))
                {
                    str_human.Add(humanDocName);
                    _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                }
               
            }
            else if (typeOfDoc == "DOZV_FOREIGHT")
            {
                string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                if (!str_human.Contains(humanDocName))
                {
                    str_human.Add(humanDocName);
                    _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                }
            }
            else if (typeOfDoc == DocType.LYST_TR_CALLSIGN.ToString())
            {
                    string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                    if (!str_human.Contains(humanDocName))
                    {
                        str_human.Add(humanDocName);
                        _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                    }
            }
            else if (typeOfDoc == DocType.LYST_ZRS_CALLSIGN.ToString())
            {
                string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                if (!str_human.Contains(humanDocName))
                {
                    str_human.Add(humanDocName);
                    _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                }
            }
            else if (((__applType == AppType.AppAR_3) && (typeOfDoc == "DOZV_TRAIN")) || ((__applType == AppType.AppAP3) && (typeOfDoc == "DOZV_TRAIN")))
            {
                List<int> tmp_l = new List<int>();
                if ((__idAddList.Count() == 0) && (__applID > 0)) { tmp_l.Add(__applID); __idAddList = tmp_l.ToArray(); }

                foreach (int appl_item in __idAddList)
                {
                    IMRecordset rsStatLic = new IMRecordset(PlugTbl.APPL, IMRecordset.Mode.ReadOnly);
                    rsStatLic.Select("ID,Abonent.ID");
                    rsStatLic.SetWhere("ID", IMRecordset.Operation.Eq, appl_item);


                    rsStatLic.Open();
                    if (!rsStatLic.IsEOF())
                    {
                        int idStation = rsStatLic.GetI("Abonent.ID");
                        if (idStation != IM.NullI)
                        {
                            ApplSource.AbonentStation station = new XICSM.UcrfRfaNET.ApplSource.AbonentStation();
                            if (appl_item != IM.NullI) station.Load(idStation);
                            if (station != null)
                            {
                                if ((station.Standard == CRadioTech.YKX) || (station.Standard == CRadioTech.KX))
                                {
                                    string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                                    if (!str_human.Contains(humanDocName))
                                    {
                                        str_human.Add(humanDocName);
                                        _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                                    }
                                }
                            }
                        }
                    }

                }
            }
            if ((__applType == AppType.AppAR_3) && (typeOfDoc == DocType.DOZV_AR3_SPEC.ToString()))
            {
                string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                if (!str_human.Contains(humanDocName))
                {
                    str_human.Add(humanDocName);
                    _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                }
            }
            if ((__applType == AppType.AppTR)  && (typeOfDoc == DocType.DOZV_TR_SPEC.ToString()))
            {
                string humanDocName = docum.GetHumanNameOfDoc(typeOfDoc).TrimStart().TrimEnd();
                if (!str_human.Contains(humanDocName))
                {
                    str_human.Add(humanDocName);
                    _docList.Add(new ComboBoxDictionary<string, string>(typeOfDoc, humanDocName));
                }
            }
           
        }

        /// <summary>
        /// Чистит документ
        /// </summary>        
        private void ClearDoc()
        {
            _docList.Clear();
        }

        //===================================================
        /// <summary>
        /// Возвращает выбраный тип документа
        /// </summary>
        /// <returns>выбраный тип документа</returns>
        public string getDocType()
        {
            return DocumentType;
        }
        //===================================================
        /// <summary>
        /// Возвращает обьект документа
        /// </summary>
        /// <returns>Возвращает дату начала</returns>
        public string getObjDoc()
        {
            if (cmbBxDocument.SelectedItem == null)
                return "";
            return cmbBxDocument.SelectedItem.ToString();
        }

        //===================================================
        /// <summary>
        /// Возвращает обьект документа
        /// </summary>
        /// <returns>Возвращает дату начала</returns>
        public string GetNumber()
        {
            return tbFileNumber.Text;
        }



        //===================================================
        /// <summary>
        /// Возвращает дату начала
        /// </summary>
        /// <returns>Возвращает дату начала</returns>
        public DateTime getStartDate()
        {
            return pFromDate;
        }
        //===================================================
        /// <summary>
        /// Возвращает дату окончания
        /// </summary>
        /// <returns>Возвращает дату окончания</returns>
        public DateTime getEndDate()
        {
            return pToDate;
        }

        /// <summary>
        /// Установлює список об'єктів документів
        /// </summary>
        /// <param name="objArr"></param>
        public void SetObj(List<object> objArr) { }
        //===================================================
        /// <summary>
        /// Устанавливаем конечную дату документа
        /// </summary>
        private void SetEndDate(DateTime toDate_SPEC)
        {
            if (cbDocumentName.SelectedIndex > -1)
            {
                dateEndComp.Enabled = true;
                IsEnableDateEnd = true;

                string retVal = DocumentType;
                if (retVal == DocType.VISN)
                {
                    int month = 6;
                    if ((applType == AppType.AppTV2) || (applType == AppType.AppTV2d))
                        month = 12;
                    pToDate = pFromDate.AddMonths(month);
                }
                else if ((retVal == DocType.DOZV) || (retVal == DocType.DOZV_TRAIN) || (retVal == DocType.DOZV_FOREIGHT))
                    pToDate = LastLicenceDate;
                else if ((retVal == DocType.VISN_NR) && ((applType == AppType.AppTV2) || (applType == AppType.AppTV2d)))
                    dateEndComp.Enabled = false;
                else if (retVal == DocType.DOZV_CANCEL)
                {
                    dateEndComp.Enabled = false;
                }
                else if (retVal == DocType.DOZV_AR3_SPEC)
                {
                    pToDate = toDate_SPEC;
                }
                else if (retVal == DocType.DOZV_TR_SPEC)
                {
                    pToDate = toDate_SPEC;
                }
                else
                {
                    pToDate = pFromDate.AddMonths(1);
                }

                labelEndDate.Enabled = dateEndComp.Enabled;
            }
        }
        //===================================================
        /// <summary>
        /// Устанавливаем начальную дату документа
        /// </summary>
        private void SetFirstDate()
        {
            if (cbDocumentName.SelectedIndex > -1)
            {
                dateEndComp.Enabled = true;
                IsEnableDateEnd = true;
                string retVal = DocumentType;
                if (retVal == DocType.DOZV_CANCEL)
                {
                    pFromDate = DateTime.Now.AddDays(11);
                }
                label2.Enabled = dateStartComp.Enabled;
            }
        }
        //===================================================
        /// <summary>
        /// Обработка ошибок Binding
        /// </summary>
        void OnBindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteState != BindingCompleteState.Success)
                MessageBox.Show(e.ErrorText, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (ParamIsOk() == false)
                DialogResult = DialogResult.None;
        }
        //===================================================
        /// <summary>
        /// Checks of the parameters
        /// </summary>
        /// <returns>true - OK, false - error</returns>
        private bool ParamIsOk()
        {
            bool retVal = true;
            string errorMessage = "";
            if (cbDocumentName.SelectedIndex < 0)
                errorMessage = CLocaliz.TxT("Type of the document is not selected.");

            if (!string.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage, CLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                retVal = false;
            }
            return retVal;
        }

        private void checkAmateurDateEnd_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAmateurDateEnd.Checked)
            {
                dateEndComp.Visible = true;
                dateEndComp.Enabled = true;
                labelEndDate.Visible = true;
                IsEnableDateEnd = true;

            }
            else
            {
                dateEndComp.Visible = false;
                dateEndComp.Enabled = false;
                labelEndDate.Visible = false;
                IsEnableDateEnd = false;
            }
        }

    }
}
