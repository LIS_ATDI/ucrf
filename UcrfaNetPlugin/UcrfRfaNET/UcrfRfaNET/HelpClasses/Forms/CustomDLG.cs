﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public enum DialogResultMessageBoxButtons
    {
        None = -1,
        // Сводка:
        //     Нжата кнопка "Да".
        OK = 0,
        //
        // Сводка:
        //     Нажата кнопка "Нет".
        No = 1,
        //
        // Сводка:
        //      Нажата кнопка "Просмотр".
        View = 2,
        //
        // Замещение
        //
        Override,
        //
        // режим добавления
        //
        Append,
        //
        // отмена операции
        //
        CancelOperation

    }

    public partial class CustomDLG : Form
    {

        public  static DialogResultMessageBoxButtons CurrentPress = DialogResultMessageBoxButtons.None;


        public CustomDLG(string Messages, string Caption)
        {
          
            InitializeComponent();
            Show(Messages,Caption);
        }

 
        public  void Show(string text, string caption)
        {
            button1.Text = CLocaliz.TxT("YES");
            button2.Text = CLocaliz.TxT("NO");
            MemoMess.Text = text;
            this.Text = caption;
            CurrentPress = DialogResultMessageBoxButtons.None;
            this.ShowDialog();
         
        }

        public DialogResultMessageBoxButtons GetStatusPress()
        {
            return CurrentPress;
        }

        private   void button1_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.OK;
            Close();
        }

        private  void button2_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.No;
            Close();
        }


        private void CustomDialogBox_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

    }
}
