﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GridCtrl;
using XICSM.UcrfRfaNET.Extension;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FBillMemoURCM : FBaseForm
   {
      protected Grid grid2 = new GridCtrl.Grid();

      protected CDRVMemoURCM drvMemo = null;   // Обьект, который управляет заявкой

      public FBillMemoURCM(CDRVMemoURCM _drvMemo, DRVStatus st)
      {
         if (_drvMemo == null)
            throw new Exception("The base object can't be NULL.");
         drvMemo = _drvMemo;
         
         InitializeComponent();
         InitGrids();         
         

         IMRecordset r = new IMRecordset(ICSMTbl.itblUsers, IMRecordset.Mode.ReadOnly);
         r.Select("ID,NAME,REGIST_NUM");
         r.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.ownerId);
         try
         {
            r.Open();
            if (!r.IsEOF())
            {
               tbName.Text = r.GetS("NAME");
               tbKod.Text = r.GetS("REGIST_NUM");
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }


         dtApplCreated.DataBindings.Add("Value", drvMemo, CDRVMemoURCM.FieldCreateDate);
         tbMemoDocLink.Text = drvMemo.docPath;
         tbUser.Text = HelpFunction.getUserFIOByID(drvMemo.emplId);
         tbNumber.Text = drvMemo.number;
         tbAddCreateDate.Text = drvMemo.addCreateDate == IM.NullT ? "" : drvMemo.addCreateDate.ToShortDateString();
         if ((drvMemo.addDocPathList != null) && (drvMemo.addDocPathList.Count > 0))
         {
             tbAddDocLinkList.Items.Clear();
             for (int i = 0; i < drvMemo.addDocPathList.Count; i++) { tbAddDocLinkList.Items.Add(drvMemo.addDocPathList[i]); }
         }
         tbDateTime.Text = drvMemo.DateResponse.ToStringNullT();
         tbCurrentUser.Text = HelpFunction.getUserFIOByID(drvMemo.EmplDrvId);

         if (drvMemo.hasDiff)
         {
             tbRemember.Text = "Потрібно створити новий додаток до рахунку";
             tbRemember.BackColor = Color.LightSalmon;
         }
         else
         {
             tbRemember.Text = "Не потрібно створювати новий додаток до рахунку";
             tbRemember.BackColor = Color.LightGreen;
         }
         UpdateStatus();
         
      }
      
      void InitHeaderCell(Cell c)
      {
         c.FontStyle = FontStyle.Bold;
         c.HorizontalAlignment = HorizontalAlignment.Center;
         c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
         c.BackColor = System.Drawing.Color.DarkGray;
         c.TextHeight = 11;
         c.CanEdit = false;
      }

      void InitGrid(Grid grid, Control parent)
      {
         // grid = new GridCtrl.Grid();
         grid.AllowDrop = true;
         grid.BackColor = System.Drawing.SystemColors.Window;
         grid.EditMode = true;
         grid.FirstColumnWidth = 120;
         grid.GridLine = GridCtrl.Grid.GridLineStyle.Both;
         grid.GridLineColor = System.Drawing.Color.Black;
         grid.GridOpacity = 100;
         grid.Image = null;
         grid.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;
         grid.LockUpdates = false;
         grid.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;
         grid.ToolTips = true;
         grid.Parent = parent;
         grid.Dock = DockStyle.Fill;
         grid.ReadOnly = true;

         Row r = grid.AddRow();
         Cell c = grid.AddColumn(grid.GetRowCount() - 1, "Стаття");
         InitHeaderCell(c);
         c = grid.AddColumn(grid.GetRowCount() - 1, "Кількість");
         InitHeaderCell(c);

         c = grid.AddColumn(grid.GetRowCount() - 1, "Технологія");
         InitHeaderCell(c);

         foreach (MemoLines.MemoLine line in drvMemo.art_lines.lines)
         {
            grid.AddRow();
            c = grid.AddColumn(grid.GetRowCount() - 1, line.article);
            c.TextHeight = 11;
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c = grid.AddColumn(grid.GetRowCount() - 1, line.workCount.ToString());
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
            c = grid.AddColumn(grid.GetRowCount() - 1, line.radioTech);
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
         }
      }

      /// <summary>
      /// Инициализация всех гридов
      /// </summary>
      void InitGrids()
      {
         InitGrid(grid2, groupBox2);    
      }

      private void btSave_Click(object sender, EventArgs e)
      {
         drvMemo.status = DRVStatus.saved;
         SaveApp();
         UpdateStatus();
         IM.RefreshQueries(PlugTbl.XvMonitorUsers);
         IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }

      private void btPrint_Click(object sender, EventArgs e)
      {
         drvMemo.PrintDoc();
         tbMemoDocLink.Text = drvMemo.docPath;
         IM.RefreshQueries(PlugTbl.XvMonitorUsers);
         IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }

      private void btDelete_Click(object sender, EventArgs e)
      {
         if (drvMemo.DeleteApp())
            this.DialogResult = DialogResult.Cancel;
         
         IM.RefreshQueries(PlugTbl.XvMonitorUsers);
         IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }

      private void UpdateAll(bool auto)
      {
          if (string.IsNullOrEmpty(tbMemoDocLink.Text))
          {
              MessageBox.Show("Відсутній номер службової записки.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return;
          }
          if (MessageBox.Show("Відправити службову записку до ДРВ?", "Підтвердження відправки", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
              if (drvMemo.AppToDRV(auto))
                  this.DialogResult = DialogResult.OK;
              else
                  this.btSave.Focus();
          }
          UpdateStatus();
      }

      private void btDRV_Click(object sender, EventArgs e)
      {
          if (FPacket.CountAddressChangeMass>0)
              MessageBox.Show(CLocaliz.TxT("The package found RFI that have a need to change the address, in the count:") + FPacket.CountAddressChangeMass);
          UpdateAll(false);
          IM.RefreshQueries(PlugTbl.XvMonitorUsers);
          IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
         
      }

      private void btnAuto_Click(object sender, EventArgs e)
      {
          if (FPacket.CountAddressChangeMass > 0)
              MessageBox.Show(CLocaliz.TxT("The package found RFI that have a need to change the address, in the count:") + FPacket.CountAddressChangeMass);
          UpdateAll(true);
          IM.RefreshQueries(PlugTbl.XvMonitorUsers);
          IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }
       

     
      /// <summary>
      /// Сохраненние заявки
      /// </summary>
      public void SaveApp()
      {
         drvMemo.number = tbNumber.Text;         
         drvMemo.SaveApp();
      }

      public void UpdateStatus()
      {
          btDRV.Enabled = false;
          btPrint.Enabled = false;
          btDelete.Enabled = false;
          btSave.Enabled = false;
          btnAuto.Enabled = false;
          btREZPrint.Enabled = false;
          btnChangeToPrinted.Enabled = false;
          btnNotPrinted.Enabled = false;
          dtApplCreated.Enabled = false;
          switch (drvMemo.status)
          {
              case DRVStatus.newapl:
                  btSave.Enabled = true;
                  //dtApplCreated.Enabled = true;
                  break;
              case DRVStatus.saved:
                  btSave.Enabled = true;
                  //dtApplCreated.Enabled = true;
                  btnAuto.Enabled = true;
                  btPrint.Enabled = true;
                  btDelete.Enabled = true;
                  btDRV.Enabled = true;
                  btREZPrint.Enabled = true;
                  break;
              case DRVStatus.sent:
                  btREZPrint.Enabled = true;
                  break;
              case DRVStatus.generated:
                  btnChangeToPrinted.Enabled = true;
                  btnNotPrinted.Enabled = true;
                  break;
              case DRVStatus.auto_gener:
                  btDelete.Enabled = true;
                  btDRV.Enabled = true;
                  break;
          }
          tbEvent.Text = drvMemo.status.FromEri();
          tbInvoice.Text = drvMemo.StatusInvoice.FromEri();

          // #7294 сделать всегда неактивными кнопки «Авто» и «Друк переліку» для филиальных пользователей
          if (CUsers.CurDepartment == UcrfDepartment.Branch)
          {
              btnAuto.Enabled = false;
              btREZPrint.Enabled = false;
          }

      }

      private void btREZPrint_Click(object sender, EventArgs e)
      {
         drvMemo.PrintAddDoc(false);
         IM.RefreshQueries(PlugTbl.XvMonitorUsers);
         IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }

      private void tbMemoDocLink_MouseDoubleClick(object sender, MouseEventArgs e)
      {
         
          try
         {
            string path = drvMemo.docFullPath;
            if (!string.IsNullOrEmpty(path))
               System.Diagnostics.Process.Start(path);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message + ex.StackTrace, "Error");
         }
         
           
      }

      private void tbAddDocLink_MouseDoubleClick(object sender, MouseEventArgs e)
      {
         try
         {
            string path = drvMemo.addDocFullPath;
            if (!string.IsNullOrEmpty(path))
               System.Diagnostics.Process.Start(path);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message + ex.StackTrace, "Error");
         }
      }
      /// <summary>
      /// Поменять статус заявки на "Документ напечатан"
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnChangeToPrinted_Click(object sender, EventArgs e)
      {
          if((drvMemo.status == DRVStatus.generated) && 
             (MessageBox.Show("Буде змінено статус заявки на 'Документ надруковано'. Продовжити?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
          {
              const DRVStatus tmpStatus = DRVStatus.printed;
              CDRVMemoURCM.UpdateApplStatus(drvMemo.memoId, tmpStatus);
              drvMemo.status = tmpStatus;
              UpdateStatus();
          }
          IM.RefreshQueries(PlugTbl.XvMonitorUsers);
          IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }
      /// <summary>
      /// Поменять статус заявки на "Документ не напечатан"
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnNotPrinted_Click(object sender, EventArgs e)
      {
          if ((drvMemo.status == DRVStatus.generated) &&
             (MessageBox.Show("Буде змінено статус заявки на 'Документ не надруковано'. Продовжити?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
          {
              const DRVStatus tmpStatus = DRVStatus.notprinted;
              CDRVMemoURCM.UpdateApplStatus(drvMemo.memoId, tmpStatus);
              drvMemo.status = tmpStatus;
              UpdateStatus();
          }
          IM.RefreshQueries(PlugTbl.XvMonitorUsers);
          IM.RefreshQueries(PlugTbl.itblXnrfaDrvApplURCM);
      }

      private void tbAddDocLinkList_SelectedIndexChanged(object sender, EventArgs e)
      {
          try
          {
              string path = ((cbItem)(tbAddDocLinkList.SelectedItem)).path_full.ToString(); 
              if (!string.IsNullOrEmpty(path))
                  System.Diagnostics.Process.Start(path);
          }
          catch (Exception ex)
          {
              MessageBox.Show(ex.Message + ex.StackTrace, "Error");
          }
      }      
   }
}
