﻿namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
   partial class FSectorRemove
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.button1 = new System.Windows.Forms.Button();
          this.cbSecs = new System.Windows.Forms.ComboBox();
          this.label1 = new System.Windows.Forms.Label();
          this.SuspendLayout();
          // 
          // button1
          // 
          this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.button1.Location = new System.Drawing.Point(189, 40);
          this.button1.Name = "button1";
          this.button1.Size = new System.Drawing.Size(122, 25);
          this.button1.TabIndex = 0;
          this.button1.Text = "Вибрати";
          this.button1.UseVisualStyleBackColor = true;
          // 
          // cbSecs
          // 
          this.cbSecs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbSecs.FormattingEnabled = true;
          this.cbSecs.Location = new System.Drawing.Point(142, 9);
          this.cbSecs.MaxDropDownItems = 6;
          this.cbSecs.Name = "cbSecs";
          this.cbSecs.Size = new System.Drawing.Size(169, 21);
          this.cbSecs.TabIndex = 1;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(12, 12);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(124, 13);
          this.label1.TabIndex = 2;
          this.label1.Text = "Сектор для видалення:";
          // 
          // FSectorRemove
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(328, 70);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.cbSecs);
          this.Controls.Add(this.button1);
          this.MaximizeBox = false;
          this.MaximumSize = new System.Drawing.Size(336, 104);
          this.MinimizeBox = false;
          this.MinimumSize = new System.Drawing.Size(336, 104);
          this.Name = "FSectorRemove";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "Вибір сектору";
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button button1;
      public System.Windows.Forms.ComboBox cbSecs;
      private System.Windows.Forms.Label label1;
   }
}