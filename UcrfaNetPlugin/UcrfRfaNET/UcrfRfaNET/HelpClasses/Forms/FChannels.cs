﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    public partial class FChannels : Form
    {
        private string Title;
        private string TitleList;
        public bool isDelete;

        public string SelectChannelsCaption { get; set; }
        public string AllChannelsCaption { get; set; }

        public FChannels()
        {
            InitializeComponent();
        }

        public FChannels(string title, string titleList)
        {
            Title = title;
            TitleList = titleList;

            InitializeComponent();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            List<object> sel = new List<object>();
            foreach (object ob in CLBSelectChannel.Items)
                sel.Add(ob);

            List<object> all = new List<object>();
            foreach (object ob in CLBAllChannel.Items)
                all.Add(ob);

            foreach (object obj in CLBAllChannel.CheckedItems)
            {
                sel.Add(obj);
                all.Remove(obj);
            }

            if ((sel is IComparable) && (sel is IComparer))
                sel.Sort();
            if ((all is IComparable) && (all is IComparer))
                all.Sort();
            CLBAllChannel.Items.Clear();
            CLBAllChannel.Items.AddRange(all.ToArray());
            CLBSelectChannel.Items.Clear();
            CLBSelectChannel.Items.AddRange(sel.ToArray());
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            List<object> sel = new List<object>();
            foreach (object ob in CLBSelectChannel.Items)
                sel.Add(ob);

            List<object> all = new List<object>();
            foreach (object ob in CLBAllChannel.Items)
                all.Add(ob);

            foreach (object obj in CLBSelectChannel.CheckedItems)
            {
                all.Add(obj);
                sel.Remove(obj);
            }
            if ((sel is IComparable) && (sel is IComparer))
                sel.Sort();
            if ((all is IComparable) && (all is IComparer))
                all.Sort();
            CLBAllChannel.Items.Clear();
            CLBAllChannel.Items.AddRange(all.ToArray());
            CLBSelectChannel.Items.Clear();
            CLBSelectChannel.Items.AddRange(sel.ToArray());
            isDelete = true;
        }

        private void BtnSelectAll_Click(object sender, EventArgs e)
        {
            List<object> sel = new List<object>();
            foreach (object ob in CLBSelectChannel.Items)
                sel.Add(ob);

            foreach (object ob in CLBAllChannel.Items)
                sel.Add(ob);
            if ((sel is IComparable) && (sel is IComparer))
                sel.Sort();
            CLBAllChannel.Items.Clear();
            CLBSelectChannel.Items.Clear();
            CLBSelectChannel.Items.AddRange(sel.ToArray());
        }

        private void BtnDeleteAll_Click(object sender, EventArgs e)
        {
            List<object> sel = new List<object>();
            foreach (object ob in CLBSelectChannel.Items)
                sel.Add(ob);

            foreach (object ob in CLBAllChannel.Items)
                sel.Add(ob);
            if ((sel is IComparable) && (sel is IComparer))
                sel.Sort();
            CLBAllChannel.Items.Clear();
            CLBSelectChannel.Items.Clear();
            CLBAllChannel.Items.AddRange(sel.ToArray());
            isDelete = true;
        }

        string GetSelectedChannel()
        {
            string retStr = "";
            foreach (object obj in CLBSelectChannel.SelectedItems)
            {
                if (retStr != "") retStr += ";";
                retStr += obj.ToString();
            }
            return retStr;
        }

        private void FChannels_Shown(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Title))
                Text = Title;

            if (!string.IsNullOrEmpty(TitleList))
                label2.Text = TitleList;
        }

        private void FChannels_Load(object sender, EventArgs e)
        {
            label1.Text = SelectChannelsCaption;
            label2.Text = AllChannelsCaption;
        }
    }
}
