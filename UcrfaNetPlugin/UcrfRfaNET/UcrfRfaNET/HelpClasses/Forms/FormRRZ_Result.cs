﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using OrmCs;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormRRZ_Result : Form
    {
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        public static DialogResult DA { get; set; }

        public FormRRZ_Result(IMQueryMenuNode.Context context, DateTime Picker, string Txt)
        {
                InitializeComponent();
                comboBox1.Items.Clear();
                Dictionary<string, string> dic = CComboList.GetDictValuesFromEri("XNRFA_MICROWA_MON");
                foreach (KeyValuePair<string, string> sd in dic) {
                    comboBox1.Items.Add(sd.Key);
                }

                dateTimePicker1.Value = DateTime.Now;
                lstContext = context.DataList;
                NameTable = context.TableName;
                Id_Table = context.TableId;
                textBox1.MaxLength = 300;
                textBox1.Text = Txt;
                if (Picker!=IM.NullT)
                    dateTimePicker1.Value = Picker;
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool FillStart(string NameTable, IMDBList lstContext, ref DateTime Picker, ref string Txt)
        {
            bool isSuccess = true;
            bool isSuccess_plan = false;
            string Status = "";
            DateTime Plan = IM.NullT;
            try
            {
                IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID,TABLE_ID,STANDARD");
                rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);

                bool isContainRecord = false;
                for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext())
                {
                    List<int> L_Plan_Year = new List<int>();
                    List<KeyValuePair<DateTime,string>> L_Plan_Date = new List<KeyValuePair<DateTime,string>>();
                    IMRecordset rs_find = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                    rs_find.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs_find.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID") - 22000000);
                    for (rs_find.Open(); !rs_find.IsEOF(); rs_find.MoveNext())
                    {
                        if (rs_find.GetT("PLAN_NEW")!=IM.NullT)
                            L_Plan_Year.Add(rs_find.GetT("PLAN_NEW").Year);
                        if (rs_find.GetT("PLAN_NEW")!=IM.NullT)
                            L_Plan_Date.Add(new KeyValuePair<DateTime, string>(rs_find.GetT("PLAN_NEW"), rs_find.GetS("STATUS")));
                        
                    }
                    rs_find.Close();
                    L_Plan_Year.Sort();

                    IMRecordset rs = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite);
                    rs.Select("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, rs_allstations.GetI("ID")-22000000);
                    rs.OrderBy("ID", OrderDirection.Descending);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        if (rs.GetT("END_DATE") != IM.NullT)
                            isContainRecord = true;


                        if (rs.GetT("PLAN_NEW") == IM.NullT)
                            isSuccess_plan = true;

                        Txt = rs.GetS("NOTES");
                        if (rs.GetT("END_DATE") != IM.NullT)
                        {
                            Picker = rs.GetT("END_DATE");
                            ///
                            if (L_Plan_Year.Count > 0) {
                                KeyValuePair<DateTime, string> DTRC = L_Plan_Date.Find(r => r.Key.Year == L_Plan_Year[L_Plan_Year.Count - 1]);
                                if (DTRC.Key != IM.NullT) {
                                    Plan = DTRC.Key;
                                    Status = DTRC.Value;
                                    if (Picker==IM.NullT)
                                        Picker = new DateTime(L_Plan_Year[L_Plan_Year.Count - 1], DTRC.Key.Month, DTRC.Key.Day);
                                    else
                                        Picker = new DateTime(L_Plan_Year[L_Plan_Year.Count - 1], Picker.Month, Picker.Day);
                                }
                            }
                            ///
                        }
                        if ((L_Plan_Year.Count > 0) && (rs.GetT("END_DATE") == IM.NullT)) {

                            Picker = new DateTime(L_Plan_Year[L_Plan_Year.Count - 1], DateTime.Now.Month, 1);
                            KeyValuePair<DateTime, string> DTRC = L_Plan_Date.Find(r => r.Key.Year == L_Plan_Year[L_Plan_Year.Count - 1]);
                            if (DTRC.Key != IM.NullT) {
                                Plan = DTRC.Key;
                                Status = DTRC.Value;
                            }
                        }
                            
                            
                        

                        //Status = rs.GetS("STATUS");
                        //Plan = rs.GetT("PLAN_NEW");

                        break;
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                  
                }

                if (isContainRecord)
                {
                    bool SpecialStatusIsSuccess = false;
                    DialogResult DA;
                    if (Status != "3") {
                        DA = MessageBox.Show(string.Format("Для даного РЕЗ РРЗ STATUS={0} роботи з моніторингу на {1}.{2}. Змінити дані?", Status, Plan.Month, Plan.Year), "", MessageBoxButtons.YesNo);
                        if (DA == System.Windows.Forms.DialogResult.Yes)
                            SpecialStatusIsSuccess = true;
                        else
                            SpecialStatusIsSuccess = false;
                    }
                    else {
                        MessageBox.Show(string.Format("Для даного РЕЗ РРЗ STATUS={0} роботи з моніторингу на {1}.{2}.", Status, Plan.Month, Plan.Year));
                        SpecialStatusIsSuccess = false;
                        isSuccess = false;
                    }

                    if (SpecialStatusIsSuccess)  {
                        DA = MessageBox.Show(CLocaliz.TxT("Record (s) containing information about the TRK result! Do you want to change the outcome TRK?"), "", MessageBoxButtons.YesNo);
                        if (DA == System.Windows.Forms.DialogResult.Yes)
                            isSuccess = true;
                        else
                            isSuccess = false;
                    }
                }

                if (isSuccess_plan)
                {
                    MessageBox.Show(CLocaliz.TxT("Adding data impossible!TRK has to be planned."));
                    isSuccess = false;
                }
                
               

            }
            catch (Exception ex)
            {
                isSuccess = false;
                MessageBox.Show("Помилка: " + ex.Message);
            }

            return isSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_OK_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.OK;
            bool isSuccess = false;
            string AllErrors = "";
            int CountUpdated = 0;
            try
            {
                IMRecordset rs_allstations = new IMRecordset(NameTable, IMRecordset.Mode.ReadWrite);
                rs_allstations.Select("ID,TABLE_ID,STANDARD");
                rs_allstations.AddSelectionFrom(lstContext, IMRecordset.WhereCopyOptions.SelectedLines);
                for (rs_allstations.Open(); !rs_allstations.IsEOF(); rs_allstations.MoveNext())
                {

                    YXnrfaMicrowaMon rs = new YXnrfaMicrowaMon();
                    rs.Table = "XNRFA_MICROWA_MON";
                    rs.Format("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs.Order = "[ID] DESC"; 
                    if (rs.Fetch(string.Format("[OBJ_ID] = {0}", rs_allstations.GetI("ID") - 22000000)))
                    {
                        if ((rs.m_plan_new!=IM.NullT) && (rs.m_end_date==IM.NullT))
                        {
                            rs.m_allstat_id = rs_allstations.GetI("ID");
                            rs.m_last_edit_name = IM.ConnectedUser();
                            rs.m_last_edit_date = DateTime.Now;
                            rs.m_obj_id = rs_allstations.GetI("ID") - 22000000;
                            string Notes = "";
                            if (!string.IsNullOrEmpty(comboBox1.Text)) Notes = comboBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") + (textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") != "" ? " " + textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") : "");
                            else Notes = textBox1.Text;
                            rs.m_notes = Notes.TrimEnd().TrimStart();
                            rs.m_obj_table = "MICROWS";
                            rs.m_end_date = dateTimePicker1.Value;
                            rs.m_status = "2";
                            rs.Save();
                            isSuccess = true;
                          
                        }
                        else if ((rs.m_end_date!=IM.NullT))
                        {
                            DialogResult DAV = MessageBox.Show("Запис " + rs_allstations.GetI("ID").ToString() + " містить інформацію про результат ТРК!  " + Environment.NewLine + "Ви дійсно бажаєте змінити планування ТРК?", "", MessageBoxButtons.YesNo);
                            if (DAV == System.Windows.Forms.DialogResult.Yes)
                            {
                                rs.m_allstat_id = rs_allstations.GetI("ID");
                                rs.m_last_edit_name = IM.ConnectedUser();
                                rs.m_last_edit_date = DateTime.Now;
                                rs.m_obj_id = rs_allstations.GetI("ID") - 22000000;
                                string Notes = "";
                                if (!string.IsNullOrEmpty(comboBox1.Text)) Notes = comboBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") + (textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") != "" ? " " + textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") : "");
                                else Notes = textBox1.Text;
                                rs.m_notes = Notes.TrimEnd().TrimStart();
                                rs.m_obj_table = "MICROWS";
                                rs.m_end_date = dateTimePicker1.Value;
                                rs.m_status = "2";
                                rs.Save();
                                isSuccess = true;
                            }

                        }
                        if (rs.m_plan_new==IM.NullT)
                            AllErrors += "[" + rs_allstations.GetI("ID").ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                    }
                    rs.Close();
                    rs.Dispose();
                    
                 

                    YXnrfaMicrowaMon rs_ = new YXnrfaMicrowaMon();
                    rs_.Table = "XNRFA_MICROWA_MON";
                    rs_.Format("ID,OBJ_ID,OBJ_TABLE,STATUS,PLAN_NEW,END_DATE,NOTES,LAST_EDIT_NAME,LAST_EDIT_DATE,ALLSTAT_ID");
                    rs_.Order = "[ID] DESC"; 
                    if (!rs_.Fetch(string.Format("[OBJ_ID] = {0}", rs_allstations.GetI("ID") - 22000000)))
                    {
                        if ((rs_.m_plan_new!=IM.NullT) && (rs_.m_end_date==IM.NullT))
                        {
                            rs_.New();
                            rs_.m_id = rs_.AllocID();
                            rs_.m_allstat_id = rs_allstations.GetI("ID");
                            rs_.m_last_edit_name = IM.ConnectedUser();
                            rs_.m_last_edit_date = DateTime.Now;
                            rs_.m_obj_id = rs_allstations.GetI("ID") - 22000000;
                            string Notes = "";
                            if (!string.IsNullOrEmpty(comboBox1.Text)) Notes = comboBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") + (textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") != "" ? " " + textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") : "");
                            else Notes = textBox1.Text;
                            rs_.m_notes = Notes.TrimEnd().TrimStart();
                            rs_.m_obj_table = "MICROWS";
                            rs_.m_end_date = dateTimePicker1.Value;
                            rs_.m_status = "2";
                            rs_.Save();
                            isSuccess = true;
                        }
                        else if ((rs_.m_end_date != IM.NullT))
                        {
                            DialogResult DAV = MessageBox.Show("Запис " + rs_allstations.GetI("ID").ToString() + " містить інформацію про результат ТРК!  " + Environment.NewLine + "Ви дійсно бажаєте змінити планування ТРК?", "", MessageBoxButtons.YesNo);
                            if (DAV == System.Windows.Forms.DialogResult.Yes)
                            {
                                rs_.New();
                                rs_.m_id = rs_.AllocID();
                                rs_.m_allstat_id = rs_allstations.GetI("ID");
                                rs_.m_last_edit_name = IM.ConnectedUser();
                                rs_.m_last_edit_date = DateTime.Now;
                                rs_.m_obj_id = rs_allstations.GetI("ID") - 22000000;
                                string Notes = "";
                                if (!string.IsNullOrEmpty(comboBox1.Text)) Notes = comboBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") + (textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") != "" ? " " + textBox1.Text.TrimEnd().TrimStart().Replace(Environment.NewLine, "") : "");
                                else Notes = textBox1.Text;
                                rs_.m_notes = Notes.TrimEnd().TrimStart();
                                rs_.m_obj_table = "MICROWS";
                                rs_.m_end_date = dateTimePicker1.Value;
                                rs_.m_status = "2";
                                rs_.Save();
                                isSuccess = true;
                            }
                        }
                        else if (rs_.m_plan_new==IM.NullT)
                            AllErrors += "[" + rs_allstations.GetI("ID").ToString() + "] - " + CLocaliz.TxT("Adding data impossible!TRK has to be planned.") + Environment.NewLine;
                    }
                    rs_.Close();
                    rs_.Dispose();

                    CountUpdated++;
                }
              
                string Mess = "";
                if (isSuccess)
                {
                    Mess = "Запис(и) створено успішно!";
                    if (CountUpdated > 0)
                        Mess += Environment.NewLine + string.Format("Кількість записів : {0}", CountUpdated);
                    MessageBox.Show(Mess);
                }

                if (AllErrors.Length > 0)  {
                    using (IMLogFile log = new IMLogFile())  {
                        log.Create("ResultWorkImportData");
                        string StrWarning = "Під час внесення даних виникли помилки!" + Environment.NewLine + "Деякі записи не оброблені: " + Environment.NewLine + AllErrors;
                        log.Warning(StrWarning);
                        log.Display("Результати імпорту");
                    }

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: "+ex.Message);
            }
            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
