﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
   

    public partial class CustomDialogBox : Form
    {


        public  static DialogResultMessageBoxButtons CurrentPress = DialogResultMessageBoxButtons.None;


        public  CustomDialogBox(string Messages, string Caption)
        {
            InitializeComponent();
            Show(Messages,Caption);
        }

 
        public  void Show(string text, string caption)
        {
            MemoMess.Text = text;
            this.Text = caption;
            CurrentPress = DialogResultMessageBoxButtons.None;
            this.ShowDialog();
         
        }

        public DialogResultMessageBoxButtons GetStatusPress()
        {
            return CurrentPress;
        }
    
    
    

        private   void button1_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.OK;
            Close();
        }

        private  void button2_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.No;
            Close();
        }

        private  void button3_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.View;
            Close();
        }

        private void CustomDialogBox_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

    }
}
