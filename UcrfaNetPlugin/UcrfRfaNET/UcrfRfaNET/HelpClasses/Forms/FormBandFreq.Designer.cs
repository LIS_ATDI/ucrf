﻿namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    partial class FormBandFreq
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBandFreq));
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.label_RRZ = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BandMin = new NetPlugins2.IcsDouble();
            this.BandMax = new NetPlugins2.IcsDouble();
            this.CentralFreq = new NetPlugins2.IcsDouble();
            this.label3 = new System.Windows.Forms.Label();
            this.DeviationP = new NetPlugins2.IcsDouble();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DeviationM = new NetPlugins2.IcsDouble();
            this.icsCombo_Type_VP = new NetPlugins2.IcsCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(293, 189);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(15, 189);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 6;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // label_RRZ
            // 
            this.label_RRZ.AutoSize = true;
            this.label_RRZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_RRZ.Location = new System.Drawing.Point(12, 26);
            this.label_RRZ.Name = "label_RRZ";
            this.label_RRZ.Size = new System.Drawing.Size(74, 13);
            this.label_RRZ.TabIndex = 4;
            this.label_RRZ.Text = "Смуга, Мгц";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Центральна частота, Мгц";
            // 
            // BandMin
            // 
            this.BandMin.Location = new System.Drawing.Point(116, 21);
            this.BandMin.Margin = new System.Windows.Forms.Padding(0);
            this.BandMin.Name = "BandMin";
            this.BandMin.Size = new System.Drawing.Size(92, 18);
            this.BandMin.Subtype = null;
            this.BandMin.TabIndex = 13;
            // 
            // BandMax
            // 
            this.BandMax.Location = new System.Drawing.Point(276, 21);
            this.BandMax.Margin = new System.Windows.Forms.Padding(0);
            this.BandMax.Name = "BandMax";
            this.BandMax.Size = new System.Drawing.Size(92, 18);
            this.BandMax.Subtype = null;
            this.BandMax.TabIndex = 14;
            // 
            // CentralFreq
            // 
            this.CentralFreq.Location = new System.Drawing.Point(196, 48);
            this.CentralFreq.Margin = new System.Windows.Forms.Padding(0);
            this.CentralFreq.Name = "CentralFreq";
            this.CentralFreq.Size = new System.Drawing.Size(92, 18);
            this.CentralFreq.Subtype = null;
            this.CentralFreq.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(235, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "-";
            // 
            // DeviationP
            // 
            this.DeviationP.Location = new System.Drawing.Point(276, 80);
            this.DeviationP.Margin = new System.Windows.Forms.Padding(0);
            this.DeviationP.Name = "DeviationP";
            this.DeviationP.Size = new System.Drawing.Size(92, 18);
            this.DeviationP.Subtype = null;
            this.DeviationP.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(250, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Допустиме частотне відхилення (+), МГц";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Допустиме частотне відхилення (-), МГц";
            // 
            // DeviationM
            // 
            this.DeviationM.Location = new System.Drawing.Point(276, 116);
            this.DeviationM.Margin = new System.Windows.Forms.Padding(0);
            this.DeviationM.Name = "DeviationM";
            this.DeviationM.Size = new System.Drawing.Size(92, 18);
            this.DeviationM.Subtype = null;
            this.DeviationM.TabIndex = 20;
            // 
            // icsCombo_Type_VP
            // 
            this.icsCombo_Type_VP.Coded = true;
            this.icsCombo_Type_VP.Location = new System.Drawing.Point(88, 140);
            this.icsCombo_Type_VP.Margin = new System.Windows.Forms.Padding(0);
            this.icsCombo_Type_VP.MaxLen = 50;
            this.icsCombo_Type_VP.Name = "icsCombo_Type_VP";
            this.icsCombo_Type_VP.ReadOnly = false;
            this.icsCombo_Type_VP.Size = new System.Drawing.Size(187, 20);
            this.icsCombo_Type_VP.Subtype = "eri_EmitterType";
            this.icsCombo_Type_VP.TabIndex = 21;
            this.icsCombo_Type_VP.Upperc = false;
            this.icsCombo_Type_VP.Value = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Тип ВП";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_RRZ);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.button_OK);
            this.groupBox1.Controls.Add(this.icsCombo_Type_VP);
            this.groupBox1.Controls.Add(this.button_Cancel);
            this.groupBox1.Controls.Add(this.DeviationM);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.BandMin);
            this.groupBox1.Controls.Add(this.DeviationP);
            this.groupBox1.Controls.Add(this.BandMax);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.CentralFreq);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 230);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // FormBandFreq
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 230);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormBandFreq";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Смуга робочих частот ВП";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Label label_RRZ;
        private System.Windows.Forms.Label label2;
        private NetPlugins2.IcsDouble BandMin;
        private NetPlugins2.IcsDouble BandMax;
        private NetPlugins2.IcsDouble CentralFreq;
        private System.Windows.Forms.Label label3;
        private NetPlugins2.IcsDouble DeviationP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private NetPlugins2.IcsDouble DeviationM;
        private NetPlugins2.IcsCombo icsCombo_Type_VP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}