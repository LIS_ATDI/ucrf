﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FProvincesList : Form
   {
      public FProvincesList()
      {
         InitializeComponent();
         btOk.Text = CLocaliz.TxT("Ok");
         btCancel.Text = CLocaliz.TxT("Cancel");
      }

      private void chb1_CheckedChanged(object sender, EventArgs e)
      {
         if (chb1.Checked)
            for (int i = 0; i < chlbProvs.Items.Count; ++i )
            {
               chlbProvs.SetItemChecked(i, true);
            }
         else
            for (int i = 0; i < chlbProvs.Items.Count; ++i)
            {
               chlbProvs.SetItemChecked(i, false);
            }
      }
   }
}
