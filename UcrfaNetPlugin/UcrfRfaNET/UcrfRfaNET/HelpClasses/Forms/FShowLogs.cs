﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   internal partial class FShowLogs : FBaseForm
   {
      public FShowLogs(string type, string what, string createdBy, DateTime createdDate, string message)
      {
         InitializeComponent();
         //------
         tbType.Text = type;
         tbWhat.Text = what;
         tbCreatedBy.Text = string.Format("Created by:{0} ({1})", createdBy, createdDate);
         tbMessage.Text = message;
      }
   }
}
