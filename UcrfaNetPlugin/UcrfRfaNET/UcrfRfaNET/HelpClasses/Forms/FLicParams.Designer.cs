﻿namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    partial class FLicParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLicParams));
            this.chbCurrentDate = new System.Windows.Forms.CheckBox();
            this.licParamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lbActualDate = new System.Windows.Forms.Label();
            this.dtpActualDate = new System.Windows.Forms.DateTimePicker();
            this.btOK = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.chbUseLicenceStatuses = new System.Windows.Forms.CheckBox();
            this.chbAutoLicences = new System.Windows.Forms.CheckBox();
            this.chbAutoTechno = new System.Windows.Forms.CheckBox();
            this.chbIgnoreGaps = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chbAutoExcludeProlongated = new System.Windows.Forms.CheckBox();
            this.chCheckProlongConsist = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.licParamsBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chbCurrentDate
            // 
            this.chbCurrentDate.AutoSize = true;
            this.chbCurrentDate.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "IsCurrentDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbCurrentDate.Location = new System.Drawing.Point(103, 43);
            this.chbCurrentDate.Name = "chbCurrentDate";
            this.chbCurrentDate.Size = new System.Drawing.Size(105, 17);
            this.chbCurrentDate.TabIndex = 2;
            this.chbCurrentDate.Text = "Use current date";
            this.chbCurrentDate.UseVisualStyleBackColor = true;
            // 
            // licParamsBindingSource
            // 
            this.licParamsBindingSource.DataSource = typeof(XICSM.UcrfRfaNET.LicParams);
            // 
            // lbActualDate
            // 
            this.lbActualDate.AutoSize = true;
            this.lbActualDate.Location = new System.Drawing.Point(27, 23);
            this.lbActualDate.Name = "lbActualDate";
            this.lbActualDate.Size = new System.Drawing.Size(61, 13);
            this.lbActualDate.TabIndex = 0;
            this.lbActualDate.Text = "Actual date";
            // 
            // dtpActualDate
            // 
            this.dtpActualDate.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.licParamsBindingSource, "ActualDate", true));
            this.dtpActualDate.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.licParamsBindingSource, "IsCurrentDate", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.dtpActualDate.Location = new System.Drawing.Point(103, 19);
            this.dtpActualDate.Name = "dtpActualDate";
            this.dtpActualDate.Size = new System.Drawing.Size(152, 20);
            this.dtpActualDate.TabIndex = 1;
            // 
            // btOK
            // 
            this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Location = new System.Drawing.Point(103, 229);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(75, 23);
            this.btOK.TabIndex = 100;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(193, 229);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 110;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // chbUseLicenceStatuses
            // 
            this.chbUseLicenceStatuses.AutoSize = true;
            this.chbUseLicenceStatuses.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "UseLicStatuses", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbUseLicenceStatuses.Location = new System.Drawing.Point(103, 68);
            this.chbUseLicenceStatuses.Name = "chbUseLicenceStatuses";
            this.chbUseLicenceStatuses.Size = new System.Drawing.Size(124, 17);
            this.chbUseLicenceStatuses.TabIndex = 10;
            this.chbUseLicenceStatuses.Text = "Use licence statuses";
            this.chbUseLicenceStatuses.UseVisualStyleBackColor = true;
            // 
            // chbAutoLicences
            // 
            this.chbAutoLicences.AutoSize = true;
            this.chbAutoLicences.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "AutoLinkLicences", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbAutoLicences.Location = new System.Drawing.Point(103, 91);
            this.chbAutoLicences.Name = "chbAutoLicences";
            this.chbAutoLicences.Size = new System.Drawing.Size(152, 17);
            this.chbAutoLicences.TabIndex = 15;
            this.chbAutoLicences.Text = "Auto link all licences found";
            this.chbAutoLicences.UseVisualStyleBackColor = true;
            // 
            // chbAutoTechno
            // 
            this.chbAutoTechno.AutoSize = true;
            this.chbAutoTechno.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "AutoSetTechno", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbAutoTechno.Location = new System.Drawing.Point(103, 137);
            this.chbAutoTechno.Name = "chbAutoTechno";
            this.chbAutoTechno.Size = new System.Drawing.Size(136, 17);
            this.chbAutoTechno.TabIndex = 17;
            this.chbAutoTechno.Text = "Auto set Techno status";
            this.chbAutoTechno.UseVisualStyleBackColor = true;
            // 
            // chbIgnoreGaps
            // 
            this.chbIgnoreGaps.AutoSize = true;
            this.chbIgnoreGaps.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.licParamsBindingSource, "AutoLinkLicences", true));
            this.chbIgnoreGaps.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "IgnoreBandGaps", true));
            this.chbIgnoreGaps.Location = new System.Drawing.Point(125, 112);
            this.chbIgnoreGaps.Name = "chbIgnoreGaps";
            this.chbIgnoreGaps.Size = new System.Drawing.Size(109, 17);
            this.chbIgnoreGaps.TabIndex = 16;
            this.chbIgnoreGaps.Text = "Ignore band gaps";
            this.chbIgnoreGaps.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.chCheckProlongConsist);
            this.panel1.Controls.Add(this.chbAutoExcludeProlongated);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 216);
            this.panel1.TabIndex = 111;
            // 
            // chbAutoExcludeProlongated
            // 
            this.chbAutoExcludeProlongated.AutoSize = true;
            this.chbAutoExcludeProlongated.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "AutoExcludeProlongated", true));
            this.chbAutoExcludeProlongated.Location = new System.Drawing.Point(99, 156);
            this.chbAutoExcludeProlongated.Name = "chbAutoExcludeProlongated";
            this.chbAutoExcludeProlongated.Size = new System.Drawing.Size(189, 17);
            this.chbAutoExcludeProlongated.TabIndex = 18;
            this.chbAutoExcludeProlongated.Text = "Auto exclude prolongated licences";
            this.chbAutoExcludeProlongated.UseVisualStyleBackColor = true;
            // 
            // chCheckProlongConsist
            // 
            this.chCheckProlongConsist.AutoSize = true;
            this.chCheckProlongConsist.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.licParamsBindingSource, "CheckProlongatedConsistency", true));
            this.chCheckProlongConsist.Location = new System.Drawing.Point(99, 179);
            this.chCheckProlongConsist.Name = "chCheckProlongConsist";
            this.chCheckProlongConsist.Size = new System.Drawing.Size(212, 17);
            this.chCheckProlongConsist.TabIndex = 19;
            this.chCheckProlongConsist.Text = "Check prolongated licence consistency";
            this.chCheckProlongConsist.UseVisualStyleBackColor = true;
            // 
            // FLicParams
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(385, 260);
            this.Controls.Add(this.chbIgnoreGaps);
            this.Controls.Add(this.chbAutoTechno);
            this.Controls.Add(this.chbAutoLicences);
            this.Controls.Add(this.chbUseLicenceStatuses);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.dtpActualDate);
            this.Controls.Add(this.lbActualDate);
            this.Controls.Add(this.chbCurrentDate);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FLicParams";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Licence Selection";
            ((System.ComponentModel.ISupportInitialize)(this.licParamsBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chbCurrentDate;
        private System.Windows.Forms.Label lbActualDate;
        private System.Windows.Forms.DateTimePicker dtpActualDate;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.BindingSource licParamsBindingSource;
        private System.Windows.Forms.CheckBox chbUseLicenceStatuses;
        private System.Windows.Forms.CheckBox chbAutoLicences;
        private System.Windows.Forms.CheckBox chbAutoTechno;
        private System.Windows.Forms.CheckBox chbIgnoreGaps;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chCheckProlongConsist;
        private System.Windows.Forms.CheckBox chbAutoExcludeProlongated;
    }
}