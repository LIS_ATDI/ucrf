﻿namespace XICSM.UcrfRfaNET
{
   partial class FormSelectOutDoc
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.label1 = new System.Windows.Forms.Label();
          this.cbDocumentName = new System.Windows.Forms.ComboBox();
          this.dateStartComp = new System.Windows.Forms.DateTimePicker();
          this.dateEndComp = new System.Windows.Forms.DateTimePicker();
          this.label2 = new System.Windows.Forms.Label();
          this.labelEndDate = new System.Windows.Forms.Label();
          this.buttonOK = new System.Windows.Forms.Button();
          this.buttonCancel = new System.Windows.Forms.Button();
          this.lblFileNum = new System.Windows.Forms.Label();
          this.tbFileNumber = new System.Windows.Forms.TextBox();
          this.lblDoc = new System.Windows.Forms.Label();
          this.cmbBxDocument = new System.Windows.Forms.ComboBox();
          this.checkAmateurDateEnd = new System.Windows.Forms.CheckBox();
          this.SuspendLayout();
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(6, 15);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(83, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Тип документа";
          // 
          // cbDocumentName
          // 
          this.cbDocumentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbDocumentName.FormattingEnabled = true;
          this.cbDocumentName.Location = new System.Drawing.Point(110, 12);
          this.cbDocumentName.Name = "cbDocumentName";
          this.cbDocumentName.Size = new System.Drawing.Size(360, 21);
          this.cbDocumentName.TabIndex = 0;
          // 
          // dateStartComp
          // 
          this.dateStartComp.Location = new System.Drawing.Point(110, 39);
          this.dateStartComp.Name = "dateStartComp";
          this.dateStartComp.Size = new System.Drawing.Size(138, 20);
          this.dateStartComp.TabIndex = 1;
          // 
          // dateEndComp
          // 
          this.dateEndComp.Location = new System.Drawing.Point(110, 65);
          this.dateEndComp.Name = "dateEndComp";
          this.dateEndComp.Size = new System.Drawing.Size(138, 20);
          this.dateEndComp.TabIndex = 2;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(6, 43);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(53, 13);
          this.label2.TabIndex = 4;
          this.label2.Text = "Start date";
          // 
          // labelEndDate
          // 
          this.labelEndDate.AutoSize = true;
          this.labelEndDate.Location = new System.Drawing.Point(6, 69);
          this.labelEndDate.Name = "labelEndDate";
          this.labelEndDate.Size = new System.Drawing.Size(50, 13);
          this.labelEndDate.TabIndex = 5;
          this.labelEndDate.Text = "End date";
          // 
          // buttonOK
          // 
          this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.buttonOK.Location = new System.Drawing.Point(257, 94);
          this.buttonOK.Name = "buttonOK";
          this.buttonOK.Size = new System.Drawing.Size(96, 23);
          this.buttonOK.TabIndex = 4;
          this.buttonOK.Text = "OK";
          this.buttonOK.UseVisualStyleBackColor = true;
          this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
          // 
          // buttonCancel
          // 
          this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.buttonCancel.Location = new System.Drawing.Point(374, 94);
          this.buttonCancel.Name = "buttonCancel";
          this.buttonCancel.Size = new System.Drawing.Size(96, 23);
          this.buttonCancel.TabIndex = 5;
          this.buttonCancel.Text = "Cancel";
          this.buttonCancel.UseVisualStyleBackColor = true;
          // 
          // lblFileNum
          // 
          this.lblFileNum.AutoSize = true;
          this.lblFileNum.Enabled = false;
          this.lblFileNum.Location = new System.Drawing.Point(6, 94);
          this.lblFileNum.Name = "lblFileNum";
          this.lblFileNum.Size = new System.Drawing.Size(72, 13);
          this.lblFileNum.TabIndex = 8;
          this.lblFileNum.Text = "Blank number";
          this.lblFileNum.Visible = false;
          // 
          // tbFileNumber
          // 
          this.tbFileNumber.Enabled = false;
          this.tbFileNumber.Location = new System.Drawing.Point(110, 91);
          this.tbFileNumber.Name = "tbFileNumber";
          this.tbFileNumber.Size = new System.Drawing.Size(138, 20);
          this.tbFileNumber.TabIndex = 3;
          this.tbFileNumber.Visible = false;
          // 
          // lblDoc
          // 
          this.lblDoc.AutoSize = true;
          this.lblDoc.Enabled = false;
          this.lblDoc.Location = new System.Drawing.Point(254, 43);
          this.lblDoc.Name = "lblDoc";
          this.lblDoc.Size = new System.Drawing.Size(67, 13);
          this.lblDoc.TabIndex = 9;
          this.lblDoc.Text = "Для об\'єкту";
          this.lblDoc.Visible = false;
          // 
          // cmbBxDocument
          // 
          this.cmbBxDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cmbBxDocument.FormattingEnabled = true;
          this.cmbBxDocument.Location = new System.Drawing.Point(263, 64);
          this.cmbBxDocument.Name = "cmbBxDocument";
          this.cmbBxDocument.Size = new System.Drawing.Size(167, 21);
          this.cmbBxDocument.TabIndex = 10;
          // 
          // checkAmateurDateEnd
          // 
          this.checkAmateurDateEnd.AutoSize = true;
          this.checkAmateurDateEnd.Location = new System.Drawing.Point(257, 66);
          this.checkAmateurDateEnd.Name = "checkAmateurDateEnd";
          this.checkAmateurDateEnd.Size = new System.Drawing.Size(173, 17);
          this.checkAmateurDateEnd.TabIndex = 11;
          this.checkAmateurDateEnd.Text = "Ввімкнути поле \"Дійсний до\"";
          this.checkAmateurDateEnd.UseVisualStyleBackColor = true;
          this.checkAmateurDateEnd.CheckedChanged += new System.EventHandler(this.checkAmateurDateEnd_CheckedChanged);
          // 
          // FormSelectOutDoc
          // 
          this.AcceptButton = this.buttonOK;
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.CancelButton = this.buttonCancel;
          this.ClientSize = new System.Drawing.Size(482, 124);
          this.Controls.Add(this.checkAmateurDateEnd);
          this.Controls.Add(this.cmbBxDocument);
          this.Controls.Add(this.lblDoc);
          this.Controls.Add(this.tbFileNumber);
          this.Controls.Add(this.lblFileNum);
          this.Controls.Add(this.buttonCancel);
          this.Controls.Add(this.buttonOK);
          this.Controls.Add(this.labelEndDate);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.dateEndComp);
          this.Controls.Add(this.dateStartComp);
          this.Controls.Add(this.cbDocumentName);
          this.Controls.Add(this.label1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.Name = "FormSelectOutDoc";
          this.Text = "Generating of an output document";
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.ComboBox cbDocumentName;
      private System.Windows.Forms.DateTimePicker dateStartComp;
      private System.Windows.Forms.DateTimePicker dateEndComp;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label labelEndDate;
      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.Label lblFileNum;
      private System.Windows.Forms.TextBox tbFileNumber;
      private System.Windows.Forms.Label lblDoc;
      public System.Windows.Forms.ComboBox cmbBxDocument;
      private System.Windows.Forms.CheckBox checkAmateurDateEnd;
   }
}