﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
   

    public partial class CustomDialogBoxOU : Form
    {


        public  static DialogResultMessageBoxButtons CurrentPress = DialogResultMessageBoxButtons.None;


        public CustomDialogBoxOU(string MessagesOld,string MessagesNew, string Caption)
        {
            InitializeComponent();
            Show(MessagesOld,MessagesNew, Caption);
        }

 
        public  void Show(string text_old,string text_new, string caption)
        {
            MemoMessOld.Text = text_old;
            MemoMessNew.Text = text_new;
            this.Text = caption;
            CurrentPress = DialogResultMessageBoxButtons.None;
            this.ShowDialog();
         
        }

        public DialogResultMessageBoxButtons GetStatusPress()
        {
            return CurrentPress;
        }
    
    
    

        private   void button1_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.Override;
            Close();
        }

        private  void button2_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.Append;
            Close();
        }

        private  void button3_Click(object sender, EventArgs e)
        {
            CurrentPress = DialogResultMessageBoxButtons.CancelOperation;
            Close();
        }

        private void CustomDialogBox_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

    }
}
