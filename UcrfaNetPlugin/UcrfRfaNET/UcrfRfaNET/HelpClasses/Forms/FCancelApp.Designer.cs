﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FCancelApp
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCancelApp));
         this.tbNumber = new System.Windows.Forms.TextBox();
         this.dtDate = new System.Windows.Forms.DateTimePicker();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.btCancel = new System.Windows.Forms.Button();
         this.btExit = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // tbNumber
         // 
         this.tbNumber.Location = new System.Drawing.Point(9, 18);
         this.tbNumber.Name = "tbNumber";
         this.tbNumber.Size = new System.Drawing.Size(149, 20);
         this.tbNumber.TabIndex = 0;
         // 
         // dtDate
         // 
         this.dtDate.AllowDrop = true;
         this.dtDate.Location = new System.Drawing.Point(9, 58);
         this.dtDate.Name = "dtDate";
         this.dtDate.Size = new System.Drawing.Size(149, 20);
         this.dtDate.TabIndex = 2;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(171, 21);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(141, 13);
         this.label1.TabIndex = 3;
         this.label1.Text = "Номер службової записки";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(171, 62);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(33, 13);
         this.label2.TabIndex = 4;
         this.label2.Text = "Дата";
         // 
         // btCancel
         // 
         this.btCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
         this.btCancel.Image = ((System.Drawing.Image)(resources.GetObject("btCancel.Image")));
         this.btCancel.Location = new System.Drawing.Point(118, 101);
         this.btCancel.Name = "btCancel";
         this.btCancel.Size = new System.Drawing.Size(135, 26);
         this.btCancel.TabIndex = 6;
         this.btCancel.Text = "Анулювати              ДРВ";
         this.btCancel.UseVisualStyleBackColor = true;
         // 
         // btExit
         // 
         this.btExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.btExit.Location = new System.Drawing.Point(277, 101);
         this.btExit.Name = "btExit";
         this.btExit.Size = new System.Drawing.Size(99, 26);
         this.btExit.TabIndex = 7;
         this.btExit.Text = "Вихід";
         this.btExit.UseVisualStyleBackColor = true;
         // 
         // FCancelApp
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(400, 135);
         this.Controls.Add(this.btExit);
         this.Controls.Add(this.btCancel);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.dtDate);
         this.Controls.Add(this.tbNumber);
         this.Name = "FCancelApp";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "Підстава для анулювання виставлення рахунку";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      public System.Windows.Forms.TextBox tbNumber;
      public System.Windows.Forms.DateTimePicker dtDate;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button btCancel;
      private System.Windows.Forms.Button btExit;
   }
}