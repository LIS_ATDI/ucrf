﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GridCtrl;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FBillMemoURZP : FBaseForm
   {
      private Grid gridParamRez = new GridCtrl.Grid();
      private Grid gridComiss = new GridCtrl.Grid();
      private Grid gridOther = new GridCtrl.Grid();

      protected CDRVMemoURZP drvMemo = null;   // Обьект, который управляет заявкой

      public FBillMemoURZP(CDRVMemoURZP _drvMemo, DRVStatus st, List<Work> lockedWorks)
      {
         if (_drvMemo == null)
            throw new Exception("The base object can't be NULL.");
         drvMemo = _drvMemo;
         
         InitializeComponent();
         InitGrids();

         
         if (st == DRVStatus.newapl)
         {
            //LoadApp();
            btDRV.Enabled = true;
            btPrint.Enabled = true;
            btDelete.Visible = false;
            ///  btCancel.Visible = true;
         }
         else if (st == DRVStatus.saved)
         {
            LoadApp();
            btDRV.Visible = true;
            btPrint.Enabled = true;
            btDelete.Visible = true;
         }

         foreach (Work w in lockedWorks)
         {            
            switch (w.type)
            {               
               case WorkType.work14:
                  {
                     chbPtk.Checked = true;                     
                     chbPtk.Enabled = false;
                     btPtk.Enabled = false;
                  } break;
               case WorkType.work15:
                  {
                     chbParamRez.Checked = true;
                     gridParamRez.GetCell(1, 0).Value = w.article;
                     gridParamRez.GetCell(1, 1).Value = w.count.ToString();
                     gridParamRez.GetCell(1, 2).Value = w.index.ToString();
                     chbParamRez.Enabled = false;
                     gridParamRez.Enabled = false;
                  } break;
               case WorkType.work19:
                  {
                     chbComiss.Checked = true;
                     gridComiss.GetCell(1, 0).Value = w.article;
                     gridComiss.GetCell(1, 1).Value = w.count.ToString();
                     gridComiss.GetCell(1, 2).Value = w.index.ToString();
                     gridComiss.GetCell(1, 3).Value = (w.count * w.index).ToString();
                     chbComiss.Enabled = false;
                     gridComiss.Enabled = false;
                  } break;
               case WorkType.work52:
                  {
                     chbOther.Checked = true;
                     gridOther.GetCell(1, 0).Value = w.article;
                     gridOther.GetCell(1, 1).Value = w.count.ToString();
                     gridOther.GetCell(1, 2).Value = w.index.ToString();
                     chbOther.Enabled = false;
                     gridOther.Enabled = false;
                  } break;
               case WorkType.work50:
                  {
                     chbOdo.Checked = true;                     
                     chbOdo.Enabled = false;
                     tbOdo.Enabled = false;
                     tbOdo.Text = w.index.ToString();
                  } break;               
            }
         }
         if ((drvMemo.packetType != EPacketType.PckDOZV) && (drvMemo.packetType != EPacketType.PckPTKNVTV))
         {
             chbPtk.Enabled = false;
             chbParamRez.Enabled = false;
             chbOther.Enabled = false;
         }

         tbOwner.Text = drvMemo.OwnerName;
      }
      
      void InitHeaderCell(Cell c)
      {
         c.FontStyle = FontStyle.Bold;
         c.HorizontalAlignment = HorizontalAlignment.Center;
         c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
         c.BackColor = System.Drawing.Color.DarkGray;
         c.TextHeight = 11;
         c.CanEdit = false;
      }

      void InitGrid(Grid grid, bool enBut, Control parent, WorkType wt)
      { 
        // grid = new GridCtrl.Grid();
         grid.AllowDrop = true;
         grid.BackColor = System.Drawing.SystemColors.Window;
         grid.EditMode = true;
         grid.FirstColumnWidth = 120;
         grid.GridLine = GridCtrl.Grid.GridLineStyle.Both;
         grid.GridLineColor = System.Drawing.Color.Black;
         grid.GridOpacity = 100;
         grid.Image = null;
         grid.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;         
         grid.LockUpdates = false;
         grid.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;       
         grid.ToolTips = true;
         grid.Parent = parent;
         grid.Dock = DockStyle.Fill;
         if (wt == WorkType.work19)
         {         
            Row r = grid.AddRow();
            grid.FirstColumnWidth = 70;
            Cell c = grid.AddColumn(grid.GetRowCount() - 1, "Стаття");
            InitHeaderCell(c);
            c = grid.AddColumn(grid.GetRowCount() - 1, 110, "Кількість людей");
            InitHeaderCell(c);            
            c = grid.AddColumn(0, 110, "Кількість годин");
            InitHeaderCell(c);
            c = grid.AddColumn(0, "Загальний витрачений час");
            InitHeaderCell(c);

            grid.AddRow();
            c = grid.AddColumn(grid.GetRowCount() - 1, "");
            c.TextHeight = 11;
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.cellStyle = EditStyle.esPickList;
            foreach (articlesList list in drvMemo.articles)
            {
               if (list.type == wt)
               {
                  c.PickList = list.articles;
                  if (list.defaultValues.ContainsKey(PriceDefaultType.pdtUsual))
                     c.Value = list.defaultValues[PriceDefaultType.pdtUsual];
                  else
                  {
                     if (list.articles.Count > 0)
                        c.Value = list.articles[0];
                  }
               }
            }

            c = grid.AddColumn(grid.GetRowCount() - 1, 110, "");
            c.Key = "menCount";
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
            c.Value = "0";
            c = grid.AddColumn(grid.GetRowCount() - 1, 110, "");            
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
            c.Value = "0";
            c = grid.AddColumn(grid.GetRowCount() - 1, "");
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
            c.Value = "0";

            grid.OnDataValidate += new Grid.DataValidateDelegate(grid_OnDataValidate);
         }
         else
         {
            Row r = grid.AddRow();
            Cell c = grid.AddColumn(grid.GetRowCount() - 1, "Стаття");
            InitHeaderCell(c);
            c = grid.AddColumn(grid.GetRowCount() - 1, "Кількість");
            InitHeaderCell(c);

            if (enBut)
            {
               c = grid.AddColumn(grid.GetRowCount() - 1, "Коефіцієнт");
               InitHeaderCell(c);
            }
            else
            {
               c = grid.AddColumn(0, "Коефіцієнт");
               InitHeaderCell(c);
            }
            grid.AddRow();
            c = grid.AddColumn(grid.GetRowCount() - 1, "");
            c.TextHeight = 11;
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.cellStyle = EditStyle.esPickList;
            foreach (articlesList list in drvMemo.articles)
            {
               if (list.type == wt)
               {
                  c.PickList = list.articles;
                  if (list.defaultValues.ContainsKey(PriceDefaultType.pdtUsual))
                     c.Value = list.defaultValues[PriceDefaultType.pdtUsual];
                  else
                  {
                     if (list.articles.Count > 0)
                        c.Value = list.articles[0];
                  }
               }
            }

            if (enBut)
               c = grid.AddColumn(grid.GetRowCount() - 1, "1");
            else
               c = grid.AddColumn(grid.GetRowCount() - 1, drvMemo.appIds.Count.ToString());
            c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
            c.TextHeight = 11;
            if (enBut)
            {
               c = grid.AddColumn(grid.GetRowCount() - 1, "");
               c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
               c.TextHeight = 11;
               c.cellStyle = EditStyle.esEllipsis;
               c.ButtonText = "Таблиця";
               grid.OnCellButtonClick += new Grid.CellButtonClickDelegate(TableButtonClick);
            }
            else
            {
               c = grid.AddColumn(1, "");
               c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
               c.TextHeight = 11;
            }
         }         
      }

      void grid_OnDataValidate(Cell cell)
      {
         if (cell.Key == "menCount")
         {
            int cnt = ConvertType.ToInt32(cell.Value, IM.NullI);
            if (cnt == IM.NullI || cnt < 0)
            {
               cell.Value = "1";
               MessageBox.Show("Невірно введена кількість людей!", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         }
         if (cell.row == gridComiss.rowList[1] && (cell.Col == 1 || cell.Col == 2))
         {
            int people = ConvertType.ToInt32(gridComiss.GetCell(1, 1).Value, IM.NullI);
            double hours = ConvertType.ToDouble(gridComiss.GetCell(1, 2).Value, IM.NullD);
            if (people != IM.NullI && hours != IM.NullD)
            {
               Cell c = gridComiss.GetCell(1, 3);
               c.Value = (people * hours).ToString();
            }
         }  
      }
      
      void TableButtonClick(Cell cell)
      {
         if (cell == null)
         {
            drvMemo.ShowTable(WorkType.work14, null);
         }
         else
         {
            if (cell.row.grid == gridParamRez)
            {
               drvMemo.ShowTable(WorkType.work15, gridParamRez);
               foreach (Work w in drvMemo.works)
                  if (w.type == WorkType.work15)
                  {
                     gridParamRez.GetCell(1, 0).Value = w.article;
                     gridParamRez.GetCell(1, 1).Value = w.count.ToString();
                     Cell c = gridParamRez.GetCell(1, 2);
                     c.Value = w.index.ToString();
                     break;
                  }
            }
         }
      }

      /// <summary>
      /// Инициализация всех гридов
      /// </summary>
      void InitGrids()
      {
         InitGrid(gridParamRez, true, pnTblParamRez, WorkType.work15);
         InitGrid(gridComiss, false, pnTblComiss, WorkType.work19);
         InitGrid(gridOther, false, pnTblOther, WorkType.work52);
      }      

      /// <summary>
      /// Клик по 2му чекбоксу
      /// </summary>
      private void chbParamRez_CheckedChanged(object sender, EventArgs e)
      {
         if (chbParamRez.Checked)
         {
            pnTblParamRez.Visible = true;
         }
         else
         {
            pnTblParamRez.Visible = false;
            for (int i = 0; i < drvMemo.works.Count; ++i )
               if (drvMemo.works[i].type == WorkType.work15)
                  drvMemo.works.RemoveAt(i--);
         }
      }

      /// <summary>
      /// Клик по 1му чекбоксу
      /// </summary>
      private void chbPtk_CheckedChanged(object sender, EventArgs e)
      {
         if (chbPtk.Checked)
         {
            pnTblPtk.Visible = true;
         }
         else
         {
            pnTblPtk.Visible = false;
            for (int i = 0; i < drvMemo.works.Count; ++i )
               if (drvMemo.works[i].type == WorkType.work14)
                  drvMemo.works.RemoveAt(i--);
         }
      }

      /// <summary>
      /// Клик по 3му чекбоксу
      /// </summary>
      private void chbComiss_CheckedChanged(object sender, EventArgs e)
      {
         if (chbComiss.Checked)
         {
            pnTblComiss.Visible = true;
         }
         else
         {
            pnTblComiss.Visible = false;            
         }
      }

      /// <summary>
      /// Клик по 4му чекбоксу
      /// </summary>
      private void chbOther_CheckedChanged(object sender, EventArgs e)
      {
         if (chbOther.Checked)
         {
            pnTblOther.Visible = true;
         }
         else
         {
            pnTblOther.Visible = false;
         }
      }

      /// <summary>
      /// Клик по 5му чекбоксу
      /// </summary>
      private void chbOdo_CheckedChanged(object sender, EventArgs e)
      {
         if (chbOdo.Checked)
            tbOdo.Enabled = true;
         else
         {
            tbOdo.Enabled = false;
            tbOdo.Text = "";
         }
      }    
      
      /// <summary>
      /// Загрузка заявки
      /// </summary>
      public void LoadApp()
      {
         drvMemo.LoadApp();
         foreach (Work w in drvMemo.works)
         {
            switch (w.type)
            {
               case WorkType.work14:
                  {
                     chbPtk.Checked = true;
                  } break;
               case WorkType.work15:
                  {
                     chbParamRez.Checked = true;
                     gridParamRez.GetCell(1, 0).Value = w.article;
                     gridParamRez.GetCell(1, 1).Value = w.count.ToString();
                     gridParamRez.GetCell(1, 2).Value = w.index.ToString();
                    // grid2.GetCell(1, 2).Value = w.notes;
                  } break;
               case WorkType.work19:
                  {
                     chbComiss.Checked = true;
                     gridComiss.GetCell(1, 0).Value = w.article;
                     gridComiss.GetCell(1, 1).Value = w.count.ToString();                     
                     gridComiss.GetCell(1, 2).Value = w.index.ToString();
                     gridComiss.GetCell(1, 3).Value = (w.count * w.index).ToString();
                  } break;
               case WorkType.work52:
                  {
                     chbOther.Checked = true;
                     gridOther.GetCell(1, 0).Value = w.article;
                     gridOther.GetCell(1, 1).Value = w.count.ToString();
                     gridOther.GetCell(1, 2).Value = w.index.ToString();
                  } break;
               case WorkType.work50:
                  {
                     chbOdo.Checked = true;
                     tbOdo.Text = w.index.ToString();
                  } break;              
               default: break;
            }
         }
      }

      /// <summary>
      /// Сохраненние заявки
      /// </summary>
      public void SaveApp()
      {
         Work w = new Work();
         
         for (int i = 0; i < drvMemo.works.Count; ++i)
            if (drvMemo.works[i].type != WorkType.work14 && drvMemo.works[i].type != WorkType.work15)
            {
               drvMemo.works.RemoveAt(i);
               i--;
            }

         if (chbComiss.Checked && chbComiss.Enabled)
         {
            w = new Work();
            w.type = WorkType.work19;
            w.article = gridComiss.GetCell(1, 0).Value.ToString();
            w.count = Convert.ToInt32(gridComiss.GetCell(1, 1).Value.ToString());
            w.index = gridComiss.GetCell(1, 2).Value == "" ? 1 : ConvertType.ToDouble(gridComiss.GetCell(1, 2).Value.ToString(), 1); //Convert.ToDouble(grid5.GetCell(1, 2).Value.ToString());
         //   w.notes = grid5.GetCell(1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chbOther.Checked && chbOther.Enabled)
         {
            w = new Work();
            w.type = WorkType.work52;
            w.article = gridOther.GetCell(1, 0).Value.ToString();
            w.count = Convert.ToInt32(gridOther.GetCell(1, 1).Value.ToString());
            w.index = gridOther.GetCell(1, 2).Value == "" ? 1 : ConvertType.ToDouble(gridOther.GetCell(1, 2).Value.ToString(), 1); //Convert.ToDouble(grid6.GetCell(1, 2).Value.ToString());
         //   w.notes = grid6.GetCell(1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chbOdo.Checked && chbOdo.Enabled)
         {
            w = new Work();
            w.type = WorkType.work50;           
            w.index = Convert.ToDouble(tbOdo.Text);            
            drvMemo.works.Add(w);
         }
        
         if (!chbPtk.Checked)
         {
            for (int i = 0; i < drvMemo.works.Count; ++i)
               if (drvMemo.works[i].type == WorkType.work14)
                  drvMemo.works.RemoveAt(i);
         }
         if (!chbParamRez.Checked)
         {
            for (int i = 0; i < drvMemo.works.Count; ++i)
               if (drvMemo.works[i].type == WorkType.work15)
                  drvMemo.works.RemoveAt(i);
         }
         
         drvMemo.SaveApp();
      }

      /// <summary>
      /// Удаление заявки
      /// </summary>
      private void Delete_Click(object sender, EventArgs e)
      {
         //LoadApp();
         if (drvMemo.DeleteApp())
            this.DialogResult = DialogResult.Cancel;            
      }

      /// <summary>
      /// Отправление заявки в ДРВ
      /// </summary>
      private void ToDRV_Click(object sender, EventArgs e)
      {
         if (MessageBox.Show("Відправити службову записку до ДРВ?", "Підтвердження відправки", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
         {
            if (drvMemo.AppToDRV())
               this.DialogResult = DialogResult.OK;
            else
               this.btPrint.Focus();
         }
      }

      /// <summary>
      /// Нажатие на кнопку "Печать"
      /// </summary>
      private void Print_Click(object sender, EventArgs e)
      {
         SaveApp();
         drvMemo.PrintDoc();
         btDelete.Visible = true;
      }

      private void btPTK_Click(object sender, EventArgs e)
      {
         TableButtonClick(null);
      }

      private void tbOwner_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.KeyCode == Keys.Enter)
         {
            drvMemo.SelectOwner(tbOwner.Text);
            tbOwner.Text = drvMemo.OwnerName;
         }
      }
   }
}
