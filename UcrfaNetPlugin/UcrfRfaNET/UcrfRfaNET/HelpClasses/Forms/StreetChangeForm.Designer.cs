﻿namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    partial class StreetChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RemLink = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel_refDoc = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.text_Code_KOATU = new System.Windows.Forms.TextBox();
            this.text_nas_punct = new System.Windows.Forms.TextBox();
            this.text_name_street = new System.Windows.Forms.TextBox();
            this.text_type_street = new System.Windows.Forms.TextBox();
            this.textBox_new_name_street = new System.Windows.Forms.TextBox();
            this.label_REF_DOC = new System.Windows.Forms.Label();
            this.label_KOATU = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.label_new_name_street = new System.Windows.Forms.Label();
            this.label_name_street = new System.Windows.Forms.Label();
            this.label_type_street = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 308);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(109, 308);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(145, 23);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RemLink);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.linkLabel_refDoc);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.text_Code_KOATU);
            this.groupBox1.Controls.Add(this.text_nas_punct);
            this.groupBox1.Controls.Add(this.text_name_street);
            this.groupBox1.Controls.Add(this.text_type_street);
            this.groupBox1.Controls.Add(this.textBox_new_name_street);
            this.groupBox1.Controls.Add(this.label_REF_DOC);
            this.groupBox1.Controls.Add(this.label_KOATU);
            this.groupBox1.Controls.Add(this.label_name);
            this.groupBox1.Controls.Add(this.label_new_name_street);
            this.groupBox1.Controls.Add(this.label_name_street);
            this.groupBox1.Controls.Add(this.label_type_street);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(380, 302);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // RemLink
            // 
            this.RemLink.ForeColor = System.Drawing.Color.Red;
            this.RemLink.Location = new System.Drawing.Point(314, 257);
            this.RemLink.Name = "RemLink";
            this.RemLink.Size = new System.Drawing.Size(45, 23);
            this.RemLink.TabIndex = 19;
            this.RemLink.Text = "Х";
            this.RemLink.UseVisualStyleBackColor = true;
            this.RemLink.Click += new System.EventHandler(this.RemLink_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 267);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Посилання:";
            // 
            // linkLabel_refDoc
            // 
            this.linkLabel_refDoc.AutoSize = true;
            this.linkLabel_refDoc.Location = new System.Drawing.Point(150, 267);
            this.linkLabel_refDoc.MaximumSize = new System.Drawing.Size(170, 0);
            this.linkLabel_refDoc.Name = "linkLabel_refDoc";
            this.linkLabel_refDoc.Size = new System.Drawing.Size(0, 13);
            this.linkLabel_refDoc.TabIndex = 17;
            this.linkLabel_refDoc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_refDoc_LinkClicked);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(153, 224);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Завантажити";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // text_Code_KOATU
            // 
            this.text_Code_KOATU.Enabled = false;
            this.text_Code_KOATU.Location = new System.Drawing.Point(153, 183);
            this.text_Code_KOATU.Name = "text_Code_KOATU";
            this.text_Code_KOATU.ReadOnly = true;
            this.text_Code_KOATU.Size = new System.Drawing.Size(206, 20);
            this.text_Code_KOATU.TabIndex = 15;
            // 
            // text_nas_punct
            // 
            this.text_nas_punct.Enabled = false;
            this.text_nas_punct.Location = new System.Drawing.Point(153, 144);
            this.text_nas_punct.Name = "text_nas_punct";
            this.text_nas_punct.ReadOnly = true;
            this.text_nas_punct.Size = new System.Drawing.Size(206, 20);
            this.text_nas_punct.TabIndex = 14;
            // 
            // text_name_street
            // 
            this.text_name_street.Enabled = false;
            this.text_name_street.Location = new System.Drawing.Point(153, 70);
            this.text_name_street.Name = "text_name_street";
            this.text_name_street.ReadOnly = true;
            this.text_name_street.Size = new System.Drawing.Size(206, 20);
            this.text_name_street.TabIndex = 13;
            // 
            // text_type_street
            // 
            this.text_type_street.Enabled = false;
            this.text_type_street.Location = new System.Drawing.Point(153, 28);
            this.text_type_street.Name = "text_type_street";
            this.text_type_street.ReadOnly = true;
            this.text_type_street.Size = new System.Drawing.Size(206, 20);
            this.text_type_street.TabIndex = 12;
            // 
            // textBox_new_name_street
            // 
            this.textBox_new_name_street.Location = new System.Drawing.Point(153, 105);
            this.textBox_new_name_street.Name = "textBox_new_name_street";
            this.textBox_new_name_street.Size = new System.Drawing.Size(206, 20);
            this.textBox_new_name_street.TabIndex = 11;
            // 
            // label_REF_DOC
            // 
            this.label_REF_DOC.AutoSize = true;
            this.label_REF_DOC.Location = new System.Drawing.Point(21, 229);
            this.label_REF_DOC.Name = "label_REF_DOC";
            this.label_REF_DOC.Size = new System.Drawing.Size(61, 13);
            this.label_REF_DOC.TabIndex = 5;
            this.label_REF_DOC.Text = "Документ:";
            // 
            // label_KOATU
            // 
            this.label_KOATU.AutoSize = true;
            this.label_KOATU.Location = new System.Drawing.Point(22, 186);
            this.label_KOATU.Name = "label_KOATU";
            this.label_KOATU.Size = new System.Drawing.Size(77, 13);
            this.label_KOATU.TabIndex = 4;
            this.label_KOATU.Text = "Код КОАТУУ:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(22, 144);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(64, 13);
            this.label_name.TabIndex = 3;
            this.label_name.Text = "Нас. пункт:";
            // 
            // label_new_name_street
            // 
            this.label_new_name_street.AutoSize = true;
            this.label_new_name_street.Location = new System.Drawing.Point(21, 108);
            this.label_new_name_street.Name = "label_new_name_street";
            this.label_new_name_street.Size = new System.Drawing.Size(103, 13);
            this.label_new_name_street.TabIndex = 2;
            this.label_new_name_street.Text = "Нова назва вулиці:";
            // 
            // label_name_street
            // 
            this.label_name_street.AutoSize = true;
            this.label_name_street.Location = new System.Drawing.Point(22, 70);
            this.label_name_street.Name = "label_name_street";
            this.label_name_street.Size = new System.Drawing.Size(76, 13);
            this.label_name_street.TabIndex = 1;
            this.label_name_street.Text = "Назва вулиці:";
            // 
            // label_type_street
            // 
            this.label_type_street.AutoSize = true;
            this.label_type_street.Location = new System.Drawing.Point(22, 28);
            this.label_type_street.Name = "label_type_street";
            this.label_type_street.Size = new System.Drawing.Size(63, 13);
            this.label_type_street.TabIndex = 0;
            this.label_type_street.Text = "Тип вулиці:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(275, 308);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // StreetChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 333);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(396, 371);
            this.MinimumSize = new System.Drawing.Size(396, 371);
            this.Name = "StreetChangeForm";
            this.Text = "Зміна назви вулиці";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_new_name_street;
        private System.Windows.Forms.Label label_REF_DOC;
        private System.Windows.Forms.Label label_KOATU;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_new_name_street;
        private System.Windows.Forms.Label label_name_street;
        private System.Windows.Forms.Label label_type_street;
        private System.Windows.Forms.TextBox text_name_street;
        private System.Windows.Forms.TextBox text_type_street;
        private System.Windows.Forms.TextBox text_nas_punct;
        private System.Windows.Forms.TextBox text_Code_KOATU;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel_refDoc;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button RemLink;
        private System.Windows.Forms.Button button3;
    }
}