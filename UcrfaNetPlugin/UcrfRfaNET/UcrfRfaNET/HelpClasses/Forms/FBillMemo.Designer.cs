﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FBillMemo
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FBillMemo));
          this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
          this.panel10 = new System.Windows.Forms.Panel();
          this.tbOwner = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
          this.pnHeader = new System.Windows.Forms.Panel();
          this.chb1 = new System.Windows.Forms.CheckBox();
          this.panel1 = new System.Windows.Forms.Panel();
          this.chb3 = new System.Windows.Forms.CheckBox();
          this.panel3 = new System.Windows.Forms.Panel();
          this.chb4 = new System.Windows.Forms.CheckBox();
          this.panel4 = new System.Windows.Forms.Panel();
          this.chb5 = new System.Windows.Forms.CheckBox();
          this.panel5 = new System.Windows.Forms.Panel();
          this.chb6 = new System.Windows.Forms.CheckBox();
          this.panel6 = new System.Windows.Forms.Panel();
          this.pnChbPtk = new System.Windows.Forms.Panel();
          this.chbPtk = new System.Windows.Forms.CheckBox();
          this.btPtk = new System.Windows.Forms.Button();
          this.chbParamRez = new System.Windows.Forms.CheckBox();
          this.pnTblParamRez = new System.Windows.Forms.Panel();
          this.chbComiss = new System.Windows.Forms.CheckBox();
          this.pnTblComiss = new System.Windows.Forms.Panel();
          this.pnOdo = new System.Windows.Forms.Panel();
          this.tbOdo = new System.Windows.Forms.TextBox();
          this.chbOdo = new System.Windows.Forms.CheckBox();
          this.chb2 = new System.Windows.Forms.CheckBox();
          this.panel2 = new System.Windows.Forms.Panel();
          this.chb7 = new System.Windows.Forms.CheckBox();
          this.panel7 = new System.Windows.Forms.Panel();
          this.chb8 = new System.Windows.Forms.CheckBox();
          this.panel8 = new System.Windows.Forms.Panel();
          this.chbOther = new System.Windows.Forms.CheckBox();
          this.pnTblOther = new System.Windows.Forms.Panel();
          this.panel9 = new System.Windows.Forms.Panel();
          this.btCancel = new System.Windows.Forms.Button();
          this.btDelete = new System.Windows.Forms.Button();
          this.btPrint = new System.Windows.Forms.Button();
          this.btDRV = new System.Windows.Forms.Button();
          this.btExit = new System.Windows.Forms.Button();
          this.flowLayoutPanel2.SuspendLayout();
          this.panel10.SuspendLayout();
          this.groupBox1.SuspendLayout();
          this.flowLayoutPanel1.SuspendLayout();
          this.pnChbPtk.SuspendLayout();
          this.pnOdo.SuspendLayout();
          this.panel9.SuspendLayout();
          this.SuspendLayout();
          // 
          // flowLayoutPanel2
          // 
          this.flowLayoutPanel2.AutoSize = true;
          this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.flowLayoutPanel2.Controls.Add(this.panel10);
          this.flowLayoutPanel2.Controls.Add(this.label1);
          this.flowLayoutPanel2.Controls.Add(this.groupBox1);
          this.flowLayoutPanel2.Controls.Add(this.panel9);
          this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
          this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
          this.flowLayoutPanel2.Name = "flowLayoutPanel2";
          this.flowLayoutPanel2.Size = new System.Drawing.Size(453, 839);
          this.flowLayoutPanel2.TabIndex = 0;
          // 
          // panel10
          // 
          this.panel10.Controls.Add(this.tbOwner);
          this.panel10.Controls.Add(this.label2);
          this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
          this.panel10.Location = new System.Drawing.Point(3, 3);
          this.panel10.Name = "panel10";
          this.panel10.Size = new System.Drawing.Size(482, 34);
          this.panel10.TabIndex = 3;
          // 
          // tbOwner
          // 
          this.tbOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbOwner.Location = new System.Drawing.Point(80, 8);
          this.tbOwner.Name = "tbOwner";
          this.tbOwner.Size = new System.Drawing.Size(385, 20);
          this.tbOwner.TabIndex = 3;
          this.tbOwner.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbOwner_KeyDown);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
          this.label2.Location = new System.Drawing.Point(9, 11);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(65, 13);
          this.label2.TabIndex = 2;
          this.label2.Text = "Контрагент";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.label1.Location = new System.Drawing.Point(3, 40);
          this.label1.Name = "label1";
          this.label1.Padding = new System.Windows.Forms.Padding(2, 10, 2, 10);
          this.label1.Size = new System.Drawing.Size(482, 33);
          this.label1.TabIndex = 0;
          this.label1.Text = "Спочатку виберіть заяви у формі Пакет заяв. Потім виберіть види робіт (послуг).";
          this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // groupBox1
          // 
          this.groupBox1.AutoSize = true;
          this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.groupBox1.Controls.Add(this.flowLayoutPanel1);
          this.groupBox1.Location = new System.Drawing.Point(3, 76);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(482, 743);
          this.groupBox1.TabIndex = 1;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Види робіт (послуг) УДЦР";
          // 
          // flowLayoutPanel1
          // 
          this.flowLayoutPanel1.AutoSize = true;
          this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.flowLayoutPanel1.Controls.Add(this.pnHeader);
          this.flowLayoutPanel1.Controls.Add(this.chb1);
          this.flowLayoutPanel1.Controls.Add(this.panel1);
          this.flowLayoutPanel1.Controls.Add(this.chb3);
          this.flowLayoutPanel1.Controls.Add(this.panel3);
          this.flowLayoutPanel1.Controls.Add(this.chb4);
          this.flowLayoutPanel1.Controls.Add(this.panel4);
          this.flowLayoutPanel1.Controls.Add(this.chb5);
          this.flowLayoutPanel1.Controls.Add(this.panel5);
          this.flowLayoutPanel1.Controls.Add(this.chb6);
          this.flowLayoutPanel1.Controls.Add(this.panel6);
          this.flowLayoutPanel1.Controls.Add(this.pnChbPtk);
          this.flowLayoutPanel1.Controls.Add(this.chbParamRez);
          this.flowLayoutPanel1.Controls.Add(this.pnTblParamRez);
          this.flowLayoutPanel1.Controls.Add(this.chbComiss);
          this.flowLayoutPanel1.Controls.Add(this.pnTblComiss);
          this.flowLayoutPanel1.Controls.Add(this.pnOdo);
          this.flowLayoutPanel1.Controls.Add(this.chb2);
          this.flowLayoutPanel1.Controls.Add(this.panel2);
          this.flowLayoutPanel1.Controls.Add(this.chb7);
          this.flowLayoutPanel1.Controls.Add(this.panel7);
          this.flowLayoutPanel1.Controls.Add(this.chb8);
          this.flowLayoutPanel1.Controls.Add(this.panel8);
          this.flowLayoutPanel1.Controls.Add(this.chbOther);
          this.flowLayoutPanel1.Controls.Add(this.pnTblOther);
          this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
          this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
          this.flowLayoutPanel1.Name = "flowLayoutPanel1";
          this.flowLayoutPanel1.Size = new System.Drawing.Size(476, 724);
          this.flowLayoutPanel1.TabIndex = 0;
          // 
          // pnHeader
          // 
          this.pnHeader.Location = new System.Drawing.Point(3, 3);
          this.pnHeader.Name = "pnHeader";
          this.pnHeader.Size = new System.Drawing.Size(425, 20);
          this.pnHeader.TabIndex = 25;
          // 
          // chb1
          // 
          this.chb1.AutoSize = true;
          this.chb1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.chb1.Location = new System.Drawing.Point(3, 29);
          this.chb1.Name = "chb1";
          this.chb1.Size = new System.Drawing.Size(470, 17);
          this.chb1.TabIndex = 0;
          this.chb1.Text = "Розгляд заяви про видачу висновків щодо ЕМС";
          this.chb1.UseVisualStyleBackColor = true;
          this.chb1.CheckedChanged += new System.EventHandler(this.chb1_CheckedChanged);
          // 
          // panel1
          // 
          this.panel1.Location = new System.Drawing.Point(3, 52);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(425, 20);
          this.panel1.TabIndex = 1;
          this.panel1.Visible = false;
          // 
          // chb3
          // 
          this.chb3.AutoSize = true;
          this.chb3.Location = new System.Drawing.Point(3, 78);
          this.chb3.Name = "chb3";
          this.chb3.Size = new System.Drawing.Size(255, 17);
          this.chb3.TabIndex = 4;
          this.chb3.Text = "Розрахунок електромагнітної сумісності РЕЗ";
          this.chb3.UseVisualStyleBackColor = true;
          this.chb3.CheckedChanged += new System.EventHandler(this.chb3_CheckedChanged);
          // 
          // panel3
          // 
          this.panel3.Location = new System.Drawing.Point(3, 101);
          this.panel3.Name = "panel3";
          this.panel3.Size = new System.Drawing.Size(425, 20);
          this.panel3.TabIndex = 5;
          this.panel3.Visible = false;
          // 
          // chb4
          // 
          this.chb4.AutoSize = true;
          this.chb4.Location = new System.Drawing.Point(3, 127);
          this.chb4.Name = "chb4";
          this.chb4.Size = new System.Drawing.Size(196, 17);
          this.chb4.TabIndex = 6;
          this.chb4.Text = "Підбір частот з розрахунком ЕМС";
          this.chb4.UseVisualStyleBackColor = true;
          this.chb4.CheckedChanged += new System.EventHandler(this.chb4_CheckedChanged);
          // 
          // panel4
          // 
          this.panel4.Location = new System.Drawing.Point(3, 150);
          this.panel4.Name = "panel4";
          this.panel4.Size = new System.Drawing.Size(425, 20);
          this.panel4.TabIndex = 7;
          this.panel4.Visible = false;
          // 
          // chb5
          // 
          this.chb5.AutoSize = true;
          this.chb5.Location = new System.Drawing.Point(3, 176);
          this.chb5.Name = "chb5";
          this.chb5.Size = new System.Drawing.Size(388, 17);
          this.chb5.TabIndex = 8;
          this.chb5.Text = "Міжнародна координація параметрів одного РЧП з однією адм. зв\'язку";
          this.chb5.UseVisualStyleBackColor = true;
          this.chb5.CheckedChanged += new System.EventHandler(this.chb5_CheckedChanged);
          // 
          // panel5
          // 
          this.panel5.Location = new System.Drawing.Point(3, 199);
          this.panel5.Name = "panel5";
          this.panel5.Size = new System.Drawing.Size(425, 20);
          this.panel5.TabIndex = 9;
          this.panel5.Visible = false;
          // 
          // chb6
          // 
          this.chb6.AutoSize = true;
          this.chb6.Location = new System.Drawing.Point(3, 225);
          this.chb6.Name = "chb6";
          this.chb6.Size = new System.Drawing.Size(257, 17);
          this.chb6.TabIndex = 10;
          this.chb6.Text = "Оформлення висновків щодо ЕМС РЕЗ та ВП";
          this.chb6.UseVisualStyleBackColor = true;
          this.chb6.CheckedChanged += new System.EventHandler(this.chb6_CheckedChanged);
          // 
          // panel6
          // 
          this.panel6.Location = new System.Drawing.Point(3, 248);
          this.panel6.Name = "panel6";
          this.panel6.Size = new System.Drawing.Size(425, 20);
          this.panel6.TabIndex = 11;
          this.panel6.Visible = false;
          // 
          // pnChbPtk
          // 
          this.pnChbPtk.AutoSize = true;
          this.pnChbPtk.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.pnChbPtk.Controls.Add(this.chbPtk);
          this.pnChbPtk.Controls.Add(this.btPtk);
          this.pnChbPtk.Location = new System.Drawing.Point(3, 274);
          this.pnChbPtk.Name = "pnChbPtk";
          this.pnChbPtk.Size = new System.Drawing.Size(210, 28);
          this.pnChbPtk.TabIndex = 16;
          // 
          // chbPtk
          // 
          this.chbPtk.AutoSize = true;
          this.chbPtk.Location = new System.Drawing.Point(0, 3);
          this.chbPtk.Name = "chbPtk";
          this.chbPtk.Size = new System.Drawing.Size(113, 17);
          this.chbPtk.TabIndex = 1;
          this.chbPtk.Text = "Проведення ПТК";
          this.chbPtk.UseVisualStyleBackColor = true;
          this.chbPtk.CheckedChanged += new System.EventHandler(this.chbPtk_CheckedChanged);
          // 
          // btPtk
          // 
          this.btPtk.Location = new System.Drawing.Point(121, 0);
          this.btPtk.Name = "btPtk";
          this.btPtk.Size = new System.Drawing.Size(86, 25);
          this.btPtk.TabIndex = 0;
          this.btPtk.Text = "Таблиця...";
          this.btPtk.UseVisualStyleBackColor = true;
          this.btPtk.Visible = false;
          this.btPtk.Click += new System.EventHandler(this.btPtk_Click);
          // 
          // chbParamRez
          // 
          this.chbParamRez.AutoSize = true;
          this.chbParamRez.Location = new System.Drawing.Point(3, 308);
          this.chbParamRez.Name = "chbParamRez";
          this.chbParamRez.Size = new System.Drawing.Size(267, 17);
          this.chbParamRez.TabIndex = 4;
          this.chbParamRez.Text = "Проведення вимірювань параметрів РЕЗ та ВП";
          this.chbParamRez.UseVisualStyleBackColor = true;
          this.chbParamRez.CheckedChanged += new System.EventHandler(this.chbParamRez_CheckedChanged);
          // 
          // pnTblParamRez
          // 
          this.pnTblParamRez.Location = new System.Drawing.Point(3, 331);
          this.pnTblParamRez.Name = "pnTblParamRez";
          this.pnTblParamRez.Size = new System.Drawing.Size(425, 20);
          this.pnTblParamRez.TabIndex = 19;
          this.pnTblParamRez.Visible = false;
          // 
          // chbComiss
          // 
          this.chbComiss.AutoSize = true;
          this.chbComiss.Location = new System.Drawing.Point(3, 357);
          this.chbComiss.Name = "chbComiss";
          this.chbComiss.Size = new System.Drawing.Size(143, 17);
          this.chbComiss.TabIndex = 6;
          this.chbComiss.Text = "Участь у роботі комісій";
          this.chbComiss.UseVisualStyleBackColor = true;
          this.chbComiss.CheckedChanged += new System.EventHandler(this.chbComiss_CheckedChanged);
          // 
          // pnTblComiss
          // 
          this.pnTblComiss.Location = new System.Drawing.Point(3, 380);
          this.pnTblComiss.Name = "pnTblComiss";
          this.pnTblComiss.Size = new System.Drawing.Size(470, 39);
          this.pnTblComiss.TabIndex = 21;
          this.pnTblComiss.Visible = false;
          // 
          // pnOdo
          // 
          this.pnOdo.Controls.Add(this.tbOdo);
          this.pnOdo.Controls.Add(this.chbOdo);
          this.pnOdo.Location = new System.Drawing.Point(3, 425);
          this.pnOdo.Name = "pnOdo";
          this.pnOdo.Size = new System.Drawing.Size(425, 24);
          this.pnOdo.TabIndex = 24;
          // 
          // tbOdo
          // 
          this.tbOdo.Enabled = false;
          this.tbOdo.Location = new System.Drawing.Point(171, 2);
          this.tbOdo.Name = "tbOdo";
          this.tbOdo.Size = new System.Drawing.Size(97, 20);
          this.tbOdo.TabIndex = 11;
          // 
          // chbOdo
          // 
          this.chbOdo.AutoSize = true;
          this.chbOdo.Location = new System.Drawing.Point(0, 3);
          this.chbOdo.Name = "chbOdo";
          this.chbOdo.Size = new System.Drawing.Size(162, 17);
          this.chbOdo.TabIndex = 10;
          this.chbOdo.Text = "Пробіг автотранспорту, км";
          this.chbOdo.UseVisualStyleBackColor = true;
          this.chbOdo.CheckedChanged += new System.EventHandler(this.chbOdo_CheckedChanged);
          // 
          // chb2
          // 
          this.chb2.AutoSize = true;
          this.chb2.Location = new System.Drawing.Point(3, 455);
          this.chb2.Name = "chb2";
          this.chb2.Size = new System.Drawing.Size(377, 17);
          this.chb2.TabIndex = 2;
          this.chb2.Text = "Оформл., переоформл. та подовження дії дозволу на експл. РЕЗ, ВП";
          this.chb2.UseVisualStyleBackColor = true;
          this.chb2.CheckedChanged += new System.EventHandler(this.chb2_CheckedChanged);
          // 
          // panel2
          // 
          this.panel2.Location = new System.Drawing.Point(3, 478);
          this.panel2.Name = "panel2";
          this.panel2.Size = new System.Drawing.Size(425, 39);
          this.panel2.TabIndex = 3;
          this.panel2.Visible = false;
          // 
          // chb7
          // 
          this.chb7.AutoSize = true;
          this.chb7.Location = new System.Drawing.Point(3, 523);
          this.chb7.Name = "chb7";
          this.chb7.Size = new System.Drawing.Size(436, 17);
          this.chb7.TabIndex = 12;
          this.chb7.Text = "Розгляд заявочн. докум. про надання дозв. на експл. РЕЗ (без розрахунку ЕМС)";
          this.chb7.UseVisualStyleBackColor = true;
          this.chb7.CheckedChanged += new System.EventHandler(this.chb7_CheckedChanged);
          // 
          // panel7
          // 
          this.panel7.Location = new System.Drawing.Point(3, 546);
          this.panel7.Name = "panel7";
          this.panel7.Size = new System.Drawing.Size(425, 39);
          this.panel7.TabIndex = 13;
          this.panel7.Visible = false;
          // 
          // chb8
          // 
          this.chb8.AutoSize = true;
          this.chb8.Location = new System.Drawing.Point(3, 591);
          this.chb8.Name = "chb8";
          this.chb8.Size = new System.Drawing.Size(49, 17);
          this.chb8.TabIndex = 14;
          this.chb8.Text = "Інше";
          this.chb8.UseVisualStyleBackColor = true;
          this.chb8.CheckedChanged += new System.EventHandler(this.chb8_CheckedChanged);
          // 
          // panel8
          // 
          this.panel8.Location = new System.Drawing.Point(3, 614);
          this.panel8.Name = "panel8";
          this.panel8.Size = new System.Drawing.Size(425, 39);
          this.panel8.TabIndex = 15;
          this.panel8.Visible = false;
          // 
          // chbOther
          // 
          this.chbOther.AutoSize = true;
          this.chbOther.Location = new System.Drawing.Point(3, 659);
          this.chbOther.Name = "chbOther";
          this.chbOther.Size = new System.Drawing.Size(88, 17);
          this.chbOther.TabIndex = 8;
          this.chbOther.Text = "Інше (УРЗП)";
          this.chbOther.UseVisualStyleBackColor = true;
          this.chbOther.CheckedChanged += new System.EventHandler(this.chbOther_CheckedChanged);
          // 
          // pnTblOther
          // 
          this.pnTblOther.Location = new System.Drawing.Point(3, 682);
          this.pnTblOther.Name = "pnTblOther";
          this.pnTblOther.Size = new System.Drawing.Size(425, 39);
          this.pnTblOther.TabIndex = 23;
          this.pnTblOther.Visible = false;
          // 
          // panel9
          // 
          this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.panel9.AutoSize = true;
          this.panel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.panel9.Controls.Add(this.btCancel);
          this.panel9.Controls.Add(this.btDelete);
          this.panel9.Controls.Add(this.btPrint);
          this.panel9.Controls.Add(this.btDRV);
          this.panel9.Controls.Add(this.btExit);
          this.panel9.Location = new System.Drawing.Point(491, 3);
          this.panel9.Name = "panel9";
          this.panel9.Size = new System.Drawing.Size(482, 43);
          this.panel9.TabIndex = 2;
          // 
          // btCancel
          // 
          this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btCancel.Image = ((System.Drawing.Image)(resources.GetObject("btCancel.Image")));
          this.btCancel.Location = new System.Drawing.Point(282, 8);
          this.btCancel.Name = "btCancel";
          this.btCancel.Size = new System.Drawing.Size(135, 26);
          this.btCancel.TabIndex = 5;
          this.btCancel.Text = "Анулювати             ДРВ";
          this.btCancel.UseVisualStyleBackColor = true;
          this.btCancel.Visible = false;
          this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
          // 
          // btDelete
          // 
          this.btDelete.Location = new System.Drawing.Point(124, 8);
          this.btDelete.Name = "btDelete";
          this.btDelete.Size = new System.Drawing.Size(79, 26);
          this.btDelete.TabIndex = 4;
          this.btDelete.Text = "Видалити";
          this.btDelete.UseVisualStyleBackColor = true;
          this.btDelete.Click += new System.EventHandler(this.Delete_Click);
          // 
          // btPrint
          // 
          this.btPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.btPrint.Location = new System.Drawing.Point(3, 8);
          this.btPrint.Name = "btPrint";
          this.btPrint.Size = new System.Drawing.Size(115, 26);
          this.btPrint.TabIndex = 0;
          this.btPrint.Text = "Друк";
          this.btPrint.UseVisualStyleBackColor = true;
          this.btPrint.Click += new System.EventHandler(this.Print_Click);
          // 
          // btDRV
          // 
          this.btDRV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btDRV.Location = new System.Drawing.Point(243, 8);
          this.btDRV.Name = "btDRV";
          this.btDRV.Size = new System.Drawing.Size(115, 26);
          this.btDRV.TabIndex = 1;
          this.btDRV.Text = "-> ДРВ";
          this.btDRV.UseVisualStyleBackColor = true;
          this.btDRV.Click += new System.EventHandler(this.ToDRV_Click);
          // 
          // btExit
          // 
          this.btExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btExit.Location = new System.Drawing.Point(364, 8);
          this.btExit.Name = "btExit";
          this.btExit.Size = new System.Drawing.Size(115, 26);
          this.btExit.TabIndex = 2;
          this.btExit.Text = "Вихід";
          this.btExit.UseVisualStyleBackColor = true;
          // 
          // FBillMemo
          // 
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.AutoSize = true;
          this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
          this.ClientSize = new System.Drawing.Size(453, 839);
          this.Controls.Add(this.flowLayoutPanel2);
          this.Name = "FBillMemo";
          this.Text = "Створення службової записки на виставлення рахунку";
          this.flowLayoutPanel2.ResumeLayout(false);
          this.flowLayoutPanel2.PerformLayout();
          this.panel10.ResumeLayout(false);
          this.panel10.PerformLayout();
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.flowLayoutPanel1.ResumeLayout(false);
          this.flowLayoutPanel1.PerformLayout();
          this.pnChbPtk.ResumeLayout(false);
          this.pnChbPtk.PerformLayout();
          this.pnOdo.ResumeLayout(false);
          this.pnOdo.PerformLayout();
          this.panel9.ResumeLayout(false);
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
      private System.Windows.Forms.CheckBox chb1;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.CheckBox chb2;
      private System.Windows.Forms.CheckBox chb3;
      private System.Windows.Forms.CheckBox chb4;
      private System.Windows.Forms.CheckBox chb5;
      private System.Windows.Forms.CheckBox chb6;
      private System.Windows.Forms.CheckBox chb7;
      private System.Windows.Forms.CheckBox chb8;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.Panel panel5;
      private System.Windows.Forms.Panel panel7;
      private System.Windows.Forms.Panel panel8;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button btPrint;
      private System.Windows.Forms.Button btDRV;
      private System.Windows.Forms.Button btExit;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Panel panel9;
      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
      private System.Windows.Forms.Button btDelete;
      private System.Windows.Forms.Button btCancel;
      private System.Windows.Forms.Panel panel10;
      protected System.Windows.Forms.TextBox tbOwner;
      protected System.Windows.Forms.Label label2;
      private System.Windows.Forms.Panel pnChbPtk;
      private System.Windows.Forms.CheckBox chbPtk;
      private System.Windows.Forms.Button btPtk;
      private System.Windows.Forms.CheckBox chbParamRez;
      private System.Windows.Forms.Panel pnTblParamRez;
      private System.Windows.Forms.CheckBox chbComiss;
      private System.Windows.Forms.Panel pnTblComiss;
      private System.Windows.Forms.CheckBox chbOther;
      private System.Windows.Forms.Panel pnTblOther;
      private System.Windows.Forms.Panel pnOdo;
      private System.Windows.Forms.TextBox tbOdo;
      private System.Windows.Forms.CheckBox chbOdo;
      private System.Windows.Forms.Panel pnHeader;
      private System.Windows.Forms.Panel panel6;
   }
}