﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class SimpleLookupStringForm : FBaseForm
    {
        private string[] _initItems;
        public int SelectedIndex { get; set; }
        public string SelectedText { get; set; }

        
        public SimpleLookupStringForm(string[] initializeItems)
        {
            SelectedIndex = -1;
            _initItems = initializeItems;
            InitializeComponent();
        }

        private void SimpleLookupStringForm_Load(object sender, EventArgs e)
        {
            foreach(string initString in _initItems)
            {
                lbItems.Items.Add(initString);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SelectedIndex = lbItems.SelectedIndex;
            SelectedText = lbItems.Items[lbItems.SelectedIndex].ToString();
        }
    }
}
