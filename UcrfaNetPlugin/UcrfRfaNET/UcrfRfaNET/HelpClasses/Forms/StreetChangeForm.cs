﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using ICSM;
using System.IO;
using Lis.CommonLib.Binding;
using XICSM.UcrfRfaNET.UtilityClass;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.HelpClasses.Component;
using OrmCs;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class StreetChangeForm :  Form
    {

        public string FileLink { get; set; }
        public static bool isActive { get; set; }
        public static int ID { get; set; }
        public static string TableName { get; set; }


        public StreetChangeForm(string FieldStreetType, string FieldStreetName, string Koatuu, string  NasPunct)
        {
            InitializeComponent();
            CLocaliz.TxT(this);
            button2.Text = CLocaliz.TxT("OK");
            button3.Text = CLocaliz.TxT("Close");
            btnDelete.Text = CLocaliz.TxT("Delete record");
            isActive = false;
            RemLink.Enabled = false;
            Init(FieldStreetType, FieldStreetName, Koatuu, NasPunct);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cntx"></param>
        public StreetChangeForm(RecordPtr cntx)
        {
            InitializeComponent();
            CLocaliz.TxT(this);
            button2.Text = CLocaliz.TxT("OK");
            button3.Text = CLocaliz.TxT("Close");
            btnDelete.Text = CLocaliz.TxT("Delete record");
            isActive = false;
            IMRecordset rs = new IMRecordset(cntx.Table, IMRecordset.Mode.ReadOnly);
            rs.Select("ID,STREET_TYPE,STREET_NAME,TERRAIN_CODE");
            rs.SetWhere("ID" , IMRecordset.Operation.Eq, cntx.Id);
            try
            {
                rs.Open();
                if (!rs.IsEOF())
                {
                    Init(rs.GetS("STREET_TYPE"), rs.GetS("STREET_NAME"), rs.GetS("TERRAIN_CODE"),"");
                }
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }

        }

        public void Init(string FieldStreetType, string FieldStreetName, string Koatuu, string  NasPunct)
        {
            text_type_street.Text = FieldStreetType;
            text_name_street.Text = FieldStreetName;
            text_Code_KOATU.Text = Koatuu;
            text_nas_punct.Text = NasPunct;
            Load();
        }

       
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FieldStreetType"></param>
        /// <param name="FieldStreetName"></param>
        /// <param name="Koatuu"></param>
        /// <param name="NasPunct"></param>
        public void Load()
        {
            try
            {
                string Terr_code = text_Code_KOATU.Text.Contains("_") ? text_Code_KOATU.Text.Replace("_", "") : text_Code_KOATU.Text;
                IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadOnly);
                rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, text_type_street.Text);
                rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, text_name_street.Text);
                rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Like, "*" + Terr_code + "*");
                for (rsStreet.Open(); !rsStreet.IsEOF(); rsStreet.MoveNext())
                {
                    textBox_new_name_street.Text = rsStreet.GetS("STREET_NAME_NEW");
                    FileLink = rsStreet.GetS("DOC_REF");
                    linkLabel_refDoc.Text = FileLink;
                    if (string.IsNullOrEmpty(FileLink)) { RemLink.Enabled = false; } else { RemLink.Enabled = true; }
                    isActive = true;
                    break;
                }
                rsStreet.Close();
                rsStreet.Destroy();
            }
            catch (Exception)
            {

            }
        }

        public static bool Load(string FieldStreetType, string FieldStreetName, string Koatuu)
        {
            bool isAct = false;
            try
            {
                IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadOnly);
                rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, FieldStreetType != null ? FieldStreetType : "");
                rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, FieldStreetName != null ? FieldStreetName : "");
                rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Like, Koatuu != null ? Koatuu : "");
                for (rsStreet.Open(); !rsStreet.IsEOF(); rsStreet.MoveNext())
                {
                    isAct = true;
                    break;
                }
                rsStreet.Close();
                rsStreet.Destroy();

                if (Koatuu != null)
                {
                    string Terr_code = Koatuu.Contains("_") ? Koatuu.Replace("_", "") : Koatuu;
                    rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadOnly);
                    rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                    rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, FieldStreetType != null ? FieldStreetType : "");
                    rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, FieldStreetName != null ? FieldStreetName : "");
                    rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Like, "*" + Terr_code + "*");
                    for (rsStreet.Open(); !rsStreet.IsEOF(); rsStreet.MoveNext())
                    {


                        isAct = true;
                        break;
                    }
                    rsStreet.Close();
                    rsStreet.Destroy();
                }
            }
            catch (Exception)
            { }
            return isAct;
        }

        public static string GetStreeNameNew(string FieldStreetType, string FieldStreetName, string Koatuu)
        {
            string Name = "";
            try
            {
                IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadOnly);
                rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, FieldStreetType != null ? FieldStreetType : "");
                rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, FieldStreetName != null ? FieldStreetName : "");
                rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Like, Koatuu != null ? Koatuu : "");
                for (rsStreet.Open(); !rsStreet.IsEOF(); rsStreet.MoveNext())
                {
                    Name = rsStreet.GetS("STREET_NAME_NEW");
                    break;
                }
                rsStreet.Close();
                rsStreet.Destroy();

                if (Koatuu != null)
                {
                    string Terr_code = Koatuu.Contains("_") ? Koatuu.Replace("_", "") : Koatuu;
                    rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadOnly);
                    rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                    rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, FieldStreetType != null ? FieldStreetType : "");
                    rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, FieldStreetName != null ? FieldStreetName : "");
                    rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Like, "*" + Terr_code + "*");
                    for (rsStreet.Open(); !rsStreet.IsEOF(); rsStreet.MoveNext())
                    {
                        Name = rsStreet.GetS("STREET_NAME_NEW");
                        break;
                    }
                    rsStreet.Close();
                    rsStreet.Destroy();
                }
            }
            catch (Exception)
            { }
            return Name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Table_Name"></param>
        /// <param name="Table_Id"></param>
        public void DeleteDocLink(string Table_Name, int Table_Id)
        {
            try
            {
                YDocfiles docf_create = new YDocfiles(); docf_create.Format("*");
                YDoclink doclnk = new YDoclink();
                string PATH_OLD = FileLink;
                if (PATH_OLD != "")
                {
                    doclnk.Format("*");
                    doclnk.Filter = string.Format(" ([REC_ID] = {0}) AND ([REC_TAB]='{1}') AND ([PATH]='{2}') ", Table_Id, Table_Name, PATH_OLD);
                    doclnk.OpenRs();
                    if (!doclnk.IsEOF())
                    {
                        if (docf_create.Fetch(doclnk.m_doc_id))
                        {
                            docf_create.Delete();
                        }
                        doclnk.Delete();
                    }
                }
                doclnk.Close(); doclnk.Dispose();
                docf_create.Close(); docf_create.Dispose();
            }
            catch (Exception)
            { }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Table_Name"></param>
        /// <param name="Table_Id"></param>
        public void CreateDocLink(string Table_Name, int Table_Id)
        {
            try {
                YDoclink doclnk_create = new YDoclink(); doclnk_create.Format("*"); doclnk_create.New();
                YDocfiles docf_create = new YDocfiles(); docf_create.Format("*"); docf_create.New(); docf_create.AllocID();
                YDoclink doclnk = new YDoclink();
                string PATH_OLD = FileLink;
                if (PATH_OLD != "") {
                    doclnk.Format("*");
                    doclnk.Filter = string.Format(" ([REC_ID] = {0}) AND ([REC_TAB]='{1}') AND ([PATH]='{2}') ", Table_Id, Table_Name, PATH_OLD);
                    doclnk.OpenRs();
                    if (doclnk.IsEOF()) {
                        if (System.IO.File.Exists(PATH_OLD)) {
                            docf_create.m_path = PATH_OLD;
                            docf_create.m_doc_date = DateTime.Now;
                            docf_create.m_doc_ref = System.IO.Path.GetFileNameWithoutExtension(PATH_OLD);
                            docf_create.m_created_by = IM.ConnectedUser();
                            docf_create.m_date_created = DateTime.Now;
                            docf_create.Save();

                            doclnk_create.m_name = System.IO.Path.GetFileName(PATH_OLD);
                            doclnk_create.m_path = PATH_OLD;
                            doclnk_create.m_rec_id = Table_Id;
                            doclnk_create.m_rec_tab = Table_Name;
                            doclnk_create.m_doc_id = docf_create.m_id;
                            doclnk_create.Save();
                        }

                    }
                }
                doclnk.Close(); doclnk.Dispose();
                doclnk_create.Close(); doclnk_create.Dispose();
                docf_create.Close(); docf_create.Dispose();
            }
            catch (Exception)
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Save(string Table_Name, int Table_Id)
        {
            try {
                IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadWrite);
                rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, text_type_street.Text);
                rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, text_name_street.Text);
                rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Eq, text_Code_KOATU.Text);
                rsStreet.Open();
                if (rsStreet.IsEOF()) {
                    rsStreet.AddNew();
                    rsStreet.Put("ID", IM.AllocID("XNRFA_STREETS", 1, -1));
                    rsStreet.Put("STREET_TYPE", text_type_street.Text);
                    rsStreet.Put("STREET_NAME", text_name_street.Text);
                    rsStreet.Put("STREET_NAME_NEW", textBox_new_name_street.Text);
                    rsStreet.Put("TERRAIN_CODE", text_Code_KOATU.Text);
                    rsStreet.Put("MODIFIED_BY", IM.ConnectedUser());
                    rsStreet.Put("DATE_MODIFIED", DateTime.Now);
                    rsStreet.Put("DOC_REF", FileLink);
                    rsStreet.Update();
                    CreateDocLink(Table_Name, Table_Id);
                }
                else {
                    rsStreet.Edit();
                    rsStreet.Put("STREET_TYPE", text_type_street.Text);
                    rsStreet.Put("STREET_NAME", text_name_street.Text);
                    rsStreet.Put("STREET_NAME_NEW", textBox_new_name_street.Text);
                    rsStreet.Put("TERRAIN_CODE", text_Code_KOATU.Text);
                    rsStreet.Put("MODIFIED_BY", IM.ConnectedUser());
                    rsStreet.Put("DATE_MODIFIED", DateTime.Now);
                    rsStreet.Put("DOC_REF", FileLink);
                    rsStreet.Update();
                    CreateDocLink(Table_Name, Table_Id);
                }
                rsStreet.Close();
                rsStreet.Destroy();
            }
            catch (Exception)
            { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult DA = MessageBox.Show(CLocaliz.TxT("Save new street name?"), CLocaliz.TxT("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DA == DialogResult.Yes) {
                Save(TableName, ID);
                MessageBox.Show(CLocaliz.TxT("Record saved!"));
            }
            Close();
        }

        private void AdditionalDataForm_Shown(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            try
            {
                DialogResult DA = MessageBox.Show(CLocaliz.TxT("Delete entry?"), CLocaliz.TxT("Warning"), MessageBoxButtons.YesNo);
                if (DA == System.Windows.Forms.DialogResult.Yes)
                {
                    if (System.IO.File.Exists(FileLink))
                    {
                        System.IO.File.Delete(FileLink);
                        FileLink = "";
                        linkLabel_refDoc.Text = "";
                    }

                    IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadWrite);
                    rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                    rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, text_type_street.Text);
                    rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, text_name_street.Text);
                    rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Eq, text_Code_KOATU.Text);
                    rsStreet.Open();
                    if (!rsStreet.IsEOF())
                    {
                        rsStreet.Delete();
                    }
                    rsStreet.Close();
                    rsStreet.Destroy();
                    Close();
                }
            }
            catch (Exception)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                DepartmentType departmentType = CUsers.GetUserDepartmentType();
                string NameItem = string.Format("XRFA_{0}_ADOC", departmentType.ToString());
                openFileDialog1.Filter = "Attach Files(*.DOC;*.DOCX;*.RTF)|*.DOC;*.DOCX;*.RTF";
                if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                    string fileName = openFileDialog1.FileName;
                    if (fileName.IndexOf("\\\\") != 0)
                    {
                        string networkLocation = "";
                        if (string.IsNullOrEmpty(networkLocation)) {
                            IMRecordset rsNetPath = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
                            rsNetPath.Select("ITEM,WHAT");
                            rsNetPath.SetWhere("ITEM", IMRecordset.Operation.Eq, NameItem!="XRFA_Unknown_ADOC" ? NameItem :  "SHDIR-DOC");
                            rsNetPath.Open();
                            if (!rsNetPath.IsEOF())
                                networkLocation = rsNetPath.GetS("WHAT");
                            rsNetPath.Destroy();

                            if (string.IsNullOrEmpty(networkLocation))  {
                                throw new Exception("Файл потрібно записати в мережеве сховище, але папку для збереження вкладень на сервері не задано.\nЗверніться до адміністратора.");
                            }
                            else {
                                if (!System.IO.File.Exists(networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName)))  {
                                    System.IO.File.Copy(openFileDialog1.FileName, networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    linkLabel_refDoc.Text = (networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    FileLink = (networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    if (string.IsNullOrEmpty(FileLink)) { RemLink.Enabled = false; } else { RemLink.Enabled = true; }
                                }
                                else {
                                    System.IO.File.Delete(networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    System.IO.File.Copy(openFileDialog1.FileName, networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    linkLabel_refDoc.Text = (networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    FileLink = (networkLocation + "\\" + System.IO.Path.GetFileName(openFileDialog1.FileName));
                                    if (string.IsNullOrEmpty(FileLink)) { RemLink.Enabled = false; } else { RemLink.Enabled = true; }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void linkLabel_refDoc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            System.Diagnostics.Process.Start(FileLink);
        }

        private void RemLink_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult DA = MessageBox.Show(CLocaliz.TxT("Remove this link?"), CLocaliz.TxT("Warning"), MessageBoxButtons.YesNo);
                if (DA == System.Windows.Forms.DialogResult.Yes)
                {
                    if (System.IO.File.Exists(FileLink))
                    {
                        IMRecordset rsStreet = new IMRecordset("XNRFA_STREETS", IMRecordset.Mode.ReadWrite);
                        rsStreet.Select("ID,STREET_TYPE,STREET_NAME,STREET_NAME_NEW,TERRAIN_CODE,MODIFIED_BY,DATE_MODIFIED,DOC_REF");
                        rsStreet.SetWhere("STREET_TYPE", IMRecordset.Operation.Eq, text_type_street.Text);
                        rsStreet.SetWhere("STREET_NAME", IMRecordset.Operation.Eq, text_name_street.Text);
                        rsStreet.SetWhere("TERRAIN_CODE", IMRecordset.Operation.Eq, text_Code_KOATU.Text);
                        rsStreet.Open();
                        if (!rsStreet.IsEOF())
                        {
                            rsStreet.Edit();
                            rsStreet.Put("DOC_REF", "");
                            rsStreet.Update();
                        }
                        rsStreet.Close();
                        rsStreet.Destroy();

                        System.IO.File.Delete(FileLink);
                        DeleteDocLink(TableName, ID);
                        FileLink = "";
                        linkLabel_refDoc.Text = "";
                        if (string.IsNullOrEmpty(FileLink)) { RemLink.Enabled = false; } else { RemLink.Enabled = true; }
                        
                    }
                    else {
                        DeleteDocLink(TableName, ID);
                        FileLink = "";
                        linkLabel_refDoc.Text = "";
                        if (string.IsNullOrEmpty(FileLink)) { RemLink.Enabled = false; } else { RemLink.Enabled = true; }
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }

}
