﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using ICSM;
using System.IO;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class AdditionalDataForm : Form
    {
        private List<ItemFldAdditionalData> LstDataArray = null;
        private int REF_ID { get; set; }
        private int REC_ID { get; set; }
        private bool Status_ { get; set; }

       // private string[] LIST_FLD_ = { "ID", "REF_ID", "ARMY_UCRF_NUMBER_IN", "ARMY_UCRF_NUMBER_IN_VALUE", "ARMY_UCRF_DATE_IN", "ARMY_UCRF_DATE_IN_VALUE", "ARMY_REZULT", "ARMY_REZULT_VALUE", "UCRF_ARMY_NUMBER_OUT", "UCRF_ARMY_NUMBER_OUT_VALUE" };
       // private string[] LIST_FLD_CAPTION_ = { "Унікальный номер", "Ссылка на ID в табл. DOCFILES", "Ответ из ГШ (Вх.УДЦР)", "Значение поля «Ответ из ГШ (Вх.УДЦР)»", "Дата ответа из ГШ (Вх.УДЦР)", "Значение поля 2 «Дата ответа из ГШ (Вх.УДЦР)»", "Результат согласования", "Значение поля «Результат согласования»", "Номер письма-запроса", "Значение поля «Номер письма-запроса»" };


        public AdditionalDataForm(int REF_ID, int REC_ID, bool Status_)
        {
            InitializeComponent();
            dataGridViewAdditionalData.Columns["ColumnName"].HeaderText = CLocaliz.TxT("Name field");
            dataGridViewAdditionalData.Columns["ColumnValue"].HeaderText= CLocaliz.TxT("Value field");
            btnCancel.Text = CLocaliz.TxT("Cancel");
            this.Text = CLocaliz.TxT("Additional Data");
            this.REF_ID = REF_ID;
            this.REC_ID = REC_ID;
            this.Status_ = Status_;
        }

        public List<PropertiesField> ParseXML(string XML)
        {
            List<PropertiesField> Props = new List<PropertiesField>();
            try
            {
                XDocument xdoc = new XDocument();
                XML = AppDomain.CurrentDomain.BaseDirectory + "\\" + XML;
                using (StreamReader reader = new StreamReader(XML, Encoding.UTF8))
                {
                    xdoc = XDocument.Parse(reader.ReadToEnd());
                    IEnumerable<XElement> WKFSettingGroup = xdoc.Element("SettingGroupObject").Element("Params").Element("ObjectDataGroup").Element("Groups").Elements("Category");

                    foreach (XElement xel in WKFSettingGroup.Elements())
                    {
                        //MessageBox.Show(xel.Name.ToString());
                        if ((xel.Name.ToString() == "Id") || (xel.Name.LocalName == "Id"))
                        {
                            int ID = int.Parse(xel.Value.ToString());
                        }

                        if ((xel.Name.ToString() == "Group") || (xel.Name.LocalName == "Group"))
                        {
                            IEnumerable<XElement> ellis_items_menu = xel.Elements("ItemMenu").Elements();

                            PropertiesField tempAdd = new PropertiesField();
                            foreach (XElement xel_item in ellis_items_menu)
                            {
                                if ((xel_item.Name.ToString() == "Id") || (xel_item.Name.LocalName == "Id"))
                                {
                                    tempAdd.ID = int.Parse(xel_item.Value.ToString());
                                }
                                if ((xel_item.Name.ToString() == "TypeField") || (xel_item.Name.LocalName == "TypeField"))
                                {
                                    tempAdd.TypeField = xel_item.Value.ToString();
                                }
                                if ((xel_item.Name.ToString() == "Caption") || (xel_item.Name.LocalName == "Caption"))
                                {
                                    tempAdd.Caption = xel_item.Value.ToString();
                                }
                                if ((xel_item.Name.ToString() == "NameField") || (xel_item.Name.LocalName == "NameField"))
                                {
                                    tempAdd.Name  = xel_item.Value.ToString();
                                    if (!Props.Contains(tempAdd))
                                    {
                                        Props.Add(tempAdd);
                                        tempAdd = new PropertiesField();
                                    }
                                }
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return Props;
        
        }

        public static int GetIDDocFilies(int REF_ID)
        {
            int ID = -1;
            IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadOnly);
            rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
            rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, REF_ID);
            rsDocLink.SetAdditional("[REC_TAB]='XNRFA_PACKET'");


            rsDocLink.Open();
            for (; !rsDocLink.IsEOF(); rsDocLink.MoveNext())
            {

                int DOC_ID = rsDocLink.GetI("DOC_ID");


                ////////////////
                IMRecordset rsAps = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadOnly);
                rsAps.Select("ID,MEMO,PATH,DOC_TYPE,CREATED_BY, DATE_CREATED");
                rsAps.SetWhere("ID", IMRecordset.Operation.Eq, DOC_ID);






                Documents.Documents Doc = new Documents.Documents();
                rsAps.Open();
                if (!rsAps.IsEOF())
                {

                    ID = rsAps.GetI("ID");

                }
            }
            return ID;
        }


        public void InitComponent(int REF_ID, int REC_ID, bool Status_)
        {
            try
            {
               
                List<PropertiesField> Props = ParseXML("SettingGSFields.xml");

                string[] ListFields = Props.Select(r =>r.Name).ToArray();
                string[] ListCaption = Props.Select(r => r.Caption).ToArray();
                string[] ListTypes = Props.Select(r => r.TypeField).ToArray();



                LstDataArray = new List<ItemFldAdditionalData>();
                using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.xnrfa_docfiles, IMRecordset.Mode.ReadOnly))
                {
                    string AllFields = "";
                    for (int i=0; i<ListFields.Count();i++)
                    {
                        string str = ListFields[i];
                        if (str.IndexOf("VALUE") != -1)
                        {
                           // string temp_Type = "STRING";
                           /// if (str.IndexOf("DATE") != -1)
                           // {
                           //     temp_Type = "DATE";
                           // }
                            string temp_OUT = str;
                            string temp_IN = str.Substring(0, str.Length - 6);
                            ItemFldAdditionalData tmpAddData = new ItemFldAdditionalData();

                            tmpAddData.IN_NAME.Name = temp_IN;
                            tmpAddData.OUT_NAME.Name = temp_OUT;

                            int kkk_IN = -1; int kkk_OUT = -1;
                            for (int k = 0; k < ListFields.Count(); k++)
                            {
                                if (ListFields[k] == temp_IN)
                                {
                                    kkk_IN = k;
                                    break;
                                }
                            }

                            for (int k = 0; k < ListFields.Count(); k++)
                            {
                                if (ListFields[k] == temp_OUT)
                                {
                                    kkk_OUT = k;
                                    break;
                                }
                            }

                            tmpAddData.IN_NAME.Caption = ListCaption[kkk_IN];
                            tmpAddData.OUT_NAME.Caption = ListCaption[kkk_OUT];
                            tmpAddData.Type = ListTypes[i];//temp_Type;
                            LstDataArray.Add(tmpAddData);

                        }
                        AllFields += str + ",";

                    }
                    AllFields = AllFields.Remove(AllFields.Length - 1);
                    r.Select(AllFields); 
                    //r.Select("ID,REF_ID,ARMY_UCRF_NUMBER_IN,ARMY_UCRF_NUMBER_IN_VALUE,ARMY_UCRF_DATE_IN,ARMY_UCRF_DATE_IN_VALUE,ARMY_REZULT,ARMY_REZULT_VALUE,UCRF_ARMY_NUMBER_OUT,UCRF_ARMY_NUMBER_OUT_VALUE");
                //    r.SetWhere("REF_ID", IMRecordset.Operation.Eq, GetIDDocFilies(REF_ID));
                    r.SetWhere("REF_ID", IMRecordset.Operation.Eq, REC_ID);

                    r.Open();
                    if (!r.IsEOF())
                    {
                        dataGridViewAdditionalData.Rows.Clear();
                        for (int i = 0; i < LstDataArray.Count(); i++)
                        {

                            if (LstDataArray[i].Type == "STRING")
                            {
                                dataGridViewAdditionalData.Rows.Add(new object[] { r.GetS(LstDataArray[i].IN_NAME.Name), r.GetS(LstDataArray[i].OUT_NAME.Name) });
                            }
                            if (LstDataArray[i].Type == "DATE")
                            {
                                if (r.Get(LstDataArray[i].OUT_NAME.Name) != null)
                                {
                                    dataGridViewAdditionalData.Rows.Add(new object[] { r.GetS(LstDataArray[i].IN_NAME.Name), r.Get(LstDataArray[i].OUT_NAME.Name) });
                                }
                                else
                                {
                                    dataGridViewAdditionalData.Rows.Add(new object[] { r.GetS(LstDataArray[i].IN_NAME.Name), "" });
                                }
                            }
                        }


                    }
                    else
                    {
                        dataGridViewAdditionalData.Rows.Clear();
                        for (int i = 0; i < LstDataArray.Count(); i++)
                        {
                            dataGridViewAdditionalData.Rows.Add(new object[] { LstDataArray[i].IN_NAME.Caption, "" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in InitComponent", ex.Message);
            }
            
        }

        private void SaveToDB(string[] ListFields)
        {
            try
            {
                GatherToArray();
                using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.xnrfa_docfiles, IMRecordset.Mode.ReadWrite))
                {

                    string AllFields = "";
                    foreach (string str in ListFields)
                    {
                        AllFields += str + ",";
                    }
                    AllFields = AllFields.Remove(AllFields.Length - 1);
                    r.Select(AllFields);
                    //r.Select("ID, REF_ID, ARMY_UCRF_NUMBER_IN, ARMY_UCRF_NUMBER_IN_VALUE, ARMY_UCRF_DATE_IN, ARMY_UCRF_DATE_IN_VALUE, ARMY_REZULT, ARMY_REZULT_VALUE, UCRF_ARMY_NUMBER_OUT, UCRF_ARMY_NUMBER_OUT_VALUE");
                    //r.SetWhere("REF_ID", IMRecordset.Operation.Eq, GetIDDocFilies(REF_ID));
                    r.SetWhere("REF_ID", IMRecordset.Operation.Eq, REC_ID);
                    r.Open();
                    if (!r.IsEOF())
                    {
                        r.Edit();
                        foreach (ItemFldAdditionalData item in LstDataArray)
                        {
                            if (item.Type == "STRING")
                            {
                                r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                r.Put(item.OUT_NAME.Name, item.OUT_VALUE);
                            }
                            if (item.Type == "DATE")
                            {
                                if (item.OUT_VALUE.ToString() != "")
                                {
                                    r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                    r.Put(item.OUT_NAME.Name, DateTime.Parse(item.OUT_VALUE.ToString()));
                                }
                                else
                                {
                                    r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                    r.Put(item.OUT_NAME.Name, IM.NullD);
                                }
                            }

                        }
                        r.Update();

                    }
                    else
                    {
                        r.AddNew();
                        int id = IM.AllocID(PlugTbl.xnrfa_docfiles, 1, -1);
                        r.Put("ID", id);
                        //r.Put("REF_ID", GetIDDocFilies(REF_ID));
                        r.Put("REF_ID", REC_ID);
                        foreach (ItemFldAdditionalData item in LstDataArray)
                        {
                            if (item.Type == "STRING")
                            {
                                r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                r.Put(item.OUT_NAME.Name, item.OUT_VALUE);
                            }
                            if (item.Type == "DATE")
                            {
                                if (item.OUT_VALUE.ToString() != "")
                                {
                                    r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                    r.Put(item.OUT_NAME.Name, DateTime.Parse(item.OUT_VALUE.ToString()));
                                }
                                else
                                {
                                    r.Put(item.IN_NAME.Name, item.IN_VALUE);
                                    r.Put(item.OUT_NAME.Name, IM.NullD);
                                }
                            }
                        }
                        r.Update();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in SaveToDB: "+ ex.Message);
            }
        }

        /// <summary>
        /// Вызов механизма сохранения в массив значений
        /// </summary>
        private void GatherToArray()
        {
            try
            {
              
                for (int f = 0; f < dataGridViewAdditionalData.Rows.Count; f++)
                {
                    if (dataGridViewAdditionalData.Rows[f].Cells[0].Value != null)
                    {
                        if (f<=LstDataArray.Count()-1)
                            LstDataArray[f].IN_VALUE = dataGridViewAdditionalData.Rows[f].Cells[0].Value.ToString();
                    }
                    else
                    {
                        if (f <= LstDataArray.Count() - 1)
                            LstDataArray[f].IN_VALUE = "";
                    }
                    if (dataGridViewAdditionalData.Rows[f].Cells[1].Value != null)
                    {
                        if (f <= LstDataArray.Count() - 1)
                            LstDataArray[f].OUT_VALUE = dataGridViewAdditionalData.Rows[f].Cells[1].Value.ToString();
                    }
                    else
                    {
                        if (f <= LstDataArray.Count() - 1)
                            LstDataArray[f].OUT_VALUE = "";
                    }

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error in GatherToArray: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult DA = MessageBox.Show(CLocaliz.TxT("Save additional data"), CLocaliz.TxT("Message"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DA == DialogResult.Yes)
            {
                List<PropertiesField> Props = ParseXML("SettingGSFields.xml");
                string[] ListFields = Props.Select(r => r.Name).ToArray();
                SaveToDB(ListFields);
                MessageBox.Show(CLocaliz.TxT("Additional data saved!"));
            }
        }

        private void AdditionalDataForm_Shown(object sender, EventArgs e)
        {
            //InitComponent(REF_ID, LIST_FLD_, LIST_FLD_CAPTION_);
            InitComponent(REF_ID,REC_ID,Status_);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }

    public sealed class ItemFldAdditionalData
    {
        public PropertiesField IN_NAME { get; set; }
        public PropertiesField OUT_NAME { get; set; }

        public object IN_VALUE { get; set; }
        public object OUT_VALUE { get; set; }

        public string Type { get; set; }

        public ItemFldAdditionalData()
        {
            IN_NAME = new PropertiesField();
            OUT_NAME = new PropertiesField();
        }
    }

    public sealed class PropertiesField
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public string TypeField { get; set; }

    }
}
