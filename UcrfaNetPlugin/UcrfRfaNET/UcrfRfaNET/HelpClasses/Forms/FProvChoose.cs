﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET.HelpClasses
{
	public partial class FProvChoose : Form
	{
        // Для Binding ComboBox
        private ComboBoxDictionaryList<string, string> _listProvince;
        /// <summary>
        /// Список провинцей
        /// </summary>
        public List<string> ProvinceList { get; set; }
        /// <summary>
        /// Быбранная область
        /// </summary>
        public string SelectedProvince { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
		public FProvChoose()
		{
            ProvinceList = new List<string>();
            _listProvince = new ComboBoxDictionaryList<string, string>();
            //----
			InitializeComponent();
            //----
            _listProvince.InitComboBox(cbProvince);
            cbProvince.DataBindings.Add("SelectedValue", this, "SelectedProvince", true, DataSourceUpdateMode.OnPropertyChanged);
		}
        /// <summary>
        /// Загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FProvChoose_Load(object sender, EventArgs e)
        {
            foreach (string province in ProvinceList)
                _listProvince.Add(new ComboBoxDictionary<string, string>(province, province));
            if (_listProvince.Count > 0)
            {
                cbProvince.SelectedIndex = 0;
                SelectedProvince = _listProvince[0].Key;
            }
        }
	}
}
