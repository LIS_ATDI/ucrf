﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
	partial class FChannels
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FChannels));
            this.CLBAllChannel = new System.Windows.Forms.CheckedListBox();
            this.CLBSelectChannel = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSelectAll = new System.Windows.Forms.Button();
            this.BtnDeleteAll = new System.Windows.Forms.Button();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CLBAllChannel
            // 
            this.CLBAllChannel.FormattingEnabled = true;
            this.CLBAllChannel.HorizontalScrollbar = true;
            this.CLBAllChannel.Location = new System.Drawing.Point(12, 32);
            this.CLBAllChannel.Name = "CLBAllChannel";
            this.CLBAllChannel.Size = new System.Drawing.Size(240, 274);
            this.CLBAllChannel.TabIndex = 0;
            // 
            // CLBSelectChannel
            // 
            this.CLBSelectChannel.FormattingEnabled = true;
            this.CLBSelectChannel.HorizontalScrollbar = true;
            this.CLBSelectChannel.Location = new System.Drawing.Point(406, 32);
            this.CLBSelectChannel.Name = "CLBSelectChannel";
            this.CLBSelectChannel.Size = new System.Drawing.Size(240, 274);
            this.CLBSelectChannel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(406, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Вибрано";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Канали ліцензій";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(374, 317);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(100, 25);
            this.BtnCancel.TabIndex = 4;
            this.BtnCancel.Text = "Вихід";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // BtnSelectAll
            // 
            this.BtnSelectAll.Location = new System.Drawing.Point(269, 118);
            this.BtnSelectAll.Name = "BtnSelectAll";
            this.BtnSelectAll.Size = new System.Drawing.Size(118, 25);
            this.BtnSelectAll.TabIndex = 5;
            this.BtnSelectAll.Text = "Вибрати всі >>";
            this.BtnSelectAll.UseVisualStyleBackColor = true;
            this.BtnSelectAll.Click += new System.EventHandler(this.BtnSelectAll_Click);
            // 
            // BtnDeleteAll
            // 
            this.BtnDeleteAll.Location = new System.Drawing.Point(269, 174);
            this.BtnDeleteAll.Name = "BtnDeleteAll";
            this.BtnDeleteAll.Size = new System.Drawing.Size(118, 25);
            this.BtnDeleteAll.TabIndex = 6;
            this.BtnDeleteAll.Text = "<< Видалити всі";
            this.BtnDeleteAll.UseVisualStyleBackColor = true;
            this.BtnDeleteAll.Click += new System.EventHandler(this.BtnDeleteAll_Click);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(269, 65);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(118, 25);
            this.BtnAdd.TabIndex = 7;
            this.BtnAdd.Text = "Вибрати >";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(269, 227);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(118, 25);
            this.BtnDelete.TabIndex = 8;
            this.BtnDelete.Text = "< Видалити";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.BtnOk.Location = new System.Drawing.Point(189, 317);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(100, 25);
            this.BtnOk.TabIndex = 9;
            this.BtnOk.Text = "Ок";
            this.BtnOk.UseVisualStyleBackColor = true;
            // 
            // FChannels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 359);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.BtnDeleteAll);
            this.Controls.Add(this.BtnSelectAll);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CLBSelectChannel);
            this.Controls.Add(this.CLBAllChannel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FChannels";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FChannels";
            this.Load += new System.EventHandler(this.FChannels_Load);
            this.Shown += new System.EventHandler(this.FChannels_Shown);
            this.ResumeLayout(false);

		}

		#endregion

		public System.Windows.Forms.CheckedListBox CLBAllChannel;
		public System.Windows.Forms.CheckedListBox CLBSelectChannel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.Button BtnSelectAll;
		private System.Windows.Forms.Button BtnDeleteAll;
		private System.Windows.Forms.Button BtnAdd;
		private System.Windows.Forms.Button BtnDelete;
		private System.Windows.Forms.Button BtnOk;
	}
}