﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FAskApplicationCancel
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.label1 = new System.Windows.Forms.Label();
          this.button1 = new System.Windows.Forms.Button();
          this.button2 = new System.Windows.Forms.Button();
          this.SuspendLayout();
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.label1.Location = new System.Drawing.Point(36, 9);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(408, 19);
          this.label1.TabIndex = 0;
          this.label1.Text = "Бажаєте анулювати електронну службову записку до ДРВ?";
          // 
          // button1
          // 
          this.button1.DialogResult = System.Windows.Forms.DialogResult.Yes;
          this.button1.Location = new System.Drawing.Point(85, 37);
          this.button1.Name = "button1";
          this.button1.Size = new System.Drawing.Size(85, 24);
          this.button1.TabIndex = 1;
          this.button1.Text = "Так";
          this.button1.UseVisualStyleBackColor = true;
          // 
          // button2
          // 
          this.button2.DialogResult = System.Windows.Forms.DialogResult.No;
          this.button2.Location = new System.Drawing.Point(302, 37);
          this.button2.Name = "button2";
          this.button2.Size = new System.Drawing.Size(85, 24);
          this.button2.TabIndex = 2;
          this.button2.Text = "Ні";
          this.button2.UseVisualStyleBackColor = true;
          // 
          // FAskApplicationCancel
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(465, 74);
          this.Controls.Add(this.button2);
          this.Controls.Add(this.button1);
          this.Controls.Add(this.label1);
          this.Name = "FAskApplicationCancel";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "Анулювання службової записки на виставлення рахунку";
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Button button2;
   }
}