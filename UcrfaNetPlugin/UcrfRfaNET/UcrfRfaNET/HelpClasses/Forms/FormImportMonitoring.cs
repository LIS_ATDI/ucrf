﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.OfficeBridge;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using OrmCs;
using XICSM.Lock;

namespace XICSM.UcrfRfaNET.HelpClasses.Forms
{
    public partial class FormImportMonitoring : Form
    {
        public bool StatusCheckPress { get; set; }
        string NameF { get; set; }
        IMDBList lstContext;
        string NameTable { get; set; }
        int Id_Table { get; set; }
        int OWNER_ID { get; set; }
        public static DialogResult DA { get; set; }
        public TypeFormatFile ChbType { get; set; }

        public FormImportMonitoring(IMQueryMenuNode.Context context)
        {
            InitializeComponent();
            lstContext = context.DataList;
            NameTable = context.TableName;
            IMRecordset rs_imp = new IMRecordset(NameTable, IMRecordset.Mode.ReadOnly);
            rs_imp.Select("ID");
            if (NameTable == "XV_MONITOR_USERS") rs_imp.Select("OWNER_ID");
            if (NameTable == "XNRFA_MON_CONTRACTS") rs_imp.Select("RN_AGENT");
            if (NameTable == "ALLSTATIONS") rs_imp.Select("OWNER_ID");
            if (NameTable == "ALLDECENTRALIZED") rs_imp.Select("OWNER_ID");
            rs_imp.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
            rs_imp.Open();
            if (!rs_imp.IsEOF())
            {
                Id_Table = rs_imp.GetI("ID");
                if (NameTable == "USERS")
                    OWNER_ID = rs_imp.GetI("ID");
                if (NameTable == "XV_MONITOR_USERS")
                    OWNER_ID = rs_imp.GetI("OWNER_ID");
                if (NameTable == "XNRFA_MON_CONTRACTS"){
                    double guid = IM.NullD;
                    if (double.TryParse(rs_imp.GetS("RN_AGENT"), out guid))
                    {
                        RecordPtr rec = XICSM.UcrfRfaNET.GlobalDB.CGlobalXML.GetRecordByGUID(guid, "USERS");
                        OWNER_ID = rec.Id;
                    }
                }
                if (NameTable == "ALLSTATIONS")
                        OWNER_ID = rs_imp.GetI("OWNER_ID");
                if (NameTable == "ALLDECENTRALIZED")
                    OWNER_ID = rs_imp.GetI("OWNER_ID");

            }
            if (rs_imp.IsOpen())
                rs_imp.Close();
            rs_imp.Destroy();
            openFileDialog1.Filter = "Attach Files(*.XLSX)|*.XLSX";
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Attach Files(*.XLSX)|*.XLSX";
                    openFileDialog1.FileName = "";
                    DialogResult DA = openFileDialog1.ShowDialog();
                    if (DA == System.Windows.Forms.DialogResult.OK)
                    {
                        NameF = openFileDialog1.FileName;
                        textBox1.Text = NameF;
                        string ext = System.IO.Path.GetExtension(NameF);
                        if (ext.ToLower().Contains("csv")) ChbType = TypeFormatFile.CSV;
                        if (ext.ToLower().Contains("xlsx")) ChbType = TypeFormatFile.XLSX;
                    }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            DA = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormLoadFile_Shown(object sender, EventArgs e)
        {
            CLocaliz.TxT(this);
        }


        /// <summary>
        ///  
        /// </summary>
        /// <param name="OWNER_ID"></param>
        /// <param name="PERM_NUM_ORDER"></param>
        private void GetParamsFromAppl(int OWNER_ID, List<KeyValuePair<string, int>> PERM_NUM_ORDER, List<string> AllPermNum, bool isApproveResultMonitoring)
        {
            try
            {
                List<string> SQL_PART = new List<string>();
                string SQL = ""; string SQL_INSERT = "";
                string SQL_ALL = " (([DOZV_NUM] IN ({0})) OR ([DOZV_NUM_OLD] IN ({1}))) ";
                string SQL_EVENT_LOG = " ([DOC_NUMBER] IN ({0})) "; string SQL_EVENT = "";
                int Count_POS = IM.NullI;
                IM.ExecuteScalar(ref Count_POS, string.Format("SELECT MAX(ID) FROM %XNRFA_IMPORT_ORDER"));
                IMProgress.Create("Please wait...");
                int Temp_Count = 0;
                foreach (KeyValuePair<string, int> item in PERM_NUM_ORDER)
                {
                    //SQL_INSERT += string.Format("INTO %XNRFA_IMPORT_ORDER(OWNER_ID,EMPLOYEE_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF) VALUES({0},{1},'{2}',{3},'{4}','{5}',{6},{7},{8},{9},{10})", OWNER_ID, CUsers.GetCurUserID(), item.Key, "NULL", "", "", "NULL", "NULL", "NULL", item.Value, "NULL") + Environment.NewLine;
                    SQL_INSERT += string.Format("INTO %XNRFA_IMPORT_ORDER(OWNER_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF,EMPLOYEE_ID) VALUES({0},'{1}',{2},'{3}','{4}',{5},{6},{7},{8},{9},{10})", OWNER_ID, item.Key, "NULL", "", "", "NULL", "NULL", "NULL", item.Value!=IM.NullI ? item.Value.ToString() : "NULL", "NULL", CUsers.GetCurUserID()) + Environment.NewLine;
                    if (!SQL.Contains(item.Key))
                        SQL += string.Format("'{0}',", item.Key);

                    if (Temp_Count == 150)
                    {
                        if (SQL.Length > 0)
                        {
                            if (SQL.Length > 0)
                            {
                                SQL = SQL.Remove(SQL.Length - 1, 1);
                            }
                            SQL_PART.Add(string.Format(SQL_ALL, SQL, SQL));
                            SQL = "";
                        }

                        if (SQL_INSERT.Length > 0)
                        {
                            SQL_INSERT = SQL_INSERT.Remove(SQL_INSERT.Length - 2, 2) + Environment.NewLine;
                            SQL_INSERT += "SELECT 1 FROM DUAL" + Environment.NewLine;
                            SQL_INSERT += "COMMIT";
                            SQL_INSERT = SQL_INSERT.Insert(0, "INSERT ALL" + Environment.NewLine);

                            IMTransaction.Begin();
                            if (IM.Execute(SQL_INSERT) != -1)
                                IMTransaction.Commit();
                            else IMTransaction.Rollback();
                            SQL_INSERT = "";
                            Temp_Count = 0;
                        }
                    }
                    Temp_Count++;
                    IMProgress.ShowProgress(Temp_Count, Count_POS);
                }

               if (SQL.Length > 0)
               {
                   if (SQL.Length > 0)
                   {
                        SQL = SQL.Remove(SQL.Length - 1, 1);
                   }
                   SQL_PART.Add(string.Format(SQL_ALL, SQL, SQL));
                   SQL = "";
                }
                if (SQL_INSERT.Length > 0)
                {
                    SQL_INSERT = SQL_INSERT.Remove(SQL_INSERT.Length - 2, 2) + Environment.NewLine;
                    SQL_INSERT += "SELECT 1 FROM DUAL" + Environment.NewLine;
                    SQL_INSERT += "COMMIT";
                    SQL_INSERT = SQL_INSERT.Insert(0, "INSERT ALL" + Environment.NewLine);

                    IMTransaction.Begin();
                    if (IM.Execute(SQL_INSERT) != -1)
                        IMTransaction.Commit();
                    else IMTransaction.Rollback();
                    SQL_INSERT = "";
                }
                /*
                if (SQL.Length > 0)
                {
                    SQL = SQL.Remove(SQL.Length - 1, 1);
                }
                SQL_ALL = string.Format(SQL_ALL, SQL, SQL);
                 */ 
                IMProgress.Destroy();

                List<int> L_S_APPL = new List<int>();
                List<ExtendClassImportOder> Ret = new List<ExtendClassImportOder>();
                foreach (string part_sql in SQL_PART)
                {
                    IMRecordset rs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                    rs.Select("ID,PAY_OWNER_ID,DOZV_NUM,DOZV_NUM_OLD,WAS_USED,IS_CONFIRMED,DOZV_DATE_FROM,DOZV_DATE_TO,DOZV_DATE_CANCEL,WORKS_COUNT");
                    rs.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, OWNER_ID);
                    rs.SetAdditional(part_sql);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        ExtendClassImportOder param = new ExtendClassImportOder();
                        param.WORK_COUNT_DIFF = rs.GetI("WORKS_COUNT");
                        param.APPL_ID = rs.GetI("ID");
                        param.DATE_FROM = rs.GetT("DOZV_DATE_FROM");
                        param.DATE_TO = rs.GetT("DOZV_DATE_TO");
                        param.DOZV_NUM = rs.GetS("DOZV_NUM");
                        param.DOZV_NUM_OLD = rs.GetS("DOZV_NUM_OLD");
                        param.DATE_CANCEL = rs.GetT("DOZV_DATE_CANCEL");
                        param.WAS_USED = rs.GetI("WAS_USED");
                        param.IS_CONFIRMED = rs.GetI("IS_CONFIRMED");
                        if (!L_S_APPL.Contains(param.APPL_ID))
                            L_S_APPL.Add(param.APPL_ID);
                        Ret.Add(param);
                    }
                    if (rs.IsOpen())
                        rs.Close();
                    rs.Destroy();
                }

                if (L_S_APPL.Count > 0) {
                    string SQ = "";
                    string SQL_ALL_IN = "[APPL_ID] in ({0})";
                    List<string> SQL_PART_IN = new List<string>();
                    int Temp_Count_in = 0;
                    foreach (int im in L_S_APPL) {
                        SQ += string.Format("{0},", im);
                        if (Temp_Count_in == 400)
                        {
                            if (SQ.Length > 0)
                            {
                                if (SQ.Length > 0)
                                {
                                    SQ = SQ.Remove(SQ.Length - 1, 1);
                                }
                                SQL_PART_IN.Add(string.Format(SQL_ALL_IN, SQ));
                                SQ = "";
                                Temp_Count_in = 0;
                            }
                        }

                        Temp_Count_in++;
                    }
                    if (SQ.Length > 0) {
                        SQ = SQ.Remove(SQ.Length - 1, 1);
                        SQ = string.Format(SQL_ALL_IN, SQ);
                        using (YAllstations allstat = new YAllstations())  {
                            allstat.Format("*");
                            allstat.Filter = string.Format("({0}) AND ([TABLE_NAME]='{1}') AND ([OWNER_ID]={2})", SQ,"MICROWA",OWNER_ID);
                            for (allstat.OpenRs(); !allstat.IsEOF(); allstat.MoveNext()) {
                                ExtendClassImportOder val = Ret.Find(r => r.APPL_ID == allstat.m_appl_id);
                                if (val != null) { val.ALL_STAT_ID.Add(allstat.m_id); }
                            }
                            allstat.Close();
                        }
                    }
                    foreach (string s in SQL_PART_IN) {
                        using (YAllstations allstat = new YAllstations()) {
                            allstat.Format("*");
                            allstat.Filter = string.Format("({0}) AND ([TABLE_NAME]='{1}') AND ([OWNER_ID]={2})", s, "MICROWA", OWNER_ID);
                            for (allstat.OpenRs(); !allstat.IsEOF(); allstat.MoveNext()) {
                                ExtendClassImportOder val = Ret.Find(r => r.APPL_ID == allstat.m_appl_id);
                                if (val != null) { val.ALL_STAT_ID.Add(allstat.m_id); }
                            }
                            allstat.Close();
                        }
                    }
                }
            
                

                SQL_EVENT = "";
                foreach (string item in AllPermNum)
                {
                    ExtendClassImportOder par = new ExtendClassImportOder();
                    int CntWrk = IM.NullI;
                    KeyValuePair<string, int> Val_Wrk_Cnt_L = PERM_NUM_ORDER.Find(r => r.Key== item);
                    if ((Val_Wrk_Cnt_L.Value!=IM.NullI)) { CntWrk = Val_Wrk_Cnt_L.Value; }
                    ExtendClassImportOder val = Ret.Find(r => r.DOZV_NUM == item || r.DOZV_NUM_OLD == item);
                    if (val != null) {
                        if (isApproveResultMonitoring) {
                            if ((CntWrk > 0) && (CntWrk!=IM.NullI))
                                val.WORK_COUNT_DIFF = CntWrk;
                            else {
                                val.WORK_COUNT_DIFF = 0;
                                if (val.ALL_STAT_ID.Count > 0)  {
                                    string SQ = "";
                                    string SQL_ALL_IN = "[ALLSTAT_ID] in ({0})";
                                    int Temp_Count_in = 0;
                                    List<string> SQL_PART_IN = new List<string>();
                                    foreach (int im in val.ALL_STAT_ID) {
                                        SQ += string.Format("{0},", im);
                                        if (Temp_Count_in == 400)
                                        {
                                            if (SQ.Length > 0)
                                            {
                                                if (SQ.Length > 0)
                                                {
                                                    SQ = SQ.Remove(SQ.Length - 1, 1);
                                                }
                                                SQL_PART_IN.Add(string.Format(SQL_ALL_IN, SQ));
                                                SQ = "";
                                                Temp_Count_in = 0;
                                            }
                                        }

                                        Temp_Count_in++;
                                    }
                                    if (SQ.Length > 0) {
                                        SQ = SQ.Remove(SQ.Length - 1, 1);
                                        using (YXnrfaMicrowaMon mon = new YXnrfaMicrowaMon()) {
                                            mon.Format("*");
                                            mon.Filter = string.Format(SQL_ALL_IN, SQ);
                                            for (mon.OpenRs(); !mon.IsEOF(); mon.MoveNext()) {
                                                if (mon.m_plan_pay != IM.NullT){
                                                    if (mon.m_plan_pay.Month != DateTime.Now.Month) {
                                                        //val.WORK_COUNT_DIFF = 0;
                                                        //break;
                                                    }
                                                    else val.WORK_COUNT_DIFF++;
                                                }
                                            }
                                            mon.Close();
                                        }
                                    }
                                    foreach (string s in SQL_PART_IN)
                                    {
                                        using (YXnrfaMicrowaMon mon = new YXnrfaMicrowaMon()) {
                                            mon.Format("*");
                                            mon.Filter =  s;
                                            for (mon.OpenRs(); !mon.IsEOF(); mon.MoveNext()){
                                                if (mon.m_plan_pay != IM.NullT){
                                                    if (mon.m_plan_pay.Month != DateTime.Now.Month)
                                                    {
                                                        //val.WORK_COUNT_DIFF = 0;
                                                        //break;
                                                    }
                                                    else val.WORK_COUNT_DIFF++;
                                                }
                                            }
                                            mon.Close();
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if ((CntWrk > 0) && (CntWrk != IM.NullI)) val.WORK_COUNT_DIFF = CntWrk;
                        }
                        par.APPL_ID = IM.NullI;
                        par.WORK_COUNT_DIFF = IM.NullI;
                        par.DATE_CANCEL = val.DATE_CANCEL;
                        par.isContainOwner = true;
                        if (val.DOZV_NUM == item)
                        {
                            par.WORK_COUNT_DIFF = val.WORK_COUNT_DIFF;
                            par.isContainDozv = true;
                            par.APPL_ID = val.APPL_ID;
                            if (val.WAS_USED == 0)
                            {
                                par.isWasUsedDozv = true;
                                par.APPL_ID = val.APPL_ID;
                            }
                            else
                            {
                                if (val.IS_CONFIRMED == 0)
                                {
                                    par.isConfirmedDozv = true;
                                    par.APPL_ID = val.APPL_ID;
                                }
                            }
                        }
                        else
                        {
                            if (val.DOZV_NUM_OLD == item)
                            {
                                par.WORK_COUNT_DIFF = val.WORK_COUNT_DIFF;
                                par.isContainDozvOld = true;
                                par.DOZV_NUM = val.DOZV_NUM;
                                par.DATE_FROM = val.DATE_FROM;
                                par.DATE_TO = val.DATE_TO;
                                par.APPL_ID = val.APPL_ID;
                            }
                        }
                        if ((!(((!par.isContainOwner) && (!par.isContainDozv) && (!par.isContainDozvOld)) || ((par.isContainOwner) && (!par.isContainDozv) && (!par.isContainDozvOld)) || ((!par.isContainOwner) && (par.isContainDozv)))) &&
                        (!((par.isContainOwner) && (par.isContainDozv) && (par.isWasUsedDozv))) &&
                        (!((par.isContainOwner) && (par.isContainDozv) && (!par.isWasUsedDozv) && (par.isConfirmedDozv))) &&
                        (!((par.isContainOwner) && (!par.isContainDozv) && (par.isContainDozvOld))))
                        {
                            par.APPL_ID = val.APPL_ID;
                            par.WORK_COUNT_DIFF = val.WORK_COUNT_DIFF;
                        }
                    }

                    if (((!par.isContainOwner) && (!par.isContainDozv) && (!par.isContainDozvOld)) || ((par.isContainOwner) && (!par.isContainDozv) && (!par.isContainDozvOld)) || ((!par.isContainOwner) && (par.isContainDozv)))
                    {
                        if (!SQL_EVENT.Contains(item))
                            SQL_EVENT += string.Format("'{0}',", item);
                        IM.Execute(string.Format(" UPDATE %XNRFA_IMPORT_ORDER SET DESCRIPTION='{0}',WORKS_COUNT_DIFF={1} WHERE ((OWNER_ID={2}) AND (PERM_NUM_ORDER='{3}') AND (EMPLOYEE_ID={4})) ", "NO", par.WORK_COUNT_DIFF != IM.NullI ? par.WORK_COUNT_DIFF.ToString() : "NULL", OWNER_ID, item, CUsers.GetCurUserID()));
                    }
                    else if ((par.isContainOwner) && (par.isContainDozv) && (par.isWasUsedDozv))
                    {
                        //IM.Execute(string.Format(" UPDATE %XNRFA_IMPORT_ORDER SET DESCRIPTION='{0}',WORKS_COUNT_DIFF={1} WHERE ((OWNER_ID={2}) AND (PERM_NUM_ORDER='{3}') AND (EMPLOYEE_ID={4})) ", "ZIP", par.WORK_COUNT_DIFF != IM.NullI ? par.WORK_COUNT_DIFF.ToString() : "NULL", OWNER_ID, item, CUsers.GetCurUserID()));
                        using (YXnrfaImportOrder rs_imp = new YXnrfaImportOrder())
                        {
                            rs_imp.Table = "XNRFA_IMPORT_ORDER";
                            rs_imp.Format("*");
                            rs_imp.Filter = string.Format(" (([OWNER_ID]={0}) AND ([PERM_NUM_ORDER]='{1}') AND (EMPLOYEE_ID={2})) ", OWNER_ID, item, CUsers.GetCurUserID());
                            for (rs_imp.OpenRs(); !rs_imp.IsEOF(); rs_imp.MoveNext())
                            {
                                rs_imp.m_description = "ZIP";
                                rs_imp.m_date_cancel = par.DATE_CANCEL;
                                rs_imp.m_appl_id = par.APPL_ID;
                                rs_imp.Save();
                            }

                        }
                    }
                    else if ((par.isContainOwner) && (par.isContainDozv) && (!par.isWasUsedDozv) && (par.isConfirmedDozv))
                    {
                        IM.Execute(string.Format(" UPDATE %XNRFA_IMPORT_ORDER SET APPL_ID={0},DESCRIPTION='{1}',WORKS_COUNT_DIFF={2} WHERE ((OWNER_ID={3}) AND (PERM_NUM_ORDER='{4}') AND (EMPLOYEE_ID={5})) ", par.APPL_ID!= IM.NullI ?  par.APPL_ID.ToString() : "NULL" ,"CONF", par.WORK_COUNT_DIFF != IM.NullI ? par.WORK_COUNT_DIFF.ToString() : "NULL", OWNER_ID, item, CUsers.GetCurUserID()));
                    }
                    else if ((par.isContainOwner) && (!par.isContainDozv) && (par.isContainDozvOld))
                    {
                        using (YXnrfaImportOrder rs_imp = new YXnrfaImportOrder())
                        {
                            rs_imp.Table = "XNRFA_IMPORT_ORDER";
                            rs_imp.Format("*");
                            rs_imp.Filter = string.Format(" (([OWNER_ID]={0}) AND ([PERM_NUM_ORDER]='{1}') AND (EMPLOYEE_ID={2})) ", OWNER_ID, item, CUsers.GetCurUserID());
                            for (rs_imp.OpenRs(); !rs_imp.IsEOF(); rs_imp.MoveNext())
                            {
                                rs_imp.m_description = "NEW";
                                rs_imp.m_appl_id = par.APPL_ID;
                                rs_imp.m_perm_num = par.DOZV_NUM;
                                rs_imp.m_date_from = par.DATE_FROM;
                                rs_imp.m_date_to = par.DATE_TO;
                                rs_imp.m_works_count_diff = par.WORK_COUNT_DIFF;
                                rs_imp.Save();
                            }

                        }
                    }
                    else
                    {
                        IM.Execute(string.Format(" UPDATE %XNRFA_IMPORT_ORDER SET APPL_ID={0},WORKS_COUNT_DIFF={1} WHERE ((OWNER_ID={2}) AND (PERM_NUM_ORDER='{3}') AND (EMPLOYEE_ID={4})) ", par.APPL_ID != IM.NullI ? par.APPL_ID.ToString() : "NULL", par.WORK_COUNT_DIFF != IM.NullI ? par.WORK_COUNT_DIFF.ToString() : "NULL", OWNER_ID, item, CUsers.GetCurUserID()));
                    }
                }

                if (SQL_EVENT.Length > 0)
                {
                    SQL_EVENT = SQL_EVENT.Remove(SQL_EVENT.Length - 1, 1);
                    if (SQL_EVENT != "")
                        SQL_EVENT_LOG = string.Format(SQL_EVENT_LOG, SQL_EVENT);
                    else SQL_EVENT_LOG = "";
                }
                else SQL_EVENT_LOG = "";

                if (SQL_EVENT_LOG.Trim().Length > 0)
                {
                    List<KeyValuePair<int, string>> L_NO = new List<KeyValuePair<int, string>>();
                    using (IMRecordset rs_event = new IMRecordset("XNRFA_EVENT_LOG", IMRecordset.Mode.ReadOnly))
                    {
                        rs_event.Select("OBJ_ID,OBJ_TABLE,DOC_NUMBER");
                        rs_event.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "XNRFA_APPL");
                        rs_event.SetAdditional(SQL_EVENT_LOG);
                        for (rs_event.Open(); !rs_event.IsEOF(); rs_event.MoveNext())
                        {
                            if (!L_NO.Contains(new KeyValuePair<int, string>(rs_event.GetI("OBJ_ID"), rs_event.GetS("DOC_NUMBER"))))
                                L_NO.Add(new KeyValuePair<int, string>(rs_event.GetI("OBJ_ID"), rs_event.GetS("DOC_NUMBER")));
                        }
                    }

                    foreach (KeyValuePair<int, string> item in L_NO)
                    {
                        string SQL_NO = string.Format(" UPDATE %XNRFA_IMPORT_ORDER SET  APPL_ID={0} WHERE ((OWNER_ID={1}) AND (PERM_NUM_ORDER='{2}') AND (EMPLOYEE_ID={3})) ", item.Key!=IM.NullI ? item.Key.ToString() : "NULL" , OWNER_ID, item.Value, CUsers.GetCurUserID());
                        IM.Execute(SQL_NO);
                    }
                }
            }
            catch (Exception ex)
            {
                CLogs.WriteError(ELogsWhat.Monitoring, ex.Message);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (ChbType == TypeFormatFile.XLSX) {
                IM.Execute(string.Format(" DELETE %XNRFA_IMPORT_ORDER WHERE (ID > 0) AND (EMPLOYEE_ID={0}) ", CUsers.GetCurUserID()));
                List<KeyValuePair<string, int>> PermnumOrder = new  List<KeyValuePair<string, int>>();
                List<string> AllPermNum = new List<string>();
                IMProgress.Create("Please wait...");
                ExcelFastNPOI excel = new ExcelFastNPOI();
                excel.GetWorkBook(NameF);
                int row_val = 0; int MaxCountCh = 0;
                if (checkBox1.Checked) { row_val = 1; }
                for (; ; ) {
                    string m_ID = excel.GetCellValue(0, row_val).ToString();
                    if (m_ID.Trim().Length > 0) {
                        string decimal_sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                        string PERM_NUM_ORDER = excel.GetCellValue(0, row_val).ToString().Trim();
                        if (!string.IsNullOrEmpty(PERM_NUM_ORDER)) {
                            MaxCountCh = 0;
                            double CountWork = IM.NullD; int CntWrk = IM.NullI;
                            if (double.TryParse(excel.GetCellValue(1, row_val).ToString().Trim(), out CountWork))
                            {
                                CntWrk = (int)CountWork;
                            }
                            PermnumOrder.Add(new KeyValuePair<string, int>(PERM_NUM_ORDER, CntWrk));
                            if (!AllPermNum.Contains(PERM_NUM_ORDER))
                            {
                                AllPermNum.Add(PERM_NUM_ORDER);
                            }
                        }

                    }
                    else if (MaxCountCh <= 8) { MaxCountCh++; row_val++; continue; }
                    else if (MaxCountCh > 8) break;
                    row_val++;

                }
                excel.Close();
                excel.Dispose();
                if (PermnumOrder.Count > 0) GetParamsFromAppl(OWNER_ID, PermnumOrder, AllPermNum, checkBox_apply_result_mon.Checked);
                IMProgress.Destroy();
            }

            FormAnaliticsImport an = new FormAnaliticsImport(OWNER_ID, NameTable, Id_Table);
            an.ShowDialog();
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ExtendClassImportOder
    {
        public int APPL_ID { get; set; }
        public DateTime DATE_TO { get; set; }
        public DateTime DATE_FROM { get; set; }
        public DateTime DATE_CANCEL { get; set; }
        public List<int> ALL_STAT_ID = new List<int>();
        public int WORK_COUNT_DIFF { get; set; }
        public string DOZV_NUM { get; set; }
        public string DOZV_NUM_OLD { get; set; }
        public int WAS_USED { get; set; }
        public int IS_CONFIRMED { get; set; }
        public string PERM_NUM_ORDER { get; set; }
        public bool isContainOwner { get; set; }
        public bool isContainDozv { get; set; }
        public bool isContainDozvOld { get; set; }
        public bool isWasUsedDozv { get; set; }
        public bool isConfirmedDozv { get; set; }
        

        public ExtendClassImportOder()
        {
            APPL_ID = IM.NullI;
            DATE_TO = IM.NullT;
            DATE_FROM = IM.NullT;
            WORK_COUNT_DIFF = IM.NullI;
            DOZV_NUM = "";
            DOZV_NUM_OLD = "";
            WAS_USED = IM.NullI;
            IS_CONFIRMED = IM.NullI;
            PERM_NUM_ORDER = "";
            isContainOwner = false;
            isContainDozv = false;
            isContainDozvOld = false;
            isWasUsedDozv = false;
            isConfirmedDozv = false;
        }
        
    }


}
