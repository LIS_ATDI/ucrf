﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GridCtrl;
using XICSM.UcrfRfaNET.HelpClasses;
using XICSM.UcrfRfaNET.ApplSource.Classes;
using XICSM.UcrfRfaNET;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FBillMemo : FBaseForm
   {
       //Ключи полей
       private const string CnArticle = "stat";
       private const string CnCount = "count";
       private const string CnKoef = "koef";

       private Grid grdHeader = new GridCtrl.Grid();

      private Grid grid1 = new GridCtrl.Grid();
      private Grid grid2 = new GridCtrl.Grid();
      private Grid grid3 = new GridCtrl.Grid();
      private Grid grid4 = new GridCtrl.Grid();
      private Grid grid5 = new GridCtrl.Grid();
      private Grid grid6 = new GridCtrl.Grid();
      private Grid grid7 = new GridCtrl.Grid();
      private Grid grid8 = new GridCtrl.Grid();

      private Grid gridParamRez = new GridCtrl.Grid();
      private Grid gridComiss = new GridCtrl.Grid();
      private Grid gridOther = new GridCtrl.Grid();

      public struct articlesList
      {
         public WorkType type;
         public string defaultValue;
         public List<string> articles;
         public List<KeyValuePair<int, string>> List_default;
         public List<KeyValuePair<int, List<string>>> List_arts;

      }

      protected CDRVMemo drvMemo = null;   // Обьект, который управляет заявкой
      protected List<articlesList> articles = new List<articlesList>(); // списки статей по всем работам
      protected List<articlesList> articles_Only_Work3 = new List<articlesList>(); // списки статей по всем работам
      protected List<ExtArticles> arts_ext_rrz = new List<ExtArticles>();
      protected CPacket packet_ = null;
      public TypeDocs tpx_docs = TypeDocs.unknown;
      public List<TypeDocs> L_tpx_docs = new List<TypeDocs>();


      public FBillMemo(CDRVMemo _drvMemo, DRVStatus st, List<Work> lockedWorks, CPacket packet)
      {
         packet_ = packet;

         if (_drvMemo == null)
            throw new Exception("The base object can't be NULL.");
         drvMemo = _drvMemo;

         string standard = "";

         IMRecordset rsPacket = new IMRecordset(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadOnly);
         rsPacket.Select("ID,STANDARD");
         rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.packetId);
         try
         {
            rsPacket.Open();
            if (!rsPacket.IsEOF())
            {// Пакет существует                  
               standard = rsPacket.GetS("STANDARD");
            }
         }
         finally
         {
            rsPacket.Close();
            rsPacket.Destroy();
         }

         string[] types = Enum.GetNames(typeof(WorkType));
         foreach (string type in types)
         {
            List<string> arts = new List<string>();
            List<string> arts_only_works = new List<string>();
            List<ExtArticles> arts_ext = new List<ExtArticles>();

            IMRecordset article = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
            article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,STATUS,DATEOUTACTION,DATESTARTACTION,MIN,MAX,LIMITTYPE");
            article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, type);
            article.SetWhere("STATUS",IMRecordset.Operation.Eq,1);
            article.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
            article.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

 //              article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, "other");
        //    if(standard != "")
        //       article.SetWhere("STANDARD", IMRecordset.Operation.Like, standard); 

            string department = Enum.GetName(typeof(ManagemenUDCR), drvMemo.memoDepartment);
            article.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
            string defValue = "";
            arts_only_works = new List<string>();
            try
            {
               article.Open();
               for (; !article.IsEOF(); article.MoveNext())
               {
                  if (article.GetS("STANDARD") == standard) 
                  {
                     arts.Add(article.GetS("ARTICLE"));
                     if (article.GetS("LIMITTYPE") == "AglLimit")
                         arts_ext.Add(new ExtArticles(article.GetS("ARTICLE"), article.GetI("ID"), article.GetD("MIN"), article.GetD("MAX"), article.GetS("ARTICLE").EndsWith("а") ? true : false ));
                     if (article.GetI("DEFAULT") == 1)
                        defValue = article.GetS("ARTICLE");
                      
                      //только для РРЗ и статей 2.7.1 - 2.7.2
                      if (article.GetS("STANDARD")=="РРЗ") {
                          if (article.GetS("ARTICLE").StartsWith("2.7."))
                            arts_ext_rrz.Add(new ExtArticles(article.GetS("ARTICLE"), article.GetI("ID"), article.GetD("MIN"), article.GetD("MAX"), article.GetS("ARTICLE").StartsWith("2.7.") ? true : false));
                      }

                   
                  }
                  else if (article.GetS("STANDARD") == "")
                  {
                     arts.Add(article.GetS("ARTICLE"));
                     if (article.GetS("LIMITTYPE") == "AglLimit")
                         arts_ext.Add(new ExtArticles(article.GetS("ARTICLE"), article.GetI("ID"), article.GetD("MIN"), article.GetD("MAX"), article.GetS("ARTICLE").EndsWith("а") ? true : false));
                     if (article.GetI("DEFAULT") == 1 && defValue == "")
                        defValue = article.GetS("ARTICLE");
                  }
               }
               arts.Sort();
               arts_only_works.AddRange(arts);
            }
            finally
            {
               article.Close();
               article.Destroy();
            }
            articlesList newList = new articlesList();
            newList.type = (WorkType)Enum.Parse(typeof(WorkType), type);
            if (newList.type == WorkType.work3) {
                bool isFLStation = false;
                bool isModernStation = false;
                var NewArtcleList = new List<string>();
                NewArtcleList = arts;
                if (_drvMemo.appIds.Count > 0)
                {
                    isFLStation = IsBaseStation(_drvMemo.appIds[0], TypeConstraint.FL);
                    isModernStation = IsBaseStation(_drvMemo.appIds[0], TypeConstraint.MODERN);
                    List<int> DelIndx = new List<int>();
                    if (!isFLStation)  {
                        for (int it = 0; it < NewArtcleList.Count; it++) {
                            if (NewArtcleList[it].EndsWith("а")) {
                                DelIndx.Add(it);
                            }
                        }
                    }
                    if (!isModernStation) {
                        for (int it = 0; it < NewArtcleList.Count; it++) {
                            if (NewArtcleList[it].EndsWith("ам")) {
                                DelIndx.Add(it);
                            }
                        }
                    }
                    for (int it = 0; it < NewArtcleList.Count; it++) {
                        ExtArticles ext_art = arts_ext.Find(r => r.NameArt == NewArtcleList[it]);
                        if (ext_art != null) {
                            if (!IsStationEquationAgl(_drvMemo.appIds[0], ext_art.LimitMin, ext_art.LimitMax)) {
                                DelIndx.Add(it);
                            }
                        }
                    }

                    ///////

                    for (int it = 0; it < NewArtcleList.Count; it++) {
                        ExtArticles ext_art_find_rrz = arts_ext_rrz.Find(r => r.NameArt == NewArtcleList[it]);
                        if (ext_art_find_rrz != null) {
                            bool Stat = false;
                            foreach (int f_sta in _drvMemo.appIds) {
                                double freq_ = GetFreqStation(f_sta);
                                if (freq_ != IM.NullD) {
                                    if (((ext_art_find_rrz.LimitMin <= freq_) && (ext_art_find_rrz.LimitMax >= freq_)))
                                    {
                                        Stat = true;
                                    }
                                }
                            }
                            if (!Stat) DelIndx.Add(it);
                        }
                    }
                    //////

                    List<string> Final_Del = new List<string>();
                    for (int it = 0; it < DelIndx.Count; it++){
                        Final_Del.Add(arts[DelIndx[it]]);
                    }
                    for (int it = 0; it < Final_Del.Count; it++) {
                        NewArtcleList.Remove(Final_Del[it]);
                    }

                    if (isFLStation) {
                        double aglx = GetAglStation(_drvMemo.appIds[0]);
                        ExtArticles ext_art_find = arts_ext.Find(r => r.LimitMin <= aglx && r.LimitMax >= aglx && r.isBaseArticle);
                        if (ext_art_find != null) {
                            defValue = ext_art_find.NameArt;
                        }
                        else {
                            if (!NewArtcleList.Contains(defValue)) defValue = "";
                        }
                    }
                    else {
                        double aglx = GetAglStation(_drvMemo.appIds[0]);
                        ExtArticles ext_art_find = arts_ext.Find(r => r.LimitMin <= aglx && r.LimitMax >= aglx && !r.isBaseArticle);
                        if (ext_art_find != null) {
                            defValue = ext_art_find.NameArt;
                        }
                        else {
                            if (!NewArtcleList.Contains(defValue)) defValue = "";
                        }
                    }

                    List<double> Min_Freq = new List<double>();
                    foreach (int f_sta in _drvMemo.appIds) {
                        double freq_ = GetFreqStation(f_sta);
                        if (freq_ != IM.NullD) {
                            Min_Freq.Add(freq_);
                        }
                    }
                    if (Min_Freq.Count > 0) {
                        Min_Freq.Sort();
                        ExtArticles ext_art_find_rrz = arts_ext_rrz.Find(r => r.LimitMax >= Min_Freq[0] && r.LimitMin <= Min_Freq[0]);
                        if (ext_art_find_rrz != null) {
                            defValue = ext_art_find_rrz.NameArt;
                        }
                        else defValue = "";
                    }
                }
                
                arts = NewArtcleList;
            }


            newList.articles = arts;
            newList.defaultValue = defValue;
            articles.Add(newList);

            /////////////////
            articlesList newList_OnlyWork3 = new articlesList();
            newList_OnlyWork3.type = (WorkType)Enum.Parse(typeof(WorkType), type);
            newList_OnlyWork3.List_default = new List<KeyValuePair<int, string>>();
            newList_OnlyWork3.List_arts = new List<KeyValuePair<int, List<string>>>();
            List<string> arts_works3 = new List<string>();
            newList = new articlesList();
            newList.type = (WorkType)Enum.Parse(typeof(WorkType), type);
            if (newList.type == WorkType.work3)  {
                bool isFLStation = false;
                bool isModernStation = false;
                var NewArtcleList = new List<string>();
                
                if (_drvMemo.appIds.Count > 0) {
                    for (int h=0; h < _drvMemo.appIds.Count; h++) {
                        NewArtcleList = new List<string>();
                        NewArtcleList.AddRange(arts_only_works);
                        newList = new articlesList();
                        isFLStation = IsBaseStation(_drvMemo.appIds[h], TypeConstraint.FL);
                        isModernStation = IsBaseStation(_drvMemo.appIds[h], TypeConstraint.MODERN);
                        List<int> DelIndx = new List<int>();
                        if (!isFLStation) {
                            for (int it = 0; it < NewArtcleList.Count; it++) {
                                if (NewArtcleList[it].EndsWith("а")) {
                                    DelIndx.Add(it);
                                }
                            }
                        }
                        if (!isModernStation){
                            for (int it = 0; it < NewArtcleList.Count; it++) {
                                if (NewArtcleList[it].EndsWith("ам")) {
                                    DelIndx.Add(it);
                                }
                            }
                        }
                        for (int it = 0; it < NewArtcleList.Count; it++) {
                            ExtArticles ext_art = arts_ext.Find(r => r.NameArt == NewArtcleList[it]);
                            if (ext_art != null) {
                                if (!IsStationEquationAgl(_drvMemo.appIds[h], ext_art.LimitMin, ext_art.LimitMax)){
                                    DelIndx.Add(it);
                                }
                            }
                        }
                        ///////
                        for (int it = 0; it < NewArtcleList.Count; it++)
                        {
                            ExtArticles ext_art_find_rrz = arts_ext_rrz.Find(r => r.NameArt == NewArtcleList[it]);
                            if (ext_art_find_rrz != null)
                            {
                                bool Stat = false;
                                foreach (int f_sta in _drvMemo.appIds)
                                {
                                    double freq_ = GetFreqStation(f_sta);
                                    if (freq_ != IM.NullD)
                                    {
                                        if (((ext_art_find_rrz.LimitMin <= freq_) && (ext_art_find_rrz.LimitMax >= freq_)))
                                        {
                                            Stat = true;
                                        }
                                    }
                                }
                                if (!Stat) DelIndx.Add(it);
                            }
                        }
                        //////

                        List<string> Final_Del = new List<string>();
                        for (int it = 0; it < DelIndx.Count; it++) {
                            Final_Del.Add(arts_only_works[DelIndx[it]]);
                        }
                        for (int it = 0; it < Final_Del.Count; it++) {
                            NewArtcleList.Remove(Final_Del[it]);
                        }


                        if (isFLStation) {
                            double aglx = GetAglStation(_drvMemo.appIds[h]);
                            ExtArticles ext_art_find = arts_ext.Find(r => r.LimitMin <= aglx && r.LimitMax >= aglx && r.isBaseArticle);
                            if (ext_art_find != null) {
                                defValue = ext_art_find.NameArt;
                            }
                            else {
                                if (!NewArtcleList.Contains(defValue)) defValue = "";
                            }
                        }
                        else {
                            double aglx = GetAglStation(_drvMemo.appIds[h]);
                            ExtArticles ext_art_find = arts_ext.Find(r => r.LimitMin <= aglx && r.LimitMax >= aglx && !r.isBaseArticle);
                            if (ext_art_find != null) {
                                defValue = ext_art_find.NameArt;
                            }
                            else {
                                if (!NewArtcleList.Contains(defValue)) defValue = "";
                            }
                        }


                        arts_works3.AddRange(NewArtcleList);
                        newList_OnlyWork3.List_default.Add(new KeyValuePair<int, string>(_drvMemo.appIds[h], defValue));
                        newList_OnlyWork3.List_arts.Add(new KeyValuePair<int, List<string>>(_drvMemo.appIds[h], NewArtcleList));
                    }
                }
                List<string> ER = new List<string>();
                foreach (string rr in arts_works3) {
                    if (!ER.Contains(rr))
                        ER.Add(rr);
                }
                arts_works3 = ER;
            }

            newList_OnlyWork3.articles = arts_works3;
            newList_OnlyWork3.defaultValue = defValue;
            articles_Only_Work3.Add(newList_OnlyWork3);
           ////////////////

         }
         InitializeComponent();
         
         pnChbPtk.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         chbParamRez.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         //pnTblParamRez.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         chbComiss.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         //pnTblComiss.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         pnOdo.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         chbOther.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
         //pnTblOther.Visible = PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;

         InitGrids();
         
         if (st == DRVStatus.newapl)
         {
            //LoadApp();
            btDRV.Enabled = true;
            btPrint.Enabled = true;
            btDelete.Visible = false;
            ///  btCancel.Visible = true;
         }
         else if (st == DRVStatus.saved)
         {
            LoadApp();
            btDRV.Visible = true;
            btPrint.Enabled = true;
            btCancel.Visible = false;
            btDelete.Visible = true;
         }

         int countWork3 = 0;
         int countWork4 = 0;
         foreach (Work w in lockedWorks)
         {            
            switch (w.type)
            {
               case WorkType.work1: {
                  Grid grid = grid1;
                  CheckBox chb = chb1;
                  chb.Checked = true;
                  grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                  grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                  grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                  chb.Enabled = false;
                  grid.Enabled = false;

                  } break;
               case WorkType.work2:
                  {
                     Grid grid = grid2;
                     CheckBox chb = chb2;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work3:
                  {
                      countWork3 += ProcessAnother(w.article) == 0 ? w.count : ProcessAnother(w.article);
                     Grid grid = grid3;
                     CheckBox chb = chb3;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = countWork3.ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work4:
                  {
                     countWork4 += ProcessAnother(w.article) == 0 ? w.count : ProcessAnother(w.article);
                     Grid grid = grid4;
                     CheckBox chb = chb4;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = countWork4.ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work5:
                  {
                     Grid grid = grid5;
                     CheckBox chb = chb5;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work6:
                  {
                     Grid grid = grid6;
                     CheckBox chb = chb6;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work7:
                  {
                     Grid grid = grid7;
                     CheckBox chb = chb7;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.other:
                  {
                     Grid grid = grid8;
                     CheckBox chb = chb8;
                     chb.Checked = true;
                     grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                     grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                     grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                     chb.Enabled = false;
                     grid.Enabled = false;
                  } break;
               case WorkType.work14:
                   {
                       CheckBox chb = chbPtk;
                       chb.Checked = true;
                       chb.Enabled = false;
                       btPtk.Enabled = false;
                   } break;
               case WorkType.work15:
                   {
                       Grid grid = gridParamRez;
                       CheckBox chb = chbParamRez;
                       chb.Checked = true;
                       grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                       grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                       grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                       chb.Enabled = false;
                       gridParamRez.Enabled = false;
                   } break;
               case WorkType.work19:
                   {
                       Grid grid = gridComiss;
                       CheckBox chb = chbComiss;
                       chb.Checked = true;
                       grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                       grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString(); 
                       grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                       grid.GetCell(grid.GetRowCount() - 1, 3).Value = ((ProcessAnother(w.article) == 0 ? w.count * w.index : ProcessAnother(w.article))).ToString();
                       chb.Enabled = false;
                       grid.Enabled = false;
                   } break;
               case WorkType.work52:
                   {
                       Grid grid = gridOther;
                       CheckBox chb = chbOther;
                       chb.Checked = true;
                       grid.GetCell(grid.GetRowCount() - 1, 0).Value = w.article;
                       grid.GetCell(grid.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString(); 
                       grid.GetCell(grid.GetRowCount() - 1, 2).Value = w.index.ToString();
                       chb.Enabled = false;
                       grid.Enabled = false;
                   } break;
               case WorkType.work50:
                   {
                       CheckBox chb = chbOdo;
                       chb.Checked = true;
                       chb.Enabled = false;
                       tbOdo.Enabled = false;
                       tbOdo.Text = w.index.ToString();
                   } break;
            }
         }
         if ((drvMemo.packetType != EPacketType.PckDOZV) && (drvMemo.packetType != EPacketType.PckPTKNVTV))
         {
             //chbPtk.Enabled = false;
             //chbParamRez.Enabled = false;
             //chbOther.Enabled = false;
         }
         if ((drvMemo.packetType != EPacketType.PckVISN) && (true))
         {
             chb1.Enabled = false;
             chb3.Enabled = false;
             chb4.Enabled = false;
             chb5.Enabled = false;
             chb6.Enabled = false;
         }

         tbOwner.Text = drvMemo.OwnerName;
      }

      void InitHeaderCell(Cell c)
      {
         c.FontStyle = FontStyle.Bold;
         c.HorizontalAlignment = HorizontalAlignment.Center;
         c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
         c.BackColor = System.Drawing.Color.DarkGray;
         c.TextHeight = 11;
         c.CanEdit = false;
      }

      void InitGrid(Grid grid, bool enBut, Control parent, WorkType wt, bool isBtnCount, bool initHeader)
      { 
        // grid = new GridCtrl.Grid();
         grid.AllowDrop = true;
         grid.BackColor = System.Drawing.SystemColors.Window;
         grid.EditMode = true;
         grid.FirstColumnWidth = 120;
         grid.GridLine = GridCtrl.Grid.GridLineStyle.Both;
         grid.GridLineColor = System.Drawing.Color.Black;
         grid.GridOpacity = 100;
         grid.Image = null;
         grid.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;         
         grid.LockUpdates = false;
         grid.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;       
         grid.ToolTips = true;
         grid.Parent = parent;
         grid.Dock = DockStyle.Fill;
         if (initHeader)
         {
             grid.AddRow();
             Cell c = grid.AddColumn(grid.GetRowCount() - 1, "Стаття");
             InitHeaderCell(c);
             c = grid.AddColumn(grid.GetRowCount() - 1, "Кількість");
             InitHeaderCell(c);
             c = grid.AddColumn(grid.GetRowCount() - 1, "Коефіцієнт");
             InitHeaderCell(c);
         }
         if (wt != WorkType.notype)
         {
             string Art="";
             grid.AddRow();
             Cell c = grid.AddColumn(grid.GetRowCount() - 1, "");
             c.Key = CnArticle;
             c.TextHeight = 11;
             c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
             c.cellStyle = EditStyle.esPickList;
             foreach (articlesList list in articles)
             {
                 if (list.type == wt)
                 {
                     c.PickList = list.articles;
                     if (list.defaultValue != "")
                     {
                         c.Value = list.defaultValue;
                         Art = list.defaultValue;
                         }
                     else
                     {
                         if (list.articles.Count > 0)
                         {
                             c.Value = list.articles[0];
                             Art = list.articles[0];
                         }
                     }
                 }
             }
             /*   c.PickList = articles[(int)wt];
                if (articles[(int)wt].Count > 0)
                   c.Value = articles[(int)wt][0];*/
             if (enBut)
                 c = grid.AddColumn(grid.GetRowCount() - 1, "1");
             else
             {
                 //c = grid.AddColumn(grid.GetRowCount() - 1, ProcessAnother(Art) == 0 ? drvMemo.appIds.Count.ToString() : ProcessAnother(Art).ToString());
                 string Ret_ = "";
                 if ((ProcessAnother(Art) == 0) && (IsAMateur(Art)))
                 {
                     Ret_ = "0";
                 }
                 else if ((ProcessAnother(Art) == 0) && (!IsAMateur(Art)))
                 {
                     Ret_ = drvMemo.appIds.Count.ToString();
                 }
                 else
                 {
                     Ret_ = ProcessAnother(Art).ToString();
                 }

                 c = grid.AddColumn(grid.GetRowCount() - 1, Ret_);
             }
             

             
             c.Key = CnCount;
             c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
             c.TextHeight = 11;
             c.CanEdit = !enBut;
             if (isBtnCount)
             {
                 //Добавляем кнопку в ячейку "Кол-во"
                 c.cellStyle = EditStyle.esEllipsis;
                 c.ButtonText = "Розр.";
                 grid.OnCellButtonClick += new Grid.CellButtonClickDelegate(CountWorkButtonClick);
                 if (grid == grid7) //просили только тут 
                 ProcessWorkButtonClick(c);
             }
             if (enBut)
             {
                 c = grid.AddColumn(grid.GetRowCount() - 1, "");
                 c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
                 c.TextHeight = 11;
                 c.cellStyle = EditStyle.esEllipsis;
                 c.ButtonText = "Таблиця";
                 grid.OnCellButtonClick += new Grid.CellButtonClickDelegate(TableButtonClick);
             }
             else
             {
                 c = grid.AddColumn(grid.GetRowCount() - 1, "");
                 c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
                 c.TextHeight = 11;
             }
             c.Key = CnKoef;
         }
      }

      /// <summary>
      /// Нажата кнопка для подсчета кол-ва работ
      /// </summary>
      /// <param name="cell">Ячейка</param>
      private void CountWorkButtonClick(Cell cell)
      {
          if (cell.Key != CnCount)
              return;
          //Запрос
          DialogResult dlgRez = MessageBox.Show(CLocaliz.TxT("Do you want to recalculate work count?"), "",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (dlgRez != DialogResult.Yes)
              return;
          //Есть подтверждение

          ProcessWorkButtonClick(cell);
          
      }
       
      /// <summary>
      /// Для (TypeConstraint=FL) Если станция FL и относится к одному из стандартов РРК,РОПС,ПД,РПС,ТтРК,БЦУР,КХ,УКХ,ТРАНК,ПЕЙДЖ,БАУР,ТЕТРА,ЦУКХ,ПД,DECT або РПАТЛ, тогда признак TRUE
      /// Для (TypeConstraint=MODERN) Если станция просто относится к одному из стандартов РРК,РОПС,ПД,РПС,ТтРК,БЦУР,КХ,УКХ,ТРАНК,ПЕЙДЖ,БАУР,ТЕТРА,ЦУКХ,ПД,DECT або РПАТЛ, тогда признак TRUE 
      /// </summary>
      /// <param name="Type_Constraint"></param>
      /// <returns></returns>
      private bool IsBaseStation(int ID, TypeConstraint Type_Constraint)
      {
          bool isCheck = false;
              int OBJ_ID1 = 0;
              string OBJ_TABLE = "";
              string STANDARD = "";
              string CLASS_STATION = "";
              using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly)) {
                  rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE,STANDARD");
                  rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
                  rs.Open();
                  if (!rs.IsEOF()) {
                      OBJ_ID1 = rs.GetI("OBJ_ID1");
                      OBJ_TABLE = rs.GetS("OBJ_TABLE");
                      STANDARD = rs.GetS("STANDARD");
                      if ((STANDARD == CRadioTech.RRK) || (STANDARD == CRadioTech.ROPS) || (STANDARD == CRadioTech.PD) || (STANDARD == CRadioTech.RPS) || (STANDARD == "ТтРК") || (STANDARD == CRadioTech.BCUR)
                          || (STANDARD == CRadioTech.KX) || (STANDARD == CRadioTech.YKX) || (STANDARD == CRadioTech.TRUNK) || (STANDARD == CRadioTech.PADG) || (STANDARD == CRadioTech.BAUR) || (STANDARD == CRadioTech.TETRA)
                          || (STANDARD == CRadioTech.ЦУКХ) || (STANDARD == CRadioTech.PD) || (STANDARD == CRadioTech.DECT) || (STANDARD == CRadioTech.RPATL))
                      {
                          if (Type_Constraint == TypeConstraint.FL) {
                              if ((OBJ_ID1 != IM.NullI) && (OBJ_TABLE!= PlugTbl.XfaAbonentStation)) {
                                  using (Icsm.LisRecordSet rs_station = new Icsm.LisRecordSet(OBJ_TABLE, IMRecordset.Mode.ReadOnly)) {
                                      rs_station.Select("ID,CLASS");
                                      rs_station.SetWhere("ID", IMRecordset.Operation.Eq, OBJ_ID1);
                                      rs_station.Open();
                                      if (!rs_station.IsEOF()) {
                                          CLASS_STATION = rs_station.GetS("CLASS");
                                          if (CLASS_STATION == "FL") {
                                                isCheck = true;
                                          }
                                      }
                                  }
                              }
                          }
                          else {
                              isCheck = true;
                          }
                      }
                  }
              }
          return isCheck;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="ID"></param>
      /// <param name="MIN"></param>
      /// <param name="MAX"></param>
      /// <returns></returns>
      private bool IsStationEquationAgl(int ID, double MIN, double MAX)
      {
          bool isCheck = false;
          int OBJ_ID1 = 0;
          string OBJ_TABLE = "";
          string STANDARD = "";
          double AGL = IM.NullD;
          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
          {
              rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE,STANDARD");
              rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
              rs.Open();
              if (!rs.IsEOF())
              {
                  OBJ_ID1 = rs.GetI("OBJ_ID1");
                  OBJ_TABLE = rs.GetS("OBJ_TABLE");
                  STANDARD = rs.GetS("STANDARD");
                  if ((STANDARD == CRadioTech.BCUR)
                      || (STANDARD == CRadioTech.KX) || (STANDARD == CRadioTech.YKX) || (STANDARD == CRadioTech.TRUNK) || (STANDARD == CRadioTech.PADG) || (STANDARD == CRadioTech.BAUR) || (STANDARD == CRadioTech.TETRA)
                      || (STANDARD == CRadioTech.ЦУКХ))
                  {
                      if (OBJ_ID1 != IM.NullI)
                      {
                          using (Icsm.LisRecordSet rs_station = new Icsm.LisRecordSet(OBJ_TABLE, IMRecordset.Mode.ReadOnly))
                          {
                              rs_station.Select("ID,AGL");
                              rs_station.SetWhere("ID", IMRecordset.Operation.Eq, OBJ_ID1);
                              rs_station.Open();
                              if (!rs_station.IsEOF())
                              {
                                  AGL = rs_station.GetD("AGL");
                                  if ((AGL >= MIN) && (AGL <= MAX))
                                  {
                                      isCheck = true;
                                  }
                              }
                          }
                      }
                  }
              }
          }
          return isCheck;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="ID"></param>
      /// <param name="MIN"></param>
      /// <param name="MAX"></param>
      /// <returns></returns>
      private double GetAglStation(int ID)
      {
          double AGL_ = IM.NullD;
          int OBJ_ID1 = 0;
          string OBJ_TABLE = "";
          string STANDARD = "";
          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
          {
              rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE,STANDARD");
              rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
              rs.Open();
              if (!rs.IsEOF())
              {
                  OBJ_ID1 = rs.GetI("OBJ_ID1");
                  OBJ_TABLE = rs.GetS("OBJ_TABLE");
                  STANDARD = rs.GetS("STANDARD");
                  if ((STANDARD == CRadioTech.BCUR)
                      || (STANDARD == CRadioTech.KX) || (STANDARD == CRadioTech.YKX) || (STANDARD == CRadioTech.TRUNK) || (STANDARD == CRadioTech.PADG) || (STANDARD == CRadioTech.BAUR) || (STANDARD == CRadioTech.TETRA)
                      || (STANDARD == CRadioTech.ЦУКХ))
                  {
                      if (OBJ_ID1 != IM.NullI)
                      {
                          using (Icsm.LisRecordSet rs_station = new Icsm.LisRecordSet(OBJ_TABLE, IMRecordset.Mode.ReadOnly))
                          {
                              rs_station.Select("ID,AGL");
                              rs_station.SetWhere("ID", IMRecordset.Operation.Eq, OBJ_ID1);
                              rs_station.Open();
                              if (!rs_station.IsEOF())
                              {
                                  AGL_ = rs_station.GetD("AGL");
                              }
                          }
                      }
                  }
              }
          }
          return AGL_;
      }

      /// <summary>
      /// Получить максимальную частоту для РРЗ
      /// </summary>
      /// <param name="ID"></param>
      /// <returns></returns>
      public static  double GetFreqStation(int ID)
      {
          double Freq = IM.NullD;
          int OBJ_ID1 = 0;
          string OBJ_TABLE = "";
          string STANDARD = "";
          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
          {
              rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE,STANDARD");
              rs.SetWhere("ID", IMRecordset.Operation.Eq, ID);
              rs.Open();
              if (!rs.IsEOF())
              {
                  OBJ_ID1 = rs.GetI("OBJ_ID1");
                  OBJ_TABLE = rs.GetS("OBJ_TABLE");
                  STANDARD = rs.GetS("STANDARD");
                  if (STANDARD == CRadioTech.RS)
                  {
                      if (OBJ_ID1 != IM.NullI)
                      {
                          using (Icsm.LisRecordSet rs_station = new Icsm.LisRecordSet(OBJ_TABLE, IMRecordset.Mode.ReadOnly))
                          {
                              rs_station.Select("ID,StationA.TX_FREQ,StationB.TX_FREQ");
                              rs_station.SetWhere("ID", IMRecordset.Operation.Eq, OBJ_ID1);
                              rs_station.Open();
                              if (!rs_station.IsEOF())
                              {
                                  double Freq_A = rs_station.GetD("StationA.TX_FREQ");
                                  double Freq_B = rs_station.GetD("StationB.TX_FREQ");
                                  if (Freq_B >= Freq_A)
                                      Freq = Freq_B;
                                  else
                                      Freq = Freq_A;
                              }
                          }
                      }
                  }
              }
          }
          return Freq;
      }


       /// <summary>
       /// 
       /// </summary>
       /// <param name="Art"></param>
       /// <returns></returns>
      private bool IsAMateur(string Art)
      {

          bool isAmateur = false;
          if (drvMemo.appIds.Count > 0)
          {
              bool[] isActiveDozv_7_2 = new bool[drvMemo.appIds.Count];
              bool[] isActiveDozv_6_4 = new bool[drvMemo.appIds.Count];


              for (int i = 0; i < drvMemo.appIds.Count; i++)
              {
                  //////////////////
                  int OBJ_ID1 = 0;
                  string OBJ_TABLE = "";

                  using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                  {
                      rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE");
                      rs.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                      rs.Open();
                      if (!rs.IsEOF())
                      {
                          OBJ_ID1 = rs.GetI("OBJ_ID1");
                          OBJ_TABLE = rs.GetS("OBJ_TABLE");
                      }
                  }

                  if ((OBJ_TABLE == PlugTbl.XfaAmateur) && ((Art == ArticleLib.ArticleFunc.article_7_2) || (Art == ArticleLib.ArticleFunc.article_6_4)))
                  {
                      isAmateur = true;
                  }
              }
          }
          return isAmateur;
      }

       private bool isContainDozv()
      {
          bool isContain = false;
          int ID_Packet = 0;
          for (int i = 0; i < drvMemo.appIds.Count; i++)
          {
              isContain = false;
              using (Icsm.LisRecordSet rs_c = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
              {
                  rs_c.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                  rs_c.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                  rs_c.SetWhere("DOC_NUMBER", IMRecordset.Operation.Eq, packet_.NumberIn);
                  rs_c.OrderBy("ID", OrderDirection.Descending);
                  for (rs_c.Open(); !rs_c.IsEOF(); rs_c.MoveNext())
                  {
                      if ((rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evBranchPacket.ToString()))
                      {
                          ID_Packet = rs_c.GetI("ID");
                          break;
                      }
                  }
              }


              using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
              {
                  rs.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                  rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                  rs.SetWhere("ID", IMRecordset.Operation.Ge, ID_Packet);
                  rs.OrderBy("ID", OrderDirection.Descending);
                  for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                  {

                      if (((rs.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evBranchPacket.ToString())))
                      {
                          break;
                      }
                      else if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evVISN.ToString())) || ((rs.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                      {
                          if (drvMemo.applType != AppType.AppA3)
                          {
                              if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                                  tpx_docs = TypeDocs.dozvil;
                              else if (rs.GetS("EVENT") == EDocEvent.evVISN.ToString())
                                  tpx_docs = TypeDocs.vysnovok;
                              else
                                  tpx_docs = TypeDocs.unknown;

                              isContain = true;
                          }
                          else
                          {
                              if (drvMemo.applType == AppType.AppA3)
                              {
                                  if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())) || ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())))
                                  {
                                      if ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()))
                                      {
                                          using (Icsm.LisRecordSet rs_serch_dozv = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                                          {
                                              rs_serch_dozv.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER, DOC_END_DATE");
                                              rs_serch_dozv.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                              for (rs_serch_dozv.Open(); !rs_serch_dozv.IsEOF(); rs_serch_dozv.MoveNext())
                                              {
                                                  if ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                                                  {
                                                      if (rs_serch_dozv.GetT("DOC_END_DATE") >= DateTime.Now)
                                                      {
                                                          tpx_docs = TypeDocs.dozvil;
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                      else tpx_docs = TypeDocs.dozvil;
                                  }
                                  else if ((rs.GetS("EVENT") == EDocEvent.evVISN.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())))
                                  {
                                      if ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()))
                                      {
                                          using (Icsm.LisRecordSet rs_serch_dozv = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                                          {
                                              rs_serch_dozv.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER, DOC_END_DATE");
                                              rs_serch_dozv.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                              for (rs_serch_dozv.Open(); !rs_serch_dozv.IsEOF(); rs_serch_dozv.MoveNext())
                                              {
                                                  if (((rs_serch_dozv.GetS("EVENT") == EDocEvent.evVISN.ToString())) || ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                                                  {
                                                      if (rs_serch_dozv.GetT("DOC_END_DATE") >= DateTime.Now)
                                                      {
                                                          tpx_docs = TypeDocs.vysnovok;
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                      else tpx_docs = TypeDocs.vysnovok;

                                  }
                                  else tpx_docs = TypeDocs.unknown;

                                  if (tpx_docs != TypeDocs.unknown)
                                      isContain = true;
                              }
                          }
                      }
                      else
                      {
                          if (drvMemo.applType == AppType.AppA3)
                          {
                              if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())) || ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())))
                              {
                                  if ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()))
                                  {
                                      using (Icsm.LisRecordSet rs_serch_dozv = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                                      {
                                          rs_serch_dozv.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER, DOC_END_DATE");
                                          rs_serch_dozv.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                          for (rs_serch_dozv.Open(); !rs_serch_dozv.IsEOF(); rs_serch_dozv.MoveNext())
                                          {
                                              if ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                                              {
                                                  if (rs_serch_dozv.GetT("DOC_END_DATE") >= DateTime.Now)
                                                  {
                                                      tpx_docs = TypeDocs.dozvil;
                                                  }
                                              }
                                          }
                                      }
                                  }
                                  else tpx_docs = TypeDocs.dozvil;
                              }
                              else if ((rs.GetS("EVENT") == EDocEvent.evVISN.ToString()) || ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())))
                              {
                                  if ((rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()))
                                  {
                                      using (Icsm.LisRecordSet rs_serch_dozv = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                                      {
                                          rs_serch_dozv.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER, DOC_END_DATE");
                                          rs_serch_dozv.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                          for (rs_serch_dozv.Open(); !rs_serch_dozv.IsEOF(); rs_serch_dozv.MoveNext())
                                          {
                                              if (((rs_serch_dozv.GetS("EVENT") == EDocEvent.evVISN.ToString())) || ((rs_serch_dozv.GetS("EVENT") == EDocEvent.evSpecialDozv.ToString())))
                                              {
                                                  if (rs_serch_dozv.GetT("DOC_END_DATE") >= DateTime.Now)
                                                  {
                                                      tpx_docs = TypeDocs.vysnovok;
                                                  }
                                              }
                                          }
                                      }
                                  }
                                  else tpx_docs = TypeDocs.vysnovok;

                              }
                              else tpx_docs = TypeDocs.unknown;
                              
                              if (tpx_docs != TypeDocs.unknown)
                                isContain = true;
                          }
                      }
                      L_tpx_docs.Add(tpx_docs);
                  }
              }
              if (!isContain) break;
              //if (isContain) break;
          }
          return isContain;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="Art"></param>
      /// <returns></returns>
      private int ProcessAnother(string Art)
      {
          int count = 0;

          if (drvMemo.appIds.Count > 0)
          {
              bool[] isActiveDozv_7_2 = new bool[drvMemo.appIds.Count];
              bool[] isActiveDozv_6_4 = new bool[drvMemo.appIds.Count];


              for (int i = 0; i < drvMemo.appIds.Count; i++)
              {
                  //////////////////
                  int OBJ_ID1 = 0;
                  string OBJ_TABLE = "";

                  using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                  {
                      rs.Select("ID,Price.ARTICLE,WORKS_COUNT,Price2.ARTICLE,WORKS_COUNT2,OBJ_ID1,OBJ_TABLE");
                      rs.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                      rs.Open();
                      if (!rs.IsEOF())
                      {
                          OBJ_ID1 = rs.GetI("OBJ_ID1");
                          OBJ_TABLE = rs.GetS("OBJ_TABLE");
                      }
                  }

                  if ((OBJ_ID1 != IM.NullI) && ((OBJ_TABLE == PlugTbl.XfaAbonentStation) || (OBJ_TABLE == PlugTbl.XfaEmitterStation)) && (Art == ArticleLib.ArticleFunc.article_6_4))
                  {
                      count = drvMemo.appIds.Count;
                  }

                  if ((OBJ_ID1 != IM.NullI) && (OBJ_TABLE == PlugTbl.Ship) && (Art == ArticleLib.ArticleFunc.article_6_3))
                  {
                      ShipStation _shipStation = new ShipStation();
                      _shipStation.Id = OBJ_ID1;
                      _shipStation.Load();
                      count += _shipStation.Equipment.Count;
                  }

                  if ((OBJ_ID1 != IM.NullI) && (OBJ_TABLE == PlugTbl.XfaAmateur) && (Art == ArticleLib.ArticleFunc.article_7_2))
                  {
                      int cnt_x = 0;
                      int ID_Packet = 0;

                      using (Icsm.LisRecordSet rs_c = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                      {
                          rs_c.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                          rs_c.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                          rs_c.SetWhere("DOC_NUMBER", IMRecordset.Operation.Eq, packet_.NumberIn);
                          rs_c.OrderBy("ID", OrderDirection.Descending);
                          for (rs_c.Open(); !rs_c.IsEOF(); rs_c.MoveNext())
                          {
                              if ((rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evBranchPacket.ToString()))
                              {
                                  ID_Packet = rs_c.GetI("ID");
                                  break;
                              }
                          }
                      }


                      using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                      {
                          rs.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                          rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                          rs.SetWhere("ID", IMRecordset.Operation.Ge, ID_Packet);
                          rs.OrderBy("ID", OrderDirection.Ascending);
                          for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                          {



                              if (((rs.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evBranchPacket.ToString())) && (rs.GetS("DOC_NUMBER").TrimEnd().TrimStart() != packet_.NumberIn.TrimEnd().TrimStart()))
                              {
                                  break;
                              }



                              using (Icsm.LisRecordSet rs_appl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                              {
                                  rs_appl.Select("ID,OBJ_ID1,OBJ_TABLE,DOZV_NUM,DOZV_NUM_NEW");
                                  rs_appl.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                  rs_appl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "XFA_AMATEUR");
                                  for (rs_appl.Open(); !rs_appl.IsEOF(); rs_appl.MoveNext())
                                  {

                                      using (Icsm.LisRecordSet rs_amateur = new Icsm.LisRecordSet(PlugTbl.XfaAmateur, IMRecordset.Mode.ReadOnly))
                                      {
                                          rs_amateur.Select("ID");
                                          rs_amateur.SetWhere("ID", IMRecordset.Operation.Eq, rs_appl.GetI("OBJ_ID1"));
                                          for (rs_amateur.Open(); !rs_amateur.IsEOF(); rs_amateur.MoveNext())
                                          {

                                              //if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || (rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()) || (rs.GetS("EVENT") == EDocEvent.evSPS.ToString()))
                                              if (rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())
                                              {
                                                  using (Icsm.LisRecordSet rs_XFA_ES_AMATEUR = new Icsm.LisRecordSet(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly))
                                                  {
                                                      rs_XFA_ES_AMATEUR.Select("ID,STA_ID,NUMB_DOZV");
                                                      rs_XFA_ES_AMATEUR.SetWhere("STA_ID", IMRecordset.Operation.Eq, rs_appl.GetI("OBJ_ID1"));
                                                      rs_XFA_ES_AMATEUR.SetWhere("NUMB_DOZV", IMRecordset.Operation.Eq, rs.GetS("DOC_NUMBER"));
                                                      //MessageBox.Show("XfaEsAmateur: " + rs_appl.GetI("ID").ToString() + " " + rs_appl.GetI("OBJ_ID1").ToString() + " " + rs.GetS("DOC_NUMBER"));
                                                      rs_XFA_ES_AMATEUR.Open();
                                                      if (!rs_XFA_ES_AMATEUR.IsEOF())
                                                      {
                                                          cnt_x++;
                                                      }
                                                  }

                                              }
                                              if (rs.GetS("EVENT") == EDocEvent.evSPS.ToString())
                                              {
                                                  using (Icsm.LisRecordSet rs_XFA_CALL_AMATEUR = new Icsm.LisRecordSet(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadOnly))
                                                  {
                                                      rs_XFA_CALL_AMATEUR.Select("ID,STA_ID,NUMBER_DOZV");
                                                      rs_XFA_CALL_AMATEUR.SetWhere("STA_ID", IMRecordset.Operation.Eq, OBJ_ID1);
                                                      rs_XFA_CALL_AMATEUR.SetWhere("NUMBER_DOZV", IMRecordset.Operation.Eq, rs.GetS("DOC_NUMBER"));
                                                      rs_XFA_CALL_AMATEUR.Open();
                                                      if (!rs_XFA_CALL_AMATEUR.IsEOF())
                                                      {
                                                          cnt_x++;
                                                      }
                                                  }

                                              }
                                              if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) && (!isActiveDozv_7_2[i]))
                                              {
                                                  cnt_x++;
                                                  isActiveDozv_7_2[i] = true;
                                              }
                                              break;
                                          }
                                      }
                                      break;
                                  }
                              }

                          }
                          count += cnt_x;
                      }
                  }
                  else if ((OBJ_ID1 != IM.NullI) && (OBJ_TABLE == PlugTbl.XfaAmateur) && (Art == ArticleLib.ArticleFunc.article_6_4))
                  {
                      int cnt_x = 0;
                      int ID_Packet = 0;

                      using (Icsm.LisRecordSet rs_c = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                      {
                          rs_c.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                          rs_c.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                          rs_c.SetWhere("DOC_NUMBER", IMRecordset.Operation.Eq, packet_.NumberIn);
                          rs_c.OrderBy("ID", OrderDirection.Descending);
                          for (rs_c.Open(); !rs_c.IsEOF(); rs_c.MoveNext())
                          {
                              if ((rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs_c.GetS("EVENT") == EDocEvent.evBranchPacket.ToString()))
                              {
                                  ID_Packet = rs_c.GetI("ID");
                                  break;
                              }
                          }
                      }


                      using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.EventLog, IMRecordset.Mode.ReadOnly))
                      {
                          rs.Select("ID,OBJ_ID,OBJ_TABLE,EVENT, DOC_NUMBER");
                          rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                          rs.SetWhere("ID", IMRecordset.Operation.Ge, ID_Packet);
                          rs.OrderBy("ID", OrderDirection.Ascending);
                          for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                          {



                              if (((rs.GetS("EVENT") == EDocEvent.evCreatePacketURCP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evCreatePacketURZP.ToString()) || (rs.GetS("EVENT") == EDocEvent.evBranchPacket.ToString())) && (rs.GetS("DOC_NUMBER").TrimEnd().TrimStart() != packet_.NumberIn.TrimEnd().TrimStart()))
                              {
                                  break;
                              }

                              using (Icsm.LisRecordSet rs_appl = new Icsm.LisRecordSet(PlugTbl.APPL, IMRecordset.Mode.ReadOnly))
                              {
                                  rs_appl.Select("ID,OBJ_ID1,OBJ_TABLE,DOZV_NUM,DOZV_NUM_NEW");
                                  rs_appl.SetWhere("ID", IMRecordset.Operation.Eq, drvMemo.appIds[i]);
                                  rs_appl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, "XFA_AMATEUR");
                                  for (rs_appl.Open(); !rs_appl.IsEOF(); rs_appl.MoveNext())
                                  {

                                      using (Icsm.LisRecordSet rs_amateur = new Icsm.LisRecordSet(PlugTbl.XfaAmateur, IMRecordset.Mode.ReadOnly))
                                      {
                                          rs_amateur.Select("ID");
                                          rs_amateur.SetWhere("ID", IMRecordset.Operation.Eq, rs_appl.GetI("OBJ_ID1"));
                                          for (rs_amateur.Open(); !rs_amateur.IsEOF(); rs_amateur.MoveNext())
                                          {

                                              //if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) || (rs.GetS("EVENT") == EDocEvent.evMOVE.ToString()) || (rs.GetS("EVENT") == EDocEvent.evSPS.ToString()))
                                              if (rs.GetS("EVENT") == EDocEvent.evMOVE.ToString())
                                              {
                                                  using (Icsm.LisRecordSet rs_XFA_ES_AMATEUR = new Icsm.LisRecordSet(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly))
                                                  {
                                                      rs_XFA_ES_AMATEUR.Select("ID,STA_ID,NUMB_DOZV");
                                                      rs_XFA_ES_AMATEUR.SetWhere("STA_ID", IMRecordset.Operation.Eq, rs_appl.GetI("OBJ_ID1"));
                                                      rs_XFA_ES_AMATEUR.SetWhere("NUMB_DOZV", IMRecordset.Operation.Eq, rs.GetS("DOC_NUMBER"));
                                                      //MessageBox.Show("XfaEsAmateur: " + rs_appl.GetI("ID").ToString() + " " + rs_appl.GetI("OBJ_ID1").ToString() + " " + rs.GetS("DOC_NUMBER"));
                                                      rs_XFA_ES_AMATEUR.Open();
                                                      if (!rs_XFA_ES_AMATEUR.IsEOF())
                                                      {
                                                          cnt_x++;
                                                      }
                                                  }

                                              }

                                              if ((rs.GetS("EVENT") == EDocEvent.evDOZV.ToString()) && (!isActiveDozv_6_4[i]))
                                              {
                                                  cnt_x++;
                                                  isActiveDozv_6_4[i] = true;
                                              }
                                              break;
                                          }
                                      }
                                      break;
                                  }
                              }

                          }
                          count += cnt_x;
                      }
                  }
              }
          }
          return count;
      }


      private void ProcessWorkButtonClick(Cell cell)
      {
          Grid curGrid = cell.row.grid;
          if (curGrid == grid1)
          {
              int count = 0;
              string article = curGrid.GetCellFromKey(CnArticle).Value;
              using (CProgressBar pb = new CProgressBar(string.Format("Calculating works for article {0}...", article)))
              {
                  pb.ShowBig("Please wait...");
                  for (int i = 0; i < drvMemo.appIds.Count; i++)
                  {
                      int applId = drvMemo.appIds[i];
                      int retCount = SqlDatabase.CalculateWorks(applId, article);
                      if (retCount != IM.NullI)
                      {
                          count += retCount;
                          pb.ShowSmall(count);
                      }
                      pb.ShowProgress(i, drvMemo.appIds.Count);
                  }
              }
              //curGrid.GetCellFromKey(CnCount).Value = count.ToString();
              curGrid.GetCellFromKey(CnCount).Value = ProcessAnother(article) == 0 ? count.ToString() : ProcessAnother(article).ToString();
          }
          else if (curGrid == grid7)
          {
              int cnt = 0;
              string article = curGrid.GetCellFromKey(CnArticle).Value;
              if (curGrid.GetCellFromKey(CnArticle).Value == "6")
              {
                  for (int i = 0; i < drvMemo.appIds.Count; i++)
                  {
                      cnt += GetNumRes(drvMemo.appIds[i]);
                  }
              }
              curGrid.GetCellFromKey(CnCount).Value = ProcessAnother(article) == 0 ? cnt.ToString() : ProcessAnother(article).ToString();
          }
      }

      private int GetNumRes(int applId)
      {
          int retVal = 0;
          IMRecordset appRs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
          string idlist = "";
          string objTable = "";
          try
          {
              appRs.Select("OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
              appRs.SetWhere("ID", IMRecordset.Operation.Eq, applId);
              appRs.Open();
              if (!appRs.IsEOF())
              {
                  retVal = 1;
                  for (int n = 1; n <= 6; n++)
                  {
                      int objId = appRs.GetI("OBJ_ID" + n.ToString());
                      if (objId != IM.NullI)
                          idlist += (idlist.Length > 0 ? "," : "") + objId.ToString();
                  }
                  objTable = appRs.GetS("OBJ_TABLE");
              }

          }
          finally
          {
              appRs.Destroy();
          }

          if (objTable == "SHIP" && idlist.Length > 0)
          {
              IM.ExecuteScalar(ref retVal, "select sum(cast(REMARK as int)) from %SHIP_EQP where STA_ID in (" + idlist + ")");
              if (retVal == IM.NullI) 
                  retVal = 0;
          }

          return retVal;
      }

       // copied from URZP form
      void InitGrid(Grid grid, bool enBut, Control parent, WorkType wt, bool initHeader)
      {
          // grid = new GridCtrl.Grid();
          grid.AllowDrop = true;
          grid.BackColor = System.Drawing.SystemColors.Window;
          grid.EditMode = true;
          grid.FirstColumnWidth = 120;
          grid.GridLine = GridCtrl.Grid.GridLineStyle.Both;
          grid.GridLineColor = System.Drawing.Color.Black;
          grid.GridOpacity = 100;
          grid.Image = null;
          grid.ImageStyle = GridCtrl.Grid.ImageStyleType.Stretch;
          grid.LockUpdates = false;
          grid.SelectionMode = GridCtrl.Grid.SelectionModeType.CellSelect;
          grid.ToolTips = true;
          grid.Parent = parent;
          grid.Dock = DockStyle.Fill;
          if (wt == WorkType.work19)
          {
              Row r = grid.AddRow();
              grid.FirstColumnWidth = 70;
              Cell c = grid.AddColumn(grid.GetRowCount() - 1, "Стаття");
              InitHeaderCell(c);
              c = grid.AddColumn(grid.GetRowCount() - 1, 110, "Кількість людей");
              InitHeaderCell(c);
              c = grid.AddColumn(0, 110, "Кількість годин");
              InitHeaderCell(c);
              c = grid.AddColumn(0, "Загальний витрачений час");
              InitHeaderCell(c);

              grid.AddRow();
              c = grid.AddColumn(grid.GetRowCount() - 1, "");
              c.TextHeight = 11;
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.cellStyle = EditStyle.esPickList;
              foreach (HelpClasses.articlesList list in drvMemo.articles)
              {
                  if (list.type == wt)
                  {
                      c.PickList = list.articles;
                      if (list.defaultValues.ContainsKey(PriceDefaultType.pdtUsual))
                          c.Value = list.defaultValues[PriceDefaultType.pdtUsual];
                      else
                      {
                          if (list.articles.Count > 0)
                              c.Value = list.articles[0];
                      }
                  }
              }

              c = grid.AddColumn(grid.GetRowCount() - 1, 110, "");
              c.Key = "menCount";
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.TextHeight = 11;
              c.Value = "0";
              c = grid.AddColumn(grid.GetRowCount() - 1, 110, "");
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.TextHeight = 11;
              c.Value = "0";
              c = grid.AddColumn(grid.GetRowCount() - 1, "");
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.TextHeight = 11;
              c.Value = "0";

              grid.OnDataValidate += new Grid.DataValidateDelegate(grid_OnDataValidate);
          }
          else
          {
              if (initHeader)
              {
                grid.AddRow();              
                InitHeaderCell(grid.AddColumn(grid.GetRowCount() - 1, "Стаття"));
                InitHeaderCell(grid.AddColumn(grid.GetRowCount() - 1, "Кількість"));
                InitHeaderCell(grid.AddColumn(grid.GetRowCount() - 1, "Коефіцієнт"));
              }              
              grid.AddRow();
              // Статья (позиция)
              Cell c = grid.AddColumn(grid.GetRowCount() - 1, "");
              c.TextHeight = 11;
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.cellStyle = EditStyle.esPickList;
              foreach (HelpClasses.articlesList list in drvMemo.articles)
              {
                  if (list.type == wt)
                  {
                      c.PickList = list.articles;
                      if (list.defaultValues.ContainsKey(PriceDefaultType.pdtUsual))
                          c.Value = list.defaultValues[PriceDefaultType.pdtUsual];
                      else
                      {
                          if (list.articles.Count > 0)
                              c.Value = list.articles[0];
                      }
                  }
              }
              // Количество
              c = grid.AddColumn(grid.GetRowCount() - 1, enBut ? "1" : drvMemo.appIds.Count.ToString());
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.TextHeight = 11;
              // Коэффициент
              c = grid.AddColumn(grid.GetRowCount() - 1, "");
              c.VerticalAlignment = Cell.VerticalAlignmentType.Center;
              c.TextHeight = 11;
              if (enBut)
              {
                  c.cellStyle = EditStyle.esEllipsis;
                  c.ButtonText = "Таблиця";
                  grid.OnCellButtonClick += new Grid.CellButtonClickDelegate(TableButtonClick);
              }
          }
      }

      void grid_OnDataValidate(Cell cell)
      {
          if (cell.Key == "menCount")
          {
              int cnt = ConvertType.ToInt32(cell.Value, IM.NullI);
              if (cnt == IM.NullI || cnt < 0)
              {
                  cell.Value = "1";
                  MessageBox.Show("Невірно введена кількість людей!", "Помилка введення даних", MessageBoxButtons.OK, MessageBoxIcon.Error);
              }
          }
          if (cell.row == gridComiss.rowList[1] && (cell.Col == 1 || cell.Col == 2))
          {
              int people = ConvertType.ToInt32(gridComiss.GetCell(1, 1).Value, IM.NullI);
              double hours = ConvertType.ToDouble(gridComiss.GetCell(1, 2).Value, IM.NullD);
              if (people != IM.NullI && hours != IM.NullD)
              {
                  gridComiss.GetCell(1, 3).Value = (people * hours).ToString();
              }
          }
      }

      void TableButtonClick(Cell cell)
      {
          // для работ УРЗП
          if (cell == null)
          {
              drvMemo.ShowTable(WorkType.work14, null);
          }
          else
          {
              if (cell.row.grid == gridParamRez)
              {
                  drvMemo.ShowTable(WorkType.work15, gridParamRez);
                  foreach (Work w in drvMemo.works)
                      if (w.type == WorkType.work15)
                      {
                          Grid grid = gridParamRez;
                          grid.GetCell((grid.GetRowCount() - 1), 0).Value = w.article;
                          grid.GetCell((grid.GetRowCount() - 1), 1).Value = w.count.ToString();
                          grid.GetCell((grid.GetRowCount() - 1), 2).Value = w.index.ToString();
                          break;
                      }
              }
          }
          // старый код, для работ УРЧП
          if (cell == null || cell.Key != CnKoef)
             return;
         if (cell.row.grid == grid3)
         {
            List<string> selArts = new List<string>();
             /*
            foreach (articlesList list in articles)
            {
               if (list.type == WorkType.work3)
               {
                  selArts = list.articles;
                  break;
               }
            }
            */
            foreach (articlesList list in articles_Only_Work3)
            {
                if (list.type == WorkType.work3)
                {
                    selArts = list.articles;
                    break;
                }
            }
           articlesList L_M =  articles_Only_Work3.Find(r => r.type == WorkType.work3);
           drvMemo.ShowTable(WorkType.work3, grid3, selArts, L_M.List_default, arts_ext_rrz, cell.row.grid.GetCellFromKey(CnArticle).Value);

            //drvMemo.ShowTable(WorkType.work3, grid3, selArts);
            int count = 0;
            string article = "";
            double koef = IM.NullD;
            foreach (Work w in drvMemo.works)
            {
                if (w.type == WorkType.work3)
                {
                    count += w.count;
                    if (string.IsNullOrEmpty(article))
                        article = w.article;
                    if (koef == IM.NullD)
                        koef = w.index;
                }

            }
            cell.row.grid.GetCellFromKey(CnCount).Value = count.ToString();
            cell.row.grid.GetCellFromKey(CnArticle).Value = article;
            cell.row.grid.GetCellFromKey(CnKoef).Value = koef.ToStringNullD();
         }
         else if (cell.row.grid == grid4)
         {
            List<string> selArts = new List<string>();
            foreach (articlesList list in articles)
            {
               if (list.type == WorkType.work4)
               {
                  selArts = list.articles;
                  break;
               }
            }
            drvMemo.ShowTable(WorkType.work4, grid4, selArts);
            int count = 0;
            string article = "";
            double koef = IM.NullD;
            foreach (Work w in drvMemo.works)
            {
                if (w.type == WorkType.work4)
                {
                    count += w.count;
                    if (string.IsNullOrEmpty(article))
                        article = w.article;
                    if (koef == IM.NullD)
                        koef = w.index;
                }

            }
            cell.row.grid.GetCellFromKey(CnCount).Value = ProcessAnother(article)== 0 ? count.ToString() : ProcessAnother(article).ToString();
            cell.row.grid.GetCellFromKey(CnArticle).Value = article;
            cell.row.grid.GetCellFromKey(CnKoef).Value = koef.ToStringNullD();
         }
      }

      /// <summary>
      /// Инициализация всех гридов
      /// </summary>
      void InitGrids()
      {
          // общий заголовок
          InitGrid(grdHeader, false, pnHeader, WorkType.notype, false, true);
          // изначальные УРЧП
          InitGrid(grid1, false, panel1, WorkType.work1, true, false);
          InitGrid(grid2, false, panel2, WorkType.work2, false, true);
          InitGrid(grid3, true, panel3, WorkType.work3, false, false);
          InitGrid(grid4, true, panel4, WorkType.work4, false, false);
          InitGrid(grid5, false, panel5, WorkType.work5, false, false);
          InitGrid(grid6, false, panel6, WorkType.work6, false, false);
          InitGrid(grid7, false, panel7, WorkType.work7, true, true);
          InitGrid(grid8, false, panel8, WorkType.other, false, true);

          // добавленные УРЗП
         InitGrid(gridParamRez, true, pnTblParamRez, WorkType.work15, false);
         InitGrid(gridComiss, false, pnTblComiss, WorkType.work19, true);
         InitGrid(gridOther, false, pnTblOther, WorkType.work52, true);
      }



      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      private bool GetStatusDocDozv()
      {
          tpx_docs = TypeDocs.unknown;
          L_tpx_docs = new List<TypeDocs>();
          bool isSuccess_ = isContainDozv();
          if ((!isSuccess_) && (L_tpx_docs.Contains(TypeDocs.dozvil)))
          {
              foreach (articlesList list in articles)
              {
                  if (list.type == WorkType.work2)
                  {
                      if (list.articles.Count > 0)
                      {
                          string Art = list.articles[0];
                          if (Art.Contains("7.") || (Art.Contains("9.")) || (Art.Contains("40.")))
                          {
                              isSuccess_ = false;
                          }
                      }
                  }
              }
          }
          else if ((!isSuccess_) && (tpx_docs == TypeDocs.unknown))
          {
              isSuccess_ = false;
          }
          else if ((isSuccess_) && (L_tpx_docs.Contains(TypeDocs.dozvil)))
          {
                  foreach (articlesList list in articles)
                  {
                      if (list.type == WorkType.work2)
                      {
                          if (list.articles.Count > 0)
                          {
                              string Art = list.articles[0];
                              if ((Art.Contains("7.") || (Art.Contains("9.")) || (Art.Contains("40."))) == false)
                              {
                                  isSuccess_ = false;
                              }
                          }
                      }
                  }
          }
          else if ((isSuccess_) && (!L_tpx_docs.Contains(TypeDocs.dozvil)))
          {
              isSuccess_ = false;
          }


          
          /*
          else if ((isSuccess_) && (tpx_docs == TypeDocs.vysnovok))
          {
              foreach (articlesList list in articles)
              {
                  if (list.type == WorkType.work2)
                  {
                      if (list.articles.Count > 0)
                      {
                          string Art = list.articles[0];
                          if (Art.Contains("5.")==false)
                          {
                              isSuccess_ = false;
                          }
                      }
                  }
              }
          }
           */ 

          return isSuccess_;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      private bool GetStatusDocVisn()
      {
          tpx_docs = TypeDocs.unknown;
          L_tpx_docs = new List<TypeDocs>();
          bool isSuccess_ = isContainDozv();
          if ((!isSuccess_) && (L_tpx_docs.Contains(TypeDocs.vysnovok)))
          {
              foreach (articlesList list in articles)
              {
                  if (list.type == WorkType.work6)
                  {
                      if (list.articles.Count > 0)
                      {
                          string Art = list.articles[0];
                          if (Art.Contains("5."))
                          {
                              isSuccess_ = false;
                          }
                      }
                  }
              }
          }
          else if ((!isSuccess_) && (tpx_docs == TypeDocs.unknown))
          {
              isSuccess_ = false;
          }
          else if ((isSuccess_) && L_tpx_docs.Contains(TypeDocs.vysnovok))
          {
              foreach (articlesList list in articles)
              {
                  if (list.type == WorkType.work6)
                  {
                      if (list.articles.Count > 0)
                      {
                          string Art = list.articles[0];
                          if (Art.Contains("5.") == false)
                          {
                              isSuccess_ = false;
                          }
                      }
                  }
              }
          }
          else if ((isSuccess_) && (!L_tpx_docs.Contains(TypeDocs.vysnovok)))
          {
              isSuccess_ = false;
          }
              
          

          return isSuccess_;
      }

      /// <summary>
      /// Клик по 2му чекбоксу
      /// </summary>
      private void chb2_CheckedChanged(object sender, EventArgs e)
      {
         if (chb2.Checked) {
            bool isSuccess_Dozv = GetStatusDocDozv();
            if (isSuccess_Dozv) {
               panel2.Visible = true;
            }
            if (!isSuccess_Dozv) {
                MessageBox.Show("Не для всіх обраних заявок створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                chb2.Checked = false;
            }
            
         }
         else {
            panel2.Visible = false;
         }

      }

      /// <summary>
      /// Клик по 1му чекбоксу
      /// </summary>
      private void chb1_CheckedChanged(object sender, EventArgs e)
      {
         if(chb1.Checked)
         {
       /*     if (!drvMemo.hasVisn)
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb1.Checked = false;
            }
            else
            {*/
               panel1.Visible = true;
               if (chb7.Enabled)
                  chb7.Checked = false;
         //   }
         }
         else
            panel1.Visible = false;
      }

      /// <summary>
      /// Клик по 3му чекбоксу
      /// </summary>
      private void chb3_CheckedChanged(object sender, EventArgs e)
      {
         if (chb3.Checked)
         {
         /*   if (!drvMemo.hasVisn)
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb3.Checked = false;
            }
            else
            {*/
               panel3.Visible = true;
               if (chb4.Enabled)
                  chb4.Checked = false;
           // }
         }
         else
         {
            panel3.Visible = false;

            for (int i = 0; i < drvMemo.works.Count; ++i )
               if (drvMemo.works[i].type == WorkType.work3)
                  drvMemo.works.RemoveAt(i--);
         }
      }

      /// <summary>
      /// Клик по 4му чекбоксу
      /// </summary>
      private void chb4_CheckedChanged(object sender, EventArgs e)
      {
         if (chb4.Checked)
         {
           /* if (!drvMemo.hasVisn)
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb4.Checked = false;
            }
            else
            {*/
               panel4.Visible = true;
               if (chb3.Enabled)
                  chb3.Checked = false;
           // }
         }
         else
         {
            panel4.Visible = false;
            for (int i = 0; i < drvMemo.works.Count; ++i)
               if (drvMemo.works[i].type == WorkType.work4)
                  drvMemo.works.RemoveAt(i--);
         }
      }

      /// <summary>
      /// Клик по 5му чекбоксу
      /// </summary>
      private void chb5_CheckedChanged(object sender, EventArgs e)
      {
         if (chb5.Checked)
         {
          /*  if (!drvMemo.hasVisn)
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb5.Checked = false;
            }
            else
            {*/
               panel5.Visible = true;
           // }
         }
         else
            panel5.Visible = false;
      }

      /// <summary>
      /// Клик по 6му чекбоксу
      /// </summary>
      private void chb6_CheckedChanged(object sender, EventArgs e)
      {
          if (chb6.Checked)
          {
              bool isSuccess_Visn = GetStatusDocVisn();
              if (isSuccess_Visn)
              {
                  panel6.Visible = true;
              }
              if ((!isSuccess_Visn) || (!drvMemo.hasVisn))
              {
                  MessageBox.Show("Не для всіх обраних заявок створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                  chb6.Checked = false;
              }
          }
          else
          {
              panel6.Visible = false;
          }
      }

      /// <summary>
      /// Клик по 7му чекбоксу
      /// </summary>
      private void chb7_CheckedChanged(object sender, EventArgs e)
      {
         if (chb7.Checked)
         {
          /*  if ((drvMemo.applType == AppType.AppR2 && !drvMemo.hasDozv) || (drvMemo.applType != AppType.AppR2 && (!drvMemo.hasDozv || !drvMemo.hasVisn)))
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb7.Checked = false;
            }
            else
            {*/
               panel7.Visible = true;
               if (chb1.Enabled)
                  chb1.Checked = false;
          //  }
         }
         else
            panel7.Visible = false;
      }

      /// <summary>
      /// Клик по 8му чекбоксу
      /// </summary>
      private void chb8_CheckedChanged(object sender, EventArgs e)
      {
         if (chb8.Checked)
         {
         /*   if ((drvMemo.applType == AppType.AppR2 && !drvMemo.hasDozv) || (drvMemo.applType != AppType.AppR2 && !drvMemo.hasVisn))
            {
               MessageBox.Show("Не для всіх обраних заявках створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               chb8.Checked = false;
            }
            else
            {*/
               panel8.Visible = true;
         //   }
         }
         else
            panel8.Visible = false;
      }

      private void chbParamRez_CheckedChanged(object sender, EventArgs e)
      {
          if (chbParamRez.Checked)
          {
              pnTblParamRez.Visible = true;
          }
          else
          {
              pnTblParamRez.Visible = false;
              for (int i = 0; i < drvMemo.works.Count; ++i)
                  if (drvMemo.works[i].type == WorkType.work15)
                      drvMemo.works.RemoveAt(i--);
          }
      }

      /// <summary>
      /// Клик по 1му чекбоксу
      /// </summary>
      private void chbPtk_CheckedChanged(object sender, EventArgs e)
      {
          if (chbPtk.Checked)
          {
              btPtk.Visible = true;
          }
          else
          {
              btPtk.Visible = false;
              for (int i = 0; i < drvMemo.works.Count; ++i)
                  if (drvMemo.works[i].type == WorkType.work14)
                      drvMemo.works.RemoveAt(i--);

          }
      }

      /// <summary>
      /// Клик по 3му чекбоксу
      /// </summary>
      private void chbComiss_CheckedChanged(object sender, EventArgs e)
      {
          if (chbComiss.Checked)
          {
              pnTblComiss.Visible = true;
          }
          else
          {
              pnTblComiss.Visible = false;
          }
      }

      /// <summary>
      /// Клик по 4му чекбоксу
      /// </summary>
      private void chbOther_CheckedChanged(object sender, EventArgs e)
      {
          if (chbOther.Checked)
          {
              pnTblOther.Visible = true;
          }
          else
          {
              pnTblOther.Visible = false;
          }
      }

      /// <summary>
      /// Клик по 5му чекбоксу
      /// </summary>
      private void chbOdo_CheckedChanged(object sender, EventArgs e)
      {
          if (chbOdo.Checked)
              tbOdo.Enabled = true;
          else
          {
              tbOdo.Enabled = false;
              tbOdo.Text = "";
          }
      }

      /// <summary>
      /// Загрузка заявки
      /// </summary>
      public void LoadApp()
      {
          int countWork3 = 0;
          int countWork4 = 0;
          drvMemo.LoadApp();
          foreach (Work w in drvMemo.works)
          {
              switch (w.type)
              {
                  case WorkType.work1:
                      {
                          chb1.Checked = true;
                          grid1.GetCell(grid1.GetRowCount() - 1, 0).Value = w.article;
                          grid1.GetCell(grid1.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid1.GetCell(grid1.GetRowCount() - 1, 2).Value = w.index.ToString();
                          // grid1.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work2:
                      {
                          chb2.Checked = true;
                          grid2.GetCell(grid2.GetRowCount() - 1, 0).Value = w.article;
                          grid2.GetCell(grid2.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid2.GetCell(grid2.GetRowCount() - 1, 2).Value = w.index.ToString();
                          // grid2.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work3:
                      {
                          countWork3 += ProcessAnother(w.article) == 0 ? w.count : ProcessAnother(w.article);
                          chb3.Checked = true;
                          grid3.GetCell(grid3.GetRowCount() - 1, 0).Value = w.article;
                          grid3.GetCell(grid3.GetRowCount() - 1, 1).Value = countWork3.ToString();
                          Cell c = grid3.GetCell(grid3.GetRowCount() - 1, 2);
                          c.Value = w.index.ToString();
                      } break;
                  case WorkType.work4:
                      {
                          countWork4 += ProcessAnother(w.article) == 0 ? w.count : ProcessAnother(w.article);
                          chb4.Checked = true;
                          grid4.GetCell(grid4.GetRowCount() - 1, 0).Value = w.article;
                          grid4.GetCell(grid4.GetRowCount() - 1, 1).Value = countWork4.ToString();
                          Cell c = grid4.GetCell(grid4.GetRowCount() - 1, 2);
                          c.Value = w.index.ToString();
                      } break;
                  case WorkType.work5:
                      {
                          chb5.Checked = true;
                          grid5.GetCell(grid5.GetRowCount() - 1, 0).Value = w.article;
                          grid5.GetCell(grid5.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid5.GetCell(grid5.GetRowCount() - 1, 2).Value = w.index.ToString();
                          //   grid5.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work6:
                      {
                          chb6.Checked = true;
                          grid6.GetCell(grid6.GetRowCount() - 1, 0).Value = w.article;
                          grid6.GetCell(grid6.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid6.GetCell(grid6.GetRowCount() - 1, 2).Value = w.index.ToString();
                          //   grid6.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work7:
                      {
                          chb7.Checked = true;
                          grid7.GetCell(grid7.GetRowCount() - 1, 0).Value = w.article;
                          grid7.GetCell(grid7.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid7.GetCell(grid7.GetRowCount() - 1, 2).Value = w.index.ToString();
                          //   grid7.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.other:
                      {
                          chb8.Checked = true;
                          grid8.GetCell(grid8.GetRowCount() - 1, 0).Value = w.article;
                          grid8.GetCell(grid8.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          grid8.GetCell(grid8.GetRowCount() - 1, 2).Value = w.index.ToString();
                          //   grid8.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work14:
                      {
                          chbPtk.Checked = true;
                      } break;
                  case WorkType.work15:
                      {
                          chbParamRez.Checked = true;
                          gridParamRez.GetCell(gridParamRez.GetRowCount() - 1, 0).Value = w.article;
                          gridParamRez.GetCell(gridParamRez.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          gridParamRez.GetCell(gridParamRez.GetRowCount() - 1, 2).Value = w.index.ToString();
                          // grid2.GetCell(grid.GetRowCount() - 1, 2).Value = w.notes;
                      } break;
                  case WorkType.work19:
                      {
                          chbComiss.Checked = true;
                          gridComiss.GetCell(gridComiss.GetRowCount() - 1, 0).Value = w.article;
                          gridComiss.GetCell(gridComiss.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          gridComiss.GetCell(gridComiss.GetRowCount() - 1, 2).Value = w.index.ToString();
                          gridComiss.GetCell(gridComiss.GetRowCount() - 1, 3).Value = ((ProcessAnother(w.article) == 0 ? w.count * w.index : ProcessAnother(w.article))).ToString();
                      } break;
                  case WorkType.work52:
                      {
                          chbOther.Checked = true;
                          gridOther.GetCell(gridOther.GetRowCount() - 1, 0).Value = w.article;
                          gridOther.GetCell(gridOther.GetRowCount() - 1, 1).Value = ProcessAnother(w.article) == 0 ? w.count.ToString() : ProcessAnother(w.article).ToString();
                          gridOther.GetCell(gridOther.GetRowCount() - 1, 2).Value = w.index.ToString();
                      } break;
                  case WorkType.work50:
                      {
                          chbOdo.Checked = true;
                          tbOdo.Text = w.index.ToString();
                      } break;
                  default: break;
              }
          }
      }


      /// <summary>
      /// Сохраненние заявки
      /// </summary>
      public void SaveApp()
      {
         Work w = new Work();
         
         for (int i = 0; i < drvMemo.works.Count; ++i)
            if (drvMemo.works[i].type != WorkType.work3 && drvMemo.works[i].type != WorkType.work4
                && drvMemo.works[i].type != WorkType.work14 && drvMemo.works[i].type != WorkType.work15)
            {
               drvMemo.works.RemoveAt(i);
               i--;
            }
         if (chb1.Checked && chb1.Enabled)
         {            
            w.type = WorkType.work1;
            w.article = grid1.GetCell(grid1.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid1.GetCell(grid1.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid1.GetCell(grid1.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid1.GetCell(grid1.GetRowCount() - 1, 2).Value.ToString(), 1);//Convert.ToDouble(grid1.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
         //   w.notes = grid1.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chb2.Checked && chb2.Enabled)
         {            
            w = new Work();
            w.type = WorkType.work2;
            w.article = grid2.GetCell(grid2.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid2.GetCell(grid2.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid2.GetCell(grid2.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid2.GetCell(grid2.GetRowCount() - 1, 2).Value.ToString(), 1);//Convert.ToDouble(grid2.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
         //   w.notes = grid2.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chb5.Checked && chb5.Enabled)
         {
            w = new Work();
            w.type = WorkType.work5;
            w.article = grid5.GetCell(grid5.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid5.GetCell(grid5.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid5.GetCell(grid5.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid5.GetCell(grid5.GetRowCount() - 1, 2).Value.ToString(), 1); //Convert.ToDouble(grid5.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
         //   w.notes = grid5.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chb6.Checked && chb6.Enabled)
         {
            w = new Work();
            w.type = WorkType.work6;
            w.article = grid6.GetCell(grid6.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid6.GetCell(grid6.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid6.GetCell(grid6.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid6.GetCell(grid6.GetRowCount() - 1, 2).Value.ToString(), 1); //Convert.ToDouble(grid6.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
         //   w.notes = grid6.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chb7.Checked && chb7.Enabled)
         {
            w = new Work();
            w.type = WorkType.work7;
            w.article = grid7.GetCell(grid7.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid7.GetCell(grid7.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid7.GetCell(grid7.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid7.GetCell(grid7.GetRowCount() - 1, 2).Value.ToString(), 1);//Convert.ToDouble(grid7.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
         //   w.notes = grid7.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }

         if (chb8.Checked && chb8.Enabled)
         {
            w = new Work();
            w.type = WorkType.other;
            w.article = grid8.GetCell(grid8.GetRowCount() - 1, 0).Value.ToString();
            w.count = Convert.ToInt32(grid8.GetCell(grid8.GetRowCount() - 1, 1).Value.ToString());
            w.index = grid8.GetCell(grid8.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(grid8.GetCell(grid8.GetRowCount() - 1, 2).Value.ToString(), 1);//Convert.ToDouble(grid8.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
        //    w.notes = grid8.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
            drvMemo.works.Add(w);
         }
         if (!chb3.Checked)
         {
            for (int i = 0; i < drvMemo.works.Count; ++i)
               if (drvMemo.works[i].type == WorkType.work3)
                  drvMemo.works.RemoveAt(i);
         }
         if (!chb4.Checked)
         {
            for (int i = 0; i < drvMemo.works.Count; ++i)
               if (drvMemo.works[i].type == WorkType.work4)
                  drvMemo.works.RemoveAt(i);
         }

         if (chbComiss.Checked && chbComiss.Enabled)
         {
             w = new Work();
             w.type = WorkType.work19;
             w.article = gridComiss.GetCell(gridComiss.GetRowCount() - 1, 0).Value.ToString();
             w.count = Convert.ToInt32(gridComiss.GetCell(gridComiss.GetRowCount() - 1, 1).Value.ToString());
             w.index = gridComiss.GetCell(gridComiss.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(gridComiss.GetCell(gridComiss.GetRowCount() - 1, 2).Value.ToString(), 1); //Convert.ToDouble(grid5.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
             //   w.notes = grid5.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
             drvMemo.works.Add(w);
         }

         if (chbOther.Checked && chbOther.Enabled)
         {
             w = new Work();
             w.type = WorkType.work52;
             w.article = gridOther.GetCell(gridOther.GetRowCount() - 1, 0).Value.ToString();
             w.count = Convert.ToInt32(gridOther.GetCell(gridOther.GetRowCount() - 1, 1).Value.ToString());
             w.index = gridOther.GetCell(gridOther.GetRowCount() - 1, 2).Value == "" ? 1 : ConvertType.ToDouble(gridOther.GetCell(gridOther.GetRowCount() - 1, 2).Value.ToString(), 1); //Convert.ToDouble(grid6.GetCell(grid.GetRowCount() - 1, 2).Value.ToString());
             //   w.notes = grid6.GetCell(grid.GetRowCount() - 1, 2).Value.ToString();
             drvMemo.works.Add(w);
         }

         if (chbOdo.Checked && chbOdo.Enabled)
         {
             w = new Work();
             w.type = WorkType.work50;
             w.index = Convert.ToDouble(tbOdo.Text);
             drvMemo.works.Add(w);
         }

         if (!chbPtk.Checked)
         {
             for (int i = 0; i < drvMemo.works.Count; ++i)
                 if (drvMemo.works[i].type == WorkType.work14)
                     drvMemo.works.RemoveAt(i);
         }
         if (!chbParamRez.Checked)
         {
             for (int i = 0; i < drvMemo.works.Count; ++i)
                 if (drvMemo.works[i].type == WorkType.work15)
                     drvMemo.works.RemoveAt(i);
         }

         drvMemo.SaveApp();
      }

      /// <summary>
      /// Удаление заявки
      /// </summary>
      private void Delete_Click(object sender, EventArgs e)
      {
         //LoadApp();
         if (drvMemo.DeleteApp())
            this.DialogResult = DialogResult.Cancel;            
      }

      /// <summary>
      /// Отправление заявки в ДРВ
      /// </summary>
      private void ToDRV_Click(object sender, EventArgs e)
      {
         if (MessageBox.Show("Відправити службову записку до ДРВ?", "Підтвердження відправки", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
         {
            drvMemo.AppToDRV();             
            this.DialogResult = DialogResult.OK;
         }
      }

      /// <summary>
      /// Нажатие на кнопку "Печать"
      /// </summary>
      private void Print_Click(object sender, EventArgs e)
      {
          if ((GetStatusDocDozv() && (chb2.Checked)) || (GetStatusDocVisn() && (chb6.Checked)))
          {
              SaveApp();
              drvMemo.PrintDoc();
              btDelete.Visible = true;
          }
          else 
          {
              if (((chb2.Checked) && (!GetStatusDocDozv())) || ((chb6.Checked) && (!GetStatusDocVisn())))
                MessageBox.Show("Не для всіх обраних заявок створені необхідні потрібні документи!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
              else
              {
                  SaveApp();
                  drvMemo.PrintDoc();
                  btDelete.Visible = true;
              }
          }
      }

      private void btPtk_Click(object sender, EventArgs e)
      {
          TableButtonClick(null);
      }

      /// <summary>
      /// Нажатие на кнопку аннулирования заявки
      /// </summary>
      private void btCancel_Click(object sender, EventArgs e)
      {
         if (drvMemo.CancelApp())
            this.DialogResult = DialogResult.Cancel;
      }

      private void tbOwner_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.KeyCode == Keys.Enter)
         {
            drvMemo.SelectOwner(tbOwner.Text);
            tbOwner.Text = drvMemo.OwnerName;
         }
      }
   }
    public enum TypeDocs
    {
        dozvil,
        vysnovok,
        unknown
    }
    public enum TypeConstraint
    {
        FL,
        MODERN
    }

    /// <summary>
    /// 
    /// </summary>
    public class ExtArticles
    {
        public string NameArt { get; set; }
        public int IDArt { get; set; }
        public double LimitMin { get; set; }
        public double LimitMax { get; set; }
        public bool isBaseArticle { get; set; }

        public ExtArticles(string NameArt_, int IDArt_, double LimitMin_, double LimitMax_, bool isBaseArticle_)
        {
            NameArt = NameArt_;
            IDArt = IDArt_;
            LimitMin = LimitMin_;
            LimitMax = LimitMax_;
            isBaseArticle = isBaseArticle_;
        }
    }
}
