﻿using System;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET.HelpClasses;
using System.Collections.Generic;
using System.Linq;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   // Состояния пакета
   public enum StatePacket
   {
      UNKNOWN = 0,       //Неизвестно
      SentToUrcp,        //Отправлено в УРЧП
   }
   //======================================================
   // Коди управлений в БД ICSM (TASKWORCE)
   public enum UcrfDepartment
   {
      UNKNOWN = 0,       //Неизвестно
      URCP = (1 << 0),   // УРЧП
      URZP = (1 << 1),   // УРЗП
      URCM = (1 << 2),   // УРЧМ
      UAMV = (1 << 3),   // УАМВ
      Branch = (1 << 4),   // Филиал
   }
   /// <summary>
   /// Разширение класса ManagementUdcr
   /// </summary>
   internal static class UcrfDepartmentHepler
   {
       //===================================================
       /// <summary>
       /// Преобразовывает ManagementUdcr в строку
       /// </summary>
       /// <param name="value">ManagementUdcr</param>
       /// <returns>строковое значение ManagementUdcr</returns>
       public static string ToText(this UcrfDepartment value)
       {
           switch (value)
           {
               case UcrfDepartment.UNKNOWN:
                   return "Невідомий";
               case UcrfDepartment.UAMV:
                   return "УАМВ";
               case UcrfDepartment.URCM:
                   return "УРЧМ";
               case UcrfDepartment.URCP:
                   return "УРЧП";
               case UcrfDepartment.URZP:
                   return "УРЗП";
               case UcrfDepartment.Branch:
                   return "Філія";
           }
           return "Error Management";
       }
       //===================================================
       /// <summary>
       /// Преобразовывает строку в ManagementUdcr
       /// </summary>
       /// <param name="value">строка</param>
       /// <returns>ManagementUdcr</returns>
       public static UcrfDepartment ToUcrfDepartment(this string value)
       {
           UcrfDepartment management;
           try
           {
               management = (UcrfDepartment)Enum.Parse(typeof(UcrfDepartment), value);
           }
           catch
           {
               management = UcrfDepartment.UNKNOWN;
           }
           return management;
       }
   }
   //======================================================
   internal class CUsers
   {

       static bool status = false;
       /// <summary>
       /// Возвращает логин текущего пользователя
       /// </summary>
       /// <returns>логин текущего пользователя</returns>
       public static string ConnectedUser()
       {
           string curUser = IM.ConnectedUser();
           if (string.IsNullOrEmpty(curUser))
               curUser = HelpClasses.PluginSetting.PluginFolderSetting.UserByDefault;
           return curUser;
       }

       private static UcrfDepartment _curDepartment = UcrfDepartment.UNKNOWN;
       public static UcrfDepartment CurDepartment { get { return _curDepartment; } set { _curDepartment = value; SaveLastDepartment(value);  } }

      private const string DefDeptParamPrefix = "DEF_MANAG_";
      /// <summary>
      /// Возвращает 32 для пустого кода области.
      /// </summary>
      /// <returns>Код области текущего пользователя</returns>
      static public string GetUserAreaCode()
      {
           return GetUserAreaCode(GetCurUserID());
      }
      /// <summary>
      /// Возвращает 00 для пустого кода области.
      /// </summary>
      /// <returns>Код области пользователя по ID</returns>
      static public string GetUserAreaCode(int userId)
      {          
          string areaCode = "00";
          using (Icsm.LisRecordSet rsEmployee = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
          {
              rsEmployee.Select("ID,PROVINCE,CITY");
              rsEmployee.SetWhere("ID", IMRecordset.Operation.Eq, userId);
              rsEmployee.Open();
              if (!rsEmployee.IsEOF())
              {
                  try
                  {
                      areaCode = HelpClasses.HelpFunction.getAreaCode(rsEmployee.GetS("PROVINCE"),
                                                                      rsEmployee.GetS("CITY"));
                  }
                  catch
                  {
                      areaCode = "00";
                  }
              }
          }
          return areaCode;
      }
      /// <summary>
      /// Возвращает области.
      /// </summary>
      /// <returns>Области текущего пользователя</returns>
      static public string GetUserProvince()
      {
          return GetUserProvince(GetCurUserID());
      }
      /// <summary>
      /// Возвращает области.
      /// </summary>
      /// <returns>Области текущего пользователя</returns>
      static public List<string> GetSubProvinceUser()
      {
          string GetProvUser = GetUserProvince(GetCurUserID());
          GetSubProvinceOnUser(GetProvUser);
          return ((status == true && !string.IsNullOrEmpty(GetProvUser))  ? GetSubProvinceOnUser(GetProvUser) : ((string.IsNullOrEmpty(GetProvUser) || (GetProvUser.Trim()=="Київ") || (GetProvUser.Trim()=="Київська")) ? GetSubProvinceOnUser():GetSubProvinceOnUser(true,GetProvUser)));
      }

      /// <summary>
      /// Возвращает области.
      /// </summary>
      /// <returns>Области пользователя по ID</returns>
      static public string GetUserProvince(int userId)
      {
          string province = "";
          using (Icsm.LisRecordSet rsEmployee = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
          {
              rsEmployee.Select("ID,PROVINCE");
              rsEmployee.SetWhere("ID", IMRecordset.Operation.Eq, userId);
              rsEmployee.Open();
              if (!rsEmployee.IsEOF())
                  province = rsEmployee.GetS("PROVINCE");
          }
          return province;
      }



      /// <summary>
      /// Возвращает список всех областей.
      /// </summary>

      static public List<string> GetSubProvinceOnUser(bool status_tx, string NameProvince)
      {
          List<string> province = new List<string>();
          province.Clear();

          using (Icsm.LisRecordSet rsArea = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
          {
              rsArea.Select("ID,NAME");
              rsArea.OrderBy("NAME", OrderDirection.Ascending);
              if (status_tx) rsArea.SetWhere("NAME", IMRecordset.Operation.Eq, NameProvince.Trim());
              for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
              {
                  province.Add(rsArea.GetS("NAME"));
              }

          }
          return province;
      }

        /// <summary>
      /// Возвращает список всех областей.
      /// </summary>
     
      static public List<string> GetSubProvinceOnUser()
      {
          List<string> province = new List<string>();
          province.Clear();
          
          using (Icsm.LisRecordSet rsArea = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
          {
                  rsArea.Select("ID,NAME");
                  rsArea.OrderBy("NAME", OrderDirection.Ascending);
                  for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                  {
                      province.Add(rsArea.GetS("NAME")); 
                  }
            
          }
          return province;
      }

      /// <summary>
      /// Возвращает список объединенных областей.
      /// </summary>
      /// <returns>Области пользователя по ID</returns>
      static public List<string> GetSubProvinceOnUser(string NameProvince)
      {
          List<string> province = new List<string>();
          province.Clear();
          int ID=IM.NullI;
          int SPLIT_ID=IM.NullI;
          string NAME = "";
          using (Icsm.LisRecordSet rsArea = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
          {
              rsArea.Select("ID,NAME,SPLIT_ID");
              rsArea.SetWhere("NAME", IMRecordset.Operation.Eq, NameProvince.Trim());
              rsArea.OrderBy("NAME",OrderDirection.Ascending);
              rsArea.Open();
              if (!rsArea.IsEOF())
              {
                 ID = rsArea.GetI("ID");
                 SPLIT_ID = rsArea.GetI("SPLIT_ID");
                 NAME = rsArea.GetS("NAME");
                 province.Add(NAME);
              }
          
          }

          status = false;

              using (Icsm.LisRecordSet rsAreaX = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
              {
                  rsAreaX.Select("ID,NAME,SPLIT_ID");
                  rsAreaX.SetWhere("SPLIT_ID", IMRecordset.Operation.Eq, SPLIT_ID);
                  rsAreaX.OrderBy("NAME", OrderDirection.Ascending);
                  for (rsAreaX.Open(); !rsAreaX.IsEOF(); rsAreaX.MoveNext())
                  {

                      if (!province.Contains(rsAreaX.GetS("NAME"))) { status = true; province.Add(rsAreaX.GetS("NAME")); }
                  }
              }

              using (Icsm.LisRecordSet rsAreaX = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
              {
                  rsAreaX.Select("ID,NAME,SPLIT_ID");
                  rsAreaX.OrderBy("NAME", OrderDirection.Ascending);
                  for (rsAreaX.Open(); !rsAreaX.IsEOF(); rsAreaX.MoveNext())
                  {
                      if ((rsAreaX.GetI("ID").Equals(SPLIT_ID)) || (rsAreaX.GetI("SPLIT_ID").Equals(ID)))
                      {
                          if (!province.Contains(rsAreaX.GetS("NAME"))) { status = true; province.Add(rsAreaX.GetS("NAME")); }
                      }
                  }
              }
          
                                
          return province;
      }

      static public DepartmentType GetUserDepartmentType()
      {
         string type = string.Empty;
         using (var rsEmployee = new Icsm.LisRecordSet(ICSMTbl.itblTskfMember, IMRecordset.Mode.ReadOnly))
         {
             rsEmployee.Select("Employee.TYPE");
             rsEmployee.SetWhere("Employee.APP_USER", IMRecordset.Operation.Like, ConnectedUser());
             rsEmployee.Open();
             if (!rsEmployee.IsEOF())
             {
                 type = rsEmployee.GetS("Employee.TYPE");
             }
         }
         if(PluginSetting.PluginFolderSetting.UseRegionFieldAsBranchAssign &&
             !string.IsNullOrEmpty(PluginSetting.PluginFolderSetting.BranchUserTypeCode) &&
             type == PluginSetting.PluginFolderSetting.BranchUserTypeCode)
         {
             return DepartmentType.FILIA;
         }
         try
         {
             return (DepartmentType) Enum.Parse(typeof (DepartmentType), type);
         }
         catch
         {
             return DepartmentType.Unknown;
         }
      }
      /// <summary>
      /// Возвращает ключ для стрики поиска последнего управления
      /// </summary>
      /// <returns>Ключ для поиска последнего управления</returns>
      static private string GetKeyUserManagement()
      {
          return DefDeptParamPrefix + ConnectedUser();
      }
      /// <summary>
      /// Сохраняет последнее управление
      /// </summary>
      /// <param name="curManagement">текущее управление</param>
      static private void SaveLastDepartment(UcrfDepartment curManagement)
      {
          if (curManagement != UcrfDepartment.UNKNOWN)
              HelpClasses.HelpFunction.WriteDataToSysConfig(GetKeyUserManagement(), curManagement.ToString());
      }
      /// <summary>
      /// Считывает последнее сохраненное управление
      /// </summary>
      /// <returns>последнее сохраненное управление</returns>
      static private UcrfDepartment ReadLastManagement()
      {
          UcrfDepartment lastManagement;
          try
          {
              string manag = HelpClasses.HelpFunction.ReadDataFromSysConfig(GetKeyUserManagement());
              lastManagement = manag.ToUcrfDepartment();
          }
          catch
          {
              lastManagement = UcrfDepartment.UNKNOWN;
          }
          return lastManagement;
      }
      //===================================================
      /// <summary>
      /// Возвращает текущее управление пользователя
      /// </summary>
      /// <returns>Возвращает текущее управление пользователя</returns>
      static public void InitUserDepartment()
      {
          int count = CountDepartments();
          if (count != 1)
          {
              //вытаскиваем последнее значение управления
              UcrfDepartment lastManagement = ReadLastManagement();
              if ((lastManagement == UcrfDepartment.UNKNOWN) || (((int)GetAllUserDepartments() & (int)lastManagement) != (int)lastManagement))
              {
                  //Нет сохраненого значения управлени или сохраненое значение не совпадает с текущим
                  //Выбераем управление
                  _curDepartment = Users.SelectUserDepartmentType.InitUserDepartment(GetCurDepartment());
                  SaveLastDepartment(_curDepartment);
              }
              else
                  _curDepartment = lastManagement;
          }
          else
              _curDepartment = GetAllUserDepartments();
      }
      //===================================================
      /// <summary>
      /// Изменить текущее управление пользователя
      /// </summary>
      static public void ChangeUserDepartment()
      {
          _curDepartment = Users.SelectUserDepartmentType.InitUserDepartment(GetCurDepartment());
          SaveLastDepartment(_curDepartment);
      }
      //===================================================
      /// <summary>
      /// Возвращает текущее управление (роль) пользователя
      /// </summary>
      /// <returns>Возвращает текущее управление (роль) пользователя</returns>
      static public UcrfDepartment GetCurDepartment()
      {
          return _curDepartment;
      }
      //===================================================
      /// <summary>
      /// Проверка управлений пользователя
      /// </summary>
      /// <param name="userManagement">Управления пользователя</param>
      /// <param name="testManagement">Проверить на управления</param>
      /// <returns>Возвращает управление пользователя</returns>
      static public bool IsUseManagement(UcrfDepartment userManagement, UcrfDepartment testManagement)
      {
          int manage = (int)userManagement;
          int test = (int)testManagement;
          return ((manage & test) == test);
      }
      //===================================================
      /// <summary>
      /// Возвращает кол-во управлений пользователя
      /// </summary>
      /// <returns>Возвращает кол-во управлений пользователя</returns>
      static public int CountDepartments()
      {
          UcrfDepartment allManagement = GetAllUserDepartments();
          int count = 0;
          if (IsUseManagement(allManagement, UcrfDepartment.URCP)) count++;
          if (IsUseManagement(allManagement, UcrfDepartment.URZP)) count++;
          if (IsUseManagement(allManagement, UcrfDepartment.URCM)) count++;
          if (IsUseManagement(allManagement, UcrfDepartment.UAMV)) count++;
          if (IsUseManagement(allManagement, UcrfDepartment.Branch)) count++;
          return count;
      }
      //===================================================
      /// <summary>
      /// Возвращает все управления пользователя
      /// </summary>
      /// <returns>Возвращает все управления пользователя</returns>
      static public UcrfDepartment GetAllUserDepartments()
      {
          return GetAllUserDepartments(ConnectedUser());
      }
      //===================================================
      /// <summary>
      /// Возвращает все управления пользователя
      /// </summary>
      /// <param name="userName">имя пользователя</param>
      /// <returns>Возвращает все управления пользователя</returns>
      static public UcrfDepartment GetAllUserDepartments(string userName)
      {
          UcrfDepartment retVal = UcrfDepartment.UNKNOWN;
          if (string.IsNullOrEmpty(userName))
              return retVal;
          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.itblTskfMember, IMRecordset.Mode.ReadOnly))
          {
              rs.Select("Employee.APP_USER,Taskforce.CUST_TXT4");
              rs.SetWhere("Employee.APP_USER", IMRecordset.Operation.Like, userName);
              for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
              {
                  try
                  {
                      string manag = rs.GetS("Taskforce.CUST_TXT4");
                      UcrfDepartment tmpManag = manag.ToUcrfDepartment();
                      retVal |= tmpManag;
                  }
                  catch
                  {
                      retVal = UcrfDepartment.UNKNOWN;
                  }
              }
          }
          return retVal;
      }
      //===================================================
      /// <summary>
      /// Текущий пользователь может редактировать пакет?
      /// </summary>
      /// <returns>TRUE - ДА, иначе FALSE</returns>
      static public bool CanReadPacket()
      {
         return IM.TableRight(PlugTbl.itblXnrfaPacket, IMTableRight.Select);
      }
      //===================================================
      /// <summary>
      /// Текущий пользователь может изменять пакет?
      /// </summary>
      /// <returns>TRUE - ДА, иначе FALSE</returns>
      static public bool CanUpdatePacket()
      {
         return (IM.TableRight(PlugTbl.itblXnrfaPacket) == IMTableRight.Update);
      }
      //===================================================
      /// <summary>
      /// Текущий пользователь может создавать новые пакеты?
      /// </summary>
      /// <returns>TRUE - ДА, иначе FALSE</returns>
      static public bool CanInsertPacket()
      {
         return (IM.TableRight(PlugTbl.itblXnrfaPacket) == IMTableRight.Insert);
      }
      //===================================================
      /// <summary>
      /// Текущий пользователь может удалять пакеты?
      /// </summary>
      /// <returns>TRUE - ДА, иначе FALSE</returns>
      static public bool CanDeletePacket()
      {
         return (IM.TableRight(PlugTbl.itblXnrfaPacket) == IMTableRight.Delete);
      }
      //==================================================
      /// <summary>
      /// Возвращает ID текущего пользователя
      /// </summary>
      /// <returns>Возвращает ID текущего пользователя</returns>
      static public int GetCurUserID()
      {
         int retVal = IM.NullI;
         string userName = ConnectedUser();
         using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
         {
             rs.Select("ID");
             rs.SetWhere("APP_USER", IMRecordset.Operation.Like, userName);
             rs.Open();
             if (!rs.IsEOF())
                 retVal = rs.GetI("ID");
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Возвращает ФИО текущего пользователя
      /// </summary>
      /// <returns>ФИО пользователя</returns>
      public static string GetUserFio()
      {
          return GetUserFio(ConnectedUser());
      }
      //===================================================
      /// <summary>
      /// Возвращает ФИО пользователя по ID
      /// </summary>
      /// <param name="id">ID пользователя</param>
      /// <returns>ФИО пользователя</returns>
      public static string GetUserFio(int id)
      {
          string login = "";
          using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
          {
              r.Select("ID,APP_USER");
              r.SetWhere("ID", IMRecordset.Operation.Eq, id);
              r.Open();
              if (!r.IsEOF())
                  login = r.GetS("APP_USER");
          }
          return GetUserFio(login);
      }

      //===================================================
      /// <summary>
      /// Возвращает логин пользователя по ID
      /// </summary>
      /// <param name="id">ID пользователя</param>
      /// <returns>ФИО пользователя</returns>
      public static string GetUserLoginById(int id)
      {
          string login = "";
          using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
          {
              r.Select("ID,APP_USER");
              r.SetWhere("ID", IMRecordset.Operation.Eq, id);
              r.Open();
              if (!r.IsEOF())
                  login = r.GetS("APP_USER");
          }
          return login;
      }
      //===================================================
      /// <summary>
      /// Возвращает ФИО пользователя по логину
      /// </summary>
      /// <param name="login">Логин пользователя</param>
      /// <returns>ФИО пользователя</returns>
      public static string GetUserFio(string login)
      {
          if (string.IsNullOrEmpty(login))
              return ""; //Нет пользователя

          string retVal = "";
          using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
          {
              r.Select("ID,LASTNAME,FIRSTNAME");
              r.SetWhere("APP_USER", IMRecordset.Operation.Eq, login);
              r.Open();
              if (!r.IsEOF())
                  retVal = string.Format("{0} {1}", r.GetS("LASTNAME"), r.GetS("FIRSTNAME"));
          }
          return retVal;
      }

      internal static string GetUserRegion(string userLogin)
      {
          string retval = "";
          IMRecordset rs = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
          try
          {
              rs.Select("ID,APP_USER,PROVINCE");
              rs.SetWhere("APP_USER", IMRecordset.Operation.Eq, userLogin);
              rs.Open();
              if (!rs.IsEOF())
                retval = rs.GetS("PROVINCE");
          }
          finally
          {
              rs.Destroy();
          }
          return retval;
      }
   }
}
