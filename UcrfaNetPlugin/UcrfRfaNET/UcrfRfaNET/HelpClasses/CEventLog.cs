﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.StationsStatus;
using OrmCs;
using XICSM.UcrfRfaNET.GlobalDB;

namespace XICSM.UcrfRfaNET
{
   class CEventLog
   {
      //============================================================
      /// <summary>
      /// Добавить собитие по заявке для документа
      /// </summary>
      /// <param name="events">текст событий</param>
      /// <param name="docDate">дата документа</param>
      /// <param name="docNumber">номер документа</param>
      /// <param name="docEndDate">конечная дата документа</param>
      /// <param name="path">путь к документа</param>
       public static void AddApplDocEvent(int applID, string docType, DateTime docDate, string docNumber, DateTime? docEndDate, string path)
      {
         EDocEvent events = EDocEvent.evUnknown;
         switch (docType)
         {
            case DocType._0159: events = EDocEvent.ev_0159; break;
            case DocType.DOZV: events = EDocEvent.evDOZV; break;
            case DocType.DRV:
                if (CUsers.GetCurDepartment() == UcrfDepartment.URCP) //#4867
                    events = EDocEvent.evDrvUrcp;
                 else if (CUsers.GetCurDepartment() == UcrfDepartment.URZP)
                     events = EDocEvent.evDrvUrzp;
                 else if (CUsers.GetCurDepartment() == UcrfDepartment.Branch)
                     events = EDocEvent.evBranchInvoice;
                 break;
            case DocType.FYLIA: events = EDocEvent.evFYLIA; break;
            case DocType.LYST: events = EDocEvent.evLYST; break;
            case DocType.PCR: events = EDocEvent.evPCR; break;
            case DocType.PCR2: events = EDocEvent.evPCR2; break;
            case DocType.VISN: events = EDocEvent.evVISN; break;
            case DocType.VVE: events = EDocEvent.evVVE; break;
            case DocType.VVP: events = EDocEvent.evVVP; break;
            case DocType.TEST_TV: events = EDocEvent.evTEST_TV; break;
            case DocType.TEST_NV: events = EDocEvent.evTEST_NV; break;
            case DocType.TEST_PTK: events = EDocEvent.evTEST_PTK; break;
            case DocType.TEST_PTK_NV: events = EDocEvent.evTEST_PTK_NV; break;
            case DocType.TEST_PCR2: events = EDocEvent.evTEST_PCR2; break;
            case DocType.LIST_FYLIA: events = EDocEvent.evLIST_FYLIA; break;
            case DocType.NOTICE_PTK: events = EDocEvent.evNOTICE_PTK; break;
            case DocType.URCM_PTK: events = EDocEvent.evNOTICE_PTK; break;
            case DocType.URCM_DOC: events = EDocEvent.evURCM_DOC; break;
            case DocType.AKT_PTK: events = EDocEvent.evAKT_PTK; break;
            case DocType.URCM_NOTICE: events = EDocEvent.evURCM_NOTICE; break;
            case DocType.REESTR_VID: events = EDocEvent.evREESTR_VID; break;
            case DocType.SELECTION_FREQ: events = EDocEvent.evSelectionFreqDoc; break;
            case DocType.LYST_ZRS_CALLSIGN : events = EDocEvent.evApplCallSign; break;
            case DocType.LYST_TR_CALLSIGN : events = EDocEvent.evApplCallSign; break;
            default: events = docType.ToEventType(); break;
         }
         AddApplEvent(applID, events, docDate, docNumber, docEndDate, path);
      }
       //============================================================
       /// <summary>
       /// Добавить собитие по заявке (перегруженная)
       /// </summary>
       /// <param name="events">текст событий</param>
       /// <param name="docDate">дата документа</param>
       /// <param name="docEndDate">конечная дата документа</param>
       public static void AddApplEvent(int applID, EDocEvent events, DateTime docDate, DateTime? docEndDate)
       {
           int EmployeeID = 0;
           IMRecordset rsEmployee = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
           rsEmployee.Select("ID,APP_USER");
           rsEmployee.SetWhere("APP_USER", IMRecordset.Operation.Like, IM.ConnectedUser());
           try
           {
               rsEmployee.Open();
               if (!rsEmployee.IsEOF())
                   EmployeeID = rsEmployee.GetI("ID");
           }
           finally
           {
               rsEmployee.Close();
               rsEmployee.Destroy();
           }

           AddEvent(PlugTbl.itblXnrfaAppl, applID, events, docDate, docEndDate, EmployeeID);
       }
       //============================================================
       /// <summary>
       /// Добавить собитие по заявке без изменения статуса
       /// </summary>
       /// <param name="events">текст событий</param>
       /// <param name="docDate">дата документа</param>
       /// <param name="docNumber">номер документа</param>
       /// <param name="docEndDate">конечная дата документа</param>
       /// <param name="path">путь к документа</param>
       public static void AddApplEventWithoutChangeStatus(int applID, EDocEvent events, DateTime docDate, string docNumber, DateTime? docEndDate, string path)
       {
           int EmployeeID = 0;
           IMRecordset rsEmployee = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
           rsEmployee.Select("ID,APP_USER");
           rsEmployee.SetWhere("APP_USER", IMRecordset.Operation.Like, IM.ConnectedUser());
           try
           {
               rsEmployee.Open();
               if (!rsEmployee.IsEOF())
                   EmployeeID = rsEmployee.GetI("ID");
           }
           finally
           {
               rsEmployee.Close();
               rsEmployee.Destroy();
           }

           AddEventWithoutChangeStatus(PlugTbl.itblXnrfaAppl, applID, events, docDate, docNumber, docEndDate, path, EmployeeID);
       }
      //============================================================
      /// <summary>
      /// Добавить собитие по заявке
      /// </summary>
      /// <param name="events">текст событий</param>
      /// <param name="docDate">дата документа</param>
      /// <param name="docNumber">номер документа</param>
      /// <param name="docEndDate">конечная дата документа</param>
      /// <param name="path">путь к документа</param>
      public static void AddApplEvent(int applID, EDocEvent events, DateTime docDate, string docNumber, DateTime? docEndDate, string path)
      {
         int EmployeeID = 0;
         IMRecordset rsEmployee = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
         rsEmployee.Select("ID,APP_USER");
         rsEmployee.SetWhere("APP_USER", IMRecordset.Operation.Like, IM.ConnectedUser());
         try
         {
            rsEmployee.Open();
            if (!rsEmployee.IsEOF())
               EmployeeID = rsEmployee.GetI("ID");
         }
         finally
         {
            rsEmployee.Close();
            rsEmployee.Destroy();
         }

         AddEvent(PlugTbl.itblXnrfaAppl, applID, events, docDate, docNumber, docEndDate, path, EmployeeID);
      }
      //============================================================
      /// <summary>
      /// Добавляем событие в журнал событий (перегруженная)
      /// </summary>
      /// <param name="ObjTable">имя таблицы, к которой прикрепить событие</param>
      /// <param name="ObjID">ID записи, к которой прикрепить событие</param>
      /// <param name="events">текст событий</param>
      /// <param name="docDate">дата документа</param>
     
      /// <param name="docEndDate">конечная дата документа</param>

      public static void AddEvent(string ObjTable, int ObjID, EDocEvent events, DateTime docDate,  DateTime? docEndDate, int employeeID)
      {
          if (employeeID == 0)
              return;

        

          if (ObjTable == PlugTbl.APPL)
          {// Изменяем статус при необходимости
              StationStatus wf = new StationStatus();
              StatusType curStatus = ApplSource.BaseAppClass.ReadStatus(ObjID);
              StatusType newStatus = wf.GetNewStatus(curStatus, events);
              if ((newStatus != curStatus)  && (EPacketType.PckSetCallSign != CGlobalXML.GetPacketType(ObjID)))
                  ApplSource.BaseAppClass.UpdateStatus(newStatus, ObjID);
          }
      }
      //============================================================
      /// <summary>
      /// Добавляем событие в журнал событий без обновления статуса
      /// </summary>
      /// <param name="ObjTable">имя таблицы, к которой прикрепить событие</param>
      /// <param name="ObjID">ID записи, к которой прикрепить событие</param>
      /// <param name="events">текст событий</param>
      /// <param name="docDate">дата документа</param>
      /// <param name="docNumber">номер документа</param>
      /// <param name="docEndDate">конечная дата документа</param>
      /// <param name="path">путь к документа</param>
      public static void AddEventWithoutChangeStatus(string ObjTable, int ObjID, EDocEvent events, DateTime docDate, string docNumber, DateTime? docEndDate, string path, int employeeID)
      {
          if (employeeID == 0)
              return;

          switch (events)
          {
              // Сюда помещаються типи документов, которые необходимо отображать в списке событий в единственном числе
              case EDocEvent.evAKT_PTK:
              case EDocEvent.evAKT_TE_PTK:
              case EDocEvent.evPARAM_REZ:
                  DisableEvent(events, ObjTable, ObjID, docNumber);
                  break;
          }

          IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadWrite);
          rsEvent.Select("ID,OBJ_ID,OBJ_TABLE,EVENT,EMPLOYEE_ID,CREATED_DATE,PATH,DOC_DATE,DOC_NUMBER,DOC_END_DATE");
          rsEvent.SetWhere("ID", IMRecordset.Operation.Eq, -1);
          try
          {

              rsEvent.Open();
              rsEvent.AddNew();
              rsEvent["ID"] = IM.AllocID(PlugTbl.itblXnrfaEventLog, 1, -1);
              rsEvent["OBJ_ID"] = ObjID;
              rsEvent["OBJ_TABLE"] = ObjTable;
              rsEvent["EVENT"] = (string)Enum.Format(typeof(EDocEvent), events, "F");
              rsEvent["CREATED_DATE"] = DateTime.Now;
              rsEvent["PATH"] = path;
              if (docDate != IM.NullT)
                  rsEvent["DOC_DATE"] = docDate;
              rsEvent["DOC_NUMBER"] = docNumber;
              if (docEndDate != IM.NullT)
                  rsEvent["DOC_END_DATE"] = docEndDate;
              rsEvent["EMPLOYEE_ID"] = employeeID;
              rsEvent.Update();
          }
          finally
          {
              rsEvent.Close();
              rsEvent.Destroy();
          }


      }
      //============================================================
      /// <summary>
      /// Добавляем событие в журнал событий
      /// </summary>
      /// <param name="ObjTable">имя таблицы, к которой прикрепить событие</param>
      /// <param name="ObjID">ID записи, к которой прикрепить событие</param>
      /// <param name="events">текст событий</param>
      /// <param name="docDate">дата документа</param>
      /// <param name="docNumber">номер документа</param>
      /// <param name="docEndDate">конечная дата документа</param>
      /// <param name="path">путь к документа</param>
      public static void AddEvent(string ObjTable, int ObjID, EDocEvent events, DateTime docDate, string docNumber, DateTime? docEndDate, string path, int employeeID)
      {
          try
          {
              if (employeeID == 0)
                  return;

              switch (events)
              {
                  // Сюда помещаються типи документов, которые необходимо отображать в списке событий в единственном числе
                  case EDocEvent.evAKT_PTK:
                  case EDocEvent.evAKT_TE_PTK:
                  case EDocEvent.evPARAM_REZ:
                      DisableEvent(events, ObjTable, ObjID, docNumber);
                      break;
              }

              YXnrfaEventLog log = new YXnrfaEventLog();
              log.Table = "XNRFA_EVENT_LOG";
              log.Format("*");
              log.New();
              log.AllocID();
              log.m_obj_id = ObjID;
              log.m_obj_table = ObjTable;
              log.m_event = (string)Enum.Format(typeof(EDocEvent), events, "F");
              log.m_created_date = DateTime.Now;
              log.m_path = path;
              if (docDate != IM.NullT)
                  log.m_doc_date = docDate;
              log.m_doc_number = docNumber;
              if (docEndDate != IM.NullT)
                  log.m_doc_end_date = docEndDate.GetValueOrDefault();
              log.m_employee_id = employeeID;
              log.Save();
              log.Close();
              log.Dispose();
              /*
             IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadWrite);
             rsEvent.Select("ID,OBJ_ID,OBJ_TABLE,EVENT,EMPLOYEE_ID,CREATED_DATE,PATH,DOC_DATE,DOC_NUMBER,DOC_END_DATE");
             rsEvent.SetWhere("ID", IMRecordset.Operation.Eq, -1);
             try
             {

                rsEvent.Open();
                rsEvent.AddNew();
                rsEvent["ID"] = IM.AllocID(PlugTbl.itblXnrfaEventLog, 1, -1);
                rsEvent["OBJ_ID"] = ObjID;
                rsEvent["OBJ_TABLE"] = ObjTable;
                rsEvent["EVENT"] = (string)Enum.Format(typeof(EDocEvent), events, "F");
                rsEvent["CREATED_DATE"] = DateTime.Now;
                rsEvent["PATH"] = path;
                if(docDate != IM.NullT)
                   rsEvent["DOC_DATE"] = docDate;
                rsEvent["DOC_NUMBER"] = docNumber;
                if (docEndDate != IM.NullT)
                   rsEvent["DOC_END_DATE"] = docEndDate;
                rsEvent["EMPLOYEE_ID"] = employeeID;
                rsEvent.Update();
             }
             finally
             {
                rsEvent.Close();
                rsEvent.Destroy();
             }
              */


              if (ObjTable == PlugTbl.APPL)
              {// Изменяем статус при необходимости
                  StationStatus wf = new StationStatus();
                  StatusType curStatus = ApplSource.BaseAppClass.ReadStatus(ObjID);
                  StatusType newStatus = wf.GetNewStatus(curStatus, events);
                  if ((newStatus != curStatus) && (EPacketType.PckSetCallSign != CGlobalXML.GetPacketType(ObjID)))
                      ApplSource.BaseAppClass.UpdateStatus(newStatus, ObjID);
              }
          }
          catch (Exception ex)
          {
              IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadWrite);
              rsEvent.Select("ID,OBJ_ID,OBJ_TABLE,EVENT,EMPLOYEE_ID,CREATED_DATE,PATH,DOC_DATE,DOC_NUMBER,DOC_END_DATE");
              rsEvent.SetWhere("ID", IMRecordset.Operation.Eq, -1);
              try
              {

                  rsEvent.Open();
                  rsEvent.AddNew();
                  rsEvent["ID"] = IM.AllocID(PlugTbl.itblXnrfaEventLog, 1, -1);
                  rsEvent["OBJ_ID"] = ObjID;
                  rsEvent["OBJ_TABLE"] = ObjTable;
                  rsEvent["EVENT"] = (string)Enum.Format(typeof(EDocEvent), events, "F");
                  rsEvent["CREATED_DATE"] = DateTime.Now;
                  rsEvent["PATH"] = path;
                  if (docDate != IM.NullT)
                      rsEvent["DOC_DATE"] = docDate;
                  rsEvent["DOC_NUMBER"] = docNumber;
                  if (docEndDate != IM.NullT)
                      rsEvent["DOC_END_DATE"] = docEndDate;
                  rsEvent["EMPLOYEE_ID"] = employeeID;
                  rsEvent.Update();
              }
              finally
              {
                  rsEvent.Close();
                  rsEvent.Destroy();
              }
          }
         
        
      }
      //===================================================
      /// <summary>
      /// Деактивируем ненужные записи
      /// </summary>
      /// <param name="events">Тип события</param>
      /// <param name="objTable">Имя таблицы</param>
      /// <param name="objId">ID записи</param>
      /// <param name="docNumber">Номер документа, если NULL, то не фильтрует по номеру документа</param>
      public static void DisableEvent(EDocEvent events, string objTable, int objId, string docNumber)
      {
          IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadWrite);
          rsEvent.Select("ID,VISIBLE");
          rsEvent.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, objId);
          rsEvent.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, objTable);
          rsEvent.SetWhere("EVENT", IMRecordset.Operation.Like, (string)Enum.Format(typeof(EDocEvent), events, "F"));
          rsEvent.SetWhere("VISIBLE", IMRecordset.Operation.Eq, 1);
          if(string.IsNullOrEmpty(docNumber) == false)
              rsEvent.SetWhere("DOC_NUMBER", IMRecordset.Operation.Like, docNumber);
          try
          {
              for (rsEvent.Open(); !rsEvent.IsEOF(); rsEvent.MoveNext())
              {
                   rsEvent.Edit();
                   rsEvent.Put("VISIBLE", 0);
                   rsEvent.Update();
              }
          }
          finally
          {
              if (rsEvent.IsOpen())
                  rsEvent.Close();
              rsEvent.Destroy();
          }
      }
  
   }
}
