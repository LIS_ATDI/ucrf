﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GridCtrl;
using ICSM;
using System.Globalization;

namespace XICSM.UcrfRfaNET
{
   static class ConvertType
   {
      //===================================================
      /// <summary>
      /// Конвертируем строку в координаты DMS
      /// </summary>
      /// <param name="strCoord">строковое представление координаты (без точек и запятых в формате GGMMSS)</param>
      /// <returns>координаты в формате DMS</returns>
      public static double StrToDMS(string strCoord)
      {
         strCoord = strCoord.ToUpper();
         double otherSide = (strCoord.IndexOf('W') > -1 || strCoord.IndexOf('S') > -1) ?
             -1.0 : 1.0;
         strCoord = strCoord.Replace("\xB0", "");
         strCoord = strCoord.Replace("\x27", "");
         strCoord = strCoord.Replace(" ", "");
         strCoord = strCoord.Replace("W", "");
         strCoord = strCoord.Replace("E", "");
         strCoord = strCoord.Replace("S", "");
         strCoord = strCoord.Replace("N", "");
         while (strCoord.Length < 6)
            strCoord = strCoord + "0";
         double tmpCoord = ToDouble(strCoord, IM.NullD) * otherSide;
         if (tmpCoord != IM.NullD)
            tmpCoord /= 10000.0;
         return tmpCoord;
      }
      //===================================================
      /// <summary>
      /// Конвертирует значение строки в int
      /// </summary>
      /// <param name="dblStr">строка</param>
      /// <param name="defVal">значение по умолчанию</param>
      /// <returns>int</returns>
      public static int ToInt32(string dblStr, int defVal)
      {
         int retVal = defVal;
         try { retVal = Convert.ToInt32(dblStr); }
         catch { };
         return retVal;
      }
      
      //===================================================
      /// <summary>
      /// Конвертирует значение ячейки в double
      /// </summary>
      /// <param name="cell">ячейка</param>
      /// <param name="defVal">значение по умолчанию</param>
      /// <returns>double</returns>
      public static double ToDouble(Cell cell, double defVal)
      {
         return ToDouble(cell.Value, defVal);
      }
      //===================================================
      /// <summary>
      /// Конвертирует значение строки в double
      /// </summary>
      /// <param name="dblStr">строка</param>
      /// <param name="defVal">значение по умолчанию</param>
      /// <returns>double</returns>
      public static double ToDouble(string dblStr, double defVal)
      {
         double retVal = defVal;
         NumberFormatInfo provider = new NumberFormatInfo();
         provider.NumberDecimalSeparator = ".";
         string newStrCoord = dblStr.Replace(',', '.');
         try { retVal = Convert.ToDouble(newStrCoord, provider); }
         catch { };
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Конвертирует значение строки в double
      /// </summary>
      /// <param name="dblStr">строка</param>
      /// <param name="defVal">значение по умолчанию</param>
      /// <returns>double</returns>
      public static double ToDoubleChecked(string dblStr, double defVal)
      {
         double retVal = defVal;
         NumberFormatInfo provider = new NumberFormatInfo();
         provider.NumberDecimalSeparator = ".";
         string newStrCoord = dblStr.Replace(',', '.');
         retVal = Convert.ToDouble(newStrCoord, provider);         
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Разбыват строку ячейки на список double
      /// </summary>
      /// <param name="cell">ячейка</param>
      /// <returns>List</returns>
      public static List<double> ToDoubleList(Cell cell)
      {
         return ToDoubleList(cell.Value);
      }
      //===================================================
      /// <summary>
      /// Разбивает строку на список double
      /// </summary>
      /// <param name="strList">строка</param>
      /// <returns>список значений</returns>
      public static List<double> ToDoubleList(string strList)
      {
         List<double> retLst = new List<double>();
         string[] parseList = strList.Split(new Char[] { ';'});
         foreach (string dblVal in parseList)
         {
            double tmpVal = ToDouble(dblVal, IM.NullD);
            if (tmpVal != IM.NullD)
               retLst.Add(tmpVal);
         }
         return retLst;
      }      

		//===================================================
		/// <summary>
      /// Разбыват строку ячейки на список double
      /// </summary>
      /// <param name="cell">ячейка</param>
      /// <returns>List</returns>
      public static List<double> ToDoubleListChecked(Cell cell)
      {
         return ToDoubleListChecked(cell.Value);
      }
      //===================================================
      /// <summary>
      /// Разбивает строку на список double
      /// </summary>
      /// <param name="strList">строка</param>
      /// <returns>список значений</returns>
      public static List<double> ToDoubleListChecked(string strList)
      {
         return ToDoubleListChecked(strList, ';');
      }
      //===================================================
      /// <summary>
      /// Разбивает строку на список double
      /// </summary>
      /// <param name="strList">строка</param>
      /// <param name="splitChars">список разделителей</param>
      /// <returns>список значений</returns>
      public static List<double> ToDoubleListChecked(string strList, params Char[] splitChars)
      {
         List<double> retLst = new List<double>();
         string[] parseList = strList.Split(splitChars);
         foreach (string dblVal in parseList)
         {
            double tmpVal = ToDoubleChecked(dblVal, IM.NullD);
            if (tmpVal != IM.NullD)
               retLst.Add(tmpVal);
         }
         return retLst;
      }      
		//===================================================
		/// <summary>
		/// Разбивает строку  в номера каналов
		/// </summary>
		/// <param name="strList">строка</param>
		/// <returns>список значений</returns>
		public static List<int> ToChanNumbList(string strList)
		{
			List<int> retVal = new List<int>();
			string[] comaSplit = strList.Split(new Char[] {','});
			foreach (string strChannels in comaSplit)
			{
				string[] spaceSplit = strChannels.Split(new Char[] {'-'});
				if(spaceSplit.Length > 1)
				{
					string strChannelMin = spaceSplit[0];
					string strChannelMax = spaceSplit[1];
					int CahnnelMin = ToInt32(strChannelMin, IM.NullI);
					int CahnnelMax = ToInt32(strChannelMax, IM.NullI);
					if((CahnnelMin != IM.NullI) && (CahnnelMax != IM.NullI))
						for(int j = CahnnelMin; j <= CahnnelMax; j++)
							retVal.Add(j);
				}
				else if(spaceSplit.Length == 1)
				{// Единичное значение
					string strChannel = spaceSplit[0];
					int Cahnnel = ToInt32(strChannel, IM.NullI);
					if(Cahnnel != IM.NullI)
						retVal.Add(Cahnnel);
				}
			}
			return retVal;
		}
      //===================================================
      /// <summary>
      /// Разбыват строку ячейки на список строк
      /// </summary>
      /// <param name="cell">ячейка</param>
      /// <returns>список строк</returns>
      public static List<string> ToStringList(Cell cell)
      {
         return ToStringList(cell.Value);
      }
      //===================================================
      /// <summary>
      /// Разбивает строку на список строк
      /// </summary>
      /// <param name="strList">строка</param>
      /// <returns>список значений</returns>
      public static List<string> ToStringList(string strList)
      {
         List<string> retLst = new List<string>();
         string[] parseList = strList.Split(new Char[] { ';' });
         foreach (string dblVal in parseList)
            retLst.Add(dblVal);
         return retLst;
      }
      //===================================================
      /// <summary>
      /// Возвращает код отдела
      /// </summary>
      /// <param name="depType">Тип департамента</param>
      /// <returns>Код департамента</returns>
      public static string DepartmetToCode(DepartmentType depType)
      {
         switch (depType)
         {
            case DepartmentType.Unknown: return "0";
            case DepartmentType.VRR: return "2";
            case DepartmentType.VFSR: return "1";
            case DepartmentType.VRS: return "3";
            case DepartmentType.VRZ: return "3";
            case DepartmentType.VMA: return "4";
            case DepartmentType.FILIA: return "5";
            case DepartmentType.URCM: return "6";
         }
         throw new IMException(string.Format("Unknown department type: {0}", depType));
      }
   }
}
