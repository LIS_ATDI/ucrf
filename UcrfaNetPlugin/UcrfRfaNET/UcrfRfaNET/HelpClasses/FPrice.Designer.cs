﻿namespace XICSM.UcrfRfaNET
{
   partial class FPrice
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.cbDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbArticle = new System.Windows.Forms.TextBox();
            this.cbWorkType = new System.Windows.Forms.ComboBox();
            this.cbStandart = new System.Windows.Forms.ComboBox();
            this.tbMeasure = new System.Windows.Forms.TextBox();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.gbState = new System.Windows.Forms.GroupBox();
            this.lblValidTo = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.lblValidFrom = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDefault = new System.Windows.Forms.ComboBox();
            this.checkBoxDefault = new System.Windows.Forms.CheckBox();
            this.gbDescription = new System.Windows.Forms.GroupBox();
            this.gbNote = new System.Windows.Forms.GroupBox();
            this.gbLimits = new System.Windows.Forms.GroupBox();
            this.cbLimitType = new System.Windows.Forms.ComboBox();
            this.tbMax = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbMin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbState.SuspendLayout();
            this.gbDescription.SuspendLayout();
            this.gbNote.SuspendLayout();
            this.gbLimits.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(348, 348);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 15;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(504, 348);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 16;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // cbDepartment
            // 
            this.cbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDepartment.FormattingEnabled = true;
            this.cbDepartment.Location = new System.Drawing.Point(135, 12);
            this.cbDepartment.Name = "cbDepartment";
            this.cbDepartment.Size = new System.Drawing.Size(225, 21);
            this.cbDepartment.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Management";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Article";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Work type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(373, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Standard";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(375, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Measure";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Price";
            // 
            // tbArticle
            // 
            this.tbArticle.Location = new System.Drawing.Point(135, 39);
            this.tbArticle.Name = "tbArticle";
            this.tbArticle.Size = new System.Drawing.Size(225, 20);
            this.tbArticle.TabIndex = 5;
            // 
            // cbWorkType
            // 
            this.cbWorkType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkType.FormattingEnabled = true;
            this.cbWorkType.Location = new System.Drawing.Point(135, 91);
            this.cbWorkType.Name = "cbWorkType";
            this.cbWorkType.Size = new System.Drawing.Size(627, 21);
            this.cbWorkType.TabIndex = 11;
            // 
            // cbStandart
            // 
            this.cbStandart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStandart.FormattingEnabled = true;
            this.cbStandart.Location = new System.Drawing.Point(448, 12);
            this.cbStandart.Name = "cbStandart";
            this.cbStandart.Size = new System.Drawing.Size(308, 21);
            this.cbStandart.TabIndex = 3;
            // 
            // tbMeasure
            // 
            this.tbMeasure.Location = new System.Drawing.Point(448, 39);
            this.tbMeasure.Name = "tbMeasure";
            this.tbMeasure.Size = new System.Drawing.Size(308, 20);
            this.tbMeasure.TabIndex = 7;
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(135, 65);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(225, 20);
            this.tbPrice.TabIndex = 9;
            // 
            // tbDescription
            // 
            this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDescription.Location = new System.Drawing.Point(6, 19);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(271, 75);
            this.tbDescription.TabIndex = 0;
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(9, 47);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(56, 17);
            this.checkBoxActive.TabIndex = 0;
            this.checkBoxActive.Text = "Active";
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // tbNote
            // 
            this.tbNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNote.Location = new System.Drawing.Point(6, 19);
            this.tbNote.Multiline = true;
            this.tbNote.Name = "tbNote";
            this.tbNote.Size = new System.Drawing.Size(443, 75);
            this.tbNote.TabIndex = 0;
            // 
            // gbState
            // 
            this.gbState.Controls.Add(this.lblValidTo);
            this.gbState.Controls.Add(this.dateTimePicker2);
            this.gbState.Controls.Add(this.lblValidFrom);
            this.gbState.Controls.Add(this.dateTimePicker1);
            this.gbState.Controls.Add(this.label7);
            this.gbState.Controls.Add(this.cbDefault);
            this.gbState.Controls.Add(this.checkBoxDefault);
            this.gbState.Controls.Add(this.checkBoxActive);
            this.gbState.Location = new System.Drawing.Point(307, 224);
            this.gbState.Name = "gbState";
            this.gbState.Size = new System.Drawing.Size(455, 104);
            this.gbState.TabIndex = 14;
            this.gbState.TabStop = false;
            this.gbState.Text = "State";
            // 
            // lblValidTo
            // 
            this.lblValidTo.AutoSize = true;
            this.lblValidTo.Location = new System.Drawing.Point(239, 16);
            this.lblValidTo.Name = "lblValidTo";
            this.lblValidTo.Size = new System.Drawing.Size(53, 13);
            this.lblValidTo.TabIndex = 7;
            this.lblValidTo.Text = "ValidFrom";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(334, 13);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(115, 20);
            this.dateTimePicker2.TabIndex = 6;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // lblValidFrom
            // 
            this.lblValidFrom.AutoSize = true;
            this.lblValidFrom.Location = new System.Drawing.Point(6, 16);
            this.lblValidFrom.Name = "lblValidFrom";
            this.lblValidFrom.Size = new System.Drawing.Size(53, 13);
            this.lblValidFrom.TabIndex = 5;
            this.lblValidFrom.Text = "ValidFrom";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(118, 13);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(115, 20);
            this.dateTimePicker1.TabIndex = 4;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(107, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Default type:";
            // 
            // cbDefault
            // 
            this.cbDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDefault.Enabled = false;
            this.cbDefault.FormattingEnabled = true;
            this.cbDefault.Location = new System.Drawing.Point(103, 67);
            this.cbDefault.Name = "cbDefault";
            this.cbDefault.Size = new System.Drawing.Size(147, 21);
            this.cbDefault.TabIndex = 2;
            // 
            // checkBoxDefault
            // 
            this.checkBoxDefault.AutoSize = true;
            this.checkBoxDefault.Location = new System.Drawing.Point(9, 70);
            this.checkBoxDefault.Name = "checkBoxDefault";
            this.checkBoxDefault.Size = new System.Drawing.Size(60, 17);
            this.checkBoxDefault.TabIndex = 1;
            this.checkBoxDefault.Text = "Default";
            this.checkBoxDefault.UseVisualStyleBackColor = true;
            this.checkBoxDefault.CheckedChanged += new System.EventHandler(this.checkBoxDefault_CheckedChanged);
            // 
            // gbDescription
            // 
            this.gbDescription.Controls.Add(this.tbDescription);
            this.gbDescription.Location = new System.Drawing.Point(16, 118);
            this.gbDescription.Name = "gbDescription";
            this.gbDescription.Size = new System.Drawing.Size(283, 100);
            this.gbDescription.TabIndex = 12;
            this.gbDescription.TabStop = false;
            this.gbDescription.Text = "Description";
            // 
            // gbNote
            // 
            this.gbNote.Controls.Add(this.tbNote);
            this.gbNote.Location = new System.Drawing.Point(307, 118);
            this.gbNote.Name = "gbNote";
            this.gbNote.Size = new System.Drawing.Size(455, 100);
            this.gbNote.TabIndex = 13;
            this.gbNote.TabStop = false;
            this.gbNote.Text = "Note";
            // 
            // gbLimits
            // 
            this.gbLimits.Controls.Add(this.cbLimitType);
            this.gbLimits.Controls.Add(this.tbMax);
            this.gbLimits.Controls.Add(this.label10);
            this.gbLimits.Controls.Add(this.tbMin);
            this.gbLimits.Controls.Add(this.label9);
            this.gbLimits.Controls.Add(this.label8);
            this.gbLimits.Location = new System.Drawing.Point(16, 224);
            this.gbLimits.Name = "gbLimits";
            this.gbLimits.Size = new System.Drawing.Size(283, 104);
            this.gbLimits.TabIndex = 17;
            this.gbLimits.TabStop = false;
            this.gbLimits.Text = "Limits";
            // 
            // cbLimitType
            // 
            this.cbLimitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLimitType.FormattingEnabled = true;
            this.cbLimitType.Location = new System.Drawing.Point(114, 16);
            this.cbLimitType.Name = "cbLimitType";
            this.cbLimitType.Size = new System.Drawing.Size(163, 21);
            this.cbLimitType.TabIndex = 12;
            // 
            // tbMax
            // 
            this.tbMax.Location = new System.Drawing.Point(114, 69);
            this.tbMax.Name = "tbMax";
            this.tbMax.Size = new System.Drawing.Size(163, 20);
            this.tbMax.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Max";
            // 
            // tbMin
            // 
            this.tbMin.Location = new System.Drawing.Point(114, 43);
            this.tbMin.Name = "tbMin";
            this.tbMin.Size = new System.Drawing.Size(163, 20);
            this.tbMin.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Min";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Limit type";
            // 
            // FPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 385);
            this.Controls.Add(this.gbLimits);
            this.Controls.Add(this.gbDescription);
            this.Controls.Add(this.gbNote);
            this.Controls.Add(this.gbState);
            this.Controls.Add(this.tbPrice);
            this.Controls.Add(this.tbMeasure);
            this.Controls.Add(this.cbStandart);
            this.Controls.Add(this.cbWorkType);
            this.Controls.Add(this.tbArticle);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDepartment);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FPrice";
            this.Text = "FPrice";
            this.gbState.ResumeLayout(false);
            this.gbState.PerformLayout();
            this.gbDescription.ResumeLayout(false);
            this.gbDescription.PerformLayout();
            this.gbNote.ResumeLayout(false);
            this.gbNote.PerformLayout();
            this.gbLimits.ResumeLayout(false);
            this.gbLimits.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button buttonOK;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.ComboBox cbDepartment;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox tbArticle;
      private System.Windows.Forms.ComboBox cbWorkType;
      private System.Windows.Forms.ComboBox cbStandart;
      private System.Windows.Forms.TextBox tbMeasure;
      private System.Windows.Forms.TextBox tbPrice;
      private System.Windows.Forms.TextBox tbDescription;
      private System.Windows.Forms.CheckBox checkBoxActive;
      private System.Windows.Forms.TextBox tbNote;
      private System.Windows.Forms.GroupBox gbState;
      private System.Windows.Forms.GroupBox gbDescription;
      private System.Windows.Forms.GroupBox gbNote;
      private System.Windows.Forms.CheckBox checkBoxDefault;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.ComboBox cbDefault;
      private System.Windows.Forms.GroupBox gbLimits;
      private System.Windows.Forms.ComboBox cbLimitType;
      private System.Windows.Forms.TextBox tbMax;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.TextBox tbMin;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label lblValidTo;
      private System.Windows.Forms.DateTimePicker dateTimePicker2;
      private System.Windows.Forms.Label lblValidFrom;
      private System.Windows.Forms.DateTimePicker dateTimePicker1;
   }
}