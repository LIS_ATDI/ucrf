﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    static class OrmHelp
    {
        /// <summary>
        /// Вертає строчку полів для роботи ImRecordSet.Select
        /// </summary>
        /// <param name="flds">масив полів БД</param>
        /// <returns>строчка з полів</returns>
        public static string GetStringNameFromOrmArray(OrmField[] flds)
        {
            return GetStringNameFromOrmList(flds.ToList());
        }
        /// <summary>
        /// Вертає строчку полів для роботи ImRecordSet.Select
        /// </summary>
        /// <param name="flds">список полів БД</param>
        /// <returns>строчка з полів</returns>
        public static string GetStringNameFromOrmList(List<OrmField> flds)
        {
            string strTemp = "";
            foreach (OrmField fld in flds)
                strTemp += fld.Name + ",";
            return strTemp.Remove(strTemp.Count() - 1);
        }

        /// <summary>
        /// Вертає строчку зі всіх полів даної таблиці
        /// </summary>
        /// <param name="tableName">ім'я таблиці</param>
        /// <returns>строчка</returns>
        public static string GetAllNamesFromTable(string tableName)
        {
            string strTemp = "";

            OrmTable ormTab = OrmSchema.Table(tableName);
            OrmField[] fldMas = ormTab.Fields;

            foreach (OrmField fld in fldMas)
                if ((fld.DDesc != null) && (fld.IsCompiledInEdition) && (fld.Nature == OrmFieldNature.Column) &&
                    ((fld.DDesc.Coding == OrmDataCoding.tvalDATETIME) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER) ||
                     (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)))
                {
                    strTemp += fld.Name + ",";
                }

            return strTemp.Remove(strTemp.Count() - 1);
        }

        public static List<string> GetAllNamesTypesFromTable(string tableName)
        {
            List<string> strTempList = new List<string>();

            OrmTable ormTab = OrmSchema.Table(tableName);
            OrmField[] fldMas = ormTab.Fields;

            foreach (OrmField fld in fldMas)
                if ((fld.DDesc != null) && (fld.IsCompiledInEdition) && (fld.Nature == OrmFieldNature.Column))
                {
                    if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME)
                    {
                        strTempList.Add(OrmDataCoding.tvalDATETIME.ToString());
                    }
                    if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                    {
                        strTempList.Add(OrmDataCoding.tvalNUMBER.ToString());
                    }
                    if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING)
                    {
                        strTempList.Add(OrmDataCoding.tvalSTRING.ToString());
                    }
                }
            return strTempList;
        }

        /// <summary>
        /// Вертає запис(у вигляді списка об'єктів) з таблиці, які задовольняють первинному ключу
        /// </summary>
        /// <param name="tableName">Таблиця</param>
        /// <param name="primaryKey">список первинних ключів</param>
        /// <returns>запис(список об'єктів)</returns>
        public static ArrayList GetListValueFromTable(string tableName, string[] primaryKey)
        {
            return GetListValueFromTable(GetAllNamesFromTable(tableName), tableName, primaryKey);
        }

        /// <summary>
        /// Вертає запис(у вигляді списка об'єктів) з таблиці, які задовольняють первинному ключу
        /// </summary>
        /// <param name="select">поля, які підлягають вибірці</param>
        /// <param name="tableName">Таблиця</param>
        /// <param name="primaryKey">список первинних ключів</param>
        /// <returns>запис(список об'єктів)</returns>
        public static ArrayList GetListValueFromTable(string select, string tableName, string[] primaryKey)
        {
            ArrayList al = new ArrayList();

            IMRecordset rec = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);


            return al;
        }

        /// <summary>
        /// Закриває відкритий ImRecordSet
        /// </summary>
        /// <param name="r">Recordset</param>
        public static void Final(this IMRecordset r)
        {
            if (r.IsOpen())
                r.Close();
            r.Dispose();
        }

        /// <summary>
        /// Формування IMRecordSet на зчитування або запис всіх полів таблиці
        /// </summary>
        /// <param name="r">ImRecordSet</param>
        /// <param name="tableName">назва таблиці</param>
        /// <param name="isRead">зчитування або запис</param>
        public static void StartReadWrite(this IMRecordset r, string tableName, bool isRead)
        {
            if (isRead)
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            else
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
            r.Select(GetAllNamesFromTable(tableName));
        }

        /// <summary>
        /// Формування IMRecordSet на зчитування деяких полів таблиці
        /// </summary>
        /// <param name="r">ImRecordSet</param>
        /// <param name="tableName">назва таблиці</param>
        /// <param name="select">список полів для вибірки</param>
        /// <param name="isRead">зчитування або запис</param>
        public static void StartReadWrite(this IMRecordset r, string tableName, string select,bool isRead)
        {
            if (isRead)
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            else
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
            r.Select(select);
        }

        /// <summary>
        /// Формування IMRecordSet на зчитування деяких полів таблиці і вибірка по id
        /// </summary>
        /// <param name="r">ImRecordSet</param>
        /// <param name="tableName">назва таблиці</param>
        /// <param name="select">список полів для вибірки</param>
        /// <param name="isRead">зчитування або запис</param>
        /// <param name="nameId">Назва Id поля</param>
        /// <param name="id">Id запису</param>
        public static void StartReadWrite(this IMRecordset r, string tableName, string select, bool isRead, string nameId, int id)
        {
            if (isRead)
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
            else
                r = new IMRecordset(tableName, IMRecordset.Mode.ReadWrite);
            r.Select(select);            
            r.SetWhere(nameId,IMRecordset.Operation.Eq,id);
        }       

    }
}
