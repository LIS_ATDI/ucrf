﻿namespace XICSM.UcrfRfaNET
{
   partial class FFindAllStation
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.tbMaxFreq = new System.Windows.Forms.TextBox();
         this.tbMinFreq = new System.Windows.Forms.TextBox();
         this.cbFreq = new System.Windows.Forms.CheckBox();
         this.tbLatitude = new System.Windows.Forms.TextBox();
         this.tbLongitude = new System.Windows.Forms.TextBox();
         this.tbRadius = new System.Windows.Forms.TextBox();
         this.cbRadius = new System.Windows.Forms.CheckBox();
         this.buttonUpdate = new System.Windows.Forms.Button();
         this.buttonExit = new System.Windows.Forms.Button();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.label4);
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.tbMaxFreq);
         this.groupBox1.Controls.Add(this.tbMinFreq);
         this.groupBox1.Controls.Add(this.cbFreq);
         this.groupBox1.Controls.Add(this.tbLatitude);
         this.groupBox1.Controls.Add(this.tbLongitude);
         this.groupBox1.Controls.Add(this.tbRadius);
         this.groupBox1.Controls.Add(this.cbRadius);
         this.groupBox1.Location = new System.Drawing.Point(12, 12);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(480, 81);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Find parameters";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(356, 18);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(10, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "-";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(184, 22);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(44, 13);
         this.label4.TabIndex = 2;
         this.label4.Text = "Position";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(356, 47);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(10, 13);
         this.label3.TabIndex = 8;
         this.label3.Text = "-";
         // 
         // tbMaxFreq
         // 
         this.tbMaxFreq.Location = new System.Drawing.Point(372, 45);
         this.tbMaxFreq.Name = "tbMaxFreq";
         this.tbMaxFreq.Size = new System.Drawing.Size(94, 20);
         this.tbMaxFreq.TabIndex = 9;
         // 
         // tbMinFreq
         // 
         this.tbMinFreq.Location = new System.Drawing.Point(256, 45);
         this.tbMinFreq.Name = "tbMinFreq";
         this.tbMinFreq.Size = new System.Drawing.Size(94, 20);
         this.tbMinFreq.TabIndex = 7;
         // 
         // cbFreq
         // 
         this.cbFreq.AutoSize = true;
         this.cbFreq.Location = new System.Drawing.Point(18, 47);
         this.cbFreq.Name = "cbFreq";
         this.cbFreq.Size = new System.Drawing.Size(137, 17);
         this.cbFreq.TabIndex = 6;
         this.cbFreq.Text = "Frequency range (MHz)";
         this.cbFreq.UseVisualStyleBackColor = true;
         this.cbFreq.CheckedChanged += new System.EventHandler(this.cbRadius_CheckedChanged);
         // 
         // tbLatitude
         // 
         this.tbLatitude.Location = new System.Drawing.Point(372, 15);
         this.tbLatitude.Name = "tbLatitude";
         this.tbLatitude.Size = new System.Drawing.Size(94, 20);
         this.tbLatitude.TabIndex = 5;
         this.tbLatitude.Validated += new System.EventHandler(this.Position_Validated);
         // 
         // tbLongitude
         // 
         this.tbLongitude.Location = new System.Drawing.Point(256, 15);
         this.tbLongitude.Name = "tbLongitude";
         this.tbLongitude.Size = new System.Drawing.Size(94, 20);
         this.tbLongitude.TabIndex = 3;
         this.tbLongitude.Validated += new System.EventHandler(this.Position_Validated);
         // 
         // tbRadius
         // 
         this.tbRadius.Location = new System.Drawing.Point(110, 18);
         this.tbRadius.Name = "tbRadius";
         this.tbRadius.Size = new System.Drawing.Size(68, 20);
         this.tbRadius.TabIndex = 1;
         // 
         // cbRadius
         // 
         this.cbRadius.AutoSize = true;
         this.cbRadius.Location = new System.Drawing.Point(18, 21);
         this.cbRadius.Name = "cbRadius";
         this.cbRadius.Size = new System.Drawing.Size(76, 17);
         this.cbRadius.TabIndex = 0;
         this.cbRadius.Text = "Radius (m)";
         this.cbRadius.UseVisualStyleBackColor = true;
         this.cbRadius.CheckedChanged += new System.EventHandler(this.cbRadius_CheckedChanged);
         // 
         // buttonUpdate
         // 
         this.buttonUpdate.Location = new System.Drawing.Point(303, 99);
         this.buttonUpdate.Name = "buttonUpdate";
         this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
         this.buttonUpdate.TabIndex = 1;
         this.buttonUpdate.Text = "Update";
         this.buttonUpdate.UseVisualStyleBackColor = true;
         this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
         // 
         // buttonExit
         // 
         this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.buttonExit.Location = new System.Drawing.Point(417, 99);
         this.buttonExit.Name = "buttonExit";
         this.buttonExit.Size = new System.Drawing.Size(75, 23);
         this.buttonExit.TabIndex = 2;
         this.buttonExit.Text = "Exit";
         this.buttonExit.UseVisualStyleBackColor = true;
         // 
         // FFindAllStation
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(502, 138);
         this.Controls.Add(this.buttonExit);
         this.Controls.Add(this.buttonUpdate);
         this.Controls.Add(this.groupBox1);
         this.Name = "FFindAllStation";
         this.Text = "FFindAllStation";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.TextBox tbRadius;
      private System.Windows.Forms.CheckBox cbRadius;
      private System.Windows.Forms.TextBox tbLatitude;
      private System.Windows.Forms.TextBox tbLongitude;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox tbMaxFreq;
      private System.Windows.Forms.TextBox tbMinFreq;
      private System.Windows.Forms.CheckBox cbFreq;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button buttonUpdate;
      private System.Windows.Forms.Button buttonExit;
   }
}