﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GridCtrl;
using System.Globalization;
using System.Collections.Specialized;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   public partial class FEnterEVP : Form
   {
      private NumberFormatInfo provider;
      private List<double> _data = new List<double>();
      string _firstColTitl = string.Empty;
      string _secondColTitl = string.Empty;

      public FEnterEVP(string Title, string FirstColTitl, string SecondColTitl)
      {
         provider = new NumberFormatInfo();
         InitializeComponent();

         this.Text = Title;
         _firstColTitl = FirstColTitl;
         _secondColTitl = SecondColTitl;

         InitializeGrid();
      }

      public FEnterEVP(List<double> data, string Title, string FirstColTitl, string SecondColTitl)
      {
         provider = new NumberFormatInfo();
         InitializeComponent();

         this.Text = Title;
         _firstColTitl = FirstColTitl;
         _secondColTitl = SecondColTitl;

         _data = data;
         InitializeGrid();
         FillGrid();
      }

      #region EVENTS

      private void BtnOk_Click(object sender, EventArgs e)
      {
         SaveDataInList();
         this.DialogResult = DialogResult.OK;
         this.Close();
      }

      private void grid_OnDataValidate(Cell cell)
      {
         if (!cell.Key.Equals("val") || cell.Value.Length == 0)
            return;

         cell.Value = ConvertType.ToDouble(cell, 0.0).ToString();//IM.NullD).ToString();
      }

      #endregion

      #region METHODS

      public List<double> Data
      {
         set
         {
            _data = value;
            FillGrid();
         }
         get
         {
            return _data;
         }
      }

      private void SaveDataInList()
      {
         _data.Clear();
         foreach (GridCtrl.Row r in grid.GetRowList())
         {
            for (int i = 0; i < 13; i++)
            {
               Cell c = r.GetCell(i);
               if (c.Key.Equals("val"))
               {
                  try
                  {
                     if (string.IsNullOrEmpty(c.Value))
                     {
                        _data.Add(0);
                     }
                     else
                     {
                        _data.Add(Convert.ToDouble(c.Value));
                     }
                  }
                  catch
                  {
                     throw new Exception("Error with convertation");
                  }
               }
            }
         }
      }

      private void FillGrid()
      {
         short iterator = 0;
         foreach (GridCtrl.Row r in grid.GetRowList())
         {
            for (int i = 0; i < 13; i++)
            {
               Cell c = r.GetCell(i);
               if (c.Key.Equals("val"))
               {
                  if (iterator < _data.Count)
                  {
                     if (_data[iterator] != IM.NullD)
                     {
                        c.Value = _data[iterator].ToString();

                        //c.FontName = grid.Font.Name;
                        c.TextHeight = 12;
                     }  
                     else
                     {
                        c.Value = "0";
                     }
                     iterator++;
                  }
                  else
                  {
                     c.Value = "0";
                  }
               }
            }
         }                  
      }

      private void InitializeGrid()
      {
         int value = 0;
         
         for (int i = 0; i < 3; i++) // creates rows
         {
            int rowcount = grid.GetRowCount();
            grid.AddRow();
            grid.AddColumn(rowcount, _firstColTitl);
            Cell c = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);
            c.CanEdit = false;
            c.FontStyle = System.Drawing.FontStyle.Bold;

            //c.FontName = grid.Font.Name;
            c.TextHeight = 12;
            c.TabOrder = -1;

            for (int i1 = 0; i1 < 12; i1++) // rows with values
            {
               grid.AddColumn(grid.GetRowCount() - 1, value.ToString());
               c = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);
               c.CanEdit = false;
               c.FontStyle = System.Drawing.FontStyle.Bold;

               //c.FontName = grid.Font.Name;
               c.TextHeight = 12;

               c.BackColor = System.Drawing.Color.LightGray;
               value += 10;
               c.TabOrder = -1;
            }
            grid.AddRow();
            rowcount = grid.GetRowCount();
            grid.AddColumn(rowcount - 1, _secondColTitl);
            c = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);
            c.CanEdit = false;
            c.TabOrder = -1;

            //c.FontName = grid.Font.Name;
            c.TextHeight = 12;

            for (int i1 = 0; i1 < 12; i1++)
            {
               grid.AddColumn(grid.GetRowCount() - 1, string.Empty);
               c = grid.GetCell(grid.GetRowCount() - 1, grid.GetColumnCount(grid.GetRowCount() - 1) - 1);

               //c.FontName = grid.Font.Name;
               c.TextHeight = 12;
               c.Key = "val";
               c.TabOrder = -1;
            }
         }

         // Выравниваем первую колонку
         int curWidth = 0;        

         for (int r = 0; r < grid.GetRowCount(); r++)
         {
            int width = grid.widthCellText(grid.GetCell(r, 0));
            if (curWidth < width)
               curWidth = width;                      
         }
         grid.FirstColumnWidth = curWidth + 20;

         int tabOrder = 0;

         for (int r = 0; r < grid.GetRowCount(); r++)
         {
            if (r % 2 != 1)
               continue;
            for (int c = 0; c < grid.GetColumnCount(r); c++)
            {
               if (c > 0)
               {
                  Cell cell = grid.GetCell(r, c);
                  cell.TabOrder = tabOrder;
                  tabOrder++;
               }                  
            }
         }
      }

      private void BtnExit_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      #endregion

      private void FEnterEVP_Shown(object sender, EventArgs e)
      {
         Cell firstCell = grid.GetCell(1,1);
         //grid.SelectCell(firstCell);
         //grid.BeginEditing();

         //firstCell.row.Size = 21;
         //firstCell.row.ResizeRow(Graphics.FromHwnd(this.Handle));
         //grid.SelectCell(firstCell);
         
         //grid.EnsureVisible(firstCell);
         //grid.BeginEditing();
         //grid.EndEdition();
      }
   }
}
