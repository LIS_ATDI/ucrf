﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   /// <summary>
   /// Container for ComboBox
   /// </summary>
   /// <typeparam name="T">Enum type</typeparam>
   internal class CComboBoxContainer<T>
   {
      //===================================================
      private class ItemComboBox
      {
         public string localizType { get; set; }
         public T type { get; set; }

         public ItemComboBox(T dt)
         {
            type = dt;
            localizType = CLocaliz.TxT(type.ToString());
         }
      }
      //---------------------------------------------------
      List<ItemComboBox> itemList;
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      public CComboBoxContainer()
      {
         System.Type type = typeof(T);
         while ((type != typeof(Enum)) && (type != null))
            type = type.BaseType;

         if (type != typeof(Enum))
            throw new Exception("CComboBoxContainer<T> - T must be based from Enum.");

         itemList = new List<ItemComboBox>();
         string[] names = Enum.GetNames(typeof(T));
         foreach(string name in names)
         {
            T item = (T)Enum.Parse(typeof(T), name);
            itemList.Add(new ItemComboBox(item));
         }
      }
      //===================================================
      /// <summary>
      /// Returns localization strings of the Enum type
      /// </summary>
      /// <returns>localization strings</returns>
      public string[] userList
      {
         get
         {
            List<string> retVal = new List<string>();
            foreach (ItemComboBox item in itemList)
               retVal.Add(item.localizType);
            return retVal.ToArray();
         }
      }
      //===================================================
      /// <summary>
      /// Returns type by index
      /// </summary>
      /// <param name="index">index</param>
      /// <returns>type</returns>
      public T getType(int index)
      {
         if ((0 <= index) && (index < itemList.Count))
            return itemList[index].type;
         throw new Exception("Error index");
      }
      //===================================================
      public int getIndex(string strType)
      {
         int retVal = -1;
         try
         {
            T item = (T)Enum.Parse(typeof(T), strType);
            retVal = getIndex(item);
         }
         catch { }
         return retVal;
      }
      //===================================================
      public int getIndex(T _type)
      {
         for (int i = 0; i < itemList.Count; i++)
            if (itemList[i].type.Equals(_type))
               return i;
         return -1;
      }
   }
}
