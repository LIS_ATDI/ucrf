﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;
using Lis.CommonLib.Binding;

namespace XICSM.UcrfRfaNET
{
    public partial class FormEditedPacket : Form
    {

        private int PacketId
        {
            get;
            set;
        }


        public FormEditedPacket(int PacketId)
        {
            InitializeComponent();

            this.PacketId = PacketId;
            this.lbl_APPL_IN_PACKET.Text = CLocaliz.TxT(this.lbl_APPL_IN_PACKET.Text.ToString());
            this.lbl_DE_COUNT.Text = CLocaliz.TxT(this.lbl_DE_COUNT.Text);
            this.lbl_DRV_DE_DATE.Text = CLocaliz.TxT(this.lbl_DRV_DE_DATE.Text);
            this.lbl_DRV_EMC_DATE.Text = CLocaliz.TxT(this.lbl_DRV_EMC_DATE.Text);
            this.lbl_EMC_COUNT.Text = CLocaliz.TxT(this.lbl_EMC_COUNT.Text);
            this.lbl_RFA_APPL_IN_PACKET.Text = CLocaliz.TxT(this.lbl_RFA_APPL_IN_PACKET.Text);
            this.LblRegion.Text = CLocaliz.TxT(this.LblRegion.Text);
            this.Text = CLocaliz.TxT(this.Text);
            this.chbConclusionIsEnabled.Text = CLocaliz.TxT("Conclusion is enabled");
            this.chbPermitsIsEnabled.Text = CLocaliz.TxT("Permits is enabled");
            this.btnCancel.Text = CLocaliz.TxT("Cancel");
            this.btnOk.Text = CLocaliz.TxT("Ok");
            this.Note.Text = CLocaliz.TxT("Note");

            chbConclusionIsEnabled.Checked = false;
            chbPermitsIsEnabled.Checked = false;

            CheckConclusion();
            CheckPermits();

            InitListRegion();
            LoadForm();




        }

        
        public void InitListRegion()
        {

            try
            {
                HashSet<string> hashProvince = new HashSet<string>();
                IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                rsArea.Select("ID,NAME");
                rsArea.OrderBy("NAME", OrderDirection.Ascending);

                Region.Items.Clear();
                Region.Items.Add("");
                try
                {
                    for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                    {
                        string province = rsArea.GetS("NAME");
                        if (!hashProvince.Contains(province))
                        {
                            Region.Items.Add(province);

                        }
                    }
                }
                finally
                {
                    if (rsArea.IsOpen())
                        rsArea.Close();
                    rsArea.Destroy();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }


        public void LoadForm()
        {
            try
            {
                if (PacketId >= -1)
                {
                    using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite))
                    {

                        r.Select("ID,REGION,APPL_IN_PACKET, RFA_APPL_IN_PACKET,EMC_COUNT, DRV_EMC_DATE,DE_COUNT,DRV_DE_DATE,TABLE_NAME,MEMO");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, PacketId);
                        r.Open();
                        if (!r.IsEOF())
                        {
                            DateTime DRV_EMC_DATE_ = IM.NullT;
                            DateTime DRV_DE_DATE_ = IM.NullT;
                            Region.Text = r.GetS("REGION");
                            textBoxNote.Text = r.GetS("MEMO");
                            if (r.GetI("APPL_IN_PACKET") < 2147483645) { APPL_IN_PACKET.Text = r.GetI("APPL_IN_PACKET").ToString(); } else { APPL_IN_PACKET.Text = "0"; }
                            if (r.GetI("RFA_APPL_IN_PACKET") < 2147483645) { RFA_APPL_IN_PACKET.Text = r.GetI("RFA_APPL_IN_PACKET").ToString(); } else { RFA_APPL_IN_PACKET.Text = "0"; }
                            if (r.GetI("EMC_COUNT") < 2147483645) { EMC_COUNT.Text = r.GetI("EMC_COUNT").ToString(); } else { EMC_COUNT.Text = "0"; }


                            if (DateTime.TryParse(r.GetT("DRV_EMC_DATE").ToString(), out DRV_EMC_DATE_))
                            {

                                if (DRV_EMC_DATE_ > DateTime.Parse("01.01.0001"))
                                {
                                     DRV_EMC_DATE.Value = DRV_EMC_DATE_;
                                }
                                else
                                {
                                    //DRV_EMC_DATE.Value = DateTime.Now;
                                    DRV_EMC_DATE.CustomFormat = " ";
                                    DRV_EMC_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom; 
                                }
                            }
                            else
                            {
                                //DRV_EMC_DATE.Value = IM.NullT;
                                //DRV_EMC_DATE.Value = DateTime.Now;
                                //DRV_EMC_DATE.CustomFormat = " ";
                                //DRV_EMC_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom; 
                            }

                            if (r.GetI("DE_COUNT") < 2147483645) { DE_COUNT.Text = r.GetI("DE_COUNT").ToString(); } else { DE_COUNT.Text = "0"; }

                            if (DateTime.TryParse(r.GetT("DRV_DE_DATE").ToString(), out DRV_DE_DATE_))
                            {
                                if (DRV_EMC_DATE_ > DateTime.Parse("01.01.0001"))
                                {
                                    DRV_DE_DATE.Value = DRV_DE_DATE_;
                                }
                                else
                                {
                                    //DRV_DE_DATE.Value = DateTime.Now;
                                    DRV_DE_DATE.CustomFormat = " ";
                                    DRV_DE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom; 
                                }
                            }
                            else
                            {
                                //DRV_DE_DATE.Value = DateTime.Now;
                                //DRV_DE_DATE.CustomFormat = " ";
                                //DRV_DE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom; 
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void SaveForm()
        {
            try
            {
                if (PacketId >= -1)
                {
                    using (Icsm.LisRecordSet r = new Icsm.LisRecordSet(PlugTbl.itblXnrfaPacket, IMRecordset.Mode.ReadWrite))
                    {

                        r.Select("ID,REGION,APPL_IN_PACKET, RFA_APPL_IN_PACKET,EMC_COUNT, DRV_EMC_DATE,DE_COUNT,DRV_DE_DATE,TABLE_NAME,MEMO");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, PacketId);
                        r.Open();
                        if (!r.IsEOF())
                        {
                            r.Edit();
                            r.Put("REGION", Region.Text);
                            r.Put("MEMO", textBoxNote.Text);
                            r.Put("APPL_IN_PACKET", int.Parse(APPL_IN_PACKET.Text));
                            r.Put("RFA_APPL_IN_PACKET", int.Parse(RFA_APPL_IN_PACKET.Text));
                            if (chbConclusionIsEnabled.Checked) r.Put("EMC_COUNT", int.Parse(EMC_COUNT.Text));
                            if (chbConclusionIsEnabled.Checked) r.Put("DRV_EMC_DATE", DRV_EMC_DATE.Value);
                            if (chbPermitsIsEnabled.Checked) r.Put("DE_COUNT", int.Parse(DE_COUNT.Text));
                            if (chbPermitsIsEnabled.Checked) r.Put("DRV_DE_DATE", DRV_DE_DATE.Value);
                            r.Update();
                            MessageBox.Show(CLocaliz.TxT("The information is saved"));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveForm();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DRV_EMC_DATE_Enter(object sender, EventArgs e)
        {
            DRV_EMC_DATE.Value = DateTime.Now;
            DRV_EMC_DATE.Format = DateTimePickerFormat.Short;
        }

        private void DRV_DE_DATE_Enter(object sender, EventArgs e)
        {
            DRV_DE_DATE.Value = DateTime.Now;
            DRV_DE_DATE.Format = DateTimePickerFormat.Short;
        }

        private void CheckConclusion()
        {
            if (chbConclusionIsEnabled.Checked)
            {
                lbl_EMC_COUNT.Visible = true;
                EMC_COUNT.Visible = true;

                lbl_DRV_EMC_DATE.Visible = true;
                DRV_EMC_DATE.Visible = true;
            }
            else
            {
                lbl_EMC_COUNT.Visible = false;
                EMC_COUNT.Visible = false;

                lbl_DRV_EMC_DATE.Visible = false;
                DRV_EMC_DATE.Visible = false;
            }
        }

        private void CheckPermits()
        {
            if (chbPermitsIsEnabled.Checked)
            {
                lbl_DE_COUNT.Visible = true;
                DE_COUNT.Visible = true;

                lbl_DRV_DE_DATE.Visible = true;
                DRV_DE_DATE.Visible = true;
            }
            else
            {
                lbl_DE_COUNT.Visible = false;
                DE_COUNT.Visible = false;

                lbl_DRV_DE_DATE.Visible = false;
                DRV_DE_DATE.Visible = false;
            }
        }


        private void chbConclusionIsEnabled_CheckedChanged(object sender, EventArgs e)
        {
            CheckConclusion();
        }

        private void chbPermitsIsEnabled_CheckedChanged(object sender, EventArgs e)
        {
            CheckPermits();
        }
    }
}
