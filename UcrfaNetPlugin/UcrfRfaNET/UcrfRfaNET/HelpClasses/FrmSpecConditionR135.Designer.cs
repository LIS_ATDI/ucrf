﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FrmSpecConditionR135
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.label1 = new System.Windows.Forms.Label();
         this.grpBoxSql = new System.Windows.Forms.GroupBox();
         this.label3 = new System.Windows.Forms.Label();
         this.btnTestSql = new System.Windows.Forms.Button();
         this.label2 = new System.Windows.Forms.Label();
         this.nmrcUpDownRecordId = new System.Windows.Forms.NumericUpDown();
         this.txtBoxSql = new System.Windows.Forms.TextBox();
         this.btnCancel = new System.Windows.Forms.Button();
         this.btnOk = new System.Windows.Forms.Button();
         this.cmbBoxDocType = new System.Windows.Forms.ComboBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.txtBoxCondition = new System.Windows.Forms.TextBox();
         this.grpBoxSql.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.nmrcUpDownRecordId)).BeginInit();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 9);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(79, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Document type";
         // 
         // grpBoxSql
         // 
         this.grpBoxSql.Controls.Add(this.label3);
         this.grpBoxSql.Controls.Add(this.btnTestSql);
         this.grpBoxSql.Controls.Add(this.label2);
         this.grpBoxSql.Controls.Add(this.nmrcUpDownRecordId);
         this.grpBoxSql.Controls.Add(this.txtBoxSql);
         this.grpBoxSql.Location = new System.Drawing.Point(15, 53);
         this.grpBoxSql.Name = "grpBoxSql";
         this.grpBoxSql.Size = new System.Drawing.Size(565, 203);
         this.grpBoxSql.TabIndex = 2;
         this.grpBoxSql.TabStop = false;
         this.grpBoxSql.Text = "Sql for condition";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(7, 158);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(296, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "In the sql guery the string \'{0}\' is changed to an application ID";
         // 
         // btnTestSql
         // 
         this.btnTestSql.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnTestSql.Location = new System.Drawing.Point(484, 174);
         this.btnTestSql.Name = "btnTestSql";
         this.btnTestSql.Size = new System.Drawing.Size(75, 23);
         this.btnTestSql.TabIndex = 3;
         this.btnTestSql.Text = "Test sql";
         this.btnTestSql.UseVisualStyleBackColor = true;
         this.btnTestSql.Click += new System.EventHandler(this.btnTestSql_Click);
         // 
         // label2
         // 
         this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(265, 179);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(67, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Aplication ID";
         // 
         // nmrcUpDownRecordId
         // 
         this.nmrcUpDownRecordId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.nmrcUpDownRecordId.Location = new System.Drawing.Point(349, 177);
         this.nmrcUpDownRecordId.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
         this.nmrcUpDownRecordId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
         this.nmrcUpDownRecordId.Name = "nmrcUpDownRecordId";
         this.nmrcUpDownRecordId.Size = new System.Drawing.Size(120, 20);
         this.nmrcUpDownRecordId.TabIndex = 1;
         this.nmrcUpDownRecordId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
         // 
         // txtBoxSql
         // 
         this.txtBoxSql.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                     | System.Windows.Forms.AnchorStyles.Left)
                     | System.Windows.Forms.AnchorStyles.Right)));
         this.txtBoxSql.Location = new System.Drawing.Point(6, 19);
         this.txtBoxSql.Multiline = true;
         this.txtBoxSql.Name = "txtBoxSql";
         this.txtBoxSql.Size = new System.Drawing.Size(553, 132);
         this.txtBoxSql.TabIndex = 0;
         // 
         // btnCancel
         // 
         this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.btnCancel.Location = new System.Drawing.Point(505, 449);
         this.btnCancel.Name = "btnCancel";
         this.btnCancel.Size = new System.Drawing.Size(75, 23);
         this.btnCancel.TabIndex = 3;
         this.btnCancel.Text = "Cancel";
         this.btnCancel.UseVisualStyleBackColor = true;
         // 
         // btnOk
         // 
         this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
         this.btnOk.Location = new System.Drawing.Point(409, 449);
         this.btnOk.Name = "btnOk";
         this.btnOk.Size = new System.Drawing.Size(75, 23);
         this.btnOk.TabIndex = 4;
         this.btnOk.Text = "Ok";
         this.btnOk.UseVisualStyleBackColor = true;
         this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
         // 
         // cmbBoxDocType
         // 
         this.cmbBoxDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cmbBoxDocType.FormattingEnabled = true;
         this.cmbBoxDocType.Location = new System.Drawing.Point(15, 26);
         this.cmbBoxDocType.Name = "cmbBoxDocType";
         this.cmbBoxDocType.Size = new System.Drawing.Size(283, 21);
         this.cmbBoxDocType.TabIndex = 5;
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.txtBoxCondition);
         this.groupBox1.Location = new System.Drawing.Point(15, 262);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(565, 181);
         this.groupBox1.TabIndex = 6;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Text of condition";
         // 
         // txtBoxCondition
         // 
         this.txtBoxCondition.Dock = System.Windows.Forms.DockStyle.Fill;
         this.txtBoxCondition.Location = new System.Drawing.Point(3, 16);
         this.txtBoxCondition.Multiline = true;
         this.txtBoxCondition.Name = "txtBoxCondition";
         this.txtBoxCondition.Size = new System.Drawing.Size(559, 162);
         this.txtBoxCondition.TabIndex = 0;
         // 
         // FrmSpecConditionR135
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(592, 484);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.cmbBoxDocType);
         this.Controls.Add(this.btnOk);
         this.Controls.Add(this.btnCancel);
         this.Controls.Add(this.grpBoxSql);
         this.Controls.Add(this.label1);
         this.Name = "FrmSpecConditionR135";
         this.Text = "Special condition for R135";
         this.grpBoxSql.ResumeLayout(false);
         this.grpBoxSql.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.nmrcUpDownRecordId)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.GroupBox grpBoxSql;
      private System.Windows.Forms.Button btnTestSql;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.NumericUpDown nmrcUpDownRecordId;
      private System.Windows.Forms.TextBox txtBoxSql;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.Button btnOk;
      private System.Windows.Forms.ComboBox cmbBoxDocType;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.TextBox txtBoxCondition;
   }
}