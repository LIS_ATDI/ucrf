﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Windows.Forms;
 
namespace XICSM.UcrfRfaNET.HelpClasses.Classes
{
    public class FunctionConvertToDOCX
    {
        public bool ConvertDOCToDOCX(string path)
        {
            bool isSuccess = false;
            try
            {
                Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

                //if (path.ToLower().EndsWith(FormatSource))
                //{
                //if (System.IO.File.Exists(path.Replace("docx", "doc"))) System.IO.File.Delete(path.Replace("docx", "doc"));
                //if (System.IO.File.Exists(path.Replace("docx", "rtf"))) System.IO.File.Delete(path.Replace("docx", "rtf"));
                //if (System.IO.File.Exists(path)) System.IO.File.Copy(path, path.Replace("docx", "doc"));
                //if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

                var sourceFile = new FileInfo(path.Replace("docx", "doc"));
                    var doc = word.Documents.Open(sourceFile.FullName);
                    string newFileName = sourceFile.FullName.Replace("doc", "docx");
                    doc.SaveAs(FileName: newFileName, FileFormat: WdSaveFormat.wdFormatXMLDocument);

                    word.ActiveDocument.Close();
                    word.Quit();

                    isSuccess = true;
                    if (System.IO.File.Exists(path.Replace("docx", "doc"))) System.IO.File.Delete(path.Replace("docx", "doc"));
                //}
            }
            catch (Exception ex)
            {
                try
                {
                    Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

                    //if (System.IO.File.Exists(path.Replace("docx", "doc"))) System.IO.File.Delete(path.Replace("docx", "doc"));
                    //if (System.IO.File.Exists(path.Replace("docx", "rtf"))) System.IO.File.Delete(path.Replace("docx", "rtf"));
                    //if (System.IO.File.Exists(path)) System.IO.File.Copy(path, path.Replace("docx", "rtf"));
                    //if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

                    var sourceFile = new FileInfo(path.Replace("docx", "rtf"));
                    var doc = word.Documents.Open(sourceFile.FullName);
                    string newFileName = sourceFile.FullName.Replace("rtf", "docx");
                    doc.SaveAs(FileName: newFileName, FileFormat: WdSaveFormat.wdFormatXMLDocument);

                    word.ActiveDocument.Close();
                    word.Quit();

                    isSuccess = true;

                    if (System.IO.File.Exists(path.Replace("docx", "rtf"))) System.IO.File.Delete(path.Replace("docx", "rtf"));

                }
                catch (Exception ex_)
                {
                    MessageBox.Show(ex_.Message);
                }
            }

            return isSuccess;
        }
    }
}