﻿using System;
using System.Collections.Generic;
using System.Linq;
using ICSM;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET
{
    //======================================================
    /// <summary>
    /// Работа с лицензиями
    /// </summary>
    internal class CLicence
    {
        //==================================================
        /// <summary>
        /// Ищет лицензии для отдела ВФСР
        /// </summary>
        /// <param name="owner_id">ID владельца</param>
        /// <param name="province">область</param>
        /// <param name="freqTx">массив передающих частот (MHz)</param>
        /// <param name="freqRx">массив принимающих частот (MHz)</param>
        /// <param name="appType">Тип заявки</param>
        /// <returns>список ID лицензий</returns>
        public static List<int> FindLicence(int owner_id, string province, double[] freqTx, double[] freqRx, AppType appType, string standard)
        {
          List<int> retList = new List<int>();
          if (province == null)
            return retList;  // Нечего считать

          Dictionary<string, int> areaCodeDict = new Dictionary<string, int>();
          using (Icsm.LisRecordSet rsArea = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
          {
             rsArea.Select("ID,CODE");
             rsArea.SetWhere("ID", IMRecordset.Operation.Ge, 0);
             for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
             {
                 string code = rsArea.GetS("CODE");
                 int idArea = rsArea.GetI("ID");
                 if (areaCodeDict.ContainsKey(code) == false)
                     areaCodeDict.Add(code, idArea);
             }
          }

          string type = "";
          switch (appType)
          {
            case AppType.AppBS: type = standard; break;
            case AppType.AppRS: type = CRadioTech.RS; break; //"РРЗ"
            case AppType.AppZS: type = CRadioTech.ZS; break; //"СР"
            default: throw new IMException("Licence - unknown application type.");
          }
          //-----------------------------------------------
          IMRecordset ch_allotments = new IMRecordset(ICSMTbl.itblAhAllotments, IMRecordset.Mode.ReadOnly);
          ch_allotments.Select("ID,AREA_ID,AREA_LIST,LIC_ID,AREA_TYPE,TX_LOW_FREQ,TX_HIGH_FREQ,RX_LOW_FREQ,RX_HIGH_FREQ");
          ch_allotments.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, owner_id);
          ch_allotments.SetWhere("Licence.STATE", IMRecordset.Operation.Eq, "P");
          try
          {
            for (ch_allotments.Open(); !ch_allotments.IsEOF(); ch_allotments.MoveNext())
            {
               //------------------------------------------
               // Вытаскиваем ID area
               List<int> area_id = new List<int>();
               string areaType = ch_allotments.GetS("AREA_TYPE");
               if (areaType == "I")
               {
                   area_id.Add(ch_allotments.GetI("AREA_ID"));
               }
               else
               {
                   string listAreaId = ch_allotments.GetS("AREA_LIST");
                   if (listAreaId != null)
                   {
                       string[] parseList = listAreaId.Split(new Char[] { ',' });
                       foreach (string dblVal in parseList)
                       {
                           string tmpCode = dblVal.Trim();
                           if (areaCodeDict.ContainsKey(tmpCode))
                           {
                               int areaId = areaCodeDict[tmpCode];
                               bool needAdd = true;
                               // Проверка на дублирование ID
                               foreach (int id in area_id)
                                   if (areaId == id)
                                   {
                                       needAdd = false;
                                       break;
                                   }
                               if (needAdd)
                                   area_id.Add(areaId);
                           }
                       }
                   }
               }
               //------------------------------------------
               // Проверка на регион
               bool isAreaOk = false;
               foreach(int id in area_id)
               {
                  IMRecordset rs_area = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                  rs_area.Select("ID,NAME");
                  rs_area.SetWhere("ID", IMRecordset.Operation.Eq, id);
                  try
                  {
                     rs_area.Open();
                     if(!rs_area.IsEOF())
                     {
                        string prov = rs_area.GetS("NAME");
                        if((prov != null) && (prov == province))
                           isAreaOk = true;
                     }
                  }
                  finally
                  {
                     rs_area.Close();
                     rs_area.Destroy();
                  }
                  if(isAreaOk == true)
                     break;  //Лицензию уже подходит
               }
               //------------------------------------------
               if(isAreaOk == true)
               {// Проверка частот
                  bool isLicOk = false;
                  //ПРМ
                  double freqMin = ch_allotments.GetD("TX_LOW_FREQ");
                  double freqMax = ch_allotments.GetD("TX_HIGH_FREQ");
                  foreach(double curFreq in freqTx)
                     if((freqMin < curFreq) && (curFreq < freqMax))
                        isLicOk = true;
                  //ПРД
                  freqMin = ch_allotments.GetD("RX_LOW_FREQ");
                  freqMax = ch_allotments.GetD("RX_HIGH_FREQ");
                  foreach (double curFreq in freqRx)
                     if ((freqMin < curFreq) && (curFreq < freqMax))
                        isLicOk = true;
                  //------------
                  if (isLicOk == true)
                  {// Добавляем лицензию в список
                     int licID = ch_allotments.GetI("LIC_ID");
                     if ((licID != 0) && (licID != IM.NullI))
                     {
                        bool add = true;
                        foreach (int locID in retList)
                           if (locID == licID)
                           {
                              add = false;
                              break;
                           }
                        if (add == true)
                        {
                           //-----------------------------
                           // Проверка на соотвествие заяки
                           IMRecordset rsLicence = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                           rsLicence.Select("STANDARD");
                           rsLicence.SetWhere("ID", IMRecordset.Operation.Eq, licID);
                           try
                           {
                              rsLicence.Open();
                              if (!rsLicence.IsEOF() && (type == rsLicence.GetS("STANDARD")))
                                 retList.Add(licID);
                           }
                           finally
                           {
                              rsLicence.Close();
                              rsLicence.Destroy();
                           }
                        }
                     }
                  }
               }
            }
          }
          finally
          {
            ch_allotments.Close();
            ch_allotments.Destroy();
          }
         return retList;
        }

        public class Band
        {
            protected int id;
            public int Id { get {return id; } }
            protected double fLow;
            public double FLow { get {return fLow; } }
            protected double fCenter;
            public double FCenter { get { return fCenter; } }
            protected double fHigh;
            public double FHigh { get {return fHigh; } }
            
            public double Bw { get { return fLow != IM.NullD && fHigh != IM.NullD && fLow < fHigh ? fHigh - fLow : 0.0; } }

            public enum Parity { No, Up, Down }
            protected Parity fParity;
            public Parity FParity { get { return fParity; } }

            public Band(int id, double fLow, double fHigh, Parity fParity)
            {
                this.id = id;
                this.fLow = IM.RoundDeci(fLow, 6);
                this.fHigh = IM.RoundDeci(fHigh, 6);
                this.fCenter = IM.RoundDeci((fLow + fHigh)/2, 6);
                this.fParity = fParity;
            }            
            public Band(int id, double fLow, double fCenter, double fHigh, Parity fParity)
                : this (id, fLow, fHigh, fParity)
            {
                this.fCenter = IM.RoundDeci(fCenter, 6);
            }

            public bool OverlapsWith(Band b) { return this.FLow < b.FHigh && this.FHigh > b.FLow; }
            public bool CoversUp(Band b) { return this.FLow <= b.FLow && this.FHigh >= b.FHigh; }
            public bool FitsIn(Band b) { return this.FLow > b.FLow && this.FHigh < b.FHigh; }
            public bool ConformsByParityWith(Band b) { return FParity != Parity.Up && b.FParity != Parity.Up || FParity != Parity.Down && b.FParity != Parity.Down; }

            public List<CLicence> RefLicences = new List<CLicence>(); // not for licence bands, for station bands only

            /// <summary>
            /// В полосе возвращает пробелы, не покрытые привязанными к полосе лицензиями
            /// </summary>
            /// <returns></returns>
            public Band[] GetGaps()
            {
                List<Band> res = new List<Band>();
                res.Add(new Band(0, fLow, fHigh, fParity));
                foreach (CLicence lic in RefLicences)
                {
                    foreach (Band licb in lic.Bands)
                    {
                        bool reiterateCollection = true;
                        while (reiterateCollection)
                        {
                            reiterateCollection = false;
                            foreach (Band resb in res)
                            {
                                if (licb.OverlapsWith(resb) && licb.ConformsByParityWith(resb))
                                {
                                    if (licb.FLow > resb.FLow)
                                        res.Add(new Band(0, resb.FLow, licb.FLow, resb.FParity));
                                    if (licb.FHigh < resb.FHigh)
                                        res.Add(new Band(0, licb.FHigh, resb.FHigh, resb.FParity));
                                    res.Remove(resb);
                                    // res is modified, so iterator is invalid (unless resb was last element, but don't make code too complicated)
                                    reiterateCollection = true; 
                                    break;
                                }
                            }
                        }
                    }
                }
                return res.ToArray();
            }
        }

        public int Id;
        public string Name;
        public DateTime SignDate;
        public DateTime StartDate;
        public DateTime StopDate;
        public DateTime EndDate;

        public override string ToString()
        {
            return string.Format(CLocaliz.TxT("{0} from {1:d} ({2:d} - {3:d})"),
                                Name,
                                SignDate != IM.NullT ? SignDate : StartDate,
                                StartDate,
                                EndDate != IM.NullT && EndDate < StopDate ? EndDate : StopDate);
        }

        public List<Band> Bands = new List<Band>();

        public CLicence(int id, string name, DateTime signDate, DateTime startDate, DateTime stopDate, DateTime endDate)
        {                                                         
            Id = id;                                   
            Name = name;                     
            SignDate  = signDate;
            StartDate = startDate;
            StopDate  = stopDate;
            EndDate   = endDate;
        }

        static Dictionary<string, string> areaCache = new Dictionary<string, string>();

        public static void ClearAreaCache()
        {
            areaCache.Clear();
        }

        public static void EnsureAreaCache()
        {
            if (areaCache.Count == 0)
            {
                IMRecordset arRs = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                try
                {
                    arRs.Select("NAME,CODE");
                    for (arRs.Open(); !arRs.IsEOF(); arRs.MoveNext())
                        areaCache.Add(arRs.GetS("NAME"), arRs.GetS("CODE"));
                }
                finally
                {
                    arRs.Destroy();
                }
            }
        }

        public static string GetChalWhereClause(int owner_id, string province, string standard, string fPlanTempl, DateTime actDate, bool useStatuses)
        {
            string sActDate = string.Format("'{0,0:d}'", actDate);

            EnsureAreaCache();
            string provCode = "";
            areaCache.TryGetValue(province, out provCode);

            string res = "[Licence.OWNER_ID] = " + owner_id.ToString();
            if (useStatuses)
                res += " and [Licence.STATE] in ('P')";
            res += " and ((" + "[Licence.SIGNING_DATE] is not NULL and trunc([Licence.SIGNING_DATE]) <= TO_DATE(" + sActDate + ")" + ") or trunc([Licence.START_DATE]) <= TO_DATE(" + sActDate + "))";
            res += " and trunc([Licence.STOP_DATE]) >= TO_DATE(" + sActDate + ") and ([Licence.END_DATE] is null or trunc([Licence.START_DATE]) >= TO_DATE(" + sActDate + ")) ";
            res += " and ([AREA_TYPE]='I' and [Area.NAME]='" + province + "'";
                if (!string.IsNullOrEmpty(provCode))
                    res += " or [AREA_TYPE]='L' and instr([AREA_LIST],'" + provCode + "')>0";
            res += ")";
            if (!string.IsNullOrEmpty(standard))
                res += " and [Licence.STANDARD] = '" + standard + "' ";
            if (!string.IsNullOrEmpty(fPlanTempl))
                res += " and upper(trim([Plan.NAME])) = upper(trim('" + fPlanTempl + "'))"; 

            return res;
        }

        /// <summary>
        /// General method for Licence search
        /// </summary>
        /// <param name="owner_id"></param>
        /// <param name="province"></param>
        /// <param name="txBands"></param>
        /// <param name="rxBands"></param>
        /// <param name="standard"></param>
        /// <param name="actDate"></param>
        /// <param name="clearAreaCache"></param>
        /// <returns></returns>
        public static Dictionary<int, CLicence> FindLicences(int owner_id, string province, ref Band[] txBands, ref Band[] rxBands, string standard, string fPlanTempl)
        {
            Dictionary<int, CLicence> retList = new Dictionary<int, CLicence>();
            
            if (province == null || owner_id == IM.NullI)
                return retList;  // Нечего считать            

            EnsureAreaCache();

            if(!areaCache.ContainsKey(province))
                return retList;

            string provCode = "";
            areaCache.TryGetValue(province, out provCode);

            IMRecordset rsChal = new IMRecordset(ICSMTbl.ChAllotments, IMRecordset.Mode.ReadOnly);
            try
            {
                rsChal.Select("ID,LIC_ID,Licence.NAME,TYPE,TX_LOW_FREQ,TX_HIGH_FREQ,RX_LOW_FREQ,RX_HIGH_FREQ,Plan.PAIRING,Plan.NAME");
                rsChal.Select("Licence.SIGNING_DATE,Licence.START_DATE,Licence.STOP_DATE,Licence.END_DATE");
                LicParams licParams = LicParams.GetLicParams();
                string additional = GetChalWhereClause(owner_id, province, standard, fPlanTempl, licParams.ActualDate, licParams.UseLicStatuses);
                rsChal.SetAdditional(additional);
                rsChal.OrderBy("Licence.START_DATE", OrderDirection.Ascending);
                for (rsChal.Open(); !rsChal.IsEOF(); rsChal.MoveNext())
                {
                    int licId = rsChal.GetI("LIC_ID");
                    int chalId = rsChal.GetI("ID");
                    CLicence lic = retList.ContainsKey(licId) ?
                        retList[licId] :
                        new CLicence(licId, rsChal.GetS("Licence.NAME"), rsChal.GetT("Licence.SIGNING_DATE"), rsChal.GetT("Licence.START_DATE"), rsChal.GetT("Licence.STOP_DATE"), rsChal.GetT("Licence.END_DATE"));

                    if (rxBands.Length == 0 && txBands.Length == 0)
                    {
                        if (!retList.ContainsKey(licId))
                            retList.Add(licId, lic);
                    }
                    else if (rsChal.GetS("TYPE") == "F")
                    {
                        Band licRxBand = new Band(chalId, rsChal.GetD("RX_LOW_FREQ"), rsChal.GetD("RX_HIGH_FREQ"), Band.Parity.Up);
                        foreach (Band band in rxBands)
                            if (licRxBand.OverlapsWith(band))       // no extra parity check 
                            {
                                if (!retList.ContainsKey(licId))
                                    retList.Add(licId, lic);
                                lic.Bands.Add(licRxBand);
                                band.RefLicences.Add(lic);
                            }
                        Band licTxBand = new Band(chalId, rsChal.GetD("TX_LOW_FREQ"), rsChal.GetD("TX_HIGH_FREQ"), Band.Parity.Down);
                        foreach (Band band in txBands)
                            if (licTxBand.OverlapsWith(band))       // no extra parity check 
                            {
                                if (!retList.ContainsKey(licId))
                                    retList.Add(licId, lic);
                                lic.Bands.Add(licTxBand);
                                band.RefLicences.Add(lic);
                            }
                    }
                    else if (rsChal.GetS("TYPE") == "C")
                    {
                        IMRecordset rsChann = new IMRecordset(ICSMTbl.itblChAllotCh, IMRecordset.Mode.ReadOnly);
                        try
                        {
                            rsChann.Select("ID,FREQ,BANDWIDTH,OFFSET,PARITY");
                            rsChann.SetWhere("ALLOT_ID", IMRecordset.Operation.Eq, rsChal.GetI("ID"));
                            for (rsChann.Open(); !rsChann.IsEOF(); rsChann.MoveNext())
                            {
                                double freq = rsChann.GetD("FREQ");
                                double bw = rsChann.GetD("BANDWIDTH") / 1000.0;     // kHz in DB
                                double offset = rsChann.GetD("OFFSET") / 1000.0;    // kHz in DB
                                string chPar = rsChann.GetS("PARITY").Trim().ToUpper();

                                Band chBand = new Band(rsChann.GetI("ID"), freq - offset - bw / 2, freq, freq - offset + bw / 2,
                                    chPar == "D" ? Band.Parity.Down : chPar == "U" ? Band.Parity.Up : Band.Parity.No);

                                foreach (Band band in rxBands)
                                    if (((chPar != "D" && band.FParity != Band.Parity.Down) || (chPar != "U" && band.FParity != Band.Parity.Up))
                                        && chBand.OverlapsWith(band))
                                    {
                                        if (!retList.ContainsKey(licId))
                                            retList.Add(licId, lic);
                                        lic.Bands.Add(chBand);
                                        band.RefLicences.Add(lic);
                                    }

                                foreach (Band band in txBands)
                                    if (((chPar != "D" && band.FParity != Band.Parity.Down) || (chPar != "U" && band.FParity != Band.Parity.Up))
                                        && chBand.OverlapsWith(band))
                                    {
                                        if (!retList.ContainsKey(licId))
                                            retList.Add(licId, lic);
                                        lic.Bands.Add(chBand);
                                        band.RefLicences.Add(lic);
                                    }
                            }
                        }
                        finally
                        {
                            rsChann.Destroy();
                        }
                    }
                    else // unknown allotment freq/channel type
                        continue;
                }
            }
            finally
            {
                rsChal.Destroy();
            }
            
            return retList;
        }      
        //==================================================
        /// <summary>
      /// Ищет лицензии РР станций для отдела ВФСР
      /// </summary>
      /// <param name="owner_id">ID владельца</param>
      /// <param name="province">область</param>
      /// <param name="freqTx">массив передающих частот (MHz)</param>
      /// <param name="freqRx">массив принимающих частот (MHz)</param>
      /// <param name="bw">Ширина полосы (MHz)</param>
      /// <returns>список ID лицензий</returns>
        public static List<int> FindLicenceRR(int owner_id, string province, List<double> freqTx, List<double> freqRx, double bw)
      {
         List<int> retList = new List<int>();
         if (province == null)
            return retList;  // Нечего считать
         
         Dictionary<string, int> areaCodeDict = new Dictionary<string, int>();
         using (Icsm.LisRecordSet rsArea = new Icsm.LisRecordSet(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly))
         {
             rsArea.Select("ID,CODE");
             rsArea.SetWhere("ID", IMRecordset.Operation.Ge, 0);
             for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
             {
                 string code = rsArea.GetS("CODE");
                 int idArea = rsArea.GetI("ID");
                 if (areaCodeDict.ContainsKey(code) == false)
                     areaCodeDict.Add(code, idArea);
             }
         }

         string type = "РРЗ";
         //-----------------------------------------------
         IMRecordset ch_allotments = new IMRecordset(ICSMTbl.itblAhAllotments, IMRecordset.Mode.ReadOnly);
         ch_allotments.Select("ID,AREA_ID,AREA_LIST,AREA_TYPE,LIC_ID,TX_LOW_FREQ,TX_HIGH_FREQ,RX_LOW_FREQ,RX_HIGH_FREQ");
         ch_allotments.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, owner_id);
         ch_allotments.SetWhere("Licence.STATE", IMRecordset.Operation.Eq, "P");
         try
         {
            for (ch_allotments.Open(); !ch_allotments.IsEOF(); ch_allotments.MoveNext())
            {
               //------------------------------------------
               // Вытаскиваем ID area
               List<int> area_id = new List<int>();
               string areaType = ch_allotments.GetS("AREA_TYPE");
               if (areaType == "I")
               {
                   area_id.Add(ch_allotments.GetI("AREA_ID"));
               }
               else
               {
                   string listAreaId = ch_allotments.GetS("AREA_LIST");
                   if (listAreaId != null)
                   {
                       string[] parseList = listAreaId.Split(new Char[] {','});
                       foreach (string dblVal in parseList)
                       {
                           string tmpCode = dblVal.Trim();
                           if(areaCodeDict.ContainsKey(tmpCode))
                           {
                               int areaId = areaCodeDict[tmpCode];
                               bool needAdd = true;
                               // Проверка на дублирование ID
                               foreach (int id in area_id)
                                   if (areaId == id)
                                   {
                                       needAdd = false;
                                       break;
                                   }
                               if (needAdd)
                                   area_id.Add(areaId);
                           }
                       }
                   }
               }
               //------------------------------------------
               // Проверка на регион
               bool isAreaOk = false;
               foreach (int id in area_id)
               {
                  IMRecordset rs_area = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                  rs_area.Select("ID,NAME");
                  rs_area.SetWhere("ID", IMRecordset.Operation.Eq, id);
                  try
                  {
                     rs_area.Open();
                     if (!rs_area.IsEOF())
                     {
                        string prov = rs_area.GetS("NAME");
                        if ((prov != null) && (prov == province))
                           isAreaOk = true;
                     }
                  }
                  finally
                  {
                     rs_area.Close();
                     rs_area.Destroy();
                  }
                  if (isAreaOk == true)
                     break;  //Лицензию уже подходит
               }
               //------------------------------------------
               double halfBw = bw / 2.0;
               if (isAreaOk == true)
               {// Проверка частот
                  bool isLicOk = false;
                  //ПРМ
                  double freqMin = ch_allotments.GetD("TX_LOW_FREQ");
                  double freqMax = ch_allotments.GetD("TX_HIGH_FREQ");
                  foreach(double curFreq in freqTx)
                      if (((freqMin < (curFreq + halfBw)) && ((curFreq - halfBw) < freqMax)) || (((curFreq - halfBw) < freqMin) &&  (freqMax < (curFreq + halfBw))))
                        isLicOk = true;
                  //ПРД
                  freqMin = ch_allotments.GetD("RX_LOW_FREQ");
                  freqMax = ch_allotments.GetD("RX_HIGH_FREQ");
                  foreach (double curFreq in freqRx)
                     if (((freqMin < (curFreq + halfBw)) && ((curFreq - halfBw) < freqMax)) || (((curFreq - halfBw) < freqMin) &&  (freqMax < (curFreq + halfBw))))
                        isLicOk = true;
                  //------------
                  if (isLicOk == true)
                  {// Добавляем лицензию в список
                     int licID = ch_allotments.GetI("LIC_ID");
                     if ((licID != 0) && (licID != IM.NullI))
                     {
                        bool add = true;
                        foreach (int locID in retList)
                           if (locID == licID)
                           {
                              add = false;
                              break;
                           }
                        if (add == true)
                        {
                           //-----------------------------
                           // Проверка на соотвествие заяки
                           IMRecordset rsLicence = new IMRecordset(ICSMTbl.itblLicence, IMRecordset.Mode.ReadOnly);
                           rsLicence.Select("STANDARD");
                           rsLicence.SetWhere("ID", IMRecordset.Operation.Eq, licID);
                           try
                           {
                              rsLicence.Open();
                              if (!rsLicence.IsEOF() && (type == rsLicence.GetS("STANDARD")))
                                 retList.Add(licID);
                           }
                           finally
                           {
                              rsLicence.Close();
                              rsLicence.Destroy();
                           }
                        }
                     }
                  }
               }
            }
         }
         finally
         {
            ch_allotments.Close();
            ch_allotments.Destroy();
         }
         return retList;
      }

        /// <summary>
       /// Формирование списка подходящих лицензий с частотами
       /// </summary>
       /// <param name="ownerId">ID владельца</param>
       /// <param name="provName">Название района</param>
       /// <param name="planName">Название плана</param>
       /// <returns>списка лицензий с частотами</returns>
        public static Dictionary<int, List<FreqLicence>> GetLicenceWithFreqs(int ownerId, string provName, string planName)
       {
           string planNameTmp = string.IsNullOrEmpty(planName) ? "" : planName;
           string provNameTmp = string.IsNullOrEmpty(provName) ? "" : provName.ToUpper();

           int planId = IM.NullI;
           IMRecordset r = new IMRecordset(ICSMTbl.itblFreqPlan, IMRecordset.Mode.ReadOnly);
           r.Select("ID,NAME");
           r.SetWhere("NAME", IMRecordset.Operation.Like, planNameTmp);
           try
           {
               r.Open();
               if (!r.IsEOF())
                   planId = r.GetI("ID");
           }
           finally
           {
               if(r.IsOpen())
                   r.Close();
               r.Destroy();
           }
           //---
           string codeArea = "XX";
           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
           rsArea.Select("ID,NAME,CODE,PROVINCE");
           try
           {
               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext() )
               {
                   string prov = rsArea.GetS("NAME").ToUpper();
                   if (prov == provNameTmp)
                   {
                       codeArea = rsArea.GetS("CODE");
                       break;
                   }
               }
           }
           finally
           {
               if(rsArea.IsOpen())
                   rsArea.Close();
               rsArea.Destroy();
           }
           return GetLicenceWithFreqs(ownerId, codeArea, planId);
       }
        /// <summary>
       /// Формирование списка подходящих лицензий с частотами
       /// </summary>
       /// <param name="ownerId">ID владельца</param>
       /// <param name="provCode">Код района</param>
       /// <param name="planId">ID частотного плана</param>
       /// <returns>списка лицензий с частотами</returns>
        public static Dictionary<int, List<FreqLicence>> GetLicenceWithFreqs(int ownerId, string provCode, int planId)
       {
           int provCodeInt = provCode.ToInt32(IM.NullI);
           Dictionary<int, List<FreqLicence>> retDict = new Dictionary<int, List<FreqLicence>>();
           //-----------------------------------------------
           IMRecordset rsChAllotments = new IMRecordset(ICSMTbl.itblAhAllotments, IMRecordset.Mode.ReadOnly);
           rsChAllotments.Select("ID");
           rsChAllotments.Select("Area.CODE");
           rsChAllotments.Select("AREA_LIST");
           rsChAllotments.Select("AREA_TYPE");
           rsChAllotments.Select("LIC_ID");
           rsChAllotments.Select("TX_LOW_FREQ");
           rsChAllotments.Select("TX_HIGH_FREQ");
           rsChAllotments.Select("RX_LOW_FREQ");
           rsChAllotments.Select("RX_HIGH_FREQ");
           rsChAllotments.Select("BW");
           rsChAllotments.Select("TYPE");
           rsChAllotments.Select("PLAN_ID");
           rsChAllotments.Select("CHANNELS");
           rsChAllotments.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerId);
           try
           {
               for (rsChAllotments.Open(); !rsChAllotments.IsEOF(); rsChAllotments.MoveNext())
               {
                   bool isLicenCeCorrect = false;
                   //Проверка на областей
                   if (rsChAllotments.GetS("AREA_TYPE").ToUpper() == "L")
                   {
                       //Список областей
                       string areaCodeList = rsChAllotments.GetS("AREA_LIST");
                       int[] areaCodes = areaCodeList.ToListInt();
                       if (areaCodes.Contains(provCodeInt))
                           isLicenCeCorrect = true;
                   }
                   else
                   {
                       //По одной области
                       string codeArea = rsChAllotments.GetS("Area.CODE");
                       if (codeArea == provCode)
                           isLicenCeCorrect = true;
                   }

                   if (isLicenCeCorrect == false)
                       continue; //Дальше нечего делать

                   List<FreqLicence> listFreq = null;
                   int licId = rsChAllotments.GetI("LIC_ID");
                   if (licId == IM.NullI)
                       continue; //Это херня

                   if (retDict.ContainsKey(licId))
                       listFreq = retDict[licId];
                   else
                   {
                       listFreq = new List<FreqLicence>();
                       retDict.Add(licId, listFreq);
                   }

                   if (rsChAllotments.GetS("TYPE").ToUpper() == "F")
                   {
                       //По частотам
                       double bwDiv2 = rsChAllotments.GetD("BW")*0.001/2.0;
                       FreqLicence newFreq = new FreqLicence();
                       newFreq.TxLowMHz = rsChAllotments.GetD("TX_LOW_FREQ");
                       newFreq.TxHighMHz = rsChAllotments.GetD("TX_HIGH_FREQ");
                       newFreq.RxLowMHz = rsChAllotments.GetD("RX_LOW_FREQ");
                       newFreq.RxHighMHz = rsChAllotments.GetD("RX_HIGH_FREQ");
                       newFreq.TxCentralMHz = newFreq.TxLowMHz + bwDiv2;
                       newFreq.RxCentralMHz = newFreq.RxLowMHz + bwDiv2;
                       listFreq.Add(newFreq);
                   }
                   else
                   {
                       //По каналам
                       int planIdTmp = rsChAllotments.GetI("PLAN_ID");
                       if((planIdTmp == planId) || (planId == IM.NullI))
                       {
                           string channelList = rsChAllotments.GetS("CHANNELS");
                           int[] channels = channelList.ToListInt();
                           listFreq.AddRange(GetFreqsFromPlan(planIdTmp, channels));
                       }
                   }
               }
           }
           finally
           {
               if (rsChAllotments.IsOpen())
                   rsChAllotments.Close();
               rsChAllotments.Destroy();
           }
           return retDict;
       }
        /// <summary>
       /// Возвращает список частот из частотного плана и каналов
       /// </summary>
       /// <param name="planId">ID плана</param>
       /// <param name="chanNum">номера каналов</param>
       /// <returns>список частот из частотного плана и каналов</returns>
        private static FreqLicence[] GetFreqsFromPlan(int planId, params int[] chanNum)
       {
           Dictionary<string, FreqLicence> chanDict = new Dictionary<string, FreqLicence>();
           IMRecordset rsFreqPlanChan = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
           rsFreqPlanChan.Select("FREQ");
           rsFreqPlanChan.Select("BANDWIDTH");
           rsFreqPlanChan.Select("PARITY");
           rsFreqPlanChan.Select("CHANNEL");
           rsFreqPlanChan.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, planId);
           try
           {
               for(rsFreqPlanChan.Open(); !rsFreqPlanChan.IsEOF(); rsFreqPlanChan.MoveNext())
               {
                   string channel = rsFreqPlanChan.GetS("CHANNEL");
                   int chanNumTmp = channel.ToInt32(IM.NullI);
                   if(chanNum.Contains(chanNumTmp))
                   {
                       double freq = rsFreqPlanChan.GetD("FREQ");
                       double bw = rsFreqPlanChan.GetD("BANDWIDTH");
                       if(freq != IM.NullD && bw != IM.NullD)
                       {
                           FreqLicence freqLic;
                           if(chanDict.ContainsKey(channel))
                               freqLic = chanDict[channel];
                           else
                           {
                               freqLic = new FreqLicence();
                               chanDict.Add(channel, freqLic);
                           }
                           string parity = rsFreqPlanChan.GetS("PARITY");
                           double bwDiv2 = bw*0.001/2.0;
                           switch (parity.ToUpper())
                           {
                               case "N":
                                   freqLic.RxHighMHz = freq + bwDiv2;
                                   freqLic.RxLowMHz = freq - bwDiv2;
                                   freqLic.TxHighMHz = freqLic.RxHighMHz;
                                   freqLic.TxLowMHz = freqLic.RxLowMHz;
                                   freqLic.TxCentralMHz = freq;
                                   freqLic.RxCentralMHz = freq;
                                   break;
                               case "L":
                               case "D":
                                   freqLic.RxHighMHz = freq + bwDiv2;
                                   freqLic.RxLowMHz = freq - bwDiv2;
                                   freqLic.RxCentralMHz = freq;
                                   break;
                               case "H":
                               case "U":
                                   freqLic.TxHighMHz = freq + bwDiv2;
                                   freqLic.TxLowMHz = freq - bwDiv2;
                                   freqLic.TxCentralMHz = freq;
                                   break;
                           }
                       }
                   }
               }
           }
           finally
           {
               if(rsFreqPlanChan.IsOpen())
                    rsFreqPlanChan.Close();
               rsFreqPlanChan.Destroy();
           }
           return chanDict.Select(keyValuePair => keyValuePair.Value).ToArray();
       }



        /// <summary>
        /// Возвращает список частот из частотного плана и каналов
        /// </summary>
        /// <param name="planId">ID плана</param>
        /// <returns>список частот из частотного плана и каналов</returns>
        public static FreqLicence[] GetFreqsFromPlan(int planId)
        {
            Dictionary<string, FreqLicence> chanDict = new Dictionary<string, FreqLicence>();
            IMRecordset rsFreqPlanChan = new IMRecordset(ICSMTbl.itblFreqPlanChan, IMRecordset.Mode.ReadOnly);
            rsFreqPlanChan.Select("FREQ");
            rsFreqPlanChan.Select("BANDWIDTH");
            rsFreqPlanChan.Select("PARITY");
            rsFreqPlanChan.Select("CHANNEL");
            rsFreqPlanChan.SetWhere("PLAN_ID", IMRecordset.Operation.Eq, planId);
            try
            {
                for (rsFreqPlanChan.Open(); !rsFreqPlanChan.IsEOF(); rsFreqPlanChan.MoveNext())
                {

                    string channel = rsFreqPlanChan.GetS("CHANNEL");
                    double freq = rsFreqPlanChan.GetD("FREQ");
                    double bw = rsFreqPlanChan.GetD("BANDWIDTH");
                    if (freq != IM.NullD && bw != IM.NullD)
                    {
                        FreqLicence freqLic;
                        if (chanDict.ContainsKey(channel))
                            freqLic = chanDict[channel];
                        else
                        {
                            freqLic = new FreqLicence();
                            chanDict.Add(channel, freqLic);
                        }
                        string parity = rsFreqPlanChan.GetS("PARITY");
                        double bwDiv2 = bw * 0.001 / 2.0;
                        switch (parity.ToUpper())
                        {
                            case "N":
                                freqLic.RxHighMHz = freq + bwDiv2;
                                freqLic.RxLowMHz = freq - bwDiv2;
                                freqLic.TxHighMHz = freqLic.RxHighMHz;
                                freqLic.TxLowMHz = freqLic.RxLowMHz;
                                freqLic.TxCentralMHz = freq;
                                freqLic.RxCentralMHz = freq;
                                break;
                            case "L":
                            case "D":
                                freqLic.RxHighMHz = freq + bwDiv2;
                                freqLic.RxLowMHz = freq - bwDiv2;
                                freqLic.RxCentralMHz = freq;
                                break;
                            case "H":
                            case "U":
                                freqLic.TxHighMHz = freq + bwDiv2;
                                freqLic.TxLowMHz = freq - bwDiv2;
                                freqLic.TxCentralMHz = freq;
                                break;
                        }
                    }

                }
            }
            finally
            {
                if (rsFreqPlanChan.IsOpen())
                    rsFreqPlanChan.Close();
                rsFreqPlanChan.Destroy();
            }
            return chanDict.Select(keyValuePair => keyValuePair.Value).ToArray();
        }

    }



    internal class FreqLicence
    {
        public double TxLowMHz { get; set; }
        public double TxHighMHz { get; set; }
        public double RxLowMHz { get; set; }
        public double RxHighMHz { get; set; }
        //----
        public double TxCentralMHz { get; set; }
        public double RxCentralMHz { get; set; }
        //----
        public Int64 TxLowMHzInt64 { get { return (Int64) (TxLowMHz*1000000.0 + 0.5); }}
        public Int64 TxHighMHzInt64 { get { return (Int64)(TxHighMHz * 1000000.0 + 0.5); } }
        public Int64 RxLowMHzInt64 { get { return (Int64)(RxLowMHz * 1000000.0 + 0.5); } }
        public Int64 RxHighMHzInt64 { get { return (Int64)(RxHighMHz * 1000000.0 + 0.5); } }
        //----
        public FreqLicence()
        {
            TxLowMHz = 0.0;
            TxHighMHz = 0.0;
            RxLowMHz = 0.0;
            RxHighMHz = 0.0;
            TxCentralMHz = 0;
            RxCentralMHz = 0;
        }
    }
    //----

    internal class LicParams
    {
        protected LicParams() 
        { 
            IsCurrentDate = true;
            UseLicStatuses = true;
            AutoLinkLicences = false;
            IgnoreBandGaps = false;
            AutoExcludeProlongated = true;
            CheckProlongatedConsistency = true;
            AutoSetTechno = false;
        }
        static protected LicParams _params = null;
        static public LicParams GetLicParams()
        {
            if (_params == null)
            {
                _params = new LicParams();
            }
            return _params;
        }

        public bool IsCurrentDate { get; set; }
        public bool UseLicStatuses { get; set; }
        public bool AutoLinkLicences { get; set; }
        public bool IgnoreBandGaps { get; set; }
        public bool AutoExcludeProlongated { get; set; }
        public bool CheckProlongatedConsistency { get; set; }
        public bool AutoSetTechno { get; set; }
        DateTime actualDate = DateTime.Today;
        public DateTime ActualDate
        {
            get { return IsCurrentDate ? DateTime.Now : actualDate; }
            set { actualDate = value; IsCurrentDate = IsCurrentDate; }
        }
    }
}
