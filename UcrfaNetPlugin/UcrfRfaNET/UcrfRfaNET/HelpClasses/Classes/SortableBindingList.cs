﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace XICSM.UcrfRfaNET.HelpClasses.Classes
{
    public class SortableBindingList<T> : BindingList<T>
    {
        private bool _isSorted = false;
        private ListSortDirection _direction = ListSortDirection.Ascending;
        private PropertyDescriptor _property;
        /// <summary>
        /// Поддержка сортировки
        /// </summary>
        protected override bool SupportsSortingCore
        {
            get { return true; }
        }
        /// <summary>
        /// Применить сортировку
        /// </summary>
        /// <param name="property">Свойство</param>
        /// <param name="direction">Направление</param>
        protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
        {
            // Get list to sort
            List<T> items = this.Items as List<T>;
            // Apply and set the sort, if items to sort
            if (items != null)
            {
                PropertyComparer<T> pc = new PropertyComparer<T>(property, direction);
                items.Sort(pc);
                _isSorted = true;
                _direction = direction;
                _property = property;
            }
            else
            {
                _isSorted = false;
            }
            // Let bound controls know they should refresh their views
            this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        protected override ListSortDirection SortDirectionCore
        {
            get{ return _direction; }
        }
        protected override PropertyDescriptor SortPropertyCore
        {
            get{ return _property; }
        }
        /// <summary>
        /// Флаг сортировки
        /// </summary>
        protected override bool IsSortedCore
        {
            get { return _isSorted; }
        }
        /// <summary>
        /// Удалить сортировку
        /// </summary>
        protected override void RemoveSortCore()
        {
            _isSorted = false;
        }
    }
    /// <summary>
    /// Класс сравнения
    /// </summary>
    /// <typeparam name="T">Тип спавнения</typeparam>
    public class PropertyComparer<T> : IComparer<T>
    {
        private PropertyDescriptor _property;
        private ListSortDirection _direction;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="property">Свойство</param>
        /// <param name="direction">Направление</param>
        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
        {
            _property = property;
            _direction = direction;
        }
        #region IComparer<T>
        public int Compare(T xValue, T yValue)
        {
            // Get property values
            object valueX = GetPropertyValue(xValue, _property.Name);
            object valueY = GetPropertyValue(yValue, _property.Name);

            // Determine sort order
            if (_direction == ListSortDirection.Ascending)
            {
                return CompareAscending(valueX, valueY);
            }
            else
            {
                return CompareDescending(valueX, valueY);
            }
        }
        public bool Equals(T xValue, T yValue)
        {
            return xValue.Equals(yValue);
        }
        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
        #endregion
        
        private int CompareAscending(object xValue, object yValue)
        {
            int result;
            // If values implement IComparer
            if (xValue is IComparable)
            {
                result = ((IComparable)xValue).CompareTo(yValue);
            }
            // If values don't implement IComparer but are equivalent
            else if (xValue.Equals(yValue))
            {
                result = 0;
            }
            // Values don't implement IComparer and are not equivalent, so compare as string values
            else result = xValue.ToString().CompareTo(yValue.ToString());
            // Return result
            return result;
        }

        private int CompareDescending(object xValue, object yValue)
        {
            // Return result adjusted for ascending or descending sort order ie
            // multiplied by 1 for ascending or -1 for descending
            return CompareAscending(xValue, yValue) * -1;
        }

        private object GetPropertyValue(T value, string property)
        {
            // Get property
            PropertyInfo propertyInfo = value.GetType().GetProperty(property);
            // Return value
            return propertyInfo.GetValue(value, null);
        }
    }
}
