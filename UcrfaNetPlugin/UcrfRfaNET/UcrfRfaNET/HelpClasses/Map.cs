﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Diagramm;
using ICSM;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.HelpClasses;

namespace XICSM.UcrfRfaNET.Map
{
    public partial class MapForm : FBaseForm
    {
        public bool IsInit { get; set; }
        
        public MapForm()
        {
            InitializeComponent();
            _mapControl.Dock = DockStyle.Fill;
            _mapControl.TabIndex = 1;
            CLocaliz.TxT(this);
        }

        public void Initialization()
        {
            _mapControl.Init();
            _mapControl.SetCenter(31.25, 48.60);
            _mapControl.SetScale(1545.0);
            IsInit = true;
        }

        public void Clear()
        {
            _mapControl.Clear(-1, true);
        }

        //================================================================
        /// <summary>
        /// Отображаем станцию определенным цветом и радиусом круга
        /// </summary>
        /// <param name="lon">Долгота</param>
        /// <param name="lat">Широта</param>
        /// <param name="name">Название станции</param>
        /// <param name="hint">Описание станции</param>
        /// <param name="txMap">Параметры станции</param>
        public void AddPoint(double lon, double lat, string name, string hint, TxOnMap txMap)
        {
            if (IsInit)
            {
                _mapControl.ShowPoint(lon, lat, txMap.LineColor, txMap.width, LisMapWf.PointType.ptPoint, name, hint);//
                                        //, txMap.FontName, txMap.FontSize, txMap.FontColor, txMap.IsBold, txMap.IsItalic, txMap.IsUnderline);
                _mapControl.FitObjects();
            }
        }

        public void AddTransmitter(double lon, double lat, string name, string hint, TxOnMap txMap)
        {
            if (IsInit)
            {
                _mapControl.ShowStation(lon, lat, name, hint);//, txMap.FontName, txMap.FontSize,
                                   //txMap.FontColor, txMap.IsBold, txMap.IsItalic, txMap.IsUnderline);
                _mapControl.FitObjects();
            }
        }

        public void AddLink(double lon1, double lat1, double lon2, double lat2)
        {
            if (IsInit)
            {
                _mapControl.ShowLink(lon1, lat1, lon2, lat2, 3, LisMapWf.MapArrowType.matArrow);
                _mapControl.FitObjects();
            }
        }

        public void CloseMap()
        {
            if (IsInit)
            {
                //_mapControl.CloseMap();
            }
        }

        public void RefreshMap()
        {
            if (IsInit)
            {
                _mapControl.Refresh();
            }
        }


        public void ShowForm()
        {
            if (IsInit)
            {
                this.ShowDialog();
            }
        }

        /// <summary>
        /// Формирование диаграммы направлености
        /// </summary>
        public void AddDiagram(double longitude, double latitude, TxOnMap stat)
        {
            if (IsInit)
            {
                //Міні фабрика для створення станції
                object statObj;
                if (stat.tableName == ICSMTbl.itblMobStation)
                    statObj = new CalculationVRR.Station(stat.tableName, stat.txId);
                else if (stat.tableName == ICSMTbl.itblFM || stat.tableName == ICSMTbl.itblTV || stat.tableName == ICSMTbl.itblDVBT || stat.tableName == ICSMTbl.itblTDAB)
                    statObj = new CalculationVRS.Station(stat.tableName, stat.txId);
                else
                    statObj = new Station(stat.tableName, stat.txId);


                AntennaDiagramm diagH = new AntennaDiagramm();
                double x = longitude;
                double y = latitude;
                int step = 1;
                double gMax;
                string aDiagH;
                double azimuth;
                //Отримати параметри станції
                GetParamStat(statObj, out gMax, out aDiagH, out azimuth);

                //Перевести vector в points
                aDiagH = ConvVectToPnt(aDiagH);

                diagH.SetMaximalGain(gMax);
                diagH.Build(aDiagH);
                double L = 1;
                List<double> xDnList = new List<double>();
                List<double> yDnList = new List<double>();
                List<double> gList = new List<double>();
                //Нормалізація Gmax
                for (int alpha = 0; alpha < 360; alpha += step)
                {
                    double gCurr = diagH.GetLossesAmount(Math.Abs(alpha - azimuth));
                    //if (aDiagH.StartsWith("POINT"))
                        //gCurr = -gCurr;
                    gList.Add(gCurr);
                }
                double maxG = gList.Max();
                gList.Clear();
                //Покроково сформувати всі точки(лінії) ДН               
                CalculateG(aDiagH, diagH, gList, step, maxG);
                //Сформувати азимут...
                CheckAzimuth(ref gList, azimuth);
                int stepGlist = 0;

                CalculatePositionDN(gList, step, x, L, stepGlist, y, xDnList, yDnList);

                List<LisMapWf.Point<double>> points = new List<LisMapWf.Point<double>>();
                for (int i = 0; i < xDnList.Count && i < yDnList.Count; ++i)
                    points.Add(new LisMapWf.Point<double>(xDnList[i], yDnList[i]));

                _mapControl.ShowContour(points.ToArray(), "", "");
                _mapControl.FitObjects();

                //Показати додаткові параметри станції
                ShowAdditionalParamStat(stat, statObj, x, y);
            }
        }

        /// <summary>
        /// Обрахунок відносного підсилення для кожної точки азимуту
        /// </summary>
        /// <param name="aDiagH"></param>
        /// <param name="diagH"></param>
        /// <param name="gList"></param>
        /// <param name="step"></param>
        /// <param name="maxG"></param>
        private void CalculateG(string aDiagH, AntennaDiagramm diagH, List<double> gList, int step, double maxG)
        {
            for (int alpha = 0; alpha < 360; alpha += step)
            {
                double gCurr = diagH.GetLossesAmount(Math.Abs(alpha));
                gCurr = gCurr - maxG;
                if (gCurr <= -30)
                    gCurr = -30;
                //if (aDiagH.StartsWith("POINT"))
                    //gCurr = -gCurr;
                gList.Add(gCurr);
            }
        }

        /// <summary>
        /// Обрахунок позиції ДН для кожної точки азимуту
        /// </summary>
        /// <param name="gList"></param>
        /// <param name="step"></param>
        /// <param name="x"></param>
        /// <param name="L"></param>
        /// <param name="stepGlist"></param>
        /// <param name="y"></param>
        /// <param name="xDnList"></param>
        /// <param name="yDnList"></param>
        private void CalculatePositionDN(List<double> gList, int step, double x, double L, int stepGlist, double y, List<double> xDnList, List<double> yDnList)
        {
            for (int alpha = 0; alpha < 360; alpha += step)
            {
                int signX = Math.Sign(Math.Sin(alpha * Math.PI / 180));
                int signY = Math.Sign(Math.Cos(alpha * Math.PI / 180));

                double xdn = x + signX * ((L * (30 + gList[stepGlist]) / 30) / 111) * (Math.Abs(Math.Sin(alpha * Math.PI / 180)) / Math.Cos(y * Math.PI / 180));
                double ydn = y + signY * ((L * (30 + gList[stepGlist]) / 30) / 111) * (Math.Abs(Math.Cos(alpha * Math.PI / 180)));

                //Якщо коеф. підсилення менше деякого порогу
                if (gList[stepGlist] <= -30)
                {
                    xdn = x;
                    ydn = y;
                }
                xDnList.Add(xdn);
                yDnList.Add(ydn);
                stepGlist++;
            }
        }

        /// <summary>
        /// Показати додаткові параметри станції
        /// </summary>
        /// <param name="stat">станція</param>
        /// <param name="statObj"></param>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <param name="mapObj">контекст карти</param>
        private void ShowAdditionalParamStat(TxOnMap stat, object statObj, double x, double y)
        {

            if (stat.tableName == ICSMTbl.MobStation2 || stat.tableName == ICSMTbl.itblMobStation || stat.tableName == ICSMTbl.itblMicrowa)
            {
                string lbl = stat.Address;//"Lat: " + x.Round(3).LongDECToString() + " " + "Lon: " + y.Round(3).LatDECToString();
                string hint = "";
                if (statObj is CalculationVRR.Station)
                {
                    CalculationVRR.Station tempStat = statObj as CalculationVRR.Station;
                    List<double> freqTx = tempStat.STFrequncyTX;
                    freqTx.Sort();
                    lbl += " BW: " + tempStat._bwTX + " MHz";
                    lbl += " " + tempStat.StationName;
                    string tempFreq = "";
                    if (freqTx.Count == 1)
                        tempFreq = "TX: " + freqTx[0] + " MHz";
                    if (freqTx.Count == 2)
                        tempFreq = "TX: " + freqTx.Min() + " , " + freqTx.Max() + " MHz";
                    if (freqTx.Count > 2)
                        tempFreq = "TX: " + freqTx.Min() + " - " + freqTx.Max() + " MHz";
                    lbl += " " + tempFreq;
                    for (int i = 0; i < freqTx.Count; i++)
                        hint += freqTx[i] + "\r\n";
                }

                if (statObj is Station)
                {
                    Station tempStat = statObj as Station;
                    List<double> freqTx = tempStat.STFrequncyTX;
                    freqTx.Sort();
                    lbl += " BW: " + tempStat._bwTX + " MHz";
                    lbl += " " + tempStat.Name;
                    string tempFreq = "";
                    if (freqTx.Count == 1)
                        tempFreq = "TX: " + freqTx[0] + " MHz";
                    if (freqTx.Count == 2)
                        tempFreq = "TX: " + freqTx.Min() + " , " + freqTx.Max() + " MHz";
                    if (freqTx.Count > 2)
                        tempFreq = "TX: " + freqTx.Min() + " - " + freqTx.Max() + " MHz";
                    lbl += " " + tempFreq;
                    for (int i = 0; i < freqTx.Count; i++)
                        hint += freqTx[i] + "\r\n";
                }
                AddPoint(x, y, lbl, hint, stat);
                //_mapControl.ShowPoint(x, y, stat.LineColor,   lbl, hint, stat.FontName, stat.FontSize, stat.FontColor, stat.IsBold, stat.IsItalic, stat.IsUndernet);
                //_mapControl.ShowStation(x, y, lbl, hint, stat.FontName, stat.FontSize, stat.FontColor, stat.IsBold, stat.IsItalic, stat.IsUndernet);
            }
        }

        /// <summary>
        /// Модифікація списку точок в залежності від азимута
        /// </summary>
        /// <param name="list"> список частот</param>
        /// <param name="azimuth">азимут</param>
        private void CheckAzimuth(ref List<double> list, double azimuth)
        {
            double currDegree = 0.0;
            double tempVar = 0.0;
            List<double> tmpList = new List<double>();
            double stepDegree = 360 / list.Count;
            int i = 0;
            while (currDegree < azimuth)
            {
                if (list.Count > 0)
                {
                    tempVar = list.Last();
                    list.RemoveAt(list.Count - 1);
                    list.Insert(0, tempVar);
                }
                currDegree += stepDegree;
                i++;
            }
            list.AddRange(tmpList);
        }

        /// <summary>
        /// Перетворення діаграми направленості вектора в точки
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string ConvVectToPnt(string s)
        {
            string tmpStr = "POINTS ";
            List<string> tmpList = new List<string>();
            if (!s.StartsWith("VECTOR"))
                return s;
            tmpList = s.Split(' ').ToList();
            double step = 0.0;
            if (tmpList.Count > 1)
                step = tmpList[1].ToDouble(IM.NullD);
            if (tmpList.Count > 2)
            {
                double currStep = 0;
                for (int i = 2; i < tmpList.Count; i++)
                {
                    tmpStr += currStep.Round(3) + " " + tmpList[i] + " ";
                    currStep += step;
                }
            }
            return tmpStr;
        }

        private void GetParamStat(object statObj, out double gMax, out string aDiagH, out double azimuth)
        {
            if (statObj is CalculationVRR.Station)
            {
                gMax = ((CalculationVRR.Station)statObj)._gMax;
                aDiagH = ((CalculationVRR.Station)statObj)._aDiagH;
                azimuth = ((CalculationVRR.Station)statObj)._azimuth;
            }
            else if (statObj is CalculationVRS.Station)
            {
                gMax = ((CalculationVRS.Station)statObj)._gMax;
                aDiagH = ((CalculationVRS.Station)statObj)._aDiagH;
                azimuth = ((CalculationVRS.Station)statObj)._azimuth;
            }
            else
            {
                gMax = ((Station)statObj)._gMax;
                aDiagH = ((Station)statObj)._aDiagH;
                azimuth = ((Station)statObj)._azimuth;
            }
        }
    }

    //======================================================
    //======================================================
    //======================================================

    public static class TxsOnMap
    {
        private static List<TxOnMap> specStationList = new List<TxOnMap>();
        private static List<TxOnMap> stationList = new List<TxOnMap>();
        private static MapForm map = null;


        public static void InitMap()
        {
            if (map == null)
            {
                map = new MapForm();
                map.Initialization();
                map.FormClosed += new FormClosedEventHandler(map_FormClosed);
            }
            else
            {
                map.Show();
                map.Clear();
            }
        }

        static void map_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClearStationList();
            map = null;
        }

        public static void AddTx(TxOnMap tx)
        {
            stationList.Add(tx);
        }
        //===================================================
        // Добавляем станцию в специальный список
        public static void AddSpecStation(TxOnMap tx)
        {
            tx.LineColor = 0x000000FF;
            tx.width = 10;
            specStationList.Add(tx);
        }
        //===================================================
        //Очистить список обычных станций
        public static void ClearStationList()
        {
            stationList.Clear();
        }
        //===================================================
        //Очистить список специальных станций
        public static void ClearSpecStationList()
        {
            specStationList.Clear();
        }
        //===================================================
        //Очистить ВСЕ станции
        public static void ClearAllStationList()
        {
            ClearSpecStationList();
            ClearStationList();
        }
        //===================================================
        /// <summary>
        /// Добавляем станцию на карту
        /// </summary>
        /// <param name="station">данные об станции</param>
        private static void AddStationOnMap(TxOnMap station)
        {
            string tableName = station.tableName;
            int _tableID = station.txId;
            if (tableName.Equals("MOB_STATION") || tableName.Equals("MOB_STATION2"))
            {
                Positions positions = BaseAppClass.GetSitePositions(tableName, _tableID);
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("Position.LONGITUDE,Position.LATITUDE,STANDARD,Position.REMARK");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double longitude = r.GetD("Position.LONGITUDE");
                        double latitude = r.GetD("Position.LATITUDE");
                        string identificator = positions.FullAddressPositionA;
                        StringBuilder hint = new StringBuilder(r.GetS("STANDARD"));
                        {// Заполняем частоты
                            ApplSource.Classes.MobFreq[] freqs = ApplSource.Classes.MobFreqManager.LoadFreq(_tableID, tableName);
                            foreach (ApplSource.Classes.MobFreq freqItem in freqs)
                            {
                                hint.Append(string.Format("{0}{1}", "\n\r", freqItem.TxMHz));
                            }
                        }
                        station.Address = identificator;
                        if(station.IsCalculation == false)
                            map.AddPoint(longitude, latitude, identificator, hint.ToString(), station);
                        else
                            map.AddDiagram(longitude, latitude, station);
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if (tableName.Equals("EARTH_STATION"))
            {
                Positions positions = BaseAppClass.GetSitePositions(tableName, _tableID);
                IMRecordset r = new IMRecordset("ESTA_ASSGN", IMRecordset.Mode.ReadOnly);
                r.Select("FREQ,Group.Antenna.Station.LONGITUDE,Group.Antenna.Station.LATITUDE,Group.Antenna.Station.Position.REMARK");
                r.SetWhere("Group.Antenna.Station.ID", IMRecordset.Operation.Eq, _tableID);
                r.SetWhere("Group.Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double _longitude = r.GetD("Group.Antenna.Station.LONGITUDE");
                        double _latitude = r.GetD("Group.Antenna.Station.LATITUDE");
                        string _identificator = positions.FullAddressPositionA;
                        StringBuilder _hint = new StringBuilder();
                        for (; !r.IsEOF(); r.MoveNext())
                        {
                            if (_hint.Length > 0)
                                _hint.Append("\n\r");
                            _hint.Append(r.GetD("FREQ").ToString());
                        }
                        map.AddPoint(_longitude, _latitude, _identificator, _hint.ToString(), station);
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if (tableName.Equals("MICROWA"))
            {
                Positions positions = BaseAppClass.GetSitePositions(tableName, _tableID);
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("StationA.TX_FREQ,StationA.Position.LONGITUDE,StationA.Position.LATITUDE,StationA.Position.REMARK");
                r.Select("StationB.TX_FREQ,StationB.Position.LONGITUDE,StationB.Position.LATITUDE,StationB.Position.REMARK");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);

                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double _longitudeA = r.GetD("StationA.Position.LONGITUDE");
                        double _latitudeA = r.GetD("StationA.Position.LATITUDE");
                        double _longitudeB = r.GetD("StationB.Position.LONGITUDE");
                        double _latitudeB = r.GetD("StationB.Position.LATITUDE");
                        string _textA = positions.FullAddressPositionA;
                        string _textB = positions.FullAddressPositionB;
                        string _hintA = r.GetD("StationA.TX_FREQ").ToString();
                        string _hintB = r.GetD("StationB.TX_FREQ").ToString();
                        if (_longitudeA != IM.NullD && _latitudeA != IM.NullD && _longitudeB != IM.NullD && _latitudeB != IM.NullD)
                        {
                            map.AddPoint(_longitudeA, _latitudeA, _textA, _hintA, station);
                            map.AddPoint(_longitudeB, _latitudeB, _textB, _hintB, station);
                            map.AddLink(_longitudeA, _latitudeA, _longitudeB, _latitudeB);
                        }
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if (tableName.Equals(ICSMTbl.itblMicrows))
            {
                int _ID = IM.NullI;
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("MW_ID");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        _ID = r.GetI("MW_ID");
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
                if (_ID != IM.NullI)
                {
                    TxOnMap tx = new TxOnMap(ICSMTbl.itblMicrowa, _ID);
                    AddStationOnMap(tx);
                }
            }
            else if ((tableName.Equals(ICSMTbl.itblTV)) || (tableName.Equals(ICSMTbl.itblDVBT)))
            {
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("Position.LONGITUDE,Position.LATITUDE,Position.NAME,CHANNEL,STANDARD");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double _longitude = r.GetD("Position.LONGITUDE");
                        double _latitude = r.GetD("Position.LATITUDE");
                        string _identificator = r.GetS("STANDARD") + " " + r.GetS("Position.NAME") + " " + r.GetS("CHANNEL");
                        StringBuilder _hint = new StringBuilder("");
                        map.AddPoint(_longitude, _latitude, _identificator, _hint.ToString(), station);
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if ((tableName.Equals(ICSMTbl.itblFM)) || (tableName.Equals(ICSMTbl.itblTDAB)))
            {
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("Position.LONGITUDE,Position.LATITUDE,Position.NAME,FREQ,STANDARD");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double _longitude = r.GetD("Position.LONGITUDE");
                        double _latitude = r.GetD("Position.LATITUDE");
                        string _identificator = r.GetS("STANDARD") + " " + r.GetS("Position.NAME") + " " + r.GetD("FREQ").ToString("F3");
                        StringBuilder _hint = new StringBuilder("");
                        map.AddPoint(_longitude, _latitude, _identificator, _hint.ToString(), station);
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if (tableName.Equals(ICSMTbl.FxLkClink))
            {
                IMRecordset r = new IMRecordset(tableName, IMRecordset.Mode.ReadOnly);
                r.Select("StationA.TX_FREQ,StationA.LONGITUDE,StationA.LATITUDE");
                r.Select("StationB.TX_FREQ,StationB.LONGITUDE,StationB.LATITUDE");
                r.SetWhere("ID", IMRecordset.Operation.Eq, _tableID);

                try
                {
                    r.Open();
                    if (!r.IsEOF())
                    {
                        double _longitudeA = r.GetD("StationA.LONGITUDE");
                        double _latitudeA = r.GetD("StationA.LATITUDE");
                        double _longitudeB = r.GetD("StationB.LONGITUDE");
                        double _latitudeB = r.GetD("StationB.LATITUDE");
                        string _textA = "";
                        string _textB = "";
                        string _hintA = r.GetD("StationA.TX_FREQ").ToString();
                        string _hintB = r.GetD("StationB.TX_FREQ").ToString();
                        if (_longitudeA != IM.NullD && _latitudeA != IM.NullD)
                        {
                            map.AddPoint(_longitudeA, _latitudeA, _textA, _hintA, station);
                        }

                        if (_longitudeB != IM.NullD && _latitudeB != IM.NullD)
                        {
                            map.AddPoint(_longitudeB, _latitudeB, _textB, _hintB, station);
                        }
                        if (_longitudeA != IM.NullD && _latitudeA != IM.NullD && _longitudeB != IM.NullD && _latitudeB != IM.NullD)
                            map.AddLink(_longitudeA, _latitudeA, _longitudeB, _latitudeB);
                    }
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }
            else if (tableName.Equals(ICSMTbl.itblPositionBro) || tableName.Equals(ICSMTbl.itblPositionEs) ||
                tableName.Equals(ICSMTbl.itblPositionFmn) || tableName.Equals(ICSMTbl.itblPositionHf) ||
                tableName.Equals(ICSMTbl.itblPositionMob2) || tableName.Equals(ICSMTbl.itblPositionMw) ||
                tableName.Equals(ICSMTbl.itblPositionWim) || tableName.Equals(PlugTbl.XfaPosition))
            {
                PositionState pos = new PositionState();
                pos.LoadStatePosition(_tableID, tableName);
                map.AddPoint(pos.LonDms, pos.LatDms, pos.FullAddress, "", station);
            }

        }

        public static void ShowMap()
        {
            //map.ReInitialization();
            using (LisProgressBar pb = new LisProgressBar("Загрузка карти..."))
            {
                int index = 0;
                int maxValue = specStationList.Count + stationList.Count;
                pb.SetBig("Загрузка даних для відображення...");
                pb.SetSmall(index++);
                InitMap();
                //Специальные станции
                foreach (TxOnMap tx in specStationList)
                {
                    pb.SetSmall(index++, maxValue);
                    AddStationOnMap(tx);
                }
                //Обычные станции         
                foreach (TxOnMap tx in stationList)
                {
                    pb.SetSmall(index++, maxValue);
                    AddStationOnMap(tx);
                }
            }
            map.Show();
        }
    }

    //======================================================
    /// <summary>
    /// Структура для отображения на карте станции
    /// </summary>
    public class TxOnMap
    {
        public string tableName;
        public int txId;
        //public int color;
        public int width;
        private uint _fontColor;
        public string FontName { get; set; }
        public int FontSize { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public bool IsUnderline { get; set; }
        public bool IsCalculation { get; set; }
        public string Address { get; set; }
        public uint FontColor
        {
            get { return _fontColor; }
            set
            {
                uint tmpVal = value & 0x00FFFFFF;
                if (tmpVal != _fontColor)
                    _fontColor = tmpVal;
            }
        }

        private uint _lineColor;
        public uint LineColor
        {
            get { return _lineColor; }
            set
            {
                uint tmpVal = value & 0x00FFFFFF;
                if (tmpVal != _lineColor)
                    _lineColor = tmpVal;
            }
        }


        public bool IsFontCorect
        {
            get
            {
                bool retVal = true;
                if (string.IsNullOrEmpty(FontName))
                    retVal = false;
                if (FontSize < 0)
                    retVal = false;
                return retVal;
            }
        }


        // Обычная станция
        public TxOnMap(string name, int id) : this(name, id, 0x00CC3333, 5) { }
        public TxOnMap(string name, int id, Color color) : this(name, id, color.ToArgb(), 5) { }
        // Станция из специальными параметрами
        public TxOnMap(string name, int id, int color, int width)
        {
            this.txId = id;
            this.tableName = name;
            this.LineColor = (uint)color;
            this.width = width;
            this.FontName = "Arial";//Lis.MapControl.FontName;
            this.FontColor = 0x00000000;// Lis.MapControl.FontColor;
            this.FontSize = 10;// Lis.MapControl.FontSize;
            this.IsBold = true;// Lis.MapControl.IsBold;
            this.IsItalic = true;// Lis.MapControl.IsItalic;
            this.IsUnderline = false;//Lis.MapControl.IsUnderline;
            this.IsCalculation = false;
            this.Address = "";
        }
    }
}
