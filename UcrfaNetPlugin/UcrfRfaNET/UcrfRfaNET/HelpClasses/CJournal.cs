﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ICSM;
using XICSM.UcrfRfaNET;
using System.IO;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET
{
    class CJournal
    {
        private static List<string> journal = new List<string>();
        private static string comments = "";
        public static string Message { get { return comments; } }

        public static void ClearList()
        {
            journal.Clear();
            comments = "";
        }

        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj">объект с измененными данными</param>		
        /// <returns>новый объект сайта</returns>
        public static void CheckTable(string fromTable, int fromId, IMObject obj)
        {
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }

            IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
            r.Open();
            if (!r.IsEOF())
            {
                IMObject oldObj = IMObject.LoadFromDB(fromTable, fromId);
                foreach (OrmField fld in fldMas)
                {
                    if (fld == null)
                        continue;
                    try
                    {
                        if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME && oldObj.GetT(fld.Name) != obj.GetT(fld.Name))
                            journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + oldObj.GetT(fld.Name).ToString() + "' -> '" + obj.GetT(fld.Name).ToString() + "'");
                        else if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                        {
                            double valOld = IM.RoundDeci(oldObj.GetD(fld.Name), 3);
                            double valNew = IM.RoundDeci(obj.GetD(fld.Name), 3);
                            if (valOld != valNew)
                                journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + valOld.ToString("F3") + "' -> '" + valNew.ToString("F3") + "'");
                        }
                        else if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING && oldObj.GetS(fld.Name) != obj.GetS(fld.Name))
                            journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + oldObj.GetS(fld.Name) + "' -> '" + obj.GetS(fld.Name) + "'");
                    }
                    catch { }
                }
            }
            r.Close();
            r.Destroy();
        }

        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj">Рекордсет с измененными данными</param>
        /// <param name="attrs">Строка со списком атрибутов рекордсета</param>
        /// <returns>новый объект сайта</returns>
        public static void CheckTable(string fromTable, int fromId, IMRecordset obj, string attrs)
        {
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }

            string delimStr = " ,";
            char[] delimiter = delimStr.ToCharArray();
            string[] atts = null;
            atts = attrs.Split(delimiter);

            IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            r.Select("ID");
            r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
            r.Open();
            if (!r.IsEOF())
            {
                IMObject oldObj = IMObject.LoadFromDB(fromTable, fromId);
                foreach (string fName in atts)
                {
                    OrmField fld = null;
                    foreach (OrmField field in fldMas)
                    {
                        if (field.Name == fName)
                        {
                            fld = field;
                            break;
                        }
                    }
                    if (fld != null)
                    {
                        try
                        {
                            if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME && oldObj.GetT(fld.Name) != obj.GetT(fld.Name))
                                journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + oldObj.GetT(fld.Name).ToString() + "' -> '" + obj.GetT(fld.Name).ToString() + "'");
                            else if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                            {
                                double valOld = IM.RoundDeci(oldObj.GetD(fld.Name), 3);
                                double valNew = IM.RoundDeci(obj.GetD(fld.Name), 3);
                                if (valOld != valNew)
                                    journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + valOld.ToString("F3") + "' -> '" + valNew.ToString("F3") + "'");
                            }
                            else if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING && oldObj.GetS(fld.Name) != obj.GetS(fld.Name))
                                journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + oldObj.GetS(fld.Name) + "' -> '" + obj.GetS(fld.Name) + "'");
                        }
                        catch { }
                    }
                }
            }
            r.Close();
            r.Destroy();
        }
        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице созданной в плагине
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj">Рекордсет с измененными данными</param>
        /// <param name="attrs">Строка со списком атрибутов рекордсета</param>
        /// <returns>новый объект сайта</returns>
        public static void CheckUcrfTable(string fromTable, int fromId, IMRecordset obj, string attrs)
        {
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }

            string delimStr = " ,";
            char[] delimiter = delimStr.ToCharArray();
            string[] atts = null;
            atts = attrs.Split(delimiter);
            IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            r.Select(attrs);
            r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
            r.Open();
            if (!r.IsEOF())
            {
                //  IMObject oldObj = IMObject.LoadFromDB(fromTable, fromId);
                if (obj == null)
                    journal.Add("Record from table '" + CLocaliz.TxT(fromTable) + "' with ID = " + fromId.ToString() + " was deleted");
                else
                {
                    foreach (string fName in atts)
                    {
                        OrmField fld = null;
                        foreach (OrmField field in fldMas)
                        {
                            if (field.Name == fName)
                            {
                                fld = field;
                                break;
                            }
                        }
                        if (fld != null)
                        {
                            try
                            {
                                if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME && r.GetT(fld.Name) != obj.GetT(fld.Name))
                                    journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + r.GetT(fld.Name).ToString() + "' -> '" + obj.GetT(fld.Name).ToString() + "'");
                                else if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                                {
                                    double valOld = IM.RoundDeci(r.GetD(fld.Name), 3);
                                    double valNew = IM.RoundDeci(obj.GetD(fld.Name), 3);
                                    if (valOld != valNew)
                                        journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + valOld.ToString("F3") + "' -> '" + valNew.ToString("F3") + "'");
                                }
                                else if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING && r.GetS(fld.Name) != obj.GetS(fld.Name))
                                    journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + r.GetS(fld.Name) + "' -> '" + obj.GetS(fld.Name) + "'");
                            }
                            catch { }
                        }
                    }
                }
            }
            r.Close();
            r.Destroy();
        }

        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице созданной в плагине
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj">Рекордсет с измененными данными</param>
        /// <param name="attrs">Строка со списком атрибутов рекордсета</param>
        /// <returns>новый объект сайта</returns>
        public static void CheckRecordsets(IMRecordset obj1, IMRecordset obj2, string fromTable, string attrs)
        {
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }

            string delimStr = " ,";
            char[] delimiter = delimStr.ToCharArray();
            string[] atts = null;
            atts = attrs.Split(delimiter);
            //IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            //r.Select(attrs);
            //r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
            //r.Open();
            //if (!r.IsEOF())
            //{
            //  IMObject oldObj = IMObject.LoadFromDB(fromTable, fromId);
            //if (obj == null)
            //   journal.Add("Record from table '" + CLocaliz.TxT(fromTable) + "' with ID = " + fromId.ToString() + " was deleted");
            //else
            //{
            foreach (string fName in atts)
            {
                OrmField fld = null;
                foreach (OrmField field in fldMas)
                {
                    if (field.Name == fName)
                    {
                        fld = field;
                        break;
                    }
                }
                if (fld != null)
                {
                    try
                    {
                        if (fld.DDesc.Coding == OrmDataCoding.tvalDATETIME && obj1.GetT(fld.Name) != obj1.GetT(fld.Name))
                            journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + obj1.GetT(fld.Name).ToString() + "' -> '" + obj2.GetT(fld.Name).ToString() + "'");
                        else if (fld.DDesc.Coding == OrmDataCoding.tvalNUMBER)
                        {
                            double valOld = IM.RoundDeci(obj1.GetD(fld.Name), 3);
                            double valNew = IM.RoundDeci(obj2.GetD(fld.Name), 3);
                            if (valOld != valNew)
                                journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + valOld.ToString("F3") + "' -> '" + valNew.ToString("F3") + "'");
                        }
                        else if (fld.DDesc.Coding == OrmDataCoding.tvalSTRING && obj1.GetS(fld.Name) != obj2.GetS(fld.Name))
                            journal.Add(CLocaliz.TxT(fromTable) + "." + CLocaliz.TxT(fld.Name) + " '" + obj1.GetS(fld.Name) + "' -> '" + obj2.GetS(fld.Name) + "'");
                    }
                    catch { }
                }
            }
            //}
            //}
            //r.Close();
            //r.Destroy();
        }
        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице созданной в плагине
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj">Рекордсет с измененными данными (если null то добавляеться сообщение об удалении записи)</param>
        /// <param name="attrs">Строка со списком атрибутов рекордсета</param>
        /// <param name="writeIfNewRecord">TRUE если необходимо создавать запись при создании новой записи</param>
        /// <returns>новый объект сайта</returns>
        public static void CheckUcrfTable(string fromTable, int fromId, IMRecordset obj, string attrs, bool writeIfNewRecord)
        {
            //Вытаскиваем поля таблицы
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }
            //-----
            if (obj == null)
                journal.Add(string.Format("Record from table '{0}' with ID={1} was deleted", CLocaliz.TxT(fromTable), fromId));
            //Разбираем поля, которые необходимо обработать
            char[] delimiter = ",".ToCharArray();
            string[] lstField = (from itemField in attrs.Split(delimiter) select itemField.Trim()).ToArray();
            IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select(attrs);
                r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
                r.Open();
                if (r.IsEOF())
                {//Новая запись
                    if ((writeIfNewRecord == true) && (obj != null))
                    {
                        foreach (string fName in lstField)
                        {
                            OrmField[] fldList =
                               (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                            if (fldList.Length > 0)
                                TestFields(fldList[0], null, obj, fromTable, fName);
                        }
                    }
                }
                else
                {//Необходимо сравнить
                    if (obj != null)
                    {
                        foreach (string fName in lstField)
                        {
                            OrmField[] fldList =
                               (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                            if (fldList.Length > 0)
                                TestFields(fldList[0], r, obj, fromTable, fName);
                        }
                    }
                    else
                    {
                        foreach (string fName in lstField)
                        {
                            OrmField[] fldList =
                               (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                            if (fldList.Length > 0)
                                TestFields(fldList[0], r, null, fromTable, fName);
                        }
                    }
                }
            }
            finally
            {
                r.Destroy();
            }
        }
        ////==================================================
        ///// <summary>
        ///// Возвращает список полей таблицы (через символ ',')
        ///// </summary>
        ///// <param name="tableName">имя таблицы</param>
        ///// <returns>Возвращает список полей таблицы (через символ ',')</returns>
        //public static string GetAllFieldsOfTable(string tableName)
        //{//Вытаскиваем поля таблицы
        //   OrmTable ormTab = OrmSchema.Table(tableName);
        //   OrmField[] fldMas = null;
        //   try
        //   {
        //      fldMas = ormTab.Fields;
        //   }
        //   catch
        //   {
        //      ormTab = OrmSchema.Table(tableName);
        //      fldMas = ormTab.Fields;
        //   }
        //   //-----
        //   StringBuilder sb = new StringBuilder(100);
        //   foreach (OrmField fld in fldMas)
        //   {
        //      if (fld.DDesc == null)
        //         continue;
        //      if (sb.Length > 0)
        //         sb.Append(',');
        //      sb.Append(fld.Name);
        //   }
        //   return sb.ToString();
        //}

        //==================================================
        /// <summary>
        /// Добавляет в список изменения по таблице созданной в плагине
        /// </summary>
        /// <param name="fromTable">Таблица объекта</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="objCurrent">IMRecorset или IMObject с данными из БД (не измененные данные)</param>
        /// <param name="objNew">IMRecorset или IMObject с измененными данными</param>
        /// <param name="attrs">Строка со списком атрибутов рекордсета</param>
        /// <param name="writeIfNewRecord">TRUE если необходимо создавать запись о создании новой записи</param>
        public static void CheckTableObj(string fromTable, int fromId, object objCurrent, object objNew, string attrs, bool writeIfNewRecord)
        {
            //Вытаскиваем поля таблицы
            OrmTable ormTab = OrmSchema.Table(fromTable);
            OrmField[] fldMas = null;
            try
            {
                fldMas = ormTab.Fields;
            }
            catch
            {
                ormTab = OrmSchema.Table(fromTable);
                fldMas = ormTab.Fields;
            }
            //-----
            if (string.IsNullOrEmpty(attrs))
            {
                StringBuilder sb = new StringBuilder(100);
                foreach (OrmField fld in fldMas)
                {
                    if (sb.Length > 0)
                        sb.Append(',');
                    sb.Append(fld.Name);
                }
                attrs = sb.ToString();
            }

            //-----
            if (objNew == null)
                journal.Add(string.Format("Record from table '{0}' with ID={1} was deleted", CLocaliz.TxT(fromTable), fromId));
            //Разбираем поля, которые необходимо обработать
            char[] delimiter = ",".ToCharArray();
            string[] lstField = (from itemField in attrs.Split(delimiter) select itemField.Trim()).ToArray();
            if (objCurrent == null)
            {//Новая запись
                if (writeIfNewRecord && (objNew != null))
                {
                    foreach (string fName in lstField)
                    {
                        OrmField[] fldList =
                           (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                        if (fldList.Length > 0)
                            TestFields(fldList[0], null, objNew, fromTable, fName);
                    }
                }
            }
            else
            {//Необходимо сравнить
                if (objNew != null)
                {
                    foreach (string fName in lstField)
                    {
                        OrmField[] fldList =
                           (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                        if (fldList.Length > 0)
                            TestFields(fldList[0], objCurrent, objNew, fromTable, fName);
                    }
                }
                else
                {
                    foreach (string fName in lstField)
                    {
                        OrmField[] fldList =
                           (from itemFld in fldMas where (itemFld.Name == fName) select itemFld).ToArray();
                        if (fldList.Length > 0)
                            TestFields(fldList[0], objCurrent, null, fromTable, fName);
                    }
                }
            }
        }
        ////===================================================
        //private void TestFields(OrmField ormField, IMObject obj1, IMObject obj2, string fromTable, string fName)
        //{
        //   try
        //   {
        //      switch (ormField.DDesc.Coding)
        //      {
        //         case OrmDataCoding.tvalSTRING:
        //            TestField((obj1 != null) ? obj1.GetS(fName) : "", (obj2 != null) ? obj2.GetS(fName) : "", fromTable, fName);
        //            break;
        //         case OrmDataCoding.tvalNUMBER:
        //            TestField((obj1 != null) ? obj1.GetD(fName) : IM.NullD, (obj2 != null) ? obj2.GetD(fName) : IM.NullD, fromTable, fName);
        //            break;
        //         case OrmDataCoding.tvalDATETIME:
        //            TestField((obj1 != null) ? obj1.GetT(fName) : IM.NullT, (obj2 != null) ? obj2.GetT(fName) : IM.NullT, fromTable, fName);
        //            break;
        //      }
        //   }
        //   catch (Exception ex)
        //   {
        //      CLogs.WriteLog(ELogsType.Warning, ELogsWhat.Appl,
        //                     string.Format("Writing journal exception: {0}\n\n{1}\n\n{2}", ex.Message, ex.Data,
        //                                   ex.StackTrace));
        //   }
        //}
        //===================================================
        /// <summary>
        /// Проверяет поля таблиц плагина
        /// </summary>
        /// <param name="fromTable">Имя таблицы</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="obj2">IMrecordset</param>
        /// <param name="fields">Список полей через ','</param>
        public static void TestFieldsOfPluginTable(string fromTable, int fromId, IMRecordset obj2, string fields)
        {
            IMRecordset r = new IMRecordset(fromTable, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select(fields);
                r.SetWhere("ID", IMRecordset.Operation.Eq, fromId);
                r.Open();
                if (!r.IsEOF())
                {
                    CheckTableObj(fromTable, fromId, r, obj2, fields, true);
                }
                else
                {
                    CheckTableObj(fromTable, fromId, null, obj2, fields, true);
                }
            }
            finally
            {
                r.Destroy();
            }
        }
        //===================================================
        /// <summary>
        /// Будет удалена запись из таблицы
        /// </summary>
        /// <param name="fromTable">Имя таблицы</param>
        /// <param name="fromId">ID записи</param>
        /// <param name="fields">Список полей через ','</param>
        public static void DeleteRecordOfPluginTable(string fromTable, int fromId, string fields)
        {
            TestFieldsOfPluginTable(fromTable, fromId, null, fields);
        }
        //===================================================
        private static void TestFields(OrmField ormField, Object obj1, Object obj2, string fromTable, string fName)
        {
            try
            {
                switch (ormField.DDesc.Coding)
                {
                    case OrmDataCoding.tvalSTRING:
                        {
                            string str1 = "";
                            if (obj1 != null)
                                str1 = obj1.GetType().InvokeMember("GetS", BindingFlags.InvokeMethod, null, obj1,
                                                                   new object[] { fName }).ToString();
                            string str2 = "";
                            if (obj2 != null)
                                str2 = obj2.GetType().InvokeMember("GetS", BindingFlags.InvokeMethod, null, obj2,
                                                                   new object[] { fName }).ToString();
                            TestField(str1, str2, fromTable, fName);
                        }
                        break;
                    case OrmDataCoding.tvalNUMBER:
                        {
                            double dbl1 = IM.NullD;
                            if (obj1 != null)
                                dbl1 = obj1.GetType().InvokeMember("GetD", BindingFlags.InvokeMethod, null, obj1,
                                                                   new object[] { fName }).ToString().ToDouble(IM.NullD);
                            double dbl2 = IM.NullD;
                            if (obj2 != null)
                                dbl2 = obj2.GetType().InvokeMember("GetD", BindingFlags.InvokeMethod, null, obj2,
                                                                   new object[] { fName }).ToString().ToDouble(IM.NullD);
                            TestField(dbl1, dbl2, fromTable, fName);
                        }
                        break;
                    case OrmDataCoding.tvalDATETIME:
                        {
                            DateTime dt1 = IM.NullT;
                            if (obj1 != null)
                                dt1 = obj1.GetType().InvokeMember("GetT", BindingFlags.InvokeMethod, null, obj1,
                                                                   new object[] { fName }).ToString().ToDataTime();
                            DateTime dt2 = IM.NullT;
                            if (obj2 != null)
                                dt2 = obj2.GetType().InvokeMember("GetT", BindingFlags.InvokeMethod, null, obj2,
                                                                   new object[] { fName }).ToString().ToDataTime();
                            TestField(dt1, dt2, fromTable, fName);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                CLogs.WriteLog(ELogsType.Warning, ELogsWhat.Appl,
                               string.Format("Writing journal exception: {0}\n\n{1}\n\n{2}", ex.Message, ex.Data,
                                             ex.StackTrace));
            }
        }

        //==================================================
        /// <summary>
        /// Сравнивает даты
        /// </summary>
        private static void TestField(DateTime val1, DateTime val2, string tableName, string fieldName)
        {
            if (val1 != val2)
            {
                string str1 = (val1 != IM.NullT) ? val1.ToString() : "";
                string str2 = (val2 != IM.NullT) ? val2.ToString() : "";
                WriteToJournal(str1, str2, tableName, fieldName);
            }
        }
        //==================================================
        /// <summary>
        /// Сравнивает числа
        /// </summary>
        private static void TestField(double val1, double val2, string tableName, string fieldName)
        {
            val1 = (val1 != IM.NullD) ? val1.Round(3) : val1;
            val2 = (val2 != IM.NullD) ? val2.Round(3) : val2;
            if (val1 != val2)
            {
                string str1 = (val1 != IM.NullI) ? val1.ToString() : "";
                string str2 = (val2 != IM.NullI) ? val2.ToString() : "";
                WriteToJournal(str1, str2, tableName, fieldName);
            }
        }
        //==================================================
        /// <summary>
        /// Сравнивает строки
        /// </summary>
        private static void TestField(string val1, string val2, string tableName, string fieldName)
        {
            if (val1 != val2)
                WriteToJournal(val1, val2, tableName, fieldName);
        }
        //==================================================
        /// <summary>
        /// Добавляет запись в журнал событий
        /// </summary>
        /// <param name="val1">значение 1</param>
        /// <param name="val2">значение 2</param>
        /// <param name="tableName">имя таблицы</param>
        /// <param name="fieldName">имя поля</param>
        private static void WriteToJournal(string val1, string val2, string tableName, string fieldName)
        {
            journal.Add(string.Format("{0}.{1}: {2} -> {3}",
                                      CLocaliz.TxT(tableName),
                                      CLocaliz.TxT(fieldName),
                                      (!string.IsNullOrEmpty(val1)) ? val1 : "",
                                      (!string.IsNullOrEmpty(val2)) ? val2 : ""));
        }
        //==================================================
        /// <summary>
        /// Добавляет комментарии к файлу изменений
        /// </summary>
        public static void AddComments()
        {
            comments = "";
            //TODO проверку заком. так как за новым алгоритмом логирования мы не знаем были ли изменения
            //if (journal.Count != 0)
            {
                using(HelpClasses.FComments comForm = new XICSM.UcrfRfaNET.HelpClasses.FComments())
                {
                    comForm.ShowDialog();
                    comments = comForm.GetComment();
                }
            }
        }
        //==================================================
        /// <summary>
        /// Создание файла отчета
        /// </summary>
        /// <param name="id">идентификатор объекта с изменениями</param>
        /// <param name="type">тип заявки</param>
        /// <returns>результат создания отчета</returns>
        public static bool CreateReport(int id, AppType type)
        {
            if ((journal.Count != 0) && (id != 0) && (id != IM.NullI))
            {
                string fname = DateTime.Now.ToString("ddMMyyy_hhmmss") + "_" + id.ToString();
                switch (type)
                {
                    case AppType.AppBS: fname += "ЗС"; break;
                    case AppType.AppRS: fname += "РС"; break;
                    case AppType.AppZS: fname += "БС"; break;
                    case AppType.AppRR: fname += "РР"; break;
                    case AppType.AppTR: fname += "PT"; break;
                    case AppType.AppTR_З: fname += "PTЗ"; break;
                    case AppType.AppR2: fname += "Р2"; break;
                    case AppType.AppR2d: fname += "Р2ц"; break;
                    case AppType.AppTV2: fname += "Т2"; break;
                    case AppType.AppTV2d: fname += "Т2ц"; break;
                    case AppType.AppAR_3: fname += "АР3"; break;
                    case AppType.AppA3: fname += "AР"; break;
                    case AppType.AppVP: fname += "ВП"; break;
                    case AppType.AppZRS: fname += "ЗPC"; break;
                    default: throw new IMException("Report - unknown application type.");
                }
                string reportPath = "";
                IMRecordset r = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadWrite);
                r.Select("ITEM,WHAT");
                r.SetWhere("ITEM", IMRecordset.Operation.Like, "XNRFA_REPORTPATH");
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        reportPath = r.GetS("WHAT");
                    else
                        reportPath = "";
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }

                if (reportPath == "")
                    return false;

                if (reportPath[reportPath.Length - 1] == '\\')
                    fname = reportPath + fname;
                else
                    fname = reportPath + "\\" + fname;

                fname.Trim();
                fname += ".txt";
                try
                {
                    using (StreamWriter outputStream = new StreamWriter(fname, false, (Encoding)System.Text.Encoding.Unicode))
                    {
                        outputStream.WriteLine(comments);
                        foreach (string line in journal)
                            outputStream.WriteLine(line);
                    }
                }
                catch
                {
                    return false;
                }

                IMRecordset log = new IMRecordset(PlugTbl.itblXnrfaChangeLog, IMRecordset.Mode.ReadWrite);
                log.Select("ID,OBJ_ID,OBJ_TABLE,EMPLOYEE_ID,CREATED_DATE,PATH");
                try
                {
                    log.Open();
                    log.AddNew();
                    log["ID"] = IM.AllocID(PlugTbl.itblXnrfaChangeLog, 1, -1);
                    log["OBJ_ID"] = id;
                    log["OBJ_TABLE"] = PlugTbl.itblXnrfaAppl;
                    string user = IM.ConnectedUser();
                    int userid = IM.NullI;
                    IMRecordset r2 = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
                    r2.Select("ID,APP_USER");
                    r2.SetWhere("APP_USER", IMRecordset.Operation.Eq, user);
                    try
                    {
                        r2.Open();
                        if (!r2.IsEOF())
                            userid = r2.GetI("ID");
                    }
                    finally
                    {
                        r2.Close();
                        r2.Destroy();
                    }
                    log["EMPLOYEE_ID"] = userid;
                    log["CREATED_DATE"] = DateTime.Now;
                    log["PATH"] = fname;
                    log.Update();
                }
                finally
                {
                    log.Close();
                    log.Destroy();
                }
                return true;
            }
            else
                return false;
        }
    }
}
