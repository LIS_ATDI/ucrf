﻿using System.Text;
using System;
using System.Collections.Generic;
using XICSM.UcrfRfaNET.Documents;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using System.Xml;
using System.Windows.Forms;
using Lis.CommonLib.Extensions;
using System.IO;
using XICSM.UcrfRfaNET.HelpClasses.Forms;
using XICSM.UcrfRfaNET.Net;
using ArticleLib;
using System.Linq;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    /// <summary>
    /// Режимы генерации служебки
    /// </summary>
    internal enum AutoRegimEnum
    {
        Standard,       //0 - Стандартный режим
        AutoGenerate,   //1 - Автоматическая генерация
        AutoStandard,   //2 - Автоматический стандартный режим (укороченый XML)
    }




    

   public class CDRVMemoURCM
   {
       //===================================================
       /// <summary>
       /// Разбирает XML файл ответа из Паруса
       /// </summary>
       /// <param name="xml">текст XML</param>
       /// <param name="err">Код возможной ошибки</param>
       /// <returns>успешно или нет прошло чтение файла</returns>
       public static bool XmlAnswerRead(string xml, out int err)
       {
           string xmlschemaerror = "";
           if (!CXMLTest.TestXmlToSchema(xml, ref xmlschemaerror))
           {
               err = (int) DRVError.de_20;
               return false;
           }
           XmlDocument xmlDoc = new XmlDocument();
           xmlDoc.Load(new StringReader(xml));
           string strid = "";
           XmlElement root = xmlDoc.DocumentElement;
           XmlNodeList idList = xmlDoc.GetElementsByTagName("IDENT_ICSM");
           if (idList.Count > 0)
           {
               XmlNode el = idList[0];
               strid = el.InnerText;
           }
           if (strid == "")
           {
               err = (int) DRVError.de_1;
               return false;
           }

           XmlNodeList resultList = root.GetElementsByTagName("ENV_TYPE");
           string strresult = "";
           if (resultList.Count > 0)
               strresult = resultList[0].InnerText;
           if (strresult == "")
           {
               err = (int) DRVError.de_2;
               return false;
           }

           int appId = ConvertType.ToInt32(strid, IM.NullI);
           if (appId == IM.NullI)
           {
               err = (int) DRVError.de_3;
               return false;
           }

           int status = ConvertType.ToInt32(strresult, IM.NullI);
           if (status == IM.NullI)
           {
               err = (int) DRVError.de_4;
               return false;
           }

           XmlNodeList datetimeList = root.GetElementsByTagName("DATETIME");
           string strDateTime = "";
           if (datetimeList.Count > 0)
               strDateTime = datetimeList[0].InnerText;
           if (strDateTime == "")
           {
               err = (int) DRVError.de_5;
               return false;
           }

           DateTime opDate = new DateTime();
           try
           {
               opDate = Convert.ToDateTime(strDateTime);
           }
           catch (Exception)
           {
               err = (int) DRVError.de_7;
               return false;
           }

           string strMsg = "";
           if (status == -1)
           {
               XmlNodeList msgList = root.GetElementsByTagName("MSG");

               if (msgList.Count > 0)
                   strMsg = msgList[0].InnerText;
               if (strMsg == "")
               {
                   err = (int)DRVError.de_13;
                   return false;
               }
           }

           RecordPtr employee;
           employee.Id = IM.NullI;
           employee.Table = "";

           if(status != 8)
           {
               XmlNodeList accagentList = root.GetElementsByTagName("ACCAGENT");
               string strAccagent = "";
               if (accagentList.Count > 0)
                   strAccagent = accagentList[0].InnerText;
               if (strAccagent == "")
               {
                   err = (int) DRVError.de_6;
                   return false;
               }

               double emplGuid = ConvertType.ToDouble(strAccagent, IM.NullD);
               if (emplGuid == IM.NullD)
               {
                   err = (int) DRVError.de_8;
                   return false;
               }

               employee = GlobalDB.CGlobalXML.GetRecordByGUID(emplGuid);
               if (employee.Id <= 0 || employee.Id == IM.NullI || employee.Table != ICSMTbl.itblEmployee)
               {
                   err = (int) DRVError.de_9;
                   return false;
               }
           }

           AutoRegimEnum autoRegim = AutoRegimEnum.Standard;
           string fullPath = "";
           switch(status)
           {
               case 7:
                   {
                       XmlNodeList fullPathList = root.GetElementsByTagName("FULL_PATCH");
                       if (fullPathList.Count > 0)
                           fullPath = fullPathList[0].InnerText;
                       if (fullPath == "")
                       {
                           err = (int) DRVError.de_28;
                           return false;
                       }
                   }
                   break;
               case 8:
                   {
                       XmlNodeList isAutoNode = root.GetElementsByTagName("IS_AUTO");
                       if (isAutoNode.Count > 0)
                           autoRegim = isAutoNode[0].InnerText.ToAutoRegimEnum();

                       if (autoRegim == AutoRegimEnum.AutoGenerate)
                       {
                           XmlNodeList fullPathList = root.GetElementsByTagName("FULLXLS");
                           if (fullPathList.Count > 0)
                               fullPath = fullPathList[0].InnerText;
                           if (fullPath == "")
                           {
                               err = (int) DRVError.de_28;
                               return false;
                           }
                       }
                   }
                   break;
           }

           switch (status)
           {
               case -1: //Ошибка
                   CLogs.WriteError(ELogsWhat.AutoXml, string.Format("DRV:{0} '{1}'", appId, strMsg));
                   UpdateApplStatus(appId, DRVStatus.error);
                   break;
               case 2: //Счет сформирован
                   UpdateInvoiceStatus(appId, InvoiceStatus.Generated);
                   break;
               case 3: //Счет выставлен
                   UpdateInvoiceStatus(appId, InvoiceStatus.Invoiced);
                   break;
               case 4: //Счет оплачен
                   UpdateInvoiceStatus(appId, InvoiceStatus.Paid);
                   break;
               case 7: //Заявка обработана
               case 8: //Заявка обработана автоматически
                   if (autoRegim == AutoRegimEnum.AutoStandard)
                   {
                       UpdateApplStatus(appId, DRVStatus.auto_added);
                   }
                   else
                   {
                       UpdateDoc(appId, fullPath, employee.Id, opDate, autoRegim);
                   }
                   break;
           }
           err = 0;
           return true;
       }

       internal static void UpdateDoc(int idApplUrcm, string fullPath, int emplId, DateTime createdDate, AutoRegimEnum autoRegim)
       {
           DRVStatus status = DRVStatus.generated;
           switch (autoRegim)
           {
               case AutoRegimEnum.AutoGenerate:
                   status = DRVStatus.auto_gener;
                   break;
               case AutoRegimEnum.AutoStandard:
                   status = DRVStatus.auto_added;
                   break;
           }
           IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
           rs.Select("ID,STATUS,ADD_DOC_PATH,ADD_CREATED_DATE");
           rs.Select("EMPLOYEE_DRV_ID");
           rs.Select("RESPONSE_DATE");
           rs.SetWhere("ID", IMRecordset.Operation.Eq, idApplUrcm);
           try
           {
               rs.Open();
               if (!rs.IsEOF())
               {
                   rs.Edit();
                   rs.Put("STATUS", status.ToString());
                   rs.Put("ADD_DOC_PATH", fullPath);
                   rs.Put("ADD_CREATED_DATE", createdDate);
                   rs.Put("EMPLOYEE_DRV_ID", emplId);
                   rs.Put("RESPONSE_DATE", DateTime.Now);
                   rs.Update();
               }
           }
           catch (Exception e)
           {
               CLogs.WriteError(ELogsWhat.Critical, e);
           }
           finally
           {
               rs.Final();
           }
       }
       /// <summary>
       /// Установить статус заявки
       /// </summary>
       /// <param name="idApplUrcm">ID заявки</param>
       /// <param name="status">статус</param>
       public static void UpdateApplStatus(int idApplUrcm, DRVStatus status)
       {
           using (Icsm.LisTransaction tr = new Icsm.LisTransaction())
           {
               using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite))
               {
                   rs.Select("ID,STATUS");
                   rs.SetWhere("ID", IMRecordset.Operation.Eq, idApplUrcm);
                   rs.Open();
                   if (!rs.IsEOF())
                   {
                       rs.Edit();
                       rs.Put("STATUS", status.ToString());
                       rs.Update();
                   }
                   tr.Commit();
               }
           }
       }
       /// <summary>
       /// Установить статус счета
       /// </summary>
       /// <param name="idApplUrcm">ID заявки</param>
       /// <param name="status">статус</param>
       private static void UpdateInvoiceStatus(int idApplUrcm, InvoiceStatus status)
       {
           using (Icsm.LisTransaction tr = new Icsm.LisTransaction()) {
               using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite)) {
                   rs.Select("ID,INVOICE_STATUS");
                   rs.SetWhere("ID", IMRecordset.Operation.Eq, idApplUrcm);
                   rs.Open();
                   if (!rs.IsEOF())
                   {
                       rs.Edit();
                       rs.Put("INVOICE_STATUS", status.ToString());
                       rs.Update();
                   }
                   tr.Commit();
               }
           }
           if (status == InvoiceStatus.Paid) {
               using (Icsm.LisRecordSet rs_mon = new Icsm.LisRecordSet("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite)) {
                   rs_mon.Select("ID,APP_URCM_ID,STATUS");
                   rs_mon.SetWhere("APP_URCM_ID", IMRecordset.Operation.Eq, idApplUrcm);
                   for (rs_mon.Open(); !rs_mon.IsEOF(); rs_mon.MoveNext()) {
                       rs_mon.Edit();
                       rs_mon.Put("STATUS", "3");
                       rs_mon.Update();
                   }
               }
           }
       }

      public int ownerId = IM.NullI;
      public int memoId = IM.NullI;
      public int emplId = IM.NullI;
      public DateTime addCreateDate = IM.NullT;
      public DRVStatus status = DRVStatus.newapl;//Статус заявки
      public InvoiceStatus StatusInvoice { get; set; }//Статус счета
      public string docPath = "";
      public string addDocPath = "";
      public List<cbItem> addDocPathList = new List<cbItem>();

      public string number = "";
      public MemoLines art_lines = new MemoLines();
      public bool hasDiff = false;
      public string docFullPath = "";
      public string addDocFullPath = "";
      public int EmplDrvId { get; set; }
      public DateTime DateResponse { get; set; }



       private List<string> _curr_prov;
       private bool _status_actual_contacts;
       private int _applId;
       private bool IsOrder=false;
       private bool Actual_Measurement = false;
       private bool OnlyWorksSave = false;
       private int _ownerIdLocal;
       private int _id_xnrfa_mon_contr;
       private string _table_Name = "";
       private string _LogInclude = "";

       public const string FieldCreateDate = "CreateDate";
       public DateTime CreateDate { get; set; }
       // для загрузки перечня станций сети 
       NetObject _net_load = new NetObject();


       int[] applIds;
       int[] netIds;
       //===================================================
       /// <summary>
       /// Конструктор
       /// </summary>
       public CDRVMemoURCM(int appId, int ownerIdLocal, string Table_name, int params1, bool is_order, bool actual_measurement, bool onlyWorksSave, int mem_id)
       {
           _table_Name = Table_name;
           EmplDrvId = IM.NullI;
           DateResponse = IM.NullT;
           _applId = appId;
           _ownerIdLocal = ownerIdLocal;
           _id_xnrfa_mon_contr = params1;
           StatusInvoice = InvoiceStatus.Unknown;
           _status_actual_contacts = false;
           _curr_prov = new List<string>();
           _curr_prov.Clear();
           IsOrder = is_order;
           Actual_Measurement = actual_measurement;
           OnlyWorksSave = onlyWorksSave;
           if (onlyWorksSave) memoId = mem_id;
           GetIdUsers_(Table_name, params1);
       }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="appId">ID заявки</param>
      /// <param name="ownerIdLocal">ID владельца или плательшика</param>
      public CDRVMemoURCM(int appId, int ownerIdLocal,string Table_name, int params1, string LogInclude)
      {
          _table_Name = Table_name;
          EmplDrvId = IM.NullI;
          DateResponse = IM.NullT;
          _applId = appId;
          _ownerIdLocal = ownerIdLocal;
          _id_xnrfa_mon_contr = params1;
          StatusInvoice = InvoiceStatus.Unknown;
          _status_actual_contacts = false;
          _curr_prov = new List<string>();
          _curr_prov.Clear();
          _LogInclude = LogInclude;
          GetIdUsers_(Table_name, params1);
          
      }
      //===================================================
      /// <summary>
      /// Конструктор 2
      /// </summary>
      /// <param name="appId">ID заявки</param>
      /// <param name="ownerIdLocal">ID владельца или плательшика</param>
      /// <param name="Table_Name"> Наименование таблицы </param>
      public CDRVMemoURCM(int appId, int ownerIdLocal, string Table_Name)
      {
          EmplDrvId = IM.NullI;
          DateResponse = IM.NullT;
          _applId = appId;
          _ownerIdLocal = ownerIdLocal;
          _id_xnrfa_mon_contr = ownerIdLocal;
          _table_Name = Table_Name;
          _curr_prov = new List<string>();
          _curr_prov.Clear();

          if (_table_Name == PlugTbl.xnrfa_monitor_contracts)
          {
              if (GetIdUsers(_applId).Length > 0)
              {
                  _ownerIdLocal = Convert.ToInt32(GetIdUsers(_applId));
              }
          }

         
          StatusInvoice = InvoiceStatus.Unknown;
          
      }

       /// <summary>
      /// Определение наименования области по коду
      /// </summary>
      /// <param name="NAME">значение искомого элемента списка</param>
      /// <returns>текстовое значение наименования области</returns>
       public static string GetNameAreaOnCode(string CODE)
       {
           string str="";
                  IMRecordset rs = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                  rs.Select("ID,CODE,NAME");
                  rs.SetWhere("CODE", IMRecordset.Operation.Eq, CODE.Trim());

                  try
                  {
                      rs.Open();
                      if (!rs.IsEOF())
                      {
                          str = rs.GetS("NAME").ToString().Trim();
                      }
                      
                  }
            finally
                  {
                      rs.Final();
                  }
                  return str;
       }



      /// <summary>
      /// Определение вхождения элемента в список
      /// </summary>
      /// <param name="NAME">значение искомого элемента списка</param>
      /// <returns>True - элемент найден, иначе False</returns>

      public static bool SearchOnElementList(List<string> str_, string NAME)
      {
          bool temp=false;
          for (int i = 0; i < str_.Count; i++)
          {
              if (str_[i].ToString().Trim() == NAME.Trim())
              {
                  temp = true;
                  break;
              }

          }
          return temp;
      }

      /// <summary>
      /// Определение выбраннфх областей для таблицы USERS
      /// </summary>
      /// <param name="ID_APPL">значение поля ID таблицы ID_APPL</param>
      
      private void GetIdUsers_(string table_name, int params1)
      {
          
          string name_prov = "";
          _curr_prov.Clear();
          
       //       IMRecordset rs = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
          IMRecordset rs = new IMRecordset(table_name, IMRecordset.Mode.ReadOnly);
              rs.Select("ID");

              if (table_name == ICSMTbl.USERS) { rs.Select("PROVINCE"); rs.SetWhere("ID", IMRecordset.Operation.Eq, _ownerIdLocal); }
              if (table_name == PlugTbl.XvMonitorUsers) { rs.Select("CONTRACT_REGION_CODE"); rs.SetWhere("ID", IMRecordset.Operation.Eq, params1); }

              try
              {
                  rs.Open();

                  if (!rs.IsEOF())
                  {// Пакет существует
              
                  if (table_name == PlugTbl.XvMonitorUsers)
              {

                  name_prov = rs.GetS("CONTRACT_REGION_CODE").ToString().Trim();
                  

                  IMRecordset rs3 = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                  rs3.Select("ID,CODE,NAME");
                  rs3.SetWhere("CODE", IMRecordset.Operation.Eq, rs.GetS("CONTRACT_REGION_CODE").ToString().Trim());
                  rs3.OrderBy("ID",OrderDirection.Descending);

                  try
                  {
                      rs3.Open();
                      if (!rs3.IsEOF())
                      {

                          if (!string.IsNullOrEmpty(rs3.GetS("NAME").ToString().Trim()))
                          {
                              //name_prov = rs3.GetS("NAME").ToString().Trim();
                              //name_prov = rs3.GetS("CODE").ToString().Trim();


                          }
                      }
                  }
                  finally
                  {
                      rs3.Final();
                  }

              }


                  if (table_name == ICSMTbl.USERS)
                      {

                          IMRecordset rsMon_Users = new IMRecordset(PlugTbl.XvMonitorUsers, IMRecordset.Mode.ReadOnly);
                          rsMon_Users.Select("ID,OWNER_ID,CONTRACT_REGION_CODE");
                          rsMon_Users.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, rs.GetI("ID").ToString().Trim());

                  try
                  {
                      rsMon_Users.Open();
                      if (!rsMon_Users.IsEOF())
                      {
                          name_prov = rsMon_Users.GetS("CONTRACT_REGION_CODE").ToString().Trim();
                      }
                  }
                  finally
                  {
                      rsMon_Users.Final();
                  }



                          //if (!string.IsNullOrEmpty(rs.GetS("PROVINCE").ToString().Trim()))
                          //{
                              //name_prov = rs.GetS("PROVINCE").ToString().Trim();
                          //}
                      }
      
                  }
              }
              finally
              {
                  rs.Final();
              }

              if (!string.IsNullOrEmpty(name_prov))
              {
                  _curr_prov.Add(name_prov);
                  if (table_name == ICSMTbl.USERS)
                  {
                      //if (CDRVMemoURCM.SearchOnElementList(_curr_prov, "32"))
                      //{
                      _curr_prov.Clear();
                          _curr_prov.Add("32");
                          _curr_prov.Add("80");
                      //}
                  }
                  else if (table_name == PlugTbl.XvMonitorUsers)
                  {
                      
                      if (CUsers.CurDepartment != UcrfDepartment.Branch)
                      {
                          if (CDRVMemoURCM.SearchOnElementList(_curr_prov, "32"))
                          {
                              _curr_prov.Add("80");
                          }
                      }
                  }
              }
          
      }

       /// <summary>
       /// Вычисление значения ID для таблицы USERS по ID таблицы XNRFA_MON_CONTRACTS
       /// </summary>
       /// <param name="ID_MON_CONTRACTS">значение поля ID таблицы XNRFA_MON_CONTRACTS</param>
       /// <returns>True - все в порядке, иначе False</returns>
       private string GetIdUsers(int ID_MON_CONTRACTS)
       {
           string temp = "";
           string name_prov="";
           _curr_prov.Clear();
           if (_ownerIdLocal > 0)
           {
               
               IMRecordset rs = new IMRecordset(PlugTbl.xnrfa_monitor_contracts, IMRecordset.Mode.ReadOnly);
               rs.Select("ID,RN_AGENT,TYPE_CONTRACTS,CODE_JURPERS");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, _ownerIdLocal);

               try
               {
                   rs.Open();
                   if (!rs.IsEOF())
                   {// Пакет существует

                       if (!string.IsNullOrEmpty(rs.GetS("CODE_JURPERS").ToString().Trim()))
                       {
                           IMRecordset rs3 = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                           rs3.Select("ID,CODE,NAME");
                           rs3.SetWhere("CODE", IMRecordset.Operation.Eq, rs.GetS("CODE_JURPERS").ToString().Trim());
                           try
                           {
                               rs3.Open();
                               if (!rs3.IsEOF())
                               {

                                   name_prov = rs3.GetS("CODE").ToString().Trim();
                                   _curr_prov.Add(name_prov);
                                   if  (SearchOnElementList(_curr_prov,"32"))
                                   {
                                         _curr_prov.Add("80");
                                   }
                               }
                           }
                           finally
                           {
                               rs3.Final();
                           }

                           if ((rs.GetS("TYPE_CONTRACTS").Trim() == "0") || (((SearchOnElementList(_curr_prov, "32")) && (rs.GetS("TYPE_CONTRACTS").Trim() == "1")) || ((SearchOnElementList(_curr_prov, "80")) && (rs.GetS("TYPE_CONTRACTS").Trim() == "1"))) || ((_curr_prov.Count >= 0)))
                           {

                               temp = rs.GetS("RN_AGENT");
                               if (!string.IsNullOrEmpty(temp.Trim()))
                               {
                                   
                                   IMRecordset rs2 = new IMRecordset(ICSMTbl.USERS, IMRecordset.Mode.ReadOnly);
                                   rs2.Select("ID,CODE");
                                   rs2.SetWhere("CODE", IMRecordset.Operation.Eq, temp.Trim());

                                   try
                                   {
                                       rs2.Open();
                                       if (!rs2.IsEOF())
                                       {// Пакет существует
                                            
                                           temp = rs2.GetI("ID").ToString().Trim();
                                           _status_actual_contacts = true;
                                       }
                                   }
                                   finally
                                   {
                                       rs2.Final();
                                   }
                               }
                           }
                       }
                   }
               }
               finally
               {
                   rs.Final();
               }

              
           }
           return temp;
           }

       /// <summary>
       /// Загрузка данных
       /// </summary>
       /// <param name="showMessage">Отображать сообщения о не проставленных статтях</param>
       /// <returns>True - все в порядке, иначе False</returns>
       private bool Load(bool showMessage)
       {
           List<string> provs = new List<string>();
           //MessageBox.Show(_ownerIdLocal.ToString());
           if (_applId != IM.NullI && _applId > 0)
           {
               List<cbItem> ListDocAll = new List<cbItem>();

               IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly);
               rs.Select("ID,OWNER_ID,CREATED_DATE,STATUS,EMPLOYEE_ID,DOC_PATH,MEMO_NUMBER,HASDIFF,ADD_DOC_PATH,ADD_CREATED_DATE,FULL_PATH");
               rs.Select("EMPLOYEE_DRV_ID");
               rs.Select("RESPONSE_DATE");
               rs.Select("INVOICE_STATUS");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, _applId);
               try
               {
                   rs.Open();
                   if (!rs.IsEOF())
                   {// Пакет существует
                       status = (DRVStatus)Enum.Parse(typeof(DRVStatus), rs.GetS("STATUS"));
                       ownerId = rs.GetI("OWNER_ID");
                       memoId = _applId;
                       emplId = rs.GetI("EMPLOYEE_ID");
                       CreateDate = rs.GetT("CREATED_DATE");
                       docPath = rs.GetS("DOC_PATH");
                       number = rs.GetS("MEMO_NUMBER");
                       hasDiff = (rs.GetI("HASDIFF") == 1) ? true : false;
                       addCreateDate = rs.GetT("ADD_CREATED_DATE");
                       addDocFullPath = rs.GetS("ADD_DOC_PATH");
                       addDocPath = addDocFullPath.ToFileNameWithoutExtension();
                       
                       // выборка перечня файлов по маске в определенной сетевой директории
                       try
                       {
                           string[] files2 = null;    
                           if (!string.IsNullOrEmpty(rs.GetS("FULL_PATH").Trim()))  files2 = Directory.GetFiles(Path.GetDirectoryName(@rs.GetS("FULL_PATH")), docPath + "*.*");
                           if (!string.IsNullOrEmpty(rs.GetS("ADD_DOC_PATH").Trim())) files2 = Directory.GetFiles(Path.GetDirectoryName(@rs.GetS("ADD_DOC_PATH")), docPath + "*.*");

                           if (files2 != null)
                           {
                               ListDocAll.Clear();
                               for (int k = 0; k < files2.Length; k++)
                               {
                                   cbItem item = new cbItem();
                                   item.short_name = Path.GetFileName(files2[k]);
                                   item.path_full = files2[k];
                                   ListDocAll.Add(item);
                               }
                           }
                           if ((ListDocAll!=null) && (ListDocAll.Count>0)) addDocPathList = ListDocAll;
                       }
                       catch (Exception ex)
                       {
                           MessageBox.Show("Ошибка при обработке сетевого пути. "+ex.Message);
                       }
                       
                       //

                       docFullPath = rs.GetS("FULL_PATH");
                       EmplDrvId = rs.GetI("EMPLOYEE_DRV_ID");
                       DateResponse = rs.GetT("RESPONSE_DATE");
                       StatusInvoice = rs.GetS("INVOICE_STATUS").ToInvoiceStatus();
                       art_lines.Load(memoId, IsOrder, ownerId);
                       art_lines.Sort();
                   }
               }
               finally
               {
                   rs.Final();
               }
           }
           else if (_ownerIdLocal != IM.NullI)
           {
               if (showMessage)
               {
                   if (((_table_Name == PlugTbl.xnrfa_monitor_contracts) && (_status_actual_contacts)) || (_table_Name != PlugTbl.xnrfa_monitor_contracts))
                   {
                       const string constUkraine = "Україна";

                       if (string.IsNullOrEmpty(CUsers.GetUserProvince()) == false)
                       {
                           //   provs.Add(CUsers.GetUserProvince());
                           //}
                           List<string> lst_filia = CUsers.GetSubProvinceUser();
                           if (lst_filia!= null)
                           {

                               FProvincesList fprov = new FProvincesList();
                               fprov.Text = CLocaliz.TxT("The choice of areas to search for networks and stations without articles");
                               IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                               rsArea.Select("NAME");
                               try
                               {
                                   for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                                       fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                                   fprov.chlbProvs.Sorted = true;

                                   for (int j = 0; j < fprov.chlbProvs.Items.Count; j++)
                                   {
                                       for (int jj = 0; jj < lst_filia.Count; jj++)
                                       {
                                           if (fprov.chlbProvs.Items[j].ToString().Trim() == lst_filia[jj].ToString().Trim())
                                               fprov.chlbProvs.SetItemChecked(j, true);
                                           //break;
                                       }
                                   }

                               }
                               finally
                               {
                                   rsArea.Close();
                                   rsArea.Destroy();
                               }

                               if (fprov.ShowDialog() == DialogResult.OK)
                               {
                                   if (fprov.chlbProvs.CheckedItems.Count != fprov.chlbProvs.Items.Count)
                                       foreach (object obj in fprov.chlbProvs.CheckedItems)
                                       {
                                           provs.Add(obj.ToString());
                                       }
                               }
                               else
                               {
                                   return false;
                               }
                           }
                       }
                       else
                       {

                           //_curr_prov.Clear();
                           if (_curr_prov.Count() == 0)
                           {
                               _curr_prov.Add("80");
                               _curr_prov.Add("32");
                           }

                           FProvincesList fprov = new FProvincesList();
                           fprov.Text = CLocaliz.TxT("The choice of areas to search for networks and stations without articles");
                           IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                           rsArea.Select("NAME");
                           try
                           {
                               for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                                   fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                               fprov.chlbProvs.Sorted = true;

                               for (int j = 0; j < fprov.chlbProvs.Items.Count; j++)
                               {
                                   for (int jj = 0; jj < _curr_prov.Count; jj++)
                                   {
                                       //if (fprov.chlbProvs.Items[j].ToString().Trim() == GetNameAreaOnCode(_curr_prov[jj].ToString().Trim()))
                                       if (fprov.chlbProvs.Items[j].ToString().Trim() == GetNameAreaOnCode(_curr_prov[jj].Length == 1 ? "0"+_curr_prov[jj] :_curr_prov[jj]))
                                           fprov.chlbProvs.SetItemChecked(j, true);
                                       //break;
                                   }
                               }

                           }
                           finally
                           {
                               rsArea.Close();
                               rsArea.Destroy();
                           }

                           if (fprov.ShowDialog() == DialogResult.OK)
                           {
                               if (fprov.chlbProvs.CheckedItems.Count != fprov.chlbProvs.Items.Count)
                                   foreach (object obj in fprov.chlbProvs.CheckedItems)
                                   {
                                       provs.Add(obj.ToString());
                                   }
                           }
                           else
                           {
                               return false;
                           }

                       }


                       if (!IsOrder)
                       {
                           if ((provs != null) && (provs.Count > 0))
                           {
                               Article.StationWithoutArticle.FindStationWithoutArticle(_ownerIdLocal, out applIds, out netIds, provs);
                           }
                           else
                           {
                               Article.StationWithoutArticle.FindStationWithoutArticle(_ownerIdLocal, out applIds, out netIds);
                           }
                       }
                       else
                       {
                           Article.StationWithoutArticle.FindStationWithoutArticleImportOrder(_ownerIdLocal, out applIds, out netIds);
                       }

                       if ((applIds.Length > 0) || (netIds.Length > 0))
                       {
                           string mess = string.Format("Знайдено  {0} станцій без статей.{1}Продовжити формування заявки?",  applIds.Length, System.Environment.NewLine);
                           //System.Threading.Thread.CurrentThread.Suspend();

                           CustomDialogBox dlg = new CustomDialogBox(mess, "");
                           //MessageBox.Show(dlg.GetStatusPress().ToString());

                           //-----------------------------------------------------------------
                           if (dlg.GetStatusPress() == DialogResultMessageBoxButtons.View)
                           {
                               if (netIds.Length > 0)
                               {
                                   //Сети
                                   StringBuilder sbTmp = new StringBuilder(1000);
                                   int i = 0;
                                   string sql = "";
                                   while (i < netIds.Length)
                                   {
                                       for (int j = 0; j < 200 && i < netIds.Length; ++j, ++i)
                                       {
                                           int id = netIds[i];
                                           sbTmp.AppendFormat("{0},", id);
                                       }
                                       if (sbTmp.Length == 0)
                                           sbTmp.Append("0,");
                                       if (sql == "")
                                           sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                                       else
                                           sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                                   }
                                   if (sql != "")
                                   {
                                         IMDBList.Show(string.Format(CLocaliz.TxT("Net without articles ({0})"), netIds.Length), PlugTbl.XfaNet, sql, "ID", OrderDirection.Ascending, 0, 0);
                                   }


                                   if (MessageBox.Show(CLocaliz.TxT("Continue / discontinue the further formation of the memo? Click Yes / No"), "Внимание!",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                                   {
                                       return false;

                                   }
                                     

                               }
                               if (applIds.Length > 0)
                               {
                                   StringBuilder sbTmp = new StringBuilder(1000);
                                   int i = 0;
                                   string sql = "";
                                   while (i < applIds.Length)
                                   {
                                       for (int j = 0; j < 200 && i < applIds.Length; ++j, ++i)
                                       {
                                           int id = applIds[i];
                                           sbTmp.AppendFormat("{0},", id);
                                       }
                                       if (sbTmp.Length == 0)
                                           sbTmp.Append("0,");
                                       if (sql == "")
                                           sql = string.Format("([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                                       else
                                           sql += string.Format(" or ([ID] in ({0}))", sbTmp.Remove(sbTmp.Length - 1, 1));
                                   }
                                   if (sql != "")
                                   {
                                       ICSM.IMDBList.Show(string.Format(CLocaliz.TxT("Stations without articles ({0})"), applIds.Length), PlugTbl.APPL, sql, "ID", OrderDirection.Ascending, 0, 0);
                                   }


                                   if (MessageBox.Show(CLocaliz.TxT("Continue / discontinue the further formation of the memo? Click Yes / No"), "Внимание!",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                                   {
                                       return false;

                                   }
                                  
                                       
                               }
                           }
                           //------------------------------------------------------------------
                           if (dlg.GetStatusPress() == DialogResultMessageBoxButtons.No)
                           {
                               return false;
                           }

                       }

                       // if (MessageBox.Show(mess, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                       //     return false;

                       SelectStation3(_ownerIdLocal, provs);  //Используем новый функционал
                       hasDiff = HasDiff(_ownerIdLocal);
                   }
                   else
                   {
                       MessageBox.Show("Формування службової за РЧП по платнику для вибраного контракту відмінено \n у зв'язку з тим, що виконано вибір децентралізованого договору");
                       
                       return false;
                   }
                   
               }
           }
           return true;
       }
       /// <summary>
       /// Выборка станций
       /// </summary>
       private void SelectStation3(int ownerIdLocal, List<string> provs_)
       {
           const string constUkraine = "Україна";
           List<string> provs = new List<string>();
           if ((provs_ == null) && (provs_.Count == 0))
           {


               if (string.IsNullOrEmpty(CUsers.GetUserProvince()) == false)
               {
                   provs.Add(CUsers.GetUserProvince());
               }
               else
               {
                   FProvincesList fprov = new FProvincesList();
                   IMRecordset rsArea = new IMRecordset(ICSMTbl.itblArea, IMRecordset.Mode.ReadOnly);
                   rsArea.Select("NAME");
                   try
                   {
                       for (rsArea.Open(); !rsArea.IsEOF(); rsArea.MoveNext())
                           fprov.chlbProvs.Items.Add(rsArea.GetS("NAME"));
                       fprov.chlbProvs.Sorted = true;
                   }
                   finally
                   {
                       rsArea.Close();
                       rsArea.Destroy();
                   }
                   if (fprov.ShowDialog() == DialogResult.OK)
                   {
                       if (fprov.chlbProvs.CheckedItems.Count != fprov.chlbProvs.Items.Count)
                           foreach (object obj in fprov.chlbProvs.CheckedItems)
                           {
                               provs.Add(obj.ToString());
                           }
                   }
               }
           }
           else
           {
               provs = provs_;
           }

           List<KeyValuePair<int, int>> L_ID_Import_Order = new List<KeyValuePair<int, int>>();
           if (IsOrder)
           {
               using (IMRecordset rsAllStation_import = new IMRecordset("XNRFA_IMPORT_ORDER", IMRecordset.Mode.ReadOnly))
               {
                   rsAllStation_import.Select("ID,OWNER_ID,EMPLOYEE_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF");
                   rsAllStation_import.SetWhere("EMPLOYEE_ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
                   rsAllStation_import.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerIdLocal);
                   for (rsAllStation_import.Open(); !rsAllStation_import.IsEOF(); rsAllStation_import.MoveNext())
                   {
                       if (!L_ID_Import_Order.Contains(new KeyValuePair<int, int>(rsAllStation_import.GetI("APPL_ID"), rsAllStation_import.GetI("WORKS_COUNT"))))
                           L_ID_Import_Order.Add(new KeyValuePair<int, int>(rsAllStation_import.GetI("APPL_ID"), rsAllStation_import.GetI("WORKS_COUNT")));
                   }
               }
           }

           List<Xnrfa_Microwa_Mon> L_ID_Microwa_Mon = new List<Xnrfa_Microwa_Mon>();
           if (Actual_Measurement)
           {
               using (IMRecordset rsAllStation_microwa = new IMRecordset("XV_XNRFA_MICROWA_MON", IMRecordset.Mode.ReadOnly))
               {
                   rsAllStation_microwa.Select("ID,MICROWS_ID,STATUS_MON,PLAN_NEW,END_DATE,NOTES,PLAN_PAY,APP_URCM_ID,MICROWA_ID,APPL_ID,WAS_USED,IS_CONFIRMED,PAY_OWNER_ID,PROVINCE");
                   rsAllStation_microwa.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, ownerIdLocal);
                   rsAllStation_microwa.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                   rsAllStation_microwa.SetWhere("IS_CONFIRMED", IMRecordset.Operation.Eq, 1);
                   rsAllStation_microwa.SetWhere("STATUS_MON", IMRecordset.Operation.Eq, "2");
                   rsAllStation_microwa.SetWhere("PLAN_PAY", IMRecordset.Operation.Eq, new DateTime(DateTime.Now.Year,DateTime.Now.Month,1));
                   for (rsAllStation_microwa.Open(); !rsAllStation_microwa.IsEOF(); rsAllStation_microwa.MoveNext()) {
                       Xnrfa_Microwa_Mon mon_xv = new Xnrfa_Microwa_Mon();
                       mon_xv.ID = rsAllStation_microwa.GetI("ID");
                       mon_xv.MICROWS_ID = rsAllStation_microwa.GetI("MICROWS_ID");
                       mon_xv.STATUS_MON = rsAllStation_microwa.GetS("STATUS_MON");
                       mon_xv.PLAN_NEW = rsAllStation_microwa.GetT("PLAN_NEW");
                       mon_xv.END_DATE = rsAllStation_microwa.GetT("END_DATE");
                       mon_xv.NOTES = rsAllStation_microwa.GetS("NOTES");
                       mon_xv.PLAN_PAY = rsAllStation_microwa.GetT("PLAN_PAY");
                       mon_xv.APP_URCM_ID = rsAllStation_microwa.GetI("APP_URCM_ID");
                       mon_xv.MICROWA_ID = rsAllStation_microwa.GetI("MICROWA_ID");
                       mon_xv.APPL_ID = rsAllStation_microwa.GetI("APPL_ID");
                       mon_xv.WAS_USED = rsAllStation_microwa.GetI("WAS_USED");
                       mon_xv.IS_CONFIRMED = rsAllStation_microwa.GetI("IS_CONFIRMED");
                       mon_xv.PAY_OWNER_ID = rsAllStation_microwa.GetI("PAY_OWNER_ID");
                       mon_xv.PROVINCE = rsAllStation_microwa.GetS("PROVINCE");
                       L_ID_Microwa_Mon.Add(mon_xv);
                   }
               }
           }

           List<int> allMicrowaMonId = new List<int>();
           List<int> allMicrowaMonSaved = new List<int>();
           List<int> allNetsId = new List<int>();
           //string[] tableNames = new string[] { PlugTbl.AllStations, PlugTbl.AllDecentral };
           //for (int numTable = 0; numTable < tableNames.Length; numTable++)
           //{
           // string curSelectTableName = tableNames[numTable];

           //string fieldPermEndDate = (curSelectTableName == PlugTbl.AllStations) ? "PERM_DATE_STOP" : "END_DATE";
           //string fieldProvince = (curSelectTableName == PlugTbl.AllStations) ? "PROVINCE" : "USE_REGION";
           //string fieldAddress = (curSelectTableName == PlugTbl.AllStations) ? "ADDRESS" : "ADRESS";
           //string fieldNameShip = (curSelectTableName == PlugTbl.AllStations) ? "" : "NAME_SHIP";
           //string fieldNet = (curSelectTableName == PlugTbl.AllStations) ? "Application.NetSector1.NET_ID" : "Application.Abonent.NET_ID";

           string fieldPermEndDate = "DOZV_DATE_TO";
           string fieldProvince1 = "PROVINCE";
           string fieldProvince2 = "PROVINCE2";
           string fieldAddress = "ADRESS";
           string fieldNameShip = "NAME_SHIP";
           string fieldNet = "NET_ID";


           using (Icsm.LisRecordSet r = new Icsm.LisRecordSet("XV_URCHMPAY", IMRecordset.Mode.ReadOnly))
           {
               r.Select(fieldProvince1);
               r.Select(fieldProvince2);
               r.Select(fieldAddress);
               r.Select("ID");
               r.Select("STANDARD");
               r.Select("PRICE_ID");
               r.Select("PRICE_ID2");
               r.Select("ARTICLE");
               r.Select("ARTICLE2");

               //r.Select("Application.Price.STATUS");
               //r.Select("Application.Price2.STATUS");

               r.Select("DATEOUTACTION");
               r.Select("DATESTARTACTION");

               r.Select("OBJ_TABLE");
               r.Select("OBJ_ID1");
               r.Select("WORKS_COUNT");
               r.Select("WORKS_COUNT2");
               r.Select("CHANGE_PROVINCE");
               r.Select("CHANGE_DOC_DATE");
               r.Select("IS_CONFIRMED");
               r.Select(fieldNet);
               r.Select("DOZV_NUM");
               r.Select(fieldPermEndDate);
               if (string.IsNullOrEmpty(fieldNameShip) == false)
                   r.Select(fieldNameShip);
               //r.SetWhere("Application.WAS_USED", IMRecordset.Operation.Eq, 1);
               r.SetWhere("PAY_OWNER_ID", IMRecordset.Operation.Eq, ownerIdLocal);
               r.OrderBy(fieldPermEndDate, OrderDirection.Ascending);

               using (LisProgressBar pb = new LisProgressBar("Searching stations..."))
               {
                   pb.SetBig("Searching stations...");
                   pb.SetSmall("Please wait...");
                   pb.SetProgress(0, 10000);
                   List<int> listApplId = new List<int>();
                   string allUkrane = PositionState.AllUkr.ToUpper();
                   for (r.Open(); !r.IsEOF(); r.MoveNext())
                   {

                       if (pb.UserCanceled())
                           break;
                       pb.Increment(true);
                       //----
                       int applicationId = r.GetI("ID");
                       int work_d = IM.NullI;

                       if (IsOrder)
                       {
                           bool isContain = false;
                           foreach (KeyValuePair<int, int> val in L_ID_Import_Order)
                           {
                               if (val.Key == applicationId)
                               {
                                   isContain = true;
                                   work_d = val.Value;
                                   break;
                               }
                           }
                           if (!isContain) continue;
                       }

                       if (Actual_Measurement)  {
                           if (r.GetS("STANDARD") == CRadioTech.RS) {
                               bool isContain = false;
                               foreach (Xnrfa_Microwa_Mon val in L_ID_Microwa_Mon)  {
                                   if (val.APPL_ID == applicationId) {
                                       isContain = true;
                                       //work_d = val.Value;
                                       List<Xnrfa_Microwa_Mon> fnd_t = L_ID_Microwa_Mon.FindAll(v => v.APPL_ID == applicationId);
                                       if (fnd_t!=null) {
                                           foreach (Xnrfa_Microwa_Mon fg_ in fnd_t){
                                               if (!allMicrowaMonId.Contains(fg_.ID))
                                                   allMicrowaMonId.Add(fg_.ID);
                                           }
                                       }
                                      
                                       break;
                                   }
                               }
                               if (!isContain) continue;
                           }
                       }

                       if (listApplId.Contains(applicationId))
                           continue;
                       //----
                       string tableObjName = r.GetS("OBJ_TABLE");
                       if (tableObjName == ICSMTbl.itblGSM)
                           continue;

                       //Проверка провинции
                       string tmpProvince = "";
                       string province = "";
                       bool is_change_province = false;
                       DateTime dateChengeProvince = r.GetT("CHANGE_DOC_DATE");
                       if ((dateChengeProvince != IM.NullT) &&
                           (((dateChengeProvince.Year == DateTime.Now.Year) &&
                             (dateChengeProvince.Month < DateTime.Now.Month)) ||
                            (dateChengeProvince.Year < DateTime.Now.Year)))
                       {
                           province = r.GetS("CHANGE_PROVINCE");
                           bool isMy = false;
                           foreach (string prov in provs)
                           {
                               if (province == prov)
                               {
                                   isMy = true;
                                   tmpProvince = prov;
                                   is_change_province = true;
                                   break;
                               }
                           }

                           if ((isMy == false) && (provs.Count > 0))
                               continue;
                       }
                       else
                       {
                           bool isMy = false;
                           province = r.GetS(fieldProvince1);
                           foreach (string prov in provs)
                           {
                               if (province.Contains(prov))
                               {
                                   isMy = true;
                                   //   tmpProvince = prov;
                                   tmpProvince = province;
                                   break;
                               }
                           }
                           if (r.GetS(fieldProvince2) != "")
                           {
                               province = r.GetS(fieldProvince2);
                               foreach (string prov in provs)
                               {
                                   if (province.Contains(prov))
                                   {
                                       isMy = true;
                                       //   tmpProvince = prov;
                                       tmpProvince = province;
                                       break;
                                   }
                               }
                           }

                           if ((isMy == false) && (province.ToUpper() == allUkrane))
                           {
                               isMy = true;
                               tmpProvince = constUkraine;
                           }

                           if (((isMy == false) && (provs.Count > 0)) && (!((CRadioTech.ToStandard(r.GetS("STANDARD")) == CRadioTech.BAUR) && ((province.Contains(";")) || (province.Contains("Вся Україна"))))))
                               continue;
                       }
                       if (string.IsNullOrEmpty(tmpProvince))
                       {
                           tmpProvince = province;
                           if (province.Contains(";"))
                           {
                               string[] allProvince = province.Split(';');
                               if (allProvince.Length > 0)
                                   tmpProvince = allProvince[0].Trim();
                           }

                           if ((tmpProvince.ToUpper() == allUkrane))
                               tmpProvince = constUkraine;
                       }

                       
                       string standardStation = CRadioTech.ToStandard(r.GetS("STANDARD"));
                       UcrfDepartment dep_ = CUsers.GetCurDepartment();

                       if (dep_ == UcrfDepartment.Branch)
                       {
                           if (standardStation == CRadioTech.BAUR)
                           {
                               if (province.Contains(";"))
                               {
                                   continue;
                               }
                               else
                               {
                                   if (CUsers.GetSubProvinceUser().Contains(province))
                                       tmpProvince = CUsers.GetUserProvince();
                                   else continue;

                                   //if (province == CUsers.GetUserProvince())
                                       //tmpProvince = CUsers.GetUserProvince();
                                   //else continue;
                               }
                           }
                       }


                       if ((province.Contains(";")) || (province.Contains("Вся Україна")))
                       {
                           /*
                           if (dep_ == UcrfDepartment.Branch)
                           {
                               if (standardStation == CRadioTech.BAUR)
                               {
                                   bool isFind = false;
                                   province = r.GetS(fieldProvince1);
                                   
                                   //if (451777668 == r.GetI("ID"))
                                    //province = province.Replace("Дніпропетровська; ", "");

                                   if (province.Contains(CUsers.GetUserProvince()))
                                   {
                                       tmpProvince = CUsers.GetUserProvince();
                                       isFind = true;
                                   }
                                   if (!isFind)
                                   {
                                       province = r.GetS(fieldProvince2);

                                       //if (451777668 == r.GetI("ID"))
                                       //province = province.Replace("Дніпропетровська; ", "");

                                       //if (province.Contains(";"))
                                       //{
                                       if (province.Contains(CUsers.GetUserProvince()))
                                       {
                                           tmpProvince = CUsers.GetUserProvince();
                                       }
                                       else continue;
                                       //}
                                       //else continue;
                                   }
                                   //}
                               }
                           }
                            */ 

                           if (dep_ == UcrfDepartment.URCM)
                           {
                               if (standardStation == CRadioTech.BAUR)
                               {
                                   bool isFind = false;
                                   province = r.GetS(fieldProvince1);
                                   // if ((province.Contains(";")) || (province.Contains("Вся Україна"))) {

                                   //if (451777668 == r.GetI("ID"))
                                   //province = province.Replace("Дніпропетровська; ", "");

                                   string[] allProvince = province.Split(';');
                                   if (allProvince.Length > 0)
                                       tmpProvince = allProvince[0].Trim();

                                   if ((province.ToUpper() == allUkrane))
                                       tmpProvince = constUkraine;

                                   isFind = true;
                                   //}

                                   if (!isFind)
                                   {
                                       province = r.GetS(fieldProvince2);
                                       //if ((province.Contains(";")) || (province.Contains("Вся Україна")))
                                       //{
                                       //if (451777668 == r.GetI("ID"))
                                       //province = province.Replace("Дніпропетровська; ", "");

                                       allProvince = province.Split(';');
                                       if (allProvince.Length > 0)
                                           tmpProvince = allProvince[0].Trim();

                                       if ((province.ToUpper() == allUkrane))
                                           tmpProvince = constUkraine;

                                       isFind = true;
                                       //}
                                       //else continue;
                                   }
                               }
                           }
                       }
                            
                           
                       
                     

                       province = tmpProvince;
                       string adres = "";
                       string dozvNum = r.GetS("DOZV_NUM");
                       DateTime validDozvFrom = ApplDocuments.GetValidPermissionDateFrom(applicationId);
                       DateTime validDozvTo = r.GetT(fieldPermEndDate);
                       if (validDozvFrom == IM.NullT)
                           continue;

                       adres = r.GetS(fieldAddress);
                       if (string.IsNullOrEmpty(adres))
                           adres = province;

                       //для караблей берем название судна #6510
                       if (!string.IsNullOrEmpty(fieldNameShip) && tableObjName == ICSMTbl.itblShip)
                           adres = r.GetS(fieldNameShip);

                       // Проверка по сети
                       int netId = r.GetI(fieldNet);

                       if (netId != IM.NullI)
                       {
                           if (allNetsId.Contains(netId))
                               continue; //Пропускаем сеть, переходим на следующую станцию

                           // если значение ID сети не NULL, тогда загрузить перечень станций сети, с целью определения БС с признаком "Технологическая"
                           _net_load.Load(netId);
                           // MessageBox.Show(NetStationItem.isTechUser.ToString() + ": " + netId.ToString());

                           int netPriceId = IM.NullI;
                           string netPriceName = "";
                           int netWorkCount = IM.NullI;
                           string netName = "";
                           bool isNetConfirmed = false;
                           bool isActivated = false;
                           DateTime DateOutAction = IM.NullT;
                           DateTime DateStartAction = IM.NullT;

                           using (Icsm.LisRecordSet rsNet = new Icsm.LisRecordSet(PlugTbl.XfaNet, IMRecordset.Mode.ReadOnly))
                           {
                               rsNet.Select("ID,PRICE_ID,WORKS_COUNT,NAME,Price.ARTICLE,Price.STATUS,Price.DATEOUTACTION,Price.DATESTARTACTION, IS_CONFIRMED");
                               rsNet.SetWhere("ID", IMRecordset.Operation.Eq, netId);
                               rsNet.Open();
                               if (!rsNet.IsEOF())
                               {
                                   netPriceId = rsNet.GetI("PRICE_ID");
                                   netPriceName = rsNet.GetS("Price.ARTICLE");
                                   netWorkCount = rsNet.GetI("WORKS_COUNT");
                                   netName = rsNet.GetS("NAME");
                                   isNetConfirmed = (rsNet.GetI("IS_CONFIRMED") == 1);
                                   isActivated = ArticleLib.ArticleFunc.GetStatusForCodePriceID(netPriceId);
                                   DateOutAction = rsNet.GetT("Price.DATEOUTACTION");
                                   DateStartAction = rsNet.GetT("Price.DATESTARTACTION");
                                   //«Дата припинення дії»>поточна дата>«Дата початку дії»


                               }
                           }
                           if ((netPriceId != IM.NullI) && (isActivated))
                           {
                               standardStation = ArticleFunc.GetStandtartForCodePriceID(netPriceId);

                               // Проверка на возможность подтверждения статьи для технлогий УКХ, ЦУКХ, посредством вызова статического метода NetObject.CanConfirmArticleCustom 
                               if ((netWorkCount != IM.NullI) && isNetConfirmed && NetObject.CanConfirmArticleCustom(netPriceId))
                               //if ((netPriceId != IM.NullI) && (netWorkCount != IM.NullI) && isNetConfirmed )
                               {
                                   //Обрабатываем как сеть
                                   MemoLines.MemoLine line = null;
                                   int itemIndex = art_lines.HasItem(netPriceId, standardStation);
                                   if ((itemIndex > -1) && (itemIndex < art_lines.lines.Count))
                                       line = art_lines.lines[itemIndex];
                                   else
                                   {
                                       line = new MemoLines.MemoLine();
                                       line.article = netPriceName;
                                       line.radioTech = standardStation;
                                       line.workCount = 0;
                                       line.priceId = netPriceId;
                                       art_lines.lines.Add(line);
                                   }
                                   MemoLines.URCMWork workNet = new MemoLines.URCMWork(applicationId,
                                                                                       netWorkCount,
                                                                                       adres, province);
                                   workNet.NetId = netId;
                                   workNet.NetName = netName;
                                   workNet.PermNum = dozvNum;
                                   workNet.PermDateStart = validDozvFrom;
                                   workNet.PermDateStop = validDozvTo;
                                   line.appIds.Add(workNet);
                                   line.workCount += workNet.count;
                                   //---
                                   allNetsId.Add(netId);
                                   continue;
                               }
                           }
                       }
                       //Как обычную станцию
                       listApplId.Add(applicationId);

                       int priceId1 = r.GetI("PRICE_ID");
                       bool isActivated1 = ArticleLib.ArticleFunc.GetStatusForCodePriceID(priceId1);
                       DateTime DateOutAction1 = r.GetT("DATEOUTACTION");
                       DateTime DateStartAction1 = r.GetT("DATESTARTACTION");
                       string articleNameGlogal1 = r.GetS("ARTICLE");
                       int priceId2 = r.GetI("PRICE_ID2");
                       // int isActivated2 = r.GetI("Application.Price2.STATUS");
                       string articleNameGlogal2 = r.GetS("ARTICLE2");



                       int wkCount = r.GetI("WORKS_COUNT"); //кол-во работ из поля в заявке.
                       int wkCount2 = r.GetI("WORKS_COUNT2");
                       bool isConfirmed = (r.GetI("IS_CONFIRMED") == 1);

                       if (tableObjName == ICSMTbl.itblMicrowa)
                       {
                           List<int> listStationId = new List<int>();
                           listStationId.Add(r.GetI("OBJ_ID1"));
                           /*wkCount = BaseAppClass.GetDataForURCM(tableObjName, listStationId,
                                                                 (provs.Count > 0 ? provs : null),
                                                                 ref standardStation,
                                                                 ref adres);*/
                           //SHS----------------------
                           wkCount = 0;
                           char[] trimChars = { ' ', '-' };
                           adres = "";

                           
                           string Province1_ = r.GetS(fieldProvince1).TrimEnd();
                           string Province2_ = r.GetS(fieldProvince2).TrimEnd();


                            adres = r.GetS(fieldAddress);
                            adres = adres.TrimEnd();

                            if (provs != null && provs.Count > 0)
                            {
                                if (provs.Contains(Province1_))
                                {
                                    ++wkCount;
                                }
                                if (provs.Contains(Province2_))
                                {
                                    ++wkCount;
                                }

                                if (is_change_province)
                                {
                                    wkCount = 2;
                                }
                            }
                            else
                            {
                                wkCount = 2;
                            }


                            if (Actual_Measurement)  {
                                wkCount = 0;
                                if (provs != null && provs.Count > 0)
                                {
                                    wkCount = 0;
                                    List<Xnrfa_Microwa_Mon> L_S = L_ID_Microwa_Mon.FindAll(v => v.APPL_ID == applicationId && v.STATUS_MON == "2" && v.PAY_OWNER_ID == ownerIdLocal && v.MICROWA_ID == r.GetI("OBJ_ID1") && (provs.Contains(v.PROVINCE)));
                                    if (L_S != null)
                                    {
                                        if (L_S.Count > 0)
                                        {
                                            wkCount = L_S.Count;
                                            foreach (Xnrfa_Microwa_Mon d in L_S)
                                                allMicrowaMonSaved.Add(d.ID);
                                        }
                                    }
                                    if (wkCount > 2) wkCount = 2;
                                }
                                if (wkCount == 0) continue;
                            }

                           /*

                           using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.AllStations, IMRecordset.Mode.ReadOnly))
                           {
                               rs.Select("ID");
                               rs.Select("ADDRESS");
                               rs.Select("PROVINCE");
                               rs.SetWhere("APPL_ID", IMRecordset.Operation.Eq, applicationId);
                               for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                               {
                                   adres = adres + " - " + rs.GetS("ADDRESS");
                                   adres = adres.TrimStart(trimChars);
                                   adres = adres.TrimEnd();
                                   if (provs != null && provs.Count > 0)
                                   {
                                       if (provs.Contains(rs.GetS("PROVINCE")))
                                           ++wkCount;
                                   }
                                   else
                                   {
                                       wkCount = 2;
                                   }
                               }
                           }
                           //SHS----------------------
                           */

                       }
                       //Проверка кол-ва работ
                       if ((wkCount == IM.NullI) || (priceId1 == IM.NullI) || (isConfirmed == false) || (!isActivated1))
                           continue;


                       
                       //if (IsOrder)
                           //work_d = MemoLines.GetWorkCountFromImportOrder(applicationId);
                       if ((work_d != IM.NullI) && (work_d > 0) && (IsOrder))
                           wkCount = work_d;  
                           

                       MemoLines.URCMWork work = art_lines.AddItem(priceId1,
                                                                   articleNameGlogal1,
                                                                   ArticleFunc.GetStandtartForCodePriceID(priceId1),
                           //standardStation,
                                                                   applicationId,
                                                                   wkCount,
                                                                   adres,
                                                                   tableObjName,
                                                                   province);
                       if (work != null)
                       {
                           work.PermNum = dozvNum;
                           work.PermDateStart = validDozvFrom;
                           work.PermDateStop = validDozvTo;
                       }

                       if ((priceId2 != IM.NullI) && (string.IsNullOrEmpty(articleNameGlogal2) == false) &&
                           (wkCount2 > 0) && (wkCount2 != IM.NullI))
                       //(wkCount2 > 0) && (wkCount2 != IM.NullI) && (isActivated2 == 1))
                       {
                           work = art_lines.AddItem(priceId2,
                                                    articleNameGlogal2,
                                                    ArticleFunc.GetStandtartForCodePriceID(priceId2),
                               //standardStation,
                                                    applicationId,
                                                    wkCount2,
                                                    adres,
                                                    tableObjName,
                                                    province);
                           if (work != null)
                           {
                               work.PermNum = dozvNum;
                               work.PermDateStart = validDozvFrom;
                               work.PermDateStop = validDozvTo;
                           }
                       }
                   }

               }
           }
           //}
           art_lines.Sort();
           if (OnlyWorksSave==false)
            memoId = AllocId();


           if (Actual_Measurement) {
               foreach (int val in allMicrowaMonSaved) {
                   using (IMRecordset rsAllStation_microwa = new IMRecordset("XNRFA_MICROWA_MON", IMRecordset.Mode.ReadWrite)) {
                       rsAllStation_microwa.Select("ID,OBJ_ID,OBJ_TABLE,APP_URCM_ID");
                       rsAllStation_microwa.SetWhere("ID", IMRecordset.Operation.Eq, val);
                       rsAllStation_microwa.Open(); 
                       if (!rsAllStation_microwa.IsEOF()) {
                           rsAllStation_microwa.Edit();
                           rsAllStation_microwa.Put("APP_URCM_ID", memoId);
                           rsAllStation_microwa.Update();
                       }
                       rsAllStation_microwa.Close();
                   }
               }
           }
                   
               
           


           emplId = HelpFunction.getUserIDByName(IM.ConnectedUser());
           CreateDate = DateTime.Now;
           ownerId = ownerIdLocal;
           number = "";
           using (Icsm.LisRecordSet rsEmployee = new Icsm.LisRecordSet(ICSMTbl.itblTskfMember, IMRecordset.Mode.ReadOnly))
           {
               rsEmployee.Select("Taskforce.CUST_TXT1,Employee.TYPE,Employee.IDENT,Employee.APP_USER");
               rsEmployee.SetWhere("Employee.APP_USER", IMRecordset.Operation.Like, IM.ConnectedUser());
               rsEmployee.Open();
               if (!rsEmployee.IsEOF())
                   number = rsEmployee.GetS("Taskforce.CUST_TXT1") + "-" +
                            ConvertType.DepartmetToCode(CUsers.GetUserDepartmentType())
                            + "-" + rsEmployee.GetS("Employee.IDENT") + "-" + memoId;
           }
       }

      /// <summary>
      /// Генерирует следующий ID
      /// </summary>
      /// <returns>Следующий ID</returns>
      private int AllocId()
      {
          //Примечание: Здесь специально использую таблицу itblXnrfaDrvAppl для
          // генерации следующего ID, чтоб была уникальность ID для ДРВ
          return IM.AllocID(PlugTbl.itblXnrfaDrvAppl, 1, -1);
      }
      /// <summary>
      /// Проверка изменений с последней корректной заявкой
      /// </summary>
      /// <returns>TRUE - есть изменения, иначе FALSE</returns>
      private bool HasDiff(int owner_ids)
      {
          int memoApplId = IM.NullI;
          int contract_id = IM.NullI;
          string NumAppl = "";

          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadOnly))
          {
              rs.Select("ID,CONTRACTS_ID");
              rs.SetWhere("STATUS", IMRecordset.Operation.Eq, Enum.GetName(typeof(DRVStatus), DRVStatus.printed));
              rs.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerId);
              if (_id_xnrfa_mon_contr>0)  
              {
                  rs.SetWhere("CONTRACTS_ID", IMRecordset.Operation.Eq, _id_xnrfa_mon_contr); 
              }
              rs.OrderBy("ID", OrderDirection.Descending);
              rs.Open();
              if (!rs.IsEOF())
              {
                  memoApplId = rs.GetI("ID");
                  contract_id = rs.GetI("CONTRACTS_ID");
              }
              
          }
          /*
           using (Icsm.LisRecordSet r_xs = new Icsm.LisRecordSet(PlugTbl.xnrfa_monitor_contracts , IMRecordset.Mode.ReadOnly))
          {
              r_xs.Select("ID, NUMB_CONTRACTS");
              r_xs.SetWhere("ID", IMRecordset.Operation.Eq, contract_id);
              r_xs.Open();
              if (!r_xs.IsEOF())
              {
                  NumAppl = r_xs.GetS("NUMB_CONTRACTS");
              }
          }
           */



          return HasDiff(memoApplId, owner_ids);
      }
      /// <summary>
      /// Проверка изменений
      /// </summary>
      /// <param name="memoApplId">ID заявки</param>
      /// <returns>TRUE - есть изменения, иначе FALSE</returns>
      private bool HasDiff(int memoApplId, int owner_ids)
      {
          MemoLines oldLines = new MemoLines();
          oldLines.Load(memoApplId, IsOrder, owner_ids);
          return !art_lines.Equals(oldLines);
      }
      /*
      //===================================================
      /// <summary>
      ///  Получить все Appl ID заявки
      /// </summary>
      /// <returns></returns>
      public List<int> GetAllApplID(List<MemoLines.MemoLine> ml)
       {
           List<int> Arr = new List<int>();
           foreach (MemoLines.MemoLine item in ml)
           {
               foreach (MemoLines.URCMWork item_work in item.appIds)
               {
                   if (!Arr.Contains(item_work.appId))
                       Arr.Add(item_work.appId);
               }
           }
           return Arr;
       }
      //===================================================
      /// <summary>
      /// Все Price ID заявки
      /// </summary>
      /// <param name="ml"></param>
      /// <returns></returns>
      public List<int> GetAllPriceID(List<MemoLines.MemoLine> ml)
      {
          List<int> Arr = new List<int>();
          foreach (MemoLines.MemoLine item in ml)
          {
                  if (!Arr.Contains(item.priceId))
                      Arr.Add(item.priceId);
          }
          return Arr;
      }
      //===================================================
      /// <summary>
      /// Получить номер разрешения по ApplID
      /// </summary>
      /// <param name="ApplID"></param>
      /// <returns></returns>
      public string GetNumDozv(int ApplID, List<MemoLines.MemoLine> ml)
      {
          string OutNumDozv = "";
          foreach (MemoLines.MemoLine item in ml)
          {
              foreach (MemoLines.URCMWork item_work in item.appIds)
              {
                  if (item_work.appId  == ApplID)
                  {
                      OutNumDozv = item_work.PermNum;
                  }
              }
          }
          return OutNumDozv;
      }
      //===================================================
      /// <summary>
      /// Получить количество работ по PriceId
      /// </summary>
      /// <param name="ApplID"></param>
      /// <returns></returns>
      public int CompareWorkCount(int PriceId, List<MemoLines.MemoLine> ml)
      {
          int CompareWorkCount=0;
          foreach (MemoLines.MemoLine item in ml)
          {
              if (item.priceId == PriceId)
                  {
                      CompareWorkCount = item.workCount;
                  }
          }
          return CompareWorkCount;
      }
      //===================================================    
      /// <summary>
      /// Сравнение номеров дозволов и количества работ для старой и новой заявки
      /// </summary>
      /// <returns></returns>
      public bool CompareStatusDozv(int memoApplId, string NumAppl)
      {
          MemoLines oldLines = new MemoLines();
          oldLines.Load(memoApplId);
          bool isCheck = true;
          List<int> ArrID = new List<int>();
          ArrID = GetAllApplID(art_lines.lines);
          foreach (int item in ArrID)
          {
              if (GetNumDozv(item, art_lines.lines) != GetNumDozv(item, oldLines.lines))
              {
                  isCheck = false;
              }
          }

          List<int> PriceID = new List<int>();
          PriceID = GetAllPriceID(art_lines.lines);
          foreach (int item in PriceID)
          {
              if (CompareWorkCount(item, art_lines.lines) != CompareWorkCount(item, oldLines.lines))
              {
                  isCheck = false;
              }
          }


          if (string.IsNullOrEmpty(NumAppl))
          {
              isCheck = false;
          }

          return !isCheck;
      }
      */ 
      //===================================================
      /// <summary>
      /// Открытие формы заявки
      /// </summary>
      public void ShowForm()
      {
          if (Load(true) == false)
              return;
          using (FBillMemoURCM fMemo = new FBillMemoURCM(this, status))
              fMemo.ShowDialog();
      }
       //===================================================
      /// <summary>
      /// Отправление заявки в ДРВ
      /// </summary>
      internal bool AppToDRV(bool isAuto)
      {
         if ((status != DRVStatus.saved) && (status != DRVStatus.auto_gener))
         {
            MessageBox.Show("Помилка! Необхідно зберегти заявку або дочекатись відповіді з ДРВ", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
         }
         bool isEqualUser = (CUsers.GetUserAreaCode() == CUsers.GetUserAreaCode(emplId));

         if (isEqualUser == false)
         {
             string message = string.Format("Коди філій користувача, який створив заявку і користувача, який виконує дану дію відрізняються:{0}" +
                                            "Заявку створив: {1} ({2}){0}" +
                                            "Дію виконує: {3} ({4}){0}" +
                                            "Продовжити?", 
                                            Environment.NewLine,
                                            CUsers.GetUserFio(emplId), CUsers.GetUserProvince(emplId),
                                            CUsers.GetUserFio(), CUsers.GetUserProvince());
             if(MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
             {
                 message = string.Format("Заявку в Парус буде відправлено від '{1}' філії.{0}" +
                                         "Продовжити?",
                                         Environment.NewLine,
                                         CUsers.GetUserProvince());
                 if (MessageBox.Show(message, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                     isEqualUser = true;
             }
         }
         if (isEqualUser)
         {
            AutoRegimEnum autoRegim = AutoRegimEnum.AutoGenerate;
            if(isAuto == false)
                autoRegim = (status == DRVStatus.auto_gener) ? AutoRegimEnum.AutoStandard : AutoRegimEnum.Standard;

            string fileName = "";
            List<string> errors = new List<string>();
            XmlDocument doc = GlobalDB.CGlobalXML.CreateAppPayXMLforURCM(memoId, ref fileName, ref errors, autoRegim);
            if (errors.Count > 0)
            {
               IMLogFile log = new IMLogFile();
               log.Create("XML_log");
               foreach (string str in errors)
                  log.Warning(str + "\n");
               log.Display("Error");
            }
            else
            {
               GlobalDB.CGlobalDB gdb = new GlobalDB.CGlobalDB();
               gdb.OpenConnection();
               gdb.WriteXMLToDB(doc.OuterXml, EEssence.APPL_PAY_ICSM);
               gdb.CloseConnection();
               using(Icsm.LisTransaction tr = new Icsm.LisTransaction())
               {
                   DRVStatus tmpStatus = (autoRegim == AutoRegimEnum.AutoGenerate) ? DRVStatus.auto : DRVStatus.sent;
                   using (Icsm.LisRecordSet rsApp = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite))
                   {
                       rsApp.Select("ID,STATUS");
                       rsApp.SetWhere("ID", IMRecordset.Operation.Eq, memoId);
                       rsApp.Open();
                       if (!rsApp.IsEOF())
                       {
                           rsApp.Edit();
                           rsApp.Put("STATUS", Enum.GetName(typeof(DRVStatus), tmpStatus));
                           rsApp.Update();
                       }
                       tr.Commit();
                       status = tmpStatus;
                   }
               }
               MessageBox.Show("Файл сохранен под именем " + fileName);
            }
         }
         return isEqualUser;
      }
      
      //===================================================
      /// <summary>
      /// Удаление заявки
      /// </summary>
      /// <returns>Возвращает удалена заявка или нет</returns>
      public bool DeleteApp()
      {
         if (status == DRVStatus.saved && MessageBox.Show("Видалити дану службову записку?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
         {
            IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
            rsApp.Select("ID,STATUS");
            rsApp.SetWhere("ID", IMRecordset.Operation.Eq, memoId);
            try
            {
               rsApp.Open();
               if (!rsApp.IsEOF())
                  rsApp.Delete();
            }
            finally
            {
               rsApp.Final();
            }
            memoId = IM.NullI;
            status = DRVStatus.newapl;
            return true;
         }
         return false;
      }
      //===================================================
      /// <summary>
      /// Сохранение заявки
      /// </summary>
      public void SaveApp()
      {
          if (OnlyWorksSave == false)
          {
              if (memoId == IM.NullI)
                  memoId = AllocId();
              //int _id_xnrfa_mon_contr_new = GlobalDB.CGlobalXML.GetID_Mon_Contracts(ownerId, ICSMTbl.USERS);         
              using (LisProgressBar pb = new LisProgressBar("Saving application..."))
              {
                  pb.SetBig("Saving application...");
                  try
                  {
                      using (Icsm.LisTransaction tran = new Icsm.LisTransaction())
                      {
                          using (Icsm.LisRecordSet rs = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite))
                          {
                              rs.Select("ID,OWNER_ID,CREATED_DATE,STATUS");
                              rs.Select("EMPLOYEE_ID,DOC_PATH,MEMO_NUMBER");
                              rs.Select("HASDIFF,ADD_CREATED_DATE,ADD_DOC_PATH,IS_ORDER,TYPE");
                              //if (_table_Name == PlugTbl.xnrfa_monitor_contracts) { rs.Select("CONTRACTS_ID "); }
                              rs.Select("CONTRACTS_ID");
                              rs.SetWhere("ID", IMRecordset.Operation.Eq, memoId);
                              rs.Open();
                              if (rs.IsEOF())
                              {
                                  // Пакет не существует
                                  rs.AddNew();
                                  rs.Put("ID", memoId);
                                  rs.Put("OWNER_ID", ownerId);
                                  rs.Put("CREATED_DATE", CreateDate);
                                  rs.Put("HASDIFF", hasDiff ? 1 : 0);
                                  if ((_table_Name == PlugTbl.xnrfa_monitor_contracts) || (_table_Name == PlugTbl.XvMonitorUsers)) { rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr); }
                                  if (_table_Name == ICSMTbl.USERS) { rs.Put("CONTRACTS_ID", IM.NullI); }
                                  //if (_table_Name == PlugTbl.xnrfa_monitor_contracts) { rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr); }
                                  //if (_id_xnrfa_mon_contr_new > -1) rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr_new); 

                              }
                              else
                              {
                                  rs.Edit();
                              }

                              if (IsOrder == true)
                                  rs.Put("TYPE", 2);
                              else if (Actual_Measurement == true)
                                  rs.Put("TYPE", 3);
                              else rs.Put("TYPE", 1);


                              rs.Put("IS_ORDER", IsOrder == false ? 0 : 1);
                              rs.Put("STATUS", Enum.GetName(typeof(DRVStatus), status));
                              rs.Put("EMPLOYEE_ID", emplId);
                              rs.Put("DOC_PATH", docPath);
                              rs.Put("MEMO_NUMBER", number);
                              rs.Put("ADD_CREATED_DATE", addCreateDate);
                              rs.Put("ADD_DOC_PATH", addDocFullPath);
                              if ((_table_Name == PlugTbl.xnrfa_monitor_contracts) || (_table_Name == PlugTbl.XvMonitorUsers)) { rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr); }
                              if (_table_Name == ICSMTbl.USERS) { rs.Put("CONTRACTS_ID", IM.NullI); }
                              //if (_table_Name == PlugTbl.xnrfa_monitor_contracts) { rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr); }
                              //if (_id_xnrfa_mon_contr_new > -1) rs.Put("CONTRACTS_ID", _id_xnrfa_mon_contr_new); 
                              rs.Update();
                          }
                          art_lines.Save(memoId, IsOrder);
                          tran.Commit();
                          status = DRVStatus.saved;
                          if (!string.IsNullOrEmpty(_LogInclude))
                            CLogs.WriteInfo(ELogsWhat.RecalcWorks, _LogInclude);
                      }
                  }
                  catch (Exception e)
                  {
                      status = DRVStatus.newapl;
                      MessageBox.Show("Неможливо зберегти заяву:" + e.Message, "Помилка збереження",
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Exclamation);
                  }
              }
          }
          else {
              //art_lines.SaveRecalWorks(memoId, true);
          }
      }
      
     
      //===================================================
      /// <summary>
      /// Печатаем документ
      /// </summary>
      /// <returns></returns>
      public bool PrintDoc()
      {
         bool retVal = false;
         string docType = DocType.DRV;
         // Вытаскиваем IRP файл
         string filter = "APPLURCMDRV";
         IcsmReport[] masRep = IM.ReportsGetList(PlugTbl.itblXnrfaDrvApplURCM, "*" + filter + "*");
         if (masRep.Length == 0)
         {
            MessageBox.Show("Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         string IRPfile = System.IO.Path.GetFileName(masRep[0].Path);

         IM.AdminDisconnect();

         if ((IRPfile == null) || (IRPfile == ""))
         {
            MessageBox.Show("!!!! Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         // Формируем имя файла
         string number = PrintDocs.getListDocNumber(docType, 1)[0];
         DepartmentType curDepart = CUsers.GetUserDepartmentType();
         string fullPath = PrintDocs.getPath(docType);
         if (fullPath == "")
            fullPath = ".";
         fullPath += "\\";
         fullPath += ManagemenUDCR.URCM.ToString("D") + "-" + ConvertType.DepartmetToCode(curDepart) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";

         string shortName = fullPath.ToFileNameWithoutExtension();
         {
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
            try
            {
               rs.Select("ID,DOC_PATH,FULL_PATH");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, memoId);
               rs.Open();
               if (!rs.IsEOF())
               {
                  rs.Edit();
                  rs.Put("DOC_PATH", shortName);
                  rs.Put("FULL_PATH", fullPath);
                  rs.Update();
               }
            }
            finally
            {
               rs.Close();
               rs.Destroy();
            }
         }
         // Печатаем документ
         string lang = "RUS";
         RecordPtr rec = new RecordPtr(PlugTbl.itblXnrfaDrvApplURCM, memoId);
         rec.PrintRTFReport(IRPfile, lang, fullPath, "", false);
         docPath = shortName;
         docFullPath = fullPath;
  /*       if (System.IO.File.Exists(fullPath))
         {// Файд напечатан
            foreach (int curApplID in appIds)
            {
               DisableEvent(curApplID);
               CEventLog.AddApplEvent(curApplID, EDocEvent.evDRV, DateTime.Now, shortName, DateTime.Now, fullPath);
            }
            retVal = true;
         }*/
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Печатаем документ
      /// </summary>
      /// <returns></returns>
      public bool PrintAddDoc(bool needUpd)
      {
         bool retVal = false;
         string docType = DocType.DRV;
         // Вытаскиваем IRP файл
         string filter = "APPLURCMADDDRV";
         IcsmReport[] masRep = IM.ReportsGetList(PlugTbl.itblXnrfaDrvApplURCM, "*" + filter + "*");
         if (masRep.Length == 0)
         {
            MessageBox.Show("Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         string IRPfile = System.IO.Path.GetFileName(masRep[0].Path);

         IM.AdminDisconnect();

         if ((IRPfile == null) || (IRPfile == ""))
         {
            MessageBox.Show("!!!! Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         // Формируем имя файла
         string number = PrintDocs.getListDocNumber(docType, 1)[0];
         DepartmentType curDepart = CUsers.GetUserDepartmentType();
         string fullPath = PrintDocs.getPath(docType);
         if (fullPath == "")
            fullPath = ".";
         fullPath += "\\";
         fullPath += ManagemenUDCR.URCM.ToString("D") + "-" + ConvertType.DepartmetToCode(curDepart) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
         
         string shortName = fullPath.ToFileNameWithoutExtension();
         if(needUpd)
         {            
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvApplURCM, IMRecordset.Mode.ReadWrite);
            try
            {
               rs.Select("ID,ADD_DOC_PATH,ADD_CREATED_DATE");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, memoId);
               rs.Open();
               if (!rs.IsEOF())
               {
                  rs.Edit();
                  rs.Put("ADD_DOC_PATH", fullPath);
                  rs.Put("ADD_CREATED_DATE", DateTime.Now);
                  rs.Update();
               }
            }
            finally
            {
               rs.Close();
               rs.Destroy();
            }
            addDocPath = shortName;
            addDocFullPath = fullPath;
            addCreateDate = DateTime.Now;
         }
         // Печатаем документ
         string lang = "RUS";
         RecordPtr rec = new RecordPtr(PlugTbl.itblXnrfaDrvApplURCM, memoId);
         rec.PrintRTFReport(IRPfile, lang, fullPath, "", false);

         return retVal;
      }
   }


   public class MemoLines
   {
      public List<MemoLine> lines = new List<MemoLine>();
      NetObject _net_load = new NetObject();

      public class URCMWork
      {
         public int Id { get; set; }
         public int appId;
         public int count;
         public string adres;
         public string Province { get; set; }
         public string PermNum { get; set; }
         public DateTime PermDateStart { get; set; } //-
         public DateTime PermDateStop { get; set; }  //-
         public int NetId { get; set; }              //-
         public string NetName { get; set; }         //-
         public URCMWork(int id, int cnt, string adr, string province)
         {
            Id = IM.NullI;
            appId = id;
            count = cnt;
            adres = adr;
            Province = province;
            PermNum = "";
            PermDateStart = IM.NullT;
            PermDateStop = IM.NullT;
            NetId = IM.NullI;
            NetName = "";
         }
      }

      public class MemoLine
      {
         public string article = "";
         public int priceId = IM.NullI;
         public int workCount = 0;
         public string radioTech = "";   //Радиотехнология - стандарт
         public List<URCMWork> appIds = new List<URCMWork>();
         public bool isOrder = false;
      }

      public int HasItem(int prId, string technology)
      {
         foreach (MemoLine line in lines)
            if (line.priceId == prId && (technology == "" || technology == line.radioTech))
               return lines.IndexOf(line);
         return -1;
      }

      public URCMWork AddItem(int prId, string art, string technology, int appId, int wrkCount, string adres, string tableName, string province)
      {
         URCMWork retVal = null;
         if (wrkCount == IM.NullI)
            wrkCount = 1;

         bool has = false;
         for (int i = 0; i < lines.Count; ++i)
         {
            MemoLine line = lines[i];
            if (line.priceId == prId && (technology == "" || technology == line.radioTech))
            {
               bool addNewAppl = true;
               has = true;
               if (tableName == ICSMTbl.itblMicrowa)
               {
                   for (int k = 0; k < line.appIds.Count; k++ )
                   {
                       if (line.appIds[k].appId == appId)
                       {
                           //Создаем новую запись
                           retVal = new URCMWork(appId, line.appIds[k].count + wrkCount, line.appIds[k].adres, province);
                           line.appIds.Add(retVal);
                           line.appIds.RemoveAt(k); //Удаляем старую
                           addNewAppl = false;
                           break;
                       }
                   }
               }
               if(addNewAppl == true)
               {
                   retVal = new URCMWork(appId, wrkCount, adres, province);
                   line.appIds.Add(retVal);
               }
               line.workCount = line.workCount + wrkCount;
               break;
            }
         }
         if (!has)
         {
            retVal = new URCMWork(appId, wrkCount, adres, province);
            MemoLine line = new MemoLine();
            line.appIds = new List<URCMWork>();
            line.workCount += wrkCount;
            line.article = art;
            line.priceId = prId;
            line.radioTech = technology;
            line.appIds.Add(retVal);
            lines.Add(line);
         }
         return retVal;
      }

       /*
      public URCMWork AddItemFast(int prId, string art, string technology, int appId, int wrkCount, string adres,string province)
      {
          URCMWork retVal = null;
          if (wrkCount == IM.NullI)
              wrkCount = 1;
          bool has = false;
          if (!has)
          {
              retVal = new URCMWork(appId, wrkCount, adres, province);
              MemoLine line = new MemoLine();
              line.appIds = new List<URCMWork>();
              line.workCount = wrkCount;
              line.article = art;
              line.priceId = prId;
              line.radioTech = technology;
              line.appIds.Add(retVal);
              lines.Add(line);
          }
          return retVal;
      }
        */ 

      public void Sort()
      {
         List<string> arts = new List<string>();
         foreach (MemoLine l in lines)
            arts.Add(l.article);
         arts.Sort();
         List<MemoLine> temp = new List<MemoLine>();
         foreach(string a in arts)
         {
            for (int i = 0; i < lines.Count; ++i)
               if (lines[i].article == a)
               {
                  temp.Add(lines[i]);
                  lines.RemoveAt(i);
                  break;
               }
         }
         lines.Clear();
         lines = temp;
      }
       /// <summary>
       /// Загружает данные заявки
       /// </summary>
       /// <param name="memoId">ID заявки</param>
      public void Load(int memoId, bool isOrder, int ownerId)
      {
          List<KeyValuePair<int, int>> L_ID_Import_Order = new List<KeyValuePair<int, int>>();
          List<MemoLine> ml_mass = new List<MemoLine>();
          if (!isOrder)
          {
              using (Icsm.LisRecordSet rsAps_works = new Icsm.LisRecordSet(PlugTbl.itblXnrfaWorksUrcm, IMRecordset.Mode.ReadOnly))
              {
                  rsAps_works.Select("ID");
                  rsAps_works.Select("APPLPAY_ID");
                  rsAps_works.Select("COUNT");
                  rsAps_works.Select("PRICE_ID");
                  rsAps_works.Select("Price.ARTICLE");
                  rsAps_works.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, memoId);
                  for (rsAps_works.Open(); !rsAps_works.IsEOF(); rsAps_works.MoveNext())
                  {
                      MemoLine ml = new MemoLine();
                      ml.priceId = rsAps_works.GetI("PRICE_ID");
                      ml.workCount = rsAps_works.GetI("COUNT");
                      ml.article = rsAps_works.GetS("Price.ARTICLE");
                      ml_mass.Add(ml);
                  }
              }
          }
          if (isOrder)
          {
                  using (IMRecordset rsAllStation_import = new IMRecordset("XNRFA_IMPORT_ORDER", IMRecordset.Mode.ReadOnly))
                  {
                      rsAllStation_import.Select("ID,OWNER_ID,EMPLOYEE_ID,PERM_NUM_ORDER,APPL_ID,DESCRIPTION,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF");
                      rsAllStation_import.SetWhere("EMPLOYEE_ID", IMRecordset.Operation.Eq, CUsers.GetCurUserID());
                      rsAllStation_import.SetWhere("OWNER_ID", IMRecordset.Operation.Eq, ownerId);
                      for (rsAllStation_import.Open(); !rsAllStation_import.IsEOF(); rsAllStation_import.MoveNext())
                      {
                          if (!L_ID_Import_Order.Contains(new KeyValuePair<int,int>(rsAllStation_import.GetI("APPL_ID"),rsAllStation_import.GetI("WORKS_COUNT"))))
                              L_ID_Import_Order.Add(new KeyValuePair<int, int>(rsAllStation_import.GetI("APPL_ID"), rsAllStation_import.GetI("WORKS_COUNT")));
                      }
                  }
          }

          using (Icsm.LisRecordSet rsAps = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplAppsURCM, IMRecordset.Mode.ReadOnly))
          {
              rsAps.Select("ID");
              rsAps.Select("APPL_ID");
              rsAps.Select("ADRES");
              rsAps.Select("COUNT");
              rsAps.Select("PROVINCE");
              rsAps.Select("NET_ID");
              rsAps.Select("Net.NAME");
              rsAps.Select("STANDARD");
              rsAps.Select("DOZV_NUM");
              rsAps.Select("DOZV_DATE_FROM");
              rsAps.Select("DOZV_DATE_TO");
              rsAps.Select("PRICE_ID,APP_URCM_ID");
              rsAps.Select("Price.ARTICLE");
              rsAps.Select("Application.OBJ_TABLE");
              rsAps.SetWhere("APP_URCM_ID", IMRecordset.Operation.Eq, memoId);

              List<Works_> LstWorks = new List<Works_>();
              for (rsAps.Open(); !rsAps.IsEOF(); rsAps.MoveNext())
              {
                  int work = IM.NullI;

                  if (isOrder)
                  {
                      bool isContain = false;
                      foreach (KeyValuePair<int, int> val in L_ID_Import_Order)
                      {
                          if (val.Key==rsAps.GetI("APPL_ID"))
                          {
                              isContain = true;
                              work = val.Value;
                              break;
                          }
                      }
                      if (!isContain) continue;
                      //work = MemoLines.GetWorkCountFromImportOrder(rsAps.GetI("APPL_ID"));
                      if (((work == IM.NullI) || (work == 0)) && (isOrder))
                          work = rsAps.GetI("COUNT");

                      MemoLine ml = new MemoLine();
                      ml.priceId = rsAps.GetI("PRICE_ID");
                      ml.workCount = work;
                      ml.article = rsAps.GetS("Price.ARTICLE");
                      ml_mass.Add(ml);
                  }

                      if (work == IM.NullI) work = rsAps.GetI("COUNT");
                  
                      Works_ wks = new Works_();
                      wks.ADRES = rsAps.GetS("ADRES");
                      wks.APP_URCM_ID = rsAps.GetI("APP_URCM_ID");
                      wks.APPL_ID = rsAps.GetI("APPL_ID");
                      wks.Application_OBJ_TABLE = rsAps.GetS("Application.OBJ_TABLE");
                      wks.COUNT = work;
                      wks.DOZV_DATE_FROM = rsAps.GetT("DOZV_DATE_FROM");
                      wks.DOZV_DATE_TO = rsAps.GetT("DOZV_DATE_TO");
                      wks.DOZV_NUM = rsAps.GetS("DOZV_NUM");
                      wks.ID = rsAps.GetI("ID");
                      wks.NET_ID = rsAps.GetI("NET_ID");
                      wks.Net_NAME = rsAps.GetS("Net.NAME");
                      wks.Price_ARTICLE = rsAps.GetS("Price.ARTICLE");
                      wks.PRICE_ID = rsAps.GetI("PRICE_ID");
                      wks.PROVINCE = rsAps.GetS("PROVINCE");
                      wks.STANDARD = rsAps.GetS("STANDARD");

                      LstWorks.Add(wks);


              }
              try
              {

                  if (isOrder)
                  {
                      List<MemoLine> ml_mass_distr = new List<MemoLine>();
                      IEnumerable<int> En_priceid = ml_mass.Select(x => x.priceId).Distinct();
                      foreach (int itm in En_priceid)
                      {
                          List<MemoLine> m = ml_mass.FindAll(r => r.priceId == itm);
                          if (m != null)
                          {
                              if (m.Count > 0)
                              {
                                  int cnt = 0;
                                  foreach (MemoLine t in m)
                                  {
                                      cnt += t.workCount;
                                  }
                                  MemoLine ml = new MemoLine();
                                  ml.priceId = itm;
                                  ml.workCount = cnt;
                                  ml.article = m[m.Count - 1].article;
                                  ml_mass_distr.Add(ml);
                              }
                          }

                      }
                      ml_mass = ml_mass_distr;
                  }
              }
              catch (Exception ex)
              {
                 // MessageBox.Show(ex.Message);
              }


              foreach (MemoLine itm in ml_mass)
              {
                  MemoLine line = new MemoLine();

                  line.appIds = new List<URCMWork>();
                  line.workCount = itm.workCount;
                  line.article = itm.article;
                  line.priceId = itm.priceId;
                  line.radioTech = ArticleFunc.GetStandtartForCodePriceID(itm.priceId);
                  lines.Add(line);

                  foreach (Works_ item in LstWorks)
                  {

                      if ((item.PRICE_ID == itm.priceId) && (item.APP_URCM_ID == memoId))
                      {

                          URCMWork work = new URCMWork(item.APPL_ID, item.COUNT, item.ADRES, item.PROVINCE);
                          lines[lines.Count() - 1].appIds.Add(work);
                          work.Id = item.ID;
                          work.NetId = item.NET_ID;
                          work.NetName = item.Net_NAME;
                          work.PermNum = item.DOZV_NUM;
                          work.PermDateStart = item.DOZV_DATE_FROM;
                          work.PermDateStop = item.DOZV_DATE_TO;

                      }
                  }
              }
          }
      }

      /// <summary>
      /// Получить количество работ с файла заказов, которые соответствуют APPL_ID
      /// </summary>
      /// <param name="APPL_ID"></param>
      /// <returns></returns>
      public static int GetWorkCountFromImportOrder(int APPL_ID)
      {
          int work = IM.NullI;
          IMRecordset rs_imp_x = new IMRecordset("XNRFA_IMPORT_ORDER", IMRecordset.Mode.ReadWrite);
          rs_imp_x.Select("ID,DESCRIPTION,APPL_ID,PERM_NUM_ORDER,OWNER_ID,PERM_NUM,DATE_FROM,DATE_TO,DATE_CANCEL,WORKS_COUNT,WORKS_COUNT_DIFF");
          rs_imp_x.SetWhere("APPL_ID", IMRecordset.Operation.Eq, APPL_ID);
          rs_imp_x.Open();
          if (!rs_imp_x.IsEOF())
          {
              work = rs_imp_x.GetI("WORKS_COUNT");
          }
          if (rs_imp_x.IsOpen())
              rs_imp_x.Close();
          rs_imp_x.Destroy();
          return work;
      }


      public void Save(int memoId, bool isorder)
       {

           using (LisProgressBar pb = new LisProgressBar("Saving application..."))
           {
               using (Icsm.LisTransaction tran = new Icsm.LisTransaction())
               {
                   pb.SetProgress(0, lines.Count);
                   foreach (MemoLine l in lines)
                   {
                       pb.Increment(false);
                       int index = 0;

                       foreach (URCMWork wrk in l.appIds)
                       {
                           pb.SetSmall(++index);
                           using (Icsm.LisRecordSet rsAps = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplAppsURCM, IMRecordset.Mode.ReadWrite))
                           {
                               rsAps.Select("ID");
                               rsAps.Select("APPL_ID");
                               rsAps.Select("ADRES");
                               rsAps.Select("COUNT");
                               rsAps.Select("PROVINCE");
                               rsAps.Select("NET_ID");
                               rsAps.Select("STANDARD");
                               rsAps.Select("DOZV_NUM");
                               rsAps.Select("DOZV_DATE_FROM");
                               rsAps.Select("DOZV_DATE_TO");
                               rsAps.Select("PRICE_ID");
                               rsAps.Select("APP_URCM_ID");

                               rsAps.SetWhere("ID", IMRecordset.Operation.Eq, wrk.Id);
                               rsAps.Open();
                               if (rsAps.IsEOF())
                               {
                                   wrk.Id = IM.AllocID(PlugTbl.itblXnrfaDrvApplAppsURCM, 1, -1);
                                   rsAps.AddNew();
                                   rsAps.Put("ID", wrk.Id);
                                   rsAps.Put("APP_URCM_ID", memoId);
                                   rsAps.Put("ADRES", wrk.adres);

                                   
                                   //if (isorder)
                                       //wrk.count = MemoLines.GetWorkCountFromImportOrder(wrk.appId);

                                   rsAps.Put("COUNT", wrk.count);
                                   rsAps.Put("PROVINCE", wrk.Province);
                                   rsAps.Put("STANDARD", l.radioTech);
                                   rsAps.Put("APPL_ID",wrk.appId);
                                   rsAps.Put("NET_ID", wrk.NetId);
                                   rsAps.Put("PRICE_ID", l.priceId);
                                   rsAps.Put("DOZV_NUM", wrk.PermNum);
                                   rsAps.Put("DOZV_DATE_FROM", wrk.PermDateStart);
                                   rsAps.Put("DOZV_DATE_TO", wrk.PermDateStop);
                                   rsAps.Update();
                               }
                           }

                           ///////////////


                           if (((l.radioTech == CRadioTech.YKX) || (l.radioTech == CRadioTech.ЦУКХ)) && (wrk.NetId!=IM.NullI))
                           {
                               using (Icsm.LisRecordSet rsAps_ = new Icsm.LisRecordSet(PlugTbl.itblXnrfaCHAPPNET, IMRecordset.Mode.ReadWrite))
                               {
                                   rsAps_.Select("ID");
                                   rsAps_.Select("NET_ID");
                                   rsAps_.Select("DOZV_NUM");
                                   rsAps_.Select("DOZV_DATE_FROM");
                                   rsAps_.Select("DOZV_DATE_TO");
                                   rsAps_.Select("APPL_URCM_ID");
                                   rsAps_.Select("APPL_ID");
                                   rsAps_.SetWhere("ID", IMRecordset.Operation.Eq, wrk.Id);
                                   rsAps_.Open();
                                   if (rsAps_.IsEOF())
                                   {
                                       //
                                       using (Icsm.LisRecordSet rspAPPNET = new Icsm.LisRecordSet(PlugTbl.XvPCHAPPNET, IMRecordset.Mode.ReadOnly))
                                       {

                                           rspAPPNET.Select("APP_ID");
                                           rspAPPNET.Select("DOZV_NUM");
                                           rspAPPNET.Select("DOZV_DATE_FROM");
                                           rspAPPNET.Select("DOZV_DATE_TO");
                                           rspAPPNET.Select("NET_ID");
                                           rspAPPNET.SetWhere("NET_ID", IMRecordset.Operation.Eq, wrk.NetId);


                                           for (rspAPPNET.Open(); !rspAPPNET.IsEOF(); rspAPPNET.MoveNext())
                                           {
                                               wrk.Id = IM.AllocID(PlugTbl.itblXnrfaCHAPPNET, 1, -1);
                                               rsAps_.AddNew();

                                               rsAps_.Put("ID", wrk.Id);
                                               rsAps_.Put("NET_ID", rspAPPNET.GetI("NET_ID"));
                                               rsAps_.Put("DOZV_NUM", rspAPPNET.GetS("DOZV_NUM"));
                                               rsAps_.Put("DOZV_DATE_FROM", rspAPPNET.GetT("DOZV_DATE_FROM"));
                                               rsAps_.Put("DOZV_DATE_TO", rspAPPNET.GetT("DOZV_DATE_TO"));
                                               rsAps_.Put("APPL_URCM_ID", memoId);
                                               rsAps_.Put("APPL_ID", rspAPPNET.GetI("APP_ID"));
                                               rsAps_.Update();
                                           }
                                       }

                                   }
                               }
                           }


                       }
                   }

                   tran.Commit();
               }

               /*
               using (Icsm.LisTransaction tran_ = new Icsm.LisTransaction())
               {
                   //
                   pb.SetProgress(0, lines.Count);
                   foreach (MemoLine l in lines)
                   {
                       pb.Increment(false);
                       int index = 0;
                       foreach (URCMWork wrk in l.appIds)
                       {
                           pb.SetSmall(++index);
                           using (Icsm.LisRecordSet rsAps_ = new Icsm.LisRecordSet(PlugTbl.itblXnrfaCHAPPNET, IMRecordset.Mode.ReadWrite))
                           {
                               rsAps_.Select("ID");
                               rsAps_.Select("NET_ID");
                               rsAps_.Select("DOZV_NUM");
                               rsAps_.Select("DOZV_DATE_FROM");
                               rsAps_.Select("DOZV_DATE_TO");
                               rsAps_.Select("APPL_URCM_ID");
                               rsAps_.Select("APPL_ID");
                               rsAps_.SetWhere("ID", IMRecordset.Operation.Eq, wrk.Id);
                               rsAps_.Open();
                               if (rsAps_.IsEOF())
                               {
                                   //


                                   using (Icsm.LisRecordSet rspAPPNET = new Icsm.LisRecordSet(PlugTbl.XvPCHAPPNET, IMRecordset.Mode.ReadOnly))
                                   {
                                      
                                       rspAPPNET.Select("APP_ID");
                                       rspAPPNET.Select("DOZV_NUM");
                                       rspAPPNET.Select("DOZV_DATE_FROM");
                                       rspAPPNET.Select("DOZV_DATE_TO");
                                       rspAPPNET.Select("NET_ID");
                                       rspAPPNET.SetWhere("NET_ID", IMRecordset.Operation.Eq, wrk.NetId);


                                       for (rspAPPNET.Open(); !rspAPPNET.IsEOF(); rspAPPNET.MoveNext())
                                       {


                                           wrk.Id = IM.AllocID(PlugTbl.itblXnrfaCHAPPNET, 1, -1);
                                           rsAps_.AddNew();

                                           rsAps_.Put("ID", wrk.Id);
                                           rsAps_.Put("NET_ID", rspAPPNET.GetI("NET_ID"));
                                           rsAps_.Put("DOZV_NUM", rspAPPNET.GetS("DOZV_NUM"));
                                           rsAps_.Put("DOZV_DATE_FROM", rspAPPNET.GetT("DOZV_DATE_FROM"));
                                           rsAps_.Put("DOZV_DATE_TO", rspAPPNET.GetT("DOZV_DATE_TO"));
                                           rsAps_.Put("APPL_URCM_ID", memoId);
                                           rsAps_.Put("APPL_ID", rspAPPNET.GetI("APP_ID"));
                                           rsAps_.Update();
                                       }
                                   }



                                   //
                                   //wrk.Id = IM.AllocID(PlugTbl.itblXnrfaCHAPPNET, 1, -1);
                                   //rsAps.AddNew();
                                   //rsAps.Put("ID", wrk.Id);
                                   //rsAps.Put("NET_ID", wrk.NetId);
                                   //rsAps.Put("DOZV_NUM", wrk.PermNum);
                                   //rsAps.Put("DOZV_DATE_FROM", wrk.PermDateStart);
                                   //rsAps.Put("DOZV_DATE_TO", wrk.PermDateStop);
                                   //rsAps.Put("APP_URCM_ID", memoId);
                                   //rsAps.Put("APPL_ID", wrk.appId);
                                   //rsAps.Update();

                               }
                           }
                       }
                   }
                   tran_.Commit();
               }
                */
           }
            

           using (LisProgressBar pb = new LisProgressBar("Saving application..."))
           {
               pb.SetProgress(0, 1000);
               using(Icsm.LisRecordSet rsWork = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplWorksURCM, IMRecordset.Mode.ReadWrite))
               {
                   rsWork.Select("ID,APPLPAY_ID,PRICE_ID,COUNT");
                   rsWork.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, memoId);
                   rsWork.Open();
                   bool isAdd = false;
                   foreach (MemoLine l in lines)
                   {
                       pb.Increment(true);
                       if (rsWork.IsEOF() || (isAdd == true))
                       {
                           isAdd = true;
                           int newId = IM.AllocID(PlugTbl.itblXnrfaDrvApplWorksURCM, 1, -1);
                           rsWork.AddNew();
                           rsWork.Put("ID", newId);
                       }
                       else
                       {
                           rsWork.Edit();
                       }
                       rsWork.Put("APPLPAY_ID", memoId);
                       rsWork.Put("PRICE_ID", l.priceId);
                       rsWork.Put("COUNT", l.workCount);
                       rsWork.Update();
                       rsWork.MoveNext();
                   }

                   if (isAdd == false)
                       while (!rsWork.IsEOF())
                       {
                           rsWork.Delete();
                           rsWork.MoveNext();
                           pb.Increment(true);
                       }
               }
           }
       }


      /// <summary>
      /// Сохранение только сведений о работах
      /// </summary>
      /// <param name="memoId"></param>
      /*
      public void SaveRecalWorks(int memoId, bool OnlyWorksSave)
      {
          if (OnlyWorksSave) {
              using (LisProgressBar pb = new LisProgressBar("Saving application...")){
                  pb.SetProgress(0, 1000);
                  using (Icsm.LisRecordSet rsWork = new Icsm.LisRecordSet(PlugTbl.itblXnrfaDrvApplWorksURCM, IMRecordset.Mode.ReadWrite)) {
                      rsWork.Select("ID,APPLPAY_ID,PRICE_ID,COUNT");
                      rsWork.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                      rsWork.Open();
                      if (rsWork.IsEOF()) {
                          foreach (MemoLine l in lines) {
                              pb.Increment(true);

                              int newId = IM.AllocID(PlugTbl.itblXnrfaDrvApplWorksURCM, 1, -1);
                              rsWork.AddNew();
                              rsWork.Put("ID", newId);
                              rsWork.Put("APPLPAY_ID", memoId);
                              rsWork.Put("PRICE_ID", l.priceId);
                              rsWork.Put("COUNT", l.workCount);
                              rsWork.Update();
                          }
                      }
                  }
              }
          }
      }
      */

       #region override
       /// <summary>
       /// Проверка равенства двух заявок
       /// </summary>
       /// <param name="obj">С чем проверяем</param>
       /// <returns>true - равны, иначе false</returns>
       public override bool Equals(object obj)
       {
           if(obj is MemoLines)
           {
               MemoLines memoLine = (MemoLines)obj;
               if (memoLine.lines.Count != lines.Count)
                   return false;

               //Ищем добавление новых разрешениий
               foreach (MemoLine line in memoLine.lines)
               {
                   foreach (URCMWork work in line.appIds)
                   {
                       bool isFind = false;
                       foreach (MemoLine localLine in lines)
                       {
                           if (localLine.priceId == line.priceId)
                           {
                               foreach (URCMWork localAppl in localLine.appIds)
                               {
                                   if (work.PermNum == localAppl.PermNum)
                                   {
                                       isFind = true;
                                       break;
                                   }
                               }
                           }
                       }
                       if (isFind == false)
                           return false;
                   }
               }
               //Ищем удаление старых разрешениий
               foreach (MemoLine localLine in lines)
               {
                   foreach (URCMWork localAppl in localLine.appIds)
                   {
                       bool isFind = false;
                       foreach (MemoLine line in memoLine.lines)
                       {
                           if (localLine.priceId == line.priceId)
                           {
                               foreach (URCMWork work in line.appIds)
                               {
                                   if (work.PermNum == localAppl.PermNum)
                                   {
                                       isFind = true;
                                       break;
                                   }
                               }
                           }
                       }
                       if (isFind == false)
                           return false;
                   }
               }
               return true; // Все совпало
           }
           return false; // Не тот тип
       }

       public bool Equals(MemoLines other)
       {
           if (ReferenceEquals(null, other)) return false;
           if (ReferenceEquals(this, other)) return true;
           return Equals(other, this);
       }

       public override int GetHashCode()
       {
           System.Text.StringBuilder sb = new System.Text.StringBuilder(100);
           foreach (MemoLine localLine in lines)
               foreach (URCMWork localAppl in localLine.appIds)
                   sb.Append(localAppl.PermNum);
           return sb.GetHashCode();
       }
       #endregion
   }


   internal static class AutoRegimEnumExtension
   {
       /// <summary>
       /// Вернуть код режима служебки для XML
       /// </summary>
       /// <param name="regim">Редим</param>
       /// <returns>Код режима</returns>
       public static string Code(this AutoRegimEnum regim)
       {
           switch (regim)
           {
               case AutoRegimEnum.Standard:
                   return "0";
               case AutoRegimEnum.AutoGenerate:
                   return "1";
               case AutoRegimEnum.AutoStandard:
                   return "2";
               default:
                   throw new Exception("Error type");
           }
       }
       /// <summary>
       /// Вернуть код режима служебки для XML
       /// </summary>
       /// <param name="code">Код режима</param>
       /// <returns>Редим</returns>
       public static AutoRegimEnum ToAutoRegimEnum(this string code)
       {
           switch (code)
           {
               case "0":
                   return AutoRegimEnum.Standard;
               case "1":
                   return AutoRegimEnum.AutoGenerate;
               case "2":
                   return AutoRegimEnum.AutoStandard;
               default:
                   return AutoRegimEnum.Standard;
           }
       }
   }

   public class cbItem
   {
       public string short_name;
       public string path_full;
       public override string ToString()
       {
           return short_name;
       }
   }

   public class Works_
   {
       public int ID;
       public int APPL_ID;
       public string ADRES;
       public int COUNT;
       public string PROVINCE;
       public int NET_ID;
       public string Net_NAME;
       public string STANDARD;
       public string DOZV_NUM;
       public DateTime DOZV_DATE_FROM;
       public DateTime DOZV_DATE_TO;
       public int PRICE_ID;
       public string Price_ARTICLE;
       public int APP_URCM_ID;
       public string Application_OBJ_TABLE;


   }

    public class Xnrfa_Microwa_Mon
    {
        public int ID {get; set;}
        public int MICROWS_ID { get; set; }
        public string STATUS_MON { get; set; }
        public DateTime PLAN_NEW { get; set; }
        public DateTime END_DATE { get; set; }
        public string NOTES { get; set; }
        public DateTime PLAN_PAY { get; set; }
        public int APP_URCM_ID { get; set; }
        public int MICROWA_ID { get; set; }
        public int APPL_ID { get; set; }
        public int WAS_USED { get; set; }
        public int IS_CONFIRMED{get; set;}
        public int PAY_OWNER_ID { get; set; }
        public string PROVINCE { get; set; }

        public Xnrfa_Microwa_Mon()
        {
            ID = IM.NullI;
            MICROWA_ID = IM.NullI;
            STATUS_MON = "";
            PLAN_NEW = IM.NullT;
            END_DATE = IM.NullT;
            NOTES = "";
            PLAN_PAY = IM.NullT;
            APP_URCM_ID = IM.NullI;
            MICROWA_ID = IM.NullI;
            APPL_ID = IM.NullI;
            WAS_USED = IM.NullI; IS_CONFIRMED = IM.NullI;
            PAY_OWNER_ID = IM.NullI;
            PROVINCE = "";
        }

    }
}
