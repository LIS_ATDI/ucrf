﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class CRxMode
   {   
      private static List<string> RxModeList = new List<string>();
      
      static CRxMode()
      {
         RxModeList.Add("MO - мобільний");
         RxModeList.Add("FX - фіксований");
         RxModeList.Add("PO - портат. зовн.");
         RxModeList.Add("PI - портат. внутр.");
      }
      
      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getRxModeList()
      {
         return new List<string>(RxModeList);
      }
      
      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение поляризации
      /// </summary>
      /// <param name="longPolarization">полная строка поляризации</param>
      /// <returns>сокращенное значение поляризации</returns>
      public static string getShortRxMode(string longRxMode)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longRxMode.Length; i++)
         {
            if (longRxMode[i] == ' ')
               break;
            else
               sb.Append(longRxMode[i]);
         }
         return sb.ToString();
      }
   }
}
