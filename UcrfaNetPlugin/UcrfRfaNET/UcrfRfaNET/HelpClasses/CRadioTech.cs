﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET
{
    public static class CRadioTech
    {
        public const string UNKNOWN = "UNKNOWN";
        //------------------------------------
        // ЗС
        public const string ZS = "СР";
        //------------------------------------
        // БС (СРТ)
        //public const string BS = "БС";
        // Abonent station
        public const string AS = "AС";
        //------------------------------------
        // РС
        public const string RS = "РРЗ";
        //------------------------------------
        // Мобильщики
        public const string CDMA_450 = "CDMA-450";
        public const string CDMA_800 = "CDMA-800";
        public const string GSM_900 = "GSM-900";
        public const string GSM_1800 = "GSM-1800";
        public const string UMTS = "UMTS";
        public const string DECT = "DECT";
        public const string DAMPS = "D-AMPS";
        //------------------------------------
        // Технический
        public const string KX = "КХ";        
        public const string YKX = "УКХ";
        public const string ЦУКХ = "ЦУКХ";
        public const string ПРІ = "ПРІ";

        public const string PD = "ПД";
        public const string TRUNK = "ТРАНК";
        public const string TETRA = "ТЕТРА";
        public const string PADG = "ПЕЙДЖ";
        public const string ROPS = "РОПС";
        public const string RRK = "РРК";
        public const string SOIPC = "САІРС";
        public const string RPATL = "РПАТЛ";
        public const string RBSS = "РБСС";
        public const string RPS = "РПС";
        public const string RSR = "РСР";
        //-------------------------------------
        // Радио аналогове
        public const string AAB = "АЗМ"; // Analog Audio Broadcasting = Аналогове Звукове Мовлення.
        // Телевидение аналоговое
        public const string ATM = "АТМ"; // Аноалогове телебачення
        // Цифровое телевидение
        public const string DVBT = "DVB-T";
        public const string DVBT2 = "DVB-T2";
        // Цифровое радио
        public const string TDAB = "T-DAB";
        //-------------------------------------
        // ВМА
        public const string SHR = "ШР";
        public const string MsR = "МсР";
        public const string MmR = "МмР";
        public const string BNT = "БНТ";
        public const string SB = "СВ";
        public const string RUZO = "РУЗО";
        public const string BAUR = "БАУР";
        public const string BCUR = "БЦУР";
        public const string SptRZ = "СптРЗ";

        public const string MMS = "РБСС";
        // Випромінювальні пристрої
        public const string VP = "ВП";
        // Аматори
        public const string AP = "АР";

        //===================================================
        public static List<string> getListRadioTech(AppType appType)
        {
            List<string> list = new List<string>();
            switch (appType)
            {
                case AppType.AppBS:
                    list.Add(SHR);
                    list.Add(MsR);
                    list.Add(MmR);
                    list.Add(BNT);
                    break;
                case AppType.AppRR:
                    list.Add(CDMA_450);
                    list.Add(CDMA_800);
                    list.Add(GSM_900);
                    list.Add(GSM_1800);
                    list.Add(UMTS);
                    list.Add(DECT);
                    list.Add(DAMPS);
                    break;
                case AppType.AppRS:
                    list.Add(RS);
                    break;
                case AppType.AppZS:
                    list.Add(ZS);
                    break;
                case AppType.AppTR:
                    list.Add(KX);                    
                    list.Add(YKX);                    
                    list.Add(ЦУКХ);                    
                    list.Add(PD);
                    list.Add(TRUNK);
                    list.Add(TETRA);
                    list.Add(PADG);
                    list.Add(ROPS);
                    list.Add(RRK);
                    list.Add(SOIPC);
                    list.Add(RPATL);
                    list.Add(RBSS);
                    list.Add(RPS);
                    list.Add(RSR);
                    break;
                case AppType.AppTR_З:
                    list.Add(KX);
                    list.Add(YKX);
                    break;
                case AppType.AppR2:
                    list.Add(AAB); // АЗМ
                    break;
                case AppType.AppTV2:
                    list.Add(ATM); // АТМ
                    break;
                case AppType.AppR2d:
                    list.Add(TDAB);
                    break;
                case AppType.AppTV2d://tvdigitall
                    list.Add(DVBT);
                    list.Add(DVBT2);
                    break;
                case AppType.AppAR_3:
                    list.Add(YKX);                    
                    list.Add(ЦУКХ);
                    list.Add(PD);
                    list.Add(RBSS);
                    list.Add(TRUNK);
                    list.Add(TETRA);
                    list.Add(SHR);
                    list.Add(MsR);
                    list.Add(MmR);
                    list.Add(RPATL);
                    list.Add(SB);
                    list.Add(KX);                    
                    list.Add(RUZO);
                    list.Add(ROPS);
                    list.Add(BAUR);
                    list.Add(BCUR);
                    list.Add(RRK);
                    list.Add(SptRZ);
                    list.Add(ПРІ);
                    break;
                case AppType.AppRRTR:
                    list.Add(CDMA_450);
                    list.Add(CDMA_800);
                    list.Add(GSM_900);
                    list.Add(GSM_1800);
                    list.Add(UMTS);
                    list.Add(DECT);
                    list.Add(DAMPS);
                    list.Add(KX);                    
                    list.Add(YKX);                    
                    list.Add(ЦУКХ);                    
                    list.Add(PD);
                    list.Add(TRUNK);
                    list.Add(TETRA);
                    list.Add(PADG);
                    list.Add(ROPS);
                    list.Add(RRK);
                    list.Add(SOIPC);
                    list.Add(RPATL);
                    list.Add(RBSS);
                    list.Add(RPS);
                    break;
                case AppType.AppA3:
                    list.Add(AP);
                    break;
                case AppType.AppR2R2d:
                    list.Add(AAB);
                    list.Add(TDAB);
                    break;
                case AppType.AppTV2TV2d:
                    list.Add(ATM); // АТМ
                    list.Add(DVBT);
                    list.Add(DVBT2);
                    break;
                case AppType.AppVP:
                    list.Add(VP);
                    break;
            case AppType.AppZRS:
                    list.Add(MMS);
               break;
            case AppType.AppAP3:
                    list.Add(KX);
                    list.Add(YKX);
               break;
            }
            return list;
        }
        //===================================================
        /// <summary>
        /// Возвращает список всех стандартов
        /// </summary>
        /// <returns>Список стандартов</returns>
        public static List<string> AllStandard()
        {
            List<string> list = new List<string>();
            list.Add(CDMA_450);
            list.Add(CDMA_800);
            list.Add(GSM_900);
            list.Add(GSM_1800);
            list.Add(UMTS);
            list.Add(DECT);
            list.Add(RS);
            list.Add(ZS);
            list.Add(KX);            
            list.Add(YKX);
            list.Add(ЦУКХ);
            list.Add(PD);
            list.Add(TRUNK);
            list.Add(TETRA);
            list.Add(PADG);
            list.Add(ROPS);
            list.Add(RRK);
            list.Add(SOIPC);
            list.Add(AAB);
            list.Add(ATM);
            list.Add(TDAB);
            list.Add(DVBT);
            list.Add(SHR);
            list.Add(AP);
            list.Add(MsR);
            list.Add(MmR);
            list.Add(BNT);
            list.Add(SB);
            list.Add(RUZO);
            list.Add(BAUR);
            list.Add(BCUR);
            list.Add(RPATL);
            list.Add(AS);
            list.Add(DAMPS);
            list.Add(RBSS);
            list.Add(RPS);
            list.Add(DVBT2);
            list.Add(VP);
            list.Add(SptRZ);
            list.Add(RSR);
            list.Add(ПРІ);
            return list;
        }
        //===================================================
        /// <summary>
        /// Проверка на стандарт
        /// </summary>
        /// <param name="enterStandart">стандарт</param>
        /// <returns>правельный стандарт</returns>
        public static string ToStandard(string enterStandart)
        {
            foreach (string stand in AllStandard())
                if (stand.ToLower() == enterStandart.ToLower())
                    return stand;
            return CRadioTech.UNKNOWN;
        }
        //===================================================
        /// <summary>
        /// Проверка на технологическую станцию по стандарту
        /// </summary>
        /// <param name="standard">название стандарта</param>
        /// <returns>TRUE - технологичный стадарт. FALSE - не технологический стандарт</returns>
        public static bool IsTechnol(string standard)
        {
            return getListRadioTech(AppType.AppTR).Contains(standard);
        }
    }
}
