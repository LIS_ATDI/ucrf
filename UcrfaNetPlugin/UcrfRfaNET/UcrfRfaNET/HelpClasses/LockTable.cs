﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;

namespace XICSM.Lock
{
   class LockTable
   {
      /// <summary>
      /// метод для проверки на блокировку таблиці
      /// </summary>
      /// <param name="fieldName">имя поля в таблице XNRFA_LOCKS</param>
      /// <returns></returns>
      public bool TryLockTable(string fieldName)
      {
         string currentUser = IM.ConnectedUser();
         int tryCount = 0;
         bool retVal = false;
         while ((tryCount < 5) && (retVal != true))
         {
            bool needRetest = false;
            // проверяю на запись существующего поля
            IMRecordset r = new IMRecordset(XICSM.UcrfRfaNET.PlugTbl.LOCKS, IMRecordset.Mode.ReadWrite);
            r.Select("WHAT,WHO,WHEN");
            r.SetWhere("WHAT", IMRecordset.Operation.Eq, fieldName);
            try
            {
               r.Open();
               if (!r.IsEOF())
               {
                  string userName = r.GetS("WHO");
                  if (userName.Equals(currentUser)) // значить хтось редагує таблицю
                     needRetest = true;  //Повторно преверим
               }
               else
               {
                  r.AddNew();
                  r.Put("WHAT", fieldName);
                  r.Put("WHO", currentUser);
                  r.Put("WHEN", DateTime.Now);
                  r.Update();
                  needRetest = true;  //Повторно преверим
               }
            }
            finally
            {
               r.Close();
               r.Destroy();
            }

            if (needRetest && ReplayCheck(fieldName))
               retVal = true;

            tryCount++;
            if (retVal == false)
               System.Threading.Thread.Sleep(1000);  //Засыпаем на 1 сек
         }
         return retVal;
      }

      /// <summary>
      /// Recheck, if records more one then we can write there and must deleted our record with lock table 
      /// Повторна перевірка, якщо записів більше 1 в таблиці то ми не можемо туди писати, і видаляємо з таблиці локів свій запис 
      /// </summary>
      /// <param name="fieldName"></param>
      /// <returns></returns>
      private bool ReplayCheck(string fieldName)
      {
         IMRecordset r = new IMRecordset(XICSM.UcrfRfaNET.PlugTbl.LOCKS, IMRecordset.Mode.ReadOnly);
         r.Select("WHAT,WHO,WHEN");
         r.SetWhere("WHAT", IMRecordset.Operation.Eq, fieldName);
         short count = 0;
         try
         {
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               count++;
               if (count >= 2)
                  UnlockTable(fieldName);
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
         return (count==1);         
      }

      /// <summary>
      /// method for ulocking table
      /// метод для розблокування таблиці
      /// </summary>
      /// <param name="fieldName">имя поля в таблице XNRFA_LOCKS</param>
      /// <returns></returns>
      public void UnlockTable(string fieldName)
      {
         string currentUser = IM.ConnectedUser();

         // проверяю на запись существующего поля
         IMRecordset r = new IMRecordset(XICSM.UcrfRfaNET.PlugTbl.LOCKS, IMRecordset.Mode.ReadWrite);
         r.Select("WHAT,WHO,WHEN");
         r.SetWhere("WHAT", IMRecordset.Operation.Eq, fieldName);
         r.SetWhere("WHO", IMRecordset.Operation.Eq, currentUser);
         try
         {
            r.Open();
            if (!r.IsEOF())
               r.Delete();
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Unlocks all data that have been locked by user
      /// </summary>
      public void UnlockTableAll()
      {
         string currentUser = IM.ConnectedUser();
         IMRecordset r = new IMRecordset(XICSM.UcrfRfaNET.PlugTbl.LOCKS, IMRecordset.Mode.ReadWrite);
         r.Select("WHAT,WHO,WHEN");
         r.SetWhere("WHO", IMRecordset.Operation.Eq, currentUser);
         try
         {
            for (r.Open(); !r.IsEOF(); r.MoveNext())
               r.Delete();
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }

   }
}
