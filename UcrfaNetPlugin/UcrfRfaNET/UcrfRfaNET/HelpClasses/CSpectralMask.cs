﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET.HelpClasses
{      
   class CSpectralMask
   {
      private static List<string> SpectralMaskList = new List<string>();
      
      static CSpectralMask()
      {
         SpectralMaskList.Add("1 - Критично");
         SpectralMaskList.Add("2 - Некритично");
      }
      
      //===================================================
      /// <summary>
      /// Возвращает список поляризаций
      /// </summary>
      /// <returns>список поляризаций</returns>
      public static List<string> getSpectralMaskList()
      {
         return new List<string>(SpectralMaskList);
      }
      
      //===================================================
      /// <summary>
      /// Возвращает сокращенное значение поляризации
      /// </summary>
      /// <param name="longPolarization">полная строка поляризации</param>
      /// <returns>сокращенное значение поляризации</returns>
      public static string getShortSpectralMask(string longMask)
      {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < longMask.Length; i++)
         {
            if (longMask[i] == ' ')
               break;
            else
               sb.Append(longMask[i]);
         }
         return sb.ToString();
      }
   }
}
