﻿namespace XICSM.UcrfRfaNET.HelpClasses
{
   partial class FrmPluginSetting
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPluginSetting));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.gbDocFolderParams = new System.Windows.Forms.GroupBox();
            this.cbUseRegionFieldAsBranchAssign = new System.Windows.Forms.CheckBox();
            this.txUserTypesToIgnore = new System.Windows.Forms.TextBox();
            this.lbExcludedUserTypes = new System.Windows.Forms.Label();
            this.cbUseBranchCodesAsSubfolders = new System.Windows.Forms.CheckBox();
            this.cbUseBranchSubfolders = new System.Windows.Forms.CheckBox();
            this.txBranchUserType = new System.Windows.Forms.TextBox();
            this.lbBranchUserType = new System.Windows.Forms.Label();
            this.chbUsePtkFeaturesInUrcpDrvAppl = new System.Windows.Forms.CheckBox();
            this.gbSeparator = new System.Windows.Forms.GroupBox();
            this.rbTabSeparator = new System.Windows.Forms.RadioButton();
            this.tbUserSeparator = new System.Windows.Forms.TextBox();
            this.rbUserSeparatoe = new System.Windows.Forms.RadioButton();
            this.rbOsSeparator = new System.Windows.Forms.RadioButton();
            this.tbUserDefault = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPageFolderSetting = new System.Windows.Forms.TabPage();
            this.btnFolderForFrequencyFitting = new System.Windows.Forms.Button();
            this.tbFolderForFrequencyFitting = new System.Windows.Forms.TextBox();
            this.lblFolderForFrequencyFitting = new System.Windows.Forms.Label();
            this.btnAktPtk = new System.Windows.Forms.Button();
            this.tbFolderAktPtk = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCalculEmc = new System.Windows.Forms.Button();
            this.tbFolderCalculatedEmc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSelectionFoto = new System.Windows.Forms.Button();
            this.txtBoxSelectionFoto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectionFreq = new System.Windows.Forms.Button();
            this.txtBoxSelectionFreq = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRunCalcCountWorkByApplAndArticle = new System.Windows.Forms.Button();
            this.tbSqlCalcCountWorkByApplAndArticle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRunCalcCountWorkByAppl = new System.Windows.Forms.Button();
            this.tbSqlCalcCountWorkByAppl = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnRunSqlSetArticle = new System.Windows.Forms.Button();
            this.tbSqlSetArticle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPageMap = new System.Windows.Forms.TabPage();
            this.btnBrowseCocot = new System.Windows.Forms.Button();
            this.tbCocotReliefPath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnBrowseMapIni = new System.Windows.Forms.Button();
            this.tbMapIniPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabRecognizer = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.tbScanFolder = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.MaxDaysDocumentRecognize = new System.Windows.Forms.NumericUpDown();
            this.DocFolders = new System.Windows.Forms.TabPage();
            this.dgDocumentFolders = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageDocType = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.dgDocTypes = new System.Windows.Forms.DataGridView();
            this.docTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.codeDocTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.builtInDocTypeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descDocTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.urcpAccessibleDocTypeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.urzpAccessibleDocTypeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.branchAccessibleDocTypeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.packetAccessibleDocTypeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.gbDocFolderParams.SuspendLayout();
            this.gbSeparator.SuspendLayout();
            this.tabPageFolderSetting.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPageMap.SuspendLayout();
            this.tabRecognizer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDaysDocumentRecognize)).BeginInit();
            this.DocFolders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocumentFolders)).BeginInit();
            this.tabPageDocType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabGeneral);
            this.tabControl1.Controls.Add(this.tabPageFolderSetting);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPageMap);
            this.tabControl1.Controls.Add(this.tabRecognizer);
            this.tabControl1.Controls.Add(this.DocFolders);
            this.tabControl1.Controls.Add(this.tabPageDocType);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(596, 402);
            this.tabControl1.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.gbDocFolderParams);
            this.tabGeneral.Controls.Add(this.chbUsePtkFeaturesInUrcpDrvAppl);
            this.tabGeneral.Controls.Add(this.gbSeparator);
            this.tabGeneral.Controls.Add(this.tbUserDefault);
            this.tabGeneral.Controls.Add(this.label17);
            this.tabGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(588, 376);
            this.tabGeneral.TabIndex = 4;
            this.tabGeneral.Text = "General";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // gbDocFolderParams
            // 
            this.gbDocFolderParams.Controls.Add(this.cbUseRegionFieldAsBranchAssign);
            this.gbDocFolderParams.Controls.Add(this.txUserTypesToIgnore);
            this.gbDocFolderParams.Controls.Add(this.lbExcludedUserTypes);
            this.gbDocFolderParams.Controls.Add(this.cbUseBranchCodesAsSubfolders);
            this.gbDocFolderParams.Controls.Add(this.cbUseBranchSubfolders);
            this.gbDocFolderParams.Controls.Add(this.txBranchUserType);
            this.gbDocFolderParams.Controls.Add(this.lbBranchUserType);
            this.gbDocFolderParams.Location = new System.Drawing.Point(9, 210);
            this.gbDocFolderParams.Name = "gbDocFolderParams";
            this.gbDocFolderParams.Size = new System.Drawing.Size(548, 146);
            this.gbDocFolderParams.TabIndex = 4;
            this.gbDocFolderParams.TabStop = false;
            this.gbDocFolderParams.Text = "Параметри збереження документiв";
            // 
            // cbUseRegionFieldAsBranchAssign
            // 
            this.cbUseRegionFieldAsBranchAssign.AutoSize = true;
            this.cbUseRegionFieldAsBranchAssign.Location = new System.Drawing.Point(8, 25);
            this.cbUseRegionFieldAsBranchAssign.Name = "cbUseRegionFieldAsBranchAssign";
            this.cbUseRegionFieldAsBranchAssign.Size = new System.Drawing.Size(257, 17);
            this.cbUseRegionFieldAsBranchAssign.TabIndex = 6;
            this.cbUseRegionFieldAsBranchAssign.Text = "Використовувати поле \'Область\' користувача";
            this.cbUseRegionFieldAsBranchAssign.UseVisualStyleBackColor = true;
            // 
            // txUserTypesToIgnore
            // 
            this.txUserTypesToIgnore.Location = new System.Drawing.Point(277, 36);
            this.txUserTypesToIgnore.Multiline = true;
            this.txUserTypesToIgnore.Name = "txUserTypesToIgnore";
            this.txUserTypesToIgnore.Size = new System.Drawing.Size(243, 89);
            this.txUserTypesToIgnore.TabIndex = 5;
            // 
            // lbExcludedUserTypes
            // 
            this.lbExcludedUserTypes.AutoSize = true;
            this.lbExcludedUserTypes.Location = new System.Drawing.Point(277, 20);
            this.lbExcludedUserTypes.Name = "lbExcludedUserTypes";
            this.lbExcludedUserTypes.Size = new System.Drawing.Size(203, 13);
            this.lbExcludedUserTypes.TabIndex = 4;
            this.lbExcludedUserTypes.Text = "Iгнорувати наступнi типи користувачiв:";
            // 
            // cbUseBranchCodesAsSubfolders
            // 
            this.cbUseBranchCodesAsSubfolders.AutoSize = true;
            this.cbUseBranchCodesAsSubfolders.Location = new System.Drawing.Point(33, 105);
            this.cbUseBranchCodesAsSubfolders.Name = "cbUseBranchCodesAsSubfolders";
            this.cbUseBranchCodesAsSubfolders.Size = new System.Drawing.Size(231, 17);
            this.cbUseBranchCodesAsSubfolders.TabIndex = 3;
            this.cbUseBranchCodesAsSubfolders.Text = "Використовувати коди регiонiв як назви";
            this.cbUseBranchCodesAsSubfolders.UseVisualStyleBackColor = true;
            this.cbUseBranchCodesAsSubfolders.Visible = false;
            // 
            // cbUseBranchSubfolders
            // 
            this.cbUseBranchSubfolders.AutoSize = true;
            this.cbUseBranchSubfolders.Location = new System.Drawing.Point(23, 82);
            this.cbUseBranchSubfolders.Name = "cbUseBranchSubfolders";
            this.cbUseBranchSubfolders.Size = new System.Drawing.Size(120, 17);
            this.cbUseBranchSubfolders.TabIndex = 2;
            this.cbUseBranchSubfolders.Text = "Пiдпапки для фiлiй";
            this.cbUseBranchSubfolders.UseVisualStyleBackColor = true;
            // 
            // txBranchUserType
            // 
            this.txBranchUserType.Location = new System.Drawing.Point(167, 52);
            this.txBranchUserType.Name = "txBranchUserType";
            this.txBranchUserType.Size = new System.Drawing.Size(92, 20);
            this.txBranchUserType.TabIndex = 1;
            // 
            // lbBranchUserType
            // 
            this.lbBranchUserType.AutoSize = true;
            this.lbBranchUserType.Location = new System.Drawing.Point(20, 55);
            this.lbBranchUserType.Name = "lbBranchUserType";
            this.lbBranchUserType.Size = new System.Drawing.Size(140, 13);
            this.lbBranchUserType.TabIndex = 0;
            this.lbBranchUserType.Text = "Код типу користувача фiлii\r\n";
            // 
            // chbUsePtkFeaturesInUrcpDrvAppl
            // 
            this.chbUsePtkFeaturesInUrcpDrvAppl.AutoSize = true;
            this.chbUsePtkFeaturesInUrcpDrvAppl.Location = new System.Drawing.Point(9, 181);
            this.chbUsePtkFeaturesInUrcpDrvAppl.Name = "chbUsePtkFeaturesInUrcpDrvAppl";
            this.chbUsePtkFeaturesInUrcpDrvAppl.Size = new System.Drawing.Size(365, 17);
            this.chbUsePtkFeaturesInUrcpDrvAppl.TabIndex = 3;
            this.chbUsePtkFeaturesInUrcpDrvAppl.Text = "Роботи з ПТК/НВ/ТВ в заявці на виставлення рахунку \"від УРЧП\"";
            this.chbUsePtkFeaturesInUrcpDrvAppl.UseVisualStyleBackColor = true;
            // 
            // gbSeparator
            // 
            this.gbSeparator.Controls.Add(this.rbTabSeparator);
            this.gbSeparator.Controls.Add(this.tbUserSeparator);
            this.gbSeparator.Controls.Add(this.rbUserSeparatoe);
            this.gbSeparator.Controls.Add(this.rbOsSeparator);
            this.gbSeparator.Location = new System.Drawing.Point(9, 44);
            this.gbSeparator.Name = "gbSeparator";
            this.gbSeparator.Size = new System.Drawing.Size(251, 123);
            this.gbSeparator.TabIndex = 2;
            this.gbSeparator.TabStop = false;
            this.gbSeparator.Text = "CSV file separator";
            // 
            // rbTabSeparator
            // 
            this.rbTabSeparator.AutoSize = true;
            this.rbTabSeparator.Location = new System.Drawing.Point(7, 43);
            this.rbTabSeparator.Name = "rbTabSeparator";
            this.rbTabSeparator.Size = new System.Drawing.Size(106, 17);
            this.rbTabSeparator.TabIndex = 3;
            this.rbTabSeparator.TabStop = true;
            this.rbTabSeparator.Text = "Tab list separator";
            this.rbTabSeparator.UseVisualStyleBackColor = true;
            // 
            // tbUserSeparator
            // 
            this.tbUserSeparator.Location = new System.Drawing.Point(187, 65);
            this.tbUserSeparator.Name = "tbUserSeparator";
            this.tbUserSeparator.Size = new System.Drawing.Size(58, 20);
            this.tbUserSeparator.TabIndex = 2;
            // 
            // rbUserSeparatoe
            // 
            this.rbUserSeparatoe.AutoSize = true;
            this.rbUserSeparatoe.Location = new System.Drawing.Point(7, 66);
            this.rbUserSeparatoe.Name = "rbUserSeparatoe";
            this.rbUserSeparatoe.Size = new System.Drawing.Size(109, 17);
            this.rbUserSeparatoe.TabIndex = 1;
            this.rbUserSeparatoe.TabStop = true;
            this.rbUserSeparatoe.Text = "User list separator";
            this.rbUserSeparatoe.UseVisualStyleBackColor = true;
            // 
            // rbOsSeparator
            // 
            this.rbOsSeparator.AutoSize = true;
            this.rbOsSeparator.Location = new System.Drawing.Point(7, 20);
            this.rbOsSeparator.Name = "rbOsSeparator";
            this.rbOsSeparator.Size = new System.Drawing.Size(131, 17);
            this.rbOsSeparator.TabIndex = 0;
            this.rbOsSeparator.TabStop = true;
            this.rbOsSeparator.Text = "Windows list separator";
            this.rbOsSeparator.UseVisualStyleBackColor = true;
            // 
            // tbUserDefault
            // 
            this.tbUserDefault.Location = new System.Drawing.Point(160, 18);
            this.tbUserDefault.Name = "tbUserDefault";
            this.tbUserDefault.Size = new System.Drawing.Size(100, 20);
            this.tbUserDefault.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "User by default";
            // 
            // tabPageFolderSetting
            // 
            this.tabPageFolderSetting.Controls.Add(this.btnFolderForFrequencyFitting);
            this.tabPageFolderSetting.Controls.Add(this.tbFolderForFrequencyFitting);
            this.tabPageFolderSetting.Controls.Add(this.lblFolderForFrequencyFitting);
            this.tabPageFolderSetting.Controls.Add(this.btnAktPtk);
            this.tabPageFolderSetting.Controls.Add(this.tbFolderAktPtk);
            this.tabPageFolderSetting.Controls.Add(this.label5);
            this.tabPageFolderSetting.Controls.Add(this.btnCalculEmc);
            this.tabPageFolderSetting.Controls.Add(this.tbFolderCalculatedEmc);
            this.tabPageFolderSetting.Controls.Add(this.label4);
            this.tabPageFolderSetting.Controls.Add(this.btnSelectionFoto);
            this.tabPageFolderSetting.Controls.Add(this.txtBoxSelectionFoto);
            this.tabPageFolderSetting.Controls.Add(this.label3);
            this.tabPageFolderSetting.Controls.Add(this.btnSelectionFreq);
            this.tabPageFolderSetting.Controls.Add(this.txtBoxSelectionFreq);
            this.tabPageFolderSetting.Controls.Add(this.label1);
            this.tabPageFolderSetting.Location = new System.Drawing.Point(4, 22);
            this.tabPageFolderSetting.Name = "tabPageFolderSetting";
            this.tabPageFolderSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFolderSetting.Size = new System.Drawing.Size(588, 376);
            this.tabPageFolderSetting.TabIndex = 0;
            this.tabPageFolderSetting.Text = "Folders";
            this.tabPageFolderSetting.UseVisualStyleBackColor = true;
            // 
            // btnFolderForFrequencyFitting
            // 
            this.btnFolderForFrequencyFitting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFolderForFrequencyFitting.Location = new System.Drawing.Point(549, 212);
            this.btnFolderForFrequencyFitting.Name = "btnFolderForFrequencyFitting";
            this.btnFolderForFrequencyFitting.Size = new System.Drawing.Size(33, 23);
            this.btnFolderForFrequencyFitting.TabIndex = 14;
            this.btnFolderForFrequencyFitting.Text = "...";
            this.btnFolderForFrequencyFitting.UseVisualStyleBackColor = true;
            this.btnFolderForFrequencyFitting.Click += new System.EventHandler(this.btnFolderForFrequencyFitting_Click);
            // 
            // tbFolderForFrequencyFitting
            // 
            this.tbFolderForFrequencyFitting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFolderForFrequencyFitting.Location = new System.Drawing.Point(10, 214);
            this.tbFolderForFrequencyFitting.Name = "tbFolderForFrequencyFitting";
            this.tbFolderForFrequencyFitting.Size = new System.Drawing.Size(518, 20);
            this.tbFolderForFrequencyFitting.TabIndex = 13;
            // 
            // lblFolderForFrequencyFitting
            // 
            this.lblFolderForFrequencyFitting.AutoSize = true;
            this.lblFolderForFrequencyFitting.Location = new System.Drawing.Point(7, 198);
            this.lblFolderForFrequencyFitting.Name = "lblFolderForFrequencyFitting";
            this.lblFolderForFrequencyFitting.Size = new System.Drawing.Size(129, 13);
            this.lblFolderForFrequencyFitting.TabIndex = 12;
            this.lblFolderForFrequencyFitting.Text = "Folder for frequency fitting";
            // 
            // btnAktPtk
            // 
            this.btnAktPtk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAktPtk.Location = new System.Drawing.Point(549, 162);
            this.btnAktPtk.Name = "btnAktPtk";
            this.btnAktPtk.Size = new System.Drawing.Size(33, 23);
            this.btnAktPtk.TabIndex = 11;
            this.btnAktPtk.Text = "...";
            this.btnAktPtk.UseVisualStyleBackColor = true;
            this.btnAktPtk.Click += new System.EventHandler(this.btnAktPtk_Click);
            // 
            // tbFolderAktPtk
            // 
            this.tbFolderAktPtk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFolderAktPtk.Location = new System.Drawing.Point(10, 164);
            this.tbFolderAktPtk.Name = "tbFolderAktPtk";
            this.tbFolderAktPtk.Size = new System.Drawing.Size(518, 20);
            this.tbFolderAktPtk.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Folder for AKT PTK";
            // 
            // btnCalculEmc
            // 
            this.btnCalculEmc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalculEmc.Location = new System.Drawing.Point(549, 116);
            this.btnCalculEmc.Name = "btnCalculEmc";
            this.btnCalculEmc.Size = new System.Drawing.Size(33, 23);
            this.btnCalculEmc.TabIndex = 8;
            this.btnCalculEmc.Text = "...";
            this.btnCalculEmc.UseVisualStyleBackColor = true;
            this.btnCalculEmc.Click += new System.EventHandler(this.btnCalculEmc_Click);
            // 
            // tbFolderCalculatedEmc
            // 
            this.tbFolderCalculatedEmc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFolderCalculatedEmc.Location = new System.Drawing.Point(10, 118);
            this.tbFolderCalculatedEmc.Name = "tbFolderCalculatedEmc";
            this.tbFolderCalculatedEmc.Size = new System.Drawing.Size(518, 20);
            this.tbFolderCalculatedEmc.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Folder for calculated EMC";
            // 
            // btnSelectionFoto
            // 
            this.btnSelectionFoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectionFoto.Location = new System.Drawing.Point(549, 69);
            this.btnSelectionFoto.Name = "btnSelectionFoto";
            this.btnSelectionFoto.Size = new System.Drawing.Size(33, 23);
            this.btnSelectionFoto.TabIndex = 5;
            this.btnSelectionFoto.Text = "...";
            this.btnSelectionFoto.UseVisualStyleBackColor = true;
            this.btnSelectionFoto.Click += new System.EventHandler(this.btnSelectionFoto_Click);
            // 
            // txtBoxSelectionFoto
            // 
            this.txtBoxSelectionFoto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSelectionFoto.Location = new System.Drawing.Point(10, 71);
            this.txtBoxSelectionFoto.Name = "txtBoxSelectionFoto";
            this.txtBoxSelectionFoto.Size = new System.Drawing.Size(518, 20);
            this.txtBoxSelectionFoto.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Folder for selection foto file";
            // 
            // btnSelectionFreq
            // 
            this.btnSelectionFreq.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectionFreq.Location = new System.Drawing.Point(549, 21);
            this.btnSelectionFreq.Name = "btnSelectionFreq";
            this.btnSelectionFreq.Size = new System.Drawing.Size(33, 23);
            this.btnSelectionFreq.TabIndex = 2;
            this.btnSelectionFreq.Text = "...";
            this.btnSelectionFreq.UseVisualStyleBackColor = true;
            this.btnSelectionFreq.Click += new System.EventHandler(this.btnSelectionFreq_Click);
            // 
            // txtBoxSelectionFreq
            // 
            this.txtBoxSelectionFreq.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSelectionFreq.Location = new System.Drawing.Point(10, 23);
            this.txtBoxSelectionFreq.Name = "txtBoxSelectionFreq";
            this.txtBoxSelectionFreq.Size = new System.Drawing.Size(518, 20);
            this.txtBoxSelectionFreq.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folder for selection frequensies file";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.btnRunCalcCountWorkByApplAndArticle);
            this.tabPage1.Controls.Add(this.tbSqlCalcCountWorkByApplAndArticle);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.btnRunCalcCountWorkByAppl);
            this.tabPage1.Controls.Add(this.tbSqlCalcCountWorkByAppl);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.btnRunSqlSetArticle);
            this.tabPage1.Controls.Add(this.tbSqlSetArticle);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(588, 376);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Sql queries";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(6, 155);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(575, 29);
            this.label8.TabIndex = 13;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // btnRunCalcCountWorkByApplAndArticle
            // 
            this.btnRunCalcCountWorkByApplAndArticle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunCalcCountWorkByApplAndArticle.Location = new System.Drawing.Point(493, 130);
            this.btnRunCalcCountWorkByApplAndArticle.Name = "btnRunCalcCountWorkByApplAndArticle";
            this.btnRunCalcCountWorkByApplAndArticle.Size = new System.Drawing.Size(88, 23);
            this.btnRunCalcCountWorkByApplAndArticle.TabIndex = 12;
            this.btnRunCalcCountWorkByApplAndArticle.Text = "Test";
            this.btnRunCalcCountWorkByApplAndArticle.UseVisualStyleBackColor = true;
            this.btnRunCalcCountWorkByApplAndArticle.Click += new System.EventHandler(this.btnRunCalcCountWorkByApplAndArticle_Click);
            // 
            // tbSqlCalcCountWorkByApplAndArticle
            // 
            this.tbSqlCalcCountWorkByApplAndArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSqlCalcCountWorkByApplAndArticle.Location = new System.Drawing.Point(9, 132);
            this.tbSqlCalcCountWorkByApplAndArticle.Name = "tbSqlCalcCountWorkByApplAndArticle";
            this.tbSqlCalcCountWorkByApplAndArticle.Size = new System.Drawing.Size(478, 20);
            this.tbSqlCalcCountWorkByApplAndArticle.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(330, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Sql query for calculating works for one application with special article";
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(6, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(575, 29);
            this.label7.TabIndex = 9;
            this.label7.Text = "Note: The string \'{APPL_ID}\' will be changed to the real application ID. For exam" +
    "ple: \'SELECT COUNT_WORK({APPL_ID}) FROM %XNRFA_APPL\'";
            // 
            // btnRunCalcCountWorkByAppl
            // 
            this.btnRunCalcCountWorkByAppl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunCalcCountWorkByAppl.Location = new System.Drawing.Point(493, 62);
            this.btnRunCalcCountWorkByAppl.Name = "btnRunCalcCountWorkByAppl";
            this.btnRunCalcCountWorkByAppl.Size = new System.Drawing.Size(88, 23);
            this.btnRunCalcCountWorkByAppl.TabIndex = 8;
            this.btnRunCalcCountWorkByAppl.Text = "Test";
            this.btnRunCalcCountWorkByAppl.UseVisualStyleBackColor = true;
            this.btnRunCalcCountWorkByAppl.Click += new System.EventHandler(this.btnRunCalcCountWorkByAppl_Click);
            // 
            // tbSqlCalcCountWorkByAppl
            // 
            this.tbSqlCalcCountWorkByAppl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSqlCalcCountWorkByAppl.Location = new System.Drawing.Point(9, 64);
            this.tbSqlCalcCountWorkByAppl.Name = "tbSqlCalcCountWorkByAppl";
            this.tbSqlCalcCountWorkByAppl.Size = new System.Drawing.Size(478, 20);
            this.tbSqlCalcCountWorkByAppl.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(241, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Sql query for calculating works for one application";
            // 
            // btnRunSqlSetArticle
            // 
            this.btnRunSqlSetArticle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunSqlSetArticle.Location = new System.Drawing.Point(493, 23);
            this.btnRunSqlSetArticle.Name = "btnRunSqlSetArticle";
            this.btnRunSqlSetArticle.Size = new System.Drawing.Size(88, 23);
            this.btnRunSqlSetArticle.TabIndex = 5;
            this.btnRunSqlSetArticle.Text = "Run";
            this.btnRunSqlSetArticle.UseVisualStyleBackColor = true;
            this.btnRunSqlSetArticle.Click += new System.EventHandler(this.btnRunSqlSetArticle_Click);
            // 
            // tbSqlSetArticle
            // 
            this.tbSqlSetArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSqlSetArticle.Location = new System.Drawing.Point(9, 25);
            this.tbSqlSetArticle.Name = "tbSqlSetArticle";
            this.tbSqlSetArticle.Size = new System.Drawing.Size(478, 20);
            this.tbSqlSetArticle.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sql query for setting articles";
            // 
            // tabPageMap
            // 
            this.tabPageMap.Controls.Add(this.btnBrowseCocot);
            this.tabPageMap.Controls.Add(this.tbCocotReliefPath);
            this.tabPageMap.Controls.Add(this.label10);
            this.tabPageMap.Controls.Add(this.btnBrowseMapIni);
            this.tabPageMap.Controls.Add(this.tbMapIniPath);
            this.tabPageMap.Controls.Add(this.label11);
            this.tabPageMap.Location = new System.Drawing.Point(4, 22);
            this.tabPageMap.Name = "tabPageMap";
            this.tabPageMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMap.Size = new System.Drawing.Size(588, 376);
            this.tabPageMap.TabIndex = 2;
            this.tabPageMap.Text = "Map";
            this.tabPageMap.UseVisualStyleBackColor = true;
            // 
            // btnBrowseCocot
            // 
            this.btnBrowseCocot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseCocot.Location = new System.Drawing.Point(548, 72);
            this.btnBrowseCocot.Name = "btnBrowseCocot";
            this.btnBrowseCocot.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseCocot.TabIndex = 11;
            this.btnBrowseCocot.Text = "...";
            this.btnBrowseCocot.UseVisualStyleBackColor = true;
            this.btnBrowseCocot.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbCocotReliefPath
            // 
            this.tbCocotReliefPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCocotReliefPath.Location = new System.Drawing.Point(9, 74);
            this.tbCocotReliefPath.Name = "tbCocotReliefPath";
            this.tbCocotReliefPath.Size = new System.Drawing.Size(518, 20);
            this.tbCocotReliefPath.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "COCOTReliefData path:";
            // 
            // btnBrowseMapIni
            // 
            this.btnBrowseMapIni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseMapIni.Location = new System.Drawing.Point(548, 24);
            this.btnBrowseMapIni.Name = "btnBrowseMapIni";
            this.btnBrowseMapIni.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseMapIni.TabIndex = 8;
            this.btnBrowseMapIni.Text = "...";
            this.btnBrowseMapIni.UseVisualStyleBackColor = true;
            this.btnBrowseMapIni.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbMapIniPath
            // 
            this.tbMapIniPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMapIniPath.Location = new System.Drawing.Point(9, 26);
            this.tbMapIniPath.Name = "tbMapIniPath";
            this.tbMapIniPath.Size = new System.Drawing.Size(518, 20);
            this.tbMapIniPath.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "maps.ini path:";
            // 
            // tabRecognizer
            // 
            this.tabRecognizer.Controls.Add(this.label19);
            this.tabRecognizer.Controls.Add(this.tbScanFolder);
            this.tabRecognizer.Controls.Add(this.label18);
            this.tabRecognizer.Controls.Add(this.label16);
            this.tabRecognizer.Controls.Add(this.MaxDaysDocumentRecognize);
            this.tabRecognizer.Location = new System.Drawing.Point(4, 22);
            this.tabRecognizer.Name = "tabRecognizer";
            this.tabRecognizer.Padding = new System.Windows.Forms.Padding(3);
            this.tabRecognizer.Size = new System.Drawing.Size(588, 376);
            this.tabRecognizer.TabIndex = 3;
            this.tabRecognizer.Text = "Recognizer";
            this.tabRecognizer.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(204, 77);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(352, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Підстрока \'%DOC_FOLDER%\' замінюється папкой елект. документа";
            // 
            // tbScanFolder
            // 
            this.tbScanFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbScanFolder.Location = new System.Drawing.Point(207, 45);
            this.tbScanFolder.Name = "tbScanFolder";
            this.tbScanFolder.Size = new System.Drawing.Size(375, 20);
            this.tbScanFolder.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(160, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Папка збереження скан копій";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(195, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Вибірка документрів за останні дні(в)";
            // 
            // MaxDaysDocumentRecognize
            // 
            this.MaxDaysDocumentRecognize.Location = new System.Drawing.Point(207, 18);
            this.MaxDaysDocumentRecognize.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.MaxDaysDocumentRecognize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaxDaysDocumentRecognize.Name = "MaxDaysDocumentRecognize";
            this.MaxDaysDocumentRecognize.Size = new System.Drawing.Size(60, 20);
            this.MaxDaysDocumentRecognize.TabIndex = 0;
            this.MaxDaysDocumentRecognize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // DocFolders
            // 
            this.DocFolders.Controls.Add(this.dgDocumentFolders);
            this.DocFolders.Location = new System.Drawing.Point(4, 22);
            this.DocFolders.Name = "DocFolders";
            this.DocFolders.Padding = new System.Windows.Forms.Padding(3);
            this.DocFolders.Size = new System.Drawing.Size(588, 376);
            this.DocFolders.TabIndex = 5;
            this.DocFolders.Text = "Document folders";
            this.DocFolders.UseVisualStyleBackColor = true;
            this.DocFolders.Enter += new System.EventHandler(this.DocFolders_Enter);
            // 
            // dgDocumentFolders
            // 
            this.dgDocumentFolders.AllowUserToAddRows = false;
            this.dgDocumentFolders.AllowUserToDeleteRows = false;
            this.dgDocumentFolders.AllowUserToResizeRows = false;
            this.dgDocumentFolders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDocumentFolders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgDocumentFolders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDocumentFolders.Location = new System.Drawing.Point(3, 3);
            this.dgDocumentFolders.MultiSelect = false;
            this.dgDocumentFolders.Name = "dgDocumentFolders";
            this.dgDocumentFolders.RowHeadersVisible = false;
            this.dgDocumentFolders.Size = new System.Drawing.Size(582, 370);
            this.dgDocumentFolders.TabIndex = 0;
            this.dgDocumentFolders.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDocumentFolders_CellContentClick);
            this.dgDocumentFolders.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDocumentFolders_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Width = 120;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Width = 120;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Width = 30;
            // 
            // tabPageDocType
            // 
            this.tabPageDocType.Controls.Add(this.label20);
            this.tabPageDocType.Controls.Add(this.dgDocTypes);
            this.tabPageDocType.Location = new System.Drawing.Point(4, 22);
            this.tabPageDocType.Name = "tabPageDocType";
            this.tabPageDocType.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDocType.Size = new System.Drawing.Size(588, 376);
            this.tabPageDocType.TabIndex = 6;
            this.tabPageDocType.Text = "Типи документів";
            this.tabPageDocType.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(546, 39);
            this.label20.TabIndex = 1;
            this.label20.Text = resources.GetString("label20.Text");
            // 
            // dgDocTypes
            // 
            this.dgDocTypes.AllowUserToAddRows = false;
            this.dgDocTypes.AllowUserToDeleteRows = false;
            this.dgDocTypes.AllowUserToResizeRows = false;
            this.dgDocTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDocTypes.AutoGenerateColumns = false;
            this.dgDocTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDocTypes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codeDocTypeColumn,
            this.builtInDocTypeColumn,
            this.descDocTypeColumn,
            this.urcpAccessibleDocTypeColumn,
            this.urzpAccessibleDocTypeColumn,
            this.branchAccessibleDocTypeColumn,
            this.packetAccessibleDocTypeColumn});
            this.dgDocTypes.DataSource = this.docTypeBindingSource;
            this.dgDocTypes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgDocTypes.Location = new System.Drawing.Point(7, 55);
            this.dgDocTypes.Name = "dgDocTypes";
            this.dgDocTypes.RowHeadersVisible = false;
            this.dgDocTypes.Size = new System.Drawing.Size(575, 312);
            this.dgDocTypes.TabIndex = 0;
            // 
            // docTypeBindingSource
            // 
            this.docTypeBindingSource.DataSource = typeof(XICSM.UcrfRfaNET.Documents.DocType);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(528, 429);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(434, 429);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "maps.ini|maps.ini||";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(549, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(10, 164);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(518, 20);
            this.textBox1.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Folder for AKT PTK";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(549, 116);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(10, 118);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(518, 20);
            this.textBox2.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Folder for calculated EMC";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(549, 69);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.Location = new System.Drawing.Point(10, 71);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(518, 20);
            this.textBox3.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Folder for selection foto file";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(549, 21);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.Location = new System.Drawing.Point(10, 23);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(518, 20);
            this.textBox4.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(169, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Folder for selection frequensies file";
            // 
            // codeDocTypeColumn
            // 
            this.codeDocTypeColumn.DataPropertyName = "code";
            this.codeDocTypeColumn.Frozen = true;
            this.codeDocTypeColumn.HeaderText = "Код";
            this.codeDocTypeColumn.Name = "codeDocTypeColumn";
            this.codeDocTypeColumn.ReadOnly = true;
            this.codeDocTypeColumn.Width = 70;
            // 
            // builtInDocTypeColumn
            // 
            this.builtInDocTypeColumn.DataPropertyName = "builtIn";
            this.builtInDocTypeColumn.HeaderText = "Вбуд.";
            this.builtInDocTypeColumn.Name = "builtInDocTypeColumn";
            this.builtInDocTypeColumn.ReadOnly = true;
            this.builtInDocTypeColumn.ThreeState = true;
            this.builtInDocTypeColumn.Width = 35;
            // 
            // descDocTypeColumn
            // 
            this.descDocTypeColumn.DataPropertyName = "desc";
            this.descDocTypeColumn.HeaderText = "Опис";
            this.descDocTypeColumn.Name = "descDocTypeColumn";
            this.descDocTypeColumn.ReadOnly = true;
            this.descDocTypeColumn.Width = 300;
            // 
            // urcpAccessibleDocTypeColumn
            // 
            this.urcpAccessibleDocTypeColumn.DataPropertyName = "urcpAccessible";
            this.urcpAccessibleDocTypeColumn.HeaderText = "УРЧП";
            this.urcpAccessibleDocTypeColumn.Name = "urcpAccessibleDocTypeColumn";
            this.urcpAccessibleDocTypeColumn.Width = 50;
            // 
            // urzpAccessibleDocTypeColumn
            // 
            this.urzpAccessibleDocTypeColumn.DataPropertyName = "urzpAccessible";
            this.urzpAccessibleDocTypeColumn.HeaderText = "УРЗП";
            this.urzpAccessibleDocTypeColumn.Name = "urzpAccessibleDocTypeColumn";
            this.urzpAccessibleDocTypeColumn.Width = 50;
            // 
            // branchAccessibleDocTypeColumn
            // 
            this.branchAccessibleDocTypeColumn.DataPropertyName = "branchAccessible";
            this.branchAccessibleDocTypeColumn.HeaderText = "Філія";
            this.branchAccessibleDocTypeColumn.Name = "branchAccessibleDocTypeColumn";
            this.branchAccessibleDocTypeColumn.Width = 50;
            // 
            // packetAccessibleDocTypeColumn
            // 
            this.packetAccessibleDocTypeColumn.DataPropertyName = "packetAccessible";
            this.packetAccessibleDocTypeColumn.HeaderText = "Пакет";
            this.packetAccessibleDocTypeColumn.Name = "packetAccessibleDocTypeColumn";
            this.packetAccessibleDocTypeColumn.Width = 50;
            // 
            // FrmPluginSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 464);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FrmPluginSetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UCRF plugin settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPluginSetting_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.gbDocFolderParams.ResumeLayout(false);
            this.gbDocFolderParams.PerformLayout();
            this.gbSeparator.ResumeLayout(false);
            this.gbSeparator.PerformLayout();
            this.tabPageFolderSetting.ResumeLayout(false);
            this.tabPageFolderSetting.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPageMap.ResumeLayout(false);
            this.tabPageMap.PerformLayout();
            this.tabRecognizer.ResumeLayout(false);
            this.tabRecognizer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDaysDocumentRecognize)).EndInit();
            this.DocFolders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDocumentFolders)).EndInit();
            this.tabPageDocType.ResumeLayout(false);
            this.tabPageDocType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPageFolderSetting;
      private System.Windows.Forms.Button btnSelectionFreq;
      private System.Windows.Forms.TextBox txtBoxSelectionFreq;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.Button btnOk;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.Button btnRunSqlSetArticle;
      private System.Windows.Forms.TextBox tbSqlSetArticle;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button btnSelectionFoto;
      private System.Windows.Forms.TextBox txtBoxSelectionFoto;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
      private System.Windows.Forms.Button btnCalculEmc;
      private System.Windows.Forms.TextBox tbFolderCalculatedEmc;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button btnAktPtk;
      private System.Windows.Forms.TextBox tbFolderAktPtk;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Button btnRunCalcCountWorkByAppl;
      private System.Windows.Forms.TextBox tbSqlCalcCountWorkByAppl;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Button btnRunCalcCountWorkByApplAndArticle;
      private System.Windows.Forms.TextBox tbSqlCalcCountWorkByApplAndArticle;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TabPage tabPageMap;
      private System.Windows.Forms.Button btnBrowseCocot;
      private System.Windows.Forms.TextBox tbCocotReliefPath;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Button btnBrowseMapIni;
      private System.Windows.Forms.TextBox tbMapIniPath;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
      private System.Windows.Forms.OpenFileDialog openFileDialog1;
      private System.Windows.Forms.Button btnFolderForFrequencyFitting;
      private System.Windows.Forms.TextBox tbFolderForFrequencyFitting;
      private System.Windows.Forms.Label lblFolderForFrequencyFitting;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Button button2;
      private System.Windows.Forms.TextBox textBox2;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Button button3;
      private System.Windows.Forms.TextBox textBox3;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Button button4;
      private System.Windows.Forms.TextBox textBox4;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.TabPage tabRecognizer;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.NumericUpDown MaxDaysDocumentRecognize;
      private System.Windows.Forms.TabPage tabGeneral;
      private System.Windows.Forms.TextBox tbUserDefault;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.TabPage DocFolders;
      private System.Windows.Forms.DataGridView dgDocumentFolders;
      private System.Windows.Forms.TextBox tbScanFolder;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.GroupBox gbSeparator;
      private System.Windows.Forms.RadioButton rbUserSeparatoe;
      private System.Windows.Forms.RadioButton rbOsSeparator;
      private System.Windows.Forms.TextBox tbUserSeparator;
      private System.Windows.Forms.RadioButton rbTabSeparator;
      private System.Windows.Forms.TabPage tabPageDocType;
      private System.Windows.Forms.DataGridView dgDocTypes;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.BindingSource docTypeBindingSource;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
      private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
      private System.Windows.Forms.CheckBox chbUsePtkFeaturesInUrcpDrvAppl;
      private System.Windows.Forms.GroupBox gbDocFolderParams;
      private System.Windows.Forms.TextBox txUserTypesToIgnore;
      private System.Windows.Forms.Label lbExcludedUserTypes;
      private System.Windows.Forms.CheckBox cbUseBranchCodesAsSubfolders;
      private System.Windows.Forms.CheckBox cbUseBranchSubfolders;
      private System.Windows.Forms.TextBox txBranchUserType;
      private System.Windows.Forms.Label lbBranchUserType;
      private System.Windows.Forms.CheckBox cbUseRegionFieldAsBranchAssign;
      private System.Windows.Forms.DataGridViewTextBoxColumn codeDocTypeColumn;
      private System.Windows.Forms.DataGridViewCheckBoxColumn builtInDocTypeColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn descDocTypeColumn;
      private System.Windows.Forms.DataGridViewCheckBoxColumn urcpAccessibleDocTypeColumn;
      private System.Windows.Forms.DataGridViewCheckBoxColumn urzpAccessibleDocTypeColumn;
      private System.Windows.Forms.DataGridViewCheckBoxColumn branchAccessibleDocTypeColumn;
      private System.Windows.Forms.DataGridViewCheckBoxColumn packetAccessibleDocTypeColumn;
   }
}