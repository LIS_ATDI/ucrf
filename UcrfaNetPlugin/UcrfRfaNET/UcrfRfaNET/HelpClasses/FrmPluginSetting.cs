﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.Documents;
using System.Collections.Generic;
using System.Linq;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    internal partial class FrmPluginSetting : Form, INotifyPropertyChanged
    {
        private const string GridCellButtonName = "GridCellButton";
        private const string GridCellPathName = "GridCellPath";
        private static int _inputedApplId = ICSM.IM.NullI;
        private static string _inputedArticle = "";
        private DocPath _documentPath = new DocPath();
        private bool _isFolderInit = false;

        private Documents.Documents docTypeConf = null;

        /// <summary>
        /// Поле для папки сохранения файлов подбора частот
        /// </summary>
        public string FolderSelectionFreq
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderSelectionFreq;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderSelectionFreq)
                {
                    PluginSetting.PluginFolderSetting.FolderSelectionFreq = value;
                    NotifyPropertyChanged("FolderSelectionFreq");
                }
            }
        }

        /// <summary>
        /// Поле для папки сохранения файлов с именами фото
        /// </summary>
        public string FolderSelectionFoto
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderSelectionFoto;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderSelectionFoto)
                {
                    PluginSetting.PluginFolderSetting.FolderSelectionFoto = value;
                    NotifyPropertyChanged("FolderSelectionFoto");
                }
            }
        }
        /// <summary>
        /// Поле для папки сохранения файлов с именами фото
        /// </summary>
        public string FolderCalculatedEmc
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderCalculatedEmc;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderCalculatedEmc)
                {
                    PluginSetting.PluginFolderSetting.FolderCalculatedEmc = value;
                    NotifyPropertyChanged("FolderCalculatedEmc");
                }
            }
        }
        /// <summary>
        /// Поле для папки сохранения файлов с именами фото
        /// </summary>
        public string FolderAktPtk
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderAktPtk;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderAktPtk)
                {
                    PluginSetting.PluginFolderSetting.FolderAktPtk = value;
                    NotifyPropertyChanged("FolderAktPtk");
                }
            }
        }

        /// <summary>
        /// Поле для папки подбора частот
        /// </summary>
        public string FolderFreqFitting
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderFreqFitting;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderFreqFitting)
                {
                    PluginSetting.PluginFolderSetting.FolderFreqFitting = value;
                    NotifyPropertyChanged("FolderFreqFitting");
                }
            }
        }

        /// <summary>
        /// Поле для sql запрса установки статей
        /// </summary>
        public string SqlSetArticle
        {
            get
            {
                return PluginSetting.PluginFolderSetting.SqlSetArticle;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.SqlSetArticle)
                {
                    PluginSetting.PluginFolderSetting.SqlSetArticle = value;
                    NotifyPropertyChanged("SqlSetArticle");
                }
            }
        }
        /// <summary>
        /// Поле для sql запрса подсчета работ по заявке
        /// </summary>
        public string SqlCalcWorksAppl
        {
            get
            {
                return PluginSetting.PluginFolderSetting.SqlSetCalcWorksAppl;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.SqlSetCalcWorksAppl)
                {
                    PluginSetting.PluginFolderSetting.SqlSetCalcWorksAppl = value;
                    NotifyPropertyChanged("SqlCalcWorksAppl");
                }
            }
        }
        /// <summary>
        /// Поле для sql запрса подсчета работ по заявке и статьи
        /// </summary>
        public string SqlCalcWorksApplAndArticle
        {
            get
            {
                return PluginSetting.PluginFolderSetting.SqlSetCalcWorksApplAndArticle;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.SqlSetCalcWorksApplAndArticle)
                {
                    PluginSetting.PluginFolderSetting.SqlSetCalcWorksApplAndArticle = value;
                    NotifyPropertyChanged("SqlCalcWorksApplAndArticle");
                }
            }
        }

        public string RsaGeographyMapsIni
        {
            get
            {
                return PluginSetting.PluginFolderSetting.RsaGeographyMapsIni;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.RsaGeographyMapsIni)
                {
                    PluginSetting.PluginFolderSetting.RsaGeographyMapsIni = value;
                    NotifyPropertyChanged("RsaGeographyMapsIni");
                }
            }
        }

        public string RsaGeographyRelief
        {
            get
            {
                return PluginSetting.PluginFolderSetting.RsaGeographyRelief;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.RsaGeographyRelief)
                {
                    PluginSetting.PluginFolderSetting.RsaGeographyRelief = value;
                    NotifyPropertyChanged("RsaGeographyRelief");
                }
            }
        }
        /// <summary>
        /// Кол-во дней, за поторые необходимо выбрать документы
        /// </summary>
        public int MaxDaysForDocuments
        {
            get
            {
                return PluginSetting.PluginFolderSetting.MaxDaysDocumentRecognizer;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.MaxDaysDocumentRecognizer)
                {
                    PluginSetting.PluginFolderSetting.MaxDaysDocumentRecognizer = value;
                    NotifyPropertyChanged("MaxDaysForDocuments");
                }
            }
        }

        public bool UseRegionFieldAsBranchAssign
        {
            get { return PluginSetting.PluginFolderSetting.UseRegionFieldAsBranchAssign; }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.UseRegionFieldAsBranchAssign)
                {
                    PluginSetting.PluginFolderSetting.UseRegionFieldAsBranchAssign = value;
                    NotifyPropertyChanged("UseRegionFieldAsBranchIndicator");
                }
            }
        }

        public string BranchUserType 
        {
            get { return PluginSetting.PluginFolderSetting.BranchUserTypeCode; }
            set { 
                if (value != PluginSetting.PluginFolderSetting.BranchUserTypeCode)
                {
                    PluginSetting.PluginFolderSetting.BranchUserTypeCode = value;
                    NotifyPropertyChanged("BranchUserType");
                }
            }
        }

        public bool UseBranchSubfolders
        {
            get { return PluginSetting.PluginFolderSetting.UseBranchSubfolders; }
            set
            {
                if (PluginSetting.PluginFolderSetting.UseBranchSubfolders != value)
                {
                    PluginSetting.PluginFolderSetting.UseBranchSubfolders = value;
                    NotifyPropertyChanged("UseBranchSubfolders");
                }
            }
        }

        public string UserTypeCodesToIgnore 
        {
            get { return PluginSetting.PluginFolderSetting.UserTypeCodesToIgnore.ToDelimitedString(Environment.NewLine); }
            set 
            {
                if (PluginSetting.PluginFolderSetting.UserTypeCodesToIgnore.ToDelimitedString(Environment.NewLine) != value)
                {
                    PluginSetting.PluginFolderSetting.UserTypeCodesToIgnore = new List<string>(value.Split(new Char[0], StringSplitOptions.RemoveEmptyEntries));
                    NotifyPropertyChanged("UserTypeCodesToIgnore");
                }
            }
        }

        public bool UsePtkFeaturesInUrcpDrvAppl
        {
            get
            {
                return PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl)
                {
                    PluginSetting.PluginFolderSetting.UsePtkFeaturesInUrcpDrvAppl = value;
                    NotifyPropertyChanged("UsePtkFeaturesInUrcpDrvAppl");
                }
            }
        }
        /// <summary>
        /// Папка скан копий
        /// </summary>
        public string FolderScanDocuments
        {
            get
            {
                return PluginSetting.PluginFolderSetting.FolderDocumentRecognizer;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.FolderDocumentRecognizer)
                {
                    PluginSetting.PluginFolderSetting.FolderDocumentRecognizer = value;
                    NotifyPropertyChanged("FolderScanDocuments");
                }
            }
        }
        /// <summary>
        /// Пользователь по умолчанию (если не можем определить текущего пользователя)
        /// </summary>
        public string UserByDefault
        {
            get
            {
                return PluginSetting.PluginFolderSetting.UserByDefault;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.UserByDefault)
                {
                    PluginSetting.PluginFolderSetting.UserByDefault = value;
                    NotifyPropertyChanged("UserByDefault");
                }
            }
        }
        /// <summary>
        /// Тип разделителя
        /// </summary>
        public PluginSetting.TypeSeparator TypeSeparatorCsv
        {
            get
            {
                return PluginSetting.PluginFolderSetting.TypeSeparator;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.TypeSeparator)
                {
                    PluginSetting.PluginFolderSetting.TypeSeparator = value;
                    NotifyPropertyChanged("TypeSeparatorCsv");
                }
            }
        }
        /// <summary>
        /// Пользовательский разделителя
        /// </summary>
        public string UserSeparatorCsv
        {
            get
            {
                return PluginSetting.PluginFolderSetting.UserSeparator;
            }
            set
            {
                if (value != PluginSetting.PluginFolderSetting.UserSeparator)
                {
                    PluginSetting.PluginFolderSetting.UserSeparator = value;
                    NotifyPropertyChanged("UserSeparatorCsv");
                }
            }
        }

        /// <summary>
        /// Коструктор
        /// </summary>
        public FrmPluginSetting()
        {            
            InitializeComponent();
            //-------         
            docTypeConf = new Documents.Documents();
            docTypeConf.LoadWfConfiguration();
            docTypeBindingSource.DataSource = from dt in docTypeConf._docTypes.Values where (dt.code != DocType.DocUnknown) select dt;

            txtBoxSelectionFreq.DataBindings.Add("Text", this, "FolderSelectionFreq");
            txtBoxSelectionFoto.DataBindings.Add("Text", this, "FolderSelectionFoto");
            tbFolderCalculatedEmc.DataBindings.Add("Text", this, "FolderCalculatedEmc");
            tbFolderAktPtk.DataBindings.Add("Text", this, "FolderAktPtk");
            tbSqlSetArticle.DataBindings.Add("Text", this, "SqlSetArticle");
            tbSqlCalcCountWorkByAppl.DataBindings.Add("Text", this, "SqlCalcWorksAppl");
            tbSqlCalcCountWorkByApplAndArticle.DataBindings.Add("Text", this, "SqlCalcWorksApplAndArticle");
            tbCocotReliefPath.DataBindings.Add("Text", this, "RsaGeographyRelief");
            tbMapIniPath.DataBindings.Add("Text", this, "RsaGeographyMapsIni");
            tbFolderForFrequencyFitting.DataBindings.Add("Text", this, "FolderFreqFitting");
            tbUserDefault.DataBindings.Add("Text", this, "UserByDefault");
            MaxDaysDocumentRecognize.DataBindings.Add("Value", this, "MaxDaysForDocuments");
            tbScanFolder.DataBindings.Add("Text", this, "FolderScanDocuments");
            tbUserSeparator.DataBindings.Add("Text", this, "UserSeparatorCsv");

            cbUseRegionFieldAsBranchAssign.DataBindings.Add("Checked", this, "UseRegionFieldAsBranchAssign");
            lbBranchUserType.DataBindings.Add("Enabled", cbUseRegionFieldAsBranchAssign, "Checked");
            txBranchUserType.DataBindings.Add("Text", this, "BranchUserType");
            txBranchUserType.DataBindings.Add("Enabled", cbUseRegionFieldAsBranchAssign, "Checked");
            cbUseBranchSubfolders.DataBindings.Add("Checked", this, "UseBranchSubfolders");
            cbUseBranchSubfolders.DataBindings.Add("Enabled", cbUseRegionFieldAsBranchAssign, "Checked");
            txUserTypesToIgnore.DataBindings.Add("Text", this, "UserTypeCodesToIgnore");

            chbUsePtkFeaturesInUrcpDrvAppl.DataBindings.Add("Checked", this, "UsePtkFeaturesInUrcpDrvAppl");
            //---
            rbOsSeparator.CheckedChanged += delegate
                                                {
                                                    if (rbOsSeparator.Checked)
                                                    {
                                                        TypeSeparatorCsv = PluginSetting.TypeSeparator.OsSeparator;
                                                        tbUserSeparator.Enabled = false;
                                                    }
                                                };
            rbTabSeparator.CheckedChanged += delegate
                                                  {
                                                      if (rbTabSeparator.Checked)
                                                      {
                                                          TypeSeparatorCsv = PluginSetting.TypeSeparator.TabSeparator;
                                                          tbUserSeparator.Enabled = false;
                                                      }
                                                  };
            rbUserSeparatoe.CheckedChanged += delegate
                                                  {
                                                      if (rbUserSeparatoe.Checked)
                                                      {
                                                          TypeSeparatorCsv = PluginSetting.TypeSeparator.UserSeparator;
                                                          tbUserSeparator.Enabled = true;
                                                      }
                                                  };
            //---
            rbOsSeparator.Checked = (TypeSeparatorCsv == PluginSetting.TypeSeparator.OsSeparator);
            rbTabSeparator.Checked = (TypeSeparatorCsv == PluginSetting.TypeSeparator.TabSeparator);
            rbUserSeparatoe.Checked = (TypeSeparatorCsv == PluginSetting.TypeSeparator.UserSeparator);
            //---
        }
        /// <summary>
        /// Инициализация грида "Папки документов"
        /// </summary>
        private void InitGridDocumentFolder()
        {
            dgDocumentFolders.Columns.Clear();
            dgDocumentFolders.AutoGenerateColumns = false;
            // Тип документа
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.Name = "gridDocType";
            column.HeaderText = "Тип документа";
            column.Width = 240;
            column.ReadOnly = true;
            column.DataPropertyName = DocPath.DocPathType.FieldDocTypeString;
            dgDocumentFolders.Columns.Add(column);
            // Департамент
            column = new DataGridViewTextBoxColumn();
            column.Name = "gridDepType";
            column.HeaderText = "Департамент";
            column.Width = 50;
            column.ReadOnly = true;
            column.DataPropertyName = DocPath.DocPathType.FieldDepTypeString;
            dgDocumentFolders.Columns.Add(column);
            // Folder
            column = new DataGridViewTextBoxColumn();
            column.Name = GridCellPathName;
            column.HeaderText = "Папка";
            column.Width = 245;
            column.ReadOnly = false;
            column.DataPropertyName = DocPath.DocPathType.FieldPath;
            dgDocumentFolders.Columns.Add(column);
            // Button
            DataGridViewButtonColumn btnColumn = new DataGridViewButtonColumn();
            btnColumn.Name = GridCellButtonName;
            btnColumn.HeaderText = "";
            btnColumn.UseColumnTextForButtonValue = true;
            btnColumn.Text = "...";
            btnColumn.Width = 25;
            dgDocumentFolders.Columns.Add(btnColumn);
            //
            dgDocumentFolders.DataSource = _documentPath.ListDocPath;
        }
        /// <summary>
        /// Выберает новую папку для файлов подбора частот
        /// </summary>
        private void btnSelectionFreq_Click(object sender, EventArgs e)
        {
            string newFolder;
            if (IsSelectedNewFolder(out newFolder))
                FolderSelectionFreq = newFolder;
        }
        /// <summary>
        /// Показывает окно для выбора новой папки
        /// </summary>
        /// <param name="newFolder">Возвращает полный путь к папке</param>
        /// <returns>TRUE - выбрана новая папка, иначе FALSE</returns>
        private bool IsSelectedNewFolder(out string newFolder)
        {
            bool retVal = false;
            newFolder = "";
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                newFolder = folderBrowserDialog.SelectedPath;
                retVal = true;
            }
            return retVal;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        /// <summary>
        /// Сохраняем изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            PluginSetting.PluginFolderSetting.Save();
            if (_isFolderInit)
            {
                _documentPath.Save();
            }
            docTypeConf.SaveWfConfiguration();
        }
        /// <summary>
        /// Закрывается форма
        /// </summary>
        private void FrmPluginSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Перезагрузим параметры
            try
            {
                PluginSetting.PluginFolderSetting.Load();
            }
            catch { }
        }
        /// <summary>
        /// Выполнить SQL запрос для установки статей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRunSqlSetArticle_Click(object sender, EventArgs e)
        {
            int retVal = ICSM.IM.Execute(SqlSetArticle);
            MessageBox.Show(string.Format(CLocaliz.TxT("Sql query '{0}' was executed and returned value ({1})."), SqlSetArticle, retVal),
                            CLocaliz.TxT("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// Выполняет SQL запрос подсчета кол-ва работ по заявке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRunCalcCountWorkByAppl_Click(object sender, EventArgs e)
        {
            ICSM.IMDialog dialogNewNet = new ICSM.IMDialog();
            dialogNewNet.Add("txtApplId", "", _inputedApplId, CLocaliz.TxT("Appl ID"));
            if (dialogNewNet.Enter() == false)
                return;
            string inputVal = dialogNewNet.Get("txtApplId").ToString();
            _inputedApplId = inputVal.ToInt32(ICSM.IM.NullI);
            int retVal = ICSM.IM.NullI;
            string sql = SqlCalcWorksAppl.Replace("{APPL_ID}", _inputedApplId.ToString());
            if (ICSM.IM.ExecuteScalar(ref retVal, sql) == true)
            {
                MessageBox.Show(string.Format("Query '{0}' has returned value {1}", sql, retVal));
            }
            else
            {
                MessageBox.Show(string.Format("Query '{0}' has finished with error", sql));
            }
        }
        /// <summary>
        /// Выполняет SQL запрос подсчета кол-ва работ по заявке и по статье
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRunCalcCountWorkByApplAndArticle_Click(object sender, EventArgs e)
        {
            ICSM.IMDialog dialogNewNet = new ICSM.IMDialog();
            dialogNewNet.Add("txtApplId", "", _inputedApplId, CLocaliz.TxT("Appl ID"));
            dialogNewNet.Add("txtArticle", "", _inputedArticle, CLocaliz.TxT("Article"));
            if (dialogNewNet.Enter() == false)
                return;
            string inputApplId = dialogNewNet.Get("txtApplId").ToString();
            _inputedArticle = dialogNewNet.Get("txtArticle").ToString();
            _inputedApplId = inputApplId.ToInt32(ICSM.IM.NullI);
            int retVal = ICSM.IM.NullI;
            string sql = SqlCalcWorksApplAndArticle.Replace("{APPL_ID}", _inputedApplId.ToString());
            sql = sql.Replace("{ARTICLE}", _inputedArticle);
            if (ICSM.IM.ExecuteScalar(ref retVal, sql) == true)
            {
                MessageBox.Show(string.Format("Query '{0}' has returned {1}", sql, retVal));
            }
            else
            {
                MessageBox.Show(string.Format("Query '{0}' has finished with error", sql));
            }
        }


        private void btnSelectionFoto_Click(object sender, EventArgs e)
        {
            string newFolder;
            if (IsSelectedNewFolder(out newFolder))
                FolderSelectionFoto = newFolder;
        }
        /// <summary>
        /// Выбор папки для файлов ЕМС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalculEmc_Click(object sender, EventArgs e)
        {
            string newFolder;
            if (IsSelectedNewFolder(out newFolder))
                FolderCalculatedEmc = newFolder;
        }
        /// <summary>
        /// Выбор папки для файлов Акта ПТК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAktPtk_Click(object sender, EventArgs e)
        {
            string newFolder;
            if (IsSelectedNewFolder(out newFolder))
                FolderAktPtk = newFolder;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                RsaGeographyMapsIni = openFileDialog1.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RsaGeographyRelief = folderBrowserDialog2.SelectedPath;
            }
        }

        private void btnFolderForFrequencyFitting_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FolderFreqFitting = folderBrowserDialog2.SelectedPath;
            }
        }
        /// <summary>
        /// Выбор папки, куда положить документы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDocumentFolders_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexColumn = -2;
            DataGridViewColumn  cell = dgDocumentFolders.Columns[GridCellButtonName];
            if (cell != null)
                indexColumn = cell.Index;

            // Ignore clicks that are not on button cells. 
            if (e.RowIndex < 0 || e.ColumnIndex != indexColumn)
                return;
            if (e.RowIndex >= _documentPath.ListDocPath.Count)
                return;
            DocPath.DocPathType elem = _documentPath.ListDocPath[e.RowIndex];
            if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
            {
                elem.Path = folderBrowserDialog2.SelectedPath;
            }
        }
        /// <summary>
        /// Редактирование пути папки для документов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDocumentFolders_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewColumn  cell = dgDocumentFolders.Columns[GridCellPathName];
            if (cell == null)
                return;
            int indexColumn = cell.Index;

            // Ignore clicks that are not on button cells. 
            if (e.RowIndex < 0 || e.ColumnIndex != indexColumn)
                return;

            DocPath.DocPathType elem = _documentPath.ListDocPath[e.RowIndex];
            if (string.IsNullOrEmpty(elem.Path) == false && System.IO.Directory.Exists(elem.Path) == false)
                MessageBox.Show(string.Format("Папка '{0}' не існує.", elem.Path));
        }
        /// <summary>
        /// Активация вкладки настройки путей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocFolders_Enter(object sender, EventArgs e)
        {
            if (_isFolderInit == false)
            {
                _documentPath.Load();
                InitGridDocumentFolder();
                _isFolderInit = true;
            }
        }

    }
}
