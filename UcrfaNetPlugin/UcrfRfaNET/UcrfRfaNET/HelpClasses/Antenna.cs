﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    public class Antenna
   {
      protected int ID = IM.NullI;
      protected string _tableName = "";
      protected string _antennaName = "";

      public int Id
      {
         get { return ID; }
         set { ID = value; }
      }

      public string TableName
      {
         get { return _tableName; }
         set { _tableName = value; }
      }

      public string Name
      {
         get { return _antennaName; }
         set { _antennaName = value; }
      }

      public virtual void Load()
      {

      }

      public virtual void Save()
      {
      }
   }
}
