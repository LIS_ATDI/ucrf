﻿using System.Collections.Generic;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    /// <summary>
    /// Класс "EriFiles" вытаскивает из eri файла описание кода
    /// </summary>
    public static class ComboFiles
    {
        /// <summary>
        /// Видаоляє всі записи, з відповідним доменом
        /// </summary>
        /// <param name="domain"></param>
        public static void DelAllDomain(string domain)
        {
            IMUserLOV uL = new IMUserLOV(domain);
            uL.ClearAll();
        }
            
        /// <summary>
        /// Додає нові записи у таблицю Комбо, якщо їх не існує
        /// </summary>
        /// <param name="code"></param>
        /// <param name="lang"></param>
        /// <param name="descr"></param>
        /// <param name="domain"></param>
        public static void SetNewCombo(string code, string lang, string descr, string domain)
        {
            bool isEnd = false;
            IMRecordset r = new IMRecordset(ICSMTbl.itblCombo, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("DOMAIN,ITEM,LANG,LEGEN");
                r.SetWhere("DOMAIN", IMRecordset.Operation.Eq, domain);
                r.SetWhere("ITEM", IMRecordset.Operation.Eq, code);
                r.SetWhere("LANG", IMRecordset.Operation.Eq, lang);
                r.Open();
                if (r.IsEOF())                
                    isEnd = true;                
            }
            finally
            {
                r.Final();
            }

            if (isEnd)
            {
                IMUserLOV uL = new IMUserLOV(domain);
                uL.Create(code, lang, descr);
            }
        }

        /// <summary>
        /// Возвращает описание из eri файла по коду
        /// </summary>
        /// <param name="code">кодовое значение</param>
        /// <param name="eriName">название eri файла</param>
        /// <returns>Возвращает описание из eri файла по коду</returns>
        public static string GetEriDescription(string code, string eriName)
        {           
            
            IMRecordset r = new IMRecordset(ICSMTbl.itblCombo, IMRecordset.Mode.ReadOnly);

            string legen = "";
            try
            {
                r.Select("DOMAIN,ITEM,LANG,LEGEN");
                r.SetWhere("DOMAIN", IMRecordset.Operation.Eq, eriName);
                r.SetWhere("ITEM", IMRecordset.Operation.Eq, code);
                r.Open();
                
                if (!r.IsEOF())
                {                    
                    legen = r.GetS("LEGEN");
                }
            }
            finally
            {
               r.Final();
            }
                        
            return legen;
        }
        
        /// <summary>
        /// Возвращает словарь "код-описание" для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>словарь "код-описание"</returns>
        public static Dictionary<string, string> GetEriCodeAndDescr(string eriName)
        {
            Dictionary<string, string> eriDict = new Dictionary<string, string>();
            
            IMRecordset r = new IMRecordset(ICSMTbl.itblCombo, IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("DOMAIN,ITEM,LANG,LEGEN");
                r.SetWhere("DOMAIN", IMRecordset.Operation.Eq, eriName);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    string item = r.GetS("ITEM");
                    //string lang = r.GetS("LANG");
                    string legen = r.GetS("LEGEN");

                    if (eriDict.ContainsKey(item))
                    {
                        eriDict[item] = legen;
                    }
                    else
                    {
                        eriDict.Add(item, legen);
                    }
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }            

            
            return eriDict;
        }

        /// <summary>
        /// Возвращает список кодов для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>список кодов</returns>
        public static List<string> GetEriCodeList(string eriName)
        {
            List<string> eriList = new List<string>();
            IMRecordset r = new IMRecordset(ICSMTbl.itblCombo, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("DOMAIN,ITEM,LANG,LEGEN");
                r.SetWhere("DOMAIN", IMRecordset.Operation.Eq, eriName);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    eriList.Add(r.GetS("ITEM"));
                }                
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
           
            return eriList;                        
        }
        /// <summary>
        /// Возвращает список описаний для заданого ERI-файла
        /// </summary>
        /// <param name="eriName">ERI-файл</param>
        /// <returns>список описаний</returns>
        public static List<string> GetEriDescrList(string eriName)
        {
            List<string> eriList = new List<string>();
            IMRecordset r = new IMRecordset(ICSMTbl.itblCombo, IMRecordset.Mode.ReadOnly);

            try
            {
                r.Select("DOMAIN,ITEM,LANG,LEGEN");
                r.SetWhere("DOMAIN", IMRecordset.Operation.Eq, eriName);

                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    eriList.Add(r.GetS("LEGEN"));
                }
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }                       

            return eriList;                        
        }
    }
}
