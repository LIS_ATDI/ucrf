﻿namespace XICSM.UcrfRfaNET.Map
{
	partial class MapForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{				
				components.Dispose();
				IsInit = false;
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this._mapControl = new LisMapWf.LisMapWfControl();
            this.SuspendLayout();
            // 
            // _mapControl
            // 
            this._mapControl.Cursor = System.Windows.Forms.Cursors.Default;
            this._mapControl.Location = new System.Drawing.Point(0, 0);
            this._mapControl.Name = "_mapControl";
            this._mapControl.Size = new System.Drawing.Size(300, 300);
            this._mapControl.TabIndex = 0;
            // 
            // MapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 377);
            this.Controls.Add(this._mapControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.Name = "MapForm";
            this.ShowInTaskbar = true;
            this.Text = "Карта";
            this.ResumeLayout(false);

		}

		#endregion

        private LisMapWf.LisMapWfControl _mapControl;


    }
}