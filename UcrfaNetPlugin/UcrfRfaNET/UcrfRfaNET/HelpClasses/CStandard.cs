﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UcrfRfaNET
{
   class CStandard
   {
      public static List<string> getListStandard(string RadioTech)
      {
         List<string> list = new List<string>();
         ICSM.IMRecordset newRs = new ICSM.IMRecordset(ICSMTbl.itblFreqPlan, ICSM.IMRecordset.Mode.ReadOnly);
         try
         {
            newRs.Select("NAME");
            newRs.SetWhere("NAME", ICSM.IMRecordset.Operation.Like, /* "*" + */RadioTech + "*");
            for (newRs.Open(); !newRs.IsEOF(); newRs.MoveNext())
            {
               list.Add(newRs.GetS("NAME"));
            }
         }
         finally
         {
            newRs.Destroy();
         }


         //if (RadioTech == CRadioTech.GSM_1800)
         //   list.Add("GSM-1800");
         // else if (RadioTech == CRadioTech.GSM_900)
         //   list.Add("GSM/E-GSM-900");
         //else if (RadioTech == CRadioTech.CDMA_800)
         //{
         //   list.Add("IS-95 (CDMA)");
         //   list.Add("IS-95 (CDMA ITC)");
         //   list.Add("IS-95 (CDMA ТСУ)");
         //   list.Add("IS-95 (CDMA 1.23)");
         //}
         //else if (RadioTech == CRadioTech.UMTS)
         //{
         //   list.Add("IMT-2000 (UMTS)");
         //   list.Add("IMT-2000 (UMTS-TDD)");
         //}
         //else if (RadioTech == CRadioTech.CDMA_450)
         //{
         //   list.Add("IMT-MC-450");
         //   list.Add("NMT-450");
         //}
         //else if (RadioTech == CRadioTech.DECT)
         //   list.Add("DECT");
         //else if (RadioTech == CRadioTech.YKX)
         //{
         //   list.Add("УКХ (410-430)1");
         //   list.Add("УКХ (410-430)");
         //   list.Add("УКХ (146-174 \"4.6\")");
         //   list.Add("УКХ (146-174 \"5.725\")");
         //   list.Add("УКХ (146-174)");
         //   list.Add("УКХ (33-58)");
         //   list.Add("УКХ (450-470)");
         //   list.Add("УКХ (440-450симп)");
         //   list.Add("УКХ (146-174 \"6.95\")");
         //   list.Add("УКХ (146-174 \"15\")");
         //   list.Add("УКХ (146-174) 12.5");
         //   list.Add("УКХ (33-58) 12.5");
         //   list.Add("УКХ (146-174 \"15\") 12.5");
         //   list.Add("УКХ (146-174 \"4.6\") 12.5");
         //   list.Add("УКХ (146-174 \"5.725\") 12.5");
         //   list.Add("УКХ (146-174 \"6.95\") 12.5");                      
         //   list.Add("УКХ (410-430) 12.5");
         //   list.Add("УКХ (440-450симп) 12.5");
         //   list.Add("УКХ (450-470) 12.5");
         //   list.Add("Алтай");
         //   list.Add("Пальма");
         //}
         //else if (RadioTech == CRadioTech.KX)
         //{
         //   list.Add("КХ");
         //}
         //else if (RadioTech == CRadioTech.PD)
         //{
         //   list.Add("ПД");
         //}
         //else if (RadioTech == CRadioTech.TRUNK)
         //{
         //   list.Add("Smar trunk II (146-174)");
         //   list.Add("Smar trunk II (146-174) 12.5");
         //   list.Add("SmarTrunk II (410-430)");
         //   list.Add("SmarTrunk II (410-430) 12.5");
         //   list.Add("SmarTrunk II (450-470)");
         //   list.Add("SmarTrunk II (450-470) 12.5");
         //}
         //else if (RadioTech == CRadioTech.TETRA)
         //{
         //   list.Add("TETRA");
         //}
         //else if (RadioTech == CRadioTech.PADG)
         //{
         //   list.Add("POCSAG");
         //   list.Add("POCSAG 450");
         //}
         //else if (RadioTech == CRadioTech.ROPS)
         //{
         //   list.Add("РОПС");
         //}
         //else if (RadioTech == CRadioTech.RRK)
         //{
         //   ICSM.IMRecordset newRs = new ICSM.IMRecordset(ICSMTbl.itblFreqPlan, ICSM.IMRecordset.Mode.ReadOnly);
         //   try
         //   {
         //      newRs.Select("NAME");
         //      newRs.SetWhere("NAME", ICSM.IMRecordset.Operation.Like, RadioTech+"*");
         //      for (newRs.Open(); !newRs.IsEOF(); newRs.MoveNext())
         //      {
         //         list.Add(newRs.GetS("NAME"));
         //      }
         //   }
         //   finally
         //   {
         //      newRs.Destroy();
         //   }
         //}
         //else if (RadioTech == CRadioTech.SOIPC)
         //{
         //   list.Add("САІРС");
         //}
         //else if (RadioTech == CRadioTech.DAMPS)
         //{
         //   list.Add("IS-136 (D-AMPS)");
         //}
         //else if (RadioTech == CRadioTech.RPATL)
         //{
         //   list.Add("Радіоподовжувачі");
         //}
         
         return list;
      }
   }
}
