﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.HelpClasses.Forms;

namespace XICSM.UcrfRfaNET
{
   internal partial class FFindAllStation : FBaseForm
   {
      //===================================================
      private double _lon = 0;
      private double _lat = 0;
      private int ID = 0;
      public int isRadius { get; set; }
      public int isFreq { get; set; }
      public int radius { get; set; }      //m
      public double minFreq { get; set; }
      public double maxFreq { get; set; }
      public string longitude
      {
         get
         {
            return HelpClasses.HelpFunction.DmsToString(_lon, HelpClasses.EnumCoordLine.Lon);
         }
         set
         {
            double lon = ConvertType.StrToDMS(value);
            if (lon == ICSM.IM.NullD)
               throw new Exception(CLocaliz.TxT("Error longitude"));
            _lon = lon;
         }
      }
      public string latitude
      {
         get
         {
            return HelpClasses.HelpFunction.DmsToString(_lat, HelpClasses.EnumCoordLine.Lat);
         }
         set
         {
            double lat = ConvertType.StrToDMS(value);
            if (lat == ICSM.IM.NullD)
               throw new Exception(CLocaliz.TxT("Error latitude"));
            _lat = lat;
         }
      }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="_id">ID записи</param>
      public FFindAllStation(int _id) : base(_id)
      {
         ID = _id;
         InitializeComponent();
         CLocaliz.TxT(this);
         SetTitle(CLocaliz.TxT("Find stations"));
         {
            //Вытаскиваем координаты
            ICSM.IMRecordset r = new ICSM.IMRecordset(PlugTbl.AllStations, ICSM.IMRecordset.Mode.ReadOnly);
            r.Select("ID,LONGITUDE,LATITUDE");
            r.SetWhere("ID", ICSM.IMRecordset.Operation.Eq, ID);
            try
            {
               r.Open();
               if (!r.IsEOF())
               {
                  _lon = ICSM.IMPosition.Dec2Dms(r.GetD("LONGITUDE"));
                  _lat = ICSM.IMPosition.Dec2Dms(r.GetD("LATITUDE"));
               }
            }
            finally
            {
               r.Close();
               r.Destroy();
            }
         }
         //-----------------------------------
         this.tbRadius.DataBindings.Add("Text", this, "radius", true, DataSourceUpdateMode.OnPropertyChanged);
         this.tbLatitude.DataBindings.Add("Text", this, "latitude", true, DataSourceUpdateMode.OnValidation);
         this.tbLongitude.DataBindings.Add("Text", this, "longitude", true, DataSourceUpdateMode.OnValidation);
         this.tbMinFreq.DataBindings.Add("Text", this, "minFreq", true, DataSourceUpdateMode.OnPropertyChanged);
         this.tbMaxFreq.DataBindings.Add("Text", this, "maxFreq", true, DataSourceUpdateMode.OnPropertyChanged);
         this.cbRadius.DataBindings.Add("Checked", this, "isRadius");
         this.cbFreq.DataBindings.Add("Checked", this, "isFreq");

         UpdateForm();
      }
      //===================================================
      /// <summary>
      /// Ищем станции
      /// </summary>
      private void buttonUpdate_Click(object sender, EventArgs e)
      {
         List<int> listID = FindStation();
         ShowStation(listID);
      }
      //===================================================
      /// <summary>
      /// Обновляем значение позиций
      /// </summary>
      private void Position_Validated(object sender, EventArgs e)
      {
         TextBox tb = (sender as TextBox);
         foreach (Binding b in tb.DataBindings) b.ReadValue();
      }
      //===================================================
      /// <summary>
      /// Возвращает ID записей, которые попали в выборку
      /// </summary>
      /// <returns>ID выбраных записей</returns>
      private List<int> FindStation()
      {
         List<int> retVal = new List<int>();
         double radiusInKm = radius / 1000.0;
         ICSM.IMPosition PosA = ICSM.IMPosition.Convert(new ICSM.IMPosition(_lon, _lat, "4DMS"), "4DEC");
         NSPosition.RecordPos positionRadius = NSPosition.Position.GetRangeCoordinates(PosA.Lon, PosA.Lat, radiusInKm, "", "");

         ICSM.IMRecordset r = new ICSM.IMRecordset(PlugTbl.AllStations, ICSM.IMRecordset.Mode.ReadOnly);
         r.Select("ID,LONGITUDE,LATITUDE");
         if (isRadius > 0)
         {
            r.SetWhere("LONGITUDE", ICSM.IMRecordset.Operation.Ge, positionRadius.X1);
            r.SetWhere("LONGITUDE", ICSM.IMRecordset.Operation.Le, positionRadius.X2);
            r.SetWhere("LATITUDE", ICSM.IMRecordset.Operation.Le, positionRadius.Y1);
            r.SetWhere("LATITUDE", ICSM.IMRecordset.Operation.Ge, positionRadius.Y2);
         }
         if(isFreq > 0)
         {
            string tmpStr = "( (({0} < [TX_LOW_FREQ])AND([TX_LOW_FREQ] < {1}))OR(({0} < [TX_HIGH_FREQ]) AND ([TX_HIGH_FREQ] < {1}))";
            tmpStr +=      "OR (({0} < [RX_LOW_FREQ])AND([RX_LOW_FREQ] < {1}))OR(({0} < [RX_HIGH_FREQ]) AND ([RX_HIGH_FREQ] < {1})) )";
            string sql = string.Format(tmpStr, minFreq, maxFreq);
            r.SetAdditional(sql);
         }
         try
         {
            for(r.Open(); !r.IsEOF(); r.MoveNext())
            {
               bool needAdd = true;
               if(isRadius > 0)
               {
                  needAdd = false;
                  ICSM.IMPosition PosB = new ICSM.IMPosition(r.GetD("LONGITUDE"), r.GetD("LATITUDE"), "4DEC");
                  double curDistance = Math.Abs(ICSM.IMPosition.Distance(PosA, PosB));
                  if (curDistance <= radiusInKm)
                     needAdd = true;
               }

               if(needAdd == true)
                  retVal.Add(r.GetI("ID"));
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Отображаем станции
      /// </summary>
      /// <returns>ID выбраных записей</returns>
      private void ShowStation(List<int> _listID)
      {
          MessageBox.Show(string.Format("Знайдено {0} записів", _listID.Count));
          FPutCountRecord dlg = new FPutCountRecord(1000, "",0,1000000);
          if (dlg.GetStatusPress() == DialogResultMessageBoxButtons.OK)
          {
              if (dlg.GetValueRecordCount()>0)
              {
                  StringBuilder sbTmp = new StringBuilder((int)(dlg.GetValueRecordCount()));
              int countRecord = 0;
              foreach (int id in _listID)
              {
                  sbTmp.AppendFormat("{0},", id);
                  countRecord++;
                  if (countRecord > (int)(dlg.GetValueRecordCount())-1)
                  {
                      MessageBox.Show("Кількість записів скорочено до " + ((int)(dlg.GetValueRecordCount())).ToString());
                      break;
                  }
              }
              if (sbTmp.Length == 0)
                  sbTmp.Append("0,");
              string sql = string.Format("[ID] in ({0})", sbTmp.Remove(sbTmp.Length - 1, 1).ToString());
              ICSM.IMDBList.Show(CLocaliz.TxT("Find result"), PlugTbl.AllStations, sql, "ID", ICSM.OrderDirection.Ascending, 0, 0);
              }
          }
      }
      //===================================================
      /// <summary>
      /// Обновляем компоненты формы
      /// </summary>
      private void UpdateForm()
      {
         bool bRadius = cbRadius.Checked;
         bool bFreq = cbFreq.Checked;

         tbLatitude.Enabled = bRadius;
         tbLongitude.Enabled = bRadius;
         tbRadius.Enabled = bRadius;

         tbMaxFreq.Enabled = bFreq;
         tbMinFreq.Enabled = bFreq;

         buttonUpdate.Enabled = (bFreq || bRadius);
      }
      //===================================================
      /// <summary>
      /// Необзодимо обновить форму
      /// </summary>
      private void cbRadius_CheckedChanged(object sender, EventArgs e)
      {
         UpdateForm();
      }
   }
}
