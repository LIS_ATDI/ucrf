﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   public partial class FSelectSatellite : Form
   {
      int id = 0;

      public FSelectSatellite(string filter)
      {
         InitializeComponent();
         CLocaliz.TxT(this);
         //
         // ID
         DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
         column.Name = "satID";
         column.HeaderText = CLocaliz.TxT("ID");
         column.Width = 50;
         column.Visible = false;
         dataGridViewSatellite.Columns.Add(column);
         // NAME
         column = new DataGridViewTextBoxColumn();
         column.Name = "satNAME";
         column.HeaderText = CLocaliz.TxT("SAT_NAME");
         column.Width = 100;
         dataGridViewSatellite.Columns.Add(column);
         // Orbit
         column = new DataGridViewTextBoxColumn();
         column.Name = "satLONG";
         column.HeaderText = CLocaliz.TxT("LONG_NOM");
         column.Width = 100;
         dataGridViewSatellite.Columns.Add(column);
         // -------------
         tbSatellite.Text = filter;
      }
      //===================================================
      /// <summary>
      /// Изменили текст
      /// </summary>
      private void tbSatellite_TextChanged(object sender, EventArgs e)
      {
         UpdateGrid();
      }
      //===================================================
      /// <summary>
      /// Обновляем зспрос
      /// </summary>
      private void UpdateGrid()
      {
         dataGridViewSatellite.Rows.Clear();
         string filter = tbSatellite.Text;

         IMRecordset r = new IMRecordset(ICSMTbl.itblSatellites, IMRecordset.Mode.ReadOnly);
         r.Select("ID,SAT_NAME,LONG_NOM");
         if(!string.IsNullOrEmpty(filter))
            r.SetWhere("SAT_NAME", IMRecordset.Operation.Like, "*"+filter+"*");
         r.OrderBy("SAT_NAME", OrderDirection.Descending);
         try
         {
            for (r.Open(); !r.IsEOF(); r.MoveNext())
            {
               string[] row = new string[] { r.GetI("ID").ToString(), 
                                             r.GetS("SAT_NAME"),
                                             IM.RoundDeci(r.GetD("LONG_NOM"), 1).ToString()
                                           };
               dataGridViewSatellite.Rows.Add(row);
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }
      }
      //===================================================
      /// <summary>
      /// Вернуть выбраный ID
      /// </summary>
      /// <returns>ID</returns>
      public int GetID()
      {
         return id;
      }
      //===================================================
      /// <summary>
      /// Выбрали запись
      /// </summary>
      private void dataGridViewSatellite_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
      {
         int row = e.RowIndex;
         try
         {
            if (row >= 0)
            {
               id = Convert.ToInt32(dataGridViewSatellite.Rows[row].Cells[0].Value.ToString());
               DialogResult = DialogResult.OK;
            }
         }
         catch (Exception ex)
         {
            id = 0;
            MessageBox.Show(ex.Message + ex.StackTrace, "Error");
         }
      }
   }
}
