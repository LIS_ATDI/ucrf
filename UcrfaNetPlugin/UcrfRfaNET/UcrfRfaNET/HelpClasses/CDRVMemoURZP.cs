﻿using System;
using System.Collections.Generic;
using ICSM;
using XICSM.UcrfRfaNET.ApplSource;
using GridCtrl;
using System.Xml;
using System.Windows.Forms;
using XICSM.UcrfRfaNET.Documents;
using Lis.CommonLib.Extensions;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   
   public class CDRVMemoURZP
   {
      public List<int> appIds = new List<int>();//Список выбранных в пакете заявок
      public List<int> trans_count = new List<int>();//Список передатчиков по каждой заявке
      private int appId = IM.NullI;//Идентификатор заявки
      public int packetId = IM.NullI;//Идентификатор пакета
      private DRVStatus status = DRVStatus.newapl;//Статус заявки
      public ManagemenUDCR memoDepartment;
      public EPacketType packetType = EPacketType.PckUnknown; //Тип пакета
      protected AppType applType = AppType.AppUnknown;

      public List<Work> works = new List<Work>();// Список работ по заявке
      public List<Work> lockedWorks = new List<Work>();// Список заблокированных работ по заявке     
      public List<articlesList> articles = new List<articlesList>(); // списки статей по всем работам

      public int _ownerId = IM.NullI;

       /// <summary>
       /// Чи являются всі статті безплатними
       /// </summary>
       public bool IsFree
       {
           get
           {
               // Рахуємо кількість безплатних статей 
                int freeWorkCount = 0;
                foreach (Work work in works)
                {
                    if (work.article == "7.1б" && work.article == "7.2б")
                        freeWorkCount++;                    
                }
                
                return (freeWorkCount == works.Count) && (works.Count > 0);
           }           
       }

      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="packId">ИД пакета</param>
      /// <param name="ids">список идентификаторов выбранных заявок</param>
      public CDRVMemoURZP(CPacket packet)
      {
         packetId = packet.PacketId;
         foreach (AppPacket.PacketRowBase row in packet.Applications)
         {
             if (row.IsSelected == true)
             {
                 appIds.Add(row.ApplId);
                 trans_count.Add(row.TransmCnt);
             }
         }
         memoDepartment = ManagemenUDCR.URZP;
         bool findNewApp = false;
         string dep = Enum.GetName(typeof(ManagemenUDCR), memoDepartment);
         foreach (int selectedId in appIds)
         {
            IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadOnly);
            rsApp.Select("ID,PACKET_ID,STATE");
            rsApp.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, packetId);
            rsApp.SetWhere("STATE", IMRecordset.Operation.Eq, (int)DRVStatus.saved);
            rsApp.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, dep);
            rsApp.OrderBy("ID", OrderDirection.Descending);
            try
            {
               rsApp.Open();
               bool isSaved = false;
               for (; !rsApp.IsEOF(); rsApp.MoveNext())
               {
                  List<int> idList = new List<int>();
                  int newid = rsApp.GetI("ID");
                  IMRecordset rsAps = new IMRecordset(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadOnly);
                  rsAps.Select("ID,APPLPAY_ID,APPL_ID");
                  rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, newid);
                  try
                  {
                     rsAps.Open();
                     for (; !rsAps.IsEOF(); rsAps.MoveNext())
                        idList.Add(rsAps.GetI("APPL_ID"));
                  }
                  finally
                  {
                     rsAps.Close();
                     rsAps.Destroy();
                  }

                  if (idList.Contains(selectedId))
                  {
                     isSaved = true;
                     if (appId != IM.NullI && appId != newid)
                     {
                        status = DRVStatus.notoneappl;
                        break;
                     }
                     else if (appId == IM.NullI)
                     {
                        appId = newid;
                        status = DRVStatus.saved;
                     }
                  }                                   
               }
               if (!isSaved)
                  findNewApp = true;
            }
            finally
            {
               rsApp.Close();
               rsApp.Destroy();
            }
            if (status == DRVStatus.notoneappl)
               break;
         }
         if (findNewApp && appId != IM.NullI)
            status = DRVStatus.notoneappl;

         //Поиск работ для блокировки
         IMRecordset rsLockedApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadOnly);
         rsLockedApp.Select("ID,PACKET_ID,STATE");
         rsLockedApp.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, packetId);
         rsLockedApp.SetAdditional("(STATE in (0,1,2,3,4,5))");
         rsLockedApp.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, dep);
         try
         {
            for (rsLockedApp.Open(); !rsLockedApp.IsEOF(); rsLockedApp.MoveNext())
            {
               int newid = IM.NullI;

               IMRecordset rsAps = new IMRecordset(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadOnly);
               rsAps.Select("ID,APPLPAY_ID,APPL_ID");
               rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, rsLockedApp.GetI("ID"));
               try
               {
                  rsAps.Open();
                  for (; !rsAps.IsEOF(); rsAps.MoveNext())
                  { 
                     if(appIds.Contains(rsAps.GetI("APPL_ID")))
                     {
                        newid = rsLockedApp.GetI("ID");
                        break;
                     }
                  }
                  if (newid != IM.NullI)
                  {
                     IMRecordset rsWorks = new IMRecordset(PlugTbl.itblXnrfaDrvApplWorks, IMRecordset.Mode.ReadWrite);
                     rsWorks.Select("ID,APPLPAY_ID,APPL_ID,WORKTYPE,ARTICLE,COUNT,NOTES,COEFFICIENT,LINKNUM");
                     rsWorks.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, newid);
                     try
                     {
                        rsWorks.Open();
                        for (; !rsWorks.IsEOF(); rsWorks.MoveNext())
                        {
                           Work w = new Work();
                           w.appId = rsWorks.GetI("APPL_ID");
                           w.type = (WorkType)rsWorks.GetI("WORKTYPE");
                           w.article = rsWorks.GetS("ARTICLE");
                           w.count = rsWorks.GetI("COUNT");
                           w.notes = rsWorks.GetS("NOTES");
                           w.index = rsWorks.GetD("COEFFICIENT");
                           w.linkNum = rsWorks.GetI("LINKNUM");
                           bool contain = false;
                           foreach (Work wr in lockedWorks)
                           {
                              if (wr.type == w.type)
                              {
                                 contain = true;
                                 break;
                              }
                           }
                           if (!contain)
                              lockedWorks.Add(w);
                        }
                     }
                     finally
                     {
                        rsWorks.Close();
                        rsWorks.Destroy();
                     }
                  }
               }
               finally
               {
                  rsAps.Close();
                  rsAps.Destroy();
               }              
            }
         }
         finally
         {
            rsLockedApp.Close();
            rsLockedApp.Destroy();
         }

         if (_ownerId == IM.NullI)
            _ownerId = packet.OwnerId;
         packetType = packet.PacketType;
         applType = packet.ApplType;
         string standard = packet.Standart;
         
         string[] types = Enum.GetNames(typeof(WorkType));
         foreach (string type in types)
         {
            List<string> arts = new List<string>();
            IMRecordset article = new IMRecordset(PlugTbl.itblXnrfaPrice, IMRecordset.Mode.ReadOnly);
            article.Select("ID,ARTICLE,DESCR_WORK,DEPARTMENT,DEFAULT,STANDARD,DEFAULTTYPE,STATUS,DATEOUTACTION,DATESTARTACTION");
            article.SetWhere("DESCR_WORK", IMRecordset.Operation.Like, type);
            article.SetWhere("STATUS", IMRecordset.Operation.Eq, 1);
            article.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
            article.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);

            string department = Enum.GetName(typeof(ManagemenUDCR), memoDepartment);
            article.SetWhere("DEPARTMENT", IMRecordset.Operation.Like, department);
            Dictionary<PriceDefaultType, string> defValues = new Dictionary<PriceDefaultType, string>();
            try
            {
               article.Open();
               for (; !article.IsEOF(); article.MoveNext())
               {
                   string artStd = article.GetS("STANDARD");
                   if (artStd == standard || artStd == "")
                  {
                     arts.Add(article.GetS("ARTICLE"));
                     if (article.GetI("DEFAULT") == 1 && article.GetS("DEFAULTTYPE") != "") 
                     {
                        PriceDefaultType defType = (PriceDefaultType)Enum.Parse(typeof(PriceDefaultType), article.GetS("DEFAULTTYPE"));
                        string art = article.GetS("ARTICLE");
                        if (!defValues.ContainsKey(defType))
                        {                           
                           defValues.Add(defType, art);
                        } else if (defValues.ContainsKey(defType) && artStd == standard)
                            defValues[defType] = art;
                     }
                  }
               }
               arts.Sort();
            }
            finally
            {
               article.Close();
               article.Destroy();
            }
            articlesList newList = new articlesList();
            newList.type = (WorkType)Enum.Parse(typeof(WorkType), type);
            newList.articles = arts;
            newList.defaultValues = defValues;
            articles.Add(newList);
         }
      }

      //===================================================
      /// <summary>
      /// Возвращает список выбранных идентификаторов заявок из пакета
      /// </summary>
      public List<int> GetIds()
      {
         return appIds;
      }

      public string SelectArticle(WorkType wt, PriceDefaultType type)
      {
         string selart = "";
         foreach (articlesList list in articles)
         {
            if (list.type == wt)
            {
             //  form.cbArticles.DataSource = list.articles;
               if (list.defaultValues.ContainsKey(PriceDefaultType.pdtUsual))
                  selart = list.defaultValues[PriceDefaultType.pdtUsual];
               else if (list.defaultValues.ContainsKey(type))
                  selart = list.defaultValues[type];
               else
               {
                  if (list.articles.Count > 0)
                     selart = list.articles[0];
               }
            }
         }
         return selart;
      }

      //===================================================
      /// <summary>
      /// Открытие формы по нажатию на кнопки "Таблица"
      /// </summary>
      /// <param name="workType">тип работы</param>
      /// <param name="grid">указатель на грид</param>
      public void ShowTable(WorkType workType, Grid grid)
      {
         List<Work> workList = new List<Work>();
         foreach (Work wk in works)
         {
            if (wk.type == workType)
               workList.Add(wk);
         }
         if (workType == WorkType.work14)
         {            
            if (workList.Count == 0)
            {
               List<string> addrDict = new List<string>();
               for(int i = 0; i < appIds.Count; ++i)
               {
                  int id = appIds[i];
                  Dictionary<int, PositionState2> fullAddr = BaseAppClass.GetPositions(PlugTbl.itblXnrfaAppl, id);
                  foreach (KeyValuePair<int, PositionState2> note in fullAddr)
                  {
                     int link = note.Key;
                     string addr = note.Value != null ? note.Value.FullAddressAuto : PositionState2.NoPosition;
                     int trCnt = trans_count[i]; 
                     bool cc = BaseAppClass.IsRegionCenter(PlugTbl.itblXnrfaAppl, id, (link == 1));
                     if ((applType == AppType.AppRS && trCnt > 2) || (applType != AppType.AppRS && trCnt > 1))
                     {
                         string art;
                         Work wr;
                         if (addrDict.Contains(addr) == false)
                         {
                             art = SelectArticle(WorkType.work14, cc ? PriceDefaultType.pdtRegCenter : PriceDefaultType.pdtOthrCities);
                             wr = new Work();
                             wr.type = workType;
                             wr.appId = id;
                             wr.notes = addr;
                             wr.linkNum = note.Key;
                             wr.count = 1;
                             wr.article = art;
                             workList.Add(wr);
                         }

                         art = SelectArticle(WorkType.work14, PriceDefaultType.pdtOthrTrans);
                         wr = new Work();
                         wr.type = workType;
                         wr.appId = id;
                         wr.notes = addr;
                         if (applType == AppType.AppRS)
                             wr.count = (int)((trCnt - 2) / 2);
                         else
                             wr.count = (addrDict.Contains(addr)) ? trCnt : trCnt - 1;
                         wr.article = art;
                         wr.linkNum = note.Key;
                         workList.Add(wr);
                     }
                     else
                     {
                        string art;
                        if (addrDict.Contains(addr))
                            art = SelectArticle(WorkType.work14, PriceDefaultType.pdtOthrTrans);
                        else
                            art = SelectArticle(WorkType.work14, cc ? PriceDefaultType.pdtRegCenter : PriceDefaultType.pdtOthrCities);
                        Work wr = new Work();
                        wr.type = workType;
                        wr.appId = id;
                        wr.notes = addr;
                        wr.count = 1;
                        wr.article = art;
                        wr.linkNum = note.Key;
                        workList.Add(wr);
                     }
                     if (addrDict.Contains(addr) == false)
                         addrDict.Add(addr);
                  }
               }
            }
         }
         else if (workType == WorkType.work15)
         {
            if (workList.Count == 0)
            {
               string enteredIndex = grid.GetCell(1, 2).Value;
               double enteredIndexInt = enteredIndex.ToDouble(1);
               List<string> addrDict = new List<string>();
               for (int i = 0; i < appIds.Count; ++i)
               {
                  int id = appIds[i];
                  Dictionary<int, PositionState2> fullAddr = BaseAppClass.GetPositions(PlugTbl.itblXnrfaAppl, id);
                  foreach (KeyValuePair<int, PositionState2> note in fullAddr)
                  {
                     int trCnt = trans_count[i];
                     int link = note.Key;
                     string addr = note.Value != null ? note.Value.FullAddressAuto : PositionState2.NoPosition;
                     bool cc = BaseAppClass.IsRegionCenter(PlugTbl.itblXnrfaAppl, id, (link == 1));
                     if ((applType == AppType.AppRS && trCnt > 2) || (applType != AppType.AppRS && trCnt > 1))
                     {
                         string art;
                         Work wr;
                         if (addrDict.Contains(addr) == false)
                         {
                             art = SelectArticle(WorkType.work15, cc ? PriceDefaultType.pdtRegCenter : PriceDefaultType.pdtOthrCities);
                             wr = new Work();
                             wr.type = workType;
                             wr.appId = id;
                             wr.notes = addr;
                             wr.linkNum = note.Key;
                             wr.count = 1;
                             wr.article = art;
                             wr.index = enteredIndexInt;
                             if ((applType == AppType.AppRS) && (string.IsNullOrEmpty(enteredIndex)))
                                 wr.index = 1;
                             workList.Add(wr);
                         }

                         art = SelectArticle(WorkType.work15, PriceDefaultType.pdtOthrTrans);
                         wr = new Work();
                         wr.type = workType;
                         wr.appId = id;
                         wr.notes = addr;
                         wr.linkNum = note.Key;
                         if (applType == AppType.AppRS)
                             wr.count = (int)((trCnt - 2) / 2);
                         else
                             wr.count = (addrDict.Contains(addr)) ? trCnt : trCnt - 1;
                         wr.article = art;
                         wr.index = enteredIndexInt;
                         workList.Add(wr);
                     }
                     else
                     {
                        Work wr = new Work();
                        wr.type = workType;
                        wr.appId = id;
                        wr.notes = addr;
                        wr.linkNum = note.Key;
                        wr.count = 1;
                        wr.index = enteredIndexInt;
                        if (addrDict.Contains(addr))
                            wr.article = SelectArticle(WorkType.work15, PriceDefaultType.pdtOthrTrans);
                        else
                        {
                            wr.article = SelectArticle(WorkType.work15, cc ? PriceDefaultType.pdtRegCenter : PriceDefaultType.pdtOthrCities);
                            if ((applType == AppType.AppRS) && (string.IsNullOrEmpty(enteredIndex)))
                                wr.index = 1;
                        }
                        workList.Add(wr);
                     }
                     if (addrDict.Contains(addr) == false)
                         addrDict.Add(addr);
                  }
               }
            }
             foreach (Work ws in workList) {
                 if (applType == AppType.AppRS)
                     ws.index=1;
             }
         }
         List<string> selArts = new List<string>();
         foreach (articlesList list in articles)
         {
            if (list.type == workType)
            {
               selArts = list.articles;
               break;
            }
         }
         FWorksEMS fworks = new FWorksEMS(workList, workType, selArts, applType);
         fworks.ShowDialog();
         Work w = null;
         foreach (Work we in workList)
            works.Remove(we);
         for (int i = 0; i < fworks.grWorks.RowCount; ++i)
         {
            w = new Work();
            w.type = workType;
            w.article = fworks.grWorks.Rows[i].Cells[0].Value.ToString();
            w.count = Convert.ToInt32(fworks.grWorks.Rows[i].Cells[1].Value.ToString());
            if (workType == WorkType.work15)
            {
               w.index = fworks.grWorks.Rows[i].Cells[3].Value.ToString() == "" ? 1 : ConvertType.ToDouble(fworks.grWorks.Rows[i].Cells[3].Value.ToString(), 1);//Convert.ToDouble(fworks.grWorks.Rows[i].Cells[3].Value.ToString());
               w.appId = Convert.ToInt32(fworks.grWorks.Rows[i].Cells[4].Value.ToString());
            }
            else
               w.appId = Convert.ToInt32(fworks.grWorks.Rows[i].Cells[3].Value.ToString());
            w.notes = fworks.grWorks.Rows[i].Cells[2].Value.ToString();            
            works.Add(w);
         }
         fworks.Dispose();
      }

      //===================================================
      /// <summary>
      /// Открытие формы заявки
      /// </summary>
      public void ShowForm()
      {
         if (status == DRVStatus.notoneappl)
            MessageBox.Show("Обрані заявки зберігаються у різних службових записках до ДРВ!", "Неможливо відкрити потрібну службову записку", MessageBoxButtons.OK, MessageBoxIcon.Error);
         else
         {
            FBillMemoURZP fMemo = new FBillMemoURZP(this, status, lockedWorks);
            if (fMemo.ShowDialog() == System.Windows.Forms.DialogResult.Yes)
            {

            }
            fMemo.Dispose();
         }
      }

      //===================================================
      /// <summary>
      /// Отправление заявки в ДРВ
      /// </summary>
      public bool AppToDRV()
      {
         if (false)//status != DRVStatus.saved)
         {
            MessageBox.Show("Спочатку треба зберегти заявку!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
         }
         else
         {            
            string fileName = "";
            List<string> errors = new List<string>();
            XmlDocument doc = GlobalDB.CGlobalXML.CreateAppPayXML(appId, ref fileName, ref errors, IsFree);
            if (errors.Count > 0)
            {
               IMLogFile log = new IMLogFile();
               log.Create("XML_log");
               foreach (string str in errors)
                  log.Warning(str + "\n");
               log.Display("Error");
            }
            else
            {
               GlobalDB.CGlobalDB gdb = new XICSM.UcrfRfaNET.GlobalDB.CGlobalDB();
               gdb.OpenConnection();
               gdb.WriteXMLToDB(doc.OuterXml, EEssence.APPL_PAY_ICSM);
               gdb.CloseConnection();
               IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
               rsApp.Select("ID,PACKET_ID,STATE");
               rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsApp.Open();
                  if (!rsApp.IsEOF())
                  {
                     rsApp.Edit();
                     rsApp["STATE"] = (int)DRVStatus.sent;
                     rsApp.Update();
                  }
               }
               finally
               {
                  rsApp.Close();
                  rsApp.Destroy();
               }
               foreach (int curApplID in appIds)
                  CEventLog.AddApplEvent(curApplID, EDocEvent.evSendToDRV, DateTime.Now, "", IM.NullT, "");
               MessageBox.Show("Файл сохранен под именем " + fileName);
            }
            return true;
         }
      }

      //===================================================
      /// <summary>
      /// Удаление заявки
      /// </summary>
      /// <returns>Возвращает удалена заявка или нет</returns>
      public bool DeleteApp()
      {
         if (status == DRVStatus.saved && MessageBox.Show("Видалити дану службову записку?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
         {
            IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
            rsApp.Select("ID,PACKET_ID,STATE");
            rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
               rsApp.Open();
               if (!rsApp.IsEOF())
               {
                  rsApp.Edit();
                  rsApp["STATE"] = (int)DRVStatus.deleted;
                  rsApp.Update();
               }
            }
            finally
            {
               rsApp.Close();
               rsApp.Destroy();
            }
            appId = IM.NullI;
            status = DRVStatus.newapl;
            return true;
         }
         return false;
      }

      //===================================================
      /// <summary>
      /// Аннулирование заявки
      /// </summary>
      /// <returns>Возвращает аннуулирована заявка или нет</returns>
      public bool CancelApp()
      {
         FAskApplicationCancel fask = new FAskApplicationCancel();
         if (fask.ShowDialog() == System.Windows.Forms.DialogResult.Yes)
         {
            FCancelApp fcancel = new FCancelApp();
            fcancel.dtDate.Value = DateTime.Now;
            if (fcancel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
               IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
               rsApp.Select("ID,PACKET_ID,STATE");
               rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsApp.Open();
                  if (!rsApp.IsEOF())
                  {
                     rsApp.Edit();
                     rsApp["STATE"] = (int)DRVStatus.cancelled;
                     rsApp.Update();
                  }
               }
               finally
               {
                  rsApp.Close();
                  rsApp.Destroy();
               }
               return true;
            }
         }
         return false;
      }

      //===================================================
      /// <summary>
      /// Сохранение заявки
      /// </summary>
      public void SaveApp()
      {
         if (works.Count != 0)
         {
            if (appId == IM.NullI)
            {
               appId = IM.AllocID(PlugTbl.itblXnrfaDrvAppl, 1, -1);
            }
            IMTransaction.Begin();
            try
            {
               IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
               rsApp.Select("ID,PACKET_ID,STATE,DEPARTMENT,OWNER_ID");
               rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsApp.Open();
                  if (rsApp.IsEOF())
                  {
                     rsApp.AddNew();
                     rsApp["ID"] = appId;
                     rsApp["PACKET_ID"] = packetId;
                     rsApp["STATE"] = (int)DRVStatus.saved;
                     string dep = Enum.GetName(typeof(ManagemenUDCR), memoDepartment);
                     rsApp["DEPARTMENT"] = dep;
                     rsApp["OWNER_ID"] = _ownerId;
                     rsApp.Update();
                  }
                  else
                  {
                     rsApp.Edit();
                     rsApp["ID"] = appId;
                     rsApp["PACKET_ID"] = packetId;
                     rsApp["STATE"] = (int)DRVStatus.saved;
                     string dep = Enum.GetName(typeof(ManagemenUDCR), memoDepartment);
                     rsApp["DEPARTMENT"] = dep;
                     rsApp["OWNER_ID"] = _ownerId;
                     rsApp.Update();
                  }
               }
               catch (Exception e)
               {
                  IMTransaction.Rollback();
                  MessageBox.Show("Неможливо зберегти заяву:" + e.Message, "Помилка збереження", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                  rsApp.Close();
                  rsApp.Destroy();
                  return;
               }
               finally
               {
                  rsApp.Close();
                  rsApp.Destroy();
               }

               IMRecordset rsAps = new IMRecordset(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadWrite);
               rsAps.Select("ID,APPLPAY_ID,APPL_ID");
               rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsAps.Open();
                  for (; !rsAps.IsEOF(); rsAps.MoveNext())
                     rsAps.Delete();
                  foreach (int id in appIds)
                  {
                     int newId = IM.AllocID(PlugTbl.itblXnrfaDrvApplApps, 1, -1);
                     rsAps.AddNew();
                     rsAps["ID"] = newId;
                     rsAps["APPLPAY_ID"] = appId;
                     rsAps["APPL_ID"] = id;
                     rsAps.Update();
                  }
               }
               catch (Exception e)
               {
                  IMTransaction.Rollback();
                  MessageBox.Show("Неможливо зберегти заяву:" + e.Message, "Помилка збереження", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                  rsAps.Close();
                  rsAps.Destroy();
                  return;
               }
               finally
               {
                  rsAps.Close();
                  rsAps.Destroy();
               }

               IMRecordset rsWorks = new IMRecordset(PlugTbl.itblXnrfaDrvApplWorks, IMRecordset.Mode.ReadWrite);
               rsWorks.Select("ID,APPLPAY_ID,APPL_ID,WORKTYPE,ARTICLE,COUNT,NOTES,COEFFICIENT,LINKNUM");
               rsWorks.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsWorks.Open();
                  for (; !rsWorks.IsEOF(); rsWorks.MoveNext())
                     rsWorks.Delete();
                  foreach (Work w in works)
                  {
                     int newId = IM.AllocID(PlugTbl.itblXnrfaDrvApplWorks, 1, -1);
                     rsWorks.AddNew();
                     rsWorks["ID"] = newId;
                     rsWorks["APPLPAY_ID"] = appId;
                     rsWorks["APPL_ID"] = w.appId;
                     rsWorks["WORKTYPE"] = (int)w.type;
                     rsWorks["ARTICLE"] = w.article;
                     rsWorks["COUNT"] = w.count;
                     rsWorks["NOTES"] = w.notes;
                     rsWorks["COEFFICIENT"] = w.index;
                     rsWorks["LINKNUM"] = w.linkNum;
                     rsWorks.Update();
                  }
               }
               catch (Exception e)
               {
                  IMTransaction.Rollback();
                  MessageBox.Show("Неможливо зберегти заяву:" + e.Message, "Помилка збереження", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                  rsWorks.Close();
                  rsWorks.Destroy();
                  return;
               }
               finally
               {
                  rsWorks.Close();
                  rsWorks.Destroy();
               }
               status = DRVStatus.saved;
               IMTransaction.Commit();
            }
            catch (Exception)
            {
               IMTransaction.Rollback();
            }
         }
         else
            MessageBox.Show("Неможливо зберегти заяву без робіт!", "Помилка збереження", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }

      //===================================================
      /// <summary>
      /// Загрузка заявки
      /// </summary>
      public void LoadApp()
      {
         if (appId != IM.NullI)
         {  
            IMRecordset rsApp = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadOnly);
            rsApp.Select("ID,PACKET_ID,STATE,OWNER_ID");
            rsApp.SetWhere("ID", IMRecordset.Operation.Eq, appId);
            try
            {
               rsApp.Open();
               if (rsApp.IsEOF() == false)
               {
                  appId = rsApp.GetI("ID");
                  packetId = rsApp.GetI("PACKET_ID");
                  status = (DRVStatus)rsApp.GetI("STATE");
                  if (rsApp.GetI("OWNER_ID") != IM.NullI)
                     _ownerId = rsApp.GetI("OWNER_ID"); 
               }
               //rsApp["STATE"] = 0;
            }
            finally
            {
               rsApp.Close();
               rsApp.Destroy();
            }

            works.Clear();
            appIds.Clear();

            if (appId != IM.NullI)
            {

               IMRecordset rsAps = new IMRecordset(PlugTbl.itblXnrfaDrvApplApps, IMRecordset.Mode.ReadWrite);
               rsAps.Select("ID,APPLPAY_ID,APPL_ID");
               rsAps.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsAps.Open();
                  for (; !rsAps.IsEOF(); rsAps.MoveNext())
                     appIds.Add(rsAps.GetI("APPL_ID"));
               }
               finally
               {
                  rsAps.Close();
                  rsAps.Destroy();
               }

               IMRecordset rsWorks = new IMRecordset(PlugTbl.itblXnrfaDrvApplWorks, IMRecordset.Mode.ReadWrite);
               rsWorks.Select("ID,APPLPAY_ID,APPL_ID,WORKTYPE,ARTICLE,COUNT,NOTES,COEFFICIENT,LINKNUM");
               rsWorks.SetWhere("APPLPAY_ID", IMRecordset.Operation.Eq, appId);
               try
               {
                  rsWorks.Open();
                  for (; !rsWorks.IsEOF(); rsWorks.MoveNext())
                  {
                     Work w = new Work();
                     w.appId = rsWorks.GetI("APPL_ID");
                     w.type = (WorkType)rsWorks.GetI("WORKTYPE");
                     w.article = rsWorks.GetS("ARTICLE");
                     w.count = rsWorks.GetI("COUNT");
                     w.notes = rsWorks.GetS("NOTES");
                     w.index = rsWorks.GetD("COEFFICIENT");
                     w.linkNum = rsWorks.GetI("LINKNUM");
                     works.Add(w);
                  }
               }
               finally
               {
                  rsWorks.Close();
                  rsWorks.Destroy();
               }
            }
         }
      }

      private string GetDocNumber(DepartmentType department, string number) {
          switch(department) {
              case DepartmentType.FILIA:
                  return PrintDocs.GetBranchDocumentName(number);
              default:
                  return string.Format("{0}-{1}-{2}-09-{3}", memoDepartment.ToString("D"),
                     ConvertType.DepartmetToCode(department), HelpFunction.getUserNumber(), number);
          }
      }
      //===================================================
      /// <summary>
      /// Печатаем документ
      /// </summary>
      /// <returns></returns>
      public bool PrintDoc()
      {
         bool retVal = false;
         string docType = DocType.DRV;
         // Вытаскиваем IRP файл
         string filter = "APPLURZPDRV";
         IcsmReport[] masRep = IM.ReportsGetList(PlugTbl.itblXnrfaDrvAppl, "*" + filter + "*");
         if (masRep.Length == 0)
         {
            MessageBox.Show("Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         string IRPfile = System.IO.Path.GetFileName(masRep[0].Path);

         IM.AdminDisconnect();

         if ((IRPfile == null) || (IRPfile == ""))
         {
            MessageBox.Show("!!!! Can't find IRP file by filter \"*" + filter + "*\"");  //Нет IRP файлов
            return false;
         }
         // Формируем имя файла
         DepartmentType curDepart = CUsers.GetUserDepartmentType(); 
         string number = PrintDocs.getListDocNumber(docType, 1, curDepart)[0];
         string docNumber = GetDocNumber(curDepart, number);
         string fullPath = PrintDocs.getPath(docType);
         if (fullPath == "")
            fullPath = ".";
         fullPath += string.Format("\\{0}.rtf", docNumber);
         string shortName = fullPath.ToFileNameWithoutExtension();
         {
            IMRecordset rs = new IMRecordset(PlugTbl.itblXnrfaDrvAppl, IMRecordset.Mode.ReadWrite);
            try
            {
               rs.Select("ID,CUST_TXT1,DOC_DATE");
               rs.SetWhere("ID", IMRecordset.Operation.Eq, appId);
               rs.Open();
               if (!rs.IsEOF())
               {
                  rs.Edit();
                  rs.Put("CUST_TXT1", shortName);
                  rs.Put("DOC_DATE", DateTime.Now); 
                  rs.Update();
               }
            }
            finally
            {
               rs.Destroy();
            }
         }
         // Печатаем документ
         string lang = "RUS";
         RecordPtr rec = new RecordPtr(PlugTbl.itblXnrfaDrvAppl, appId);
         rec.PrintRTFReport(IRPfile, lang, fullPath, "", false);

         if (System.IO.File.Exists(fullPath))
         {// Файд напечатан
            PrintDocs pr = new PrintDocs();
            foreach (int curApplID in appIds)
            {
               DisableEvent(curApplID);
            }
            pr.LoadDocLink(DocType.DRV, "", fullPath, DateTime.Now, IM.NullT, appIds,true);
           /* foreach (int curApplID in appIds)
            {
               DisableEvent(curApplID);
               CEventLog.AddApplEvent(curApplID, EDocEvent.evDRV, DateTime.Now, shortName, DateTime.Now, fullPath);
            }*/
            retVal = true;
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Деактивируем ненужные записи
      /// </summary>
      /// <param name="_applID">ID заявки</param>
      /// <param name="_eventType">Тип документа</param>
      private void DisableEvent(int _applID)
      {
         DateTime dt = DateTime.MinValue;
         {
            IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadOnly);
            rsEvent.Select("CREATED_DATE");
            rsEvent.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, _applID);
            rsEvent.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.itblXnrfaAppl);
            rsEvent.SetWhere("EVENT", IMRecordset.Operation.Like, (string)Enum.Format(typeof(EDocEvent), EDocEvent.evSendToDRV, "F"));
            rsEvent.OrderBy("CREATED_DATE", OrderDirection.Descending);
            try
            {
               rsEvent.Open();
               if(!rsEvent.IsEOF())
                  dt = rsEvent.GetT("CREATED_DATE");
            }
            finally
            {
               rsEvent.Destroy();
            }
         }
         //-------
         // Деактивируем ненужные записи
         {
            IMRecordset rsEvent = new IMRecordset(PlugTbl.itblXnrfaEventLog, IMRecordset.Mode.ReadWrite);
            rsEvent.Select("ID,VISIBLE,CREATED_DATE");
            rsEvent.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, _applID);
            rsEvent.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, PlugTbl.itblXnrfaAppl);
            rsEvent.SetWhere("CREATED_DATE", IMRecordset.Operation.Gt, dt);
            rsEvent.SetWhere("EVENT", IMRecordset.Operation.Like, (string)Enum.Format(typeof(EDocEvent), EDocEvent.evDrvUrzp, "F"));
            rsEvent.SetWhere("VISIBLE", IMRecordset.Operation.Eq, 1);
            try
            {
               for (rsEvent.Open(); !rsEvent.IsEOF(); rsEvent.MoveNext())
               {
                  if (rsEvent.GetT("CREATED_DATE") > dt)
                  {
                     rsEvent.Edit();
                     rsEvent.Put("VISIBLE", 0);
                     rsEvent.Update();
                  }
               }
            }
            finally
            {
               rsEvent.Destroy();
            }
         }
      }
      //===================================================
      /// <summary>
      /// Возвращает имя котрагента
      /// </summary>
      public string OwnerName
      {
         get
         {
            string name = "";
            IMRecordset r = new IMRecordset(ICSMTbl.itblUsers, IMRecordset.Mode.ReadOnly);
            r.Select("ID,NAME");
            r.SetWhere("ID", IMRecordset.Operation.Eq, _ownerId);
            try
            {
               r.Open();
               if (!r.IsEOF())
                  name = r.GetS("NAME");
            }
            finally
            {
               r.Close();
               r.Destroy();
            }
            return name;
         }
      }
      //===================================================
      /// <summary>
      /// Выбираем контрагента
      /// </summary>
      /// <param name="filter">фильтер</param>
      /// <returns>выбрали или нет</returns>
      public bool SelectOwner(string filter)
      {
         string param = "{NAME=\"*" + HelpFunction.ReplaceQuotaSumbols(filter) + "*\"}";
         RecordPtr user = RecordPtr.UserSearch(CLocaliz.TxT("Seaching of an owner"), ICSMTbl.itblUsers, param);
         if (user.Id != IM.NullI)
         {
            _ownerId = user.Id;
            return true;
         }
         return false;
      }
   }
}
