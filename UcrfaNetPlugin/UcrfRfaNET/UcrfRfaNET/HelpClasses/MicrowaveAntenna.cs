﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
   class MicrowaveAntenna : Antenna, ICloneable
   {
      public double Diameter { get; set; }
      public double Gain { get; set; }

      public override void Load()
      {
         //ICSMTbl.itblMicrowAntenna
         IMRecordset r = new IMRecordset(TableName , IMRecordset.Mode.ReadOnly);

         if (Id==IM.NullI)
         {
            throw new Exception("Microwave antenna ID is nit initialized");
         }

         try
         {
            r.Select("ID,NAME,DIAMETER,GAIN");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();

            if (!r.IsEOF())
            {
               //if (r.GetS("ROLE") == roleValue)               
               Name = r.GetS("NAME");
               Diameter = r.GetD("DIAMETER");
               Gain = r.GetD("GAIN");               
            }
         }
         finally
         {
            r.Close();
            r.Destroy();
         }         
      }

      public override void Save()
      {
      }

      public object Clone()
      {
         MicrowaveAntenna clone = new MicrowaveAntenna();

         clone.Name = Name;
         clone.Diameter = Diameter;
         clone.Id = Id;
         clone.TableName = TableName;
         return clone;
      }
   }
}
