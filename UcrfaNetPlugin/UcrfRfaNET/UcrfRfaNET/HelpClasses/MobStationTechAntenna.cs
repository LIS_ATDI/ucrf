﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET.HelpClasses
{
    class MobStationTechAntenna: Antenna, ICloneable
    {        
        //public int Id { get; set; }
        public double AntennaWidth { get; set; }
        public double Gain { get; set; }

        #region Implementation of ICloneable
        public object Clone()
        {
            MobStationAntenna clone = new MobStationAntenna();

            clone.Name = Name;
            clone.AntennaWidth = AntennaWidth;
            clone.Id = Id;
            clone.TableName = TableName;
            return clone;
        }
        #endregion

        public override void Load()
        {
            //ICSMTbl.itblMicrowAntenna
            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);

            /*if (Id == IM.NullI)
            {
                throw new Exception("MobStation antenna ID is nit initialized");
            }*/

            try
            {
                r.Select("ID,NAME,H_BEAMWIDTH,GAIN");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();

                if (!r.IsEOF())
                {
                    //if (r.GetS("ROLE") == roleValue)               
                    Name = r.GetS("NAME");
                    AntennaWidth = r.GetD("H_BEAMWIDTH");
                    Gain = r.GetD("GAIN");
                }
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
        }        
    }
}
