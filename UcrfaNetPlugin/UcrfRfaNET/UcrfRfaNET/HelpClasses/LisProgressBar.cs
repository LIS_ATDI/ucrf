﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   internal class LisProgressBar : IDisposable
   {
      private class structSaveData
      {
         public string BigText { get; set; }
         public string SmallText { get; set; }
         public int done { get; set; }
         public int total { get; set; }
         //--------------------------------
         public structSaveData()
         {
            BigText = "";
            SmallText = "";
            done = 0;
            total = 0;
         }
      }
      private static Stack<structSaveData> stackData = null;
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="caption">A caption of a progress form</param>
      public LisProgressBar(string caption)
      {
         if (stackData == null)
         {
            IMProgress.Create(caption);
            stackData = new Stack<structSaveData>();
         }
         stackData.Push(new structSaveData());
         //-------
         //Set up data by default
         //structSaveData tmp = stackData.Peek();
         //IMProgress.ShowBig(tmp.BigText);
         //IMProgress.ShowSmall(tmp.SmallText);
         //IMProgress.ShowProgress(tmp.done, tmp.total);
      }
      //===================================================
      /// <summary>
      /// Destroys the progress form 
      /// </summary>
      public void Dispose()
      {
         stackData.Pop();

         if (stackData.Count == 0)
         {
            IMProgress.Destroy();
            stackData = null;
         }
         else
         {
            structSaveData tmp = stackData.Peek();
            IMProgress.ShowBig(tmp.BigText);
            IMProgress.ShowSmall(tmp.SmallText);
            IMProgress.ShowProgress(tmp.done, tmp.total);
         }
      }
      //===================================================
      /// <summary>
      /// Sets a text in a big label
      /// </summary>
      /// <param name="text">A text</param>
      public void SetBig(string text)
      {
         stackData.Peek().BigText = text;
         IMProgress.ShowBig(text);
      }
      //===================================================
      /// <summary>
      /// Sets a text in the small label
      /// </summary>
      /// <param name="text">A text</param>
      public void SetSmall(string text)
      {
         stackData.Peek().SmallText = text;
         IMProgress.ShowSmall(text);
      }
      //===================================================
      /// <summary>
      /// Sets a number in the small label
      /// </summary>
      /// <param name="number">A number</param>
      public void SetSmall(int number)
      {
         stackData.Peek().SmallText = number.ToString();
         IMProgress.ShowSmall(number);
      }
      //===================================================
      /// <summary>
      /// Sets numbers in the small label
      /// </summary>
      /// <param name="number1">A number 1</param>
      /// <param name="number2">A number 2</param>
      public void SetSmall(int number1, int number2)
      {
         stackData.Peek().SmallText = number1.ToString() + " / " + number2.ToString();
         IMProgress.ShowSmall(number1, number2);
      }
      //===================================================
      /// <summary>
      /// Sets a progress bar
      /// </summary>
      /// <param name="done"></param>
      /// <param name="total"></param>
      public void SetProgress(int done, int total)
      {
         stackData.Peek().done = done;
         stackData.Peek().total = total;
         IMProgress.ShowProgress(done, total);
      }
      //===================================================
      /// <summary>
      /// Increment a progress bar
      /// </summary>
      /// <param name="showInSmal">Is show in the small window</param>
      public void Increment(bool showInSmal)
      {
          stackData.Peek().done = stackData.Peek().done + 1;
          IMProgress.ShowProgress(stackData.Peek().done, stackData.Peek().total);
          if (showInSmal)
              SetSmall(stackData.Peek().done, stackData.Peek().total);
      }
      //===================================================
      /// <summary>
      /// Was canceled by user
      /// </summary>
      /// <returns>TRUE if a process was canceled by user else FALSE</returns>
      public bool UserCanceled()
      {
         return IMProgress.UserCanceled();
      }
   }
}
