﻿namespace XICSM.UcrfRfaNET
{
   partial class FSelectSatellite
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.dataGridViewSatellite = new System.Windows.Forms.DataGridView();
         this.tbSatellite = new System.Windows.Forms.TextBox();
         ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSatellite)).BeginInit();
         this.SuspendLayout();
         // 
         // dataGridViewSatellite
         // 
         this.dataGridViewSatellite.AllowUserToAddRows = false;
         this.dataGridViewSatellite.AllowUserToDeleteRows = false;
         this.dataGridViewSatellite.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
         this.dataGridViewSatellite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dataGridViewSatellite.Location = new System.Drawing.Point(12, 38);
         this.dataGridViewSatellite.MultiSelect = false;
         this.dataGridViewSatellite.Name = "dataGridViewSatellite";
         this.dataGridViewSatellite.ReadOnly = true;
         this.dataGridViewSatellite.RowHeadersWidth = 4;
         this.dataGridViewSatellite.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dataGridViewSatellite.Size = new System.Drawing.Size(359, 225);
         this.dataGridViewSatellite.TabIndex = 0;
         this.dataGridViewSatellite.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSatellite_CellDoubleClick);
         // 
         // tbSatellite
         // 
         this.tbSatellite.Location = new System.Drawing.Point(12, 12);
         this.tbSatellite.Name = "tbSatellite";
         this.tbSatellite.Size = new System.Drawing.Size(359, 20);
         this.tbSatellite.TabIndex = 1;
         this.tbSatellite.TextChanged += new System.EventHandler(this.tbSatellite_TextChanged);
         // 
         // FSelectSatellite
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(383, 275);
         this.Controls.Add(this.tbSatellite);
         this.Controls.Add(this.dataGridViewSatellite);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "FSelectSatellite";
         this.ShowInTaskbar = false;
         this.Text = "FSelectSatellite";
         ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSatellite)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.DataGridView dataGridViewSatellite;
      private System.Windows.Forms.TextBox tbSatellite;
   }
}