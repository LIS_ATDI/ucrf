﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UcrfRfaNET
{
   //======================================================
   /// <summary>
   /// Базовая форма для всех форм
   /// </summary>
   public partial class FBaseFormClear : Form, INotifyPropertyChanged
   {
        private const string KeyOfHeight = "Height";
        private const string KeyOfWidth = "Width";
        private const string KeyOfLeft = "Left";
        private const string KeyOfTop = "Top";
        private const string KeyOfWindowState = "FormWindowState";
        protected const string FieldIsChanged = "IsChanged";
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get { return _isChanged;}
            set
            {
                if(_isChanged != value)
                {
                    _isChanged = value;
                    InvokeNotifyPropertyChanged(FieldIsChanged);
                }
            }
        }

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != FieldIsChanged)
                    IsChanged = true;
            }
        }
        #endregion
   
      private int _localId = 0;
      public int FormId { get { return _localId; } set { _localId = value; } }
      protected bool CanEscClose { get; set; }
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="id">ID of the record</param>
      public FBaseFormClear(int id)
      {
         InitializeComponent();
         _localId = id;
         CanEscClose = true;
      }
      //===================================================
      /// <summary>
      /// Base constructor
      /// </summary>
      public FBaseFormClear() : this(-1) { }
      /// <summary>
      /// Формирует название ключа
      /// </summary>
      /// <param name="key">Под ключ</param>
      /// <returns>Ключ поля сонфигурации</returns>
      private string GetKeyConfig(string key)
      {
          return string.Format("{0}_{1}", this.GetType().FullName, key);
      }
      /// <summary>
      /// Загрузка формы
      /// </summary>
      /// <param name="e"></param>
      protected override void OnLoad(EventArgs e)
      {
          LoadFormPosition();
          base.OnLoad(e);
      }
       /// <summary>
       /// Загрузка позиции формы
       /// </summary>
       private void LoadFormPosition()
       {
           string key;
           LocalSetting.Config conf = new LocalSetting.Config();
           key = GetKeyConfig(KeyOfWindowState);
           if (conf.IsExist(key))
           {
               try
               {
                   this.WindowState = (FormWindowState) Enum.Parse(typeof (FormWindowState), conf.Get(key));
               }
               catch
               {
                   this.WindowState = FormWindowState.Normal;
               }
           }
           key = GetKeyConfig(KeyOfLeft);
           if (conf.IsExist(key))
           {
               this.Left = (int) conf.Get(key, 0);
               if (this.Left < 0)
                   this.Left = 0;
           }
           key = GetKeyConfig(KeyOfTop);
           if (conf.IsExist(key))
           {
               this.Top = (int) conf.Get(key, 0);
               if (this.Top < 0)
                   this.Top = 0;
           }
           key = GetKeyConfig(KeyOfHeight);
           if (conf.IsExist(key))
           {
               this.Height = (int)conf.Get(key, 400);
           }
           key = GetKeyConfig(KeyOfWidth);
           if (conf.IsExist(key))
           {
               this.Width = (int)conf.Get(key, 650);
           }
       }
       /// <summary>
       /// Сохранить позицию окна
       /// </summary>
       private void SaveFormPosition()
       {
           LocalSetting.Config conf = new LocalSetting.Config();
           conf.Set(GetKeyConfig(KeyOfWindowState), this.WindowState.ToString());
           if (this.WindowState == FormWindowState.Normal)
           {
               conf.Set(GetKeyConfig(KeyOfLeft), this.Left);
               conf.Set(GetKeyConfig(KeyOfTop), this.Top);
               conf.Set(GetKeyConfig(KeyOfHeight), this.Height);
               conf.Set(GetKeyConfig(KeyOfWidth), this.Width);
           }
           conf.Save();
       }


      protected override void OnKeyDown(KeyEventArgs e)
      {
          base.OnKeyDown(e);
          if (CanEscClose && (e.KeyCode == Keys.Escape))
          {// Выход
              Close();
          }
      }

      protected override void OnClosing(CancelEventArgs e)
      {
          e.Cancel = !ConfirmClose();
          base.OnClosing(e);
      }

      protected override void OnClosed(EventArgs e)
      {
          base.OnClosed(e);
          SaveFormPosition();
      }
      /// <summary>
      /// Подтвердить закрытие формы
      /// </summary>
      /// <returns>TRUE - Можна закрыть форму</returns>
      protected virtual bool ConfirmClose()
      {
          return true;
      }
      //===================================================
      /// <summary>
      /// Sets the title of the form
      /// </summary>
      /// <param name="title"></param>
      protected void SetTitle(string title)
      {
         string ttl = title;
         if ((_localId > 0) && (_localId != IM.NullI))
            ttl += string.Format(" ID:{0}", _localId);
         this.Text = ttl;
      }
   }
}
