﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using XICSM.NSDoc;
using XICSM.UcrfRfaNET.Documents;
using XICSM.UcrfRfaNET.HelpClasses;
using Lis.CommonLib.Extensions;
using XICSM.UcrfRfaNET.ApplSource;
using XICSM.UcrfRfaNET.HelpClasses.Classes;
using OrmCs;
using System.IO;
using System.Windows.Forms;

namespace XICSM.UcrfRfaNET
{
    /// <summary>
    /// Позволяет создавать документы по частотным присвоениям
    /// </summary>
    internal class PrintDocs
    {
        public static string Language = "UKR";   //Язык по умолчанию
        public int NewIdLinkDoc { get; set; } // Номер ID привязанного дока
        //===================================================
        /// <summary>
        /// Возвращает путь, куда необходимо положить документ
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="depType">Тип отдела</param>
        /// <returns>Путь, куда необходимо поместить документ</returns>
        public static string getPath(string docType)
        {
            string depType = "";
            string areaSubPath = "";
            string area = "";
            IMRecordset rs = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly);
            try
            {
                rs.Select("ID,APP_USER,PROVINCE,TYPE");
                rs.SetWhere("APP_USER", IMRecordset.Operation.Eq, CUsers.ConnectedUser());
                rs.Open();
                if (!rs.IsEOF())
                {
                    area = rs.GetS("PROVINCE").Trim();
                    depType = rs.GetS("TYPE").Trim();
                }
            }
            finally
            {
                rs.Destroy();
            }

            if (PluginSetting.PluginFolderSetting.UseRegionFieldAsBranchAssign && area != "") // 'FILIA', or 'Branch' in English
            {
                depType = PluginSetting.PluginFolderSetting.BranchUserTypeCode;
                if (PluginSetting.PluginFolderSetting.UseBranchSubfolders)
                {
                    if (PluginSetting.PluginFolderSetting.UseBranchCodesAsSubfolders)
                        IM.ExecuteScalar(ref areaSubPath, "select CODE from %AREA where NAME = " + area);
                    else
                        areaSubPath = area;
                }
            }
            
            string retVal = "";
            string itemName = "XRFA_" + depType + "_" + docType.ToString();
            IMRecordset r = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
            r.Select("ITEM,WHAT");
            r.SetWhere("ITEM", IMRecordset.Operation.Like, itemName);
            try
            {
                r.Open();
                if (!r.IsEOF())
                    retVal = r.GetS("WHAT");
                else
                    CLogs.WriteWarning(ELogsWhat.Data,
                                       String.Format("Can't find path for document '{0}' and department {1} (SYS_CONFIG.ITEM = {2})",
                                                     docType, depType.ToString(), itemName));
            }
            finally
            {
                r.Close();
                r.Destroy();
            }
            
            if (retVal == "")
            {
                r = new IMRecordset(ICSMTbl.itblSysConfig, IMRecordset.Mode.ReadOnly);
                r.Select("ITEM,WHAT");
                r.SetWhere("ITEM", IMRecordset.Operation.Like, "SHDIR-DOC");
                try
                {
                    r.Open();
                    if (!r.IsEOF())
                        retVal = r.GetS("WHAT");
                    else
                        CLogs.WriteWarning(ELogsWhat.Data, "Default document path not set (is empty)");
                }
                finally
                {
                    r.Close();
                    r.Destroy();
                }
            }

            retVal += (areaSubPath == "" ? "" : ("\\" + areaSubPath));

            return retVal;

        }
        //===================================================
        public static string[] getListDocNumber(string docType, int countDocs, DepartmentType departmentType = DepartmentType.Unknown)
        {
            var numCollect = new System.Collections.Specialized.StringCollection();
            var docNaming = new DocNaming();
            switch (docType)
            {
                case DocType.VISN:
                case DocType.VISN_NR:
                    numCollect = docNaming.GetVisnovokEMS(countDocs);
                    break;
                case DocType.DOZV:
                case DocType.DOZV_OPER:
                case DocType.DOZV_SPS:
                case DocType.DOZV_RETR:
                case DocType.DOZV_APC:
                case DocType.DOZV_APC50:
                case DocType.GARM_CERT:
                case DocType.NOVIC_CERT:
                case DocType.DOZV_MOVE:
                case DocType.DOZV_TRAIN:
                case DocType.LYST_TR_CALLSIGN:
                case DocType.LYST_ZRS_CALLSIGN:
                case DocType.DOZV_TR_SPEC:
                case DocType.DOZV_AR3_SPEC:
                    numCollect = docNaming.GetNumberOnExpluat(countDocs);
                    break;
                case DocType.DRV:
                    numCollect = (departmentType == DepartmentType.FILIA) ? docNaming.GetBranchMemoNumbers(countDocs) :
                       docNaming.GetSlujbovaZapis(countDocs);
                    break;
                case DocType.PCR:
                case DocType.VVP:
                case DocType.VVE:
                case DocType.LYST:
                case DocType._0159:
                case DocType.FYLIA:
                case DocType.PCR2:
                case DocType.REESTR_VID:
                case DocType.GIVE_LICENCE:
                case DocType.MESS_DOZV_CANCEL:
                    numCollect = docNaming.GetVihidnyList(countDocs);
                    break;
                case DocType.DOZV_CANCEL:
                    numCollect = docNaming.GetNumDozvAnn(countDocs);
                    break;
            }
            List<string> retVal = new List<string>();
            foreach (string str in numCollect)
                retVal.Add(str);

            if (retVal.Count != countDocs)
                throw new Exception(CLocaliz.TxT("Can't generate a name of the document. Please, try again later."));

            return retVal.ToArray();
        }
        //===================================================
        public static string[] GetListDocNumberForDozvWma(int countDocs)
        {
            DocNaming docNaming = new DocNaming();
            System.Collections.Specialized.StringCollection numCollect = docNaming.GetNumDozvAr3(countDocs);
            List<string> retVal = numCollect.Cast<string>().ToList();

            if (retVal.Count != countDocs)
                throw new Exception(CLocaliz.TxT("Can't generate a name of the document. Please, try again later."));

            return retVal.ToArray();
        }
        //===================================================
        /// <summary>
        /// Создает один документ
        /// </summary>
        /// <param name="tblName">имя таблицы</param>
        /// <param name="id">ID записи</param>
        /// <param name="lang">язык</param>
        /// <param name="IRPfile">массив IRP файлов (шаблонов)</param>
        /// <param name="outFile">Полный путь к файлу, который необходимо создать</param>
        /// <param name="isShowFile">true - отображает содержимое созданого файла</param>
        /// <returns>номер IRF шаблока</returns>
        private bool Report(string tblName, int id, string lang, string IRPfile, string outFile, bool isShowFile)
        {
            if ((IRPfile == null) || (IRPfile == ""))
                return false;

            RecordPtr rec = new RecordPtr(tblName, id);
            rec.PrintRTFReport(IRPfile, lang, outFile, "", !isShowFile);            
            return true;
        }
        //===================================================
        /// <summary>
        /// Возвращает имя IRF файла
        /// </summary>
        /// <param name="docType">тип документа</param>
        /// <param name="appType">тип заявки</param>
        /// <param name="dopFilter">дополнение к фильтру (количество секторов)</param>
        /// <param name="tableName">имя таблицы</param>
        /// <returns>имя IRF файла</returns>
        private string getIRPfile(string docType, AppType appType, string dopFilter, string tableName)
        {
            string filter = "";
            // Устанавливаем тип документа
            switch (docType)
            {
                case DocType.VISN: filter += "VISN"; break;
                case DocType.DOZV: filter += "DOZV"; break;
                case DocType.DOZV_TRAIN: filter += "DOZV_TRAIN"; break;
                case DocType.LYST_TR_CALLSIGN: filter += "LYST_TR_CALLSIGN"; break;
                case DocType.LYST_ZRS_CALLSIGN: filter += "LYST_ZRS"; break;
                case DocType.DOZV_AR3_SPEC: filter += "DOZV_AR3_SPEC"; break;
                case DocType.DOZV_TR_SPEC: filter += "DOZV_TR_SPEC"; break;
                case DocType.DOZV_FOREIGHT: filter += "DOZV_FOREIGHT"; break;
                case DocType.DOZV_SPS: filter += "DOZV_SPS"; break;
                case DocType.DOZV_RETR: filter += "DOZV_RETR"; break;
                case DocType.DOZV_APC: filter += "DOZV_APC"; break;
                case DocType.DOZV_APC50: filter += "DOZV_APC50"; break;
                case DocType.GARM_CERT: filter += "GARM_SERT"; break;
                case DocType.NOVIC_CERT: filter += "NOVICE_SERT"; break;
                case DocType.DOZV_MOVE: filter += "DOZV_MOVE"; break;
                case DocType.DOZV_OPER: filter += "DOZV_OPER"; break;
                case DocType.PCR: filter += "PCR"; break;
                case DocType.VVP: filter += "VVP"; break;
                case DocType.DRV: filter += "DRV"; break;
                case DocType.VVE: filter += "VVE"; break;
                case DocType.LYST: filter += "LYST"; break;
                case DocType._0159: filter += "0159"; break;
                case DocType.FYLIA: filter += "FYLIA"; break;
                case DocType.PCR2: filter += "PCR2"; break;
                case DocType.VISN_NR: filter += "VISN_NR"; break;
                case DocType.DOZV_CANCEL: filter += "DOZV_CANCEL"; break;
                case DocType.REESTR_VID: filter += "REESTR_VID"; break;
                case DocType.GIVE_LICENCE: filter += "GIVE_LICENCE"; break;
                case DocType.MESS_DOZV_CANCEL: filter += "MESS_DOZV_CANCEL"; break;
                case DocType.AKT_PTK: filter += "AKT_PTK_URZP"; break;
                case DocType.AKT_TE_PTK: filter += "AKT_TE_PTK_URZP"; break;
                case DocType.PARAM_REZ: filter += "PARAM_REZ"; break;
                case DocType.REPORT: filter += "REPORT_FORM"; break;
                default:
                    throw new Exception("Невідомий тип документу при визначенні звіту");
            }
            //
            if (docType != DocType.DOZV_CANCEL && docType != DocType.REPORT)
            {
                switch (appType)
                {
                    case AppType.AppZS: filter += "_ZS"; break;
                    case AppType.AppRS: filter += "_RS"; break;
                    case AppType.AppBS:
                        filter += "_BS";
                        if ((docType == DocType.VISN) || (docType == DocType.DOZV))
                            filter += "_" + dopFilter;
                        break;
                    case AppType.AppTR:
                        if (docType == DocType.DOZV)
                            if (!string.IsNullOrEmpty(dopFilter))
                                filter += "_" + dopFilter;
                        if ((docType != DocType.LYST_TR_CALLSIGN) && (docType != DocType.DOZV_TR_SPEC))
                            filter += "_TR";
                        break;
                    case AppType.AppTR_З:
                        if (docType == DocType.DOZV)
                            filter += "_" + dopFilter;
                        filter += "_TR_З";
                        break;
                    case AppType.AppRR:
                        filter += "_RR";
                        if ((docType == DocType.VISN) || (docType == DocType.DOZV))
                            filter += "_" + dopFilter;
                        break;
                    case AppType.AppR2:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_RA";
                        break;
                    case AppType.AppR2d:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_RD";
                        break;
                    case AppType.AppTV2:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_TA";
                        break;
                    case AppType.AppTV2d:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_TD";
                        break;
                    case AppType.AppAP3:
                        filter+="_AR3";
                        break;
                    case AppType.AppAR_3:
                        if ((docType == DocType.DOZV) && !string.IsNullOrEmpty(dopFilter))
                        {
                            filter += "_" + dopFilter;
                            filter += "_AR3";
                        }
                        else if (docType == DocType.DOZV_FOREIGHT)
                        {
                            filter += "_AR3";
                        }
                        else
                        {
                            if (docType != DocType.DOZV_AR3_SPEC)
                            {
                                filter += "_AR3";
                            }
                        }
                         break;
                    case AppType.AppVP:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_VP"; break;
                    case AppType.AppA3:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_A3"; break;
                    case AppType.AppZRS:
                        if (!string.IsNullOrEmpty(dopFilter))
                         if ((docType != DocType.LYST_ZRS_CALLSIGN))
                            filter += "_" + dopFilter;
                        if ((docType != DocType.LYST_ZRS_CALLSIGN))
                        filter += "_ZRS"; 
                        break;
                    default:
                        throw new Exception("Невідомий тип заявки при визначенні звіту");
                }
            }
            IcsmReport[] masRep =  IM.ReportsGetList(tableName, "*" + filter + "*");
                if (masRep.Length == 0)
                    throw new IMException("Не можу знайти звіт за зразком \"*" + filter + "*\"");  //Нет IRP файлов
                if (masRep.Length > 1)
                {
                    //TODO сделать форму для выбора шаблона
                }
            return System.IO.Path.GetFileName(masRep[0].Path);
        }

        //только для технологии РБСС
        //===================================================
        /// <summary>
        /// Возвращает имя IRF файла
        /// </summary>
        /// <param name="docType">тип документа</param>
        /// <param name="appType">тип заявки</param>
        /// <param name="dopFilter">дополнение к фильтру (количество секторов)</param>
        /// <param name="tableName">имя таблицы</param>
        /// <returns>имя IRF файла</returns>
        private string getIRPfile(string technologie, string docType, AppType appType, string dopFilter, string tableName)
        {
            string temp = "";
            string filter = "";
            // Устанавливаем тип документа
            switch (docType)
            {
                case DocType.VISN: filter += "VISN"; break;
                case DocType.DOZV: filter += "DOZV"; break;
                case DocType.LYST_TR_CALLSIGN: filter += "LYST_TR_CALLSIGN"; break;
                case DocType.LYST_ZRS_CALLSIGN: filter += "LYST_ZRS"; break;
                case DocType.DOZV_AR3_SPEC: filter += "DOZV_AR3_SPEC"; break;
                case DocType.DOZV_TR_SPEC: filter += "DOZV_TR_SPEC"; break;
                case DocType.DOZV_SPS: filter += "DOZV_SPS"; break;
                case DocType.DOZV_RETR: filter += "DOZV_RETR"; break;
                case DocType.DOZV_APC: filter += "DOZV_APC"; break;
                case DocType.DOZV_APC50: filter += "DOZV_APC50"; break;
                case DocType.GARM_CERT: filter += "GARM_SERT"; break;
                case DocType.NOVIC_CERT: filter += "NOVICE_SERT"; break;
                case DocType.DOZV_MOVE: filter += "DOZV_MOVE"; break;
                case DocType.DOZV_OPER: filter += "DOZV_OPER"; break;
                case DocType.PCR: filter += "PCR"; break;
                case DocType.VVP: filter += "VVP"; break;
                case DocType.DRV: filter += "DRV"; break;
                case DocType.VVE: filter += "VVE"; break;
                case DocType.LYST: filter += "LYST"; break;
                case DocType._0159: filter += "0159"; break;
                case DocType.FYLIA: filter += "FYLIA"; break;
                case DocType.PCR2: filter += "PCR2"; break;
                case DocType.VISN_NR: filter += "VISN_NR"; break;
                case DocType.DOZV_CANCEL: filter += "DOZV_CANCEL"; break;
                case DocType.REESTR_VID: filter += "REESTR_VID"; break;
                case DocType.GIVE_LICENCE: filter += "GIVE_LICENCE"; break;
                case DocType.MESS_DOZV_CANCEL: filter += "MESS_DOZV_CANCEL"; break;
                case DocType.AKT_PTK: filter += "AKT_PTK_URZP"; break;
                case DocType.AKT_TE_PTK: filter += "AKT_TE_PTK_URZP"; break;
                case DocType.PARAM_REZ: filter += "PARAM_REZ"; break;
                case DocType.REPORT: filter += "REPORT_FORM"; break;
                default:
                    throw new Exception("Невідомий тип документу при визначенні звіту");
            }
            //
            if (docType != DocType.DOZV_CANCEL && docType != DocType.REPORT)
            {
                switch (appType)
                {
                    case AppType.AppZS: filter += "_ZS"; break;
                    case AppType.AppRS: filter += "_RS"; break;
                    case AppType.AppBS:
                        filter += "_BS";
                        if ((docType == DocType.VISN) || (docType == DocType.DOZV))
                            filter += "_" + dopFilter;
                        break;
                    case AppType.AppTR:
                        if (technologie == CRadioTech.RBSS)
                        {
                            //только для технологии РБСС
                            if (docType == DocType.DOZV)
                            {
                                if (!string.IsNullOrEmpty(dopFilter))
                                    filter += "_" + dopFilter;
                                filter += "_TS";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(dopFilter))
                                    filter += "_" + dopFilter;
                                if ((docType != DocType.LYST_TR_CALLSIGN) && (docType != DocType.DOZV_TR_SPEC))
                                filter += "_TR";
                            }

                             temp = filter.TrimEnd().TrimStart()+".IRP";
                            
                        }
                        else
                        {
                            if (docType == DocType.DOZV)
                                if (!string.IsNullOrEmpty(dopFilter))
                                    filter += "_" + dopFilter;
                            if ((docType != DocType.LYST_TR_CALLSIGN) && (docType != DocType.DOZV_TR_SPEC))
                                filter += "_TR";
                        }
                        break;
                    case AppType.AppTR_З:
                        if (docType == DocType.DOZV)
                            filter += "_" + dopFilter;
                        filter += "_TR_З";
                        break;
                    case AppType.AppRR:
                        filter += "_RR";
                        if ((docType == DocType.VISN) || (docType == DocType.DOZV))
                            filter += "_" + dopFilter;
                        break;
                    case AppType.AppR2:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_RA";
                        break;
                    case AppType.AppR2d:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_RD";
                        break;
                    case AppType.AppTV2:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_TA";
                        break;
                    case AppType.AppTV2d:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_TD";
                        break;
                    case AppType.AppAP3:
                        filter += "_AR3";
                        break;
                    case AppType.AppAR_3:
                        if ((docType == DocType.DOZV) && !string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        if (docType != DocType.DOZV_AR3_SPEC)
                            filter += "_AR3"; 
                        break;
                    case AppType.AppVP:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_VP"; break;
                    case AppType.AppA3:
                        if (!string.IsNullOrEmpty(dopFilter))
                            filter += "_" + dopFilter;
                        filter += "_A3"; break;
                    case AppType.AppZRS:
                        if (!string.IsNullOrEmpty(dopFilter))
                            if ((docType != DocType.LYST_ZRS_CALLSIGN))
                                filter += "_" + dopFilter;
                        if ((docType != DocType.LYST_ZRS_CALLSIGN))
                            filter += "_ZRS"; 
                        break;
                    default:
                        throw new Exception("Невідомий тип заявки при визначенні звіту");
                }
            }

                IcsmReport[] masRep = IM.ReportsGetList(tableName, "*" + filter + "*");
                if (masRep.Length == 0)
                {
                    throw new IMException("Не можу знайти звіт за зразком \"*" + filter + "*\"");  //Нет IRP файлов
                }
                else
                {
                    temp = System.IO.Path.GetFileName(masRep[0].Path);
                }
                      
            return temp;
        }

        //===================================================
        // Только для технологии РБСС
        /// <summary>
        /// Пичатает документ и заности все необходимые поля  только для технологии РБСС
        /// </summary>
        /// <returns>true</returns>
        private bool CreateReport(string technologie, string docType, AppType appType, string tableName, int id, string outFile, bool isShowFile, DateTime startDate, DateTime endDate, string irpFile, string convertDocFileName)
        {
            IM.AdminDisconnect();
            if (Report(tableName, id, Language, irpFile, outFile, isShowFile) == true)
            {
                if (System.IO.File.Exists(outFile))
                {
                    bool createEvent = true;
                    string fileName = outFile;
                    if (string.IsNullOrEmpty(convertDocFileName) == false)
                    {
                        Office.WordManager.RtfToDoc(outFile, convertDocFileName);
                        fileName = convertDocFileName;
                        System.IO.File.Delete(outFile);
                    }
                    if (tableName == PlugTbl.XfaEsAmateur)
                    {
                        int stationId = IM.NullI;
                        IMRecordset r = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,STA_ID");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                                stationId = r.GetI("STA_ID");
                        }
                        finally
                        {
                            r.Final();
                        }
                        id = ApplSource.BaseAppClass.GetApplId(stationId, PlugTbl.XfaAmateur);
                        createEvent = false;
                    }
                    else if (tableName == PlugTbl.XfaCallAmateur)
                    {
                        int stationId = IM.NullI;
                        IMRecordset r = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,STA_ID");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                                stationId = r.GetI("STA_ID");
                        }
                        finally
                        {
                            r.Final();
                        }
                        id = ApplSource.BaseAppClass.GetApplId(stationId, PlugTbl.XfaAmateur);
                        createEvent = false;
                    }

                    //FunctionConvertToDOCX f = new FunctionConvertToDOCX();
                    //f.ConvertDOCToDOCX(fileName); 
                    //fileName = fileName.Replace(".doc", ".docx").Replace(".DOC", ".DOCX");
                    LoadDocLink(docType, "", fileName, startDate, endDate, new List<int> { id }, createEvent, true);
                    //if (System.IO.File.Exists(fileName.Replace(".docx", ".doc").Replace(".DOCX", ".DOC"))) { System.IO.File.Delete(fileName.Replace(".docx", ".doc").Replace(".DOCX", ".DOC")); }
                }
            }
            return true;
        }
        //===================================================
        /// <summary>
        /// Пичатает документ и заности все необходимые поля
        /// </summary>
        /// <returns>true</returns>
        private bool CreateReport(string docType, AppType appType, string tableName, int id, string outFile, bool isShowFile, DateTime startDate, DateTime endDate, string irpFile, string convertDocFileName)
        {
            IM.AdminDisconnect();
            if (Report(tableName, id, Language, irpFile, outFile, isShowFile) == true)
            {
                if (System.IO.File.Exists(outFile))
                {
                    bool createEvent = true;
                    string fileName = outFile;
                    if (string.IsNullOrEmpty(convertDocFileName) == false)
                    {
                        Office.WordManager.RtfToDoc(outFile, convertDocFileName);
                        fileName = convertDocFileName;
                        System.IO.File.Delete(outFile);
                    }
                    if (tableName == PlugTbl.XfaEsAmateur)
                    {
                        int stationId = IM.NullI;
                        IMRecordset r = new IMRecordset(PlugTbl.XfaEsAmateur, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,STA_ID");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                                stationId = r.GetI("STA_ID");
                        }
                        finally
                        {
                            r.Final();
                        }
                        id = ApplSource.BaseAppClass.GetApplId(stationId, PlugTbl.XfaAmateur);
                        createEvent = false;
                    }
                    else if(tableName == PlugTbl.XfaCallAmateur)
                    {
                        int stationId = IM.NullI;
                        IMRecordset r = new IMRecordset(PlugTbl.XfaCallAmateur, IMRecordset.Mode.ReadOnly);
                        r.Select("ID,STA_ID");
                        r.SetWhere("ID", IMRecordset.Operation.Eq, id);
                        try
                        {
                            r.Open();
                            if (!r.IsEOF())
                                stationId = r.GetI("STA_ID");
                        }
                        finally
                        {
                            r.Final();
                        }
                        id = ApplSource.BaseAppClass.GetApplId(stationId, PlugTbl.XfaAmateur);
                        createEvent = false;
                    }
                    //FunctionConvertToDOCX f = new FunctionConvertToDOCX();
                    //f.ConvertDOCToDOCX(fileName);
                    //fileName = fileName.Replace(".doc", ".docx").Replace(".DOC", ".DOCX");

                    
                    int Appl__ = IM.NullI;

                    if (tableName == PlugTbl.itblXnrfaPacket)
                    {
                        
                        IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                        rs.Select("APPL_ID");
                        rs.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, id);

                        try
                        {
                            rs.Open();
                            if (!rs.IsEOF())
                                Appl__ = rs.GetI("APPL_ID");
                        }
                        finally
                        {
                            rs.Final();
                        }

                        if (Appl__ != IM.NullI) id = Appl__;
                    }
                      

                    LoadDocLink(docType, "", fileName, startDate, endDate, new List<int> { id }, createEvent,true);
                    //if (System.IO.File.Exists(fileName.Replace(".docx", ".doc").Replace(".DOCX", ".DOC"))) { System.IO.File.Delete(fileName.Replace(".docx", ".doc").Replace(".DOCX", ".DOC")); }
                }
            }
            return true;
        }
       

        public bool CreateOneReport(string docType, AppType appType, DateTime startDate, DateTime endDate, string tableName, int id, string dopfilter, string outFullFileName, bool isShowDoc)
        {
            return CreateOneReport(docType, appType, startDate, endDate, tableName, id, dopfilter, outFullFileName, isShowDoc, "");
        }
        // только для технологии РБСС
        /// <summary>
        /// Создаем документ документ
        /// </summary>
        /// <returns>true</returns>
        public bool CreateOneReport(string technologie, string docType, AppType appType, DateTime startDate, DateTime endDate, string tableName, int id, string dopfilter, string outFullFileName, bool isShowDoc)
        {
            return CreateOneReport(technologie,docType, appType, startDate, endDate, tableName, id, dopfilter, outFullFileName, isShowDoc, "");
        }
        //===================================================
        // только для технологии РБСС
        /// <summary>
        /// Создаем документ документ
        /// </summary>
        /// <returns>true</returns>
        public bool CreateOneReport(string technologie, string docType, AppType appType, DateTime startDate, DateTime endDate, string tableName, int id, string dopfilter, string outFullFileName, bool isShowDoc, string convertDocFileName)
        {
            if (string.IsNullOrEmpty(convertDocFileName) == false)
                isShowDoc = false; //Открывать стандартным способом запрещено
            //Выбераем IRP файл
            string irpFile = getIRPfile(technologie,docType, appType, dopfilter, tableName);
            return CreateReport(technologie,docType, appType, tableName, id, outFullFileName, isShowDoc, startDate, endDate, irpFile, convertDocFileName);
        }
        /// <summary>
        /// Создаем документ документ
        /// </summary>
        /// <returns>true</returns>
        public bool CreateOneReport(string docType, AppType appType, DateTime startDate, DateTime endDate, string tableName, int id, string dopfilter, string outFullFileName, bool isShowDoc, string convertDocFileName)
        {
            if (string.IsNullOrEmpty(convertDocFileName) == false)
                isShowDoc = false; //Открывать стандартным способом запрещено
            //Выбераем IRP файл
            string irpFile = getIRPfile(docType, appType, dopfilter, tableName);
            return CreateReport(docType, appType, tableName, id, outFullFileName, isShowDoc, startDate, endDate, irpFile, convertDocFileName);
        }
        //===================================================
        /// <summary>
        /// Пеать документа для нескольких станций
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="startDate">Начальная дата документа</param>
        /// <param name="endDate">Конечная дата документа</param>
        /// <param name="departmentType">Тип департамента</param>
        /// <param name="managementUDCR">Тип управления УЧН</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="listId">Список ID записей</param>
        /// <param name="appType">Тип заявки</param>
        /// <param name="fileName">Имя файла. Если пусто или null, то имя генерируется</param>
        /// <returns>имя созданого файла</returns>
        public string PrintManyReports(string docType, DateTime startDate, DateTime endDate, ManagemenUDCR managementUDCR, string tableName, List<int> listId, AppType appType, string fileName, List<int> listSelectApplId)
        {
            DepartmentType departmentType = DepartmentType.Unknown;
            if (docType != DocType.DOZV_CANCEL)
            {
                switch (appType)
                {
                    case AppType.AppZS:
                    case AppType.AppBS:
                    case AppType.AppRS:
                        departmentType = DepartmentType.VFSR;
                        break;
                    case AppType.AppRR:
                    case AppType.AppTR:
                    case AppType.AppTR_З:
                    case AppType.AppZRS:
                        departmentType = DepartmentType.VRR;
                        break;
                    case AppType.AppA3:
                    case AppType.AppVP:
                    case AppType.AppAR_3:
                    case AppType.AppAP3:
                        departmentType = DepartmentType.VMA;
                        break;
                    case AppType.AppR2:
                    case AppType.AppR2d:
                    case AppType.AppTV2:
                    case AppType.AppTV2d:
                        departmentType = DepartmentType.VRS;
                        break;
                    default:
                        throw new Exception("(error 2) Это сообщение Вы недолжны видеть. Если это не так, то необходимо обратиться к программисту.");
                }
            }
            else
            {
                departmentType = CUsers.GetUserDepartmentType();
            }
            string fullFileName = "";
            if (string.IsNullOrEmpty(fileName) == false)
            {
                string fullPath = PrintDocs.getPath(docType);
                if (fullPath == "")
                    fullPath = ".";
                fullPath += "\\";
                
               
                if (docType == DocType.LYST_ZRS_CALLSIGN)
                {
                    string number = PrintDocs.GetListDocNumberForDozvWma(1)[0];
                    fullPath += "МС-" + CUsers.GetUserAreaCode() + "-" + number + ".rtf";
                    fullFileName = fullPath;
                }
                else fullFileName = fullPath + fileName;
            }
            else
            {
                string[] numberDoc = PrintDocs.getListDocNumber(docType, 1);
                fullFileName = GetDocName(docType, departmentType, managementUDCR, numberDoc[0]);
                if (docType == DocType.LYST_ZRS_CALLSIGN)
                {
                    string fullPath = PrintDocs.getPath(docType);
                    if (fullPath == "")
                        fullPath = ".";
                    fullPath += "\\";
                    string number = PrintDocs.GetListDocNumberForDozvWma(1)[0];
                    fullPath += "МС-" + CUsers.GetUserAreaCode() + "-" + number + ".rtf";
                    fullFileName = fullPath;
                }
            }
            CreateManyReport(docType, appType, tableName, listId, "", fullFileName, false, startDate, endDate, listSelectApplId);
            return fullFileName;
        }
        //===================================================
        /// <summary>
        /// Создает отчет для нескольких станций
        /// </summary>
        private bool CreateManyReport(string docType, AppType appType, string tableName, List<int> listId, string dopfilter, string outFullFileName, bool isShowDoc, DateTime startDate, DateTime endDate, List<int> listSelectApplId)
        {
            string irpFile = getIRPfile(docType, appType, dopfilter, tableName);
            return CreateManyReports(docType, tableName, listId, outFullFileName, isShowDoc, irpFile, startDate, endDate, listSelectApplId);
        }
        //===================================================
        /// <summary>
        /// Возвращает полный путь к документу
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="departmentType">Тип департамента</param>
        /// <param name="managementUDCR">Тип управления УДЦР</param>
        /// <param name="number">номер документа</param>
        /// <returns>полное имя документа</returns>
        private string GetDocName(string docType, DepartmentType departmentType, ManagemenUDCR managementUDCR, string number)
        {
            string fullPath = PrintDocs.getPath(docType);
            if (fullPath == "")
                fullPath = ".";
            fullPath += "\\";

            if (docType == DocType.DRV)
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(departmentType) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if ((docType == DocType.PCR) || (docType == DocType.VVP)
                  || (docType == DocType.VVE) || (docType == DocType.LYST)
                  || (docType == DocType._0159) || (docType == DocType.FYLIA)
                  || (docType == DocType.PCR2) || (docType == DocType.REESTR_VID) || (docType == DocType.LYST_ZRS_CALLSIGN))
                fullPath += managementUDCR.ToString("D") + "." + ConvertType.DepartmetToCode(departmentType) + "-" + HelpFunction.getUserNumber() + "-" + number + ".rtf";
            else if (docType == DocType.DOZV_CANCEL)
                fullPath += "анулювання_" + number + ".rtf";
            return fullPath;
        }
        //===================================================
        /// <summary>
        /// Созадет документ из нескольких заяв
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="listId">Список ID записей таблицы</param>
        /// <param name="outFullFileName">Имя выходного файла</param>
        /// <param name="isShowDoc">Загрузить созданый документ</param>
        /// <param name="IRFFile">Путь к шаблону</param>
        /// <param name="startDate">дата начала документа</param>
        /// <param name="endDate">дата окончания документа</param>
        /// <returns>true</returns>
        private bool CreateManyReports(string docType, string tableName, List<int> listId, string outFullFileName, bool isShowDoc, string IRFFile, DateTime startDate, DateTime endDate, List<int> listSelectApplId)
        {
            //Создаем запрос
            bool needComma = false;
            string query = "([ID] in (";
            foreach (int id in listId)
            {
                if (needComma)
                    query += ", ";
                query += id.ToString();
                needComma = true;
            }
            query += "))";



            //query = string.Format(" ((select count(*) from %XNRFA_PAC_TO_APPL b where b.PACKET_ID=[ID] and b.APPL_ID in ({1}))>0)", listId[0], query);
            /*
            if (tableName == PlugTbl.itblXnrfaPacket)
            {
                needComma = false;
                query += " ([APPL_ID] in (";
                foreach (int id in listSelectApplId)
                {
                    if (needComma)
                        query += ", ";
                    query += id.ToString();
                    needComma = true;
                }
                query += "))";
            }
             */ 

            
            /*
            List<int> listApplId = new List<int>();
            if ((tableName == PlugTbl.itblXnrfaPacket) && (listId.Count > 0))
            {
                //Есди печатаем из пакета #4320
                IMRecordset rs = new IMRecordset(PlugTbl.itblPacketToAppl, IMRecordset.Mode.ReadOnly);
                rs.Select("APPL_ID");
                rs.SetWhere("PACKET_ID", IMRecordset.Operation.Eq, listId[0]);
                try
                {
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                        listApplId.Add(rs.GetI("APPL_ID"));
                }
                finally
                {
                    rs.Destroy();
                }
            }
            else
                listApplId = listId;
             */ 
              


            if (docType == DocType.DOZV_CANCEL)
            {
                // Устанавливаем дату анулирования
                foreach (int applId in listSelectApplId)
                {
                    ApplDocuments.CancelPermision(applId, startDate);
                }
            }

            IM.AdminDisconnect();
            // Создаем отчет
            RecordSqlSelection recSqlSelect = new RecordSqlSelection(tableName, query, "ID", OrderDirection.Descending);
            recSqlSelect.PrintRTFReport(IRFFile, "RUS", outFullFileName, "", isShowDoc);
            if (System.IO.File.Exists(outFullFileName))
            {
                LoadDocLink(docType, "", outFullFileName, startDate, endDate, listSelectApplId, true);
            }
            return true;
        }
        /// <summary>
        /// Привязывает документ к станции
        /// </summary>
        /// <param name="docType"></param>
        /// <param name="outFullFileName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="listId"></param>
        public void LoadDocLink(string docType, string docRef, string outFullFileName, DateTime startDate, DateTime? endDate, List<int> listId, bool isNullNumberDoc)
        {
            LoadDocLink(docType, docRef, outFullFileName, startDate, endDate, listId, true, isNullNumberDoc);
        }
        //===================================================
        public void LoadDocLink(string docType, string docRef, string outFullFileName, DateTime startDate, DateTime? endDate, List<int> listId, int IDPacket)
        {
            LoadDocLink(docType, docRef, outFullFileName, startDate, endDate, listId, true, IDPacket);
        }

        /// <summary>
        /// Проверка таблицы  DocFiles на предмет наличия записи с номером ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool isDublicateRec(int ID)
        {
            bool isDublicate = false;
            IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadWrite);
            rsDocFiles.Select("ID,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,CREATED_BY,PATH,DATE_CREATED");
            rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, ID);
            try
            {
                rsDocFiles.Open();
                if (!rsDocFiles.IsEOF())
                {
                    isDublicate = true;
                }
               
            }
            finally
            {
                rsDocFiles.Close();
                rsDocFiles.Destroy();
            }
            return isDublicate;
        }

        public void InsertDuplicateStr(string SourceFile, int Type)
        {
            try
            {
                bool isManualCorrect = false;
                string str_max = @"{\headerr \ltrpar \pard\plain \ltrpar\s19\qr \li0\ri0\widctlpar\tqc\tx4677\tqr\tx9355\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid8395446 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 \fs24\lang1049\langfe1049\cgrid\langnp1049\langfenp1049 {\rtlch\fcs1 \af0 \ltrch\fcs0 \lang1033\langfe1049\langnp1033\insrsid8395446 \'c4\'d3\'c1\'cb\'b2\'ca\'c0\'d2}{\rtlch\fcs1 \af0 \ltrch\fcs0 \lang1033\langfe1049\langnp1033\insrsid6161860\charrsid8395446 \par }}";
                string str_min = @"{\headerr \ltrpar \pard\plain \ltrpar\s19\qr \li0\ri0\widctlpar\tqc\tx4677\tqr\tx9355\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid8395446 \rtlch\fcs1 \af2\afs20\alang1025 \ltrch\fcs0 \f2\fs20\lang1055\langfe1049\cgrid\langnp1055\langfenp1049 {\rtlch\fcs1 \af0\afs16 \ltrch\fcs0 \f0\fs16\lang1033\langfe1049\langnp1033\insrsid8395446\charrsid4988403 \'c4\'d3\'c1\'cb\'b2\'ca\'c0\'d2}{\rtlch\fcs1 \af0\afs16 \ltrch\fcs0 \f0\fs16\lang1033\langfe1049\langnp1033\insrsid6161860\charrsid4988403 \par }}";
                using (FileStream fstream = new FileStream(SourceFile, FileMode.OpenOrCreate)) {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);
                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                    int idx = 0;
                    int idx1 = textFromFile.IndexOf(@"{\footerr");
                    int idx2 = textFromFile.IndexOf(@"\footery709\");
                    //if (idx2 <= 0) idx2 = textFromFile.IndexOf(@"\footery710\");
                    //if (idx2 <= 0) idx2 = textFromFile.IndexOf(@"\footery708\");
                    //if (idx2 <= 0) idx2 = textFromFile.IndexOf(@"\footery707\");
                    //if (idx2 <= 0) idx2 = textFromFile.IndexOf(@"\footery706\");
                    if (idx2 <= 0)  {
                        for (int i=1;i<2000;i++) {
                            idx2 = textFromFile.IndexOf(string.Format(@"\footery{0}\",i));
                            if (idx2 > 0) break;
                        }
                    }
                    
                    if (idx1 > 0) idx = idx1;
                    else if (idx2 > 0) {
                        string rx = textFromFile.Substring(idx2, textFromFile.Length - idx2- 1);
                        int fnd_r = rx.IndexOf(" ");
                        if (fnd_r>0) {
                            idx = fnd_r+idx2;
                        }
                    }

                    if (idx > 0) {
                        isManualCorrect = false;
                        string s = textFromFile.Insert(idx, Type==1 ? str_min : str_max);
                        byte[] array_paste = System.Text.Encoding.Default.GetBytes(s);
                        fstream.Position = 0;
                        fstream.Write(array_paste, 0, array_paste.Length);
                    }
                    else isManualCorrect = true;
                    fstream.Close();
                }
                if (isManualCorrect)
                {
                    MessageBox.Show("В процесі створення дублікату плагін не зміг знайти секцію для вставки слова 'ДУБЛІКАТ'."+Environment.NewLine+ "Прохання внести відповідні правки. ");
                    System.Diagnostics.Process.Start(SourceFile);
                }

            }
            catch (Exception ex)
            {
                
            }
        }


        /// <summary>
        /// Создать дубликат документа
        /// </summary>
        /// <param name="DOZV_NUM">номер разрешения</param>
        public void CreateDuplicateDoc(int APPL_ID, string DOZV_NUM, AppType applType, string radiotech)
        {

            string SQL= string.Format("SELECT COUNT(*) FROM %DOCLINK R WHERE R.PATH LIKE '%{0}Д%'",DOZV_NUM);
            int CountDuplicate = 0;
            IM.ExecuteScalar(ref CountDuplicate, SQL);
            if ((CountDuplicate>=0) && (CountDuplicate!=IM.NullI)) CountDuplicate++;
            if (CountDuplicate==IM.NullI) CountDuplicate=1;
            YDoclink doclnk_create = new YDoclink(); doclnk_create.Format("*"); doclnk_create.New();
            YDocfiles docf_create = new YDocfiles(); docf_create.Format("*"); docf_create.New(); docf_create.AllocID(); 
            YDoclink doclnk = new YDoclink();
            doclnk.Format("*");
            if (doclnk.Fetch(string.Format(" ([PATH] LIKE '%{0}%') AND ([PATH] NOT LIKE '%{1}Д%') ",DOZV_NUM,DOZV_NUM))) 
            {
                string PATH_OLD = doclnk.m_path;
                if (PATH_OLD != "") {
                    if (System.IO.File.Exists(PATH_OLD)) {
                        //string OLD_FLE = System.IO.Path.GetDirectoryName(PATH_OLD) + @"\" + System.IO.Path.GetFileNameWithoutExtension(PATH_OLD);
                        string NEW_FLE = System.IO.Path.GetDirectoryName(PATH_OLD) + @"\" + System.IO.Path.GetFileNameWithoutExtension(PATH_OLD) + "Д" + CountDuplicate.ToString() +  System.IO.Path.GetExtension(PATH_OLD);
                        if (System.IO.File.Exists(NEW_FLE)) {
                            System.IO.File.Delete(NEW_FLE);
                        }
                        System.IO.File.Copy(PATH_OLD, NEW_FLE);
                        if (System.IO.File.Exists(NEW_FLE)) {

                            if ((((applType== AppType.AppAR3) || (applType== AppType.AppAR_3)) && ((radiotech==CRadioTech.YKX) || (radiotech==CRadioTech.KX) || (radiotech==CRadioTech.ЦУКХ) || (radiotech==CRadioTech.PD) || (radiotech==CRadioTech.RBSS) || (radiotech==CRadioTech.TETRA) || (radiotech==CRadioTech.TRUNK) || (radiotech==CRadioTech.SHR) || (radiotech==CRadioTech.MsR) || (radiotech==CRadioTech.MmR) || (radiotech==CRadioTech.RPATL) || (radiotech==CRadioTech.ROPS) || (radiotech==CRadioTech.BAUR) || (radiotech==CRadioTech.BCUR) || (radiotech==CRadioTech.SptRZ))) || ((applType== AppType.AppAP3) && ((radiotech==CRadioTech.YKX) || (radiotech==CRadioTech.KX))))
                                InsertDuplicateStr(NEW_FLE, 1);
                            else
                                InsertDuplicateStr(NEW_FLE, 2);
                            doclnk_create.m_name = doclnk.m_name.Replace(System.IO.Path.GetFileName(PATH_OLD), System.IO.Path.GetFileName(NEW_FLE));
                            doclnk_create.m_path = NEW_FLE;
                            doclnk_create.m_rec_id = doclnk.m_rec_id;
                            doclnk_create.m_rec_tab = doclnk.m_rec_tab;
                            int DOC_ID = doclnk.m_doc_id;
                            if (DOC_ID != IM.NullI) {
                                YDocfiles docf = new YDocfiles();
                                docf.Format("*");
                                if (docf.Fetch(DOC_ID)) {
                                    docf_create.m_path = NEW_FLE;
                                    docf_create.m_doc_date = docf.m_doc_date;
                                    docf_create.m_doc_ref = System.IO.Path.GetFileNameWithoutExtension(NEW_FLE);
                                    docf_create.m_doc_type = docf.m_doc_type;
                                    docf_create.m_exp_date = docf.m_exp_date;
                                    docf_create.m_memo = docf.m_memo;
                                    docf_create.m_name = docf.m_name;
                                    docf_create.m_created_by = IM.ConnectedUser();
                                    docf_create.m_date_created = DateTime.Now;
                                    docf_create.Save();
                                    doclnk_create.m_doc_id = docf_create.m_id;
                                }
                            }
                            doclnk_create.Save();

                            // Заполняем таблицы
                            string dc_type="";
                            switch (docf_create.m_doc_type)
                            {
                                case "FIL":
                                    dc_type = DocType.FYLIA;
                                    break;
                                case "0159":
                                    dc_type = DocType._0159;
                                    break;
                                case "LYST":
                                    dc_type = DocType.LYST;
                                    break;
                                case "DRV":
                                    dc_type = DocType.DRV;
                                    break;
                                case "VVE":
                                    dc_type = DocType.VVE;
                                    break;
                                case "PCR":
                                    dc_type = DocType.PCR;
                                    break;
                                case "DE":
                                    dc_type = DocType.DOZV;
                                    break;
                                case "VE":
                                    dc_type = DocType.VISN;
                                    break;
                                case "ANN":
                                    dc_type = DocType.DOZV_CANCEL;
                                    break;
                                case "ANN_CANCEL":
                                    dc_type = DocType.DOZV_CANCEL_ANNUL;
                                    break;
                            }

                            CEventLog.AddApplDocEvent(APPL_ID, dc_type, docf_create.m_doc_date, System.IO.Path.GetFileNameWithoutExtension(PATH_OLD), docf_create.m_exp_date, NEW_FLE);
                        }
                    }
                }
            }
            doclnk_create.Close(); doclnk_create.Dispose();
            docf_create.Close(); docf_create.Dispose();
        }
        //===================================================
        /// <summary>
        /// Привязывает документ к станции
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="outFullFileName">Файл</param>
        /// <param name="docDate">Дата начала</param>
        /// <param name="expDate">Дата окончания</param>
        /// <param name="listId">Список ID заявок</param>
        /// <param name="createEvent">Создать событие</param>
        public void LoadDocLink(string docType, string docRef, string outFullFileName, DateTime docDate, DateTime? expDate, List<int> listId, bool createEvent,bool isNullNumberDoc)
        {
            // Заполняем таблицы
            string StrDocType = "";
            switch (docType)
            {
                case DocType.FYLIA:
                    StrDocType = "FIL";
                    break;
                case DocType._0159:
                    StrDocType = "0159";
                    break;
                case DocType.LYST:
                    StrDocType = "LYST";
                    break;
                case DocType.DRV:
                    StrDocType = "DRV";
                    break;
                case DocType.VVE:
                    StrDocType = "VVE";
                    break;
                case DocType.PCR:
                    StrDocType = "PCR";
                    break;
                case DocType.DOZV:
                case DocType.DOZV_TRAIN:
                case DocType.DOZV_FOREIGHT:
                case DocType.DOZV_RETR:
                case DocType.DOZV_APC:
                case DocType.DOZV_APC50:
                case DocType.DOZV_MOVE:
                case DocType.DOZV_SPS:
                case DocType.LYST_ZRS_CALLSIGN:
                case DocType.LYST_TR_CALLSIGN:
                    StrDocType = "DE";
                    break;
                case DocType.VISN:
                    StrDocType = "VE";
                    break;
                case DocType.DOZV_CANCEL:
                    StrDocType = "ANN";
                    break;
                case DocType.DOZV_CANCEL_ANNUL:
                    StrDocType = "ANN_CANCEL";
                    break;
                default:
                    StrDocType = docType;
                    break;
            }
            string StrDocNum ="";
            if (isNullNumberDoc) { StrDocNum = string.IsNullOrEmpty(docRef) ? outFullFileName.ToFileNameWithoutExtension() : docRef; } else { StrDocNum = string.IsNullOrEmpty(docRef) ? "" : docRef; }

            
            string StrFileName = System.IO.Path.GetFileName(outFullFileName);
            string remark = StrDocType + " " + docDate.ToString("dd.MM.yyyy") + "; " + StrDocNum + "; " +
                (expDate == null ? "" : ((DateTime)expDate).ToString("dd.MM.yyyy")) + "; " + IM.ConnectedUser() + "; " + StrFileName;

            // сохраняем данные в БД
            int dfId = IM.AllocID(ICSMTbl.DocFiles, 1, -1);

            {
                IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadWrite);
                rsDocFiles.Select("ID,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,CREATED_BY,PATH,DATE_CREATED");
                rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                try
                {
                    rsDocFiles.Open();
                    rsDocFiles.AddNew();
                    rsDocFiles.Put("ID", dfId);
                    rsDocFiles.Put("DOC_TYPE", StrDocType);
                    rsDocFiles.Put("DOC_REF", StrDocNum);
                    rsDocFiles.Put("DOC_DATE", docDate);
                    rsDocFiles.Put("EXP_DATE", expDate);
                    rsDocFiles.Put("CREATED_BY", IM.ConnectedUser());
                    rsDocFiles.Put("PATH", outFullFileName);
                    rsDocFiles.Put("DATE_CREATED", DateTime.Now);
                    rsDocFiles.Update();
                }
                finally
                {
                    rsDocFiles.Close();
                    rsDocFiles.Destroy();
                }
            }

            // По станциям
            foreach (int applID in listId)
            {
                if (createEvent)
                    CEventLog.AddApplDocEvent(applID, docType, docDate, StrDocNum, expDate, outFullFileName);
                List<int> objList = new List<int>();
                string objTable = "";
                // Для заявки
                {
                    IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                    try
                    {
                        rsDocLink.Open();
                        rsDocLink.AddNew();
                        rsDocLink.Put("DOC_ID", dfId);
                        rsDocLink.Put("REC_ID", applID);
                        rsDocLink.Put("REC_TAB", PlugTbl.itblXnrfaAppl);
                        rsDocLink.Put("NAME", remark);
                        rsDocLink.Put("PATH", outFullFileName);
                        rsDocLink.Update();
                    }
                    finally
                    {
                        rsDocLink.Close();
                        rsDocLink.Destroy();
                    }
                }
                {
                    // Вытаскаваем все станции, которые привязаны к заявке
                    IMRecordset rsAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                    rsAppl.Select("OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                    rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                    try
                    {
                        for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                        {
                            if (string.IsNullOrEmpty(objTable))
                                objTable = rsAppl.GetS("OBJ_TABLE");
                            for (int i = 1; i <= 6; i++)
                            {
                                int stationID = rsAppl.GetI("OBJ_ID" + i.ToString());
                                if ((stationID != 0) && (stationID != IM.NullI))
                                    objList.Add(stationID);
                                else break;
                            }
                        }
                    }
                    finally
                    {
                        rsAppl.Close();
                        rsAppl.Destroy();
                    }
                }

                // Сохраняем ссилку на документ в каждой заявке
                foreach (int objId in objList)
                {
                    IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                    try
                    {
                        rsDocLink.Open();
                        rsDocLink.AddNew();
                        rsDocLink.Put("DOC_ID", dfId);
                        rsDocLink.Put("REC_ID", objId);
                        rsDocLink.Put("REC_TAB", objTable);
                        rsDocLink.Put("NAME", remark);
                        rsDocLink.Put("PATH", outFullFileName);
                        rsDocLink.Update();
                    }
                    finally
                    {
                        rsDocLink.Close();
                        rsDocLink.Destroy();
                    }
                }
            }
        }



        public void LoadDocLink(string docType, string docRef, string outFullFileName, DateTime docDate, DateTime? expDate, List<int> listId, bool createEvent, int IDPacket)
        {
            // Заполняем таблицы
            string StrDocType = "";
            switch (docType)
            {
                case DocType.FYLIA:
                    StrDocType = "FIL";
                    break;
                case DocType._0159:
                    StrDocType = "0159";
                    break;
                case DocType.LYST:
                    StrDocType = "LYST";
                    break;
                case DocType.DRV:
                    StrDocType = "DRV";
                    break;
                case DocType.VVE:
                    StrDocType = "VVE";
                    break;
                case DocType.PCR:
                    StrDocType = "PCR";
                    break;
                case DocType.DOZV:
                case DocType.DOZV_TRAIN:
                case DocType.DOZV_FOREIGHT:
                case DocType.DOZV_RETR:
                case DocType.DOZV_APC:
                case DocType.DOZV_APC50:
                case DocType.DOZV_MOVE:
                case DocType.DOZV_SPS:
                    StrDocType = "DE";
                    break;
                case DocType.VISN:
                    StrDocType = "VE";
                    break;
                case DocType.DOZV_CANCEL:
                    StrDocType = "ANN";
                    break;
                case DocType.DOZV_CANCEL_ANNUL:
                    StrDocType = "ANN_CANCEL";
                    break;
                default:
                    StrDocType = docType;
                    break;
            }

            //string StrDocNum = string.IsNullOrEmpty(docRef) ? outFullFileName.ToFileNameWithoutExtension() : docRef;
            string StrDocNum = string.IsNullOrEmpty(docRef) ? "" : docRef;
            string StrFileName = System.IO.Path.GetFileName(outFullFileName);
            string remark = StrDocType + " " + docDate.ToString("dd.MM.yyyy") + "; " + StrDocNum + "; " +
                (expDate == null ? "" : ((DateTime)expDate).ToString("dd.MM.yyyy")) + "; " + IM.ConnectedUser() + "; " + StrFileName;

            // сохраняем данные в БД
            int dfId = IM.AllocID(ICSMTbl.DocFiles, 1, -1);
            NewIdLinkDoc = dfId;

            {
                IMRecordset rsDocFiles = new IMRecordset(ICSMTbl.DocFiles, IMRecordset.Mode.ReadWrite);
                rsDocFiles.Select("ID,DOC_TYPE,DOC_REF,DOC_DATE,EXP_DATE,CREATED_BY,PATH,DATE_CREATED,MEMO");
                rsDocFiles.SetWhere("ID", IMRecordset.Operation.Eq, -1);
                try
                {
                    rsDocFiles.Open();
                    rsDocFiles.AddNew();
                    rsDocFiles.Put("ID", dfId);
                    rsDocFiles.Put("DOC_TYPE", StrDocType);
                    rsDocFiles.Put("DOC_REF", StrDocNum);
                    rsDocFiles.Put("DOC_DATE", docDate);
                    rsDocFiles.Put("EXP_DATE", expDate);
                    rsDocFiles.Put("CREATED_BY", IM.ConnectedUser());
                    rsDocFiles.Put("PATH", outFullFileName);
                    rsDocFiles.Put("DATE_CREATED", DateTime.Now);
                    rsDocFiles.Update();
                    //rsDocFiles.Put("MEMO", IDPacket.ToString());

                    if (IDPacket > -1)
                    {
                        IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                        rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                        //rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                        try
                        {
                            rsDocLink.Open();
                            rsDocLink.AddNew();
                            rsDocLink.Put("DOC_ID", dfId);
                            rsDocLink.Put("REC_ID", IDPacket);
                            rsDocLink.Put("REC_TAB", PlugTbl.itblXnrfaPacket);
                            rsDocLink.Put("NAME", remark);
                            rsDocLink.Put("PATH", outFullFileName);
                            rsDocLink.Update();
                        }
                        finally
                        {
                            rsDocLink.Close();
                            rsDocLink.Destroy();
                        }
                    }
                    
                    
                }
                finally
                {
                    rsDocFiles.Close();
                    rsDocFiles.Destroy();
                }
            }

            
            // По станциям
            foreach (int applID in listId)
            {
                if (createEvent)
                    CEventLog.AddApplDocEvent(applID, docType, docDate, StrDocNum, expDate, outFullFileName);
                List<int> objList = new List<int>();
                string objTable = "";
                // Для заявки
                {
                    IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                    try
                    {
                        rsDocLink.Open();
                        rsDocLink.AddNew();
                        rsDocLink.Put("DOC_ID", dfId);
                        rsDocLink.Put("REC_ID", applID);
                        rsDocLink.Put("REC_TAB", PlugTbl.itblXnrfaAppl);
                        rsDocLink.Put("NAME", remark);
                        rsDocLink.Put("PATH", outFullFileName);
                        rsDocLink.Update();
                    }
                    finally
                    {
                        rsDocLink.Close();
                        rsDocLink.Destroy();
                    }
                }
                {
                    // Вытаскаваем все станции, которые привязаны к заявке
                    IMRecordset rsAppl = new IMRecordset(PlugTbl.itblXnrfaAppl, IMRecordset.Mode.ReadOnly);
                    rsAppl.Select("OBJ_TABLE,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6");
                    rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applID);
                    try
                    {
                        for (rsAppl.Open(); !rsAppl.IsEOF(); rsAppl.MoveNext())
                        {
                            if (string.IsNullOrEmpty(objTable))
                                objTable = rsAppl.GetS("OBJ_TABLE");
                            for (int i = 1; i <= 6; i++)
                            {
                                int stationID = rsAppl.GetI("OBJ_ID" + i.ToString());
                                if ((stationID != 0) && (stationID != IM.NullI))
                                    objList.Add(stationID);
                                else break;
                            }
                        }
                    }
                    finally
                    {
                        rsAppl.Close();
                        rsAppl.Destroy();
                    }
                }

                // Сохраняем ссилку на документ в каждой заявке
                foreach (int objId in objList)
                {
                    IMRecordset rsDocLink = new IMRecordset(ICSMTbl.itblDocLink, IMRecordset.Mode.ReadWrite);
                    rsDocLink.Select("REC_ID,REC_TAB,NAME,DOC_ID,PATH");
                    rsDocLink.SetWhere("REC_ID", IMRecordset.Operation.Eq, -1);
                    try
                    {
                        rsDocLink.Open();
                        rsDocLink.AddNew();
                        rsDocLink.Put("DOC_ID", dfId);
                        rsDocLink.Put("REC_ID", objId);
                        rsDocLink.Put("REC_TAB", objTable);
                        rsDocLink.Put("NAME", remark);
                        rsDocLink.Put("PATH", outFullFileName);
                        rsDocLink.Update();
                    }
                    finally
                    {
                        rsDocLink.Close();
                        rsDocLink.Destroy();
                    }
                }
            }
             
        }

        /// <summary>
        /// Формирует имя файла "Службова записка в ДРВ" для филии
        /// </summary>
        /// <param name="number">Порядковый номер документа</param>
        /// <returns>Имя Файла</returns>
        internal static string GetBranchDocumentName(string number)
        {
            return string.Format("Ф{0}-{1}", CUsers.GetUserAreaCode().AppendFirstSymbols('0', 2),
                  number.AppendFirstSymbols('0', 6));
        }
    }
}
