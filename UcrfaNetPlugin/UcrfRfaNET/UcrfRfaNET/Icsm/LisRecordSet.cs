﻿using System;
using ICSM;

namespace XICSM.UcrfRfaNET.Icsm
{
    /// <summary>
    /// Класс для безопасной работи с IMRecordset
    /// </summary>
    internal class LisRecordSet : IMRecordset, IDisposable
    {
        //===================================================
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tableName">tableName</param>
        /// <param name="mode">Mode</param>
        public LisRecordSet(string tableName, Mode mode) : base(tableName, mode)
        {
        }
        //===================================================
        /// <summary>
        /// Destroys the progress form 
        /// </summary>
        public new void Dispose()
        {
            if (this.IsOpen())
                this.Close();
            Destroy();
        }
    }
}
