﻿using System;
using ICSM;

namespace XICSM.UcrfRfaNET.Icsm
{
    internal class LisTransaction : IDisposable
    {
        private static int _levelTransaction = 0;
        private bool _isCommited = false;
        //===================================================
        /// <summary>
        /// Constructor
        /// </summary>
        public LisTransaction()
        {
            if (_levelTransaction == 0)
                IMTransaction.Begin();
            _levelTransaction++;
        }
        //===================================================
        /// <summary>
        /// Destroys the progress form 
        /// </summary>
        public void Dispose()
        {
            if (_levelTransaction == 0)
                throw new Exception("Level transaction is zero");
            _levelTransaction--;
            if ((_levelTransaction == 0) && (_isCommited == false))
                IMTransaction.Rollback();
        }
        //===================================================
        /// <summary>
        /// Commit
        /// </summary>
        public void Commit()
        {
            if (_isCommited == true)
                throw new Exception("Transaction is already commited");
            if (_levelTransaction == 1)
                IMTransaction.Commit();
            _isCommited = true;
        }
    }
}
