﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace ArticleLib
{
    class MobStation
    {
        public const string TableName = "MOB_STATION";
        /// <summary>
        /// Возвращает ID статьи
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static bool UpdateArticle(int applId)
        {
            string NetworkIdent = "";
            int isLicence = -1;
            double powerdBw = IM.NullD;
            double powerW = IM.NullD;
            string setArticle = "";
            string standard = "";
            int netId = IM.NullI;
            string explType = "";
            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsAs = new IMRecordset("MOB_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD,PWR_ANT,CUST_CHB1,NETWORK_IDENT");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        NetworkIdent = rsAs.GetS("NETWORK_IDENT");
                        isLicence = rsAs.GetI("CUST_CHB1");
                        powerdBw = rsAs.GetD("PWR_ANT");
                        if (powerdBw != IM.NullD)
                            powerW = IM.RoundDeci(Math.Pow(10.0, powerdBw / 10.0), 4);
                        standard = rsAs.GetS("STANDARD");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }

            switch (standard)
            {
                case "ЦУКХ":
                case "УКХ":

                    if (isLicence == 0)
                    {
                        setArticle = ArticleFunc.article_22_1;
                    }
                    else
                    {

                        if (standard == "ЦУКХ")
                        {

                            if (powerW != IM.NullD)
                            {
                                if (powerW <= 3.0)
                                    setArticle = ArticleFunc.article_22_2_1;
                                else
                                    setArticle = ArticleFunc.article_22_2_2;
                            }
                        }
                        else if (standard == "УКХ")
                        {
                            if (NetworkIdent.Contains("АЛТАЙ"))
                            {
                                setArticle = ArticleFunc.article_22_3;
                            }
                            else
                            {
                                if (powerW != IM.NullD)
                                {
                                    if (powerW <= 3.0)
                                        setArticle = ArticleFunc.article_22_2_1;
                                    else
                                        setArticle = ArticleFunc.article_22_2_2;
                                }
                            }
                        }
                                

                        
                    }
                    break;
                default:
                    return false;
            }
            return ArticleFunc.SetArticle(applId, ArticleFunc.GetArticleId(setArticle, standard));
        }
        /// <summary>
        /// Обновляет статью у земной станции
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>TRUE - было обновление статьи. FALSE - обновлений небыло</returns>
        //public static bool UpdateArticle(int applId)
        //{
            //return ArticleFunc.SetArticle(applId, GetArticleId(applId));
        //}
        /// <summary>
        /// Расчитывает кол-во работ
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWork(int applId, string Article)
        {
            int stationId = IM.NullI;
            string tableName = "";
            string article = "";
            string standard = "";
            int netId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1,OBJ_TABLE,Price.ARTICLE");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                    tableName = rsAppl.GetS("OBJ_TABLE");
                    article = rsAppl.GetS("Price.ARTICLE");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            if ((tableName != "MOB_STATION") || (stationId == IM.NullI))
                return IM.NullI;

            int count = 0;
            {
                IMRecordset rsAs = new IMRecordset("MOB_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        standard = rsAs.GetS("STANDARD");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }

            if (article == "") article = Article;
            //----
            switch (article)
            {
                case ArticleFunc.article_22_2_1:
                case ArticleFunc.article_22_2_2:
                case ArticleFunc.article_22_3:
                    {
                        if ((standard == "УКХ") || (standard == "ЦУКХ"))
                        {
                            //Загрузка частот АС
                            HashSet<double> countUnicFreqs = new HashSet<double>();
                            IMRecordset rsAs = new IMRecordset("MOBSTA_FREQS", IMRecordset.Mode.ReadOnly);
                            rsAs.Select("STA_ID,TX_FREQ,RX_FREQ");
                            rsAs.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId);
                            try
                            {
                                for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                {
                                    double freqTx = IM.RoundDeci(rsAs.GetD("TX_FREQ"), 6);
                                    double freqRx = IM.RoundDeci(rsAs.GetD("RX_FREQ"), 6);
                                    if (countUnicFreqs.Contains(freqRx) == false)
                                        countUnicFreqs.Add(freqRx);
                                    if (countUnicFreqs.Contains(freqTx) == false)
                                        countUnicFreqs.Add(freqTx);
                                }
                            }
                            finally
                            {
                                if (rsAs.IsOpen())
                                    rsAs.Close();
                                rsAs.Destroy();
                            }
                            count = countUnicFreqs.Count;
                        }
                    }
                    break;
                    // !1
                case ArticleFunc.article_22_1:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,MobileSector1.AssignedFrequencies.TX_FREQ,MobileSector1.AssignedFrequencies.RX_FREQ");
                        rsAs.SetWhere("MobileSector1.ID", IMRecordset.Operation.Eq, stationId);
                        rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("MobileSector1.AssignedFrequencies.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("MobileSector1.AssignedFrequencies.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                default:
                    count = 0;
                    break;
            }
            if (count > 0)
                return count;
            return IM.NullI;
        }
    }

}
