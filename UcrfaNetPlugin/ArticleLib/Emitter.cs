﻿using ICSM;

namespace ArticleLib
{
    class Emitter
    {
        public const string TableName = "XFA_EMI_STATION";
        /// <summary>
        /// Возвращает ID статьи
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static int GetArticleId(int applId)
        {
            return ArticleFunc.GetArticleId(ArticleFunc.article_39, "ВП");
        }
        /// <summary>
        /// Расчитывает кол-во работ
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWork(int applId)
        {
            string tableName;
            int[] stationId = ArticleUpdate.GetStationsIdFromAppl(applId, out tableName);
            if ((tableName != TableName) || (stationId.Length < 1))
                return IM.NullI;
            return 1;
        }
    }
}
