﻿using System.Collections.Generic;
using System.Text;
using ICSM;
using OrmCs;

namespace ArticleLib
{
    public class CountWork
    {
        /// <summary>
        /// Установить кол-во работ
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns></returns>
        public static bool SetWorkCount(int applId)
        {
            int wrkCount = CountWorkAppl(applId);
            if ((wrkCount > 0) && (wrkCount != IM.NullI))
            {
                IMRecordset r = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadWrite);
                r.Select("ID,WORKS_COUNT");
                r.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                try
                {
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        r.Edit();
                        r.Put("WORKS_COUNT", wrkCount);
                        r.Update();
                    }
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
                return true;
            }
            return false;
         }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static bool SetWorkCountExt(int applId)
        {
            int[] wrkCount = CountWorkApplExt(applId);
            if (wrkCount.Length > 0)
            {
                for (int i = 0; i < wrkCount.Length; i++)
                {
                    YXnrfaAppl appl_ = new YXnrfaAppl();
                    appl_.Table = "XNRFA_APPL";
                    appl_.Format("ID,WORKS_COUNT,WORKS_COUNT2");
                    if (appl_.Fetch(applId))
                    {
                        if (i == 0) { appl_.m_works_count = wrkCount[i]; appl_.Save(); }
                        if (i == 1) { appl_.m_works_count2 = wrkCount[i]; appl_.Save(); }
                        
                    }
                    appl_.Close();
                    appl_.Dispose();

                    /*
                    IMRecordset r = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadWrite);
                    r.Select("ID,WORKS_COUNT,WORKS_COUNT2");
                    r.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                    try
                    {
                        for (r.Open(); !r.IsEOF(); r.MoveNext())
                        {
                            r.Edit();
                            if (i == 0) r.Put("WORKS_COUNT", wrkCount[i]);
                            if (i == 1) r.Put("WORKS_COUNT2", wrkCount[i]);
                            r.Update();
                        }
                    }
                    finally
                    {
                        if (r.IsOpen())
                            r.Close();
                        r.Destroy();
                    }
                     */ 
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Подсчет работ по определенной статье
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="articleId">ID статьи</param>
        /// <returns>Кол-во работ (1 - по умолчания)</returns>
        public static int CountWorkByArticle(int applId, int stationId, int articleId)
        {
            string articleName = "";
            IMRecordset rsPrice = new IMRecordset("XNRFA_PRICE", IMRecordset.Mode.ReadOnly);
            rsPrice.Select("ID,ARTICLE");
            rsPrice.SetWhere("ID", IMRecordset.Operation.Eq, articleId);
            try
            {
                rsPrice.Open();
                if (!rsPrice.IsEOF())
                    articleName = rsPrice.GetS("ARTICLE");
            }
            finally
            {
                if (rsPrice.IsOpen())
                    rsPrice.Close();
                rsPrice.Destroy();
            }
            return CountWorkByArticle(applId, stationId, articleName);
        }
        /// <summary>
        /// Подсчет работ по определенной статье
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="article">статья</param>
        /// <returns>Кол-во работ (1 - по умолчания)</returns>
        public static int CountWorkByArticle(int applId, int stationId, string article)
        {
            string standard = GetStandradOnAppl(applId);
            int retVal = IM.NullI;
            switch (article)
            {
                case ArticleFunc.article_1_2_1:
                case ArticleFunc.article_1_2_2:
                case ArticleFunc.article_1_2_2_m:
                    retVal = CountWorkArticle_1_2(applId, standard);
                    break;
                case ArticleFunc.article_2_3_1:
                case ArticleFunc.article_2_3_2:
                    retVal = CountWorkArticle_2_3(applId, standard);
                    break;
                case ArticleFunc.article_21_1:
                    retVal = Ship.GetCountWork(applId);
                    break;
                case ArticleFunc.article_31:
                    retVal = Ship.GetCountWorkGroupEquip29(applId);
                    break;
                case ArticleFunc.article_21_2:
                    //тут должны быть только абоненты. Если нет - метод справится
                    retVal = Abonent.GetCountWork(applId, article); 
                    break;
                case ArticleFunc.article_22_1:
                //case ArticleFunc.article_22_1_1:
                //case ArticleFunc.article_22_1_2:
                //case ArticleFunc.article_22_1_3:
                //case ArticleFunc.article_22_1_4:
                //case ArticleFunc.article_22_1_5:
                case ArticleFunc.article_22_2_1:
                case ArticleFunc.article_22_2_2:
                case ArticleFunc.article_23_1:
                case ArticleFunc.article_23_2:
                case ArticleFunc.article_37:
                case ArticleFunc.article_38_1:
                case ArticleFunc.article_38_2:
                case ArticleFunc.article_22_3:
                     //retVal = Abonent.GetCountWork(applId); 
                    if (GetNameTable(applId) != MobStation.TableName) { retVal = (stationId == IM.NullI ? Abonent.GetCountWork(applId, article) : Abonent.GetCountWorkExt(applId,stationId)); }
                    if (GetNameTable(applId) == MobStation.TableName) { retVal = MobStation.GetCountWork(applId, article); }
                    break;
                case ArticleFunc.article_39:
                    retVal = Emitter.GetCountWork(applId);
                    break;
                case ArticleFunc.article_32:
                    retVal = MobStation2.GetCountWork(applId, article);
                    break;
            }
            return retVal;
        }
        /// <summary>
        /// Подсчет работ пол заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Кол-во работ (0 - по умолчания)</returns>
        public static int CountWorkAppl(int applId)
        {
            string article = "";
            string tablename = "";
            //Выбераем статью заявки
            IMRecordset rsPacket = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsPacket.Select("ID,Price.ARTICLE,OBJ_TABLE");
            rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsPacket.Open();
                if (!rsPacket.IsEOF())
                {
                    article = rsPacket.GetS("Price.ARTICLE");
                    tablename = rsPacket.GetS("OBJ_TABLE");
                }
            }
            finally
            {
                if (rsPacket.IsOpen())
                    rsPacket.Close();
                rsPacket.Destroy();
            }

            if (string.IsNullOrEmpty(article))
                return IM.NullI;
            return CountWorkByArticle(applId, IM.NullI, article);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static int[] CountWorkApplExt(int applId)
        {
            List<int> Cnt = new List<int>();
            string article = "";
            string article2 = "";
            string tablename = "";
            int[] Al_ = ArticleLib.ArticleUpdate.GetStationsIdFromAppl(applId);
            //Выбераем статью заявки
            IMRecordset rsPacket = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsPacket.Select("ID,Price.ARTICLE,Price2.ARTICLE,OBJ_TABLE");
            rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsPacket.Open();
                if (!rsPacket.IsEOF())
                {
                    article = rsPacket.GetS("Price.ARTICLE");
                    article2 = rsPacket.GetS("Price2.ARTICLE");
                    tablename = rsPacket.GetS("OBJ_TABLE");
                }
            }
            finally
            {
                if (rsPacket.IsOpen())
                    rsPacket.Close();
                rsPacket.Destroy();
            }

            if (Al_.Length == 1)
            {
                if (string.IsNullOrEmpty(article))
                    Cnt.Add(IM.NullI);
                else Cnt.Add(CountWorkByArticle(applId, IM.NullI, article));


                if (tablename == "SHIP") {
                    if (string.IsNullOrEmpty(article2))
                        Cnt.Add(IM.NullI);
                    else
                        Cnt.Add(CountWorkByArticle(applId, IM.NullI, article2));
                }
               
            }

            else if (Al_.Length == 2)
            {
                if (string.IsNullOrEmpty(article))
                    Cnt.Add(IM.NullI);
                else Cnt.Add(CountWorkByArticle(applId, Al_[0], article));

                if (string.IsNullOrEmpty(article2))
                    Cnt.Add(IM.NullI);
                else Cnt.Add(CountWorkByArticle(applId, Al_[1], article2));
            }

            
             return Cnt.ToArray();
        }

        /// <summary>
        /// Получить значение стандарта по ID
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static string GetStandradOnAppl(int applId)
        {
            string standard = "";
            IMRecordset rsPacket = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsPacket.Select("ID,STANDARD");
            rsPacket.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsPacket.Open();
                if (!rsPacket.IsEOF())
                {
                    standard = rsPacket.GetS("STANDARD");
                }
            }
            finally
            {
                if (rsPacket.IsOpen())
                    rsPacket.Close();
                rsPacket.Destroy();
            }

            if (string.IsNullOrEmpty(standard))
                return "";
            return standard;
        }

        /// <summary>
        /// Кол-во работ по статье "1.2"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Кол-во работ (1 - по умолчания)</returns>
        private static int CountWorkArticle_1_2(int applId, string standard)
        {
            return CountWorkArticle_2_3(applId, standard);
        }
        /// <summary>
        /// Кол-во работ по статье "2.3"
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>Кол-во работ (1 - по умолчания)</returns>
        private static int CountWorkArticle_2_3(int applId, string standard)
        {
            int retVal = 1;
            int[] objIdList;

            //string standard = GetStandradOnAppl(applId);
            //int[] objIdList = ArticleUpdate.GetStationsIdFromAppl(applId);
            if (standard == "UMTS")
            {
                //objIdList = ArticleUpdate.GetStationsIdFromApplWithStandard(applId, standard);
                objIdList = ArticleUpdate.GetStationsIdFromAppl(applId);
            }
            else
            {
                //objIdList = ArticleUpdate.GetStationsIdFromAppl(applId);
                //objIdList = ArticleUpdate.GetStationsIdFromApplWithStandard(applId, standard);
                objIdList = ArticleUpdate.GetStationsIdFromAppl(applId);
            }
        

            if (objIdList.Length == 0)
                return retVal;
            retVal = 0;
            {
                StringBuilder sql = new StringBuilder(100);
                foreach (int id in objIdList)
                {
                    if (sql.Length > 0)
                        sql.Append(" OR ");
                    sql.Append(string.Format("(STA_ID = {0})", id));
                }
                //Выбераем частоты
                IMRecordset rsFreq = new IMRecordset("MOBSTA_FREQS", IMRecordset.Mode.ReadOnly);
                rsFreq.Select("ID,STA_ID");
                rsFreq.SetAdditional(string.Format("({0})", sql));
                try
                {
                    for (rsFreq.Open(); !rsFreq.IsEOF(); rsFreq.MoveNext())
                        retVal++;
                }
                finally
                {
                    if (rsFreq.IsOpen())
                        rsFreq.Close();
                    rsFreq.Destroy();
                }
            }

            if (retVal > 0)
                retVal *= 2;
            return retVal;
        }


        /// <summary>
        /// Получить имя таблицы по ID
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static string GetNameTable(int applId)
        {
            string tableName = "";
            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_TABLE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            rsAppl.Open();
            if (!rsAppl.IsEOF())
            {
                tableName = rsAppl.GetS("OBJ_TABLE");
            }
            if (rsAppl.IsOpen())
                rsAppl.Close();
            rsAppl.Destroy();
            return tableName;
        }
    }
}
