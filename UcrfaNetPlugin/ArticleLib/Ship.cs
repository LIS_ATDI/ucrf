﻿using ICSM;
using System.Collections.Generic;

namespace ArticleLib
{
    class Ship
    {
        public const string TableName = "SHIP";
        /// <summary>
        /// Возвращает ID статьи (для PRICE_ID)
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static int GetArticleId(int applId)
        {
            return ArticleFunc.GetArticleId(ArticleFunc.article_21_1, "РБСС");
        }

        public static int[] GetArticleIdAll()
        {
            List<int> Res = new List<int>();
            Res.Add(GetArticleId(IM.NullI));
            Res.Add(GetArticleIdGroupEquip29(IM.NullI));
            return Res.ToArray();
        }

        /// <summary>
        /// Возвращает ID статьи (для PRICE_ID)
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static int GetArticleIdGroupEquip29(int applId)
        {
            return ArticleFunc.GetArticleId(ArticleFunc.article_31 , "РБСС");
        }

        public static bool UpdateArticle(int applId)
        {

            int ArticleID_1=IM.NullI;
            int ArticleID_2 = IM.NullI;
            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsAs = new IMRecordset("SHIP", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        ArticleID_1 = Ship.GetArticleId(applId);
                        if ((Ship.GetCountWorkGroupEquip29(applId) > 0) && (Ship.GetCountWorkGroupEquip29(applId) != IM.NullI))
                        {
                            ArticleID_2 = Ship.GetArticleIdGroupEquip29(applId);
                            if (ArticleID_2 != IM.NullI)
                                ArticleFunc.SetArticle2(applId, ArticleID_2);
                        }
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }
            return ArticleFunc.SetArticle(applId, ArticleID_1);
        }

        /// <summary>
        /// Расчитывает кол-во работ в которые не включаются группы оборудования 24,25,26,27,28,29,30,32
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWork(int applId)
        {
            string tableName;
            int[] stationId = ArticleUpdate.GetStationsIdFromAppl(applId, out tableName);
            if ((tableName != "SHIP") || (stationId.Length < 1))
                return IM.NullI;

            int count = 0;
            IMRecordset rsPrice = new IMRecordset("SHIP_EQP", IMRecordset.Mode.ReadOnly);
            rsPrice.Select("ID,Equipment.CUST_TXT5,REMARK");
            rsPrice.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId[0]);
            //rsPrice.SetWhere("Equipment.CUST_TXT6", IMRecordset.Operation.Like, "Основні системи*");
            try
            {
                for (rsPrice.Open(); !rsPrice.IsEOF(); rsPrice.MoveNext())
                {

                    string remark = rsPrice.GetS("REMARK");
                    int quantity = 0;
                    int.TryParse(remark, out quantity);
                    
                    

                    bool isCheck = true;
                    string TXT5 = rsPrice.GetS("Equipment.CUST_TXT5");
                    switch (TXT5)
                    {
                        case "24":
                        case "25":
                        case "26":
                        case "27":
                        case "28":
                        case "29":
                        case "30":
                        case "32":
                        isCheck = false;
                        break;
                    }
                    if (isCheck)
                    {
                        if (quantity > 0) { count += quantity; } else { count++; }
                    }
                }
            }
            finally
            {
                if (rsPrice.IsOpen())
                    rsPrice.Close();
                rsPrice.Destroy();
            }
            if(count > 0)
                return count;
            return IM.NullI;
        }

        /// <summary>
        /// Расчитывает кол-во работ в которые включаются группа оборудования 29
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWorkGroupEquip29(int applId)
        {
            string tableName;
            int[] stationId = ArticleUpdate.GetStationsIdFromAppl(applId, out tableName);
            if ((tableName != "SHIP") || (stationId.Length < 1))
                return IM.NullI;

            int count = 0;
            IMRecordset rsPrice = new IMRecordset("SHIP_EQP", IMRecordset.Mode.ReadOnly);
            rsPrice.Select("ID,Equipment.CUST_TXT5,REMARK");
            rsPrice.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId[0]);
            try {
                for (rsPrice.Open(); !rsPrice.IsEOF(); rsPrice.MoveNext()) {
                    string remark = rsPrice.GetS("REMARK");
                    int quantity = 0;
                    int.TryParse(remark, out quantity);
                    bool isCheck = false;
                    string TXT5 = rsPrice.GetS("Equipment.CUST_TXT5");
                    switch (TXT5) {
                        case "29":
                            isCheck = true;
                            break;
                    }
                    if (isCheck) {
                        if (quantity > 0) { count += quantity; } else { count++; }
                    }
                }
            }
            finally {
                if (rsPrice.IsOpen())
                    rsPrice.Close();
                rsPrice.Destroy();
            }
            if (count > 0)
                return count;
            return IM.NullI;
        }
    }
}
