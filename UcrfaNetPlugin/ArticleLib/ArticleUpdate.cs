﻿using ICSM;
using System.Collections.Generic;


namespace ArticleLib
{
    /// <summary>
    /// Обновляет статьи в станциях
    /// </summary>
    public class ArticleUpdate
    {

        /// <summary>
        /// Вытаскивает статью заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static int[] GetArticle(int applId)
        {
            string tableName;
            int[] Res = GetStationsIdFromAppl(applId, out tableName);
            if ((Res.Length == 1) && (tableName!="SHIP")) return new int[] { GetArticle(applId, tableName) };
            else return GetArticle(Res, tableName);
        }

        public static int GetArticleSimple(int applId)
        {
            string tableName;
            GetStationsIdFromAppl(applId, out tableName);
            return GetArticle(applId, tableName);
        }


        /// <summary>
        /// Вытаскивает статью заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="stationTable">Название таблицы станции</param>
        /// <returns>ID статьи</returns>
        private static int GetArticle(int applId, string stationTable)
        {
            int retVal = IM.NullI;
            switch (stationTable)
            {
                case Ship.TableName:
                    retVal = Ship.GetArticleId(applId);
                    break;
                case Emitter.TableName:
                    retVal = Emitter.GetArticleId(applId);
                    break;
                case Abonent.TableName:
                    retVal = Abonent.GetArticleId(applId);
                    break;
                case EarthStation.TableName:
                    retVal = EarthStation.GetArticle(applId);
                    break;

            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <param name="stationTable"></param>
        /// <returns></returns>
        private static int[] GetArticle(int[] applIds, string stationTable)
        {
            List<int> retVal = new List<int>();
            switch (stationTable)
            {
                case Abonent.TableName:
                    retVal.AddRange((Abonent.GetArticleId(applIds)));
                    break;
                case Ship.TableName:
                    retVal.AddRange((Ship.GetArticleIdAll()));
                    break;
            }
            return retVal.ToArray();
        }
        /// <summary>
        /// Обновляет статью в заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>TRUE - были обновления статей. FALSE - обновлений небыло</returns>
        public static bool UpdateArticle(int applId)
        {
            string tableName = "";
            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_TABLE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            rsAppl.Open();
            if (!rsAppl.IsEOF())
            {
                tableName = rsAppl.GetS("OBJ_TABLE");
            }
            if (rsAppl.IsOpen())
                rsAppl.Close();
            rsAppl.Destroy();
            int[] StationIds = GetArticle(applId);
            return UpdateArticle(applId, StationIds, tableName);
        }
        /// <summary>
        /// Обновляет статью в заявке
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="stationTable">Название таблицы станции</param>
        /// <returns>TRUE - были обновления статей. FALSE - обновлений небыло</returns>
        public static bool UpdateArticle(int applId, int[] StationIds, string stationTable)
        {
            bool retVal = false;
            switch (stationTable)
            {
                case Abonent.TableName:
                    retVal = Abonent.UpdateArticle(applId, StationIds);
                    break;
                case EarthStation.TableName:
                    retVal = EarthStation.UpdateArticle(applId);
                    break;
                case MobStation2.TableName:
                    retVal = MobStation2.UpdateArticle(applId);
                    break;
                case MobStation.TableName:
                    retVal = MobStation.UpdateArticle(applId);
                    break;
                case Ship.TableName:
                    retVal = Ship.UpdateArticle(applId);
                    break;
                case Emitter.TableName:
                    retVal = ArticleFunc.SetArticle(applId,Emitter.GetArticleId(applId));
                    break;
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает список ID станций в заявку
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>список ID станций</returns>
        public static int[] GetStationsIdFromAppl(int applId)
        {
            string tableName;
            return GetStationsIdFromAppl(applId, out tableName);
        }
        /// <summary>
        /// Возвращает список ID станций в заявку
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="tableStation">Название таблицы станции</param>
        /// <returns>список ID станций</returns>
        public static int[] GetStationsIdFromAppl(int applId, out string tableStation)
        {
            tableStation = "";
            System.Collections.Generic.List<int> retVal = new System.Collections.Generic.List<int>();
            //Выбераем все станции из заявки
            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    for (int i = 0; i < 6; i++)
                    {
                        int id = rsAppl.GetI("OBJ_ID" + (i + 1));
                        if (id != IM.NullI)
                            retVal.Add(id);
                    }
                    tableStation = rsAppl.GetS("OBJ_TABLE" );
                }
            }
            finally
            {
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            return retVal.ToArray();
        }

        public static int[] GetStationsIdFromApplWithStandard(int applId, string Standard)
        {
            System.Collections.Generic.List<int> retVal = new System.Collections.Generic.List<int>();
            //Выбераем все станции из заявки
            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_ID1,OBJ_ID2,OBJ_ID3,OBJ_ID4,OBJ_ID5,OBJ_ID6,OBJ_TABLE,STANDARD");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            rsAppl.SetAdditional(string.Format("[STANDARD]='{0}'",Standard));
            try
            {
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    for (int i = 0; i < 6; i++)
                    {
                        int id = rsAppl.GetI("OBJ_ID" + (i + 1));
                        if (id != IM.NullI)
                            retVal.Add(id);
                    }
                }
            }
            finally
            {
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            return retVal.ToArray();
        }

        

    }


}