﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace ArticleLib
{
    public class Abonent
    {
        public const string TableName = "XFA_ABONENT_STATION";
        /// <summary>
        /// Возвращает ID статьи
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>ID статьи</returns>
        public static int GetArticleId(int applId)
        {
            double powerdBw = IM.NullD;
            double powerW = IM.NullD;
            string setArticle = "";
            string standard = "";
            int netId = IM.NullI;
            string explType = "";
            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD,PWR_ANT,NET_ID,EXPL_TYPE");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        powerdBw = rsAs.GetD("PWR_ANT");
                        if (powerdBw != IM.NullD)
                            powerW = IM.RoundDeci(Math.Pow(10.0, powerdBw / 10.0), 4);
                        standard = rsAs.GetS("STANDARD");
                        netId = rsAs.GetI("NET_ID");
                        explType = rsAs.GetS("EXPL_TYPE");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }

            switch (standard)
            {
                case "ЦУКХ":
                case "УКХ":
                    //> УКХ - 22
                    //> в зависимости от кол-во РЕЗ:
                    //> =< 5 22.1.1
                    //> > 5 <=15 22.1.2 
                    //> > 15 <=30 22.1.3
                    //> > 30 <=50 22.1.4
                    //> >50   22.1.5
                    //По УКХ еще всплыла статья 22.3 -(Отличия от 22.1)если у базовой станции нет лицензии и/или MOB_station.cust_chb=1
                    //в зависимости от мощности станции меньше 3Вт включно 22.3.1 22.3.2
                    {
                        int countBaseStationInNet = 0;
                        bool isBaseStationWithLicence = true;
                        {
                            HashSet<int> idBaseStationInNet = new HashSet<int>();
                            //Проверяем наличия БС в сети и наличие лицензий у БС
                            IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                            rsAs.Select("NetSector1.ID,NetSector1.BaseMobStation.CUST_CHB1,NetSector1.BaseMobStation2.CUST_CHB1");
                            rsAs.SetWhere("NetSector1.NET_ID", IMRecordset.Operation.Eq, netId);
                            rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                            try
                            {
                                for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                {
                                    int idBaseStation = rsAs.GetI("NetSector1.ID");
                                    if (idBaseStationInNet.Contains(idBaseStation) == false)
                                    {
                                        idBaseStationInNet.Add(idBaseStation);
                                        countBaseStationInNet++;
                                    }
                                    if ((isBaseStationWithLicence) &&
                                        ((rsAs.GetI("NetSector1.BaseMobStation.CUST_CHB1") == 1) ||
                                         (rsAs.GetI("NetSector1.BaseMobStation2.CUST_CHB1") == 1)))
                                    {
                                        isBaseStationWithLicence = false;
                                    }
                                }
                            }
                            finally
                            {
                                if (rsAs.IsOpen())
                                    rsAs.Close();
                                rsAs.Destroy();
                            }
                        }

                        if ((countBaseStationInNet == 0) || (isBaseStationWithLicence == false))
                        {
                            if (powerW != IM.NullD)
                            {
                                if (powerW <= 3.0)
                                    setArticle = ArticleFunc.article_22_2_1;
                                else
                                    setArticle = ArticleFunc.article_22_2_2;
                            }
                        }
                        //========
                        //Сетевая статья проставляеться в сети.
                        //========
                        //else
                        //{
                        //    Dictionary<int, List<double>> listFreqsTxOfStation = new Dictionary<int, List<double>>();
                        //    Dictionary<int, List<double>> listFreqsRxOfStation = new Dictionary<int, List<double>>();
                        //    //Загрузка частот АС
                        //    IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                        //    rsAs.Select("Abonent.ID,Abonent.Freqs.TX_FREQ,Abonent.Freqs.RX_FREQ");
                        //    rsAs.SetWhere("Abonent.NET_ID", IMRecordset.Operation.Eq, netId);
                        //    rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                        //    try
                        //    {
                        //        for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                        //        {
                        //            int idStation = rsAs.GetI("Abonent.ID");
                        //            if (listFreqsTxOfStation.ContainsKey(idStation) == false)
                        //                listFreqsTxOfStation.Add(idStation, new List<double>());
                        //            if (listFreqsRxOfStation.ContainsKey(idStation) == false)
                        //                listFreqsRxOfStation.Add(idStation, new List<double>());
                        //            listFreqsTxOfStation[idStation].Add(IM.RoundDeci(rsAs.GetD("Abonent.Freqs.TX_FREQ"), 6));
                        //            listFreqsRxOfStation[idStation].Add(IM.RoundDeci(rsAs.GetD("Abonent.Freqs.RX_FREQ"), 6));
                        //        }
                        //    }
                        //    finally
                        //    {
                        //        if (rsAs.IsOpen())
                        //            rsAs.Close();
                        //        rsAs.Destroy();
                        //    }
                        //    //Проверка частот
                        //    bool isCorrect = true;
                        //    //Проверка по кол-ву
                        //    int freqCount = IM.NullI;
                        //    foreach (KeyValuePair<int, List<double>> freq in listFreqsTxOfStation)
                        //    {
                        //        if (freqCount == IM.NullI)
                        //            freqCount = freq.Value.Count;
                        //        else if (freqCount != freq.Value.Count)
                        //        {
                        //            isCorrect = false;
                        //            break;
                        //        }
                        //    }
                        //    if(isCorrect == true)
                        //    {
                        //        freqCount = IM.NullI;
                        //        foreach (KeyValuePair<int, List<double>> freq in listFreqsRxOfStation)
                        //        {
                        //            if (freqCount == IM.NullI)
                        //                freqCount = freq.Value.Count;
                        //            else if (freqCount != freq.Value.Count)
                        //            {
                        //                isCorrect = false;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //    //Проверка по номиналам
                        //    List<double> lstFreq = null;
                        //    foreach (KeyValuePair<int, List<double>> freq in listFreqsTxOfStation)
                        //    {
                        //        if (isCorrect == false)
                        //            break;
                        //        if (lstFreq == null)
                        //            lstFreq = freq.Value;
                        //        else
                        //        {
                        //            foreach (double value in freq.Value)
                        //            {
                        //                if(lstFreq.Contains(value) == false)
                        //                {
                        //                    isCorrect = false;
                        //                    break;  
                        //                }
                        //            }
                        //        }
                        //    }
                        //    lstFreq = null;
                        //    foreach (KeyValuePair<int, List<double>> freq in listFreqsRxOfStation)
                        //    {
                        //        if (isCorrect == false)
                        //            break;
                        //        if (lstFreq == null)
                        //            lstFreq = freq.Value;
                        //        else
                        //        {
                        //            foreach (double value in freq.Value)
                        //            {
                        //                if (lstFreq.Contains(value) == false)
                        //                {
                        //                    isCorrect = false;
                        //                    break;
                        //                }
                        //            }
                        //        }
                        //    }
                        //    if(isCorrect == true)
                        //    {
                        //        int countStation = Math.Max(listFreqsTxOfStation.Count + countBaseStationInNet, listFreqsRxOfStation.Count + countBaseStationInNet);
                        //        if(countStation <= 5)
                        //            setArticle = ArticleFunc.article_22_1_1;
                        //        else if(countStation <= 15)
                        //            setArticle = ArticleFunc.article_22_1_2;
                        //        else if (countStation <= 30)
                        //            setArticle = ArticleFunc.article_22_1_3;
                        //        else if (countStation <= 50)
                        //            setArticle = ArticleFunc.article_22_1_4;
                        //        else
                        //            setArticle = ArticleFunc.article_22_1_5;
                        //    }
                        //}
                    }
                    break;
                case "РПАТЛ":
                    //> РПАТЛ - 37
                    if ((string.IsNullOrEmpty(explType) == false) && (explType == "0"))
                        setArticle = ArticleFunc.article_37;
                    break;
                case "КХ":
                    //> КХ - 23
                    //> в зависимости от мощности абонентской станции 
                    //> до 100Вт вкл 23.1
                    //> больше 100 23.2
                    if (powerW != IM.NullD)
                    {
                        if (powerW <= 100)
                            setArticle = ArticleFunc.article_23_1;
                        else
                            setArticle = ArticleFunc.article_23_2;
                    }
                    break;
                case "РРК":
                    //> РРК - 38.1
                    setArticle = ArticleFunc.article_38_1;
                    break;
                case "РОПС":
                    //> РОПС - 38.2
                    setArticle = ArticleFunc.article_38_2;
                    break;
                case "БАУР":
                case "БЦУР":
                    if (powerW != IM.NullD)
                    {
                        if (powerW <= 3.0)
                            setArticle = ArticleFunc.article_22_2_1;
                        else
                            setArticle = ArticleFunc.article_22_2_2;
                    }
                    break;
                case "РБСС":
                    setArticle = ArticleFunc.article_21_2;
                    break;
                default:
                    return IM.NullI;
            }
            return ArticleFunc.GetArticleId(setArticle, standard);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stationIds"></param>
        /// <returns></returns>
        public static List<int> GetArticleId(int[] stationIds)
        {
            List<int> Res = new List<int>();
            double powerdBw = IM.NullD;
            double powerW = IM.NullD;
            string setArticle = "";
            string standard = "";
            int netId = IM.NullI;
            string explType = "";

            foreach (int stationId in stationIds)
            {
                IMRecordset rsAs_ = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                rsAs_.Select("ID,STANDARD,PWR_ANT,NET_ID,EXPL_TYPE");
                rsAs_.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs_.Open();
                    if (!rsAs_.IsEOF())
                    {
                        powerdBw = rsAs_.GetD("PWR_ANT");
                        if (powerdBw != IM.NullD)
                            powerW = IM.RoundDeci(Math.Pow(10.0, powerdBw / 10.0), 4);
                        standard = rsAs_.GetS("STANDARD");
                        netId = rsAs_.GetI("NET_ID");
                        explType = rsAs_.GetS("EXPL_TYPE");
                    }
                }
                finally
                {
                    if (rsAs_.IsOpen())
                        rsAs_.Close();
                    rsAs_.Destroy();
                }


                switch (standard)
                {
                    case "ЦУКХ":
                    case "УКХ":
                        //> УКХ - 22
                        //> в зависимости от кол-во РЕЗ:
                        //> =< 5 22.1.1
                        //> > 5 <=15 22.1.2 
                        //> > 15 <=30 22.1.3
                        //> > 30 <=50 22.1.4
                        //> >50   22.1.5
                        //По УКХ еще всплыла статья 22.3 -(Отличия от 22.1)если у базовой станции нет лицензии и/или MOB_station.cust_chb=1
                        //в зависимости от мощности станции меньше 3Вт включно 22.3.1 22.3.2
                        {
                            int countBaseStationInNet = 0;
                            bool isBaseStationWithLicence = true;
                            {
                                HashSet<int> idBaseStationInNet = new HashSet<int>();
                                //Проверяем наличия БС в сети и наличие лицензий у БС
                                IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                                rsAs.Select("NetSector1.ID,NetSector1.BaseMobStation.CUST_CHB1,NetSector1.BaseMobStation2.CUST_CHB1");
                                rsAs.SetWhere("NetSector1.NET_ID", IMRecordset.Operation.Eq, netId);
                                rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                                try
                                {
                                    for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                    {
                                        int idBaseStation = rsAs.GetI("NetSector1.ID");
                                        if (idBaseStationInNet.Contains(idBaseStation) == false)
                                        {
                                            idBaseStationInNet.Add(idBaseStation);
                                            countBaseStationInNet++;
                                        }
                                        if ((isBaseStationWithLicence) &&
                                            ((rsAs.GetI("NetSector1.BaseMobStation.CUST_CHB1") == 1) ||
                                             (rsAs.GetI("NetSector1.BaseMobStation2.CUST_CHB1") == 1)))
                                        {
                                            isBaseStationWithLicence = false;
                                        }
                                    }
                                }
                                finally
                                {
                                    if (rsAs.IsOpen())
                                        rsAs.Close();
                                    rsAs.Destroy();
                                }
                            }

                            if ((countBaseStationInNet == 0) || (isBaseStationWithLicence == false))
                            {
                                if (powerW != IM.NullD)
                                {
                                    if (powerW <= 3.0)
                                        setArticle = ArticleFunc.article_22_2_1;
                                    else
                                        setArticle = ArticleFunc.article_22_2_2;
                                }
                            }

                        }
                        break;
                    case "РПАТЛ":
                        //> РПАТЛ - 37
                        if ((string.IsNullOrEmpty(explType) == false) && (explType == "0"))
                            setArticle = ArticleFunc.article_37;
                        break;
                    case "КХ":
                        //> КХ - 23
                        //> в зависимости от мощности абонентской станции 
                        //> до 100Вт вкл 23.1
                        //> больше 100 23.2
                        if (powerW != IM.NullD)
                        {
                            if (powerW <= 100)
                                setArticle = ArticleFunc.article_23_1;
                            else
                                setArticle = ArticleFunc.article_23_2;
                        }
                        break;
                    case "РРК":
                        //> РРК - 38.1
                        setArticle = ArticleFunc.article_38_1;
                        break;
                    case "РОПС":
                        //> РОПС - 38.2
                        setArticle = ArticleFunc.article_38_2;
                        break;
                    case "БАУР":
                    case "БЦУР":
                        if (powerW != IM.NullD)
                        {
                            if (powerW <= 3.0)
                                setArticle = ArticleFunc.article_22_2_1;
                            else
                                setArticle = ArticleFunc.article_22_2_2;
                        }
                        break;
                    case "РБСС":
                        setArticle = ArticleFunc.article_21_2;
                        break;
                    default:
                        return null;
                }
                Res.Add(ArticleFunc.GetArticleId(setArticle, standard));
            }
            return Res;
        }

        /// <summary>
        /// Проверка входит ли частота в список частот ниже,
        /// если входит, то ее надо исключить из учета (т.е. не учатвать при расчете count)
        /// </summary>
        /// <param name="freq"></param>
        /// <returns></returns>
        public static bool CheckRemoveFreq(double freq)
        {
            bool isContain = false;
            double[] List_Freq = new double[] {0.49,0.50,2.1745,2.1834,2.1875,2.6500,3.023,4.125,4.1775,4.2075,5.680,6.215,6.268,6.312,8.291,8.364,8.3765,8.4145,10.003,12.290,12.520,12.577,14.993,16.420,16.695,16.804,19.993,156.3,156.525,156.65,156.8,300.2};
            if (List_Freq.Contains(freq)) {
                isContain = true;
            }
            return isContain;
        }

        /// <summary>
        /// Обновляет статью у земной станции
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>TRUE - было обновление статьи. FALSE - обновлений небыло</returns>
        public static bool UpdateArticle(int applId, int[] StationIds)
        {
            return ArticleFunc.SetArticleExt(applId, (StationIds.Length == 1 ? new int[] { GetArticleId(applId) } : StationIds));
        }
        /// <summary>
        /// Расчитывает кол-во работ
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWork(int applId, string Article)
        {
            int stationId = IM.NullI;
            string tableName = "";
            string article = "";
            string standard = "";
            int netId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1,OBJ_TABLE,Price.ARTICLE");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                    tableName = rsAppl.GetS("OBJ_TABLE");
                    article = rsAppl.GetS("Price.ARTICLE");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            if ((tableName != "XFA_ABONENT_STATION") || (stationId == IM.NullI))
                return IM.NullI;

            int count = 0;
            {
                IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD,NET_ID");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        standard = rsAs.GetS("STANDARD");
                        netId = rsAs.GetI("NET_ID");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }

            if (article == "") article = Article;
            //----
            switch (article)
            {
                case ArticleFunc.article_21_2:
                case ArticleFunc.article_22_2_1:
                case ArticleFunc.article_22_2_2:
                case ArticleFunc.article_22_3_1:
                case ArticleFunc.article_22_3_2:
                    {
                        if ((standard == "БАУР") || (standard == "БЦУР"))
                            count = 1;
                        else
                        {
                            //Загрузка частот АС
                            HashSet<double> countUnicFreqs = new HashSet<double>();
                            IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                            rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                            rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                            try
                            {
                                for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                {
                                    if ((article==ArticleFunc.article_21_2) && (standard == "РБСС")) {
                                        if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))) ||
                                        (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))) {
                                            if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))))
                                                continue;
                                            else if (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))
                                                continue;
                                        }
                                    }

                                    double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                    double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                    if (countUnicFreqs.Contains(freqRx) == false)
                                        countUnicFreqs.Add(freqRx);
                                    if (countUnicFreqs.Contains(freqTx) == false)
                                        countUnicFreqs.Add(freqTx);
                                }
                            }
                            finally
                            {

                                if (rsAs.IsOpen())
                                    rsAs.Close();
                                rsAs.Destroy();
                            }
                            count = countUnicFreqs.Count;
                        }
                    }
                    break;
                //case ArticleFunc.article_22_1_1:
                //case ArticleFunc.article_22_1_2:
                //case ArticleFunc.article_22_1_3:
                //case ArticleFunc.article_22_1_4:
                //case ArticleFunc.article_22_1_5:
                case ArticleFunc.article_22_1:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Abonent.Freqs.TX_FREQ,Abonent.Freqs.RX_FREQ");
                        if (netId != IM.NullI)
                            rsAs.SetWhere("Abonent.NET_ID", IMRecordset.Operation.Eq, netId);
                        else
                            rsAs.SetWhere("Abonent.ID", IMRecordset.Operation.Eq, stationId);
                        rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_37:
                    count = 0;
                    break;
                case ArticleFunc.article_23_1:
                case ArticleFunc.article_23_2:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                        rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_38_1:
                case ArticleFunc.article_38_2:
                    count = 1;
                    break;
                default:
                    count = 0;
                    break;
            }
            if (count > 0)
                return count;
            return IM.NullI;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stationId"></param>
        /// <returns></returns>
        public static int GetCountWorkExt(int applId, int stationId)
        {
            string tableName = "";
            string article = "";
            string standard = "";
            int netId = IM.NullI;


            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_ID1,OBJ_TABLE,Price.ARTICLE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            rsAppl.Open();
            if (!rsAppl.IsEOF())
            {
                tableName = rsAppl.GetS("OBJ_TABLE");
                article = rsAppl.GetS("Price.ARTICLE");
            }
            if (rsAppl.IsOpen())
                rsAppl.Close();
            rsAppl.Destroy();


            if (((tableName != "XFA_ABONENT_STATION") && (tableName!="")) || (stationId == IM.NullI))
                return IM.NullI;

            int count = 0;
            {
                IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD,NET_ID");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        standard = rsAs.GetS("STANDARD");
                        netId = rsAs.GetI("NET_ID");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }
            //----
            switch (article)
            {
                case ArticleFunc.article_21_2:
                case ArticleFunc.article_22_2_1:
                case ArticleFunc.article_22_2_2:
                case ArticleFunc.article_22_3_1:
                case ArticleFunc.article_22_3_2:
                    {
                        if ((standard == "БАУР") || (standard == "БЦУР"))
                            count = 1;
                        else
                        {
                            //Загрузка частот АС
                            HashSet<double> countUnicFreqs = new HashSet<double>();
                            IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                            rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                            rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                            try
                            {
                                for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                {
                                    if ((article == ArticleFunc.article_21_2) && (standard == "РБСС")) {
                                        if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))) ||
                                        (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))) {
                                            if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))))
                                                continue;
                                            else if (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))
                                                continue;
                                        }
                                    }

                                    double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                    double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                    if (countUnicFreqs.Contains(freqRx) == false)
                                        countUnicFreqs.Add(freqRx);
                                    if (countUnicFreqs.Contains(freqTx) == false)
                                        countUnicFreqs.Add(freqTx);
                                }
                            }
                            finally
                            {
                                if (rsAs.IsOpen())
                                    rsAs.Close();
                                rsAs.Destroy();
                            }
                            count = countUnicFreqs.Count;
                        }
                    }
                    break;
                //case ArticleFunc.article_22_1_1:
                //case ArticleFunc.article_22_1_2:
                //case ArticleFunc.article_22_1_3:
                //case ArticleFunc.article_22_1_4:
                //case ArticleFunc.article_22_1_5:
                case ArticleFunc.article_22_1:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Abonent.Freqs.TX_FREQ,Abonent.Freqs.RX_FREQ");
                        if (netId != IM.NullI)
                            rsAs.SetWhere("Abonent.NET_ID", IMRecordset.Operation.Eq, netId);
                        else
                            rsAs.SetWhere("Abonent.ID", IMRecordset.Operation.Eq, stationId);
                        rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_37:
                    count = 0;
                    break;
                case ArticleFunc.article_23_1:
                case ArticleFunc.article_23_2:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                        rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_38_1:
                case ArticleFunc.article_38_2:
                    count = 1;
                    break;
                default:
                    count = 0;
                    break;
            }
            if (count > 0)
                return count;
            return IM.NullI;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <param name="stationId"></param>
        /// <param name="NumSector"></param>
        /// <returns></returns>
        public static int GetCountWorkExt_(int applId, int article_in, int NumSector)
        {
            string tableName = "";
            string article = "";
            string standard = "";
            int stationId= IM.NullI;
            int netId = IM.NullI;

            article = ArticleLib.ArticleFunc.GetNameArticle(article_in);

            IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
            rsAppl.Select("ID,OBJ_ID1,OBJ_ID2, OBJ_TABLE,Price.ARTICLE");
            rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
            rsAppl.Open();
            if (!rsAppl.IsEOF())
            {
                tableName = rsAppl.GetS("OBJ_TABLE");
                if (NumSector == 1)
                    stationId = rsAppl.GetI("OBJ_ID1");
                if (NumSector == 2)
                    stationId = rsAppl.GetI("OBJ_ID2");
            }
            if (rsAppl.IsOpen())
                rsAppl.Close();
            rsAppl.Destroy();

            if (((tableName != "XFA_ABONENT_STATION") && (tableName != "")) || (stationId == IM.NullI))
                return IM.NullI;

            int count = 0;
            {
                IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD,NET_ID");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        standard = rsAs.GetS("STANDARD");
                        netId = rsAs.GetI("NET_ID");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }
            //----
            switch (article)
            {
                case ArticleFunc.article_21_2:
                case ArticleFunc.article_22_2_1:
                case ArticleFunc.article_22_2_2:
                case ArticleFunc.article_22_3_1:
                case ArticleFunc.article_22_3_2:
                    {
                        if ((standard == "БАУР") || (standard == "БЦУР"))
                            count = 1;
                        else
                        {
                            //Загрузка частот АС
                            HashSet<double> countUnicFreqs = new HashSet<double>();
                            IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                            rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                            rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                            try
                            {
                                for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                                {
                                    if ((article == ArticleFunc.article_21_2) && (standard == "РБСС")) {
                                        if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))) ||
                                        (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))) {
                                            if ((CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6))))
                                                continue;
                                            else if (CheckRemoveFreq(IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6)))
                                                continue;
                                        }
                                    }

                                    double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                    double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                    if (countUnicFreqs.Contains(freqRx) == false)
                                        countUnicFreqs.Add(freqRx);
                                    if (countUnicFreqs.Contains(freqTx) == false)
                                        countUnicFreqs.Add(freqTx);
                                }
                            }
                            finally
                            {
                                if (rsAs.IsOpen())
                                    rsAs.Close();
                                rsAs.Destroy();
                            }
                            count = countUnicFreqs.Count;
                        }
                    }
                    break;
                //case ArticleFunc.article_22_1_1:
                //case ArticleFunc.article_22_1_2:
                //case ArticleFunc.article_22_1_3:
                //case ArticleFunc.article_22_1_4:
                //case ArticleFunc.article_22_1_5:
                case ArticleFunc.article_22_1:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Abonent.Freqs.TX_FREQ,Abonent.Freqs.RX_FREQ");
                        if (netId != IM.NullI)
                            rsAs.SetWhere("Abonent.NET_ID", IMRecordset.Operation.Eq, netId);
                        else
                            rsAs.SetWhere("Abonent.ID", IMRecordset.Operation.Eq, stationId);
                        rsAs.SetWhere("WAS_USED", IMRecordset.Operation.Eq, 1);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Abonent.Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_37:
                    count = 0;
                    break;
                case ArticleFunc.article_23_1:
                case ArticleFunc.article_23_2:
                    {
                        //Загрузка частот АС
                        HashSet<double> countUnicFreqs = new HashSet<double>();
                        IMRecordset rsAs = new IMRecordset("XFA_ABONENT_STATION", IMRecordset.Mode.ReadOnly);
                        rsAs.Select("ID,Freqs.TX_FREQ,Freqs.RX_FREQ");
                        rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                        try
                        {
                            for (rsAs.Open(); !rsAs.IsEOF(); rsAs.MoveNext())
                            {
                                double freqTx = IM.RoundDeci(rsAs.GetD("Freqs.TX_FREQ"), 6);
                                double freqRx = IM.RoundDeci(rsAs.GetD("Freqs.RX_FREQ"), 6);
                                if (countUnicFreqs.Contains(freqRx) == false)
                                    countUnicFreqs.Add(freqRx);
                                if (countUnicFreqs.Contains(freqTx) == false)
                                    countUnicFreqs.Add(freqTx);
                            }
                        }
                        finally
                        {
                            if (rsAs.IsOpen())
                                rsAs.Close();
                            rsAs.Destroy();
                        }
                        count = countUnicFreqs.Count;
                    }
                    break;
                case ArticleFunc.article_38_1:
                case ArticleFunc.article_38_2:
                    count = 1;
                    break;
                default:
                    count = 0;
                    break;
            }
            if (count > 0)
                return count;
            return IM.NullI;
        }

      
    }
}
