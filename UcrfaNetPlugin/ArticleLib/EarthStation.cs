﻿using ICSM;

namespace ArticleLib
{
    class EarthStation
    {
        public const string TableName = "EARTH_STATION";
        /// <summary>
        /// Обновляет статью у земной станции
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>TRUE - было обновление статьи. FALSE - обновлений небыло</returns>
        public static bool UpdateArticle(int applId)
        {
            double diametr = IM.NullD;
            double power = IM.NullD;
            string standard = "";

            string satName = "";
            int ownerId = IM.NullI;

            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsEs = new IMRecordset("ESTA_GROUP", IMRecordset.Mode.ReadOnly);
                rsEs.Select("ID,PWR_MAX,Antenna.DIAMETER,Antenna.Station.STANDARD");
                rsEs.Select("Antenna.GAIN");
                rsEs.SetWhere("Antenna.Station.ID", IMRecordset.Operation.Eq, stationId);
                rsEs.SetWhere("Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");
                
                try
                {
                    rsEs.Open();
                    if (!rsEs.IsEOF())
                    {
                        double gain = rsEs.GetD("Antenna.GAIN"); //dB
                        if (gain == IM.NullD)
                            return false;
                        power = rsEs.GetD("PWR_MAX"); //dBW
                        power += gain;
                        diametr = rsEs.GetD("Antenna.DIAMETER"); //m
                        standard = rsEs.GetS("Antenna.Station.STANDARD");
                    }
                }
                finally
                {
                    if (rsEs.IsOpen())
                        rsEs.Close();
                    rsEs.Destroy();
                }
            }

            {                
                IMRecordset rsEs = new IMRecordset("EARTH_STATION", IMRecordset.Mode.ReadOnly);
                rsEs.Select("ID,SAT_NAME,OWNER_ID");
                rsEs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsEs.Open();
                    if (!rsEs.IsEOF())
                    {            
                        ownerId = rsEs.GetI("OWNER_ID");
                        satName = rsEs.GetS("SAT_NAME");
                    }
                }
                finally
                {
                    if (rsEs.IsOpen())
                        rsEs.Close();
                    rsEs.Destroy();
                }
            }

            string setArticle = "";//ArticleFunc.article_34_1;

            if (((power != IM.NullD) && (power > 50)) || ((diametr != IM.NullD) && (diametr > 2)))
                setArticle = ArticleFunc.article_34_2;
            else
            {
                string formatSql = "";
                formatSql += "SELECT COUNT(*)";
                formatSql += " FROM %{0} ES, %{1} AP";
                formatSql += " WHERE ES.SAT_NAME LIKE '{2}' AND ES.OWNER_ID = {3}";
                formatSql += " AND ES.ID = AP.OBJ_ID1";
                formatSql += " AND AP.OBJ_TABLE LIKE '{0}'";
                formatSql += " AND AP.WAS_USED = 1";
                string selSql = string.Format(formatSql, "EARTH_STATION", "XNRFA_APPL", satName, ownerId);
                int cnt = 0;
                if (IM.ExecuteScalar(ref cnt, selSql))
                {
                    if (cnt != 0)
                    {
                        if (cnt <= 30)
                            setArticle = ArticleFunc.article_34_1_1;
                        else if (cnt <= 300)
                            setArticle = ArticleFunc.article_34_1_2;
                        else if (cnt <= 3000)
                            setArticle = ArticleFunc.article_34_1_3;
                        else
                            setArticle = ArticleFunc.article_34_1_4;
                    }
                }                                                       
            }
            return ArticleFunc.SetArticle(applId, ArticleFunc.GetArticleId(setArticle, standard));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <returns></returns>
        public static int GetArticle(int applId)
        {
            double diametr = IM.NullD;
            double power = IM.NullD;
            string standard = "";

            string satName = "";
            int ownerId = IM.NullI;
          
            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsEs = new IMRecordset("ESTA_GROUP", IMRecordset.Mode.ReadOnly);
                rsEs.Select("ID,PWR_MAX,Antenna.DIAMETER,Antenna.Station.STANDARD");
                rsEs.Select("Antenna.GAIN");
                rsEs.SetWhere("Antenna.Station.ID", IMRecordset.Operation.Eq, stationId);
                rsEs.SetWhere("Antenna.EMI_RCP", IMRecordset.Operation.Like, "E");

                try
                {
                    rsEs.Open();
                    if (!rsEs.IsEOF())
                    {
                        double gain = rsEs.GetD("Antenna.GAIN"); //dB
                        if (gain != IM.NullD)
                        {
                            power = rsEs.GetD("PWR_MAX"); //dBW
                            power += gain;
                            diametr = rsEs.GetD("Antenna.DIAMETER"); //m
                            standard = rsEs.GetS("Antenna.Station.STANDARD");
                        }
                    }
                }
                finally
                {
                    if (rsEs.IsOpen())
                        rsEs.Close();
                    rsEs.Destroy();
                }
            }

            {
                IMRecordset rsEs = new IMRecordset("EARTH_STATION", IMRecordset.Mode.ReadOnly);
                rsEs.Select("ID,SAT_NAME,OWNER_ID");
                rsEs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsEs.Open();
                    if (!rsEs.IsEOF())
                    {
                        ownerId = rsEs.GetI("OWNER_ID");
                        satName = rsEs.GetS("SAT_NAME");
                    }
                }
                finally
                {
                    if (rsEs.IsOpen())
                        rsEs.Close();
                    rsEs.Destroy();
                }
            }

            string setArticle = "";//ArticleFunc.article_34_1;

            if (((power != IM.NullD) && (power > 50)) || ((diametr != IM.NullD) && (diametr > 2)))
                setArticle = ArticleFunc.article_34_2;
            else
            {
                string formatSql = "";
                formatSql += "SELECT COUNT(*)";
                formatSql += " FROM %{0} ES, %{1} AP";
                formatSql += " WHERE ES.SAT_NAME LIKE '{2}' AND ES.OWNER_ID = {3}";
                formatSql += " AND ES.ID = AP.OBJ_ID1";
                formatSql += " AND AP.OBJ_TABLE LIKE '{0}'";
                formatSql += " AND AP.WAS_USED = 1";
                string selSql = string.Format(formatSql, "EARTH_STATION", "XNRFA_APPL", satName, ownerId);
                int cnt = 0;
                if (IM.ExecuteScalar(ref cnt, selSql))
                {
                    if (cnt != 0)
                    {
                        if (cnt <= 30)
                            setArticle = ArticleFunc.article_34_1_1;
                        else if (cnt <= 300)
                            setArticle = ArticleFunc.article_34_1_2;
                        else if (cnt <= 3000)
                            setArticle = ArticleFunc.article_34_1_3;
                        else
                            setArticle = ArticleFunc.article_34_1_4;
                    }
                }
            }
            return ArticleFunc.GetArticleId(setArticle, standard);
        }
    }
}
