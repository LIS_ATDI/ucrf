﻿using ICSM;

namespace ArticleLib
{
    class MobStation2
    {
        public const string TableName = "MOB_STATION2";
        /// <summary>
        /// Обновляет статью
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>TRUE - было обновление статьи. FALSE - обновлений небыло</returns>
        public static bool UpdateArticle(int applId)
        {
            string setArticle = "";
            string standard = "";
            int stationId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, TableName);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            {
                IMRecordset rsAs = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
                rsAs.Select("ID,STANDARD");
                rsAs.SetWhere("ID", IMRecordset.Operation.Eq, stationId);
                try
                {
                    rsAs.Open();
                    if (!rsAs.IsEOF())
                    {
                        standard = rsAs.GetS("STANDARD");
                    }
                }
                finally
                {
                    if (rsAs.IsOpen())
                        rsAs.Close();
                    rsAs.Destroy();
                }
            }

            switch (standard)
            {
                case "БНТ":
                    setArticle = ArticleFunc.article_32;
                    break;
                default:
                    return false;
            }
            return ArticleFunc.SetArticle(applId, ArticleFunc.GetArticleId(setArticle, standard));
        }
        /// <summary>
        /// Расчитывает кол-во работ
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <returns>кол-во работ</returns>
        public static int GetCountWork(int applId, string Article)
        {
            int stationId = IM.NullI;
            string article = "";
            int netId = IM.NullI;
            {
                IMRecordset rsAppl = new IMRecordset(ArticleFunc.ApplTableName, IMRecordset.Mode.ReadOnly);
                rsAppl.Select("ID,OBJ_ID1,OBJ_TABLE,Price.ARTICLE");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, TableName);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    stationId = rsAppl.GetI("OBJ_ID1");
                    article = rsAppl.GetS("Price.ARTICLE");
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            if (stationId == IM.NullI)
                return IM.NullI;

            int count = IM.NullI;
            //----
            if (article == "") article = Article;

            switch (article)
            {
                case ArticleFunc.article_32:
                    count = 1;
                    break;
            }
            return count;
        }
    }
}
