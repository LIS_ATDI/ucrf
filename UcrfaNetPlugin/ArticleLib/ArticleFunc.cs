﻿using ICSM;
using System;
using OrmCs;

namespace ArticleLib
{
    public class ArticleFunc
    {
        public const string ApplTableName = "XNRFA_APPL";
        public const string article_6_3 = "6.3";
        public const string article_6_4 = "6.4";
        public const string article_7_2 = "7.2";
        public const string article_1_2_x = "1.2";
        public const string article_1_2_1 = "1.2.1";
        public const string article_1_2_2 = "1.2.2";
        public const string article_1_2_2_m = "1.2.2м";

        public const string article_2_3_x = "2.3";
        public const string article_2_3_1 = "2.3.1";
        public const string article_2_3_2 = "2.3.2";

        public const string article_1_2m = "1.2m";
        public const string article_2_3m = "2.3m";

        public const string article_21_1 = "21.1";
        public const string article_21_2 = "21.2";

        public const string article_22_1 = "22.1";
        //public const string article_22_1_1 = "22.1.1";
        //public const string article_22_1_2 = "22.1.2";
        //public const string article_22_1_3 = "22.1.3";
        //public const string article_22_1_4 = "22.1.4";
        //public const string article_22_1_5 = "22.1.5";
        public const string article_22_2_1 = "22.2.1";
        public const string article_22_2_2 = "22.2.2";

        public const string article_22_3_1 = "22.3.1";
        public const string article_22_3_2 = "22.3.2";


        public const string article_22_3 = "22.3";

        public const string article_23_1 = "23.1";
        public const string article_23_2 = "23.2";
        public const string article_31 = "31";
        public const string article_32 = "32";
        //public const string article_34_1 = "34.1";
        public const string article_34_1_1 = "34.1.1";
        public const string article_34_1_2 = "34.1.2";
        public const string article_34_1_3 = "34.1.3";
        public const string article_34_1_4 = "34.1.4";        
        public const string article_34_2 = "34.2";
        public const string article_37 = "37";
        public const string article_38_1 = "38.1";
        public const string article_38_2 = "38.2";
        public const string article_39 = "39";
        public const string article_19 = "19";

        /// <summary>
        /// Устанавливает поле 
        /// </summary>
        /// <param name="applId">ID заявки</param>
        /// <param name="priceId">ID статьи</param>
        /// <returns>TRUE - было обновление статьи. FALSE - обновлений небыло</returns>
        /// 
        public static bool SetArticle(int applId, int priceId)
        {
            bool retVal = false;
            if (priceId != IM.NullI)
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadWrite);
                rsAppl.Select("ID,PRICE_ID");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("PRICE_ID", priceId);
                    rsAppl.Update();
                    retVal = true;
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            return retVal;
        }

        public static bool SetArticle2(int applId, int priceId)
        {
            bool retVal = false;
            if (priceId != IM.NullI)
            {
                IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadWrite);
                rsAppl.Select("ID,PRICE_ID2");
                rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                rsAppl.Open();
                if (!rsAppl.IsEOF())
                {
                    rsAppl.Edit();
                    rsAppl.Put("PRICE_ID2", priceId);
                    rsAppl.Update();
                    retVal = true;
                }
                if (rsAppl.IsOpen())
                    rsAppl.Close();
                rsAppl.Destroy();
            }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applId"></param>
        /// <param name="priceIds"></param>
        /// <returns></returns>
        public static bool SetArticleExt(int applId, int[] priceIds)
        {
            bool retVal = false;
            if (priceIds!=null)
            {
                for (int p=0; p< priceIds.Length; p++)
                {
                    if (priceIds[p] != IM.NullI)
                    {

                        YXnrfaAppl appl_ = new YXnrfaAppl();
                        appl_.Table = "XNRFA_APPL";
                        appl_.Format("ID,PRICE_ID,PRICE_ID2");
                        if (appl_.Fetch(applId))
                        {
                            if (p == 0) appl_.m_price_id = priceIds[p];
                            if (p == 1) appl_.m_price_id2 = priceIds[p];
                            appl_.Save();
                            retVal = true;
                        }
                        appl_.Close();
                        appl_.Dispose();
                        /*
                        IMRecordset rsAppl = new IMRecordset("XNRFA_APPL", IMRecordset.Mode.ReadWrite);
                        rsAppl.Select("ID,PRICE_ID,PRICE_ID2");
                        rsAppl.SetWhere("ID", IMRecordset.Operation.Eq, applId);
                        rsAppl.Open();
                        if (!rsAppl.IsEOF())
                        {
                            rsAppl.Edit();
                            if (p == 0) rsAppl.Put("PRICE_ID", priceIds[p]);
                            if (p == 1) rsAppl.Put("PRICE_ID2", priceIds[p]);
                            rsAppl.Update();
                            retVal = true;
                        }
                        if (rsAppl.IsOpen())
                            rsAppl.Close();
                        rsAppl.Destroy();
                         */ 
                    }
                }
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает ID статьи
        /// </summary>
        /// <param name="codeArticle">Строковое значение статьи</param>
        /// <param name="standard">Стандарт технологии</param>
        /// <returns>ID статьи</returns>
        public static int GetArticleId(string codeArticle, string standard)
        {
            if (string.IsNullOrEmpty(codeArticle))
                return IM.NullI;
            int priceId = IM.NullI;
            IMRecordset rsPrice = new IMRecordset("XNRFA_PRICE", IMRecordset.Mode.ReadOnly);
            rsPrice.Select("ID,STATUS,DATEOUTACTION,DATESTARTACTION");
            rsPrice.SetWhere("ARTICLE", IMRecordset.Operation.Like, codeArticle);
            rsPrice.SetWhere("STANDARD", IMRecordset.Operation.Like, standard);
            rsPrice.SetWhere("STATUS", IMRecordset.Operation.Eq,1);
            rsPrice.SetWhere("DATEOUTACTION", IMRecordset.Operation.Gt, DateTime.Now);
            rsPrice.SetWhere("DATESTARTACTION", IMRecordset.Operation.Lt, DateTime.Now);
            try
            {
                rsPrice.Open();
                if (!rsPrice.IsEOF())
                    priceId = rsPrice.GetI("ID");
            }
            finally
            {
                if (rsPrice.IsOpen())
                    rsPrice.Close();
                rsPrice.Destroy();
            }
            return priceId;
        }
        /// <summary>
        /// Возвращает название радиотехнологии по ID статьи
        /// </summary>
        /// <param name="codeArticle">Строковое значение статьи</param>
        /// <param name="standard">Стандарт технологии</param>
        /// <returns>ID статьи</returns>
        public static string GetStandtartForCodePriceID(int netPriceId)
        {
            string RadioTech = "";
            if (netPriceId != IM.NullI)
            {

                int priceId = IM.NullI;
                IMRecordset rsPrice = new IMRecordset("XNRFA_PRICE", IMRecordset.Mode.ReadOnly);
                rsPrice.Select("ID,ARTICLE,STANDARD");
                rsPrice.SetWhere("ID", IMRecordset.Operation.Eq, netPriceId);
                try
                {
                    rsPrice.Open();
                    if (!rsPrice.IsEOF())
                        RadioTech = rsPrice.GetS("STANDARD");
                }
                finally
                {
                    if (rsPrice.IsOpen())
                        rsPrice.Close();
                    rsPrice.Destroy();
                }
            }
            return RadioTech;
        }
        
        /// <summary>
        /// Получить наименование статьи по номеру
        /// </summary>
        /// <param name="ArticleID"></param>
        /// <returns></returns>
        public static string GetNameArticle(int ArticleID)
        {
            string Article = "";
            if (ArticleID != IM.NullI)
            {

                int priceId = IM.NullI;
                IMRecordset rsPrice = new IMRecordset("XNRFA_PRICE", IMRecordset.Mode.ReadOnly);
                rsPrice.Select("ID,ARTICLE,STANDARD");
                rsPrice.SetWhere("ID", IMRecordset.Operation.Eq, ArticleID);
                try
                {
                    rsPrice.Open();
                    if (!rsPrice.IsEOF())
                        Article = rsPrice.GetS("ARTICLE");
                }
                finally
                {
                    if (rsPrice.IsOpen())
                        rsPrice.Close();
                    rsPrice.Destroy();
                }
            }
            return Article;
        }

        /// <summary>
        /// Получить признак активности статьи
        /// </summary>
        /// <param name="netPriceId"></param>
        /// <returns></returns>
        public static bool GetStatusForCodePriceID(int netPriceId)
        {
            bool isActive = false;
            if (netPriceId != IM.NullI)
            {

                int priceId = IM.NullI;
                IMRecordset rsPrice = new IMRecordset("XNRFA_PRICE", IMRecordset.Mode.ReadOnly);
                rsPrice.Select("ID,ARTICLE,STATUS,DATEOUTACTION,DATESTARTACTION");
                rsPrice.SetWhere("ID", IMRecordset.Operation.Eq, netPriceId);
                try
                {
                    rsPrice.Open();
                    if (!rsPrice.IsEOF())
                    {
                      if ((rsPrice.GetI("STATUS")==1) && (rsPrice.GetT("DATEOUTACTION") > DateTime.Now) &&  (rsPrice.GetT("DATESTARTACTION") < DateTime.Now))
                      {
                          isActive = true;
                      }
                    }
                        
                }
                finally
                {
                    if (rsPrice.IsOpen())
                        rsPrice.Close();
                    rsPrice.Destroy();
                }
            }
            return isActive;
        }
    }
}
