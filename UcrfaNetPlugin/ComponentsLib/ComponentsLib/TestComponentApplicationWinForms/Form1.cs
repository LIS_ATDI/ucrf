﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using ComponentsLib;

namespace TestComponentApplicationWinForms
{
   public partial class Form1 : Form
   {
      public List<TreeColumnRow> TreeRow { get; set; }
      private TreeColumnCtrl TreeColumn;

      public Form1()
      {
         InitializeComponent();
         TreeColumn = new TreeColumnCtrl();
         elementHost1.Child = TreeColumn;
         InitTreeColumn();
      }
      //===================================================
      /// <summary>
      /// Инициализация TreeColumnCtrl
      /// </summary>
      public void InitTreeColumn()
      {
         TreeRow = new List<TreeColumnRow>();
         //----
         TreeColumnRow mainRow = new TreeColumnRow();
         TreeColumnRow subRow = null;
         TreeColumnRow subSubRow = null;
         {
            subRow = new TreeColumnRow();
            for (int i = 0; i < 4; i++)
            {
               subSubRow = new TreeColumnRow();
               subSubRow.GridTestProperty1GridBind.Value = string.Format("sub1 {0}", i);
               subSubRow.GridTestProperty2GridBind.Value = string.Format("sub2 {0}", i);
               subRow.Children.Add(subSubRow);
            }
         }
         mainRow.Children.Add(subRow);
         mainRow.Children.Add(subRow);
         mainRow.Children.Add(subRow);
         TreeRow.Add(mainRow);
         //----
         //TreeColumn.AddTextColumn("Header1", "GridTestProperty1");
         //TreeColumn.AddTextColumn("Header1_1", "GridTestProperty1");
         //TreeColumn.AddTextColumn("Header2", "GridTestProperty2");
         //TreeColumn.AddTextColumn("Header2_2", "GridTestProperty2");
         //----
         TreeColumn.ItemsSource = TreeRow;
         TreeColumn.LoadColumnFromFile();
      }
   }

   public class TreeColumnRow : TreeColumnRowBase
   {
      /// <summary>
      /// Comment 1
      /// </summary>
      [TreeColumn("Comment1")]
      public StringElemCell GridTestProperty1GridBind { get; set; }
      /// <summary>
      /// Comment 2
      /// </summary>
      [TreeColumn("Comment2")]
      public StringElemCell GridTestProperty2GridBind { get; set; }
      [TreeColumn("Comment3")]
      public StringElemCell GridTestProperty3GridBind { get; set; }
      //===============================================
      public TreeColumnRow()
         : base()
      {
         GridTestProperty1GridBind = new StringElemCell();
         GridTestProperty2GridBind = new StringElemCell();
         GridTestProperty3GridBind = new StringElemCell();
      }
   }
}
