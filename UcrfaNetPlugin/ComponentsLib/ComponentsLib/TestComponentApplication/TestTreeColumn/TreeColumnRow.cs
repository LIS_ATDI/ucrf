﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComponentsLib;

namespace TestComponentApplication
{
   public class TreeColumnRow : TreeColumnRowBase
   {
      /// <summary>
      /// Comment 1
      /// </summary>
      [TreeColumn("GridTestProperty1GridBind1221")]
      public StringElemCell GridTestProperty1GridBind { get; set; }
      /// <summary>
      /// Comment 2
      /// </summary>
      [TreeColumn("GridTestProperty2GridBind1212")]
      public IntElemCell GridTestProperty2GridBind { get; set; }
      [TreeColumn("GridTestProperty3GridBind1212")]
      public StringElemCell GridTestProperty3GridBind { get; set; }
      //===============================================
      public TreeColumnRow()
         : base()
      {
         GridTestProperty1GridBind = new StringElemCell();
         GridTestProperty2GridBind = new IntElemCell();
         GridTestProperty3GridBind = new StringElemCell();
      }
   }
}
