﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestComponentApplication
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public List<TreeColumnRow> TreeRow { get; set; }
      //===================================================
      public MainWindow()
      {
         InitializeComponent();
         //==========
         //Инициализация
         InitTreeColumn();
      }
      //===================================================
      /// <summary>
      /// Инициализация TreeColumnCtrl
      /// </summary>
      public void InitTreeColumn()
      {
         TreeRow = new List<TreeColumnRow>();
         //----
         TreeColumnRow mainRow = new TreeColumnRow();
         TreeColumnRow subRow = null;
         TreeColumnRow subSubRow = null;
         for (int j = 0; j < 4; j++)
         {
            subRow = new TreeColumnRow();
            for (int i = 0; i < 4; i++)
            {
               subSubRow = new TreeColumnRow();
               subSubRow.GridTestProperty1GridBind.Value = string.Format("sub1 {0}", i);
               subSubRow.GridTestProperty2GridBind.Value = 10-i;
               subRow.Children.Add(subSubRow);
            }
            mainRow.Children.Add(subRow);
         }
         //mainRow.Children.Add(subRow);
         //mainRow.Children.Add(subRow);
         //mainRow.Children.Add(subRow);
         TreeRow.Add(mainRow);
         //----
         //TreeColumn.AddTextColumn("Header1", "GridTestProperty1");
         //TreeColumn.AddTextColumn("Header1_1", "GridTestProperty1");
         //TreeColumn.AddTextColumn("Header2", "GridTestProperty2");
         //TreeColumn.AddTextColumn("Header2_2", "GridTestProperty2");
         //----
         TreeColumn.ItemsSource = TreeRow;
         TreeColumn.RootSetting = "Test1";
         TreeColumn.SettingFile = "Test.xml";
         TreeColumn.LoadColumnFromFile();
      }
   }
}
