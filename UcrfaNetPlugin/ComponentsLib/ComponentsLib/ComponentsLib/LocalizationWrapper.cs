﻿using System;
using System.Collections.Generic;

namespace ComponentsLib
{
    class LocalizationWrapper
    {
        static LisLocalizationLib.Localization _valLocal = null;
        static LocalizationWrapper()
        {
            _valLocal = new LisLocalizationLib.Localization(Environment.CurrentDirectory, "XICSM_UcrfRfaNET");
        }
        //===================================================
        // Переводим фразу
        //===================================================
        public static string TxT(string trunsl)
        {
            if (_valLocal != null)
                return _valLocal.Translate(trunsl);
            return trunsl;
        }
        //===================================================
        // Список языков
        //===================================================
        public static List<string> Lenguages()
        {
            return _valLocal.ListLanguage;
        }
    }
}
