﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ComponentsLib
{
   /// <summary>
   /// Базовый класс для строки компоненты TreeColumnsControl
   /// </summary>
   public abstract class TreeColumnRowBase : INotifyPropertyChanged
   {
      /// <summary>
      /// Отображать / не отображать CheckBox в колонку Description
      /// </summary>
      public bool IsCheckBoxVisible { get; set; }
      /// <summary>
      /// Свойство видимости для привязки к компоненте CheckBox
      /// </summary>
      public Visibility IsCheckBoxVisibility { get { return (IsCheckBoxVisible == true) ? Visibility.Visible : Visibility.Collapsed; } }
      /// <summary>
      /// Свойство видимости для привязки к компоненте TextBox
      /// </summary>
      public Visibility IsTextBoxVisibility { get { return (IsCheckBoxVisible == false) ? Visibility.Visible : Visibility.Collapsed; } }
      /// <see cref="IsChecked"/>
      private bool _isChecked;
      /// <summary>
      /// Выбрана / не выбрана строка
      /// </summary>
      public bool IsChecked
      {
         get { return _isChecked; }
         set
         {
            if (IsCheckBoxVisible == false)
               return;

            if (_isChecked != value)
            {
               _isChecked = value;
               OnPropertyChanged("IsChecked");
               foreach (TreeColumnRowBase child in Children)
                  child.IsChecked = _isChecked;
            }
            //TODO Возможно необходимо удалить. Нужно проверить глувинную простановку
            //ICollectionView dataView = CollectionViewSource.GetDefaultView(Children);
            //dataView.Refresh();
         }
      }
      /// <summary>
      /// Описание колонки (колонка Description)
      /// </summary>
      public StringElemCell Description { get; set; }
      /// <summary>
      /// Подстроки данной строки
      /// </summary>
      public List<TreeColumnRowBase> Children { get; private set; }
      //===================================================
      /// <summary>
      /// Конструктор
      /// </summary>
      protected TreeColumnRowBase()
      {
         this.Children = new List<TreeColumnRowBase>();
         this.Description = new StringElemCell();
         IsChecked = false;
         IsCheckBoxVisible = true;
      }
      /// <summary>
      /// Возвращает описание всех свойств в строке
      /// </summary>
      /// <returns>Описание всех свойств в строке</returns>
      public List<PropertyDescription> GetPropertyDescription()
      {
         List<PropertyDescription> retList = new List<PropertyDescription>();
         Type typeToReflect = GetType();
         PropertyInfo[] members = typeToReflect.GetProperties();
         foreach (PropertyInfo member in members)
         {
            foreach (Attribute attr in Attribute.GetCustomAttributes(member))
            {
               if (attr.GetType() == typeof (TreeColumnAttribute))
               {
                  string comment = ((TreeColumnAttribute) attr).Comment;
                  if (string.IsNullOrEmpty(comment))
                     comment = member.Name;
                  PropertyDescription newPropertyDescription = new PropertyDescription();
                  newPropertyDescription.DescriptionProperty = LocalizationWrapper.TxT(comment);
                  newPropertyDescription.NameProperty = string.Format("{0}.Value", member.Name);
                  newPropertyDescription.NamePropertyShort = string.Format("{0}", member.Name);
                  newPropertyDescription.BackColorProperty = string.Format("{0}.BackColor", member.Name);
                  newPropertyDescription.BackColorBrushProperty = string.Format("{0}.BackColorBrush", member.Name);
                  retList.Add(newPropertyDescription);
                  break;
               }
            }
         }
         return retList;
      }
      /// <summary>
      /// Сортирует список
      /// </summary>
      /// <param name="sortBy">Название поля, по которому сортируем</param>
      /// <param name="direction">Направление сортировки</param>
      public void Sort(string sortBy, ListSortDirection direction)
      {
         if (Children != null)
         {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(Children);
            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
            //-----
            foreach (TreeColumnRowBase child in Children)
               child.Sort(sortBy, direction);
         }
      }

      public event PropertyChangedEventHandler PropertyChanged;
      // Create the OnPropertyChanged method to raise the event
      protected void OnPropertyChanged(string name)
      {
         PropertyChangedEventHandler handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(name));
         }
      }
   }
   /// <summary>
   /// Класс описывает параметры одного свойства колонки в TreeColumnView
   /// </summary>
   public class PropertyDescription
   {
      /// <summary>
      /// Описание свойства
      /// </summary>
      public string DescriptionProperty { get; set; }
      /// <summary>
      /// Название свойства для отобрадения
      /// </summary>
      public string NameProperty { get; set; }
      /// <summary>
      /// Название свойства (короткое)
      /// </summary>
      public string NamePropertyShort { get; set; }
      /// <summary>
      /// Название свойства для цвета фона ячейки
      /// </summary>
      public string BackColorProperty { get; set; }
      /// <summary>
      /// Название свойства кисти фона ячейки
      /// </summary>
      public string BackColorBrushProperty { get; set; }
      /// <summary>
      /// Ширина ячейки по умолчанию
      /// </summary>
      public int Width { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      public PropertyDescription()
      {
          Width = 50;
      }
   }
}
