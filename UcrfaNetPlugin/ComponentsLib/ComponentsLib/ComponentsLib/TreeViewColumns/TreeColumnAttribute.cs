﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComponentsLib
{
   public class TreeColumnAttribute : Attribute
   {
      public string Comment { get; set; }
      public TreeColumnAttribute(string comment)
      {
         Comment = comment;
      }

   }
}
