﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ComponentsLib
{
   /// <summary>
   /// Элемент ячейки: Базовый клас
   /// </summary>
   public class ElemCellBase
   {
      /// <summary>
      /// Цвет фона ячейки
      /// </summary>
      private Color _backColor;
      /// <summary>
      /// Цвет фона ячейки
      /// </summary>
      public Color BackColor
      {
         get { return _backColor; }
         set
         {
            if (_backColor != value)
            {
               _backColor = value;
               BackColorBrush = new SolidColorBrush(_backColor);
            }
         }
      }
      /// <summary>
      /// Кисть фона ячейки
      /// </summary>
      public SolidColorBrush BackColorBrush { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      public ElemCellBase()
      {
         BackColor = Colors.Transparent;
      }
   }
   /// <summary>
   /// Элемент ячейки с строкой
   /// </summary>
   public class StringElemCell : ElemCellBase
   {
      /// <summary>
      /// Строка, которая отображается
      /// </summary>
      public string Value { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      public StringElemCell() : base()
      {
         Value = "";
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="initValue">Строка инициализации</param>
      public StringElemCell(string initValue) : this()
      {
         Value = initValue;
      }
      /// <summary>
      /// Перевод в строку
      /// </summary>
      /// <returns>строку</returns>
      public override string ToString()
      {
          return Value;
      }
   }
   /// <summary>
   /// Элемент ячейки с целым числом
   /// </summary>
   public class IntElemCell : ElemCellBase
   {
      /// <summary>
      /// Цисло, которое отображаем
      /// </summary>
      public int Value { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      public IntElemCell() : base()
      {
         Value = 0;
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="initValue">Число инициализации</param>
      public IntElemCell(int initValue) : this()
      {
         Value = 0;
      }
      /// <summary>
      /// Перевод в строку
      /// </summary>
      /// <returns>строку</returns>
      public override string ToString()
      {
          return Value.ToString();
      }

   }
   /// <summary>
   /// Элемент ячейки с дробным числом
   /// </summary>
   public class DoubleElemCell : ElemCellBase
   {
      private double _value;
      /// <summary>
      /// Цисло, которое отображаем
      /// </summary>
      public double Value
      {
         get { return Math.Round(_value, RoundNumber);}
         set { if (_value != value) _value = value; }
      }
      /// <summary>
      /// До какого знака округлять
      /// </summary>
      public int RoundNumber { get; set; }
      /// <summary>
      /// Конструктор
      /// </summary>
      public DoubleElemCell()
      {
          Value = 0.0;
          RoundNumber = 10;
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      public DoubleElemCell(int rounNum) : base()
      {
         RoundNumber = rounNum;
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="initValue">Число инициализации</param>
      public DoubleElemCell(double initValue) : this()
      {
         Value = initValue;
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="initValue">Число инициализации</param>
      /// <param name="round">Знаков после запятой</param>
      public DoubleElemCell(double initValue, int round) : this()
      {
          Value = initValue;
          RoundNumber = round;
      }
      /// <summary>
      /// Перевод в строку
      /// </summary>
      /// <returns>строку</returns>
      public override string ToString()
      {
          return Value.ToString();
      }
   }
   /// <summary>
   /// Элемент ячейки с дробным числом
   /// </summary>
   public class DateTimeElemCell : ElemCellBase
   {
       private DateTime _valueDateTime;
       /// <summary>
       /// Дата, которая отображается
       /// </summary>
       public DateTime ValueDateTime
       {
           get { return _valueDateTime; }
           set { if (_valueDateTime != value) _valueDateTime = value; }
       }
       /// <summary>
       /// Дата, которая отображается
       /// </summary>
       public string Value
       {
           get { return ToString(); }
       }
       /// <summary>
       /// Формат отображения даты
       /// </summary>
       public string DateFormat { get; set; }
       /// <summary>
       /// Конструктор
       /// </summary>
       public DateTimeElemCell()
           : this(DateTime.Now, "")
       {
       }
       /// <summary>
       /// Конструктор
       /// </summary>
       /// <param name="dateFormat">Формат отображения даты</param>
       public DateTimeElemCell(string dateFormat)
           : this(DateTime.Now, dateFormat)
       {
       }
       /// <summary>
       /// Конструктор
       /// </summary>
       /// <param name="initValue">Дата инициализации</param>
       public DateTimeElemCell(DateTime initValue)
           : this(initValue, "")
       {
       }
       /// <summary>
       /// Конструктор
       /// </summary>
       /// <param name="initValue">Число инициализации</param>
       /// <param name="dateFormat">Знаков после запятой</param>
       public DateTimeElemCell(DateTime initValue, string dateFormat)
       {
           ValueDateTime = initValue;
           DateFormat = dateFormat;
       }
       /// <summary>
       /// Перевод в строку
       /// </summary>
       /// <returns>строку</returns>
       public override string ToString()
       {
           if(string.IsNullOrEmpty(DateFormat))
               return ValueDateTime.ToString();
           return ValueDateTime.ToString(DateFormat);
       }
   }
}
