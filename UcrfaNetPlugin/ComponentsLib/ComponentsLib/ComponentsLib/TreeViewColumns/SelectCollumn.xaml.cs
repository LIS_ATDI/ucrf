﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Documents;

namespace ComponentsLib
{
   /// <summary>
   /// Interaction logic for SelectCollumn.xaml
   /// </summary>
   public partial class SelectCollumn : Window
   {
      /// <summary>
      /// Класс колонки
      /// </summary>
      private TreeColumnCtrl _listCtrl = null;
      //===================================================
      private BindingList<PropertyDescription> _notSelectedProperties = new BindingList<PropertyDescription>();
      /// <summary>
      /// Все свойства (колонки)
      /// </summary>
      public BindingList<PropertyDescription> NotSelectedProperties
      {
         get { return _notSelectedProperties; }
         set
         {
            if (_notSelectedProperties != value)
               _notSelectedProperties = value;
         }
      }
      //===================================================
      private BindingList<PropertyDescription> _selectedProperties = new BindingList<PropertyDescription>();
      /// <summary>
      /// Выбраные свойства (колонки)
      /// </summary>
      public BindingList<PropertyDescription> SelectedProperties
      {
         get { return _selectedProperties; }
         set
         {
            if (_selectedProperties != value)
               _selectedProperties = value;
         }
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      protected SelectCollumn()
      {
         InitializeComponent();
      }
      /// <summary>
      /// Конструктор
      /// </summary>
      /// <param name="listBase">Класс списка</param>
      public SelectCollumn(TreeColumnCtrl listBase)
         : this()
      {
         SetListView(listBase);
         lstNotSelectedColumns.ItemsSource = NotSelectedProperties;
         lstSelectedColumns.ItemsSource = SelectedProperties;
      }
      /// <summary>
      /// Инициализирует списки данными для отображения
      /// </summary>
      /// <param name="listBase"></param>
      public void SetListView(TreeColumnCtrl listBase)
      {
         _listCtrl = listBase;
         NotSelectedProperties.Clear();
         SelectedProperties.Clear();
         //===========================
         //Загружаем все свойства для отображения
         IEnumerator enumer = _listCtrl.ItemsSource.GetEnumerator();
         enumer.Reset();
         if (enumer.MoveNext())
         {
            List<PropertyDescription> tmpAllPropertyList = new List<PropertyDescription>();
            object curElem = enumer.Current;
            if (curElem is TreeColumnRowBase)
            {
               TreeColumnRowBase tmpBaseRow = (TreeColumnRowBase) curElem;
               tmpAllPropertyList = tmpBaseRow.GetPropertyDescription();
            }
            PropertyDescription[] selectedPropertyList = _listCtrl.GetSelectedProperties();
            
            foreach(PropertyDescription property in tmpAllPropertyList)
            {
               bool needAdd = true;
               foreach (PropertyDescription selectProp in selectedPropertyList)
               {
                  if(property.NameProperty == selectProp.NameProperty)
                  {
                     SelectedProperties.Add(property);
                     needAdd = false;
                     break;
                  }
               }
               if(needAdd == true)
                  NotSelectedProperties.Add(property);
            }
         }
      }
      /// <summary>
      /// Добавляет новую колонку
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnAddSelectedColumn_Click(object sender, RoutedEventArgs e)
      {
         int index = lstNotSelectedColumns.SelectedIndex;
         if((index != -1) && (index < NotSelectedProperties.Count))
         {
            PropertyDescription property = NotSelectedProperties[index];
            NotSelectedProperties.RemoveAt(index);
            SelectedProperties.Add(property);
         }
      }
      /// <summary>
      /// Удаляет колонку из выбраных колонок
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnDeleteSelectedColumn_Click(object sender, RoutedEventArgs e)
      {
         int index = lstSelectedColumns.SelectedIndex;
         if ((index != -1) && (index < SelectedProperties.Count))
         {
            PropertyDescription property = SelectedProperties[index];
            SelectedProperties.RemoveAt(index);
            NotSelectedProperties.Add(property);
         }
      }
      /// <summary>
      /// Поднять колонку на одну позицию вверх
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnUpSelectedColumn_Click(object sender, RoutedEventArgs e)
      {
         int index = lstSelectedColumns.SelectedIndex;
         int newIndex = index - 1;
         if ((index != -1) && (newIndex >= 0))
         {
            PropertyDescription property = SelectedProperties[index];
            SelectedProperties.RemoveAt(index);
            SelectedProperties.Insert(newIndex, property);
            lstSelectedColumns.SelectedIndex = newIndex;
         }
      }
      /// <summary>
      /// Опустить колонку на одну позицию вниз
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnDownSelectedColumn_Click(object sender, RoutedEventArgs e)
      {
         int index = lstSelectedColumns.SelectedIndex;
         int newIndex = index + 1;
         if ((index != -1) && (newIndex < SelectedProperties.Count))
         {
            PropertyDescription property = SelectedProperties[index];
            SelectedProperties.RemoveAt(index);
            SelectedProperties.Insert(newIndex, property);
            lstSelectedColumns.SelectedIndex = newIndex;
         }
      }
      /// <summary>
      /// Применить изменения
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnOk_Click(object sender, RoutedEventArgs e)
      {
         DialogResult = true;
      }
   }
}
