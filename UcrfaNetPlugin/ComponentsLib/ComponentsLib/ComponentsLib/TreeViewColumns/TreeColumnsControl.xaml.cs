﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using AMS.Profile;

namespace ComponentsLib
{
   /// <summary>
   /// Interaction logic for TreeColumnControl.xaml
   /// </summary>
   public partial class TreeColumnCtrl : UserControl
   {
      public delegate void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e);
      public delegate void SelectedItemDoubleClick(object sender, MouseButtonEventArgs e, TreeColumnRowBase row);
      // Declare the event.
      public event SelectedItemChanged OnSelectedItemChanged;
      public event SelectedItemDoubleClick OnSelectedItemDoubleClick;

      public IEnumerable ItemsSource
      {
         get { return TreeColumnElem.ItemsSource; }
         set { TreeColumnElem.ItemsSource = value;}
      }

      public IEnumerable<TreeColumnRowBase> ItemsSourceBaseRow
      {
         get { return ItemsSource as IEnumerable<TreeColumnRowBase>; }
      }
      /// <summary>
      /// Путь к файлу с настройками
      /// </summary>
      public string SettingFile { get; set; }
      /// <summary>
      /// Корневой элемент для сохрания настроек
      /// </summary>
      public string RootSetting { get; set; }
      /// <summary>
      /// Колонка, по которой выполняеться сортировка
      /// </summary>
      private GridViewColumnHeader _lastHeaderClicked;
      /// <summary>
      /// Текущее направление сортировки
      /// </summary>
      private ListSortDirection _lastDirection = ListSortDirection.Ascending;
      /// <summary>
      /// Текущая выбранная строка
      /// </summary>
      private TreeColumnRowBase curRow = null;
      //===================================================
      /// <summary>
      /// Consttructor
      /// </summary>
      public TreeColumnCtrl()
      {
         InitializeComponent();
         //======
         SettingFile = "";
         RootSetting = "";
         ContextMenuQuery.Header = LocalizationWrapper.TxT(ContextMenuQuery.Header.ToString());
         ContextMenuCollapse.Header = LocalizationWrapper.TxT(ContextMenuCollapse.Header.ToString());
         ContextMenuExpand.Header = LocalizationWrapper.TxT(ContextMenuExpand.Header.ToString());
         if(TreeColumnElem.Columns.Count > 0)
             TreeColumnElem.Columns[0].Header = LocalizationWrapper.TxT(TreeColumnElem.Columns[0].Header.ToString());
      }
      //===================================================
      /// <summary>
      /// Returns a list of header's name
      /// </summary>
      /// <returns>A list of header's name</returns>
      public string[] GetHeaderNames()
      {
          List<string> headerNames = new List<string>();
          foreach (GridViewColumn column in TreeColumnElem.Columns)
          {
              headerNames.Add(column.Header.ToString());
          }
          return headerNames.ToArray();
      }
      //===================================================
      /// <summary>
      /// Returns a list of selected properties
      /// </summary>
      /// <returns>A list of selected properties</returns>
      public PropertyDescription[] GetSelectedProperties()
      {
         List<PropertyDescription> selectedProperties = new List<PropertyDescription>();
         foreach (GridViewColumn column in TreeColumnElem.Columns)
         {
            GridViewColumnExtension clmnExtension = column as GridViewColumnExtension;
            if((clmnExtension != null) && (clmnExtension.Tag != null))
            {
               PropertyDescription property = clmnExtension.Tag as PropertyDescription;
               if(property != null)
                  selectedProperties.Add(property);
            }
         }
         return selectedProperties.ToArray();
      }
      //===================================================
      /// <summary>
      /// Deletes a column on index
      /// </summary>
      /// <param name="index">An index of column</param>
      /// <exception cref="IndexOutOfRangeException"
      public void DeleteColumn(int index)
      {
         DeleteColumn(GetProperyDescriptionAtIndex(index));
      }
      //===================================================
      /// <summary>
      /// Deletes a column on ProperyDescription
      /// </summary>
      /// <param name="property">An property description of column</param>
      public void DeleteColumn(PropertyDescription property)
      {
         if(property == null)
            return;
         int i = 0;
         while(i<TreeColumnElem.Columns.Count)
         {
            PropertyDescription tmpProperty = GetProperyDescriptionAtIndex(i);
            if((tmpProperty != null) && (tmpProperty.NameProperty == property.NameProperty))
            {
               TreeColumnElem.Columns.RemoveAt(i);
            }
            else
            {
               i++;
            }
         }
      }
      //===================================================
      /// <summary>
      /// Adds a new column into TreeColumnCtrl as TextBlock
      /// </summary>
      /// <param name="property">Column description</param>
      public void AddTextColumn(PropertyDescription property)
      {
         DataTemplate dataTemplate = new DataTemplate();
         FrameworkElementFactory factory = new FrameworkElementFactory(typeof(StackPanel));
         dataTemplate.VisualTree = factory;
         FrameworkElementFactory childFactory = new FrameworkElementFactory(typeof(TextBlock));
         childFactory.SetBinding(TextBlock.TextProperty, new Binding(property.NameProperty));
         childFactory.SetBinding(TextBlock.BackgroundProperty, new Binding(property.BackColorBrushProperty));
         factory.AppendChild(childFactory);
         GridViewColumnExtension newColumn = new GridViewColumnExtension();
         newColumn.Header = property.DescriptionProperty;
         newColumn.Tag = property;
         newColumn.Width = property.Width;
         newColumn.CellTemplate = dataTemplate;
         TreeColumnElem.Columns.Add(newColumn);
         //-----
         newColumn.HeaderTemplate = Resources["HeaderTemplateNormal"] as DataTemplate;
      }
      //===================================================
      /// <summary>
      /// Loads all collumn into TreeColumnView
      /// </summary>
      private void LoadAllColumn()
      {
         TreeColumnRowBase tmpBaseRow = GetTreeObject();
         if (tmpBaseRow != null)
         {
            List<PropertyDescription> allProperties = GetTreeObject().GetPropertyDescription();
            foreach (PropertyDescription property in allProperties)
               AddTextColumn(property);
         }
      }
      /// <summary>
      /// Возвращет описание колонки по индексу колонки
      /// </summary>
      /// <param name="index">Индекс колонки</param>
      /// <returns>Описание колонки или null если описание не может быть получено</returns>
      private PropertyDescription GetProperyDescriptionAtIndex(int index)
      {
         if (index >= TreeColumnElem.Columns.Count)
            return null;   //Выход за пределы колонок
         GridViewColumnExtension grdExtension =  TreeColumnElem.Columns[index] as GridViewColumnExtension;
         if (grdExtension == null)
            return null; //Ошибка получения класса
         if (grdExtension.Tag == null)
            return null; //свойство не установленно
         PropertyDescription retVal = grdExtension.Tag as PropertyDescription;
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Deletes all collumn from TreeColumnView
      /// </summary>
      public void DeleteAllColumn()
      {
         foreach (PropertyDescription property in GetSelectedProperties())
            DeleteColumn(property);
      }
      //===================================================
      /// <summary>
      /// Loads all collumn into TreeColumnView from file
      /// </summary>
      public void LoadColumnFromFile()
      {
         ReloadColumns();
      }
      //===================================================
      /// <summary>
      /// Loads columns from the setting file
      /// </summary>
      public void ReloadColumns()
      {
         DeleteAllColumn();
         if ((string.IsNullOrEmpty(RootSetting) == false) &&
             (string.IsNullOrEmpty(SettingFile) == false))
         {
            TreeColumnRowBase tmpBaseRow = GetTreeObject();
            if (tmpBaseRow != null)
            {
               List<PropertyDescription> allProperties = tmpBaseRow.GetPropertyDescription();
               Xml settingFile = new Xml(SettingFile);
               string[] fields = settingFile.GetEntryNames(RootSetting);
               if (fields != null)
               {
                   foreach (string field in fields)
                   {
                       foreach (PropertyDescription property in allProperties)
                           if (property.NameProperty == field)
                           {
                               int width = 0;
                               string strWidth = settingFile.GetValue(RootSetting, field).ToString();
                               if (int.TryParse(strWidth, out width) && width > 0)
                                   property.Width = width;
                               AddTextColumn(property);
                           }
                   }
               }
               else
               {
                   //Загружаем все колонки
                   LoadAllColumn();
               }

            }
         }
         else
         {//Загружаем все колонки
            LoadAllColumn();
         }
      }
      //===================================================
      /// <summary>
      /// Saves columns to the setting file
      /// </summary>
      public void SaveColumns()
      {
         if ((string.IsNullOrEmpty(RootSetting) == false) &&
             (string.IsNullOrEmpty(SettingFile) == false))
         {
                Xml settingFile = new Xml(SettingFile);
            using (settingFile.Buffer())
            {
               settingFile.RemoveSection(RootSetting);
               foreach (PropertyDescription property in GetSelectedProperties())
               {
                   property.Width = 50;
                   foreach (GridViewColumn column in TreeColumnElem.Columns)
                   {
                       GridViewColumnExtension newColumn = column as GridViewColumnExtension;
                       if((newColumn != null) && (newColumn.Tag != null))
                       {
                           PropertyDescription prpDescr = newColumn.Tag as PropertyDescription;
                           if((prpDescr != null) && (prpDescr.NamePropertyShort == property.NamePropertyShort))
                               property.Width = (int)newColumn.Width;
                       }
                   }
                   settingFile.SetValue(RootSetting, property.NameProperty, property.Width);
               }
            }
         }
      }
      //===================================================
      /// <summary>
      /// Возвращает объект элемента компоненты
      /// </summary>
      /// <returns>Объект колонки или null поле ItemsSource не установлено или невозможно определить объект</returns>
      private TreeColumnRowBase GetTreeObject()
      {
         TreeColumnRowBase retVal = null;
         if (ItemsSource != null)
         {
            IEnumerator enumer = ItemsSource.GetEnumerator();
            enumer.Reset();
            if (enumer.MoveNext())
            {
               object curElem = enumer.Current;
               if (curElem is TreeColumnRowBase)
                  retVal = (TreeColumnRowBase)curElem;
            }
         }
         return retVal;
      }
      /// <summary>
      /// Отображаем окно для настройки колонок
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnEditColoumn_Click(object sender, RoutedEventArgs e)
      {
          SelectCollumn frmSelectColumn = new SelectCollumn(this);
          frmSelectColumn.ShowDialog();
          if (frmSelectColumn.DialogResult.HasValue && frmSelectColumn.DialogResult.Value)
          {// Необходимо изменить колонки
              DeleteAllColumn();
              foreach (PropertyDescription property in frmSelectColumn.SelectedProperties)
                  AddTextColumn(property);
          }
      }
      /// <summary>
      /// Развуернуть дерево
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnExpand_Click(object sender, RoutedEventArgs e)
      {
          ExpandRecursively(TreeColumnElem, true);
      }
      /// <summary>
      /// Свернуть дерево
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void btnCollapse_Click(object sender, RoutedEventArgs e)
      {
          ExpandRecursively(TreeColumnElem, false);
      }



      private static void ExpandRecursively(ItemsControl itemsControl, bool expand)
      {
          //TreeViewItem treeViewItem = itemsControl as TreeViewItem;
          //if (treeViewItem != null)
          //    treeViewItem.IsExpanded = expand;
          ItemContainerGenerator itemContainerGenerator = itemsControl.ItemContainerGenerator;
          //for (int i = itemsControl.Items.Count - 1; i >= 0; --i)
          for (int i = 0; i < itemsControl.Items.Count; ++i)
          {
              ItemsControl childControl = itemContainerGenerator.ContainerFromIndex(i) as ItemsControl;
              if (childControl != null)
              {
                  //TreeViewItem treeViewItem = childControl as TreeViewItem;
                  //if (treeViewItem != null)
                  //    treeViewItem.IsExpanded = expand;
                  ExpandRecursively(childControl, expand);
              }
          }
          TreeViewItem treeViewItem = itemsControl as TreeViewItem;
          if (treeViewItem != null)
          {
              treeViewItem.IsExpanded = expand;

              //treeViewItem.ApplyTemplate();
              //ItemsPresenter itemsPresenter =
              //    (ItemsPresenter)treeViewItem.Template.FindName("ItemsPresenter", treeViewItem);
              //if (itemsPresenter != null)
              //{
              //    itemsPresenter.Visibility = expand ? Visibility.Visible : Visibility.Collapsed;
              //    itemsPresenter.ApplyTemplate();
              //}
              //else
              //{
              //    //// The Tree template has not named the ItemsPresenter, 
              //    //// so walk the descendents and find the child.
              //    //itemsPresenter = FindVisualChild<ItemsPresenter>(container);
              //    //if (itemsPresenter == null)
              //    //{
              //    //    container.UpdateLayout();

              //    //    itemsPresenter = FindVisualChild<ItemsPresenter>(container);
              //    //}
              //}
          }
      }

      /// <summary>
      /// Собитие сортировки колонки
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void OnSortColumn_Click(object sender, MouseEventArgs e)
      {
         GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
         // Проверки
         if (headerClicked == null)
            return;
         if (headerClicked.Role == GridViewColumnHeaderRole.Padding)
            return;
         if (headerClicked.Column == null)
            return;
         GridViewColumnExtension grdExtnsn = headerClicked.Column as GridViewColumnExtension;
         if (grdExtnsn == null)
            return;
         PropertyDescription propery = grdExtnsn.Tag as PropertyDescription;
         if (propery == null)
            return;
         //-----------------------------------
         ListSortDirection direction;
         if (headerClicked != _lastHeaderClicked)
         {
            direction = ListSortDirection.Ascending;
         }
         else
         {
            if (_lastDirection == ListSortDirection.Ascending)
            {
               direction = ListSortDirection.Descending;
            }
            else
            {
               direction = ListSortDirection.Ascending;
            }
         }
         Sort(propery.NameProperty, direction);

         if (direction == ListSortDirection.Ascending)
         {
            headerClicked.Column.HeaderTemplate = Resources["HeaderTemplateArrowUp"] as DataTemplate;
         }
         else
         {
            headerClicked.Column.HeaderTemplate = Resources["HeaderTemplateArrowDown"] as DataTemplate;
         }
         // Remove arrow from previously sorted header
         if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
         {
            _lastHeaderClicked.Column.HeaderTemplate = Resources["HeaderTemplateNormal"] as DataTemplate;
         }
         _lastHeaderClicked = headerClicked;
         _lastDirection = direction;
      }
      /// <summary>
      /// Сортирует список
      /// </summary>
      /// <param name="sortBy">Название поля, по которому сортируем</param>
      /// <param name="direction">Направление сортировки</param>
      private void Sort(string sortBy, ListSortDirection direction)
      {
         if (ItemsSource == null) 
            return;
         //-------
         ICollectionView dataView = CollectionViewSource.GetDefaultView(ItemsSource);
         dataView.SortDescriptions.Clear();
         SortDescription sd = new SortDescription(sortBy, direction);
         dataView.SortDescriptions.Add(sd);
         dataView.Refresh();
         //-----
         foreach (object itemRow in ItemsSource)
         {
            if (itemRow is TreeColumnRowBase)
               ((TreeColumnRowBase)itemRow).Sort(sortBy, direction);
         }
      }
      /// <summary>
      /// Изменился Item
      /// </summary>
      private void TreeColumnElem_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         if (e.NewValue == null)
            return;
         curRow = e.NewValue as TreeColumnRowBase;
         if (OnSelectedItemChanged != null)
            OnSelectedItemChanged(sender, e);
      }
      /// <summary>
      /// Обработка клавиатуры
      /// </summary>
      private void TreeColumnElem_KeyUp(object sender, KeyEventArgs e)
      {
         if ((e.Key == Key.Space) && (curRow != null) && (ItemsSource != null))
         {
            curRow.IsChecked = !curRow.IsChecked;
         }
      }
      /// <summary>
      /// Обработка двойного клика мишки на строке
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void TreeColumnElem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
          if (curRow == null)
              return;
          if (OnSelectedItemDoubleClick != null)
              OnSelectedItemDoubleClick(sender, e, curRow);
      }
      /// <summary>
      /// Форма прекратила сущетвование
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void UserControl_Unloaded(object sender, RoutedEventArgs e)
      {
          //Сохраняем параметры колонок
          SaveColumns();
      }
   }
}
