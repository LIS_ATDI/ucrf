﻿using System.Windows.Controls;
//=========================================================
namespace ComponentsLib
{
   /// <summary>
   /// Класс GridViewColumnExtension разширяет класс GridViewColumn
   /// новыми свойствами
   /// </summary>
   public class GridViewColumnExtension : GridViewColumn
   {
      /// <summary>
      /// Здесь можно хранить пользовательские данные
      /// </summary>
      public object Tag { get; set; }
   }

}
