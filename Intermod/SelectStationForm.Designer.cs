﻿namespace XICSM.Intermodulation
{
	partial class SelectStationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
         this.StationListView = new System.Windows.Forms.ListView();
         this.IDColumnHeader = new System.Windows.Forms.ColumnHeader();
         this.NameColumnHeader = new System.Windows.Forms.ColumnHeader();
         this.OwnerColumnHeader = new System.Windows.Forms.ColumnHeader();
         this.button1 = new System.Windows.Forms.Button();
         this.OkButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // StationListView
         // 
         this.StationListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                     | System.Windows.Forms.AnchorStyles.Left)
                     | System.Windows.Forms.AnchorStyles.Right)));
         this.StationListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IDColumnHeader,
            this.NameColumnHeader,
            this.OwnerColumnHeader});
         this.StationListView.FullRowSelect = true;
         this.StationListView.Location = new System.Drawing.Point(7, 12);
         this.StationListView.Name = "StationListView";
         this.StationListView.Size = new System.Drawing.Size(714, 339);
         this.StationListView.TabIndex = 0;
         this.StationListView.UseCompatibleStateImageBehavior = false;
         this.StationListView.View = System.Windows.Forms.View.Details;
         // 
         // IDColumnHeader
         // 
         this.IDColumnHeader.Text = "ID";
         this.IDColumnHeader.Width = 45;
         // 
         // NameColumnHeader
         // 
         this.NameColumnHeader.Text = "Name";
         this.NameColumnHeader.Width = 102;
         // 
         // OwnerColumnHeader
         // 
         this.OwnerColumnHeader.Text = "Owner";
         this.OwnerColumnHeader.Width = 361;
         // 
         // button1
         // 
         this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.button1.Location = new System.Drawing.Point(565, 357);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 4;
         this.button1.Text = "Cancel";
         this.button1.UseVisualStyleBackColor = true;
         // 
         // OkButton
         // 
         this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
         this.OkButton.Location = new System.Drawing.Point(646, 357);
         this.OkButton.Name = "OkButton";
         this.OkButton.Size = new System.Drawing.Size(75, 23);
         this.OkButton.TabIndex = 3;
         this.OkButton.Text = "Select";
         this.OkButton.UseVisualStyleBackColor = true;
         this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
         // 
         // SelectStationForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(733, 392);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.OkButton);
         this.Controls.Add(this.StationListView);
         this.Name = "SelectStationForm";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Select Station";
         this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView StationListView;
		private System.Windows.Forms.ColumnHeader IDColumnHeader;
		private System.Windows.Forms.ColumnHeader NameColumnHeader;
		private System.Windows.Forms.ColumnHeader OwnerColumnHeader;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button OkButton;
	}
}