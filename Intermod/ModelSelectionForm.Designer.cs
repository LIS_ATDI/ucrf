﻿namespace XICSM.Intermodulation
{
	partial class ModelSelectionForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "525 Free Space",
            "Simplest model"}, -1);
			this.ModelListView = new System.Windows.Forms.ListView();
			this.NameColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.DescriptionColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.OkButton = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ModelListView
			// 
			this.ModelListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameColumnHeader,
            this.DescriptionColumnHeader});
			this.ModelListView.FullRowSelect = true;
			this.ModelListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.ModelListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
			this.ModelListView.Location = new System.Drawing.Point(12, 12);
			this.ModelListView.Name = "ModelListView";
			this.ModelListView.Size = new System.Drawing.Size(411, 195);
			this.ModelListView.TabIndex = 0;
			this.ModelListView.UseCompatibleStateImageBehavior = false;
			this.ModelListView.View = System.Windows.Forms.View.Details;
			// 
			// NameColumnHeader
			// 
			this.NameColumnHeader.Text = "Name";
			this.NameColumnHeader.Width = 140;
			// 
			// DescriptionColumnHeader
			// 
			this.DescriptionColumnHeader.Text = "Description";
			this.DescriptionColumnHeader.Width = 226;
			// 
			// OkButton
			// 
			this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkButton.Location = new System.Drawing.Point(348, 223);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(75, 23);
			this.OkButton.TabIndex = 1;
			this.OkButton.Text = "Select";
			this.OkButton.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button1.Location = new System.Drawing.Point(267, 223);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "Cancel";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// ModelSelectionForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(435, 258);
			this.ControlBox = false;
			this.Controls.Add(this.button1);
			this.Controls.Add(this.OkButton);
			this.Controls.Add(this.ModelListView);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ModelSelectionForm";
			this.Text = "Model";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView ModelListView;
		private System.Windows.Forms.Button OkButton;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ColumnHeader NameColumnHeader;
		private System.Windows.Forms.ColumnHeader DescriptionColumnHeader;
	}
}