﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM
{	
	public class Math
	{
		static public double GetEarthRadius()
		{
			return 6371.0;		
		}

		public struct GeoPosition
		{
			public double Longitude;
			public double Latitude;
		};

		public struct SphereCoordinate
		{
			public double R;
			public double Phita;
			public double Phi;
		};

		public struct DecartCoordinate
		{
			public double X;
			public double Y;
			public double Z;
		};

		public Math()
		{
		}

		static public double GeoPositionToAngleDegree(double GeoCoordinate)
		{
			double Degree = System.Math.Truncate(GeoCoordinate);
			double FracPart = GeoCoordinate - Degree;

			double Minutes = System.Math.Truncate(FracPart * 100.0);
			double Seconds = System.Math.Truncate(FracPart * 10000.0) - Minutes * 100.0;

			return Degree + (Minutes) / 60.0 + (Seconds) / 3600.0;
		}

		static public double AngleDegreeToGeoPosition(double Degree)
		{
			double Seconds = Degree * 3600;
			double Minutes = Degree * 60;

			double TruncatedDegree = System.Math.Truncate(Degree);
			double TruncatedMinutes = System.Math.Truncate(Minutes - TruncatedDegree*60.0);
			double TruncatedSeconds = System.Math.Round(Seconds - (TruncatedMinutes*60.0 + TruncatedDegree*3600.0));

			return TruncatedDegree + TruncatedMinutes / 100 + TruncatedSeconds/10000;
		}


		static public double DegreeToRadian(double Angle)
		{
			return System.Math.PI * Angle / 180.0;
		}

		static public double RadianToDegree(double Angle)
		{
			return Angle * (180.0 / System.Math.PI);
		}
		
		static public SphereCoordinate SphereCoordinateFromGeoPosition(double Longitude, double Lattitude)
		{
			SphereCoordinate SphereCoord = new SphereCoordinate();
			SphereCoord.R = 6371.0; //Average Radius;
			SphereCoord.Phi = GeoPositionToAngleDegree(Longitude);
			SphereCoord.Phita = GeoPositionToAngleDegree(Lattitude);
			return SphereCoord;
		}

		static private SphereCoordinate SphereCoordinateFromGeoPosition(GeoPosition GeoPosition)
		{
			SphereCoordinate SphereCoord = new SphereCoordinate();
			SphereCoord.R = 6371.0; //Average Radius;
			SphereCoord.Phi = GeoPositionToAngleDegree(GeoPosition.Longitude);
			SphereCoord.Phita = GeoPositionToAngleDegree(GeoPosition.Latitude);
			return SphereCoord;
		}

		static private GeoPosition GeoPositionFromSphereCoordinates(SphereCoordinate SphereCoord)
		{
			GeoPosition GeoPosition = new GeoPosition();
			GeoPosition.Latitude = AngleDegreeToGeoPosition(SphereCoord.Phi);
			GeoPosition.Longitude = AngleDegreeToGeoPosition(SphereCoord.Phita);
			return GeoPosition;
		}

		static public DecartCoordinate DecartCoordinateFromSphereCoordinate(SphereCoordinate SphereCoord)
		{
			DecartCoordinate DecartCoord = new DecartCoordinate();
			DecartCoord.X = SphereCoord.R * System.Math.Sin(DegreeToRadian(SphereCoord.Phita)) * System.Math.Cos(DegreeToRadian(SphereCoord.Phi));
			DecartCoord.Y = SphereCoord.R * System.Math.Sin(DegreeToRadian(SphereCoord.Phita)) * System.Math.Sin(DegreeToRadian(SphereCoord.Phi));
			DecartCoord.Z = SphereCoord.R * System.Math.Cos(DegreeToRadian(SphereCoord.Phita));
			return DecartCoord;
		}

		static public SphereCoordinate SphereCoordinateFromDecartCoordinate(DecartCoordinate DecardCoord)
		{
			SphereCoordinate SphereCoord = new SphereCoordinate();
			SphereCoord.R = System.Math.Sqrt(DecardCoord.X * DecardCoord.X + DecardCoord.Y * DecardCoord.Y + DecardCoord.Z * DecardCoord.Z);
			SphereCoord.Phita = System.Math.Acos(DecardCoord.Z / SphereCoord.R);

			if (DecardCoord.X > 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X < 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = (System.Math.PI / 2.0) + System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X < 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = -(System.Math.PI / 2.0) - System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X > 0 && DecardCoord.Y < 0)
				SphereCoord.Phi = -System.Math.Atan(DecardCoord.Y / DecardCoord.X);

			return SphereCoord;
		}

		static public double OneDegreeLatitudeRange()
		{
			return GetEarthRadius()*DegreeToRadian(1);
		}

		static public double OneDegreeLongitudeRange(double Latitude)
		{
			return System.Math.PI*GetEarthRadius()/180.0 * System.Math.Cos(DegreeToRadian(Latitude));
		}

		static public double GetDeltaLatitudeForRange(double Range)
		{
			return Range / OneDegreeLatitudeRange();
		}

		static public double GetDeltaLongitudeForRange(double Range, double Latitude)
		{
			return Range / OneDegreeLongitudeRange(Latitude);
		}

		static public double RangeOnSphereCoorinates(SphereCoordinate Sphere1, SphereCoordinate Sphere2)
		{
			//Math.SphereCoordinate Sphere1 = Math.SphereCoordinateFromGeoPosition( PositionX, PositionY);
			//Math.SphereCoordinate Sphere2 = Math.SphereCoordinateFromGeoPosition(station.PositionX, station.PositionY);

			double DegreeRange = Sphere1.R * Math.DegreeToRadian(1);

			double Delta1 = (Sphere1.Phita - Sphere2.Phita) * DegreeRange;
			double Delta2 = (Sphere1.Phi - Sphere2.Phi) * DegreeRange * System.Math.Cos(Math.DegreeToRadian(Sphere1.Phita + Sphere2.Phita) / 2.0);

			/*Math.SphereCoordinate Sphere1 = Math.SphereCoordinateFromGeoPosition(PositionX, PositionY);
			Math.DecartCoordinate thisCoord = Math.DecartCoordinateFromSphereCoordinate(Sphere1);
			Math.SphereCoordinate Sphere2 = Math.SphereCoordinateFromGeoPosition(station.PositionX, station.PositionY);
			Math.DecartCoordinate itsCoord = Math.DecartCoordinateFromSphereCoordinate(Sphere2);         
			
			double Range = System.Math.Sqrt(
				(thisCoord.X - itsCoord.X) * (thisCoord.X - itsCoord.X)+
				(thisCoord.Y - itsCoord.Y) * (thisCoord.Y - itsCoord.Y)+
				(thisCoord.Z - itsCoord.Z) * (thisCoord.Z - itsCoord.Z));*/

			return System.Math.Sqrt(Delta1 * Delta1 + Delta2 * Delta2);			
		}

		static public double GetAzimuth(SphereCoordinate Sphere1, SphereCoordinate Sphere2)
		{
			//Math.SphereCoordinate Sphere1 = Math.SphereCoordinateFromGeoPosition(PositionX, PositionY);
			//Math.SphereCoordinate Sphere2 = Math.SphereCoordinateFromGeoPosition(station.PositionX, station.PositionY);

			double Delta1 = System.Math.PI * (Sphere1.Phita - Sphere2.Phita) / 180.0 * Sphere1.R;
			double Delta2 = System.Math.PI * (Sphere1.Phi - Sphere2.Phi) / 180.0 * Sphere2.R * System.Math.Cos((Sphere1.Phita + Sphere2.Phita) / 2);

			double TanAngle = System.Math.PI / 2;
			if (Delta2 != 0)
				TanAngle = System.Math.Abs(Delta1 / Delta2);
			else
			{
				if (Delta1 > 0)
					return 0;
				else
					return System.Math.PI;
			}

			if (Delta1 >= 0 && Delta2 >= 0)
				return System.Math.PI / 2.0 - System.Math.Atan(TanAngle);

			if (Delta1 >= 0 && Delta2 <= 0)
				return 3.0 * System.Math.PI / 2.0 + System.Math.Atan(TanAngle);

			if (Delta1 <= 0 && Delta2 >= 0)
				return System.Math.PI / 2.0 + System.Math.Atan(TanAngle);

			if (Delta1 <= 0 && Delta2 <= 0)//!!
				return 3.0 * System.Math.PI / 2.0 - System.Math.Atan(TanAngle);

			return 0.0;				
		}

		static public SphereCoordinate SphereCoordinateByCoorinateSystem(string cSys, double Longitude, double Latitude)
		{
			if (cSys=="4DMS")
				return SphereCoordinateFromGeoPosition(Longitude, Latitude);

			if (cSys=="4DEC")
			{
				SphereCoordinate SphereCoord = new SphereCoordinate();
				SphereCoord.R = 6371.0; //Average Radius;
				SphereCoord.Phi = Longitude;
				SphereCoord.Phita = Latitude;
				return SphereCoord;
			}

			if (cSys == "4SEC")
			{
				SphereCoordinate SphereCoord = new SphereCoordinate();
				SphereCoord.R = 6371.0; //Average Radius;
				SphereCoord.Phi = Longitude / 3600.0;
				SphereCoord.Phita = Latitude / 3600.0;
				return SphereCoord;
			}

			if (cSys == "4DMD")
			{
				SphereCoordinate SphereCoord = new SphereCoordinate();
				SphereCoord.R = 6371.0; //Average Radius;

				double retVal = (int)Longitude;
				Longitude -= retVal;
				retVal += 100 * Longitude / 60;
				SphereCoord.Phi = retVal;

				retVal = (int)Latitude;
				Longitude -= retVal;
				retVal += 100 * Latitude / 60;
				SphereCoord.Phita = retVal;

				//SphereCoord.Phi = Longitude;
				//SphereCoord.Phita = Latitude;
				return SphereCoord;
			}


			SphereCoordinate DummyCoord = new SphereCoordinate();
			return DummyCoord;

			/*	if (sysCoord.IndexOf("DEC") != -1)
				{// DEC
					return pos;
				}
				else if (sysCoord.IndexOf("DMD") != -1)
				{// DMD
					double retVal = (int)pos;
					pos -= retVal;
					retVal += 100 * pos / 60;
					return retVal;
				}
				else if (sysCoord.IndexOf("DMS") != -1)
				{// DMS
					//Convert to DMD
					double retval = ((int)(pos * 100.0)) / 100.0;
					double tmpVal = (pos - retval) / 0.6;
					pos = retval + tmpVal;
					retval = (int)pos;
					pos -= retval;
					retval += 100 * pos / 60;
					return retval;
				}
				else if (sysCoord.IndexOf("SEC") != -1)
				{// SEC
					return pos / 3600;
				}
				return pos; */
		}
	}
}
