﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using NSPosition;
using ICSM;
using Microsoft.Office.Interop.Excel;

namespace XICSM.Intermodulation
{
   public partial class IntermodulationMainForm : Form
   {
      public bool _backgroundWork = false;

      private string nameTable;
      private string nameFreqTable;
      private int idStation;

      private TreeNode ReceiverMainNode;
      private TreeNode InterferenceMainNode;
      private TreeNode NewNode;

      private double PreselectorGain;
      private double InputStageLinearity;
      
      List<int> singleStationList;
      List<int> otherStations;
      List<IntermodulationInterference> interfencesList;
      Hashtable treeHash;

      private double MinFrequency;
      private double MaxFrequency;
      private double MinPower;
      private double MinSumPower;
      private double Range;
      private int typeCalculation;
      private double UnderValue;
      private double K;

      private double TransmitterGain;
      private double TransmitterAzimuth;
      private double TransmitterElevation;
      private double ReceiverGain;
      private double ReceiverAzimuth;
      private double ReceiverElevation;

      //=================================================
      private const double K_Freq = 0.1;   //От [0:1] - апараметр, который определяет как далеко будет находимться по чатоте вторая интермодуляционная помеха
                                           // 0 - на тех же частотах
                                           // 1 - в диапазоне чатот о 0 до 2f
      private const double MAX_FREQ = 999999999999.0;

      //=================================================
      // Вспомагательный конструктор
      //=================================================
      public IntermodulationMainForm()
         : this("", 0)
      {
      }

      public IntermodulationMainForm(string _nameTable, int _id,
         double preselectorGain, double inputStageLinearity, double minFrequency,
         double maxFrequency, double minPower, double minSumPower, double range,
         double k)
      {
         InitializeComponent();

         FrequencyCheckBox.Checked = true;
         FromFrequencyTextBox.Text = minFrequency.ToString();
         ToFrequencyTextBox.Text = maxFrequency.ToString();
         PreselectorCoefficientTextBox.Text = k.ToString();
         MinimalRangeTextBox.Text = range.ToString();

         /////////////////////////////////////////////
         PreselectorGain = preselectorGain;
         InputStageLinearity = inputStageLinearity;
         MinFrequency = minFrequency;
         MaxFrequency = maxFrequency;
         MinPower = minPower;
         MinSumPower = minSumPower;
         Range = range;
         //typeCalculation = TypeCalculation;
         K = k;

         nameTable = _nameTable;
         idStation = _id;
         nameFreqTable = "MOBSTA_FREQS";

         interfencesList = new List<IntermodulationInterference>();
         if (nameTable != "" && idStation > 0)
         {// Есть запись
            List<int> selected = new List<int>();
            selected.Add(idStation);
            singleStationList = selected;
         }
         else
         {// Записи нет, ее необходимо выбрать
            singleStationList = null;
         }
         UprateInformationAboutStation(nameTable, idStation);
         UnderValue = -1E+10;

         treeHash = new Hashtable();

         //_backgroundWork = true;
         TLocaliz.TxT(this);
      }

      public IntermodulationMainForm(string _nameTable, int _id,
         double preselectorGain, double inputStageLinearity, double minFrequency,
         double maxFrequency, double minPower, double minSumPower, double range,
         int TypeCalculation, double k)
      {
         InitializeComponent();
         ///////////////////////////////////////////////////////////
         ///////////////////////////////////////////////////////////
         ///////////////////////////////////////////////////////////
         PreselectorGain = preselectorGain;
         InputStageLinearity = inputStageLinearity;
         MinFrequency = minFrequency;
         MaxFrequency = maxFrequency;
         MinPower = minPower;
         MinSumPower = minSumPower;
         Range = range;
         typeCalculation = TypeCalculation;
         K = k;

         nameTable = _nameTable;
         idStation = _id;
         nameFreqTable = "MOBSTA_FREQS";

         interfencesList = new List<IntermodulationInterference>();
         if (nameTable != "" && idStation > 0)
         {// Есть запись
            List<int> selected = new List<int>();
            selected.Add(idStation);
            singleStationList = selected;
         }
         else
         {// Записи нет, ее необходимо выбрать
            singleStationList = null;
         }
         UprateInformationAboutStation(nameTable, idStation);
         UnderValue = -1E+10;

         treeHash = new Hashtable();

         _backgroundWork = true;
         TLocaliz.TxT(this);
      }

      public List<IntermodulationInterference> GetData()
      {
         return interfencesList;
      }
      //=================================================
      // Главный конструктор
      //=================================================
      public IntermodulationMainForm(string _nameTable, int _id)
      {
         InitializeComponent();
         TLocaliz.TxT(this);
         K = 30.0;
         MinSumPower = 0.0;
         MinPower = 0.0;
         nameTable = _nameTable;
         nameFreqTable = "MOBSTA_FREQS";
         idStation = _id;
         interfencesList = new List<IntermodulationInterference>();
         if (nameTable != "" && idStation > 0)
         {// Есть запись
            List<int> selected = new List<int>();
            selected.Add(idStation);
            singleStationList = selected;
         }
         else
         {// Записи нет, ее необходимо выбрать
            singleStationList = null;
         }
         UprateInformationAboutStation(nameTable, idStation);
         UnderValue = -1E+10;

         treeHash = new Hashtable();
      }

      private void StationListButton_Click(object sender, EventArgs e)
      {
         SelectStationForm stationForm = new SelectStationForm(true, nameTable);
         if (stationForm.ShowDialog() == DialogResult.OK)
         {
            otherStations = stationForm.SelectedId;
         }
      }

      private void ModelButton_Click(object sender, EventArgs e)
      {
         ModelSelectionForm modelSelection = new ModelSelectionForm();
         modelSelection.ShowDialog();
      }

      private void btnSelectStation_Click(object sender, EventArgs e)
      {
         SelectStationForm stationForm = new SelectStationForm(false, nameTable);
         if (stationForm.ShowDialog() == DialogResult.OK)
         {
            singleStationList = stationForm.SelectedId;
            idStation = singleStationList[0];
            UprateInformationAboutStation(nameTable, idStation);
         }
      }

      //===================================================
      // Додсчитываем сумарную мощность
      //===================================================
      private double CalcSumPower(double Power1, double Power2, double PreselGain, double StageLinearity)
      {
         double retVal = 3.0 + ((2.0 * Power1 + Power2) + 3.0 * PreselGain) - 2 * StageLinearity - PreselGain;
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Проверка станции на попадани частот
      /// </summary>
      /// <param name="_table">имя таблицы</param>
      /// <param name="isStat">ID станции</param>
      /// <param name="Fmin">минимальная частота</param>
      /// <param name="Fmax">максимальная частота</param>
      /// <param name="_bandwidth">полоса частот</param>
      /// <param name="isTX">TRUE - если передатчик</param>
      /// <returns></returns>
      private bool CheckFreq(string _table, int _isStat, double Fmin, double Fmax, double _bandwidth, bool isTX)
      {
         bool retVal = false;

         double bandWidthDiv2 = _bandwidth / 2.0;

         IMRecordset rChannel = new IMRecordset(_table, IMRecordset.Mode.ReadOnly);
         rChannel.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
         rChannel.SetWhere("STA_ID", IMRecordset.Operation.Eq, _isStat);

         try
         {
            //int counter = 0;
            for (rChannel.Open(); !rChannel.IsEOF(); rChannel.MoveNext())
            {
               double tmpFreq = 0.0;
               if (isTX)
                  tmpFreq = rChannel.GetD("TX_FREQ");
               else tmpFreq = rChannel.GetD("RX_FREQ");

               if (((Fmin - bandWidthDiv2) <= tmpFreq) && (tmpFreq <= (Fmax + bandWidthDiv2)))
               {
                  retVal = true;
                  break;
               }
            }
         }
         finally
         {
            rChannel.Close();
            rChannel.Dispose();
         }
         return retVal;
      }

      public void FindMethod()
      {
         if (singleStationList.Count == 0)
            return;

         using (CProgressBar pBar = new CProgressBar(TLocaliz.TxT("Intermodulation")))
         {

            int counter = 0;
            Station singleStation = new Station(singleStationList[0]);
            singleStation.Load(nameTable);
            if (typeCalculation == 0)
                 singleStation.LoadChannels(nameFreqTable, 0.0, MAX_FREQ, false, 0.0, 0.0);
            else singleStation.LoadChannels(nameFreqTable, 0.0, MAX_FREQ, true, 0.0, MAX_FREQ);

            if(singleStation.GetStandard() == "УКХ")
            {
               double curFreq = IM.NullD;
               List<Station.Channel> lstFreq = singleStation.GetTransmitters();
               curFreq = (lstFreq.Count > 0) ? lstFreq[0].Frequency : IM.NullD;
               if(curFreq == IM.NullD)
               {
                  lstFreq = singleStation.GetReceivers();
                  curFreq = (lstFreq.Count > 0) ? lstFreq[0].Frequency : IM.NullD;
               }
               //-----
               if ((30 <= curFreq) && (curFreq <= 60))
               {
                  MinFrequency = 30.0;
                  MaxFrequency = 60.0;
               }
               else if ((150 <= curFreq) && (curFreq <= 200))
               {
                  MinFrequency = 150.0;
                  MaxFrequency = 200.0;
               }
               else if ((400 <= curFreq) && (curFreq <= 500))
               {
                  MinFrequency = 400.0;
                  MaxFrequency = 500.0;
               }
            }

            RecordPos positionRadius = Position.GetRangeCoordinates(singleStation.GetPhi(), singleStation.GetPhita(),
                Range, "Position.LONGITUDE", "Position.LATITUDE");

            List<Station> stationList = Station.GetAllStations(nameTable, positionRadius, MinFrequency, MaxFrequency, singleStation.GetStandard());
            List<Station> nearStations = new List<Station>();

            interfencesList.Clear();

            pBar.ShowBig(TLocaliz.TxT("Adding a station..."));
            
            {
               double FmaxAnalizedStation = singleStation.GetMaxFreqTX();
               double FminAnalizedStation = singleStation.GetMinFreqTX();
               FminAnalizedStation -= (K_Freq * FminAnalizedStation);
               FmaxAnalizedStation += (K_Freq * FmaxAnalizedStation);
               counter = 0;

               foreach (Station SecondStation in stationList)
               {
                  //ProgressBar
                  pBar.ShowProgress(counter, stationList.Count);
                  counter++;

                  if (singleStation.RangeToStation(SecondStation) < Range)
                  {
                     bool needAdd = false;

                     if ((typeCalculation == 0) && (CheckFreq(nameFreqTable, SecondStation.GetID(), MinFrequency, MaxFrequency, SecondStation.GetBandwidth(), true)))
                        needAdd = true;
                     else if (typeCalculation == 1)
                     {
                        if ((CheckFreq(nameFreqTable, SecondStation.GetID(), MinFrequency, MaxFrequency, SecondStation.GetBandwidth(), false)) ||
                           (CheckFreq(nameFreqTable, SecondStation.GetID(), FminAnalizedStation, FmaxAnalizedStation, SecondStation.GetBandwidth(), false)))
                           needAdd = true;
                     }
                     if (needAdd)
                     {
                        nearStations.Add(SecondStation);
                        SecondStation.LoadChannels(nameFreqTable, MinFrequency, MaxFrequency, (typeCalculation == 0), FminAnalizedStation, FmaxAnalizedStation);
                        pBar.ShowSmall(nearStations.Count);
                        pBar.UserCanceled();  //Для прорисовки
                     }
                  }
               }
            }

            Int64 allCounter = 0;  //For debug
            Int64 inCounter = 0;   //For debug
            int cicleForStation = 0;

            if (typeCalculation == 0)
            {
               List<Station.Channel> nearChannels = new List<Station.Channel>();
               List<Station.Channel> allChannels = new List<Station.Channel>();

               pBar.ShowBig(TLocaliz.TxT("Calculating..."));
               counter = 0;

               foreach (Station NearStation1 in nearStations)
               {
                  pBar.ShowProgress(counter, nearStations.Count);
                  counter++;
                  cicleForStation = 0;
                  if (pBar.UserCanceled())
                     break;
                  if (NearStation1._isOK == false)
                     continue;

                  foreach (Station NearStation2 in nearStations)
                     foreach (Station.Channel Transmitter1 in NearStation1.GetTransmitters())
                        foreach (Station.Channel Transmitter2 in NearStation2.GetTransmitters())
                           foreach (Station.Channel Receiver in singleStation.GetReceivers())
                           {
                              pBar.ShowSmall(++cicleForStation);
                              pBar.UserCanceled();
                              allCounter++;

                              if (NearStation1.GetID() != singleStation.GetID() && NearStation2.GetID() != singleStation.GetID() && Transmitter1.ID != Transmitter2.ID
                                  && Transmitter1.Frequency >= MinFrequency && Transmitter2.Frequency >= MinFrequency && Receiver.Frequency >= 0.001
                                  && Transmitter1.Frequency <= MaxFrequency && Transmitter2.Frequency <= MaxFrequency)
                              {
                                 inCounter++;
                                 if (IntermodulationInterference(Transmitter1.Frequency, Transmitter2.Frequency, Receiver.Frequency, NearStation1.GetBandwidth(), NearStation2.GetBandwidth(), singleStation.GetBandwidth()))
                                 {

                                    IntermodulationInterference interference = new IntermodulationInterference();
                                    interference.Preselector1 = PreselectorPower(Transmitter1, Receiver);
                                    interference.Power1 = IntermodulationPower(NearStation1, Transmitter1, singleStation, Receiver);

                                    interference.TransmitterGain1 = TransmitterGain;
                                    interference.TransmitterAzimuth1 = TransmitterAzimuth;
                                    interference.TransmitterElevation1 = TransmitterElevation;
                                    interference.ReceiverGain1 = ReceiverGain;
                                    interference.ReceiverAzimuth1 = ReceiverAzimuth;
                                    interference.ReceiverElevation1 = ReceiverElevation;

                                    interference.Preselector2 = PreselectorPower(Transmitter2, Receiver);
                                    interference.Power2 = IntermodulationPower(NearStation2, Transmitter2, singleStation, Receiver);
                                    interference.SumPower = CalcSumPower(interference.Power1, interference.Power2, PreselectorGain, InputStageLinearity);

                                    interference.TransmitterGain2 = TransmitterGain;
                                    interference.TransmitterAzimuth2 = TransmitterAzimuth;
                                    interference.TransmitterElevation2 = TransmitterElevation;
                                    interference.ReceiverGain2 = ReceiverGain;
                                    interference.ReceiverAzimuth2 = ReceiverAzimuth;
                                    interference.ReceiverElevation2 = ReceiverElevation;

                                    interference.Transmitter1 = Transmitter1;
                                    interference.Transmitter2 = Transmitter2;
                                    interference.Receiver = Receiver;
                                    interference.CalculateRanges();

                                    if (MinPower < interference.Power1 && MinPower < interference.Power2
                                        && MinSumPower < interference.SumPower)
                                    {
                                       interfencesList.Add(interference);
                                    }
                                 }
                              }
                           }
               }
            }
            else if (typeCalculation == 1)
            {
               pBar.ShowBig(TLocaliz.TxT("Calculating..."));
               counter = 0;
               foreach (Station NearStation1 in nearStations)
               {
                  pBar.ShowProgress(counter, nearStations.Count);
                  counter++;
                  cicleForStation = 0;
                  if (pBar.UserCanceled())
                     break;

                  if (NearStation1._isOK == false)
                     continue;

                  foreach (Station NearStation2 in nearStations)
                     foreach (Station.Channel Transmitter1 in NearStation1.GetTransmitters())
                        foreach (Station.Channel Receiver in NearStation2.GetReceivers())
                           foreach (Station.Channel Transmitter2 in singleStation.GetTransmitters())
                           {
                              pBar.ShowSmall(++cicleForStation);
                              pBar.UserCanceled();
                              allCounter++;

                              if (NearStation1.GetID() != NearStation2.GetID() && NearStation2.GetID() != singleStation.GetID() && Transmitter1.ID != Transmitter2.ID
                                 && Transmitter1.Frequency >= MinFrequency && Transmitter2.Frequency >= MinFrequency && Receiver.Frequency >= 0.001
                                 && Transmitter1.Frequency <= MaxFrequency)
                              {
                                 inCounter++;
                                 if (IntermodulationInterference(Transmitter1.Frequency, Transmitter2.Frequency, Receiver.Frequency, NearStation1.GetBandwidth(), singleStation.GetBandwidth(), NearStation2.GetBandwidth()))
                                 {
                                    IntermodulationInterference interference = new IntermodulationInterference();
                                    interference.Preselector1 = PreselectorPower(Transmitter1, Receiver);
                                    interference.Power1 = IntermodulationPower(NearStation1, Transmitter1, NearStation2, Receiver);

                                    interference.TransmitterGain1 = TransmitterGain;
                                    interference.TransmitterAzimuth1 = TransmitterAzimuth;
                                    interference.TransmitterElevation1 = TransmitterElevation;
                                    interference.ReceiverGain1 = ReceiverGain;
                                    interference.ReceiverAzimuth1 = ReceiverAzimuth;
                                    interference.ReceiverElevation1 = ReceiverElevation;

                                    interference.Preselector2 = PreselectorPower(Transmitter2, Receiver);
                                    interference.Power2 = IntermodulationPower(singleStation, Transmitter2, NearStation2, Receiver);
                                    interference.SumPower = CalcSumPower(interference.Power1, interference.Power2, PreselectorGain, InputStageLinearity);

                                    interference.TransmitterGain2 = TransmitterGain;
                                    interference.TransmitterAzimuth2 = TransmitterAzimuth;
                                    interference.TransmitterElevation2 = TransmitterElevation;
                                    interference.ReceiverGain2 = ReceiverGain;
                                    interference.ReceiverAzimuth2 = ReceiverAzimuth;
                                    interference.ReceiverElevation2 = ReceiverElevation;

                                    interference.Transmitter1 = Transmitter1;
                                    interference.Transmitter2 = Transmitter2;
                                    interference.Receiver = Receiver;
                                    interference.CalculateRanges();

                                    if (MinPower < interference.Power1 && MinPower < interference.Power2
                                        && MinSumPower < interference.SumPower)
                                       interfencesList.Add(interference);
                                 }

                                 if (IntermodulationInterference(Transmitter2.Frequency, Transmitter1.Frequency, Receiver.Frequency, singleStation.GetBandwidth(), NearStation1.GetBandwidth(), NearStation2.GetBandwidth()))
                                 {
                                    IntermodulationInterference interference = new IntermodulationInterference();

                                    interference.Preselector2 = PreselectorPower(Transmitter2, Receiver);
                                    interference.Power2 = IntermodulationPower(NearStation1, Transmitter1, NearStation2, Receiver);

                                    interference.TransmitterGain2 = TransmitterGain;
                                    interference.TransmitterAzimuth2 = TransmitterAzimuth;
                                    interference.TransmitterElevation2 = TransmitterElevation;
                                    interference.ReceiverGain2 = ReceiverGain;
                                    interference.ReceiverAzimuth2 = ReceiverAzimuth;
                                    interference.ReceiverElevation2 = ReceiverElevation;

                                    interference.Preselector1 = PreselectorPower(Transmitter1, Receiver);
                                    interference.Power1 = IntermodulationPower(singleStation, Transmitter2, NearStation2, Receiver);
                                    interference.SumPower = CalcSumPower(interference.Power1, interference.Power2, PreselectorGain, InputStageLinearity);

                                    interference.TransmitterGain1 = TransmitterGain;
                                    interference.TransmitterAzimuth1 = TransmitterAzimuth;
                                    interference.TransmitterElevation1 = TransmitterElevation;
                                    interference.ReceiverGain1 = ReceiverGain;
                                    interference.ReceiverAzimuth1 = ReceiverAzimuth;
                                    interference.ReceiverElevation1 = ReceiverElevation;

                                    interference.Transmitter1 = Transmitter2;
                                    interference.Transmitter2 = Transmitter1;
                                    interference.Receiver = Receiver;
                                    interference.CalculateRanges();

                                    if (MinPower < interference.Power1 && MinPower < interference.Power2
                                        && MinSumPower < interference.SumPower)
                                       interfencesList.Add(interference);
                                 }
                              }
                           }
               }
            }
            else
            {// Неверный тип
               MessageBox.Show("You can't calculate this type!!!");
            }

            if (pBar.UserCanceled())
               MessageBox.Show(TLocaliz.TxT("Aborted by user"));
            //MessageBox.Show("All = " + allCounter.ToString() + "   in=" + inCounter.ToString());

            interfencesList.Sort();
            FillTree(interfencesList);
            stationList.Clear();
            nearStations.Clear();
         }
      }

      private void FillTree(List<IntermodulationInterference> InterferenceList)
      {
         if (!_backgroundWork)
            ResultTreeView.Invoke(new TreeViewDraw(TreeViewDrawDisable));
         try
         {
            treeHash.Clear();
            Hashtable hashTable = new Hashtable();
            int Step = 0;
            SetMessageThreadSafe(TLocaliz.TxT("Creating list of stations..."));
            SetProgressMaxThreadSafe(InterferenceList.Count);
            SetProgressValueThreadSafe(Step);

            foreach (IntermodulationInterference Interference in InterferenceList)
            {
               string tmpStr;
               if (hashTable.ContainsKey(Interference.Receiver))
               {
                  ReceiverMainNode = hashTable[Interference.Receiver] as TreeNode;
               }
               else
               {
                  if (!_backgroundWork)
                  {
                     tmpStr = TLocaliz.TxT(" Frequency") + " = " + Interference.Receiver.Frequency.ToString("0.000 MHz ");
                     ResultTreeView.Invoke(new AddNewNode(AddNewMainNode), new object[] { tmpStr });
                  }
                  hashTable.Add(Interference.Receiver, ReceiverMainNode);

                  if (!treeHash.ContainsKey(ReceiverMainNode))
                  {
                     ReceiverInfo ReceiverInfo = new ReceiverInfo();
                     ReceiverInfo.Receiver = Interference.Receiver;
                     ReceiverInfo.countStation = 0;
                     ReceiverInfo.maxInterference = 0.0;
                     treeHash.Add(ReceiverMainNode, ReceiverInfo);
                  }
               }

               if (!_backgroundWork)
               {
                  tmpStr = TLocaliz.TxT("Interference Overall Power") + " = " + Interference.SumPower.ToString("0.000 dBm");
                  ResultTreeView.Invoke(new AddNewNode(AddNewSubNode), new object[] { tmpStr });
               }

               ReceiverInfo ReceiverInfoUpdate = treeHash[ReceiverMainNode] as ReceiverInfo;
               // Поиск максимального значениея помехи
               if (ReceiverInfoUpdate.countStation == 0)
                  ReceiverInfoUpdate.maxInterference = Interference.SumPower;
               else if (ReceiverInfoUpdate.maxInterference < Interference.SumPower)
                  ReceiverInfoUpdate.maxInterference = Interference.SumPower;
               ReceiverInfoUpdate.countStation++;  //Подсчет кол-ва помех

               if (!treeHash.ContainsKey(InterferenceMainNode))
               {
                  IntermodulationInterferenceInfo InterefenceInfo = new IntermodulationInterferenceInfo();
                  InterefenceInfo.Interference = Interference;
                  treeHash.Add(InterferenceMainNode, InterefenceInfo);
               }

               double Range = Interference.Transmitter1.Station.RangeToStation(Interference.Receiver.Station);
               Interference.Range1 = Range;

               if (!_backgroundWork)
               {
                  tmpStr = TLocaliz.TxT("Transmitter") + " (P=" + Interference.Power1.ToString("0.000 dBm") + ")" +
                           TLocaliz.TxT(" Frequency") + " = " + Interference.Transmitter1.Frequency.ToString("0.000 MHz ");// +

                  ResultTreeView.Invoke(new AddNewNode(AddNewSubSubNode), new object[] { tmpStr });
               }

               if (!treeHash.ContainsKey(NewNode))
               {
                  InterefenceInfo InterefenceInfo = new InterefenceInfo();
                  InterefenceInfo.Range = Range;
                  InterefenceInfo.Power = Interference.Power1;
                  InterefenceInfo.TransmitterAzimuth = Interference.TransmitterAzimuth1;
                  InterefenceInfo.TransmitterElevation = Interference.TransmitterElevation1;
                  InterefenceInfo.TransmitterGain = Interference.TransmitterGain1;
                  InterefenceInfo.ReceiverAzimuth = Interference.ReceiverAzimuth1;
                  InterefenceInfo.ReceiverElevation = Interference.ReceiverElevation1;
                  InterefenceInfo.ReceiverGain = Interference.ReceiverGain1;
                  InterefenceInfo.Tranmitter = Interference.Transmitter1;
                  InterefenceInfo.Preselector = Interference.Preselector1;
                  treeHash.Add(NewNode, InterefenceInfo);
               }

               Range = Interference.Transmitter2.Station.RangeToStation(Interference.Receiver.Station);
               Interference.Range2 = Range;

               if (!_backgroundWork)
               {
                  tmpStr = TLocaliz.TxT("Transmitter") + " (P=" + Interference.Power2.ToString("0.000 dBm") + ")" +
                           TLocaliz.TxT(" Frequency") + " = " + Interference.Transmitter2.Frequency.ToString("0.000 MHz ");// +
                  ResultTreeView.Invoke(new AddNewNode(AddNewSubSubNode), new object[] { tmpStr });
               }

               if (!treeHash.ContainsKey(NewNode))
               {
                  InterefenceInfo InterefenceInfo = new InterefenceInfo();
                  InterefenceInfo.Range = Range;
                  InterefenceInfo.Power = Interference.Power2;
                  InterefenceInfo.TransmitterAzimuth = Interference.TransmitterAzimuth2;
                  InterefenceInfo.TransmitterElevation = Interference.TransmitterElevation2;
                  InterefenceInfo.TransmitterGain = Interference.TransmitterGain2;
                  InterefenceInfo.ReceiverAzimuth = Interference.ReceiverAzimuth2;
                  InterefenceInfo.ReceiverElevation = Interference.ReceiverElevation2;
                  InterefenceInfo.ReceiverGain = Interference.ReceiverGain2;
                  InterefenceInfo.Tranmitter = Interference.Transmitter2;
                  InterefenceInfo.Preselector = Interference.Preselector2;
                  treeHash.Add(NewNode, InterefenceInfo);
               }
                SetProgressValueThreadSafe(++Step);               
            }

            SetMessageThreadSafe(TLocaliz.TxT("Completed"));
            ExportToExcelButton.Enabled = (ResultTreeView.Nodes.Count != 0);
            hashTable.Clear();

            // Обновляем строки приемнико
            foreach (DictionaryEntry de in treeHash)
            {
               ReceiverInfo ReceiverInfoShow = de.Value as ReceiverInfo;
               if (ReceiverInfoShow != null)
               {
                  TreeNode ReceiverNode = de.Key as TreeNode;
                  string tmpStr = ReceiverNode.Text +
                                  " " + TLocaliz.TxT("Count") + "=" + ReceiverInfoShow.countStation.ToString() +
                                  " " + TLocaliz.TxT("Max") + "=" + ReceiverInfoShow.maxInterference.ToString("0.000 dBm");
                  ReceiverNode.Text = tmpStr;
               }
            }
         }
         catch
         {
            
         }
         finally
         {
            if (!_backgroundWork)
               ResultTreeView.Invoke(new TreeViewDraw(TreeViewDrawEnable));
         }
      }

      public bool IntermodulationInterference(double Frequency1, double Frequency2, double Frequency3, double Bandwidth1, double Bandwidth2, double Bandwidth3)
      {
         double diff1 = 2.0 * Frequency1 - Frequency2 - Frequency3;
         // It will be at next iteration
         //double diff2 = 2.0 * Frequency2 - Frequency1 - Frequency3;

         if (System.Math.Abs(diff1) < (Bandwidth1 + Bandwidth2/2.0 + Bandwidth3/2.0))
         {

            return true;
         }

         return false;
      }

      private double CalculateIntermodilationPower(Station Station1, Station Station2, Station SingleStation, Station.Channel Transmitter1, Station.Channel Transmitter2, Station.Channel Receiver)
      {
         double P1 = IntermodulationPower(Station1, Transmitter1, SingleStation, Receiver);
         double P2 = IntermodulationPower(Station2, Transmitter2, SingleStation, Receiver);

         if (P1 == UnderValue && P2 == UnderValue)
            return UnderValue;

         if (P1 == UnderValue && P2 > UnderValue)
            return P2;

         if (P1 > UnderValue && P2 == UnderValue)
            return P1;

         return 20.0 * System.Math.Log10(System.Math.Pow(10, P1 / 20.0) + System.Math.Pow(10, P2 / 20.0));
      }

      private double DifussionLoss(double Frequency, double Range)
      {
         double retVal;
         if (((-0.0000001 < Frequency) && (Frequency < 0.0000001)) ||
            ((-0.0000001 < Range) && (Range < 0.0000001)))
            retVal = 60.0;
         else
            retVal = 92.5 + 20.0 * System.Math.Log10(Frequency / 1000.0) + 20.0 * System.Math.Log10(Range);

         return retVal;
      }

      private double OldPreselectorPower(Station.Channel Transmitter, Station.Channel Receiver)
      {
          double DeltaFrequency = System.Math.Abs((Transmitter.Frequency - Receiver.Frequency) / 2.0);
          double Brf = K * Receiver.Station.GetBandwidth();
          double Beta = 60.0 * System.Math.Log10(1.0 + (2.0 * DeltaFrequency / Brf) * (2.0 * DeltaFrequency / Brf));

          if (Brf == 0)
              Beta = 0;

          return Beta;
      }

      private double PreselectorPower(Station.Channel Transmitter, Station.Channel Receiver)
      {
         /*double DeltaFrequency = System.Math.Abs((Transmitter.Frequency - Receiver.Frequency) / 2.0);
         double Brf = K * Receiver.Station.GetBandwidth();
         double Beta = 60.0 * System.Math.Log10(1.0 + (2.0 * DeltaFrequency / Brf) * (2.0 * DeltaFrequency / Brf));

         if (Brf == 0)
            Beta = 0;*/
         double Beta = 0;
         short operationNumber = 0;

         if (Receiver.Station._filterR.Count > 0 && Transmitter.Station._filterT.Count > 0)
         {
            operationNumber = 0;            
         }
         else
         {
             if (Receiver.Station._filterR.Count == 0 && Transmitter.Station._filterT.Count == 0)
             {
                 operationNumber = 1;
             }
             else
             {
                 if (Receiver.Station._filterR.Count > 0)
                 {
                     operationNumber = 0;
                 }
                 else
                 {
                     operationNumber = 2;
                 }
             }
         }

          if (operationNumber == 0)
            {
               Beta = LisUtility.Mob.ACHMob.GetIRF(Transmitter.Frequency, Receiver.Frequency,
                           Transmitter.Station._freqFilterTX, Receiver.Station._freqFilterRX,
                           Transmitter.Bandwidth, true, Transmitter.Station._maskaT,
                           Receiver.Station._maskaR, Transmitter.Station._filterT,
                           Receiver.Station._filterR);
            }
            if (operationNumber == 1)
            {
               Beta = OldPreselectorPower(Receiver, Transmitter);
            }
            if (operationNumber == 2)
            {
               Beta = OldPreselectorPower(Receiver, Transmitter);
               double Beta1 = LisUtility.Mob.ACHMob.GetIRF(Transmitter.Frequency, Receiver.Frequency,
                           Transmitter.Station._freqFilterTX, Receiver.Station._freqFilterRX,
                           Transmitter.Bandwidth, true, Transmitter.Station._maskaT,
                           Receiver.Station._maskaR, Transmitter.Station._filterT,
                           Receiver.Station._filterR);

               Beta += Beta1;
            }
            
         

         return Beta;
      }

      private double IntermodulationPower(Station TransmitterStation, Station.Channel Transmitter, Station ReceiverStation, Station.Channel Receiver)
      {
         double Range = TransmitterStation.RangeToStation(ReceiverStation);

         if (Range > -0.00001)
         {
            double tan = TransmitterStation.GetHorizontalAngle(ReceiverStation);
            ReceiverAzimuth = XICSM.Math.RadianToDegree(TransmitterStation.GetHorizontalAngle(ReceiverStation));
            TransmitterAzimuth = XICSM.Math.RadianToDegree(ReceiverStation.GetHorizontalAngle(TransmitterStation));

            if ((-0.0000001 < Range) && (Range < 0.0000001))
               TransmitterElevation = 90.0;
            else
               TransmitterElevation = XICSM.Math.RadianToDegree(TransmitterStation.GetVerticalAngle(ReceiverStation));

            TransmitterGain = TransmitterStation.GetLosses(TransmitterAzimuth - TransmitterStation.GetAzimuth(), TransmitterElevation - TransmitterStation.GetElevation());

            if ((-0.0000001 < Range) && (Range < 0.0000001))
               ReceiverElevation = 90.0;
            else
               ReceiverElevation = XICSM.Math.RadianToDegree(ReceiverStation.GetVerticalAngle(TransmitterStation));
            
            ReceiverGain = ReceiverStation.GetLosses(ReceiverAzimuth - ReceiverStation.GetAzimuth(), ReceiverElevation - ReceiverStation.GetElevation());

            double Diffusion = DifussionLoss(Range, Transmitter.Frequency);
            double Power = TransmitterStation.GetPower();


            double Beta = PreselectorPower(Transmitter, Receiver);

            return Power - TransmitterStation.GetTxLosses() + TransmitterGain - Diffusion - ReceiverStation.GetRxLosses() + ReceiverGain - Beta;
         }
         else
            return UnderValue;
      }

      private void UprateInformationAboutStation(string _nameTable, int _id)
      {
         tbAboutSelectStation.Clear();
         if (_nameTable == "" || _id == 0)
            return;  // Выходим, так как нечего отображать
         IMRecordset rStations = new IMRecordset(_nameTable, IMRecordset.Mode.ReadOnly);
         rStations.Select("ID,NAME,AZIMUTH,ELEVATION,TX_LOSSES,RX_LOSSES,AGL,Owner.NAME,DESIG_EMISSION,Position.X,Position.Y,Position.CSYS,Antenna.DIAGH,Antenna.DIAGV,Equipment.KTBF");
         rStations.SetWhere("ID", IMRecordset.Operation.Eq, _id);
         rStations.Open();
         try
         {
            if (!rStations.IsEOF())
            {
               tbAboutSelectStation.Text = "ID=" + rStations.GetI("ID").ToString()
                                           + "\r\nNAME=" + rStations.GetS("NAME")
                                           + "\r\nX=" + rStations.GetD("Position.X").ToString()
                                           + "\r\nY=" + rStations.GetD("Position.Y").ToString()
                                           + "\r\nOWNER=" + rStations.GetS("Owner.NAME");
               double ktbf = rStations.GetD("Equipment.KTBF");
               if (ktbf == 1.0E-99)
                  MinSumPowerEdit.Text = "0";
               else MinSumPowerEdit.Text = ktbf.ToString();
            }
         }
         finally
         {
            rStations.Close();
            rStations.Dispose();
         }
      }

      private void FrequencyCheckBox_CheckedChanged(object sender, EventArgs e)
      {
         if (FrequencyCheckBox.Checked)
         {
            FromFrequencyTextBox.Enabled = true;
            ToFrequencyTextBox.Enabled = true;
            ToTitleLabel.Enabled = true;
         }
         else
         {
            FromFrequencyTextBox.Enabled = false;
            ToFrequencyTextBox.Enabled = false;
            ToTitleLabel.Enabled = false;
         }
      }

      private void ResultTreeView_DoubleClick(object sender, EventArgs e)
      {
         TreeNode SelectedNode = ResultTreeView.SelectedNode;
         object value = treeHash[SelectedNode];

         if (value is ReceiverInfo)
         {
            ReceiverInfo ReceiverInfo = value as ReceiverInfo;
            RecordPtr mobStationRecord = new RecordPtr(nameTable, ReceiverInfo.Receiver.Station.GetID());
            mobStationRecord.UserEdit();
         }

         if (value is InterefenceInfo)
         {
            InterefenceInfo InterferenceInfo = value as InterefenceInfo;
            RecordPtr mobStationRecord = new RecordPtr(nameTable, InterferenceInfo.Tranmitter.Station.GetID());
            mobStationRecord.UserEdit();
         }
      }

      private void btnFind_Click(object sender, EventArgs e)
      {
         // Проверка параметров
         if ((PreselectorTextBox.Text == "")
             || (InputStageLinearityTextBox.Text == "")
             || (FrequencyCheckBox.Checked && ((FromFrequencyTextBox.Text == "") || (ToFrequencyTextBox.Text == "")))
             || (MinimalInterferenceTextBox.Text == "")
             || (MinimalRangeTextBox.Text == "")
             || (SelectCalculationComboBox.SelectedIndex < 0)
             || (MinSumPowerEdit.Text == "")
            )
         {// Не все данные ввели
            MessageBox.Show(TLocaliz.TxT("You didn't fill all fields"), TLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Question);
            return;
         }
         // Загружаем данные из окна
         try
         {
            PreselectorGain = Convert.ToDouble(PreselectorTextBox.Text);
            InputStageLinearity = Convert.ToDouble(InputStageLinearityTextBox.Text);
            MinFrequency = 0.001;
            MaxFrequency = 1E+20;
            if (FrequencyCheckBox.Checked)
            {
               MinFrequency = Convert.ToDouble(FromFrequencyTextBox.Text);
               MaxFrequency = Convert.ToDouble(ToFrequencyTextBox.Text);
            }
            MinPower = Convert.ToDouble(MinimalInterferenceTextBox.Text);
            MinSumPower = Convert.ToDouble(MinSumPowerEdit.Text);
            Range = Convert.ToDouble(MinimalRangeTextBox.Text);
            typeCalculation = SelectCalculationComboBox.SelectedIndex;
            K = Convert.ToDouble(PreselectorCoefficientTextBox.Text);
         }
         catch
         {
            MessageBox.Show(TLocaliz.TxT("Invalidate data"), TLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Question);
            return;
         }
         if (MinFrequency >= MaxFrequency)
         {
            MessageBox.Show(TLocaliz.TxT("Incorrect frequency range"), TLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Question);
            return;
         }
         // Все ОК
         if (WorkerThread.IsBusy)
         {
            MessageBox.Show(TLocaliz.TxT("You can't run the calculation. The calculation is doing now."), TLocaliz.TxT("Start calculation"));
            return;
         }
         ResultTreeView.Nodes.Clear();   //Очищаем список
         //WorkerThread.RunWorkerAsync();
         FindMethod();
      }

      private void IntermodulationMainForm_FormClosing(object sender, FormClosingEventArgs e)
      {
         if (WorkerThread.IsBusy)
            e.Cancel = true;
         else e.Cancel = false;
      }

      private void ResultTreeView_AfterSelect(object sender, TreeViewEventArgs e)
      {
         object value = treeHash[e.Node];

         if (value is ReceiverInfo)
         {
            ReceiverInfo ReceiverInfo = value as ReceiverInfo;

            int Count = e.Node.Nodes.Count;
            string tmpStr = TLocaliz.TxT("Intermodilation interference count") + " = " + Count.ToString() + "\r\n\r\n" +
                            TLocaliz.TxT("Receiver ID") + " = " + ReceiverInfo.Receiver.ID.ToString() + "\r\n" +
                            TLocaliz.TxT("Frequency") + " = " + ReceiverInfo.Receiver.Frequency.ToString("0.000 MHz ") + "\r\n\r\n" +
                            TLocaliz.TxT("Station") + " = " + ReceiverInfo.Receiver.Station.GetName() + "\r\n" +
                            TLocaliz.TxT("Owner") + " = " + ReceiverInfo.Receiver.Station.GetOwner() + "\r\n" +
                            TLocaliz.TxT("Station ID") + " = " + ReceiverInfo.Receiver.Station.GetID();

            DetailsTextBox.Text = tmpStr;
         }

         if (value is IntermodulationInterferenceInfo)
         {
            IntermodulationInterferenceInfo IntermodulationInterferenceInfo = value as IntermodulationInterferenceInfo;

            string tmpStr = TLocaliz.TxT("Power") + " = " + IntermodulationInterferenceInfo.Interference.SumPower.ToString("0.000 dBm") + "\r\n" +
                  TLocaliz.TxT("Excess") + " = " + (IntermodulationInterferenceInfo.Interference.SumPower - MinSumPower).ToString("0.000 dB");

            DetailsTextBox.Text = tmpStr;
         }

         if (value is InterefenceInfo)
         {
            InterefenceInfo InterferenceInfo = value as InterefenceInfo;

            string tmpStr = TLocaliz.TxT("Transmitter Interference Power") + " = " + InterferenceInfo.Power.ToString("0.000 dBm") + "\r\n" +
            TLocaliz.TxT("Transmitter ID") + " = " + InterferenceInfo.Tranmitter.ID.ToString() + "\r\n" +
            TLocaliz.TxT("Frequency") + " = " + InterferenceInfo.Tranmitter.Frequency.ToString("0.000 MHz ") + "\r\n\r\n" +
            TLocaliz.TxT("Range") + " = " + InterferenceInfo.Range.ToString("0.000 km") + "\r\n" +
            TLocaliz.TxT("Preselector") + " = " + InterferenceInfo.Preselector.ToString("0.000 dB") + "\r\n\r\n" +
            TLocaliz.TxT("Transmitter Azimuth") + " = " + InterferenceInfo.TransmitterAzimuth.ToString("0.000°") + "\r\n" +
            TLocaliz.TxT("Transmitter Elevation") + " = " + InterferenceInfo.TransmitterElevation.ToString("0.000°") + "\r\n" +
            TLocaliz.TxT("Transmitter Gain") + " = " + InterferenceInfo.TransmitterGain.ToString("0.000 db") + "\r\n\r\n" +
            TLocaliz.TxT("Receiver Azimuth") + " = " + InterferenceInfo.ReceiverAzimuth.ToString("0.000°") + "\r\n" +
            TLocaliz.TxT("Receiver Elevation") + " = " + InterferenceInfo.ReceiverElevation.ToString("0.000°") + "\r\n" +
            TLocaliz.TxT("Receiver Gain") + " = " + InterferenceInfo.ReceiverGain.ToString("0.000 db") + "\r\n\r\n" +
            TLocaliz.TxT("Station Name") + " = " + InterferenceInfo.Tranmitter.Station.GetName() + "\r\n" +
            TLocaliz.TxT("Owner") + " = " + InterferenceInfo.Tranmitter.Station.GetOwner() + "\r\n" +
            TLocaliz.TxT("Station ID") + " = " + InterferenceInfo.Tranmitter.Station.GetID();

            DetailsTextBox.Text = tmpStr;
         }
      }

      private void ExportToExcelButton_Click(object sender, EventArgs e)
      {
         Cursor.Current = Cursors.WaitCursor;
         try
         {
            //if (ExcelSaveFileDialog.ShowDialog() == DialogResult.OK)
            //{            
            ApplicationClass appExcel = new ApplicationClass();
            Workbook Workbook = appExcel.Workbooks.Add(Type.Missing);
            Worksheet Worksheet = Workbook.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing) as Worksheet;

            Worksheet.Cells[2, 1] = TLocaliz.TxT("Standard");
            Worksheet.Cells[2, 2] = TLocaliz.TxT("Name");
            Worksheet.Cells[2, 3] = TLocaliz.TxT("Owner");
            Worksheet.Cells[2, 4] = TLocaliz.TxT("Address");
            Worksheet.Cells[2, 5] = TLocaliz.TxT("Frequency");
            Worksheet.Cells[2, 6] = TLocaliz.TxT("Sensitivity");
            Worksheet.Cells[2, 7] = TLocaliz.TxT("Bandwidth");
            Worksheet.Cells[2, 8] = TLocaliz.TxT("Height");
            Worksheet.Cells[2, 9] = TLocaliz.TxT("Azimuth");
            Worksheet.Cells[2, 10] = TLocaliz.TxT("Azimuth to P1");
            Worksheet.Cells[2, 11] = TLocaliz.TxT("Azimuth to P2");
            Worksheet.Cells[2, 12] = TLocaliz.TxT("Elevation");
            Worksheet.Cells[2, 13] = TLocaliz.TxT("Elevation to P1");
            Worksheet.Cells[2, 14] = TLocaliz.TxT("Elevation to P2");
            Worksheet.Cells[2, 15] = TLocaliz.TxT("Antenna Gain");
            Worksheet.Cells[2, 16] = TLocaliz.TxT("Antenna Gain to P1");
            Worksheet.Cells[2, 17] = TLocaliz.TxT("Antenna Gain to P2");
            Worksheet.Cells[2, 18] = TLocaliz.TxT("Phider Losses");
            Worksheet.Cells[2, 19] = TLocaliz.TxT("Losses for P1");
            Worksheet.Cells[2, 20] = TLocaliz.TxT("Losses for P2");

            Worksheet.Cells[2, 21] = TLocaliz.TxT("Standard");
            Worksheet.Cells[2, 22] = TLocaliz.TxT("Name");
            Worksheet.Cells[2, 23] = TLocaliz.TxT("Owner");
            Worksheet.Cells[2, 24] = TLocaliz.TxT("Address");
            Worksheet.Cells[2, 25] = TLocaliz.TxT("Frequency");
            Worksheet.Cells[2, 26] = TLocaliz.TxT("Radiated Power");
            Worksheet.Cells[2, 27] = TLocaliz.TxT("Bandwidth");
            Worksheet.Cells[2, 28] = TLocaliz.TxT("Height");
            Worksheet.Cells[2, 29] = TLocaliz.TxT("Azimuth");
            Worksheet.Cells[2, 30] = TLocaliz.TxT("Azimuth to P1");
            Worksheet.Cells[2, 31] = TLocaliz.TxT("Elevation");
            Worksheet.Cells[2, 32] = TLocaliz.TxT("Elevation to P1");
            Worksheet.Cells[2, 33] = TLocaliz.TxT("Antenna Gain");
            Worksheet.Cells[2, 34] = TLocaliz.TxT("Antenna Gain to P1");
            Worksheet.Cells[2, 35] = TLocaliz.TxT("Phider Losses");
            Worksheet.Cells[2, 36] = TLocaliz.TxT("Stranges Losses");
            Worksheet.Cells[2, 37] = TLocaliz.TxT("Range");
            Worksheet.Cells[2, 38] = TLocaliz.TxT("Power P1");


            Worksheet.Cells[2, 39] = TLocaliz.TxT("Standard");
            Worksheet.Cells[2, 40] = TLocaliz.TxT("Name");
            Worksheet.Cells[2, 41] = TLocaliz.TxT("Owner");
            Worksheet.Cells[2, 42] = TLocaliz.TxT("Address");
            Worksheet.Cells[2, 43] = TLocaliz.TxT("Frequency");
            Worksheet.Cells[2, 44] = TLocaliz.TxT("Radiated Power");
            Worksheet.Cells[2, 45] = TLocaliz.TxT("Bandwidth");
            Worksheet.Cells[2, 48] = TLocaliz.TxT("Height");
            Worksheet.Cells[2, 47] = TLocaliz.TxT("Azimuth");
            Worksheet.Cells[2, 48] = TLocaliz.TxT("Azimuth to P2");
            Worksheet.Cells[2, 49] = TLocaliz.TxT("Elevation");
            Worksheet.Cells[2, 50] = TLocaliz.TxT("Elevation to P2");
            Worksheet.Cells[2, 51] = TLocaliz.TxT("Antenna Gain");
            Worksheet.Cells[2, 52] = TLocaliz.TxT("Antenna Gain to P2");
            Worksheet.Cells[2, 53] = TLocaliz.TxT("Phider Losses");
            Worksheet.Cells[2, 54] = TLocaliz.TxT("Stranges Losses");
            Worksheet.Cells[2, 55] = TLocaliz.TxT("Range");
            Worksheet.Cells[2, 56] = TLocaliz.TxT("Power P2");
            Worksheet.Cells[2, 57] = TLocaliz.TxT("Summ Power");
            Worksheet.Cells[2, 58] = TLocaliz.TxT("Threshold");
            Worksheet.Cells[2, 59] = TLocaliz.TxT("Difference");
            int Row = 3;

            foreach (IntermodulationInterference Interference in interfencesList)
            {
               Worksheet.Cells[Row, 1] = Interference.Receiver.Station.GetStandard();
               Worksheet.Cells[Row, 2] = Interference.Receiver.Station.GetName();
               Worksheet.Cells[Row, 3] = Interference.Receiver.Station.GetOwner();
               Worksheet.Cells[Row, 4] = Interference.Receiver.Station.GetAddress();
               Worksheet.Cells[Row, 5] = Interference.Receiver.Frequency;
               Worksheet.Cells[Row, 6] = Interference.ReceiverSensitivity;
               Worksheet.Cells[Row, 7] = Interference.Receiver.Station.GetBandwidth();
               Worksheet.Cells[Row, 8] = Interference.Receiver.Station.GetHeight();
               Worksheet.Cells[Row, 9] = Interference.Receiver.Station.GetAzimuth();
               Worksheet.Cells[Row, 10] = Interference.TransmitterAzimuth1;
               Worksheet.Cells[Row, 11] = Interference.TransmitterAzimuth2;
               Worksheet.Cells[Row, 12] = Interference.Receiver.Station.GetElevation();
               Worksheet.Cells[Row, 13] = Interference.ReceiverElevation1;
               Worksheet.Cells[Row, 14] = Interference.ReceiverElevation2;
               Worksheet.Cells[Row, 15] = Interference.Receiver.Station.GetMaxGain();
               Worksheet.Cells[Row, 16] = Interference.ReceiverGain1;
               Worksheet.Cells[Row, 17] = Interference.ReceiverGain2;
               Worksheet.Cells[Row, 18] = Interference.Receiver.Station.GetRxLosses();
               Worksheet.Cells[Row, 19] = Interference.Preselector1;
               Worksheet.Cells[Row, 20] = Interference.Preselector2;

               Worksheet.Cells[Row, 21] = Interference.Transmitter1.Station.GetStandard();
               Worksheet.Cells[Row, 22] = Interference.Transmitter1.Station.GetName();
               Worksheet.Cells[Row, 23] = Interference.Transmitter1.Station.GetOwner();
               Worksheet.Cells[Row, 24] = Interference.Transmitter1.Station.GetAddress();
               Worksheet.Cells[Row, 25] = Interference.Transmitter1.Frequency;
               Worksheet.Cells[Row, 26] = Interference.Transmitter1.Station.GetPower();
               Worksheet.Cells[Row, 27] = Interference.Transmitter1.Station.GetBandwidth();
               Worksheet.Cells[Row, 28] = Interference.Transmitter1.Station.GetHeight();
               Worksheet.Cells[Row, 29] = Interference.Transmitter1.Station.GetAzimuth();
               Worksheet.Cells[Row, 30] = Interference.TransmitterAzimuth1;
               Worksheet.Cells[Row, 31] = Interference.Transmitter1.Station.GetElevation();
               Worksheet.Cells[Row, 32] = Interference.TransmitterElevation1;// "Elevation to P1";
               Worksheet.Cells[Row, 33] = Interference.Transmitter1.Station.GetMaxGain();
               Worksheet.Cells[Row, 34] = Interference.TransmitterGain1;
               Worksheet.Cells[Row, 35] = Interference.Transmitter1.Station.GetTxLosses();
               //Worksheet.Cells[Row, 36] = "--";
               Worksheet.Cells[Row, 37] = Interference.Range1;
               Worksheet.Cells[Row, 38] = Interference.Power1;

               Worksheet.Cells[Row, 21] = Interference.Transmitter2.Station.GetStandard();
               Worksheet.Cells[Row, 40] = Interference.Transmitter2.Station.GetName();
               Worksheet.Cells[Row, 41] = Interference.Transmitter2.Station.GetOwner();
               Worksheet.Cells[Row, 42] = Interference.Transmitter2.Station.GetAddress();
               Worksheet.Cells[Row, 43] = Interference.Transmitter2.Frequency;
               Worksheet.Cells[Row, 44] = Interference.Transmitter2.Station.GetPower();
               Worksheet.Cells[Row, 45] = Interference.Transmitter2.Station.GetBandwidth();
               Worksheet.Cells[Row, 48] = Interference.Transmitter2.Station.GetHeight();
               Worksheet.Cells[Row, 47] = Interference.Transmitter2.Station.GetAzimuth();
               Worksheet.Cells[Row, 48] = Interference.TransmitterAzimuth2;
               Worksheet.Cells[Row, 49] = Interference.Transmitter2.Station.GetElevation();
               Worksheet.Cells[Row, 50] = Interference.TransmitterElevation2;
               Worksheet.Cells[Row, 51] = Interference.Transmitter2.Station.GetMaxGain();
               Worksheet.Cells[Row, 52] = Interference.TransmitterGain2;
               Worksheet.Cells[Row, 53] = Interference.Transmitter2.Station.GetTxLosses();
               //Worksheet.Cells[Row, 54] = "--";
               Worksheet.Cells[Row, 55] = Interference.Range2;
               Worksheet.Cells[Row, 56] = Interference.Power2;

               Worksheet.Cells[Row, 57] = Interference.SumPower;
               Worksheet.Cells[Row, 58] = MinSumPower;
               Worksheet.Cells[Row, 59] = MinSumPower - Interference.SumPower;

               Row++;
            }
            appExcel.Visible = true;
            //Worksheet.Cells[1, 1] = "Value1";
            //Worksheet.Cells[2, 1] = 222.2;
            //Workbook.SaveAs(ExcelSaveFileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }
         //}
      }
      //=======================================================
      // Calling HELP
      //=======================================================
      private void buttonHelp_Click(object sender, EventArgs e)
      {
         List<string> listLng = TLocaliz.Lenguages();
         bool ShowHelp = false;
         foreach (string lang in listLng)
         {
            string NameHelp = string.Format("{0}\\Help\\IntermodUserGuide_{1}.pdf", Environment.CurrentDirectory, lang);
            if (System.IO.File.Exists(NameHelp))
            {
               System.Diagnostics.Process proc = new System.Diagnostics.Process();
               proc.StartInfo.FileName = NameHelp;
               proc.StartInfo.UseShellExecute = true;
               proc.Start();
               ShowHelp = true;
               break;
            }
         }
         if (ShowHelp == false)
            System.Windows.Forms.MessageBox.Show(TLocaliz.TxT("Can't find a help file \"IntermodUserGuide_xxx.pdf\""), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      private void IntermodulationMainForm_Load(object sender, EventArgs e)
      {

      }
   }
   //=========================================================
   //=========================================================
   //=========================================================
   //     
   //=========================================================
   //=========================================================
   //=========================================================
   public class IntermodulationInterference : IComparable
   {
      public Station.Channel Transmitter1;
      public Station.Channel Transmitter2;
      public Station.Channel Receiver;

      public double Power1; // Уровень помехи 1, дБВт
      public double Power2; // Уровень помехи 2, дБВт
      public double SumPower; // Сумарна завада, дБВт

      public double TransmitterGain1; // ЕВП у напрямку приймача, дБВт
      public double TransmitterAzimuth1; // Азимут макс. випромінювання, град.
      public double TransmitterElevation1; // кут місця максимального випромінювання від передавача до приймача
      public double ReceiverGain1;
      public double ReceiverAzimuth1;
      public double ReceiverElevation1;

      public double TransmitterGain2; // ЕВП у напрямку приймача, дБВт
      public double TransmitterAzimuth2; // Азимут макс. випромінювання, град.
      public double TransmitterElevation2; // кут місця максимального випромінювання від передавача до приймача
      public double ReceiverGain2;
      public double ReceiverAzimuth2;
      public double ReceiverElevation2;

      public double ReceiverSensitivity;
      public double Range1; // Відстань до РЕЗ, джерела завади (до анализируемой станции), км
      public double Range2; // Відстань до РЕЗ, джерела завади (до анализируемой станции), км

      public double Preselector1;
      public double Preselector2;

      public IntermodulationInterference()
      {
         ReceiverSensitivity = 0.1;
      }

      public int CompareTo(object obj)
      {
         IntermodulationInterference otherInterference = (IntermodulationInterference)obj;
         if (otherInterference.Receiver.Frequency > Receiver.Frequency)
            return -1;
         if (otherInterference.Receiver.Frequency < Receiver.Frequency)
            return 1;
         if (otherInterference.Receiver.Frequency == Receiver.Frequency)
         {
            if (otherInterference.Power1 + otherInterference.Power2 < Power1 + Power2)
            {
               return -1;
            }
            if (otherInterference.Power1 + otherInterference.Power2 > Power1 + Power2)
            {
               return 1;
            }
            if (otherInterference.Power1 + otherInterference.Power2 == Power1 + Power2)
            {
               return 0;
            }
         }
         return 0;
      }

       public void CalculateRanges()
       {
           if (Receiver.Station != null)
           {
               if (Transmitter1.Station != null)
                   Range1 = Transmitter1.Station.RangeToStation(Receiver.Station);
               if (Transmitter2.Station != null)
                   Range2 = Transmitter2.Station.RangeToStation(Receiver.Station);
           }
       }
   }

   public class ReceiverInfo
   {
      public Station.Channel Receiver;
      public int countStation;      //кол-во помех
      public double maxInterference;//мак. помеха
   };

   public class InterefenceInfo
   {
      public Station.Channel Tranmitter;
      public IntermodulationInterference Interference;
      public double Range;
      public double Power;
      public double TransmitterAzimuth;
      public double TransmitterElevation;
      public double TransmitterGain;
      public double ReceiverAzimuth;
      public double ReceiverElevation;
      public double ReceiverGain;
      public double Preselector;
   };

   public class IntermodulationInterferenceInfo
   {
      public IntermodulationInterference Interference;
   };
}
