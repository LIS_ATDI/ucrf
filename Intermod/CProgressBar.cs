﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.Intermodulation
{
   class CProgressBar : IDisposable
   {
      //===================================================
      private bool isShow = false;   //TRUE - я должен его закрыть
      //===================================================
      /// <summary>
      /// Сонструктор
      /// </summary>
      public CProgressBar(string _title)
      {
         if (!IMProgress.IsDisplayed())
         {
            IMProgress.Create(_title);
            isShow = true;
         }
      }
      //==================================================
      /// <summary>
      /// Деструктор
      /// </summary>
      public void Dispose()
      {
         if (isShow == true)
            IMProgress.Destroy();
      }
      //==================================================
      /// <summary>
      /// Отобразить больщой текст
      /// </summary>
      /// <param name="_text">текст</param>
      public void ShowBig(string _text)
      {
         IMProgress.ShowBig(_text);
      }
      //==================================================
      /// <summary>
      /// Отобразить строку в маленьком окне
      /// </summary>
      /// <param name="_text">строка</param>
      public void ShowSmall(string _text)
      {
         IMProgress.ShowSmall(_text);
      }
      //==================================================
      /// <summary>
      /// Отобразить число в маленьком окне
      /// </summary>
      /// <param name="_nbr">число</param>
      public void ShowSmall(int _nbr)
      {
         IMProgress.ShowSmall(_nbr);
      }
      //==================================================
      /// <summary>
      /// Отобразить числа в маленьком окне
      /// </summary>
      /// <param name="_nbr1">число 1</param>
      /// <param name="_nbr2">число 2</param>
      public void ShowSmall(int _nbr1, int _nbr2)
      {
         IMProgress.ShowSmall(_nbr1, _nbr2);
      }
      //==================================================
      /// <summary>
      /// Отобразить прогресс бар
      /// </summary>
      /// <param name="_done">сделано</param>
      /// <param name="_total">общее</param>
      public void ShowProgress(int _done, int _total)
      {
         IMProgress.ShowProgress(_done, _total);
      }
      //==================================================
      /// <summary>
      /// Отмена пользователем
      /// </summary>
      /// <returns>TRUE - пользователь отменил, иначе FALSE</returns>
      public bool UserCanceled()
      {
         return IMProgress.UserCanceled();
      }
      //==================================================
      /// <summary>
      /// Скрываем форму
      /// </summary>
      public void Close()
      {
         if (isShow == true)
         {
            IMProgress.Destroy();
            isShow = false;   //Уже не отображается
         }
      }
   }
}
