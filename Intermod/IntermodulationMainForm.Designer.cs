﻿namespace XICSM.Intermodulation
{
	partial class IntermodulationMainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ModelButton = new System.Windows.Forms.Button();
            this.StationListButton = new System.Windows.Forms.Button();
            this.SelectCalculationComboBox = new System.Windows.Forms.ComboBox();
            this.SelectCalculationTitleLabel = new System.Windows.Forms.Label();
            this.MinimalRangeTitleLabel = new System.Windows.Forms.Label();
            this.MinimalRangeTextBox = new System.Windows.Forms.TextBox();
            this.ModelTitleLabel = new System.Windows.Forms.Label();
            this.btnSelectStation = new System.Windows.Forms.Button();
            this.ResultTreeView = new System.Windows.Forms.TreeView();
            this.tbAboutSelectStation = new System.Windows.Forms.TextBox();
            this.FromFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.ToFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.ToTitleLabel = new System.Windows.Forms.Label();
            this.FrequencyCheckBox = new System.Windows.Forms.CheckBox();
            this.MinimalInterferenceTextBox = new System.Windows.Forms.TextBox();
            this.MinimalInterferencePower = new System.Windows.Forms.Label();
            this.SpreadnessModelLabel = new System.Windows.Forms.Label();
            this.PreselectorGainLabel = new System.Windows.Forms.Label();
            this.PreselectorTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.InputStageLinearityTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MinSumPowerEdit = new System.Windows.Forms.TextBox();
            this.PreselectorCoefficientTextBox = new System.Windows.Forms.TextBox();
            this.PreselectorCoefficientLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.WorkerThread = new System.ComponentModel.BackgroundWorker();
            this.labelMessage = new System.Windows.Forms.Label();
            this.labelValue = new System.Windows.Forms.Label();
            this.DetailsTextBox = new System.Windows.Forms.TextBox();
            this.ExportToExcelButton = new System.Windows.Forms.Button();
            this.ExcelSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ModelButton
            // 
            this.ModelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ModelButton.Location = new System.Drawing.Point(345, 216);
            this.ModelButton.Name = "ModelButton";
            this.ModelButton.Size = new System.Drawing.Size(75, 23);
            this.ModelButton.TabIndex = 4;
            this.ModelButton.Text = "Model...";
            this.ModelButton.UseVisualStyleBackColor = true;
            this.ModelButton.Click += new System.EventHandler(this.ModelButton_Click);
            // 
            // StationListButton
            // 
            this.StationListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StationListButton.Enabled = false;
            this.StationListButton.Location = new System.Drawing.Point(426, 216);
            this.StationListButton.Name = "StationListButton";
            this.StationListButton.Size = new System.Drawing.Size(75, 23);
            this.StationListButton.TabIndex = 3;
            this.StationListButton.Text = "Station list...";
            this.StationListButton.UseVisualStyleBackColor = true;
            this.StationListButton.Visible = false;
            this.StationListButton.Click += new System.EventHandler(this.StationListButton_Click);
            // 
            // SelectCalculationComboBox
            // 
            this.SelectCalculationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectCalculationComboBox.FormattingEnabled = true;
            this.SelectCalculationComboBox.Items.AddRange(new object[] {
            "Interferences for station",
            "Interferences by station"});
            this.SelectCalculationComboBox.Location = new System.Drawing.Point(152, 186);
            this.SelectCalculationComboBox.Name = "SelectCalculationComboBox";
            this.SelectCalculationComboBox.Size = new System.Drawing.Size(181, 21);
            this.SelectCalculationComboBox.TabIndex = 1;
            // 
            // SelectCalculationTitleLabel
            // 
            this.SelectCalculationTitleLabel.AutoSize = true;
            this.SelectCalculationTitleLabel.Location = new System.Drawing.Point(14, 190);
            this.SelectCalculationTitleLabel.Name = "SelectCalculationTitleLabel";
            this.SelectCalculationTitleLabel.Size = new System.Drawing.Size(132, 13);
            this.SelectCalculationTitleLabel.TabIndex = 9;
            this.SelectCalculationTitleLabel.Text = "Select calculation method:";
            // 
            // MinimalRangeTitleLabel
            // 
            this.MinimalRangeTitleLabel.AutoSize = true;
            this.MinimalRangeTitleLabel.Location = new System.Drawing.Point(342, 189);
            this.MinimalRangeTitleLabel.Name = "MinimalRangeTitleLabel";
            this.MinimalRangeTitleLabel.Size = new System.Drawing.Size(108, 13);
            this.MinimalRangeTitleLabel.TabIndex = 10;
            this.MinimalRangeTitleLabel.Text = "Search in range (km):";
            // 
            // MinimalRangeTextBox
            // 
            this.MinimalRangeTextBox.Location = new System.Drawing.Point(595, 186);
            this.MinimalRangeTextBox.Name = "MinimalRangeTextBox";
            this.MinimalRangeTextBox.Size = new System.Drawing.Size(70, 20);
            this.MinimalRangeTextBox.TabIndex = 2;
            // 
            // ModelTitleLabel
            // 
            this.ModelTitleLabel.AutoSize = true;
            this.ModelTitleLabel.Location = new System.Drawing.Point(14, 221);
            this.ModelTitleLabel.Name = "ModelTitleLabel";
            this.ModelTitleLabel.Size = new System.Drawing.Size(108, 13);
            this.ModelTitleLabel.TabIndex = 8;
            this.ModelTitleLabel.Text = "Model of spreadness:";
            // 
            // btnSelectStation
            // 
            this.btnSelectStation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectStation.Location = new System.Drawing.Point(245, 16);
            this.btnSelectStation.Name = "btnSelectStation";
            this.btnSelectStation.Size = new System.Drawing.Size(75, 23);
            this.btnSelectStation.TabIndex = 0;
            this.btnSelectStation.Text = "Station...";
            this.btnSelectStation.UseVisualStyleBackColor = true;
            this.btnSelectStation.Click += new System.EventHandler(this.btnSelectStation_Click);
            // 
            // ResultTreeView
            // 
            this.ResultTreeView.Location = new System.Drawing.Point(15, 245);
            this.ResultTreeView.Name = "ResultTreeView";
            this.ResultTreeView.Size = new System.Drawing.Size(334, 284);
            this.ResultTreeView.TabIndex = 6;
            this.ResultTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ResultTreeView_AfterSelect);
            this.ResultTreeView.DoubleClick += new System.EventHandler(this.ResultTreeView_DoubleClick);
            // 
            // tbAboutSelectStation
            // 
            this.tbAboutSelectStation.Location = new System.Drawing.Point(6, 44);
            this.tbAboutSelectStation.Multiline = true;
            this.tbAboutSelectStation.Name = "tbAboutSelectStation";
            this.tbAboutSelectStation.ReadOnly = true;
            this.tbAboutSelectStation.Size = new System.Drawing.Size(314, 107);
            this.tbAboutSelectStation.TabIndex = 1;
            // 
            // FromFrequencyTextBox
            // 
            this.FromFrequencyTextBox.Enabled = false;
            this.FromFrequencyTextBox.Location = new System.Drawing.Point(133, 41);
            this.FromFrequencyTextBox.Name = "FromFrequencyTextBox";
            this.FromFrequencyTextBox.Size = new System.Drawing.Size(73, 20);
            this.FromFrequencyTextBox.TabIndex = 4;
            // 
            // ToFrequencyTextBox
            // 
            this.ToFrequencyTextBox.Enabled = false;
            this.ToFrequencyTextBox.Location = new System.Drawing.Point(227, 41);
            this.ToFrequencyTextBox.Name = "ToFrequencyTextBox";
            this.ToFrequencyTextBox.Size = new System.Drawing.Size(78, 20);
            this.ToFrequencyTextBox.TabIndex = 5;
            // 
            // ToTitleLabel
            // 
            this.ToTitleLabel.AutoSize = true;
            this.ToTitleLabel.Enabled = false;
            this.ToTitleLabel.Location = new System.Drawing.Point(211, 44);
            this.ToTitleLabel.Name = "ToTitleLabel";
            this.ToTitleLabel.Size = new System.Drawing.Size(13, 13);
            this.ToTitleLabel.TabIndex = 7;
            this.ToTitleLabel.Text = "--";
            // 
            // FrequencyCheckBox
            // 
            this.FrequencyCheckBox.AutoSize = true;
            this.FrequencyCheckBox.Location = new System.Drawing.Point(133, 15);
            this.FrequencyCheckBox.Name = "FrequencyCheckBox";
            this.FrequencyCheckBox.Size = new System.Drawing.Size(113, 14);
            this.FrequencyCheckBox.TabIndex = 3;
            this.FrequencyCheckBox.Text = "Frequency from -- to (MHz)";
            this.FrequencyCheckBox.UseVisualStyleBackColor = true;
            this.FrequencyCheckBox.CheckedChanged += new System.EventHandler(this.FrequencyCheckBox_CheckedChanged);
            // 
            // MinimalInterferenceTextBox
            // 
            this.MinimalInterferenceTextBox.Location = new System.Drawing.Point(227, 97);
            this.MinimalInterferenceTextBox.Name = "MinimalInterferenceTextBox";
            this.MinimalInterferenceTextBox.Size = new System.Drawing.Size(78, 20);
            this.MinimalInterferenceTextBox.TabIndex = 6;
            this.MinimalInterferenceTextBox.Text = "-75";
            // 
            // MinimalInterferencePower
            // 
            this.MinimalInterferencePower.AutoSize = true;
            this.MinimalInterferencePower.Location = new System.Drawing.Point(6, 100);
            this.MinimalInterferencePower.Name = "MinimalInterferencePower";
            this.MinimalInterferencePower.Size = new System.Drawing.Size(133, 13);
            this.MinimalInterferencePower.TabIndex = 12;
            this.MinimalInterferencePower.Text = "Minimal interference power";
            // 
            // SpreadnessModelLabel
            // 
            this.SpreadnessModelLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SpreadnessModelLabel.Location = new System.Drawing.Point(152, 217);
            this.SpreadnessModelLabel.Name = "SpreadnessModelLabel";
            this.SpreadnessModelLabel.Size = new System.Drawing.Size(181, 20);
            this.SpreadnessModelLabel.TabIndex = 15;
            this.SpreadnessModelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PreselectorGainLabel
            // 
            this.PreselectorGainLabel.AutoSize = true;
            this.PreselectorGainLabel.Location = new System.Drawing.Point(6, 16);
            this.PreselectorGainLabel.Name = "PreselectorGainLabel";
            this.PreselectorGainLabel.Size = new System.Drawing.Size(18, 13);
            this.PreselectorGainLabel.TabIndex = 9;
            this.PreselectorGainLabel.Text = "G:";
            // 
            // PreselectorTextBox
            // 
            this.PreselectorTextBox.Location = new System.Drawing.Point(43, 13);
            this.PreselectorTextBox.Name = "PreselectorTextBox";
            this.PreselectorTextBox.Size = new System.Drawing.Size(73, 20);
            this.PreselectorTextBox.TabIndex = 0;
            this.PreselectorTextBox.Text = "19";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "IP3:";
            // 
            // InputStageLinearityTextBox
            // 
            this.InputStageLinearityTextBox.Location = new System.Drawing.Point(43, 41);
            this.InputStageLinearityTextBox.Name = "InputStageLinearityTextBox";
            this.InputStageLinearityTextBox.Size = new System.Drawing.Size(73, 20);
            this.InputStageLinearityTextBox.TabIndex = 1;
            this.InputStageLinearityTextBox.Text = "23";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.MinSumPowerEdit);
            this.groupBox1.Controls.Add(this.PreselectorCoefficientTextBox);
            this.groupBox1.Controls.Add(this.PreselectorCoefficientLabel);
            this.groupBox1.Controls.Add(this.ToFrequencyTextBox);
            this.groupBox1.Controls.Add(this.PreselectorGainLabel);
            this.groupBox1.Controls.Add(this.ToTitleLabel);
            this.groupBox1.Controls.Add(this.PreselectorTextBox);
            this.groupBox1.Controls.Add(this.InputStageLinearityTextBox);
            this.groupBox1.Controls.Add(this.MinimalInterferenceTextBox);
            this.groupBox1.Controls.Add(this.FromFrequencyTextBox);
            this.groupBox1.Controls.Add(this.MinimalInterferencePower);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.FrequencyCheckBox);
            this.groupBox1.Location = new System.Drawing.Point(15, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 157);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Params";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Minimal total interference power";
            // 
            // MinSumPowerEdit
            // 
            this.MinSumPowerEdit.Location = new System.Drawing.Point(227, 123);
            this.MinSumPowerEdit.Name = "MinSumPowerEdit";
            this.MinSumPowerEdit.Size = new System.Drawing.Size(78, 20);
            this.MinSumPowerEdit.TabIndex = 7;
            this.MinSumPowerEdit.Text = "0";
            // 
            // PreselectorCoefficientTextBox
            // 
            this.PreselectorCoefficientTextBox.Location = new System.Drawing.Point(43, 68);
            this.PreselectorCoefficientTextBox.Name = "PreselectorCoefficientTextBox";
            this.PreselectorCoefficientTextBox.Size = new System.Drawing.Size(73, 20);
            this.PreselectorCoefficientTextBox.TabIndex = 2;
            this.PreselectorCoefficientTextBox.Text = "130";
            // 
            // PreselectorCoefficientLabel
            // 
            this.PreselectorCoefficientLabel.AutoSize = true;
            this.PreselectorCoefficientLabel.Location = new System.Drawing.Point(6, 71);
            this.PreselectorCoefficientLabel.Name = "PreselectorCoefficientLabel";
            this.PreselectorCoefficientLabel.Size = new System.Drawing.Size(17, 13);
            this.PreselectorCoefficientLabel.TabIndex = 11;
            this.PreselectorCoefficientLabel.Text = "K:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbAboutSelectStation);
            this.groupBox2.Controls.Add(this.btnSelectStation);
            this.groupBox2.Location = new System.Drawing.Point(339, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 157);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selected station";
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(584, 592);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 5;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(15, 563);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(650, 23);
            this.progressBar.TabIndex = 13;
            // 
            // WorkerThread
            // 
            this.WorkerThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerThread_DoWork);
            this.WorkerThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerThread_RunWorkerCompleted);
            // 
            // labelMessage
            // 
            this.labelMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMessage.Location = new System.Drawing.Point(12, 544);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(590, 16);
            this.labelMessage.TabIndex = 11;
            // 
            // labelValue
            // 
            this.labelValue.Location = new System.Drawing.Point(608, 544);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(57, 16);
            this.labelValue.TabIndex = 12;
            this.labelValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DetailsTextBox
            // 
            this.DetailsTextBox.Location = new System.Drawing.Point(361, 245);
            this.DetailsTextBox.Multiline = true;
            this.DetailsTextBox.Name = "DetailsTextBox";
            this.DetailsTextBox.ReadOnly = true;
            this.DetailsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.DetailsTextBox.Size = new System.Drawing.Size(304, 284);
            this.DetailsTextBox.TabIndex = 7;
            this.DetailsTextBox.WordWrap = false;
            // 
            // ExportToExcelButton
            // 
            this.ExportToExcelButton.Enabled = false;
            this.ExportToExcelButton.Location = new System.Drawing.Point(490, 592);
            this.ExportToExcelButton.Name = "ExportToExcelButton";
            this.ExportToExcelButton.Size = new System.Drawing.Size(75, 23);
            this.ExportToExcelButton.TabIndex = 33;
            this.ExportToExcelButton.Text = "Export...";
            this.ExportToExcelButton.UseVisualStyleBackColor = true;
            this.ExportToExcelButton.Click += new System.EventHandler(this.ExportToExcelButton_Click);
            // 
            // ExcelSaveFileDialog
            // 
            this.ExcelSaveFileDialog.DefaultExt = "xls";
            this.ExcelSaveFileDialog.Filter = "Excel files (*.xls)|*.xls|All files|*.*||";
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(392, 593);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 34;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.UseVisualStyleBackColor = true;
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // IntermodulationMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 636);
            this.Controls.Add(this.buttonHelp);
            this.Controls.Add(this.ExportToExcelButton);
            this.Controls.Add(this.DetailsTextBox);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SpreadnessModelLabel);
            this.Controls.Add(this.ResultTreeView);
            this.Controls.Add(this.ModelTitleLabel);
            this.Controls.Add(this.MinimalRangeTextBox);
            this.Controls.Add(this.MinimalRangeTitleLabel);
            this.Controls.Add(this.SelectCalculationTitleLabel);
            this.Controls.Add(this.SelectCalculationComboBox);
            this.Controls.Add(this.StationListButton);
            this.Controls.Add(this.ModelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IntermodulationMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Intermodulation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IntermodulationMainForm_FormClosing);
            this.Load += new System.EventHandler(this.IntermodulationMainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button ModelButton;
		private System.Windows.Forms.Button StationListButton;
		private System.Windows.Forms.ComboBox SelectCalculationComboBox;
		private System.Windows.Forms.Label SelectCalculationTitleLabel;
		private System.Windows.Forms.Label MinimalRangeTitleLabel;
		private System.Windows.Forms.TextBox MinimalRangeTextBox;
		private System.Windows.Forms.Label ModelTitleLabel;
      private System.Windows.Forms.Button btnSelectStation;
		private System.Windows.Forms.TreeView ResultTreeView;
      private System.Windows.Forms.TextBox tbAboutSelectStation;
	  private System.Windows.Forms.TextBox FromFrequencyTextBox;
	  private System.Windows.Forms.TextBox ToFrequencyTextBox;
	  private System.Windows.Forms.Label ToTitleLabel;
	  private System.Windows.Forms.CheckBox FrequencyCheckBox;
	  private System.Windows.Forms.TextBox MinimalInterferenceTextBox;
	  private System.Windows.Forms.Label MinimalInterferencePower;
	  private System.Windows.Forms.Label SpreadnessModelLabel;
	  private System.Windows.Forms.Label PreselectorGainLabel;
	  private System.Windows.Forms.TextBox PreselectorTextBox;
	  private System.Windows.Forms.Label label2;
	  private System.Windows.Forms.TextBox InputStageLinearityTextBox;
     private System.Windows.Forms.GroupBox groupBox1;
     private System.Windows.Forms.GroupBox groupBox2;
     private System.Windows.Forms.Button btnFind;
     private System.Windows.Forms.ProgressBar progressBar;
     private System.ComponentModel.BackgroundWorker WorkerThread;
     private System.Windows.Forms.Label labelMessage;
     private System.Windows.Forms.Label labelValue;
     private System.Windows.Forms.TextBox PreselectorCoefficientTextBox;
     private System.Windows.Forms.Label PreselectorCoefficientLabel;
     private System.Windows.Forms.TextBox DetailsTextBox;
     private System.Windows.Forms.TextBox MinSumPowerEdit;
     private System.Windows.Forms.Button ExportToExcelButton;
     private System.Windows.Forms.SaveFileDialog ExcelSaveFileDialog;
     private System.Windows.Forms.Label label1;
     private System.Windows.Forms.Button buttonHelp;
	}
}