﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.Windows.Forms;

namespace XICSM.Intermodulation
{
	public partial class Plugin : IPlugin
	{
		private IntermodulationMainForm MainForm;

		public string Ident { get { return "Intermodulation"; } }
		public string Description { get { return "Plugin for intermodulation"; } }
		public double SchemaVersion { get { return 0.0; } }
		public void RegisterSchema(IMSchema s) { }
		public bool UpgradeDatabase(IMSchema s, double dbCurVersion) { return true; }

      //=================================================
      // Регестрируем вызовы Popup menu
      //=================================================
      public void RegisterBoard(IMBoard b)
		{
         b.RegisterQueryMenuBuilder("MOB_STATION2", OnGetQueryMenu);
         b.RegisterQueryMenuBuilder("MOB_STATION", OnGetQueryMenu);
      }

		public bool OtherMessage(String message, Object inParam, ref Object outParam)
		{		
			return false; //not handled
		}
      //=================================================
      // Запрос отображения Popup Menu
      //=================================================
      public List<IMQueryMenuNode> OnGetQueryMenu(String tableName, int nbSelMin)
      {
         List<IMQueryMenuNode> lst = new List<IMQueryMenuNode>();

         if (tableName == "MOB_STATION2" || tableName == "MOB_STATION")
         {
            if (nbSelMin == 1)
               lst.Add(new IMQueryMenuNode(TLocaliz.TxT("Calculate intermodulation..."), null, OnCalcIntermod, IMQueryMenuNode.ExecMode.FirstRecord));
         }
         return lst;
      }


		public void OnIntermodulationAbout()
		{
         MessageBox.Show(TLocaliz.TxT("Intermodulation Version") + " " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), "About...");
		}

		public void GetMainMenu(IMMainMenu mm)
		{
			//mm.SetInsertLocation("Control\\Argus connec*", IMMainMenu.InsertLocation.After);
			mm.SetInsertLocation("Help\\*", IMMainMenu.InsertLocation.Bottom);

			mm.InsertItem("Help\\<SEPARATOR>", null, null);
			mm.InsertItem("Help\\About Intermodulation...", OnIntermodulationAbout, "MOB_STATION2");


			//mm.SetInsertLocation("Control\\*", IMMainMenu.InsertLocation.Bottom);
			//mm.InsertItem("Control\\<SEPARATOR>", null, null);
			//mm.InsertItem("Control\\Intermodulation form...", OnIntermodulationForm, "MOB_STATION2");
		}

		public void OnIntermodulationForm()
		{		
			MainForm = new IntermodulationMainForm();		
			MainForm.ShowDialog();
			MainForm.Dispose();		
		}
      //=================================================
      // Необходимо расчитать интермодуляцию
      //=================================================
      private bool OnCalcIntermod(IMQueryMenuNode.Context context)
      {
         MainForm = new IntermodulationMainForm(context.TableName, context.TableId);
         MainForm.ShowDialog();
         MainForm.Dispose();
         return true;
      }      
	}
}
