﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.Intermodulation
{
   class CHelper
   {
      public static string DoubleToString(double _value, char _separator)
      {
         char oldChar = ',';
         if (_separator == ',')
            oldChar = '.';
         string retVal = "";
         retVal = _value.ToString("F4");
         return retVal.Replace(oldChar, _separator);
      }
   }
}
