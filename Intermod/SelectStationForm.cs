﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.Intermodulation
{
	public partial class SelectStationForm : Form
	{
		public bool MultiSelect;
      private string nameTable;
		public List<int> SelectedId;
		
		public SelectStationForm(bool newMultiSelect, string _nameTable)
		{
			MultiSelect = newMultiSelect;
         nameTable = _nameTable;
			InitializeComponent();
         TLocaliz.TxT(this);
			StationListView.MultiSelect = MultiSelect;
			FillList();
		}

		public List<int> GetSelectedStationId()
		{
			List<int> selected = new List<int>();

			for(int i=0;i<StationListView.Items.Count;i++)
			{
				if (StationListView.Items[i].Selected)
					selected.Add(Convert.ToInt32(StationListView.Items[i].Text));
			}

			return selected;			
		}

		public void FillList()
		{					
			//MultiSelect	
			StationListView.Items.Clear();					

			IMRecordset rStations = new IMRecordset(nameTable, IMRecordset.Mode.ReadOnly);
			rStations.Select("ID, NAME, Owner.NAME");
			//r21.SetWhere("STA_ID", IMRecordset.Operation.Eq, stationId);
			rStations.Open();
			do{			
				int Id =  rStations.GetI("ID");
				string Name = rStations.GetS("NAME");
				string OwnerName = rStations.GetS("Owner.NAME");

				ListViewItem item = StationListView.Items.Add(Id.ToString());
				item.SubItems.Add(Name);
				item.SubItems.Add(OwnerName);

				rStations.MoveNext();						
			}while(!rStations.IsEOF());
			rStations.Close();
			rStations.Dispose();
		}

		private void OkButton_Click(object sender, EventArgs e)
		{
			SelectedId = GetSelectedStationId();
		}
	}
}

