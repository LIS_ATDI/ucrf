﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM
{
	public class Math
	{
		public struct SphereCoordinate
		{
			public double R;
			public double Phita;
			public double Phi;
		};

		public struct DecartCoordinate
		{
			public double X;
			public double Y;
			public double Z;			
		};
		
		public Math()
		{
		}

		static public double GeoPositionToAngleDegree(double GeoCoordinate)
		{
			double Degree = System.Math.Truncate(GeoCoordinate);
			double FracPart = GeoCoordinate - Degree;

			double Minutes = System.Math.Truncate(FracPart * 100.0);
			double Seconds = System.Math.Truncate(FracPart * 10000.0)-Minutes*100.0;
			
			return Degree + (Minutes)/60.0 + (Seconds)/3600.0;
		}

		static public double DegreeToRadian(double Angle)
		{
			return System.Math.PI * Angle / 180.0;
		}

		static public double RadianToDegree(double Angle)
		{
			return Angle * (180.0 / System.Math.PI);
		}

		static public SphereCoordinate SphereCoordinateFromGeoPosition(double Longitude, double Lattitude)
		{
			SphereCoordinate SphereCoord = new SphereCoordinate();
			SphereCoord.R = 6371.0; //Average Radius;
			SphereCoord.Phi = GeoPositionToAngleDegree(Longitude);
			SphereCoord.Phita = GeoPositionToAngleDegree(Lattitude);
			return SphereCoord;
		}
		
		static public DecartCoordinate DecartCoordinateFromSphereCoordinate(SphereCoordinate SphereCoord)
		{
			DecartCoordinate DecartCoord = new DecartCoordinate();
			DecartCoord.X = SphereCoord.R * System.Math.Sin(DegreeToRadian(SphereCoord.Phita)) * System.Math.Cos(DegreeToRadian(SphereCoord.Phi));
			DecartCoord.Y = SphereCoord.R * System.Math.Sin(DegreeToRadian(SphereCoord.Phita)) * System.Math.Sin(DegreeToRadian(SphereCoord.Phi));
			DecartCoord.Z = SphereCoord.R * System.Math.Cos(DegreeToRadian(SphereCoord.Phita));
			return DecartCoord;
		}

		static public SphereCoordinate SphereCoordinateFromDecartCoordinate(DecartCoordinate DecardCoord)
		{
			SphereCoordinate SphereCoord = new SphereCoordinate();
			SphereCoord.R = System.Math.Sqrt(DecardCoord.X * DecardCoord.X + DecardCoord.Y * DecardCoord.Y + DecardCoord.Z * DecardCoord.Z);
			SphereCoord.Phita = System.Math.Acos(DecardCoord.Z / SphereCoord.R);

			if (DecardCoord.X > 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X < 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = (System.Math.PI/2.0)+System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X < 0 && DecardCoord.Y > 0)
				SphereCoord.Phi = -(System.Math.PI / 2.0) - System.Math.Atan(DecardCoord.Y / DecardCoord.X);
			if (DecardCoord.X > 0 && DecardCoord.Y < 0)
				SphereCoord.Phi = - System.Math.Atan(DecardCoord.Y / DecardCoord.X);

			return SphereCoord;		
		}
	}
}
