﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LisUtility.Mob;
using NSPosition;

using ICSM;

namespace XICSM.Intermodulation
{
	public class Station
	{
      public List<ACHMob.DataElement> _maskaT = new List<ACHMob.DataElement>(); // спектральна маска передатчика
      public List<ACHMob.DataElement> _maskaR = new List<ACHMob.DataElement>(); // спектральна маска прийомника
      public List<ACHMob.DataElement> _filterT = new List<ACHMob.DataElement>(); // фильтр передатчика
      public List<ACHMob.DataElement> _filterR = new List<ACHMob.DataElement>(); // фильтр прийомника

      public double _freqFilterTX; // частота зовнішнього фільтра передатчика
      public double _freqFilterRX; // частота зовнішнього фільтра прийомника
      public bool _isOK = true;   // Флаг корректности станции

		public struct Channel
		{		
			public int ID; 
			public double Bandwidth;
			public double Frequency;
			public Station Station;
		}

      //public class AntennaDiagramm
      //{
      //   List<double> Angles;
      //   List<double> Losses;

      //   double Alpha;
      //   string Type;
      //   double Aff;
      //   double MaximalGain;			

      //   public AntennaDiagramm()
      //   {
      //      Angles = new List<double>();
      //      Losses = new List<double>();
      //   }

      //   public bool IsValid()
      //   {
      //      if (Angles.Count==Losses.Count && Losses.Count>0 || Type.Length>0)
      //         return true;
      //      else
      //         return false;
      //   }

      //   public void SetMaximalGain(double Gain)
      //   {	
      //      MaximalGain = Gain;				
      //   }

      //   private void SetWienType(String Points)
      //   {
      //      if (Points.Length>0)
      //      {
      //         string[] split = Points.Split(new Char[] { ' ' });
      //         try{
      //            String wienCode = split[1];
      //            Alpha = Convert.ToDouble(wienCode.Substring(0,3));
      //            Type = wienCode.Substring(3,2);
      //            Aff = Convert.ToDouble(wienCode.Substring(5));
      //         }
      //         catch(Exception e)
      //         {
      //            //MessageBox.Show("Cannot parse "+Points+"\r\nThis is WIEN type abbr"); 
      //            Type = "";
      //            Angles.Clear();
      //            Losses.Clear();
      //         }
      //      }
      //   }

      //   private void BuildFromPoints(String Points)
      //   {
      //      string[] split = Points.Split(new Char[] { ' ' });
				
      //      if (split[0]=="POINTS")
      //      {
      //         int k=0;
      //         foreach (string s in split) 
      //         {
      //            double Result;
      //            string OldSeparator = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
      //            //System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator = ".";
      //            string newString = s;
      //            if (s.Contains("."))						
      //               newString = s.Replace(".", OldSeparator);

      //            if (k > 0 && Double.TryParse(newString, out Result))
      //            {
      //               if (k%2!=0)
      //                  Angles.Add(Result);
      //               else
      //                  Losses.Add(Result);
      //            } 
      //            //System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator = OldSeparator;
      //            k++;
      //         }
      //         if (Angles.Count!=Losses.Count)
      //         {
						
      //         }
      //         Angles.Add(360);
      //         Losses.Add(Losses[0]);			
      //      }
      //   }

      //   public void Build(String Points)
      //   {
      //      string[] split = Points.Split(new Char[] { ' ' });

      //      if (split[0] == "POINTS")
      //         BuildFromPoints(Points);

      //      if (split[0] == "WIEN")
      //         SetWienType(Points);
      //   }

      //   public double GetLossesAmount(double Degree)
      //   {				
      //      if (Type.Length>0)
      //      {
      //         AntennaDiagrammClass CalculatedDiagram = new AntennaDiagrammClass(Alpha, Type, Aff);
      //         return CalculatedDiagram.GetValue(Degree, MaximalGain);					         
      //      }
      //      else
      //      {
      //         return Interpolate(Degree);
      //      }						
      //   }
						
      //   public double Interpolate(double Degree)
      //   {				
      //      if (Angles.Count==1)
      //         return Losses[0];
      //      else {						
      //         if (Degree<0)
      //            Degree += 360.0;
      //         int last = Angles.Count-1;
      //         for (int i = 0; i < last; i++)
      //         {
      //            try{
      //               double df = Angles[i+1]-Angles[i];
      //               double dl = Losses[i+1]-Losses[i];
      //               if (Degree>=Angles[i] && Degree<=Angles[i+1])
      //               {
      //                  double k = dl / df;
      //                  double b = Losses[i] - k * Angles[i];

      //                  return k * Degree + b;
      //               }
      //            }
      //            catch(Exception e)
      //            {
							
      //            }
      //         }		
      //      }
      //      return 0.0;
      //   } 			
      //};

		private int ID;		
		private XICSM.Math.SphereCoordinate Position;
	   public double Bandwidth;

      public double _sensitivity;
      public double _ktbf;

		private string Name;
		private string Owner;

		private double Azimuth;
		private double Elevation;
		private double RadiatedPower;
		private double TxLosses;
		private double RxLosses;
		private double Height;

      private string Standard;
      private string Address;

      private double MaxGain;

		private List<Channel> TransmitterList;
		private List<Channel> ReceiverList;
		private Diagramm.AntennaDiagramm HorizontalDiagramm;
		private Diagramm.AntennaDiagramm VerticalDiagramm;

        public string DesEmi { get; set; }
        public string Status { get; set; }
        public string TableName { get; set; }

		public Station(int _id)
		{
			TransmitterList = new List<Channel>();
			ReceiverList = new List<Channel>();
         HorizontalDiagramm = new Diagramm.AntennaDiagramm();
         VerticalDiagramm = new Diagramm.AntennaDiagramm();
         ID = _id;
		}

      //===================================================
      /// <summary>
      /// Конструктор по умолчанию
      /// </summary>
      public Station() : this(0) {}

		public int GetID()
		{
			return ID;
		}

		/*public double GetPositionX()
		{
			return PositionX;
		}

		public double GetPositionY()
		{
			return PositionY;
		}*/

      public double GetMaxGain()
      {
         return MaxGain;
      }      
		
		public double GetBandwidth()
		{
			return Bandwidth;
		}

		public List<Channel> GetTransmitters()
		{
			return TransmitterList;			
		}

		public List<Channel> GetReceivers()
		{
			return ReceiverList;
		}

		public string GetName()
		{
			return Name;
		}

		public string GetOwner()
		{
			return Owner;
		}

		public double GetAzimuth()
		{
			return Azimuth;
		}

		public double GetElevation()
		{
			return Elevation;
		}

		public double GetPower()
		{			
			return RadiatedPower + 30.0;
		}

		public double GetTxLosses()
		{
			return TxLosses;
		}

		public double GetRxLosses()
		{
			return RxLosses;
		}

		public double GetHeight()
		{
			return Height;
		}

      public string GetStandard()
      {
         return Standard;
      }

      public string GetAddress()
      {
         return Address;
      }

		Diagramm.AntennaDiagramm GetHorizontalDiagramm()
		{
			return HorizontalDiagramm;		
		}

      Diagramm.AntennaDiagramm GetVerticalDiagramm()
		{
			return VerticalDiagramm;
		}

		static private double GetBandwidthFromString(string DesigEmission)
		{
			string number = "";
			double multiply=1.0;
			try
			{
				int Digit = 0;
				string SubEmission = DesigEmission.Substring(0,1);
 
				//DesigEmission != "NON" 
                //&& DesigEmission != "---------"           
				if (Int32.TryParse(SubEmission, out Digit) && DesigEmission != "")
				{
					for (int i = 0; i < DesigEmission.Length; i++)
						if (DesigEmission[i] >= '0' && DesigEmission[i] <= '9')
							number += DesigEmission[i];
						else
						{
							if (DesigEmission[i] == 'M')
								multiply = 1.0;

							if (DesigEmission[i] == 'K')
								multiply = 0.001;

							if (DesigEmission[i] == 'G')
								multiply = 1000.0;

							break;
						}
					return Convert.ToInt32(number) * multiply;
				} else 
					return 0.001;
			}
			catch (Exception)
			{
			//MessageBox.Show("The field \"DESIG_EMISSION\" has wrong data.", "Wrong \"DESIG_EMISSION\"",
            //                 MessageBoxButtons.OK, MessageBoxIcon.Question);
            return 0.001;
			}
		}

		public double GetPhita()
		{			
			return Position.Phita;
		}

		public double GetPhi()
		{
			return Position.Phi;
		}

		public XICSM.Math.SphereCoordinate GetPosition()
		{
			return Position;
		}

      static public List<Station> GetAllStations(string Table, RecordPos positionRadius, double fMin, double fMax, string standard)
      {
          const string GSM_1800 = "GSM-1800";
          const string GSM_900 = "GSM-900";
         List<string> statusForEmc = XICSM.UcrfRfaNET.StationsStatus.StatusDivision.GetStatusForEms();
			List<Station> StationList = new List<Station>();
         HashSet<int> hash = new HashSet<int>();

         using (CProgressBar pBar = new CProgressBar(TLocaliz.TxT("Loading station...")))
         {
            pBar.ShowBig(TLocaliz.TxT("Loading station..."));
            IMRecordset rStations = new IMRecordset(Table, IMRecordset.Mode.ReadOnly);
            rStations.Select("ID");
            rStations.Select("STATUS");
            rStations.Select("STANDARD");
            rStations.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Ge, positionRadius.X1);
            rStations.SetWhere("Position.LONGITUDE", IMRecordset.Operation.Le, positionRadius.X2);
            rStations.SetWhere("Position.LATITUDE", IMRecordset.Operation.Le, positionRadius.Y1);
            rStations.SetWhere("Position.LATITUDE", IMRecordset.Operation.Ge, positionRadius.Y2);

            string sql = string.Format("(([AssignedFrequencies.TX_FREQ] > {0}) AND ([AssignedFrequencies.TX_FREQ] < {1})) OR (([AssignedFrequencies.RX_FREQ] > {0}) AND ([AssignedFrequencies.RX_FREQ] < {1}))", CHelper.DoubleToString(fMin, '.'), CHelper.DoubleToString(fMax, '.'));
            rStations.SetAdditional(sql);

            try
            {
               int counter = 0;
               for (rStations.Open(); !rStations.IsEOF(); rStations.MoveNext())
               {
                  if (statusForEmc.Contains(rStations.GetS("STATUS")) == false)
                     continue; //Отбрасываем ненужные станции

                  if((standard == GSM_1800) || (standard == GSM_900))
                  {
                      string stnd = rStations.GetS("STANDARD");
                      if ((stnd == GSM_1800) || (stnd == GSM_900))
                          continue;
                  }

                  int id = rStations.GetI("ID");
                  if (!hash.Contains(id))
                  {
                     pBar.ShowSmall(++counter);
                     pBar.UserCanceled();
                     Station station = new Station();
                     station.ID = rStations.GetI("ID");
                     station.Load(Table);
                     StationList.Add(station);
                     hash.Add(id);
                  }
               }
            }
            finally
            {
               rStations.Close();
               rStations.Dispose();
            }
         }
			return StationList;
		}

		public void Load(string Table)
      {
         int _equipID = IM.NullI;
         {
            IMRecordset rStations = new IMRecordset(Table, IMRecordset.Mode.ReadOnly);
            rStations.Select("ID,EQUIP_ID,STANDARD,NAME,AZIMUTH,ELEVATION,PWR_ANT,TX_LOSSES,RX_LOSSES,AGL,Owner.NAME,DESIG_EMISSION,Antenna.DIAGH,Antenna.DIAGV,Antenna.GAIN, Position.PROVINCE, Position.City.CUST_TXT1, Position.SUBPROVINCE, Position.City.CUST_TXT2, Position.City.CUST_TXT3, Position.CITY, Position.ADDRESS,Equipment.SENSITIVITY,Equipment.KTBF,Equipment.RXTH_10,Equipment.C_I,DESIG_EMISSION,Position.X,Position.Y,Position.CSYS");
            rStations.Select("STATUS");
            rStations.SetWhere("ID", IMRecordset.Operation.Eq, ID);

            try
            {
               rStations.Open();
               if (!rStations.IsEOF())
               {
                  Name = rStations.GetS("NAME");
                  Owner = rStations.GetS("Owner.NAME");
                  Status = rStations.GetS("STATUS");
                  TableName = Table;

                  double PositionX = rStations.GetD("Position.X");
                  double PositionY = rStations.GetD("Position.Y");
                  string CoordinateSystem = rStations.GetS("Position.CSYS");
                  IMPosition pos1 = new IMPosition(PositionX, PositionY, CoordinateSystem);
                  IMPosition pos2 = IMPosition.Convert(pos1, "4DEC");
                  Position = XICSM.Math.SphereCoordinateByCoorinateSystem(CoordinateSystem, PositionX, PositionY);
                  //Переопределяем так как не верно считается
                  Position.Phi = pos2.Lon;
                  Position.Phita = pos2.Lat;

                  DesEmi = rStations.GetS("DESIG_EMISSION");
                  Bandwidth = 1.2;
                  if (!string.IsNullOrEmpty(DesEmi))
                  {
                     double BW = 0.0;
                     double minF = 0.0;
                     double maxF = 0.0;
                     if ((IM.DesigEmissionToBW(DesEmi, ref BW, ref minF, ref maxF) == true) && (BW != 0.0))
                        Bandwidth = BW / 1000.0;
                  }

                  Azimuth = rStations.GetD("AZIMUTH");
                  if (Azimuth == IM.NullD)
                     Azimuth = 0.0;

                  Elevation = rStations.GetD("ELEVATION");
                  if (Elevation == IM.NullD)
                     Elevation = 0.0;

                  RadiatedPower = rStations.GetD("PWR_ANT");
                  if (RadiatedPower == IM.NullD)
                     RadiatedPower = -9999.0;

                  TxLosses = rStations.GetD("TX_LOSSES");
                  if (TxLosses == IM.NullD)
                     TxLosses = 0.0;

                  RxLosses = rStations.GetD("RX_LOSSES");
                  if (RxLosses == IM.NullD)
                     RxLosses = 0.0;

                  Height = rStations.GetD("AGL");
                  if (Height == IM.NullD)
                     Height = 10.0;

                  Standard = rStations.GetS("STANDARD");
                  Address = rStations.GetS("Position.PROVINCE") +
                       " " + rStations.GetS("Position.City.CUST_TXT1") +
                       ", " + rStations.GetS("Position.SUBPROVINCE") +
                       " " + rStations.GetS("Position.City.CUST_TXT2") +
                       " " + rStations.GetS("Position.City.CUST_TXT3") +
                       ", " + rStations.GetS("Position.CITY") +
                       ", " + rStations.GetS("Position.ADDRESS");

                  MaxGain = rStations.GetD("Antenna.GAIN");
                  if (MaxGain == IM.NullD)
                     MaxGain = 0.0;

                  {
                     string diag = rStations.GetS("Antenna.DIAGH");
                     if (string.IsNullOrEmpty(diag))
                        diag = "WIEN 000ND00";
                     //??
                     HorizontalDiagramm.SetMaximalGain(MaxGain);
                     HorizontalDiagramm.Build(diag);
                  }
                  {
                     string diag = rStations.GetS("Antenna.DIAGH");
                     if (string.IsNullOrEmpty(diag))
                        diag = "WIEN 000ND00";
                     //??
                     VerticalDiagramm.SetMaximalGain(MaxGain);
                     VerticalDiagramm.Build(diag);
                  }

                  ////////////////////////////////////////////////////////////////////
                  _ktbf = rStations.GetD("Equipment.KTBF");
                  if (_ktbf == IM.NullD)
                     _ktbf = 10.0 * System.Math.Log10(1.38 * System.Math.Pow(10, -14) * 400.0 * Bandwidth);

                  _sensitivity = rStations.GetD("Equipment.SENSITIVITY");
                  if (_sensitivity == IM.NullD)
                  {
                     double ci = rStations.GetD("Equipment.C_I");
                     if (ci == IM.NullD)
                        ci = 9;
                     _sensitivity = _ktbf + ci;
                  }
                  ////////////////////////////////////////////////////////////////////

                  _equipID = rStations.GetI("EQUIP_ID");
                  if (_equipID == IM.NullI)
                     _isOK = false;
               }
            }
            finally
            {
               rStations.Close();
               rStations.Destroy();
            }
         }

         {
            // filling the data of mask transmitter
            IMRecordset rsTmp = new IMRecordset("EQUIP_PMR_MPT", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("EQUIP_ID,TYPE,FREQ,ATTN");
            rsTmp.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, _equipID);
            rsTmp.SetWhere("TYPE", IMRecordset.Operation.Like, "TS");
            rsTmp.OrderBy("FREQ", OrderDirection.Ascending);
            try
            {
               rsTmp.Open();
               if (!rsTmp.IsEOF())
               {// Есть маска
                  for (; !rsTmp.IsEOF(); rsTmp.MoveNext())
                  {
                     ACHMob.DataElement mask = new ACHMob.DataElement();
                     mask.frequency = rsTmp.GetD("FREQ");
                     mask.losses = rsTmp.GetD("ATTN");
                     _maskaT.Add(mask);
                  }
               }
               else
               {// Нет маски
                  ACHMob.DataElement mask = new ACHMob.DataElement();
                  mask.frequency = -Bandwidth / 2.0 - 0.00001;
                  mask.losses = 200.0;
                  _maskaT.Add(mask);

                  mask = new ACHMob.DataElement();
                  mask.frequency = -Bandwidth / 2.0;
                  mask.losses = 0.0;
                  _maskaT.Add(mask);

                  mask = new ACHMob.DataElement();
                  mask.frequency = Bandwidth / 2.0;
                  mask.losses = 0.0;
                  _maskaT.Add(mask);

                  mask = new ACHMob.DataElement();
                  mask.frequency = Bandwidth / 2.0 + 0.00001;
                  mask.losses = 200.0;
                  _maskaT.Add(mask);
               }
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }

         double _exfilterID = 0, _rexfilterID = 0;

         {
            // filling the ids of filter receiver and transmitter
            IMRecordset rsTmp = new IMRecordset("XNRFA_STAT_EXFLTR", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("ID,OBJ_ID,OBJ_TABLE,EX_FILTER_ID,IS_TX");
            rsTmp.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, ID);
            rsTmp.SetWhere("OBJ_TABLE", IMRecordset.Operation.Like, "MOB_STATION");
            try
            {
               for (rsTmp.Open(); !rsTmp.IsEOF(); rsTmp.MoveNext())
               {
                  int t = rsTmp.GetI("IS_TX");
                  if (t > 0)
                     _exfilterID = rsTmp.GetI("EX_FILTER_ID");
                  else
                     _rexfilterID = rsTmp.GetI("EX_FILTER_ID");
               }
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }
         //??
         //!!!!!!!!!!!!!   ВНИМАНИЕ   !!!!!!!!!!!!!!!!!!!
         //
         //   Фильтр на передачу не используется в расчете
         //    интермодуляционных помех (физика процесса)
         //
         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         _exfilterID = -1;

         {
            // filling the params of filter transmitter
            IMRecordset rsTmp = new IMRecordset("XNRFA_FILTER_PARAM", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("EXT_FILT_ID,DELTA_FREQ,LOSS");
            rsTmp.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _exfilterID);
            rsTmp.OrderBy("DELTA_FREQ", OrderDirection.Ascending);
            try
            {
               for (rsTmp.Open(); !rsTmp.IsEOF(); rsTmp.MoveNext())
               {
                  ACHMob.DataElement filter = new ACHMob.DataElement();
                  filter.frequency = rsTmp.GetD("DELTA_FREQ") / 1000.0;
                  filter.losses = rsTmp.GetD("LOSS");
                  _filterT.Add(filter);
               }
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }

         {
            // filling the params of filter receiver
            IMRecordset rsTmp = new IMRecordset("XNRFA_FILTER_PARAM", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("EXT_FILT_ID,DELTA_FREQ,LOSS");
            rsTmp.SetWhere("EXT_FILT_ID", IMRecordset.Operation.Eq, _rexfilterID);
            rsTmp.OrderBy("DELTA_FREQ", OrderDirection.Ascending);
            try
            {
               for (rsTmp.Open(); !rsTmp.IsEOF(); rsTmp.MoveNext())
               {
                  ACHMob.DataElement filter = new ACHMob.DataElement();
                  filter.frequency = rsTmp.GetD("DELTA_FREQ") / 1000.0;
                  filter.losses = rsTmp.GetD("LOSS");
                  _filterR.Add(filter);
               }
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }

         {
            IMRecordset rsTmp = new IMRecordset("XNRFA_EXTERN_FILTERS", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("ID,FREQ");
            rsTmp.SetWhere("ID", IMRecordset.Operation.Eq, _exfilterID);
            try
            {
               rsTmp.Open();
               if(!rsTmp.IsEOF())
                  _freqFilterTX = rsTmp.GetD("FREQ");
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }

         {
            IMRecordset rsTmp = new IMRecordset("XNRFA_EXTERN_FILTERS", IMRecordset.Mode.ReadOnly);
            rsTmp.Select("ID,FREQ");
            rsTmp.SetWhere("ID", IMRecordset.Operation.Eq, _rexfilterID);
            try
            {
               rsTmp.Open();
               if (!rsTmp.IsEOF())
                  _freqFilterRX = rsTmp.GetD("FREQ");
            }
            finally
            {
               rsTmp.Close();
               rsTmp.Destroy();
            }
         }
      }

		public void LoadChannels(string Table, double Fmin, double Fmax, bool isTX, double FminAnaliz, double FmaxAnaliz)
		{
			TransmitterList.Clear();
			ReceiverList.Clear();

         Fmin -= Bandwidth / 2.0;
         Fmax += Bandwidth / 2.0;
         FminAnaliz -= Bandwidth / 2.0;
         FmaxAnaliz += Bandwidth / 2.0;

			IMRecordset rChannel = new IMRecordset(Table, IMRecordset.Mode.ReadOnly);
			rChannel.Select("ID,STA_ID,TX_FREQ,RX_FREQ");
			rChannel.SetWhere("STA_ID", IMRecordset.Operation.Eq, ID);

			rChannel.Open();			
			while (!rChannel.IsEOF())
			{
				Channel TxChannel = new Channel();
				Channel RxChannel = new Channel();	
			   TxChannel.ID = RxChannel.ID = rChannel.GetI("ID");
				TxChannel.Station = RxChannel.Station = this;
				TxChannel.Frequency = rChannel.GetD("TX_FREQ");
				RxChannel.Frequency = rChannel.GetD("RX_FREQ");

            if ((isTX == true) && (Fmin <= TxChannel.Frequency) && (TxChannel.Frequency <= Fmax))
               TransmitterList.Add(TxChannel);
            else if (isTX == false)
            {
               if((Fmin <= RxChannel.Frequency) && (RxChannel.Frequency <= Fmax))
                  ReceiverList.Add(RxChannel);
               if ((FminAnaliz <= TxChannel.Frequency) && (TxChannel.Frequency <= FmaxAnaliz)
                   && ((TxChannel.Frequency < 914.0) || (960.0 < TxChannel.Frequency))
                   && ((TxChannel.Frequency < 1805.0) || (1880.0 < TxChannel.Frequency))
                  )
                  TransmitterList.Add(TxChannel);
            }

            rChannel.MoveNext();
			}

			rChannel.Close();
			rChannel.Dispose();			
		}

		public double RangeToStation(Station station)
		{			
			Math.SphereCoordinate Sphere1 = GetPosition();
			Math.SphereCoordinate Sphere2 = station.GetPosition();
		    double l1 = System.Math.Abs(Sphere1.Phita - Sphere2.Phita)*111.0;
            double l2 = System.Math.Abs(Sphere1.Phi - Sphere2.Phi) * 111.0 * System.Math.Cos((Sphere1.Phita + Sphere2.Phita)/2.0*System.Math.PI/180.0);
		    double d = System.Math.Sqrt(l1*l1 + l2*l2);
		    return d;
			//return XICSM.Math.RangeOnSphereCoorinates(Sphere1, Sphere2);
		}

		public double GetHorizontalAngle(Station station)
		{
			Math.SphereCoordinate Sphere1 = GetPosition();
			Math.SphereCoordinate Sphere2 = station.GetPosition();
			return XICSM.Math.GetAzimuth(Sphere1, Sphere2);			
		}

		public double GetVerticalAngle(Station station)
		{
			double Range = RangeToStation(station);
			double AvegareRedius = 6371.0; //Average Radius;
			return ((station.GetHeight() - GetHeight()) / Range - 1000.0 * Range / (2 * AvegareRedius))/1000.0;
		}

		public double GetLosses(double HorizontalAngle, double VerticalAngle)
		{
            if (HorizontalAngle < 0)
                HorizontalAngle = 360.0 + HorizontalAngle;
			double HorizontalLosses = HorizontalDiagramm.GetLossesAmount(HorizontalAngle);
			double VerticalLosses = VerticalDiagramm.GetLossesAmount(VerticalAngle);

			if (HorizontalLosses < VerticalLosses)
				return HorizontalLosses;
			else
				return VerticalLosses;			
		}
		public bool IsDiagrammsValid()
		{
			return (HorizontalDiagramm.IsValid() && VerticalDiagramm.IsValid());
		}

      //===================================================
      /// <summary>
      /// Максимальная частота передатчика
      /// </summary>
      /// <returns>Максимальная частота передатчика</returns>
      public double GetMaxFreqTX()
      {
         if ((TransmitterList == null) || (TransmitterList.Count < 1))
            return IM.NullD;
         double retVal = IM.NullD;
         for(int i=0; i < TransmitterList.Count; i++)
         {
            if(i==0)
               retVal = TransmitterList[i].Frequency;
            else if(retVal < TransmitterList[i].Frequency)
               retVal = TransmitterList[i].Frequency;
         }
         return retVal;
      }
      //===================================================
      /// <summary>
      /// Минимальная частота передатчика
      /// </summary>
      /// <returns>Минимальная частота передатчика</returns>
      public double GetMinFreqTX()
      {
         if ((TransmitterList == null) || (TransmitterList.Count < 1))
            return IM.NullD;
         double retVal = IM.NullD;
         for (int i = 0; i < TransmitterList.Count; i++)
         {
            if (i == 0)
               retVal = TransmitterList[i].Frequency;
            else if (retVal > TransmitterList[i].Frequency)
               retVal = TransmitterList[i].Frequency;
         }
         return retVal;
      }

      //public static void OnAdditionFiltersShows(String Table, int StationID)
      //{         
      //   IMRecordset rStations = new IMRecordset(Table, IMRecordset.Mode.ReadOnly);

      //   rStations.Select("ID,STANDARD,NAME,AZIMUTH,ELEVATION,PWR_ANT,TX_LOSSES,RX_LOSSES,AGL,Owner.NAME,DESIG_EMISSION,Position.X,Position.Y,Position.CSYS,Antenna.DIAGH,Antenna.DIAGV,Antenna.GAIN, Position.PROVINCE, Position.City.CUST_TXT1, Position.SUBPROVINCE, Position.City.CUST_TXT2, Position.City.CUST_TXT3, Position.CITY, Position.ADDRESS");
      //   rStations.SetWhere("ID", IMRecordset.Operation.Eq, StationID);
      //   rStations.Open();

      //   int ID = rStations.GetI("ID");

      //   double PositionX = rStations.GetD("Position.X");
      //   double PositionY = rStations.GetD("Position.Y");
      //   string CoordinateSystem = rStations.GetS("Position.CSYS");

      //   rStations.Close();
      //   rStations.Destroy();

      //   MessageBox.Show("Position =" + PositionX, "Position =" + PositionX);
      //}
	}
}
