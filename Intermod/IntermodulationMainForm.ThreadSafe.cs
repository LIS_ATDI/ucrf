﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections;
//using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace XICSM.Intermodulation
{
   partial class IntermodulationMainForm
   {
      // Делегаты
      public delegate void AddNewNode(string _str);
      public delegate void UpdateMessageDelegate(string _str);
      public delegate void SetProgressMaxDelegate(int _maxValue);
      public delegate void SetProgressValueDelegate(int _value);
      public delegate void TreeViewDraw();

      //=================================================
      // Заблокировать прорисовку дерева
      //=================================================
      public void TreeViewDrawDisable()
      {
         if (!_backgroundWork)
         {
            ResultTreeView.BeginUpdate();
         }
      }
      //=================================================
      // Разблокировать прорисовку дерева
      //=================================================
      public void TreeViewDrawEnable()
      {
         if (!_backgroundWork)
         {
            ResultTreeView.EndUpdate();
         }
      }
      //=================================================
      // Добавить основное дерево грида
      //=================================================
      public void AddNewMainNode(string _str)
      {
         if (!_backgroundWork)
         {
            ReceiverMainNode = ResultTreeView.Nodes.Add(_str);
         }
      }
      //=================================================
      // Добавить поддерево грида
      //=================================================
      public void AddNewSubNode(string _str)
      {
         if (!_backgroundWork)
         {
            InterferenceMainNode = ReceiverMainNode.Nodes.Add(_str);
         }
      }
      //=================================================
      // Добавить под-поддерево грида
      //=================================================
      public void AddNewSubSubNode(string _str)
      {
         if (!_backgroundWork)
         {
            NewNode = InterferenceMainNode.Nodes.Add(_str);
         }
      }
      //=================================================
      // Обновляем строку сообщений (Thread-safe)
      //=================================================
      public void SetMessageThreadSafe(string _str)
      {
         if (!_backgroundWork)
         {
            if (labelMessage.InvokeRequired)
               labelMessage.Invoke(new UpdateMessageDelegate(UpdateMessage), new object[] { _str });
            else
               UpdateMessage(_str);
         }
      }
      //=================================================
      // Делегат для обновления строки сообщений
      private void UpdateMessage(string _str)
      {
         if (!_backgroundWork)
         {
            labelMessage.Text = _str;
         }
      }
      //=================================================
      // Обновляем вспомогательную строку (Thread-safe)
      //=================================================
      public void SetDopMessageThreadSafe(string _str)
      {
         if (!_backgroundWork)
         {
            if (labelValue.InvokeRequired)
               labelValue.Invoke(new UpdateMessageDelegate(UpdateDopMessage), new object[] { _str });
            else
               UpdateDopMessage(_str);
         }
      }
      //=================================================
      // Делегат для обновления строки сообщений
      private void UpdateDopMessage(string _str)
      {
         if (!_backgroundWork)
         {
            labelValue.Text = _str;
         }
      }
      //=================================================
      // Установить максимальное значение ProgressBar (Thread-safe)
      //=================================================
      public void SetProgressMaxThreadSafe(int _maxValue)
      {
         if (!_backgroundWork)
         {
            if (progressBar.InvokeRequired)
               progressBar.Invoke(new SetProgressMaxDelegate(SetProgressMax), new object[] { _maxValue });
            else
               SetProgressMax(_maxValue);
         }
      }
      //=================================================
      // Делегат для установки максимального значения ProgresBar
      private void SetProgressMax(int _maxValue)
      {
         if (!_backgroundWork)
         {
            progressBar.Maximum = _maxValue;
         }
      }
      //=================================================
      // Установить текущее значение ProgressBar (Thread-safe)
      //=================================================
      public void SetProgressValueThreadSafe(int _maxValue)
      {
         if (!_backgroundWork)
         {
            if (progressBar.InvokeRequired)
               progressBar.Invoke(new SetProgressValueDelegate(SetProgressValue), new object[] { _maxValue });
            else
               SetProgressValue(_maxValue);
         }
      }
      //=================================================
      // Делегат для установки текущего значения ProgresBar
      private void SetProgressValue(int _maxValue)
      {
         if (!_backgroundWork)
         {
            progressBar.Value = _maxValue;
            string strPercent = "";
            if (progressBar.Maximum > 0)
            {
               try
               {
                  double percent = (double)_maxValue * 100.0 / (double)progressBar.Maximum / 100.0;
                  if (percent > 0.1)
                     strPercent = percent.ToString("0.00%");
               }
               catch { }
            }
            UpdateDopMessage(strPercent);
         }
      }
      //===================================================================
      //===================================================================
      //===================================================================
      //            Функции для работы с WorkerThread
      //===================================================================
      //===================================================================
      //===================================================================
      //*******************************************************************
      //===================================================================
      // Выпонить фоновую задачу
      //===================================================================
      private void WorkerThread_DoWork(object sender, DoWorkEventArgs e)
      {
         FindMethod();
      }
      //===================================================================
      // Выпонение паралельного потока закончено
      //===================================================================
      private void WorkerThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         SetMessageThreadSafe("");
         SetProgressValueThreadSafe(0);
         // First, handle the case where an exception was thrown.
         if (e.Error != null)
         {
            MessageBox.Show(e.Error.Message, TLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Question);
            SetMessageThreadSafe(e.Error.Message);
         }
         else if (e.Cancelled)
         {
            // Next, handle the case where the user canceled 
            // the operation.
            // Note that due to a race condition in 
            // the DoWork event handler, the Cancelled
            // flag may not have been set, even though
            // CancelAsync was called.
            MessageBox.Show(TLocaliz.TxT("Canceled by user"), TLocaliz.TxT("Error"), MessageBoxButtons.OK, MessageBoxIcon.Question);
            SetMessageThreadSafe(TLocaliz.TxT("Canceled by user"));
         }
         else
         {
            // Finally, handle the case where the operation succeeded.
            SetMessageThreadSafe(TLocaliz.TxT("Finished"));
         }
      }      
   }
}
