﻿using ICSM;
using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace XICSM.RedRegister
{
    public class Plugin : IPlugin
    {
        public string Ident { get { return "RedRegister"; } }
        public string Description { get { return "Модуль Реєстру РЕЗ та ВП"; } }
        public double SchemaVersion { get { return 20121129.1500; } }

        public void RegisterSchema(IMSchema s)
        {
            s.DeclareTable("XREDREG_CONTENT", "RED Register imported content", "ADM_EQUIP");
            s.DeclareField("ID", "NUMBER(9,0)", "", "NOTNULL", "");
            s.DeclareIndex("PK_XREDREG_CONTENT", "PRIMARY", "ID");
            s.DeclareField("EQUIP_ID", "NUMBER(9,0)", "", "", "");
            s.DeclareJoin("AdmEquip", "ADM_EQUIP", "DBMS", "EQUIP_ID", "ID");
            s.DeclareField("NUM", "NUMBER(9,0)", "", "NOTNULL", "");
            //s.DeclareField("STATUS", "VARCHAR(2)", "", "", "");
            //s.Info("Status");
            s.DeclareField("CONTENT", "VARCHAR(4000)", "", "", "");
            //s.Info("Unit content");
            
        }

        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            try
            {
                if (dbCurVersion < 20121129.1500)
                {
                    s.CreateTables("XREDREG_CONTENT");
                    s.SetDatabaseVersion(20121129.1500);
                }
                /*
                else if (dbCurVersion < )
                {
                    
                }
                */
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("UpgradeDatabase(): {0}", ex.Message), Description, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public void GetMainMenu(IMMainMenu mainMenu)
        {
        }

        public bool OtherMessage(string message, object inParam, ref object outParam)
        {
            switch (message)
            {
                case "DBLCLK_RECORD":
                    if (inParam is RecordPtr)
                    {
                        RecordPtr rp = (RecordPtr)inParam;
                        if (rp.Table == "XREDREG_CONTENT")
                        {
                            OnRedRegEdit(new IMQueryMenuNode.Context() { TableId = rp.Id, TableName = rp.Table });
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }

        public void RegisterBoard(IMBoard b)
        {
            b.RegisterQueryMenuBuilder("ADM_EQUIP", OnQueryAdmEquipMenuBuilder);
            b.RegisterQueryMenuBuilder("XREDREG_CONTENT", OnQueryRedRegMenuBuilder);
        }

        public List<IMQueryMenuNode> OnQueryAdmEquipMenuBuilder(string tableName, int nbRecMin)
        {
            List<IMQueryMenuNode> mnl = new List<IMQueryMenuNode>();
            if (nbRecMin == 1 && (IM.TableRight("ADM_EQUIP") & IMTableRight.Update) == IMTableRight.Update)
                mnl.Add(new IMQueryMenuNode("Запис Реєстру РЕЗ та ВП...", "", OnRedRegEdit, IMQueryMenuNode.ExecMode.FirstRecord));
            return mnl;
        }

        public List<IMQueryMenuNode> OnQueryRedRegMenuBuilder(string tableName, int nbRecMin)
        {
            List<IMQueryMenuNode> mnl = new List<IMQueryMenuNode>();
            if (nbRecMin == 1 && (IM.TableRight("ADM_EQUIP") & IMTableRight.Update) == IMTableRight.Update)
                mnl.Add(new IMQueryMenuNode("Edit record...", "", OnRedRegEdit, IMQueryMenuNode.ExecMode.FirstRecord));
            return mnl;
        }

        public bool OnRedRegEdit(IMQueryMenuNode.Context context)
        {
            if (context.TableName == "ADM_EQUIP" || context.TableName == "XREDREG_CONTENT")
            {
                int id = context.TableId;
                if (context.TableName == "XREDREG_CONTENT")
                {
                    IMRecordset rs = IMRecordset.ForRead(new RecordPtr(context.TableName, context.TableId), "EQUIP_ID");
                    id = rs.GetI("EQUIP_ID");
                    rs.Destroy();
                }
                            
                using (RedEntryForm frm = new RedEntryForm(id))
                {
                    frm.ShowDialog();
                }
            }
            return false;
        }
    }
}
