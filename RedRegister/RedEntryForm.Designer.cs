﻿namespace XICSM.RedRegister
{
    partial class RedEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RedEntryForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbxPurposes = new System.Windows.Forms.ListBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lbxAreas = new System.Windows.Forms.ListBox();
            this.btClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grdEqBlocks = new System.Windows.Forms.DataGridView();
            this.lbMaxPower = new System.Windows.Forms.Label();
            this.tbMaxPower = new System.Windows.Forms.TextBox();
            this.tbMaxPowerUnit = new System.Windows.Forms.TextBox();
            this.tbSensitivityUnit = new System.Windows.Forms.TextBox();
            this.tbSensitivity = new System.Windows.Forms.TextBox();
            this.lbSensitivity = new System.Windows.Forms.Label();
            this.lbTechRemark = new System.Windows.Forms.Label();
            this.tbTechRemark = new System.Windows.Forms.TextBox();
            this.IsActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Rtech = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DbSection = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bands = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmiClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btUse = new System.Windows.Forms.Button();
            this.redEntryFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEqBlocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redEntryFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "title", true));
            this.textBox1.Location = new System.Drawing.Point(133, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(636, 20);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "trademark", true));
            this.textBox2.Location = new System.Drawing.Point(133, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "model", true));
            this.textBox3.Location = new System.Drawing.Point(133, 64);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 3;
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "manufacturerName", true));
            this.textBox4.Location = new System.Drawing.Point(133, 99);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(200, 20);
            this.textBox4.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Назва";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Торгова марка";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Модель";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Виробник";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Виробник в БД";
            // 
            // textBox5
            // 
            this.textBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "manufacturerDb", true));
            this.textBox5.Location = new System.Drawing.Point(133, 125);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(200, 36);
            this.textBox5.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Країна";
            // 
            // textBox6
            // 
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "manufacturerCountry", true));
            this.textBox6.Location = new System.Drawing.Point(133, 169);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(200, 20);
            this.textBox6.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Країна виробництва";
            // 
            // textBox7
            // 
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "manufactureCountry", true));
            this.textBox7.Location = new System.Drawing.Point(133, 195);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(200, 20);
            this.textBox7.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(375, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Розділ Реєстру";
            // 
            // textBox8
            // 
            this.textBox8.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "sectionNum", true));
            this.textBox8.Location = new System.Drawing.Point(378, 57);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(64, 20);
            this.textBox8.TabIndex = 17;
            // 
            // textBox9
            // 
            this.textBox9.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "sectionTitle", true));
            this.textBox9.Location = new System.Drawing.Point(506, 39);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(263, 38);
            this.textBox9.TabIndex = 18;
            // 
            // textBox10
            // 
            this.textBox10.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "classifierTitle", true));
            this.textBox10.Location = new System.Drawing.Point(506, 87);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(263, 38);
            this.textBox10.TabIndex = 21;
            // 
            // textBox11
            // 
            this.textBox11.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "classifierCode", true));
            this.textBox11.Location = new System.Drawing.Point(378, 105);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(122, 20);
            this.textBox11.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(375, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Класифікатор";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(375, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Рішення №";
            // 
            // textBox12
            // 
            this.textBox12.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "decreeRef", true));
            this.textBox12.Location = new System.Drawing.Point(443, 139);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(57, 20);
            this.textBox12.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(506, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "від";
            // 
            // textBox13
            // 
            this.textBox13.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "decreeDate", true));
            this.textBox13.Location = new System.Drawing.Point(533, 139);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(104, 20);
            this.textBox13.TabIndex = 25;
            // 
            // textBox14
            // 
            this.textBox14.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "certificateBegDate", true));
            this.textBox14.Location = new System.Drawing.Point(533, 165);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(104, 20);
            this.textBox14.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(506, 168);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "від";
            // 
            // textBox15
            // 
            this.textBox15.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "certificateRef", true));
            this.textBox15.Location = new System.Drawing.Point(443, 165);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(57, 20);
            this.textBox15.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(375, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Сертифікат";
            // 
            // textBox16
            // 
            this.textBox16.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "certificateEndDate", true));
            this.textBox16.Location = new System.Drawing.Point(665, 165);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(104, 20);
            this.textBox16.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(644, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "до";
            // 
            // textBox17
            // 
            this.textBox17.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "declarationModule", true));
            this.textBox17.Location = new System.Drawing.Point(559, 191);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(78, 20);
            this.textBox17.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(506, 194);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Модуль";
            // 
            // textBox18
            // 
            this.textBox18.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.redEntryFormBindingSource, "declarationRef", true));
            this.textBox18.Location = new System.Drawing.Point(443, 191);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(57, 20);
            this.textBox18.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(375, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Декларація";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(51, 231);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "Призначення";
            // 
            // lbxPurposes
            // 
            this.lbxPurposes.FormattingEnabled = true;
            this.lbxPurposes.Location = new System.Drawing.Point(133, 231);
            this.lbxPurposes.Name = "lbxPurposes";
            this.lbxPurposes.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbxPurposes.Size = new System.Drawing.Size(307, 56);
            this.lbxPurposes.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(465, 231);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Регіони використання";
            // 
            // lbxAreas
            // 
            this.lbxAreas.FormattingEnabled = true;
            this.lbxAreas.Location = new System.Drawing.Point(592, 231);
            this.lbxAreas.Name = "lbxAreas";
            this.lbxAreas.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbxAreas.Size = new System.Drawing.Size(177, 56);
            this.lbxAreas.TabIndex = 39;
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btClose.Location = new System.Drawing.Point(657, 575);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(112, 23);
            this.btClose.TabIndex = 40;
            this.btClose.Text = "Закрити";
            this.btClose.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btUse);
            this.groupBox1.Controls.Add(this.tbTechRemark);
            this.groupBox1.Controls.Add(this.lbTechRemark);
            this.groupBox1.Controls.Add(this.tbSensitivityUnit);
            this.groupBox1.Controls.Add(this.tbSensitivity);
            this.groupBox1.Controls.Add(this.lbSensitivity);
            this.groupBox1.Controls.Add(this.tbMaxPowerUnit);
            this.groupBox1.Controls.Add(this.tbMaxPower);
            this.groupBox1.Controls.Add(this.lbMaxPower);
            this.groupBox1.Controls.Add(this.grdEqBlocks);
            this.groupBox1.Location = new System.Drawing.Point(16, 293);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 276);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Технічні характеристики";
            // 
            // grdEqBlocks
            // 
            this.grdEqBlocks.AllowUserToAddRows = false;
            this.grdEqBlocks.AllowUserToDeleteRows = false;
            this.grdEqBlocks.AllowUserToResizeColumns = false;
            this.grdEqBlocks.AllowUserToResizeRows = false;
            this.grdEqBlocks.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.grdEqBlocks.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdEqBlocks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdEqBlocks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEqBlocks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsActive,
            this.Rtech,
            this.DbSection,
            this.bands,
            this.EmiClass});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdEqBlocks.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdEqBlocks.Location = new System.Drawing.Point(6, 19);
            this.grdEqBlocks.MultiSelect = false;
            this.grdEqBlocks.Name = "grdEqBlocks";
            this.grdEqBlocks.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdEqBlocks.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grdEqBlocks.RowHeadersVisible = false;
            this.grdEqBlocks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.grdEqBlocks.RowTemplate.Height = 17;
            this.grdEqBlocks.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdEqBlocks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEqBlocks.ShowCellErrors = false;
            this.grdEqBlocks.ShowEditingIcon = false;
            this.grdEqBlocks.ShowRowErrors = false;
            this.grdEqBlocks.Size = new System.Drawing.Size(391, 251);
            this.grdEqBlocks.TabIndex = 0;
            this.grdEqBlocks.VirtualMode = true;
            this.grdEqBlocks.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEqBlocks_CellDoubleClick);
            this.grdEqBlocks.CurrentCellChanged += new System.EventHandler(this.grdEqBlocks_CurrentCellChanged);
            // 
            // lbMaxPower
            // 
            this.lbMaxPower.AutoSize = true;
            this.lbMaxPower.Location = new System.Drawing.Point(424, 25);
            this.lbMaxPower.Name = "lbMaxPower";
            this.lbMaxPower.Size = new System.Drawing.Size(136, 13);
            this.lbMaxPower.TabIndex = 1;
            this.lbMaxPower.Text = "Максимальна потужність";
            // 
            // tbMaxPower
            // 
            this.tbMaxPower.Location = new System.Drawing.Point(566, 22);
            this.tbMaxPower.Name = "tbMaxPower";
            this.tbMaxPower.ReadOnly = true;
            this.tbMaxPower.Size = new System.Drawing.Size(78, 20);
            this.tbMaxPower.TabIndex = 2;
            // 
            // tbMaxPowerUnit
            // 
            this.tbMaxPowerUnit.Location = new System.Drawing.Point(658, 22);
            this.tbMaxPowerUnit.Name = "tbMaxPowerUnit";
            this.tbMaxPowerUnit.ReadOnly = true;
            this.tbMaxPowerUnit.Size = new System.Drawing.Size(67, 20);
            this.tbMaxPowerUnit.TabIndex = 3;
            // 
            // tbSensitivityUnit
            // 
            this.tbSensitivityUnit.Location = new System.Drawing.Point(658, 48);
            this.tbSensitivityUnit.Name = "tbSensitivityUnit";
            this.tbSensitivityUnit.ReadOnly = true;
            this.tbSensitivityUnit.Size = new System.Drawing.Size(67, 20);
            this.tbSensitivityUnit.TabIndex = 6;
            // 
            // tbSensitivity
            // 
            this.tbSensitivity.Location = new System.Drawing.Point(566, 48);
            this.tbSensitivity.Name = "tbSensitivity";
            this.tbSensitivity.ReadOnly = true;
            this.tbSensitivity.Size = new System.Drawing.Size(78, 20);
            this.tbSensitivity.TabIndex = 5;
            // 
            // lbSensitivity
            // 
            this.lbSensitivity.AutoSize = true;
            this.lbSensitivity.Location = new System.Drawing.Point(424, 51);
            this.lbSensitivity.Name = "lbSensitivity";
            this.lbSensitivity.Size = new System.Drawing.Size(114, 13);
            this.lbSensitivity.TabIndex = 4;
            this.lbSensitivity.Text = "Чутливість приймача";
            // 
            // lbTechRemark
            // 
            this.lbTechRemark.AutoSize = true;
            this.lbTechRemark.Location = new System.Drawing.Point(424, 77);
            this.lbTechRemark.Name = "lbTechRemark";
            this.lbTechRemark.Size = new System.Drawing.Size(54, 13);
            this.lbTechRemark.TabIndex = 7;
            this.lbTechRemark.Text = "Примітки";
            // 
            // tbTechRemark
            // 
            this.tbTechRemark.Location = new System.Drawing.Point(427, 93);
            this.tbTechRemark.Multiline = true;
            this.tbTechRemark.Name = "tbTechRemark";
            this.tbTechRemark.ReadOnly = true;
            this.tbTechRemark.Size = new System.Drawing.Size(298, 135);
            this.tbTechRemark.TabIndex = 8;
            // 
            // IsActive
            // 
            this.IsActive.DataPropertyName = "IsActive";
            this.IsActive.HeaderText = "Вик.";
            this.IsActive.Name = "IsActive";
            this.IsActive.ReadOnly = true;
            this.IsActive.Width = 45;
            // 
            // Rtech
            // 
            this.Rtech.DataPropertyName = "Rtech";
            this.Rtech.HeaderText = "Р/тех";
            this.Rtech.Name = "Rtech";
            this.Rtech.ReadOnly = true;
            this.Rtech.Width = 70;
            // 
            // DbSection
            // 
            this.DbSection.DataPropertyName = "DbSection";
            this.DbSection.HeaderText = "";
            this.DbSection.Name = "DbSection";
            this.DbSection.ReadOnly = true;
            this.DbSection.Width = 35;
            // 
            // bands
            // 
            this.bands.DataPropertyName = "FreqBands";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bands.DefaultCellStyle = dataGridViewCellStyle2;
            this.bands.HeaderText = "Смуги прд/прм";
            this.bands.Name = "bands";
            this.bands.ReadOnly = true;
            this.bands.Width = 140;
            // 
            // EmiClass
            // 
            this.EmiClass.DataPropertyName = "EmiClass";
            this.EmiClass.HeaderText = "Режим";
            this.EmiClass.Name = "EmiClass";
            this.EmiClass.ReadOnly = true;
            this.EmiClass.Width = 80;
            // 
            // btUse
            // 
            this.btUse.Location = new System.Drawing.Point(429, 238);
            this.btUse.Name = "btUse";
            this.btUse.Size = new System.Drawing.Size(139, 23);
            this.btUse.TabIndex = 9;
            this.btUse.Text = "Використовувати";
            this.btUse.UseVisualStyleBackColor = true;
            this.btUse.Click += new System.EventHandler(this.btUse_Click);
            // 
            // redEntryFormBindingSource
            // 
            this.redEntryFormBindingSource.DataSource = typeof(XICSM.RedRegister.RedEntryForm);
            // 
            // RedEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btClose;
            this.ClientSize = new System.Drawing.Size(787, 606);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.lbxAreas);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lbxPurposes);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RedEntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Позиція Реєстру РЕЗ та ВП";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEqBlocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redEntryFormBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListBox lbxPurposes;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListBox lbxAreas;
        private System.Windows.Forms.BindingSource redEntryFormBindingSource;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView grdEqBlocks;
        private System.Windows.Forms.TextBox tbTechRemark;
        private System.Windows.Forms.Label lbTechRemark;
        private System.Windows.Forms.TextBox tbSensitivityUnit;
        private System.Windows.Forms.TextBox tbSensitivity;
        private System.Windows.Forms.Label lbSensitivity;
        private System.Windows.Forms.TextBox tbMaxPowerUnit;
        private System.Windows.Forms.TextBox tbMaxPower;
        private System.Windows.Forms.Label lbMaxPower;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rtech;
        private System.Windows.Forms.DataGridViewTextBoxColumn DbSection;
        private System.Windows.Forms.DataGridViewTextBoxColumn bands;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmiClass;
        private System.Windows.Forms.Button btUse;
    }
}

