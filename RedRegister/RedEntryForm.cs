﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using ICSM;
using Oracle.DataAccess.Client;
using OrmCs;

namespace XICSM.RedRegister
{
    public partial class RedEntryForm : Form
    {
        protected RedEntryForm()
        {
            InitializeComponent();
        }

        int id;

        public string title { get; set; }
        public string trademark { get; set; }
        public string model { get; set; }

        public string manufacturerName { get; set; }
        public string manufacturerCountry { get; set; }
        public string manufactureCountry { get; set; }
        public string manufacturerDb { get; set; }
        int mnfId = IM.NullI;

        public string sectionNum { get; set; }
        public string sectionTitle { get; set; }

        public string classifierCode { get; set; }
        public string classifierTitle { get; set; }

        public string decreeRef { get; set; }
        public DateTime decreeDate { get; set; }

        public string certificateRef { get; set; }
        public DateTime certificateBegDate { get; set; }
        public DateTime certificateEndDate { get; set; }

        public string declarationRef { get; set; }
        public string declarationModule { get; set; }

        public List<string> purposes = new List<string>();
        public List<string> areas = new List<string>();

        internal class NatFPlanEntry
        {
            public int Id = IM.NullI;
            public double FreqTxLow = IM.NullD;
            public double FreqTxHigh = IM.NullD;
            public double FreqRxLow = IM.NullD;
            public double FreqRxHigh = IM.NullD;
        }

        internal class Equipment
        {
            public int Id { get; set; }
            public bool IsValid { get; set; }
            public bool IsActive { get; set; }
            public string Rtech { get; set; }
            public string DbSection { get; set; }
            public double FreqTxLow = IM.NullD;
            public double FreqTxHigh = IM.NullD;
            public double FreqRxLow = IM.NullD;
            public double FreqRxHigh = IM.NullD;
            public string FreqBands { 
                get 
                { 
                    string retval = "";
                    if ((FreqTxLow != IM.NullD) || (FreqTxHigh != IM.NullD)) 
                        retval = "прд: " + (FreqTxLow != IM.NullD ? FreqTxLow.ToString() : "???") + " - " + (FreqTxHigh != IM.NullD ? FreqTxHigh.ToString() : "???");
                    if ((FreqRxLow != IM.NullD) || (FreqRxHigh != IM.NullD)) 
                    { 
                        if (retval.Length > 0) retval += Environment.NewLine;
                        retval += ("прм: " + (FreqRxLow != IM.NullD ? FreqRxLow.ToString() : "???") + " - " + (FreqRxHigh != IM.NullD ? FreqRxHigh.ToString() : "???"));
                    }
                    return retval;
                }
            }
            public string EmiClass { get; set; }
            public int PlanId { get; set; }

            public string MaxPower { get; set; }
            public string MaxPowerUnit { get; set; }
            public string Sensitivity { get; set; }
            public string SensitivityUnit { get; set; }
            public string TechRemark { get; set; }

            public Equipment baseEq = null;

            public XElement eqBlElm = null;

            public static string unknownSection = "???";
            public static string[] types = new string[] {"PMR", "MW", "ESTA", "MOB2", "BRO"};
        }

        BindingList<Equipment> eqList = new BindingList<Equipment>();
        List<Equipment> eqBaseList = new List<Equipment>();
        internal BindingSource eqBindingSource;
        
        public RedEntryForm(int id) : this()
        {
            this.id = id;
            //decreeDate = IM.NullT;
            //certificateBegDate = IM.NullT;
            //certificateEndDate = IM.NullT;
            try
            {
                string contentStr = "";

                System.Globalization.NumberFormatInfo customProvider = new System.Globalization.NumberFormatInfo();
                IFormatProvider decSepProvider = customProvider;
                customProvider.NumberDecimalSeparator = ".";

                IMRecordset rs = new IMRecordset("XREDREG_CONTENT", IMRecordset.Mode.ReadOnly);
                mnfId = IM.NullI;
                string mnfName = "";
                string mnfCountry = "";
                manufacturerDb = " <немає> ";
                try
                {
                    rs.Select("ID,EQUIP_ID,NUM,CONTENT,AdmEquip.Manufacturer.ID,AdmEquip.Manufacturer.NAME,AdmEquip.Manufacturer.COUNTRY_ID");
                    rs.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, id);
                    rs.OrderBy("NUM", OrderDirection.Ascending);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        contentStr += rs.GetS("CONTENT");
                        mnfId = rs.GetI("AdmEquip.Manufacturer.ID");
                        mnfName = rs.GetS("AdmEquip.Manufacturer.NAME");
                        mnfCountry = rs.GetS("AdmEquip.Manufacturer.COUNTRY_ID");
                        manufacturerDb = mnfName + " (" + mnfCountry + ")";
                    }
                }
                finally
                {
                    rs.Destroy();
                }

                if (string.IsNullOrEmpty(contentStr))
                    throw new Exception("Схоже, запис не був імпортований з Реєстру (принаймні, імпортовані дані відсутні)");

                XDocument content = XDocument.Parse(contentStr);
                XElement root = content.Root;
                XElement xelm = null;
                XElement xelm2 = null;

                if ((xelm = root.Element("title")) != null)
                    title = xelm.Value;
                if ((xelm = root.Element("trademark")) != null)
                    trademark = xelm.Value;
                if ((xelm = root.Element("model")) != null)
                    model = xelm.Value;
                if ((xelm = root.Element("section")) != null)
                {
                    if ((xelm2 = xelm.Element("number")) != null)
                        sectionNum = xelm2.Value;
                    if ((xelm2 = xelm.Element("title")) != null)
                        sectionTitle = xelm2.Value;
                }

                if ((xelm = root.Element("classifier")) != null)
                {
                    if ((xelm2 = xelm.Element("code")) != null)
                        classifierCode = xelm2.Value;
                    if ((xelm2 = xelm.Element("title")) != null)
                        classifierTitle = xelm2.Value;
                }

                if ((xelm = root.Element("declaration")) != null)
                {
                    if ((xelm2 = xelm.Element("code")) != null)
                        declarationRef = xelm2.Value;
                    if ((xelm2 = xelm.Element("markmodule")) != null)
                        declarationModule = xelm2.Value;
                }

                if ((xelm = root.Element("certificate")) != null)
                {
                    if ((xelm2 = xelm.Element("code")) != null)
                        certificateRef = xelm2.Value;
                    if ((xelm2 = xelm.Element("begindate")) != null)
                        try { certificateBegDate = Convert.ToDateTime(xelm2.Value); } 
                        catch {}
                    if ((xelm2 = xelm.Element("enddate")) != null)
                        try { certificateEndDate = Convert.ToDateTime(xelm2.Value); }
                        catch { }
                }

                if ((xelm = root.Element("solution")) != null)
                {
                    if ((xelm2 = xelm.Element("code")) != null)
                        decreeRef = xelm2.Value;
                    if ((xelm2 = xelm.Element("date")) != null)
                        try { decreeDate = Convert.ToDateTime(xelm2.Value); }
                        catch { }
                }

                if ((xelm = root.Element("manufacturer")) != null)
                {
                    if ((xelm2 = xelm.Element("title")) != null)
                        manufacturerName = xelm2.Value;
                    if ((xelm2 = xelm.Element("office_country_code")) != null)
                        manufacturerCountry = xelm2.Value;
                    if ((xelm2 = xelm.Element("factory_country_code")) != null)
                        manufactureCountry = xelm2.Value;
                }

                if ((xelm = root.Element("purposes")) != null)
                {
                    foreach (XElement el in xelm.Elements())
                        purposes.Add(el.Value);
                    lbxPurposes.DataSource = purposes;
                }
                lbxPurposes.BackColor = this.BackColor;

                if ((xelm = root.Element("areas")) != null)
                {
                    foreach (XElement el in xelm.Elements())
                        areas.Add(el.Value);
                    lbxAreas.DataSource = areas;
                }
                lbxAreas.BackColor = this.BackColor;

                redEntryFormBindingSource.DataSource = this;
                grdEqBlocks.DataSource = eqList;

                eqBindingSource = new BindingSource(this.components);
                tbMaxPower.DataBindings.Add(new Binding("Text", eqBindingSource, "MaxPower"));
                tbMaxPowerUnit.DataBindings.Add(new Binding("Text", eqBindingSource, "MaxPowerUnit"));
                tbSensitivity.DataBindings.Add(new Binding("Text", eqBindingSource, "Sensitivity"));
                tbSensitivityUnit.DataBindings.Add(new Binding("Text", eqBindingSource, "SensitivityUnit"));
                tbTechRemark.DataBindings.Add(new Binding("Text", eqBindingSource, "TechRemark"));

                ReadBaseEquipmentList();
                
                if ((xelm = root.Element("equipment-blocks")) != null)
                {
                    foreach (XElement eqBlock in xelm.Elements())
                    {
                        XElement freqbands = eqBlock.Element("freqbands");
                        XElement emitclasses = eqBlock.Element("emitclasses");
                        // normalize freg bands - group them into pairs;
                        Dictionary <int, NatFPlanEntry> planBands = new Dictionary<int,NatFPlanEntry>();
                        if (freqbands != null) 
                            foreach (XElement freqband in freqbands.Elements())
                            {
                                int plan_id = 0;
                                XElement xel = null;
                                if ((xel = freqband.Element("plan_id")) != null)
                                    try { plan_id = Convert.ToInt32(xel.Value); }
                                    catch { continue; }
                                NatFPlanEntry currBand = null;
                                if (planBands.ContainsKey(plan_id))
                                    currBand = planBands[plan_id];
                                else
                                {
                                    currBand = new NatFPlanEntry();
                                    currBand.Id = plan_id;
                                    planBands.Add(plan_id, currBand);
                                }
                                int dir = -1;
                                if ((xel = freqband.Element("direction")) != null)
                                    try { dir = Convert.ToInt32(xel.Value); }
                                    catch { }
                                if ((xel = freqband.Element("min_freq")) != null)
                                {
                                    try
                                    {
                                        double f = Convert.ToDouble(xel.Value, decSepProvider);
                                        switch (dir) { case 0: currBand.FreqTxLow = f; break; case 1: currBand.FreqRxLow = f; break; }
                                    }
                                    catch { }
                                }
                                if ((xel = freqband.Element("max_freq")) != null)
                                {
                                    try
                                    {
                                        double f = Convert.ToDouble(xel.Value, decSepProvider);
                                        switch (dir) { case 0: currBand.FreqTxHigh = f; break; case 1: currBand.FreqRxHigh = f; break; }
                                    }
                                    catch { }
                                }                
                            }

                        if (planBands.Count > 0)
                            foreach (NatFPlanEntry band in planBands.Values)
                                if (emitclasses != null) 
                                    foreach (XElement emitclass in emitclasses.Elements())
                                        AddEquipment(eqBlock, band, emitclass);
                                else
                                    AddEquipment(eqBlock, band, null);
                        else
                        {
                            if (emitclasses != null) 
                                foreach (XElement emitclass in emitclasses.Elements())
                                    AddEquipment(eqBlock, null, emitclass);
                            else
                                AddEquipment(eqBlock, null, null);
                        }                        
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Помилка завантаження даних: " + e.Message, "Реєстр РЕЗ та ВП", MessageBoxButtons.OK, MessageBoxIcon.Error);
                grdEqBlocks.Refresh();
            }
        }

        private void AddEquipment(XElement eqBlock, NatFPlanEntry freqband, XElement emitclass)
        {
            Equipment eq = new Equipment();
            eqList.Add(eq);
            eq.EmiClass = "";
            eq.Rtech = Equipment.unknownSection;
            eq.DbSection = Equipment.unknownSection;

            eq.eqBlElm = eqBlock;
            
            XElement xel = null;
            if (freqband != null)
            {
                eq.FreqTxLow = freqband.FreqTxLow; 
                eq.FreqRxLow = freqband.FreqRxLow;
                eq.FreqTxHigh = freqband.FreqTxHigh; 
                eq.FreqRxHigh = freqband.FreqRxHigh; 
                eq.PlanId = freqband.Id;
            }   

            if (emitclass != null)
            {
                if ((xel = emitclass.Element("title")) != null)
                    eq.EmiClass = xel.Value;
            }
            
            XElement rtech_id = eqBlock.Element("rtech_id");
            if (rtech_id != null)
            {
                IMRecordset rs = new IMRecordset("RADIO_SYSTEMS", IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("NAME,CUST_TXT1");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, Convert.ToInt32(rtech_id.Value));
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        eq.Rtech = rs.GetS("NAME");
                        eq.DbSection = rs.GetS("CUST_TXT1");
                    }
                }
                finally
                {
                    rs.Destroy();
                }
            }

            if ((xel = eqBlock.Element("max_power_out")) != null)
            {
                try
                {
                    eq.MaxPower = xel.Element("val").Value;
                    eq.MaxPowerUnit = xel.Element("measu").Value;
                }
                catch { }
            }

            if ((xel = eqBlock.Element("receiver_sens")) != null)
            {
                try
                {
                    eq.Sensitivity = xel.Element("val").Value;
                    eq.SensitivityUnit = xel.Element("measu").Value;
                }
                catch { }
            }

            if ((xel = eqBlock.Element("notes")) != null)
            {
                eq.TechRemark = xel.Value;
            }

            Equipment[] baseEq = (from equ in eqBaseList
                                  where 
                                      (equ.DbSection == eq.DbSection &&
                                       equ.EmiClass == eq.EmiClass &&
                                       equ.FreqRxHigh == eq.FreqRxHigh &&
                                       equ.FreqRxLow == eq.FreqRxLow &&
                                       equ.FreqTxHigh == eq.FreqTxHigh &&
                                       equ.FreqTxLow == eq.FreqTxLow)
                                  select equ).ToArray();
            if (baseEq != null && baseEq.Count() > 0)
            {
                eq.IsActive = true;
                eq.baseEq = baseEq[0];
                eq.Id = eq.baseEq.Id;
            }

        }

        private void ReadBaseEquipmentList()
        {
            foreach (string eqSuffix in Equipment.types )
            {
                IMRecordset rs = new IMRecordset("EQUIP_" + eqSuffix, IMRecordset.Mode.ReadOnly);
                try
                {
                    rs.Select("ID,NAME,CODE,EQUIP_ID,MANUFACTURER,FAMILY");
                    rs.Select("LOWER_FREQ,UPPER_FREQ,RX_LOWER_FREQ,RX_UPPER_FREQ,FREQ_STABILITY,DESIG_EMISSION,BW,USEF_BW,TUNING_STEP");
                    rs.Select("MODULATION,MIN_POWER,POWER,MAX_POWER,KTBF,KTBF_G,NOISE_F,SENSITIVITY,SENSIT_UNIT,REMARK,TECH_REMARK");
                    rs.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, this.id);
                    for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                    {
                        Equipment eq = new Equipment();
                        eqBaseList.Add(eq);
                        eq.DbSection = eqSuffix;
                        eq.Id = rs.GetI("ID");
                        eq.FreqRxHigh = rs.GetD("RX_UPPER_FREQ");
                        eq.FreqTxHigh = rs.GetD("UPPER_FREQ");
                        eq.FreqRxLow = rs.GetD("RX_LOWER_FREQ");
                        eq.FreqTxLow = rs.GetD("LOWER_FREQ");
                        eq.EmiClass = rs.GetS("DESIG_EMISSION");
                        eq.PlanId = IM.NullI;
                        eq.Rtech = "???";
                        
                    }
                }
                finally
                {
                    rs.Destroy();
                }
            }
        }

        private void ShowWarning(string msg)
        {
            MessageBox.Show(msg, "Попередження", MessageBoxButtons.OK, MessageBoxIcon.Warning); ;
        }

        private void grdEqBlocks_CurrentCellChanged(object sender, EventArgs e)
        {
            int position = this.grdEqBlocks.CurrentCellAddress.Y;
            try { eqBindingSource.DataSource = position > -1 ? eqList[position] : null; }
            catch { } // throws an exception on null
        }

        private void btUse_Click(object sender, EventArgs e)
        {
            int position = this.grdEqBlocks.CurrentCellAddress.Y;
            if (position > -1 && position < eqList.Count)
            {
                if (MessageBox.Show("Додати / оновити запис в БД?", "Підтвердження", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;
                Equipment eq = eqList[position];
                //if (eq.baseEq == null && eq.IsActive == false || eq.DbSection == Equipment.unknownSection || eq.Id == IM.NullI || eq.Id == 0)
                //{
                int id = eq.Id;
                string tablename = "EQUIP_" + eq.DbSection;
                IMRecordset rs = new IMRecordset(tablename, IMRecordset.Mode.ReadWrite);
                try
                {
                    rs.Select("ID,EQUIP_ID,NAME,CODE,EQUIP_ID,MANUFACTURER,FAMILY");
                    rs.Select("LOWER_FREQ,UPPER_FREQ,RX_LOWER_FREQ,RX_UPPER_FREQ,FREQ_STABILITY,DESIG_EMISSION,BW,USEF_BW,TUNING_STEP");
                    rs.Select("MODULATION,MIN_POWER,POWER,MAX_POWER,KTBF,KTBF_G,NOISE_F,SENSITIVITY,SENSIT_UNIT,REMARK,TECH_REMARK");
                    rs.Select("DATE_CREATED,DATE_MODIFIED,CREATED_BY,MODIFIED_BY");
                    rs.SetWhere("ID", IMRecordset.Operation.Eq, id);
                    rs.Open();
                    if (!rs.IsEOF())
                    {
                        rs.Edit();
                        rs.Put("DATE_MODIFIED", DateTime.Now);
                        rs.Put("MODIFIED_BY", IM.ConnectedUser());
                    }
                    else
                    {
                        rs.AddNew();
                        id = IM.AllocID(tablename, 1, -1);
                        rs.Put("ID", id);
                        rs.Put("DATE_CREATED", DateTime.Now);
                        rs.Put("CREATED_BY", IM.ConnectedUser());
                    }
                    rs.Put("CODE", "#"+id.ToString());
                    rs.Put("EQUIP_ID", this.id);
                    rs.Put("MANUFACTURER", this.manufacturerName);
                    rs.Put("NAME", this.model);
                    rs.Put("FAMILY", this.trademark);
                    
                    rs.Put("RX_UPPER_FREQ", eq.FreqRxHigh);
                    rs.Put("UPPER_FREQ", eq.FreqTxHigh);
                    rs.Put("RX_LOWER_FREQ", eq.FreqRxLow);
                    rs.Put("LOWER_FREQ", eq.FreqTxLow);
                    rs.Put("DESIG_EMISSION", eq.EmiClass);
                    try { rs.Put("MAX_POWER", eq.MaxPower == "" ? IM.NullD : (Convert.ToDouble(eq.MaxPower))); }
                    catch { }
                    try { rs.Put("SENSITIVITY", eq.Sensitivity == "" ? IM.NullD : Convert.ToDouble(eq.Sensitivity)); }
                    catch { }
                    //rs.Put("SENSIT_UNIT", "");
                    rs.Put("TECH_REMARK", eq.TechRemark);
                    
                    rs.Update();
                    eq.Id = id;
                    eq.IsActive = true;

                    MessageBox.Show("Успішно", "Інфо", 0, MessageBoxIcon.Information);
                }
                finally
                {
                    rs.Destroy();
                }
            }
        }

        private void grdEqBlocks_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int position = this.grdEqBlocks.CurrentCellAddress.Y;
            if (position > -1 && position < eqList.Count)
            {
                Equipment eq = eqList[position];
                if (eq.IsActive == false || eq.DbSection == Equipment.unknownSection || eq.Id == IM.NullI || eq.Id == 0)
                {
                    string msg = "Схоже, цей рядок не є завантаженим до БД";
                    if (eq.IsActive)
                        msg += ("." + Environment.NewLine + "Спробуйте за наступного відкриття форми.");
                    MessageBox.Show(msg, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    RecordPtr rp = new RecordPtr("EQUIP_" + eq.DbSection, eq.Id);
                    rp.UserEdit();
                }
            }
        }
    }
}
