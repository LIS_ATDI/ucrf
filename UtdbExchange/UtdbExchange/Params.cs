﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;

namespace XICSM.UtdbExchange
{
    public class Params
    {
        public string IpPortService { get { return _ipPortService; } set { SetIpPortService(value); } }
        public string Ip { get { return _ip; } set { _ip = value; ReassembleIpPortService(); } }
        public string Port { get { return _port; } set { _port = value; ReassembleIpPortService(); } }
        public string Service { get { return _service; } set { _service = value; ReassembleIpPortService(); } }
        public string UserName { get { return _userName; } set { _userName = value; } }
        public string Passwd { get { return _passwd; } set { _passwd = value; } }
        public string IcsmServName { get { return _icsmServName; } set { _icsmServName = value; } }
        public string OracleUtdbSchema { get { return _oracleUtdbSchema; } set { _oracleUtdbSchema = value; } }
        public string XmlSchemaPath { get { return _xmlSchemaPath; } set { _xmlSchemaPath = value; } }

        private void SetIpPortService(string value)
        {
            if (_ipPortService != value) 
            { 
                string[] sl = value.Split(';');
                _ipPortService = value;
                _ip = sl.Length > 0 ? sl[0] : "";
                _port = sl.Length > 1 ? sl[1] : "";
                _service = sl.Length > 2 ? sl[2] : "";
            }            
        }
        private void ReassembleIpPortService()
        {
            IpPortService = Ip + ';' + Port + ';' + Service;
        }
        
        private string _ipPortService;
        private string _ip;
        private string _port;
        private string _service;
        private string _userName;
        private string _passwd;
        private string _icsmServName;
        private string _oracleUtdbSchema;
        private string _xmlSchemaPath;

        public Params()
        {
            LoadFromDb();
        }

        public void LoadFromDb()
        {
            IMRecordset rs = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
            try
            {
                rs.Select("ITEM,WHAT");
                for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                {
                    switch (rs.GetS("ITEM"))
                    {
                        case "XNFRA_UTDB_IP":
                            string val = rs.GetS("WHAT");
                            string[] spl = val.Split(';');
                            if (spl.Length > 0)
                                _ip = spl[0];
                            if (spl.Length > 1)
                                _port = spl[1];
                            if (spl.Length > 2)
                                _service = spl[2];
                            break;
                        case "XNFRA_UTDB_USER":
                            _userName = rs.GetS("WHAT");
                            break;
                        case "XNFRA_UTDB_PASSW":
                            _passwd = rs.GetS("WHAT");
                            break;
                        case "XNRFA_SCHEMA_PATH":
                            _xmlSchemaPath = rs.GetS("WHAT");
                            break;
                        case "XNFRA_UTDB_SERVICE_ID":
                            _icsmServName = rs.GetS("WHAT");
                            break;
                        case "XNRFA_SCHEMA":
                            _oracleUtdbSchema = rs.GetS("WHAT");
                            break;
                    }
                }
            }
            finally
            {
                rs.Destroy();
            }
        }

        public void SaveToDb()
        {
            SaveParameter("XNFRA_UTDB_IP", IpPortService);
            SaveParameter("XNFRA_UTDB_USER", UserName);
            SaveParameter("XNFRA_UTDB_PASSW", Passwd);
            SaveParameter("XNFRA_UTDB_SERVICE_ID", IcsmServName);
            SaveParameter("XNRFA_SCHEMA", OracleUtdbSchema);
            SaveParameter("XNRFA_SCHEMA_PATH", XmlSchemaPath);
        }

        private void SaveParameter(string paramName, string paramValue)
        {
            IMRecordset rs = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("WHAT,ITEM");
                rs.SetWhere("ITEM", IMRecordset.Operation.Like, paramName);
                rs.Open();
                if (rs.IsEOF())
                {
                    rs.AddNew();
                    rs.Put("ITEM", paramName);
                }
                else
                    rs.Edit();
                rs.Put("WHAT", paramValue);
                rs.Update();
            }
            finally
            {
                rs.Destroy();
            }
        }
    }
}
