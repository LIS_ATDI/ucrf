﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XICSM.UtdbExchange
{
    class EEssence
    {
        public static string APPL_PAY_ICSM = "APPL_PAY_ICSM";    //Заявка на выставление счета из ICSM
        public static string STATE_APPL_PAY_PARUS = "STATE_APPL_PAY_PARUS";  // Ответ Паруса по заявке
        public static string EMPLOYEE_PARUS = "EMPLOYEE_PARUS";  // Контрагент паруса
        public static string StationIcsm = "STATION_ICSM";  // Станции ICSM
        public static string PriceIcsm = "PRICE_ICSM";    // Прайсы ICSM
        public static string TestEssence = "TEST_ESSENCE";    // Тестовая сущность
        public static string Rres = "RRES"; // Тест RRES
        public static string RadioSystems = "RADIO_SYSTEMS";
        public static string Nf_Plan = "NF_PLAN";
    }
}
