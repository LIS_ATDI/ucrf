﻿namespace XICSM.UtdbExchange
{
    partial class DgSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbPasswd = new System.Windows.Forms.Label();
            this.tbPasswd = new System.Windows.Forms.TextBox();
            this.lbUserName = new System.Windows.Forms.Label();
            this.tbUserName = new System.Windows.Forms.TextBox();
            this.lbXmlSchemasPath = new System.Windows.Forms.Label();
            this.tbXmlSchemasPath = new System.Windows.Forms.TextBox();
            this.lbIpAddress = new System.Windows.Forms.Label();
            this.tbIpAddress = new System.Windows.Forms.TextBox();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOk = new System.Windows.Forms.Button();
            this.lbPort = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lbOracleService = new System.Windows.Forms.Label();
            this.tbOracleService = new System.Windows.Forms.TextBox();
            this.lbUtdbIcsmService = new System.Windows.Forms.Label();
            this.tbUtdbIcsmService = new System.Windows.Forms.TextBox();
            this.lbOracleSchema = new System.Windows.Forms.Label();
            this.tbOracleSchema = new System.Windows.Forms.TextBox();
            this.paramsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.paramsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lbPasswd
            // 
            this.lbPasswd.AutoSize = true;
            this.lbPasswd.Location = new System.Drawing.Point(84, 186);
            this.lbPasswd.Name = "lbPasswd";
            this.lbPasswd.Size = new System.Drawing.Size(53, 13);
            this.lbPasswd.TabIndex = 60;
            this.lbPasswd.Text = "Password";
            // 
            // tbPasswd
            // 
            this.tbPasswd.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "Passwd", true));
            this.tbPasswd.Location = new System.Drawing.Point(143, 183);
            this.tbPasswd.Name = "tbPasswd";
            this.tbPasswd.PasswordChar = '*';
            this.tbPasswd.Size = new System.Drawing.Size(125, 20);
            this.tbPasswd.TabIndex = 61;
            // 
            // lbUserName
            // 
            this.lbUserName.AutoSize = true;
            this.lbUserName.Location = new System.Drawing.Point(108, 153);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Size = new System.Drawing.Size(29, 13);
            this.lbUserName.TabIndex = 50;
            this.lbUserName.Text = "User";
            // 
            // tbUserName
            // 
            this.tbUserName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "UserName", true));
            this.tbUserName.Location = new System.Drawing.Point(143, 150);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(125, 20);
            this.tbUserName.TabIndex = 51;
            // 
            // lbXmlSchemasPath
            // 
            this.lbXmlSchemasPath.AutoSize = true;
            this.lbXmlSchemasPath.Location = new System.Drawing.Point(37, 255);
            this.lbXmlSchemasPath.Name = "lbXmlSchemasPath";
            this.lbXmlSchemasPath.Size = new System.Drawing.Size(100, 13);
            this.lbXmlSchemasPath.TabIndex = 80;
            this.lbXmlSchemasPath.Text = "XML Schemas path";
            // 
            // tbXmlSchemasPath
            // 
            this.tbXmlSchemasPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "XmlSchemaPath", true));
            this.tbXmlSchemasPath.Location = new System.Drawing.Point(143, 252);
            this.tbXmlSchemasPath.Name = "tbXmlSchemasPath";
            this.tbXmlSchemasPath.Size = new System.Drawing.Size(307, 20);
            this.tbXmlSchemasPath.TabIndex = 81;
            // 
            // lbIpAddress
            // 
            this.lbIpAddress.AutoSize = true;
            this.lbIpAddress.Location = new System.Drawing.Point(64, 24);
            this.lbIpAddress.Name = "lbIpAddress";
            this.lbIpAddress.Size = new System.Drawing.Size(73, 13);
            this.lbIpAddress.TabIndex = 10;
            this.lbIpAddress.Text = "UTDB host IP";
            // 
            // tbIpAddress
            // 
            this.tbIpAddress.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "Ip", true));
            this.tbIpAddress.Location = new System.Drawing.Point(143, 21);
            this.tbIpAddress.Name = "tbIpAddress";
            this.tbIpAddress.Size = new System.Drawing.Size(125, 20);
            this.tbIpAddress.TabIndex = 11;
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(247, 290);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 100;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // btOk
            // 
            this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOk.Location = new System.Drawing.Point(156, 290);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 90;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.Ok_Click);
            // 
            // lbPort
            // 
            this.lbPort.AutoSize = true;
            this.lbPort.Location = new System.Drawing.Point(111, 57);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(26, 13);
            this.lbPort.TabIndex = 20;
            this.lbPort.Text = "Port";
            // 
            // tbPort
            // 
            this.tbPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "Port", true));
            this.tbPort.Location = new System.Drawing.Point(143, 54);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(125, 20);
            this.tbPort.TabIndex = 21;
            // 
            // lbOracleService
            // 
            this.lbOracleService.AutoSize = true;
            this.lbOracleService.Location = new System.Drawing.Point(60, 90);
            this.lbOracleService.Name = "lbOracleService";
            this.lbOracleService.Size = new System.Drawing.Size(77, 13);
            this.lbOracleService.TabIndex = 30;
            this.lbOracleService.Text = "Oracle Service";
            // 
            // tbOracleService
            // 
            this.tbOracleService.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "Service", true));
            this.tbOracleService.Location = new System.Drawing.Point(143, 87);
            this.tbOracleService.Name = "tbOracleService";
            this.tbOracleService.Size = new System.Drawing.Size(125, 20);
            this.tbOracleService.TabIndex = 31;
            // 
            // lbUtdbIcsmService
            // 
            this.lbUtdbIcsmService.AutoSize = true;
            this.lbUtdbIcsmService.Location = new System.Drawing.Point(66, 219);
            this.lbUtdbIcsmService.Name = "lbUtdbIcsmService";
            this.lbUtdbIcsmService.Size = new System.Drawing.Size(71, 13);
            this.lbUtdbIcsmService.TabIndex = 70;
            this.lbUtdbIcsmService.Text = "ICSm Service";
            // 
            // tbUtdbIcsmService
            // 
            this.tbUtdbIcsmService.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "IcsmServName", true));
            this.tbUtdbIcsmService.Location = new System.Drawing.Point(143, 216);
            this.tbUtdbIcsmService.Name = "tbUtdbIcsmService";
            this.tbUtdbIcsmService.Size = new System.Drawing.Size(125, 20);
            this.tbUtdbIcsmService.TabIndex = 71;
            // 
            // lbOracleSchema
            // 
            this.lbOracleSchema.AutoSize = true;
            this.lbOracleSchema.Location = new System.Drawing.Point(57, 121);
            this.lbOracleSchema.Name = "lbOracleSchema";
            this.lbOracleSchema.Size = new System.Drawing.Size(80, 13);
            this.lbOracleSchema.TabIndex = 40;
            this.lbOracleSchema.Text = "Oracle Schema";
            // 
            // tbOracleSchema
            // 
            this.tbOracleSchema.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.paramsBindingSource, "OracleUtdbSchema", true));
            this.tbOracleSchema.Location = new System.Drawing.Point(143, 118);
            this.tbOracleSchema.Name = "tbOracleSchema";
            this.tbOracleSchema.Size = new System.Drawing.Size(125, 20);
            this.tbOracleSchema.TabIndex = 41;
            // 
            // paramsBindingSource
            // 
            this.paramsBindingSource.DataSource = typeof(XICSM.UtdbExchange.Params);
            // 
            // DgSettings
            // 
            this.AcceptButton = this.btOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(470, 332);
            this.Controls.Add(this.lbOracleSchema);
            this.Controls.Add(this.tbOracleSchema);
            this.Controls.Add(this.lbUtdbIcsmService);
            this.Controls.Add(this.tbUtdbIcsmService);
            this.Controls.Add(this.lbOracleService);
            this.Controls.Add(this.tbOracleService);
            this.Controls.Add(this.lbPort);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.lbPasswd);
            this.Controls.Add(this.tbPasswd);
            this.Controls.Add(this.lbUserName);
            this.Controls.Add(this.tbUserName);
            this.Controls.Add(this.lbXmlSchemasPath);
            this.Controls.Add(this.tbXmlSchemasPath);
            this.Controls.Add(this.lbIpAddress);
            this.Controls.Add(this.tbIpAddress);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DgSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UTDB Exchange Settings";
            ((System.ComponentModel.ISupportInitialize)(this.paramsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbPasswd;
        private System.Windows.Forms.TextBox tbPasswd;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.Label lbXmlSchemasPath;
        private System.Windows.Forms.TextBox tbXmlSchemasPath;
        private System.Windows.Forms.Label lbIpAddress;
        private System.Windows.Forms.TextBox tbIpAddress;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbOracleService;
        private System.Windows.Forms.TextBox tbOracleService;
        private System.Windows.Forms.Label lbUtdbIcsmService;
        private System.Windows.Forms.TextBox tbUtdbIcsmService;
        private System.Windows.Forms.Label lbOracleSchema;
        private System.Windows.Forms.TextBox tbOracleSchema;
        private System.Windows.Forms.BindingSource paramsBindingSource;
    }
}