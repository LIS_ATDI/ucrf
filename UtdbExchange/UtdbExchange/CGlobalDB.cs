﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using ICSM;

namespace XICSM.UtdbExchange
{
    class CGlobalDB
    {
        public Params parameters { get; set; }
        private OracleConnection _localConn;
        /// <summary>
        /// Constructor
        /// </summary>
         public CGlobalDB()
        {
            _localConn = null;
        }

        #region Implementation of IGlobalDb

        /// <summary>
        /// Opening Connection to datatbase
        /// </summary>
        /// <returns>if connected success return true</returns>
        public bool OpenConnection()
        {
            if (parameters == null)
                parameters = new Params();

            if (_localConn == null)
            {
                string oradb = GetConnString();
                _localConn = new OracleConnection(oradb);
                _localConn.Open();
            }
            return true;
        }

        /// <summary>
        /// Closes conection of DB
        /// </summary>
        /// <returns>true - OK, false - Error</returns>
        public bool CloseConnection()
        {
            if (_localConn != null)
            {
                _localConn.Close();
                _localConn.Dispose();
                _localConn = null;
            }
            return true;
        }

        /// <summary>
        /// Возвращает количество записей, которые необходимо обработать
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>Кол-во не обработаных записей или -1 если ошибка</returns>
        //public int GetCountRecords(string service)
        //{
        //    int retVal = -1;
        //    if (_localConn != null)
        //    {
        //        string sqlQuery = string.Format("select UTDB.CountUnreadXML('{0}') as COUNT from dual", service);
        //        OracleDataReader dr = ExecuteQuery(sqlQuery);
        //        if ((dr != null) && (dr.Read()))
        //        {
        //            int index = dr.GetOrdinal("COUNT");
        //            object str = dr.GetValue(index);
        //            retVal = str.ToString().ToInt32(-1);
        //            dr.Close();
        //        }
        //    }
        //    return retVal;
        //}

        /// <summary>
        /// Передаем XML в промежуточную БД (создаем все необходимые ссылки)
        /// </summary>
        /// <param name="clobString">XML строка</param>
        /// <param name="essence">имя сущности</param>
        /// <param name="service">имя сервиса</param>
        /// <returns>TRUE - файл сохранен, иначе FALSE</returns>
        public bool WriteXml(string clobString, string essence, string service)
        {
            bool retVal = false;
            if (_localConn != null)
            {
                OracleCommand cmd = new OracleCommand("" + parameters.OracleUtdbSchema + ".WriteXML", _localConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                OracleParameter param1 = new OracleParameter("XML_CLOB", OracleDbType.Clob);
                param1.Direction = System.Data.ParameterDirection.Input;
                param1.Value = clobString;
                OracleParameter param2 = new OracleParameter("strESSENCE", OracleDbType.Varchar2);
                param2.Direction = System.Data.ParameterDirection.Input;
                param2.Value = essence;
                OracleParameter param3 = new OracleParameter("strOwnServ", OracleDbType.Varchar2);
                param3.Direction = System.Data.ParameterDirection.Input;
                param3.Value = service;
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param2);
                cmd.Parameters.Add(param3);
                cmd.ExecuteNonQuery();
                retVal = true;
            }
            return retVal;
        }
        public bool WriteXml(string clobString, string essence, string service, string schema)
        {
            bool retVal = false;
            if (_localConn != null)
            {
                OracleCommand cmd = new OracleCommand(schema+".WriteXML", _localConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                OracleParameter param1 = new OracleParameter("XML_CLOB", OracleDbType.Clob);
                param1.Direction = System.Data.ParameterDirection.Input;
                param1.Value = clobString;
                OracleParameter param2 = new OracleParameter("strESSENCE", OracleDbType.Varchar2);
                param2.Direction = System.Data.ParameterDirection.Input;
                param2.Value = essence;
                OracleParameter param3 = new OracleParameter("strOwnServ", OracleDbType.Varchar2);
                param3.Direction = System.Data.ParameterDirection.Input;
                param3.Value = service;
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param2);
                cmd.Parameters.Add(param3);
                cmd.ExecuteNonQuery();
                retVal = true;
            }
            return retVal;
        }

        /// <summary>
        /// Ситает XML данные
        /// </summary>
        /// <param name="service">Имя сервиса</param>
        /// <returns>XML строка</returns>
        public string ReadXml(string service,string schema)
        {
            string xml = "";
            if (_localConn != null)
            {
                string sqlQuery = string.Format("select {1}.ReadXML('{0}') as XMLSTR from dual", service, schema);
                OracleDataReader dr = ExecuteQuery(sqlQuery);
                if ((dr != null) && (dr.Read()))
                {
                    object str = dr.GetValue(dr.GetOrdinal("XMLSTR"));
                    xml = str.ToString();
                    dr.Close();
                }
            }
            return xml;
        }
        public string ReadXml(string service)
        {
            string xml = "";
            if (_localConn != null)
            {
                string sqlQuery = string.Format("select " + parameters.OracleUtdbSchema + ".ReadXML('{0}') as XMLSTR from dual", service);
                OracleDataReader dr = ExecuteQuery(sqlQuery);
                if ((dr != null) && (dr.Read()))
                {
                    object str = dr.GetValue(dr.GetOrdinal("XMLSTR"));
                    xml = str.ToString();
                    dr.Close();
                }
            }
            return xml;
        }

        /// <summary>
        /// Изменить статус обработки записи
        /// </summary>
        /// <returns>XML строка</returns>
        public bool ChangeStatus(int state, int errorCode, string service)
        {
            bool retVal = false;
            if (_localConn != null)
            {
                string sqlQuery = string.Format("begin " + parameters.OracleUtdbSchema + ".ChangeStateXML('{0}',{1},{2}); end;",
                                                service,
                                                state,
                                                errorCode);
                retVal = ExecuteNonQuery(sqlQuery);
            }
            return retVal;
        }

        /// <summary>
        /// Generate new GUID
        /// </summary>
        /// <returns>new GUID</returns>
        //public int NewGuid(string tableName)
        //{
        //    int retVal = -1;
        //    string sqlQuery = "SELECT UTDB.SQV_" + tableName + ".NEXTVAL as ID from dual";
        //    OracleDataReader dr = ExecuteQuery(sqlQuery);
        //    if ((dr != null) && (dr.Read()))
        //    {
        //        object str = dr.GetValue(dr.GetOrdinal("ID"));
        //        retVal = str.ToString().ToInt32(-1);
        //        dr.Close();
        //    }
        //    return retVal;
        //}

        /// <summary>
        /// Return all essences
        /// </summary>
        /// <returns>list of all assences</returns>
        public string[] GetEssences()
        {
            List<string> entitiesList = new List<string>();
            string sqlQuery = "SELECT NAME from "+parameters.OracleUtdbSchema+".ESSENCES";
            OracleDataReader dr = ExecuteQuery(sqlQuery);
            if (dr != null)
            {
                while (dr.Read())
                {
                    string str = dr.GetValue(dr.GetOrdinal("NAME")).ToString();
                    entitiesList.Add(str);
                }
                dr.Close();
            }
            return entitiesList.ToArray();
        }

        #endregion

        //===================================================
        /// <summary>
        /// Возвращает строку подключения
        /// </summary>
        /// <returns>строка подключения</returns>
        private string GetConnString()
        {
            if (parameters == null)
            {
                parameters = new Params();
            }
            
            string oradb = "Data Source=(DESCRIPTION="
                         + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))"
                         + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2})));"
                         + "User Id={3};Password={4};";
            string retStr = string.Format(oradb, parameters.Ip, parameters.Port, parameters.Service, parameters.UserName, parameters.Passwd);
            return retStr;
        }
        //===================================================
        /// <summary>
        /// Выполнить запрос без ответа
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns>TRUE - все ОК</returns>
        private bool ExecuteNonQuery(string query)
        {
            bool retVal = false;
            if (_localConn != null)
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = _localConn;
                cmd.CommandText = query;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.ExecuteNonQuery();
                retVal = true;
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Выполнить запрос с ответом
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns>OracleDataReader - ответ</returns>
        private OracleDataReader ExecuteQuery(string query)
        {
            OracleDataReader retVal = null;
            if (_localConn != null)
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = _localConn;
                cmd.CommandText = query;
                cmd.CommandType = System.Data.CommandType.Text;
                retVal = cmd.ExecuteReader();
            }
            return retVal;
        }

        internal int GetUnreadRecordsCount(string client)
        {
            int retVal = -1;
            if (_localConn != null)
            {
                string sqlQuery = string.Format("select " + parameters.OracleUtdbSchema + ".CountUnreadXML('{0}') as COUNT from dual", client);
                OracleDataReader dr = ExecuteQuery(sqlQuery);
                if ((dr != null) && (dr.Read()))
                {                    
                    string str = dr.GetValue(0).ToString();
                    retVal = (string.IsNullOrEmpty(str)) ? -1 : Convert.ToInt32(str);
                    dr.Close();
                }
            }
            return retVal;    
        }
    }
}
