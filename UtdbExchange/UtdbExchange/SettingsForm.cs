﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UtdbExchange
{
    public partial class DgSettings : Form
    {
        Params p;
        public DgSettings(Params _p)
        {
            InitializeComponent();
            paramsBindingSource.Add(_p);
            p = _p;

            //IMRecordset rs = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadWrite);
            //rs.Select("XNFRA_UTDB_IP,XNFRA_UTDB_USER,XNFRA_UTDB_PASSW,XNRFA_SCHEMA_PATH");
            //this.XNFRA_UTDB_IP.Text = rs.GetS("XNFRA_UTDB_IP");
            //this.XNFRA_UTDB_PASSW.Text = rs.GetS("XNFRA_UTDB_PASSW");
            //this.XNFRA_UTDB_USER.Text = rs.GetS("XNFRA_UTDB_USER");
            //this.XNRFA_SCHEMA_PATH.Text = rs.GetS("XNRFA_SCHEMA_PATH");
        }

        private void CanWrite(ref TextBox textBox, ref bool canWrite)
        {
            if (textBox.Text.Length < 1)
            {
                textBox.BackColor = Color.Red;
                canWrite = false;
                DialogResult = System.Windows.Forms.DialogResult.None;
            }
            else
            {
                textBox.BackColor = Color.White;
            }
        }
        private void Ok_Click(object sender, EventArgs e)
        {
            bool canWrite = true;
            CanWrite(ref tbIpAddress, ref canWrite);
            CanWrite(ref tbPort, ref canWrite);
            CanWrite(ref tbOracleService, ref canWrite);
            CanWrite(ref tbUserName, ref canWrite);
            CanWrite(ref tbPasswd, ref canWrite);
            CanWrite(ref tbXmlSchemasPath, ref canWrite);
            CanWrite(ref tbUtdbIcsmService, ref canWrite);
            CanWrite(ref tbOracleSchema, ref canWrite); 
        }
    }
}
