﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using ICSM;

namespace XICSM.UtdbExchange
{
    enum ExchangeDir
    {
        edUnknown = 0
        , edIn
        , edOut
    }

    class ExchangeModule
    {
        private bool isInteractive;

        private XmlDocument docRadioSystems;
        private XmlDocument docPlanSyst;

        protected ExchangeModule() { isInteractive = false; }
        public ExchangeModule(bool _isInteractive)
        {
            isInteractive = _isInteractive;
        }

        void LoadCachedData()
        {
            docRadioSystems = null;
            docPlanSyst = null;
            List<int> RadioSystemsID = new List<int>();
            List<string> RadioSystemsAction = new List<string>();
            Dictionary<int, string> PlanSyst = new Dictionary<int, string>();
            List<int> PlanBandID = new List<int>();
            List<string> PlanBandAction = new List<string>();
            List<int> delBand = new List<int>();
            IMRecordset r = new IMRecordset("XUTDB_X_CACHE", IMRecordset.Mode.ReadWrite);
            try
            {
                r.Select("OBJ_TABLE,OBJ_ID,DATE_CACHED,ACTION,INFO,STATUS");
                r.SetWhere("STATUS", IMRecordset.Operation.Eq, 0);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    string Table = r.GetS("OBJ_TABLE");
                    int id = r.GetI("OBJ_ID");
                    string action = r.GetS("ACTION");

                    if (Table == "RADIO_SYSTEMS") //составляем перечень последних изменений в каждой таблице по айди
                    {
                        if (RadioSystemsID.Contains(id) == false)// при множественных изменениях записи айди должен сохраняться только раз.
                        {
                            RadioSystemsID.Add(id);
                            RadioSystemsAction.Add(action);
                        }
                        else if (action == "D")
                        {
                            int index = RadioSystemsID.IndexOf(id);
                            RadioSystemsAction[index] = action;// при множественном изменении записи сохраняется только первое действие(если удаление и редакирование то так и нужно). Если последним действием было удаление, то нужно его поставить.
                        }
                    }
                    else if (Table == "NATFPLAN_SYST")
                    {
                        if (PlanSyst.ContainsKey(id) == false)
                        {
                            PlanSyst.Add(id, action);
                        }
                        else if (action == "D")
                        {
                            PlanSyst[id] = action;
                        }
                    }
                    else if (Table == "NATFPLAN_BAND")
                    {
                        if ((PlanBandID.Contains(id) == false) && (action != "D"))
                        {
                            PlanBandID.Add(id);
                            PlanBandAction.Add(action);
                        }
                        else if (action == "D")
                        {
                            if (!delBand.Contains(id))
                                delBand.Add(id);
                        }
                    }
                    else
                    {
                        WriteLog(string.Format("Unknown table in Cache record: \"ObjTable: {0} ObjId={1} Action={2}\"", Table, id, action)
                            , Table, id, ExchangeDir.edIn, action, "Warning");
                    }
                    r.Edit();
                    r.Put("STATUS", 1);
                    r.Update();
                }
            }
            finally
            {
                r.Destroy();
            }
            //Эти хитрые манипуляции призваны избежать повторений в случае одновременного изменения таблиц NATFPLAN_SYST и NATFPLAN_BAND
            if (PlanBandID.Count() > 0)
            {
                for (int i = 0; i < PlanBandID.Count(); i++)
                {
                    IMRecordset rs = new IMRecordset("NATFPLAN_BAND", IMRecordset.Mode.ReadOnly);
                    try
                    {
                        rs.Select("NFPS_ID,ROLE");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, PlanBandID[i]);
                        rs.Open();

                        if (!rs.IsEOF() && !PlanSyst.ContainsKey(rs.GetI("NFPS_ID")))
                        {
                            PlanSyst.Add(rs.GetI("NFPS_ID"), "");
                        }
                    }
                    finally
                    {
                        rs.Destroy();
                    }
                }
            }

            if (RadioSystemsID.Count() > 0)
            {
                docRadioSystems = new XmlDocument();
                docRadioSystems.AppendChild(docRadioSystems.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
                docRadioSystems.AppendChild(docRadioSystems.CreateNode(XmlNodeType.Element, "ROOT", ""));
                for (int i = 0; i < RadioSystemsID.Count(); i++)
                {
                    IMRecordset rs = new IMRecordset("RADIO_SYSTEMS", IMRecordset.Mode.ReadWrite);
                    try
                    {
                        rs.Select("ID,NAME,DESCRIPTION,EFIS_NAME,SONS,SERVICE,PARAMETERS,ICST_TYPE,ICST_SIGNAL,RS_MEMO,CUST_TXT1,CUST_TXT2,CUST_TXT3,SERVICE_ID,SERVICE_NAME,SERVICE_DESC,SERVICE_PARENT,SERVICE_LEVEL,SERVICE_CODE,SERVICE_NATURAL,SERVICE_APPARATUS,SERVICE_FORM,SERVICE_OBJECT,SERVICE_STATUS,SERVICE_SEQ,IMPOP_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, RadioSystemsID[i]);
                        rs.Open();
                        XmlNode node = docRadioSystems.CreateElement("Row");
                        docRadioSystems.DocumentElement.AppendChild(node);
                        XmlNode child;
                        child = docRadioSystems.CreateElement("NAME");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("NAME")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("DESCRIPTION");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("DESCRIPTION")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("EFIS_NAME");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("EFIS_NAME")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SONS");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SONS")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("PARAMETERS");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("PARAMETERS")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("ICST_TYPE");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("ICST_TYPE").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("ICST_SIGNAL");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("ICST_SIGNAL").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("RS_MEMO");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("RS_MEMO")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("CUST_TXT1");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT1")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("RS_MEMO");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("RS_MEMO")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("CUST_TXT2");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT2")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("CUST_TXT3");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CUST_TXT3")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_ID");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_ID")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_NAME");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_NAME")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_DESC");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_DESC")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_PARENT");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_PARENT")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_LEVEL");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("SERVICE_LEVEL").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_CODE");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_CODE")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_NATURAL");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_NATURAL")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_APPARATUS");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_APPARATUS")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_FORM");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_FORM")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_OBJECT");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_OBJECT")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_STATUS");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("SERVICE_STATUS")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("SERVICE_SEQ");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("SERVICE_SEQ").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("IMPOP_ID");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetI("IMPOP_ID").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("CREATED_BY");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("CREATED_BY")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("DATE_MODIFIED");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.Get("DATE_MODIFIED").ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("MODIFIED_BY");
                        child.AppendChild(docRadioSystems.CreateTextNode(rs.GetS("MODIFIED_BY")));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("OBJ_ID");
                        child.AppendChild(docRadioSystems.CreateTextNode(RadioSystemsID[i].ToString()));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("ACTION");
                        child.AppendChild(docRadioSystems.CreateTextNode(RadioSystemsAction[i]));
                        node.AppendChild(child);
                        child = docRadioSystems.CreateElement("GUID");
                        child.AppendChild(docRadioSystems.CreateTextNode(getGuid("RADIO_SYSTEMS", RadioSystemsID[i], "ICSM")));
                        node.AppendChild(child);
                        docRadioSystems.DocumentElement.AppendChild(node);
                    }
                    finally
                    {
                        rs.Destroy();
                    }
                }
            }

            if (PlanSyst.Count() > 0)
            {
                docPlanSyst = new XmlDocument();
                docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
                docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.Element, "ROOT", ""));
                //for (int i = 0; i < PlanSyst.Count(); i++)
                foreach (var item in PlanSyst)
                {
                    IMRecordset rs = new IMRecordset("NATFPLAN_SYST", IMRecordset.Mode.ReadWrite);
                    try
                    {
                        rs.Select("ID,RADIOSYS,NAME,ABBREV,RS_MEMO,ACCEPT_SERV,CLASS_OF_STATION,GEO_USE,FREQ_IDENT,A,B,DUPLEX_SPACING,BIUSE_DATE,BIUSE_REF,FREEZE_DATE,FREEZE_REF,EOUSE_DATE,IMPOP_ID,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,STATUS");
                        rs.Select("BandA.ID,BandB.ID");
                        rs.SetWhere("ID", IMRecordset.Operation.Eq, item.Key);
                        rs.Open();
                        XmlNode node = docPlanSyst.CreateElement("Row");
                        docPlanSyst.DocumentElement.AppendChild(node);
                        XmlNode child;
                        child = docPlanSyst.CreateElement("ID");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetI("ID").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("RADIOSYS");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("RADIOSYS")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("NAME");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("NAME")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("ABBREV");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("ABBREV")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("RS_MEMO");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("RS_MEMO")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("ACCEPT_SERV");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("ACCEPT_SERV")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("CLASS_OF_STATION");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("CLASS_OF_STATION")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("GEO_USE");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("GEO_USE")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("FREQ_IDENT");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("FREQ_IDENT")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("A");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("A")));
                        node.AppendChild(child);

                        int bandAid = rs.GetI("BandA.ID");
                        if (bandAid != IM.NullI)
                        {
                            int indexA = PlanBandID.IndexOf(bandAid);
                            if (indexA >= 0)
                                node.AppendChild(getBand(rs.GetI("ID"), "A", docPlanSyst, PlanBandAction[indexA]));
                            else
                                BandNode(0, "", docPlanSyst, "");
                        }
                        child = docPlanSyst.CreateElement("B");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("B")));
                        node.AppendChild(child);

                        int bandBid = rs.GetI("BandB.ID");
                        if (bandBid != IM.NullI)
                        {
                            int indexB = PlanBandID.IndexOf(rs.GetI("BandA.ID"));
                            if (indexB >= 0)
                                node.AppendChild(getBand(rs.GetI("ID"), "B", docPlanSyst, PlanBandAction[indexB]));
                            else
                                BandNode(0, "", docPlanSyst, "");
                        }
                        child = docPlanSyst.CreateElement("DUPLEX_SPACING");
                        int iVal = rs.GetI("DUPLEX_SPACING");
                        child.AppendChild(docPlanSyst.CreateTextNode(iVal == IM.NullI ? "" : iVal.ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("STATUS");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("STATUS")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("BIUSE_DATE");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("BIUSE_DATE").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("BIUSE_REF");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("BIUSE_REF")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("FREEZE_DATE");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("FREEZE_DATE").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("FREEZE_REF");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("FREEZE_REF")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("EOUSE_DATE");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("EOUSE_DATE").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("IMPOP_ID");
                        iVal = rs.GetI("IMPOP_ID");
                        child.AppendChild(docPlanSyst.CreateTextNode(iVal == IM.NullI ? "" : iVal.ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("DATE_CREATED");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("DATE_CREATED").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("CREATED_BY");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("CREATED_BY")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("DATE_MODIFIED");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.Get("DATE_MODIFIED").ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("MODIFIED_BY");
                        child.AppendChild(docPlanSyst.CreateTextNode(rs.GetS("MODIFIED_BY")));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("OBJ_ID");
                        child.AppendChild(docPlanSyst.CreateTextNode(item.Key.ToString()));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("ACTION");
                        child.AppendChild(docPlanSyst.CreateTextNode(item.Value));
                        node.AppendChild(child);
                        child = docPlanSyst.CreateElement("GUID");
                        child.AppendChild(docPlanSyst.CreateTextNode(getGuid("NATFPLAN_SYST", item.Key, "ICSM")));
                        node.AppendChild(child);
                        node.AppendChild(getSTD(rs.GetI("ID"), docPlanSyst));
                        docPlanSyst.DocumentElement.AppendChild(node);
                    }
                    finally
                    {
                        rs.Destroy();
                    }
                }
            }

            if (delBand.Count > 0)
            {
                if (docPlanSyst == null)
                {
                    docPlanSyst = new XmlDocument();
                    docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
                    docPlanSyst.AppendChild(docPlanSyst.CreateNode(XmlNodeType.Element, "ROOT", ""));
                }
                foreach (int id in delBand)
                {
                    docPlanSyst.DocumentElement.AppendChild(getDelBand(id, docPlanSyst));
                }
            }

        }

        public void ExportToUtdb()
        {
            try
            {
                LoadCachedData();
            }
            catch (Exception ex)
            {
                WriteLog(String.Format("Data load: {0}", ex.Message), "", 0, ExchangeDir.edOut, "", "Error");
                return;
            }
            WriteLog(string.Format("ExportToUtdb(): {0} radiosystems, {1} plan records to process", docRadioSystems == null ? 0 : 1, docPlanSyst == null ? 0 : 1),
                "", 0, ExchangeDir.edOut, "", "Info");
            CGlobalDB gdb = new CGlobalDB();
            bool ifConection = false;
            try
            {
                ifConection = gdb.OpenConnection();
            }
            catch (Exception ex)
            {
                WriteLog(String.Format("Open connection to Utdb: {0}", ex.Message), "", 0, ExchangeDir.edOut, "", "Error");
                return;
            }
            if (ifConection)
            {
                try
                {
                    if (docRadioSystems != null && docRadioSystems.InnerXml.Length > 0)
                        gdb.WriteXml(docRadioSystems.InnerXml, EEssence.RadioSystems, gdb.parameters.IcsmServName, gdb.parameters.OracleUtdbSchema);

                    if (docPlanSyst != null && docPlanSyst.InnerXml.Length > 0)
                        gdb.WriteXml(docPlanSyst.InnerXml, EEssence.Nf_Plan, gdb.parameters.IcsmServName, gdb.parameters.OracleUtdbSchema);
                }
                catch (Exception ex)
                {
                    WriteLog(String.Format("Write to Utdb: {0}", ex.Message), "", 0, ExchangeDir.edOut, "", "Error");
                }
                finally
                {
                    gdb.CloseConnection();
                }
            }
            IM.RefreshQueries("XUTDB_X_LOG,XUTDB_X_SYN,XUTDB_X_CACHE");
        }

        XmlNode getDelBand(int ID, XmlDocument docPlanSyst)
        {
            XmlNode node = docPlanSyst.CreateElement("Row");
            docPlanSyst.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanSyst.CreateElement("ID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("RADIOSYS");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("NAME");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ABBREV");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("RS_MEMO");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ACCEPT_SERV");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("CLASS_OF_STATION");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("GEO_USE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREQ_IDENT");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("A");
            node.AppendChild(child);
            node.AppendChild(BandNode(ID, "A", docPlanSyst, "D"));
            child = docPlanSyst.CreateElement("B");
            node.AppendChild(child);
            node.AppendChild(BandNode(ID, "B", docPlanSyst, "D"));
            child = docPlanSyst.CreateElement("DUPLEX_SPACING");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("STATUS");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("BIUSE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("BIUSE_REF");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREEZE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("FREEZE_REF");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("EOUSE_DATE");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("IMPOP_ID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("DATE_CREATED");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("CREATED_BY");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("DATE_MODIFIED");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MODIFIED_BY");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("OBJ_ID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ACTION");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("GUID");
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("NATFPLAN_STD");
            node.AppendChild(child);

            return node;
        }

        XmlNode BandNode(int ID, string role, XmlDocument docPlanSyst, string action)
        {
            XmlNode node = docPlanSyst.CreateElement("BAND_" + role);
            docPlanSyst.DocumentElement.AppendChild(node);
            XmlNode child;
            child = docPlanSyst.CreateElement("ID");
            child.AppendChild(docPlanSyst.CreateTextNode(ID.ToString()));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("ROLE");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MIN_FREQ");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            node.AppendChild(child);
            child = docPlanSyst.CreateElement("MAX_FREQ");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            child = docPlanSyst.CreateElement("PAIRING");
            child.AppendChild(docPlanSyst.CreateTextNode(""));
            child = docPlanSyst.CreateElement("ACTION");
            child.AppendChild(docPlanSyst.CreateTextNode(action));
            node.AppendChild(child);
            return node;
        }

        XmlNode getBand(int Nfps_ID, string role, XmlDocument docPlanBand, string action)
        {
            XmlNode node;
            IMRecordset rs = new IMRecordset("NATFPLAN_BAND", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("ID,NFPS_ID,ROLE,MIN_FREQ,MAX_FREQ,PAIRING");
                rs.SetWhere("NFPS_ID", IMRecordset.Operation.Eq, Nfps_ID);
                rs.SetWhere("ROLE", IMRecordset.Operation.Eq, role);
                rs.Open();
                node = docPlanBand.CreateElement("BAND_" + role);
                docPlanBand.DocumentElement.AppendChild(node);
                XmlNode child;
                child = docPlanBand.CreateElement("ID");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("ID").ToString()));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("ROLE");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("ROLE")));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("MIN_FREQ");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("MIN_FREQ").ToString()));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("MAX_FREQ");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("MAX_FREQ").ToString()));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("PAIRING");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("PAIRING")));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("ACTION");
                child.AppendChild(docPlanSyst.CreateTextNode(action));
                node.AppendChild(child);
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return node;
        }


        XmlNode getSTD(int ID, XmlDocument docPlanBand)
        {
            XmlNode node = null;
            IMRecordset rs = new IMRecordset("NATFPLAN_STD", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("ID,LINKF1,ID_NUMBER");
                rs.SetWhere("LINKF1", IMRecordset.Operation.Eq, ID);
                rs.Open();
                node = docPlanBand.CreateElement("NATFPLAN_STD");
                docPlanBand.DocumentElement.AppendChild(node);
                XmlNode child = docPlanBand.CreateElement("ID");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetI("ID").ToString()));
                node.AppendChild(child);
                child = docPlanBand.CreateElement("ID_NUMBER");
                child.AppendChild(docPlanBand.CreateTextNode(rs.GetS("ID_NUMBER")));
                node.AppendChild(child);
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return node;
        }

        public void ImportFromUtdb()
        {
            try
            {
                CGlobalDB gdb = new CGlobalDB();
                if (gdb.OpenConnection())
                {
                    int total = 0;
                    //MessageBox.Show(string.Format("{0} unread records in DB", count), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    for (int count = gdb.GetUnreadRecordsCount(gdb.parameters.IcsmServName); count > 0; count--)
                    {
                        string xml = gdb.ReadXml(gdb.parameters.IcsmServName, gdb.parameters.OracleUtdbSchema);
                        MessageBox.Show("Record " + (total+1).ToString() + ", length " + xml.Length.ToString() +
                            ":\n\n" + xml, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        try
                        {
                            total += ProcessInputXml(xml);
                        }
                        catch (Exception e)
                        {
                            WriteLog("Processing data packet: " + e.Message, "", 0, ExchangeDir.edIn, "", "Error");
                        }

                        DialogResult dr = MessageBox.Show("Mark the packet as processed?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
                        if (dr == DialogResult.Yes)
                            gdb.ChangeStatus(1/*processed*/, 0, gdb.parameters.IcsmServName);
                        else
                            break;
                    }
                    gdb.CloseConnection();
                    if (isInteractive)
                        MessageBox.Show(string.Format("{0} Records processed", total), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    WriteLog("Connection to UTDB failed", "", 0, ExchangeDir.edIn, "", "Error");
                }
            }
            catch (Exception ex)
            {
                WriteLog("Import from UTDB: " + ex.Message, "", 0, ExchangeDir.edIn, "", "Error");
            }
            IM.RefreshQueries("ADM_EQUIP,MANUFACTURER,XREDREG_CONTENT");
            IM.RefreshQueries("XUTDB_X_LOG,XUTDB_X_SYN");
        }

        private int ProcessInputXml(string xml)
        {
            const string funcName = "ProcessInputXml()";

            XDocument xdoc = XDocument.Parse(xml);
            const string rootObjElm = "reestrunit";
            XElement root = xdoc.Root;

            int objectsProcessed = 0;
            foreach (XElement record in root.Elements())
            {
                if (record == null)
                    continue;

                string objTable;
                int objId = 0;
                string action = "";

                switch (record.Name.ToString().ToLower())
                {
                    case rootObjElm:
                        objTable = "ADM_EQUIP";
                        break;
                    default:
                        continue;
                }

                string idTag = "id";
                string objExtId = "";
                XElement idEl = record.Element(idTag);
                if (idEl != null)
                    objExtId = idEl.Value.ToString();

                if (string.IsNullOrEmpty(objExtId))
                {
                    WriteLog(funcName + ": empty or missing <" + idTag + "> tag", objTable, objId, ExchangeDir.edIn, action, "Error");
                    //throw new Exception(funcName + ": empty or missing <" + idTag + "> tag");
                    continue;
                }

                int id = GetIdFromExtId(objTable, objExtId, "RRES"); //TODO : determine external service 
                using (IMObject obj = (id > 0) ? IMObject.LoadFromDB(objTable, id) : IMObject.New(objTable))
                {
                    objId = obj.GetI("ID");

                    obj["CATEGORY"] = "Реєстр";
                    try
                    {
                        obj["CUST_NBR1"] = Convert.ToInt32(record.Element("code").Value);
                    }
                    catch { }


                    PutString(obj, "DESCRIPTION", record.Element("title"), 100);

                    string tm = record.Element("trademark") == null ? "" : record.Element("trademark").Value.ToString();
                    string model = record.Element("model") == null ? "" : record.Element("model").Value.ToString();
                    string name = tm + (string.IsNullOrEmpty(tm) ? "" : " ") + model;
                    if (name.Length > 50) name = name.Remove(50);

                    obj["NAME"] = name;

                    XElement subElm = null;
                    subElm = record.Element("section");
                    if (subElm != null)
                    {
                        PutString(obj, "CUST_TXT10", subElm.Element("number"), 50);
                        PutString(obj, "CUST_TXT11", subElm.Element("title"), 50);
                    }

                    subElm = record.Element("classifier");
                    if (subElm != null)
                    {
                        string code = subElm.Element("code") == null ? "" : subElm.Element("code").Value.ToString();
                        string title = subElm.Element("title") == null ? "" : subElm.Element("title").Value.ToString();
                        string section = code + (string.IsNullOrEmpty(code) ? "" : " ") + title;
                        if (section.Length > 1000) section = section.Remove(1000);

                        obj["CUST_TXT7"] = section;
                    }

                    subElm = record.Element("solution");
                    if (subElm != null)
                    {
                        PutString(obj, "CUST_TXT1", subElm.Element("code"), 50);
                        PutDate(obj, "CUST_DAT1", subElm.Element("date"), objTable, objId, (id > 0) ? "M" : "A");
                    }

                    subElm = record.Element("certificate");
                    if (subElm != null)
                    {
                        PutString(obj, "CUST_TXT2", subElm.Element("code"), 50);
                        PutDate(obj, "CUST_DAT2", subElm.Element("begindate"), objTable, objId, (id > 0) ? "M" : "A");
                        PutDate(obj, "CUST_DAT3", subElm.Element("enddate"), objTable, objId, (id > 0) ? "M" : "A");
                    }

                    subElm = record.Element("declaration");
                    if (subElm != null)
                    {
                        PutString(obj, "CUST_TXT8", subElm.Element("code"), 50);
                        PutString(obj, "CUST_TXT9", subElm.Element("markmodule"), 50);
                    }

                    subElm = record.Element("manufacturer");
                    if (subElm != null)
                    {
                        PutString(obj, "COUNTRY", subElm.Element("factory_country_code"), 50);

                        string mnfExtId = subElm.Element("id") == null ? "" : subElm.Element("id").Value.ToString(); ;
                        string mnfName = subElm.Element("title") == null ? "" : subElm.Element("title").Value.ToString();
                        if (mnfName.Length > 50) mnfName = mnfName.Remove(50);

                        if (!string.IsNullOrEmpty(mnfExtId))
                        {
                            int mnfId = GetIdFromExtId("MANUFACTURER", mnfExtId, "RRES"); //TODO : determine external service
                            using (IMObject mnfObj = (mnfId > 0) ? IMObject.LoadFromDB("MANUFACTURER", mnfId) : IMObject.New("MANUFACTURER"))
                            {
                                mnfObj["NAME"] = mnfName;
                                PutString(mnfObj, "COUNTRY_ID", subElm.Element("office_country_code"), 3);
                                mnfObj.SaveToDB();
                                if (mnfId <= 0)
                                    SaveNewSynonym("MANUFACTURER", (int)mnfObj["ID"], mnfExtId, "RRES");

                                obj["MANUF_ID"] = mnfObj.GetI("ID");
                            }
                        }
                        obj["MANUFACTURER"] = mnfName;
                    }

                    subElm = record.Element("purposes");
                    if (subElm != null)
                    {
                        string s = "";
                        foreach (XElement prpXel in subElm.Elements())
                        {
                            if (prpXel.Name.ToString().ToLower() == "purpose")
                            {
                                if (!string.IsNullOrEmpty(s)) s += '\n';
                                s += prpXel.Value.ToString();
                            }
                        }
                        if (s.Length > 1000) s = s.Remove(1000);
                        obj["CUST_TXT6"] = s;
                    }

                    List<int> rtechIds = new List<int>();
                    subElm = record.Element("equipment-blocks");
                    if (subElm != null)
                    {
                        MessageBox.Show("'equipment-blocks' length: " + subElm.ToString().Length.ToString());
                        foreach (XElement tabsXel in subElm.Elements())
                        {
                            if (tabsXel.Name.ToString().ToLower() == "equipment_block")
                            {
                                XElement tabElm = null;
                                tabElm = tabsXel.Element("rtech_id");
                                if (tabElm != null)
                                    try
                                    {
                                        int rtechId = Convert.ToInt32(tabElm.Value);
                                        if (!rtechIds.Contains(rtechId))
                                            rtechIds.Add(rtechId);
                                    }
                                    catch { }
                            }
                        }
                    }

                    if (rtechIds.Count() == 1)  // if only one Rtech then assign it to the record
                    {
                        try
                        {
                            IMObject rtechObj = IMObject.LoadFromDB("RADIO_SYSTEMS", rtechIds.First());
                            obj["RADIOSYS"] = rtechObj.GetS("NAME");
                        }
                        catch (Exception e)
                        {
                            WriteLog(funcName + ": Loading RTECH: " + e.Message, objTable, objId, ExchangeDir.edIn, "", "Error");
                        }
                    }

                    if (id <= 0)
                    {
                        obj.Put("DATE_CREATED", DateTime.Now);
                        obj["CREATED_BY"] = IM.ConnectedUser();
                    }
                    else
                    {
                        obj.Put("DATE_MODIFIED", DateTime.Now);
                        obj["MODIFIED_BY"] = IM.ConnectedUser();
                    }

                    obj.SaveToDB();

                    if (id <= 0)
                        SaveNewSynonym(objTable, (int)obj["ID"], objExtId, "RRES");
                }

                // XRED_REGISTER
                IMRecordset rs = new IMRecordset("XREDREG_CONTENT", IMRecordset.Mode.ReadWrite);
                try
                {
                    try
                    {
                        rs.Select("ID,EQUIP_ID,NUM,CONTENT");
                        rs.SetWhere("EQUIP_ID", IMRecordset.Operation.Eq, objId);
                        for (rs.Open(); !rs.IsEOF(); rs.MoveNext())
                            rs.Delete();
                        string content = record.ToString();
                        const int maxChunkLen = 4000;
                        int num = 1;
                        while (content.Length > 0 || num == 1)
                        {
                            rs.AddNew();
                            rs.Put("ID", IM.AllocID("XREDREG_CONTENT", 1, -1));
                            rs.Put("EQUIP_ID", objId);
                            rs.Put("NUM", num++);
                            int chunkLen = content.Length > maxChunkLen ? maxChunkLen : content.Length;
                            rs.Put("CONTENT", content.Substring(0, chunkLen));                            
                            rs.Update();
                            content = content.Remove(0, chunkLen);
                        }
                    }
                    finally
                    {
                        rs.Destroy();
                    }
                }
                catch (Exception e)
                {
                    WriteLog(funcName + ": Exception writing XRED_REGISTER record: " + e.Message, objTable, objId, ExchangeDir.edIn, action, "Error");
                }
                objectsProcessed++;
            }
            return objectsProcessed;
        }
        /// <summary>
        /// Puts a string into the specified field of the given object. String is shortened to maxLen specified, if needed
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="fieldName"></param>
        /// <param name="xel"></param>
        /// <param name="maxLen"></param>
        void PutString(IMObject obj, string fieldName, XElement xel, int maxLen)
        {
            if (xel != null)
            {
                string str = xel.Value.ToString();
                if ((maxLen > 0) && (str.Length > maxLen))
                    str = str.Remove(maxLen);
                obj[fieldName] = str;
            }
        }

        void PutDate(IMObject obj, string fieldName, XElement xel, string tablename, int id, string action)
        {
            if (xel != null)
            {
                string sVal = xel.Value.ToString();
                try
                {
                    obj.Put(fieldName, string.IsNullOrEmpty(sVal) ? IM.NullT : DateTime.Parse(sVal));
                }
                catch (Exception e)
                {
                    WriteLog(string.Format("{0}={1}: {2}", xel.Name.ToString(), sVal, e.Message), tablename, id, ExchangeDir.edIn, action, "Error");
                }
            }
        }

        void SaveNewSynonym(string icsmTbl, int id, string extId, string extSvc)
        {
            IMRecordset rs = new IMRecordset("XUTDB_X_SYN", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");
                rs.SetWhere("EXTERNAL_ID", IMRecordset.Operation.Eq, extId);
                rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, icsmTbl);
                rs.Open();
                rs.AddNew();
                rs.Put("OBJ_TABLE", icsmTbl);
                rs.Put("OBJ_ID", id);
                rs.Put("EXTERNAL_SVC", extSvc);
                rs.Put("EXTERNAL_ID", extId);
                rs.Update();
            }
            finally
            {
                rs.Destroy();
            }
        }
        /// <summary>
        /// Looks for ID of the object received from the external system
        /// </summary>
        /// <param name="icsmTbl">ICSM table</param>
        /// <param name="extId"></param>
        /// <param name="extSvc"></param>
        /// <returns>ID of the object, -1 if search gives no result</returns>
        int GetIdFromExtId(string icsmTbl, string extId, string extSvc)
        {
            int retVal = -1;
            IMRecordset rs = new IMRecordset("XUTDB_X_SYN", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");
                rs.SetWhere("EXTERNAL_ID", IMRecordset.Operation.Eq, extId);
                rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, icsmTbl);
                rs.Open();
                if (!rs.IsEOF())
                    retVal = rs.GetI("OBJ_ID");
            }
            finally
            {
                rs.Destroy();
            }
            return retVal;
        }

        string getGuid(string ObjTable, int id, string External_SVC)
        {
            string guid = "";
            IMRecordset rs = new IMRecordset("XUTDB_X_SYN", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");
                rs.SetWhere("OBJ_ID", IMRecordset.Operation.Eq, id);
                rs.SetWhere("OBJ_TABLE", IMRecordset.Operation.Eq, ObjTable);
                rs.Open();
                if (rs.GetS("EXTERNAL_ID").Length > 1)
                {
                    guid = rs.GetS("EXTERNAL_ID");
                }
                else
                {
                    guid = Guid.NewGuid().ToString();
                    rs.AddNew();
                    rs.Put("OBJ_TABLE", ObjTable);
                    rs.Put("OBJ_ID", id);
                    rs.Put("EXTERNAL_SVC", External_SVC);
                    rs.Put("EXTERNAL_ID", guid);
                    rs.Update();
                }
            }
            catch (Exception ex)
            {
                WriteLog(String.Format("Get GUID: ObjTable={1}, id={2}, External_SVC={3}, Exception: {0}", ex.Message, ObjTable, id, External_SVC),
                    "XUTDB_X_SYN", 0, ExchangeDir.edUnknown, "", "Error");
            }
            finally
            {
                rs.Close();
                rs.Destroy();
            }
            return guid;
        }

        void WriteLog(string Message, string table, int id, ExchangeDir direction, string action, string eventType)
        {
            if (string.IsNullOrEmpty(table))
                table = "---";

            IMRecordset rs = new IMRecordset("XUTDB_X_LOG", IMRecordset.Mode.ReadWrite);
            try
            {
                rs.Select("INFO, EVENT, ACTION, DIRECTION, EVENT_DATE, OBJ_ID, OBJ_TABLE,");
                rs.Open();
                rs.AddNew();
                rs.Put("OBJ_TABLE", table);
                rs.Put("EVENT_DATE", DateTime.Now);
                rs.Put("INFO", Message);
                rs.Put("OBJ_ID", id);
                rs.Put("ACTION", !string.IsNullOrEmpty(action) ? action : "?");
                rs.Put("EVENT", !string.IsNullOrEmpty(eventType) ? eventType : "?");
                rs.Put("DIRECTION", direction);
                rs.Update();
            }
            catch
            {

            }
            rs.Destroy();
            if (isInteractive)
            {
                MessageBox.Show(Message, eventType, MessageBoxButtons.OK,
                    eventType.ToLower().StartsWith("error") ? MessageBoxIcon.Error
                    : eventType.ToLower().StartsWith("warn") ? MessageBoxIcon.Warning
                    : MessageBoxIcon.Information
                    );
            }
        }
    }
}
