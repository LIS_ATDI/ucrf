﻿using System;
using System.Linq;
using System.Text;
using ICSM;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace XICSM.UtdbExchange
{
    public class Plugin : IPlugin
    {
        public string Ident { get { return "UtdbExchange"; } }
        public string Description { get { return "UTDB exchange module"; } }
        public double SchemaVersion { get { return 20120712.1041; } }

        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            try
            {
                if (dbCurVersion < 20120712.1040)
                {
                    s.CreateTables("XUTDB_X_CACHE");
                    s.CreateTables("XUTDB_X_LOG");
                    s.CreateTables("XUTDB_X_SYN");
                    s.CreateTables("XUTDB_X_STATUS");
                    s.CreateTables("XUTDB_X_EVENT");
                    s.SetDatabaseVersion(20120712.1041);
                }
                else if (dbCurVersion < 20120712.1041)
                {
                    s.CreateTableFields("XUTDB_X_CACHE", "STATUS");
                    s.CreateTables("XUTDB_X_STATUS");
                    s.CreateTables("XUTDB_X_EVENT");
                    s.SetDatabaseVersion(20120712.1041);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("UpgradeDatabase(): {0}", ex.Message), Description, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public void GetMainMenu(IMMainMenu mainMenu)
        {
            try
            {
                mainMenu.InsertItem("UTDB Exchange\\Export", OnExportToUtdb, "XUTDB_X_CACHE");
                mainMenu.InsertItem("UTDB Exchange\\Import", OnImportFromUtdb, "XUTDB_X_CACHE");
                mainMenu.InsertItem("Configuration\\UTDB Exchange\\Settings", OnSettings, "XUTDB_X_CACHE");
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("GetMainMenu(): {0}", ex.Message), Description, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool OtherMessage(string message, object inParam, ref object outParam)
        {
            System.Diagnostics.Debug.Indent();
            System.Diagnostics.Debug.WriteLine("OtherMessage(" + message + ")");
            System.Diagnostics.Debug.Unindent();
            try
            {
                if (message == "UTDB_EXCHANGE")
                {
                    ExchangeModule xMod = new ExchangeModule(false);
                    xMod.ImportFromUtdb();
                    xMod.ExportToUtdb();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("OtherMessage("+message+"):\n"+ex.Message, 
                    Ident + " " + Description, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void RegisterBoard(IMBoard b)
        {
            //throw new NotImplementedException();
        }

        public void RegisterSchema(IMSchema s)
        {
            s.DeclareTable("XUTDB_X_CACHE", "UTDB Exchange Queue", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("DATE_CACHED", "DATE", null, "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_CACHE", "PRIMARY", "OBJ_TABLE,OBJ_ID,DATE_CACHED");
                s.DeclareField("ACTION", "VARCHAR(1)", null, "NOTNULL", null);
                s.DeclareField("INFO", "VARCHAR(1000)", null, null, null);
                s.DeclareField("STATUS", "NUMBER(2,0)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_LOG", "UTDB Exchange Log", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("EVENT_DATE", "DATE", "Date", "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_LOG", "PRIMARY", "OBJ_TABLE,OBJ_ID,EVENT_DATE");
                s.DeclareField("DIRECTION", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("ACTION", "VARCHAR(1)", null, "NOTNULL", null);
                s.DeclareField("EVENT", "VARCHAR(1000)", null, "NOTNULL", null);
                s.DeclareField("INFO", "VARCHAR(1000)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_SYN", "UTDB Exchange Synonyms", "PLUGIN_3,99");
            {
                s.DeclareField("OBJ_TABLE", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("OBJ_ID", "NUMBER(9,0)", null, "NOTNULL", null);
                s.DeclareField("EXTERNAL_SVC", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareField("EXTERNAL_ID", "VARCHAR(32)", null, "NOTNULL", null);
                s.DeclareIndex("PK_XUTDB_X_SYN", "PRIMARY", "OBJ_TABLE,OBJ_ID,EXTERNAL_SVC,EXTERNAL_ID");

            }
            s.DeclareTable("XUTDB_X_STATUS", "UTDB Exchange Statuses", "PLUGIN_3,99");
            {
                s.DeclareField("CODE", "NUMBER(2,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(30)", null, null, null);
                s.DeclareField("DESC", "VARCHAR(1000)", null, null, null);
            }
            s.DeclareTable("XUTDB_X_EVENT", "UTDB Exchange Event Types", "PLUGIN_3,99");
            {
                s.DeclareField("CODE", "NUMBER(2,0)", null, null, null);
                s.DeclareField("NAME", "VARCHAR(30)", null, null, null);
                s.DeclareField("DESC", "VARCHAR(1000)", null, null, null);
            }
        }

        private void OnExportToUtdb()
        {
            ExchangeModule xMod = new ExchangeModule(true);
            xMod.ExportToUtdb();
        }

        private void OnImportFromUtdb()
        {
            ExchangeModule xMod = new ExchangeModule(true);
            xMod.ImportFromUtdb();
        }

        public void OnSettings()
        {
            Params p = new Params();
            using (DgSettings settings = new DgSettings(p))
            {
                if (settings.ShowDialog() == DialogResult.OK)
                {
                    p.SaveToDb();
                }
            }
        }
        
    }
}
