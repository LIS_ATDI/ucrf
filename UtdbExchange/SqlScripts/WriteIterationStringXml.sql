CREATE OR REPLACE FUNCTION UTDB.GetNextExchangeId
    return integer
as
    iRecId integer;
begin
    SELECT UTDB.SQV_EXCHANGE.nextval into iRecId 
    FROM DUAL;
    return(iRecId);
end;
/
CREATE OR REPLACE PROCEDURE UTDB.WriteStringXmlFirst (iRecId integer, strXml varchar, strEssence varchar, strService varchar)
as
    iEssId integer;
    iServId integer;
    clobXml CLOB;
begin
    select es.ID into iEssId
    from UTDB.ESSENCES es
    where es.NAME=strEssence;

    select ser.ID into iServId
    from UTDB.SERVICES ser
    where ser.NAME=strService;

    insert into UTDB.EXCHANGE
        (ID,
        XML_CLOB,
        DATE_TIME,
        ESSENCE_ID, 
        OWNER_SERVICE_ID) 
    values (
        iRecId,
        TO_CLOB(strXml),
        sysdate,
        iEssId,
        iServId);
end;
/
CREATE OR REPLACE PROCEDURE UTDB.WriteStringXmlNext (iRecId varchar, strXml varchar)
as
begin
    update UTDB.EXCHANGE
    set XML_CLOB = XML_CLOB || TO_CLOB(strXml)
    where ID = iRecId;
end;
/
CREATE OR REPLACE PROCEDURE UTDB.WriteStringXmlLast (iRecId varchar, strXml varchar)
as
begin
    update UTDB.EXCHANGE
    set XML_CLOB = XML_CLOB || TO_CLOB(strXml)
    where ID = iRecId;
    
    UTDB.instLINK(iRecId, 0, 0, sysdate);
end;