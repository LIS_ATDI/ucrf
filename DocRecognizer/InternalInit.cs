﻿using DocRecognizer.Interface;
using DocRecognizer.Object;

namespace DocRecognizer
{
    internal static class InternalInit
    {
        /// <summary>
        /// Возвращает класс фабрики
        /// </summary>
        /// <returns></returns>
        public static IFactory GetFactory()
        {
            return new Factory();
        }
        /// <summary>
        /// Возвращает класс фабрики-одиночки
        /// </summary>
        /// <returns></returns>
        public static ISingletonFactory GetSingletonFactory()
        {
            return new SingletonFactory();
        }
        /// <summary>
        /// Формирует путь к файлу на основании установленной папки приложения
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Путь к файлу на основании установленной папки приложения</returns>
        public static string GetFullPath(string fileName)
        {
            return fileName;
        }
    }
}
