﻿using System;
using System.Collections.Generic;
using DocRecognizer.Interface;
using DocRecognizer.Object;

namespace DocRecognizer
{
    public class DocData
    {
        /// <summary>
        /// Номер документа
        /// </summary>
        public string DocNumber { get; set; }
        /// <summary>
        /// Номер бланка
        /// </summary>
        public string BlankNum { get; set; }
        /// <summary>
        /// Дата печати
        /// </summary>
        public DateTime PrintDate { get; set; }
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime EndDate { get; set; }
    }

    internal class DocRecognizer : IDocRecognizer
    {
        private static string _basePath = "";
        public static string BasePath
        {
            get
            {
                if (string.IsNullOrEmpty(_basePath))
                    return "";
                return _basePath;
            }
        }
        private bool _disposed;
        public bool IsInit { get; protected set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public DocRecognizer()
        {
            InternalInit.GetSingletonFactory().Init();
            _disposed = true;
            IsInit = false;
            _basePath = "";
        }
        /// <summary>
        /// Деструктор
        /// </summary>
        ~DocRecognizer()
        {
            Dispose(false);
        }

        #region Implementation of IDocRecognizer

        public event CommitScanCopyDelegate CommitScanCopy;

        /// <summary>
        /// Инициализация библиотеки
        /// </summary>
        /// <returns>TRUE - инициализация успешно, иначе FALSE</returns>
        public bool Init(string basePath)
        {
            _basePath = basePath;
            if (System.IO.Directory.Exists(_basePath) == false)
                System.IO.Directory.CreateDirectory(_basePath);
            DataBase.DbSqLite.InitDatabase();
            SettingsObject.Reload();
            InternalInit.GetSingletonFactory().GetDocumentNumberManager.ClearAll();
            IsInit = true;
            return IsInit;
        }
        /// <summary>
        /// Загрузить номера известных документав. Документы, у которых нет привязки к скан-копии
        /// </summary>
        /// <param name="listKnownDocuments">Список номеров документов</param>
        /// <param name="docType">Тип документа</param>
        public void LoadDocuments(string docType, params DocData[] listKnownDocuments)
        {
            InternalInit.GetSingletonFactory().GetDocumentNumberManager.AddDocuments(docType, listKnownDocuments);
        }
        /// <summary>
        /// Загрузить номера известных документав. Документы, у которых нет привязки к скан-копии
        /// </summary>
        /// <param name="listKnownDocuments">Список номеров документов</param>
        /// <param name="docType">Тип документа</param>
        public void LoadScanedDocuments(string docType, params DocData[] listKnownDocuments)
        {
            InternalInit.GetSingletonFactory().GetDocumentNumberManager.AddScanedDocuments(docType, listKnownDocuments);
        }
        /// <summary>
        /// Возвращает интерфейс работи с формой распознования
        /// </summary>
        /// <returns>Интерфейс работи с формой распознования</returns>
        public IRecognizeWindow GetRecognizeWindow()
        {
            return new RecognizeWindow();
        }
        /// <summary>
        /// Вызов функции для коммита
        /// </summary>
        public virtual bool RaiseCommitScanCopyEvent(params ICommitScanData[] param)
        {
            bool retVal = false;
            if (CommitScanCopy != null)
            {
                CommitScanCopy(param);
                retVal = true;
            }
            return retVal;
        }
        #endregion
        //=================================================
        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
        //=================================================
        /// <summary>
        /// Реализация Dispose
        /// </summary>
        /// <param name="disposing">TRUE - вызвано из метода IDisposable.Dispose()</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (this)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        //Удаляем manage resource
                    }
                    _disposed = true;
                }
            }
        }
    }
    /// <summary>
    /// Данные для коммита во внешнюю систему
    /// </summary>
    internal class CommitScanData : ICommitScanData
    {
        #region Implementation of ICommitScanData

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime DateOut { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Номер бланка
        /// </summary>
        public string BlankNumber { get; set; }

        /// <summary>
        /// Путь к скан копии
        /// </summary>
        public string ScanFileName { get; set; }

        /// <summary>
        /// Флаг необходимости удалить скан
        /// </summary>
        public bool NeedDeleteScanFile { get; set; }

        /// <summary>
        /// Результат операции
        /// </summary>
        public bool CommitIsOk { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }

        #endregion
        /// <summary>
        /// Конструктор
        /// </summary>
        public CommitScanData()
        {
            Number = "";
            DateOut = DateTime.MinValue;
            DateEnd = DateTime.MinValue;
            BlankNumber = "";
            ScanFileName = "";
            NeedDeleteScanFile = true;
            CommitIsOk = false;
            ErrorMessage = "";
        }
    }
}
