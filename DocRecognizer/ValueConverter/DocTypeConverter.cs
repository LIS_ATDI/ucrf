﻿using System;
using System.Globalization;
using System.Windows.Data;
using DocRecognizer.Enum;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(string), typeof(string))]
    public class DocTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = "";
            string dt = value.ToString();
            retVal = dt.DocTypeEnumToStr();
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = value.ToString().ToDocTypeEnum();
            return retVal;
        }
    }
}
