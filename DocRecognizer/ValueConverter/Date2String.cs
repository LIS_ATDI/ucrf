﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class Date2String : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = "";
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                if(dt != DateTime.MinValue)
                   retVal = dt.ToString("yyyy.MM.dd");
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string newStr = value.ToString().Replace(',', '.');
            DateTime retVal;
            if (DateTime.TryParseExact(newStr, "yyyy.MM.dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out retVal))
                return retVal;
            return DateTime.MinValue;
        }
    }
}
