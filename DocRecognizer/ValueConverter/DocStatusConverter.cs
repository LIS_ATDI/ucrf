﻿using System;
using System.Globalization;
using System.Windows.Data;
using DocRecognizer.Enum;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(string), typeof(DocStatusEnum))]
    public class DocStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = "";
            if (value is DocStatusEnum)
            {
                DocStatusEnum dt = (DocStatusEnum)value;
                retVal = dt.ToTranslateString();
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DocStatusEnum retVal = value.ToString().ToDocStatusEnumFromTranslate();
            return retVal;
        }
    }
}
