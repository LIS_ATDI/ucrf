﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(BitmapImage), typeof(Bitmap))]
    public class Bitmap2BitmapImage : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage retVal = null;
            try
            {
                Bitmap bm = (Bitmap) value;
                if (bm != null)
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        bm.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Bmp);
                        BitmapImage bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.StreamSource = memoryStream;
                        bitmapImage.EndInit();
                        retVal = bitmapImage;
                    }
                }
            }
            catch
            {
                retVal = null;
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
