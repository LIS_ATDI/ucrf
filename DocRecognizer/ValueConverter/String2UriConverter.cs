﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(Uri), typeof(string))]
    public class String2UriConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri retVal = new Uri(value.ToString());
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = value.ToString();
            return retVal;
        }
    }
}
