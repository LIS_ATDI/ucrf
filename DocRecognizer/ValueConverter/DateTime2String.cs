﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace DocRecognizer.ValueConverter
{
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class DateTime2String : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = "";
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                retVal = dt.ToString("yyyy.MM.dd HH:mm:ss");
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
