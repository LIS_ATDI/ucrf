﻿using System;
using System.ComponentModel;
using System.Windows;
using Microsoft.Windows.Controls;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Interaction logic for SetDateOutWindow.xaml
    /// </summary>
    internal partial class SetDateOutWindow : Window, INotifyPropertyChanged
    {
        private DateTime _selectedDate;
        /// <summary>
        /// Выбраная дата
        /// </summary>
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if (value != _selectedDate)
                {
                    _selectedDate = value;
                    InvokeNotifyPropertyChanged("SelectedDate");
                }
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public SetDateOutWindow() : this(DateTime.Now)
        {}
        /// <summary>
        /// Конструктор
        /// </summary>
        public SetDateOutWindow(DateTime setDate)
        {
            InitializeComponent();
            dtCalendar.DataContext = this;
            DateTime endDate = new DateTime(2100, 1, 1);
            if (dtCalendar.DisplayDateEnd != null)
                endDate = dtCalendar.DisplayDateEnd.Value;
            CalendarDateRange r = new CalendarDateRange(DateTime.Now.AddDays(1), endDate);
            dtCalendar.BlackoutDates.Add(r);
            //--
            btnOk.DataContext = this;
            SelectedDate = setDate;
        }
        /// <summary>
        /// Подтвердили дату
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        #region Implementation of INotifyPropertyChanged
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get
            {
                return _isChanged;
            }
            set
            {
                if (value != _isChanged)
                {
                    _isChanged = value;
                    InvokeNotifyPropertyChanged("IsChanged");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != "IsChanged")
                    IsChanged = true;
            }
        }
        #endregion
    }
}
