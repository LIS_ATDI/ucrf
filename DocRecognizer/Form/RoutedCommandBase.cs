﻿using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DocRecognizer.Form
{
    public class RoutedCommandBase : RoutedUICommand
    {
        /// <summary>
        /// Картинка команды
        /// </summary>
        public BitmapImage ImageBitmap { get; set; }
        /// <summary>
        /// Кончтруктор
        /// </summary>
        public RoutedCommandBase()
        {
            ImageBitmap = null;
        }
    }
}
