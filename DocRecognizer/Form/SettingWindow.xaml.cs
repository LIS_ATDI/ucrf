﻿using System;
using System.Collections.Generic;
using System.Windows;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Interaction logic for SettingWindow.xaml
    /// </summary>
    internal partial class SettingWindow : Window
    {
        public SettingWindow()
        {
            InitializeComponent();
            //-----
            this.DataContext = SettingsObject.Instance;
            RecognizeGrid.DataContext = SettingsObject.Instance.SettingsDocument;
        }
        /// <summary>
        /// Выбор Dll для Flexi Capture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.Filter = "FCEngine.dll|FCEngine.dll";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SettingsObject.Instance.FlexiCaptureDll = openFileDialog1.FileName;
            }
        }
        /// <summary>
        /// Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_btnCancel(object sender, RoutedEventArgs e)
        {
            SettingsObject.Reload();
            this.Close();
        }
        /// <summary>
        /// Сохраняем даные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            SettingsObject.Save(SettingsObject.Instance);
            Enum.DocTypeExtension.Reload();
            this.Close();
        }
        /// <summary>
        /// Удалить документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteDocument_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Do you want to delete selected document", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                List<object> selectedRecord = new List<object>();
                foreach (object item in RecognizeGrid.SelectedItems)
                {
                    selectedRecord.Add(item);
                }
                foreach (object record in selectedRecord)
                {
                    SettingsDocRecognizer val = record as SettingsDocRecognizer;
                    if (val != null)
                    {
                        SettingsObject.Instance.SettingsDocument.Remove(val);
                    }
                }
            }
        }
        /// <summary>
        /// Добавить документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddDocument_Click(object sender, RoutedEventArgs e)
        {
            SettingsObject.Instance.SettingsDocument.Add(new SettingsDocRecognizer { DocType = Guid.NewGuid().ToString("N")});
        }

        private void Button_Click_SelectFleciCaptureProject(object sender, RoutedEventArgs e)
        {
            if (RecognizeGrid.SelectedItem != null)
            {
                System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
                openFileDialog1.Filter = "ABBYY FlexiCapture project file|*.fcproj";
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SettingsDocRecognizer val = RecognizeGrid.SelectedItem as SettingsDocRecognizer;
                    if (val != null)
                        val.FlexiCaptureProject = openFileDialog1.FileName;
                }
            }
        }
    }
}
