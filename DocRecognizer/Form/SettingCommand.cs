﻿using System.Windows.Input;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды удаления строки из списка документов
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase SetSettingCommand = new RoutedCommandBase();
        /// <summary>
        /// Управление доступом к команде удвления документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandCanExecuteSetSetting(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy;
        }
        /// <summary>
        /// Удаление документов из списка документов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedSetSetting(object sender, ExecutedRoutedEventArgs e)
        {
            SettingWindow settingForm = new SettingWindow();
            settingForm.ShowDialog();
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
