﻿using System.Windows;
using DocRecognizer.Enum;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Interaction logic for SelectTypeDocument.xaml
    /// </summary>
    partial class SelectTypeDocument : Window
    {
        public string DocType { get; protected set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public SelectTypeDocument()
        {
            InitializeComponent();
            //---
            cbDocType.DataContext = DocTypeExtension.GetDictDocTypeEnum();
            DocType = "";
        }
        /// <summary>
        /// Закрываем форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cbDocType.SelectedItem != null)
            {
                DocType = ((DocTypeExtension.ValueDescriptionDocTypeEnum) cbDocType.SelectedItem).Value;
                this.Close();
            }
        }
    }
}
