﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Загрузка картинок"
    /// </summary>
    internal partial class RecognizeForm
    {
        /// <summary>
        /// вспомагательный класс для передачи параметров
        /// </summary>
        private class AddImageData
        {
            public string DocType { get; set; }
            public List<string> FileNames { get; set; }

            public AddImageData()
            {
                DocType = "";
                FileNames = new List<string>();
            }
        }
        //
        public static RoutedCommandBase LoadImageCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Загрузки сканов во внешнюю систему"
        /// </summary>
        public bool IsLoadImageEnable
        {
            get
            {
                return !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy && !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Выполнение команды Загрузить картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedLoadImageCommand(object sender, ExecutedRoutedEventArgs e)
        {
            SelectTypeDocument selectDocType = new SelectTypeDocument();
            selectDocType.Owner = this;
            selectDocType.ShowDialog();
            if (string.IsNullOrEmpty(selectDocType.DocType))
                return;
            
            string docType = selectDocType.DocType;
            System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.Filter = "Image Files(*.PNG;*.JPG;*.TIF;*.BMP)|*.PNG;*.JPG;*.TIF;*.BMP";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Multiselect = true;

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                List<string> fileList = new List<string>();
                foreach (string fileName in openFileDialog1.FileNames)
                {
                    bool isExist = false;
                    for (int i = 0; i < _dbDocuments.Count; i++)
                    {
                        DocumentData doc = _dbDocuments[i];
                        if (System.IO.Path.GetFileName(doc.ImageFile) == System.IO.Path.GetFileName(fileName))
                        {
                            isExist = true;
                            break;
                        }
                    }
                    if (isExist)
                    {
                        System.Windows.MessageBox.Show(
                            string.Format("Файл '{0}' вже завантажено до поекту. Завантаження файлу буде відмінено.",
                                          System.IO.Path.GetFileName(fileName)));
                    }
                    else
                    {
                        fileList.Add(fileName);
                    }
                }
                if (fileList.Count > 0)
                {
                    AddImageData data = new AddImageData();
                    data.FileNames = fileList;
                    data.DocType = docType;
                    BackgroundThread.BackgroundThreadParameter param = new BackgroundThread.BackgroundThreadParameter
                                                                           {
                                                                               CommandType =
                                                                                   BackgroundThread.CommandParallelEnum.
                                                                                   AddFiles,
                                                                               Data = data,
                                                                               ExecuteCallBack = RunAddImage
                                                                           };
                    BackgroundThread.BackgroundThreadObject.Run(param);
                }
            }
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Функция распознования (паралельный поток)
        /// </summary>
        /// <param name="bgWorker"></param>
        /// <param name="e"></param>
        /// <param name="data"></param>
        private void RunAddImage(BackgroundWorker bgWorker, DoWorkEventArgs e, object data)
        {
            AddImageData inData = data as AddImageData;
            if(inData == null)
                throw new Exception("Error parameters");
            List<string> listDoc = inData.FileNames;
            //-------
            StatusBarObject sentStatus = new StatusBarObject();
            sentStatus.MaxVal = listDoc.Count;
            string folder = GetPictureFolder();
            string docType = inData.DocType;
            DateTime loadDate = DateTime.Now;
            for (int index = 0; index < listDoc.Count; index++)
            {
                string fileName = listDoc[index];
                sentStatus.CurVal = index;
                sentStatus.BigText = string.Format("Додавання скан-копії '{0}'", fileName);
                sentStatus.SmallText = string.Format("{0}/{1}", index, sentStatus.MaxVal);
                if (bgWorker != null)
                    bgWorker.ReportProgress(0, sentStatus);
                string fileNameTo = System.IO.Path.Combine(folder, System.IO.Path.GetFileName(fileName));
                System.IO.File.Copy(fileName, fileNameTo, true);
                if (System.IO.File.Exists(fileNameTo))
                    _dbDocuments.Add(new DocumentData { ImageFile = fileNameTo, Type = docType, ScanDate = loadDate });
            }
        }
        /// <summary>
        /// Проверка возможности выполнения команды Загрузить картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteLoadImageCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsLoadImageEnable;
        }
        /// <summary>
        /// Возвращает папку для хранения картинок
        /// </summary>
        /// <returns>папку для хранения картинок</returns>
        private string GetPictureFolder()
        {
            string retVar = System.IO.Path.Combine(DocRecognizer.BasePath, "Picture");
            if (System.IO.Directory.Exists(retVar) == false)
                System.IO.Directory.CreateDirectory(retVar);
            return retVar;
        }
    }
}
