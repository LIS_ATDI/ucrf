﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using DocRecognizer.Object;
using Microsoft.Windows.Controls;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды удаления строки из списка документов
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase DeleteRowCommand = new RoutedCommandBase();
        /// <summary>
        /// Управление доступом к команде удвления документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandDataGridCanExecuteDeleteRow(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            DataGrid dg = e.Source as DataGrid;
            if (dg != null)
            {
                canExecute = (dg.SelectedItems.Count > 0);
            }
            e.CanExecute = canExecute & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
        }
        /// <summary>
        /// Удаление документов из списка документов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedDeleteRow(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show("Видалити вибрані записи?", "", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                DataGrid dg = e.Source as DataGrid;
                if (dg != null)
                {
                    List<DocumentData> listDeletedItems = new List<DocumentData>();
                    foreach (var selectedItem in dg.SelectedItems)
                    {
                        DocumentData docItem = selectedItem as DocumentData;
                        if (docItem != null)
                            listDeletedItems.Add(docItem);
                    }
                    foreach (DocumentData item in listDeletedItems)
                    {
                        _dbDocuments.Remove(item);
                    }
                }
                CommandManager.InvalidateRequerySuggested();
            }
        }
    }
}
