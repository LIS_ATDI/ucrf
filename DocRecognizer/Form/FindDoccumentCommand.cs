﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using DocRecognizer.Object;
using System.Windows.Forms;

namespace DocRecognizer.Form
{
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase FindDocumentCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Поиска документа"
        /// </summary>
        public bool IsFindDocumentEnable
        {
            get
            {
                bool isEnabled = false;
                foreach (DocumentData document in _dbDocuments)
                {
                    if (NeedToFindDocument(document))
                    {
                        isEnabled = true;
                        break;
                    }
                }
                return isEnabled & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Выполнение команды "Распознать все документы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedFindDocument(object sender, ExecutedRoutedEventArgs e)
        {
            List<DocumentData> list = new List<DocumentData>();
            for (int indexDoc = 0; indexDoc < _dbDocuments.Count; indexDoc++)
            {
                DocumentData document = _dbDocuments[indexDoc];
                if (NeedToFindDocument(document) == false)
                    continue;
                list.Add(document);
            }
            BackgroundThread.BackgroundThreadParameter param = new BackgroundThread.BackgroundThreadParameter
            {
                CommandType = BackgroundThread.CommandParallelEnum.FindDocument,
                Data = list,
                ExecuteCallBack = RunFindDocumentCommand
            };
            BackgroundThread.BackgroundThreadObject.Run(param);
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Проверка возможности выполнения команды Загрузить картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteFindDocument(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsFindDocumentEnable;
        }
        /// <summary>
        /// Проверка необходимости распознования документа
        /// </summary>
        /// <param name="document">Документ</param>
        /// <returns>TRUE - необходимо распознать, иначе FALSE</returns>
        private bool NeedToFindDocument(DocumentData document)
        {
            switch (document.Status)
            {
                case Enum.DocStatusEnum.Recognized:
                case Enum.DocStatusEnum.Exist:
                case Enum.DocStatusEnum.NotFound:
                case Enum.DocStatusEnum.Found:
                case Enum.DocStatusEnum.DuplicateNumber:
                    return true;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Функция поиска документов
        /// </summary>
        /// <param name="bgWorker"></param>
        /// <param name="e"></param>
        /// <param name="data"></param>
        private void RunFindDocumentCommand(BackgroundWorker bgWorker, DoWorkEventArgs e, object data)
        {
            int Cnt_AttachScanSucess = 0; int Cnt_AttachScanFail = 0;
            List<DocumentData> listDoc = data as List<DocumentData>;
            if (listDoc == null)
                throw new Exception("Error parameters");
            //-------
            var docListOfSystem = InternalInit.GetSingletonFactory().GetDocumentNumberManager.ListDocuments;
            var docListOfSystemScaned = InternalInit.GetSingletonFactory().GetDocumentNumberManager.ListScanedDocuments;
            StatusBarObject sentStatus = new StatusBarObject();
            sentStatus.BigText = "Пошук документів...";
            CLogs.WriteInfo(ELogsWhat.ScanDoc, "Початок процедури пошуку документів");
            bgWorker.ReportProgress(0, sentStatus);
            sentStatus.MaxVal = listDoc.Count;
            for (int i = 0; i < listDoc.Count; i++)
            {
                var docum = listDoc[i];

                if (string.IsNullOrEmpty(docum.LastNumber))
                    continue;

                if(string.IsNullOrEmpty(docum.LastNumber) == false)
                {
                    var listNumber = from p in listDoc
                                     where p != docum && (p.LastNumber == docum.LastNumber)
                                     select p;
                    if (listNumber.Count() > 0)
                    {
                        docum.SetStatusWithoutCheck(Enum.DocStatusEnum.DuplicateNumber);
                        CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Документ - {0} дублюється", docum.ImageFile));
                        continue;
                    }
                }
                bool ifFound = false;
                foreach (var dict in docListOfSystemScaned)
                {
                    if ((dict.Key != docum.Type) && (string.IsNullOrEmpty(dict.Key) == false))
                        continue;
                    if(dict.Value.Where(a => a.DocNumber.EndsWith(docum.LastNumber)).Count() > 0)
                    {
                        docum.SetStatusWithoutCheck(Enum.DocStatusEnum.Exist);
                        ifFound = true;
                        break;
                    }
                }

                if (ifFound)
                {
                    continue;
                }

                foreach (var systemDoc in docListOfSystem)
                {
                    if ((systemDoc.Key != docum.Type) && (string.IsNullOrEmpty(systemDoc.Key) == false))
                        continue;
                    bool inBool = false;
                    foreach (DocData value in systemDoc.Value)
                    {
                        if (value.DocNumber.EndsWith(docum.LastNumber))
                        {
                            docum.SetStatusWithoutCheck(Enum.DocStatusEnum.Found);
                            docum.Number = value.DocNumber;
                            docum.DateOut = value.PrintDate;
                            docum.BlankNumber = value.BlankNum;
                            docum.DateEnd = value.EndDate;
                            ifFound = true;
                            inBool = true;
                            break;
                        }
                    }
                    if (inBool)
                        break;
                }

                if (string.IsNullOrEmpty(docum.Number) == false)
                {
                    var listNumber = from p in listDoc
                                     where p != docum && (p.Number == docum.Number)
                                     select p;
                    if (listNumber.Count() > 0)
                    {
                        docum.SetStatusWithoutCheck(Enum.DocStatusEnum.DuplicateNumber);
                        foreach (var documentData in listNumber)
                            documentData.SetStatusWithoutCheck(Enum.DocStatusEnum.DuplicateNumber);

                        CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Документ - {0} дублюється", docum.ImageFile));
                        continue;
                    }
                }

                if (ifFound)
                {
                    Cnt_AttachScanSucess++;
                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Документ - {0} знайдено", docum.ImageFile));
                    continue;
                }

                docum.SetStatusWithoutCheck(Enum.DocStatusEnum.NotFound);
                CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Документ - {0} не знайдено", docum.ImageFile));

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            CLogs.WriteInfo(ELogsWhat.ScanDoc, "Кінець процедури пошуку документів");
            if (listDoc.Count > 0)
            {
                Cnt_AttachScanFail = listDoc.Count - Cnt_AttachScanSucess;
                MessageBox.Show(string.Format("Успішно знайдених документів - {0}. Документів, з іншими статусами  - {1}", Cnt_AttachScanSucess, Cnt_AttachScanFail));
            }
        }
    }
}
