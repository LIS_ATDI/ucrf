﻿using System.Windows.Input;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Сохранить документы"
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase SaveDocCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Поиска документа"
        /// </summary>
        public bool IsSaveDocCommandEnable
        {
            get
            {
                return _dbDocuments.IsChanged & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Выполнение команды "Сохранить документы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedSaveDocCommand(object sender, ExecutedRoutedEventArgs e)
        {
            _dbDocuments.Save();
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Проверка возможности выполнения команды "Сохранить документы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteSaveDocCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsSaveDocCommandEnable;
        }
    }
}
