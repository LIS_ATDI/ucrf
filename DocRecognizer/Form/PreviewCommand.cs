﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using DocRecognizer.Object;
using Microsoft.Windows.Controls;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды просмотр документа
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase PreviewCommand = new RoutedCommandBase();
        /// <summary>
        /// Управление доступом к команде удвления документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandDataGridCanExecutePreview(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            DataGrid dg = e.Source as DataGrid;
            if (dg != null)
            {
                canExecute = (dg.SelectedItems.Count > 0);
            }
            e.CanExecute = canExecute;
        }
        /// <summary>
        /// Удаление документов из списка документов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedPreview(object sender, ExecutedRoutedEventArgs e)
        {
            DataGrid dg = e.Source as DataGrid;
            if (dg != null)
            {
                foreach (var item in dg.SelectedItems)
                {
                    DocumentData dd = item as DocumentData;
                    if ((dd != null) && (System.IO.File.Exists(dd.ImageFile)))
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(dd.ImageFile);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
