﻿using System.Windows;
using System.Windows.Input;
using DocRecognizer.Object;
using Microsoft.Windows.Controls;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды изменение типа документа
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase ChangeTypeCommand = new RoutedCommandBase();
        /// <summary>
        /// Управление доступом к команде удвления документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandDataGridCanExecuteChangeType(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            DataGrid dg = e.Source as DataGrid;
            if (dg != null)
            {
                bool canExecuteTmp = true;
                foreach (var selectedItem in dg.SelectedItems)
                {
                    DocumentData docItem = selectedItem as DocumentData;
                    if (docItem != null)
                        canExecuteTmp &= IsChangeTypeEnable(docItem);
                }
                canExecute = canExecuteTmp & (dg.SelectedItems.Count > 0);
            }
            e.CanExecute = canExecute & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
        }
        /// <summary>
        /// Проверка возможности выполнения команды "Загрузки сканов во внешнюю систему"
        /// </summary>
        private bool IsChangeTypeEnable(DocumentData data)
        {
            if(data == null)
                return false;
            switch (data.Status)
            {
                case Enum.DocStatusEnum.Found:
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Удаление документов из списка документов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedChangeType(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show("Змінити тип документа?", "", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                SelectTypeDocument selectDocType = new SelectTypeDocument();
                selectDocType.Owner = this;
                selectDocType.ShowDialog();
                if (string.IsNullOrEmpty(selectDocType.DocType))
                    return;
                string typeDoc = selectDocType.DocType;
                DataGrid dg = e.Source as DataGrid;
                if (dg != null)
                {
                    foreach (var selectedItem in dg.SelectedItems)
                    {
                        DocumentData docItem = selectedItem as DocumentData;
                        if ((docItem != null) && IsChangeTypeEnable(docItem))
                        {
                            docItem.Type = typeDoc;
                            docItem.Status = Enum.DocStatusEnum.NotRecognized;
                            docItem.IsChecked = false;
                        }
                    }
                }
                CommandManager.InvalidateRequerySuggested();
            }
        }
    }
}
