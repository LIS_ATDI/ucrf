﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using DocRecognizer.Object;
using Microsoft.Windows.Controls;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды Установить дату выдачи
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase SetDateCommand = new RoutedCommandBase();
        /// <summary>
        /// Управление доступом к команде Установить дату выдачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteSetDateCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            DataGrid dg = e.Source as DataGrid;
            if (dg != null)
            {
                canExecute = (dg.SelectedItems.Count > 0);
            }
            e.CanExecute = canExecute & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
        }
        /// <summary>
        /// Установить дату выдачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedSetDateCommand(object sender, ExecutedRoutedEventArgs e)
        {
            SetDateOutWindow selectDateForm = new SetDateOutWindow();
            selectDateForm.SelectedDate = DateTime.Now;
            selectDateForm.Owner = this;
            if (selectDateForm.ShowDialog() == true)
            {
                DataGrid dg = e.Source as DataGrid;
                if (dg != null)
                {
                    foreach (var selectedItem in dg.SelectedItems)
                    {
                        DocumentData docItem = selectedItem as DocumentData;
                        if (docItem != null)
                            docItem.DateOut = selectDateForm.SelectedDate;
                    }
                }
                CommandManager.InvalidateRequerySuggested();
            }
        }
    }
}
