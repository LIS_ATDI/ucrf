﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DocRecognizer.Object;
using Rectangle=System.Drawing.Rectangle;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Interaction logic for RecognizeForm.xaml
    /// </summary>
    internal partial class RecognizeForm : Window
    {
        private const string KeyHeight = "RecognizeForm_Height";
        private const string KeyWidth = "RecognizeForm_Width";
        private const string KeyLeft = "RecognizeForm_Left";
        private const string KeyTop = "RecognizeForm_Top";
        private const string KeyWindowState = "RecognizeForm_WindowState";
        private DocumentsList _dbDocuments;
        public static StatusBarManager StatusBarElem = new StatusBarManager();
        /// <summary>
        /// Статический конструктор
        /// </summary>
        static RecognizeForm()
        {
            LoadImageCommand.Text = "Відкрити документ... ";
            LoadImageCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/document_open.png"));
            RecognizeAllDocCommand.Text = "Розпізнати всі скан-копії ";
            RecognizeAllDocCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/recognize_16_16.png"));
            SetSettingCommand.Text = "Налаштування... ";
            SetSettingCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/configure.png"));
            SaveDocCommand.Text = "Зберегти ";
            SaveDocCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/save.png"));
            FindDocumentCommand.Text = "Пошук документів ";
            FindDocumentCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/find.png"));
            CommitScanCommand.Text = "Залити скан-копії ";
            CommitScanCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/commit.png"));
            DeleteRowCommand.Text = "Видалити... ";
            DeleteRowCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/delete.png"));
            PreviewCommand.Text = "Перегляд... ";
            PreviewCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/icon_preview.png"));
            CheckAllCommand.Text = "Відмітити все ";
            CheckAllCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/checkbox_checked.png"));
            UncheckAllCommand.Text = "Скинути всі відмітки ";
            UncheckAllCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/checkbox_unchecked.png"));
            SetDateCommand.Text = "Встановити дату видачі ";
            SetDateCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/calendar.png"));
            ChangeTypeCommand.Text = "Змінити тип документа ";
            ChangeTypeCommand.ImageBitmap = new BitmapImage(new Uri("pack://application:,,,/DocRecognizer;Component/Icons/changeType.png"));
        }
        //-----------------
        public RecognizeForm()
        {
            _dbDocuments = new DocumentsList();
            _dbDocuments.Load();
            //-------
            InitializeComponent();
            DocsGrid.DataContext = _dbDocuments;
            this.Closing += Window_Closing;
            ImageView.SizeChanged += new SizeChangedEventHandler(ImageBorder_SizeChanged);
            ImageView.Loaded += new RoutedEventHandler(ImageView_Loaded);
            ImageBorder.SizeChanged += new SizeChangedEventHandler(ImageBorder_SizeChanged);
            //----
            this.Title += " (V1.1.1)";
            Config conf = new Config();
            try
            {
                this.WindowState = (WindowState) System.Enum.Parse(typeof (WindowState), conf.Get(KeyWindowState));
            }
            catch
            {
                this.WindowState = WindowState.Normal;
            }
            this.Left = conf.Get(KeyLeft, 100);
            if (this.Left < 0.0)
                this.Left = 0.0;
            this.Top = conf.Get(KeyTop, 100);
            if (this.Top < 0.0)
                this.Top = 0.0;
            this.Height = conf.Get(KeyHeight, 400);
            this.Width = conf.Get(KeyWidth, 650);
        }

        void ImageView_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateAllRectangles();
        }


        private void DocumentsGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            UpdateAllRectangles();
        }

        void ImageBorder_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateAllRectangles();
        }

        private void UpdateAllRectangles()
        {
            if (ImageView.Source == null)
            {
                HideLines(LineNumber1, LineNumber2, LineNumber3, LineNumber4);
                HideLines(LineNumberBlank1, LineNumberBlank2, LineNumberBlank3, LineNumberBlank4);
                HideLines(LineDateOut1, LineDateOut2, LineDateOut3, LineDateOut4);
                return;
            }
            BitmapSource sourceData = (BitmapSource)ImageView.Source;
            if (sourceData == null)
            {
                HideLines(LineNumber1, LineNumber2, LineNumber3, LineNumber4);
                HideLines(LineNumberBlank1, LineNumberBlank2, LineNumberBlank3, LineNumberBlank4);
                HideLines(LineDateOut1, LineDateOut2, LineDateOut3, LineDateOut4);
                return;
            }

            double xOffset = (ImageBorder.ActualWidth - ImageView.ActualWidth) / 2.0;
            double yOffset = (ImageBorder.ActualHeight - ImageView.ActualHeight) / 2.0;
            double xKoef = ImageView.ActualWidth / sourceData.PixelWidth;
            double yKoef = ImageView.ActualHeight / sourceData.PixelHeight;
            UpdateRectangle(xOffset, xKoef, yOffset, yKoef, LineNumber1, LineNumber2, LineNumber3, LineNumber4);
            UpdateRectangle(xOffset, xKoef, yOffset, yKoef, LineNumberBlank1, LineNumberBlank2, LineNumberBlank3, LineNumberBlank4);
            UpdateRectangle(xOffset, xKoef, yOffset, yKoef, LineDateOut1, LineDateOut2, LineDateOut3, LineDateOut4);
        }

        /// <summary>
        /// обновление квадрата
        /// </summary>
        /// <param name="xOffset"></param>
        /// <param name="xKoef"></param>
        /// <param name="yOffset"></param>
        /// <param name="yKoef"></param>
        /// <param name="line1"></param>
        /// <param name="line2"></param>
        /// <param name="line3"></param>
        /// <param name="line4"></param>
        private void UpdateRectangle(double xOffset, double xKoef, double yOffset, double yKoef, Line line1, Line line2, Line line3, Line line4)
        {
            bool isNull = false;
            Rectangle rec = new Rectangle();
            if (line1.Tag == null)
                isNull = true;
            else
            {
                rec = (Rectangle) line1.Tag;
                if (rec.IsEmpty)
                    isNull = true;
            }
            if (isNull)
            {
                HideLines(line1, line2, line3, line4);
            }
            else
            {
                rec.X -= 10;
                rec.Y -= 10;
                rec.Width += 10;
                rec.Height += 10;
                double x1 = xOffset + rec.Left * xKoef;
                double y1 = yOffset + rec.Top * yKoef;
                double x2 = x1 + rec.Width * xKoef;
                double y2 = y1 + rec.Height * yKoef;
                //--
                line1.X1 = x1;
                line1.Y1 = y2;
                line1.X2 = x1;
                line1.Y2 = y1;
                line1.Visibility = Visibility.Visible;
                //--
                line2.X1 = x1;
                line2.Y1 = y1;
                line2.X2 = x2;
                line2.Y2 = y1;
                line2.Visibility = Visibility.Visible;
                //--
                line3.X1 = x2;
                line3.Y1 = y1;
                line3.X2 = x2;
                line3.Y2 = y2;
                line3.Visibility = Visibility.Visible;
                //--
                line4.X1 = x2;
                line4.Y1 = y2;
                line4.X2 = x1;
                line4.Y2 = y2;
                line4.Visibility = Visibility.Visible;
            }
        }
        /// <summary>
        /// скрыть линии
        /// </summary>
        /// <param name="line1"></param>
        /// <param name="line2"></param>
        /// <param name="line3"></param>
        /// <param name="line4"></param>
        private void HideLines(Line line1, Line line2, Line line3, Line line4)
        {
            line1.Visibility = Visibility.Hidden;
            line2.Visibility = Visibility.Hidden;
            line3.Visibility = Visibility.Hidden;
            line4.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Закрываем окно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsSaveDocCommandEnable == true)
            {
                MessageBoxResult rez = MessageBox.Show("Ви бажаєте зберегти зміни?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (rez)
                {
                    case MessageBoxResult.Yes:
                        _dbDocuments.Save();
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
            if(e.Cancel == false)
            {
                Config conf = new Config();
                conf.Set(KeyWindowState, this.WindowState.ToString());
                if (this.WindowState == WindowState.Normal)
                {
                    conf.Set(KeyLeft, this.Left);
                    conf.Set(KeyTop, this.Top);
                    conf.Set(KeyHeight, this.Height);
                    conf.Set(KeyWidth, this.Width);
                }
                conf.Save();
            }
        }
        /// <summary>
        /// Отменить операцию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_CancelOperation(object sender, RoutedEventArgs e)
        {
            BackgroundThread.BackgroundThreadObject.BwCancel();
        }
    }

    /// <summary>
    /// Конвертор, для отображения количесва записей в гриде
    /// </summary>
    [ValueConversion(typeof(string), typeof(int))]
    public class CountRecordConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string retVal = string.Format("Завантажено документів ({0})", value);
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
