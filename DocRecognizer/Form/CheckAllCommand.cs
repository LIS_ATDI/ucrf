﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Отметить галочкой"
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase CheckAllCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Отметить галочкой"
        /// </summary>
        public bool IsCheckAllEnable
        {
            get
            {
                bool isEnabled = false;
                foreach (DocumentData document in _dbDocuments)
                {
                    if (CanCheckRow(document) && (document.IsChecked == false))
                    {
                        isEnabled = true;
                        break;
                    }
                }
                return isEnabled & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy && !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Управление доступом к команде "Отметить галочкой"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandDataGridCanExecuteCheckAll(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsCheckAllEnable;
        }
        /// <summary>
        /// "Отметить галочкой"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedCheckAll(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (DocumentData document in _dbDocuments)
            {
                if (CanCheckRow(document))
                    document.IsChecked = true;
            }
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Проверка необходимости "Отметить галочкой"
        /// </summary>
        /// <param name="document">Документ</param>
        /// <returns>TRUE - необходимо "Отметить галочкой", иначе FALSE</returns>
        private bool CanCheckRow(DocumentData document)
        {
            return document.CanCheck();
        }
    }
}
