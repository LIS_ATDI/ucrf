﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Отметить галочкой"
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase UncheckAllCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Отметить галочкой"
        /// </summary>
        public bool IsUncheckAllEnable
        {
            get
            {
                bool isEnabled = false;
                foreach (DocumentData document in _dbDocuments)
                {
                    if (document.IsChecked == true)
                    {
                        isEnabled = true;
                        break;
                    }
                }
                return isEnabled & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Управление доступом к команде "Отметить галочкой"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandDataGridCanExecuteUncheckAll(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsUncheckAllEnable;
        }
        /// <summary>
        /// "Отметить галочкой"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExecutedUncheckAll(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (DocumentData document in _dbDocuments)
            {
                document.IsChecked = false;
            }
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
