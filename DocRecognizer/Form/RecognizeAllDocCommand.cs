﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using DocRecognizer.Object;
using Lis.FlexiCapture;
using Rectangle=System.Drawing.Rectangle;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Распознать все документы"
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase RecognizeAllDocCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Расознать все документы"
        /// </summary>
        public bool IsRecognizeAllDocCommandEnable
        {
            get
            {
                bool isEnabled = false;
                foreach (DocumentData document in _dbDocuments)
                {
                    if (NeedToRecognize(document))
                    {
                        isEnabled = true;
                        break;
                    }
                }
                return isEnabled & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy & !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Выполнение команды "Распознать все документы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedRecognizeAllDocCommand(object sender, ExecutedRoutedEventArgs e)
        {
            List<DocumentData> list = new List<DocumentData>();
            for (int indexDoc = 0; indexDoc < _dbDocuments.Count; indexDoc++)
            {
                DocumentData document = _dbDocuments[indexDoc];
                if (NeedToRecognize(document) == false)
                    continue;
                list.Add(document);
            }
            BackgroundThread.BackgroundThreadParameter param = new BackgroundThread.BackgroundThreadParameter
                                                                   {
                                                                       CommandType = BackgroundThread.CommandParallelEnum.Recognize,
                                                                       Data = list,
                                                                       ExecuteCallBack = RecognizeCommand
                                                                   };
            BackgroundThread.BackgroundThreadObject.Run(param);
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Проверка возможности выполнения команды Загрузить картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteRecognizeAllDocCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsRecognizeAllDocCommandEnable;
        }
        /// <summary>
        /// Проверка необходимости распознования документа
        /// </summary>
        /// <param name="document">Документ</param>
        /// <returns>TRUE - необходимо распознать, иначе FALSE</returns>
        private bool NeedToRecognize(DocumentData document)
        {
            if (document.Status == Enum.DocStatusEnum.NotRecognized)
                return true;
            return false;
        }

        /// <summary>
        /// Функция распознования (паралельный поток)
        /// </summary>
        /// <param name="bgWorker"></param>
        /// <param name="e"></param>
        /// <param name="data"></param>
        private void RecognizeCommand(BackgroundWorker bgWorker, DoWorkEventArgs e, object data)
        {
            int Cnt_SuccessRecognize = 0; int Cnt_FailRecognize = 0;
            List<DocumentData> listDoc = data as List<DocumentData>;
            if (listDoc == null)
                throw new Exception("Error parameters");
            //-------
            IFlexiCapture flexiCapture = Wrapper.Create();
            StatusBarObject sentStatus = new StatusBarObject();
            CLogs.WriteInfo(ELogsWhat.ScanDoc, "Початок процедури розпізнавання документів");
            try
            {
                sentStatus.BigText = "Ініціалізація ABBYY FineReader Engine...";
                bgWorker.ReportProgress(0, sentStatus);
                if (flexiCapture.Initialized == false)
                {
                    //flexiCapture.SerialNumber = SettingsObject.Instance.SerialNumber; //"FEDC-8014-0000-4158-1516";
                    flexiCapture.SerialNumber = "FEDC-8014-0000-6669-7644"; //"FEDC-8014-0000-4158-1516";
                    flexiCapture.DllPath = SettingsObject.Instance.FlexiCaptureDll;   //"C:\\Program Files\\ABBYY FineReader Engine 8.1\\Bin\\FCEngine.dll";
                    flexiCapture.Initialize();
                }

                sentStatus.MaxVal = listDoc.Count;
                long tics = 0;
                for (int indexDoc = 0; indexDoc < listDoc.Count; indexDoc++)
                {
                    DateTime startTime = DateTime.Now;
                    if ((bgWorker.CancellationPending == true))
                    {
                        e.Cancel = true;
                        break;
                    }
                    DocumentData document = listDoc[indexDoc];
                    Enum.DocStatusEnum docState = document.Status;
                    try
                    {
                        document.SetStatusWithoutCheck(Enum.DocStatusEnum.Recognizing);
                        //---
                        CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Початок розпізнавання документа '{0}'", System.IO.Path.GetFileName(document.ImageFile)));
                        sentStatus.BigText = string.Format("Розпізнавання документа '{0}'", System.IO.Path.GetFileName(document.ImageFile));
                        sentStatus.CurVal = indexDoc;
                        sentStatus.SmallText = "";
                        if (tics > 0)
                        {
                            DateTime outDate = new DateTime(tics * (listDoc.Count - indexDoc));
                            string outString = "";
                            if (outDate.Hour > 0)
                                outString = outDate.ToString("hh год. mm хв.");
                            else if (outDate.Minute > 0)
                                outString = outDate.ToString("m хв.");
                            else
                                outString = outDate.ToString("< 1 хв.");
                            if (string.IsNullOrEmpty(outString) == false)
                                sentStatus.SmallText = string.Format("Залишилось: {0}  ", outString);
                        }
                        sentStatus.SmallText += string.Format("{0}/{1}", sentStatus.CurVal, sentStatus.MaxVal);
                        bgWorker.ReportProgress(0, sentStatus);
                        //---
                        flexiCapture.ClearImageFiles();
                        flexiCapture.AddImageFile(document.ImageFile);
                        SettingsDocRecognizer docRecognizer = null;
                        for (int index = 0; index < SettingsObject.Instance.SettingsDocument.Count; index++)
                        {
                            SettingsDocRecognizer docRecognizerTmp = SettingsObject.Instance.SettingsDocument[index];
                            if (docRecognizerTmp.DocType == document.Type)
                            {
                                docRecognizer = docRecognizerTmp;
                                break;
                            }
                        }

                        if (docRecognizer == null)
                            continue; // Заглушка

                        flexiCapture.ProjectFileName = docRecognizer.FlexiCaptureProject;
                            //"C:\\RecognizeAppl\\RecognizeAppl.fcproj";
                        flexiCapture.Recognize();
                        int count = flexiCapture.GetRecognizedBlockCount();
                        for (int i = 0; i < count; i++)
                        {
                            RecognizedTextBlock recognizedBlock = flexiCapture.GetRecognizedBlockByIndex(i);
                            string recognizedData = recognizedBlock.Text;
                            Rectangle bitmapRectangle = recognizedBlock.Bounds;
                            string fieldName = docRecognizer.GetFieldName(recognizedBlock.Name);
                            switch (fieldName)
                            {
                                case DocumentData.FieldNumber: //Номер документа
                                    if (string.IsNullOrEmpty(recognizedData))
                                        document.LastNumber = "";
                                    else
                                    {
                                        int startIndex = recognizedData.Length - 10;
                                        if (startIndex < 0)
                                            startIndex = 0;
                                        document.LastNumber = recognizedData.Substring(startIndex);
                                    }
                                    document.NumberBitmapRect = bitmapRectangle;
                                    break;
                                case DocumentData.FieldDateOut: //Дата выдачи
                                    {
                                        //DateTime dt = DateTime.MinValue;
                                        //if (DateTime.TryParse(recognizedData, out dt))
                                        //    document.DateOut = dt;
                                        document.DateOutBitmapRect = bitmapRectangle;
                                    }
                                    break;
                                case DocumentData.FieldBlankNumber: //Номер бланка
                                    //document.BlankNumber = recognizedData;
                                    document.NumberBlankBitmapRect = bitmapRectangle;
                                    break;
                            }
                        }
                        document.SetStatusWithoutCheck(Enum.DocStatusEnum.Recognized);
                        Cnt_SuccessRecognize++;
                        CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Документ '{0}' розпізнано успішно", System.IO.Path.GetFileName(document.ImageFile)));
                    }
                    catch(Exception ex)
                    {
                        //Возвращаем статус обратно
                        Cnt_FailRecognize++;
                        CLogs.WriteError(ELogsWhat.ScanDoc, string.Format("Документ '{0}' не розпізнано", System.IO.Path.GetFileName(document.ImageFile)));
                        document.SetStatusWithoutCheck(docState);
                        throw ex;
                    }
                    long allTick = (DateTime.Now - startTime).Ticks;
                    tics = (tics > 0) ? (long)(tics + allTick) / 2 : allTick;
                    CLogs.WriteInfo(ELogsWhat.ScanDoc, string.Format("Кінець процедури розпізнавання документа '{0}'", System.IO.Path.GetFileName(document.ImageFile)));
                }
            }
            finally
            {
                CLogs.WriteInfo(ELogsWhat.ScanDoc, "Кінець процедури розпізнавання документів");
                if (listDoc.Count > 0)
                {
                    MessageBox.Show(string.Format("Успішно розпізнаних документів - {0}. Документів, що не було розпізнано - {1}", Cnt_SuccessRecognize, listDoc.Count - Cnt_SuccessRecognize));
                }
                flexiCapture.Dispose();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
