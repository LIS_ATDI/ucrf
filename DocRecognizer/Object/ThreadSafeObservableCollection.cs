﻿using System.Windows.Threading;

namespace DocRecognizer.Object
{
    public class ThreadSafeObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T> where T : class
    {
        /// <summary>
        /// Диспатчер
        /// </summary>
        protected Dispatcher InternalDispatcher;
        protected delegate void EmptyMethodDelegate();
        protected delegate void InsertDelegate(int index, T item);
        protected delegate void RemoveIndexDelegate(int index);
        protected delegate void MoveDelegate(int oldIndex, int newIndex);
        protected delegate void SetItemDelegete(int index, T item);
        /// <summary>
        /// Конструктор
        /// </summary>
        public ThreadSafeObservableCollection()
            : base()
        {
            InternalDispatcher = Dispatcher.CurrentDispatcher;
        }

        protected override void InsertItem(int index, T item)
        {
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                base.InsertItem(index, item);
            }
            else
            {
                object[] param = { index, item };
                InternalDispatcher.Invoke(new InsertDelegate(base.InsertItem), param);
            }
        }

        protected override void RemoveItem(int index)
        {
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                base.RemoveItem(index);
            }
            else
            {
                object[] param = { index };
                InternalDispatcher.Invoke(new RemoveIndexDelegate(base.RemoveItem), param);
            }
        }

        protected override void ClearItems()
        {
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                base.ClearItems();
            }
            else
            {
                InternalDispatcher.Invoke(new EmptyMethodDelegate(base.ClearItems), null);
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                base.MoveItem(oldIndex, newIndex);
            }
            else
            {
                object[] param = { oldIndex, newIndex };
                InternalDispatcher.Invoke(new MoveDelegate(base.MoveItem), param);
            }
        }

        protected override void SetItem(int index, T item)
        {
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                base.SetItem(index, item);
            }
            else
            {
                object[] param = { index, item };
                InternalDispatcher.Invoke(new SetItemDelegete(base.SetItem), param);
            }
        }
    }
}
