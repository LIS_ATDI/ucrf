﻿using System.Windows;
using DocRecognizer.Interface;

namespace DocRecognizer.Object
{
    internal class RecognizeWindow : IRecognizeWindow
    {
        private Form.RecognizeForm _form;

        public RecognizeWindow()
        {
            _form = new Form.RecognizeForm();
        }
        #region Implementation of IRecognizeWindow

        public Window GetWindow()
        {
            return _form;
        }

        public void ShowDialog()
        {
            if (_form != null)
                _form.ShowDialog();
        }

        #endregion
    }
}
