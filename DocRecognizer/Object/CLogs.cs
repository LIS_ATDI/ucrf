﻿#if DEBUG
//#define SHOW_MESSAGE_LOGS
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using System.IO;

namespace DocRecognizer.Object
{
   //============================================
   /// <summary>
   /// Тип сообщения
   /// </summary>
   public enum ELogsType
   {
      Info = (1<<0),
      Warning = (1<<1),
      Error = (1<<2)
   }
   //============================================
   /// <summary>
   /// Кто создал сообщение
   /// </summary>
   public enum ELogsWhat
   {
      Unknown = 0,
      Monitoring = 1,
      Delete = 2,
      Appl = 3,
      AutoXml = 4,
      Critical = 5,  //Критическая ошибка программы
      Data = 7,      //Данные
      AutoSetArticle = 8,//Данные
      OpenForm = 9,  //Открытие формы
      AutoAction = 10,  //Автоматические деййствия
      Utdb = 11,  //UTDB
      ScanDoc = 12
   }

   public class CLogs
   {
       static public int GetCurUserID()
       {
           int retVal = IM.NullI;
           string userName = IM.ConnectedUser();
           using (IMRecordset rs = new IMRecordset(ICSMTbl.itblEmployee, IMRecordset.Mode.ReadOnly))
           {
               rs.Select("ID");
               rs.SetWhere("APP_USER", IMRecordset.Operation.Like, userName);
               rs.Open();
               if (!rs.IsEOF())
                   retVal = rs.GetI("ID");
           }
           return retVal;
       }
      //===================================================
      /// <summary>
      /// Writes a message into the log
      /// </summary>
      /// <param name="type">A type of message</param>
      /// <param name="who">Who write the message</param>
      /// <param name="message">A message</param>
      public static void WriteLog(ELogsType type, ELogsWhat what, string message)
      {
         IMRecordset rs = new IMRecordset(PlugTbl.LOGS, IMRecordset.Mode.ReadWrite);
         rs.Select("ID,TYPE,WHAT,MESSAGE,DATE_CREATED,CREATED_BY");
         rs.SetWhere("ID", IMRecordset.Operation.Eq, -1);
         try
         {
            rs.Open();
            rs.AddNew();
            rs.Put("ID", IM.AllocID(PlugTbl.LOGS, 1, -1));
            rs.Put("TYPE", type.ToString());
            if (what != ELogsWhat.Unknown)
               rs.Put("WHAT", what.ToString());
            rs.Put("MESSAGE", message);
            rs.Put("DATE_CREATED", DateTime.Now);
            rs.Put("CREATED_BY", GetCurUserID());
            rs.Update();
         }
         finally
         {
             if (rs.IsOpen())
                 rs.Close();
            rs.Destroy();
         }
#if SHOW_MESSAGE_LOGS
         System.Windows.Forms.MessageBox.Show(message, "Log:" + what.ToString(), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
#endif
      }
      //===================================================
      /// <summary>
      /// Writes an error message from Exception
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="ex">Exception</param>
      public static void WriteError(ELogsWhat who, Exception ex)
      {
          WriteError(who, ex, true);
      }
      //===================================================
      /// <summary>
      /// Writes an error message from Exception
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="ex">Exception</param>
      /// <param name="isSilent">Тихий режим</param>
      public static void WriteError(ELogsWhat who, Exception ex, bool isSilent)
      {
         string message = string.Format("{0} Source:{1} TargetSite:{2} StackTrace:{3}", ex.Message, ex.Source, ex.TargetSite, ex.StackTrace);
         WriteLog(ELogsType.Error, who, message);
         if (!isSilent)
             System.Windows.Forms.MessageBox.Show(message, "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
      }
      //===================================================
      /// <summary>
      /// Writes an error message
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="message">A message</param>
      public static void WriteError(ELogsWhat who, string message)
      {
         WriteError(who, message, true);
      }
      //===================================================
      /// <summary>
      /// Writes an error message
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="message">A message</param>
      /// <param name="isSilent">TRUE - тихий режим</param>
      public static void WriteError(ELogsWhat who, string message, bool isSilent)
      {
          WriteLog(ELogsType.Error, who, message);
          if (!isSilent)
              System.Windows.Forms.MessageBox.Show(message, "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
      }
      //===================================================
      /// <summary>
      /// Writes a warning message
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="message">A message</param>
      public static void WriteWarning(ELogsWhat who, string message)
      {
         WriteLog(ELogsType.Warning, who, message);
      }
      //===================================================
      /// <summary>
      /// Writes an info message
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="message">A message</param>
      public static void WriteInfo(ELogsWhat who, string message)
      {
         WriteInfo(who, message, true);
      }

      //===================================================
      /// <summary>
      /// Writes LOG in TXT files
      /// </summary>
      /// <param name="message">A message</param>
      public static void WriteLogTxt(string message)
      {
          
          using (StreamWriter  fs = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "log.txt",true))
          {
              fs.WriteLine(message);
          }

          
      }
      //===================================================
      /// <summary>
      /// Writes an info message
      /// </summary>
      /// <param name="who">Who writes the message</param>
      /// <param name="message">A message</param>
      /// <param name="isSilent">TRUE - Тихий режим</param>
      public static void WriteInfo(ELogsWhat who, string message, bool isSilent)
      {
          WriteLog(ELogsType.Info, who, message);
          if(!isSilent)
              System.Windows.Forms.MessageBox.Show(message, "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
      }
           
   }
}
