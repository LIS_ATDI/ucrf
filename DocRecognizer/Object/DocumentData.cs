﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DocRecognizer.Enum;
using DocRecognizer.Interface;

namespace DocRecognizer.Object
{
    /// <summary>
    /// Описывает все данные документа
    /// </summary>
    internal partial class DocumentData : NotifyPropertyChanged
    {
        private bool _isChecked = false;
        private string _lastNumber = "";
        private string _number = "";
        private string _imageFile = "";
        private DateTime _dateOut = DateTime.MinValue;
        private DateTime _dateEnd = DateTime.MinValue;
        private string _blankNumber = "";
        private DateTime _scanDate = DateTime.Now;
        private string _type = "";
        private DocStatusEnum _status = DocStatusEnum.NotRecognized;
        private Rectangle _numberBitmapRect;
        private Rectangle _numberBlankBitmapRect;
        private Rectangle _dateOutBitmapRect;
        /// <summary>
        /// Record ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Флаг Ошибки в строке. Устанавливаеться если данные в не корректны
        /// </summary>
        private bool _isError = false;
        public bool IsError
        {
            get { return GetThreadSafeValue(ref _isError); }
            set { SetThreadSafeValue(ref _isError, value, "IsError");}
        }
        public const string FieldIsChecked = "IsChecked";
        /// <summary>
        /// Флаг выбора документа
        /// </summary>
        public bool IsChecked
        {
            get { return GetThreadSafeValue(ref _isChecked); }
            set { SetThreadSafeValue(ref _isChecked, value, FieldIsChecked);}
        }
        public const string FieldLastNumber = "LastNumber";
        /// <summary>
        /// Номер документа
        /// </summary>
        public string LastNumber
        {
            get { return GetThreadSafeValue(ref _lastNumber); }
            set
            {
                string lastVal = LastNumber;
                SetThreadSafeValue(ref _lastNumber, value, FieldLastNumber);
                if ((lastVal != LastNumber) &&
                    ((Status == DocStatusEnum.Found) || (Status == DocStatusEnum.DuplicateNumber)))
                {
                    Status = DocStatusEnum.NotFound;
                    Number = "";
                    BlankNumber = "";
                    DateOut = DateTime.MinValue;
                    IsChecked = false;
                }
            }
        }
        /// <summary>
        /// Название поля "Number"
        /// </summary>
        public const string FieldNumber = "Number";
        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number
        {
            get { return GetThreadSafeValue(ref _number); }
            set { SetThreadSafeValue(ref _number, value, FieldNumber);}
        }
        /// <summary>
        /// Путь к скан файлу
        /// </summary>
        public string ImageFile
        {
            get { return GetThreadSafeValue(ref _imageFile); }
            set { SetThreadSafeValue(ref _imageFile, value, "ImageFile");}
        }
        /// <summary>
        /// Название поля "DateOut"
        /// </summary>
        public const string FieldDateOut = "DateOut";
        /// <summary>
        /// Дата документа
        /// </summary>
        public DateTime DateOut
        {
            get { return GetThreadSafeValue(ref _dateOut); }
            set { SetThreadSafeValue(ref _dateOut, value, FieldDateOut);}
        }
        /// <summary>
        /// Название поля "DateEnd"
        /// </summary>
        public const string FieldDateEnd = "DateEnd";
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime DateEnd
        {
            get { return GetThreadSafeValue(ref _dateEnd); }
            set { SetThreadSafeValue(ref _dateEnd, value, FieldDateEnd); }
        }
        /// <summary>
        /// Название поля "BlankNumber"
        /// </summary>
        public const string FieldBlankNumber = "BlankNumber";
        /// <summary>
        /// Номер бланка
        /// </summary>
        public string BlankNumber
        {
            get { return GetThreadSafeValue(ref _blankNumber); }
            set { SetThreadSafeValue(ref _blankNumber, value, FieldBlankNumber);}
        }
        /// <summary>
        /// Дата сканирования
        /// </summary>
        public DateTime ScanDate
        {
            get { return GetThreadSafeValue(ref _scanDate); }
            set { SetThreadSafeValue(ref _scanDate, value, "ScanDate");}
        }
        /// <summary>
        /// Тип документа
        /// </summary>
        public string Type
        {
            get { return GetThreadSafeValue(ref _type); }
            set { SetThreadSafeValue(ref _type, value, "Type");}
        }
        /// <summary>
        /// Название поля "Status"
        /// </summary>
        public const string FieldStatus = "Status";
        /// <summary>
        /// Статус документа
        /// </summary>
        public DocStatusEnum Status
        {
            get { return GetThreadSafeValue(ref _status); }
            set { SetThreadSafeValue(ref _status, value, FieldStatus);}
        }

        public Rectangle NumberBitmapRect
        {
            get { return GetThreadSafeValue(ref _numberBitmapRect); }
            set { SetThreadSafeValue(ref _numberBitmapRect, value, "NumberBitmapRect"); }
        }

        public Rectangle NumberBlankBitmapRect
        {
            get { return GetThreadSafeValue(ref _numberBlankBitmapRect); }
            set { SetThreadSafeValue(ref _numberBlankBitmapRect, value, "NumberBlankBitmapRect"); }
        }

        public Rectangle DateOutBitmapRect
        {
            get { return GetThreadSafeValue(ref _dateOutBitmapRect); }
            set { SetThreadSafeValue(ref _dateOutBitmapRect, value, "DateOutBitmapRect"); }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DocumentData()
        {
            Id = 0;
        }
        /// <summary>
        /// Изменить Статус записи без проверки корректности
        /// </summary>
        /// <param name="status">Новый статус</param>
        public void SetStatusWithoutCheck(DocStatusEnum status)
        {
            if (_status != status)
            {
                _status = status;
                InvokeNotifyPropertyChanged(FieldStatus);
            }
        }
        /// <summary>
        /// Копирование данных для возможности возврата
        /// </summary>
        public void CloneFrom(DocumentData data)
        {
            IsChecked = data.IsChecked;
            Number = data.Number;
            DateOut = data.DateOut;
            BlankNumber = data.BlankNumber;
            LastNumber = data.LastNumber;
        }
        /// <summary>
        /// Проверка возможности выбора данной записи
        /// </summary>
        /// <returns>TRUE - можна выберать, иначе FALSE</returns>
        public bool CanCheck()
        {
            bool retVal = false;
            if(string.IsNullOrEmpty(BlankNumber) == false)
                retVal = CanCheckByStatus();
            return retVal;
        }
        /// <summary>
        /// Проверка возможности выбора данной записи по статусу
        /// </summary>
        /// <returns>TRUE - можна выберать, иначе FALSE</returns>
        private bool CanCheckByStatus()
        {
            bool retVal = false;
            switch (Status)
            {
                case DocStatusEnum.Found:
                case DocStatusEnum.ErrorTransfer:
                    retVal = true;
                    break;
                default:
                    retVal = false;
                    break;
            }
            return retVal;
        }
    }

    internal partial class DocumentData : IDataErrorInfo
    {
        public string Error
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                string error = this[FieldIsChecked];
                if (error != string.Empty)
                    sb.AppendLine(error);
                error = this[FieldBlankNumber];
                if (error != string.Empty)
                    sb.AppendLine(error);
                error = this[FieldLastNumber];
                if (error != string.Empty)
                    sb.AppendLine(error);
                //-----
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    IsError = true;
                    return sb.ToString();
                }
                IsError = false;
                return "";
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case FieldIsChecked:
                        if ((IsChecked == true) && (CanCheckByStatus() == false))
                            return "Статус документа не коректний";
                        break;
                    case FieldLastNumber:
                        if ((_tempDocumentData != null) && (_tempDocumentData.LastNumber != LastNumber) &&
                            (Status == DocStatusEnum.Found))
                            Status = DocStatusEnum.NotFound;
                        break;
                }
                return "";
            }
        }
    }

    internal partial class DocumentData : IEditableObject
    {
        bool _inEdit = false;
        DocumentData _tempDocumentData = null;
        /// <summary>
        /// Начало редактирования
        /// </summary>
        public void BeginEdit()
        {
            if (_inEdit)
                return;
            _inEdit = true;
            _tempDocumentData = new DocumentData();
            _tempDocumentData.CloneFrom(this);
        }
        /// <summary>
        /// Отмена редактирования
        /// </summary>
        public void CancelEdit()
        {
            if (!_inEdit)
                return;
            _inEdit = false;
            this.CloneFrom(_tempDocumentData);
            _tempDocumentData = null;
        }
        /// <summary>
        /// Редактирование закончено
        /// </summary>
        public void EndEdit()
        {
            if (!_inEdit)
                return;
            if ((_tempDocumentData.Number != Number) && ((Status == DocStatusEnum.Found) || (Status == DocStatusEnum.DuplicateNumber)))
            {
                Status = DocStatusEnum.NotFound;
                IsChecked = false;
            }
            _inEdit = false;
            _tempDocumentData = null;
        }
    }



    class ColumnDocumentDataValidation : ValidationRule
    {
        private ListDocumentDataHelper _listPerson;

        //Данное свойство является вспомогательным, служит для реализации DependencyProperty
        public ListDocumentDataHelper ListPerson
        {
            get { return _listPerson; }
            set { _listPerson = value; }
        }


        //Проверка на валидность данных
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            BindingGroup bg = value as BindingGroup;
            if(bg == null)
                return ValidationResult.ValidResult;
            if(bg.Items == null)
                return ValidationResult.ValidResult;
            if (bg.Items.Count == 0)
                return ValidationResult.ValidResult;
            DocumentData person = bg.Items[0] as DocumentData;
            if(person == null)
                return ValidationResult.ValidResult;
            //
            //var listNumber = from p in ListPerson.Items
            //             where p != person && (p.Number == person.Number)
            //             select p;
            //if (listNumber.Count() > 0)
            //    return new ValidationResult(false, "Document number duplicate");
            ////
            //var listBlankNumber = from p in ListPerson.Items
            //             where p != person && (p.BlankNumber == person.BlankNumber)
            //             select p;
            //if (listBlankNumber.Count() > 0)
            //    return new ValidationResult(false, "Blank number duplicate");

            return ValidationResult.ValidResult;
        }
    }

    //Вспомогательный класс для реализации DependencyProperty
    internal class ListDocumentDataHelper : DependencyObject
    {
        //На свойство Items можно ссылаться в XAML, так как оно объявленно как DependencyProperty
        public DocumentsList Items
        {
            get { return (DocumentsList)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register
            ("Items",
            typeof(DocumentsList),
            typeof(ListDocumentDataHelper),
            new UIPropertyMetadata(null));
    }

}
