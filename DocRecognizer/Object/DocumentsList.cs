﻿using System;
using System.Data;
using System.Windows.Threading;
using DocRecognizer.DataBase;
using DocRecognizer.Enum;

namespace DocRecognizer.Object
{
    internal class DocumentsList : ThreadSafeObservableCollection<DocumentData>
    {
        /// <summary>
        /// List of deleted data
        /// </summary>
        private ThreadSafeObservableCollection<DocumentData> _deletedData = new ThreadSafeObservableCollection<DocumentData>();
        /// <summary>
        /// Is changed
        /// </summary>
        private bool _isChanged = false;
        /// <summary>
        /// Remove item
        /// </summary>
        /// <param name="index">Item index</param>
        protected override void RemoveItem(int index)
        {
            DocumentData tmpDoc = this[index];
            base.RemoveItem(index);
            _deletedData.Add(tmpDoc);
            _isChanged = true;
        }
        /// <summary>
        /// Insert item
        /// </summary>
        /// <param name="index">index</param>
        /// <param name="item">item</param>
        protected override void InsertItem(int index, DocumentData item)
        {
            base.InsertItem(index, item);
            _isChanged = true;
        }
        /// <summary>
        /// Проверка корректности данных в гриде
        /// </summary>
        public bool IsErrorRow
        {
            get
            {
                bool retVal = false;
                for (int index = 0; index < this.Count; index++)
                {
                    if (this[index].IsError)
                    {
                        retVal = true;
                        break;
                    }
                }
                return retVal;
            }
        }
        /// <summary>
        /// Is changed
        /// </summary>
        public bool IsChanged
        {
            get
            {
                bool retVal = false;
                for (int index = 0; index < this.Count; index++)
                {
                    if (this[index].IsChanged)
                    {
                        retVal = true;
                        break;
                    }
                }
                return retVal | _isChanged;
            }
        }
        /// <summary>
        /// Сохранить в БД
        /// </summary>
        public bool Save()
        {
            bool retVal = false;
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                retVal = InternalSave();
            }
            else
            {
                object rez = InternalDispatcher.Invoke(new Func<bool>(InternalSave));
                if (rez is bool)
                    retVal = (bool) rez;
            }
            return retVal;
        }
        /// <summary>
        /// Загрузить из БД
        /// </summary>
        public bool Load()
        {
            bool retVal = false;
            if (InternalDispatcher == Dispatcher.CurrentDispatcher)
            {
                retVal = InternalLoad();
            }
            else
            {
                object rez = InternalDispatcher.Invoke(new Func<bool>(InternalLoad));
                if (rez is bool)
                    retVal = (bool)rez;
            }
            return retVal;
        }
        /// <summary>
        /// Сохраняет данные о документах
        /// </summary>
        protected bool InternalSave()
        {
            bool retVal = true;
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();
                    using (IDbTransaction dbTrans = connect.BeginTransaction())
                    {
                        using (IDbCommand cmd = connect.CreateCommand())
                        {
                            while(_deletedData.Count > 0)
                            {
                                var data = _deletedData[0];
                                cmd.CommandText = string.Format(@"DELETE FROM {0} WHERE ID={1}", DbTableName.Tdocument, data.Id);
                                cmd.ExecuteNonQuery();
                                data.Id = 0;
                                _deletedData.RemoveAt(0);
                                bool canDelete = true;
                                foreach (var document in this)
                                {
                                    if(document.ImageFile == data.ImageFile)
                                    {
                                        canDelete = false;
                                        break;
                                    }
                                }
                                if (canDelete && System.IO.File.Exists(data.ImageFile))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(data.ImageFile);
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Debug.WriteLine(ex.Message);
                                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                                    }
                                }
                            }
                            foreach (var document in this)
                            {
                                if (document.Id > 0)
                                {
                                    if (document.IsChanged == false)
                                        continue;
                                    cmd.CommandText =
                                        string.Format(
                                            @"UPDATE {0} SET IS_CHECKED='{2}',DOC_NUMBER='{3}',BLANK_NUMBER='{4}',DATE_OUT='{5}',FILE_IMAGE='{6}',DATE_SCAN='{7}',DOC_TYPE='{8}',STATUS='{9}',DOC_RECT='{10}',BLANK_RECT='{11}',DATE_RECT='{12}',LAST_NUMBER='{13}'
                                            WHERE ID={1}",
                                            DbTableName.Tdocument, document.Id, document.IsChecked, document.Number.ToDbString(),
                                            document.BlankNumber.ToDbString(), document.DateOut.ToDbDateTime(),
                                            document.ImageFile, document.ScanDate.ToDbDateTime(), document.Type,
                                            document.Status.ToDocStatusString(), document.NumberBitmapRect.ToDbString(),
                                            document.NumberBlankBitmapRect.ToDbString(), document.DateOutBitmapRect.ToDbString(),
                                            document.LastNumber);
                                }
                                else
                                {
                                    cmd.CommandText =
                                        string.Format(
                                            @"INSERT INTO {0} (IS_CHECKED,DOC_NUMBER,BLANK_NUMBER,DATE_OUT,FILE_IMAGE,DATE_SCAN,DOC_TYPE,STATUS,DOC_RECT,BLANK_RECT,DATE_RECT,LAST_NUMBER)
                                            VALUES ('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}')",
                                            DbTableName.Tdocument, document.IsChecked, document.Number.ToDbString(),
                                            document.BlankNumber.ToDbString(), document.DateOut.ToDbDateTime(),
                                            document.ImageFile, document.ScanDate.ToDbDateTime(), document.Type,
                                            document.Status.ToDocStatusString(), document.NumberBitmapRect.ToDbString(),
                                            document.NumberBlankBitmapRect.ToDbString(), document.DateOutBitmapRect.ToDbString(),
                                            document.LastNumber);
                                }
                                cmd.ExecuteNonQuery();
                                if (document.Id <= 0)
                                    document.Id = connect.GelLastInsertedId();
                            }
                        }
                        dbTrans.Commit();
                    }
                    foreach (var document in this)
                        document.IsChanged = false;
                    _isChanged = false;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    retVal = false;
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                        connect.Close();
                }
            }
            return retVal;
        }
        /// <summary>
        /// Загружает данные о документах
        /// </summary>
        protected bool InternalLoad()
        {
            DocumentsList obj = new DocumentsList();
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();

                    using (IDbCommand cmd = connect.CreateCommand())
                    {
                        cmd.CommandText =
                            string.Format(
                                @"SELECT ID,IS_CHECKED,DOC_NUMBER,BLANK_NUMBER,DATE_OUT,FILE_IMAGE,DATE_SCAN,DOC_TYPE,STATUS,DOC_RECT,BLANK_RECT,DATE_RECT,LAST_NUMBER FROM {0}",
                                DbTableName.Tdocument);
                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj.Add(new DocumentData
                                            {
                                                Id = reader.GetInt32(0),
                                                IsChecked = reader.ToBool(1),
                                                Number = reader.GetString(2),
                                                BlankNumber = reader.GetString(3),
                                                DateOut = reader.ToDateTime(4),
                                                ImageFile = reader.GetString(5),
                                                ScanDate = reader.ToDateTime(6),
                                                Type = reader.GetString(7),
                                                Status = reader.GetString(8).ToDocStatusEnum(),
                                                NumberBitmapRect = reader.GetString(9).ToDbRectangle(),
                                                NumberBlankBitmapRect = reader.GetString(10).ToDbRectangle(),
                                                DateOutBitmapRect = reader.GetString(11).ToDbRectangle(),
                                                LastNumber = reader.GetValue(12).ToString(),
                                            });
                            }
                            reader.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    obj = null;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
            if (obj != null)
            {
                this.Clear();
                foreach (DocumentData documentData in obj)
                    this.Add(documentData);
                _isChanged = false;
            }
            return true;
        }
    }
}
