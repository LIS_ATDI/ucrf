﻿using System;
using System.Data;
using DocRecognizer.DataBase;
using DocRecognizer.Interface;

namespace DocRecognizer.Object
{
    internal sealed class SettingsObject : NotifyPropertyChanged
    {
        public static SettingsObject Instance = new SettingsObject();
        private string _flexiCaptureDll;
        private string _serialNumber;

        /// <summary>
        /// Путь к dll Flexi Capture
        /// </summary>
        public string FlexiCaptureDll
        {
            get { return _flexiCaptureDll; }
            set
            {
                if (_flexiCaptureDll != value)
                {
                    _flexiCaptureDll = value;
                    InvokeNotifyPropertyChanged("FlexiCaptureDll");
                }
            }
        }
        /// <summary>
        /// Серийный номер
        /// </summary>
        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                if (_serialNumber != value)
                {
                    _serialNumber = value;
                    InvokeNotifyPropertyChanged("SerialNumber");
                }
            }
        }
        /// <summary>
        /// Настройки по каждому документу
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<SettingsDocRecognizer> SettingsDocument { get; private set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        private SettingsObject()
        {
            FlexiCaptureDll = "";
            SerialNumber = "";
            SettingsDocument = new System.Collections.ObjectModel.ObservableCollection<SettingsDocRecognizer>();
        }

        #region Static Function
        /// <summary>
        /// Перезагрузить настроеки
        /// </summary>
        public static void Reload()
        {
            Instance = Load();
        }
        /// <summary>
        /// Загрузка настроек
        /// </summary>
        /// <returns>Обьект настроек</returns>
        public static SettingsObject Load()
        {
            SettingsObject obj = new SettingsObject();
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();
                    
                        using (IDbCommand cmd = connect.CreateCommand())
                        {
                            cmd.CommandText = string.Format(@"SELECT FC_DLL,FC_SN FROM {0}", DbTableName.Tsetting);
                            using (IDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    obj.FlexiCaptureDll = reader.GetString(0);
                                    obj.SerialNumber = reader.GetString(1);
                                }
                                reader.Close();
                            }
                            //-------
                            cmd.CommandText = string.Format(@"SELECT ID,DOC_TYPE,DOC_TYPE_DESCR,FC_PROJECT,NAME_DOCNUMBER,NAME_BLANKNUMBER,NAME_DATEOUT FROM {0}", DbTableName.TsettingRecog);
                            using (IDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    obj.SettingsDocument.Add(new SettingsDocRecognizer
                                                                 {
                                                                     Id = reader.GetInt32(0),
                                                                     DocType = reader.GetString(1),
                                                                     DocTypeDescription = reader.GetString(2),
                                                                     FlexiCaptureProject = reader.GetString(3),
                                                                     NameBlockNumberDoc = reader.GetString(4),
                                                                     NameBlockNumberBlank = reader.GetString(5),
                                                                     NameBlockDateOut = reader.GetString(6)
                                                                 });
                                }
                                reader.Close();
                            }
                        }
                    
                }
                catch (Exception ex)
                {
                    obj = null;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
            if (obj == null)
                obj = new SettingsObject();
            obj.IsChanged = false;
            return obj;
        }
        /// <summary>
        /// Сохранение настроек
        /// </summary>
        /// <param name="obj">SettingsObject</param>
        public static void Save(SettingsObject obj)
        {
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (IDbConnection connect = DbSqLite.GetConnection())
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();
                    using (IDbTransaction dbTrans = connect.BeginTransaction())
                    {
                        using (IDbCommand cmd = connect.CreateCommand())
                        {
                            cmd.CommandText = string.Format(@"DELETE FROM {0}", DbTableName.Tsetting);
                            cmd.ExecuteNonQuery();
                            cmd.CommandText = string.Format(@"DELETE FROM {0}", DbTableName.TsettingRecog);
                            cmd.ExecuteNonQuery();
                            cmd.CommandText = string.Format(@"INSERT INTO {0} (FC_DLL,FC_SN) VALUES ('{1}','{2}')",
                                        DbTableName.Tsetting, obj.FlexiCaptureDll, obj.SerialNumber);
                            cmd.ExecuteNonQuery();
                            foreach (var setting in obj.SettingsDocument)
                            {
                                cmd.CommandText =
                                    string.Format(
                                        @"INSERT INTO {0} (DOC_TYPE,DOC_TYPE_DESCR,FC_PROJECT,NAME_DOCNUMBER,NAME_BLANKNUMBER,NAME_DATEOUT) VALUES ('{1}','{2}','{3}','{4}','{5}','{6}')",
                                        DbTableName.TsettingRecog, setting.DocType, setting.DocTypeDescription,
                                        setting.FlexiCaptureProject, setting.NameBlockNumberDoc,
                                        setting.NameBlockNumberBlank, setting.NameBlockDateOut);
                                cmd.ExecuteNonQuery();
                            }
                        }
                        dbTrans.Commit();
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
        }
        #endregion
    }
    /// <summary>
    /// Настроки роспознавания документа
    /// </summary>
    internal class SettingsDocRecognizer : NotifyPropertyChanged
    {
        private string _docType = "";
        private string _docTypeDescription = "";
        private string _flexiCaptureProject = "";
        private string _nameBlockDateOut = "";
        private string _nameBlockNumberDoc = "";
        private string _nameBlockNumberBlank = "";
        /// <summary>
        /// ID записи
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Тип документа
        /// </summary>
        public string DocType
        {
            get { return _docType; }
            set
            {
                if (_docType != value)
                {
                    _docType = value;
                    InvokeNotifyPropertyChanged("DocType");
                }
            }
        }
        /// <summary>
        /// Описание тип документа
        /// </summary>
        public string DocTypeDescription
        {
            get { return _docTypeDescription; }
            set
            {
                if (_docTypeDescription != value)
                {
                    _docTypeDescription = value;
                    InvokeNotifyPropertyChanged("DocTypeDescription");
                }
            }
        }
        /// <summary>
        /// Путь к проекту для распознования документа
        /// </summary>
        public string FlexiCaptureProject
        {
            get { return _flexiCaptureProject; }
            set
            {
                if (_flexiCaptureProject != value)
                {
                    _flexiCaptureProject = value;
                    InvokeNotifyPropertyChanged("FlexiCaptureProject");
                }
            }
        }
        /// <summary>
        /// Название блока "Номер документа"
        /// </summary>
        public string NameBlockNumberDoc
        {
            get { return _nameBlockNumberDoc; }
            set
            {
                if (_nameBlockNumberDoc != value)
                {
                    _nameBlockNumberDoc = value;
                    InvokeNotifyPropertyChanged("NameBlockNumberDoc");
                }
            }
        }
        /// <summary>
        /// Название блока "Номер бланка"
        /// </summary>
        public string NameBlockNumberBlank
        {
            get { return _nameBlockNumberBlank; }
            set
            {
                if (_nameBlockNumberBlank != value)
                {
                    _nameBlockNumberBlank = value;
                    InvokeNotifyPropertyChanged("NameBlockNumberBlank");
                }
            }
        }
        /// <summary>
        /// Название блока "Дата выдачи"
        /// </summary>
        public string NameBlockDateOut
        {
            get { return _nameBlockDateOut; }
            set
            {
                if (_nameBlockDateOut != value)
                {
                    _nameBlockDateOut = value;
                    InvokeNotifyPropertyChanged("NameBlockDateOut");
                }
            }
        }
        /// <summary>
        /// Преобразовывает поля FlexiCapture в поля программы
        /// </summary>
        /// <param name="fcField">Название поля в проекте FC</param>
        /// <returns>Название поля в программе</returns>
        public string GetFieldName(string fcField)
        {
            if(string.IsNullOrEmpty(fcField))
                return "";
            string retVal = "";
            if(fcField == NameBlockDateOut)
                retVal = DocumentData.FieldDateOut;
            else if(fcField == NameBlockNumberBlank)
                retVal = DocumentData.FieldBlankNumber;
            else if(fcField == NameBlockNumberDoc)
                retVal = DocumentData.FieldNumber;
            return retVal;
        }
    }
}
