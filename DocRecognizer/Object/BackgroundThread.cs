﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using DocRecognizer.Form;

namespace DocRecognizer.Object
{
    internal class BackgroundThread
    {
        public static BackgroundThread BackgroundThreadObject = new BackgroundThread();
        private BackgroundWorker _threadWorker = new BackgroundWorker();
        private StatusBarObject _threadStatusBar = null;

        public bool IsBackgroundThreadBuzy { get { return _threadWorker.IsBusy; } }
        /// <summary>
        /// Конструктор
        /// </summary>
        private BackgroundThread()
        {
            _threadWorker.WorkerSupportsCancellation = true;
            _threadWorker.WorkerReportsProgress = true;
            _threadWorker.WorkerSupportsCancellation = true;
            _threadWorker.DoWork += new DoWorkEventHandler(BwDoWork);
            _threadWorker.ProgressChanged += new ProgressChangedEventHandler(BwProgressChanged);
            _threadWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BwRunWorkerCompleted);
        }
        /// <summary>
        /// Запустить задачи на выполнение
        /// </summary>
        /// <param name="parameters">Список задач</param>
        public void Run(params BackgroundThreadParameter[] parameters)
        {
            BackgroundThreadParameters tmpParams = new BackgroundThreadParameters();
            tmpParams.AddRange(parameters);
            Run(tmpParams);
        }
        /// <summary>
        /// Запустить задачи на выполнение
        /// </summary>
        /// <param name="parameters">Список задач</param>
        public void Run(BackgroundThreadParameters parameters)
        {
            if (IsBackgroundThreadBuzy)
                throw new Exception("BackgroundThread is buzy");
            _threadStatusBar = RecognizeForm.StatusBarElem.Push(true);
            _threadStatusBar.СanCancel = true;
            _threadWorker.RunWorkerAsync(parameters);
        }

        #region BackgroundWorker
        /// <summary>
        /// Распознование закончено
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RecognizeForm.StatusBarElem.Pop(_threadStatusBar);
            if (e.Error != null)
                RecognizeForm.StatusBarElem.SetBigText(e.Error.Message);
            else if (e.Cancelled)
                RecognizeForm.StatusBarElem.SetBigText("Команду відмінено");
            else
                RecognizeForm.StatusBarElem.SetBigText("Команда виконана");
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Изменение статуса распознования
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            StatusBarObject tmpVal = e.UserState as StatusBarObject;
            if (tmpVal != null)
            {
                _threadStatusBar.BigText = tmpVal.BigText;
                _threadStatusBar.SmallText = tmpVal.SmallText;
                _threadStatusBar.CurVal = tmpVal.CurVal;
                _threadStatusBar.MaxVal = tmpVal.MaxVal;
            }
        }
        /// <summary>
        /// отменить операцию
        /// </summary>
        public void BwCancel()
        {
            if ((_threadStatusBar != null) && _threadStatusBar.СanCancel && IsBackgroundThreadBuzy)
            {
                _threadWorker.CancelAsync();
                _threadStatusBar.SmallText = "Зачекайте, виконується відміна... ";
                _threadStatusBar.СanCancel = false;
            }
        }
        /// <summary>
        /// Функция распознования (паралельный поток)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BwDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker == null)
                throw new NullReferenceException("BackgroundWorker pointer");

            BackgroundThreadParameters parameters = e.Argument as BackgroundThreadParameters;
            if (parameters == null)
                throw new NullReferenceException("BackgroundThreadParameters pointer");

            //Обработка каманды Добавить картинки
            foreach (BackgroundThreadParameter parameter in parameters)
            {
                if (parameter.CommandType == CommandParallelEnum.AddFiles)
                {
                    if (parameter.ExecuteCallBack != null)
                        parameter.ExecuteCallBack(worker, e, parameter.Data);
                    parameter.IsExecuted = true;
                    if (parameter.FinishCallback != null)
                        parameter.FinishCallback(parameter.CommandType, parameters);
                }
            }
            //Обработка каманды recognize
            foreach (BackgroundThreadParameter parameter in parameters)
            {
                if (parameter.CommandType == CommandParallelEnum.Recognize)
                {
                    if (parameter.ExecuteCallBack != null)
                        parameter.ExecuteCallBack(worker, e, parameter.Data);
                    parameter.IsExecuted = true;
                    if (parameter.FinishCallback != null)
                        parameter.FinishCallback(parameter.CommandType, parameters);
                }
            }
            //Обработка каманды поиска документов
            foreach (BackgroundThreadParameter parameter in parameters)
            {
                if (parameter.CommandType == CommandParallelEnum.FindDocument)
                {
                    if (parameter.ExecuteCallBack != null)
                        parameter.ExecuteCallBack(worker, e, parameter.Data);
                    parameter.IsExecuted = true;
                    if (parameter.FinishCallback != null)
                        parameter.FinishCallback(parameter.CommandType, parameters);
                }
            }
            //Обработка каманды поиска документов
            foreach (BackgroundThreadParameter parameter in parameters)
            {
                if (parameter.CommandType == CommandParallelEnum.CommitScan)
                {
                    if (parameter.ExecuteCallBack != null)
                        parameter.ExecuteCallBack(worker, e, parameter.Data);
                    parameter.IsExecuted = true;
                    if (parameter.FinishCallback != null)
                        parameter.FinishCallback(parameter.CommandType, parameters);
                }
            }
        }

        #endregion
        /// <summary>
        /// Перечисление команд паралельного потока
        /// </summary>
        internal enum CommandParallelEnum
        {
            Recognize,
            FindDocument,
            CommitScan,
            AddFiles,
        }
        /// <summary>
        /// Параметры для паралельного потока
        /// </summary>
        internal class BackgroundThreadParameter
        {
            public delegate void ExecuteCallbackDelegate(BackgroundWorker bgWorker, DoWorkEventArgs e, object data);
            /// <summary>
            /// Функция завершения выполнения команды
            /// </summary>
            /// <param name="command">Тип команды</param>
            /// <param name="commandList">Список всех команды (Запрещается измениять кол-во записей)</param>
            public delegate void FinishCallbackDelegate(CommandParallelEnum command, BackgroundThreadParameters commandList);
            /// <summary>
            /// Тип команды
            /// </summary>
            public CommandParallelEnum CommandType;
            /// <summary>
            /// Данные для команды
            /// </summary>
            public object Data;
            /// <summary>
            /// Функция выполнения команды
            /// </summary>
            public ExecuteCallbackDelegate ExecuteCallBack;
            /// <summary>
            /// Функция завершения выполнения команды
            /// </summary>
            public FinishCallbackDelegate FinishCallback;
            /// <summary>
            /// Флаг активности команды
            /// </summary>
            public bool IsExecuted { get; set; }
            /// <summary>
            /// Конструктор
            /// </summary>
            public BackgroundThreadParameter()
            {
                IsExecuted = false;
                Data = null;
                ExecuteCallBack = null;
                FinishCallback = null;
            }
        }
        /// <summary>
        /// Список команд для паралельного потока
        /// </summary>
        internal class BackgroundThreadParameters : List<BackgroundThreadParameter>
        {
        }
    }
}
