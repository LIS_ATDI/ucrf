﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocRecognizer.Enum;
using DocRecognizer.Interface;

namespace DocRecognizer.Object
{
    /// <summary>
    /// Вспомагательный класс
    /// </summary>
    internal class ExtDocumentsNumberList : List<DocData> { }
    internal class ExtDocumentNumberDict : Dictionary<string, ExtDocumentsNumberList> { }
    /// <summary>
    /// Класс содержит все номера документов
    /// </summary>
    internal class DocumentNumberManager
    {
        /// <summary>
        /// Список документов без сканов
        /// </summary>
        private ExtDocumentNumberDict _listDocuments;
        public ExtDocumentNumberDict ListDocuments { get { return _listDocuments; } }
        /// <summary>
        /// Список документов с сканами
        /// </summary>
        private ExtDocumentNumberDict _listScanedDocuments;
        public ExtDocumentNumberDict ListScanedDocuments { get { return _listScanedDocuments; } }
        /// <summary>
        /// Конструктор
        /// </summary>
        public DocumentNumberManager()
        {
            _listDocuments = new ExtDocumentNumberDict();
            _listScanedDocuments = new ExtDocumentNumberDict();
        }
        /// <summary>
        /// Добавляет новые документы в список документов
        /// Дубликати удаляються
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="number">Номера документов</param>
        public void AddDocuments(string docType, params DocData[] number)
        {
            ExtDocumentsNumberList list = null;
            if (ListDocuments.ContainsKey(docType))
            {
                list = ListDocuments[docType];
            }
            else
            {
                list = new ExtDocumentsNumberList();
                ListDocuments.Add(docType, list);
            }
            if (list != null)
            {
                foreach (DocData num in number)
                {
                    if ((num == null) || (string.IsNullOrEmpty(num.DocNumber)))
                        continue;
                    DocData data = num;
                    var listNumber = list.Where(a => a.DocNumber == data.DocNumber);
                    if (listNumber.Count() == 0)
                        list.Add(num);
                }
            }
        }
        /// <summary>
        /// Добавляет документы в список документов со сканами
        /// Дубликати удаляються
        /// </summary>
        /// <param name="docType">Тип документа</param>
        /// <param name="number">Номера документов</param>
        public void AddScanedDocuments(string docType, params DocData[] number)
        {
            ExtDocumentsNumberList list = null;
            if (ListScanedDocuments.ContainsKey(docType))
            {
                list = ListScanedDocuments[docType];
            }
            else
            {
                list = new ExtDocumentsNumberList();
                ListScanedDocuments.Add(docType, list);
            }
            if (list != null)
            {
                foreach (DocData num in number)
                {
                    if ((num == null) || (string.IsNullOrEmpty(num.DocNumber)))
                        continue;
                    DocData data = num;
                    var listNumber = list.Where(a => a.DocNumber == data.DocNumber);
                    if (listNumber.Count() == 0)
                        list.Add(num);
                }
            }
        }
        /// <summary>
        /// Очистить списки всех документов
        /// </summary>
        public void ClearAll()
        {
            ListDocuments.Clear();
            ListScanedDocuments.Clear();
        }
        /// <summary>
        /// Очистить список определенного документа
        /// </summary>
        /// <param name="docType">Тип документа</param>
        public void Clear(string docType)
        {
            if (ListDocuments.ContainsKey(docType))
                ListDocuments[docType].Clear();
            if (ListScanedDocuments.ContainsKey(docType))
                ListScanedDocuments[docType].Clear();
        }
    }
}
