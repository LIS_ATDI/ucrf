﻿using System;
using DocRecognizer.Interface;

namespace DocRecognizer.Object
{
    internal sealed class SingletonFactory : ISingletonFactory
    {
        /// <summary>
        /// Template singleton
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private class Singleton<T> where T : class, new()
        {
            private static volatile T _instance;
            private static object _syncRoot = new object();

            private Singleton() { }

            public static T Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        lock (_syncRoot)
                        {
                            if (_instance == null)
                                _instance = new T();
                        }
                    }
                    return _instance;
                }
            }

            public static void Init()
            {
                if (_instance != null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance != null)
                            _instance = null;
                    }
                }
            }
        }
        #region Implementation of ISingletonFactory
        /// <summary>
        /// Возвращает DocumentNumberManager одиночку
        /// </summary>
        public DocumentNumberManager GetDocumentNumberManager { get { return Singleton<DocumentNumberManager>.Instance; } }
        /// <summary>
        /// Возвращает DocRecognizer
        /// </summary>
        public DocRecognizer GetDocRecognizer{get { return Singleton<DocRecognizer>.Instance; }}
        /// <summary>
        /// Инициализация
        /// </summary>
        public void Init()
        {
            Singleton<DocumentNumberManager>.Init();
            Singleton<DocRecognizer>.Init();
        }

        #endregion
    }
}
