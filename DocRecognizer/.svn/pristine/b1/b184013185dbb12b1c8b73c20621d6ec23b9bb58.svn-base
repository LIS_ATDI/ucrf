﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using DocRecognizer.Object;

namespace DocRecognizer.Form
{
    /// <summary>
    /// Реализация команды "Загрузки сканов во внешнюю систему"
    /// </summary>
    internal partial class RecognizeForm
    {
        public static RoutedCommandBase CommitScanCommand = new RoutedCommandBase();
        /// <summary>
        /// Проверка возможности выполнения команды "Загрузки сканов во внешнюю систему"
        /// </summary>
        public bool IsCommitScanEnable
        {
            get
            {
                bool isEnabled = false;
                foreach (DocumentData document in _dbDocuments)
                {
                    if (NeedToCommitScan(document))
                    {
                        isEnabled = true;
                        break;
                    }
                }
                return isEnabled & !BackgroundThread.BackgroundThreadObject.IsBackgroundThreadBuzy && !_dbDocuments.IsErrorRow;
            }
        }
        /// <summary>
        /// Выполнение команды "Загрузки сканов во внешнюю систему"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedCommitScanCommand(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show("Завантажити вибрані записи?", "", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) != MessageBoxResult.Yes)
                return;

            _dbDocuments.Save();
            List<DocumentData> list = new List<DocumentData>();
            for (int indexDoc = 0; indexDoc < _dbDocuments.Count; indexDoc++)
            {
                DocumentData document = _dbDocuments[indexDoc];
                if (NeedToCommitScan(document) == false)
                    continue;
                list.Add(document);
            }
            BackgroundThread.BackgroundThreadParameter param = new BackgroundThread.BackgroundThreadParameter
            {
                CommandType = BackgroundThread.CommandParallelEnum.CommitScan,
                Data = list,
                ExecuteCallBack = RunCommitScan
            };
            BackgroundThread.BackgroundThreadObject.Run(param);
            CommandManager.InvalidateRequerySuggested();
        }
        /// <summary>
        /// Проверка возможности выполнения команды Загрузить картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteCommitScanCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsCommitScanEnable;
        }
        /// <summary>
        /// Проверка необходимости распознования документа
        /// </summary>
        /// <param name="document">Документ</param>
        /// <returns>TRUE - необходимо распознать, иначе FALSE</returns>
        private bool NeedToCommitScan(DocumentData document)
        {
            if ((document.IsChecked) && ((document.Status == Enum.DocStatusEnum.Found) || (document.Status == Enum.DocStatusEnum.ErrorTransfer)))
                return true;
            return false;
        }

        /// <summary>
        /// Функция распознования (паралельный поток)
        /// </summary>
        /// <param name="bgWorker"></param>
        /// <param name="e"></param>
        /// <param name="data"></param>
        private void RunCommitScan(BackgroundWorker bgWorker, DoWorkEventArgs e, object data)
        {
            List<DocumentData> listDoc = data as List<DocumentData>;
            if (listDoc == null)
                throw new Exception("Error parameters");
            //-------
            var docListOfSystem = InternalInit.GetSingletonFactory().GetDocumentNumberManager.ListDocuments;
            var docListOfSystemScaned = InternalInit.GetSingletonFactory().GetDocumentNumberManager.ListScanedDocuments;
            StatusBarObject sentStatus = new StatusBarObject();
            sentStatus.BigText = "Завантаження скан-копій...";
            sentStatus.MaxVal = listDoc.Count;
            CommitScanData commitData = new CommitScanData();
            DocRecognizer docRecognizerObj = InternalInit.GetSingletonFactory().GetDocRecognizer;
            for (int index = 0; index < listDoc.Count; index++)
            {
                sentStatus.CurVal = index;
                if(bgWorker != null)
                    bgWorker.ReportProgress(0, sentStatus);
                DocumentData doc = listDoc[index];
                commitData.Number = doc.Number;
                commitData.BlankNumber = doc.BlankNumber;
                commitData.DateOut = doc.DateOut;
                commitData.DateEnd = doc.DateEnd;
                commitData.ScanFileName = doc.ImageFile;
                commitData.CommitIsOk = false;
                commitData.ErrorMessage = "";
                commitData.NeedDeleteScanFile = true;
                if (docRecognizerObj.RaiseCommitScanCopyEvent(commitData))
                {
                    if (commitData.CommitIsOk)
                    {
                        string docType = "";
                        string docNum = doc.Number;
                        DocData dateDoc = new DocData();
                        foreach (var systemDoc in docListOfSystem)
                        {
                            if (systemDoc.Value.Where(a => a.DocNumber == docNum).Count() > 0)
                            {
                                docType = systemDoc.Key;
                                dateDoc.DocNumber = docNum;
                                break;
                            }
                        }
                        if(docListOfSystemScaned.ContainsKey(docType))
                            docListOfSystemScaned[docType].Add(dateDoc);
                        else
                        {
                            ExtDocumentsNumberList docList = new ExtDocumentsNumberList();
                            docList.Add(dateDoc);
                            docListOfSystemScaned.Add(docType, docList);
                        }

                        _dbDocuments.Remove(doc);
                        _dbDocuments.Save();
                    }
                    else if (!string.IsNullOrEmpty(commitData.ErrorMessage))
                    {
                        doc.SetStatusWithoutCheck(Enum.DocStatusEnum.ErrorTransfer);
                        string message = commitData.ErrorMessage;
                        if (string.IsNullOrEmpty(message))
                            message = "Помилка підчас завантаження скан-копій";
                        throw new Exception(message);
                    }
                }
            }
        }
    }
}
