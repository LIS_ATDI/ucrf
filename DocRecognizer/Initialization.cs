﻿using DocRecognizer.Interface;

namespace DocRecognizer
{
    /// <summary>
    /// Класс инициализации библиотеки
    /// </summary>
    public static class Initialization
    {
        /// <summary>
        /// Создает и возввращает класс DocRecognizer
        /// </summary>
        /// <returns>возвращает класс DocRecognizer</returns>
        public static IDocRecognizer CreateDocRecognizer()
        {
            return InternalInit.GetSingletonFactory().GetDocRecognizer;
        }
    }
}
