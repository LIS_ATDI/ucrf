﻿using System;
using System.Collections.Generic;

namespace DocRecognizer.Enum
{
    /// <summary>
    /// Статус документов
    /// </summary>
    internal enum DocStatusEnum
    {
        /// <summary>
        /// Не распознан документ
        /// </summary>
        NotRecognized = 0,
        /// <summary>
        /// Не найден
        /// </summary>
        NotFound,
        /// <summary>
        /// Распознан документ
        /// </summary>
        Recognized,
        /// <summary>
        /// Ошибка передачи
        /// </summary>
        ErrorTransfer,
        /// <summary>
        /// Ошибка передачи
        /// </summary>
        Found,
        /// <summary>
        /// Ошибка передачи
        /// </summary>
        Exist,
        /// <summary>
        /// Распознаваемый документ
        /// </summary>
        Recognizing,
        /// <summary>
        /// Дублирование номера
        /// </summary>
        DuplicateNumber,
    }
    /// <summary>
    /// Класс разширения типа DocStatusEnum
    /// </summary>
    internal static class DocStatusExtension
    {
        private static Dictionary<DocStatusEnum, string> _dictStatus;
        static DocStatusExtension()
        {
            _dictStatus = new Dictionary<DocStatusEnum, string>();
            _dictStatus.Add(DocStatusEnum.NotFound, "Не знайдено ");
            _dictStatus.Add(DocStatusEnum.NotRecognized, "Не розпізнано");
            _dictStatus.Add(DocStatusEnum.Recognized, "Розпізнано");
            _dictStatus.Add(DocStatusEnum.ErrorTransfer, "Помилка передачі");
            _dictStatus.Add(DocStatusEnum.Found, "Знайдено");
            _dictStatus.Add(DocStatusEnum.Exist, "Вже існує");
            _dictStatus.Add(DocStatusEnum.Recognizing, "Розпізнавання");
            _dictStatus.Add(DocStatusEnum.DuplicateNumber, "Дублювання номера");
        }
        /// <summary>
        /// Переводит тип DocStatusEnum в строку
        /// </summary>
        /// <param name="value">тип DocStatusEnum</param>
        /// <returns>строка</returns>
        public static string ToTranslateString(this DocStatusEnum value)
        {
            string retVal = "";
            if (_dictStatus.ContainsKey(value))
                retVal = _dictStatus[value];
            return retVal;
        }
        /// <summary>
        /// Переводит строку в тип DocStatusEnum
        /// </summary>
        /// <param name="value">Строка</param>
        /// <returns>тип DocStatusEnum</returns>
        public static DocStatusEnum ToDocStatusEnumFromTranslate(this string value)
        {
            DocStatusEnum retVal = DocStatusEnum.NotRecognized;
            if(_dictStatus.ContainsValue(value))
            {
                foreach (KeyValuePair<DocStatusEnum, string> valuePair in _dictStatus)
                {
                    if(valuePair.Value == value)
                    {
                        retVal = valuePair.Key;
                        break;
                    }
                }
            }
            return retVal;
        }
        /// <summary>
        /// Переводит тип DocStatusEnum в строку
        /// </summary>
        /// <param name="value">тип DocStatusEnum</param>
        /// <returns>строка</returns>
        public static string ToDocStatusString(this DocStatusEnum value)
        {
            return value.ToString();
        }
        /// <summary>
        /// Переводит строку в тип DocStatusEnum
        /// </summary>
        /// <param name="value">Строка</param>
        /// <returns>тип DocStatusEnum</returns>
        public static DocStatusEnum ToDocStatusEnum(this string value)
        {
            DocStatusEnum retVal = DocStatusEnum.NotRecognized;
            try
            {
                retVal = (DocStatusEnum)System.Enum.Parse(typeof(DocStatusEnum), value);
            }
            catch (Exception)
            {
                retVal = DocStatusEnum.NotRecognized;
            }
            return retVal;
        }
    }
}
