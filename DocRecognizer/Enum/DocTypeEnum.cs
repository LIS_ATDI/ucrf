﻿using System.Collections.Generic;
using DocRecognizer.Object;

namespace DocRecognizer.Enum
{
    /// <summary>
    /// Класс разширения типа DocTypeEnum
    /// </summary>
    internal static class DocTypeExtension
    {
        private static Dictionary<string, string> _dictType;
        internal class ValueDescriptionDocTypeEnum : ValueDescription<string> { }
        private static List<ValueDescriptionDocTypeEnum> _listType;
        static DocTypeExtension()
        {
            Reload();
        }
        /// <summary>
        /// Перезагрузить тиры документов
        /// </summary>
        public static void Reload()
        {
            _dictType = new Dictionary<string, string>();
            foreach (SettingsDocRecognizer document in SettingsObject.Instance.SettingsDocument)
            {
                _dictType.Add(document.DocType, document.DocTypeDescription);
            }

            _listType = new List<ValueDescriptionDocTypeEnum>();
            foreach (var dictType in _dictType)
                _listType.Add(new ValueDescriptionDocTypeEnum() {Value = dictType.Key, Description = dictType.Value});
        }

        /// <summary>
        /// Переводит тип DocTypeEnum в строку
        /// </summary>
        /// <param name="value">тип DocTypeEnum</param>
        /// <returns>строка</returns>
        public static string DocTypeEnumToStr(this string value)
        {
            string retVal = "";
            if (_dictType.ContainsKey(value))
                retVal = _dictType[value];
            return retVal;
        }
        /// <summary>
        /// Переводит строку в тип DocTypeEnum
        /// </summary>
        /// <param name="value">Строка</param>
        /// <returns>тип DocTypeEnum</returns>
        public static string ToDocTypeEnum(this string value)
        {
            string retVal = "";
            if (_dictType.ContainsValue(value))
            {
                foreach (KeyValuePair<string, string> valuePair in _dictType)
                {
                    if (valuePair.Value == value)
                    {
                        retVal = valuePair.Key;
                        break;
                    }
                }
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает словарь документов
        /// </summary>
        /// <returns>Возвращает словарь документов</returns>
        public static List<ValueDescriptionDocTypeEnum> GetDictDocTypeEnum()
        {
            return _listType;
        }
    }
}
