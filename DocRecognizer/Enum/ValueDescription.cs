﻿namespace DocRecognizer.Enum
{
    internal class ValueDescription<T>
    {
        public T Value { get; set; }
        public string Description { get; set; }
    }
}
