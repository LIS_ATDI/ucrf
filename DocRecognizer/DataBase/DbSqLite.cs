﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Windows;

namespace DocRecognizer.DataBase
{
    class DbTableName
    {
        public const string Tsetting = "SETTING";
        public const string TsettingRecog = "SETTING_RECOG";
        public const string Tdocument = "DOCUMENT";
        public const string Tconfig = "CONFIG";
    }
    /// <summary>
    /// Класс для работы с БД
    /// </summary>
    class DbSqLite
    {
        /// <summary>
        /// Возвращает IDbConnection
        /// </summary>
        /// <returns>IDbConnection</returns>
        public static IDbConnection GetConnection()
        {
            return new SQLiteConnection(GetDbConnectionString());
        }
        /// <summary>
        /// Возвращает строку подключения
        /// </summary>
        /// <returns></returns>
        private static string GetDbConnectionString()
        {
            return string.Format("data source={0};New=True;UseUTF16Encoding=True", GetFullDataBaseFileName());
        }
        /// <summary>
        /// Возвращает полный путь к файлу БД
        /// </summary>
        /// <returns></returns>
        private static string GetFullDataBaseFileName()
        {
            return System.IO.Path.Combine(DocRecognizer.BasePath, DatabaseFileName);
        }
        /// <summary>
        /// Имя файла БД
        /// </summary>
        private const string DatabaseFileName = "working.db";
        /// <summary>
        /// Тип БД
        /// </summary>
        private const string DbType = "DOC_RECOGNIZER";
        /// <summary>
        /// Конструктор
        /// </summary>
        protected DbSqLite()
        {
        }
        /// <summary>
        /// инициализация БД
        /// </summary>
        /// <returns></returns>
        public static bool InitDatabase()
        {
            if (!System.IO.File.Exists(GetFullDataBaseFileName()))
                CreateDatabase();
            return UpdateDatebase();
        }
        /// <summary>
        /// Создать базу данных и таблицу
        /// </summary>
        private static void CreateDatabase()
        {
            //SQL-запрос для таблицы
            const string sqlCreateDataBese = @"CREATE TABLE 'VERSION'
                                      ('ID' INTEGER PRIMARY KEY AUTOINCREMENT,
                                       'DB_TYPE' TEXT,
                                       'DB_DATE' datetime NOT null DEFAULT '2009-01-01')";
            string sqlInsertDbVersion = string.Format(@"INSERT INTO 'VERSION' ('DB_TYPE') VALUES ('{0}')", DbType);
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (SQLiteConnection connect = new SQLiteConnection(GetDbConnectionString()))
            {
                try
                {
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                    {
                        //открываем соединение
                        connect.Open();
                    }
                    //создаем новую таблицу
                    SQLiteCommand command = new SQLiteCommand(sqlCreateDataBese, connect);
                    command.ExecuteNonQuery();
                    command = new SQLiteCommand(sqlInsertDbVersion, connect);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
        }
        /// <summary>
        /// обновляет структуру БД при необходимости
        /// </summary>
        private static bool UpdateDatebase()
        {
            bool retVal = true;
            DbUpdateCommand[] commands = GetDatabaseUpdateCommand();
            ConnectionState previousConnectionState = ConnectionState.Closed;
            using (SQLiteConnection connect = new SQLiteConnection(GetDbConnectionString()))
            {
                try
                {
                    DateTime dbDate = DateTime.Now;
                    //проверяем предыдущее состояние
                    previousConnectionState = connect.State;
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();
                    {
                        //Вытаскиваем дату последнего обновления
                        using (IDbCommand dbcmd = connect.CreateCommand())
                        {
                            dbcmd.CommandText = string.Format(@"SELECT DB_DATE FROM VERSION WHERE DB_TYPE LIKE '{0}'",
                                                              DbType);
                            using (IDataReader reader = dbcmd.ExecuteReader())
                            {
                                if (reader.Read())
                                    dbDate = reader.ToDateTime(0);
                                reader.Close();
                            }
                        }
                    }
                    DateTime lastDateTime = dbDate;
                    foreach (DbUpdateCommand updateCommand in commands)
                    {
                        if(dbDate < updateCommand.DateUpdate)
                        {
                            using (IDbCommand dbcmd = connect.CreateCommand())
                            {
                                dbcmd.CommandText = updateCommand.SqlCommand;
                                dbcmd.ExecuteNonQuery();
                            }
                            if(lastDateTime < updateCommand.DateUpdate)
                                lastDateTime = updateCommand.DateUpdate;
                        }
                    }
                    if (dbDate < lastDateTime)
                    {
                        //Записываем дату последнего обновления
                        using (SQLiteCommand dbcmd = connect.CreateCommand())
                        {
                            dbcmd.CommandText = string.Format(@"UPDATE VERSION SET DB_DATE='{1}' WHERE DB_TYPE='{0}'",
                                                              DbType, lastDateTime.ToDbDateTime());
                            dbcmd.ExecuteNonQuery();
                        }
                    }
                }
                catch(Exception ex)
                {
                    retVal = false;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                finally
                {
                    //закрываем соединение, если оно было закрыто перед открытием
                    if (previousConnectionState == ConnectionState.Closed)
                    {
                        connect.Close();
                    }
                }
            }
            return retVal;
        }
        /// <summary>
        /// Возвращает список команд обновления БД
        /// </summary>
        /// <returns>список команд обновления БД</returns>
        private static DbUpdateCommand[] GetDatabaseUpdateCommand()
        {
            List<DbUpdateCommand> retList = new List<DbUpdateCommand>();
            retList.Add(new DbUpdateCommand
            {
                DateUpdate = ToDateTime("20111014.1622"),
                SqlCommand = string.Format(@"CREATE TABLE {0} (
                                                    'ID' INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    'FC_DLL' TEXT,
                                                    'FC_SN' TEXT
                                                    )", DbTableName.Tsetting),
            });
            retList.Add(new DbUpdateCommand
            {
                DateUpdate = ToDateTime("20111014.1623"),
                SqlCommand = string.Format(@"CREATE TABLE {0} ( 
                                                    'ID' INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    'DOC_TYPE' TEXT,
                                                    'DOC_TYPE_DESCR' TEXT,
                                                    'FC_PROJECT' TEXT,
                                                    'NAME_DOCNUMBER' TEXT,
                                                    'NAME_BLANKNUMBER' TEXT,
                                                    'NAME_DATEOUT' TEXT
                                                    )", DbTableName.TsettingRecog),
            });
            retList.Add(new DbUpdateCommand
            {
                DateUpdate = ToDateTime("20111014.1625"),
                SqlCommand = string.Format(@"CREATE TABLE {0} ( 
                                                    'ID' INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    'IS_CHECKED' BOOL,
                                                    'DOC_NUMBER' TEXT,
                                                    'DOC_RECT' TEXT,
                                                    'BLANK_NUMBER' TEXT,
                                                    'BLANK_RECT' TEXT,
                                                    'DATE_OUT' DATETIME,
                                                    'DATE_RECT' DATETIME,
                                                    'FILE_IMAGE' TEXT,
                                                    'DATE_SCAN' DATETIME,
                                                    'DOC_TYPE' TEXT,
                                                    'STATUS' TEXT
                                                    )", DbTableName.Tdocument),
            });
            retList.Add(new DbUpdateCommand
                            {
                                DateUpdate = ToDateTime("20111114.1253"),
                                SqlCommand = string.Format(@"ALTER TABLE {0} ADD 'LAST_NUMBER' TEXT", DbTableName.Tdocument)
            });
            retList.Add(new DbUpdateCommand
            {
                DateUpdate = ToDateTime("20120229.1246"),
                SqlCommand = string.Format(@"CREATE TABLE {0} ( 
                                                    'ID' INTEGER PRIMARY KEY AUTOINCREMENT,
                                                    'ITEM_KEY' TEXT,
                                                    'ITEM_DATA' TEXT
                                                    )", DbTableName.Tconfig),
            });
            return retList.ToArray();
        }
        /// <summary>
        /// Класс структуры команд обновления БД
        /// </summary>
        private class DbUpdateCommand
        {
            public DateTime DateUpdate { get; set; }
            public string SqlCommand { get; set; }
        }
        /// <summary>
        /// Преобразовывает строку в ДАТУ
        /// </summary>
        /// <param name="strDateTime">строка в формате "YYYYMMDD.HHmm"</param>
        /// <returns>Дата</returns>
        private static DateTime ToDateTime(string strDateTime)
        {
            DateTime retDate;
            if (DateTime.TryParseExact(strDateTime, "yyyyMMdd.HHmm",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out retDate))
                return retDate;
            return DateTime.MinValue;
        }
    }
    /// <summary>
    /// Конвертор типов БД
    /// </summary>
    static class DbExtension
    {
        /// <summary>
        /// В тип Bool
        /// </summary>
        /// <param name="reader">IDataReader</param>
        /// <param name="index">Номер поля</param>
        /// <returns>BOOL</returns>
        public static bool ToBool(this IDataReader reader, int index)
        {
            bool retVal = false;
            if (reader.GetString(index).ToLower() == "true")
                retVal = true;
            return retVal;
        }
        /// <summary>
        /// В тип DateTime
        /// </summary>
        /// <param name="reader">IDataReader</param>
        /// <param name="index">Номер поля</param>
        /// <returns>DateTime</returns>
        public static DateTime ToDateTime(this IDataReader reader, int index)
        {
            DateTime retDate;
            if (DateTime.TryParseExact(reader.GetString(index), "yyyy-dd-MM HH:mm:ss",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out retDate))
                return retDate;
            return DateTime.MinValue;
        }
        /// <summary>
        /// Преобразовывает дату в строку БД
        /// </summary>
        /// <param name="dt">дата</param>
        /// <returns></returns>
        public static string ToDbDateTime(this DateTime dt)
        {
            return dt.ToString("yyyy-dd-MM HH:mm:ss");
        }
        /// <summary>
        /// Преобразовывает строку в строку БД
        /// </summary>
        /// <param name="str">строка</param>
        /// <returns></returns>
        public static string ToDbString(this string str)
        {
            return str.Replace("'", "\"");
        }
        /// <summary>
        /// Преобразовывает Rectangle в строку БД
        /// </summary>
        /// <param name="rec">Rectangle</param>
        /// <returns></returns>
        public static string ToDbString(this Rectangle rec)
        {
            Rect r = new Rect(rec.X, rec.Y, rec.Width, rec.Height);
            return string.Format("{0},{1},{2},{3}", rec.X, rec.Y, rec.Width, rec.Height);
        }
        /// <summary>
        /// Преобразовывает строку БД в Rectangle
        /// </summary>
        /// <param name="str">строка</param>
        /// <returns></returns>
        public static Rectangle ToDbRectangle(this string str)
        {
            Rect r;
            try
            {
                r = Rect.Parse(str);
            }
            catch
            {
                r = new Rect();
            }
            return new Rectangle((int)r.X, (int)r.Y, (int)r.Width, (int)r.Height);
        }
        /// <summary>
        /// возвращает ID последней вставленой записи
        /// </summary>
        /// <param name="connect">Подключение</param>
        /// <returns>ID последней вставленной записи, или 0 если ошибка</returns>
        public static int GelLastInsertedId(this IDbConnection connect)
        {
            int retVal = 0;
            if ((connect != null) && (connect.State == ConnectionState.Open))
            {
                //Вытаскиваем дату последнего обновления
                using (IDbCommand dbcmd = connect.CreateCommand())
                {
                    dbcmd.CommandText = @"SELECT last_insert_rowid()";
                    using (IDataReader reader = dbcmd.ExecuteReader())
                    {
                        if (reader.Read())
                            retVal = reader.GetInt32(0);
                        reader.Close();
                    }
                }
            }
            return retVal;
        }
    }
}
