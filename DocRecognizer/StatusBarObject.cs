﻿using DocRecognizer.Interface;
using DocRecognizer.Object;

namespace DocRecognizer
{
    /// <summary>
    /// Класс данных строки состояния
    /// </summary>
    internal class StatusBarObject : NotifyPropertyChanged
    {
        private string _bigText;
        private string _smallText;
        private int _curVal;
        private int _maxVal;
        private bool _canCancel;
        /// <summary>
        /// Большой текст
        /// </summary>
        public string BigText
        {
            get { return GetThreadSafeValue(ref _bigText);}
            set { SetThreadSafeValue(ref _bigText, value, "BigText");}
        }
        /// <summary>
        /// Маленький текст
        /// </summary>
        public string SmallText
        {
            get { return GetThreadSafeValue(ref _smallText);}
            set { SetThreadSafeValue(ref _smallText, value, "SmallText");}
        }
        /// <summary>
        /// Текущее значение ProgressPar
        /// </summary>
        public int CurVal
        {
            get { return GetThreadSafeValue(ref _curVal);}
            set { SetThreadSafeValue(ref _curVal, value, "CurVal");}
        }
        /// <summary>
        /// Максимальное значение ProgressPar
        /// </summary>
        public int MaxVal
        {
            get { return GetThreadSafeValue(ref _maxVal);}
            set { SetThreadSafeValue(ref _maxVal, value, "MaxVal");}
        }
        /// <summary>
        /// Можна отменить операцияю
        /// </summary>
        public bool СanCancel
        {
            get { return GetThreadSafeValue(ref _canCancel);}
            set { SetThreadSafeValue(ref _canCancel, value, "СanCancel");}
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public StatusBarObject()
        {
            BigText = "";
            SmallText = "";
            CurVal = 0;
            MaxVal = 1;
            СanCancel = false;
        }
    }
    /// <summary>
    /// Класс управления строкой состояния
    /// </summary>
    internal class StatusBarManager : NotifyPropertyChanged
    {
        ThreadSafeObservableCollection<StatusBarObject> ListState { get; set; }

        private StatusBarObject _currentState;
        /// <summary>
        /// Текущее состояния
        /// </summary>
        public StatusBarObject CurrentState
        {
            get { return GetThreadSafeValue(ref _currentState);}
            set { SetThreadSafeValue(ref _currentState, value, "CurrentState");}
        }
        /// <summary>
        /// Сонструктор
        /// </summary>
        public StatusBarManager()
        {
            ListState = new ThreadSafeObservableCollection<StatusBarObject>();
            CurrentState = new StatusBarObject();
        }
        /// <summary>
        /// Инициализация нового статус бара
        /// </summary>
        /// <param name="isClear"></param>
        public StatusBarObject Push(bool isClear)
        {
            ListState.Add(CurrentState);
            if (isClear)
                CurrentState = new StatusBarObject();
            return CurrentState;
        }
        /// <summary>
        /// Инициализация предыдущего статус бара
        /// </summary>
        public void Pop()
        {
            if (ListState.Count > 0)
            {
                CurrentState = ListState[ListState.Count - 1];
                ListState.Remove(CurrentState);
            }
        }
        /// <summary>
        /// Удаление статуса
        /// </summary>
        public void Pop(StatusBarObject state)
        {
            if (ListState.IndexOf(state) > -1)
            {
                ListState.Remove(state);
            }
            if (CurrentState == state)
            {
                if (ListState.Count > 0)
                    CurrentState = ListState[ListState.Count - 1];
                else
                    CurrentState = new StatusBarObject();
            }
        }
        /// <summary>
        /// Установка строки BigText
        /// </summary>
        /// <param name="str"></param>
        public void SetBigText(string str)
        {
            CurrentState.BigText = str;
        }
        /// <summary>
        /// Установка строки SmallText
        /// </summary>
        /// <param name="str">строка</param>
        public void SetSmallText(string str)
        {
            CurrentState.SmallText = str;
        }
        /// <summary>
        /// Установка строки SmallText
        /// </summary>
        public void SetSmallText(int val1, int val2)
        {
            SetSmallText(string.Format("{0} / {1}", val1, val2));
        }

        #region Thread safe function
        private object _inLock = new object();
        protected StatusBarObject GetThreadSafeValue(ref StatusBarObject oldVal)
        {
            StatusBarObject retVal;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref StatusBarObject oldVal, StatusBarObject newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }
        #endregion
    }
}
