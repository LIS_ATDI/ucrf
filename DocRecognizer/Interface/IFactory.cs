﻿using DocRecognizer.Object;

namespace DocRecognizer.Interface
{
    /// <summary>
    /// Интерфейс создания компонентов
    /// </summary>
    internal interface IFactory
    {
    }

    /// <summary>
    /// Интерфейс создания компонентов-одиночек
    /// </summary>
    internal interface ISingletonFactory
    {
        /// <summary>
        /// Возвращает DocumentNumberManager одиночку
        /// </summary>
        DocumentNumberManager GetDocumentNumberManager { get; }
        DocRecognizer GetDocRecognizer { get; }
        void Init();
    }
}
