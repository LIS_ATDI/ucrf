﻿using System;
using System.Collections.Generic;

namespace DocRecognizer.Interface
{
    public delegate void CommitScanCopyDelegate(params ICommitScanData[] param);
    /// <summary>
    /// Интерфейс работы с библиотекой
    /// </summary>
    public interface IDocRecognizer : IDisposable
    {
        event CommitScanCopyDelegate CommitScanCopy;
        /// <summary>
        /// Инициализация библиотеки
        /// </summary>
        /// <param name="basePath">путь к файлу настроек</param>
        /// <returns>TRUE - инициализация успешно, иначе FALSE</returns>
        bool Init(string basePath);
        /// <summary>
        /// Загрузить номера известных документав. Документы, у которых нет привязки к скан-копии
        /// </summary>
        /// <param name="listKnownDocuments">Список документов</param>
        /// <param name="docType">Тип документа</param>
        void LoadDocuments(string docType, params DocData[] listKnownDocuments);
        /// <summary>
        /// Загрузить номера документав у которых уже есть скан-копии.
        /// </summary>
        /// <param name="listKnownDocuments">Список документов</param>
        /// <param name="docType">Тип документа</param>
        void LoadScanedDocuments(string docType, params DocData[] listKnownDocuments);
        /// <summary>
        /// Возвращает интерфейс работи с формой распознования
        /// </summary>
        /// <returns>Интерфейс работи с формой распознования</returns>
        IRecognizeWindow GetRecognizeWindow();
    }
    /// <summary>
    /// интерфейс данных для загрузки 
    /// </summary>
    public interface ICommitScanData
    {
        /// <summary>
        /// Номер документа
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// Дата выдачи
        /// </summary>
        DateTime DateOut { get; set; }
        /// <summary>
        /// Дата окончания
        /// </summary>
        DateTime DateEnd { get; set; }
        /// <summary>
        /// Номер бланка
        /// </summary>
        string BlankNumber { get; set; }
        /// <summary>
        /// Путь к скан копии
        /// </summary>
        string ScanFileName { get; set; }
        /// <summary>
        /// Флаг необходимости удалить скан
        /// </summary>
        bool NeedDeleteScanFile { get; set; }
        /// <summary>
        /// Результат операции
        /// </summary>
        bool CommitIsOk { get; set; }
        /// <summary>
        /// Текст ошибки
        /// </summary>
        string ErrorMessage { get; set; }
    }
}
