﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DocRecognizer.Interface
{
    /// <summary>
    /// Интерфейс Окна формы распознования
    /// </summary>
    public interface IRecognizeWindow
    {
        Window GetWindow();
        void ShowDialog();
    }
}
