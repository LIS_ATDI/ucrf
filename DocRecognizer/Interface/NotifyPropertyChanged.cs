﻿using System;
using System.ComponentModel;

namespace DocRecognizer.Interface
{
    [Serializable]
    internal class NotifyPropertyChanged : INotifyPropertyChanged
    {
        [NonSerialized]
        private object _inLock = new object();
        [NonSerialized]
        private bool _isChanged = false;
        /// <summary>
        /// Флаг изменения свойства класса
        /// </summary>
        public bool IsChanged
        {
            get
            {
                return GetThreadSafeValue(ref _isChanged);
            }
            set
            {
                SetThreadSafeValue(ref _isChanged, value, "IsChanged");
            }
        }

        #region Implementation of INotifyPropertyChanged

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Инициализаци изменения свойства
        /// </summary>
        /// <param name="info">название свойства</param>
        protected void InvokeNotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                if (info != "IsChanged")
                    IsChanged = true;
            }
        }
        #endregion

        #region Thread safe function
        protected bool GetThreadSafeValue(ref bool oldVal)
        {
            bool retVal = false;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref bool oldVal, bool newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected string GetThreadSafeValue(ref string oldVal)
        {
            string retVal = "";
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref string oldVal, string newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected DateTime GetThreadSafeValue(ref DateTime oldVal)
        {
            DateTime retVal;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref DateTime oldVal, DateTime newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected Enum.DocStatusEnum GetThreadSafeValue(ref Enum.DocStatusEnum oldVal)
        {
            Enum.DocStatusEnum retVal;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref Enum.DocStatusEnum oldVal, Enum.DocStatusEnum newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected System.Drawing.Rectangle GetThreadSafeValue(ref System.Drawing.Rectangle oldVal)
        {
            System.Drawing.Rectangle retVal;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref System.Drawing.Rectangle oldVal, System.Drawing.Rectangle newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        protected int GetThreadSafeValue(ref int oldVal)
        {
            int retVal;
            lock (_inLock)
            {
                retVal = oldVal;
            }
            return retVal;
        }

        protected void SetThreadSafeValue(ref int oldVal, int newValue, string propertyName)
        {
            bool localChange = false;
            lock (_inLock)
            {
                if (oldVal != newValue)
                {
                    oldVal = newValue;
                    localChange = true;
                }
            }
            if (localChange && (string.IsNullOrEmpty(propertyName) == false))
                InvokeNotifyPropertyChanged(propertyName);
        }

        #endregion
    }
}
