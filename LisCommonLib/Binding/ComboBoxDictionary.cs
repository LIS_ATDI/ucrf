﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace Lis.CommonLib.Binding
{
    public interface IComboDictionary<TKey, TDesc>
    {
        TKey Key { get; set; }
        TDesc Description { get; set; }
    }

    public class ComboBoxDictionary<TKey, TDesc> : IComboDictionary<TKey, TDesc>
    {
        private TKey key;
        private TDesc description;
        public TKey Key { get { return key; } set { key = value; } }
        public TDesc Description { get { return description; } set { description = value; } }
        //===================================================
        public ComboBoxDictionary(TKey _key, TDesc _description)
        {
            Key = _key;
            Description = _description;
        }
    }
    //======================================================
    public class ComboBoxDictionaryList<TKey, TDesc> : BindingList<ComboBoxDictionary<TKey, TDesc>>
    {
        private ComboBox comboBox = null;
        //===================================================
        public void InitComboBox(ComboBox _comboBox)
        {
            comboBox = null;
            comboBox = _comboBox;
            comboBox.DataSource = this;
            comboBox.DisplayMember = "Description";
            comboBox.ValueMember = "Key";
            SetMaxWidthForDropDown(comboBox);
            ListChanged += new ListChangedEventHandler(DictioryListChanged);
        }

        public void InitComboBox(ComboBox _comboBox, bool Status)
        {
            comboBox = null;
            comboBox = _comboBox;
            comboBox.DataSource = null;
            comboBox.DataSource = this;
            if (!Status) comboBox.DisplayMember = "Description";
            if (Status) comboBox.DisplayMember = "Key";
            comboBox.ValueMember = "Key";
            SetMaxWidthForDropDown(comboBox);
            ListChanged += new ListChangedEventHandler(DictioryListChanged);
        }
        //===================================================
        private void SetMaxWidthForDropDown(ComboBox comboBox)
        {
            int widestWidth = comboBox.DropDownWidth;
            int currentWidth = 0;
            using (Graphics g = comboBox.CreateGraphics())
            {
                string desc = "";
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].Description != null)
                        desc = this[i].Description.ToString();
                    else
                        desc = "";
                    currentWidth = (int)g.MeasureString(desc, comboBox.Font).Width;
                    if (currentWidth > widestWidth) { widestWidth = currentWidth; }
                }
            }
           
            comboBox.DropDownWidth = widestWidth;
        }
        //===================================================
        // Изменился структура списка
        void DictioryListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                case ListChangedType.ItemAdded:
                case ListChangedType.ItemChanged:
                case ListChangedType.ItemDeleted:
                case ListChangedType.ItemMoved:
                    SetMaxWidthForDropDown(comboBox);
                    break;
            }
        }
    }
}
