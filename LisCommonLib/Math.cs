﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lis.CommonLib
{    
    /// <summary>
    /// Статический класс для математических функций 
    /// Перенести сюда код для конвертирования децибельных величин.
    /// 
    /// Данная библиотека не должна иметь никакой зависимости от библиотек ICSM.
    /// </summary>
    public static class MathLib
    {
        /// <summary>
        /// Логарифмическая сумма от 10.
        /// </summary>
        /// <param name="add1"></param>
        /// <param name="add2"></param>
        /// <returns></returns>
        public static double SummLog10(double add1, double add2)
        {
            return 10.0 * System.Math.Log10(System.Math.Pow(10, add1 / 10) + System.Math.Pow(10, add2 / 10));            
        }
    }
}
