﻿using System;
using System.Globalization;

namespace Lis.CommonLib.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTime NullDate = new DateTime(1,1,1);

        //===================================================
        /// <summary>
        /// Converts a DateTime to string
        /// </summary>
        /// <param name="value">DateTime value</param>
        /// <returns>string</returns>
        public static string ToShortDateStringNullT(this DateTime value)
        {
            return (value == NullDate) ? "" : value.ToShortDateString();
        }
        //===================================================
        /// <summary>
        /// Converts a DateTime to string
        /// </summary>
        /// <param name="value">DateTime value</param>
        /// <returns>string</returns>
        public static string ToStringNullT(this DateTime value)
        {
            return (value == NullDate) ? "" : value.ToString(CultureInfo.CurrentCulture);
        }


        public static string ToFilePartString(this DateTime value)
        {
            return value.Year.ToString("D4") + value.Month.ToString("D2") + value.Day.ToString("D2");
        }
        public static DateTime TruncateByDay(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day);
        }
    }
}
