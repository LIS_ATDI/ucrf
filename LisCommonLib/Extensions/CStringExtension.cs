﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace Lis.CommonLib.Extensions
{
    public static class CStringExtension
    {        
        //===================================================
        /// <summary>
        /// Converts a string to DateTime
        /// </summary>
        /// <param name="value">a string</param>
        /// <param name="format">format of Date</param>
        /// <returns>DateTime</returns>
        public static DateTime ToDataTime(this string value, string format)
        {
            DateTime retVal = DateTimeExtension.NullDate;
            DateTime outParam = DateTimeExtension.NullDate;

            if (DateTime.TryParse(value, out outParam))
                retVal = outParam.ToString(format).ToDataTime();
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Converts a string to DateTime
        /// </summary>
        /// <param name="value">a string</param>
        /// <returns>DateTime</returns>
        public static DateTime ToDataTime(this string value)
        {
            DateTime retVal = DateTimeExtension.NullDate;
            DateTime outParam = DateTimeExtension.NullDate;

            if (DateTime.TryParse(value, out outParam))
                retVal = outParam;
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Конвертирует значение строки в int
        /// </summary>
        /// <param name="dblStr">строка</param>
        /// <param name="defVal">значение по умолчанию</param>
        /// <returns>int</returns>
        public static int ToInt32(this string dblStr, int defVal)
        {
            int retVal = defVal;
            try { retVal = Convert.ToInt32(dblStr); }
            catch { };
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Конвертирует значение строки в double
        /// </summary>
        /// <param name="dblStr">строка</param>
        /// <param name="defVal">значение по умолчанию</param>
        /// <returns>double</returns>
        public static double ToDouble(this string dblStr, double defVal)
        {
            double retVal = defVal;
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";

            string newStrCoord = "";
            if (!string.IsNullOrEmpty(dblStr))
                newStrCoord = dblStr.Trim().Replace(',', '.');
            try {
                if (!string.IsNullOrEmpty(newStrCoord))
                    retVal = Convert.ToDouble(newStrCoord, provider);
            }
            catch
            {
                retVal = defVal;
            }
            return retVal;
        }
        //===================================================
        /// <summary>
        /// Преобразовывает запрещенные символы в XML
        /// </summary>
        /// <param name="value">строка</param>
        /// <returns>Возвращает строку без запрещенных символов для XML</returns>
        public static string ToStrXML(this string value)
        {
            return SecurityElement.Escape(value);
        }
        //===================================================
        /// <summary>
        /// Возвращает имя файла без разширения
        /// Переопределена функция System.IO.Path.GetFileNameWithoutExtension(...),
        /// так как в некоторых случаях были проблемы с номерами, где присутствует точка
        /// </summary>
        /// <param name="value">строка</param>
        /// <returns>имя файла без разширения</returns>
        public static string ToFileNameWithoutExtension(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";
            string retVal = value;
            if (value.Contains("\\"))
                retVal = Path.GetFileName(value);
            if(retVal.ToLower().Contains(".doc") ||
               retVal.ToLower().Contains(".rtf") ||
               retVal.ToLower().Contains(".pdf"))
                retVal = Path.GetFileNameWithoutExtension(retVal);
            return retVal;
        }

        public static string ToSubstringLast(this string value)
        {
            string retVal = "";
            if (value.Length > 0)
                retVal = value.Remove(value.Length - 1);
            return retVal;
        }

        //===================================================
        /// <summary>
        /// Преобразовывает строку со списоком чисел в список чисел
        /// </summary>
        /// <param name="value">строка</param>
        /// <param name="separator">разделитель чисел</param>
        /// <returns></returns>
        public static double[] ToDoubleArray(this string value, params char[] separator)
        {
            List<double> retLst = new List<double>();
            string[] parseList = value.Split(separator);
            foreach (string dblVal in parseList)
            {
                double tmpVal = dblVal.ToDouble(CDoubleExtension.NullDouble);
                if (tmpVal != CDoubleExtension.NullDouble)
                    retLst.Add(tmpVal);
            }
            return retLst.ToArray();
        }

        /// <summary>
        /// Перетворює строчку чисел в список цілих чисел
        /// </summary>
        /// <param name="value"></param>
        /// <returns>список цілих чисел</returns>
        public static int[] ToListInt(this string value)
        {
            List<int> intArrTmp = new List<int>();
            string[] strArrTmp = value.Split(',');
            for (int i = 0; i < strArrTmp.Length; i++)
            {
                if (strArrTmp[i].Contains("-"))
                {
                    int min = strArrTmp[i].Split('-').First().ToInt32(CIntegerExtension.NullInteger);
                    int max = strArrTmp[i].Split('-').Last().ToInt32(CIntegerExtension.NullInteger);
                    if (min <= max && min != CIntegerExtension.NullInteger && max != CIntegerExtension.NullInteger)
                        for (int j = min; j <= max; j++)
                            intArrTmp.Add(j);
                }
                else
                {
                    int tmpInt = strArrTmp[i].ToInt32(CIntegerExtension.NullInteger);
                    if (tmpInt != CIntegerExtension.NullInteger)
                        intArrTmp.Add(tmpInt);
                }
            }
                       
            return intArrTmp.ToArray();
        }

        public static string AppendFirstSymbols(this string value, char symbol, int length) {
           if (value.Length >= length)
           {
              return value;
           }
           var stringBuilder = new StringBuilder();
           for(int i = value.Length; i < length; i++)
           {
              stringBuilder.Append(symbol);
           }
           stringBuilder.Append(value);
           return stringBuilder.ToString();
        }
    }
}
