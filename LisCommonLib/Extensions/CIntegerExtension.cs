﻿

namespace Lis.CommonLib.Extensions
{
   public static class CIntegerExtension
   {
      public static int NullInteger = 2147483647;
      //===================================================
      /// <summary>
      /// Checks for NULLI
      /// </summary>
      /// <param name="value">a int value</param>
      /// <returns>true if the value == IM.NULLI</returns>
      public static bool IsNullI(this int value)
      {
          return (value == NullInteger);
      }
      //===================================================
      /// <summary>
      /// Checks for NOT NULLI
      /// </summary>
      /// <param name="value">a int value</param>
      /// <returns>true if the value != IM.NULLI</returns>
      public static bool IsNotNullI(this int value)
      {
         return !value.IsNullI();
      }
   }
}
