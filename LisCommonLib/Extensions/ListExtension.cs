﻿using System.Collections.Generic;
using System.Text;

namespace Lis.CommonLib.Extensions
{
   public static class ListExtension
   {
      //===================================================
      /// <summary>
      /// Converts a List to string with separator
      /// </summary>
      /// <param name="value">a list</param>
      /// <param name="separator">a separator</param>
      /// <param name="round">round</param>
      /// <returns>string</returns>
      public static string ToStringWithSeparator(this List<double> value, string separator, int round)
      {
          return value.ToArray().ToStringWithSeparator(separator, round);
      }
      //===================================================
      /// <summary>
      /// Converts a array to string with separator
      /// </summary>
      /// <param name="value">a list</param>
      /// <param name="separator">a separator</param>
      /// <param name="round">round</param>
      /// <returns>string</returns>
      public static string ToStringWithSeparator(this double[] value, string separator, int round)
      {
          StringBuilder retVal = new StringBuilder();
          bool isFirst = true;
          foreach (double val in value)
          {
              if (isFirst == false)
                  retVal.Append(separator + " ");
              isFirst = false;

              retVal.Append(val.ToString("F"+round));////Round(round));
          }
          return retVal.ToString();
      }
   }
}
