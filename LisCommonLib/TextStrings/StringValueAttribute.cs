﻿using System;

namespace LisCommonLib.TextStrings
{    
    /// <summary>
    /// Вспомогательный класс для того чтобы делать enum со строками,
    /// как показанано ниже 
    /// public enum HandTools
    /// {
    ///     [StringValue("Cordless Power Drill")
    ///       Drill = 5,
    ///     [StringValue("Long nose pliers")
    ///       Pliers = 7,
    ///     [StringValue("20mm Chisel")
    ///       Chisel = 9
    /// }
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        private string _value;

        public StringValueAttribute(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
