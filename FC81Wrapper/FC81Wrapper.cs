﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Lis.FlexiCapture
{            
    public static class Wrapper
    {
        public static IFlexiCapture Create()
        {
            return new FlexiCapture81Wrapper();
        }
    }
}
