﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lis.FlexiCapture
{
    public interface IFlexiCapture : IDisposable
    {
        /// <summary>
        /// Файл проекта fcproj
        /// </summary>
        string ProjectFileName { get; set; }
        /// <summary>
        /// Серийный номер для лицензии        
        /// </summary>
        string SerialNumber { get; set; }
        /// <summary>
        /// Путь к FCEngine.DLL 
        /// </summary>
        string DllPath { get; set; }
        /// <summary>
        /// Является ли инициализированным
        /// </summary>
        bool Initialized { get; }

        /// <summary>
        /// Очистить список файлов c изображениями
        /// </summary>
        void ClearImageFiles();
        /// <summary>
        /// Добавить изображение 
        /// </summary>
        /// <param name="fileName">Путсь к файлу с изобраджением</param>
        void AddImageFile(string fileName);
        /// <summary>
        /// Распознать
        /// 
        /// Вызывать после того, как все параметры установлены
        /// </summary>
        void Recognize();
        /// <summary>
        /// Получить количество распознанных блоков
        /// </summary>
        /// <returns></returns>
        int GetRecognizedBlockCount();
        /// <summary>
        /// Получить распознанный блок по индексу
        /// </summary>
        /// <param name="index">Индекс распознанного блока</param>
        /// <returns></returns>
        RecognizedTextBlock GetRecognizedBlockByIndex(int index);

        /// <summary>
        /// Инициализация, должна быть вызвана перед распознаванием
        /// </summary>
        void Initialize();

        /// <summary>
        /// Деинициализация, должна быть вызвана в конце
        /// </summary>
        //void Uninitialize();
    };
}
