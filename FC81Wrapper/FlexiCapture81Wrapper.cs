﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using FCEngine;

namespace Lis.FlexiCapture
{
    internal class FlexiCapture81Wrapper : IFlexiCapture
    {
        private string _projectFileName;
        private string _serialNumber;
        private string _dllPath;

        private bool _disposed;
        private bool _initialized;

        public string ProjectFileName { get { return _projectFileName; } set { _projectFileName = value; } }
        public string SerialNumber { get { return _serialNumber; } set { _serialNumber = value; } }
        public string DllPath { get { return _dllPath; } set { _dllPath = value; } }
        public bool Initialized{ get{ return _initialized; } }

        private List<string> _imageFiles = new List<string>();
        private List<RecognizedTextBlock> _recognized = new List<RecognizedTextBlock>();

        private IFCEngine _engine = null;

        /// <summary>
        /// To load the dll - dllFilePath dosen't have to be const - so I can read path from registry
        /// </summary>
        /// <param name="dllFilePath">file path with file name</param>        
        /// </param>
        /// <returns>Pointer to loaded Dll</returns>
        [DllImport("kernel32.dll")]
        private static extern IntPtr LoadLibraryEx(string dllFilePath, IntPtr hFile, uint dwFlags);

        /// <summary>
        /// To unload library 
        /// </summary>
        /// <param name="dllPointer">Pointer to Dll witch was returned from LoadLibraryEx</param>
        /// <returns>If unloaded library was correct then true, else false</returns>
        [DllImport("kernel32.dll")]
        public extern static bool FreeLibrary(IntPtr dllPointer);

        /// <summary>
        /// To get function pointer from loaded dll 
        /// </summary>
        /// <param name="dllPointer">Pointer to Dll witch was returned from LoadLibraryEx</param>
        /// <param name="functionName">Function name with you want to call</param>
        /// <returns>Pointer to function</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Ansi)]
        public extern static IntPtr GetProcAddress(IntPtr dllPointer, string functionName);

        /// <summary>
        /// This will to load concret dll file
        /// </summary>
        /// <param name="dllFilePath">Dll file path</param>
        /// <returns>Pointer to loaded dll</returns>
        /// <exception cref="ApplicationException">
        /// when loading dll will failure
        /// </exception>
        private static IntPtr LoadWin32Library(string dllFilePath)
        {
            IntPtr moduleHandle = LoadLibraryEx(dllFilePath, IntPtr.Zero, 8);
            if (moduleHandle == IntPtr.Zero)
            {
                // I'm gettin last dll error
                int errorCode = Marshal.GetLastWin32Error();
                throw new ApplicationException(
                    string.Format("There was an error during dll loading : {0}, error - {1}", dllFilePath, errorCode)
                    );
            }
            return moduleHandle;
        }
        //=================================================
        //=================================================
        #region Implementation of IDisposable
        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
        //=================================================
        /// <summary>
        /// Реализация Dispose
        /// </summary>
        /// <param name="disposing">TRUE - вызвано из метода IDisposable.Dispose()</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (this)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        Uninitialize();
                    }
                    _disposed = true;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private IntPtr _fcDll = IntPtr.Zero;
        public void Initialize()
        {
            if(_fcDll == IntPtr.Zero)
                _fcDll = LoadWin32Library(DllPath);
            InitializeEngine(SerialNumber, ref _engine);

        }
        /// <summary>
        /// Инициализация Engine
        /// </summary>
        /// <param name="devSn"></param>
        /// <param name="reserved1"></param>
        /// <param name="reserved2"></param>
        /// <param name="engine"></param>
        public delegate void InitializeEngineDelegate([MarshalAs(UnmanagedType.BStr)]string devSn, [MarshalAs(UnmanagedType.BStr)]string reserved1, [MarshalAs(UnmanagedType.BStr)]string reserved2, ref IFCEngine engine);
        private void InitializeEngine(string serialNumber, ref IFCEngine engine)
        {
            if (_fcDll == IntPtr.Zero)
                throw new ApplicationException("FC dll is not initialized");
            IntPtr pProc = GetProcAddress(_fcDll, "InitializeEngine");
            InitializeEngineDelegate initilizaEngine = (InitializeEngineDelegate)Marshal.GetDelegateForFunctionPointer(pProc, typeof(InitializeEngineDelegate));
            // Now i'm calling delegate, with is calling function from dll file 
            serialNumber = "FEDC-8014-0000-6669-7644";
            initilizaEngine(serialNumber, null, null, ref engine);
            _initialized = true;
            _engine.StartLogging("C:\\FC.log", true);
        }
        /// <summary>
        /// Деинициализация FC
        /// </summary>
        public delegate void DeinitializeEngineDelegate();
        protected void Uninitialize()
        {
            if (_engine != null)
            {
                _engine = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            if (_fcDll != IntPtr.Zero)
            {
                IntPtr pProc = GetProcAddress(_fcDll, "DeinitializeEngine");
                DeinitializeEngineDelegate deinitilizaEngine = (DeinitializeEngineDelegate)Marshal.GetDelegateForFunctionPointer(pProc, typeof(DeinitializeEngineDelegate));
                // Now i'm calling delegate, with is calling function from dll file 
                deinitilizaEngine();
                FreeLibrary(_fcDll);
                _fcDll = IntPtr.Zero;
            }
            _initialized = false;
        }
        /// <summary>
        /// Сбросить список файлов
        /// </summary>
        public void ClearImageFiles()
        {
            _imageFiles.Clear();
        }
        /// <summary>
        /// Добавить файл в список для распознования
        /// </summary>
        /// <param name="fileName"></param>
        public void AddImageFile(string fileName)
        {
            if (!_imageFiles.Contains(fileName))
                _imageFiles.Add(fileName);
        }
        /// <summary>
        /// Количество распоз. блоков
        /// </summary>
        /// <returns></returns>
        public int GetRecognizedBlockCount()
        {
            return _recognized.Count;
        }
        /// <summary>
        /// Вернуть распознаный блок
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public RecognizedTextBlock GetRecognizedBlockByIndex(int index)
        {
            return _recognized[index];
        }
        /// <summary>
        /// Распознать документ
        /// </summary>
        public void Recognize()
        {
            if (!_initialized)
                throw new Exception("It's necessary to call Initialize before Recognize");

            if (_imageFiles.Count==0)
                throw new Exception("No image files");

            if (string.IsNullOrEmpty(ProjectFileName))
                throw new Exception("No project name");

            IFCProject project = _engine.OpenProject(ProjectFileName);
            IFCBatch batch = project.Batches.Add("__TestBatch__");
            batch.Load();
            foreach (string imageFileName in _imageFiles)
                batch.AddImage(imageFileName);
            batch.ApplyDocumentDefinitions(null, false, null);
            batch.Recognize(null, RecognitionModeEnum.FCRM_ReRecognizeMinimal, null);

            IFCDocuments documents = batch.Documents;
            int docCount = documents.Count;
            _recognized.Clear();
            for (int i = 0; i < docCount; i++)
            {
                IFCDocument document = documents[i];
                document.Load(false);
                if (document.Fields != null)
                {
                    int fieldCount = document.Fields.Count;
                    for (int k = 0; k < fieldCount; k++)
                    {
                        int subFieldCount = document.Fields[k].Children.Count;
                        for (int l = 0; l < subFieldCount; l++)
                        {
                            RecognizedTextBlock recognizedBlock = new RecognizedTextBlock();
                            recognizedBlock.Name = document.Fields[k].Children[l].Name;
                            recognizedBlock.Percentage = 100.0;
                            recognizedBlock.Suspictious = false;
                            if (document.Fields[k].Children[l].Value != null)
                                recognizedBlock.Suspictious = document.Fields[k].Children[l].Value.Suspicious;
                            recognizedBlock.PageId = document.Fields[k].Page.ID;
                            recognizedBlock.DocumentId = document.Fields[k].Document.ID;

                            if (document.Fields[k].Children[l].Value != null)
                                recognizedBlock.Text = document.Fields[k].Children[l].Value.StringValue;

                            int blockCount = document.Fields[k].Children[l].Blocks.Count;
                            if (blockCount > 0)
                            {
                                int regionCount = document.Fields[k].Children[l].Blocks.Item(0).Region.Count;
                                if (regionCount > 0)
                                {
                                    int left = document.Fields[k].Children[l].Blocks.Item(0).Region.get_Left(0);
                                    int top = document.Fields[k].Children[l].Blocks.Item(0).Region.get_Top(0);
                                    int right = document.Fields[k].Children[l].Blocks.Item(0).Region.get_Right(0);
                                    int bottom = document.Fields[k].Children[l].Blocks.Item(0).Region.get_Bottom(0);
                                    recognizedBlock.Bounds = new Rectangle(left, top, right - left, bottom - top);
                                }
                            }
                            _recognized.Add(recognizedBlock);
                        }
                    }
                }
                document.Unload(true);
            }
            batch.Unload();
            project.Batches.DeleteAll();
        }
    }
}
