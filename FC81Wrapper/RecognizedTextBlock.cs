﻿using System.Drawing;

namespace Lis.FlexiCapture
{
    public class RecognizedTextBlock
    {
        /// <summary>
        /// Текст, (что распознано)
        /// </summary>        
        public string Text { get; set; }
        /// <summary>
        /// Имя поля в файле проекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Границы поля в пикселях
        /// </summary>
        public Rectangle Bounds { get; set; }
        /// <summary>
        /// Идентификатор страницы (начинается с одного)
        /// Скорее всего это сквозной идентификатор страницы.
        /// </summary>
        public int PageId { get; set; }
        /// <summary>
        /// Идентификатор документа (начинается с 1)
        /// </summary>
        public int DocumentId { get; set; }
        /// <summary>
        /// уверенно ли распознано (true - неуверенно)
        /// </summary>
        public bool Suspictious { get; set; }
        /// <summary>
        /// Пока фиктивное поле
        /// </summary>
        public double Percentage { get; set; }
    };
}
