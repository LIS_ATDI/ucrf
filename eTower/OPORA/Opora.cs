﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSM;
using OPORA;

namespace XICSM.OPORA
{
    public class Plugin: IPlugin
    {
        #region IPlugin Members

        public string Ident
        {
            get { return "OPORA"; }
        }

        public string Description
        {
            get { return "OPORA"; }
        }

        public double SchemaVersion
        {
            get { return 0.0; }
        }

        public void GetMainMenu(IMMainMenu mainMenu)
        {
           mainMenu.SetInsertLocation("Tools\\", IMMainMenu.InsertLocation.Before);
           mainMenu.InsertItem("Tools\\Import\\Import eTower file", Import, "MOB_STATION");
        }


        public bool OtherMessage(string message, object inParam, ref object outParam)
        {
            return false;
        }

        public void RegisterBoard(IMBoard b)
        {
          
        }

        public void RegisterSchema(IMSchema s)
        {
            
        }

        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            return true;
        }

        #endregion

        void Import()
        {
            CMain_logic main = new CMain_logic();
            main.Execute();
        }
    }
}
