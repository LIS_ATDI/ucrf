﻿using System;
using System.Collections.Generic;
using ICSM;

namespace OPORA.Icsm
{
   internal class LisProgressBar : IDisposable
   {
      private class StructSaveData
      {
         public string BigText { get; set; }
         public string SmallText { get; set; }
         public int Done { get; set; }
         public int Total { get; set; }
         //--------------------------------
         public StructSaveData()
         {
            BigText = "";
            SmallText = "";
            Done = 0;
            Total = 0;
         }
      }
      private static Stack<StructSaveData> _stackData = null;
      //===================================================
      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="caption">A caption of a progress form</param>
      public LisProgressBar(string caption)
      {
         if (_stackData == null)
         {
            IMProgress.Create(caption);
            _stackData = new Stack<StructSaveData>();
         }
         _stackData.Push(new StructSaveData());
         //-------
         //Set up data by default
         //StructSaveData tmp = _stackData.Peek();
         //IMProgress.ShowBig(tmp.BigText);
         //IMProgress.ShowSmall(tmp.SmallText);
         //IMProgress.ShowProgress(tmp.Done, tmp.Total);
      }
      //===================================================
      /// <summary>
      /// Destroys the progress form 
      /// </summary>
      public void Dispose()
      {
         _stackData.Pop();

         if (_stackData.Count == 0)
         {
            IMProgress.Destroy();
            _stackData = null;
         }
         else
         {
            StructSaveData tmp = _stackData.Peek();
            IMProgress.ShowBig(tmp.BigText);
            IMProgress.ShowSmall(tmp.SmallText);
            IMProgress.ShowProgress(tmp.Done, tmp.Total);
         }
      }
      //===================================================
      /// <summary>
      /// Sets a text in a big label
      /// </summary>
      /// <param name="text">A text</param>
      public void SetBig(string text)
      {
         _stackData.Peek().BigText = text;
         IMProgress.ShowBig(text);
      }
      //===================================================
      /// <summary>
      /// Sets a text in the small label
      /// </summary>
      /// <param name="text">A text</param>
      public void SetSmall(string text)
      {
         _stackData.Peek().SmallText = text;
         IMProgress.ShowSmall(text);
      }
      //===================================================
      /// <summary>
      /// Sets a number in the small label
      /// </summary>
      /// <param name="number">A number</param>
      public void SetSmall(int number)
      {
         _stackData.Peek().SmallText = number.ToString();
         IMProgress.ShowSmall(number);
      }
      //===================================================
      /// <summary>
      /// Sets numbers in the small label
      /// </summary>
      /// <param name="number1">A number 1</param>
      /// <param name="number2">A number 2</param>
      public void SetSmall(int number1, int number2)
      {
         _stackData.Peek().SmallText = number1 + " / " + number2;
         IMProgress.ShowSmall(number1, number2);
      }
      //===================================================
      /// <summary>
      /// Sets a progress bar
      /// </summary>
      /// <param name="done"></param>
      /// <param name="total"></param>
      public void SetProgress(int done, int total)
      {
         _stackData.Peek().Done = done;
         _stackData.Peek().Total = total;
         IMProgress.ShowProgress(done, total);
      }
      //===================================================
      /// <summary>
      /// Increment a progress bar
      /// </summary>
      /// <param name="showInSmal">Is show in the small window</param>
      public void Increment(bool showInSmal)
      {
          _stackData.Peek().Done = _stackData.Peek().Done + 1;
          IMProgress.ShowProgress(_stackData.Peek().Done, _stackData.Peek().Total);
          if (showInSmal)
              SetSmall(_stackData.Peek().Done);
      }
      //===================================================
      /// <summary>
      /// Was canceled by user
      /// </summary>
      /// <returns>TRUE if a process was canceled by user else FALSE</returns>
      public bool UserCanceled()
      {
         return IMProgress.UserCanceled();
      }
   }
}
