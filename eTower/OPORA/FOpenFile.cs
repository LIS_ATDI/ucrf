﻿using System;
using System.Windows.Forms;

namespace OPORA
{
    public partial class FOpenFile : Form
    {
        public string FileName { get { return tbFile.Text; } }
        public string DirectoryName { get { return tbAlbumPath.Text; } }
        public bool IsChecked { get { return cbAddNewTower.Checked; } }
        public FOpenFile()
        {
            InitializeComponent();
            EnableBtn();
        }

        private void EnableBtn()
        {
            string fileName = tbFile.Text;
            bool enabled = (string.IsNullOrEmpty(fileName) == false) && System.IO.File.Exists(fileName);
            string directoryName = tbAlbumPath.Text;
            enabled = enabled && (string.IsNullOrEmpty(directoryName) == false) &&
                      System.IO.Directory.Exists(directoryName);
            btnOk.Enabled = enabled;
        }

        private void btnOpenFileClick(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Файли програми eTower|*.xml";
            fd.DefaultExt = "*.xml";
            fd.FilterIndex = 0;
            if (fd.ShowDialog()==DialogResult.OK)
            {
                tbFile.Text = fd.FileName;
            }
            EnableBtn();
        }

        private void btnSelectDirectoryClick(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                tbAlbumPath.Text = fbd.SelectedPath;
            }
            EnableBtn();
        }
    }
}
