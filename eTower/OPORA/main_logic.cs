﻿using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;

//TODO:
//1. check once more coordinats and write normal function wich will be ok with 4dms and DEC
//2. Rewrite code for user desition import
//3. write UI and function for search
namespace OPORA
{
    /// <summary>
    /// class which encapsulate logic of plugin
    /// </summary>
    public class CMain_logic
    {
        CXML_CLASS cxml;// XML class
        List<COpora> _unimportedOporaList = new List<COpora>();
        /// <summary>
        /// + Main function to do all work
        /// </summary>
        public void Execute()
        {
            string fileName = "";
            string directoryName = "";
            bool isChecked = false;
            bool isOk = false;
            {
                FOpenFile opendlg = new FOpenFile();
                if (opendlg.ShowDialog() == DialogResult.OK)
                {
                    fileName = opendlg.FileName;
                    directoryName = opendlg.DirectoryName;
                    isChecked = opendlg.IsChecked;
                    isOk = true;
                }
            }
            if (isOk == false)
                return; //выход

            //Импортируем
            cxml = new CXML_CLASS();
            if (cxml.Execute(fileName, directoryName))
            {
                ImportData(isChecked);
            }
            else
            {
                MessageBox.Show("Операція була відмінена користувачем");
            }
        }
        /// <summary>
        /// Imports data into site table
        /// </summary>
        /// <param name="addnew">if true we add new records automaticly</param>
        private void ImportData(bool addnew)
        {
            using (Icsm.LisProgressBar pb = new Icsm.LisProgressBar("Імпорт даних"))
            {
                pb.SetBig("Імпорт даних...");
                pb.SetProgress(0, cxml.OporaList.Count);
                foreach (COpora opora in cxml.OporaList)
                {
                    if (pb.UserCanceled())
                        break;
                    if (opora == null)
                        continue;
                    if (opora.Defected)
                        continue;
                    ImportOpora(opora, addnew); //analize data from icsm and try to write what we can
                    if (opora.Records.Count == 1)
                        SecondTry(opora, opora.Records[0]); //it`s ok we updating data
                    pb.Increment(true);
                }
            }
            CalculateUnimportedOpora();
            if (_unimportedOporaList.Count > 0)
            {
                using (FUserUI fui = new FUserUI())
                {
                    fui.Init(_unimportedOporaList);
                }
            }
            else
            {
                MessageBox.Show("Всі опори були автоматично занесені в БД", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Get total unimported fields
        /// </summary>
        private void CalculateUnimportedOpora()
        {
            _unimportedOporaList.Clear();
            for (int i = 0; i < cxml.OporaList.Count; i++)
            {
                if ((cxml.OporaList[i] != null) && (!cxml.OporaList[i].Imported))
                    _unimportedOporaList.Add(cxml.OporaList[i]);
            }
        }
        //private static bool IsOblCity(string city)
        //{
        //    string[] arr = new string[] { "Сімферополь", "Вінниця", "Луцьк", "Дніпропетровськ", "Донецьк", "Житомир", "Ужгород", "Запоріжжя", "Івано-Франківськ", "Київ", "Кіровоград", "Луганськ", "Львів", "Миколаїв", "Одеса", "Полтава", "Рівне", "Суми", "Тернопіль", "Харків", "Херсон", "Хмельницький", "Черкаси", "Чернівці", "Чернігів", "Київ", "Севастополь"};
        //    for (int i = 0; i < arr.Length; i++)
        //    {
        //        if (city == arr[i])
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        ///// <summary>
        ///// Tring to get city ID
        ///// </summary>
        ///// <param name="city">name of the city</param>
        ///// <returns>ID of city record if none returns zero</returns>
        //private CityData getCityID(COpora opora)
        //{
        //    IMRecordset cities = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
        //    CityData data = new CityData();
        //    try
        //    {
               
        //        cities.Select("ID, NAME, PROVINCE,SUBPROVINCE");
        //        cities.SetWhere("NAME", IMRecordset.Operation.Like, opora.City);
        //        cities.SetWhere("PROVINCE", IMRecordset.Operation.Like, opora.Area);
        //        if (!IsOblCity(opora.City))
        //        {
        //            if (opora.City != opora.District)
        //            {
        //                cities.SetWhere("SUBPROVINCE", IMRecordset.Operation.Like, opora.District);
        //            }
        //        }
        //        cities.Open();
        //        if (!cities.IsEOF())
        //        {
        //            data.id = cities.GetI("ID");
        //            data.subprovince = cities.GetS("SUBPROVINCE");
        //            data.province = cities.GetS("PROVINCE");
        //            data.name = cities.GetS("NAME");
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        data.id = 0;
        //    }
        //    finally
        //    {
        //        cities.Destroy();
        //    }
        //    return data;
        //}
        ///// <summary>
        ///// Update record in SITE table WE nee to use it after USER choose to update manualy
        ///// </summary>
        ///// <param name="opora">opora data to update</param>
        ///// <param name="selectedID">iD of record to Update</param>
        ///// <returns>true if ok otherwise false</returns>
        //public bool Rewrite(COpora opora, int selectedID)
        //{
        //    IMRecordset site = new IMRecordset("SITE", IMRecordset.Mode.ReadWrite);
        //    bool result = true;
        //    if (opora.Imported)
        //        return result;
        //    try
        //    {
        //       site.Select("ID ,CODE, NAME,CUST_TXT9, CSYS, LATITUDE, LONGITUDE, COUNTRY_ID, ASL,CITY_ID, ADDRESS, CITY, SUBPROVINCE, PROVINCE, REMARK, CUST_TXT6, CUST_TXT7, TOWER, X,Y,DATE_CREATED,CREATED_BY,CUST_TXT3,DATE_MODIFIED,MODIFIED_BY");
        //        site.SetWhere("ID", IMRecordset.Operation.Eq, selectedID);
        //        site.Open();
        //        if (!site.IsEOF())
        //        {
        //                site.Edit(); 
        //                site.Put("NAME", opora.StadNum);
        //                site.Put("CSYS", "4DMS");
        //              //  site.Put("LATITUDE", opora.dmsLat);
        //                site.Put("Y", opora.dmsLat);
        //             //   site.Put("LONGITUDE", opora.dmsLong);
        //                site.Put("X", opora.dmsLong);
        //                site.Put("COUNTRY_ID", "UKR");
        //                site.Put("ASL", opora.SeaHeight);
        //                site.Put("ADDRESS", opora.FullAddress);
        //                site.Put("CITY", opora.City);
        //               CityData data = getCityID(opora);
        //                if (data.id != 0)
        //                {
        //                    site.Put("CITY_ID", data.id);
        //                    site.Put("SUBPROVINCE", data.subprovince);
        //                    site.Put("PROVINCE", data.province);
        //                    string Full = "";
        //                    if (data.province.Length > 0)
        //                    {
        //                        Full += "Область: " + data.province;
        //                    }
        //                    if (data.subprovince.Length > 0)
        //                    {
        //                        Full += " Район:" + data.subprovince;
        //                    }
        //                    if (data.name.Length > 0)
        //                    {
        //                        Full += " Населений пункт:" + data.name;
        //                    }
        //                    Full += " Адреса:" + opora.FullAddress;
        //                    site.Put("REMARK", Full);
        //                }
        //                //imported in case of subprovince
        //                site.Put("CUST_TXT3", opora.Owner);
        //                site.Put("TOWER", opora.StandType);
        //                site.Put("CUST_TXT6", opora.PanelsData);
        //                //site.Put("CUST_TXT7", opora.RemarkData);
        //                site.Put("CUST_TXT9", opora.Enclosure);
        //                site.Put("DATE_MODIFIED", DateTime.Now);
        //                site.Put("MODIFIED_BY", IM.ConnectedUser());
        //                site.Update();
        //                opora.Imported = true;
        //                if (opora.PhotoList.Count > 0)
        //                {
        //                    for (int i = 0; i < opora.PhotoList.Count; i++)
        //                    {
        //                        IMRecordset doclink = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
        //                        doclink.Select("REC_TAB,REC_ID,PATH");
        //                        doclink.SetWhere("REC_TAB", IMRecordset.Operation.Eq, "SITE");
        //                        doclink.SetWhere("REC_ID", IMRecordset.Operation.Eq, selectedID);
        //                        doclink.SetWhere("PATH", IMRecordset.Operation.Eq, opora.PhotoList[i]);
        //                        doclink.Open();
        //                        if (doclink.IsEOF())
        //                        {
        //                            doclink.AddNew();
        //                            doclink.Put("REC_TAB", "SITE");
        //                            doclink.Put("REC_ID", selectedID);
        //                            doclink.Put("PATH", opora.PhotoList[i]);
        //                            doclink.Update();
        //                        }
        //                        doclink.Destroy();
        //                    }
        //                }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        result = false;
        //    }
        //    finally
        //    {
        //        site.Destroy();
        //    }
        //    return result;
        //}
        ///// <summary>
        ///// Check if opora has same data as ICSM 
        ///// </summary>
        ///// <param name="site">opened recordset</param>
        ///// <param name="opora">opora to compare</param>
        ///// <returns></returns>
        //private bool IsTheSame(IMRecordset site, COpora opora)
        //{
        //    bool result =true;
        //    if (site.GetS("NAME") != opora.StadNum)
        //    {
        //        result = false;
        //    }
        //    if (IM.RoundDeci(site.GetD("Y"), 4) != IM.RoundDeci(opora.dmsLat, 4))
        //    {
        //        result = false;
        //    }
        //    if (IM.RoundDeci(site.GetD("X"),4) !=IM.RoundDeci(opora.dmsLong,4))
        //    {
        //        result = false;
        //    }
        //    if (site.GetD("ASL") != opora.SeaHeight)
        //    {
        //        result = false;
        //    }
        //   /* if (site.GetS("ADDRESS") != opora.FullAddress)
        //    {
        //        result = false;
        //    }*/
        //    if (site.GetS("CITY") != opora.City)
        //    {
        //        result = false;
        //    }
        //  /*  if (site.GetS("SUBPROVINCE") != opora.District)
        //    {
        //        result = false;
        //    }
        //    if (site.GetS("PROVINCE") != opora.Area)
        //    {
        //        result = false;
        //    }*/
        //    if (site.GetS("TOWER") != opora.StandType)
        //    {
        //        result = false;
        //    }
        //    if (site.GetS("CUST_TXT3") != opora.Owner)
        //    {
        //        result = false;
        //    }
           
        //    return result;
        //}
        /// <summary>
        /// + After first walk we trying to Update data automaticly
        /// </summary>
        /// <param name="opora">Opora data to update </param>
        /// <param name="selectedId">ID to update</param>
        private void SecondTry(COpora opora, int selectedId)
        {
            if ((opora.Imported == false) && opora.IsTheSame(selectedId))
            {
                opora.Save(selectedId);
                opora.Imported = true;
            }
        }
        /// <summary>
        /// + First walk add new records analize old
        /// </summary>
        /// <param name="opora">Opora data to update </param>
        /// <param name="addnew">if true adding records automatycly</param>
        /// <returns>true if ok otherwise false</returns>
        public  bool ImportOpora(COpora opora, bool addnew)
        {           
            {
                IMRecordset site = new IMRecordset("SITE", IMRecordset.Mode.ReadWrite);
                try
                {
                    site.Select("ID");
                    site.Select("NAME");
                    site.SetWhere("NAME", IMRecordset.Operation.Like, "*" + opora.StadNum + "*");
                    for (site.Open(); !site.IsEOF(); site.MoveNext())
                    {
                        //string temp = site.GetS("NAME");
                        int id = site.GetI("ID");
                        opora.Records.Add(id);
                    }
                }
                finally
                {
                    if (site.IsOpen())
                        site.Close();
                    site.Destroy();
                }
            }
            if ((opora.Records.Count == 0) && addnew)
            {
                int idSite = opora.Save(IM.NullI);
                if (idSite != IM.NullI)
                    opora.Imported = true;
            }
            return true;
        }
    }
}
