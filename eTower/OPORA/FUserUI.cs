﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ICSM;

namespace OPORA
{
    public partial class FUserUI : Form
    {
        //======================================================
        /// <summary>
        /// Перечисление широты/долготы
        /// </summary>
        enum EnumCoordLine
        {
            Lon = 1,   //долгота
            Lat = 2    //широта
        };
        private List<COpora> _oporaList;
        private int _current = 0;
        private string[] _addLinesGrid = new string[] { "Власник", "Область", "Район","Населенний пункт", "Адреса", "Тип вежі", "Висота над рівнем моря"};
        public FUserUI()
        {
            InitializeComponent();
        }
        private void DeleteDefected()
        {
            List<COpora> tmp = new List<COpora>();
            for (int i = 0; i < _oporaList.Count; i++)
            {
                if (!_oporaList[i].Defected)
                {
                    tmp.Add(_oporaList[i]);
                }
            }
            _oporaList = tmp;
        }
        /// <summary>
        /// Инициализация процеса обработки
        /// </summary>
        /// <param name="oporaList">Список станций, которые не импортиловались</param>
        public void Init(List<COpora> oporaList)
        {
            _oporaList = oporaList;
            if ((_oporaList != null) && (_oporaList.Count > 0))
            {
                DeleteDefected();
                if (_oporaList.Count > 0)
                {
                    PutData(0);
                    this.ShowDialog();
                }
            }
        }
        /// <summary>
        /// + Обновление гридав
        /// </summary>
        private void RebuildGrid()
        {
            listOporaView.Columns.Clear();
            ColumnHeader columnHeader0 = new ColumnHeader();
            columnHeader0.Text = "Назва поля";
            columnHeader0.Width = -1;
            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "Дані eTower";
            columnHeader1.Width = -1;
            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Дані ICSM";
            columnHeader2.Width = -1;
            listOporaView.Columns.AddRange(new[] { columnHeader0, columnHeader1, columnHeader2 });
            listOporaView.View = View.Details;

            listOporaView.Items.Clear();
            ListViewItem item = new ListViewItem("Назва опори");

            ListViewItem item1 = new ListViewItem("Довгота");

            ListViewItem item2 = new ListViewItem("Широта");

            listOporaView.Items.AddRange(new[] { item, item1, item2 });
            for (int i = 0; i < _addLinesGrid.Length; i++)
            {
                ListViewItem tempItem = new ListViewItem(_addLinesGrid[i]);
                listOporaView.Items.Add(tempItem);
            }        
        }
        /// <summary>
        /// + Загрузка позиции ICSM
        /// </summary>
        /// <param name="oporaDest">Опора, с которой будем сравнивать</param>
        /// <param name="idCounter">Номер записи в списке подходящих опор</param>
        /// <returns></returns>
        COpora LoadFromIcsm(COpora oporaDest, int idCounter)
        {
            if (oporaDest == null)
            {
                return null;
            }
            else if (oporaDest.Records.Count < idCounter)
            {
                return null;
            }
            else
            {
                COpora opora = new COpora();
                opora.Load(oporaDest.Records[idCounter]);
                return opora;
            }            
        }
        /// <summary>
        /// + Обновляет кол-во записей
        /// </summary>
        private void ShowCurrent()
        {
            this.Text = (_current + 1) + "/" + _oporaList.Count;
        }

        private string DmsToString(double coord, EnumCoordLine line)
        {
            Int64 coordInt = (Int64)((coord + 0.000005) * 100000.0);
            Char[] symbol = new Char[] { '\xB0', '\x27' };
            bool isNegative = false;
            if (coordInt < 0)
            {// Меняем знак
                isNegative = true;
                coordInt = -coordInt;
            }
            // Секунды
            Int64 sec = coordInt % 1000;
            string seconds = "";
            if ((sec % 10) != 0)
            {
                double tmp = (double)sec / 10.0;
                seconds = tmp.ToString(" 00.0");
            }
            else
            {
                double tmp = (double)sec / 10.0;
                seconds = tmp.ToString(" 00");
            }
            seconds += Convert.ToString(symbol[1]) + Convert.ToString(symbol[1]);
            // Минуты
            coordInt = coordInt / 1000;
            string minutes = (coordInt % 100).ToString(" 00") + Convert.ToString(symbol[1]);
            // Градусы
            coordInt = coordInt / 100;
            string degree = (coordInt % 100).ToString("00") + Convert.ToString(symbol[0]);
            if (line == EnumCoordLine.Lon)
            {
                degree += (isNegative) ? "W" : "E";
            }
            else
            {
                degree += (isNegative) ? "S" : "N";
            }
            return degree + minutes + seconds;
        }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="coord"></param>
        ///// <param name="where"></param>
        ///// <returns></returns>
        //private string CoordToString(double coord, string where)
        //{
        //    123
        //    string res = "";
        //    try
        //    {
        //        res = IM.RoundDeci(coord, 4).ToString();
        //        res = res.Replace(",", "°");                
        //        res = res.Insert(5, "'");
        //        res = res + "''";
        //        if (res.Length < 9)
        //        {
        //            res = res + "00";
        //        }
        //        res = res + where;
        //        return res;
        //    }
        //    catch
        //    {
        //        return res;
        //    }
        //}

        private void PutData(int oporaNum)
        {
            if (oporaNum < 0)
                return;
            if (oporaNum < _oporaList.Count)
            {
                _current = oporaNum;
                ShowCurrent();
                COpora opora = _oporaList[oporaNum];
                if (!opora.Defected)
                {
                    if (!opora.IsNew)
                    {
                        
                        COpora icsmOpora = LoadFromIcsm(opora, 0);
                        if (icsmOpora == null)
                            return;

                        RebuildGrid();
                        btnAddUpdate.Text = "Оновити запис";
                        listOporaView.Items[0].BackColor = CompareData(opora.StadNum, icsmOpora.StadNum);
                        listOporaView.Items[0].SubItems.Add(opora.StadNum, Color.Black, CompareData(opora.StadNum, icsmOpora.StadNum), this.Font);
                        listOporaView.Items[0].SubItems.Add(icsmOpora.StadNum, Color.Black, CompareData(opora.StadNum, icsmOpora.StadNum), this.Font);
                        listOporaView.Items[1].BackColor = CompareData(opora.LongDms, icsmOpora.LongDms);
                        listOporaView.Items[1].SubItems.Add(DmsToString(opora.LongDms, EnumCoordLine.Lon), Color.Black, CompareData(opora.LongDms, icsmOpora.LongDms), this.Font);
                        listOporaView.Items[1].SubItems.Add(DmsToString(icsmOpora.LongDms, EnumCoordLine.Lon), Color.Black, CompareData(opora.LongDms, icsmOpora.LongDms), this.Font);
                        listOporaView.Items[2].BackColor = CompareData(opora.LatDms, icsmOpora.LatDms);
                        listOporaView.Items[2].SubItems.Add(DmsToString(opora.LatDms, EnumCoordLine.Lat), Color.Black, CompareData(opora.LatDms, icsmOpora.LatDms), this.Font);
                        listOporaView.Items[2].SubItems.Add(DmsToString(icsmOpora.LatDms, EnumCoordLine.Lat), Color.Black, CompareData(opora.LatDms, icsmOpora.LatDms), this.Font);

                        ////////////////////////////////////////////////////////////////////////////
                        listOporaView.Items[3].BackColor = CompareData(opora.Owner, icsmOpora.Owner);
                        listOporaView.Items[3].SubItems.Add(opora.Owner);
                        listOporaView.Items[3].SubItems.Add(icsmOpora.Owner);

                        listOporaView.Items[4].BackColor = CompareData(opora.Area, icsmOpora.Area);
                        listOporaView.Items[4].SubItems.Add(opora.Area);
                        listOporaView.Items[4].SubItems.Add(icsmOpora.Area);

                        listOporaView.Items[5].BackColor = CompareData(opora.District, icsmOpora.District);
                        listOporaView.Items[5].SubItems.Add(opora.District);
                        listOporaView.Items[5].SubItems.Add(icsmOpora.District);

                        listOporaView.Items[6].BackColor = CompareData(opora.City, icsmOpora.City);
                        listOporaView.Items[6].SubItems.Add(opora.City);
                        listOporaView.Items[6].SubItems.Add(icsmOpora.City);

                        listOporaView.Items[7].BackColor = CompareData(opora.FullAddress, icsmOpora.IcsmAddress);
                        listOporaView.Items[7].SubItems.Add(opora.FullAddress);
                        listOporaView.Items[7].SubItems.Add(icsmOpora.IcsmAddress);

                        listOporaView.Items[8].BackColor = CompareData(opora.StandType, icsmOpora.StandType);
                        listOporaView.Items[8].SubItems.Add(opora.StandType);
                        listOporaView.Items[8].SubItems.Add(icsmOpora.StandType);

                        listOporaView.Items[9].BackColor = CompareData(opora.SeaHeight, icsmOpora.SeaHeight);
                        listOporaView.Items[9].SubItems.Add(opora.SeaHeight.ToString());
                        listOporaView.Items[9].SubItems.Add(icsmOpora.SeaHeight.ToString());
                        
                        
                        listOporaView.Columns[0].Width = listOporaView.Width / 3 - 1;
                        listOporaView.Columns[1].Width = listOporaView.Width / 3 - 1;
                        listOporaView.Columns[2].Width = listOporaView.Width / 3 - 1;
                    }
                    else
                    {
                        RebuildGrid();
                        btnAddUpdate.Text = "Додати запис";
                        listOporaView.Items[0].SubItems.Add(opora.StadNum);
                        listOporaView.Items[1].SubItems.Add(DmsToString(opora.LongDms, EnumCoordLine.Lon));
                        listOporaView.Items[2].SubItems.Add(DmsToString(opora.LatDms, EnumCoordLine.Lat));
                        listOporaView.Items[3].SubItems.Add(opora.Owner);

                        listOporaView.Items[4].SubItems.Add(opora.Area);
                        listOporaView.Items[5].SubItems.Add(opora.District);
                        listOporaView.Items[6].SubItems.Add(opora.City);
                        listOporaView.Items[7].SubItems.Add(opora.FullAddress);                       
                        listOporaView.Items[8].SubItems.Add(opora.StandType);
                        listOporaView.Items[9].SubItems.Add(opora.SeaHeight.ToString());

                        listOporaView.Columns[0].Width = listOporaView.Width / 3;
                        listOporaView.Columns[1].Width = listOporaView.Width / 3;
                        listOporaView.Columns[2].Width = listOporaView.Width / 3;
                    }
                }//not defected
            }
        }
        /// <summary>
        /// Сравнивает два значения
        /// </summary>
        /// <param name="s1">Значение 1</param>
        /// <param name="s2">Значение 2</param>
        /// <returns>Цвет</returns>
        Color CompareData(string s1, string s2)
        {
            Color c = listOporaView.BackColor;
            if (s1 != s2)
                c = Color.LightPink;
            return c;
        }

        /// <summary>
        /// Сравнивает два значения
        /// </summary>
        /// <param name="d1">Значение 1</param>
        /// <param name="d2">Значение 2</param>
        /// <returns>Цвет</returns>
        Color CompareData(double d1, double d2)
        {
            Color c = listOporaView.BackColor;
            if ((Math.Abs(d1 - d2)) > 0.00001)
                c = Color.LightPink;
            return c;
        }

        private void prev_Click(object sender, EventArgs e)
        {
            _current--;
            if (_current > -1)
            {
                if (_current < _oporaList.Count)
                {
                    PutData(_current);
                    return;
                }
            }
            _current++;
        }

        private void next_Click(object sender, EventArgs e)
        {
            _current++;
            if (_current > 0)
            {
                if (_current < _oporaList.Count)
                {
                    PutData(_current);
                    return;
                }
            }
            _current--;
        }

        private void BtnAddUpdateClick(object sender, EventArgs e)
        {
            COpora op = _oporaList[_current];
            if (op.IsNew)
            {
                op.Save(IM.NullI);
            }
            else
            {
                op.Save(op.Records[0]);
            }
            _oporaList.Remove(op);
            if (_oporaList.Count == 0)
            {
                this.Close();
            }
            if (_current > _oporaList.Count)
            {
                _current = _oporaList.Count - 1;
                PutData(_current);
            }
            else if (_current == _oporaList.Count)
            {
                _current--;
                PutData(_current);
            }
            else
            {
                PutData(_current);
            }
        }

    }
}
