﻿using System;
using System.Collections.Generic;
using System.Text;
using ICSM;


namespace OPORA
{
    public class COpora
    {
        struct CityData
        {
            public string Subprovince;
            public string Province;
            public int Id;
            public string Name;
        }

        public const string SiteTableName = "SITE";
        private string _area;
        private string _city;
        private string _district;
        private double _latitudeDms;
        private double _longitudeDms;

        //
        public string StadNum { get; set; } //Название опоры
        public bool Defected { get; set; } //Флаг корректности записис
        public string Adress { get; set; } //Адрес
        public string StreetType { get; set; } //тип улицы
        public string Street { get; set; } //Улица
        public string StandType { get; set; } //тип
        public string Enclosure { get; set; } //Ограждение
        public double Height { get; set; } //Высота
        public double HeightAntMax { get; set; } //Высота антены
        public double SeaHeight { get; set; } //Высота над уровнем моря
        public string Owner { get; set; } //Владелец
        public string IcsmAddress { get; set; } //Адрес ICSM

        public bool Imported { get; set; } //Импортированно

        public List<string> Panels  { get; protected set; } //Панели
        public List<string> Remarks  { get; protected set; } //комментарии
        public List<string> PhotoList  { get; protected set; } //Фотографии
        public List<int> Records  { get; protected set; } //Id записай

        public string Area
        {
            get { return NormalizeArea(); }
            set { _area = value; }
        }
        public string City
        {
            get { return RemoveCityType(); }
            set { _city = value; }
        }
        public string District
        {
            get { return RemoveDistrictType(); }
            set { _district = value; }
        }
        public string FullAddress
        {
            get
            {
                string retVal = StreetType.TrimEnd() + " " + Street.TrimStart();
                if (string.IsNullOrEmpty(Adress) == false)
                    retVal += ", " + Adress;
                return retVal;
            }
        }
        public string RemarkData
        {
            get
            {
                StringBuilder sb = new StringBuilder(100);
                foreach (string remark in Remarks)
                {
                    if(string.IsNullOrEmpty(remark))
                        sb.Append(Environment.NewLine);
                    else
                        sb.Append(remark);
                }
                return sb.ToString();
            }
        }
        public string PanelsData
        {
            get
            {
                StringBuilder sb = new StringBuilder(100);
                foreach (string panel in Panels)
                {
                    if (string.IsNullOrEmpty(panel))
                        sb.Append(Environment.NewLine);
                    else
                        sb.Append(panel);
                }
                return sb.ToString();
            }
        }
        public bool IsNew
        {
            get
            {
                return Records.Count == 0;
            }
        }
        public double LongDec
        {
            get { return IMPosition.Dms2Dec(LongDms); }
        }
        public double LatDec
        {
            get { return IMPosition.Dms2Dec(LatDms); }
        }
        public double LongDms
        {
            get { return _longitudeDms; }
        }
        public double LatDms
        {
            get { return _latitudeDms; }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public COpora()
        {
            _area = "";
            _city = "";
            _district = "";
            _latitudeDms = IM.NullD;
            _longitudeDms = IM.NullD;

            StadNum = "";
            Adress = "";
            StreetType = "";
            StandType = "";
            Enclosure = "";
            Height = IM.NullD;
            HeightAntMax = IM.NullD;
            SeaHeight = IM.NullD;
            Owner = "";
            Street = "";
            IcsmAddress = "";
            Defected = false;
            Imported = false;
            Panels = new List<string>();
            Remarks = new List<string>();
            PhotoList = new List<string>();
            Records = new List<int>();
        }
        /// <summary>
        /// Сохраняет опору
        /// </summary>
        /// <param name="id">ID записи (может быть NULLI)</param>
        /// <returns>ID записи</returns>
        public int Save(int id)
        {
            IMRecordset site = new IMRecordset(SiteTableName, IMRecordset.Mode.ReadWrite);
            try
            {
                site.Select("ID");
                site.Select("CODE");
                site.Select("NAME");
                site.Select("CSYS");
                site.Select("X");
                site.Select("Y");
                site.Select("LATITUDE");
                site.Select("LONGITUDE");
                site.Select("COUNTRY_ID");
                site.Select("CITY_ID");
                site.Select("ASL");
                site.Select("ADDRESS");
                site.Select("CITY");
                site.Select("SUBPROVINCE");
                site.Select("PROVINCE");
                site.Select("REMARK");
                site.Select("TOWER");
                site.Select("CUST_TXT3");
                site.Select("CUST_TXT4");
                site.Select("CUST_TXT6");
                site.Select("CUST_TXT7");
                site.Select("CUST_TXT8");
                site.Select("CUST_TXT9");
                site.Select("CUST_NBR1");
                site.Select("CUST_NBR2");
                site.Select("DATE_CREATED");
                site.Select("CREATED_BY");
                site.SetWhere("ID", IMRecordset.Operation.Eq, id);
                site.Open();
                if (site.IsEOF())
                {
                    site.AddNew();
                    if (id == IM.NullI)
                        id = IM.AllocID(SiteTableName, 1, -1);
                    site.Put("ID", id);
                    site.Put("CODE", id.ToString());
                    site.Put("CREATED_BY", IM.ConnectedUser());
                    site.Put("DATE_CREATED", DateTime.Now);
                }
                else
                {
                    site.Edit();
                }
                site.Put("NAME", StadNum);
                site.Put("CSYS", "4DMS");
                site.Put("Y", LatDms);
                site.Put("X", LongDms);
                site.Put("LATITUDE", LatDec);
                site.Put("LONGITUDE", LongDec);
                site.Put("COUNTRY_ID", "UKR");
                site.Put("ASL", SeaHeight);
                site.Put("CUST_TXT4", StreetType);
                site.Put("CUST_TXT3", Street);
                site.Put("CUST_TXT8", Adress);
                site.Put("ADDRESS", FullAddress);
                site.Put("CITY", City);
                CityData data = GetCityId();
                if (data.Id != IM.NullI)
                {
                    site.Put("CITY_ID", data.Id);
                    site.Put("SUBPROVINCE", data.Subprovince);
                    site.Put("PROVINCE", data.Province);
                    string full = "";
                    if (string.IsNullOrEmpty(data.Province) == false)
                        full += "Область: " + data.Province;
                    if (string.IsNullOrEmpty(data.Subprovince) == false)
                        full += " Район:" + data.Subprovince;
                    if (string.IsNullOrEmpty(data.Name) == false)
                        full += " Населений пункт:" + data.Name;
                    full += " Адреса:" + FullAddress;
                    site.Put("REMARK", full);
                }
                //imported in case of Subprovince
                site.Put("CUST_TXT6", Owner);
                site.Put("TOWER", StandType);
                //site.Put("CUST_TXT6", PanelsData);
                site.Put("CUST_TXT9", Enclosure);
                site.Put("CUST_NBR2", HeightAntMax);
                site.Put("CUST_NBR1", Height);
                site.Update();
            }
            finally
            {
                if (site.IsOpen())
                    site.Close();
                site.Destroy();
            }
            foreach (string photo in PhotoList)
            {
                if (id == IM.NullI)
                    break;
                IMRecordset doclink = new IMRecordset("DOCLINK", IMRecordset.Mode.ReadWrite);
                try
                {
                    doclink.Select("REC_TAB,REC_ID,PATH");
                    doclink.SetWhere("REC_TAB", IMRecordset.Operation.Eq, SiteTableName);
                    doclink.SetWhere("REC_ID", IMRecordset.Operation.Eq, id);
                    doclink.SetWhere("PATH", IMRecordset.Operation.Eq, photo);
                    doclink.Open();
                    if (doclink.IsEOF())
                    {
                        doclink.AddNew();
                        doclink.Put("REC_TAB", SiteTableName);
                        doclink.Put("REC_ID", id);
                        doclink.Put("PATH", photo);
                        doclink.Update();
                    }
                }
                finally
                {
                    if (doclink.IsOpen())
                        doclink.Close();
                    doclink.Destroy();
                }
            }
            return id;
        }
        /// <summary>
        /// Загружаем опору
        /// </summary>
        /// <param name="id">ID записи</param>
        /// <returns>ID записи</returns>
        public int Load(int id)
        {
            IMRecordset site = new IMRecordset(SiteTableName, IMRecordset.Mode.ReadWrite);
            try
            {
                site.Select("ID");
                site.Select("CODE");
                site.Select("NAME");
                site.Select("CSYS");
                site.Select("X");
                site.Select("Y");
                site.Select("LATITUDE");
                site.Select("LONGITUDE");
                site.Select("COUNTRY_ID");
                site.Select("CITY_ID");
                site.Select("ASL");
                site.Select("ADDRESS");
                site.Select("CITY");
                site.Select("SUBPROVINCE");
                site.Select("PROVINCE");
                site.Select("REMARK");
                site.Select("TOWER");
                site.Select("CUST_TXT3");
                site.Select("CUST_TXT4");
                site.Select("CUST_TXT6");
                site.Select("CUST_TXT7");
                site.Select("CUST_TXT8");
                site.Select("CUST_TXT9");
                site.Select("CUST_NBR1");
                site.Select("CUST_NBR2");
                site.Select("DATE_CREATED");
                site.Select("CREATED_BY");
                site.SetWhere("ID", IMRecordset.Operation.Eq, id);
                site.Open();
                if (site.IsEOF())
                {
                    id = IM.NullI;
                }
                else
                {
                    StadNum = site.GetS("NAME");
                    string csys = site.GetS("CSYS");
                    double y = site.GetD("Y");
                    double x = site.GetD("X");
                    PutCoordinate(y, x, csys);
                    SeaHeight = site.GetD("ASL");
                    City = site.GetS("CITY");
                    IcsmAddress = site.GetS("ADDRESS");
                    District = site.GetS("SUBPROVINCE");
                    Area = site.GetS("PROVINCE");
                    Owner = site.GetS("CUST_TXT6");
                    Street = site.GetS("CUST_TXT3");
                    StreetType = site.GetS("CUST_TXT4");
                    StandType = site.GetS("TOWER");
                    Enclosure = site.GetS("CUST_TXT9");
                    HeightAntMax = site.GetD("CUST_NBR2");
                    Height = site.GetD("CUST_NBR1");
                }
            }
            finally
            {
                if (site.IsOpen())
                    site.Close();
                site.Destroy();
            }
            return id;
        }

        /// <summary>
        /// Tring to get city ID
        /// </summary>
        /// <param Name="city">Name of the city</param>
        /// <returns>ID of city record if none returns zero</returns>
        private CityData GetCityId()
        {
            CityData data = new CityData();
            data.Id = IM.NullI;
            IMRecordset cities = new IMRecordset("CITIES", IMRecordset.Mode.ReadOnly);
            try
            {
                cities.Select("ID,NAME,PROVINCE,SUBPROVINCE");
                cities.SetWhere("NAME", IMRecordset.Operation.Like, City);
                cities.SetWhere("PROVINCE", IMRecordset.Operation.Like, Area);
                if (!IsOblCity(City) && (City != District))
                    cities.SetWhere("SUBPROVINCE", IMRecordset.Operation.Like, District);
                cities.Open();
                if (!cities.IsEOF())
                {
                    data.Id = cities.GetI("ID");
                    data.Subprovince = cities.GetS("SUBPROVINCE");
                    data.Province = cities.GetS("PROVINCE");
                    data.Name = cities.GetS("NAME");
                }
            }
            finally
            {
                if (cities.IsOpen())
                    cities.Close();
                cities.Destroy();
            }
            return data;
        }
        /// <summary>
        /// Проверка на обласные центры
        /// </summary>
        /// <param Name="city">Нозвание города</param>
        /// <returns>TRUE - областной центр, иначе FALSE</returns>
        private static bool IsOblCity(string city)
        {
            string[] arr = new[] { "Сімферополь", "Вінниця", "Луцьк", "Дніпропетровськ", "Донецьк", "Житомир", "Ужгород", "Запоріжжя", "Івано-Франківськ", "Київ", "Кіровоград", "Луганськ", "Львів", "Миколаїв", "Одеса", "Полтава", "Рівне", "Суми", "Тернопіль", "Харків", "Херсон", "Хмельницький", "Черкаси", "Чернівці", "Чернігів", "Київ", "Севастополь" };
            for (int i = 0; i < arr.Length; i++)
            {
                if (city == arr[i])
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsTheSame(int idSite)
        {
            bool result = true;
            IMRecordset site = new IMRecordset(SiteTableName, IMRecordset.Mode.ReadWrite);
            try
            {
                site.Select("ID");
                site.Select("NAME");
                site.Select("LATITUDE");
                site.Select("LONGITUDE");
                site.Select("ASL");
                site.Select("CITY");
                site.Select("TOWER");
                site.Select("CUST_TXT3");
                site.SetWhere("ID", IMRecordset.Operation.Eq, idSite);
                site.Open();
                if (!site.IsEOF())
                {
                    if (site.GetS("NAME") != StadNum)
                        result = false;
                    if ((site.GetD("LATITUDE") - LatDec) < 0.000000001)
                        result = false;
                    if ((site.GetD("LONGITUDE") - LongDec) < 0.000000001)
                        result = false;
                    if (site.GetD("ASL") != SeaHeight)
                        result = false;
                    if (site.GetS("CITY") != City)
                        result = false;
                    if (site.GetS("TOWER") != StandType)
                        result = false;
                    if (site.GetS("CUST_TXT3") != Owner)
                        result = false;
                }
            }
            finally
            {
                if (site.IsOpen())
                    site.Close();
                site.Destroy();
            }
            return result;
        }
        /// <summary>
        /// put latitude
        /// </summary>
        /// <param name="latitude">широта</param>
        /// <param name="longitude">долгота</param>
        /// <param name="csys">Тим системы координат</param>
        private void PutCoordinate(double latitude, double longitude, string csys)
        {
            IMPosition oldPos = new IMPosition(longitude, latitude, csys);
            IMPosition newPos = IMPosition.Convert(oldPos, "4DMS");
            _latitudeDms = newPos.Lat;
            _longitudeDms = newPos.Lon;
        }
        /// <summary>
        /// parse latitude from string
        /// </summary>
        /// <param Name="latitudeStr">string from opora coordinats</param>
        public void PutLatDms(string latitudeStr)
        {
            if (latitudeStr.Length < 6)
                return;
            if (latitudeStr[0]=='<')
                return;
            string temp = "";
            int i = 0;
            while (latitudeStr[i] != 'N')
            {
                temp += latitudeStr[i];
                i++;
            }
            double dtmp1 = double.Parse(temp);
            i++;
            temp = "";
            temp += latitudeStr[i]; i++;
            temp += latitudeStr[i]; i++;
            double dtmp2 = double.Parse(temp);

            temp = "";
            temp += latitudeStr[i]; i++;
            temp += latitudeStr[i]; i++;
            double dtmp3 = double.Parse(temp);
            dtmp1 = dtmp1 + (dtmp2 / 100) + (dtmp3 / 10000);
            _latitudeDms = dtmp1;
        }
        /// <summary>
        /// parse Longitude from string
        /// </summary>
        /// <param Name="longitudeStr">string from opora coordinats</param>
        public void PutLongDms(string longitudeStr)
        {
            if (longitudeStr.Length < 6)
                return;
            if (longitudeStr[0]=='<')
                return;
            string temp = "";
            int i = 0;
            while (longitudeStr[i] != 'E')
            {
                temp += longitudeStr[i];
                i++;
            }
            double dtmp1 = double.Parse(temp);

            temp = "";
            i++;
            temp += longitudeStr[i]; i++;
            temp += longitudeStr[i]; i++;
            double dtmp2 = double.Parse(temp);

            temp = "";
            temp += longitudeStr[i]; i++;
            temp += longitudeStr[i]; i++;
            double dtmp3 = double.Parse(temp);
            dtmp1 = dtmp1 + (dtmp2 / 100) + (dtmp3 / 10000);
            _longitudeDms = dtmp1;
        }
        /// <summary>
        /// + Remove from area everything but it`s Name
        /// </summary>
        /// <returns>normal area Name</returns>
        private string NormalizeArea()
        {
            string[] arr = _area.Split(' ');
            if (arr.Length ==2)
            {
                if (arr[0] !="області")
                {
                    arr[0]=arr[0].Replace("ої", "а");
                    return arr[0];
                }
                else
                {
                   arr[1]= arr[1].Replace("ої", "а");
                    return arr[1];
                }
            }
            else if (_area == "Автономної Республіки Крим")
            {
                return "АР Крим";
            }
            else
            {
                return _area;
            }
        }
        /// <summary>
        /// Удаляет тип области
        /// </summary>
        /// <returns>Область</returns>
        private string RemoveDistrictType()
        {
            string[] arr = _district.Split(' ');
            if (arr.Length == 2)
            {
                if (arr.Length > 1)
                {
                    if (arr[0].Length > arr[1].Length)
                        return arr[0];
                    else
                        return arr[1];
                }
                else
                {
                    return _district;
                }
            }
            else if (arr.Length == 1)
            {
                return _district;
            }
            else
            {
                string result = "";
                for (int i = 1; i < arr.Length ; i++)
                {
                    result += arr[i];
                    if (i < arr.Length - 1)
                    {
                        result += " ";
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// + Remove city type
        /// </summary>
        /// <returns>Name of the city</returns>
        private string RemoveCityType()
        {            
            string[] arr = _city.Split(' ');
            if (arr.Length == 2)
            {
                if (arr.Length > 1)
                {
                    if (arr[0].Length > arr[1].Length)
                        return arr[0];
                    else
                        return arr[1];
                }
                else
                {
                    return _city;
                }
            }
            else if (arr.Length == 1)
            {
                return _city;
            }
            else
            {
                string result = "";
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    result += arr[i];
                    if (i < arr.Length - 2)
                    {
                        result += " ";
                    }
                }
                return result;
            }
        }
    }
}
