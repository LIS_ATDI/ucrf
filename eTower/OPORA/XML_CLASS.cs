﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Windows.Forms;
using ICSM;


namespace OPORA
{
    /// <summary>
    /// class that cpontains imported from XML opora file data
    /// </summary>
    class CXML_CLASS
    {
        private string _albumPath;
        private string _fileName;
        private bool _isFinishedCorrectly;
        public List<COpora> OporaList { get; protected set; }

        public CXML_CLASS()
        {
            OporaList = new List<COpora>();
            _isFinishedCorrectly = true;
        }
        /// <summary>
        /// Выполняет процесс обработки XML
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="albumPath">Путь к картинкам</param>
        public bool Execute(string fileName, string albumPath)
        {
            _fileName = fileName;
            _albumPath = albumPath;
            using(Icsm.LisProgressBar pb = new Icsm.LisProgressBar("Обробка даних"))
            {
                pb.SetBig("Загрузка файла...");
                try
                {
                    string tmp1 = "";
                    using (StreamReader srr = new StreamReader(_fileName, Encoding.GetEncoding(1251), false, 512))
                    {
                        tmp1 = srr.ReadToEnd();
                    }
                    XDocument xdoc = XDocument.Parse(tmp1);
                    if (xdoc.Root != null)
                        FillList(xdoc.Root.Element("ROWDATA"));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    _isFinishedCorrectly = false;
                }
            }
            return _isFinishedCorrectly;
        }
        /// <summary>
        /// + Fills list of opora data list
        /// </summary>
        /// <param name="root">Element with rows</param>
        private void FillList(XElement root)
        {
            if (root == null)
                throw new ArgumentException("Can't find element \"ROWDATA\"");

            IMLogFile log = new IMLogFile();
            log.Create(IM.GetWorkspaceFolder() + "\\eTowerImport.log");
            foreach (XElement rowData in root.Elements())
            {
                COpora opora = FillData(rowData, log);
                if (opora != null)
                    OporaList.Add(opora);
            }
            if ((log.Errors > 0) || (log.Warnings > 0))
            {
                log.Display("Помилки під час імпортування даних");
                string msg = string.Format("Під час імпорту даних з файла '{0}' виникли помилки.{1}{1}Продовжувати імпорт?", _fileName, Environment.NewLine);
                if (MessageBox.Show(msg, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.Cancel)
                {
                    _isFinishedCorrectly = false;
                }
            }
            log.Destroy();
        }
        /// <summary>
        /// + конвертируем в UTF
        /// </summary>
        /// <param name="str">Строка в 1251</param>
        /// <returns>Строка в UTF</returns>
        private string UpdateStringToUtf(string str)
        {
            Encoding src = Encoding.GetEncoding(1251);
            Encoding dst = Encoding.Unicode;
            byte[] result = Encoding.Convert(src, dst, Encoding.GetEncoding(1251).GetBytes(str));
            return Encoding.Unicode.GetString(result);
        }
        /// <summary>
        /// Fill Opora data class
        /// </summary>
        /// <param name="rowData">Element with rows</param>
        /// <param name="log"></param>
        /// <returns>COpora object, if something goes wrong it returns null</returns>
        private COpora FillData(XElement rowData, IMLogFile log)
        {
            if (rowData == null)
            {
                if (log != null)
                    log.Error("Error XML element");
                return null;
            }
            try
            {
                COpora op = new COpora();
                XAttribute xatr = rowData.Attribute("STAND_NUM");
                if (xatr != null)
                    op.StadNum = UpdateStringToUtf(xatr.Value);
                else
                    op.Defected = true;

                xatr = rowData.Attribute("AREA");
                if (xatr != null)
                    op.Area = UpdateStringToUtf(xatr.Value);
                else
                    op.Defected = true;

                xatr = rowData.Attribute("ADDRESS");
                if (xatr != null)
                    op.Adress = UpdateStringToUtf(xatr.Value);

                xatr = rowData.Attribute("STREET_TYPE");
                if (xatr != null)
                    op.StreetType = UpdateStringToUtf(xatr.Value);
                
                xatr = rowData.Attribute("STREET");
                if (xatr != null)
                    op.Street = UpdateStringToUtf(xatr.Value);

                xatr = rowData.Attribute("CITY");
                if (xatr != null)
                    op.City = UpdateStringToUtf(xatr.Value);
                
                xatr = rowData.Attribute("DISTRICT");
                if (xatr != null)
                    op.District = UpdateStringToUtf(xatr.Value);
               
                xatr = rowData.Attribute("STAND_TYPE");
                if (xatr != null)
                    op.StandType = UpdateStringToUtf(xatr.Value);
                
                xatr = rowData.Attribute("ENCLOSURE");
                if (xatr != null) 
                    op.Enclosure = UpdateStringToUtf(xatr.Value);
                
                xatr=rowData.Attribute("HEIGHT");
                if (xatr != null)
                {
                    try
                    {
                        op.Height = double.Parse(UpdateStringToUtf(xatr.Value));
                    }
                    catch (Exception e)
                    {
                        if (log != null)
                            log.Error(e.Message);
                    }
                }

                xatr=rowData.Attribute("HEIGHT_ANT_MAX");
                if (xatr != null)
                {
                    try
                    {
                        op.HeightAntMax = double.Parse(UpdateStringToUtf(xatr.Value));
                    }
                    catch (Exception e)
                    {
                        if (log != null)
                            log.Error(e.Message);
                    }
                }

                xatr=rowData.Attribute("HEIGHT_SEA");
                if (xatr != null)
                {
                    try
                    {
                        op.SeaHeight = double.Parse(UpdateStringToUtf(xatr.Value));
                    }
                    catch (Exception e)
                    {
                        if (log != null)
                            log.Error(e.Message);
                    }
                }

                xatr=rowData.Attribute("LATITUDE");
                if(xatr!=null)
                    op.PutLatDms(xatr.Value);
                else
                    op.Defected = true;

                xatr=rowData.Attribute("LONGITUDE");
                if(xatr!=null)
                    op.PutLongDms(xatr.Value);
                else
                    op.Defected = true;

                xatr=rowData.Attribute("OWNER");
                if (xatr != null)
                    op.Owner = UpdateStringToUtf(xatr.Value);
                else
                    op.Defected = true;

                xatr=rowData.Attribute("PHOTOS");
                if (xatr != null)
                {
                    string tmp = UpdateStringToUtf(xatr.Value);
                    string[] separator = {", "};
                    string[] arr = tmp.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string path = _albumPath + "\\" + arr[i];
                        FileInfo fi = new FileInfo(path);
                        if (fi.Exists)
                        {
                            op.PhotoList.Add(path);
                        }
                        else
                        {
                            if (log != null)
                                log.Warning(DateTime.Now.ToShortTimeString() + " " + DateTime.Now.ToShortDateString() +
                                            " Неможливо знайти файл зображення -" + path + " для опори №" + op.StadNum +
                                            "\n");
                        }
                    }
                }

                for (int i = 1; i < 21; i++)
                {
                    xatr = rowData.Attribute("HEIGHT_PANELS" + i);
                    if (xatr != null)
                        op.Panels.Add(xatr.Value);
                }

                for (int i = 1; i < 21; i++)
                {
                    xatr = rowData.Attribute("REMARK" + i);
                    if (xatr != null)
                        op.Remarks.Add(xatr.Value);
                }
                return op;
            }
            catch(Exception e)
            {
                if (log != null)
                    log.Error(e.Message);
                return null;
            }
        }
    }
}
