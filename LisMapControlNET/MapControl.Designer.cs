﻿using System.Drawing;

namespace Lis
{
    partial class MapControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapControl));
            this.axLisMapX = new AxLisMap.AxLisMapX();
            ((System.ComponentModel.ISupportInitialize)(this.axLisMapX)).BeginInit();
            this.SuspendLayout();
            // 
            // axLisMapX
            // 
            this.axLisMapX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axLisMapX.Location = new System.Drawing.Point(0, 0);
            this.axLisMapX.Name = "axLisMapX";
            this.axLisMapX.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axLisMapX.OcxState")));
            this.axLisMapX.Size = new System.Drawing.Size(503, 379);
            this.axLisMapX.TabIndex = 0;
            // 
            // MapControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axLisMapX);
            this.Name = "MapControl";
            this.Size = new System.Drawing.Size(503, 379);
            ((System.ComponentModel.ISupportInitialize)(this.axLisMapX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxLisMap.AxLisMapX axLisMapX;        
    }
}
