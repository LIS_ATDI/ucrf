﻿using System.Drawing;
using System.Windows.Forms;

namespace Lis
{
    public partial class MapControl : UserControl
    {
        public const string FontName = "Arial";
        public const int FontSize = 10;
        public const uint FontColor = 0x00000000;
        public const bool IsBold = true;
        public const bool IsItalic = true;
        public const bool IsUnderline = false;

        public MapControl()
        {
            InitializeComponent();                       
        }

        public void ShowTestMap(int mapNumber)
        {
            axLisMapX.Init();
            axLisMapX.Clear(1, 0);
            axLisMapX.SetCenter(31.25, 48.60);
            axLisMapX.SetScale(1545.0);

            axLisMapX.Font = new Font("Times New Roman", 16, FontStyle.Bold);
            //mapcl.ShowStation(31.25, 48.60, "Bobryjsk", "MegaStation");
            //double[] zone = new double[36]{200, 300, 400, 500, 100, 200, 300, 400, 200, 300,
            // 400, 500, 100, 200, 300, 400, 200, 300, 400, 500, 100, 200, 300, 400, 200, 300,
            // 400, 500, 100, 200, 300, 400, 200, 300, 400, 500};   
            double[] zone = new double[] { 0, 100, 0, 100 };
            double[] zone1 = new double[] { 0, 200, 200, 0 };
            //	mapcl.ShowCoordZone(31.25, 48.60, zone);
            //   mapcl.ShowCoordZone(0, 0, zone);

            axLisMapX.ShowContour(zone, zone1, "Label 1", "Hint for Label 1");

            //axLisMapX.Font = new Font("Arial", 12, FontStyle.Regular);
            //mapcl.ShowLink(31.25, 49.6, 30.25, 47.6, 0, LisMap.MapArrowType.matNone);
            //mapcl.ShowStation(31.25, 49.60, "New York", "MegaStation1");
            //mapcl.ShowStation(30.25, 47.60, "Chicago", "MegaStation2");
            axLisMapX.FitObjects();
        }


        public void Init()
        {
            axLisMapX.Init();
        }

        public void SetCenter(double lon, double lat)
        {
            axLisMapX.SetCenter(lon, lat);
        }

        public void SetScale(double scale)
        {
            axLisMapX.SetScale(scale);
        }

        public void Clear(int layer, int refresh)
        {
            axLisMapX.Clear(layer, refresh);
        }

        public void ShowStation(double lon, double lat, string label, string hint)
        {
            ShowStation(lon, lat, label, hint, FontName, FontSize, FontColor, IsBold, IsItalic, IsUnderline);
        }

        public void ShowStation(double lon, double lat, string label, string hint, string fontName, int fontSize, uint textColor, bool isBold, bool isItalic, bool isUnderline)
        {
            LisMap.ILisMapX2 mapc = ((LisMap.ILisMapX2)axLisMapX.GetOcx());
            mapc.ShowStationEx(lon, lat, label, hint, fontName, fontSize, textColor, isBold, isItalic, isUnderline);
        }

        public void ShowCoordZone(double centLon, double centLat, double[] zone)
        {
            axLisMapX.ShowCoordZone(centLon, centLat, zone);
        }

        public void ShowPoint(double lon, double lat, int colour, int width, LisMap.PointType type, string label, string hint)
        {
            ShowPoint(lon, lat, (uint)colour, width, type, label, hint, FontName, FontSize, FontColor, IsBold, IsItalic, IsUnderline);
        }

        public void ShowPoint(double lon, double lat, uint colour, int width, LisMap.PointType type, string label, string hint, string fontName, int fontSize, uint textColor, bool isBold, bool isItalic, bool isUnderline)
        {
            LisMap.ILisMapX2 mapc = ((LisMap.ILisMapX2)axLisMapX.GetOcx());
            mapc.ShowPointEx(lon, lat, type, width, label, hint, colour, fontName, fontSize, textColor, isBold, isItalic, isUnderline);
        }

        public void ShowLabel(double lon, double lat, string label, string hint, string fontName, int fontSize, uint textColor, bool isBold, bool isItalic, bool isUnderline)
        {
            LisMap.ILisMapX2 mapc = ((LisMap.ILisMapX2)axLisMapX.GetOcx());
            mapc.ShowLabelEx(lon, lat, label, hint, fontName, fontSize, textColor, isBold, isItalic, isUnderline);
        }

        public void RefreshControl()
        {
            axLisMapX.refresh();
        }

        public void ShowContour(double[] lon, double[] lat, string label, string hint)
        {
            axLisMapX.ShowContour(lon, lat, label, hint);
        }

        //public void ShowContour(double[] lon, double[] lat, string label, string hint, string fontName, int fontSize, uint textColor, bool isBold, bool isItalic, bool isUnderline)
        //{
        //    LisMap.ILisMapX2 mapc = ((LisMap.ILisMapX2)axLisMapX.GetOcx());
        //    mapc.ShowContourEx(lon, lat, label, hint, fontName, fontSize, textColor, isBold, isItalic, isUnderline);
        //}

        public void ShowZone(double centLon, double centLat, double[] zone,
                              int width, int color, int style, int objType, int layer)
        {
            axLisMapX.ShowZone(centLon, centLat, zone,
                               width, color, style, objType, layer);
        }
        public void ShowLink(double lon1, double lat1, double lon2, double lat2, int trim, LisMap.MapArrowType mat)
        {
            axLisMapX.ShowLink(lon1, lat1, lon2, lat2, trim, mat);
        }
        public void FitObjects()
        {
            axLisMapX.FitObjects();
        }
        public void CloseMap()
        {
            axLisMapX.CloseMap();
        }
    }
}
