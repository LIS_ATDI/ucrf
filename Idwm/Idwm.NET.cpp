// This is the main DLL file.
#include "stdafx.h"
#include "Idwm.NET.h"

using namespace System;
using namespace System::Collections::Generic;

//=========================================
#define LoadProcAddress(Var, type, hLib, ProcName)          \
{                                                           \
    Var = (type) GetProcAddress(hLib, ProcName);            \
    if(Var == NULL)                                         \
    {                                                       \
        System::String ^LProceName = gcnew System::String(ProcName);    \
        System::String ^message = gcnew System::String("Can't load procedure '" + LProceName + "'");\
        throw gcnew System::Exception(message);             \
    }                                                       \
}
//=========================================
#define DestroyVect(vect)       \
{                               \
    if(vect != NULL)            \
    {                           \
        delete[] vect;          \
        vect = NULL;            \
    }                           \
}

namespace IdwmNET 
{
    //===========================================
    Idwm::Idwm()
    {
		hinst = 0;
	}
	//===========================================
	Idwm::~Idwm()
	{
        if(!hinst)
			::FreeLibrary(hinst);
		hinst = 0;
	}
	//===========================================
	float Idwm::DecToRadian(float decCoord)
	{
		return (float)(decCoord * Math::PI / 180.0);
	}
	//===========================================
	bool Idwm::Init(System::UInt32 initVal)
	{
		if(hinst == 0)
		{
			hinst = ::LoadLibrary(L"IDWM32D.DLL");
			if(hinst)
            {
                LoadProcAddress(_WDBUNIT, PROC1INT, hinst, "WDBUNIT");
                LoadProcAddress(_GEOPLC, PROC2F1C, hinst, "_GEOPLC@12");
                LoadProcAddress(_GEODCM9, PROC3F1L1C2L2F1C1L, hinst, "_GEODCM9@44");
                LoadProcAddress(_GEOVA2, PROC1L1F, hinst, "_GEOVA2@8");
                //-----
                DWORD tmpDW = initVal;
                (_WDBUNIT)(&tmpDW);
			}
		}
		return (hinst != 0);
	}
    //===========================================
    System::String^ Idwm::GetCountry(float rlong, float rlat)
    {
        char cty[4] = {0,0,0,0};
        (_GEOPLC)(&rlong,&rlat,cty);
        cty[3] = 0;
        return gcnew System::String(cty);
    }
    //===========================================
    array<NearestCountryItem^>^ Idwm::GetNearestCountries(float rlong, float rlat, float radius, array<System::String^>^ excludeCountry, unsigned int maxCountries)
    {
        List<NearestCountryItem^>^ retVal = gcnew List<NearestCountryItem^>();
        unsigned int MaxCity = maxCountries;
        float* distanceVect = NULL;
		float* azimuthVect = NULL;
		float* coordVect = NULL;
		char* cityCodeVector = NULL;
		char* excludeCityVect = NULL;
		long maxCity = MaxCity;
        long excludeCityCount = 0;
        long numCity = 0;
        long rest = 0;
		float range = radius;
        try
        {
            const int codeCitySize = 3;
		    distanceVect = new float[MaxCity];
		    azimuthVect = new float[MaxCity];
		    coordVect = new float[MaxCity * 2];
            cityCodeVector = new char[MaxCity * codeCitySize];
            //----
            excludeCityCount = excludeCountry->Length;
            if(excludeCityCount > 0)
                excludeCityVect = new char[excludeCityCount * codeCitySize];
			else
			{
				excludeCityVect = new char[1 * codeCitySize];
				for(int j = 0; j < codeCitySize; j++)
					excludeCityVect[j] = 0;
			}
            for(int i = 0; i < excludeCountry->Length; i++)
            {
                for(int j = 0; j < codeCitySize; j++)
                {
                    System::String^ tmpStr = excludeCountry[i];
                    if(j < tmpStr->Length)
                        excludeCityVect[i * codeCitySize + j] = (char)tmpStr[j];
                    else 
                        excludeCityVect[i * codeCitySize + j] = ' ';
                }
            }
            //----
		    (_GEODCM9)(&rlong, &rlat, &range, &excludeCityCount, excludeCityVect, &maxCity,
				    &numCity, &distanceVect[0], &azimuthVect[0], cityCodeVector, &rest);
		    // Get the coordinates of the calculated closest points.
		    (_GEOVA2)(&numCity, coordVect);
            //-----
            for(int i = 0; i < numCity; i++)
            {
                NearestCountryItem^ item = gcnew NearestCountryItem;
                item->country = gcnew System::String(&cityCodeVector[i * codeCitySize], 0, 3);
                item->distance = distanceVect[i];
                item->azimuth = azimuthVect[i];
                item->rLongitude = coordVect[i * 2 + 0];
                item->rLatitude = coordVect[i * 2 + 1];
                retVal->Add(item);
            }
        }
        finally
        {
            DestroyVect(distanceVect);
            DestroyVect(azimuthVect);
            DestroyVect(coordVect);
            DestroyVect(cityCodeVector);
            DestroyVect(excludeCityVect);
        }
        return retVal->ToArray();
    }
}