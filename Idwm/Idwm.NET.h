// Idwm.NET.h

#pragma once
#include <windows.h>

namespace IdwmNET 
{
    // ������� ���������� "��������� ������"
    public ref class NearestCountryItem
    {
    public:
        System::String^ country;  //��� ������
        float distance;   // ���������
        float azimuth;    // ������
        float rLongitude; // ������� (�������)
        float rLatitude;  // ������ (�������)
    };
    // ������� IDWM
	public ref class Idwm
	{
        typedef bool (__stdcall *PROC1INT)(DWORD*);
        typedef void (__stdcall *PROC2F1C)(float*,float*,char*);
        typedef void (__stdcall *PROC1L1F)(long*,float*);
        typedef void (__stdcall *PROC3F1L1C2L2F1C1L)(float*,float*,float*,long*,char*,long*,long*,float*,float*,char*,long*);
	public:
		// �����������
		Idwm();
		// ����������
		~Idwm();
        // ������������� ������
	    // ��� ������� ���������� ����������� ������� ����� ������� ��������� �������
	    // - �������� ���������� "IDWM32D.DLL"
	    // - ������������� �������
        // - �������������� ��������� "IDWM32D.DLL" (����� ������� "WDBUNIT")
		bool Init(System::UInt32 initVal);
		// ����������� DEC � Radian
		static float DecToRadian(float decCoord);
        // ���������� ������ �� ����������� (���������� � ��������)
        System::String^ GetCountry(float rlong, float rlat);
        array<NearestCountryItem^>^ GetNearestCountries(float rlong, float rlat, float radius, array<System::String^>^ excludeCountry, unsigned int maxCountries);
	private:
		HMODULE hinst;
        PROC1INT _WDBUNIT;
        PROC2F1C _GEOPLC;
        PROC1L1F _GEOVA2;
        PROC3F1L1C2L2F1C1L _GEODCM9;
	};
}
