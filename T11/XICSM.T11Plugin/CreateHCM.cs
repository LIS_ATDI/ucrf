﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ICSM;

namespace XICSM.T11Plugin
{
    class CreateHCM
    {
        /// <summary>
        /// Конструктор для создания файла HCM
        /// </summary>   

        private int TableID;
        private string dirHcm = string.Empty;
        private string fileNameHcm = string.Empty;
        public CreateHCM(int tableID)
        {
            TableID = tableID;
        }

        /// <summary>
        /// Функция перевода файла с расширением HCM
        /// </summary>
        public void MakeHCM()
        {
            //#6427
            {
                List<int> lstId = new List<int>();
                IMRecordset r = new IMRecordset("HCMFIX_MSG", IMRecordset.Mode.ReadOnly);
                try
                {
                    r.Select("ID,SEND_ID,Link.ID");
                    r.SetWhere("SEND_ID", IMRecordset.Operation.Eq, TableID);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        int id = r.GetI("Link.ID");
                        if (lstId.Contains(id) == false)
                            lstId.Add(id);
                    }
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
                foreach (int id in lstId)
                {
                    AddMissingValues(id, "GO", "TX");
                    AddMissingValues(id, "GO", "RX");
                    AddMissingValues(id, "RET", "TX");
                    AddMissingValues(id, "RET", "RX");
                }
            }
            //End #6427
            //Показать модальное окно изменения таблицы
            RecordPtr rec = new RecordPtr("HCMFIX_SENDING", TableID);
            rec.UserEdit();
            //Нахождение файла с расширением HCM                              
            string fullHcm = FinFilesHCM(TableID);
            string outPacketNumber = GetPacketOutNumber(TableID);

            //if (Path.GetDirectoryName(fileNameHcm).ToLower() == "lis")
            //{

            //}

            System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(Path.GetDirectoryName(fullHcm));
            System.IO.DirectoryInfo[] dirs = info.GetDirectories();
            System.IO.FileInfo[] files = info.GetFiles();
            string f_name = Path.GetFileNameWithoutExtension(fullHcm) + ".LIS";
            foreach (FileInfo item in files)
            {

                if (f_name == Path.GetFileName(item.Name))
                {
                    File.Delete(Path.GetDirectoryName(fullHcm) + @"\" + Path.GetFileNameWithoutExtension(item.Name) +".TXT");
                    File.Copy(Path.GetDirectoryName(fullHcm) + @"\" + f_name, Path.GetDirectoryName(fullHcm) + @"\" + Path.GetFileNameWithoutExtension(item.Name) + ".TXT");
                    
                }

            }
            


            ////

            if (File.Exists(fullHcm))
            {
                //Чтение текста из файла и запись обратно в файл измененной строчки             
                using (StreamReader sr = new StreamReader(fullHcm, Encoding.Default))
                {
                    if (File.Exists(Application.LocalUserAppDataPath + @"\" + fileNameHcm)) File.Delete(Application.LocalUserAppDataPath + @"\" + fileNameHcm);
                    String currentLine;
                    using (StreamWriter sw = new StreamWriter(Application.LocalUserAppDataPath + @"\" + fileNameHcm, false, Encoding.Default))
                    {
                        if (!File.Exists(fullHcm))
                            return;
                        bool isFirst = true;
                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            if (isFirst)
                            {
                                isFirst = false;
                                if (string.IsNullOrEmpty(outPacketNumber) == false)
                                {
                                    string[] splittedLine = currentLine.Split(';');
                                    if (splittedLine.Length > 0)
                                        splittedLine[0] = outPacketNumber;
                                    StringBuilder sb = new StringBuilder();
                                    foreach (string s in splittedLine)
                                        sb.Append(";" + s);
                                    currentLine = sb.ToString().Substring(1);
                                }
                            }
                            //Транслитерация построчно...
                            string transliteLine = MakeTranslitString(currentLine);
                            //Запись в файл...
                            sw.WriteLine(transliteLine);
                        }
                    }
                }
                //Удаление старого и создание нового файла 
                if (File.Exists(Application.LocalUserAppDataPath + @"\" + fileNameHcm))
                {
                    if (File.Exists(fullHcm)) File.Delete(fullHcm);
                    File.Move(Application.LocalUserAppDataPath + @"\" + fileNameHcm, fullHcm);
                }
            }
            else
            {
                MessageBox.Show(string.Format("Файл \"{0}\" не знайдено{1}Локалізація не виконана.", fullHcm, Environment.NewLine), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Возвращает исходящий номер пакета
        /// </summary>
        /// <param name="id">ID записи в HCMFIX_SENDING</param>
        /// <returns>Исходящий номер пакета</returns>
        private string GetPacketOutNumber(int id)
        {
            string retVal = "";
            int stationId = IM.NullI;
            {
                IMRecordset r = new IMRecordset("HCMFIX_MSG", IMRecordset.Mode.ReadOnly);
                try
                {
                    r.Select("ID,Sending.ID,Link.Microwave.ID");
                    r.SetWhere("Sending.ID", IMRecordset.Operation.Eq, id);
                    r.OrderBy("ID", OrderDirection.Descending);
                    r.Open();
                    if (!r.IsEOF())
                        stationId = r.GetI("Link.Microwave.ID");
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
            }
            if(stationId != IM.NullI)
            {
                IMRecordset r = new IMRecordset("XNRFA_PAC_TO_APPL", IMRecordset.Mode.ReadOnly);
                try
                {
                    string sqlQuery = "([Application.Microwave.ID] = {0}) OR ";
                    sqlQuery += "([Application.Microwave2.ID] = {0}) OR ";
                    sqlQuery += "([Application.Microwave3.ID] = {0}) OR ";
                    sqlQuery += "([Application.Microwave4.ID] = {0}) OR ";
                    sqlQuery += "([Application.Microwave5.ID] = {0}) OR ";
                    sqlQuery += "([Application.Microwave6.ID] = {0})";
                    string sql = string.Format(sqlQuery, stationId);

                    r.Select("APPL_ID,PACKET_ID,Packet.NUMBER_OUT");
                    r.SetAdditional(sql);
                    r.OrderBy("PACKET_ID", OrderDirection.Descending);
                    r.Open();
                    if (!r.IsEOF())
                        retVal = r.GetS("Packet.NUMBER_OUT");
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
            }
            return retVal;
        }

        private void AddMissingValues(int clinkId, string goret, string entryType)
        {
            IMTransaction.Begin();
            try
            {
                IMRecordset r = new IMRecordset("HCMFIX_STA", IMRecordset.Mode.ReadWrite);
                try
                {
                    r.Select("ID,SERVICE,CAT_USE,EQUIP_MAN_NAME,CHANNEL_SEP,BW,ANT_MAN_NAME");
                    r.SetWhere("CLINK_ID", IMRecordset.Operation.Eq, clinkId);
                    r.SetWhere("GORET", IMRecordset.Operation.Like, goret);
                    r.SetWhere("ENTRY_TYPE", IMRecordset.Operation.Like, entryType);
                    for (r.Open(); !r.IsEOF(); r.MoveNext())
                    {
                        r.Edit();
                        r.Put("SERVICE", "CP");
                        r.Put("CAT_USE", "H");
                        if(string.IsNullOrEmpty(r.GetS("EQUIP_MAN_NAME")))
                            r.Put("EQUIP_MAN_NAME", "DEFAULT");
                        if (string.IsNullOrEmpty(r.GetS("ANT_MAN_NAME")))
                            r.Put("ANT_MAN_NAME", "DEFAULT");
                        if (r.GetD("CHANNEL_SEP") == IM.NullD)
                            r.Put("CHANNEL_SEP", r.GetD("BW"));
                        r.Update();
                    }
                }
                finally
                {
                    if (r.IsOpen())
                        r.Close();
                    r.Destroy();
                }
                IMTransaction.Commit();
            }
            catch
            {
                IMTransaction.Rollback();
            }
        }
        /// <summary>
        /// Нахождение файла HCM в каталоге
        /// </summary>
        /// <param name="dirHcm">Путь к каталогу</param>
        /// <param name="fileCurrentMax">Параметры найденного файла</param>
        /// <returns></returns>
        private string FinFilesHCM(int Id)
        {
            IMRecordset r = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
            try
            {
                r.Select("ITEM,WHAT");
                r.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-NOT-XCF");
                r.Open();
                if (!r.IsEOF())
                    dirHcm = r.GetS("WHAT");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
            //Определение названия файла            
            r = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadOnly);
            r.Select("ID,PATH");
            r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
            r.Open();
            if (!r.IsEOF())
                fileNameHcm = r.GetS("PATH");
            if (r.IsOpen())
                r.Close();
            r.Destroy();

            string fileNameCalculate = CreateT11.GetFileName(Id);

            if (fileNameCalculate != fileNameHcm)
            {
                fileNameHcm = fileNameCalculate;
                //Определение названия файла            
                r = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadWrite);
                r.Select("ID,PATH,FILE_NBR");
                r.SetWhere("ID", IMRecordset.Operation.Eq, Id);
                r.Open();
                if (!r.IsEOF())
                {
                    r.Edit();
                    r.Put("PATH",fileNameCalculate);
                    r.Update();
                }
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }

            return dirHcm + @"\" + fileNameHcm;
        }

        /// <summary>
        /// Транслитерация ряда
        /// </summary>
        /// <param name="inputLine">начальный ряд</param>
        /// <returns>транслитерованный ряд</returns>
        private string MakeTranslitString(string inputLine)
        {
            string[] splittedLine = inputLine.Split(';');
            //Список индексов записей, над которыми необходимо выполнить транслитерацию
            int[] latineIndex = {8,   //Адрес
                                 13,  //Производитель оборудования
                                 14,  //Название оборудования
                                 44,  //Произвродитель антены
                                 45   //Название антены
                                };
            //Поля ДН антенн, прямых и кросс
            HashSet <string> antPatts = new HashSet <string> () {"VV", "VH", "HH", "HV"}; 

            foreach (int index in latineIndex)
            {
                if ((splittedLine.Length > index) && (splittedLine[index] != null))
                    splittedLine[index] = ToLatin(splittedLine[index]);
            }
            if(splittedLine.Length > 4)
            {
                /*
                if (("VV" == splittedLine[splittedLine.Length - 4] && "0" == splittedLine[splittedLine.Length - 3] &&
                     "VH" == splittedLine[splittedLine.Length - 2] && "0" == splittedLine[splittedLine.Length - 1]) ||
                    ("HH" == splittedLine[splittedLine.Length - 4] && "0" == splittedLine[splittedLine.Length - 3] &&
                     "HV" == splittedLine[splittedLine.Length - 2] && "0" == splittedLine[splittedLine.Length - 1]))
                */
                //Если есть поле ДН из нуля элементов, удаляем его. Предполагаем, что ДН исключительно в конце строки.
                //Если прямая ДН из 0 элементов, а кросс - из больше 0, и кросс после прямой, то прямая останется, и будет трабл.
                while ((splittedLine.Length > 2) && antPatts.Contains(splittedLine[splittedLine.Length - 2])
                    && (splittedLine[splittedLine.Length - 1].Trim() == "0" || splittedLine[splittedLine.Length - 1].Trim() == "00"))
                {
                    List<string> listTmp = new List<string>(splittedLine);
                    if(listTmp.Count > 2)
                        listTmp.RemoveRange(listTmp.Count - 2, 2);
                    splittedLine = listTmp.ToArray();
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (string s in splittedLine)
                sb.Append(";" + s);
            return sb.ToString().Substring(1);
        }

        /// <summary>
        /// Транслитерация строки
        /// </summary>
        /// <param name="source">начальная строка</param>
        /// <returns>транслитерованная строка</returns>
        private string ToLatin(string source)
        {
            return LatinPlugin.Transliteration.Translite(source, false);
        }
    }
}
