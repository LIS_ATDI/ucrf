﻿using System;
using System.Collections.Generic;
using ICSM;

namespace XICSM.T11Plugin
{
    /// <summary>
    /// This class is plugin, that create T11 file
    /// </summary>
    public class Plugin : IPlugin
    {

        /// <summary>
        /// Implements field of IPlugin
        /// </summary>
        public string Ident { get { return "T11Plugin"; } }
        public string Description { get { return "get T11 file"; } }
        public double SchemaVersion { get { return 20110803.1751; } }
        public bool OtherMessage(string message, Object inParam, ref Object outParam)
        {
            return false;
        }
        public void GetMainMenu(IMMainMenu mainMenu) { }

        /// <summary>
        /// Create schema for Tables
        /// </summary>
        /// <param name="b"></param>
        public void RegisterBoard(IMBoard b)
        {
            b.RegisterQueryMenuBuilder("HCMFIX_SENDING", OnCreateFileT11);
        }

        /// <summary>
        /// Get list of context menu
        /// </summary>
        /// <param name="tablename">Table name</param>
        /// <param name="nbRecMin">number of selected lines</param>
        /// <returns></returns>
        private List<IMQueryMenuNode> OnCreateFileT11(String tablename, int nbRecMin)
        {
            List<IMQueryMenuNode> lst = new List<IMQueryMenuNode>();
            if (tablename == "HCMFIX_SENDING")
            {
                lst.Add(new IMQueryMenuNode("Create as T11 file...", null, OnCreateT11, IMQueryMenuNode.ExecMode.FirstRecord));
                lst.Add(new IMQueryMenuNode("Create as HCM file...", null, OnCreateHCM, IMQueryMenuNode.ExecMode.FirstRecord));
            }
            return lst;
        }
        /// <summary>
        /// generate event when create NEW record Create as HCM file...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnCreateHCM(IMQueryMenuNode.Context context)
        {
            IMRecordset r = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadWrite);
            r.Select("ID,PATH,FILE_NBR");
            r.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("FILE_NBR", 1);
                r.Update();
            }
            if (r.IsOpen())
                r.Close();
            r.Destroy();

            CreateHCM sampleHCM = new CreateHCM(context.TableId);
            sampleHCM.MakeHCM();
            return true;
        }

        /// <summary>
        /// generate event when create NEW record Create as T11 file...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnCreateT11(IMQueryMenuNode.Context context)
        {

            IMRecordset r = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadWrite);
            r.Select("ID,PATH,FILE_NBR");
            r.SetWhere("ID", IMRecordset.Operation.Eq, context.TableId);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("FILE_NBR", 1);
                r.Update();
            }
            if (r.IsOpen())
                r.Close();
            r.Destroy();

            bool isError = false;
            CreateT11 sampleT11 = new CreateT11(context.TableId);
            {
                string[] errors = sampleT11.CheckRecord();
                isError = (errors.Length > 0);
                if (isError)
                {
                    IMLogFile log = new IMLogFile();
                    try
                    {
                        log.Create("T11Errors.log");
                        foreach (string error in errors)
                            log.Error(string.Format("{0}{1}", error, Environment.NewLine));

                        log.Display("T11 errors");
                    }
                    finally
                    {
                        log.Destroy();
                    }
                }
            }
            if (isError &&
                System.Windows.Forms.MessageBox.Show(
                    "Виявлено відсутність деяких даних. Продовжити формування файла Т11?", "",
                    System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question,
                    System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                isError = false;

            if (isError == false)
                sampleT11.MakeT11();

            return true;
        }

        /// <summary>
        /// Upgrade DataBase
        /// </summary>
        /// <param name="s"></param>
        /// <param name="dbCurVersion"></param>
        /// <returns></returns>
        public bool UpgradeDatabase(IMSchema s, double dbCurVersion)
        {
            if (dbCurVersion < 20110803.1751)
                s.SetDatabaseVersion(20110803.1751);
            return true;
        }
        /// <summary>
        /// Register Schema
        /// </summary>
        /// <param name="s"></param>
        public void RegisterSchema(IMSchema s) { }
    }
}
