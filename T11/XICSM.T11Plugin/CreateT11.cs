﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ICSM;
using System.Globalization;

namespace XICSM.T11Plugin
{
    public class CreateT11
    {
        /// <summary>
        /// Конструктор для создания файла T11
        /// </summary>
        private static int TableId;
        private const string TableName = "HCMFIX_MSG";
        private int countNotice = 0;
        public CreateT11(int tableId)
        {
            TableId = tableId;
            InitRecords();
        }

        private string admSec = string.Empty;

        /// <summary>
        /// Executing function to create T11
        /// </summary>
        public void MakeT11()
        {
            string fileName;
            //Create empty File  *.t11
           // FolderBrowserDialog foldBrowsDial = new FolderBrowserDialog();
            //foldFrowsDial.SelectedPath

            string dir_ = "";
            IMRecordset r_dir = new IMRecordset("SYS_CONFIG", IMRecordset.Mode.ReadOnly);
            try
            {
                r_dir.Select("ITEM,WHAT");
                r_dir.SetWhere("ITEM", IMRecordset.Operation.Eq, "SHDIR-NOT-XCF");
                r_dir.Open();
                if (!r_dir.IsEOF())
                    dir_ = r_dir.GetS("WHAT");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (r_dir.IsOpen())
                    r_dir.Close();
                r_dir.Destroy();
            }


            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.FileName = GetFileName(TableId);            
            saveDlg.Filter = "File T11 (*.txt)|*.txt";
            if (dir_.Trim()!="") { saveDlg.DefaultExt = dir_;}
            if (saveDlg.ShowDialog() != DialogResult.OK)
           // if (foldBrowsDial.ShowDialog() != DialogResult.OK)
                return;
            //Fill t11 file            
            StringBuilder bodyFile = new StringBuilder();

            IMRecordset r = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                foreach (var record in _listRecords)
                    r.Select(record.Key);
                r.SetWhere("Sending.ID", IMRecordset.Operation.Eq, TableId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            int i1 = 0;
            r.Open();

            //Fill T11 file
            try
            {
                bodyFile.AppendLine("<HEAD>");
                bodyFile.AppendLine("t_adm = " + r.GetS("Link.SRC_ADM"));
                bodyFile.AppendLine("t_d_sent =" + ParseDate(r.GetT("Link.REQ_DATE")));
                bodyFile.AppendLine("</HEAD>");

             
                //NOTICE                       
                for (int i = 0; !r.IsEOF(); r.MoveNext())
                {
                    //Detect one direct A->B (without A<->)!!
                    int linkId = r.GetI("CLINK_ID");
                    List<int> TxRx = new List<int>();
                    IMRecordset rCheck = new IMRecordset("HCMFIX_MSG13XYZ", IMRecordset.Mode.ReadOnly);
                    rCheck.Select("ID,CLINK_ID,COMM_ID,O2");
                    rCheck.SetWhere("CLINK_ID", IMRecordset.Operation.Eq, linkId);
                    int Count = 0;
                    try
                    {
                        TxRx.Clear();
                        for (rCheck.Open(); !rCheck.IsEOF(); rCheck.MoveNext())
                        {
                            TxRx.Add(rCheck.GetI("O2"));
                            Count++;
                        }
                    }
                    finally
                    {
                        if (rCheck.IsOpen())
                            rCheck.Close();
                        rCheck.Destroy();
                    }
                    //when tx rx is 3 and 4 then invert station!
                    if (!TxRx.Contains(1))
                    {
                        for (int stations = Count / 2; stations > 0; stations--)
                            CreateTwoDirectNotice(bodyFile, r, stations);
                    }
                    else
                    {
                        for (int stations = 0; stations < Count / 2; stations++)
                            CreateTwoDirectNotice(bodyFile, r, stations);
                    }
                    i1 += i + 1;
                }
                //TILE
                bodyFile.AppendLine("<TAIL>");
                bodyFile.AppendLine("t_num_notices = " + countNotice);
                bodyFile.AppendLine("</TAIL>");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();

            }
            //forming filename...

            fileName = saveDlg.FileName;
            //   fileName = foldBrowsDial.SelectedPath + fileName;
            //Save all records
            if (File.Exists(fileName))
                File.Delete(fileName);
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.Write(bodyFile.ToString());
                MessageBox.Show("Создание файла T11 и его транслитерация произошла успешно!");
            }

            if (Path.GetDirectoryName(fileName) != dir_)
            {
                File.Copy(fileName, dir_ + @"\" + Path.GetFileName(fileName));
            }
            

            
            r = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadWrite);
            r.Select("ID,PATH");
            r.SetWhere("ID", IMRecordset.Operation.Eq, TableId);
            r.Open();
            if (!r.IsEOF())
            {
                r.Edit();
                r.Put("PATH", Path.GetFileName(fileName));
                r.Update();
            }
            if (r.IsOpen())
                r.Close();
            r.Destroy();

        }

        public static string GetFileName(int id)
        {
            string fileName;
            IMRecordset rf = new IMRecordset("HCMFIX_SENDING", IMRecordset.Mode.ReadOnly);
            try
            {
                rf.Select("ID,COUNTRY_ID,COUNTRY_DEST_ID,WRITING_DATE");
                rf.SetWhere("ID", IMRecordset.Operation.Eq, id);
                rf.Open();
                if (!rf.IsEOF())
                {
                    int idField = rf.GetI("ID");
                    string countryID = rf.GetS("COUNTRY_ID");
                    string countryDestID = rf.GetS("COUNTRY_DEST_ID");
                    string date = ParseDate2(rf.GetT("WRITING_DATE"));
                    fileName = "Fix_N_" + countryID + "_" + countryDestID + "_" + date + "_" + idField + ".txt";
                }
                else
                {
                    fileName = "default.txt";
                }
            }
            finally
            {
                if (rf.IsOpen())
                    rf.Close();
                rf.Destroy();
            }
            return fileName;
        }

        /// <summary>
        /// Create records <NOTICE></NOTICE>
        /// </summary>
        /// <param name="bodyFile"></param>
        /// <param name="r"></param>
        /// <param name="direct">First or Second</param>        
        private void CreateTwoDirectNotice(StringBuilder bodyFile, IMRecordset r, int direct)
        {
            countNotice++;
            string station, nstation;
            if (direct == 0)
            {
                station = "StationA";
                nstation = "StationB";
            }
            else
            {
                station = "StationB";
                nstation = "StationA";
            }
            try
            {
                bodyFile.AppendLine("<NOTICE>");
                bodyFile.AppendLine("t_notice_type = " + "T11");
                bodyFile.AppendLine("t_d_adm_ntc =" + ParseDate(DateTime.Now));
                bodyFile.AppendLine("t_fragment = " + "NTFD_RR");
                bodyFile.AppendLine("t_prov = " + "RR11.2");
                //Fill t_action 
                if (r.GetS("Link.REQ_13Y") == "B")
                    bodyFile.AppendLine("t_action = " + "ADD");
                else if (r.GetS("Link.REQ_13Y") == "M")
                    bodyFile.AppendLine("t_action = " + "MODIFY");
                bodyFile.AppendLine("t_is_resub = " + "FALSE");
                admSec = ToLatin(r.GetS("Link.Microwave.OPER_KEY"));
                bodyFile.AppendLine("t_adm_ref_id = " + admSec);
                bodyFile.AppendLine("t_freq_assgn = " + (r.GetD("Link.Microwave." + station + ".TX_FREQ")).ToString(CultureInfo.CreateSpecificCulture("en-US")));
                bodyFile.AppendLine("t_d_inuse = " + ParseDate(r.GetT("Link.BIUSE_TONOT"), true));
                bodyFile.AppendLine("t_ctry = " + r.GetS("Link.SRC_ADM"));

                string ncityName = GetCityName(r, nstation);
                string cityName = GetCityName(r, station);

                bodyFile.AppendLine("t_site_name = " + cityName);

                bodyFile.AppendLine("t_long = " + DoubleTableToDDMMSS(r.GetD("Link.Microwave." + station + ".Position.LONGITUDE"), true));
                bodyFile.AppendLine("t_lat = " + DoubleTableToDDMMSS(r.GetD("Link.Microwave." + station + ".Position.LATITUDE"), false));
                bodyFile.AppendLine("t_stn_cls = " + "FX");
                bodyFile.AppendLine("t_nat_srv = " + "CP");
                bodyFile.AppendLine("t_bdwdth_cde = " + (r.GetS("Link.Microwave.DESIG_EM")).Substring(0, 4));
                bodyFile.AppendLine("t_emi_cls = " + r.GetS("Link.Microwave.DESIG_EM").Substring(4));
                bodyFile.AppendLine("t_site_alt = " + (Convert.ToInt32(r.GetD("Link.Microwave." + station + ".Position.ASL"))).ToString(CultureInfo.CreateSpecificCulture("en-US")));
                double tOpHhFr = r.GetD("Link.Microwave.START_TIME");

                bodyFile.AppendLine("t_op_hh_fr = " + ParseTime(tOpHhFr, true));
                double tOpHhTo = r.GetI("Link.Microwave.STOP_TIME");

                bodyFile.AppendLine("t_op_hh_to = " + ParseTime(tOpHhTo, false));
                bodyFile.AppendLine("t_addr_code = " + "A");
                bodyFile.AppendLine("t_op_agcy = " + "001");
                bodyFile.AppendLine("<ANTENNA>");
                bodyFile.AppendLine("t_pwr_xyz = " + "Y");
                bodyFile.AppendLine("t_pwr_ant = " + (r.GetD("Link.Microwave." + station + ".POWER") - 30.0).ToString(CultureInfo.CreateSpecificCulture("en-US")));
                bodyFile.AppendLine("t_pwr_eiv = " + "I");
                bodyFile.AppendLine("t_pwr_dbw = " + ((r.GetD("Link.Microwave." + station + ".POWER") + r.GetD("Link.Microwave." + station + ".GAIN")) - 30.0).ToString(CultureInfo.CreateSpecificCulture("en-US")));
                bodyFile.AppendLine("t_gain_type = " + "I");
                string tAzmMaxE = (r.GetD("Link.Microwave." + station + ".AZIMUTH")).ToString(CultureInfo.CreateSpecificCulture("en-US"));
                if (tAzmMaxE != string.Empty)
                {
                    bodyFile.AppendLine("t_ant_dir = " + "D");
                    bodyFile.AppendLine("t_azm_max_e = " + tAzmMaxE);
                }
                else
                    bodyFile.AppendLine("t_ant_dir = " + "ND");

                string tBmwdth = r.Get("Link.Microwave." + station + ".Ant1.DIAGH").ToString().ToString(CultureInfo.CreateSpecificCulture("en-US"));
                if (tBmwdth != string.Empty)
                    bodyFile.AppendLine("t_bmwdth = " + tBmwdth);
                else
                    bodyFile.AppendLine("t_bmwdth = " + "2");
                bodyFile.AppendLine("t_elev = " + (r.GetD("Link.Microwave." + station + ".ANGLE_ELEV")).ToString(CultureInfo.CreateSpecificCulture("en-US")));

                string tpolar = r.GetS("Link.Microwave." + station + ".POLAR");
                if (tpolar == string.Empty)
                    tpolar = "V";
                bodyFile.AppendLine("t_polar = " + tpolar);

                bodyFile.AppendLine("t_hgt_agl = " + r.GetD("Link.Microwave." + station + ".AGL1"));
                bodyFile.AppendLine("t_gain_max = " + r.GetD("Link.Microwave." + station + ".GAIN"));
                //RX_STATION
                bodyFile.AppendLine("<RX_STATION>");
                bodyFile.AppendLine("t_ctry = " + r.GetS("Link.SRC_ADM"));
                bodyFile.AppendLine("t_site_name = " + ncityName);
                bodyFile.AppendLine("t_long = " + DoubleTableToDDMMSS(r.GetD("Link.Microwave." + nstation + ".Position.LONGITUDE"), true));
                bodyFile.AppendLine("t_lat = " + DoubleTableToDDMMSS(r.GetD("Link.Microwave." + nstation + ".Position.LATITUDE"), false));
                bodyFile.AppendLine("t_geo_type = " + "POINT");
                bodyFile.AppendLine("</RX_STATION>");
                //RX_STATION
                bodyFile.AppendLine("</ANTENNA>");
                bodyFile.AppendLine("</NOTICE>");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string GetCityName(IMRecordset r, string station)
        {
            string cityName;
            cityName = r.GetS("Link.Microwave." + station + ".Position.CITY");
            cityName = ToLatin(cityName);

            if (cityName.Length > 29)
                cityName = cityName.Remove(29);
            return cityName;
        }

        /// <summary>
        /// Get Date in formats: YYYYMMDD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ParseDate2(DateTime dt)
        {
            string year = dt.Year.ToString();
            string month = dt.Month.ToString();
            string day = dt.Day.ToString();
            while (year.Length < 4)
                year = "0" + year;
            while (month.Length < 2)
                month = "0" + month;
            while (day.Length < 2)
                day = "0" + day;
            return year + month + day;
        }

        /// <summary>
        /// Get Date in formats: YYYY-MM-DD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private string ParseDate(DateTime dt)
        {
            string year = dt.Year.ToString();
            string month = dt.Month.ToString();
            string day = dt.Day.ToString();
            while (year.Length < 4)
                year = "0" + year;
            while (month.Length < 2)
                month = "0" + month;
            while (day.Length < 2)
                day = "0" + day;
            return year + "-" + month + "-" + day;
        }
        /// <summary>
        /// Get Date in formats: YYYY-MM-DD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private string ParseDate(DateTime dt, bool ifNullIsNow)
        {
            int year = dt.Year;
            int month = dt.Month;
            int day = dt.Day;
            if (year == 1 && month == 1 && day == 1)
                return ParseDate(DateTime.Now);
            return ParseDate(dt);
        }


        /// <summary>
        /// Транслитерация строки
        /// </summary>
        /// <param name="source">начальная строка</param>
        /// <returns>транслитерованная строка</returns>
        private string ToLatin(string source)
        {
            return LatinPlugin.Transliteration.Translite(source, false);
        }

        //===================================================
        /// <summary>
        /// Конвертирует DOUBLE в строку
        /// </summary>
        /// <param name="coord">координата в формате Double значения +/-XX.YYYY</param>        
        /// <returns>строку в формате +/-DDMMSS</returns>
        private string DoubleTableToDDMMSS(double coord, bool isLongitude)
        {
            double coordDms = IMPosition.Dec2Dms(coord);
            int convValue = (Convert.ToInt32(coordDms * 10000));
            string convStr;
            if (convValue > 0)
                convStr = convValue.ToString();
            else
                convStr = (convValue.ToString()).Substring(1);
            while (convStr.Length < 6)
                convStr = "0" + convStr;
            if (isLongitude)
                while (convStr.Length < 7)
                    convStr = "0" + convStr;
            if (convValue > 0)
                return "+" + convStr;
            return convStr;
        }

        /// <summary>
        /// Get Time in format HH:MM
        /// </summary>
        /// <param name="time">time in int format</param>
        /// <param name="isStartTime">start/end</param>
        /// <returns>time in string</returns>
        private string ParseTime(double time, bool isStartTime)
        {
            string minute;
            if (time == int.MaxValue || time == int.MinValue || time < 0 || time > 23.59)
                if (isStartTime)
                    time = 0;
                else
                    return "23:59";

            string hour = (Convert.ToInt32(time * 100) / 100).ToString();
            minute = (Convert.ToInt32(time * 100) % 100).ToString();

            while (hour.Length < 2)
                hour = "0" + hour;
            while (minute.Length < 2)
                minute = "0" + minute;

            return hour + ":" + minute;
        }

        #region Check data
        /// <summary>
        /// Тип записи
        /// </summary>
        private enum TypeRecord
        {
            /// <summary>
            /// Целое число
            /// </summary>
            Integer = 0,
            /// <summary>
            /// Число с плавающ. точкой
            /// </summary>
            Real = 1,
            /// <summary>
            /// Строка
            /// </summary>
            String = 3,
            /// <summary>
            /// Дата
            /// </summary>
            Date = 4,
        }
        /// <summary>
        /// Описание записи
        /// </summary>
        private class DescriptionRecord
        {
            public TypeRecord Type { get; set; }
            public bool NeedCheck { get; set; }
            public string OutMessage { get; set; }
        }
        /// <summary>
        /// Список записей для проверки
        /// </summary>
        private Dictionary<string, DescriptionRecord> _listRecords = new Dictionary<string, DescriptionRecord>();
        /// <summary>
        /// Ингициализация записей для проверки
        /// </summary>
        private void InitRecords()
        {
            const string message = "Поле {0} пусте.";
            _listRecords.Add("ID", 
                new DescriptionRecord() { NeedCheck = false, Type = TypeRecord.Integer, OutMessage = message });
            _listRecords.Add("CLINK_ID",
                new DescriptionRecord() { NeedCheck = false, Type = TypeRecord.Integer, OutMessage = message });
            _listRecords.Add("SEND_ID",
                new DescriptionRecord() { NeedCheck = false, Type = TypeRecord.Integer, OutMessage = message });
            _listRecords.Add("Link.SRC_ADM",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.REQ_DATE",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Date, OutMessage = message });
            _listRecords.Add("Link.BIUSE_TONOT",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Date, OutMessage = message });
            _listRecords.Add("Link.REQ_13Y",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.OPER_KEY",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.DESIG_EM",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.START_TIME",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.STOP_TIME",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.TX_FREQ",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.TX_FREQ",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Position.LATITUDE",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Position.LATITUDE",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Position.LONGITUDE",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Position.LONGITUDE",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Position.ASL",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Position.ASL",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.POWER",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.POWER",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.GAIN",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.GAIN",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Ant1.ANT_DIR",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Ant1.ANT_DIR",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Ant1.DIAGH",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Ant1.DIAGH",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.ANGLE_ELEV",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.ANGLE_ELEV",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.POLAR",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.POLAR",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.AGL1",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.AGL1",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.AZIMUTH",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.AZIMUTH",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.Real, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Position.REMARK",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Position.REMARK",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationA.Position.CITY",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Link.Microwave.StationB.Position.CITY",
                new DescriptionRecord() { NeedCheck = true, Type = TypeRecord.String, OutMessage = message });
            _listRecords.Add("Sending.ID",
                new DescriptionRecord() { NeedCheck = false, Type = TypeRecord.Integer, OutMessage = message });
        }
        /// <summary>
        /// Проверка записи на корректность данных
        /// </summary>
        /// <returns>Список ошибок</returns>
        public string[] CheckRecord()
        {
            List<string> retVal = new List<string>();
            IMRecordset rsRecord = new IMRecordset(TableName, IMRecordset.Mode.ReadOnly);
            try
            {
                foreach (var record in _listRecords)
                    rsRecord.Select(record.Key);
                rsRecord.SetWhere("Sending.ID", IMRecordset.Operation.Eq, TableId);
                for(rsRecord.Open(); !rsRecord.IsEOF(); rsRecord.MoveNext())
                {
                    foreach (var record in _listRecords)
                    {
                        if (record.Value.NeedCheck == false)
                            continue;

                        switch (record.Value.Type)
                        {
                            case TypeRecord.Date:
                                {
                                    DateTime val = rsRecord.GetT(record.Key);
                                    if (val == IM.NullT)
                                        retVal.Add(string.Format(record.Value.OutMessage, record.Key));
                                }
                                break;
                            case TypeRecord.Integer:
                                {
                                    int val = rsRecord.GetI(record.Key);
                                    if (val == IM.NullI)
                                        retVal.Add(string.Format(record.Value.OutMessage, record.Key));
                                }
                                break;
                            case TypeRecord.Real:
                                {
                                    double val = rsRecord.GetD(record.Key);
                                    if (val == IM.NullD)
                                        retVal.Add(string.Format(record.Value.OutMessage, record.Key));
                                }
                                break;
                            case TypeRecord.String:
                                {
                                    string val = rsRecord.GetS(record.Key);
                                    if (string.IsNullOrEmpty(val))
                                        retVal.Add(string.Format(record.Value.OutMessage, record.Key));
                                }
                                break;
                        }
                    }
                }
            }
            finally
            {
                if (rsRecord.IsOpen())
                    rsRecord.Close();
                rsRecord.Destroy();
            }
            return retVal.ToArray();
        }
        #endregion
    }
}
