﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using ICSM;

namespace XICSM.LatinPlugin
{

    /// <summary>
    /// Class that convert text from DB
    /// </summary>
    public class Transliteration
    {
        /// <summary>
        /// This fields incapsulate data from DB(private)
        /// </summary>
        private static Dictionary<string, string> data = null;
        private static Dictionary<string, string> dataAdd = null;

        /// <summary>
        /// Read all data from DB
        /// </summary>
        static Transliteration()
        {
            FirstFillDate();
        }

        private static void FirstFillDate()
        {
            data = new Dictionary<string, string>();
            dataAdd = new Dictionary<string, string>();
            //Fill Dicitionary
            IMRecordset r = null;
            try
            {
                r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadOnly);
                r.Select("BASE,TRANS,TRANSADD");
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                {
                    string key = r.GetS("BASE");
                    if (!data.ContainsKey(key))
                        data.Add(r.GetS("BASE"), r.GetS("TRANS"));
                    if (!dataAdd.ContainsKey(key))
                        dataAdd.Add(r.GetS("BASE"), r.GetS("TRANSADD"));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        /// <summary>
        /// this function translate the whole text
        /// </summary>
        /// <param name="inStr">input text</param>
        /// <param name="allLetterLower">ask convert text to all lower symbols</param>
        /// <returns>translated text</returns>
        public static string Translite(string inStr, bool allLetterLower)
        {
           FirstFillDate();
            if (allLetterLower)
                inStr = inStr.ToLower();

            string outStr = string.Empty;
            string[] words = inStr.Split(' ');

            // Translate words
            for (int i = 0; i < words.Length; i++)
            {
                string parsed = ParseWord(words[i]);
                outStr += parsed + " ";
            }
            return outStr.Trim();
        }

        /// <summary>
        /// this function translate the word of the text
        /// </summary>
        /// <param name="inpStr">input word</param>
        /// <returns>translated word</returns>
        private static string ParseWord(string inpStr)
        {
            string wordOut = string.Empty;
            int position = 0;
            while (position < inpStr.Length)
            {
                bool replace = false;
                for (int arrSymb = inpStr.Length - position; arrSymb > 0; arrSymb--)
                {
                    string wordCheck = inpStr.Substring(position, arrSymb);
                    if (data.ContainsKey(wordCheck))
                    {
                        replace = true;
                        if (position == 0 && dataAdd[wordCheck] != string.Empty)
                            wordOut += dataAdd[wordCheck];
                        else
                            wordOut += data[wordCheck];
                        position += arrSymb - 1;
                        break;
                    }
                }
                if (!replace)
                    wordOut += inpStr[position];
                position += 1;
            }

            return wordOut;
        }
    }
}
