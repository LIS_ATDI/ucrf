﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ICSM;

namespace XICSM.LatinPlugin
{
    /// <summary>
    /// This class dynamically load to ICS Manager
    /// </summary>
    public class Plugin : IPlugin
    {

        /// <summary>
        /// Implements field of IPlugin
        /// </summary>
        public string Ident { get { return "LatinPlugin"; } }
        public string Description { get { return "Translate text!"; } }
        public double SchemaVersion { get { return 20110423.1359; } }
        public void GetMainMenu(IMMainMenu mm) { }
        public bool OtherMessage(string message, Object inParam, ref Object outParam) { return false; }

        /// <summary>
        /// Register submenu for table "X_TRANSLITERATION"
        /// </summary>
        /// <param name="board"></param>
        public void RegisterBoard(IMBoard board) { board.RegisterQueryMenuBuilder("X_TRANSLITERATION", ONReg); }

        /// <summary>
        /// Create context menu
        /// </summary>
        /// <param name="tablename">Tables name</param>
        /// <param name="nbRecMin">selected rows</param>
        /// <returns>List with Menu elements</returns>
        private List<IMQueryMenuNode> ONReg(String tablename, int nbRecMin)
        {
            List<IMQueryMenuNode> lst = new List<IMQueryMenuNode>();
            if (tablename == "X_TRANSLITERATION")
            {
                lst.Add(new IMQueryMenuNode("New record...", null, OnNew, IMQueryMenuNode.ExecMode.Table));
                if (nbRecMin == 1)
                    lst.Add(new IMQueryMenuNode("Edit record...", null, OnEdit, IMQueryMenuNode.ExecMode.FirstRecord));
                lst.Add(new IMQueryMenuNode("Statistics...", null, OnStat, IMQueryMenuNode.ExecMode.Table));
                if (nbRecMin >= 1)
                    lst.Add(new IMQueryMenuNode("Delete record...", null, OnDelete, IMQueryMenuNode.ExecMode.SelectionOfRecords));
                lst.Add(new IMQueryMenuNode("Translit", null, OnTrans, IMQueryMenuNode.ExecMode.Table));
            }
            return lst;
        }

        /// <summary>
        /// Invokes when select New record...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnNew(IMQueryMenuNode.Context context)
        {
            FormTranslit frmTransl = new FormTranslit();
            frmTransl.ShowDialog();
            return true;
        }
        /// <summary>
        /// Invokes when select Edit record...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnEdit(IMQueryMenuNode.Context context)
        {
            FormTranslit frmTransl = new FormTranslit(context.TableId);
            frmTransl.ShowDialog();
            return true;
        }

        /// <summary>
        /// Invokes when select Statistics...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnStat(IMQueryMenuNode.Context context)
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadWrite);
                r.Select("ID");
                int count = 0;
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    count++;
                MessageBox.Show("Count of records is: " + count);
            }
            catch { MessageBox.Show("Can't count statistics!"); }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                }
                IM.RefreshQueries("X_TRANSLITERATION");
            }
            return true;
        }
        /// <summary>
        /// Invokes when select Delete record...
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnDelete(IMQueryMenuNode.Context context)
        {
            IMRecordset r = null;
            try
            {
                r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadWrite);
                r.Select("ID");
                r.AddSelectionFrom(context.DataList, IMRecordset.WhereCopyOptions.SelectedLines);
                for (r.Open(); !r.IsEOF(); r.MoveNext())
                    r.Delete();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                if (r != null)
                {
                    r.Close();
                    r.Destroy();
                }
                IM.RefreshQueries("X_TRANSLITERATION");
            }
            return true;
        }

        /// <summary>
        /// Invokes when select Translit
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool OnTrans(IMQueryMenuNode.Context context)
        {
            FormTranslateText ftt = new FormTranslateText();
            ftt.ShowDialog();
            return true;
        }

        /// <summary>
        /// Creates schema of the tables
        /// </summary>
        /// <param name="schema"></param>
        public void RegisterSchema(IMSchema schema)
        {
            schema.DeclareTable("X_TRANSLITERATION", "Transliteration", "EMPLOYEE");
            schema.DeclareField("ID", "NUMBER(9,0)", null, "NOTNULL", null);
            schema.DeclareIndex("PK_X_TRANSLITER", "PRIMARY", "ID");
            schema.DeclareField("BASE", "VARCHAR(100)", null, null, null);
            schema.DeclareField("TRANS", "VARCHAR(100)", null, null, null);
            schema.DeclareField("TRANSADD", "VARCHAR(100)", null, null, null);
        }
        /// <summary>
        /// Upgrade DataBase
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="dbCurVerion">current version</param>
        /// <returns></returns>
        public bool UpgradeDatabase(IMSchema schema, double dbCurVerion)
        {
            if (dbCurVerion < 20110423.1359)
            {
                schema.CreateTables("X_TRANSLITERATION");
                schema.SetDatabaseVersion(20110423.1359);
            }
            return true;
        }
    }
}