﻿using System;
using System.Windows.Forms;
using XICSM.UcrfRfaNET;

namespace XICSM.LatinPlugin
{
    /// <summary>
    /// This class represent Form for translate the text
    /// </summary>

    public partial class FormTranslateText : Form
    {
        public FormTranslateText()
        {
            InitializeComponent();
            this.Text = CLocaliz.TxT("Translate the text");
            label1.Text = CLocaliz.TxT("OriginalText");
            label2.Text = CLocaliz.TxT("TranslatedText");
            btnOK.Text = CLocaliz.TxT("Ok");
            btnCancel.Text = CLocaliz.TxT("Cancel");
             
                
        }
        /// <summary>
        /// Implement event Click on Ok Button
        /// Translate text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {           
            //Show translate
            txtTranslText.Text = Transliteration.Translite(txtOrigText.Text,  false);            
        }
        /// <summary>
        /// Implement event Click on Cancel Button
        /// Close form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
