﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ICSM;
using XICSM.UcrfRfaNET;


namespace XICSM.LatinPlugin
{
    /// <summary>
    /// This class represent Form for editing BD
    /// </summary>
    public partial class FormTranslit : Form, INotifyPropertyChanged
    {
        private bool Editable { get; set; }
        private int table_ID { get; set; }
        private String sBaseOrig = string.Empty;

        /// <summary>
        /// Constructor for Edit mode
        /// </summary>
        /// <param name="Id">Number of Id row, that can be edited</param>
        public FormTranslit(int Id)
            : this()
        {
            table_ID = Id;
            Editable = true;
            IMRecordset r = null;
            try
            {
                r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadWrite);
                r.Select("ID,BASE,TRANS,TRANSADD");
                r.SetWhere("ID", IMRecordset.Operation.Eq, table_ID);
                r.Open();
                STextBase = r.GetS("BASE");
                sBaseOrig = STextBase;
                STextTranslate = r.GetS("TRANS");
                STextTranslateAdd = r.GetS("TRANSADD");
                this.Text = CLocaliz.TxT("Editing record")+"...";
                
            }
            catch (Exception ex) { MessageBox.Show("Can't edit record! " + ex.Message); }
            finally
            {
                r.Close();
                r.Destroy();
            };
        }
        /// <summary>
        /// Default constructor
        /// </summary>
        public FormTranslit()
        {
            InitializeComponent();
            this.Text = CLocaliz.TxT("Creating new record") + "...";
            lblBase.Text = CLocaliz.TxT("Base word");
            lblTranslate.Text = CLocaliz.TxT("Translate word");
            lblTranslAdd.Text = CLocaliz.TxT("Translate word on  begin of the word");
            btnOK.Text = CLocaliz.TxT("Ok");
            btnCancel.Text = CLocaliz.TxT("Cancel");
        }
        /// <summary>
        /// Implements INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        private string stextBase;
        public string STextBase
        {
            get { return stextBase; }
            set
            {
                if (value != stextBase)
                {
                    stextBase = value;
                    NotifyPropertyChanged("STextBase");
                }
            }
        }

        private string stextTranslate;
        public string STextTranslate
        {
            get { return stextTranslate; }
            set
            {
                if (value != stextTranslate)
                {
                    stextTranslate = value;
                    NotifyPropertyChanged("STextTranslate");
                }
            }
        }

        private string stextTranslateAdd;
        public string STextTranslateAdd
        {
            get { return stextTranslateAdd; }
            set
            {
                if (value != stextTranslateAdd)
                {
                    stextTranslateAdd = value;
                    NotifyPropertyChanged("STextTranslateAdd");
                }
            }
        }

        /// <summary>
        /// Load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormTranslit_Load(object sender, EventArgs e)
        {
            //binding data
            txtBase.DataBindings.Add("Text", this, "STextBase");
            textTrans.DataBindings.Add("Text", this, "STextTranslate");
            txtBxTranslAdd.DataBindings.Add("Text", this, "STextTranslateAdd");
            if (!Editable)
            {
                STextBase = string.Empty;
                STextTranslate = string.Empty;
                STextTranslateAdd = string.Empty;
            }
        }
        /// <summary>
        /// OK Button Press 
        /// Save all changes in DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //Creating new Records...
            if (!Editable)
            {
                bool isAdd = true;
                IMRecordset r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadOnly);
                r.Select("ID,BASE");
                r.SetWhere("BASE", IMRecordset.Operation.Eq, STextBase);
                r.Open();
                if (!r.IsEOF())
                {
                    DialogResult = System.Windows.Forms.DialogResult.None;
                    isAdd = false;
                }
                if (r.IsOpen())
                    r.Close();
                r.Destroy();

                if (isAdd)
                    AddNewRecord(STextBase, STextTranslate,STextTranslateAdd);
                else
                    MessageBox.Show("Value \"" + STextBase + "\" is allready exists in DB");
            }
            //Edititing record...
            else
            {
                bool isAdd = false;
                IMRecordset r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadOnly);
                r.Select("ID,BASE,TRANS");
                r.SetWhere("BASE", IMRecordset.Operation.Eq, STextBase);
                r.Open();
                if (!r.IsEOF() && r.GetI("ID") != table_ID)
                {
                    STextBase = sBaseOrig;
                    isAdd = true;
                }
                if (!r.IsOpen())
                    r.Close();
                r.Destroy();
                if (!isAdd)                
                    EditRecord(STextBase, STextTranslate,STextTranslateAdd);                
                else
                    MessageBox.Show("Value \"" + STextBase + "\" is allready exists in DB");                
            }            
        }

        private void EditRecord(string origText, string translText, string translTextAdd)
        {
            IMRecordset r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadWrite);
            r.Select("ID,BASE,TRANS,TRANSADD");
            r.SetWhere("ID", IMRecordset.Operation.Eq, table_ID);
            try
            {
                r.Open();
                r.Edit();
                r["BASE"] = origText;
                r["TRANS"] = translText;
                r["TRANSADD"] = translTextAdd;
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

        private void AddNewRecord(string origText, string translText, string translTextAdd)
        {
            IMRecordset r = new IMRecordset("X_TRANSLITERATION", IMRecordset.Mode.ReadWrite);
            r.Select("ID,BASE,TRANS,TRANSADD");
            r.SetWhere("ID", IMRecordset.Operation.Eq, -1.0);
            try
            {
                r.Open();
                r.AddNew();
                r["ID"] = IM.AllocID("X_TRANSLITERATION", 1, -1);
                r["BASE"] = origText;
                r["TRANS"] = translText;
                r["TRANSADD"] = translTextAdd;
                r.Update();
            }
            finally
            {
                if (r.IsOpen())
                    r.Close();
                r.Destroy();
            }
        }

       
    }
}
