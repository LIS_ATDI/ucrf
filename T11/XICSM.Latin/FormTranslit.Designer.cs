﻿namespace XICSM.LatinPlugin
{
    partial class FormTranslit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBase = new System.Windows.Forms.TextBox();
            this.textTrans = new System.Windows.Forms.TextBox();
            this.lblBase = new System.Windows.Forms.Label();
            this.lblTranslate = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblTranslAdd = new System.Windows.Forms.Label();
            this.txtBxTranslAdd = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtBase
            // 
            this.txtBase.Location = new System.Drawing.Point(198, 7);
            this.txtBase.MaxLength = 100;
            this.txtBase.Name = "txtBase";
            this.txtBase.Size = new System.Drawing.Size(280, 20);
            this.txtBase.TabIndex = 1;
            // 
            // textTrans
            // 
            this.textTrans.Location = new System.Drawing.Point(198, 33);
            this.textTrans.MaxLength = 100;
            this.textTrans.Name = "textTrans";
            this.textTrans.Size = new System.Drawing.Size(280, 20);
            this.textTrans.TabIndex = 2;
            // 
            // lblBase
            // 
            this.lblBase.AutoSize = true;
            this.lblBase.Location = new System.Drawing.Point(12, 14);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(57, 13);
            this.lblBase.TabIndex = 3;
            this.lblBase.Text = "Base word";
            // 
            // lblTranslate
            // 
            this.lblTranslate.AutoSize = true;
            this.lblTranslate.Location = new System.Drawing.Point(12, 40);
            this.lblTranslate.Name = "lblTranslate";
            this.lblTranslate.Size = new System.Drawing.Size(77, 13);
            this.lblTranslate.TabIndex = 4;
            this.lblTranslate.Text = "Translate word";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(117, 86);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(248, 86);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblTranslAdd
            // 
            this.lblTranslAdd.AutoSize = true;
            this.lblTranslAdd.Location = new System.Drawing.Point(12, 63);
            this.lblTranslAdd.Name = "lblTranslAdd";
            this.lblTranslAdd.Size = new System.Drawing.Size(180, 13);
            this.lblTranslAdd.TabIndex = 8;
            this.lblTranslAdd.Text = "Translate word on  begin of the word";
            // 
            // txtBxTranslAdd
            // 
            this.txtBxTranslAdd.Location = new System.Drawing.Point(198, 60);
            this.txtBxTranslAdd.MaxLength = 100;
            this.txtBxTranslAdd.Name = "txtBxTranslAdd";
            this.txtBxTranslAdd.Size = new System.Drawing.Size(280, 20);
            this.txtBxTranslAdd.TabIndex = 7;
            // 
            // FormTranslit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 108);
            this.Controls.Add(this.lblTranslAdd);
            this.Controls.Add(this.txtBxTranslAdd);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTranslate);
            this.Controls.Add(this.lblBase);
            this.Controls.Add(this.textTrans);
            this.Controls.Add(this.txtBase);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(489, 146);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(489, 146);
            this.Name = "FormTranslit";
            this.Text = "FormTranslit";
            this.Load += new System.EventHandler(this.FormTranslit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBase;
        private System.Windows.Forms.TextBox textTrans;
        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.Label lblTranslate;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTranslAdd;
        private System.Windows.Forms.TextBox txtBxTranslAdd;
    }
}