﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Distribution
{
   public class L525
   {
      //===========================================================
      /// <summary>
      /// Возвращает результат расчета L525 (Free Space)
      /// </summary>
      /// <param name="f">частота передатчика[ГГц]</param>
      /// <param name="d">расстояние между приемником и передатчиком [км]</param>                 
      /// <returns>Расчет L525 (Free Space)</returns> 
      public static double GetL525(double f, double d)
      {
         double l525 = 92.45 + 20 * Math.Log10(f * d);
         return l525;
      }
   }

   public class HATA
   {
      /* Розрахункова функція Окамура-Хата, подібна до моделі
       * ITU-1546
       * Функція працює на частотах від 30 МГц до 2500 МГц (?)
       * 
       * Вхідні дані:
       *   f - частота, МГц
       *   d = довжина, км
       *   h1 - висота базової станції на поверхнею землі, м
       *   h2 - висота абонентскої станції на поверхнею землі, м
       *   
       * Результат:
       *   Ослаблення на прольоті, дБ.
       *   
       *   Допускається можливість переставити h1 і h2 - це не впливатиме на результат
       *   
       *   Функція оптимізована для розрахунків УДЦР, і дещо відрізняється 
       *      від традиційної:
       *      
       *   1) Додається врахування відсотку часу (10 дб)
       *   2) Використання функції допускається для висот абоненту, що перевищують 10 м,
       *      Власна модель фукнції, для висот абонентсів, що вище 10м.
       *   3) Для відстані менше 15 км - використовується ITU-525
       *   4) Якщо відстань = 0, послаблення 60 дБ.
       */
      public static double GetL(double f, double d, double h1, double h2)
      {
         // Підготовка даних.
         h1 = Math.Max(Math.Min(h1, 1000.0), 1.0);
         h2 = Math.Max(Math.Min(h2, 1000.0), 1.0);

         if (h1<h2)
         {
            double hh = h1;
            h1 = h2;
            h2 = hh;            
         }        
        
         if (f<30.0 || f>2500.0)
         {
            return 0.0;
         }
                  
         d = Math.Abs(d);

         if (d>1000.0)
         {                     
            return 1000.0;
         }

         double H2_ = Math.Min(25.0, h2);        
         double Alpha = (1.1 * Math.Log10(f) - 0.7) * H2_ - (1.56 * Math.Log10(f) - 0.8);

         if (h2>25.0)         
            h1 = h1 + h2 - 25;
                  
         double H1_ = h1 / Math.Sqrt(1+ 0.000007*h1*h1);
         double B = 1;

         if (d>20.0)
         {
            B = 1+(0.14 + 0.000187*f+0.00107*H1_)*Math.Pow(Math.Log10(0.05*d),0.8);
         }
         
         // Напруженість поля для 1кВт передавача.
         double E = 69.82 - 6.16*Math.Log10(f)+13.82*Math.Log10(h1) + 
            Alpha - (44.9 - 6.55*Math.Log10(h1))*Math.Pow(Math.Log10(d), B);
         
         // Ослаблення на прольтах для 50% часу на сухопутній трасі.
         double Lb = 139.3 - E + 20.0*Math.Log10(f);              
         double LB_ = Lb - 10.0;

         // Ослаблення по ITU-525
         double L0 = 32.5 + 20.0*Math.Log10(d)+20*Math.Log10(f);
         double L = Math.Max(LB_, L0);
         
         return L;
      }
   }
}
